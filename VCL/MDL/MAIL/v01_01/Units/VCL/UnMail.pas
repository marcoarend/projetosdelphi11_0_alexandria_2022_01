unit UnMail;

interface

uses dmkGeral, dmkImage, Forms, Controls, Windows, SysUtils, ComCtrls, Classes,
  StdCtrls, UnDmkProcFunc, ExtCtrls, UnInternalConsts, mySQLDbTables, DmkDAC_PF,
  Dialogs, UnDmkEnums, IdPOP3, IdIMAP4, IdSSLOpenSSL, Variants, DB, UnTreeView,
  IdExplicitTLSClientServerBase, IdMessage, IdMessageCollection, IdText,
  IdCoderHeader, IdAttachment;

type
  TFlagEmailMsg = (istLido, istNaoLido, istSinalizado, istNaoSinalizado);
  TTipoConta = (istNenhum, istPOP3, istIMAP4);
  TUnMail = class(TObject)
  private
    function  ConfiguraTipoConta(Tipo: Integer): TTipoConta;
  public
    procedure InsereEmailsListView(ListView: TListView; Codigo, Lido,
                Sinalizado, Anexos: Integer; De, Assunto, UID: String;
                Index: Integer);
    procedure ReopenEmailGer(QueryEmailGer: TmySQLQuery; Database: TmySQLDatabase;
                Codigo: Integer);
    procedure ReopenEmailGerDi(QueryEmailGerDi: TmySQLQuery;
                Database: TmySQLDatabase; Codigo, MailConta: Integer);
    procedure ReopenEmailGerMs(ListView: TListView; QueryEmailGerMs: TmySQLQuery;
                Database: TmySQLDatabase; Diretorio: String; MailConta,
                Codigo: Integer);
    procedure ReopenEmailGerCP(QueryEmailGerCP: TmySQLQuery;
                Database: TmySQLDatabase; Codigo: Integer);
    procedure DesconectaMailServer(IMAP: TIdIMAP4; POP: TIdPOP3;
                var TipoConexao: TTipoConta);
    procedure AtualizaDiretoriosIMAP(QueryUpd, QueryEmailGerDi: TmySQLQuery;
                DataBase: TmySQLDatabase; IMAP: TIdIMAP4; MailConta: Integer;
                Separador: String);
    (*
    procedure ConfiguraDiretorios(DirPadrao: String; var DirMsg,
                DirHeader: String);
    *)
    function  AtualizaEmailDB(MailCfg: Integer; Diretorio: String;
                Query, QueryUpd: TmySQLQuery; DataBase: TmySQLDatabase;
                IMAP: TIdIMAP4; Progress: TProgressBar): Integer;
    function  TraduzNomeMailDir(DirName: String): String;
    function  VerificaDirEspecial(DirName, Separador: String): Integer;
    //function  ObtemCodigoDirEspecial(DirName: String): Integer;
    function  ConectaContaEmail(MailCfg: Integer; Query: TmySQLQuery;
                DataBase: TmySQLDatabase; POP: TIdPOP3; IMAP: TIdIMAP4;
                IdSSL: TIdSSLIOHandlerSocketOpenSSL): TTipoConta;
    function  ConfiguraTextoFlag(Flag: TIdMessageFlagsSet): String;
    function  LocalizaMsgDB(IDMsg, De, Assunto, DataHora: String;
                QrEmailGerMs: TmySQLQuery; Database: TmySQLDatabase;
                var CodMsg: Integer; var Diretorio: String): Boolean;
    //IMAP
    function  CriaPastaEmail_IMAP(MailCfg: Integer; Diretorio: String; QueryDir,
                QueryLoc: TmySQLQuery; DataBase: TmySQLDatabase;
                IMAP: TIdIMAP4): Boolean;
    function  CriaSubPastaEmail_IMAP(PastaCod: Integer; Separador: String;
                QueryDir, QueryLoc: TmySQLQuery; DataBase: TmySQLDatabase;
                IMAP: TIdIMAP4): Boolean;
    function  ExcluiPastaEmail_IMAP(PastaCod: Integer; QueryDir,
                QueryLoc: TmySQLQuery; DataBase: TmySQLDatabase;
                IMAP: TIdIMAP4): Boolean;
    function  RenomeiaPastaEmail_IMAP(CodDir: Integer; PastaAtual: String;
                QueryDir, QueryLoc: TmySQLQuery; DataBase: TmySQLDatabase;
                IMAP: TIdIMAP4): Boolean;
    function  AlteraFlagEmailMsg(Flag: TFlagEmailMsg; UID: String;
                IMAP: TIdIMAP4): Boolean;
    function  InsereEmailDB(UID, Diretorio: String; MailCfg: Integer; Query,
                QueryUpd: TmySQLQuery; DataBase: TmySQLDatabase; Msg: TIdMessage;
                Flag: TIdMessageFlagsSet; ListView: TListView; var De,
                Assunto: String; var Data: TDateTime; var Lido,
                Sinalizado, Anexos: Integer): Integer;
    procedure SalvaEmailNosItensEnviados(MailCfg: Integer; Query: TmySQLQuery;
                Database: TmySQLDatabase; IMAP: TIdIMAP4;
                IdSSL: TIdSSLIOHandlerSocketOpenSSL; Msg: TIdMessage;
                LabelAviso1: TLabel = nil; LabelAviso2: TLabel = nil);
  end;

var
  UMail: TUnMail;

implementation

uses UMySQLModule, UnMyObjects, MyDBCheck, UnMLAGeral, UnGrl_Consts, UnMailAllOS;

{ TUnMail }

function TUnMail.RenomeiaPastaEmail_IMAP(CodDir: Integer; PastaAtual: String;
  QueryDir, QueryLoc: TmySQLQuery; DataBase: TmySQLDatabase;
  IMAP: TIdIMAP4): Boolean;

  function ConfiguraCaminhoDir(CaminhoDir: String): String;
  var
    Lista: TStringList;
    Txt: String;
    I: Integer;
  begin
    Lista := Geral.Explode(CaminhoDir, IMAP.MailBoxSeparator, 0);
    Txt   := '';
    //
    for I := 0 to Lista.Count - 1 do
    begin
      if I < Lista.Count - 1 then
        Txt := Txt + Lista[I] + IMAP.MailBoxSeparator;
    end;
    Result := Txt;
  end;

var
  CaminhoDB, CaminhoNew, Txt, TxtMsg: String;
begin
  Result := False;
  //
  if not QueryDir.Locate('Codigo', CodDir, []) then
  begin
    Geral.MB_Aviso('Diret�rio n�o localizado!');
    Exit;
  end;
  if QueryDir.FieldByName('DirEspecial').AsInteger <> 0 then
  begin
    Geral.MB_Aviso('O diret�rio "' + PastaAtual + '" n�o pode ser renomeado!');
    Exit;
  end;
  Txt := PastaAtual;
  if InputQuery('Nome do diret�rio', 'Nome do diret�rio', Txt) then
  begin
    if MyObjects.FIC(Txt = '', nil, 'Defina o nome do diret�rio!') then Exit;
    if MyObjects.FIC(not UTreeView.TreeView_VerificaCaracteresEspeciais(Txt, IMAP.MailBoxSeparator, TxtMsg), nil, TxtMsg) then Exit;
    //
    CaminhoDB  := QueryDir.FieldByName('Nome').AsString;
    CaminhoNew := ConfiguraCaminhoDir(CaminhoDB) + Txt;
    //
    if MyObjects.FIC(not UTreeView.TreeView_VerificaSeDiretorioExiste(CaminhoNew,
      'Nome', 'emailgerdi', '', QueryLoc, DataBase, TxtMsg), nil, TxtMsg) then Exit;
    //
    if IMAP.RenameMailBox(CaminhoDB, CaminhoNew) then
      Result := True
    else
      Geral.MB_Aviso('Falha ao renomear diret�rio!');
  end;
end;

function TUnMail.AlteraFlagEmailMsg(Flag: TFlagEmailMsg; UID: String;
  IMAP: TIdIMAP4): Boolean;
begin
  Result := False;
  //
  if IMAP.Connected then
  begin
    case Flag of
      istLido:
        Result := IMAP.UIDStoreFlags(UID, sdAdd, [mfSeen]);
      istNaoLido:
        Result := IMAP.UIDStoreFlags(UID, sdRemove, [mfSeen]);
      istSinalizado:
        Result := IMAP.UIDStoreFlags(UID, sdAdd, [mfFlagged]);
      istNaoSinalizado:
        Result := IMAP.UIDStoreFlags(UID, sdRemove, [mfFlagged]);
    end;
  end;
end;

function TUnMail.InsereEmailDB(UID, Diretorio: String; MailCfg: Integer; Query,
  QueryUpd: TmySQLQuery; DataBase: TmySQLDatabase; Msg: TIdMessage;
  Flag: TIdMessageFlagsSet; ListView: TListView; var De, Assunto: String;
  var Data: TDateTime; var Lido, Sinalizado, Anexos: Integer): Integer;

  procedure InsereTextoEmailDB(Codigo, Tipo: Integer; ContentType, Texto: String);
  var
    Controle: Integer;
  begin
    Controle := UMyMod.BuscaNovoCodigo_Int(QueryUpd, 'emailgercp', 'Controle',
                [], [], stIns, 0, siPositivo, nil);
    //
    UMyMod.SQLInsUpd(QueryUpd, stIns, 'emailgercp', False,
      ['ContentType', 'Texto', 'Tipo', 'Codigo'], ['Controle'],
      [ContentType, Texto, Tipo, Codigo], [Controle], True);
  end;

var
  MsgExiste: Boolean;
  CodMsgAtu, Codigo, J, Tipo: Integer;
  MsgID, DataHora, Remetente, ResponderA,
  Destinatarios, CCLista, BccLista, CodDirAtu: String;
begin
  Result   := 0;
  Tipo     := -1;
  MsgID    := Msg.MsgId;
  Assunto  := Msg.Subject;
  DataHora := Geral.FDT(Msg.Date, 109);
  Data     := Msg.Date;
  //
  DecodeAddresses(Msg.FromList.EMailAddresses, Msg.FromList);
  DecodeAddress(Msg.Sender);
  DecodeAddresses(Msg.ReplyTo.EMailAddresses, Msg.ReplyTo);
  DecodeAddresses(Msg.Recipients.EMailAddresses, Msg.Recipients);
  DecodeAddresses(Msg.CCList.EMailAddresses, Msg.CCList);
  DecodeAddresses(Msg.BccList.EMailAddresses, Msg.BccList);
  //
  De            := Msg.FromList.EMailAddresses;
  Remetente     := Msg.Sender.Text;
  ResponderA    := Msg.ReplyTo.EMailAddresses;
  Destinatarios := Msg.Recipients.EMailAddresses;
  CCLista       := Msg.CCList.EMailAddresses;
  BccLista      := Msg.BccList.EMailAddresses;
  //
  if mfSeen in Flag then
    Lido := 1
  else
    Lido := 0;
  //
  if mfFlagged in Flag then
    Sinalizado := 1
  else
    Sinalizado := 0;
  //
  //Atualiza no banco de dados
  MsgExiste := LocalizaMsgDB(MsgID, De, Assunto, DataHora, Query,
                 Database, CodMsgAtu, CodDirAtu);
  //
  if MsgExiste = False then //N�o achou a mensagem
  begin
    Anexos := 0;
    Codigo := UMyMod.BuscaNovoCodigo_Int(QueryUpd, 'emailgerms', 'Codigo',
                [], [], stIns, 0, siPositivo, nil);
    //
    if Msg.MessageParts.Count > 0 then
    begin
      for J := 0 to Msg.MessageParts.Count - 1 do
      begin
        if Msg.MessageParts[J] is TIdAttachment then
          Anexos := Anexos + 1;
      end;
    end;
    //
    if UMyMod.SQLInsUpd(QueryUpd, stIns, 'emailgerms', False,
      ['De', 'Remetente', 'ResponderA', 'Destinatarios', 'CCLista',
      'BccLista', 'Assunto', 'DataHora', 'UID', 'IDMsg',
      'MailConta', 'Diretorio', 'Lido', 'Sinalizado', 'Anexos'], ['Codigo'],
      [De, Remetente, ResponderA, Destinatarios, CCLista,
      BccLista, Assunto, DataHora, UID, MsgID,
      MailCfg, Diretorio, Lido, Sinalizado, Anexos], [Codigo], True) then
    begin
      //Obtem texto
      if Msg.MessageParts.Count > 0 then
      begin
        for J := 0 to Msg.MessageParts.Count - 1 do
        begin
          if Msg.MessageParts[J] is TIdText then //Textos
          begin
            Tipo := 0;
            //
            InsereTextoEmailDB(Codigo, Tipo, LowerCase(Msg.MessageParts[J].ContentType),
              DecodeHeader(TIdText(Msg.MessageParts[J]).Body.Text));
          end else
          if Msg.MessageParts[J] is TIdAttachment then  //Anexos
          begin
            Tipo := 1;
            //
            InsereTextoEmailDB(Codigo, Tipo, LowerCase(Msg.MessageParts[J].ContentType),
              Msg.MessageParts[J].FileName)
          end else
            Geral.MB_Aviso('Conte�do do e-mail ' + UID + ' desconhecido!');
        end;
      end else
      begin
        Tipo := 0;
        //
        InsereTextoEmailDB(Codigo, Tipo, Msg.ContentType, DecodeHeader(Msg.Body.Text));
      end;
      Result  := Codigo;
    end;
  end else
  begin
    UMyMod.SQLInsUpd(QueryUpd, stUpd, 'emailgerms', False,
      ['De', 'Remetente', 'ResponderA', 'Destinatarios', 'CCLista', 'BccLista',
      'Assunto', 'DataHora', 'UID', 'IDMsg', 'MailConta', 'Diretorio',
      'Lido', 'Sinalizado'], ['Codigo'],
      [De, Remetente, ResponderA, Destinatarios, CCLista, BccLista,
      Assunto, DataHora, UID, MsgID, MailCfg, Diretorio,
      Lido, Sinalizado], [CodMsgAtu], True);
    //
    De      := '';
    Assunto := '';
    Data    := 0;
    //
    Result := CodMsgAtu;
  end;
end;

procedure TUnMail.ReopenEmailGer(QueryEmailGer: TmySQLQuery;
  Database: TmySQLDatabase; Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryEmailGer, Database, [
    'SELECT Codigo, Nome, DataHoraSinc, DownDias, ',
    'VerifiMail, MailBoxSeparator ',
    'FROM emailconta ',
    'WHERE TipoConta = 1 ',
    'AND PermisNiv IN (0, ' + Geral.FF0(VAR_USUARIO) + ') ',
    'AND SincEmail = 1 ',
    'ORDER BY Nome ',
    '']);
  if Codigo <> 0 then
    QueryEmailGer.Locate('Codigo', Codigo, []);
end;

procedure TUnMail.ReopenEmailGerCP(QueryEmailGerCP: TmySQLQuery;
  Database: TmySQLDatabase; Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryEmailGerCP, Database, [
    'SELECT * ',
    'FROM emailgercp ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
end;

function TUnMail.LocalizaMsgDB(IDMsg, De, Assunto, DataHora: String;
  QrEmailGerMs: TmySQLQuery; Database: TmySQLDatabase; var CodMsg: Integer;
  var Diretorio: String): Boolean;
begin
  Diretorio := '';
  CodMsg    := 0;
  Result    := False;
  //
  if QrEmailGerMs.RecordCount > 0 then
  begin
    if QrEmailGerMs.Locate('IDMsg; De; Assunto; DataHora', VarArrayOf([IDMsg, De, Assunto, DataHora]), []) then
    begin
      Diretorio := QrEmailGerMs.FieldByName('Diretorio').AsString;
      CodMsg    := QrEmailGerMs.FieldByName('Codigo').AsInteger;
      Result    := True;
    end;
  end;
end;

function TUnMail.AtualizaEmailDB(MailCfg: Integer; Diretorio: String;
  Query, QueryUpd: TmySQLQuery; DataBase: TmySQLDatabase; IMAP: TIdIMAP4;
  Progress: TProgressBar): Integer;
var
  MsgExiste: Boolean;
  I, J, CodMsgAtu, Codigo: Integer;
  MsgID, Assunto, DataHora, De, Remetente, ResponderA, Destinatarios,
  CCLista, BccLista, Texto, CodDirAtu: String;
  Msgs: TIdMessageCollection;
  Mensagens: TStringList;
begin
  Result := 0;
  //
  if IMAP.SelectMailBox(Diretorio) then
  begin
    Progress.Position := 0;
    Progress.Max      := IMAP.MailBox.TotalMsgs;
    //
   {$IfDef VER_BEFORE_BERLIN}
   Msgs      := TIdMessageCollection.Create(TIdMessageItem);
   {$Else}
   Msgs      := TIdMessageCollection.Create();
   {$EndIf}
    Mensagens := TStringList.Create;
    try
      IMAP.KeepAlive;
      IMAP.UIDRetrieveAllEnvelopes(Msgs);
      //
      for I := 0 to Msgs.Count - 1 do
      begin
        MsgID    := Msgs[I].MsgId;
        Assunto  := DecodeHeader(Msgs[I].Subject);
        DataHora := Geral.FDT(Msgs[I].Date, 109);
        //
        DecodeAddresses(Msgs[I].FromList.EMailAddresses, Msgs[I].FromList);
        DecodeAddress(Msgs[I].Sender);
        DecodeAddresses(Msgs[I].ReplyTo.EMailAddresses, Msgs[I].ReplyTo);
        DecodeAddresses(Msgs[I].Recipients.EMailAddresses, Msgs[I].Recipients);
        DecodeAddresses(Msgs[I].CCList.EMailAddresses, Msgs[I].CCList);
        DecodeAddresses(Msgs[I].BccList.EMailAddresses, Msgs[I].BccList);
        //
        De            := Msgs[I].FromList.EMailAddresses;
        Remetente     := Msgs[I].Sender.Text;
        ResponderA    := Msgs[I].ReplyTo.EMailAddresses;
        Destinatarios := Msgs[I].Recipients.EMailAddresses;
        CCLista       := Msgs[I].CCList.EMailAddresses;
        BccLista      := Msgs[I].BccList.EMailAddresses;
        Texto         := '';
        //
        //Atualiza no banco de dados
        MsgExiste := LocalizaMsgDB(MsgID, De, Assunto, DataHora, Query,
                       Database, CodMsgAtu, CodDirAtu);
        //
        if MsgExiste = False then //N�o achou a mensagem
        begin
          //Obtem texto
          if Msgs[I].MessageParts.TextPartCount > 0 then
          begin
            for J := 0 to Msgs[I].MessageParts.Count - 1 do
            begin
              if Msgs[I].MessageParts[J] is TIdText then
                Texto := Texto + TIdText(Msgs[I].MessageParts[J]).Body.Text
              else if Msgs[I].MessageParts[J] is TIdAttachment then
                   
            end;
            //
            Mensagens.Add(Texto);
          end
          else
            Mensagens.Add(DecodeHeader(Msgs[I].Body.Text));
          //
          Texto := Mensagens.Text;
          //
          Codigo := UMyMod.BuscaNovoCodigo_Int(QueryUpd, 'emailgerms', 'Codigo',
                      [], [], stIns, 0, siPositivo, nil);
          //
          UMyMod.SQLInsUpd(QueryUpd, stIns, 'emailgerms', False,
            ['De', 'Remetente', 'ResponderA', 'Destinatarios', 'CCLista',
            'BccLista', 'Assunto', 'Texto', 'DataHora', 'IDMsg',
            'MailConta', 'Diretorio'], ['Codigo'],
            [De, Remetente, ResponderA, Destinatarios, CCLista,
            BccLista, Assunto, Texto, DataHora, MsgID,
            MailCfg, Diretorio], [Codigo], True);
          //
          Result := Codigo;
        end else
        if CodDirAtu = Diretorio then //Atualiza o diret�rio
        begin
          UMyMod.SQLInsUpd(QueryUpd, stUpd, 'emailgerms', False,
            ['De', 'Remetente', 'ResponderA', 'Destinatarios', 'CCLista',
            'BccLista', 'Assunto', 'DataHora', 'IDMsg',
            'MailConta', 'Codigo'], ['Codigo'],
            [De, Remetente, ResponderA, Destinatarios, CCLista, BccLista,
            Assunto, DataHora, MsgID,
            MailCfg, Diretorio], [CodMsgAtu], True);
            //
            Result := CodMsgAtu;
        end;
        //
        Progress.Position := Progress.Position + 1;
        Progress.Update;
        Application.ProcessMessages;
      end;
      Msgs.Free;
      Mensagens.Free;
      //
    except
      Geral.MB_Aviso('Falha ao verificar e-mail!');
      //
      Msgs.Free;
      Mensagens.Free;
    end;
    //
    Progress.Position := 0;
  end;
end;

function TUnMail.ConectaContaEmail(MailCfg: Integer; Query: TmySQLQuery;
  DataBase: TmySQLDatabase; POP: TIdPOP3; IMAP: TIdIMAP4;
  IdSSL: TIdSSLIOHandlerSocketOpenSSL): TTipoConta;
var
  Tipo: TTipoConta;
  Porta, SSL, TLSUse, TLSTipo: Integer;
  Server, Usuario, Senha: String;
begin
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
      'SELECT ReceTipo, ReceServer, ',
      'RecePorta_Mail, ReceLogi_SSL, ReceTLSUse, ReceTLSTipo, ',
      'Usuario, AES_DECRYPT(Password, "' + CO_RandStrWeb01 + '") Password ',
      'FROM emailconta ',
      'WHERE Codigo=' + Geral.FF0(MailCfg),
      '']);
    if Query.RecordCount > 0 then
    begin
      Tipo    := ConfiguraTipoConta(Query.FieldByName('ReceTipo').AsInteger);
      Porta   := Query.FieldByName('RecePorta_Mail').AsInteger;
      SSL     := Query.FieldByName('ReceLogi_SSL').AsInteger;
      Server  := Query.FieldByName('ReceServer').AsString;
      Usuario := Query.FieldByName('Usuario').AsString;
      Senha   := Query.FieldByName('Password').AsString;
      TLSUse  := Query.FieldByName('ReceTLSUse').AsInteger;
      TLSTipo := Query.FieldByName('ReceTLSTipo').AsInteger;
      //
      if (Tipo = istPOP3) and (POP = nil) then
        Exit
      else
      if (Tipo = istIMAP4) and (IMAP = nil) then
        Exit;
      //
      if IMAP <> nil then
      begin
        if IMAP.Connected then
          IMAP.Disconnect();
      end;
      if POP <> nil then
      begin
        if POP.Connected then
          POP.Disconnect;
      end;
      //
      if (Server = '') or (Usuario = '') or (Senha = '')  then
      begin
        Result := istNenhum;
        Exit;
      end;
      case Tipo of
        istPOP3:
        begin
          if POP.Connected = True then
          begin
            Result := istPOP3;
            Exit;
          end;
          //
          POP.Host     := Server;
          POP.Port     := Porta;
          POP.Username := Usuario;
          POP.Password := Senha;
          //
          UMailAllOS.ConfiguraConexaoSSL(POP, IdSSL, SSL, TLSUse, TLSTipo);
          //
          POP.Connect;
          //
          Result := istPOP3;
        end;
        istIMAP4:
        begin
          if IMAP.Connected = True then
          begin
            Result := istIMAP4;
            Exit;
          end;
          //
          IMAP.Host     := Server;
          IMAP.Port     := Porta;
          IMAP.Username := Usuario;
          IMAP.Password := Senha;
          //
          UMailAllOS.ConfiguraConexaoSSL(IMAP, IdSSL, SSL, TLSUse, TLSTipo);
          //
          IMAP.Connect();
          //
          Result := istIMAP4;
        end;
        istNenhum:
          Result := istNenhum;
      end;
    end;
  except
    Result := istNenhum;
  end;
end;

function TUnMail.ConfiguraTipoConta(Tipo: Integer): TTipoConta;
begin
  case Tipo of
     0: Result := istPOP3;
     1: Result := istIMAP4;
    -1: Result := istNenhum;
  end;
end;

function TUnMail.CriaSubPastaEmail_IMAP(PastaCod: Integer; Separador: String;
  QueryDir, QueryLoc: TmySQLQuery; DataBase: TmySQLDatabase; IMAP: TIdIMAP4): Boolean;
var
  MailCfg: Integer;
  Pasta: String;
begin
  Result := False;
  //
  if QueryDir.Locate('Codigo', PastaCod, []) then
  begin
    MailCfg   := QueryDir.FieldByName('MailConta').AsInteger;
    Pasta     := QueryDir.FieldByName('Nome').AsString;
    //
    if MyObjects.FIC(VerificaDirEspecial(Pasta, Separador) <> 0, nil,
      'N�o � poss�vel criar um subdiret�rio � partir deste diret�rio!') then Exit;
    //
    Result := CriaPastaEmail_IMAP(MailCfg, Pasta, QueryDir, QueryLoc, DataBase, IMAP);
  end else
    Geral.MB_Aviso('Falha ao localizar diret�rio de origem!');
end;

function TUnMail.CriaPastaEmail_IMAP(MailCfg: Integer; Diretorio: String;
  QueryDir, QueryLoc: TmySQLQuery; DataBase: TmySQLDatabase; IMAP: TIdIMAP4): Boolean;
const
  Msg = 'Falha ao criar diret�rio!';
var
  Caminho, Txt, TxtMsg: String;
begin
  Result := False;
  //
  if QueryDir.Locate('Nome', Diretorio, [loCaseInsensitive]) or (Diretorio = '') then
  begin
    if InputQuery('Nome do diret�rio', 'Nome do diret�rio', Txt) then
    begin
      if Diretorio = '' then
        Caminho := Txt
      else
        Caminho := QueryDir.FieldByName('Nome').AsString + IMAP.MailBoxSeparator + Txt;
      //
      if MyObjects.FIC(Txt = '', nil, 'Defina o nome do diret�rio!') then Exit;
      if MyObjects.FIC(not UTreeView.TreeView_VerificaCaracteresEspeciais(Txt,
        IMAP.MailBoxSeparator, TxtMsg), nil, TxtMsg) then Exit;
      if MyObjects.FIC(not UTreeView.TreeView_VerificaSeDiretorioExiste(Caminho,
        'Nome', 'emailgerdi', 'AND Codigo=' + Geral.FF0(MailCfg), QueryLoc,
        DataBase, TxtMsg), nil, TxtMsg) then Exit;
      //
      if not IMAP.CreateMailBox(Caminho) then
      begin
        //CPanel => Cria as pastas dentro do inbox
        if Diretorio = '' then
        begin
          if QueryDir.Locate('Nome', 'inbox', [loCaseInsensitive]) then
          begin
            Caminho := QueryDir.FieldByName('Nome').AsString + IMAP.MailBoxSeparator + Txt;
            //
            if not IMAP.CreateMailBox(Caminho) then
            begin
              Geral.MB_Aviso(Msg);
              Exit;
            end else
              Result := True;
          end else
          begin
            Geral.MB_Aviso(Msg);
            Exit;
          end;
        end else
        begin
          Geral.MB_Aviso(Msg);
          Exit;
        end;
      end else
        Result := True;
    end;
  end else
  begin
    Geral.MB_Aviso(Msg);
    Result := False;
  end;
end;

procedure TUnMail.DesconectaMailServer(IMAP: TIdIMAP4; POP: TIdPOP3;
  var TipoConexao: TTipoConta);
begin
  try
    if IMAP <> nil then
    begin
      if IMAP.Connected then
        IMAP.Disconnect();
    end;
    if POP <> nil then
    begin
      if POP.Connected then
        POP.Disconnect;
    end;
  finally
    TipoConexao := istNenhum;
  end;
end;

function TUnMail.ExcluiPastaEmail_IMAP(PastaCod: Integer; QueryDir,
  QueryLoc: TmySQLQuery; DataBase: TmySQLDatabase; IMAP: TIdIMAP4): Boolean;
var
  MailCfg, DirEspecial: Integer;
  Pasta: String;
begin
  //Fazer => Se tiver mensagens n�o deixar excluir
  Result := False;
  //
  if QueryDir.Locate('Codigo', PastaCod, []) then
  begin
    MailCfg     := QueryDir.FieldByName('MailConta').AsInteger;
    Pasta       := QueryDir.FieldByName('Nome').AsString;
    DirEspecial := QueryDir.FieldByName('DirEspecial').AsInteger;
    //
    if Geral.MB_Pergunta('Confirma a exclus�o do diret�rio "' + Pasta + '"?') = ID_YES then
    begin
      if MyObjects.FIC(DirEspecial <> 0, nil, 'O diret�rio "' + Pasta + '" n�o pode ser exclu�do!') then Exit;
      //
      if IMAP.DeleteMailBox(Pasta) then
      begin
        UMyMod.ExcluiRegistroInt1('', 'emailgerdi', 'Codigo', PastaCod, DataBase);
        //
        Result := True;
      end;
    end;
  end else
    Geral.MB_Aviso('Falha ao localizar diret�rio!');
end;

procedure TUnMail.InsereEmailsListView(ListView: TListView; Codigo, Lido,
  Sinalizado, Anexos: Integer; De, Assunto, UID: String; Index: Integer);
var
  Item: TListItem;
  IdxLido, IdxSinal, IdxAnexos: Integer;
begin
  Item            := ListView.Items.Insert(Index);
  Item.Caption    := Geral.FF0(Codigo);
  Item.ImageIndex := -1;
  Item.SubItems.Add(UID);
  Item.SubItems.Add(De);
  Item.SubItems.Add(Assunto);
  IdxLido  := Item.SubItems.Add('');
  IdxSinal := Item.SubItems.Add('');
  Item.SubItems.Add(Geral.FF0(Lido));
  Item.SubItems.Add(Geral.FF0(Sinalizado));
  IdxAnexos := Item.SubItems.Add('');
  Item.SubItems.Add(Geral.FF0(Anexos));
  //
  if Lido = 0 then
    Item.SubItemImages[IdxLido] := 10
  else
    Item.SubItemImages[IdxLido] := -1;
  //
  if Sinalizado = 1 then
    Item.SubItemImages[IdxSinal] := 11
  else
    Item.SubItemImages[IdxSinal] := -1;
  //
  if Anexos <> 0 then
    Item.SubItemImages[IdxAnexos] := 17
  else
    Item.SubItemImages[IdxAnexos] := -1;
end;

procedure TUnMail.AtualizaDiretoriosIMAP(QueryUpd, QueryEmailGerDi: TmySQLQuery;
  DataBase: TmySQLDatabase; IMAP: TIdIMAP4; MailConta: Integer;
  Separador: String);
var
  Achou: Boolean;
  I, Codigo, DirEspecial: Integer;
  Diretorio: String;
  DirList: TStringList;
begin
  if MailConta <> 0 then
  begin
    if IMAP.Connected then
    begin
      DirList := TStringList.Create;
      try
        if IMAP.ListMailBoxes(DirList) then
        begin
          UMyMod.SQLInsUpd(QueryUpd, stUpd, 'emailconta', False,
            ['MailBoxSeparator'], ['Codigo'],
            [IMAP.MailBoxSeparator], [MailConta], True);
          //
          ReopenEmailGerDi(QueryEmailGerDi, DataBase, 0, MailConta);
          //
          for I := 0 to DirList.Count - 1 do
          begin
            Diretorio   := DirList[I];
            DirEspecial := VerificaDirEspecial(Diretorio, Separador);
            //
            if not QueryEmailGerDi.Locate('MailConta;Nome', VarArrayOf([MailConta, Diretorio]), []) then
            begin
              Codigo := UMyMod.BuscaNovoCodigo_Int(QueryUpd, 'emailgerdi',
                          'Codigo', [], [], stIns, 0, siPositivo, nil);
              //
              UMyMod.SQLInsUpd(QueryUpd, stIns, 'emailgerdi', False,
                ['Nome', 'DirEspecial', 'MailConta'], ['Codigo'],
                [Diretorio, DirEspecial, MailConta], [Codigo], True);
            end;
          end;
          QueryEmailGerDi.First;
          while not QueryEmailGerDi.Eof do
          begin
            Achou := False;
            //
            for I := 0 to DirList.Count - 1 do
            begin
              Diretorio := DirList[I];
              //
              if QueryEmailGerDi.FieldByName('Nome').AsString =  Diretorio then
              begin
                Achou := True;
                Break;
              end;
            end;
            if Achou = False then
            begin
              Codigo := QueryEmailGerDi.FieldByName('Codigo').AsInteger;
              //
              UMyMod.ExcluiRegistroInt1('', 'emailgerdi', 'Codigo', Codigo, DataBase);
            end;
            //
            QueryEmailGerDi.Next;
          end;
        end else
          Geral.MB_Aviso('Falha ao sincronizar pastas!' + sLineBreak +
            'Tente novamente mais tarde!');
      finally
        DirList.Free;
      end;
    end;
  end else
    Geral.MB_Aviso('Conta de e-mail n�o definida!');
end;

procedure TUnMail.ReopenEmailGerDi(QueryEmailGerDi: TmySQLQuery;
  Database: TmySQLDatabase; Codigo, MailConta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryEmailGerDi, Database, [
    'SELECT dir.Codigo, dir.Nome, dir.MailConta, ',
    'dir.DirEspecial, cfg.MailBoxSeparator, MAX(msg.DataHora) DataHoraSinc ',
    'FROM emailgerdi dir ',
    'LEFT JOIN emailconta cfg ON cfg.Codigo = dir.MailConta ',
    'LEFT JOIN emailgerms msg ON (msg.Diretorio = dir.Nome AND msg.MailConta = dir.MailConta) ',
    'WHERE dir.MailConta = ' + Geral.FF0(MailConta),
    'GROUP BY dir.Codigo ',
    'ORDER BY dir.DirEspecial DESC, dir.Codigo ASC ',
    '']);
  if Codigo <> 0 then
    QueryEmailGerDi.Locate('Codigo', Codigo, []);
end;

procedure TUnMail.ReopenEmailGerMs(ListView: TListView;
  QueryEmailGerMs: TmySQLQuery; Database: TmySQLDatabase; Diretorio: String;
  MailConta, Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryEmailGerMs, Database, [
    'SELECT msg.*, dir.Nome NomeDir, dir.Codigo CodDir ',
    'FROM emailgerms msg ',
    'LEFT JOIN emailgerdi dir ON (msg.Diretorio = dir.Nome AND msg.MailConta = dir.MailConta) ',
    'WHERE msg.Diretorio="' + Diretorio + '"',
    'AND msg.MailConta=' + Geral.FF0(MailConta),
    'GROUP BY msg.Codigo ',
    'ORDER BY msg.DataHora ASC ',
    '']);
  ListView.Clear;
  //
  if QueryEmailGerMs.RecordCount > 0 then
  begin
    QueryEmailGerMs.First;
    //
    while not QueryEmailGerMs.Eof do
    begin
      InsereEmailsListView(ListView, QueryEmailGerMs.FieldByName('Codigo').AsInteger,
        QueryEmailGerMs.FieldByName('Lido').AsInteger,
        QueryEmailGerMs.FieldByName('Sinalizado').AsInteger,
        QueryEmailGerMs.FieldByName('Anexos').AsInteger,
        QueryEmailGerMs.FieldByName('De').AsString,
        QueryEmailGerMs.FieldByName('Assunto').AsString,
        QueryEmailGerMs.FieldByName('UID').AsString, 0);
      //
      QueryEmailGerMs.Next;
    end;
  end;
  //
  if Codigo <> 0 then
    QueryEmailGerMs.Locate('Codigo', Codigo, []);
end;

procedure TUnMail.SalvaEmailNosItensEnviados(MailCfg: Integer;
  Query: TmySQLQuery; Database: TmySQLDatabase; IMAP: TIdIMAP4;
  IdSSL: TIdSSLIOHandlerSocketOpenSSL; Msg: TIdMessage;
  LabelAviso1: TLabel = nil; LabelAviso2: TLabel = nil);

  function SalvaEmEnviados(const Mensagem: TIdMessage): String;
  var
    MemoryStream: TMemoryStream;
  begin
    Result       := '';
    MemoryStream := TMemoryStream.Create;
    try
      Mensagem.SaveToStream(MemoryStream);
      MemoryStream.Position := 0;
      //
      if Pos('bol', LowerCase(IMAP.Username)) > 0 then //A pasta enviados do BOL � diferente
        IMAP.AppendMsgNoEncodeFromStream('Sent Messages', MemoryStream, [mfSeen])
      else
      IMAP.AppendMsgNoEncodeFromStream('INBOX.Sent', MemoryStream, [mfSeen]);
    finally
      MemoryStream.Free;
    end;
  end;
var
  TmpMailDir, MailTxt: String;
  SincIMAP: Boolean;
begin
  //S� tem suporte no IMAP
  //
  ConectaContaEmail(MailCfg, Query, Database, nil, IMAP, IdSSL);
  //
  if IMAP.Connected then
  begin
    try
      if (LabelAviso1 <> nil) and (LabelAviso2 <> nil) then
        MyObjects.Informa2(LabelAviso1, LabelAviso2, False, 'Salvando mensagem em itens enviados!');
      //
      if not DirectoryExists(CO_DIR_RAIZ_DMK) then
      begin
        if ForceDirectories(CO_DIR_RAIZ_DMK) then
          SincIMAP := True
        else
          SincIMAP := False;
      end else
        SincIMAP := True;
      //
      if SincIMAP then
      begin
        MailTxt    := MLAGeral.GeraStringRandomica(32) + '.eml';
        TmpMailDir := CO_DIR_RAIZ_DMK + '\' + MailTxt;
        //
        if FileExists(TmpMailDir) then
          DeleteFile(TmpMailDir);
        //
        SalvaEmEnviados(Msg);
        DeleteFile(TmpMailDir);
      end;
      if (LabelAviso1 <> nil) and (LabelAviso2 <> nil) then
        MyObjects.Informa2(LabelAviso1, LabelAviso2, False, 'Mensagem foi salva em itens enviados!');
    finally
      IMAP.Disconnect();
    end;
  end;
end;

function TUnMail.TraduzNomeMailDir(DirName: String): String;
begin
  if LowerCase(DirName) = 'inbox' then
    Result := 'Caixa de entrada'
  else
  if LowerCase(DirName) = 'draft' then
    Result := 'Rascunhos'
  else
  if LowerCase(DirName) = 'drafts' then
    Result := 'Rascunhos'
  else
  if LowerCase(DirName) = 'sent' then
    Result := 'Enviadas'
  else
  if LowerCase(DirName) = 'sent messages' then
    Result := 'Enviadas'
  else
  if LowerCase(DirName) = 'bulk mail' then
    Result := 'Lixo eletr�nico'
  else
  if LowerCase(DirName) = 'junk' then
    Result := 'Lixo eletr�nico'
  else
  if LowerCase(DirName) = 'trash' then
    Result := 'Lixeira'
  else
  if LowerCase(DirName) = 'deleted' then
    Result := 'Exclu�das'
  else
  if LowerCase(DirName) = 'deleted messages' then
    Result := 'Exclu�das'
  else
  if LowerCase(DirName) = 'deleted items' then
    Result := 'Exclu�das'
  else
  if LowerCase(DirName) = 'archive' then
    Result := 'Arquivo'
  else
    Result := DirName;
end;

(*
procedure TUnMail.ConfiguraDiretorios(DirPadrao: String; var DirMsg,
  DirHeader: String);
const
  Mensagem = 'Falha ao criar diret�rio para recebimento do mensagens!';
  Msg      = 'Msg';
  Header   = 'Header';
begin
  DirMsg    := DirPadrao + '\' + Msg;
  DirHeader := DirPadrao + '\' + Header;
  //
  if not DirectoryExists(DirMsg) then
  begin
    if not ForceDirectories(DirMsg) then
    begin
      Geral.MB_Aviso(Mensagem);
      DirMsg := '';
    end;
  end;
  if not DirectoryExists(DirHeader) then
  begin
    if not ForceDirectories(DirHeader) then
    begin
      Geral.MB_Aviso(Mensagem);
      DirHeader := '';
    end;
  end;
end;
*)

function TUnMail.ConfiguraTextoFlag(Flag: TIdMessageFlagsSet): String;
var
  Txt: String;
begin
  Txt := '';
  //
  if mfAnswered in Flag then
    Txt := Txt + 'Mensagem foi respondida; ';
  if mfFlagged in Flag then
    Txt := Txt + 'A mensagem foi "marcada" para: Urg�ncia / aten��o especial; ';
  if mfDeleted in Flag then
    Txt := Txt + 'A mensagem foi "eliminada" para remo��o por desaparecer depois; ';
  if mfDraft in Flag then
    Txt := Txt + 'Mensagem n�o completou composi��o (marcado como um rascunho); ';
  if mfSeen in Flag then
    Txt := Txt + 'Mensagem foi lida; ';
  if mfRecent in Flag then
    Txt := Txt + 'A mensagem "recentemente" chegou a esta caixa de correio; ';
  //
  Result := Txt;
end;

function TUnMail.VerificaDirEspecial(DirName, Separador: String): Integer;
begin
  if (LowerCase(DirName) = 'archive') or (LowerCase(DirName) = 'inbox' + Separador + 'archive')  then
    Result := 1
  else
  if (LowerCase(DirName) = 'bulk mail') or (LowerCase(DirName) = 'inbox' + Separador + 'bulk mail') then
    Result := 1
  else
  if (LowerCase(DirName) = 'deleted') or (LowerCase(DirName) = 'inbox' + Separador + 'deleted') then
    Result := 1
  else
  if (LowerCase(DirName) = 'deleted messages') or (LowerCase(DirName) = 'inbox' + Separador + 'deleted messages') then
    Result := 1
  else
  if (LowerCase(DirName) = 'deleted items') or (LowerCase(DirName) = 'inbox' + Separador + 'deleted items') then
    Result := 1
  else
  if (LowerCase(DirName) = 'draft') or (LowerCase(DirName) = 'inbox' + Separador + 'draft') then
    Result := 1
  else
  if (LowerCase(DirName) = 'drafts') or (LowerCase(DirName) = 'inbox' + Separador + 'drafts') then
    Result := 1
  else
  if Pos('gmail', LowerCase(DirName)) > 0 then
    Result := 1
  else
  if LowerCase(DirName) = 'inbox' then
    Result := 2
  else
  if (LowerCase(DirName) = 'junk') or (LowerCase(DirName) = 'inbox' + Separador + 'junk') then
    Result := 1
  else
  if (LowerCase(DirName) = 'sent') or (LowerCase(DirName) = 'inbox' + Separador + 'sent') then
    Result := 1
  else
  if (LowerCase(DirName) = 'sent messages') or (LowerCase(DirName) = 'inbox' + Separador + 'sent messages') then
    Result := 1
  else
  if (LowerCase(DirName) = 'trash') or (LowerCase(DirName) = 'inbox' + Separador + 'trash') then
    Result := 1
  else
  if (LowerCase(DirName) = 'quarentena') or (LowerCase(DirName) = 'inbox' + Separador + 'quarentena') then
    Result := 1
  else
    Result := 0;
end;

(*
function TUnMail.ObtemCodigoDirEspecial(DirName: String): Integer;
begin
  if DirName = 'INBOX' then
    Result := -5
  else
  if DirName = 'INBOX.Drafts' then
    Result := -4
  else
  if DirName = 'INBOX.Sent' then
    Result := -3
  else
  if DirName = 'INBOX.Junk' then
    Result := -2
  else
  if DirName = 'INBOX.Trash' then
    Result := -1
  else
    Result := 0;
end;
*)

end.
