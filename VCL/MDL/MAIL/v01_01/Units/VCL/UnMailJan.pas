unit UnMailJan;

interface

uses Forms, Controls, Windows, SysUtils, ComCtrls, Classes, ExtCtrls, Dialogs,
  UnDmkEnums;

type
  TUnMailJan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraMailGerenOpc();
  end;

var
  UMailJan: TUnMailJan;

implementation

uses UnMyObjects, Module, MyGlyfs, Principal, UMySQLModule, UnDmkWeb, UnMailEnv,
  ModuleGeral, MyDBCheck, MailGerenOpc;

{ TUnMailJan }

procedure TUnMailJan.MostraMailGerenOpc();
begin
  if DBCheck.CriaFm(TFmMailGerenOpc, FmMailGerenOpc, afmoNegarComAviso) then
  begin
    FmMailGerenOpc.ShowModal;
    FmMailGerenOpc.Destroy;
  end;
end;

end.
