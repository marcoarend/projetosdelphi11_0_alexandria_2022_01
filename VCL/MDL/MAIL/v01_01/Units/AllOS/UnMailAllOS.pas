unit UnMailAllOS;

interface

uses Classes,
{$IFDEF HAS_FMX}
UnGeral,
{$ELSE}
dmkGeral,
{$ENDIF}
IdSMTP, IdSSLOpenSSL, IdExplicitTLSClientServerBase, IdMessageClient, IdIMAP4,
IdPOP3;

type
  TUnMailAllOS = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ConfiguraConexaoSSL(IdCompCfg: TIdMessageClient;
                IdSSL: TIdSSLIOHandlerSocketOpenSSL; SSL, TLSUse, TLSTipo: Integer);
    function  TestarConexaoSMTP(IdSMTP: TIdSMTP;
                IdSSL: TIdSSLIOHandlerSocketOpenSSL; Host, Username,
                Password: String; Port, Logi_SSL, TLSUse, TLSTipo: Integer;
                Avisa: Boolean): Boolean;
    function  ConectaIMAP(Imap: TIdIMAP4;
                IdSSL: TIdSSLIOHandlerSocketOpenSSL; Host, Username,
                Password: String; Port, Logi_SSL, TLSUse, TLSTipo: Integer;
                Avisa: Boolean; Desconecta: Boolean = False): Boolean;
    function  ObtemListaTLS_Type(): TStringList;
    function  ObtemListaTLS_Use(): TStringList;
  end;

var
  UMailAllOS: TUnMailAllOS;
const
  MaxTLS_Use = 3;
  sListaTLS_Use: array[0..MaxTLS_Use] of String =
    ('utNoTLSSupport',
     'utUseImplicitTLS',
     'utUseRequireTLS',
     'utUseExplicitTLS');
  MaxTLS_Type = 5;
  sListaTLS_Type: array[0..MaxTLS_Type] of String =
    ('sslvSSLv2',
     'sslvSSLv23',
     'sslvSSLv3',
     'sslvTLSv1',
     //2022-07-18  envio de email NFe
     'sslvTLSv1_1',
     'sslvTLSv1_2'
     );

implementation

{ TUnMailAllOS }

procedure TUnMailAllOS.ConfiguraConexaoSSL(IdCompCfg: TIdMessageClient;
  IdSSL: TIdSSLIOHandlerSocketOpenSSL; SSL, TLSUse, TLSTipo: Integer);

  function ConfiguraTLSUse(TLSUse: Integer): TIdUseTLS;
  begin
    if sListaTLS_Use[TLSUse] = 'utNoTLSSupport' then
      Result := utNoTLSSupport
    else if sListaTLS_Use[TLSUse] = 'utUseImplicitTLS' then
      Result := utUseImplicitTLS
    else if sListaTLS_Use[TLSUse] = 'utUseRequireTLS' then
      Result := utUseRequireTLS
    else if sListaTLS_Use[TLSUse] = 'utUseExplicitTLS' then
      Result := utUseExplicitTLS;
  end;

const
  //CO_UseTLS = utUseImplicitTLS;//utUseRequireTLS;
  CO_SSLOptions_Mode = sslmClient;
var
  IdIMAP: TIdIMAP4;
  IdSMTP: TIdSMTP;
  IdPOP: TIdPOP3;
begin
  if IdCompCfg is TIdIMAP4 then
  begin
    IdIMAP := TIdIMAP4(IdCompCfg);
    //
    if SSL = 1 then
    begin
      IdIMAP.IOHandler := IdSSL;
      IdIMAP.UseTLS    := ConfiguraTLSUse(TLSUse);
    end else
    begin
      IdIMAP.IOHandler := nil;
      IdIMAP.UseTLS    := utNoTLSSupport;
    end;
  end else
  if IdCompCfg is TIdSMTP then
  begin
    IdSMTP := TIdSMTP(IdCompCfg);
    //
    if SSL = 1 then
    begin
      IdSMTP.IOHandler := IdSSL;
      IdSMTP.UseTLS    := ConfiguraTLSUse(TLSUse);
    end else
    begin
      IdSMTP.IOHandler := nil;
      IdSMTP.UseTLS    := utNoTLSSupport;
    end;
  end else
  if IdCompCfg is TIdPOP3 then
  begin
    IdPOP := TIdPOP3(IdCompCfg);
    //
    if SSL = 1 then
    begin
      IdPOP.IOHandler := IdSSL;
      IdPOP.UseTLS    := ConfiguraTLSUse(TLSUse);
    end else
    begin
      IdPOP.IOHandler := nil;
      IdPOP.UseTLS    := utNoTLSSupport;
    end;
  end else
  begin
    Geral.MB_Erro('Tipo inv�lido!' + sLineBreak + 'Vari�vel: IdCompCfg' +
      sLineBreak + 'Fun��o: TUnMailAllOS.ConfiguraConexaoSSL');
  end;
  //
  if SSL = 1 then
  begin
    IdSSL.SSLOptions.Mode := CO_SSLOptions_Mode;
    //
    if sListaTLS_Type[TLSTipo] = 'sslvSSLv2' then
    begin
      IdSSL.SSLOptions.Method      := sslvSSLv2;
      IdSSL.SSLOptions.SSLVersions := [sslvSSLv2];
    end else
    if sListaTLS_Type[TLSTipo] = 'sslvSSLv23' then
    begin
      IdSSL.SSLOptions.Method      := sslvSSLv23;
      IdSSL.SSLOptions.SSLVersions := [sslvSSLv23];
    end else
    if sListaTLS_Type[TLSTipo] = 'sslvSSLv3' then
    begin
      IdSSL.SSLOptions.Method      := sslvSSLv3;
      IdSSL.SSLOptions.SSLVersions := [sslvSSLv3];
    end else
    if sListaTLS_Type[TLSTipo] = 'sslvTLSv1' then
    begin
      IdSSL.SSLOptions.Method      := sslvTLSv1;
      IdSSL.SSLOptions.SSLVersions := [sslvTLSv1];
(**)
    // ini 2022-11-14 envio email nfe
    end else
    if sListaTLS_Type[TLSTipo] = 'sslvTLSv1_1' then
    begin
      IdSSL.SSLOptions.Method      := sslvTLSv1_1;
      IdSSL.SSLOptions.SSLVersions := [sslvTLSv1_1];
    end else
    if sListaTLS_Type[TLSTipo] = 'sslvTLSv1_2' then
    begin
    // ini 2023-03-22 envio email nfe
      //IdSSL.SSLOptions.Method      := sslvTLSv1_2;
      IdSSL.SSLOptions.SSLVersions := [sslvTLSv1, sslvTLSv1_1, sslvTLSv1_2];
(*
      IdSSL.SSLOptions.Method      := sslvTLSv1_2;
      IdSSL.SSLOptions.SSLVersions := [sslvTLSv1_2];
*)
    // fim 2023-03-22

    // fim 2022-11-14
(**)
    end;
  end;
end;

function TUnMailAllOS.ObtemListaTLS_Use: TStringList;
var
  I: Integer;
  Lista: TStringList;
begin
  Lista := TStringList.Create;
  try
    for I := 0 to Length(sListaTLS_Use) - 1 do
      Lista.Add(sListaTLS_Use[I]);
  finally
    Result := Lista;
  end;
end;

function TUnMailAllOS.ObtemListaTLS_Type: TStringList;
var
  I: Integer;
  Lista: TStringList;
begin
  Lista := TStringList.Create;
  try
    for I := 0 to Length(sListaTLS_Type) - 1 do
      Lista.Add(sListaTLS_Type[I]);
  finally
    Result := Lista;
  end;
end;

function TUnMailAllOS.ConectaIMAP(Imap: TIdIMAP4;
  IdSSL: TIdSSLIOHandlerSocketOpenSSL; Host, Username, Password: String; Port,
  Logi_SSL, TLSUse, TLSTipo: Integer; Avisa, Desconecta: Boolean): Boolean;
begin
  try
    try
      if Imap.Connected then
        Imap.Disconnect;
      //
      Imap.Host     := Host;
      Imap.Port     := Port;
      Imap.Username := Username;
      Imap.Password := Password;
      //
      ConfiguraConexaoSSL(IMAP, IdSSL, TLSUse, Logi_SSL, TLSTipo);
      //
      IMAP.Connect;
    finally
      Result := IMAP.Connected;
      if Avisa then
      begin
        if Result then
          {$IFDEF HAS_FMX}
          Geral.MB_Aviso('Conex�o com o serivdor IMAP ' + Host +
            ' estabelecida com �xito!')
          {$ELSE}
          Geral.MB_Aviso('Conex�o com o serivdor IMAP ' + Host +
            ' estabelecida com �xito!')
          {$ENDIF}
        else
          {$IFDEF HAS_FMX}
          Geral.MB_Aviso('N�o foi poss�vel abrir conex�o com o servidor IMAP ' +
            Host + '!');
          {$ELSE}
          Geral.MB_Aviso('N�o foi poss�vel abrir conex�o com o servidor IMAP ' +
            Host + '!');
          {$ENDIF}
      end;
      if Desconecta then
        IMAP.Disconnect;
    end;
  finally
    ;
  end;
end;

function TUnMailAllOS.TestarConexaoSMTP(IdSMTP: TIdSMTP;
  IdSSL: TIdSSLIOHandlerSocketOpenSSL; Host, Username, Password: String; Port,
  Logi_SSL, TLSUse, TLSTipo: Integer; Avisa: Boolean): Boolean;
begin
  try
    try
      if IdSMTP.Connected then
        IdSMTP.Disconnect;
      //
      IdSMTP.Host     := Host;
      IdSMTP.Username := Username;
      IdSMTP.Password := Password;
      //
      if Port > 0 then
        IdSMTP.Port := Port
      else
        IdSMTP.Port := 25;
      //
      ConfiguraConexaoSSL(IdSMTP, IdSSL, Logi_SSL, TLSUse, TLSTipo);
      //
      IdSMTP.Connect;
    finally
      Result := IdSMTP.Connected;
      if Avisa then
      begin
        if Result then
          {$IFDEF HAS_FMX}
          Geral.MB_Aviso('Conex�o com o serivdor SMTP ' + Host +
            ' estabelecida com �xito!')
          {$ELSE}
          Geral.MB_Aviso('Conex�o com o serivdor SMTP ' + Host +
            ' estabelecida com �xito!')
          {$ENDIF}
        else
          {$IFDEF HAS_FMX}
          Geral.MB_Aviso('N�o foi poss�vel abrir conex�o com o servidor SMTP ' +
            Host + '!');
          {$ELSE}
          Geral.MB_Aviso('N�o foi poss�vel abrir conex�o com o servidor SMTP ' +
            Host + '!');
          {$ENDIF}
      end;
      IdSMTP.Disconnect;
    end;
  finally
    ;
  end;
end;

end.
