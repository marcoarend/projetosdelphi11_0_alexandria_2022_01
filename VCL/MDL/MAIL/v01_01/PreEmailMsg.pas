unit PreEmailMsg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Mask, DBCtrls, dmkEdit, dmkRadioGroup,
  dmkCheckBox, dmkDBEdit, dmkLabel, dmkMemo, ExtDlgs, dmkGeral, UnDmkProcFunc,
  dmkImage, UnDmkEnums, UnMLAGeral, Soap.HTTPUtil, Vcl.ComCtrls, frxDock,
  UnProjGroup_Consts;

type
  TFmPreEmailMsg = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Panel4: TPanel;
    dmkEdControle: TdmkEdit;
    Label9: TLabel;
    dmkEdOrdem: TdmkEdit;
    Label3: TLabel;
    dmkRGTipo: TdmkRadioGroup;
    dmkCkAtivo: TdmkCheckBox;
    dmkDBEdCodigo: TdmkDBEdit;
    dmkEdit1: TdmkEdit;
    Label4: TLabel;
    dmkMemo1: TdmkMemo;
    Label5: TLabel;
    Label6: TLabel;
    OpenPictureDialog1: TOpenPictureDialog;
    Edit30: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    BtOK: TBitBtn;
    BitBtn1: TBitBtn;
    Panel6: TPanel;
    LVItens: TListView;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LVItensDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmPreEmailMsg: TFmPreEmailMsg;

implementation

uses UnMyObjects, PreEmail, UnMyVCLref, UmySQlModule, Module, ModuleGeral,
  UnInternalConsts, UnitDmkTags, MyListas;

{$R *.DFM}


procedure TFmPreEmailMsg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPreEmailMsg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPreEmailMsg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkTags.CarregaPreEmailTags(CO_DMKID_APP, LVItens, Panel1,
    DModG.QrMaster.FieldByName('HabilModulos').AsString);
end;

procedure TFmPreEmailMsg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPreEmailMsg.BtOKClick(Sender: TObject);
var
  Txt: String;
  Controle: Integer;
begin
  Controle := UMyMod.BuscaEmLivreY_Def('PreEmMsg', 'Controle', ImgTipo.SQLType,
                FmPreEmail.QrPreEmMsgControle.Value);
  //
  if UMyMod.ExecSQLInsUpdFm(FmPreEmailMsg, ImgTipo.SQLType, 'PreEmMsg', Controle,
    Dmod.QrUpd) then
  begin
    FmPreEmail.ReopenPreEmMsg(Controle);
    Close;
  end;
end;

procedure TFmPreEmailMsg.BitBtn1Click(Sender: TObject);
var
  ImgPath, TxtPath: String;
  Comando: PChar;
begin
  if OpenPictureDialog1.Execute then
  begin
    if FileExists(VAR_BASE64_EXE) then
    begin
      ImgPath := OpenPictureDialog1.FileName;
      TxtPath := dmkPF.MudaExtensaoDeArquivo(ImgPath, 'txt');
      //if FileExists(TxtPath) then
        //DeleteFile(TxtPath);
      Geral.SalvaTextoEmArquivo(TxtPath, '', True);
      Comando := PChar(VAR_BASE64_EXE + ' -e ' + dmkPF.NomeLongoParaCurto(
        ImgPath) + ' ' + dmkPF.NomeLongoParaCurto(TxtPath));
      //"C:\base64 -e bg2.jpg bg2.txt"
      dmkPF.WinExecAndWaitOrNot(Comando, SW_SHOW, TMemo(dmkMemo1), True);
      //ArqExiste := FileExists(ImgPath);
      if FileExists(TxtPath) then
      begin
        dmkMemo1.Lines.LoadFromFile(TxtPath);
        //DeleteFile(TxtPath);
      end;
    end else Geral.MB_Aviso('O aplicativo "' + VAR_BASE64_EXE +
    '" n�o existe para criar a imagem "' + ImgPath + '".');
  end;
end;

procedure TFmPreEmailMsg.LVItensDblClick(Sender: TObject);
begin
  UnDmkTags.AdicionaTag(LVItens, TMemo(dmkMemo1));
end;

end.

