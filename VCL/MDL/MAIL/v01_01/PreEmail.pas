unit PreEmail;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkEdit,
  UnMyVCLRef, dmkCheckBox, Menus, ComCtrls, Grids, DBGrids, dmkDBGrid, dmkLabel,
  dmkGeral, ImgList, UnDmkProcFunc, DmkDAC_PF, OleCtrls, SHDocVw, MSHTML,
  dmkValUsu, dmkImage, dmkDBLookupComboBox, dmkEditCB, Variants, UnDmkEnums,
  Web.HTTPApp, UnProjGroup_Consts;

type
  TFmPreEmail = class(TForm)
    PainelDados: TPanel;
    DsPreEmail: TDataSource;
    QrPreEmail: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    dmkEdNome: TdmkEdit;
    dmkEdCodigo: TdmkEdit;
    Label4: TLabel;
    dmkEdSend_Name: TdmkEdit;
    Label12: TLabel;
    dmkEdMail_Titu: TdmkEdit;
    QrPreEmailCodigo: TIntegerField;
    QrPreEmailNome: TWideStringField;
    QrPreEmailAlterWeb: TSmallintField;
    QrPreEmailAtivo: TSmallintField;
    QrPreEmailLk: TIntegerField;
    QrPreEmailDataCad: TDateField;
    QrPreEmailDataAlt: TDateField;
    QrPreEmailUserCad: TIntegerField;
    QrPreEmailUserAlt: TIntegerField;
    QrPreEmailSend_Name: TWideStringField;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label13: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit9: TDBEdit;
    QrPreEmailMail_Titu: TWideStringField;
    PMPreMail: TPopupMenu;
    Incluinovopremeio1: TMenuItem;
    Alterapremeioatual1: TMenuItem;
    Excluipremeioatual1: TMenuItem;
    PMMsg: TPopupMenu;
    Incluinovotpico1: TMenuItem;
    Alteratpicoselecionado1: TMenuItem;
    Excluitpicoselecionado1: TMenuItem;
    QrPreEmMsg: TmySQLQuery;
    DsPreEmMsg: TDataSource;
    QrPreEmMsgCodigo: TIntegerField;
    QrPreEmMsgControle: TIntegerField;
    QrPreEmMsgOrdem: TIntegerField;
    QrPreEmMsgTipo: TIntegerField;
    QrPreEmMsgTexto: TWideMemoField;
    QrPreEmMsgLk: TIntegerField;
    QrPreEmMsgDataCad: TDateField;
    QrPreEmMsgDataAlt: TDateField;
    QrPreEmMsgUserCad: TIntegerField;
    QrPreEmMsgUserAlt: TIntegerField;
    QrPreEmMsgAlterWeb: TSmallintField;
    QrPreEmMsgAtivo: TSmallintField;
    PainelMsgs: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    dmkDBGrid1: TdmkDBGrid;
    QrPreEmMsgNOMETIPO: TWideStringField;
    QrPreEmMsgDescricao: TWideStringField;
    Label22: TLabel;
    EdSaudacao: TdmkEdit;
    QrPreEmailSaudacao: TWideStringField;
    DBEdit10: TDBEdit;
    Label23: TLabel;
    QrPreEmMsgCidID_Img: TWideStringField;
    N1: TMenuItem;
    Recriapadres1: TMenuItem;
    Memo1: TMemo;
    Memo2: TMemo;
    Memo3: TMemo;
    Memo4: TMemo;
    Memo5: TMemo;
    CkNaoEnvBloq: TdmkCheckBox;
    QrPreEmailNaoEnvBloq: TSmallintField;
    DBCheckBox2: TDBCheckBox;
    Memo11: TMemo;
    Memo12: TMemo;
    Emailspadres1: TMenuItem;
    Criarnoexistentes1: TMenuItem;
    Memo13: TMemo;
    PageControl2: TPageControl;
    TSTxtPlano: TTabSheet;
    TSTxtHTML: TTabSheet;
    DBMemo1: TDBMemo;
    WebBrowser1: TWebBrowser;
    DBCheckBox4: TDBCheckBox;
    CkReturnReciept: TdmkCheckBox;
    QrPreEmailReturnReciept: TSmallintField;
    Memo15: TMemo;
    Memo14: TMemo;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtPreMail: TBitBtn;
    BtMsg: TBitBtn;
    BtExclui: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    DBEdit3: TDBEdit;
    Label3: TLabel;
    QrPreEmailEmailConta_TXT: TWideStringField;
    LaSolicitante: TLabel;
    EdEmailConta: TdmkEditCB;
    CBEmailConta: TdmkDBLookupComboBox;
    SBSolicitante: TSpeedButton;
    QrEmailConta: TmySQLQuery;
    DsEmailConta: TDataSource;
    QrEmailContaCodigo: TIntegerField;
    QrEmailContaNome: TWideStringField;
    QrEmailContaSend_Name: TWideStringField;
    QrPreEmailEmailConta: TIntegerField;
    Memo16: TMemo;
    N2: TMenuItem;
    Duplicaritematual1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtPreMailClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPreEmailAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPreEmailBeforeOpen(DataSet: TDataSet);
    procedure Incluinovopremeio1Click(Sender: TObject);
    procedure Alterapremeioatual1Click(Sender: TObject);
    procedure Excluipremeioatual1Click(Sender: TObject);
    procedure BtMsgClick(Sender: TObject);
    procedure Incluinovotpico1Click(Sender: TObject);
    procedure QrPreEmailAfterScroll(DataSet: TDataSet);
    procedure Alteratpicoselecionado1Click(Sender: TObject);
    procedure Excluitpicoselecionado1Click(Sender: TObject);
    procedure Recriapadres1Click(Sender: TObject);
    procedure Criarnoexistentes1Click(Sender: TObject);
    procedure QrPreEmMsgAfterScroll(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure SBSolicitanteClick(Sender: TObject);
    procedure EdEmailContaChange(Sender: TObject);
    procedure QrPreEmMsgBeforeClose(DataSet: TDataSet);
    procedure PMPreMailPopup(Sender: TObject);
    procedure PMMsgPopup(Sender: TObject);
    procedure Duplicaritematual1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure VerificarPadroes(ForcaRecriacao: Boolean);
    procedure ReopenEmailConta();
    function  QrPreEmailLocate(Codigo: Integer): Boolean;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPreEmMsg(Controle: Integer);
  end;

var
  FmPreEmail: TFmPreEmail;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, PreEmailMsg, MyDBCheck, MyListas, UnDmkWeb, MailCfg,
  UnMailEnv, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPreEmail.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPreEmail.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPreEmailCodigo.Value, LaRegistro.Caption[2]);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPreEmail.DefParams;
begin
  VAR_GOTOTABELA := 'PreEmail';
  VAR_GOTOMYSQLTABLE := QrPreEmail;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT pre.*, cfg.Usuario EmailConta_TXT ');
  VAR_SQLx.Add('FROM preemail pre ');
  VAR_SQLx.Add('LEFT JOIN emailconta cfg ON cfg.Codigo = pre.EmailConta ');
  VAR_SQLx.Add('WHERE pre.Codigo <> 0 ');
  //
  VAR_SQL1.Add('AND pre.Codigo=:P0');
  //
  VAR_SQLa.Add('AND pre.Nome Like :P0');
end;

procedure TFmPreEmail.Duplicaritematual1Click(Sender: TObject);
var
  Codigo, Controle: Integer;
  Nome: String;
begin
  if (QrPreEmail.State = dsInactive) or (QrPreEmail.RecordCount = 0) then Exit;
  //
  if Geral.MB_Pergunta('Confirma a duplica��o do pr� e-mail atual?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      QrPreEmail.DisableControls;
      QrPreEmMsg.DisableControls;
      //
      Codigo := UMyMod.BuscaEmLivreY_Def('preemail', 'Codigo', stIns, 0);
      Nome   := Copy(QrPreEmailNome.Value, 1, 93) + CO_COPIA;
      //
      if MyObjects.FIC(Codigo = 0, nil, 'Falha ao definir C�digo para a tabela "preemail"!') then Exit;
      //
      if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'preemail', TMeuDB,
        ['Codigo'], [QrPreEmailCodigo.Value], ['Codigo', 'Nome'], [Codigo, Nome],
        '', True, LaAviso1, LaAviso2) then
      begin
        if QrPreEmMsg.RecordCount > 0 then
        begin
          QrPreEmMsg.First;
          while not QrPreEmMsg.Eof do
          begin
            Controle := UMyMod.BuscaEmLivreY_Def('preemmsg', 'Controle', stIns, 0);
            //
            if MyObjects.FIC(Controle = 0, nil, 'Falha ao definir Controle para a tabela "preemmsg"!') then Exit;
            //
            if  not UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'preemmsg', TMeuDB,
              ['Controle'], [QrPreEmMsgControle.Value], ['Codigo', 'Controle'],
              [Codigo, Controle],'', True, LaAviso1, LaAviso2) then
            begin
              Geral.MB_Erro('Falha ao duplicar registro!');
              Exit;
            end;
            //
            QrPreEmMsg.Next;
          end;
        end;
      end;
    finally
      QrPreEmail.EnableControls;
      QrPreEmMsg.EnableControls;
      Va(vpLast);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      Screen.Cursor := crDefault;
      Geral.MB_Aviso('Duplica��o finalizada!');
    end;
  end;
end;

procedure TFmPreEmail.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        dmkEdCodigo.ValueVariant  := Codigo;
        dmkEdNome.Text            := '';
        dmkEdSend_Name.Text       := '';
        dmkEdMail_Titu.Text       := '';
        EdSaudacao.Text           := '';
        CkNaoEnvBloq.Checked      := False;
        CkReturnReciept.Checked   := True;
        EdEmailConta.ValueVariant := 0;
        CBEmailConta.KeyValue     := Null;
      end else begin
        dmkEdCodigo.ValueVariant  := QrPreEmailCodigo.Value;
        dmkEdNome.Text            := QrPreEmailNome.Value;
        dmkEdSend_Name.Text       := QrPreEmailSend_Name.Value;
        dmkEdMail_Titu.Text       := QrPreEmailMail_Titu.Value;
        EdSaudacao.Text           := QrPreEmailSaudacao.Value;
        CkNaoEnvBloq.Checked      := Geral.IntToBool(QrPreEmailNaoEnvBloq.Value);
        CkReturnReciept.Checked   := Geral.IntToBool(QrPreEmailReturnReciept.Value);
        EdEmailConta.ValueVariant := QrPreEmailEmailConta.Value;
        CBEmailConta.KeyValue     := QrPreEmailEmailConta.Value;
      end;
      dmkEdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPreEmail.PMMsgPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPreEmail.State <> dsInactive) and (QrPreEmail.RecordCount > 0);
  Enab2 := (QrPreEmMsg.State <> dsInactive) and (QrPreEmMsg.RecordCount > 0);
  //
  Incluinovotpico1.Enabled        := Enab;
  Alteratpicoselecionado1.Enabled := Enab and Enab2;
  Excluitpicoselecionado1.Enabled := Enab and Enab2;
end;

procedure TFmPreEmail.PMPreMailPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrPreEmail.State <> dsInactive) and (QrPreEmail.RecordCount > 0);
  //
  Alterapremeioatual1.Enabled := Enab;
  Excluipremeioatual1.Enabled := False;
end;

procedure TFmPreEmail.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPreEmail.Criarnoexistentes1Click(Sender: TObject);
begin
  VerificarPadroes(False);
end;

procedure TFmPreEmail.AlteraRegistro;
var
  PreEmail : Integer;
begin
  PreEmail := QrPreEmailCodigo.Value;
  if not UMyMod.SelLockY(PreEmail, Dmod.MyDB, 'PreEmail', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(PreEmail, Dmod.MyDB, 'PreEmail', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPreEmail.IncluiRegistro;
var
  Cursor : TCursor;
  PreEmail : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    PreEmail := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'PreEmail', 'PreEmail', 'Codigo');
    if Length(FormatFloat(FFormatFloat, PreEmail))>Length(FFormatFloat) then
    begin
      Geral.MB_Aviso(
      'Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, PreEmail);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmPreEmail.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPreEmail.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPreEmail.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPreEmail.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPreEmail.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPreEmail.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPreEmail.BtPreMailClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMpremail, btPreMail);
end;

procedure TFmPreEmail.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPreEmailCodigo.Value;
  Close;
end;

procedure TFmPreEmail.BtConfirmaClick(Sender: TObject);
var
  Codigo, EmailConta: Integer;
  Nome: String;
begin
  Nome       := dmkEdNome.Text;
  EmailConta := EdEmailConta.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, dmkEdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(EmailConta = 0, EdEmailConta, 'Defina uma configura��o de e-mail!') then Exit;
  //
  Codigo := Geral.IMV(dmkEdCodigo.Text);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'preemail', False, [
    'Nome', 'Send_Name',
    'Mail_Titu', 'Saudacao',
    'NaoEnvBloq', 'ReturnReciept',
    'EmailConta'
  ], ['Codigo'], [
    dmkEdNome.ValueVariant, dmkEdSend_Name.ValueVariant,
    dmkEdMail_Titu.ValueVariant, EdSaudacao.Text,
    Geral.BoolToInt(CkNaoEnvBloq.Checked),
    Geral.BoolToInt(CkReturnReciept.Checked), EmailConta
  ], [dmkEdCodigo.ValueVariant], True)
  then begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PreEmail', 'Codigo');
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmPreEmail.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(dmkEdCodigo.Text);
  //
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PreEmail', Codigo);
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PreEmail', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PreEmail', 'Codigo');
end;

procedure TFmPreEmail.BtExcluiClick(Sender: TObject);
begin
  Geral.MB_Info('Exclus�o indispon�vel para esta janela!');
end;

procedure TFmPreEmail.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenEmailConta;
  //
  PainelEdita.Align      := alClient;
  PainelDados.Align      := alClient;
  PainelEdit.Align       := alClient;
  PainelMsgs.Align       := alClient;
  CriaOForm;
end;

procedure TFmPreEmail.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPreEmailCodigo.Value,LaRegistro.Caption);
end;

procedure TFmPreEmail.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Info('Impress�o indispon�vel para esta janela!');
end;

procedure TFmPreEmail.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPreEmail.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Info('Tipo de pesquisa indispon�vel para esta janela!');
end;

procedure TFmPreEmail.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPreEmail.QrPreEmailAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPreEmail.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  dmkEdMail_Titu .CharCase := ecNormal;
end;

procedure TFmPreEmail.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPreEmailCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'PreEmail', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPreEmail.SBSolicitanteClick(Sender: TObject);
begin
  UMailEnv.MostraMailCfg(EdEmailConta.ValueVariant);
  if VAR_CADASTRO > 0 then
  begin
    ReopenEmailConta();
    //
    EdEmailConta.ValueVariant := VAR_CADASTRO;
    CBEmailConta.KeyValue     := VAR_CADASTRO;
    EdEmailConta.SetFocus;
  end;
end;

procedure TFmPreEmail.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPreEmail.QrPreEmailBeforeOpen(DataSet: TDataSet);
begin
  QrPreEmailCodigo.DisplayFormat := FFormatFloat;
end;

function TFmPreEmail.QrPreEmailLocate(Codigo: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Codigo FROM preemail ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
  //
  Result := Dmod.QrAux.RecordCount > 0;
end;

procedure TFmPreEmail.QrPreEmMsgAfterScroll(DataSet: TDataSet);
(*
var
  Doc: IHTMLDocument2;
*)
begin
  if QrPreEmMsgTipo.Value = 1 then
  begin
    TSTxtPlano.TabVisible := False;
    TSTxtHTML.TabVisible  := True;
    //
    DmkWeb.WBLoadHTML(WebBrowser1, QrPreEmMsgTexto.Value);
  end else
  begin
    TSTxtPlano.TabVisible := True;
    TSTxtHTML.TabVisible  := False;
  end;
end;

procedure TFmPreEmail.QrPreEmMsgBeforeClose(DataSet: TDataSet);
begin
  TSTxtPlano.TabVisible := True;
  TSTxtHTML.TabVisible  := False;
end;

procedure TFmPreEmail.Incluinovopremeio1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmPreEmail.Alterapremeioatual1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmPreEmail.EdEmailContaChange(Sender: TObject);
begin
  dmkEdSend_Name.ValueVariant := QrEmailContaSend_Name.Value;
end;

procedure TFmPreEmail.Excluipremeioatual1Click(Sender: TObject);
begin
  //Exclui
end;

procedure TFmPreEmail.BtMsgClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMsg, BtMsg);
end;

procedure TFmPreEmail.Incluinovotpico1Click(Sender: TObject);
begin
  UMyMod.FormInsUpd_SHow(TFmPreEmailMsg, FmPreEmailMsg,
    afmoLiberado, QrPreEmMsg, stIns);
end;

procedure TFmPreEmail.QrPreEmailAfterScroll(DataSet: TDataSet);
begin
  ReopenPreEmMsg(0);
end;

procedure TFmPreEmail.Recriapadres1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Este procedimento ir� excluir todas as modifica��es feitas nos registros padr�o e recri�-las novamente com as confiugura��es padr�o!' +
    sLineBreak + 'Deseja continuar mesmo assim?') <> ID_YES
  then
    Exit;
  //
  VerificarPadroes(True);
end;

procedure TFmPreEmail.ReopenEmailConta;
begin
  UMyMod.AbreQuery(QrEmailConta, Dmod.MyDB);
end;

procedure TFmPreEmail.ReopenPreEmMsg(Controle: Integer);
begin
  QrPreEmMsg.Close;
  QrPreEmMsg.Params[0].AsInteger := QrPreEmailCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPreEmMsg, Dmod.MyDB);
  //
  if Controle > 0 then QrPreEmMsg.Locate('Controle', Controle, []);
end;

procedure TFmPreEmail.Alteratpicoselecionado1Click(Sender: TObject);
begin
  UMyMod.FormInsUpd_SHow(TFmPreEmailMsg, FmPreEmailMsg,
    afmoNegarComAviso, QrPreEmMsg, stUpd);
end;

procedure TFmPreEmail.Excluitpicoselecionado1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPreEmMsg, TDBGrid(dmkDBGrid1),
    'preemmsg', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmPreEmail.VerificarPadroes(ForcaRecriacao: Boolean);
var
  Send_Name_1, Send_Name_2, Send_Name_3, Send_Name_4, Send_Name_5, Send_Name_6,
  Send_Name_7, Send_Name_8,
  Mail_Titu_1, Mail_Titu_2, Mail_Titu_3, Mail_Titu_4, Mail_Titu_5, Mail_Titu_6,
  Mail_Titu_7, Mail_Titu_8,
  Nome_1, Nome_2, Nome_3, Nome_4, Nome_5, Nome_6, Nome_7, Nome_8: String;
  Texto11, Texto21, Texto31, Texto41, Texto51, Texto52, Texto53, Texto54,
  Texto61, Texto71, Texto81: WideString;
  NFSe, NFe, Condom, Web: Boolean;
begin
  if ForcaRecriacao then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('Delete FROM preemail WHERE Codigo<0');
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('Delete FROM preemmsg WHERE Codigo<0');
    Dmod.QrUpd.ExecSQL;
  end;
  //
  (*Condom := False;
  NFSe   := False;
  NFe    := False;
  *)
  //
  //Seta modelos utilizados
  if CO_DMKID_APP in [4, 43] then //SYNDI2, SYNDI3
  begin
    Condom := True;
    NFSe   := True;
    NFe    := False;
    Web    := True;
  end else
  if (CO_DMKID_APP = 6) //PLANNING
  or (CO_DMKID_APP = 2) //BLUEDERM
  then
  begin
    Condom := False;
    NFSe   := False;
    NFe    := True;
    Web    := False;
  end else
  if (CO_DMKID_APP = 28) //TOOLRENT
  or (CO_DMKID_APP = 24) //BUGSTROL
  then
  begin
    Condom := False;
    NFSe   := True;
    NFe    := True;
    //
    if CO_DMKID_APP = 24 then //BUGSTROL
      Web := True
    else
      Web := False;
  end else
  if (CO_DMKID_APP = 17) //DCONTROL
  or (CO_DMKID_APP = 22) //CREDITO2
  or (CO_DMKID_APP = 3)  //CREDITOR
  then
  begin
    Condom := False;
    NFSe   := True;
    NFe    := False;
    //
    if CO_DMKID_APP = 17 then //DCONTROL
      Web := True
    else
      Web := False;
  end else
  begin
    Geral.MB_Info('Applicativo sem pr�-email padr�o!' + sLineBreak +
    'Avise a DERMATEK!');
    Exit;
  end;
  //
  //Configura campos
  if NFe then
  begin
    Nome_1      := 'Padr�o pr�-email sem imagem - NFe autorizada';//NFe
    Nome_2      := 'Padr�o pr�-email sem imagem - NFe cancelada';//NFe
    Nome_3      := 'Padr�o pr�-email sem imagem - Carta de Corre��o Eletr�nica';//NFe
    Send_Name_1 := 'Empresa ???';//NFe
    Send_Name_2 := 'Empresa ???';//NFe
    Send_Name_3 := 'Empresa ???';//NFe
    Mail_Titu_1 := 'NFe - [Empresa]';//NFe
    Mail_Titu_2 := 'NFe - [Empresa]';//NFe
    Mail_Titu_3 := 'NFe - [Empresa]';//NFe
    Texto11     := Geral.WideStringToSQLString(Memo11.Text);//NFe
    Texto21     := Geral.WideStringToSQLString(Memo12.Text);//NFe
    Texto31     := Geral.WideStringToSQLString(Memo13.Text);//NFe
  end;
  if Condom then
  begin
    Nome_4      := 'Padr�o pr�-email sem imagem';//Condom
    Nome_5      := 'Padr�o pr�-email com imagem';//Condom
    Send_Name_4 := 'Administradora ???';//Condom
    Send_Name_5 := 'Administradora ???';//Condom
    Mail_Titu_4 := 'Seu bloqueto condominial - [Condominio]';//Condom
    Mail_Titu_5 := 'Seu bloqueto condominial - [Condominio]';//Condom
    Texto41     := Geral.WideStringToSQLString(Memo1.Text);//Condom
    Texto51     := Geral.WideStringToSQLString(Memo2.Text);//Condom
    Texto52     := Geral.WideStringToSQLString(Memo3.Text);//Condom
    Texto53     := Geral.WideStringToSQLString(Memo4.Text);//Condom
    Texto54     := Geral.WideStringToSQLString(Memo5.Text);//Condom
  end;
  if NFSe then
  begin
    Nome_6      := 'Padr�o pr�-email sem imagem - NFSe autorizada';//NFSe
    Nome_7      := 'Padr�o pr�-email sem imagem - NFSe cancelada';//NFSe
    Send_Name_6 := 'Empresa ???';//NFSe
    Send_Name_7 := 'Empresa ???';//NFSe
    Mail_Titu_6 := 'NFSe - [Empresa]';//NFSe
    Mail_Titu_7 := 'NFSe - [Empresa]';//NFSe
    Texto61     := Geral.WideStringToSQLString(Memo14.Text);//NFSe
    Texto71     := Geral.WideStringToSQLString(Memo15.Text);//NFSe
  end;
  if Web then
  begin
    Nome_8      := 'E-mail para edi��o de senha na internet';//WEB
    Send_Name_8 := 'Empresa ???';//WEB
    Mail_Titu_8 := 'Edi��o de senha no site da Empresa ???';//WEB
    Texto81     := Geral.WideStringToSQLString(Memo16.Text);//WEB
  end;
  //
  //Insere no banco de dados
  if Nome_1 <> '' then //NFe
  begin
    if not QrPreEmailLocate(-1) then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemail', False,
        ['Nome', 'Send_Name', 'Mail_Titu', 'Saudacao'], ['Codigo'],
        [Nome_1, Send_Name_1, Mail_Titu_1, ''], [-1], False);
      if Texto11 <> '' then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False,
          ['Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'],
          ['Controle'], [ -1, 1, 1, Texto11, 'HTML', ''], [-1], False);
      end;
    end;
  end;
  if Nome_2 <> '' then //NFe
  begin
    if not QrPreEmailLocate(-2) then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemail', False,
        ['Nome', 'Send_Name', 'Mail_Titu', 'Saudacao'], ['Codigo'],
        [Nome_2, Send_Name_2, Mail_Titu_2, ''], [-2], False);
      if Texto21 <> '' then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False,
          ['Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'],
          ['Controle'], [ -2, 1, 1, Texto21, 'HTML', ''], [-2], False);
      end;
    end;
  end;
  if Nome_3 <> '' then //NFe
  begin
    if not QrPreEmailLocate(-3) then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemail', False,
        ['Nome', 'Send_Name', 'Mail_Titu', 'Saudacao'], ['Codigo'],
        [Nome_3, Send_Name_3, Mail_Titu_3, ''], [-3], False);
      if Texto31 <> '' then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False, [
          'Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'],
          ['Controle'], [ -3, 1, 1, Texto31, 'HTML', ''], [-3], False);
      end;
    end;
  end;
  if Nome_4 <> '' then //Condom
  begin
    if not QrPreEmailLocate(-4) then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemail', False,
        ['Nome', 'Send_Name', 'Mail_Titu', 'Saudacao'], ['Codigo'],
        [Nome_4, Send_Name_4, Mail_Titu_4, ''], [-4], False);
      if Texto41 <> '' then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False, [
          'Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'
          ], ['Controle'], [ -4, 1, 1, Texto41, 'HTML', ''], [-4], False);
      end;
    end;
  end;
  if Nome_5 <> '' then //Condom
  begin
    if not QrPreEmailLocate(-5) then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemail', False,
        ['Nome', 'Send_Name', 'Mail_Titu', 'Saudacao'], ['Codigo'],
        [Nome_5, Send_Name_5, Mail_Titu_5, ''], [-5], False);
      //
      if Texto51 <> '' then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False, [
          'Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'
          ], ['Controle'], [ -5, 1, 1, Texto51, 'HTML', ''], [-5], False);
      end;
      if Texto52 <> '' then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False, [
          'Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'
          ], ['Controle'], [ -5, 2, 2, Texto52, 'bar.jpg', '0000122'], [-6], False);
      end;
      if Texto53 <> '' then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False, [
          'Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'
          ], ['Controle'], [ -5, 3, 2, Texto53, 'bg.jpg', '0000133'], [-7], False);
      end;
      if Texto54 <> '' then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False, [
          'Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'
        ], ['Controle'], [ -5, 4, 2, Texto54, 'bot.jpg', '0000144'], [-8], False);
      end;
    end;
  end;
  if Nome_6 <> '' then //NFSe
  begin
    if not QrPreEmailLocate(-6) then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemail', False, [
        'Nome', 'Send_Name', 'Mail_Titu', 'Saudacao'], ['Codigo'],
        [Nome_6, Send_Name_6, Mail_Titu_6, ''], [-6], False);
      if Texto61 <> '' then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False, [
          'Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'
          ], ['Controle'], [ -6, 1, 1, Texto61, 'HTML', ''], [-9], False);
      end;
    end;
  end;
  if Nome_7 <> '' then //NFSe
  begin
    if not QrPreEmailLocate(-7) then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemail', False, [
        'Nome', 'Send_Name', 'Mail_Titu', 'Saudacao'], ['Codigo'],
        [Nome_7, Send_Name_7, Mail_Titu_7, ''], [-7], False);
      if Texto71 <> '' then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False, [
          'Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'
          ], ['Controle'], [ -7, 1, 1, Texto71, 'HTML', ''], [-10], False);
      end;
    end;
  end;
  if Nome_8 <> '' then //WEB
  begin
    if not QrPreEmailLocate(-8) then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemail', False, [
        'Nome', 'Send_Name', 'Mail_Titu', 'Saudacao'], ['Codigo'],
        [Nome_8, Send_Name_8, Mail_Titu_8, ''], [-8], False);
      if Texto71 <> '' then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False, [
          'Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'
          ], ['Controle'], [ -8, 1, 1, Texto81, 'HTML', ''], [-11], False);
      end;
    end;
  end;
  {
  if CO_DMKID_APP in [4, 43] then //SYNDI2, SYNDI3
  begin
    Nome_1    := 'Padr�o pr�-email sem imagem';
    Nome_2    := 'Padr�o pr�-email com imagem';
    Nome_3    := '';
    Send_Name := 'Administradora ???';
    Mail_Titu := 'Seu bloqueto condominial - [Condominio]';
    Texto11   := Geral.WideStringToSQLString(Memo1.Text);
    Texto21   := Geral.WideStringToSQLString(Memo2.Text);
    Texto22   := Geral.WideStringToSQLString(Memo3.Text);
    Texto23   := Geral.WideStringToSQLString(Memo4.Text);
    Texto24   := Geral.WideStringToSQLString(Memo5.Text);
    Texto31   := '';
  end else
  if (CO_DMKID_APP = 6) //PLANNING
  or (CO_DMKID_APP = 2) //BLUEDERM
  or (CO_DMKID_APP = 28) //TOOLRENT
  or (CO_DMKID_APP = 24) //BUGSTROL
  then
  begin
    Nome_1    := 'Padr�o pr�-email sem imagem - NFe autorizada';
    Nome_2    := 'Padr�o pr�-email sem imagem - NFe cancelada';
    Nome_3    := 'Padr�o pr�-email sem imagem - Carta de Corre��o Eletr�nica';
    Send_Name := 'Empresa ???';
    Mail_Titu := 'NFe - [Empresa]';
    Texto11   := Geral.WideStringToSQLString(Memo11.Text);
    Texto21   := Geral.WideStringToSQLString(Memo12.Text);
    Texto22   := '';
    Texto23   := '';
    Texto24   := '';
    Texto31   := Geral.WideStringToSQLString(Memo13.Text);
  end else
  begin
    Geral.MB_Info('Applicativo sem pr�-email padr�o!' + sLineBreak +
    'Avise a DERMATEK!');
    Exit;
  end;
  //
  if Nome_1 <> '' then
  begin
    if not QrPreEmailLocate(-1) then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemail', False, [
        'Nome', 'SMTPServer', 'Send_Name',
        'Send_Mail', 'Pass_Mail', 'Logi_Name',
        'Logi_Pass', 'Logi_Auth', 'Mail_Titu',
        'Saudacao'
      ], ['Codigo'], [
        Nome_1, 'smtp.???.com.br', Send_Name,
        '???@???.com.br', 'senha', '???@???.com.br',
        'senha', 0, Mail_Titu, ''
      ], [-1], False);
      if Texto11 <> '' then
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False, [
        'Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'
      ], ['Controle'], [ -1, 1, 1, Texto11, 'HTML', ''], [-1], False);
    end;
  end;
  //
  if Nome_2 <> '' then
  begin
    if not QrPreEmailLocate(-2) then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemail', False, [
        'Nome', 'SMTPServer', 'Send_Name',
        'Send_Mail', 'Pass_Mail', 'Logi_Name',
        'Logi_Pass', 'Logi_Auth', 'Mail_Titu',
        'Saudacao'
      ], ['Codigo'], [
        Nome_2, 'smtp.???.com.br', Send_Name,
        '???@???.com.br', 'senha', '???@???.com.br',
        'senha', 0, Mail_Titu, ''], [
        -2], False);
      //
      if Texto21 <> '' then
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False, [
        'Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'
      ], ['Controle'], [ -2, 1, 1, Texto21, 'HTML', ''], [-2], False);
      //
      if Texto22 <> '' then
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False, [
        'Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'
      ], ['Controle'], [ -2, 2, 2, Texto22, 'bar.jpg', '0000122'], [-3], False);
      //
      if Texto23 <> '' then
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False, [
        'Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'
      ], ['Controle'], [ -2, 3, 2, Texto23, 'bg.jpg', '0000133'], [-4], False);
      //
      if Texto24 <> '' then
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False, [
        'Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'
      ], ['Controle'], [ -2, 4, 2, Texto24, 'bot.jpg', '0000144'], [-5], False);
    end;
  end;
  //
  if Nome_3 <> '' then
  begin
    if not QrPreEmailLocate(-3) then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemail', False, [
        'Nome', 'SMTPServer', 'Send_Name',
        'Send_Mail', 'Pass_Mail', 'Logi_Name',
        'Logi_Pass', 'Logi_Auth', 'Mail_Titu',
        'Saudacao'
      ], ['Codigo'], [
        Nome_3, 'smtp.???.com.br', Send_Name,
        '???@???.com.br', 'senha', '???@???.com.br',
        'senha', 0, Mail_Titu, ''
      ], [-3], False);
      if Texto31 <> '' then
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'preemmsg', False, [
        'Codigo', 'Ordem', 'Tipo', 'Texto', 'Descricao', 'CidID_Img'
      ], ['Controle'], [ -3, 1, 1, Texto31, 'HTML', ''], [-6], False);
    end;
  end;
  //
  }
  Va(vpFirst);
end;

end.

