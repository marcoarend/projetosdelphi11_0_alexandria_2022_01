object FmPreEmailMsg: TFmPreEmailMsg
  Left = 388
  Top = 196
  Caption = 'PRE-EMAIL-002 :: Texto de Pr'#233'-email'
  ClientHeight = 565
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 403
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label2: TLabel
        Left = 68
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object DBEdit1: TDBEdit
        Left = 68
        Top = 20
        Width = 717
        Height = 21
        DataField = 'Nome'
        DataSource = FmPreEmail.DsPreEmail
        TabOrder = 0
      end
      object dmkDBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 20
        Width = 57
        Height = 21
        TabStop = False
        Color = clInactiveCaption
        DataField = 'Codigo'
        DataSource = FmPreEmail.DsPreEmail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBackground
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 48
      Width = 792
      Height = 85
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label9: TLabel
        Left = 8
        Top = 4
        Width = 42
        Height = 13
        Caption = 'Controle:'
      end
      object Label3: TLabel
        Left = 68
        Top = 4
        Width = 34
        Height = 13
        Caption = 'Ordem:'
      end
      object Label4: TLabel
        Left = 524
        Top = 5
        Width = 61
        Height = 13
        Caption = 'Descri'#231#227'o *'#185':'
      end
      object Label5: TLabel
        Left = 8
        Top = 48
        Width = 772
        Height = 13
        Caption = 
          '*'#185': Quando for imagem informe o nome do arquivo e seu "CID:" do ' +
          'mesmo modo que aparece no html ('#39'teste.jpg'#39' -> informe: teste.jp' +
          'g e '#39'cid:123abc'#39' informe 123abc).'
      end
      object Label6: TLabel
        Left = 4
        Top = 68
        Width = 185
        Height = 13
        Caption = 'Texto: (n'#227'o pode conter aspas duplas):'
      end
      object Label7: TLabel
        Left = 635
        Top = 5
        Width = 27
        Height = 13
        Caption = 'cid *'#185':'
      end
      object dmkEdControle: TdmkEdit
        Left = 8
        Top = 20
        Width = 57
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clBackground
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clInactiveCaption
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdOrdem: TdmkEdit
        Left = 66
        Top = 21
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Ordem'
        UpdCampo = 'Ordem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkRGTipo: TdmkRadioGroup
        Left = 152
        Top = 2
        Width = 309
        Height = 41
        Caption = ' Visualiza'#231#227'o '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Texto plano'
          'Texto HTML'
          'Imagem')
        TabOrder = 2
        QryCampo = 'Tipo'
        UpdCampo = 'Tipo'
        UpdType = utYes
        OldValor = 0
      end
      object dmkCkAtivo: TdmkCheckBox
        Left = 467
        Top = 25
        Width = 51
        Height = 17
        Caption = 'Ativo.'
        TabOrder = 3
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object dmkEdit1: TdmkEdit
        Left = 524
        Top = 21
        Width = 105
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Descricao'
        UpdCampo = 'Descricao'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Edit30: TdmkEdit
        Left = 635
        Top = 21
        Width = 150
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'CidID_Img'
        UpdCampo = 'CidID_Img'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object dmkMemo1: TdmkMemo
      Left = 0
      Top = 133
      Width = 576
      Height = 270
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssBoth
      TabOrder = 2
      QryCampo = 'Texto'
      UpdCampo = 'Texto'
      UpdType = utYes
      ExplicitWidth = 543
    end
    object Panel6: TPanel
      Left = 576
      Top = 133
      Width = 216
      Height = 270
      Align = alRight
      TabOrder = 3
      object LVItens: TListView
        Left = 1
        Top = 1
        Width = 214
        Height = 268
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Align = alClient
        Columns = <>
        TabOrder = 0
        OnDblClick = LVItensDblClick
        ExplicitLeft = 5
        ExplicitTop = 24
        ExplicitWidth = 224
        ExplicitHeight = 245
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 223
        Height = 32
        Caption = 'Texto de Pr'#233'-email'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 223
        Height = 32
        Caption = 'Texto de Pr'#233'-email'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 223
        Height = 32
        Caption = 'Texto de Pr'#233'-email'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 451
    Width = 792
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 495
    Width = 792
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 646
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 644
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BitBtn1: TBitBtn
        Tag = 227
        Left = 292
        Top = 3
        Width = 140
        Height = 40
        Caption = 'Carregar imagem'
        TabOrder = 1
        OnClick = BitBtn1Click
      end
    end
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Left = 140
    Top = 236
  end
end
