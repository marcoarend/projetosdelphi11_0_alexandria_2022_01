unit MailEnvEmail;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkDBGrid, DmkDAC_PF, UnDmkEnums;

type
  TFmMailEnvEmail = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    EdPesquisa: TdmkEdit;
    dmkDBGrid1: TdmkDBGrid;
    DsEntiMail: TDataSource;
    QrEntiMail: TmySQLQuery;
    QrEntiMailEMail: TWideStringField;
    QrEntiMailENTNOME: TWideStringField;
    QrEntiMailCONNOME: TWideStringField;
    QrEntiMailNOMECTO: TWideStringField;
    BtEntidades: TBitBtn;
    QrEntiMailCodigo: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPesquisaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dmkDBGrid1DblClick(Sender: TObject);
    procedure BtEntidadesClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenEntiMail(Descricao: String);
  public
    { Public declarations }
    FTipoEmail: Integer;
  end;

  var
  FmMailEnvEmail: TFmMailEnvEmail;

implementation

uses UnMyObjects, Module, MailEnv, Entidades, MyDBCheck, Entidade2;

{$R *.DFM}

procedure TFmMailEnvEmail.BtEntidadesClick(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade     := QrEntiMailCodigo.Value;
  VAR_CADASTRO := 0;
  VAR_CAD_NOME := '';
  //
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    if Entidade <> 0 then
      FmEntidade2.LocCod(Entidade, Entidade);
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    EdPesquisa.ValueVariant := VAR_CAD_NOME;
    ReopenEntiMail(VAR_CAD_NOME);
  end;
end;

procedure TFmMailEnvEmail.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMailEnvEmail.dmkDBGrid1DblClick(Sender: TObject);
var
  Email: String;
begin
  Email := QrEntiMailEMail.Value;
  //
  case FTipoEmail of
    0://Para
    begin
      if Length(FmMailEnv.EdPara.ValueVariant) = 0 then
        FmMailEnv.EdPara.ValueVariant := Email
      else
        FmMailEnv.EdPara.ValueVariant := FmMailEnv.EdPara.ValueVariant + ';' + Email
    end;
    1://CC
    begin
      if Length(FmMailEnv.EdCC.ValueVariant) = 0 then
        FmMailEnv.EdCC.ValueVariant := Email
      else
        FmMailEnv.EdCC.ValueVariant := FmMailEnv.EdCC.ValueVariant + ';' + Email;
    end;
    2://Cco
    begin
      if Length(FmMailEnv.EdCCO.ValueVariant) = 0 then
        FmMailEnv.EdCCO.ValueVariant := Email
      else
        FmMailEnv.EdCCO.ValueVariant := FmMailEnv.EdCCO.ValueVariant + ';' + Email;
    end;
  end;
  EdPesquisa.SetFocus;
end;

procedure TFmMailEnvEmail.EdPesquisaChange(Sender: TObject);
begin
  try
    Screen.Cursor := crHourGlass;
    ReopenEntiMail(EdPesquisa.ValueVariant);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMailEnvEmail.FormActivate(Sender: TObject);
begin
{$IfNDef ServicoDoWindows}
  MyObjects.CorIniComponente();
{$EndIf}
end;

procedure TFmMailEnvEmail.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmMailEnvEmail.FormResize(Sender: TObject);
begin
{$IfNDef ServicoDoWindows}
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
{$EndIf}
end;

procedure TFmMailEnvEmail.FormShow(Sender: TObject);
var
  R :TRect;
begin
  if SystemParametersInfo(SPI_GETWORKAREA, 0, @R, 0) then
    SetBounds(R.Right -Width, R.Bottom -Height, Width, Height);
end;

procedure TFmMailEnvEmail.ReopenEntiMail(Descricao: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntiMail, Dmod.MyDB, [
  'SELECT ema.EMail, con.Nome CONNOME, etc.Nome NOMECTO, ',
  'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) ENTNOME, ent.Codigo ',
  'FROM entimail ema ',
  'LEFT JOIN enticontat con ON con.Controle = ema.Controle ',
  'LEFT JOIN entidades ent ON ent.Codigo = ema.Codigo ',
  'LEFT JOIN entitipcto etc ON etc.Codigo=ema.EntiTipCto ',
  'WHERE IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) LIKE "%' + Descricao + '%" ',
  'OR con.Nome LIKE "%' + Descricao + '%" OR ema.EMail LIKE "%' + Descricao + '%" ',
  'ORDER BY ENTNOME, ema.Ordem, ema.Conta ',
  '']);
end;

end.
