object FmPreEmail: TFmPreEmail
  Left = 382
  Top = 172
  Caption = 'PRE-EMAIL-001 :: Cadastro de Pr'#233'-email'
  ClientHeight = 554
  ClientWidth = 780
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 94
    Width = 780
    Height = 460
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 780
      Height = 148
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label2: TLabel
        Left = 67
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label13: TLabel
        Left = 9
        Top = 43
        Width = 153
        Height = 13
        Caption = 'Nome de identifica'#231#227'o de envio:'
      end
      object Label19: TLabel
        Left = 229
        Top = 43
        Width = 41
        Height = 13
        Caption = 'Assunto:'
      end
      object Label23: TLabel
        Left = 556
        Top = 43
        Width = 52
        Height = 13
        Caption = 'Sauda'#231#227'o:'
      end
      object Label3: TLabel
        Left = 9
        Top = 106
        Width = 111
        Height = 13
        Caption = 'Configura'#231#227'o de e-mail:'
      end
      object DBEdit1: TDBEdit
        Left = 67
        Top = 20
        Width = 706
        Height = 21
        DataField = 'Nome'
        DataSource = DsPreEmail
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        TabStop = False
        Color = clInactiveCaption
        DataField = 'Codigo'
        DataSource = DsPreEmail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBackground
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object DBEdit4: TDBEdit
        Left = 8
        Top = 59
        Width = 217
        Height = 21
        DataField = 'Send_Name'
        DataSource = DsPreEmail
        TabOrder = 2
      end
      object DBEdit9: TDBEdit
        Left = 229
        Top = 59
        Width = 323
        Height = 21
        DataField = 'Mail_Titu'
        DataSource = DsPreEmail
        TabOrder = 3
      end
      object DBEdit10: TDBEdit
        Left = 556
        Top = 59
        Width = 217
        Height = 21
        DataField = 'Saudacao'
        DataSource = DsPreEmail
        TabOrder = 4
      end
      object DBCheckBox2: TDBCheckBox
        Left = 9
        Top = 86
        Width = 324
        Height = 16
        Caption = 'A mensagem n'#227'o conter'#225' anexos'
        DataField = 'NaoEnvBloq'
        DataSource = DsPreEmail
        TabOrder = 5
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBCheckBox4: TDBCheckBox
        Left = 340
        Top = 86
        Width = 324
        Height = 16
        Caption = 'Solicitar confirma'#231#227'o de leitura'
        DataField = 'ReturnReciept'
        DataSource = DsPreEmail
        TabOrder = 6
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit3: TDBEdit
        Left = 9
        Top = 122
        Width = 763
        Height = 21
        DataField = 'EmailConta_TXT'
        DataSource = DsPreEmail
        TabOrder = 7
      end
    end
    object PainelMsgs: TPanel
      Left = 0
      Top = 164
      Width = 780
      Height = 233
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitTop = 163
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 780
        Height = 233
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 975
        object TabSheet1: TTabSheet
          Caption = 'Cadastros de itens (textos e imagens)'
          object PageControl2: TPageControl
            Left = 293
            Top = 0
            Width = 479
            Height = 205
            ActivePage = TSTxtPlano
            Align = alClient
            TabOrder = 9
            ExplicitWidth = 674
            ExplicitHeight = 202
            object TSTxtPlano: TTabSheet
              Caption = 'Texto plano'
              object DBMemo1: TDBMemo
                Left = 0
                Top = 0
                Width = 471
                Height = 177
                Align = alClient
                DataField = 'Texto'
                DataSource = DsPreEmMsg
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Courier New'
                Font.Style = []
                ParentFont = False
                ScrollBars = ssBoth
                TabOrder = 0
                ExplicitWidth = 474
                ExplicitHeight = 183
              end
              object Memo15: TMemo
                Left = 252
                Top = 38
                Width = 118
                Height = 34
                Lines.Strings = (
                  
                    '<!DOCTYPE html PUBLIC '#39'-//W3C//DTD XHTML 1.0 Transitional//EN'#39' '#39 +
                    'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'#39'>'
                  '<html xmlns='#39'http://www.w3.org/1999/xhtml'#39'>'
                  '<head>'
                  '<title>[Empresa]</title>'
                  
                    '<meta http-equiv='#39'Content-Type'#39' content='#39'text/html; charset=iso-' +
                    '8859-1'#39'>'
                  '<body style='#39'background-color:#FFFFFF; margin:0; padding:0;'#39'>'
                  '  <table cellpadding='#39'0'#39' cellspacing='#39'0'#39' border='#39'0'#39' width='#39'500'#39'>'
                  '    <tr>'
                  
                    '      <td height='#39'50'#39' style='#39'font-family:Verdana, Arial, Helveti' +
                    'ca, sans-serif; font-weight:bold; font-size:18px; color:#003399;' +
                    #39' align='#39'center'#39'>'
                  '        <p>[Empresa]</p>'
                  '      </td>'
                  '    </tr>'
                  '    <tr>'
                  
                    '      <td height='#39'25'#39' align='#39'center'#39' style='#39'background-color:#FF' +
                    '4500; border-bottom:1px solid #FF0000;'#39'>'
                  
                    '        <p style='#39'font-family: Arial, Helvetica, sans-serif; fon' +
                    't-weight:bold; font-size:14px; color:#FFFFFF;'#39'>[Saudacao] [Clien' +
                    'te]</p>'
                  '      </td>'
                  '    </tr>'
                  '    <tr>'
                  
                    '      <td style='#39'line-height:15px; padding:10px; font-family:Ver' +
                    'dana, Arial, Helvetica, sans-serif; font-size:12px; font-weight:' +
                    'normal; color:#FF0000; border-left:1px solid #FF0000; border-rig' +
                    'ht:1px solid #FF0000;'#39'>'
                  
                    '        Segue em anexo o XML com protocolo de CANCELAMENTO da NF' +
                    'Se: [ChaveNFe].<br/>'
                  '      </td>'
                  '    </tr>'
                  '    <tr>'
                  
                    '      <td style='#39'margin: 0; padding: 0; list-style: none; paddin' +
                    'g:10px; border-bottom:1px solid #FF0000; border-left:1px solid #' +
                    'FF0000; border-right:1px solid #FF0000;'#39'>&nbsp;</td>'
                  '    </tr>'
                  '  </table>'
                  '</body>'
                  '</html>')
                TabOrder = 1
                Visible = False
                WantReturns = False
                WordWrap = False
              end
              object Memo14: TMemo
                Left = 252
                Top = 0
                Width = 118
                Height = 33
                Lines.Strings = (
                  
                    '<!DOCTYPE html PUBLIC '#39'-//W3C//DTD XHTML 1.0 Transitional//EN'#39' '#39 +
                    'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'#39'>'
                  '<html xmlns='#39'http://www.w3.org/1999/xhtml'#39'>'
                  '<head>'
                  '<title>[Empresa]</title>'
                  
                    '<meta http-equiv='#39'Content-Type'#39' content='#39'text/html; charset=iso-' +
                    '8859-1'#39'>'
                  '<body style='#39'background-color:#FFFFFF; margin:0; padding:0;'#39'>'
                  '  <table cellpadding='#39'0'#39' cellspacing='#39'0'#39' border='#39'0'#39' width='#39'500'#39'>'
                  '    <tr>'
                  '      <td height='#39'50'#39' align='#39'center'#39'>'
                  
                    '        <p style='#39'font-family:Verdana, Arial, Helvetica, sans-se' +
                    'rif; font-weight:bold; font-size:18px; color:#003399;'#39'>[Empresa]' +
                    '</p>'
                  '      </td>'
                  '    </tr>'
                  '    <tr>'
                  
                    '      <td height='#39'25'#39' align='#39'center'#39' style='#39'background-color:#F3' +
                    'F3F3;  border-bottom:1px solid #CCCCCC;'#39'>'
                  
                    '        <p style='#39'font-family: Arial, Helvetica, sans-serif; fon' +
                    't-weight:bold; font-size:14px; color:#000000;'#39'>[Saudacao] [Clien' +
                    'te]</p>'
                  '      </td>'
                  '    </tr>'
                  '    <tr>'
                  
                    '      <td style='#39'line-height:15px; padding:10px; font-family:Ver' +
                    'dana, Arial, Helvetica, sans-serif; font-size:12px; font-weight:' +
                    'normal; color:#000000; border-left:1px solid #CCCCCC; border-rig' +
                    'ht:1px solid #CCCCCC;'#39'>'
                  
                    '        Segue em anexo o XML com protocolo de autoriza'#231#227'o e o DA' +
                    'NFSE da NFSe: [ChaveNFe].<br/>'
                  '      </td>'
                  '    </tr>'
                  '    <tr>'
                  
                    '      <td style='#39'margin: 0; padding: 0; list-style: none; paddin' +
                    'g:10px; border-bottom:1px solid #CCCCCC; border-left:1px solid #' +
                    'CCCCCC; border-right:1px solid #CCCCCC;'#39'>&nbsp;</td>'
                  '    </tr>'
                  '  </table>'
                  '</body>'
                  '</html>')
                TabOrder = 2
                Visible = False
                WantReturns = False
                WordWrap = False
              end
              object Memo16: TMemo
                Left = 252
                Top = 78
                Width = 118
                Height = 33
                Lines.Strings = (
                  '<HTML>'
                  
                    '<P>Clique no link abaixo para configurar seus dados no site da E' +
                    'mpresa ????</P>'
                  '</BR>'
                  '<P>[LINK]</P>'
                  '</BR>'
                  
                    '<P>Por motivo de seguran'#231'a este link ir'#225' expirar no dia [DATAEXP' +
                    '] ou ap'#243's concluir o cadastro.</P>'
                  '</BR>'
                  
                    '<P>Caso voc'#234' n'#227'o tenha solicitado sua senha desconsidere esta me' +
                    'nsagem ou entre em contato clicando no link abaixo</P>'
                  '</BR>'
                  '<P>'
                  
                    '<A HREF="Link para contato no site ???">Link para contato no sit' +
                    'e ???</A>'
                  '</P>'
                  '</BR>'
                  
                    '<P>*************************************************************' +
                    '*****************************</P>'
                  '</BR>'
                  '<P>N'#227'o responda esta mensagem.</P>'
                  '</HTML>')
                TabOrder = 3
                Visible = False
                WantReturns = False
                WordWrap = False
              end
            end
            object TSTxtHTML: TTabSheet
              Caption = 'Texto HTML'
              ImageIndex = 1
              object WebBrowser1: TWebBrowser
                Left = 0
                Top = 0
                Width = 474
                Height = 183
                Align = alClient
                TabOrder = 0
                ControlData = {
                  4C00000008310000EF1200000000000000000000000000000000000000000000
                  000000004C000000000000000000000001000000E0D057007335CF11AE690800
                  2B2E126208000000000000004C0000000114020000000000C000000000000046
                  8000000000000000000000000000000000000000000000000000000000000000
                  00000000000000000100000000000000000000000000000000000000}
              end
            end
          end
          object dmkDBGrid1: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 293
            Height = 205
            Align = alLeft
            Columns = <
              item
                Expanded = False
                FieldName = 'Ordem'
                Width = 30
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ativo'
                Width = 14
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETIPO'
                Title.Caption = 'Tipo'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Title.Caption = 'Nome'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CidID_Img'
                Title.Caption = 'cid imagem'
                Width = 48
                Visible = True
              end>
            Color = clWindow
            DataSource = DsPreEmMsg
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Ordem'
                Width = 30
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ativo'
                Width = 14
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETIPO'
                Title.Caption = 'Tipo'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Title.Caption = 'Nome'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CidID_Img'
                Title.Caption = 'cid imagem'
                Width = 48
                Visible = True
              end>
          end
          object Memo1: TMemo
            Left = 298
            Top = 24
            Width = 119
            Height = 32
            Lines.Strings = (
              
                '<!DOCTYPE html PUBLIC '#39'-//W3C//DTD XHTML 1.0 Transitional//EN'#39' '#39 +
                'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'#39'>'
              '<html xmlns='#39'http://www.w3.org/1999/xhtml'#39'>'
              '<head>'
              '<title>[Administradora]</title>'
              
                '<meta http-equiv='#39'Content-Type'#39' content='#39'text/html; charset=iso-' +
                '8859-1'#39'>'
              '<body style='#39'background-color:#FFFFFF; margin:0; padding:0;'#39'>'
              '  [Protocolo]'
              '  <table cellpadding='#39'0'#39'cellspacing='#39'0'#39' border='#39'0'#39'width='#39'500'#39'>'
              '    <tr>'
              '      <td height='#39'50'#39' align='#39'center'#39'>'
              
                '        <p style='#39'font-family:Verdana, Arial,Helvetica, sans-ser' +
                'if; font-weight:bold; font-size:18px; color:#003399;'#39'>[Administr' +
                'adora]</p>'
              '      </td>'
              '    </tr>'
              '    <tr>'
              
                '      <td height='#39'25'#39' align='#39'center'#39' style='#39'background-color:#F3' +
                'F3F3; border-bottom:1px solid #CCCCCC;'#39'>'
              
                '        <p style='#39'font-family: Arial, Helvetica,sans-serif; font' +
                '-weight:bold; font-size:14px; color:#000000;'#39'>[Saudacao] [Pronom' +
                'e] [Nome]</p>'
              '      </td>'
              '    </tr>'
              '    <tr>'
              
                '      <td style='#39'line-height:15px; padding:10px; font-family:Ver' +
                'dana, Arial, Helvetica,sans-serif; font-size:12px; font-weight:n' +
                'ormal; color:#000000; border-left:1px solid #CCCCCC; border-righ' +
                't:1px solid #CCCCCC;'#39'>'
              '        Segue em anexo seu bloqueto do [Condominio] <br/>'
              '        no valor de [Valor] para pagamento em [Vencimento]<br/>'
              
                '        Favor confirmar o recebimento clicando no bot'#227'o abaixo.<' +
                'br/>'
              '      </td>'
              '    </tr>'
              '    <tr>'
              
                '      <td style='#39'margin: 0; padding: 0; list-style: none; paddin' +
                'g:10px; border-bottom:1px solid #CCCCCC; border-left:1px solid #' +
                'CCCCCC; border-right:1px solid #CCCCCC;'#39'>'
              '        <ul style='#39'padding:0; margin:0;'#39'>'
              
                '          <li style='#39'float: left; margin: 0; padding: 0; text-al' +
                'ign: center; list-style:none;'#39'>'
              
                '            <a style='#39'font-family:Verdana, Arial, Helvetica,sans' +
                '-serif; text-decoration: none; text-transform: uppercase; font-s' +
                'ize: 65%; font-weight: bold; letter-spacing: 0.1em; margin: 0 1p' +
                'x 0 0; padding: 8px 8px; float: left; display: block; -moz-user-' +
                'select: none; -khtml-user-select: none; background: #F0F0F0; col' +
                'or: #003399; border-top: solid 1px #FFFFFF; border-right: solid ' +
                '1px #CCCCCC; border-bottom: solid 1px #CCCCCC; border-left: soli' +
                'd 1px #FFFFFF;'#39' href='#39'[LinkConfirma]'#39' title='#39'Clique aqui'#39'>Clique' +
                ' Aqui</a>'
              '          </li>'
              '        </ul>'
              '      </td>'
              '    </tr>'
              '  </table>'
              '</body>'
              '</html>')
            TabOrder = 1
            Visible = False
            WantReturns = False
            WordWrap = False
          end
          object Memo2: TMemo
            Left = 298
            Top = 62
            Width = 119
            Height = 33
            Lines.Strings = (
              
                '<!DOCTYPE html PUBLIC '#39'-//W3C//DTD XHTML 1.0 Transitional//EN'#39' '#39 +
                'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'#39'>'
              '<html xmlns='#39'http://www.w3.org/1999/xhtml'#39'>'
              '<head>'
              '<title>[Administradora]</title>'
              
                '<meta http-equiv='#39'Content-Type'#39' content='#39'text/html; charset=iso-' +
                '8859-1'#39'>'
              '<style type='#39'text/css'#39'>'
              '<!--'
              'body {'
              '                background-color:#EEEEEE;'
              '}'
              '.texto {'
              #9'font-family:Verdana, Arial, '
              'Helvetica, sans-serif;'
              #9'font-size:12px;'
              #9'color:#333333;'
              #9'margin-left:10px;'
              #9'line-height:15px;'
              '}'
              '.texto2 p {'
              '                font-family:Arial, Helvetica, sans-serif;'
              #9'font-size:16px;'
              #9'font-weight:bold;'
              #9'text-align:center;'
              #9'color:#FFFFFF;'
              #9'padding:0;'
              #9'margin:0;'
              #9'padding-top:2px;'
              '}'
              'img {'
              '                border:0;'
              '}'
              '#transport {'
              #9'margin: 0;'
              #9'padding: 0;'
              #9'list-style: none;'
              #9'padding:5px;'
              '}'
              '#transport ul {'
              '                padding:0;'
              #9'margin:0;'
              '}'
              '#transport li {'
              #9'float: left;'
              #9'margin: 0;'
              #9'padding: 0;'
              #9'text-align: center;'
              #9'list-style:none;'
              '}'
              '#transport a {'
              
                '                font-family:Verdana, Arial, Helvetica, sans-seri' +
                'f;'
              #9'text-decoration: none;'
              #9'text-transform: uppercase;'
              #9'font-size: 65%;'
              #9'font-weight: bold;'
              #9'letter-spacing: 0.1em;'
              #9'margin: 0 1px 0 0;'
              #9'padding: 2px 2px;'
              #9'float: left;'
              #9'display: block;'
              #9'-moz-user-select: none;'
              #9'-khtml-user-select: none;'
              #9'background: #000A92;'
              #9'color: #FFFFFF;'
              #9'border-top: solid 1px #FFFFFF;'
              #9'border-right: solid 1px #FFFFFF;'
              #9'border-bottom: solid 1px #FFFFFF;'
              #9'border-left: solid 1px #FFFFFF;'
              '}'
              '#transport a:hover {'
              #9'background: #03349A;'
              #9'color: #F8ED21;'
              #9'border-top: solid 1px #FAF269;'
              #9'border-right: solid 1px #F8ED21;'
              #9'border-bottom: solid 1px #F8ED21;'
              #9'border-left: solid 1px #F8ED21;'
              '}'
              '#transport a:focus {'
              #9'-moz-user-select: none;'
              #9'-khtml-user-select: none;'
              '}'
              '-->'
              '</style>'
              '<body style='#39'margin:0'#39'>'
              '  [Protocolo]'
              '  <div style='#39'position:relative;width:800px;margin:auto'#39'>'
              '    <div style='#39'position:relative'#39'>'
              '      <div><img src='#39'cid:0000133'#39' /></div>'
              
                '      <table cellpadding='#39'0'#39' cellspacing='#39'0'#39' border='#39'0'#39' width='#39'7' +
                '20'#39' style='#39'position:absolute; left: 39px; top:77px; top:68px;'#39'>'
              '        <tr>'
              '          <td>'
              
                '            <table width='#39'500'#39' cellpadding='#39'0'#39' cellspacing='#39'0'#39' b' +
                'order='#39'0'#39' style='#39'margin-left:99px'#39'>'
              '              <tr>'
              '                <td height='#39'25'#39'>'
              '                  <div style='#39'position:relative'#39'>'
              
                '                    <div style='#39'position:absolute'#39'><img src='#39'cid' +
                ':0000122'#39' /></div>'
              
                '                    <div style='#39'position:absolute;width:500px'#39' c' +
                'lass='#39'texto2'#39'><p>[Saudacao] [Pronome] [Nome]</p></div>'
              '                  </div>'
              '                </td>'
              '              </tr>'
              '              <tr>'
              
                '                <td style='#39'background-color:#FFFFFF'#39' height='#39'127' +
                #39'>'
              '                  <div class='#39'texto'#39'>'
              
                '                    Segue em anexo seu bloqueto do [Condominio] ' +
                '<br/>'
              
                '                    no valor de [Valor] para pagamento em [Venci' +
                'mento]<br/>'
              
                '                    Favor confirmar o recebimento clicando no bo' +
                't'#227'o abaixo.<br/>'
              '                  </div>'
              '                  <div id='#39'transport'#39'>'
              '                      <ul>'
              
                '                        <li><a href='#39'[LinkConfirma]'#39' title='#39'Cliq' +
                'ue aqui'#39'>Clique aqui para confirmar o recebimento</a></li>'
              '                      </ul>'
              '                  </div>'
              '                </td>'
              '              </tr>'
              '            </table>'
              '          </td>'
              '        </tr>'
              '        <tr>'
              '          <td><img src='#39'cid:0000144'#39' /></td>'
              '        </tr>'
              '      </table>'
              '    </div>'
              '  </div>'
              '</body>'
              '</html>'
              ''
              '')
            TabOrder = 2
            Visible = False
            WantReturns = False
            WordWrap = False
          end
          object Memo3: TMemo
            Left = 298
            Top = 102
            Width = 119
            Height = 33
            Lines.Strings = (
              '/9j/4AAQSkZJRgABAQEASABIAAD/'
              '2wBDAAYEBAQFBAYFBQYJBgUGC'
              'QsIBgYICwwKCgsKCgwQ'
              'DAwMDAwMEAwODxAPDgwTExQU'
              'ExMcGxsbHCAgICAgICAgICD/2wBD'
              'AQcHBw0MDRgQEBgaFREV'
              'GiAgICAgICAgICAgICAgICAgICAgICA'
              'gICAgICAgICAgICAgICAgICAgICAgIC'
              'AgICD/wAAR'
              'CAAZAfQDAREAAhEBAxEB/8QAFw'
              'ABAQEBAAAAAAAAAAAAAAAAAAY'
              'HCP/EAB8QAAEBCQEAAAAA'
              'AAAAAAAAAAABAgQVFhdRUlWj4v/'
              'EABkBAQACAwAAAAAAAAAAAAAA'
              'AAADBAIFBv/EABkRAQEA'
              'AwEAAAAAAAAAAAAAAAASARETF'
              'P/aAAwDAQACEQMRAD8A0CYmbo'
              'dT51CiYmboPOUTEzdB5yiY'
              'mboPOUTEzdB5yiYmboPOUTEzdB5'
              'yiYmboPOUTEzdB5yiYmboPOUTEzd'
              'B5yiYmboPOUTEzdB5'
              'yiYmboPOUTEzdB5yiYmboPOUTEzd'
              'B5yiYmboPOUTEzdB5yiYmboPOUTE'
              'zdB5yiYmboPOUTEz'
              'dB5yiYmboPOUTEzdB5yiYmboPOUT'
              'EzdB5yiYmboPOUTEzdB5yiYmboPO'
              'UTEzdB5yiYmboPOU'
              'TEzdB5yiYmboPOUTEzdB5yiYmboP'
              'OUTEzdB5yiYmboPOUTEzdB5yiYmb'
              'oPOUTEzdB5yiYmbo'
              'POUTEzdB5yiYmboPOUTEzdB5yiYm'
              'boPOUTEzdB5yiYmboPOUTEzdB5yi'
              'YmboPOUTEzdB5yiY'
              'mboPOUTEzdB5yiYmboPOUTEzdB5'
              'yiYmboPOUTEzdB5yiYmboPOUTEzd'
              'B5yiYmboPOUTEzdB5'
              'yiYmboPOUTEzdB5ynPVZXnWd/Bv'
              '+GEUlZXnWd/A4YJKyvOs7+BwwSV'
              'ledZ38DhgkrK86zv4H'
              'DBJWV51nfwOGCSsrzrO/gcMElZXn'
              'Wd/A4YJKyvOs7+BwwSVledZ38Dh'
              'gkrK86zv4HDBJWV51'
              'nfwOGCSsrzrO/gcMElZXnWd/A4YJ'
              'KyvOs7+BwwSVledZ38DhgkrK86zv4'
              'HDBJWV51nfwOGCS'
              'srzrO/gcMElZXnWd/A4YJKyvOs7+B'
              'wwSVledZ38DhgkrK86zv4HDBJWV'
              '51nfwOGCSsrzrO/g'
              'cMElZXnWd/A4YJKyvOs7+BwwSVle'
              'dZ38DhgkrK86zv4HDBJWV51nfwO'
              'GCSsrzrO/gcMElZXn'
              'Wd/A4YJKyvOs7+BwwSVledZ38Dh'
              'gkrK86zv4HDBJWV51nfwOGCSsrzr'
              'O/gcMElZXnWd/A4YJ'
              'KyvOs7+BwwSVledZ38DhgkrK86zv4'
              'HDBJWV51nfwOGCSsrzrO/gcMElZX'
              'nWd/A4YJKyvOs7+'
              'BwwSVledZ38DhgkrK86zv4HDBJW'
              'V51nfwOGCSsrzrO/gcMElZXnWd/A'
              '4YJKyvOs7+BwwSVle'
              'dZ38DhgkrK86zv4HDBJWV51nfwO'
              'GCSsrzrO/gcMElZXnWd/A4YJKyvOs'
              '7+BwwSVledZ38Dhg'
              'krK86zv4HDBJWV51nfwOGCSsrzrO/'
              'gcMElZXnWd/A4YJKyvOs7+BwwSm'
              '4EmJd0jogSYjRRAk'
              'xGiiBJiNFECTEaKIEmI0UQJMRoogS'
              'YjRRAkxGiiBJiNFECTEaKIEmI0UQJ'
              'MRoogSYjRRAkxG'
              'iiBJiNFECTEaKIEmI0UQJMRoogSYj'
              'RRAkxGiiBJiNFECTEaKIEmI0UQJM'
              'RoogSYjRRAkxGii'
              'BJiNFECTEaKIEmI0UQJMRoogSYjR'
              'RAkxGiiBJiNFECTEaKIEmI0UQJMRo'
              'ogSYjRRAkxGiiBJ'
              'iNFECTEaKIEmI0UQJMRoogSYjRRA'
              'kxGiiBJiNFECTEaKIEmI0UQJMRoog'
              'SYjRRAkxGiiBJiN'
              'FECTEaKIEmI0UQJMRoogSYjRRAk'
              'xGiiBJiNFECTEaKIEmI0UQJMRoogS'
              'YjRRAkxGilOZIgAA'
              'AAAAAAAAAAAAAAAAAAAAAAAAA'
              'AAAAAAAAAAAAAAAAAAAAAAAAA'
              'AAAAAAAAAAAAAAAAAAAB//'
              '2Q=='
              ''
              '')
            TabOrder = 3
            Visible = False
            WantReturns = False
            WordWrap = False
          end
          object Memo4: TMemo
            Left = 298
            Top = 141
            Width = 119
            Height = 33
            Lines.Strings = (
              '/9j/4AAQSkZJRgABAQEASABIAAD/'
              '2wBDAAYEBAQFBAYFBQYJBgUGC'
              'QsIBgYICwwKCgsKCgwQ'
              'DAwMDAwMEAwODxAPDgwTExQU'
              'ExMcGxsbHCAgICAgICAgICD/2wBD'
              'AQcHBw0MDRgQEBgaFREV'
              'GiAgICAgICAgICAgICAgICAgICAgICA'
              'gICAgICAgICAgICAgICAgICAgICAgIC'
              'AgICD/wAAR'
              'CACWAyADAREAAhEBAxEB/8QAH'
              'AABAAMBAQEBAQAAAAAAAAAAAA'
              'QFBgcDAgEI/8QAShAAAQMD'
              'AwMCAQYKBgYLAAAAAAECEwME'
              'BQYREgchMSJBURQVMmFxgRYjM'
              '0JSZJGhsrMIJlV0ktIXJVNy'
              'grEkYmV1g5OjwcLR8P/EABsBAQA'
              'CAwEBAAAAAAAAAAAAAAADBQEC'
              'BAYH/8QARBEAAgIBAQUE'
              'BQkGAwgDAAAAABEBAgMEBQYSI'
              'TETQVFhFCIycYEHM3ORobHB0fAV'
              'IyQlQrIWUlM0NUNicpKi'
              '4cLS8f/aAAwDAQACEQMRAD8A/n'
              'mE+nMrRCGBCGBCGBCGBCGBCGB'
              'CGBCGBCGBCGBCGBCGBCGB'
              'CGBCGBCGBCGBCGBCGBCGBCGB'
              'CGBCGBCGBCGCbhcT85Zmwxyu4J'
              'eXFK35/CV6M3+7c5ddqexw'
              'Xy9eCs2+qGb0q7RHidq1H0w6DaYv'
              'm47OZvJW166m2tw9VTdjlVEXelbPb'
              '5avufLdm7z7w6/H'
              '2unw4rUa8OfxyRPeWmTTafHKtMv9'
              'eRnOqXS/R+C0nidSaYvLq5tMlWbT'
              'b8pVNnMqU3VGuakd'
              'JyfQ9y73V3n1ur1mXS6utK3x1fq+MT'
              'ETHtWjv7iHVaWlKRarUnK4T6CyuEI'
              'YEIYJWLwt/lch'
              'Qx2PouuLy5dwo0m+VX/95OfVavHp'
              '8c5ck8NKw5k2rWbSo6naKfR3ppo+'
              'zo1+oObV99WTdLK3'
              'VyN+5tNrqz0RfzuyHy+2+O1NpXmuz'
              'cPqR/VZffMxWPdzLT0PFjj95PM9KP'
              'TPolrGS20fmatl'
              'lGt3p0Ki1FR3/h3DWvd/wO7Gl95dv'
              'bMVtbhi+LxhffSZiPjBmNNgy+xKk4/qj'
              'SOX0zma2Jyt'
              'KO5pd0cndj2L4exfdqn0rZW1sOuwR'
              'mwy6z9cT4T5lblxTSykqYSxZEIQwIQ'
              'wIQwIQwIQwIQw'
              'IQwIQwIQwIQwIQwIQwIQwIQwIQwI'
              'QwIQwIQwIQwIQwIQwIQwIQwIQwIQ'
              'wIQwIQwIQwIQwIQw'
              'IQwIQwIQwIQwIQwIQwIQwIQwIQwI'
              'QwIQwIQwIQwIQwIQwIQwIQwIQwIQ'
              'wIQwIQwIQwIQwIQw'
              'IQwIQwIQwIQwIQwIQwIQwIQwIQwT'
              '4SJmRCGBCGBCGBCGBCGBCGBC'
              'GBCGBCGBCGBCGBCGBCGB'
              'CGBCGBCGBCGBCGBCGBCGBCGB'
              'CGBCGBCGBCGC40bS/rfg/wDvC1/'
              'nNKzbU/wWb6K/9skuD249'
              '8G7/AKR1Plr6gv8A2fR/mVTyHyaT/'
              'LZ+lt91Ts2n858CywvVXp7V0VitPap'
              'wt1frjWtRqMbT'
              'dT5MRWtem9Wmu/F3wOLXbo7Trr8'
              'up0eamPtfGZfPnMezPeb01mLs4reJl'
              'EXrHpXR9rpnT2e0'
              '7j/m9mUSRWd91p1KTajOScnoipv7'
              'E25W1dbk1eo02qydpOLl8YtMSuUc'
              'vea63FSKVtWEzkkJ'
              '9JZWkvH4LK5Jz2Y6yr3r6acqjbek+qr'
              'U+K8EXY5tTrsOCInLetIn/NMR95tW'
              'k26Qzc9GM/pX'
              'TObv8tnXrTuKVstPHt4Ofu9y+vbZF2'
              'ds3bddvKnkt99m6zX4KYdPDrN3fnE'
              'cu73x3/A7NDlp'
              'S0zYxmfy+QzuYusrkKi1Lm6er3b+Gp'
              '7Nb8GtTsiHqtn6HHpMFcOOFWsfqff'
              'JyZMk3lybbpX0'
              'uvs6v4Q3N87D4jHv5pfNVG1FdT7qt'
              'NXelqN93KeT3t3qx6T+GrTts2SPZ7u'
              'fj3y+6IOzSaWb'
              '+s1EHRdY6k6F6ouKFHOZB1xc2jVo'
              '0ryky4bty239dNnB3dN/Gx4jYuy94dB'
              'WbafHFa35zWZp'
              '90y4+87c2XT5Panp7zB676LMxmK/'
              'CHS97874LjJU7o+oxn6fJnpe1Pfsmx'
              '6/d/fft83ousp2'
              'Oo6eETPgp5xPh1ZyajQ8McVJdTE6'
              'M01S1DqjHYarW+TU7yrwfW8qiIiuX'
              'bf3XbZPrPWbb2lO'
              'i0mTPEcU0joceDHx3ividiuuhPSzF1V'
              'pZnU9S1qr3Yyrc2ls7ivjtUYu58xxb+7'
              'Y1EPBpotH'
              'lTJb7pLSdBhr7VvtgyXUXpjoXBYFMt'
              'gNRJfOWo1jLR1SjXdUR3lWvo8fop3'
              '+iej3a3o2hq9R'
              '2Op0/BymeJWqvfFn195zanS46Vdb'
              'M5hCe/ZXiEMCEMCEMCEMCEMCE'
              'MCEMCEMCEMCEMCEMCEM'
              'CEMCEMCEMCEMCEMCEMCEMCE'
              'MCEMCEMCEMCEMCEMCEMCEMC'
              'EMCEMCEMCEMCEMCEMCEMCEM'
              'CEM'
              'CEMCEMCEMCEMCEMCEMCEMCE'
              'MCEMCEMCEMCEMCEMCEMCEMC'
              'EMCEMCEMCEMCEMCEMCEMCEM'
              'FhC'
              'RsyIQwIQwIQwIQwIQwIQwIQwIQwI'
              'QwIQwIQwIQwIQwIQwIQwIQwIQwIQ'
              'wIQwIQwIQwIQwIQw'
              'IQwIQwW2kaP9bML/AH+2/nNKzbU'
              '/wWb6K/8AbJLh9uPfBt/6QtPlrqiv6h'
              'S/mVDyPyaz/Lp+'
              'lt91Tr2n858DmUJ9BZXnXuqaNd0t0'
              'O3fulvR7fZatQ+Y7ov9sa3/AKrf3yW'
              'ms+Zp+u4x2gum'
              'WY1bdotNq22Kpu2ub9yelPi1n6Tv+'
              'Xueo3i3pwbNpz9bNPSn4z4R+oOXT'
              'aW2WfI6NlOo2mOn'
              '1Ojp7SFnTvXUHp85V3O9LlT6SSJ9'
              'Kovx8N/ceF0e6+s21M6rXXmkWj1I8'
              'PDl3V+2ftO6+qph'
              '9WnPxPHUmiNN9RcY/U2kHNoZdE'
              '/6bYLszm/yqPT82p8HeHfvJNmbd1e'
              'w80aTXO2H+m/VR5eN'
              'fLrH2GMuCueOPH1OL3eOubO5qWt'
              '1SdRuKLlZVpPTZzXJ7Kh9Yw6imWk'
              'XpMWrPSYKqazHKTea'
              'l1/Y3XTzDaUxDKlvAxnzoqojWvcxu6'
              'omyrujqiq5Txuyt2smPambW55i3FM8'
              'Hk5/CvI7Mupi'
              'cUUr8Tn0J7ZnCdb/AKPeTuvnnIYCq'
              'smMurV9Z1B3dqPa5rF2T/rNeqKfM/l'
              'J0dOwx6mOWWl4'
              'h+UxM/ZMciz2beeKa9yM/oXE0rXrD'
              'aWNJdqVpkK7Ka+fTR57fwl3vBq5yb'
              'CtknrfFWf+7h/M'
              'h09VnXhJ0vqH0ZyOrNUvy1PIUbS3'
              'dSp00a5jnv8AQi7rt2T954DdrfjFs3Rx'
              'hnHa9uKZ6xEc'
              'yw1OhnJdtGA1h0Y/Bt2IauXp3dTJ3d'
              'O0WhHE9vP89u738mp4Ve3lD2mxN'
              '+PT+1/czSMWObtu'
              'OXdPKFP/ALOLPoezXNuSL1f0Jg9K'
              'ZextcS6qtO4t5KrKzkeqKjlbui7J52On'
              'crb+o2jgvfMn'
              'W6hcu5/Yaa3BXHaIgu9adJNPYp2k'
              '7axr1m18zc0rS7qVFR35RWItRrdk22'
              'V/gqdhb46rU+lX'
              'yVrw4KTeq8nymfgTZ9HWvCv6pNH'
              'X6G9OcblqLMhla6U71zaWPx76jG1'
              'H1PC+pG8nJuvsibfE'
              'oce/21c+CZxYqvHEze6lRH1qPrl+BP'
              'OgxVnnPUx2e6PK7qOmmcHUVLOp'
              'QZdvrVvXBSVeL+W2'
              '3L1J6ftT7T1Gzt9Fsr0vUR68WmijlxT'
              '3e7l190+45cmi/e8FehqWdEum93Uu'
              'cLYZys/UFsxX'
              'VEkpP4OTsvKkjU7IqpunLdPieetv3tX'
              'HFdRkwVjTWnlytD91n18OSk6PQM'
              'U+rFvWOL5fC3WK'
              'yl1jbtu1zaVXUau3jdq7bp9S+UPq+i1t'
              'NThrlp7N4iY+JVXpNZU9xEhOlmghD'
              'AhDAhDAhDAh'
              'DAhDAhDAhDAhDAhDAhDAhDAhD'
              'AhDAhDAhDAhDAhDAhDAhDAhDA'
              'hDAhDAhDAhDAhDAhDAhDAh'
              'DAhDAhDAhDAhDAhDAhDAhDAhD'
              'AhDAhDAhDAhDAhDAhDAhDAhDA'
              'hDAhDAhDAhDAhDAhDAhDAh'
              'DBPhI2ZEIYEIYEIYEIYEIYEIYEIYEI'
              'YEIYEIYEIYEIYEIYEIYEIYEIYEIYEIY'
              'EIYEIYEIYEI'
              'YEIYEIYEIYLXSlHbVOH/AL9bfzWlbt'
              'mf4LN9Ff8Atklw+3Hvg2fXuny1tRX9'
              'RpfzKh5H5N5/'
              'l0/S2+6p17S+c+BzeE+gMrzpOhumu'
              'RzlrRyepbqpbaatGyUWVaipzYnnhuu'
              '1Onsnn9h8+3h3'
              'qxaO84dJWLau8qVHSfP/ADW8vrL'
              'DT6Sbw7+xB7a76nsqWn4O6Raljha'
              'SRvr0k4OqN/RZ7tb9'
              'flTTd3c+Yv6Xrv3monmp5r3+M/ZBn'
              'UazlwU5VOYQn0RlcWenc7l9P5Knk'
              'MZWWjXZ9JPLXt92'
              'vT3RSu2nszBrcU4s0Os/XHnBJiyWp'
              'Lg61XtNK9VsZNR4Y3Vduz1t/S2+Pj'
              'mz6/LT5hTLrd2s'
              '3DZ5dFaf17reXSS0mKamPC5yDNae'
              'yeFyFSwyVBaFzT8tXwqezmr4VF+J9'
              'T0G0cOrxRlw24qT'
              '+lPmVWTHNJUkGE7WaHSOglPjraq'
              'v6jV/jpngPlIn+XR9LX7rFhs35z4Hzov'
              'hS6383rsi39+3'
              '73trNT96m23om27qj/SxfZNJMYP9p'
              '+M/iWnU/S/UDI6yyFbG295WxlRKM'
              'Eb1i2SgxHbN5bJ6'
              '0Xcrd0drbLwaDHXNbHXNHE3HP2p'
              'XNeCJdXhy2ySmjErpjUmnMljcpmbGr'
              'bUUuqbkrVe6K5jk'
              'fsq7+dk9z18bV0mvxZMOnvW9uznlH'
              'nCOPsr45ibR3nTes+hdSaizGOu8PaL'
              'dsbbrRqKj6bUa'
              'vNXJ9NzfPI+d7ibw6TQ4MlNRfgnjcc'
              'pnuXdE+BY6/T3vaJrDLHqVSc3OdP'
              '2uTZzMlSRU+tKl'
              'A4d1LPTbQmO/Db7rkmr9rH7/AMjFd'
              'cq91R19a3FCq6nVtrWi+g9q7KxyVH'
              'uRW/Bd+56/5PMN'
              'L7MtW0RMWvaJ84UdTj2jMxl+BadD'
              's7WvdU5Z+VunXGTvbdi061V27nJS'
              'X1NT7lRdk9kK75Qt'
              'mxh0WLsa8OHHeXEdI4uk/wDvzJN'
              'n5HeX1klaA6daqxHUO5y+SYjLKitw7'
              '5Wr2rPLvsqIiq7v'
              'vyXc5d5N59FqdlVwYZeS3B6qn1V8'
              'F5cjfTaW9cvFPQ5pr+/tstrLLX9sqOt6'
              'tdUpPTw5rERi'
              'OT/e47n0LdvSX02z8WK/tRXn5Pmv'
              'g0V2ptFskzBn4S8ZAIQwIQwIQwIQw'
              'IQwIQwIQwIQwIQw'
              'IQwIQwIQwIQwIQwIQwIQwIQwIQwI'
              'QwIQwIQwIQwIQwIQwIQwIQwIQwIQ'
              'wIQwIQwIQwIQwIQw'
              'IQwIQwIQwIQwIQwIQwIQwIQwIQwI'
              'QwIQwIQwIQwIQwIQwIQwIQwIQwIQ'
              'wIQwT4SNmRCGBCGB'
              'CGBCGBCGBCGBCGBCGBCGBCGB'
              'CGBCGBCGBCGBCGBCGBCGBCGB'
              'CGBCGBCGBCGBCGBCGBCGC00v'
              'R/rLif77b/zWlbtmf4PN9Ff+2SXD7ce'
              '+DX9cqfLWVJf1Kl/HUPJ/Jz/u+fpbfd'
              'U6tpfOfAue'
              'm3Se3fbW+fzbUuGVGJWs7BNlRUV'
              'N2uqb9l39m/tKfe3fS0XtpdN6sxKtf74j'
              '8/qJ9Jov6rfU'
              'UfVTVGpr3JOxF5QdjLCjstOx5NVXtX'
              '6L6jmK5rvsRdkLvczYujxYfSMdozZL'
              'db8+XjEOImPf'
              '1kg1ua8zwzyg5/Ce7ZwCEMCEMEix'
              'uLywuqd3Z1XULmkvKnVYuyopBqM'
              'GPNSaZIi1J6xJtW0x'
              'Lg7hirRvUjSTHZ6w+T3LN0tMpT47K'
              'rV4q5ib8vKKjmqm3w+r4xrc3+H9fMa'
              'XJxUn2scv3qeS'
              '6dJjn4+d1SPSMfrR8Tjup9K3unsvVxt'
              '2rXPZs5lRnhzHfRd9X2H1nY218ev0'
              '8ZsbiJ7p7p74'
              'KjNhnHZSa3obT46yqr+pVf46Z5b5R'
              'v8Ad8fS1+6x1bN+c+BlM++vb6uyVzb'
              'vWlXpX9epSqN7'
              'K1zayqioeo2Zjrk0GKlodZxViY8uGDm'
              'yyskz5mvxvUnqhcY+6uqCsurawajru5'
              'dRZ6UXsm+3'
              'Hf7kPK6vdHYuPLWlnS+WfVrxTz+8'
              '6qazNMPrEGZ1NrbU2paVOhlLhH29'
              'J0jKDGNY3ntty7d1'
              'XZV8notkbuaPZ8zbDVWmE5mZleBz'
              '5tTfJ1LqvqHqRpvB4r/W21lkaEtmzZl'
              'V7KabbIrqjFc3'
              'sqbbOKfFsrZG0NTl/c/vMVlbrETPuiy'
              'nzcE05c2Osc+UlFlNZaoylzYXN7fLVr'
              'YxUfZv4tTi'
              '9qovPsmyu3andS70e72i01MlMdFXL'
              'yt15x4eUc+4gvqL2UzPQXjdU6uub7'
              'L10W9qWdGS7qpx'
              'Y2nRZ8E9KfFdk+sxgjRbLpjwVWOMl'
              'lWOcu0/X9c+UC3HlmbdUU1s+4tbin'
              'cW1R1GvSXlTq01'
              'Vrmqnuip3Qt8uOuSs0vEWrPWJ6EM'
              'SuhrLzUnUnM6cuq9e9rVcLbq2jd1G'
              'pSp/lNkRrlYjHvR'
              'd038/WeXwbH2PpNXWtaVjUWc1j1'
              'p6d8NxH2eR12zZr06+qY6E9azjJCY'
              'q9WyW+S3f8iR8S3P'
              'FY5Nt+PLxvt7EHpWPtOy4o7RPhfN'
              'eK8DbglPuI8JOzUQhgQhgtbfS1/X09'
              'd55kaWVnVZQqIq'
              'rzVz9vopttsnJN+5V5dr4qaumll9pes2j'
              'wUeP1EsYZmk27oKqEtGRCEMCEM'
              'CEMCEMFrb6Wv6'
              '+nrvPMjSys6rKFRFVeauft9FNttk5Jv3'
              'KvLtfFTV00svtL1m0eCjx+oljDM0m3d'
              'BVQloyIQh'
              'gm4nAZPL3jbPG2zrm4d34M9k+Kquy'
              'In1qceu2hh0uPtM1opTz/XP4G9Mc2'
              'lRzJWoNG57T60E'
              'y9r8mW5Ry0fWx+/Dbl3Yrk7ckOXZe'
              '3NLr+L0e3HwJ8pjr06xBvlwWx+13lR'
              'CWzIRCGD2s8fV'
              'u7uja0URa1xUbSpov6T14p+9SHPnr'
              'ipa9vZrEzPujmZrVyiTncBdYXLXGLul'
              'Y64tnI17qaqr'
              'F3RHdlVGr4X4HPs3aFNZgrno+G/j1'
              '8PM3yY5pZT3ECE7mRiEMF7pnR1z'
              'm1uKzqzLLGWTed5k'
              'K35On8E2/OcvwQpNr7cpo+GqnJmy'
              'Sq0jrP5R5k+HBN/KI7yjWgiKqJ3T4l1'
              'EkBNo6eylfFXG'
              'WpW6ux9o5tO4uN02a5+yNTbfdfKe'
              'EOPJtHDTPXBNv3t4mYjxiDeMczXi7'
              'oIMJ2s0EIYEIYEI'
              'YEIYEIYEIYEIYEIYEIYEIYEIYEIYEIY'
              'EIYEIYEIYEIYEIYEIYEIYEIYEIYEIYE'
              'IYEIYEIYEI'
              'YEIYEIYLCAiZkQBgQBgQBgQBgQB'
              'gQBgQBgQBgQBgQBgQBgQBgQBg'
              'QBgQBgQBgQBgQBgQBgQB'
              'gQBgQBgQBgQBgQBgstNUdtRYr++'
              'UP5rSu2xP8Hm+jv8A2yS4fbj3wavrT'
              'T5aupL+p0/43nlf'
              'k7n+Xz9Jb7qnVtH5z4FRpnUupaWT'
              'xlkzJXKWaV6NKCR3DhzROO3w29i'
              '32zsXRWwZcs4qdpwW'
              'lrm1PP3kWHNfiiHKZcdaaXLVtFf1On'
              '/HUKf5Op/l9vpZ+6pLtH5z4FZobp/'
              'W1FWfXuH/ACbF'
              'W/5ev23VfPFu/bx5X2LHebeeuzqxS'
              'scee/SPxn8I7yPTaXtP+k09fPdKcK9'
              'bOzwjcnGuz7hz'
              'GVWqvvs+sqqv3JseaxbJ29rI7TJn7F'
              '/0ua/ZX8eZ0zl09OUVZ6UcP011ox9'
              'HGUlw2X23p09k'
              'Zv8A8CKtNyf7uykeTW7a2NMWzz6'
              'Rp++ev2riife4MxTDm9n1bHOM5p6/'
              'wuSq4+9ZxrU/Cp9F'
              'zV8OavwU+i7N2ni1mGM2KXWfsnw'
              'krsuKaSpN7XyWTxnR/B1cfc1LWst5'
              'UY6pSXiqtV9wu37U'
              'Q8Ji0WHU7xaiuasXr2USp8ViO+bzX'
              'T1XLn+Zzu/ur/IXC3F9cVLmuqbLVq'
              'uV7tk8Jup9C02m'
              'xYKcGKsUr4RCK61ptznmbXovT46u'
              'qr+p1P42HjPlEn+Xx9JX7rHbs75z4Gb'
              'y+Mub7V1/aWrJ'
              'Li4vqzKTOybudVXbup6PQammDZ+P'
              'JeVSuGsz/wBsHNkrNskxHidpxmk2Y'
              '3AVNM020vk9xZVU'
              'ubjkklS6qpxVUb+iieF+w+Mazbk6jVRr'
              'Z4uKmWvDVcq0rz6+M+Hv8S6pg4a'
              'cHl9pzfT+ntN4'
              'rS66j1FZ1L9a1ytrbWbHcUbtvu52yt7'
              '+hx9H2ptPWarXehaO9cfDTjtaefhyjr4'
              'wVuLFStOO'
              '8PmjaXuKwuU1Rpe0fbI/EsxlSpbWtT'
              'uiN4tRiO7qq7N28qeL0+s1Om0OryR'
              'ZZ5zxFrR4uWvf'
              'PkdtqVtekf08JXtwHTCnjbLIMxlavbvv'
              'Pm1rle/u9XKktT1p6fSqp9vj4WE7S2'
              '3bNfDOWtbx'
              'i7XpHRPhry68/s6+MfZYFErvRWfgZi'
              '7F2uqDWVONhQpOsvxj02bVY6tsuy'
              '+vbin0tyy/b+fN'
              'Gz7cnlvbi5R1rMV8OXWeiI/R6x2nlB'
              'es0f0+p3uGx1fHLUvsra9ka96NYlOkt'
              'R1V2zkXk5ex'
              'SW27te2PPmrlWLBk8I5u0ViscukdSf'
              'sMLrC52gybdM4yhofN1ai1JLXLpbSo'
              '520VNzG7xovB'
              'V2qO8oeqna2e+1NPWEr6bjSjrMWnr'
              '1iOUd5ydjWMVvKxc6j0xou00xc3GK'
              'xLr2jD+Iy1CstX'
              'jV7d6reScdvK+nb7Cm2RtfaWXXVpn'
              'zRjtxc8Vq8Lj/llc/Lm/eTZsOOKOsPzJ'
              'ltlNHp04dcL'
              'hHfNbbpGPsJXKq1tm+uTdFOXLoNo'
              '/tng7eO37Nxfhj2efJG8ZMfYvh9V9Ct'
              's8D06x+EwWRy9'
              'lUWpk1ci/jHrTTd30qmyp2pp2Tb7yx'
              'z7R2xqNTqMOnvCwruh9OkcutvP4Ijr'
              'jw1rWbR1I7NO'
              'aGxtne6lvKNS/wATWvHW2Is6blRN'
              'k39Tnbtdt6Xbb+ye+50TtTampyY9Fj'
              'mMWeuKLZbTHu5L'
              'nHfD857ka9lirE3nnV8idkOnel6WrL5'
              '60alPCY2wS+ubWm9VV7t3+lqr6kar'
              'WL7nBpd6tbbQ'
              'Y4cTqc2bs62mOkerznubnw+BvbSU'
              '7Sf8sQz7yCYbJ9N30tPWS46he5KlSf'
              'QevL8aqsbvuqu7'
              'bI0xpY1Om2zE6y/a2x4LS45erznwjz'
              'M24bYfUhOx909FaFq5C60hTtavzvb'
              '2srss5yp+NVrV'
              '+jvtts9O22339yO+8O1K4abRm1fR75'
              'F2a7nPen3Tzb+HIzGnxPs/6ojqQcfp/'
              'p7jcBp/IZq0'
              'qVa+S3a9/N8fde76iIqbIxPGx3araW1'
              '9Rq9Th014iuHyh+6OXW3n8ER1xYa'
              '0rNu88Wac0Njb'
              'O91LeUal/ia1462xFnTcqJsm/qc7drt'
              'vS7bf2T33Jp2ptTU5MeixzGLPXFFst'
              'pj3clzjvh+c'
              '9yMdlirE3nnV8ik6macxWG1Ilri6S0bZ'
              '9BlVafJXpycrkXjy3Xbt8S63N2pn1mi4'
              '888V4vMN'
              'LlER1RBrcVaXUHxoPS+Nylxf3uVR7'
              'sZird1zcUafZ1TbdUb7L4avg33o2vm'
              '0tMePAozZ78NZ'
              'npHn9sDS4Ys5t0rBrsh8zZLpu+lp6yX'
              'H0L3I0qT7d68vxqqxN91V3bZGnktL'
              'Gp022YnWX7W2'
              'PBaXHL1ec+EeZ124bYfUhOxOvNA'
              '6DxGNq0craLTo0qXfLvrbVKtbbxSpI'
              '7v9nHb7Th0+821N'
              'Xmi2C7ta3zUV5Vr/AM1l+L93Q3tpcVI'
              '9b6zjUB9jZTHQ69WtpPp9jkxm9LJag'
              '3q3N8zs9tNu'
              'yoxrvzez0/efPsWKu1dr5e254dLyrSe'
              'kzPfMd/Sf/EsZnssMLrbvPLT2LyWo'
              'XfKNXVbmrh8H'
              'ZuuqVOoitfUpr39LvS5yLH9Lffsibku1t'
              'Xh0HqbPjHGo1OWKTMc4ifOOcRM'
              'Pou+ZRripOTnk'
              'fDWGWf4KaQyi6czWOsXWtlfXvyW'
              '7sHucqO25qi+V/wBl32UrP21tDS+la'
              'bNki+TFi463iI5e'
              'z5f8xL2OO3DaIUTKR753SWhqmO1'
              'Lb4yzdSvsK1td11zcqc6nJ8bU324tR'
              'nHwQbM23tSubS3z'
              '3icWonh4VHSFHFPLrLfX7ORtlwYlaIj'
              'nU/cTgtG6byuBxl9Y1bzO3kNz8t5Kj'
              'KVRX+jZEc1N'
              'ke34f/RjXbR2jtHT6jPiyVx6bHxU4Fzt'
              'C5909Yn8l1FMePHatZh2k8chgMLVz'
              '2qNTZ+m+6sc'
              'dXbSp2jF4yPVGNTdUVF2Tk0m0u0t'
              'TXS6TRaSYplzUmZtPdHOfwn8DFsV'
              'eK9784iSRZaJ0pW1'
              'Pg722s9sXlrOtcfN9VVcjXsa3zuq/wC'
              '08b+UOfU7w6+mh1GK9/3+DLWvH'
              'HJxMz/9fqk2rp8c'
              '3rMRytHQ+24DphTxtlkGYytXt33nza1'
              'yvf3erlSWp609PpVU+3x8NJ2ltu2a+'
              'Gcta3jF2vSO'
              'ifDXl15/Z18XZYFErvRWN0Zi7a61tY'
              'OZUdbY+1+UWNKR6Na5aTqrFVEX'
              '1q3wnLcsp2/nyU0G'
              'WFx5cnDeVDmOKKz3cn5Ij9HrE5I7o'
              'gvWaP6fU73DY6vjlqX2VteyNe9GsS'
              'nSWo6q7ZyLycvY'
              'pLbd2vbHnzVyrFgyeEc3aKxWOXSO'
              'pP2GF1hc7QZa20jaO0bmGUuS3rMy'
              '2yoPV7kbs19NjeTE'
              'Xgv5Re6oeny7cyRtLBNl2c6Wckwof'
              'S0yp6/0x3nLGCOzt48SLynorQtXIXW'
              'kKdrV+d7e1ldl'
              'nOVPxqtav0d9ttnp222+/uUl94dqVw'
              '02jNq+j3yLs13Oe9Punm38ORPGnxP'
              's/wCqI6kbRml9'
              'GXOnKF383rmsj3+cKDa3GtSTf82jyYi'
              'pt96/uOneLa+0sWstj7T0fD/RPC629'
              '9lK+6PtNNPh'
              'xzRrinvOaXtvTS9rpSpOo0pHx0X/AE'
              'mN5dmu+tE7KfStNaezrxTFrKHMdJ5'
              'dY95W26nhATM1'
              'EAYEAYEAYEAYEAYEAYEAYEAYE'
              'AYEAYEAYEAYEAYEAYEAYEAYEA'
              'YEAYEAYEAYEAYEAYEAYEAY'
              'EAYEAYJ8JGzIhDAhDAhDAhDAhDA'
              'hDAhDAhDAhDAhDAhDAhDAhDAh'
              'DAhDAhDAhDAhDAhDAhDA'
              'hDAhDAhDAhDAhDBYaepbZ/GL8L'
              'qh/MQr9rz/AAeb6O/9skmH2495vup'
              'ej9QZfULLrH2a16CW'
              '7KavRzE9SOcqp6lRfc+e7l7waPSaO'
              'cea/DbjmUp6KPCCx1unve7iO4z+H6'
              'eatt8vY16uOc2l'
              'SuKT3u50+zWvRVX6R6DaO9uzsmn'
              'yVrlibWpaI5W749xzY9Jki0cu8ldYWc'
              'tU0v7pT/jec3ye'
              'f7BP0s/dU32j858C01pUdpzRmM07a'
              '/i6t0ze8c3yu2y1P8b3fsQqt2scbS2nm'
              '1t+daT6n/x+'
              'qI+uWS6mezxRSO85nCfUGVZ6W7q'
              '9tXp3FB6061JyPp1G9lRU8KhHlx1yV'
              'ml4dbQpgzEzHM6N'
              'renT1FonHalRiNu6G1O52+Cu4O/9R'
              'E2+0+bbszOztq5dC/3dudfqcf8Aj19x'
              'Z6r95ii/efK4'
              'LJ5fpThrXHUFr1mXVSo5iK1PSj66b+'
              'pU93IZ/aeDR7wai+a3BWccR39Vj8'
              'PcY7K19PWI8fzM'
              '1/o11l/Zrv8AzKX+Y9P/AIy2Z/qx9Vv'
              'yOb0LL4Gr6aaP1BiNQvushZrQoLbv'
              'po9XMX1K5qon'
              'pVV9jyW+m8Gj1ejjHhvxW44lKeinxg6'
              '9Fp70u5juMn871cLrS+ydCiyvVp3Nz'
              'G2pvxRXuc3f'
              'tt43PX/s2ut2ZjwWma1nHja8oiUcfac'
              'GWbeckLGZ3JWGfbnGulvEqOqVOf'
              'h/Pfmi/aindrdk'
              'Yc+knS+zjURC7l0NKZrVvxd5d4zqD'
              'd2zb2heY+3v7C8rvufkdRPQx9R3JU'
              'buju2/sqFHrd0K'
              'ZOztjy3xZcdIrxx1mIhc+nP3E1NZMN'
              'xExJ91+o+Qfm7PK07GjTdZ0H27KO'
              '7uGz/2bbduxpi3'
              'LxRpb6ecl5jJeLPk+RmdbPFFl0graW'
              'qalPTSYf5PyqNvUvW3Ku8bd+KM2+'
              'P1llfYEW13pPFy'
              '7Ls+Ffa3+BHGo9Th82Tcnr68u62ZfS'
              's6dFmZo0qFVquV6tSkit3RfTuqo9fY4'
              'tFujjxVwRa8'
              '2nT2taOSfFMT5+Hib31czxcvaPl+uq'
              '78/iMutmiOxdBLeJH/AJT0ua5eW3p'
              '35/BTau6tY0mf'
              'Tcfz9+JrpziY5Pn08h6X69bL2YI/4Y3'
              'qWd5aNtaMN5kPnJ7XorkR3Jr49uyK'
              'z0IdH+GsfaY8'
              'k3vxY8HZcuXJTHF7+cmvpMqYUc7'
              'Mn5HqHVr426s7HE2uOffsjvbii1OT2q'
              'my+ET2VU77+Tg0'
              'e50Y89MmXNkzRil0rbpE/XPl0XQ3vr'
              'HWYiIhlImeuG6Wdp9KTYXXHyl1bde'
              'Xjbjt9xeTsek6'
              '/wBMc8UU4V3e8g7aez4PM/ctn6+R'
              'wuLxb6LadPGNc1tRFVVfy+Pw8GNB'
              'seun1ObPFpmc0xy8'
              'EZyZuKsV8Cw07rWtisU7FXOPoZOx'
              '5y0qVwm/By/ajkVN+/grtr7sV1WojU'
              'UyXw5UpmvfH2fr'
              'uJMOq4K8MxEwTH9TcsuZpZNtpRa'
              'qW/yW6oJvwqsRyuTz9HZXdjjruRp/'
              'Rpwcd59fjrPJ1lRH'
              'xa5/gb+nW4uJdx45vXlS/wAV822eM'
              'oYyi2syuxbfts5nffZEam6u77k2zd04w'
              'ajt8mW+a3DN'
              'fW8J+Mz07jXLq+KqiIg973qZkriyqNZ'
              'Y29DJ16UFxk2J+NdT+CfD9pBptyM'
              'OPLEzkvbDW3FX'
              'HPsxP68vebW11pjpD8Siy2fr5HC4vF'
              'votp08Y1zW1EVVV/L4/DwX2g2PX'
              'T6nNni0zOaY5eCI'
              'MmbirFfAsNO61rYrFOxVzj6GTsectKlc'
              'Jvwcv2o5FTfv4K7a+7FdVqI1FMl8O'
              'VKZr3x9n67i'
              'TDquCvDMRMELVmo6+o7u3uq9tTt'
              '6tGilF0W+ztnK738efB3bB2JXZ2O2O'
              'trWra3Fz7uS/A0z'
              '5+0lnxpbUd/p2+dc2zW1adVsdxb1P'
              'ovb/wCyobbc2Ji2ji4Lus1l1tHWJMYM'
              '845cFtmte1L/'
              'ABSY6zxlDGUW1mXDFt+2zmd99kR'
              'qb8u+5U7N3SjBn7bLlvmtNJrPF4T8Z'
              'np3EuTV8VVERBLq'
              '9ULyoxtd2Js1y7G8GZJWbvb9aIqbp/'
              'i2OSm4uOs8MZsvo8y5xvlP69z8zedd'
              'PhHF4mGdSVyq'
              'q+V7qe7jkcBqsHr7I4zFsxta0t8hbUV5'
              'WyXDeS0189vs37Hktp7pYdTnnPW'
              '98V7e1wz1/X6g'
              '7MWrmteFRMH7bdRNQszb8nccLlt'
              'WnBUs3JtRi334tTvt9vcZtzdHOljBR0'
              'ms8UX/AKuLxn8u'
              'XkhGsvxcU8z9ute3dbKYy4pWVG1s'
              'MVUkt8dR9DN1+l3RPK+3bsYwbpY'
              '6YMtLZLXy54Vsluc+'
              'X6fPxFtXM2iUor3HjQ1jWpv1E5bRrv'
              'wga5rm8+1Lly7+PVslRfgT5N2q2jSxx'
              'zHokx3e108+'
              'XTzNY1PtcvbLGx6m3dG1tWXuMtr+'
              '7skRtte1fyjUT7l77e6KhW6rcil8l5xZr4'
              'seT2qR0n7e'
              'nlLJa66VDiJmO8i2Ov723yWTuKtnR'
              'urLKv53FhV7s38Jsq7+3ndO51andH'
              'FkwYqVyWplwQq3'
              'jr+vDnyNK6uYtMpxbuPz/SDlfwgo5dt'
              'vRa22out7WyTdKVOmqeE22M/4Qw'
              'ehzpuKzvbitf8A'
              'qmR6Zbj4vDuIdLVNSnppMP8AJ+VR'
              't6l625V3jbvxRm3x+s7L7Ai2u9J4uXZ'
              'dnwr7W/wNI1Hq'
              'cPmybkNfXlzeZi4pWdOi3MWzbWqx'
              'XK9Wta1WckX07rs5fY4tJujjx48FLXt'
              'b0fJN45J82u/v'
              'jxN76uZm0r2oR8v11Xfn8Rl1s0R2Lo'
              'JbxI/8p6XNcvLb078/gptXdWsaTPp'
              'uP5+/E105xMcn'
              'z6eQ9L9etl7MEWtq6/djshY0qLKNO/'
              'v1yKvbvypvVyO4t9tkViHXj3cxRmx5b'
              'TNpxYey8phT'
              'Dn65NJ1MqY8ZZaXvUzJXFlUayxt6'
              'GTr0oLjJsT8a6n8E+H7Sq025GHHliZ'
              'yXthrbirjn2Yn9'
              'eXvJba60x0h+J54jqD822NtSZhbJ99'
              'aU47e+47PRNtt12TluqednJub6/dD0'
              'nLa06jLGLJLt'
              'R8vyXhylGMes4Y9mHHeZK7fWu7qt'
              'dV15V7h7qtV3jdz15Kv7VPX4MVcW'
              'OuOvKtYiI90coOO0'
              'uWeUJKzAhDAhDAhDAhDAhDAhD'
              'AhDAhDAhDAhDAhDAhDAhDAhDA'
              'hDAhDAhDAhDAhDAhDAhDAh'
              'DAhDAhDAhDAhDBYQkbMiEMCEM'
              'CEMCEMCEMCEMCEMCEMCEMCE'
              'MCEMCEMCEMCEMCEMCEMCEMC'
              'E'
              'MCEMCEMCEMCEMCEMCEMCEMH'
              '6xj2Pa9i8XtXdqp7KhraItCnpJkv2651'
              'g1qNTIv2T4spr/wA2'
              'nnZ3R2ZP/Cj67fmdHpeXxP38O9Y/2'
              'i7/AAUv8pj/AAhsz/Sj67fmZ9My+JT5'
              'C7vsjcrdXtVa'
              '9d3ZXu+CfYXej0eLTU7PFWK08CC9'
              '5tLk9ctlMnl67K+QrT1KbeDF2a3Zu+/'
              'hqInuRbP2Zg0d'
              'Zrhrw1mX3zz+JnJltfqQYTvZGIQwTm'
              '5XKNxL8S2t/q+o7m6hs36W6L5238'
              'p8SvtszTzqI1PD'
              '++iE+f8A+d5J2tuHh7iTjdT6jxlslrZXr6'
              'Vu3dW09muRN13Xbki7dzm1u7+h1V'
              '+0y44tfx5x'
              '90m9NResKJ5Er8O9Y/2i7/BS/wApx'
              '/4Q2Z/pR9dvzN/TMvifjtdawciouRfs'
              'vwZTT/4mY3R2'
              'ZH/Cj67fmY9Ly+JQPY973PevJ7l3cq'
              '+6qeirEVhR0g5z8hNmYEIYEIYEIYEI'
              'YEIYEIYEIYEI'
              'YEIYEIYEIYEIYEIYEIYEIYEIYEIYEIY'
              'EIYEIYEIYEIYEIYEIYEIYEIYEIYEIYE'
              'IYEIYEIYEI'
              'YEIYEIYEIYEIYEIYEIYEIYEIYEIYEIY'
              'EIYEIYEIYEIYEIYEIYEIYEIYEIYEIYE'
              'IYEIYEIYEI'
              'YEIYEIYEIYEIYEIYJ8BGzIgDAgDAg'
              'DAgDAgDAgDAgDAgDAgDAgDAgD'
              'AgDAgDAgDAgDAgDAgD'
              'AgDAgDAgDAgDAgDAgDAgDAgDA'
              'gDAgDAgDAgDAgDAgDAgDAgDAg'
              'DAgDAgDAgDAgDAgDAgDAgD'
              'AgDAgDAgDAgDAgDAgDAgDAgDA'
              'gDAgDAgDAgDAgDAgDAgDAgDAg'
              'DAgDAgDAgDAgDAgDAgDAgD'
              'AgDAgDAgDAgDAgDAgDAgDAgDA'
              'gDAgDAgDAgDAgDAgDAgDAgDAg'
              'DAgDAgDAgDAgDAgDAgDAgD'
              'AgDAgDAgDAgDAgDAgDAgDAgDA'
              'gDAgDBPgImZEAYEAYEAYEAYEAY'
              'EAYEAYEAYEAYEAYEAYEAY'
              'EAYEAYEAYEAYEAYEAYEAYEAYE'
              'AYEAYEAYEAYEAYEAYEAYEAYEA'
              'YEAYEAYEAYEAYEAYEAYEAY'
              'EAYEAYEAYEAYEAYEAYEAYEAYE'
              'AYEAYEAYEAYEAYEAYEAYEAYEA'
              'YEAYEAYEAYEAYEAYEAYEAY'
              'EAYEAYEAYEAYEAYEAYEAYEAYE'
              'AYEAYEAYEAYEAYEAYEAYEAYEA'
              'YEAYEAYEAYEAYEAYEAYEAY'
              'EAYEAYEAYEAYEAYEAYEAYEAYE'
              'AYEAYEAYEAYEAYEAYEAYJ8JGzIh'
              'DAhDAhDAhDAhDAhDAhDA'
              'hDAhDAhDAhDAhDAhDAhDAhDAh'
              'DAhDAhDAhDAhDAhDAhDAhDAhD'
              'AhDAhDAhDAhDAhDAhDAhDA'
              'hDAhDAhDAhDAhDAhDAhDAhDAh'
              'DAhDAhDAhDAhDAhDAhDAhDAhD'
              'AhDAhDAhDAhDAhDAhDAhDA'
              'hDAhDAhDAhDAhDAhDAhDAhDAh'
              'DAhDAhDAhDAhDAhDAhDAhDAhD'
              'AhDAhDAhDAhDAhDAhDAhDA'
              'hDAhDAhDAhDAhDAhDAhDAhDAh'
              'DAhDAhDAhDAhDAhDAhDAhDAhD'
              'AhDAhDAhDBYQkbMiEMCEMC'
              'EMCEMCEMCEMCEMCEMCEMCEM'
              'CEMCEMCEMCEMCEMCEMCEMCE'
              'MCEMCEMCEMCEMCEMCEMCEMC'
              'EMC'
              'EMCEMCEMCEMCEMCEMCEMCEM'
              'CEMCEMCEMCEMCEMCEMCEMCE'
              'MCEMCEMCEMCEMCEMCEMCEMC'
              'EMC'
              'EMCEMCEMCEMCEMCEMCEMCEM'
              'CEMCEMCEMCEMCEMCEMCEMCE'
              'MCEMCEMCEMCEMCEMCEMCEMC'
              'EMC'
              'EMCEMCEMCEMCEMCEMCEMCEM'
              'CEMCEMCEMCEMCEMCEMCEMCE'
              'MCEMCEMCEMCEMCEMCEMCEMC'
              'EMC'
              'EME+AjZkQBgQBgQBgQBgQBgQBg'
              'QBgQBgQBgQBgQBgQBgQBgQBgQ'
              'BgQBgQBgQBgQBgQBgQBgQ'
              'BgQBgQBgQBgQBgQBgQBgQBgQB'
              'gQBgQBgQBgQBgQBgQBgQBgQBg'
              'QBgQBgQBgQBgQBgQBgQBgQ'
              'BgQBgQBgQBgQBgQBgQBgQBgQB'
              'gQBgQBgQBgQBgQBgQBgQBgQBg'
              'QBgQBgQBgQBgQBgQBgQBgQ'
              'BgQBgQBgQBgQBgQBgQBgQBgQB'
              'gQBgQBgQBgQBgQBgQBgQBgQBg'
              'QBgQBgQBgQBgQBgQBgQBgQ'
              'BgQBgQBgQBgQBgQBgsISJmRCGB'
              'CGBCGBCGBCGBCGBCGBCGBCGB'
              'CGBCGBCGBCGBCGBCGBCGBC'
              'GBCGBCGBCGBCGBCGBCGBCGBC'
              'GBCGBCGBCGBCGBCGBCGBCGBC'
              'GBCGBCGBCGBCGBCGBCGBCGBC'
              'GBCGBCGBCGBCGBCGBCGBCGBC'
              'GBCGBCGBCGBCGBCGBCGBCGBC'
              'GBCGBCGBCGBCGBCGBCGBCGBC'
              'GBCGBCGBCGBCGBCGBCGBCGBC'
              'GBCGBCGBCGBCGBCGBCGBCGBC'
              'GBCGBCGBCGBCGBCGBCGBCGBC'
              'GBCGBCGBCGBCGBCGBCGBCGBC'
              'GBCGBCGCfARsyIAwIAwIAwIAwIAw'
              'IAwIAwIAwIAwIAwIAwIA'
              'wIAwIAwIAwIAwIAwIAwIAwIAwIAwI'
              'AwIAwIAwIAwIAwIAwIAwIAwIAwIAw'
              'IAwIAwIAwIAwIA'
              'wIAwIAwIAwIAwIAwIAwIAwIAwIAwI'
              'AwIAwIAwIAwIAwIAwIAwIAwIAwIAw'
              'IAwIAwIAwIAwIA'
              'wIAwIAwIAwIAwIAwIAwIAwIAwIAwI'
              'AwIAwIAwIAwIAwIAwIAwIAwIAwIAw'
              'IAwIAwIAwIAwIA'
              'wIAwIAwIAwIAwIAwIAwIAwIAwIAwI'
              'AwIAwIAwIAwIAwIAwT4CNmRAGBA'
              'GBAGBAGBAGBAGBAG'
              'BAGBAGBAGBAGBAGBAGBAGBAG'
              'BAGBAGBAGBAGBAGBAGBAGBAG'
              'BAGBAGBAGBAGBAGBAGBAGBAG'
              'BAGBAGBAGBAGBAGBAGBAGBAG'
              'BAGBAGBAGBAGBAGBAGBAGBAG'
              'BAGBAGBAGBAGBAGBAGBAGBAG'
              'BAGBAGBAGBAGBAGBAGBAGBAG'
              'BAGBAGBAGBAGBAGBAGBAGBAG'
              'BAGBAGBAGBAGBAGBAGBAGBAG'
              'BAGBAGBAGBAGBAGBAGBAGBAG'
              'BAGBAGBAGBAGBAGBAGBAGBAG'
              'BAGBAGBAGBAGCwgImZEAYEAY'
              'EAYEAYEAYEAYEAYEAYEAYEAYE'
              'AYEAYEAYEAYEAYEAYEAYEAYEA'
              'YEAYEAYEAYEAYEAYEAYEAY'
              'EAYEAYEAYEAYEAYEAYEAYEAYE'
              'AYEAYEAYEAYEAYEAYEAYEAYEA'
              'YEAYEAYEAYEAYEAYEAYEAY'
              'EAYEAYEAYEAYEAYEAYEAYEAYE'
              'AYEAYEAYEAYEAYEAYEAYEAYEA'
              'YEAYEAYEAYEAYEAYEAYEAY'
              'EAYEAYEAYEAYEAYEAYEAYEAYE'
              'AYEAYEAYEAYEAYEAYEAYEAYEA'
              'YEAYEAYEAYEAYEAYEAYEAY'
              'EAYJ8BGzIgDAgDAgDAgDAgDAgD'
              'AgDAgDAgDAgDAgDAgDAgDAgDA'
              'gDAgDAgDAgDAgDAgDAgDA'
              'gDAgDAgDAgDAgDAgDAgDAgDAg'
              'DAgDAgDAgDAgDAgDAgDAgDAgD'
              'AgDAgDAgDAgDAgDAgDAgDA'
              'gDAgDAgDAgDAgDAgDAgDAgDAg'
              'DAgDAgDAgDAgDAgDAgDAgDAgD'
              'AgDAgDAgDAgDAgDAgDAgDA'
              'gDAgDAgDAgDAgDAgDAgDAgDAg'
              'DAgDAgDAgDAgDAgDAgDAgDAgD'
              'AgDAgDAgDAgDAgDAgDAgDA'
              'gDAgDAgDAgDAgDAgDBPjI2ZEYYE'
              'YYEYYEYYEYYEYYEYYEYYEYYEY'
              'YEYYEYYEYYEYYEYYEYYE'
              'YYEYYEYYEYYEYYEYYEYYEYYEY'
              'YEYYEYYEYYEYYEYYEYYEYYEYY'
              'EYYEYYEYYEYYEYYEYYEYYE'
              'YYEYYEYYEYYEYYEYYEYYEYYEY'
              'YEYYEYYEYYEYYEYYEYYEYYEYY'
              'EYYEYYEYYEYYEYYEYYEYYE'
              'YYEYYEYYEYYEYYEYYEYYEYYEY'
              'YEYYEYYEYYEYYEYYEYYEYYEYY'
              'EYYEYYEYYEYYEYYEYYEYYE'
              'YYEYYEYYEYYEYYEYYEYYEYYEY'
              'YEYYEYYP//Z'
              ''
              '')
            TabOrder = 4
            Visible = False
            WantReturns = False
            WordWrap = False
          end
          object Memo5: TMemo
            Left = 424
            Top = 141
            Width = 118
            Height = 33
            Lines.Strings = (
              '/9j/4AAQSkZJRgABAQEASABIAAD/'
              '2wBDAAYEBAQFBAYFBQYJBgUGC'
              'QsIBgYICwwKCgsKCgwQ'
              'DAwMDAwMEAwODxAPDgwTExQU'
              'ExMcGxsbHCAgICAgICAgICD/2wBD'
              'AQcHBw0MDRgQEBgaFREV'
              'GiAgICAgICAgICAgICAgICAgICAgICA'
              'gICAgICAgICAgICAgICAgICAgICAgIC'
              'AgICD/wAAR'
              'CAA6AtADAREAAhEBAxEB/8QAGA'
              'ABAQEBAQAAAAAAAAAAAAAAAAE'
              'CAwj/xAAaEAEBAQEBAQEA'
              'AAAAAAAAAAAAEQJRAQNB/8QAF'
              'AEBAAAAAAAAAAAAAAAAAAAAAP'
              '/EABQRAQAAAAAAAAAAAAAA'
              'AAAAAAD/2gAMAwEAAhEDEQA/A'
              'PSQAAAAAAAAANeeUHXHy8/Qdv'
              'M54CzPAJngEzwCZ4BM8Ame'
              'ATPAJngEzwCZ4BM8AmeATPAJng'
              'EzwCZ4BM8AmeATPAJngEzwCZ4B'
              'M8AmeATPAJngEzwCZ4BM'
              '8AmeATPAJngEzwCZ4BM8AmeATP'
              'AJngEzwCZ4BM8AmeATPAJngEzwC'
              'Z4BM8AmeATPAJngEzwC'
              'Z4BM8AmeATPAJngEzwCZ4BM8Am'
              'eATPAT3GPfwHHfx4Dl7n3wEBAAA'
              'AAAAAAAAAAAAAAAAAWA'
              '15kHXPgNgtAoLQKBQKBQKBQKBQ'
              'KBQKBQKBQKBQKBQKBQKBQKBQ'
              'KBQKBQKBQKBQKBQKBQKBQK'
              'BQKBQKBQKBQKBQKBQKBQKBQK'
              'CUFoFAoJQPQZ14DlrAMe5BAQAAA'
              'AAAAAAAAAAAAFBYDXngN+'
              'A0C0CgUCgUCgUCgUCgUCgUCgU'
              'CgUCgUCgUCgUCgUCgUCgUCgUC'
              'gUCgUCgUCgUCgUCgUCgUCg'
              'UCgUCgUCgUCgUCgUCgUCgUCgU'
              'CgUCggM++Ax74DMBAQAAAAAAA'
              'AAAAAFBQa8BfAa8AoLQQFo'
              'FAoFAoFAoFAoIC0CgUCgAUCgUAA'
              'AACgAUCgUCgUCgUCgAAUCgUAC'
              'gUCgUCgUCgAUAEBQAKBQ'
              'KBQKBQAAAKBQQAFoJQT0E98Bm'
              'AzAAQAAAAAAAFAABQUFBQAKBQ'
              'KBQKBQKBQKBQKBQKBQKBQK'
              'BQKBQKBQKBQKBQKBQKBQKBQK'
              'BQKBQKBQKBQKBQKBQKBQKBQK'
              'BQKBQKBQKBQKBQKBQKBQKBQK'
              'BQKBQKAACAgICAAAgAAAKAAACg'
              'AoFAoFAoFAoFAoFAoFAoFAoFAoFA'
              'oFAoFAoFAoFAoFAoFA'
              'oFAoFAoFAoFAoFAoFAoFAoFAoFAo'
              'FAoFAoFAoFAoFAoFAoFAoFAoFAoF'
              'AoFAoFAoFABAAAQA'
              'AEAAABQAAAAAKAAAAAAAAAAAA'
              'AAAAAAAAAAAAAAAAAAAAAAAAA'
              'AAAAAAAAAAAAAAAAAAAAAA'
              'AAAAAAAAAAAAAAAAAAAACAAAA'
              'AAAAAAAAAAAAAAAAAAAAAAAAA'
              'AAAAAAAAAAAAAAAAAAAAAA'
              'AAAAAAAAAAAAAAAAAAAAAAAAA'
              'AAAAAAAAAAAAAAAA//Z'
              ''
              '')
            TabOrder = 5
            Visible = False
            WantReturns = False
            WordWrap = False
          end
          object Memo11: TMemo
            Left = 424
            Top = 24
            Width = 118
            Height = 32
            Lines.Strings = (
              
                '<!DOCTYPE html PUBLIC '#39'-//W3C//DTD XHTML 1.0 Transitional//EN'#39' '#39 +
                'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'#39'>'
              '<html xmlns='#39'http://www.w3.org/1999/xhtml'#39'>'
              '<head>'
              '<title>[Empresa]</title>'
              
                '<meta http-equiv='#39'Content-Type'#39' content='#39'text/html; charset=iso-' +
                '8859-1'#39'>'
              '<body style='#39'background-color:#FFFFFF; margin:0; padding:0;'#39'>'
              '  <table cellpadding='#39'0'#39' cellspacing='#39'0'#39' border='#39'0'#39' width='#39'500'#39'>'
              '    <tr>'
              
                '      <td height='#39'50'#39' align='#39'center'#39' style='#39'font-family:Verdana,' +
                ' Arial, Helvetica, sans-serif; font-weight:bold; font-size:18px;' +
                ' color:#003399;'#39'>'
              '        <p>[Empresa]</p>'
              '      </td>'
              '    </tr>'
              '    <tr>'
              
                '      <td height='#39'25'#39' align='#39'center'#39' style='#39'background-color:#F3' +
                'F3F3; border-bottom:1px solid #CCCCCC;'#39'>'
              
                '        <p style='#39'font-family: Arial, Helvetica, sans-serif; fon' +
                't-weight:bold; font-size:14px; color:#000000;'#39'>[Saudacao] [Clien' +
                'te]</p>'
              '      </td>'
              '    </tr>'
              '    <tr>'
              
                '      <td style='#39'line-height:15px; padding:10px; font-family:Ver' +
                'dana, Arial, Helvetica, sans-serif; font-size:12px; font-weight:' +
                'normal; color:#000000; border-left:1px solid #CCCCCC; border-rig' +
                'ht:1px solid #CCCCCC;'#39'>'
              
                '        Segue em anexo o XML com protocolo de autoriza'#231#227'o e o DA' +
                'NFE da NFe:<br><br> '
              '        <strong>S'#233'rie:</strong> [SerieNFe].<br>'
              '        <strong>N'#250'mero:</strong> [NumeroNFe].<br>'
              '        <strong>Chave:</strong> [ChaveNFe].'
              '      </td>'
              '    </tr>'
              '    <tr>'
              
                '      <td style='#39'margin: 0; padding: 0; list-style: none; paddin' +
                'g:10px; border-bottom:1px solid #CCCCCC; border-left:1px solid #' +
                'CCCCCC; border-right:1px solid #CCCCCC;'#39'>&nbsp;</td>'
              '    </tr>'
              '  </table>'
              '</body>'
              '</html>')
            TabOrder = 6
            Visible = False
            WantReturns = False
            WordWrap = False
          end
          object Memo12: TMemo
            Left = 424
            Top = 62
            Width = 118
            Height = 33
            Lines.Strings = (
              
                '<!DOCTYPE html PUBLIC '#39'-//W3C//DTD XHTML 1.0 Transitional//EN'#39' '#39 +
                'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'#39'>'
              '<html xmlns='#39'http://www.w3.org/1999/xhtml'#39'>'
              '<head>'
              '<title>[Empresa]</title>'
              
                '<meta http-equiv='#39'Content-Type'#39' content='#39'text/html; charset=iso-' +
                '8859-1'#39'>'
              '<body style='#39'background-color:#FFFFFF; margin:0; padding:0;'#39'>'
              '  <table cellpadding='#39'0'#39' cellspacing='#39'0'#39' border='#39'0'#39' width='#39'500'#39'>'
              '    <tr>'
              
                '      <td height='#39'50'#39' style='#39'font-family:Verdana, Arial, Helveti' +
                'ca, sans-serif; font-weight:bold; font-size:18px; color:#003399;' +
                ' '#39' align='#39'center'#39'>'
              '        <p>[Empresa]</p>'
              '      </td>'
              '    </tr>'
              '    <tr>'
              
                '      <td height='#39'25'#39' align='#39'center'#39' style='#39'background-color:#FF' +
                '4500; border-bottom:1px solid #FF0000;'#39'>'
              
                '        <p style='#39'font-family: Arial, Helvetica, sans-serif; fon' +
                't-weight:bold; font-size:14px; color:#FFFFFF;'#39'>[Saudacao] [Clien' +
                'te]</p>'
              '      </td>'
              '    </tr>'
              '    <tr>'
              
                '      <td style='#39'line-height:15px; padding:10px; font-family:Ver' +
                'dana, Arial, Helvetica, sans-serif; font-size:12px; font-weight:' +
                'normal; color:#FF0000; border-left:1px solid #FF0000; border-rig' +
                'ht:1px solid #FF0000;'#39'>'
              
                '        Segue em anexo o XML com protocolo de CANCELAMENTO da NF' +
                'e:<br><br>'
              '        <strong>S'#233'rie:</strong> [SerieNFe].<br>'
              '        <strong>N'#250'mero:</strong> [NumeroNFe].<br>'
              '        <strong>Chave:</strong> [ChaveNFe].'
              '      </td>'
              '    </tr>'
              '    <tr>'
              
                '      <td style='#39'margin: 0; padding: 0; list-style: none; paddin' +
                'g:10px; border-bottom:1px solid #FF0000; border-left:1px solid #' +
                'FF0000; border-right:1px solid #FF0000;'#39'>&nbsp;</td>'
              '    </tr>'
              '  </table>'
              '</body>'
              '</html>')
            TabOrder = 7
            Visible = False
            WantReturns = False
            WordWrap = False
          end
          object Memo13: TMemo
            Left = 424
            Top = 102
            Width = 118
            Height = 33
            Lines.Strings = (
              
                '<!DOCTYPE HTML PUBLIC '#39'-//W3C//DTD HTML 4.01 Transitional//EN'#39' '#39 +
                'http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd'#39'>'
              '<html xmlns='#39'http://www.w3.org/1999/xhtml'#39'>'
              '  <head>'
              '    <title>[Empresa]</title>'
              
                '    <meta content='#39'text/html; charset=iso-8859-1'#39' http-equiv='#39'Co' +
                'ntent-Type'#39' />'
              '    <style type='#39'text/css'#39'>'
              'body {'
              '  background-color:#FFFFFF;'
              '  margin:0;'
              '  padding:0;'
              '}'
              'h1 {'
              '  margin:0;'
              '  padding:0;'
              '  border:0;'
              '  font-size:16px;'
              '  font-family:Verdana, Arial, Helvetica, sans-serif;'
              '  font-weight:bold;'
              '  color:#997744;'
              '  padding-left:15px;'
              '  padding-top:5px;'
              '}'
              'h2 {'
              '  margin:0;'
              '  padding:0;'
              '  border:0;'
              '  font-size:14px;'
              '  font-family:Verdana, Arial, Helvetica, sans-serif;'
              '  font-weight:bold;'
              '  color:#997744;'
              '  padding-left:15px;'
              '  padding-top:5px;'
              '}'
              'h3 {'
              '  margin:0;'
              '  padding:0;'
              '  border:0;'
              '  font-size:10px;'
              '  font-family:Verdana, Arial, Helvetica, sans-serif;'
              '  font-weight:bold;'
              '  color:#997744;'
              '}'
              'h4 {'
              '  margin:0;'
              '  padding:0;'
              '  border:0;'
              '  font-size:10px;'
              '  font-family:Verdana, Arial, Helvetica, sans-serif;'
              '  font-weight:bold;'
              '  color:#997744;'
              '  padding-left:15px;'
              '  padding-top:5px;'
              '}'
              'p {'
              '  margin:0;'
              '  padding:0;'
              '  border:0;'
              '  font-size:10px;'
              '  font-family:Verdana, Arial, Helvetica, sans-serif;'
              '  font-weight:bold;'
              '  color:#997744;'
              '  padding-left:15px;'
              '}'
              '.corpo {'
              '  width:635px;'
              '  border:2px solid #BBAA77;'
              '  padding-bottom:10px;'
              '  margin-bottom:10px;'
              '}'
              '.tit {'
              '  width:635px;'
              '  height:30px;'
              '  background:#FFE599;'
              '}'
              '.subtit {'
              '  width:605px;'
              '  margin:10px;'
              '}'
              '.tabcont {'
              '  margin-top:10px;'
              '  margin-left:15px;'
              '  border:1px solid #BBAA77;'
              '  background:#FFE599;'
              '}'
              '.tabline {'
              '  height:20px;'
              '  background:#FFFFFF;'
              '}'
              '.tabline p {'
              '  margin:0;'
              '  padding:0;'
              '  border:0;'
              '  font-size:10px;'
              '  font-family: arial, verdana, sans-serif;'
              '  color:#000000;'
              '  padding-left:2px;'
              '}'
              '#tabmsg {'
              '  margin:2px;'
              '}'
              '    </style>'
              '  </head>'
              '  <body>'
              '    <div class='#39'corpo'#39'>'
              '      <div class='#39'tit'#39'>'
              '        <h1>[Empresa]</h1>'
              '      </div>'
              '      <h4>[Saudacao] [Cliente]</h4>'
              
                '      <p>Segue em anexo o XML com protocolo de CARTA DE CORRE'#199#195'O' +
                ' da NFe:</p>'
              '      <p style='#39'font-size:12px'#39'>'
              '      <strong>S'#233'rie:</strong> [SerieNFe]<br>'
              '      <strong>N'#250'mero:</strong> [NumeroNFe]<br>'
              '      <strong>Chave:</strong> [ChaveNFe]      '
              '      <p>'
              
                '      <p>Abaixo est'#227'o descritos alguns detalhes desta CARTA DE C' +
                'ORRE'#199#195'O.</p>'
              '    </div>'
              '    <div class='#39'corpo'#39'>'
              '      <div class='#39'tit'#39'>'
              '        <h1>Carta de Corre'#231#227'o Eletr'#244'nica</h1>'
              '      </div>'
              
                '      <table class='#39'tabcont'#39' border='#39'0'#39' cellspacing='#39'4'#39' cellpadd' +
                'ing='#39'0'#39' width='#39'605'#39'>'
              '        <tbody>'
              '          <tr>'
              '            <td><h3>Vers'#227'o</h3></td>'
              '          </tr>'
              '          <tr>'
              '            <td class='#39'tabline'#39'><p>[versao]</p></td>'
              '          </tr>'
              '        </tbody>'
              '      </table>'
              
                '      <table class='#39'tabcont'#39' border='#39'0'#39' cellspacing='#39'4'#39' cellpadd' +
                'ing='#39'0'#39' width='#39'605'#39'>'
              '        <tbody>'
              '          <tr>'
              '            <td width='#39'235'#39'><h3>Org'#227'o</h3></td>'
              '            <td><h3>Ambiente</h3></td>'
              '          </tr>'
              '          <tr>'
              '            <td class='#39'tabline'#39'><p>[ORGAO]</p></td>'
              '            <td class='#39'tabline'#39'><p>[AMBIENTE]</p></td>'
              '          </tr>'
              '        </tbody>'
              '      </table>'
              
                '      <table class='#39'tabcont'#39' border='#39'0'#39' cellspacing='#39'4'#39' cellpadd' +
                'ing='#39'0'#39' width='#39'605'#39'>'
              '        <tbody>'
              '          <tr>'
              '            <td width='#39'115'#39'><h3>CNPJ / CPF</h3></td>'
              '            <td width='#39'355'#39'><h3>Chave de Acesso</h3></td>'
              '            <td><h3>Data</h3></td>'
              '          </tr>'
              '          <tr>'
              '            <td class='#39'tabline'#39'><p>[CNPJ_CPF]</p></td>'
              '            <td class='#39'tabline'#39'><p>[CHAVE_ACESSO]</p></td>'
              '            <td class='#39'tabline'#39'><p>[DATA]</p></td>'
              '          </tr>'
              '        </tbody>'
              '      </table>'
              
                '      <table class='#39'tabcont'#39' border='#39'0'#39' cellspacing='#39'4'#39' cellpadd' +
                'ing='#39'0'#39' width='#39'605'#39'>'
              '        <tbody>'
              '          <tr>'
              '            <td width='#39'265'#39'><h3>C'#243'digo do evento</h3></td>'
              '            <td><h3>Sequencial do evento</h3></td>'
              '          </tr>'
              '          <tr>'
              '            <td class='#39'tabline'#39'><p>[CODIGO_EVENTO]</p></td>'
              '            <td class='#39'tabline'#39'><p>[SEQUENCIAL_EVENTO]</p></td>'
              '          </tr>'
              '        </tbody>'
              '      </table>'
              '      <div class='#39'subtit'#39'>'
              '        <h2>Informa'#231#245'es da carta de corre'#231#227'o</h2>'
              '      </div>'
              
                '      <table class='#39'tabcont'#39' border='#39'0'#39' cellspacing='#39'4'#39' cellpadd' +
                'ing='#39'0'#39' width='#39'605'#39'>'
              '        <tbody>'
              '          <tr>'
              '            <td width='#39'155'#39'><h3>Vers'#227'o</h3></td>'
              '            <td><h3>Descri'#231#227'o do evento</h3></td>'
              '          </tr>'
              '          <tr>'
              '            <td class='#39'tabline'#39'><p>[verEvento]</p></td>'
              '            <td class='#39'tabline'#39'><p>[descEvento]</p></td>'
              '          </tr>'
              '        </tbody>'
              '      </table>'
              
                '      <table class='#39'tabcont'#39' border='#39'0'#39' cellspacing='#39'4'#39' cellpadd' +
                'ing='#39'0'#39' width='#39'605'#39'>'
              '        <tbody>'
              '          <tr>'
              '            <td><h3>Texto da Carta de Corre'#231#227'o</h3></td>'
              '          </tr>'
              '          <tr>'
              '            <td class='#39'tabline'#39'><p>[xCorrecao]</p></td>'
              '          </tr>'
              '        </tbody>'
              '      </table>'
              
                '      <table class='#39'tabcont'#39' border='#39'0'#39' cellspacing='#39'4'#39' cellpadd' +
                'ing='#39'0'#39' width='#39'605'#39'>'
              '        <tbody>'
              '          <tr>'
              
                '            <td><h3>Condi'#231#245'es de uso da Carta de Corre'#231#227'o</h3></' +
                'td>'
              '          </tr>'
              '          <tr>'
              
                '            <td class='#39'tabline'#39'><p id='#39'tabmsg'#39'>[xCondUso]</p></t' +
                'd>'
              '          </tr>'
              '        </tbody>'
              '      </table>'
              '    </div>'
              '  </body>'
              '</html>')
            TabOrder = 8
            Visible = False
            WantReturns = False
            WordWrap = False
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 397
      Width = 780
      Height = 63
      Align = alBottom
      TabOrder = 2
      ExplicitTop = 396
      object Panel5: TPanel
        Left = 2
        Top = 14
        Width = 169
        Height = 48
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 126
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 86
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 47
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 171
        Top = 14
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 266
        Top = 14
        Width = 512
        Height = 48
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 382
          Top = 0
          Width = 131
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 118
            Height = 39
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtPreMail: TBitBtn
          Tag = 406
          Left = 4
          Top = 4
          Width = 89
          Height = 39
          Cursor = crHandPoint
          Caption = '&Pr'#233'-email'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtPreMailClick
        end
        object BtMsg: TBitBtn
          Tag = 11
          Left = 94
          Top = 4
          Width = 89
          Height = 39
          Cursor = crHandPoint
          Caption = '&Item'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtMsgClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 185
          Top = 4
          Width = 89
          Height = 39
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtExcluiClick
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 94
    Width = 780
    Height = 460
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 2
      Top = 4
      Width = 808
      Height = 242
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 67
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 498
        Top = 102
        Width = 153
        Height = 13
        Caption = 'Nome de identifica'#231#227'o de envio:'
      end
      object Label12: TLabel
        Left = 8
        Top = 43
        Width = 41
        Height = 13
        Caption = 'Assunto:'
      end
      object Label22: TLabel
        Left = 336
        Top = 43
        Width = 52
        Height = 13
        Caption = 'Sauda'#231#227'o:'
      end
      object LaSolicitante: TLabel
        Left = 8
        Top = 102
        Width = 111
        Height = 13
        Caption = 'Configura'#231#227'o de e-mail:'
      end
      object SBSolicitante: TSpeedButton
        Left = 470
        Top = 118
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SBSolicitanteClick
      end
      object dmkEdNome: TdmkEdit
        Left = 67
        Top = 20
        Width = 647
        Height = 20
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdType = utNil
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object dmkEdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 20
        TabStop = False
        Alignment = taRightJustify
        Color = clInactiveCaption
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBackground
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdType = utNil
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdSend_Name: TdmkEdit
        Left = 498
        Top = 118
        Width = 216
        Height = 21
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Send_Name'
        UpdCampo = 'Send_Name'
        UpdType = utNil
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object dmkEdMail_Titu: TdmkEdit
        Left = 8
        Top = 59
        Width = 324
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Mail_Titu'
        UpdCampo = 'Mail_Titu'
        UpdType = utNil
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSaudacao: TdmkEdit
        Left = 336
        Top = 59
        Width = 378
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Mail_Titu'
        UpdCampo = 'Saudacao'
        UpdType = utNil
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkNaoEnvBloq: TdmkCheckBox
        Left = 8
        Top = 82
        Width = 178
        Height = 17
        Caption = 'N'#227'o enviar anexo'
        TabOrder = 4
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object CkReturnReciept: TdmkCheckBox
        Left = 357
        Top = 82
        Width = 324
        Height = 17
        Caption = 'Solicitar confirma'#231#227'o de leitura'
        TabOrder = 5
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object EdEmailConta: TdmkEditCB
        Left = 8
        Top = 118
        Width = 55
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'EmailConta'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmailContaChange
        DBLookupComboBox = CBEmailConta
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmailConta: TdmkDBLookupComboBox
        Left = 63
        Top = 118
        Width = 403
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEmailConta
        TabOrder = 7
        dmkEditCB = EdEmailConta
        QryCampo = 'EmailConta'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 391
      Width = 780
      Height = 69
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 390
      object Panel6: TPanel
        Left = 2
        Top = 14
        Width = 776
        Height = 54
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 635
          Top = 0
          Width = 142
          Height = 54
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 118
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 5
          Top = 2
          Width = 118
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 780
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 733
      Top = 0
      Width = 47
      Height = 51
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 213
      Height = 51
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 86
        Top = 8
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 128
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 170
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 213
      Top = 0
      Width = 520
      Height = 51
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 259
        Height = 31
        Caption = 'Cadastro de Pr'#233'-email'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 259
        Height = 31
        Caption = 'Cadastro de Pr'#233'-email'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 259
        Height = 31
        Caption = 'Cadastro de Pr'#233'-email'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 51
    Width = 780
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 14
      Width = 776
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsPreEmail: TDataSource
    DataSet = QrPreEmail
    Left = 416
    Top = 9
  end
  object QrPreEmail: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPreEmailBeforeOpen
    AfterOpen = QrPreEmailAfterOpen
    AfterScroll = QrPreEmailAfterScroll
    SQL.Strings = (
      'SELECT pre.*, cfg.Usuario EmailConta_TXT'
      'FROM preemail pre'
      'LEFT JOIN emailconta cfg ON cfg.Codigo = pre.EmailConta'
      'WHERE pre.Codigo > 0')
    Left = 388
    Top = 9
    object QrPreEmailCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'preemail.Codigo'
    end
    object QrPreEmailNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'preemail.Nome'
      Size = 100
    end
    object QrPreEmailAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'preemail.AlterWeb'
    end
    object QrPreEmailAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'preemail.Ativo'
    end
    object QrPreEmailLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'preemail.Lk'
    end
    object QrPreEmailDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'preemail.DataCad'
    end
    object QrPreEmailDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'preemail.DataAlt'
    end
    object QrPreEmailUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'preemail.UserCad'
    end
    object QrPreEmailUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'preemail.UserAlt'
    end
    object QrPreEmailSend_Name: TWideStringField
      FieldName = 'Send_Name'
      Origin = 'preemail.Send_Name'
      Size = 50
    end
    object QrPreEmailMail_Titu: TWideStringField
      FieldName = 'Mail_Titu'
      Origin = 'preemail.Mail_Titu'
      Size = 100
    end
    object QrPreEmailSaudacao: TWideStringField
      FieldName = 'Saudacao'
      Size = 100
    end
    object QrPreEmailNaoEnvBloq: TSmallintField
      FieldName = 'NaoEnvBloq'
    end
    object QrPreEmailReturnReciept: TSmallintField
      FieldName = 'ReturnReciept'
    end
    object QrPreEmailEmailConta_TXT: TWideStringField
      FieldName = 'EmailConta_TXT'
      Size = 100
    end
    object QrPreEmailEmailConta: TIntegerField
      FieldName = 'EmailConta'
    end
  end
  object PMPreMail: TPopupMenu
    OnPopup = PMPreMailPopup
    Left = 356
    Top = 496
    object Incluinovopremeio1: TMenuItem
      Caption = '&Inclui novo pr'#233'-email'
      OnClick = Incluinovopremeio1Click
    end
    object Alterapremeioatual1: TMenuItem
      Caption = '&Altera pr'#233'-email atual'
      OnClick = Alterapremeioatual1Click
    end
    object Excluipremeioatual1: TMenuItem
      Caption = '&Exclui pr'#233'-email atual'
      Enabled = False
      OnClick = Excluipremeioatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Duplicaritematual1: TMenuItem
      Caption = '&Duplicar item atual'
      OnClick = Duplicaritematual1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Emailspadres1: TMenuItem
      Caption = 'E-mails padr'#245'es'
      object Criarnoexistentes1: TMenuItem
        Caption = '&Criar padr'#245'es n'#227'o criados'
        OnClick = Criarnoexistentes1Click
      end
      object Recriapadres1: TMenuItem
        Caption = '&Recriar todos padr'#245'es'
        OnClick = Recriapadres1Click
      end
    end
  end
  object PMMsg: TPopupMenu
    OnPopup = PMMsgPopup
    Left = 452
    Top = 500
    object Incluinovotpico1: TMenuItem
      Caption = '&Inclui novo t'#243'pico'
      OnClick = Incluinovotpico1Click
    end
    object Alteratpicoselecionado1: TMenuItem
      Caption = '&Altera t'#243'pico selecionado'
      OnClick = Alteratpicoselecionado1Click
    end
    object Excluitpicoselecionado1: TMenuItem
      Caption = '&Exclui t'#243'pico selecionado'
      OnClick = Excluitpicoselecionado1Click
    end
  end
  object QrPreEmMsg: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPreEmMsgBeforeClose
    AfterScroll = QrPreEmMsgAfterScroll
    SQL.Strings = (
      'SELECT ELT(pem.Tipo+1, '
      #39'Plano'#39', '#39'HTML'#39', '#39'Imagem'#39') NOMETIPO, pem.* '
      'FROM preemmsg pem'
      'WHERE pem.Codigo=:P0'
      'ORDER BY Ordem, Controle, Tipo')
    Left = 472
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPreEmMsgNOMETIPO: TWideStringField
      FieldName = 'NOMETIPO'
      Size = 10
    end
    object QrPreEmMsgCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'preemmsg.Codigo'
    end
    object QrPreEmMsgControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'preemmsg.Controle'
    end
    object QrPreEmMsgOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'preemmsg.Ordem'
    end
    object QrPreEmMsgTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'preemmsg.Tipo'
    end
    object QrPreEmMsgTexto: TWideMemoField
      FieldName = 'Texto'
      Origin = 'preemmsg.Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPreEmMsgLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'preemmsg.Lk'
    end
    object QrPreEmMsgDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'preemmsg.DataCad'
    end
    object QrPreEmMsgDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'preemmsg.DataAlt'
    end
    object QrPreEmMsgUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'preemmsg.UserCad'
    end
    object QrPreEmMsgUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'preemmsg.UserAlt'
    end
    object QrPreEmMsgAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'preemmsg.AlterWeb'
    end
    object QrPreEmMsgAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'preemmsg.Ativo'
      MaxValue = 1
    end
    object QrPreEmMsgDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'preemmsg.Descricao'
      Size = 255
    end
    object QrPreEmMsgCidID_Img: TWideStringField
      FieldName = 'CidID_Img'
      Size = 32
    end
  end
  object DsPreEmMsg: TDataSource
    DataSet = QrPreEmMsg
    Left = 500
    Top = 9
  end
  object QrEmailConta: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPreEmMsgAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Usuario Nome, Send_Name'
      'FROM emailconta'
      'WHERE TipoConta=1'
      'AND PermisNiv=0'
      'AND Ativo=1'
      'ORDER BY Nome')
    Left = 568
    Top = 17
    object QrEmailContaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmailContaNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrEmailContaSend_Name: TWideStringField
      FieldName = 'Send_Name'
      Size = 50
    end
  end
  object DsEmailConta: TDataSource
    DataSet = QrEmailConta
    Left = 596
    Top = 17
  end
end
