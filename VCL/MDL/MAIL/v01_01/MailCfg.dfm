object FmMailCfg: TFmMailCfg
  Left = 368
  Top = 194
  Caption = 'MAI-CONFI-001 :: Configura'#231#227'o de E-mails e Contas'
  ClientHeight = 492
  ClientWidth = 862
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 94
    Width = 862
    Height = 398
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 862
      Height = 98
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 75
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label15: TLabel
        Left = 75
        Top = 55
        Width = 39
        Height = 13
        Caption = 'Usu'#225'rio:'
        FocusControl = dmkDBEdit1
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 31
        Width = 55
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsEmailConta
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 75
        Top = 31
        Width = 681
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsEmailConta
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 75
        Top = 71
        Width = 681
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Usuario'
        DataSource = DsEmailConta
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 334
      Width = 862
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 169
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 126
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 86
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 47
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 171
        Top = 15
        Width = 176
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 347
        Top = 15
        Width = 513
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 248
          Top = 4
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExcluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 126
          Top = 4
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 382
          Top = 0
          Width = 131
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 118
            Height = 39
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnDmk: TPanel
    Left = 0
    Top = 94
    Width = 862
    Height = 398
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 862
      Height = 269
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label5: TLabel
        Left = 16
        Top = 55
        Width = 39
        Height = 13
        Caption = 'Usu'#225'rio:'
      end
      object Label6: TLabel
        Left = 294
        Top = 55
        Width = 34
        Height = 13
        Caption = 'Senha:'
      end
      object EdCodigoDmk: TdmkEdit
        Left = 16
        Top = 31
        Width = 55
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdUsuarioDmk: TdmkEdit
        Left = 16
        Top = 71
        Width = 271
        Height = 20
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSenhaDmk: TEdit
        Left = 294
        Top = 72
        Width = 271
        Height = 22
        Font.Charset = SYMBOL_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        PasswordChar = 'l'
        TabOrder = 2
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 336
      Width = 862
      Height = 62
      Align = alBottom
      TabOrder = 1
      object BtConfirmaDmk: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaDmkClick
      end
      object Panel1: TPanel
        Left = 725
        Top = 15
        Width = 135
        Height = 45
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesisteDmk: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 118
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteDmkClick
        end
      end
      object CkMostrarSenhaDmk: TdmkCheckBox
        Left = 136
        Top = 27
        Width = 213
        Height = 17
        Caption = 'Mostrar caracteres nos campos de senha'
        TabOrder = 2
        OnClick = CkMostrarSenhaDmkClick
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
    end
  end
  object PnEmailPOP: TPanel
    Left = 0
    Top = 94
    Width = 862
    Height = 398
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 4
    Visible = False
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 862
      Height = 324
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object PageControl1: TPageControl
        Left = 2
        Top = 15
        Width = 858
        Height = 307
        ActivePage = TabSheet1
        Align = alClient
        TabHeight = 20
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Configura'#231#245'es do e-mail'
          object Label17: TLabel
            Left = 214
            Top = 213
            Width = 96
            Height = 13
            Caption = 'Servidor de entrada:'
          end
          object Label10: TLabel
            Left = 679
            Top = 63
            Width = 79
            Height = 13
            Caption = 'Senha da conta:'
          end
          object Label11: TLabel
            Left = 460
            Top = 63
            Width = 28
            Height = 13
            Caption = 'E-mail'
          end
          object Label9: TLabel
            Left = 238
            Top = 62
            Width = 153
            Height = 13
            Caption = 'Nome de identifica'#231#227'o de envio:'
          end
          object Label8: TLabel
            Left = 16
            Top = 62
            Width = 155
            Height = 13
            Caption = 'Servidor SMTP (envio de e-mail):'
          end
          object Label3: TLabel
            Left = 16
            Top = 16
            Width = 14
            Height = 13
            Caption = 'ID:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label4: TLabel
            Left = 294
            Top = 16
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
            Color = clBtnFace
            ParentColor = False
          end
          object GroupBox3: TGroupBox
            Left = 16
            Top = 106
            Width = 438
            Height = 80
            Caption = '      '
            TabOrder = 5
            object Label12: TLabel
              Left = 16
              Top = 31
              Width = 109
              Height = 13
              Caption = 'Login de autentica'#231#227'o:'
            end
            object Label13: TLabel
              Left = 276
              Top = 31
              Width = 114
              Height = 13
              Caption = 'Senha de autentica'#231#227'o:'
            end
            object EdLogi_NamePOP: TdmkEdit
              Left = 16
              Top = 47
              Width = 256
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = True
              NoForceUppercase = True
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'Logi_Name'
              UpdCampo = 'Logi_Name'
              UpdType = utNil
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLogi_PassPOP: TEdit
              Left = 276
              Top = 47
              Width = 138
              Height = 22
              Font.Charset = SYMBOL_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              PasswordChar = '|'
              TabOrder = 1
            end
          end
          object GroupBox4: TGroupBox
            Left = 460
            Top = 106
            Width = 360
            Height = 80
            Caption = 'N'#250'meros de porta do servidor de sa'#237'da'
            TabOrder = 0
            object Label14: TLabel
              Left = 8
              Top = 21
              Width = 117
              Height = 13
              Caption = 'Emails da sa'#237'da (SMTP):'
            end
            object Label16: TLabel
              Left = 8
              Top = 39
              Width = 54
              Height = 13
              Caption = 'N'#186' da porta'
            end
            object LaEnvTLSTipo: TLabel
              Left = 238
              Top = 21
              Width = 92
              Height = 13
              Caption = 'Tipo de seguran'#231'a:'
            end
            object LaEnvTLSUse: TLabel
              Left = 130
              Top = 21
              Width = 90
              Height = 13
              Caption = 'Uso de seguran'#231'a:'
            end
            object EdPorta_MailPOP: TdmkEdit
              Left = 66
              Top = 35
              Width = 57
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = True
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Porta_Mail'
              UpdCampo = 'Porta_Mail'
              UpdType = utNil
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object CkLogi_SSLPOP: TdmkCheckBox
              Left = 8
              Top = 58
              Width = 282
              Height = 16
              Caption = 'Este servidor requer uma conex'#227'o de seguran'#231'a (SSL)'
              TabOrder = 3
              OnClick = CkLogi_SSLPOPClick
              QryCampo = 'Logi_SSL'
              UpdCampo = 'Logi_SSL'
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
            object CBEnvTLSTipo: TComboBox
              Left = 238
              Top = 35
              Width = 104
              Height = 21
              AutoDropDown = True
              Style = csDropDownList
              Color = clWhite
              DropDownCount = 12
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 7622183
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
            end
            object CBEnvTLSUse: TComboBox
              Left = 130
              Top = 35
              Width = 104
              Height = 21
              AutoDropDown = True
              Style = csDropDownList
              Color = clWhite
              DropDownCount = 12
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 7622183
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
          object GroupBox5: TGroupBox
            Left = 460
            Top = 186
            Width = 360
            Height = 80
            Caption = 'N'#250'meros de porta do servidor de entrada'
            TabOrder = 1
            object Label18: TLabel
              Left = 8
              Top = 21
              Width = 87
              Height = 13
              Caption = 'Emails de entrada:'
            end
            object Label19: TLabel
              Left = 8
              Top = 39
              Width = 54
              Height = 13
              Caption = 'N'#186' da porta'
            end
            object LaReceTLSTipo: TLabel
              Left = 238
              Top = 21
              Width = 92
              Height = 13
              Caption = 'Tipo de seguran'#231'a:'
            end
            object LaReceTLSUse: TLabel
              Left = 130
              Top = 21
              Width = 90
              Height = 13
              Caption = 'Uso de seguran'#231'a:'
            end
            object EdRecePorta_MailPOP: TdmkEdit
              Left = 66
              Top = 35
              Width = 57
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = True
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Porta_Mail'
              UpdCampo = 'Porta_Mail'
              UpdType = utNil
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object CkReceLogi_SSLPOP: TdmkCheckBox
              Left = 8
              Top = 57
              Width = 282
              Height = 17
              Caption = 'Este servidor requer uma conex'#227'o de seguran'#231'a (SSL)'
              TabOrder = 3
              OnClick = CkReceLogi_SSLPOPClick
              QryCampo = 'Logi_SSL'
              UpdCampo = 'Logi_SSL'
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
            object CBReceTLSTipo: TComboBox
              Left = 238
              Top = 35
              Width = 104
              Height = 21
              AutoDropDown = True
              Style = csDropDownList
              Color = clWhite
              DropDownCount = 12
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 7622183
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
            end
            object CBReceTLSUse: TComboBox
              Left = 130
              Top = 35
              Width = 104
              Height = 21
              AutoDropDown = True
              Style = csDropDownList
              Color = clWhite
              DropDownCount = 12
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 7622183
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
          object EdReceServerPOP: TdmkEdit
            Left = 214
            Top = 227
            Width = 216
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = True
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SMTPServer'
            UpdCampo = 'SMTPServer'
            UpdType = utNil
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object RGReceTipoPOP: TRadioGroup
            Left = 14
            Top = 192
            Width = 192
            Height = 56
            Caption = 'Tipo de servidor de entrada'
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'POP3'
              'IMAP4')
            TabOrder = 3
          end
          object CkLogi_AuthPOP: TdmkCheckBox
            Left = 31
            Top = 106
            Width = 187
            Height = 17
            Caption = ' Meu servidor requer autentica'#231#227'o: '
            TabOrder = 4
            QryCampo = 'Logi_Auth'
            UpdCampo = 'Logi_Auth'
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object EdSMTPServerPOP: TdmkEdit
            Left = 16
            Top = 78
            Width = 216
            Height = 21
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = True
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SMTPServer'
            UpdCampo = 'SMTPServer'
            UpdType = utNil
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSend_NamePOP: TdmkEdit
            Left = 238
            Top = 78
            Width = 216
            Height = 21
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = True
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Send_Name'
            UpdCampo = 'Send_Name'
            UpdType = utNil
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdUsuarioPOP: TdmkEdit
            Left = 460
            Top = 78
            Width = 217
            Height = 21
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = True
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Send_Mail'
            UpdCampo = 'Send_Mail'
            UpdType = utNil
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdUsuarioPOPExit
          end
          object EdSenhaPOP: TEdit
            Left = 679
            Top = 78
            Width = 103
            Height = 22
            Font.Charset = SYMBOL_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            PasswordChar = 'l'
            TabOrder = 9
            OnExit = EdSenhaPOPExit
          end
          object RgTipoPOP: TdmkRadioGroup
            Left = 77
            Top = 16
            Width = 210
            Height = 36
            Caption = 'N'#237'vel de permiss'#227'o:'
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Liberado'
              'Apenas eu')
            TabOrder = 10
            UpdType = utYes
            OldValor = 0
          end
          object EdCodigoPOP: TdmkEdit
            Left = 16
            Top = 31
            Width = 55
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 11
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Codigo'
            UpdCampo = 'Codigo'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNomePOP: TdmkEdit
            Left = 294
            Top = 31
            Width = 488
            Height = 21
            TabOrder = 12
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Nome'
            UpdCampo = 'Nome'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Configura'#231#245'es para gerenciamento de e-mails'
          ImageIndex = 1
          object Label20: TLabel
            Left = 6
            Top = 16
            Width = 99
            Height = 13
            Caption = 'Baixar novos e-mails:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label21: TLabel
            Left = 6
            Top = 58
            Width = 332
            Height = 13
            Caption = 
              'Baixar e-mails deste per'#237'odo: (Tentar n'#227'o usar caso der certo re' +
              'mover)'
            Color = clBtnFace
            ParentColor = False
          end
          object Label22: TLabel
            Left = 6
            Top = 195
            Width = 711
            Height = 13
            Caption = 
              'Caso o campo "Usar assinatura" seja marcado, somente os e-mails ' +
              'gerados '#224' partir da janela de gerenciamento de e-mails ir'#227'o rece' +
              'ber esta assinatura.'
            Color = clBtnFace
            ParentColor = False
          end
          object CBVerifiMail: TComboBox
            Left = 6
            Top = 31
            Width = 310
            Height = 21
            Style = csDropDownList
            TabOrder = 0
          end
          object CBDownDias: TComboBox
            Left = 6
            Top = 74
            Width = 424
            Height = 21
            Style = csDropDownList
            TabOrder = 2
          end
          object MeAssinatura: TdmkMemo
            Left = 6
            Top = 125
            Width = 424
            Height = 64
            Lines.Strings = (
              'MeAssinatura')
            TabOrder = 4
            UpdType = utYes
          end
          object CkUsarAssinatura: TdmkCheckBox
            Left = 6
            Top = 103
            Width = 96
            Height = 17
            Caption = 'Usar assinatura'
            TabOrder = 3
            OnClick = CkUsarAssinaturaClick
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object CkSincEmail: TCheckBox
            Left = 321
            Top = 34
            Width = 109
            Height = 16
            Caption = 'Sincronizar e-mail'
            TabOrder = 1
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'TabSheet3'
          ImageIndex = 2
          object DBGrid1: TDBGrid
            Left = 0
            Top = 0
            Width = 850
            Height = 277
            Align = alClient
            DataSource = DsEmailConta
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 336
      Width = 862
      Height = 62
      Align = alBottom
      TabOrder = 1
      object BtConfirmaPOP: TBitBtn
        Tag = 14
        Left = 12
        Top = 18
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaPOPClick
      end
      object Panel7: TPanel
        Left = 725
        Top = 15
        Width = 135
        Height = 45
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesistePOP: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 118
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteDmkClick
        end
      end
      object CkMostrarSenhaPOP: TdmkCheckBox
        Left = 404
        Top = 27
        Width = 212
        Height = 17
        Caption = 'Mostrar caracteres nos campos de senha'
        TabOrder = 2
        OnClick = CkMostrarSenhaPOPClick
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object BtTestar: TBitBtn
        Tag = 10001
        Left = 133
        Top = 17
        Width = 148
        Height = 39
        Cursor = crHandPoint
        Caption = '&Testar configura'#231#227'o'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtTestarClick
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 862
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 814
      Top = 0
      Width = 48
      Height = 51
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 213
      Height = 51
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 86
        Top = 8
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 128
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 170
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 213
      Top = 0
      Width = 601
      Height = 51
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 392
        Height = 31
        Caption = 'Configura'#231#227'o de E-mails e Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 392
        Height = 31
        Caption = 'Configura'#231#227'o de E-mails e Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 392
        Height = 31
        Caption = 'Configura'#231#227'o de E-mails e Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 51
    Width = 862
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 858
      Height = 26
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrEmailConta: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrEmailContaBeforeOpen
    AfterOpen = QrEmailContaAfterOpen
    BeforeClose = QrEmailContaBeforeClose
    BeforeScroll = QrEmailContaBeforeScroll
    AfterScroll = QrEmailContaAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM cadastro_simples')
    Left = 680
    Top = 48
    object QrEmailContaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmailContaNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
    object QrEmailContaTipoConta: TSmallintField
      FieldName = 'TipoConta'
    end
    object QrEmailContaLogi_SSL: TSmallintField
      FieldName = 'Logi_SSL'
    end
    object QrEmailContaSENHA: TWideStringField
      FieldName = 'SENHA'
      Size = 32
    end
    object QrEmailContaUsuario: TWideStringField
      FieldName = 'Usuario'
      Size = 100
    end
    object QrEmailContaSMTPServer: TWideStringField
      FieldName = 'SMTPServer'
      Size = 50
    end
    object QrEmailContaSend_Name: TWideStringField
      FieldName = 'Send_Name'
      Size = 50
    end
    object QrEmailContaLogi_Auth: TSmallintField
      FieldName = 'Logi_Auth'
    end
    object QrEmailContaLogi_Name: TWideStringField
      FieldName = 'Logi_Name'
      Size = 100
    end
    object QrEmailContaPorta_Mail: TIntegerField
      FieldName = 'Porta_Mail'
    end
    object QrEmailContaLogi_SENHA: TWideStringField
      FieldName = 'Logi_SENHA'
      Size = 30
    end
    object QrEmailContaPermisNiv: TIntegerField
      FieldName = 'PermisNiv'
    end
    object QrEmailContaReceTipo: TIntegerField
      FieldName = 'ReceTipo'
    end
    object QrEmailContaReceServer: TWideStringField
      FieldName = 'ReceServer'
      Size = 50
    end
    object QrEmailContaRecePorta_Mail: TIntegerField
      FieldName = 'RecePorta_Mail'
    end
    object QrEmailContaReceLogi_SSL: TIntegerField
      FieldName = 'ReceLogi_SSL'
    end
    object QrEmailContaVerifiMail: TIntegerField
      FieldName = 'VerifiMail'
    end
    object QrEmailContaDownDias: TIntegerField
      FieldName = 'DownDias'
    end
    object QrEmailContaUsarAssinatura: TIntegerField
      FieldName = 'UsarAssinatura'
    end
    object QrEmailContaAssinatura: TWideStringField
      FieldName = 'Assinatura'
      Size = 255
    end
    object QrEmailContaSincEmail: TIntegerField
      FieldName = 'SincEmail'
    end
    object QrEmailContaEnvTLSTipo: TIntegerField
      FieldName = 'EnvTLSTipo'
    end
    object QrEmailContaReceTLSTipo: TIntegerField
      FieldName = 'ReceTLSTipo'
    end
    object QrEmailContaReceTLSUse: TIntegerField
      FieldName = 'ReceTLSUse'
    end
    object QrEmailContaEnvTLSUse: TIntegerField
      FieldName = 'EnvTLSUse'
    end
  end
  object DsEmailConta: TDataSource
    DataSet = QrEmailConta
    Left = 680
    Top = 96
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 684
    Top = 144
  end
  object PMInclui: TPopupMenu
    Left = 504
    Top = 128
    object ContaDermatek1: TMenuItem
      Caption = 'Conta &Dermatek'
      OnClick = ContaDermatek1Click
    end
    object ContaEmailPOP31: TMenuItem
      Caption = 'Conta &E-mail POP3 / IMAP4'
      OnClick = ContaEmailPOP31Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object N2: TMenuItem
      Caption = 'Conta Dermatek envio NFe (c'#243'd. -1)'
      OnClick = N2Click
    end
  end
  object IMAP: TIdIMAP4
    IOHandler = IdSSL1
    SASLMechanisms = <>
    MilliSecsToWaitToClearBuffer = 10
    Left = 435
    Top = 64
  end
  object IdSSL1: TIdSSLIOHandlerSocketOpenSSL
    Destination = ':25'
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Method = sslvSSLv2
    SSLOptions.SSLVersions = [sslvSSLv2]
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 464
    Top = 64
  end
  object IdSMTP1: TIdSMTP
    IOHandler = IdSSL1
    SASLMechanisms = <>
    UseTLS = utUseExplicitTLS
    Left = 492
    Top = 64
  end
end
