object FmMailEnv: TFmMailEnv
  Left = 339
  Top = 185
  Caption = 'MAI-ENVIA-001 :: Envio de e-mails'
  ClientHeight = 662
  ClientWidth = 874
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 874
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 826
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 778
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 200
        Height = 32
        Caption = 'Envio de e-mails'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 200
        Height = 32
        Caption = 'Envio de e-mails'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 200
        Height = 32
        Caption = 'Envio de e-mails'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 874
    Height = 484
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 874
      Height = 484
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 874
        Height = 484
        Align = alClient
        TabOrder = 0
        object PnTopo: TPanel
          Left = 2
          Top = 15
          Width = 870
          Height = 148
          Align = alTop
          TabOrder = 0
          object Label23: TLabel
            Left = 8
            Top = 32
            Width = 25
            Height = 13
            Caption = 'Para:'
          end
          object Label1: TLabel
            Left = 8
            Top = 59
            Width = 25
            Height = 13
            Caption = 'Cc...:'
          end
          object Label2: TLabel
            Left = 8
            Top = 80
            Width = 31
            Height = 13
            Caption = 'Cco...:'
          end
          object Label3: TLabel
            Left = 8
            Top = 104
            Width = 41
            Height = 13
            Caption = 'Assunto:'
          end
          object SBPara: TSpeedButton
            Left = 837
            Top = 29
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SBParaClick
          end
          object SBCc: TSpeedButton
            Left = 837
            Top = 54
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SBCcClick
          end
          object SBCco: TSpeedButton
            Left = 837
            Top = 77
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SBCcoClick
          end
          object LaSolicitante: TLabel
            Left = 8
            Top = 11
            Width = 111
            Height = 13
            Caption = 'Configura'#231#227'o de e-mail:'
          end
          object SBSolicitante: TSpeedButton
            Left = 837
            Top = 8
            Width = 21
            Height = 20
            Caption = '...'
            OnClick = SBSolicitanteClick
          end
          object SBInfoDest: TSpeedButton
            Left = 837
            Top = 125
            Width = 21
            Height = 21
            Caption = '?'
            OnClick = SBInfoDestClick
          end
          object EdPara: TdmkEdit
            Left = 60
            Top = 29
            Width = 775
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = True
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCC: TdmkEdit
            Left = 60
            Top = 54
            Width = 775
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = True
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCCO: TdmkEdit
            Left = 60
            Top = 77
            Width = 775
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = True
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdAssunto: TdmkEdit
            Left = 60
            Top = 102
            Width = 798
            Height = 20
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object CkConfLei: TCheckBox
            Left = 8
            Top = 126
            Width = 130
            Height = 17
            Caption = 'Confirma'#231#227'o de leitura'
            Checked = True
            State = cbChecked
            TabOrder = 4
          end
          object CkTextoPlano: TCheckBox
            Left = 150
            Top = 126
            Width = 130
            Height = 17
            Caption = 'Texto sem formata'#231#227'o'
            TabOrder = 5
            OnClick = CkTextoPlanoClick
          end
          object EdEmailConta: TdmkEditCB
            Left = 125
            Top = 8
            Width = 56
            Height = 20
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEmailConta
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEmailConta: TdmkDBLookupComboBox
            Left = 181
            Top = 8
            Width = 654
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsEmailCfg
            TabOrder = 7
            dmkEditCB = EdEmailConta
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object CkDestinatarios: TCheckBox
            Left = 681
            Top = 126
            Width = 154
            Height = 17
            Caption = 'Individualizar destinat'#225'rios'
            TabOrder = 8
            OnClick = CkTextoPlanoClick
          end
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 163
          Width = 870
          Height = 319
          ActivePage = TabSheet1
          Align = alClient
          TabHeight = 25
          TabOrder = 1
          object TabSheet1: TTabSheet
            Caption = 'Texto HTML'
            object WebBrowser1: TWebBrowser
              Left = 0
              Top = 74
              Width = 862
              Height = 210
              Align = alClient
              TabOrder = 0
              ExplicitWidth = 851
              ExplicitHeight = 227
              ControlData = {
                4C00000017590000B41500000000000000000000000000000000000000000000
                000000004C000000000000000000000001000000E0D057007335CF11AE690800
                2B2E126208000000000000004C0000000114020000000000C000000000000046
                8000000000000000000000000000000000000000000000000000000000000000
                00000000000000000100000000000000000000000000000000000000}
            end
            object ToolBar1: TToolBar
              Left = 0
              Top = 0
              Width = 862
              Height = 45
              ButtonHeight = 40
              ButtonWidth = 40
              Caption = 'ToolBar1'
              Images = FmMyGlyfs.Lista_32X32_Textos
              TabOrder = 1
              Transparent = True
              object TBCor: TToolButton
                Left = 0
                Top = 0
                Hint = 'Cor da Fonte'
                Caption = 'Font Color'
                ImageIndex = 15
                ParentShowHint = False
                ShowHint = True
                OnClick = TBCorClick
              end
              object ToolButton2: TToolButton
                Left = 40
                Top = 0
                Width = 8
                Caption = 'ToolButton2'
                ImageIndex = 22
                Style = tbsSeparator
              end
              object TBNegrito: TToolButton
                Left = 48
                Top = 0
                Hint = 'Negrito'
                Caption = 'Bold'
                ImageIndex = 8
                ParentShowHint = False
                ShowHint = True
                Style = tbsCheck
                OnClick = TBNegritoClick
              end
              object TBItalico: TToolButton
                Left = 88
                Top = 0
                Hint = 'It'#225'lico'
                Caption = 'Italic'
                ImageIndex = 9
                ParentShowHint = False
                ShowHint = True
                Style = tbsCheck
                OnClick = TBItalicoClick
              end
              object TBSublinhado: TToolButton
                Left = 128
                Top = 0
                Hint = 'Sublinhado'
                Caption = 'Underline'
                ImageIndex = 10
                ParentShowHint = False
                ShowHint = True
                Style = tbsCheck
                OnClick = TBSublinhadoClick
              end
              object ToolButton3: TToolButton
                Left = 168
                Top = 0
                Hint = 'Tachado'
                Caption = 'Strikethrough'
                ImageIndex = 22
                ParentShowHint = False
                ShowHint = True
                OnClick = ToolButton3Click
              end
              object ToolButton5: TToolButton
                Left = 208
                Top = 0
                Width = 10
                Caption = 'ToolButton5'
                ImageIndex = 4
                Style = tbsSeparator
              end
              object TTBSub: TToolButton
                Left = 218
                Top = 0
                Hint = 'Subscrito'
                Caption = 'Subscript'
                ImageIndex = 24
                ParentShowHint = False
                ShowHint = True
                OnClick = TTBSubClick
              end
              object TTBSob: TToolButton
                Left = 258
                Top = 0
                Hint = 'Sobrescrito'
                Caption = 'Superscript'
                ImageIndex = 23
                ParentShowHint = False
                ShowHint = True
                OnClick = TTBSobClick
              end
              object ToolButton4: TToolButton
                Left = 298
                Top = 0
                Width = 8
                Caption = 'ToolButton4'
                ImageIndex = 22
                Style = tbsSeparator
              end
              object TBLista: TToolButton
                Left = 306
                Top = 0
                Hint = 'Lista - Marcadores'
                Caption = 'Markers'
                ImageIndex = 14
                ParentShowHint = False
                ShowHint = True
                OnClick = TBListaClick
              end
              object TBListaNum: TToolButton
                Left = 346
                Top = 0
                Hint = 'Lista - Numera'#231#227'o'
                Caption = 'Numbering'
                ImageIndex = 17
                ParentShowHint = False
                ShowHint = True
                OnClick = TBListaNumClick
              end
              object ToolButton1: TToolButton
                Left = 386
                Top = 0
                Width = 8
                Caption = 'ToolButton1'
                ImageIndex = 22
                Style = tbsSeparator
              end
              object TBDimRec: TToolButton
                Left = 394
                Top = 0
                Hint = 'Diminuir recuo'
                Caption = 'Decrease Indent'
                ImageIndex = 18
                ParentShowHint = False
                ShowHint = True
                OnClick = TBDimRecClick
              end
              object TBAumRec: TToolButton
                Left = 434
                Top = 0
                Hint = 'Aumentar recuo'
                Caption = 'Increase Indent'
                ImageIndex = 19
                ParentShowHint = False
                ShowHint = True
                OnClick = TBAumRecClick
              end
              object ToolButton10: TToolButton
                Left = 474
                Top = 0
                Width = 10
                Caption = 'ToolButton10'
                ImageIndex = 8
                Style = tbsSeparator
              end
              object TTBAlinEsq: TToolButton
                Left = 484
                Top = 0
                Hint = 'Alinhar texto '#224' esquerda'
                Caption = 'Align Left'
                ImageIndex = 11
                ParentShowHint = False
                ShowHint = True
                Style = tbsCheck
                OnClick = TTBAlinEsqClick
              end
              object TTBAlinCen: TToolButton
                Left = 524
                Top = 0
                Hint = 'Centralizar texto'
                Caption = 'Center'
                ImageIndex = 12
                ParentShowHint = False
                ShowHint = True
                Style = tbsCheck
                OnClick = TTBAlinCenClick
              end
              object TTBAlinDir: TToolButton
                Left = 564
                Top = 0
                Hint = 'Alinhar '#224' direita'
                Caption = 'Align Right'
                ImageIndex = 13
                ParentShowHint = False
                ShowHint = True
                Style = tbsCheck
                OnClick = TTBAlinDirClick
              end
              object TTBAlinJus: TToolButton
                Left = 604
                Top = 0
                Hint = 'Justificar'
                Caption = 'Justify'
                ImageIndex = 25
                ParentShowHint = False
                ShowHint = True
                Style = tbsCheck
                OnClick = TTBAlinJusClick
              end
              object ToolButton14: TToolButton
                Left = 644
                Top = 0
                Width = 10
                Caption = 'ToolButton14'
                ImageIndex = 11
                Style = tbsSeparator
              end
              object ToolButton16: TToolButton
                Left = 654
                Top = 0
                Hint = 'Adicionar Link'
                Caption = 'Link'
                ImageIndex = 21
                ParentShowHint = False
                ShowHint = True
                OnClick = ToolButton16Click
              end
              object ToolButton6: TToolButton
                Left = 694
                Top = 0
                Hint = 'Inserir Imagem'
                Caption = 'Image'
                ImageIndex = 27
                ParentShowHint = False
                ShowHint = True
                OnClick = ToolButton6Click
              end
              object ToolButton7: TToolButton
                Left = 734
                Top = 0
                Hint = 'Inserir Tabela'
                Caption = 'Insert Table'
                ImageIndex = 28
                ParentShowHint = False
                ShowHint = True
                OnClick = ToolButton7Click
              end
              object ToolButton15: TToolButton
                Left = 774
                Top = 0
                Hint = 'Inserir linha'
                Caption = 'crDefault'
                ImageIndex = 20
                ParentShowHint = False
                ShowHint = True
                OnClick = ToolButton15Click
              end
            end
            object ToolBar2: TToolBar
              Left = 0
              Top = 45
              Width = 862
              Height = 29
              ButtonHeight = 19
              Caption = 'ToolBar2'
              TabOrder = 2
              object CBFontName: TdmkPopOutFntCBox
                Left = 0
                Top = 0
                Width = 200
                Height = 21
                OnChange = PFCpaLin3FonNomChange
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                MoveUsedToTop = True
                PopUpWidth = 0
                PopUpHeight = 75
                UpdType = utYes
              end
              object CBFontSize: TComboBox
                Left = 200
                Top = 0
                Width = 100
                Height = 19
                Style = csOwnerDrawFixed
                ItemHeight = 13
                TabOrder = 1
                OnChange = CBFontSizeChange
                Items.Strings = (
                  'Pequena'
                  'M'#233'dia'
                  'Normal'
                  'Maior'
                  'Grande'
                  'Extra Grande')
              end
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Texto plano'
            ImageIndex = 1
            object MeTextoPlano: TdmkMemo
              Left = 0
              Top = 0
              Width = 865
              Height = 291
              Align = alClient
              TabOrder = 0
              UpdType = utYes
            end
          end
          object TabSheet3: TTabSheet
            Caption = 'Anexos'
            ImageIndex = 2
            object Panel10: TPanel
              Left = 0
              Top = 0
              Width = 865
              Height = 48
              Align = alTop
              ParentBackground = False
              TabOrder = 0
              object BitBtn2: TBitBtn
                Tag = 10
                Left = 8
                Top = 4
                Width = 40
                Height = 40
                Cursor = crHandPoint
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BitBtn2Click
              end
              object BitBtn3: TBitBtn
                Tag = 12
                Left = 50
                Top = 4
                Width = 40
                Height = 40
                Cursor = crHandPoint
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BitBtn3Click
              end
              object BitBtn1: TBitBtn
                Tag = 25
                Left = 92
                Top = 4
                Width = 40
                Height = 40
                Cursor = crHandPoint
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BitBtn1Click
              end
            end
            object Grade: TStringGrid
              Left = 0
              Top = 48
              Width = 865
              Height = 243
              Align = alClient
              ColCount = 2
              DefaultRowHeight = 18
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColMoving]
              TabOrder = 1
              ColWidths = (
                13
                454)
              RowHeights = (
                18
                18)
            end
          end
          object TabSheet4: TTabSheet
            Caption = 'Codifica'#231#227'o de imagens'
            ImageIndex = 3
            object MeEncode: TMemo
              Left = 0
              Top = 0
              Width = 865
              Height = 291
              Align = alClient
              TabOrder = 0
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 532
    Width = 874
    Height = 60
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 870
      Height = 43
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 26
        Width = 870
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 592
    Width = 874
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 728
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = 'Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 726
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso3: TLabel
        Left = 138
        Top = 19
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso4: TLabel
        Left = 137
        Top = 18
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object BtEnviar: TBitBtn
        Tag = 244
        Left = 12
        Top = 3
        Width = 120
        Height = 41
        Caption = 'Enviar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtEnviarClick
      end
    end
  end
  object QrPreEmMsg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ELT(pem.Tipo+1, '
      #39'Plano'#39', '#39'HTML'#39', '#39'Imagem'#39') NOMETIPO, pem.* '
      'FROM preemmsg pem'
      'WHERE pem.Codigo=:P0'
      'AND pem.Tipo=:P1'
      'ORDER BY Ordem, Controle, Tipo')
    Left = 344
    Top = 512
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPreEmMsgNOMETIPO: TWideStringField
      FieldName = 'NOMETIPO'
      Size = 10
    end
    object QrPreEmMsgCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'preemmsg.Codigo'
    end
    object QrPreEmMsgControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'preemmsg.Controle'
    end
    object QrPreEmMsgOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'preemmsg.Ordem'
    end
    object QrPreEmMsgTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'preemmsg.Tipo'
    end
    object QrPreEmMsgTexto: TWideMemoField
      FieldName = 'Texto'
      Origin = 'preemmsg.Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPreEmMsgLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'preemmsg.Lk'
    end
    object QrPreEmMsgDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'preemmsg.DataCad'
    end
    object QrPreEmMsgDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'preemmsg.DataAlt'
    end
    object QrPreEmMsgUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'preemmsg.UserCad'
    end
    object QrPreEmMsgUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'preemmsg.UserAlt'
    end
    object QrPreEmMsgAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'preemmsg.AlterWeb'
    end
    object QrPreEmMsgAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'preemmsg.Ativo'
      MaxValue = 1
    end
    object QrPreEmMsgDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'preemmsg.Descricao'
      Size = 255
    end
    object QrPreEmMsgCidID_Img: TWideStringField
      FieldName = 'CidID_Img'
      Size = 32
    end
  end
  object IdSMTP1: TIdSMTP
    OnStatus = IdSMTP1Status
    IOHandler = IdSSL1
    OnDisconnected = IdSMTP1Disconnected
    OnWork = IdSMTP1Work
    OnWorkBegin = IdSMTP1WorkBegin
    OnWorkEnd = IdSMTP1WorkEnd
    OnConnected = IdSMTP1Connected
    OnFailedRecipient = IdSMTP1FailedRecipient
    SASLMechanisms = <>
    UseTLS = utUseRequireTLS
    OnTLSNotAvailable = IdSMTP1TLSNotAvailable
    Left = 324
    Top = 400
  end
  object IdSSL1: TIdSSLIOHandlerSocketOpenSSL
    Destination = ':25'
    MaxLineAction = maException
    Port = 25
    DefaultPort = 0
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 352
    Top = 400
  end
  object OpenDialog1: TOpenDialog
    Left = 440
    Top = 368
  end
  object QrPreEmail: TMySQLQuery
    Database = Dmod.MyDB
    Left = 316
    Top = 512
  end
  object QrEmailConta: TMySQLQuery
    Database = Dmod.MyDB
    Left = 287
    Top = 512
  end
  object DsEmailCfg: TDataSource
    DataSet = QrEmailCfg
    Left = 540
    Top = 472
  end
  object QrEmailCfg: TMySQLQuery
    Database = Dmod.MyDB
    Left = 511
    Top = 473
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object IMAP: TIdIMAP4
    SASLMechanisms = <>
    MilliSecsToWaitToClearBuffer = 10
    Left = 152
    Top = 408
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 248
    Top = 448
  end
end
