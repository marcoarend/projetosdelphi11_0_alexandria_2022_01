unit MailCfg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, Menus, dmkCheckBox, UnDmkEnums, Vcl.ComCtrls, dmkMemo,
  IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdIMAP4, IdSMTPBase, IdSMTP,
  UnProjGroup_Consts, Vcl.Grids, Vcl.DBGrids;

type
  TFmMailCfg = class(TForm)
    PnDados: TPanel;
    PnDmk: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrEmailConta: TmySQLQuery;
    QrEmailContaCodigo: TIntegerField;
    QrEmailContaNome: TWideStringField;
    DsEmailConta: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigoDmk: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirmaDmk: TBitBtn;
    Panel1: TPanel;
    BtDesisteDmk: TBitBtn;
    PnEmailPOP: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    BtConfirmaPOP: TBitBtn;
    Panel7: TPanel;
    BtDesistePOP: TBitBtn;
    PMInclui: TPopupMenu;
    ContaDermatek1: TMenuItem;
    ContaEmailPOP31: TMenuItem;
    Label5: TLabel;
    Label6: TLabel;
    EdUsuarioDmk: TdmkEdit;
    QrEmailContaTipoConta: TSmallintField;
    QrEmailContaSENHA: TWideStringField;
    QrEmailContaUsuario: TWideStringField;
    Label15: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    QrEmailContaSMTPServer: TWideStringField;
    QrEmailContaSend_Name: TWideStringField;
    QrEmailContaLogi_Auth: TSmallintField;
    QrEmailContaLogi_Name: TWideStringField;
    QrEmailContaPorta_Mail: TIntegerField;
    QrEmailContaLogi_SSL: TSmallintField;
    CkMostrarSenhaPOP: TdmkCheckBox;
    CkMostrarSenhaDmk: TdmkCheckBox;
    QrEmailContaLogi_SENHA: TWideStringField;
    EdSenhaDmk: TEdit;
    QrEmailContaPermisNiv: TIntegerField;
    QrEmailContaReceTipo: TIntegerField;
    QrEmailContaReceServer: TWideStringField;
    QrEmailContaRecePorta_Mail: TIntegerField;
    QrEmailContaReceLogi_SSL: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox4: TGroupBox;
    Label14: TLabel;
    Label16: TLabel;
    EdPorta_MailPOP: TdmkEdit;
    CkLogi_SSLPOP: TdmkCheckBox;
    GroupBox5: TGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    EdRecePorta_MailPOP: TdmkEdit;
    CkReceLogi_SSLPOP: TdmkCheckBox;
    EdReceServerPOP: TdmkEdit;
    Label17: TLabel;
    RGReceTipoPOP: TRadioGroup;
    CkLogi_AuthPOP: TdmkCheckBox;
    GroupBox3: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    EdLogi_NamePOP: TdmkEdit;
    EdLogi_PassPOP: TEdit;
    EdSMTPServerPOP: TdmkEdit;
    EdSend_NamePOP: TdmkEdit;
    EdUsuarioPOP: TdmkEdit;
    EdSenhaPOP: TEdit;
    Label10: TLabel;
    Label11: TLabel;
    Label9: TLabel;
    Label8: TLabel;
    RgTipoPOP: TdmkRadioGroup;
    EdCodigoPOP: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    EdNomePOP: TdmkEdit;
    Label20: TLabel;
    CBVerifiMail: TComboBox;
    Label21: TLabel;
    CBDownDias: TComboBox;
    MeAssinatura: TdmkMemo;
    CkUsarAssinatura: TdmkCheckBox;
    Label22: TLabel;
    QrEmailContaVerifiMail: TIntegerField;
    QrEmailContaDownDias: TIntegerField;
    QrEmailContaUsarAssinatura: TIntegerField;
    QrEmailContaAssinatura: TWideStringField;
    CkSincEmail: TCheckBox;
    QrEmailContaSincEmail: TIntegerField;
    BtTestar: TBitBtn;
    IMAP: TIdIMAP4;
    IdSSL1: TIdSSLIOHandlerSocketOpenSSL;
    IdSMTP1: TIdSMTP;
    LaEnvTLSTipo: TLabel;
    CBEnvTLSTipo: TComboBox;
    CBReceTLSTipo: TComboBox;
    LaReceTLSTipo: TLabel;
    QrEmailContaEnvTLSTipo: TIntegerField;
    QrEmailContaReceTLSTipo: TIntegerField;
    LaEnvTLSUse: TLabel;
    CBEnvTLSUse: TComboBox;
    LaReceTLSUse: TLabel;
    CBReceTLSUse: TComboBox;
    QrEmailContaReceTLSUse: TIntegerField;
    QrEmailContaEnvTLSUse: TIntegerField;
    N1: TMenuItem;
    N2: TMenuItem;
    TabSheet3: TTabSheet;
    DBGrid1: TDBGrid;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaDmkClick(Sender: TObject);
    procedure BtDesisteDmkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEmailContaAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEmailContaBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure ContaDermatek1Click(Sender: TObject);
    procedure ContaEmailPOP31Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtConfirmaPOPClick(Sender: TObject);
    procedure CkMostrarSenhaPOPClick(Sender: TObject);
    procedure CkMostrarSenhaDmkClick(Sender: TObject);
    procedure EdUsuarioPOPExit(Sender: TObject);
    procedure EdSenhaPOPExit(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure CkUsarAssinaturaClick(Sender: TObject);
    procedure BtTestarClick(Sender: TObject);
    procedure CkReceLogi_SSLPOPClick(Sender: TObject);
    procedure CkLogi_SSLPOPClick(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure QrEmailContaBeforeScroll(DataSet: TDataSet);
    procedure QrEmailContaBeforeClose(DataSet: TDataSet);
    procedure QrEmailContaAfterScroll(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(Mostra: Integer; Status: TSQLType; Codigo: Integer);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    function  InsereMailCfg(SQLType: TSQLType; var PermisNiv: Integer; Nome,
                SMTPServer, Send_Name, Usuario, Senha, Logi_Name,
                Logi_Pass, ReceServer: String; Logi_Auth, Porta_Mail, Logi_SSL,
                CodMailCfg, RecePorta_Mail, ReceLogi_SSL, ReceTipo, EnvTLSUse,
                EnvTLSTipo, ReceTLSUse, ReceTLSTipo, VerifiMail, DownDias,
                SincEmail, UsarAssinatura: Integer; Assinatura: String): Integer;
  end;

var
  FmMailCfg: TFmMailCfg;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnDmkWeb, MyDBCheck, UnMailAllOS, ModuleGeral,
  MyListas, UnMailEnv, UnGrl_Consts, UnGrl_Vars, UnGrl_Geral, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMailCfg.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMailCfg.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEmailContaCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmMailCfg.DefParams;
begin
  VAR_GOTOTABELA := 'emailconta';
  VAR_GOTOMYSQLTABLE := QrEmailConta;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *,');
  VAR_SQLx.Add('AES_DECRYPT(Password, "' + CO_RandStrWeb01 + '") SENHA,');
  VAR_SQLx.Add('AES_DECRYPT(Logi_Pass, "' + CO_RandStrWeb01 + '") Logi_SENHA');
  VAR_SQLx.Add('FROM emailconta');
  VAR_SQLx.Add('WHERE Codigo <> 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //                                                       
end;

procedure TFmMailCfg.CkUsarAssinaturaClick(Sender: TObject);
begin
  MeAssinatura.Enabled := CkUsarAssinatura.Checked;
end;

procedure TFmMailCfg.EdSenhaPOPExit(Sender: TObject);
begin
  if EdLogi_PassPOP.Text = '' then
    EdLogi_PassPOP.Text := EdSenhaPOP.Text;
end;

procedure TFmMailCfg.EdUsuarioPOPExit(Sender: TObject);
begin
  if EdLogi_NamePOP.ValueVariant = '' then
    EdLogi_NamePOP.ValueVariant := EdUsuarioPOP.ValueVariant;
end;

procedure TFmMailCfg.CkLogi_SSLPOPClick(Sender: TObject);
var
  Visi: Boolean;
begin
  Visi := CkLogi_SSLPOP.Checked = True;
  //
  LaEnvTLSUse.Visible := Visi;
  CBEnvTLSUse.Visible := Visi;
  //
  LaEnvTLSTipo.Visible := Visi;
  CBEnvTLSTipo.Visible := Visi;
end;

procedure TFmMailCfg.CkMostrarSenhaDmkClick(Sender: TObject);
begin
  if CkMostrarSenhaDmk.Checked then
  begin
    EdSenhaDmk.PasswordChar := #0;
    EdSenhaDmk.Font.Charset := DEFAULT_CHARSET;
  end else
  begin
    EdSenhaDmk.PasswordChar := '|';
    EdSenhaDmk.Font.Charset := SYMBOL_CHARSET;
  end;
end;

procedure TFmMailCfg.CkMostrarSenhaPOPClick(Sender: TObject);
begin
  if CkMostrarSenhaPOP.Checked then
  begin
    if QrEmailContaCodigo.Value < 1 then
    begin
      Geral.MB_Aviso('Visualiza��o n�o permitida para este cadastro!');
      CkMostrarSenhaPOP.Checked := False;
    end else
    begin
      EdSenhaPOP.PasswordChar     := #0;
      EdSenhaPOP.Font.Charset     := DEFAULT_CHARSET;
      EdLogi_PassPOP.PasswordChar := #0;
      EdLogi_PassPOP.Font.Charset := DEFAULT_CHARSET;
    end;
  end else
  begin
    EdSenhaPOP.PasswordChar     := '|';
    EdSenhaPOP.Font.Charset     := SYMBOL_CHARSET;
    EdLogi_PassPOP.PasswordChar := '|';
    EdLogi_PassPOP.Font.Charset := SYMBOL_CHARSET;
  end;
end;

procedure TFmMailCfg.CkReceLogi_SSLPOPClick(Sender: TObject);
var
  Visi: Boolean;
begin
  Visi := CkReceLogi_SSLPOP.Checked = True;
  //
  LaReceTLSUse.Visible := Visi;
  CBReceTLSUse.Visible := Visi;
  //
  LaReceTLSTipo.Visible := Visi;
  CBReceTLSTipo.Visible := Visi;
end;

procedure TFmMailCfg.ContaDermatek1Click(Sender: TObject);
begin
  //Dermatek
  MostraEdicao(0, stIns, 0);
end;

procedure TFmMailCfg.ContaEmailPOP31Click(Sender: TObject);
begin
  //E-mail POP3
  MostraEdicao(1, stIns, 0);
end;

procedure TFmMailCfg.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmMailCfg.QueryPrincipalAfterOpen;
begin
end;

procedure TFmMailCfg.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmMailCfg.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMailCfg.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMailCfg.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMailCfg.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMailCfg.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMailCfg.BtTestarClick(Sender: TObject);
var
  Host, Username, Password, SevidorImap: String;
  Port, Logi_SSL, EnvTLSUse, EnvTLSTipo, ReceTLSUse, ReceTLSTipo: Integer;
begin
  //Verifica SMTP
  //
  Host       := EdSMTPServerPOP.ValueVariant;
  Username   := EdUsuarioPOP.ValueVariant;
  Password   := EdSenhaPOP.Text;
  Port       := EdPorta_MailPOP.ValueVariant;
  Logi_SSL   := Geral.BoolToInt(CkLogi_SSLPOP.Checked);
  EnvTLSUse  := CBEnvTLSUse.ItemIndex;
  EnvTLSTipo := CBEnvTLSTipo.ItemIndex;
  //
  if (Host = '') or (Username = '') or (Password = '') then
  begin
    Geral.MB_Info('Os dados de autentica��o est�o incompletos!' + sLineBreak +
      'Verifique se os campos: Host, Usu�rio e Senha est�o preenchidos!');
    Exit;
  end;
  //
  UMailAllOS.TestarConexaoSMTP(IdSMTP1, IdSSL1, Host, Username, Password, Port,
    Logi_SSL, EnvTLSUse, EnvTLSTipo, True);
  //
  if RGReceTipoPOP.ItemIndex = 1 then //IMAP
  begin
    //Verifica IMAP
    SevidorImap := EdReceServerPOP.ValueVariant;
    //
    if SevidorImap <> '' then
    begin
      Port        := EdRecePorta_MailPOP.ValueVariant;
      Logi_SSL    := Geral.BoolToInt(CkReceLogi_SSLPOP.Checked);
      ReceTLSUse  := CBReceTLSUse.ItemIndex;
      ReceTLSTipo := CBReceTLSTipo.ItemIndex;
      //
      UMailAllOS.ConectaIMAP(IMAP, IdSSL1, SevidorImap, Username, Password,
        Port, Logi_SSL, ReceTLSUse, ReceTLSTipo, True, True);
    end;
  end;
end;

function TFmMailCfg.InsereMailCfg(SQLType: TSQLType; var PermisNiv: Integer;
  Nome, SMTPServer, Send_Name, Usuario, Senha, Logi_Name, Logi_Pass,
  ReceServer: String; Logi_Auth, Porta_Mail, Logi_SSL, CodMailCfg,
  RecePorta_Mail, ReceLogi_SSL, ReceTipo, EnvTLSUse, EnvTLSTipo, ReceTLSUse,
  ReceTLSTipo, VerifiMail, DownDias, SincEmail, UsarAssinatura: Integer;
  Assinatura: String): Integer;
const
  TipoConta = 1; //E-mail
var
  Codigo: Integer;
begin
  if PermisNiv = 1 then
    PermisNiv := VAR_USUARIO;
  //
  if SQLType = stIns then
    Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpd, 'emailconta', 'Codigo',
                [], [], stIns, 0, siPositivo, nil)
  else
    Codigo := CodMailCfg;
  //
  Dmod.QrUpd.SQL.Clear;
  if SQLType = stIns then
    Dmod.QrUpd.SQL.Add('INSERT INTO emailconta SET ')
  else
    Dmod.QrUpd.SQL.Add('UPDATE emailconta SET ');
  //
  Dmod.QrUpd.SQL.Add('Nome=:P0, TipoConta=:P1, PermisNiv=:P2, ');
  Dmod.QrUpd.SQL.Add('Password=AES_ENCRYPT(:P3, :P4), Usuario=:P5, ');
  Dmod.QrUpd.SQL.Add('SMTPServer=:P6, Send_Name=:P7, Logi_Auth=:P8, ');
  Dmod.QrUpd.SQL.Add('Logi_Name=:P9, Logi_SSL=:P10, Porta_Mail=:P11, ');
  Dmod.QrUpd.SQL.Add('Logi_Pass=AES_ENCRYPT(:P12, :P13), ReceServer=:P14, ');
  Dmod.QrUpd.SQL.Add('RecePorta_Mail=:P15, ReceLogi_SSL=:P16, ReceTipo=:P17, ');
  Dmod.QrUpd.SQL.Add('EnvTLSUse=:P18, EnvTLSTipo=:P19, ReceTLSUse=:P20, ');
  Dmod.QrUpd.SQL.Add('ReceTLSTipo=:P21, VerifiMail=:P22, DownDias=:P23, ');
  Dmod.QrUpd.SQL.Add('UsarAssinatura=:P24, Assinatura=:P25, SincEmail=:P26, ');
  //
  if SQLType = stIns then
    Dmod.QrUpd.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else
    Dmod.QrUpd.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  //
  Dmod.QrUpd.Params[00].AsString  := Nome;
  Dmod.QrUpd.Params[01].AsInteger := TipoConta;
  Dmod.QrUpd.Params[02].AsInteger := PermisNiv;
  Dmod.QrUpd.Params[03].AsString  := Senha;
  Dmod.QrUpd.Params[04].AsString  := CO_RandStrWeb01;
  Dmod.QrUpd.Params[05].AsString  := Usuario;
  Dmod.QrUpd.Params[06].AsString  := SMTPServer;
  Dmod.QrUpd.Params[07].AsString  := Send_Name;
  Dmod.QrUpd.Params[08].AsInteger := Logi_Auth;
  Dmod.QrUpd.Params[09].AsString  := Logi_Name;
  Dmod.QrUpd.Params[10].AsInteger := Logi_SSL;
  Dmod.QrUpd.Params[11].AsInteger := Porta_Mail;
  Dmod.QrUpd.Params[12].AsString  := Logi_Pass;
  Dmod.QrUpd.Params[13].AsString  := CO_RandStrWeb01;
  Dmod.QrUpd.Params[14].AsString  := ReceServer;
  Dmod.QrUpd.Params[15].AsInteger := RecePorta_Mail;
  Dmod.QrUpd.Params[16].AsInteger := ReceLogi_SSL;
  Dmod.QrUpd.Params[17].AsInteger := ReceTipo;
  //
  Dmod.QrUpd.Params[18].AsInteger := EnvTLSUse;
  Dmod.QrUpd.Params[19].AsInteger := EnvTLSTipo;
  Dmod.QrUpd.Params[20].AsInteger := ReceTLSUse;
  Dmod.QrUpd.Params[21].AsInteger := ReceTLSTipo;
  //
  Dmod.QrUpd.Params[22].AsInteger := VerifiMail;
  Dmod.QrUpd.Params[23].AsInteger := DownDias;
  Dmod.QrUpd.Params[24].AsInteger := UsarAssinatura;
  Dmod.QrUpd.Params[25].AsString  := Assinatura;
  Dmod.QrUpd.Params[26].AsInteger := SincEmail;
  //
  Dmod.QrUpd.Params[27].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpd.Params[28].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[29].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  Result := Codigo;
end;

procedure TFmMailCfg.BtConfirmaPOPClick(Sender: TObject);
const
  TipoConta = 1; //E-mail
var
  Codigo, PermisNiv, Logi_Auth, Porta_Mail, Logi_SSL, RecePorta_Mail,
  ReceLogi_SSL, ReceTipo, VerifiMail, DownDias, UsarAssinatura, SincEmail,
  EnvTLSUse, EnvTLSTipo, ReceTLSUse,  ReceTLSTipo: Integer;
  Nome, SMTPServer, Send_Name, Usuario, Senha, Logi_Name, Logi_Pass,
  ReceServer, Assinatura: String;
begin
  PermisNiv      := RgTipoPOP.ItemIndex;
  Nome           := EdNomePOP.ValueVariant;
  SMTPServer     := EdSMTPServerPOP.ValueVariant;
  Send_Name      := EdSend_NamePOP.ValueVariant;
  Usuario        := EdUsuarioPOP.ValueVariant;
  Senha          := EdSenhaPOP.Text;
  Logi_Auth      := Geral.BoolToInt(CkLogi_AuthPOP.Checked);
  Logi_Name      := EdLogi_NamePOP.ValueVariant;
  Logi_Pass      := EdLogi_PassPOP.Text;
  Porta_Mail     := EdPorta_MailPOP.ValueVariant;
  Logi_SSL       := Geral.BoolToInt(CkLogi_SSLPOP.Checked);
  ReceServer     := EdReceServerPOP.ValueVariant;
  RecePorta_Mail := EdRecePorta_MailPOP.ValueVariant;
  ReceLogi_SSL   := Geral.BoolToInt(CkReceLogi_SSLPOP.Checked);
  ReceTipo       := RGReceTipoPOP.ItemIndex;
  //
  if Logi_SSL = 1 then
  begin
    EnvTLSUse  := CBEnvTLSUse.ItemIndex;
    EnvTLSTipo := CBEnvTLSTipo.ItemIndex;
  end else
  begin
    EnvTLSUse  := -1;
    EnvTLSTipo := -1;
  end;
  //
  if ReceLogi_SSL = 1 then
  begin
    ReceTLSUse  := CBReceTLSUse.ItemIndex;
    ReceTLSTipo := CBReceTLSTipo.ItemIndex;
  end else
  begin
    ReceTLSUse  := -1;
    ReceTLSTipo := -1;
  end;
  //
  VerifiMail     := UMailEnv.ConfiguraVerifiMailValorToDias(CBVerifiMail.ItemIndex);
  DownDias       := UMailEnv.ConfiguraDownDiasValorToDias(CBDownDias.ItemIndex);
  UsarAssinatura := Geral.BoolToInt(CkUsarAssinatura.Checked);
  SincEmail      := Geral.BoolToInt(CkSincEmail.Checked);
  if UsarAssinatura = 1 then
    Assinatura := MeAssinatura.Text
  else
    Assinatura := '';
  //
  PageControl1.ActivePageIndex := 0;
  //
  if MyObjects.FIC(Nome = '', EdNomePOP, 'Descri��o n�o definida!') then Exit;
  if MyObjects.FIC(Usuario = '', EdUsuarioPOP, 'Usu�rio n�o definido!') then Exit;
  if MyObjects.FIC(Senha   = '', EdSenhaPOP, 'Senha n�o definida!') then Exit;
  //
  Codigo := InsereMailCfg(ImgTipo.SQLType, PermisNiv, Nome, SMTPServer,
              Send_Name, Usuario, Senha, Logi_Name, Logi_Pass, ReceServer,
              Logi_Auth, Porta_Mail, Logi_SSL, QrEmailContaCodigo.Value,
              RecePorta_Mail, ReceLogi_SSL, ReceTipo, EnvTLSUse, EnvTLSTipo,
              ReceTLSUse, ReceTLSTipo, VerifiMail, DownDias, SincEmail,
              UsarAssinatura, Assinatura);
  //
  LocCod(Codigo, Codigo);
  MostraEdicao(-1, stLok, 0);
end;

procedure TFmMailCfg.BtAlteraClick(Sender: TObject);
begin
  if (QrEmailConta.State = dsInactive) or (QrEmailConta.RecordCount = 0) then Exit;
  //  
  case QrEmailContaTipoConta.Value of
    0: //Login Dermatek
      MostraEdicao(0, stUpd, QrEmailContaCodigo.Value);
    1: //E-mail
      MostraEdicao(1, stUpd, QrEmailContaCodigo.Value);
  end;
end;

procedure TFmMailCfg.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEmailContaCodigo.Value;
  Close;
end;

procedure TFmMailCfg.BtConfirmaDmkClick(Sender: TObject);
{$IfDef UsaWSuport}
var
  Codigo: Integer;
  Usuario, Senha, MsgLogin: String;
{$EndIf}
begin
  {$IfDef UsaWSuport}
  Usuario := EdUsuarioDmk.ValueVariant;
  Senha   := EdSenhaDmk.Text;
  //
  if MyObjects.FIC(Usuario = '', EdUsuarioDmk, 'Usu�rio n�o definido!') then Exit;
  if MyObjects.FIC(Senha   = '', EdSenhaDmk, 'Senha n�o definida!') then Exit;
  //
  MsgLogin := DmkWeb.LoginUser(Usuario, Senha);
  //
  if VAR_WEB_CONECTADO <> 100 then
  begin
    Geral.MB_Aviso(MsgLogin);
    EdUsuarioDmk.SetFocus;
    Exit;
  end;
  Codigo := DmkWeb.InsereContaDmk(ImgTipo.SQLType, Dmod.QrUpd, Dmod.QrAux,
              Dmod.MyDB, Usuario, Senha, VAR_USUARIO, QrEmailContaCodigo.Value);
  //
  LocCod(Codigo, Codigo);
  MostraEdicao(-1, stLok, 0);
  {$EndIf}
end;

procedure TFmMailCfg.BtDesisteDmkClick(Sender: TObject);
begin
  MostraEdicao(-1, stLok, 0);
end;

procedure TFmMailCfg.BtExcluiClick(Sender: TObject);
var
  Continua: Boolean;
  Codigo, PermisNiv, TipoConta: Integer;
begin
  Codigo    := QrEmailContaCodigo.Value;
  PermisNiv := QrEmailContaPermisNiv.Value;
  TipoConta := QrEmailContaTipoConta.Value;
  //
  if (TipoConta = 0) and (PermisNiv = VAR_USUARIO) then
    Continua := True
  else
    Continua := DBCheck.LiberaPelaSenhaBoss;
  if Continua then
  begin
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da conta atual?',
      'emailconta', 'Codigo', Codigo, Dmod.MyDB) = ID_YES
    then
      Va(vpLast);
  end;
end;

procedure TFmMailCfg.MostraEdicao(Mostra: Integer; Status: TSQLType; Codigo: Integer);
  function PermiteEdicao(var Msg: String): Boolean;
  begin
    if (QrEmailContaPermisNiv.Value <> 0) and
      (QrEmailContaPermisNiv.Value <> VAR_USUARIO) then
    begin
      Msg    := 'Voc� pode editar somente contas sem restri��es ou que voc� seja o propriet�rio!';
      Result := False;
    end else
      Result := True;
  end;
var
  Msg: String;
  MostraOpcGer: Boolean;
begin
  case Mostra of
    0: //Login Dermatek
    begin
      if Status = stIns then
      begin
        EdCodigoDmk.ValueVariant  := FormatFloat('000', Codigo);
        EdUsuarioDmk.ValueVariant := '';
        EdSenhaDmk.Text           := '';
      end else
      begin
        if not PermiteEdicao(Msg) then
        begin
          Geral.MB_Aviso(Msg);
          Exit;
        end;
        EdCodigoDmk.ValueVariant  := QrEmailContaCodigo.Value;
        EdUsuarioDmk.ValueVariant := QrEmailContaUsuario.Value;
        EdSenhaDmk.Text           := QrEmailContaSENHA.Value;
      end;
      PnDados.Visible    := False;
      PnEmailPOP.Visible := False;
      PnDmk.Visible      := True;
      //
      EdUsuarioDmk.SetFocus;
    end;
    1: //E-mail
    begin
      PageControl1.ActivePageIndex := 0;
      TabSheet2.TabVisible         := Grl_Geral.LiberaModulo(CO_DMKID_APP,
                                        Grl_Geral.ObtemSiglaModulo(mdlappEmail),
                                        DModG.QrMasterHabilModulos.Value);
      if Status = stIns then
      begin
        EdCodigoPOP.ValueVariant         := FormatFloat('000', Codigo);
        EdNomePOP.ValueVariant           := 'Conta e-mail POP';
        EdSMTPServerPOP.ValueVariant     := '';
        EdSend_NamePOP.ValueVariant      := '';
        EdUsuarioPOP.ValueVariant        := '';
        EdSenhaPOP.Text                  := '';
        CkLogi_AuthPOP.Checked           := False;
        EdLogi_NamePOP.ValueVariant      := '';
        EdLogi_PassPOP.Text              := '';
        EdPorta_MailPOP.ValueVariant     := 587;
        CkLogi_SSLPOP.Checked            := False;
        RGReceTipoPOP.ItemIndex          := 0;
        EdReceServerPOP.ValueVariant     := '';
        EdRecePorta_MailPOP.ValueVariant := 110;
        CkReceLogi_SSLPOP.Checked        := False;
        CBEnvTLSUse.ItemIndex            := -1;
        CBEnvTLSTipo.ItemIndex           := -1;
        CBReceTLSUse.ItemIndex           := -1;
        CBReceTLSTipo.ItemIndex          := -1;
        //
        LaEnvTLSUse.Visible   := False;
        CBEnvTLSUse.Visible   := False;
        LaEnvTLSTipo.Visible  := False;
        CBEnvTLSTipo.Visible  := False;
        LaReceTLSUse.Visible  := False;
        CBReceTLSUse.Visible  := False;
        LaReceTLSTipo.Visible := False;
        CBReceTLSTipo.Visible := False;
        //
        //Gerenciamento
        CkSincEmail.Checked      := False;
        CBVerifiMail.ItemIndex   := 2; //A cada 30 minutos
        CBDownDias.ItemIndex     := 2; //�ltimos 7 dias
        CkUsarAssinatura.Checked := True;
        MeAssinatura.Text        := 'Enviado do E-mail Dermatek';
        MeAssinatura.Enabled     := True;
      end else
      begin
        if not PermiteEdicao(Msg) then
        begin
          Geral.MB_Aviso(Msg);
          Exit;
        end;
        EdCodigoPOP.ValueVariant         := QrEmailContaCodigo.Value;
        EdNomePOP.ValueVariant           := QrEmailContaNome.Value;
        EdSMTPServerPOP.ValueVariant     := QrEmailContaSMTPServer.Value;
        EdSend_NamePOP.ValueVariant      := QrEmailContaSend_Name.Value;
        EdUsuarioPOP.ValueVariant        := QrEmailContaUsuario.Value;
        EdSenhaPOP.Text                  := QrEmailContaSENHA.Value;
        CkLogi_AuthPOP.Checked           := Geral.IntToBool(QrEmailContaLogi_Auth.Value);
        EdLogi_NamePOP.ValueVariant      := QrEmailContaLogi_Name.Value;
        EdLogi_PassPOP.Text              := QrEmailContaLogi_SENHA.Value;
        EdPorta_MailPOP.ValueVariant     := QrEmailContaPorta_Mail.Value;
        CkLogi_SSLPOP.Checked            := Geral.IntToBool(QrEmailContaLogi_SSL.Value);
        RGReceTipoPOP.ItemIndex          := QrEmailContaReceTipo.Value;
        EdReceServerPOP.ValueVariant     := QrEmailContaReceServer.Value;
        EdRecePorta_MailPOP.ValueVariant := QrEmailContaRecePorta_Mail.Value;
        CkReceLogi_SSLPOP.Checked        := Geral.IntToBool(QrEmailContaReceLogi_SSL.Value);
        CBEnvTLSUse.ItemIndex            := QrEmailContaEnvTLSUse.Value;
        CBEnvTLSTipo.ItemIndex           := QrEmailContaEnvTLSTipo.Value;
        CBReceTLSUse.ItemIndex           := QrEmailContaReceTLSUse.Value;
        CBReceTLSTipo.ItemIndex          := QrEmailContaReceTLSTipo.Value;
        //
        LaEnvTLSUse.Visible   := CkLogi_SSLPOP.Checked = True;
        CBEnvTLSUse.Visible   := CkLogi_SSLPOP.Checked = True;
        LaEnvTLSTipo.Visible  := CkLogi_SSLPOP.Checked = True;
        CBEnvTLSTipo.Visible  := CkLogi_SSLPOP.Checked = True;
        LaReceTLSUse.Visible  := CkReceLogi_SSLPOP.Checked = True;
        CBReceTLSUse.Visible  := CkReceLogi_SSLPOP.Checked = True;
        LaReceTLSTipo.Visible := CkReceLogi_SSLPOP.Checked = True;
        CBReceTLSTipo.Visible := CkReceLogi_SSLPOP.Checked = True;
        //
        //Gerenciamento
        CkSincEmail.Checked      := Geral.IntToBool(QrEmailContaSincEmail.Value);
        CBVerifiMail.ItemIndex   := UMailEnv.DiasToConfiguraVerifiMailValor(QrEmailContaVerifiMail.Value);
        CBDownDias.ItemIndex     := UMailEnv.ConfiguraDiasToDownDiasValor(QrEmailContaDownDias.Value);
        CkUsarAssinatura.Checked := Geral.IntToBool(QrEmailContaUsarAssinatura.Value);
        MeAssinatura.Text        := QrEmailContaAssinatura.Value;
        MeAssinatura.Enabled     := Geral.IntToBool(QrEmailContaUsarAssinatura.Value);
      end;
      PnDados.Visible    := False;
      PnEmailPOP.Visible := True;
      PnDmk.Visible      := False;
      //
      RgTipoPOP.SetFocus;
    end;
    else
    begin
      PnDados.Visible    := True;
      PnEmailPOP.Visible := False;
      PnDmk.Visible      := False;
    end;
  end;
  ImgTipo.SQLType := Status;
  GOTOy.BotoesSb(ImgTipo.SQLType); 
end;

procedure TFmMailCfg.N2Click(Sender: TObject);
var
  Cria: Boolean;
var
  Nome, Usuario, Password, SMTPServer, Send_Name, Logi_Name, Logi_Pass,
  ReceServer, DataHoraSinc, Assinatura, MailBoxSeparator: String;
  Codigo, TipoConta, PermisNiv, Logi_Auth, Porta_Mail, Logi_SSL, ReceTipo,
  RecePorta_Mail, ReceLogi_SSL, SincEmail, VerifiMail, DownDias, UsarAssinatura,
  ErrSinc, MaxHistorico, ReceTLSTipo, EnvTLSTipo, ReceTLSUse, EnvTLSUse: Integer;
  SQLType: TSQLType;
begin
  Cria := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM emailconta ',
  'WHERE Codigo=-1 ',
  '']);
  //
  if Dmod.QrAux.RecordCount > 0 then
  begin
    if Geral.MB_Pergunta('Cadastro -1 j� existe! Deseja recri�-lo?') = ID_YES then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'DELETE FROM emailconta ',
      'WHERE Codigo=-1 ',
      '']);
      //
      Cria := True;
    end else
      LocCod(-1, -1);
  end else Cria := True;
  //
  if Cria then
  begin
    SQLType        := stIns;
    Codigo         := -1;
    Nome           := 'Envio de NFe';
    TipoConta      := 1;
    PermisNiv      := 0;
    Usuario        := 'nf@dermatek.com.br';
    // N�o mexer caso a senha n�o mude no CPanel!!!!
    Password       := '�i#<�T�0�D�ޱ�'; // '�i#<�T�0�D�ޱ�'  lQ!8S6lSHbBc
    SMTPServer     := 'mail.dermatek.com.br';
    Send_Name      := 'NFe ';
    Logi_Auth      := 0;
    Logi_Name      := 'nf@dermatek.com.br';
    // N�o mexer caso a senha n�o mude no CPanel!!!!
    Logi_Pass      := '�i#<�T�0�D�ޱ�';  // '�i#<�T�0�D�ޱ�'  lQ!8S6lSHbBc
    Porta_Mail     := 587;
    Logi_SSL       := 1;
    ReceTipo       := 0;
    ReceServer     := 'mail.dermatek.com.br';
    RecePorta_Mail := 143;
    ReceLogi_SSL   := 0;
    SincEmail      := 0;
    VerifiMail     := 30;
    DownDias       := 14;
    DataHoraSinc   := '0000-00-00 00:00:00';
    UsarAssinatura := 1;
    Assinatura     := 'Enviado do E-mail Dermatek';
    ErrSinc        := 0;
    MaxHistorico   := 110;
    MailBoxSeparator:= '';
    ReceTLSTipo    := -1;
    EnvTLSTipo     := 5;
    ReceTLSUse     := -1;
    EnvTLSUse      := 3;

    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'emailconta', False, [
    'Nome', 'TipoConta', 'PermisNiv',
    'Usuario', 'Password', 'SMTPServer',
    'Send_Name', 'Logi_Auth', 'Logi_Name',
    'Logi_Pass', 'Porta_Mail', 'Logi_SSL',
    'ReceTipo', 'ReceServer', 'RecePorta_Mail',
    'ReceLogi_SSL', 'SincEmail', 'VerifiMail',
    'DownDias', 'DataHoraSinc', 'UsarAssinatura',
    'Assinatura', 'ErrSinc', 'MaxHistorico',
    'MailBoxSeparator', 'ReceTLSTipo', 'EnvTLSTipo',
    'ReceTLSUse', 'EnvTLSUse'], [
    'Codigo'], [
    Nome, TipoConta, PermisNiv,
    Usuario, Password, SMTPServer,
    Send_Name, Logi_Auth, Logi_Name,
    Logi_Pass, Porta_Mail, Logi_SSL,
    ReceTipo, ReceServer, RecePorta_Mail,
    ReceLogi_SSL, SincEmail, VerifiMail,
    DownDias, DataHoraSinc, UsarAssinatura,
    Assinatura, ErrSinc, MaxHistorico,
    MailBoxSeparator, ReceTLSTipo, EnvTLSTipo,
    ReceTLSUse, EnvTLSUse], [
    Codigo], True) then
    begin
      LocCod(-1, -1);
    end;
  end;
end;

procedure TFmMailCfg.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmMailCfg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType           := stLok;
  GBEdita.Align             := alClient;
  GBDados.Align             := alClient;
  CkMostrarSenhaPOP.Checked := False;
  CkMostrarSenhaDmk.Checked := False;
  //
  CBVerifiMail.Items := UMailEnv.ConfiguraVerifiMail;
  CBDownDias.Items   := UMailEnv.ConfiguraDownDias;
  //
  CBEnvTLSUse.Items.Clear;
  CBEnvTLSUse.Items.AddStrings(UMailAllOS.ObtemListaTLS_Use());
  //
  CBEnvTLSTipo.Items.Clear;
  CBEnvTLSTipo.Items.AddStrings(UMailAllOS.ObtemListaTLS_Type());
  //
  CBReceTLSUse.Items.Clear;
  CBReceTLSUse.Items.AddStrings(UMailAllOS.ObtemListaTLS_Use());
  //
  CBReceTLSTipo.Items.Clear;
  CBReceTLSTipo.Items.AddStrings(UMailAllOS.ObtemListaTLS_Type());
  //
  CriaOForm;
end;

procedure TFmMailCfg.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEmailContaCodigo.Value, LaRegistro.Caption);
end;

procedure TFmMailCfg.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Info('Impress�o indispon�vel para esta janela!');
end;

procedure TFmMailCfg.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmMailCfg.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Info('Tipo de pesquisa indispon�vel para esta janela!');
end;

procedure TFmMailCfg.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmMailCfg.QrEmailContaAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmMailCfg.QrEmailContaAfterScroll(DataSet: TDataSet);
begin
  CkMostrarSenhaPOP.Checked := False;
end;

procedure TFmMailCfg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  EdUsuarioPOP.CharCase   := ecNormal;
  EdSenhaPOP.CharCase     := ecNormal;
  EdLogi_NamePOP.CharCase := ecNormal;
  EdLogi_PassPOP.CharCase := ecNormal;
  EdUsuarioDmk.CharCase   := ecNormal;
  EdSenhaDmk.CharCase     := ecNormal;
end;

procedure TFmMailCfg.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEmailContaCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'emailconta', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmMailCfg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMailCfg.QrEmailContaBeforeClose(DataSet: TDataSet);
begin
  CkMostrarSenhaPOP.Checked := False;
end;

procedure TFmMailCfg.QrEmailContaBeforeOpen(DataSet: TDataSet);
begin
  QrEmailContaCodigo.DisplayFormat := FFormatFloat;
  CkMostrarSenhaPOP.Checked := False;
end;

procedure TFmMailCfg.QrEmailContaBeforeScroll(DataSet: TDataSet);
begin
  CkMostrarSenhaPOP.Checked := False;
end;

end.

