unit CEST;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums, dmkMemo;

type
  TFmCEST = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCEST: TMySQLQuery;
    DsCEST: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    BtImporta: TBitBtn;
    QrCESTCodigo: TIntegerField;
    QrCESTCEST: TWideStringField;
    QrCESTNCM: TWideStringField;
    QrCESTNome: TWideStringField;
    QrCESTSegmentoCEST: TWideStringField;
    QrCESTItemCEST: TWideStringField;
    QrCESTAnexoXXVII: TWideStringField;
    QrCESTLk: TIntegerField;
    QrCESTDataCad: TDateField;
    QrCESTDataAlt: TDateField;
    QrCESTUserCad: TIntegerField;
    QrCESTUserAlt: TIntegerField;
    QrCESTAlterWeb: TSmallintField;
    QrCESTAWServerID: TIntegerField;
    QrCESTAWStatSinc: TSmallintField;
    QrCESTAtivo: TSmallintField;
    DBMemo1: TDBMemo;
    dmkDBEdit1: TdmkDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    Label8: TLabel;
    DBEdit3: TDBEdit;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    MeNome: TdmkMemo;
    EdCEST: TdmkEdit;
    EdNCM: TdmkEdit;
    EdSegmentoCEST: TdmkEdit;
    EdItemCEST: TdmkEdit;
    EdAnexoXXVII: TdmkEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCESTAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCESTBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCEST: TFmCEST;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, LoadCSV_CEST, MyDBCheck;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCEST.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCEST.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCESTCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCEST.DefParams;
begin
  VAR_GOTOTABELA := 'cest';
  VAR_GOTOMYSQLTABLE := QrCEST;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM cest');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCEST.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCEST.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCEST.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCEST.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCEST.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCEST.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCEST.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCEST.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCEST.BtAlteraClick(Sender: TObject);
begin
  if (QrCEST.State <> dsInactive) and (QrCEST.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCEST, [PnDados],
      [PnEdita], EdNCM, ImgTipo, 'cest');
    EdCEST.Enabled := False;
  end;
end;

procedure TFmCEST.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCESTCodigo.Value;
  Close;
end;

procedure TFmCEST.BtConfirmaClick(Sender: TObject);
var
  CEST, NCM, Nome, SegmentoCEST, ItemCEST, AnexoXXVII: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  //
  Codigo         := EdCodigo.ValueVariant;
  CEST           := Geral.SoNumero_TT(EdCEST.Text);
  NCM            := EdNCM.ValueVariant;
  Nome           := MeNome.Text;
  SegmentoCEST   := EdSegmentoCEST.ValueVariant;
  ItemCEST       := EdItemCEST.ValueVariant;
  AnexoXXVII     := EdAnexoXXVII.ValueVariant;
  //
  if MyObjects.FIC(Length(CEST) = 0, EdCEST, 'Defina o CEST!') then Exit;
  if MyObjects.FIC(Length(Nome) = 0, MeNome, 'Defina uma descri��o!') then Exit;
  //
  if SQLType = stIns then
    Codigo := Geral.IMV(CEST);
  //ou > ? := UMyMod.BPGS1I32('cest', 'Codigo', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cest', False, [
  'CEST', 'NCM', 'Nome',
  'SegmentoCEST', 'ItemCEST', 'AnexoXXVII'], [
  'Codigo'], [
  CEST, NCM, Nome,
  SegmentoCEST, ItemCEST, AnexoXXVII], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCEST.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cest', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCEST.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrCESTCodigo.Value;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do CEST selecionado?',
  'cest', 'Codigo', Codigo, Dmod.MyDB) = ID_YES then
  begin
    Va(vpNext);
    LocCod(Codigo, QrCESTCodigo.Value);
  end;
end;

procedure TFmCEST.BtImportaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmLoadCSV_CEST, FmLoadCSV_CEST, afmoNegarComAviso) then
  begin
    FmLoadCSV_CEST.ShowModal;
    FmLoadCSV_CEST.Destroy;
  end;
end;

procedure TFmCEST.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCEST, [PnDados],
  [PnEdita], EdCEST, ImgTipo, 'cest');
  //
  EdCEST.Enabled := True;
end;

procedure TFmCEST.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmCEST.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCESTCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCEST.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCEST.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCESTCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCEST.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCEST.QrCESTAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtExclui.Enabled := QrCEST.RecordCount > 0;
end;

procedure TFmCEST.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCEST.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCESTCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cest', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCEST.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCEST.QrCESTBeforeOpen(DataSet: TDataSet);
begin
  QrCESTCodigo.DisplayFormat := FFormatFloat;
end;

end.

