object FmLoadCSV_CEST: TFmLoadCSV_CEST
  Left = 339
  Top = 185
  Caption = 'FIS-CEST_-003 :: Load CEST de Arquivo CSV'
  ClientHeight = 629
  ClientWidth = 1073
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1073
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1025
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 977
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 339
        Height = 32
        Caption = 'Load CEST de Arquivo CSV'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 339
        Height = 32
        Caption = 'Load CEST de Arquivo CSV'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 339
        Height = 32
        Caption = 'Load CEST de Arquivo CSV'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 504
    Width = 1073
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1069
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 1069
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1073
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 927
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 925
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 150
        Height = 40
        Caption = '&1. Carrega Xls'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtInsereB: TBitBtn
        Tag = 14
        Left = 160
        Top = 4
        Width = 150
        Height = 40
        Caption = '&2. Insere no BD'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtInsereBClick
      end
      object BtAtrela: TBitBtn
        Tag = 14
        Left = 464
        Top = 4
        Width = 150
        Height = 40
        Caption = '&4. Atrela Tabs'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtAtrelaClick
      end
      object BtVerifCadastr: TBitBtn
        Tag = 14
        Left = 616
        Top = 4
        Width = 150
        Height = 40
        Caption = '&5. Verifica Cadastros'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtVerifCadastrClick
      end
      object BtGeraBWLici: TBitBtn
        Tag = 14
        Left = 768
        Top = 4
        Width = 150
        Height = 40
        Caption = '&6. Gera registros'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtGeraBWLiciClick
      end
      object BtGeraCads: TBitBtn
        Tag = 14
        Left = 312
        Top = 4
        Width = 150
        Height = 40
        Caption = '&3. Gera cadastros'
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtGeraCadsClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1073
    Height = 456
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1073
      Height = 53
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 4
        Top = 8
        Width = 113
        Height = 13
        Caption = 'Arquivo a ser carregado'
      end
      object SbLoadCSVOthDir: TSpeedButton
        Left = 772
        Top = 24
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = SbLoadCSVOthDirClick
      end
      object EdLoadCSVArq: TdmkEdit
        Left = 4
        Top = 24
        Width = 765
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 
          'C:\_Sincro\Projetos_Aux\_Manuais e Tutoriais\_NF-e\Tabelas\TABEL' +
          'A-CEST-2020-Oobj.xlsx'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 
          'C:\_Sincro\Projetos_Aux\_Manuais e Tutoriais\_NF-e\Tabelas\TABEL' +
          'A-CEST-2020-Oobj.xlsx'
        ValWarn = False
      end
      object BtDropTable: TBitBtn
        Left = 816
        Top = 16
        Width = 181
        Height = 25
        Caption = 'Exclui tabelas para recriar!'
        TabOrder = 1
        OnClick = BtDropTableClick
      end
      object BitBtn1: TBitBtn
        Left = 1004
        Top = 16
        Width = 47
        Height = 25
        Caption = 'BitBtn1'
        TabOrder = 2
        OnClick = BitBtn1Click
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 53
      Width = 1073
      Height = 403
      ActivePage = TabSheet3
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' StringList'
        object MeAvisos: TMemo
          Left = 0
          Top = 0
          Width = 1065
          Height = 375
          Align = alClient
          Lines.Strings = (
            'MeAvisos')
          TabOrder = 0
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'StringGrid'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Grade1: TStringGrid
          Left = 0
          Top = 0
          Width = 804
          Height = 375
          Align = alClient
          ColCount = 13
          DefaultColWidth = 56
          DefaultRowHeight = 18
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
          TabOrder = 0
          ExplicitWidth = 776
          ExplicitHeight = 176
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'SG Faster'
        ImageIndex = 2
        object Grade2: TStringGrid
          Left = 0
          Top = 0
          Width = 1065
          Height = 375
          Align = alClient
          ColCount = 13
          DefaultColWidth = 56
          DefaultRowHeight = 18
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
          TabOrder = 0
        end
      end
      object TabSheet4: TTabSheet
        Caption = ' Cadastros a verificar'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 1065
          Height = 375
          Align = alClient
          DataSource = DsCadVerif
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Campo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Width = 43
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Linha'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Texto'
              Visible = True
            end>
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object Query: TMySQLQuery
    Database = Dmod.MyDB
    Left = 384
    Top = 284
  end
  object QrCadVerif: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT "Status" Campo, Codigo, Linha, NO_BWStatus Texto '
      'FROM bwcsvload'
      'WHERE NO_BWStatus <> ""'
      'AND BWStatus=0'
      ''
      'UNION '
      ''
      'SELECT "Cliente" Campo, Codigo, Linha, Cliente Texto '
      'FROM bwcsvload'
      'WHERE Cliente <> "" '
      'AND CodCliente=0'
      ''
      'UNION '
      ''
      'SELECT "Produto" Campo, Codigo, Linha, Produto Texto '
      'FROM bwcsvload'
      'WHERE Produto <> "" '
      'AND CodProduto=0'
      ''
      'UNION '
      ''
      'SELECT "Unidade" Campo, Codigo, Linha, Unidade Texto '
      'FROM bwcsvload'
      'WHERE Unidade <> "" '
      'AND CodUnidade=0'
      ''
      'UNION '
      ''
      'SELECT "Portal" Campo, Codigo, Linha, Portal Texto '
      'FROM bwcsvload'
      'WHERE Portal <> "" '
      'AND CodPortal=0'
      ''
      'UNION '
      ''
      'SELECT "Amostras" Campo, Codigo, Linha, Amostras Texto '
      'FROM bwcsvload'
      'WHERE Amostras <> "" '
      'AND CodPrzAmostra=0'
      ''
      'UNION '
      ''
      'SELECT "PrazoEntrega" Campo, Codigo, Linha, PrazoEntrega Texto '
      'FROM bwcsvload'
      'WHERE PrazoEntrega <> "" '
      'AND CodPrzEntrega=0'
      ''
      'UNION '
      ''
      'SELECT "PrazoEntrega" Campo, Codigo, Linha, PrazoEntrega Texto '
      'FROM bwcsvload'
      'WHERE PrazoEntrega <> "" '
      'AND CodPrzEntrega=0'
      ''
      'UNION '
      ''
      'SELECT "Responsaveis" Campo, Codigo, Linha, Responsaveis Texto '
      'FROM bwcsvload'
      'WHERE Responsaveis <> "" '
      'AND CodResponsa1=0'
      ''
      'UNION '
      ''
      'SELECT "Empresa" Campo, Codigo, Linha, Empresa Texto '
      'FROM bwcsvload'
      'WHERE Empresa <> "" '
      'AND CodEmpresa=0'
      ''
      'UNION '
      ''
      
        'SELECT "EmpresaGanhadora" Campo, Codigo, Linha, EmpresaGanhadora' +
        ' Texto '
      'FROM bwcsvload'
      'WHERE EmpresaGanhadora <> "" '
      'AND CodEmpresaGanhadora=0'
      ''
      'UNION '
      ''
      
        'SELECT "MarcaDoGanhador" Campo, Codigo, Linha, MarcaDoGanhador T' +
        'exto '
      'FROM bwcsvload'
      'WHERE MarcaDoGanhador <> "" '
      'AND CodMarcaDoGanhador=0'
      ''
      'UNION '
      ''
      'SELECT "Segmento" Campo, Codigo, Linha, Segmento Texto '
      'FROM bwcsvload'
      'WHERE Segmento <> "" '
      'AND CodSegmento=0'
      ''
      'UNION '
      ''
      
        'SELECT "Representante" Campo, Codigo, Linha, Representante Texto' +
        ' '
      'FROM bwcsvload'
      'WHERE Representante <> "" '
      'AND CodRepresentante=0'
      ''
      '')
    Left = 456
    Top = 288
    object QrCadVerifCampo: TWideStringField
      FieldName = 'Campo'
      Required = True
      Size = 16
    end
    object QrCadVerifCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCadVerifLinha: TIntegerField
      FieldName = 'Linha'
      Required = True
    end
    object QrCadVerifTexto: TWideStringField
      FieldName = 'Texto'
      Size = 255
    end
  end
  object DsCadVerif: TDataSource
    DataSet = QrCadVerif
    Left = 456
    Top = 337
  end
  object QrBWCsvLoad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM bwcsvload'
      'ORDER BY Linha')
    Left = 92
    Top = 321
    object QrBWCsvLoadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrBWCsvLoadLinha: TIntegerField
      FieldName = 'Linha'
      Required = True
    end
    object QrBWCsvLoadBWStatus: TIntegerField
      FieldName = 'BWStatus'
      Required = True
    end
    object QrBWCsvLoadNO_BWStatus: TWideStringField
      FieldName = 'NO_BWStatus'
      Size = 60
    end
    object QrBWCsvLoadReabertura: TDateTimeField
      FieldName = 'Reabertura'
      Required = True
    end
    object QrBWCsvLoadData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrBWCsvLoadHora: TTimeField
      FieldName = 'Hora'
      Required = True
    end
    object QrBWCsvLoadCadastroDaProposta: TWideStringField
      FieldName = 'CadastroDaProposta'
      Size = 60
    end
    object QrBWCsvLoadEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrBWCsvLoadCodCliente: TIntegerField
      FieldName = 'CodCliente'
      Required = True
    end
    object QrBWCsvLoadCliente: TWideStringField
      FieldName = 'Cliente'
      Size = 255
    end
    object QrBWCsvLoadEstado: TWideStringField
      FieldName = 'Estado'
      Size = 10
    end
    object QrBWCsvLoadPregao: TWideStringField
      FieldName = 'Pregao'
      Size = 60
    end
    object QrBWCsvLoadLote: TWideStringField
      FieldName = 'Lote'
      Size = 30
    end
    object QrBWCsvLoadItemLt: TWideStringField
      FieldName = 'ItemLt'
      Size = 11
    end
    object QrBWCsvLoadCodProduto: TIntegerField
      FieldName = 'CodProduto'
      Required = True
    end
    object QrBWCsvLoadProduto: TWideStringField
      FieldName = 'Produto'
      Size = 255
    end
    object QrBWCsvLoadCodUnidade: TIntegerField
      FieldName = 'CodUnidade'
      Required = True
    end
    object QrBWCsvLoadUnidade: TWideStringField
      FieldName = 'Unidade'
    end
    object QrBWCsvLoadQuantidade: TFloatField
      FieldName = 'Quantidade'
      Required = True
    end
    object QrBWCsvLoadEstimativa: TFloatField
      FieldName = 'Estimativa'
      Required = True
    end
    object QrBWCsvLoadEstimativa2: TFloatField
      FieldName = 'Estimativa2'
      Required = True
    end
    object QrBWCsvLoadCodPortal: TIntegerField
      FieldName = 'CodPortal'
      Required = True
    end
    object QrBWCsvLoadPortal: TWideStringField
      FieldName = 'Portal'
      Size = 60
    end
    object QrBWCsvLoadNProcesso: TWideStringField
      FieldName = 'NProcesso'
      Size = 60
    end
    object QrBWCsvLoadColocacao: TWideStringField
      FieldName = 'Colocacao'
      Size = 10
    end
    object QrBWCsvLoadEstah: TWideStringField
      FieldName = 'Estah'
      Size = 10
    end
    object QrBWCsvLoadCodPrzAmostra: TIntegerField
      FieldName = 'CodPrzAmostra'
      Required = True
    end
    object QrBWCsvLoadAmostras: TWideStringField
      FieldName = 'Amostras'
      Size = 255
    end
    object QrBWCsvLoadCodPrzEntrega: TIntegerField
      FieldName = 'CodPrzEntrega'
      Required = True
    end
    object QrBWCsvLoadPrazoEntrega: TWideStringField
      FieldName = 'PrazoEntrega'
      Size = 255
    end
    object QrBWCsvLoadCodResponsa1: TIntegerField
      FieldName = 'CodResponsa1'
      Required = True
    end
    object QrBWCsvLoadCodResponsa2: TIntegerField
      FieldName = 'CodResponsa2'
      Required = True
    end
    object QrBWCsvLoadCodResponsa3: TIntegerField
      FieldName = 'CodResponsa3'
      Required = True
    end
    object QrBWCsvLoadResponsaveis: TWideStringField
      FieldName = 'Responsaveis'
      Size = 60
    end
    object QrBWCsvLoadCodEmpresa: TIntegerField
      FieldName = 'CodEmpresa'
      Required = True
    end
    object QrBWCsvLoadEmpresa: TWideStringField
      FieldName = 'Empresa'
      Size = 60
    end
    object QrBWCsvLoadCodEmpresaGanhadora: TIntegerField
      FieldName = 'CodEmpresaGanhadora'
      Required = True
    end
    object QrBWCsvLoadEmpresaGanhadora: TWideStringField
      FieldName = 'EmpresaGanhadora'
      Size = 120
    end
    object QrBWCsvLoadCodMarcaDoGanhador: TIntegerField
      FieldName = 'CodMarcaDoGanhador'
      Required = True
    end
    object QrBWCsvLoadMarcaDoGanhador: TWideStringField
      FieldName = 'MarcaDoGanhador'
      Size = 60
    end
    object QrBWCsvLoadCodSegmento: TIntegerField
      FieldName = 'CodSegmento'
      Required = True
    end
    object QrBWCsvLoadSegmento: TWideStringField
      FieldName = 'Segmento'
      Size = 60
    end
    object QrBWCsvLoadCodRepresentante: TIntegerField
      FieldName = 'CodRepresentante'
      Required = True
    end
    object QrBWCsvLoadRepresentante: TWideStringField
      FieldName = 'Representante'
      Size = 60
    end
    object QrBWCsvLoadValorNoPregao: TFloatField
      FieldName = 'ValorNoPregao'
      Required = True
    end
    object QrBWCsvLoadValorGanhador: TFloatField
      FieldName = 'ValorGanhador'
      Required = True
    end
    object QrBWCsvLoadClass_: TIntegerField
      FieldName = 'Class_'
      Required = True
    end
    object QrBWCsvLoadMes: TWideStringField
      FieldName = 'Mes'
      Size = 15
    end
    object QrBWCsvLoadAno: TWideStringField
      FieldName = 'Ano'
      Size = 10
    end
    object QrBWCsvLoadLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrBWCsvLoadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBWCsvLoadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBWCsvLoadUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrBWCsvLoadUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrBWCsvLoadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrBWCsvLoadAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrBWCsvLoadAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrBWCsvLoadAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object QrLote: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'Linha, BWStatus, NO_BWStatus, '
      'Reabertura, Data, Hora, '
      'CadastroDaProposta, Entidade, CodCliente, '
      'Cliente, Estado, Pregao, '
      'Lote, '
      '/*'
      'ItemLt, CodProduto, '
      'Produto, CodUnidade, Unidade, '
      'Quantidade, Estimativa, Estimativa2, '
      '*/'
      'CodPortal, Portal, NProcesso, '
      'Colocacao, Estah, CodPrzAmostra, '
      'Amostras, CodPrzEntrega, PrazoEntrega, '
      'CodResponsa1, CodResponsa2, CodResponsa3, '
      'Responsaveis, CodEmpresa, Empresa, '
      'CodEmpresaGanhadora, EmpresaGanhadora, CodMarcaDoGanhador, '
      'MarcaDoGanhador, CodSegmento, Segmento, '
      'CodRepresentante, Representante, ValorNoPregao, '
      'ValorGanhador, Class_  '
      'FROM bwcsvload'
      'GROUP BY Data, Cliente, Pregao, Lote')
    Left = 636
    Top = 292
    object QrLoteCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLoteLinha: TIntegerField
      FieldName = 'Linha'
      Required = True
    end
    object QrLoteBWStatus: TIntegerField
      FieldName = 'BWStatus'
      Required = True
    end
    object QrLoteNO_BWStatus: TWideStringField
      FieldName = 'NO_BWStatus'
      Size = 60
    end
    object QrLoteReabertura: TDateTimeField
      FieldName = 'Reabertura'
      Required = True
    end
    object QrLoteData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrLoteHora: TTimeField
      FieldName = 'Hora'
      Required = True
    end
    object QrLoteCadastroDaProposta: TWideStringField
      FieldName = 'CadastroDaProposta'
      Size = 60
    end
    object QrLoteEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrLoteCodCliente: TIntegerField
      FieldName = 'CodCliente'
      Required = True
    end
    object QrLoteCliente: TWideStringField
      FieldName = 'Cliente'
      Size = 255
    end
    object QrLoteEstado: TWideStringField
      FieldName = 'Estado'
      Size = 10
    end
    object QrLotePregao: TWideStringField
      FieldName = 'Pregao'
      Size = 60
    end
    object QrLoteLote: TWideStringField
      FieldName = 'Lote'
      Size = 30
    end
    object QrLoteCodPortal: TIntegerField
      FieldName = 'CodPortal'
      Required = True
    end
    object QrLotePortal: TWideStringField
      FieldName = 'Portal'
      Size = 60
    end
    object QrLoteNProcesso: TWideStringField
      FieldName = 'NProcesso'
      Size = 60
    end
    object QrLoteColocacao: TWideStringField
      FieldName = 'Colocacao'
      Size = 10
    end
    object QrLoteEstah: TWideStringField
      FieldName = 'Estah'
      Size = 10
    end
    object QrLoteCodPrzAmostra: TIntegerField
      FieldName = 'CodPrzAmostra'
      Required = True
    end
    object QrLoteAmostras: TWideStringField
      FieldName = 'Amostras'
      Size = 255
    end
    object QrLoteCodPrzEntrega: TIntegerField
      FieldName = 'CodPrzEntrega'
      Required = True
    end
    object QrLotePrazoEntrega: TWideStringField
      FieldName = 'PrazoEntrega'
      Size = 255
    end
    object QrLoteCodResponsa1: TIntegerField
      FieldName = 'CodResponsa1'
      Required = True
    end
    object QrLoteCodResponsa2: TIntegerField
      FieldName = 'CodResponsa2'
      Required = True
    end
    object QrLoteCodResponsa3: TIntegerField
      FieldName = 'CodResponsa3'
      Required = True
    end
    object QrLoteResponsaveis: TWideStringField
      FieldName = 'Responsaveis'
      Size = 60
    end
    object QrLoteCodEmpresa: TIntegerField
      FieldName = 'CodEmpresa'
      Required = True
    end
    object QrLoteEmpresa: TWideStringField
      FieldName = 'Empresa'
      Size = 60
    end
    object QrLoteCodEmpresaGanhadora: TIntegerField
      FieldName = 'CodEmpresaGanhadora'
      Required = True
    end
    object QrLoteEmpresaGanhadora: TWideStringField
      FieldName = 'EmpresaGanhadora'
      Size = 120
    end
    object QrLoteCodMarcaDoGanhador: TIntegerField
      FieldName = 'CodMarcaDoGanhador'
      Required = True
    end
    object QrLoteMarcaDoGanhador: TWideStringField
      FieldName = 'MarcaDoGanhador'
      Size = 60
    end
    object QrLoteCodSegmento: TIntegerField
      FieldName = 'CodSegmento'
      Required = True
    end
    object QrLoteSegmento: TWideStringField
      FieldName = 'Segmento'
      Size = 60
    end
    object QrLoteCodRepresentante: TIntegerField
      FieldName = 'CodRepresentante'
      Required = True
    end
    object QrLoteRepresentante: TWideStringField
      FieldName = 'Representante'
      Size = 60
    end
    object QrLoteValorNoPregao: TFloatField
      FieldName = 'ValorNoPregao'
      Required = True
    end
    object QrLoteValorGanhador: TFloatField
      FieldName = 'ValorGanhador'
      Required = True
    end
    object QrLoteClass_: TIntegerField
      FieldName = 'Class_'
      Required = True
    end
  end
  object QrPsq2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 216
    Top = 128
    object QrPsq2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPsq2Nome: TWideStringField
      DisplayWidth = 512
      FieldName = 'Nome'
      Size = 512
    end
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    Left = 100
    Top = 120
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqNome: TWideStringField
      DisplayWidth = 512
      FieldName = 'Nome'
      Size = 512
    end
  end
end
