unit UnFiscal_PF;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  Variants, mySQLDbTables, frxClass, frxDBSet, DmkDAC_PF, UnDmkEnums,
  SPED_Listas, UnInternalConsts;

type
  TUnFiscal_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  BuscaCFOP_NatOp(var CFOP, Descr: String): Boolean;
    procedure MostraFormCEST(Codigo: Integer);
  end;

var
  Fiscal_PF: TUnFiscal_PF;

implementation

uses MyDBCheck, Module, ModuleGeral, UMySQLModule, CEST;

{ TUnFiscal_PF }


{ TUnFiscal_PF }

function TUnFiscal_PF.BuscaCFOP_NatOp(var CFOP, Descr: String): Boolean;
const
  Aviso  = '...';
  Titulo = 'Sele��o de Status de NFe';
  Prompt = 'Informe o status: [F7 para pesquisar]';
  Campo  = 'Descricao';
  //
  LocF7CodiFldName = 'CodTxt';
  LocF7NameFldName = 'Nome';
var
  Qry: TmySQLQuery;
  PesqSQL: String;
  Res: Variant;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    Result := False;
    CFOP   := '';
    Descr  := '';
    //
    PesqSQL := Geral.ATS([
    'SELECT CodTxt Codigo, CONCAT(CodTXT, " - ", Nome) Descricao ',
    'FROM ' + CO_NOME_TbSPEDEFD_CFOP,
    'ORDER BY Descricao ',
    '']);
    CFOP :=
      DBCheck.EscolheTextoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, CFOP,
      [PesqSQL], DModG.AllID_DB, True, LocF7CodiFldName, LocF7NameFldName);
    if Res <> Null then
    begin
      CFOP  := Copy(VAR_SELNOM, 1, 4);
      Descr := VAR_SELNOM;
      //
      Result := CFOP <> '';
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnFiscal_PF.MostraFormCEST(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCEST, FmCEST, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCEST.LocCod(Codigo, Codigo);
    FmCEST.ShowModal;
    FmCEST.Destroy;
  end;
end;

end.
