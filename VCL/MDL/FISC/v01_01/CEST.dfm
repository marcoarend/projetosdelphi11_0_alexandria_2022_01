object FmCEST: TFmCEST
  Left = 368
  Top = 194
  Caption = 'FIS-CEST_-001 :: Cadastro de CEST'
  ClientHeight = 439
  ClientWidth = 946
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 946
    Height = 343
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitWidth = 784
    ExplicitHeight = 225
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 946
      Height = 229
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 12
        Top = 20
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 108
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label10: TLabel
        Left = 12
        Top = 60
        Width = 31
        Height = 13
        Caption = 'CEST:'
      end
      object Label11: TLabel
        Left = 12
        Top = 100
        Width = 27
        Height = 13
        Caption = 'NCM:'
      end
      object Label12: TLabel
        Left = 12
        Top = 140
        Width = 82
        Height = 13
        Caption = 'Segmento CEST:'
      end
      object Label13: TLabel
        Left = 12
        Top = 180
        Width = 54
        Height = 13
        Caption = 'Item CEST:'
      end
      object Label14: TLabel
        Left = 76
        Top = 180
        Width = 63
        Height = 13
        Caption = 'Anexo XXVII:'
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 36
        Width = 89
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object MeNome: TdmkMemo
        Left = 108
        Top = 36
        Width = 833
        Height = 101
        TabOrder = 3
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
      end
      object EdCEST: TdmkEdit
        Left = 12
        Top = 76
        Width = 89
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'CEST'
        UpdCampo = 'CEST'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNCM: TdmkEdit
        Left = 12
        Top = 116
        Width = 89
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'NCM'
        UpdCampo = 'NCM'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSegmentoCEST: TdmkEdit
        Left = 12
        Top = 156
        Width = 929
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'SegmentoCEST'
        UpdCampo = 'SegmentoCEST'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdItemCEST: TdmkEdit
        Left = 12
        Top = 196
        Width = 61
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'ItemCEST'
        UpdCampo = 'ItemCEST'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdAnexoXXVII: TdmkEdit
        Left = 76
        Top = 196
        Width = 865
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'AnexoXXVII'
        UpdCampo = 'AnexoXXVII'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 280
      Width = 946
      Height = 63
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 162
      ExplicitWidth = 784
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 807
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 645
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 946
    Height = 343
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitWidth = 784
    ExplicitHeight = 225
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 946
      Height = 221
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 56
        Width = 31
        Height = 13
        Caption = 'CEST:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 104
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label3: TLabel
        Left = 8
        Top = 96
        Width = 27
        Height = 13
        Caption = 'NCM:'
        FocusControl = dmkDBEdit1
      end
      object Label4: TLabel
        Left = 8
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = dmkDBEdit2
      end
      object Label5: TLabel
        Left = 8
        Top = 136
        Width = 82
        Height = 13
        Caption = 'Segmento CEST:'
        FocusControl = DBEdit1
      end
      object Label6: TLabel
        Left = 8
        Top = 176
        Width = 54
        Height = 13
        Caption = 'Item CEST:'
        FocusControl = DBEdit2
      end
      object Label8: TLabel
        Left = 72
        Top = 176
        Width = 57
        Height = 13
        Caption = 'AnexoXXVII'
        FocusControl = DBEdit3
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 72
        Width = 89
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'CEST'
        DataSource = DsCEST
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBMemo1: TDBMemo
        Left = 104
        Top = 32
        Width = 833
        Height = 101
        DataField = 'Nome'
        DataSource = DsCEST
        TabOrder = 1
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 8
        Top = 112
        Width = 89
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'NCM'
        DataSource = DsCEST
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 8
        Top = 32
        Width = 89
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCEST
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 3
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 8
        Top = 152
        Width = 929
        Height = 21
        DataField = 'SegmentoCEST'
        DataSource = DsCEST
        TabOrder = 4
      end
      object DBEdit2: TDBEdit
        Left = 8
        Top = 192
        Width = 61
        Height = 21
        DataField = 'ItemCEST'
        DataSource = DsCEST
        TabOrder = 5
      end
      object DBEdit3: TDBEdit
        Left = 72
        Top = 192
        Width = 865
        Height = 21
        DataField = 'AnexoXXVII'
        DataSource = DsCEST
        TabOrder = 6
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 279
      Width = 946
      Height = 64
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 161
      ExplicitWidth = 784
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 66
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
        ExplicitWidth = 88
      end
      object Panel3: TPanel
        Left = 240
        Top = 15
        Width = 704
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        ExplicitLeft = 372
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExcluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 571
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          ExplicitLeft = 387
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtImporta: TBitBtn
          Tag = 19
          Left = 376
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'I&mporta'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtImportaClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 946
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 784
    object GB_R: TGroupBox
      Left = 898
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 736
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 682
      Height = 52
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 520
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 229
        Height = 32
        Caption = 'Cadastro de CEST'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 229
        Height = 32
        Caption = 'Cadastro de CEST'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 229
        Height = 32
        Caption = 'Cadastro de CEST'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 946
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitWidth = 784
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 942
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 780
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrCEST: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCESTBeforeOpen
    AfterOpen = QrCESTAfterOpen
    SQL.Strings = (
      'SELECT *'
      'FROM cest')
    Left = 64
    Top = 64
    object QrCESTCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCESTCEST: TWideStringField
      FieldName = 'CEST'
      Required = True
      Size = 11
    end
    object QrCESTNCM: TWideStringField
      FieldName = 'NCM'
      Size = 12
    end
    object QrCESTNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 1024
    end
    object QrCESTSegmentoCEST: TWideStringField
      FieldName = 'SegmentoCEST'
      Size = 120
    end
    object QrCESTItemCEST: TWideStringField
      FieldName = 'ItemCEST'
      Size = 12
    end
    object QrCESTAnexoXXVII: TWideStringField
      FieldName = 'AnexoXXVII'
      Size = 120
    end
    object QrCESTLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrCESTDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCESTDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCESTUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrCESTUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrCESTAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrCESTAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrCESTAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrCESTAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsCEST: TDataSource
    DataSet = QrCEST
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
end
