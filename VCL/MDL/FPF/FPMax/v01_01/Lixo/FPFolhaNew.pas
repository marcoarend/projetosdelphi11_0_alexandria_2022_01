unit FPFolhaNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Db, mySQLDbTables;

type
  TFmFPFolhaNew = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Label32: TLabel;
    CBMes: TComboBox;
    LaAnoI: TLabel;
    CBAno: TComboBox;
    QrPesq: TmySQLQuery;
    QrPesqPeriodo: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmFPFolhaNew: TFmFPFolhaNew;

implementation

uses UnMyObjects, Module, UnMsgInt, UMySQLModule, UnInternalConsts, CondGer, FPFolha;

{$R *.DFM}

procedure TFmFPFolhaNew.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPFolhaNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPFolhaNew.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFPFolhaNew.FormCreate(Sender: TObject);
var
  i: Integer;
  //
  Ano, Mes, Dia: Word;
begin
  with CBMes.Items do
  begin
    Add(FIN_JANEIRO);
    Add(FIN_FEVEREIRO);
    Add(FIN_MARCO);
    Add(FIN_ABRIL);
    Add(FIN_MAIO);
    Add(FIN_JUNHO);
    Add(FIN_JULHO);
    Add(FIN_AGOSTO);
    Add(FIN_SETEMBRO);
    Add(FIN_OUTUBRO);
    Add(FIN_NOVEMBRO);
    Add(FIN_DEZEMBRO);
  end;
  DecodeDate(Date, Ano, Mes, Dia);
  with CBAno.Items do
  begin
    for i := Ano-50 to Ano+50 do Add(IntToStr(i));
  end;
  CBAno.ItemIndex := 50;
  CBMes.ItemIndex := (Mes - 1);
  //////////////////////////////////////////////////////////////////////////
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT Max(Periodo) Periodo');
  QrPesq.SQL.Add('FROM fpfolha');
  QrPesq.Open;
  if QrPesqPeriodo.Value > 0 then
  begin
    MLAGeral.MesEAnoDePeriodoShort(QrPesqPeriodo.Value+1, Mes, Ano);
    for i := 0 to CBAno.Items.Count do
    begin
      if CBAno.Items[i] = IntToStr(Ano) then
      begin
        CBAno.ItemIndex := i;
        Break;
      end;
    end;
    CBMes.ItemIndex := Mes -1;
  end;
end;

procedure TFmFPFolhaNew.BtOKClick(Sender: TObject);
var
  Periodo: Integer;
begin
  Periodo := MLAGeral.PeriodoEncode(
    Geral.IMV(CBAno.Items[CBAno.ItemIndex]), CBMes.ItemIndex+1);
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT Periodo');
  QrPesq.SQL.Add('FROM fpfolha');
  QrPesq.SQL.Add('WHERE Periodo=:P0');
  QrPesq.Params[0].AsInteger := Periodo;
  QrPesq.Open;
  if QrPesqPeriodo.Value = Periodo then
  begin
    Application.MessageBox(PChar('Inclus�o de novo per�odo abortado!'+Chr(13)+
    Chr(10)+'O per�odo j� existe!'), 'Erro',
    MB_OK+MB_ICONERROR);
    Exit;
  end;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpfolha SET Periodo=:P0, ');
  Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb');
  //
  Dmod.QrUpd.Params[00].AsInteger := Periodo;
  Dmod.QrUpd.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpd.Params[02].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.ExecSQL;
  //
  FmFPFolha.LocPeriodo(Periodo, Periodo);
  //
  Close;
end;

end.

