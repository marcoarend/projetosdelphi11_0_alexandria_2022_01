object FmFPFolhaNew: TFmFPFolhaNew
  Left = 520
  Top = 259
  Width = 275
  Height = 200
  Caption = 'Novo Per�odo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 118
    Width = 267
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 155
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 267
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Novo Per�odo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 263
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 267
    Height = 70
    Align = alClient
    TabOrder = 0
    object Label32: TLabel
      Left = 4
      Top = 15
      Width = 23
      Height = 13
      Caption = 'M�s:'
    end
    object LaAnoI: TLabel
      Left = 180
      Top = 15
      Width = 22
      Height = 13
      Caption = 'Ano:'
    end
    object CBMes: TComboBox
      Left = 5
      Top = 32
      Width = 172
      Height = 21
      Style = csDropDownList
      Color = clWhite
      DropDownCount = 12
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 7622183
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
    end
    object CBAno: TComboBox
      Left = 179
      Top = 32
      Width = 78
      Height = 21
      Style = csDropDownList
      Color = clWhite
      DropDownCount = 3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 7622183
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemHeight = 13
      ParentFont = False
      TabOrder = 1
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Periodo) Periodo'
      'FROM fpfolha')
    Left = 84
    Top = 52
    object QrPesqPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
  end
end

