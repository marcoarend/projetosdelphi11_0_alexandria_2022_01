object FmFPCBO02: TFmFPCBO02
  Left = 339
  Top = 185
  Width = 800
  Height = 530
  Caption = 'CBO 2002'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 680
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa�da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'CBO 2002'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 788
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 400
    Align = alClient
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 790
      Height = 398
      Align = alClient
      DataSource = DsFPCBO02
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CodID'
          Title.Caption = 'C�digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri��o'
          Width = 707
          Visible = True
        end>
    end
  end
  object TbFPCBO02: TmySQLTable
    Database = Dmod.MyDB
    BeforePost = TbFPCBO02BeforePost
    TableName = 'fpcbo02'
    Left = 140
    Top = 176
    object TbFPCBO02CodID: TWideStringField
      FieldName = 'CodID'
      Size = 7
    end
    object TbFPCBO02Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
  end
  object DsFPCBO02: TDataSource
    DataSet = TbFPCBO02
    Left = 168
    Top = 176
  end
end
