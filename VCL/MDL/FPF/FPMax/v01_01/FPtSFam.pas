unit FPtSFam;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Grids, DBGrids,
  dmkDBGridDAC, Db, mySQLDbTables, dmkDBGrid, Menus;

type
  TFmFPtSFam = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DsFPtSFam: TDataSource;
    QrFPtSFam: TmySQLQuery;
    dmkDBGridDAC1: TdmkDBGridDAC;
    BtAcao: TBitBtn;
    PMAcao: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui1: TMenuItem;
    QrFPtSFamLimite: TFloatField;
    QrFPtSFamValor: TFloatField;
    QrFPtSFamAtivo: TSmallintField;
    Memo1: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure dmkDBGridDAC1CellClick(Column: TColumn);
    procedure dmkDBGridDAC1AfterSQLExec(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenQrFPtSFam(Limite: Double);
  public
    { Public declarations }
  end;

  var
  FmFPtSFam: TFmFPtSFam;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmFPtSFam.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPtSFam.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPtSFam.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFPtSFam.FormCreate(Sender: TObject);
begin
  ReopenQrFPtSFam(0);
end;

procedure TFmFPtSFam.Inclui1Click(Sender: TObject);
var
  Limite: String;
  LimVal: Double;
begin
  Limite := '0,00';
  if InputQuery('Novo item de sal�rio fam�lia', 'Informe o sal�rio limite:',
  Limite) then
  begin
    LimVal := Geral.DMV(Limite);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO fptsfam SET Limite=:P0');
    Dmod.QrUpd.Params[0].AsFloat  := LimVal;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPtSFam(LimVal);
  end;
end;

procedure TFmFPtSFam.ReopenQrFPtSFam(Limite: Double);
begin
  QrFPtSFam.Close;
  QrFPtSFam.Open;
  QrFPtSFam.Locate('Limite', Limite, []);
end;

procedure TFmFPtSFam.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmFPtSFam.Exclui1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o do registro selecionado?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fptsfam WHERE Limite=:P0');
    Dmod.QrUpd.SQL.Add('AND Valor=:P1');
    Dmod.QrUpd.Params[00].AsFloat   := QrFPtSFamLimite.Value;
    Dmod.QrUpd.Params[01].AsFloat   := QrFPtSFamValor.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPtSFam(Int(Date));
  end;
end;

procedure TFmFPtSFam.dmkDBGridDAC1CellClick(Column: TColumn);
var
  Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if QrFPtSFamAtivo.Value = 1 then Ativo := 0 else Ativo := 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE fptsfam SET Ativo=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Limite=:P1');
    //
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsFloat   := QrFPtSFamLimite.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPtSFam(QrFPtSFamLimite.Value);
  end;
end;

procedure TFmFPtSFam.dmkDBGridDAC1AfterSQLExec(Sender: TObject);
begin
  //Memo1.Text := dmkDBGridDAC1.SQL.Text;
end;

end.

