unit FPtINSS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Grids, DBGrids,
  dmkDBGridDAC, Db, mySQLDbTables, dmkDBGrid, Menus;

type
  TFmFPtINSS = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DsFPtINSS: TDataSource;
    QrFPtINSS: TmySQLQuery;
    dmkDBGridDAC1: TdmkDBGridDAC;
    BtAcao: TBitBtn;
    PMAcao: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui1: TMenuItem;
    QrFPtINSSFaixa: TFloatField;
    QrFPtINSSAliquota: TFloatField;
    QrFPtINSSDeducao: TFloatField;
    QrFPtINSSAtivo: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure dmkDBGridDAC1CellClick(Column: TColumn);
  private
    { Private declarations }
    procedure ReopenQrFPtINSS(Faixa: Double);
  public
    { Public declarations }
  end;

  var
  FmFPtINSS: TFmFPtINSS;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmFPtINSS.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPtINSS.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPtINSS.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFPtINSS.FormCreate(Sender: TObject);
begin
  ReopenQrFPtINSS(0);
end;

procedure TFmFPtINSS.Inclui1Click(Sender: TObject);
var
  Faixa: String;
  FaxVal: Double;
begin
  Faixa := '0,00';
  if InputQuery('Novo item de desconto de INSS', 'Informe o a faixa salarial:',
  Faixa) then
  begin
    FaxVal := Geral.DMV(Faixa);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO fptinss SET Faixa=:P0');
    Dmod.QrUpd.Params[0].AsFloat  := FaxVal;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPtINSS(FaxVal);
  end;
end;

procedure TFmFPtINSS.ReopenQrFPtINSS(Faixa: Double);
begin
  QrFPtINSS.Close;
  QrFPtINSS.Open;
  QrFPtINSS.Locate('Faixa', Faixa, []);
end;

procedure TFmFPtINSS.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmFPtINSS.Exclui1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o do registro selecionado?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fptinss WHERE Faixa=:P0');
    Dmod.QrUpd.Params[00].AsFloat   := QrFPtINSSFaixa.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPtINSS(Int(Date));
  end;
end;

procedure TFmFPtINSS.dmkDBGridDAC1CellClick(Column: TColumn);
var
  Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if QrFPtINSSAtivo.Value = 1 then Ativo := 0 else Ativo := 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE fptinss SET Ativo=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Faixa=:P1');
    //
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsFloat   := QrFPtINSSFaixa.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPtINSS(QrFPtINSSFaixa.Value);
  end;
end;

end.

