object FmImportSalCfgCab: TFmImportSalCfgCab
  Left = 368
  Top = 194
  Caption = 'SAL-IMP02-002 :: Configura'#231#227'o de Importa'#231#227'o de Proventos'
  ClientHeight = 497
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 401
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsImportSalCfgCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 689
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsImportSalCfgCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 337
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 10124
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Config.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 354
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'Con&ta'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 65
      Width = 784
      Height = 148
      Align = alTop
      DataSource = DsImportSalCfgCta
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Referencia'
          Title.Caption = 'Refer'#234'ncia'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'Conta'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CTA'
          Title.Caption = 'Nome da conta'
          Width = 624
          Visible = True
        end>
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 401
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GB1: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 338
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 3
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GroupBox3: TGroupBox
      Left = 0
      Top = 61
      Width = 784
      Height = 144
      Align = alTop
      Caption = ' Disposi'#231#227'o dos dados nas linhas do arquivo a ser importado:'
      TabOrder = 1
      object Label3: TLabel
        Left = 12
        Top = 44
        Width = 107
        Height = 13
        Caption = 'CNPJ do empregador: '
      end
      object Label4: TLabel
        Left = 168
        Top = 20
        Width = 82
        Height = 13
        Caption = 'Pos. ini. / Ordem:'
      end
      object Label5: TLabel
        Left = 252
        Top = 20
        Width = 48
        Height = 13
        Caption = 'Tamanho:'
      end
      object Label8: TLabel
        Left = 12
        Top = 68
        Width = 96
        Height = 13
        Caption = 'CPF do funcion'#225'rio: '
      end
      object Label6: TLabel
        Left = 12
        Top = 92
        Width = 109
        Height = 13
        Caption = 'C'#243'digo do funcion'#225'rio: '
      end
      object Label10: TLabel
        Left = 12
        Top = 116
        Width = 104
        Height = 13
        Caption = 'Nome do funcion'#225'rio: '
      end
      object Label11: TLabel
        Left = 384
        Top = 44
        Width = 103
        Height = 13
        Caption = 'Refer'#234'ncia da conta: '
      end
      object Label12: TLabel
        Left = 384
        Top = 68
        Width = 105
        Height = 13
        Caption = 'M'#234's de compet'#234'ncia: '
      end
      object Label13: TLabel
        Left = 384
        Top = 92
        Width = 104
        Height = 13
        Caption = 'Ano de compet'#234'ncia: '
      end
      object Label14: TLabel
        Left = 384
        Top = 116
        Width = 92
        Height = 13
        Caption = 'Valor l'#237'quido pago: '
      end
      object Label15: TLabel
        Left = 540
        Top = 20
        Width = 82
        Height = 13
        Caption = 'Pos. ini. / Ordem:'
      end
      object Label16: TLabel
        Left = 624
        Top = 20
        Width = 48
        Height = 13
        Caption = 'Tamanho:'
      end
      object EdEmpCNPJIni: TdmkEdit
        Left = 168
        Top = 40
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'EmpCNPJIni'
        UpdCampo = 'EmpCNPJIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdEmpCNPJTam: TdmkEdit
        Left = 252
        Top = 40
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'EmpCNPJTam'
        UpdCampo = 'EmpCNPJTam'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdFunCPFIni: TdmkEdit
        Left = 168
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FunCPFIni'
        UpdCampo = 'FunCPFIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdFunCPFTam: TdmkEdit
        Left = 252
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FunCPFTam'
        UpdCampo = 'FunCPFTam'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdFunCodIni: TdmkEdit
        Left = 168
        Top = 88
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FunCodIni'
        UpdCampo = 'FunCodIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdFunNomIni: TdmkEdit
        Left = 168
        Top = 112
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FunNomIni'
        UpdCampo = 'FunNomIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EDFunCodTam: TdmkEdit
        Left = 252
        Top = 88
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FunCodTam'
        UpdCampo = 'FunCodTam'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdFunNomTam: TdmkEdit
        Left = 252
        Top = 112
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FunNomTam'
        UpdCampo = 'FunNomTam'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdCtaCodIni: TdmkEdit
        Left = 540
        Top = 40
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CtaCodIni'
        UpdCampo = 'CtaCodIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdCtaCodTam: TdmkEdit
        Left = 624
        Top = 40
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CtaCodTam'
        UpdCampo = 'CtaCodTam'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdMesRefIni: TdmkEdit
        Left = 540
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MesRefIni'
        UpdCampo = 'MesRefIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdMesRefTam: TdmkEdit
        Left = 624
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MesRefTam'
        UpdCampo = 'MesRefTam'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAnoRefIni: TdmkEdit
        Left = 540
        Top = 88
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'AnoRefIni'
        UpdCampo = 'AnoRefIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAnoRefTam: TdmkEdit
        Left = 624
        Top = 88
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'AnoRefTam'
        UpdCampo = 'AnoRefTam'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdValLiqIni: TdmkEdit
        Left = 540
        Top = 112
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ValLiqIni'
        UpdCampo = 'ValLiqIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdValLiqTam: TdmkEdit
        Left = 624
        Top = 112
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ValLiqTam'
        UpdCampo = 'ValLiqTam'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 205
      Width = 784
      Height = 56
      Align = alTop
      Caption = 'Leitura das linhas do arquivo: '
      TabOrder = 2
      object Label17: TLabel
        Left = 8
        Top = 31
        Width = 151
        Height = 13
        Caption = 'Caracter separador de campos: '
      end
      object Label18: TLabel
        Left = 188
        Top = 31
        Width = 94
        Height = 13
        Caption = 'Separador decimal: '
      end
      object EdSeparador: TdmkEdit
        Left = 160
        Top = 27
        Width = 21
        Height = 21
        MaxLength = 1
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = ';'
        QryCampo = 'Separador'
        UpdCampo = 'Separador'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ';'
      end
      object RGUsaSepara: TdmkRadioGroup
        Left = 316
        Top = 15
        Width = 466
        Height = 39
        Align = alRight
        Caption = ' Forma de leitura das linhas do arquivo: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Pela posi'#231#227'o inicial e tamanho dos campos'
          'Pela ordem do campo e caracter separador')
        TabOrder = 1
        QryCampo = 'UsaSepara'
        UpdCampo = 'UsaSepara'
        UpdType = utYes
        OldValor = 0
      end
      object EdFloatSep: TdmkEdit
        Left = 288
        Top = 27
        Width = 21
        Height = 21
        MaxLength = 1
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '.'
        QryCampo = 'FloatSep'
        UpdCampo = 'FloatSep'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '.'
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 261
      Width = 784
      Height = 16
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 4
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 510
        Height = 32
        Caption = 'Configura'#231#227'o de Importa'#231#227'o de Proventos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 510
        Height = 32
        Caption = 'Configura'#231#227'o de Importa'#231#227'o de Proventos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 510
        Height = 32
        Caption = 'Configura'#231#227'o de Importa'#231#227'o de Proventos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 192
    Top = 60
  end
  object QrImportSalCfgCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrImportSalCfgCabBeforeOpen
    AfterOpen = QrImportSalCfgCabAfterOpen
    BeforeClose = QrImportSalCfgCabBeforeClose
    AfterScroll = QrImportSalCfgCabAfterScroll
    SQL.Strings = (
      'SELECT * FROM importsalcfgcab'
      'WHERE Codigo > 0')
    Left = 80
    Top = 61
    object QrImportSalCfgCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImportSalCfgCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrImportSalCfgCabEmpCNPJIni: TIntegerField
      FieldName = 'EmpCNPJIni'
    end
    object QrImportSalCfgCabEmpCNPJTam: TIntegerField
      FieldName = 'EmpCNPJTam'
    end
    object QrImportSalCfgCabFunCPFIni: TIntegerField
      FieldName = 'FunCPFIni'
    end
    object QrImportSalCfgCabFunCPFTam: TIntegerField
      FieldName = 'FunCPFTam'
    end
    object QrImportSalCfgCabFunCodIni: TIntegerField
      FieldName = 'FunCodIni'
    end
    object QrImportSalCfgCabFunCodTam: TIntegerField
      FieldName = 'FunCodTam'
    end
    object QrImportSalCfgCabFunNomIni: TIntegerField
      FieldName = 'FunNomIni'
    end
    object QrImportSalCfgCabFunNomTam: TIntegerField
      FieldName = 'FunNomTam'
    end
    object QrImportSalCfgCabCtaCodIni: TIntegerField
      FieldName = 'CtaCodIni'
    end
    object QrImportSalCfgCabCtaCodTam: TIntegerField
      FieldName = 'CtaCodTam'
    end
    object QrImportSalCfgCabMesRefIni: TIntegerField
      FieldName = 'MesRefIni'
    end
    object QrImportSalCfgCabMesRefTam: TIntegerField
      FieldName = 'MesRefTam'
    end
    object QrImportSalCfgCabAnoRefIni: TIntegerField
      FieldName = 'AnoRefIni'
    end
    object QrImportSalCfgCabAnoRefTam: TIntegerField
      FieldName = 'AnoRefTam'
    end
    object QrImportSalCfgCabValLiqIni: TIntegerField
      FieldName = 'ValLiqIni'
    end
    object QrImportSalCfgCabValLiqTam: TIntegerField
      FieldName = 'ValLiqTam'
    end
    object QrImportSalCfgCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrImportSalCfgCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrImportSalCfgCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrImportSalCfgCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrImportSalCfgCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrImportSalCfgCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrImportSalCfgCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrImportSalCfgCabSeparador: TWideStringField
      FieldName = 'Separador'
      Size = 1
    end
    object QrImportSalCfgCabUsaSepara: TSmallintField
      FieldName = 'UsaSepara'
    end
    object QrImportSalCfgCabFloatSep: TWideStringField
      FieldName = 'FloatSep'
      Size = 1
    end
  end
  object DsImportSalCfgCab: TDataSource
    DataSet = QrImportSalCfgCab
    Left = 108
    Top = 61
  end
  object QrImportSalCfgCta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.Nome NO_CTA, scc.* '
      'FROM importsalcfgcta scc'
      'LEFT JOIN contas cta ON cta.Codigo=scc.Genero '
      'WHERE scc.Codigo =:P0'
      'ORDER BY Referencia')
    Left = 136
    Top = 61
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrImportSalCfgCtaNO_CTA: TWideStringField
      FieldName = 'NO_CTA'
      Size = 50
    end
    object QrImportSalCfgCtaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImportSalCfgCtaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrImportSalCfgCtaGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrImportSalCfgCtaReferencia: TWideStringField
      FieldName = 'Referencia'
      Required = True
      Size = 60
    end
    object QrImportSalCfgCtaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrImportSalCfgCtaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrImportSalCfgCtaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrImportSalCfgCtaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrImportSalCfgCtaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrImportSalCfgCtaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrImportSalCfgCtaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsImportSalCfgCta: TDataSource
    DataSet = QrImportSalCfgCta
    Left = 164
    Top = 61
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 444
    Top = 352
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 312
    Top = 356
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
end
