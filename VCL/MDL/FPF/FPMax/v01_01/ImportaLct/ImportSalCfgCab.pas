unit ImportSalCfgCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmImportSalCfgCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB1: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrImportSalCfgCab: TmySQLQuery;
    DsImportSalCfgCab: TDataSource;
    QrImportSalCfgCta: TmySQLQuery;
    DsImportSalCfgCta: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrImportSalCfgCabCodigo: TIntegerField;
    GroupBox3: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    EdEmpCNPJIni: TdmkEdit;
    EdEmpCNPJTam: TdmkEdit;
    EdFunCPFIni: TdmkEdit;
    EdFunCPFTam: TdmkEdit;
    EdFunCodIni: TdmkEdit;
    EdFunNomIni: TdmkEdit;
    EDFunCodTam: TdmkEdit;
    EdFunNomTam: TdmkEdit;
    EdCtaCodIni: TdmkEdit;
    EdCtaCodTam: TdmkEdit;
    EdMesRefIni: TdmkEdit;
    EdMesRefTam: TdmkEdit;
    EdAnoRefIni: TdmkEdit;
    EdAnoRefTam: TdmkEdit;
    EdValLiqIni: TdmkEdit;
    EdValLiqTam: TdmkEdit;
    GroupBox1: TGroupBox;
    Label17: TLabel;
    EdSeparador: TdmkEdit;
    RGUsaSepara: TdmkRadioGroup;
    Panel6: TPanel;
    QrImportSalCfgCabNome: TWideStringField;
    QrImportSalCfgCabEmpCNPJIni: TIntegerField;
    QrImportSalCfgCabEmpCNPJTam: TIntegerField;
    QrImportSalCfgCabFunCPFIni: TIntegerField;
    QrImportSalCfgCabFunCPFTam: TIntegerField;
    QrImportSalCfgCabFunCodIni: TIntegerField;
    QrImportSalCfgCabFunCodTam: TIntegerField;
    QrImportSalCfgCabFunNomIni: TIntegerField;
    QrImportSalCfgCabFunNomTam: TIntegerField;
    QrImportSalCfgCabCtaCodIni: TIntegerField;
    QrImportSalCfgCabCtaCodTam: TIntegerField;
    QrImportSalCfgCabMesRefIni: TIntegerField;
    QrImportSalCfgCabMesRefTam: TIntegerField;
    QrImportSalCfgCabAnoRefIni: TIntegerField;
    QrImportSalCfgCabAnoRefTam: TIntegerField;
    QrImportSalCfgCabValLiqIni: TIntegerField;
    QrImportSalCfgCabValLiqTam: TIntegerField;
    QrImportSalCfgCabLk: TIntegerField;
    QrImportSalCfgCabDataCad: TDateField;
    QrImportSalCfgCabDataAlt: TDateField;
    QrImportSalCfgCabUserCad: TIntegerField;
    QrImportSalCfgCabUserAlt: TIntegerField;
    QrImportSalCfgCabAlterWeb: TSmallintField;
    QrImportSalCfgCabAtivo: TSmallintField;
    QrImportSalCfgCabSeparador: TWideStringField;
    QrImportSalCfgCabUsaSepara: TSmallintField;
    QrImportSalCfgCtaNO_CTA: TWideStringField;
    QrImportSalCfgCtaCodigo: TIntegerField;
    QrImportSalCfgCtaControle: TIntegerField;
    QrImportSalCfgCtaGenero: TIntegerField;
    QrImportSalCfgCtaReferencia: TWideStringField;
    QrImportSalCfgCtaLk: TIntegerField;
    QrImportSalCfgCtaDataCad: TDateField;
    QrImportSalCfgCtaDataAlt: TDateField;
    QrImportSalCfgCtaUserCad: TIntegerField;
    QrImportSalCfgCtaUserAlt: TIntegerField;
    QrImportSalCfgCtaAlterWeb: TSmallintField;
    QrImportSalCfgCtaAtivo: TSmallintField;
    Label18: TLabel;
    EdFloatSep: TdmkEdit;
    QrImportSalCfgCabFloatSep: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrImportSalCfgCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrImportSalCfgCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrImportSalCfgCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrImportSalCfgCabBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraImportSalCfgCta(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure ReopenImportSalCfgCta(Controle: Integer);

  end;

var
  FmImportSalCfgCab: TFmImportSalCfgCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ImportSalCfgCta;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmImportSalCfgCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmImportSalCfgCab.MostraImportSalCfgCta(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmImportSalCfgCta, FmImportSalCfgCta, afmoNegarComAviso) then
  begin
    FmImportSalCfgCta.ImgTipo.SQLType := SQLType;
    FmImportSalCfgCta.FQrCab := QrImportSalCfgCab;
    FmImportSalCfgCta.FDsCab := DsImportSalCfgCab;
    FmImportSalCfgCta.FQrIts := QrImportSalCfgCta;
    if SQLType = stIns then
      //
    else
    begin
      FmImportSalCfgCta.EdControle.ValueVariant := QrImportSalCfgCtaControle.Value;
      //
      FmImportSalCfgCta.EdGenero.ValueVariant := QrImportSalCfgCtaGenero.Value;
      FmImportSalCfgCta.CBGenero.KeyValue     := QrImportSalCfgCtaGenero.Value;
      //
      FmImportSalCfgCta.EdReferencia.Text := QrImportSalCfgCtaReferencia.Value;
      //
    end;
    FmImportSalCfgCta.ShowModal;
    FmImportSalCfgCta.Destroy;
  end;
end;

procedure TFmImportSalCfgCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrImportSalCfgCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrImportSalCfgCab, QrImportSalCfgCta);
end;

procedure TFmImportSalCfgCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrImportSalCfgCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrImportSalCfgCta);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrImportSalCfgCta);
end;

procedure TFmImportSalCfgCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrImportSalCfgCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmImportSalCfgCab.DefParams;
begin
  VAR_GOTOTABELA := 'importsalcfgcab';
  VAR_GOTOMYSQLTABLE := QrImportSalCfgCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM importsalcfgcab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmImportSalCfgCab.ItsAltera1Click(Sender: TObject);
begin
  MostraImportSalCfgCta(stUpd);
end;

procedure TFmImportSalCfgCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MensagemBox('A��o n�o implementada! Solicite � Dermatek:' + #13#10 +
  Caption + #13#10 + TMenuItem(Sender).Name, 'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmImportSalCfgCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmImportSalCfgCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmImportSalCfgCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'ImportSalCfgCta', 'Controle', QrImportSalCfgCtaControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrImportSalCfgCta,
      QrImportSalCfgCtaControle, QrImportSalCfgCtaControle.Value);
    ReopenImportSalCfgCta(Controle);
  end;
end;

procedure TFmImportSalCfgCab.ReopenImportSalCfgCta(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrImportSalCfgCta, Dmod.MyDB, [
  'SELECT cta.Nome NO_CTA, scc.*  ',
  'FROM importsalcfgcta scc ',
  'LEFT JOIN contas cta ON cta.Codigo=scc.Genero  ',
  'WHERE scc.Codigo=' + Geral.FF0(QrImportSalCfgCabCodigo.Value),
  'ORDER BY Referencia ',
  '']);
  //
  QrImportSalCfgCta.Locate('Controle', Controle, []);
end;


procedure TFmImportSalCfgCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmImportSalCfgCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmImportSalCfgCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmImportSalCfgCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmImportSalCfgCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmImportSalCfgCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmImportSalCfgCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrImportSalCfgCabCodigo.Value;
  Close;
end;

procedure TFmImportSalCfgCab.ItsInclui1Click(Sender: TObject);
begin
  MostraImportSalCfgCta(stIns);
end;

procedure TFmImportSalCfgCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrImportSalCfgCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'importsalcfgcab');
end;

procedure TFmImportSalCfgCab.BtConfirmaClick(Sender: TObject);
var
  Nome, Separador, FloatSep: String;
  Codigo, EmpCNPJIni, EmpCNPJTam, FunCPFIni, FunCPFTam, FunCodIni, FunCodTam,
  FunNomIni, FunNomTam, CtaCodIni, CtaCodTam, MesRefIni, MesRefTam, AnoRefIni,
  AnoRefTam, ValLiqIni, ValLiqTam, UsaSepara: Integer;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  EmpCNPJIni     := EdEmpCNPJIni.ValueVariant;
  EmpCNPJTam     := EdEmpCNPJTam.ValueVariant;
  FunCPFIni      := EdFunCPFIni.ValueVariant;
  FunCPFTam      := EdFunCPFTam.ValueVariant;
  FunCodIni      := EdFunCodIni.ValueVariant;
  FunCodTam      := EdFunCodTam.ValueVariant;
  FunNomIni      := EdFunNomIni.ValueVariant;
  FunNomTam      := EdFunNomTam.ValueVariant;
  CtaCodIni      := EdCtaCodIni.ValueVariant;
  CtaCodTam      := EdCtaCodTam.ValueVariant;
  MesRefIni      := EdMesRefIni.ValueVariant;
  MesRefTam      := EdMesRefTam.ValueVariant;
  AnoRefIni      := EdAnoRefIni.ValueVariant;
  AnoRefTam      := EdAnoRefTam.ValueVariant;
  ValLiqIni      := EdValLiqIni.ValueVariant;
  ValLiqTam      := EdValLiqTam.ValueVariant;
  Separador      := EdSeparador.ValueVariant;
  UsaSepara      := RGUsaSepara.ItemIndex;
  FloatSep      := EdFloatSep.ValueVariant;
  //
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('importsalcfgcab', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrImportSalCfgCabCodigo.Value);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'importsalcfgcab', False, [
  'Nome', 'EmpCNPJIni', 'EmpCNPJTam',
  'FunCPFIni', 'FunCPFTam', 'FunCodIni',
  'FunCodTam', 'FunNomIni', 'FunNomTam',
  'CtaCodIni', 'CtaCodTam', 'MesRefIni',
  'MesRefTam', 'AnoRefIni', 'AnoRefTam',
  'ValLiqIni', 'ValLiqTam', 'Separador',
  'UsaSepara', 'FloatSep'], [
  'Codigo'], [
  Nome, EmpCNPJIni, EmpCNPJTam,
  FunCPFIni, FunCPFTam, FunCodIni,
  FunCodTam, FunNomIni, FunNomTam,
  CtaCodIni, CtaCodTam, MesRefIni,
  MesRefTam, AnoRefIni, AnoRefTam,
  ValLiqIni, ValLiqTam, Separador,
  UsaSepara, FloatSep], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    LocCod(Codigo, Codigo);
    GOTOy.BotoesSb(ImgTipo.SQLType);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmImportSalCfgCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'importsalcfgcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'importsalcfgcab', 'Codigo');
end;

procedure TFmImportSalCfgCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmImportSalCfgCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmImportSalCfgCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  Panel6.Align  := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmImportSalCfgCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrImportSalCfgCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmImportSalCfgCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmImportSalCfgCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrImportSalCfgCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmImportSalCfgCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmImportSalCfgCab.QrImportSalCfgCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmImportSalCfgCab.QrImportSalCfgCabAfterScroll(DataSet: TDataSet);
begin
  ReopenImportSalCfgCta(0);
end;

procedure TFmImportSalCfgCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrImportSalCfgCabCodigo.Value <> FCabIni then Geral.MensagemBox(
    'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
end;

procedure TFmImportSalCfgCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrImportSalCfgCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'importsalcfgcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmImportSalCfgCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmImportSalCfgCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrImportSalCfgCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'importsalcfgcab');
end;

procedure TFmImportSalCfgCab.QrImportSalCfgCabBeforeClose(DataSet: TDataSet);
begin
  QrImportSalCfgCta.Close;
end;

procedure TFmImportSalCfgCab.QrImportSalCfgCabBeforeOpen(DataSet: TDataSet);
begin
  QrImportSalCfgCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

