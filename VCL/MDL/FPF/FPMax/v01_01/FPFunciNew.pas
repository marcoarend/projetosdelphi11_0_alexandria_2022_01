unit FPFunciNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DB, mySQLDbTables,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, Variants, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmFPFunciNew = class(TForm)
    Panel1: TPanel;
    QrFunci: TmySQLQuery;
    QrFunciCodigo: TIntegerField;
    QrFunciNOMEFUNCI: TWideStringField;
    DsFunci: TDataSource;
    QrEmpresa: TmySQLQuery;
    QrEmpresaCodigo: TIntegerField;
    QrEmpresaCliInt: TIntegerField;
    QrEmpresaNOMEEMPRESA: TWideStringField;
    DsEmpresa: TDataSource;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label5: TLabel;
    LaCadastro: TLabel;
    EdCodigo: TdmkEditCB;
    CBCodigo: TdmkDBLookupComboBox;
    Label6: TLabel;
    EdFunci: TdmkEdit;
    Label1: TLabel;
    EdChapa: TdmkEdit;
    QrPesq: TmySQLQuery;
    QrPesqFunci: TIntegerField;
    QrPesqChapa: TWideStringField;
    SpeedButton1: TSpeedButton;
    QrMax: TmySQLQuery;
    QrMaxFunci: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    LaNome: TLabel;
    EdNome: TdmkEdit;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdCodigoChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdNomeChange(Sender: TObject);
    procedure EdFunciKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure VerificaCodigo();
  public
    { Public declarations }
  end;

  var
  FmFPFunciNew: TFmFPFunciNew;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, Principal, ModuleGeral;

{$R *.DFM}

procedure TFmFPFunciNew.BtOKClick(Sender: TObject);
var
  Funci, Empresa, Codigo: Integer;
begin
  VAR_CODIGO := 0;
  Funci   := Geral.IMV(EdFunci.Text);
  Codigo  := Geral.IMV(EdCodigo.Text);
  Empresa := Geral.IMV(EdEmpresa.Text);
  if Empresa <> 0 then
    Empresa := QrEmpresaCodigo.Value;
  //
  if (Funci=0) or (Empresa=0) or (Codigo=0) then
  begin
    Geral.MensagemBox('A empresa, o funcion�rio e o c�digo do ' +
    'funcion�rio devem ser informados!', 'Erro', MB_OK+MB_ICONERROR);
    if Empresa = 0 then EdEmpresa.SetFocus else
    if Codigo = 0 then EdCodigo.SetFocus else
    if Funci = 0 then EdFunci.SetFocus;
    Exit;
  end;
  //
  {
  QrPesq1.Close;
  QrPesq1.Params[00].AsInteger := Empresa;
  QrPesq1.Params[01].AsInteger := Funci;
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    Geral.MensagemBox('O c�digo "' + IntToStr(Funci) + '" j� est� ' +
    'cadastrado para o funcion�rio n�mero ' + IntToStr(QrPesq1Codigo.Value) + '!'),
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  }
  //
  if UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'fpfunci', False,
  ['Funci', 'Chapa'],
  ['Empresa', 'Codigo'],
  ['Funci', 'Chapa'],
  [Funci, EdChapa.Text],
  [Empresa, Codigo], 
  [Funci, EdChapa.Text], True) then
  begin
    VAR_CADASTRO := Codigo;
    Close;
  end;
end;

procedure TFmFPFunciNew.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPFunciNew.EdCodigoChange(Sender: TObject);
begin
  VerificaCodigo();
end;

procedure TFmFPFunciNew.EdEmpresaChange(Sender: TObject);
var
  Empresa: Integer;
begin
  Empresa := Geral.IMV(EdEmpresa.Text);
  if Empresa <> 0 then Empresa := QrEmpresaCodigo.Value;
  //
  EdCodigo.Text := '';
  CBCodigo.KeyValue := NULL;
  QrFunci.Close;
  if Empresa > 0 then
  begin
    QrFunci.Close;
    QrFunci.SQL.Clear;
    QrFunci.SQL.Add('SELECT en.Codigo,');
    QrFunci.SQL.Add('IF(en.Nome="", en.RazaoSocial, en.Nome) NOMEFUNCI');
    QrFunci.SQL.Add('FROM entidades en');
    QrFunci.SQL.Add('WHERE ' + VAR_FP_FUNCION);
    {
    QrFunci.SQL.Add('AND NOT (Codigo IN (');
    QrFunci.SQL.Add('  SELECT Codigo FROM fpfunci');
    QrFunci.SQL.Add('  WHERE Empresa=' + IntToStr(Empresa));
    QrFunci.SQL.Add('  AND DataDemiss=0))');
    }
    QrFunci.SQL.Add('ORDER BY NOMEFUNCI');
    //
    QrFunci.Open;
  end;
  //
  VerificaCodigo();
end;

procedure TFmFPFunciNew.EdFunciKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Empresa: Integer;
begin
  if Key = VK_F4 then
  begin
    Empresa := Geral.IMV(EdEmpresa.Text);
    if Empresa = 0 then
    begin
      Geral.MensagemBox('Informe a empresa!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end else Empresa := QrEmpresaCodigo.Value;
    QrMax.Close;
    QrMax.Params[0].AsInteger := Empresa;
    QrMax.Open;
    EdFunci.Text := IntToStr(QrMaxFunci.Value + 1);
  end;
end;

procedure TFmFPFunciNew.EdNomeChange(Sender: TObject);
begin
  if Trim(EdNome.Text) <> '' then
  begin
    LaNome.Visible := True;
    EdNome.Visible := True;
  end;
end;

procedure TFmFPFunciNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if EdEmpresa.ValueVariant <> 0 then
  begin
    if EdCodigo.ValueVariant <> 0 then
    begin
      if EdFunci.ValueVariant <> 0 then
        EdChapa.SetFocus
      else EdFunci.SetFocus;
    end else EdCodigo.SetFocus;
  end else EdEmpresa.SetFocus;
end;

procedure TFmFPFunciNew.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrEmpresa.Open;
end;

procedure TFmFPFunciNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFPFunciNew.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  DModG.CadastroDeEntidade(EdCodigo.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdCodigo, CBCodigo, QrFunci,
    VAR_CADASTRO, 'Codigo', 'Codigo');
  end;
end;

procedure TFmFPFunciNew.VerificaCodigo();
var
  Empresa, Codigo: Integer;
begin
  Empresa := EdEmpresa.ValueVariant;
  if Empresa <> 0 then
    Empresa := QrEmpresaCodigo.Value;
  Codigo := EdCodigo.ValueVariant;
  if (Empresa <> 0) and (Codigo <> 0) then
  begin
    QrPesq.Close;
    QrPesq.Params[00].AsInteger := Empresa;
    QrPesq.Params[01].AsInteger := Codigo;
    QrPesq.Open;
    //
    if QrPesq.RecordCount > 0 then
    begin
      if EdFunci.ValueVariant = 0 then
        EdFunci.ValueVariant := QrPesqFunci.Value;
      if Trim(EdChapa.Text) = '' then
        EdChapa.Text := QrPesqChapa.Value;
    end;
  end;
end;

end.
