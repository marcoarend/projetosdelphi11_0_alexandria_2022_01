object FmFPtIRRF: TFmFPtIRRF
  Left = 339
  Top = 185
  Width = 380
  Height = 319
  Caption = 'Desconto IRRF'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 237
    Width = 372
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 260
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa�da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object BtAcao: TBitBtn
      Tag = 294
      Left = 2
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&A��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtAcaoClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 372
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Desconto IRRF'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 368
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 372
    Height = 189
    Align = alClient
    TabOrder = 0
    object dmkDBGridDAC1: TdmkDBGridDAC
      Left = 1
      Top = 1
      Width = 370
      Height = 187
      SQLFieldsToChange.Strings = (
        'Teto'
        'Aliquota'
        'Deducao')
      SQLIndexesOnUpdate.Strings = (
        'Teto')
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Teto'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Aliquota'
          Title.Caption = 'Al�quota'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Deducao'
          Title.Caption = 'Dedu��o'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ativo'
          Width = 30
          Visible = True
        end>
      Color = clWindow
      DataSource = DsFPtIRRF
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dmkDBGridDAC1CellClick
      SQLTable = 'FPtIRRF'
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Teto'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Aliquota'
          Title.Caption = 'Al�quota'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Deducao'
          Title.Caption = 'Dedu��o'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ativo'
          Width = 30
          Visible = True
        end>
    end
  end
  object DsFPtIRRF: TDataSource
    DataSet = QrFPtIRRF
    Left = 168
    Top = 176
  end
  object QrFPtIRRF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM fptirrf'
      'ORDER BY Teto')
    Left = 140
    Top = 176
    object QrFPtIRRFTeto: TFloatField
      FieldName = 'Teto'
      DisplayFormat = '#,###,##0.00'
    end
    object QrFPtIRRFAliquota: TFloatField
      FieldName = 'Aliquota'
      DisplayFormat = '#,###,##0.00'
    end
    object QrFPtIRRFDeducao: TFloatField
      FieldName = 'Deducao'
      DisplayFormat = '#,###,##0.00'
    end
    object QrFPtIRRFAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object PMAcao: TPopupMenu
    Left = 104
    Top = 249
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
  end
end

