unit FPPonto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Grids, DBGrids,
  DBCtrls,    Db, mySQLDbTables, ComCtrls,
  dmkEdit,  dmkDBGrid, Mask, Menus, LMDCustomCheckBox, LMDCheckBox,
  LMDButtonControl;

type
  TFmFPPonto = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    QrFunci: TmySQLQuery;
    DsFunci: TDataSource;
    QrEmpresa: TmySQLQuery;
    DsEmpresa: TDataSource;
    LaTipo: TLabel;
    QrFPPontoHor: TmySQLQuery;
    DsFPPontoHor: TDataSource;
    dmkDBGrid1: TdmkDBGrid;
    QrSumHr: TmySQLQuery;
    QrSumHrHoras: TFloatField;
    QrSumHrFatHr: TFloatField;
    DsSumHr: TDataSource;
    QrFunciCodigo: TIntegerField;
    QrFunciHEFator: TFloatField;
    QrFunciFunci: TIntegerField;
    QrFunciNOMEFUNCI: TWideStringField;
    PMExclui: TPopupMenu;
    ExcluiitemAtual1: TMenuItem;
    ExcluiitensSelecionados1: TMenuItem;
    ExcluiTodositens1: TMenuItem;
    QrFPPontoHorControle: TIntegerField;
    QrFPPontoHorCodigo: TIntegerField;
    QrFPPontoHorEmpresa: TIntegerField;
    QrFPPontoHorDataP: TDateField;
    QrFPPontoHorTur1Ent: TTimeField;
    QrFPPontoHorTur1DeI: TTimeField;
    QrFPPontoHorTur1DeF: TTimeField;
    QrFPPontoHorTur1Sai: TTimeField;
    QrFPPontoHorTur2Ent: TTimeField;
    QrFPPontoHorTur2DeI: TTimeField;
    QrFPPontoHorTur2DeF: TTimeField;
    QrFPPontoHorTur2Sai: TTimeField;
    QrFPPontoHorHorasANCar: TFloatField;
    QrFPPontoHorHorasANCal: TFloatField;
    QrFPPontoHorHorasHETot: TFloatField;
    QrFPPontoHorHorasHEDia: TFloatField;
    QrFPPontoHorHorasHENot: TFloatField;
    QrFPPontoHorHorasHEDom: TFloatField;
    QrFPPontoHorHorasHEFer: TFloatField;
    QrFPPontoHorHrBancoTra: TFloatField;
    QrFPPontoHorHrBancoGoz: TFloatField;
    QrFPPontoHorHrFaltaJust: TFloatField;
    QrFPPontoHorHrFaltaInju: TFloatField;
    QrFPPontoHorFator: TFloatField;
    QrFPPontoHorLk: TIntegerField;
    QrFPPontoHorDataCad: TDateField;
    QrFPPontoHorDataAlt: TDateField;
    QrFPPontoHorUserCad: TIntegerField;
    QrFPPontoHorUserAlt: TIntegerField;
    QrFPPontoHorNOMEEMP: TWideStringField;
    QrFPPontoHorNOMEFUN: TWideStringField;
    PnTudo: TPanel;
    Panel4: TPanel;
    Panel6: TPanel;
    QrFPFunciHor: TmySQLQuery;
    DsFPFunciHor: TDataSource;
    QrFPFunciHorCodigo: TIntegerField;
    QrFPFunciHorControle: TIntegerField;
    QrFPFunciHorTur1Ent: TTimeField;
    QrFPFunciHorTur1DeI: TTimeField;
    QrFPFunciHorTur1DeF: TTimeField;
    QrFPFunciHorTur1Sai: TTimeField;
    QrFPFunciHorTur2Ent: TTimeField;
    QrFPFunciHorTur2DeI: TTimeField;
    QrFPFunciHorTur2DeF: TTimeField;
    QrFPFunciHorTur2Sai: TTimeField;
    QrFPFunciHorLk: TIntegerField;
    QrFPFunciHorDataCad: TDateField;
    QrFPFunciHorDataAlt: TDateField;
    QrFPFunciHorUserCad: TIntegerField;
    QrFPFunciHorUserAlt: TIntegerField;
    QrFPFunciHorHORARIO: TWideStringField;
    QrFeriados: TmySQLQuery;
    QrFeriadosData: TDateField;
    QrFunciPriDdPonto: TSmallintField;
    QrFPPontoHorPeriodo: TIntegerField;
    QrFPPontoHorHrAlimenta: TFloatField;
    Panel16: TPanel;
    Panel17: TPanel;
    Panel5: TPanel;
    Label5: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    EdEmpresa: TLMDEdit;
    CBEmpresa: TDBLookupComboBox;
    CBCodigo: TDBLookupComboBox;
    EdCodigo: TLMDEdit;
    CkPonto: TCheckBox;
    Panel7: TPanel;
    GroupBox1: TGroupBox;
    TPIni: TDateTimePicker;
    TPFim: TDateTimePicker;
    CkIni: TCheckBox;
    CkFim: TCheckBox;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Panel18: TPanel;
    Panel9: TPanel;
    BtOK: TBitBtn;
    Panel2: TPanel;
    PnConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel3: TPanel;
    BtDesiste: TBitBtn;
    PnInclui: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    Panel10: TPanel;
    BtSaida: TBitBtn;
    PnEdita2: TPanel;
    Panel36: TPanel;
    GroupBox12: TGroupBox;
    Label133: TLabel;
    Label134: TLabel;
    Label135: TLabel;
    Label136: TLabel;
    dmkEdTur1Ent: TdmkEdit;
    dmkEdTur1DeI: TdmkEdit;
    dmkEdTur1DeF: TdmkEdit;
    dmkEdTur1Sai: TdmkEdit;
    Panel37: TPanel;
    Label142: TLabel;
    Label143: TLabel;
    Label148: TLabel;
    Label149: TLabel;
    LaTur1HrT: TLabel;
    LaTur1HrE: TLabel;
    LaTur1HrN: TLabel;
    LaTur1HrB: TLabel;
    Label152: TLabel;
    Label154: TLabel;
    Label156: TLabel;
    LaTur1HrS: TLabel;
    Label158: TLabel;
    LaTur1HrC: TLabel;
    Label159: TLabel;
    Label162: TLabel;
    Label163: TLabel;
    dmkEdTur1HrT: TdmkEdit;
    dmkEdTur1HrE: TdmkEdit;
    dmkEdTur1HrN: TdmkEdit;
    dmkEdTur1HrB: TdmkEdit;
    dmkEdTur1HrS: TdmkEdit;
    dmkEdTur1HrC: TdmkEdit;
    Panel38: TPanel;
    Label144: TLabel;
    LaTur1HrA: TLabel;
    dmkEdTur1HrA: TdmkEdit;
    PnEdita1: TPanel;
    PnAlteracao: TPanel;
    Panel8: TPanel;
    Label8: TLabel;
    dmkEdControle: TdmkEdit;
    Panel12: TPanel;
    Label3: TLabel;
    dmkEdHoras: TdmkEdit;
    RGFator: TRadioGroup;
    CkContinua: TCheckBox;
    PnInclusao: TPanel;
    Panel14: TPanel;
    LBTurnos: TDBLookupListBox;
    Panel15: TPanel;
    Label9: TLabel;
    Label1: TLabel;
    Label16: TLabel;
    TPDataIni: TDateTimePicker;
    dmkEdPeriodo: TdmkEdit;
    Edit1: TEdit;
    Panel11: TPanel;
    Panel13: TPanel;
    PnCalenTitulo: TPanel;
    PnCalendar: TPanel;
    Grade: TDrawGrid;
    QrFPFunciHorTur1HrN: TTimeField;
    QrFPFunciHorTur1HrE: TTimeField;
    QrFPFunciHorTur1HrA: TTimeField;
    QrFPFunciHorTur2HrN: TTimeField;
    QrFPFunciHorTur2HrE: TTimeField;
    QrFPFunciHorTur2HrA: TTimeField;
    QrFPFunciHorTur1HrT: TTimeField;
    QrFPFunciHorTur1HrS: TTimeField;
    QrFPFunciHorTur1HrB: TTimeField;
    QrFPFunciHorTur2HrT: TTimeField;
    QrFPFunciHorTur2HrS: TTimeField;
    QrFPFunciHorTur2HrB: TTimeField;
    QrFPFunciHorTur1HrC: TTimeField;
    QrFPFunciHorTur2HrC: TTimeField;
    QrFunciANMinHr: TTimeField;
    QrFunciANIniHr: TTimeField;
    QrFunciANFimHr: TTimeField;
    Memo1: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdEmpresaExit(Sender: TObject);
    procedure CBEmpresaClick(Sender: TObject);
    procedure CBEmpresaDropDown(Sender: TObject);
    procedure EdCodigoChange(Sender: TObject);
    procedure EdCodigoExit(Sender: TObject);
    procedure CBCodigoClick(Sender: TObject);
    procedure CBCodigoDropDown(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrFPPontoHorBeforeClose(DataSet: TDataSet);
    procedure QrFPPontoHorAfterOpen(DataSet: TDataSet);
    procedure ExcluiitemAtual1Click(Sender: TObject);
    procedure ExcluiitensSelecionados1Click(Sender: TObject);
    procedure ExcluiTodositens1Click(Sender: TObject);
    procedure QrFPFunciHorCalcFields(DataSet: TDataSet);
    procedure LBTurnosClick(Sender: TObject);
    procedure dmkEdPeriodoChange(Sender: TObject);
    procedure dmkEdPeriodoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TPDataIniChange(Sender: TObject);
    procedure CkPontoClick(Sender: TObject);
    procedure dmkEdTur1EntExit(Sender: TObject);
  private
    { Private declarations }
    FListaCk: array[0..30] of TCheckBox;
    FListaDS: array[0..6] of TButton;
    FListaOu: array[0..6] of TButton;
    FTL, FL0, FL6: Integer;
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure ReabreFPPonto(Controle: Integer);
    function ExcluiItens(Tipo: TSelType): Boolean;
    procedure ReopenFPFunciHor(Controle: Integer);
    procedure ReorganizaDias;
    procedure ButtonXClick(Sender: TObject);
    procedure ButtonYClick(Sender: TObject);
    procedure MudaPeriodo(Key: Word);
    procedure DefineDataIni;
    procedure ConfereHora1;
  public
    { Public declarations }
  end;

  var
  FmFPPonto: TFmFPPonto;

implementation

uses UnInternalConsts, Module, UnGOTOy, UMySQLModule, UnSalario;

{$R *.DFM}

procedure TFmFPPonto.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPPonto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPPonto.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFPPonto.EdEmpresaChange(Sender: TObject);
var
  Empresa: Integer;
begin
  MLAGeral.SincroI(FmFPPonto, Sender, 0, siNegativo);
  Empresa := Geral.IMV(EdEmpresa.Text);
  //
  EdCodigo.Text := '';
  CBCodigo.KeyValue := NULL;
  QrFunci.Close;
  if Empresa > 0 then
  begin
    QrFunci.Close;
    QrFunci.SQL.Clear;
    QrFunci.SQL.Add('SELECT en.Codigo, fu.HEFator, fu.Funci, fu.PriDdPonto, ');
    QrFunci.SQL.Add('IF(en.Nome="", en.RazaoSocial, en.Nome) NOMEFUNCI, ');
    QrFunci.SQL.Add('fu.ANMinHr, fu.ANIniHr, fu.ANFimHr ');
    QrFunci.SQL.Add('FROM entidades en');
    QrFunci.SQL.Add('LEFT JOIN fpfunci fu ON fu.Codigo=en.Codigo');
    QrFunci.SQL.Add('WHERE ' + VAR_FP_FUNCION);
    QrFunci.SQL.Add('AND DataDemiss =0');
    if CkPonto.Checked then
      QrFunci.SQL.Add('AND PriDdPonto>0');
    QrFunci.SQL.Add('AND (fu.Codigo IN (');
    QrFunci.SQL.Add('  SELECT Codigo FROM fpfunci');
    QrFunci.SQL.Add('  WHERE Empresa=' + IntToStr(Empresa));
    QrFunci.SQL.Add('  AND DataDemiss=0))');
    QrFunci.SQL.Add('ORDER BY NOMEFUNCI');
    QrFunci.Open;
  end;
end;

procedure TFmFPPonto.EdEmpresaExit(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPPonto, Sender, 1, siNegativo);
end;

procedure TFmFPPonto.CBEmpresaClick(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPPonto, Sender, 2, siNegativo);
end;

procedure TFmFPPonto.CBEmpresaDropDown(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPPonto, Sender, 3, siNegativo);
end;

procedure TFmFPPonto.EdCodigoChange(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPPonto, Sender, 0, siNegativo);
  //
  ReopenFPFunciHor(0);
  //
  DefineDataIni;
end;

procedure TFmFPPonto.EdCodigoExit(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPPonto, Sender, 1, siNegativo);
end;

procedure TFmFPPonto.CBCodigoClick(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPPonto, Sender, 2, siNegativo);
end;

procedure TFmFPPonto.CBCodigoDropDown(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPPonto, Sender, 3, siNegativo);
end;

procedure TFmFPPonto.FormCreate(Sender: TObject);
var
  i: integer;
begin
  // Top Left do calend�rio no Panel
  FTL := 2;
  //
  // Bot�es de dias da semana
  for i := 0 to 6 do
  begin
    FListaDS[i] := TButton.Create(Self);
    with FListaDS[i] do
    begin
      Parent := PnCalendar;
      Height := Grade.DefaultRowHeight;
      Width  := Grade.DefaultColWidth;
      Top  := 2;
      Left := 2 + ((i+1) * (Grade.DefaultColWidth  + 1));
      Caption := MLAGeral.DiaDaSemana(i + 1, 3);
      OnClick := ButtonXClick;
    end;
  end;
  FL0 := FListaDS[0].Left;
  FL6 := FListaDS[6].Left;
  //
  // Bot�es laterais
  for i := 0 to 6 do
  begin
    FListaOu[i] := TButton.Create(Self);
    with FListaOu[i] do
    begin
      Parent := PnCalendar;
      Height := Grade.DefaultRowHeight;
      Width  := Grade.DefaultColWidth;
      Top  := 2 + i * (Grade.DefaultRowHeight + 1);
      Left := 2;
      Caption := intToStr(i);
      OnClick := ButtonYClick;
    end;
  end;
  FListaOu[0].Caption := '�teis';
  //
  // Dias do m�s
  for i := 0 to 30 do
  begin
    FListaCk[i] := TCheckBox.Create(Self);
    with FListaCk[i] do
    begin
      Parent := PnCalendar;
      Font.Style := [fsBold];
      Height := Grade.DefaultRowHeight;
      Width  := Grade.DefaultColWidth;
      Top := 2 + (i div 7 + 1) * (Grade.DefaultRowHeight + 1);
      Left := 2 + (i mod 7 + 1)* (Grade.DefaultColWidth  + 1);
      Caption := FormatFloat('00', i+1);
      //CtlXP := True;
    end;
  end;
  // Fim da cria��o do calend�rio
  //
  PnConfirma.Align := alClient;
  PnTudo.Height    := 300;
  PnInclui.Align   := alClient;
  TPDataIni.Date   := Date;
  TPIni.Date       := Date -30;
  TPFim.Date       := Date;
  //
  QrEmpresa.Close;
  QrEmpresa.SQL.Clear;
  QrEmpresa.SQL.Add('SELECT en.Codigo,');
  QrEmpresa.SQL.Add('IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEEMPRESA');
  QrEmpresa.SQL.Add('FROM entidades en');
  QrEmpresa.SQL.Add('WHERE ' + VAR_FP_EMPRESA);
  QrEmpresa.SQL.Add('ORDER BY NOMEEMPRESA');
  QrEmpresa.Open;
  //QrFunci.Open;
  dmkEdPeriodo.ValueVariant := Geral.Periodo2000(Date);
end;

procedure TFmFPPonto.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PnInclui.Visible    := True;
      PnEdita1.Visible    := False;
      PnEdita2.Visible    := False;
      PnConfirma.Visible  := False;
      PnInclusao.Visible  := False;
      PnAlteracao.Visible := False;
    end;
    1:
    begin
      PnConfirma.Visible := True;
      PnEdita1.Visible   := True;
      PnEdita2.Visible   := True;
      PnInclui.Visible   := False;
      if Status = CO_INCLUSAO then
      begin
        PnInclusao.Visible         := True;
        dmkEdControle.ValueVariant := 0;
        dmkEdHoras.ValueVariant    := 0;
      end else begin
        PnAlteracao.Visible        := True;
        dmkEdControle.ValueVariant := QrFPPontoHorControle.Value;
        //dmkEdHoras.ValueVariant    := QrFPPontoHoras.Value;
        TPDataIni.Date             := Date;
        if Round(QrFPPontoHorFator.Value) = 200 then
          RGFator.ItemIndex        := 1
        else
          RGFator.ItemIndex        := 0;
      end;
      if EdEmpresa.Visible then
        EdEmpresa.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmFPPonto.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmFPPonto.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, CO_INCLUSAO, 0);
end;

procedure TFmFPPonto.BtAlteraClick(Sender: TObject);
begin
  MostraEdicao(1, CO_ALTERACAO, 0);
end;

procedure TFmFPPonto.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmFPPonto.ReabreFPPonto(Controle: Integer);
  procedure AddExtras(Query: TmySQLQuery);
  var
    Codigo, Empresa: Integer;
  begin
    Query.SQL.Add('');
    Query.SQL.Add('FROM fpponto hre');
    Query.SQL.Add('LEFT JOIN entidades emp ON emp.Codigo=hre.Empresa');
    Query.SQL.Add('LEFT JOIN entidades fun ON fun.Codigo=hre.Codigo');
    Query.SQL.Add('');

    Query.SQL.Add(dmkPF.SQL_Periodo('WHERE hre.DataHE ',
      TPIni.Date, TPFim.Date, CkIni.Checked, CkFim.Checked));
    //
    Empresa := Geral.IMV(EdEmpresa.Text);
    if Empresa <> 0 then
      Query.SQL.Add('AND hre.Empresa = ' + FormatFloat('0', Empresa));
    //
    Codigo := Geral.IMV(EdCodigo.Text);
    if Codigo <> 0 then
      Query.SQL.Add('AND hre.Codigo = ' + FormatFloat('0', Codigo));
    //
  end;
begin
  QrFPPontoHor.Close;
  QrFPPontoHor.SQL.Clear;
  QrFPPontoHor.SQL.Add('SELECT hre.*,');
  QrFPPontoHor.SQL.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,');
  QrFPPontoHor.SQL.Add('IF(fun.Tipo=0, fun.RazaoSocial, fun.Nome) NOMEFUN');
  AddExtras(QrFPPontoHor);
  QrFPPontoHor.Open;
  //
  QrFPPontoHor.Locate('Controle', Controle, []);

  QrSumHr.Close;
  QrSumHr.SQL.Clear;
  QrSumHr.SQL.Add('SELECT SUM(hre.Horas) Horas,');
  QrSumHr.SQL.Add('SUM(hre.Horas*hre.Fator/100) FatHr');
  AddExtras(QrSumHr);
  QrSumHr.Open;
end;

procedure TFmFPPonto.BtOKClick(Sender: TObject);
begin
  ReabreFPPonto(QrFPPontoHorControle.Value);
end;

procedure TFmFPPonto.QrFPPontoHorBeforeClose(DataSet: TDataSet);
begin
  BtAltera.Enabled := False;
  BtExclui.Enabled := False;
end;

procedure TFmFPPonto.QrFPPontoHorAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrFPPontoHor.RecordCount > 0;
  BtExclui.Enabled := QrFPPontoHor.RecordCount > 0;
end;

function TFmFPPonto.ExcluiItens(Tipo: TSelType): Boolean;
  function ExcluiItemAtual: Boolean;
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fpponto WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrFPPontoHorControle.Value;
    Dmod.QrUpd.ExecSQL;
    Result := True;
  end;
var
  i, Next: Integer;
begin
  Next := 0;
  Result := True;
  case Tipo of
    istTodos:
    begin
      if Application.MessageBox('Confirma a exclus�o de todos itens pesquisados?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        QrFPPontoHor.First;
        while not QrFPPontoHor.Eof do
        begin
          ExcluiItemAtual;
          QrFPPontoHor.Next;
        end;
        Next := 0;
      end;
    end;
    istSelecionados:
    begin
      if Application.MessageBox('Confirma a exclus�o dos itens selecionados?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        with dmkDBGrid1.DataSource.DataSet do
        for i:= 0 to dmkDBGrid1.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(dmkDBGrid1.SelectedRows.Items[i]));
          ExcluiItemAtual;
        end;
        Next  := UMyMod.ProximoRegistro(QrFPPontoHor, 'Controle', QrFPPontoHorControle.Value);
      end;  
    end;
    istAtual: if Application.MessageBox('Confirma a exclus�o do item selecionado?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      ExcluiItemAtual;
      Next  := UMyMod.ProximoRegistro(QrFPPontoHor, 'Controle', QrFPPontoHorControle.Value);
    end;
  end;
  ReabreFPPonto(Next);
end;

procedure TFmFPPonto.ExcluiitemAtual1Click(Sender: TObject);
begin
  ExcluiItens(istAtual);
end;

procedure TFmFPPonto.ExcluiitensSelecionados1Click(Sender: TObject);
begin
  ExcluiItens(istSelecionados);
end;

procedure TFmFPPonto.ExcluiTodositens1Click(Sender: TObject);
begin
  ExcluiItens(istTodos);
end;

procedure TFmFPPonto.QrFPFunciHorCalcFields(DataSet: TDataSet);
begin
  QrFPFunciHorHORARIO.Value := 'Turno 1 > [' +
    Geral.FDT(QrFPFunciHorTur1Ent.Value, 102) + ' (' +
    Geral.FDT(QrFPFunciHorTur1DeI.Value, 102) + ' - ' +
    Geral.FDT(QrFPFunciHorTur1DeF.Value, 102) + ') ' +
    Geral.FDT(QrFPFunciHorTur1Sai.Value, 102) + '] Turno 2 > ' +

    Geral.FDT(QrFPFunciHorTur2Ent.Value, 102) + ' (' +
    Geral.FDT(QrFPFunciHorTur2DeI.Value, 102) + ' - ' +
    Geral.FDT(QrFPFunciHorTur2DeF.Value, 102) + ') ' +
    Geral.FDT(QrFPFunciHorTur2Sai.Value, 102) + ']';
end;

procedure TFmFPPonto.ReopenFPFunciHor(Controle: Integer);
begin
  QrFPFunciHor.Close;
  QrFPFunciHor.Params[0].AsInteger := Geral.IMV(EdCodigo.Text);
  QrFPFunciHor.Open;
  //
end;

procedure TFmFPPonto.LBTurnosClick(Sender: TObject);
begin
  dmkEdTur1Ent.ValueVariant := QrFPFunciHorTur1Ent.Value;
  dmkEdTur1DeI.ValueVariant := QrFPFunciHorTur1DeI.Value;
  dmkEdTur1DeF.ValueVariant := QrFPFunciHorTur1DeF.Value;
  dmkEdTur1Sai.ValueVariant := QrFPFunciHorTur1Sai.Value;
  //
  dmkEdTur1HrT.ValueVariant := QrFPFunciHorTur1HrT.Value;
  dmkEdTur1HrN.ValueVariant := QrFPFunciHorTur1HrN.Value;
  dmkEdTur1HrS.ValueVariant := QrFPFunciHorTur1HrS.Value;
  dmkEdTur1HrA.ValueVariant := QrFPFunciHorTur1HrA.Value;
  dmkEdTur1HrB.ValueVariant := QrFPFunciHorTur1HrB.Value;
  dmkEdTur1HrE.ValueVariant := QrFPFunciHorTur1HrE.Value;
  dmkEdTur1HrC.ValueVariant := QrFPFunciHorTur1HrC.Value;
  //
  ConfereHora1;
  {
  dmkEdTur2Ent.ValueVariant := QrFPFunciHorTur2Ent.Value;
  dmkEdTur2DeI.ValueVariant := QrFPFunciHorTur2DeI.Value;
  dmkEdTur2DeF.ValueVariant := QrFPFunciHorTur2DeF.Value;
  dmkEdTur2Sai.ValueVariant := QrFPFunciHorTur2Sai.Value;
  }
end;

procedure TFmFPPonto.ReorganizaDias;
var
  Dia, Mes, Ano, DiaI: Word;
  DOW: Word;
  i: Integer;
  Virou: Boolean;
  DataI, DataF: Integer;
begin
  DataI := Trunc(TPDataIni.Date);
  DataF := DataI;
  DOW := DayOfWeek(DataI);
  //
  Virou := False;
  DecodeDate(DataI, Ano, Mes, DiaI);
  for i := 0 to 30 do
  begin
    DecodeDate(DataI + i, Ano, Mes, Dia);
    if (Dia = 1) and (i > 0) then Virou := True;
    if Virou and (Dia >= DiaI) then
    begin
      FListaCk[i].Caption := '00';
      FListaCk[i].Tag     := 0;
      FListaCk[i].Visible := False;
    end else begin
      FListaCk[i].Caption := FormatFloat('00', Dia);
      FListaCk[i].Tag     := DataI + i;
      FListaCk[i].Visible := True;
      DataF := DataI + i;
    end;
    FListaCk[i].Top := 2 + ((i + DOW - 1) div 7 + 1) * (Grade.DefaultRowHeight + 1);
    FListaCk[i].Left := 2 + ((i + DOW - 1) mod 7 + 1)* (Grade.DefaultColWidth  + 1);
    if FListaCk[i].Left = FL0 then FListaCk[i].Font.Color := clSilver else
    if FListaCk[i].Left = FL6 then FListaCk[i].Font.Color := clGray else
                                   FListaCk[i].Font.Color := clNavy;
  end;
  if DiaI = 1 then PnCalenTitulo.Caption := Geral.FDT(DataI, 5)
  else PnCalenTitulo.Caption := Geral.FDT(DataI, 3) + ' a ' + Geral.FDT(DataF, 3);
  QrFeriados.Close;
  QrFeriados.Params[0].AsString := Geral.FDT(DataI, 1);
  QrFeriados.Params[1].AsString := Geral.FDT(DataF, 1);
  QrFeriados.Open;
  while not QrFeriados.Eof do
  begin
    FListaCk[Trunc(QrFeriadosData.Value - DataI)].Font.Color := clRed;
    QrFeriados.Next;
  end;
end;

procedure TFmFPPonto.ButtonXClick(Sender: TObject);
var
  i, k, t, x: Integer;
  OK: Boolean;
begin
  k := 0;
  t := 0;
  x := TButton(Sender).Left;
  for i := 0 to 30 do
  begin
    if FListaCk[i].Left = x then
    begin
      if FListaCk[i].Checked then inc(k, 1);
      if FListaCk[i].Visible then inc(t, 1);
    end;
  end;
  OK := k > t / 2;
  for i := 0 to 30 do
  begin
    if FListaCk[i].Left = x then
      FListaCk[i].Checked := not OK;
  end;
end;

procedure TFmFPPonto.ButtonYClick(Sender: TObject);
var
  i, k, t, x: Integer;
  OK: Boolean;
begin
  k := 0;
  t := 0;
  x := TButton(Sender).Top;
  if x > Grade.DefaultRowHeight then
  begin
    for i := 0 to 30 do
    begin
      if FListaCk[i].Top = x then
      begin
        if FListaCk[i].Checked then inc(k, 1);
        if FListaCk[i].Visible then inc(t, 1);
      end;
    end;
    OK := k > t / 2;
    for i := 0 to 30 do
    begin
      if FListaCk[i].Top = x then
        FListaCk[i].Checked := not OK;
    end;
  end else begin
    for i := 0 to 30 do
    begin
      if (FListaCk[i].Left = FL0) or (FListaCk[i].Left = FL6)
      or (FListaCk[i].Font.Color = clRed) then
        FListaCk[i].Checked := False
      else
        FListaCk[i].Checked := True;
    end;
  end;
end;

procedure TFmFPPonto.dmkEdPeriodoChange(Sender: TObject);
begin
  Edit1.Text := MLAGeral.MesEAnoDoPeriodoLongo(dmkEdPeriodo.ValueVariant);
  DefineDataIni;
end;

procedure TFmFPPonto.dmkEdPeriodoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MudaPeriodo(Key);
end;

procedure TFmFPPonto.MudaPeriodo(Key: Word);
begin
  case key of
    VK_DOWN: dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant - 1;
    VK_UP: dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant + 1;
    VK_PRIOR: dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant - 12;
    VK_NEXT: dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant + 12;
  end;
end;


procedure TFmFPPonto.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MudaPeriodo(Key);
end;

procedure TFmFPPonto.DefineDataIni;
var
  Dia: Word;
  Data: TDateTime;
begin
  Dia := QrFunciPriDDPonto.Value;
  if Dia < 2 then
    Data := MLAGeral.PeriodoToDate(dmkEdPeriodo.ValueVariant, 1)
  else
    Data := MLAGeral.PeriodoToDate(dmkEdPeriodo.ValueVariant - 1, Dia);
  TPDataIni.Date := Data;
  //
  ReorganizaDias;
end;

procedure TFmFPPonto.TPDataIniChange(Sender: TObject);
begin
  ReorganizaDias;
end;

procedure TFmFPPonto.CkPontoClick(Sender: TObject);
begin
  ReopenFPFunciHor(QrFPFunciHorControle.Value);
  //
  DefineDataIni;
end;

procedure TFmFPPonto.ConfereHora1;
begin
  USalario.ConfereHoras(FmFPPonto, 1, QrFunciANMinHr.Value,
    QrFunciANIniHr.Value, QrFunciANFimHr.Value);
end;

procedure TFmFPPonto.dmkEdTur1EntExit(Sender: TObject);
begin
  ConfereHora1;
end;

procedure TFmFPPonto.BtConfirmaClick(Sender: TObject);
var
  i, Codigo, Empresa, Controle, {Turno, }Periodo: Integer;
  Txt, DataP, Tur1Ent, Tur1DeI, Tur1DeF, Tur1Sai,
  Tur1HrT, Tur1HrN, Tur1HrS, Tur1HrA, Tur1HrB, Tur1HrE, Tur1HrC: String;
begin
  Empresa := Geral.IMV(EdEmpresa.Text);
  if Empresa = 0 then
  begin
    Application.MessageBox('Defina a empresa!', 'Aviso', MB_OK+MB_ICONWARNING);
    if EdEmpresa.Visible then
      EdEmpresa.SetFocus;
    Exit;
  end;
  //
  Codigo := Geral.IMV(EdCodigo.Text);
  if Codigo = 0 then
  begin
    Application.MessageBox('Defina o funcion�rio!', 'Aviso', MB_OK+MB_ICONWARNING);
    if EdCodigo.Visible then
      EdCodigo.SetFocus;
    Exit;
  end;
  //
  if LBTurnos.KeyValue = NULL then
  begin
    Application.MessageBox('Defina o turno!', 'Aviso', MB_OK+MB_ICONWARNING);
    if LBTurnos.Visible then
      LBTurnos.SetFocus;
    Exit;
  end;
  //
  Screen.Cursor := crHourGlass;
  //
  //Turno := LBTurnos.KeyValue;
  Periodo := dmkEdPeriodo.ValueVariant;
  Tur1Ent := dmkEdTur1Ent.Text;
  Tur1DeI := dmkEdTur1DeI.Text;
  Tur1Def := dmkEdTur1DeF.Text;
  Tur1Sai := dmkEdTur1Sai.Text;
  (*
  Tur2Ent := '00:00';
  Tur2DeI := '00:00';
  Tur2Def := '00:00';
  Tur2Sai := '00:00';
  *)
  Tur1HrT := dmkEdTur1HrT.Text;
  Tur1HrN := dmkEdTur1HrN.Text;
  Tur1HrS := dmkEdTur1HrS.Text;
  Tur1HrA := dmkEdTur1HrA.Text;
  Tur1HrB := dmkEdTur1HrB.Text;
  Tur1HrE := dmkEdTur1HrE.Text;
  Tur1HrC := dmkEdTur1HrC.Text;
  //
  // Dias do m�s
  Memo1.Lines.Clear;
  for i := 0 to 30 do
  begin
    Txt := Geral.FDT(FListaCk[i].Tag, 2) + '(Dia ' + FListaCk[i].Caption + '): ';
    if FListaCk[i].Checked then
    begin
      DataP := Geral.FDT(FListaCk[i].Tag, 1);
      // Falta Fazer ?
      Controle := 0;
{
function TUModule.BuscaEmLivreY_Def(Table, Field, LaTipo: String; Atual: Integer): Integer;
begin
  if LaTipo = CO_ALTERACAO then
    Result := Atual
  else
    Result := BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', Table, Table, Field);
end;
}
      //
      UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'fppontohor', False, [
      'Codigo', 'Empresa', 'Periodo', 'DataP', 'Tur1Ent', 'Tur1DeI', 'Tur1DeF',
      'Tur1Sai', 'Tur1HrT', 'Tur1HrN', 'Tur1HrS', 'Tur1HrA', 'Tur1HrB',
      'Tur1HrE', 'Tur1HrC'], [
      'Controle'], [
      Codigo, Empresa, Periodo, DataP, Tur1Ent, Tur1DeI, Tur1DeF,
      Tur1Sai, Tur1HrT, Tur1HrN, Tur1HrS, Tur1HrA, Tur1HrB, Tur1HrE, Tur1HrC
      ], [Controle]);
      //Parei aqui
      //testar
      Memo1.Lines.Add(Txt + 'Sim');
    end else
      Memo1.Lines.Add(Txt + 'N�o');
  end;
  Screen.Cursor := crHourGlass;
  //
  // Fim da cria��o do calend�rio
  (*
  Horas  := dmkEdHoras.ValueVariant;
  if Horas <= 0 then
  begin
    Application.MessageBox('Defina a quantidade de horas extras!', 'Aviso', MB_OK+MB_ICONWARNING);
    if dmkEdHoras.Visible then
      dmkEdHoras.SetFocus;
    Exit;
  end;
  case RGFator.ItemIndex of
    0: Fator := QrFunciHEFator.Value + 100;
    1: Fator := 200;
    else begin
      Application.MessageBox('Defina o tipo de hora extra!',
        'Aviso', MB_OK+MB_ICONWARNING);
      if RGFator.Visible then
        RGFator.SetFocus;
      Exit;
    end;
  end;
  DataHE := Geral.FDT(TPDataIni.Date, 1);
  Controle := UMyMod.BuscaEmLivreY_Def('FPPonto', 'Controle', LaTipo.Caption,
    QrFPPontoHorControle.Value);
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.Caption, 'FPPonto', [
  'Codigo', 'Empresa', 'DataHE', 'Horas', 'Fator'], ['Controle'], [
  Codigo,    Empresa,   DataHE,   Horas,   Fator ], [ Controle ]);
  //
  ReabreFPPonto(Controle);
  Application.MessageBox(PChar(LaTipo.Caption + ' realizada com sucesso!'),
  'Informa��o', MB_OK+MB_ICONINFORMATION);
  if CkContinua.Checked then
  begin
    LaTipo.Caption := CO_INCLUSAO;
    dmkEdControle.ValueVariant := 0;
    dmkEdHoras.ValueVariant    := 0;
    if EdEmpresa.Visible then
      EdEmpresa.SetFocus;
  end else MostraEdicao(0, CO_TRAVADO, 0);}*)
end;

end.

