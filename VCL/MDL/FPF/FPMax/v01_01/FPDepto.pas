unit FPDepto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Grids, DBGrids,
  dmkDBGridDAC, Db, mySQLDbTables, dmkDBGrid;

type
  TFmFPDepto = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    TbFPDepto: TmySQLTable;
    TbFPDeptoCodID: TWideStringField;
    TbFPDeptoDescricao: TWideStringField;
    DsFPDepto: TDataSource;
    DBGrid1: TDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TbFPDeptoBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmFPDepto: TFmFPDepto;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmFPDepto.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPDepto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPDepto.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFPDepto.FormCreate(Sender: TObject);
begin
  TbFPDepto.Open;
end;

procedure TFmFPDepto.TbFPDeptoBeforePost(DataSet: TDataSet);
begin
  TbFPDeptoCodID.Value := MLAGeral.SoNumeroELetraESeparadores_TT(TbFPDeptoCodID.Value);
end;

end.
