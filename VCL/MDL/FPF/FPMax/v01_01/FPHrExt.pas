unit FPHrExt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Grids, DBGrids,
  DBCtrls,    Db, mySQLDbTables, ComCtrls,
  dmkEdit,  dmkDBGrid, Mask, Menus;

type
  TFmFPHrExt = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    PnTudo: TPanel;
    Panel4: TPanel;
    QrFunci: TmySQLQuery;
    DsFunci: TDataSource;
    QrEmpresa: TmySQLQuery;
    DsEmpresa: TDataSource;
    EdEmpresa: TLMDEdit;
    Label4: TLabel;
    CBEmpresa: TDBLookupComboBox;
    Label5: TLabel;
    Label2: TLabel;
    EdCodigo: TLMDEdit;
    CBCodigo: TDBLookupComboBox;
    Panel6: TPanel;
    PnEdita: TPanel;
    Label3: TLabel;
    RGFator: TRadioGroup;
    dmkEdHoras: TdmkEdit;
    PnConfig: TPanel;
    Label1: TLabel;
    TPDataHE: TDateTimePicker;
    GroupBox1: TGroupBox;
    TPIni: TDateTimePicker;
    TPFim: TDateTimePicker;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel8: TPanel;
    BtConfirma: TBitBtn;
    LaTipo: TLabel;
    Panel3: TPanel;
    BtDesiste: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    QrFPHrExt: TmySQLQuery;
    DsFPHrExt: TDataSource;
    QrFPHrExtControle: TIntegerField;
    QrFPHrExtCodigo: TIntegerField;
    QrFPHrExtEmpresa: TIntegerField;
    QrFPHrExtDataHE: TDateField;
    QrFPHrExtHoras: TFloatField;
    QrFPHrExtLk: TIntegerField;
    QrFPHrExtDataCad: TDateField;
    QrFPHrExtDataAlt: TDateField;
    QrFPHrExtUserCad: TIntegerField;
    QrFPHrExtUserAlt: TIntegerField;
    QrFPHrExtFator: TFloatField;
    QrFPHrExtNOMEEMP: TWideStringField;
    QrFPHrExtNOMEFUN: TWideStringField;
    Label8: TLabel;
    dmkEdControle: TdmkEdit;
    CkContinua: TCheckBox;
    CkIni: TCheckBox;
    CkFim: TCheckBox;
    dmkDBGrid1: TdmkDBGrid;
    QrSumHr: TmySQLQuery;
    QrSumHrHoras: TFloatField;
    QrSumHrFatHr: TFloatField;
    DsSumHr: TDataSource;
    GroupBox2: TGroupBox;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit2: TDBEdit;
    QrFunciCodigo: TIntegerField;
    QrFunciHEFator: TFloatField;
    QrFunciFunci: TIntegerField;
    QrFunciNOMEFUNCI: TWideStringField;
    PMExclui: TPopupMenu;
    ExcluiitemAtual1: TMenuItem;
    ExcluiitensSelecionados1: TMenuItem;
    ExcluiTodositens1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdEmpresaExit(Sender: TObject);
    procedure CBEmpresaClick(Sender: TObject);
    procedure CBEmpresaDropDown(Sender: TObject);
    procedure EdCodigoChange(Sender: TObject);
    procedure EdCodigoExit(Sender: TObject);
    procedure CBCodigoClick(Sender: TObject);
    procedure CBCodigoDropDown(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrFPHrExtBeforeClose(DataSet: TDataSet);
    procedure QrFPHrExtAfterOpen(DataSet: TDataSet);
    procedure ExcluiitemAtual1Click(Sender: TObject);
    procedure ExcluiitensSelecionados1Click(Sender: TObject);
    procedure ExcluiTodositens1Click(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure ReabreFPHrExt(Controle: Integer);
    function ExcluiItens(Tipo: TSelType): Boolean;
  public
    { Public declarations }
  end;

  var
  FmFPHrExt: TFmFPHrExt;

implementation

uses UnInternalConsts, Module, UnGOTOy, UMySQLModule;

{$R *.DFM}

procedure TFmFPHrExt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPHrExt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPHrExt.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFPHrExt.EdEmpresaChange(Sender: TObject);
var
  Empresa: Integer;
begin
  MLAGeral.SincroI(FmFPHrExt, Sender, 0, siNegativo);
  Empresa := Geral.IMV(EdEmpresa.Text);
  //
  EdCodigo.Text := '';
  CBCodigo.KeyValue := NULL;
  QrFunci.Close;
  if Empresa > 0 then
  begin
    QrFunci.Close;
    QrFunci.SQL.Clear;
    QrFunci.SQL.Add('SELECT en.Codigo, fu.HEFator, fu.Funci,');
    QrFunci.SQL.Add('IF(en.Nome="", en.RazaoSocial, en.Nome) NOMEFUNCI');
    QrFunci.SQL.Add('FROM entidades en');
    QrFunci.SQL.Add('LEFT JOIN fpfunci fu ON fu.Codigo=en.Codigo');
    QrFunci.SQL.Add('WHERE ' + VAR_FP_FUNCION);
    QrFunci.SQL.Add('AND DataDemiss =0');
    QrFunci.SQL.Add('AND (fu.Codigo IN (');
    QrFunci.SQL.Add('  SELECT Codigo FROM fpfunci');
    QrFunci.SQL.Add('  WHERE Empresa=' + IntToStr(Empresa));
    QrFunci.SQL.Add('  AND DataDemiss=0))');
    QrFunci.SQL.Add('ORDER BY NOMEFUNCI');
    {
    QrFunci.Close;
    QrFunci.SQL.Clear;
    QrFunci.SQL.Add('SELECT en.Codigo,');
    QrFunci.SQL.Add('IF(en.Nome="", en.RazaoSocial, en.Nome) NOMEFUNCI');
    QrFunci.SQL.Add('FROM entidades en');
    QrFunci.SQL.Add('WHERE ' + VAR_FP_FUNCION);
    QrFunci.SQL.Add('AND (Codigo IN (');
    QrFunci.SQL.Add('  SELECT Codigo FROM fpfunci');
    QrFunci.SQL.Add('  WHERE Empresa=' + IntToStr(Empresa));
    QrFunci.SQL.Add('  AND DataDemiss=0))');
    QrFunci.SQL.Add('ORDER BY NOMEFUNCI');
    //
    }
    QrFunci.Open;
  end;
end;

procedure TFmFPHrExt.EdEmpresaExit(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPHrExt, Sender, 1, siNegativo);
end;

procedure TFmFPHrExt.CBEmpresaClick(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPHrExt, Sender, 2, siNegativo);
end;

procedure TFmFPHrExt.CBEmpresaDropDown(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPHrExt, Sender, 3, siNegativo);
end;

procedure TFmFPHrExt.EdCodigoChange(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPHrExt, Sender, 0, siNegativo);
end;

procedure TFmFPHrExt.EdCodigoExit(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPHrExt, Sender, 1, siNegativo);
end;

procedure TFmFPHrExt.CBCodigoClick(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPHrExt, Sender, 2, siNegativo);
end;

procedure TFmFPHrExt.CBCodigoDropDown(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPHrExt, Sender, 3, siNegativo);
end;

procedure TFmFPHrExt.FormCreate(Sender: TObject);
begin
  PnTudo.Height  := 150;
  PnConfig.Align := alClient;
  TPDataHE.Date  := Date;
  TPIni.Date     := Date -30;
  TPFim.Date     := Date;
  //
  QrEmpresa.Close;
  QrEmpresa.SQL.Clear;
  QrEmpresa.SQL.Add('SELECT en.Codigo,');
  QrEmpresa.SQL.Add('IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEEMPRESA');
  QrEmpresa.SQL.Add('FROM entidades en');
  QrEmpresa.SQL.Add('WHERE ' + VAR_FP_EMPRESA);
  QrEmpresa.SQL.Add('ORDER BY NOMEEMPRESA');
  QrEmpresa.Open;
  //QrFunci.Open;
end;

procedure TFmFPHrExt.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PnConfig.Visible := True;
      PnEdita.Visible  := False;
    end;
    1:
    begin
      PnEdita.Visible  := True;
      PnConfig.Visible := False;
      if Status = CO_INCLUSAO then
      begin
        dmkEdControle.ValueVariant := 0;
        dmkEdHoras.ValueVariant    := 0;
      end else begin
        dmkEdControle.ValueVariant := QrFPHrExtControle.Value;
        dmkEdHoras.ValueVariant    := QrFPHrExtHoras.Value;
        TPDataHE.Date              := Date;
        if Round(QrFPHrExtFator.Value) = 200 then
          RGFator.ItemIndex        := 1
        else
          RGFator.ItemIndex        := 0;
      end;
      EdEmpresa.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmFPHrExt.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmFPHrExt.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, CO_INCLUSAO, 0);
end;

procedure TFmFPHrExt.BtAlteraClick(Sender: TObject);
begin
  MostraEdicao(1, CO_ALTERACAO, 0);
end;

procedure TFmFPHrExt.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmFPHrExt.BtConfirmaClick(Sender: TObject);
var
  Codigo, Empresa, Controle: Integer;
  DataHE: String;
  Horas, Fator: Double;
begin
  Empresa := Geral.IMV(EdEmpresa.Text);
  if Empresa = 0 then
  begin
    Application.MessageBox('Defina a empresa!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdEmpresa.SetFocus;
    Exit;
  end;
  //
  Codigo := Geral.IMV(EdCodigo.Text);
  if Codigo = 0 then
  begin
    Application.MessageBox('Defina o funcion�rio!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdCodigo.SetFocus;
    Exit;
  end;
  //
  Horas  := dmkEdHoras.ValueVariant;
  if Horas <= 0 then
  begin
    Application.MessageBox('Defina a quantidade de horas extras!', 'Aviso', MB_OK+MB_ICONWARNING);
    dmkEdHoras.SetFocus;
    Exit;
  end;
  case RGFator.ItemIndex of
    0: Fator := QrFunciHEFator.Value + 100;
    1: Fator := 200;
    else begin
      Application.MessageBox('Defina o tipo de hora extra!',
        'Aviso', MB_OK+MB_ICONWARNING);
      RGFator.SetFocus;
      Exit;
    end;
  end;
  DataHE := Geral.FDT(TPDataHE.Date, 1);
  Controle := UMyMod.BuscaEmLivreY_Def_Old('FPHrExt', 'Controle', LaTipo.Caption,
    QrFPHrExtControle.Value);
  //
  UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'FPHrExt', False, [
  'Codigo', 'Empresa', 'DataHE', 'Horas', 'Fator'], ['Controle'], [
  Codigo,    Empresa,   DataHE,   Horas,   Fator ], [ Controle ]);
  //
  ReabreFPHrExt(Controle);
  Application.MessageBox(PChar(LaTipo.Caption + ' realizada com sucesso!'),
  'Informa��o', MB_OK+MB_ICONINFORMATION);
  if CkContinua.Checked then
  begin
    LaTipo.Caption := CO_INCLUSAO;
    dmkEdControle.ValueVariant := 0;
    dmkEdHoras.ValueVariant    := 0;
    EdEmpresa.SetFocus;
  end else MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmFPHrExt.ReabreFPHrExt(Controle: Integer);
  procedure AddExtras(Query: TmySQLQuery);
  var
    Codigo, Empresa: Integer;
  begin
    Query.SQL.Add('');
    Query.SQL.Add('FROM fphrext hre');
    Query.SQL.Add('LEFT JOIN entidades emp ON emp.Codigo=hre.Empresa');
    Query.SQL.Add('LEFT JOIN entidades fun ON fun.Codigo=hre.Codigo');
    Query.SQL.Add('');

    Query.SQL.Add(dmkPF.SQL_Periodo('WHERE hre.DataHE ',
      TPIni.Date, TPFim.Date, CkIni.Checked, CkFim.Checked));
    //
    Empresa := Geral.IMV(EdEmpresa.Text);
    if Empresa <> 0 then
      Query.SQL.Add('AND hre.Empresa = ' + FormatFloat('0', Empresa));
    //
    Codigo := Geral.IMV(EdCodigo.Text);
    if Codigo <> 0 then
      Query.SQL.Add('AND hre.Codigo = ' + FormatFloat('0', Codigo));
    //
  end;
begin
  QrFPHrExt.Close;
  QrFPHrExt.SQL.Clear;
  QrFPHrExt.SQL.Add('SELECT hre.*,');
  QrFPHrExt.SQL.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,');
  QrFPHrExt.SQL.Add('IF(fun.Tipo=0, fun.RazaoSocial, fun.Nome) NOMEFUN');
  AddExtras(QrFPHrExt);
  QrFPHrExt.Open;
  //
  QrFPHrExt.Locate('Controle', Controle, []);

  QrSumHr.Close;
  QrSumHr.SQL.Clear;
  QrSumHr.SQL.Add('SELECT SUM(hre.Horas) Horas,');
  QrSumHr.SQL.Add('SUM(hre.Horas*hre.Fator/100) FatHr');
  AddExtras(QrSumHr);
  QrSumHr.Open;
end;

procedure TFmFPHrExt.BtOKClick(Sender: TObject);
begin
  ReabreFPHrExt(QrFPHrExtControle.Value);
end;

procedure TFmFPHrExt.QrFPHrExtBeforeClose(DataSet: TDataSet);
begin
  BtAltera.Enabled := False;
  BtExclui.Enabled := False;
end;

procedure TFmFPHrExt.QrFPHrExtAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrFPHrExt.RecordCount > 0;
  BtExclui.Enabled := QrFPHrExt.RecordCount > 0;
end;

function TFmFPHrExt.ExcluiItens(Tipo: TSelType): Boolean;
  procedure ExcluiItemAtual;
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fphrext WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrFPHrExtControle.Value;
    Dmod.QrUpd.ExecSQL;
  end;
var
  i, Next: Integer;
begin
  Next := 0;
  Result := True;
  case Tipo of
    istTodos:
    begin
      if Application.MessageBox('Confirma a exclus�o de todos itens pesquisados?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        QrFPHrExt.First;
        while not QrFPHrExt.Eof do
        begin
          ExcluiItemAtual;
          QrFPHrExt.Next;
        end;
        Next := 0;
      end;
    end;
    istSelecionados:
    begin
      if Application.MessageBox('Confirma a exclus�o dos itens selecionados?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        with dmkDBGrid1.DataSource.DataSet do
        for i:= 0 to dmkDBGrid1.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(dmkDBGrid1.SelectedRows.Items[i]));
          ExcluiItemAtual;
        end;
        Next  := UMyMod.ProximoRegistro(QrFPHrExt, 'Controle', QrFPHrExtControle.Value);
      end;  
    end;
    istAtual: if Application.MessageBox('Confirma a exclus�o do item selecionado?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      ExcluiItemAtual;
      Next  := UMyMod.ProximoRegistro(QrFPHrExt, 'Controle', QrFPHrExtControle.Value);
    end;
  end;
  ReabreFPHrExt(Next);
end;

procedure TFmFPHrExt.ExcluiitemAtual1Click(Sender: TObject);
begin
  ExcluiItens(istAtual);
end;

procedure TFmFPHrExt.ExcluiitensSelecionados1Click(Sender: TObject);
begin
  ExcluiItens(istSelecionados);
end;

procedure TFmFPHrExt.ExcluiTodositens1Click(Sender: TObject);
begin
  ExcluiItens(istTodos);
end;

end.

