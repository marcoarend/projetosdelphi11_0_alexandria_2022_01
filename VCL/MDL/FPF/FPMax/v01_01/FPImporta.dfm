object FmFPImporta: TFmFPImporta
  Left = 339
  Top = 185
  Width = 800
  Height = 491
  Caption = 'Importa��o de Tabelas'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 409
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 0
    object Panel2: TPanel
      Left = 680
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa�da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object BtImportar: TBitBtn
      Tag = 19
      Left = 19
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Inclui novo banco'
      Caption = 'I&mporta'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtImportarClick
      NumGlyphs = 2
    end
    object CkLinha: TCheckBox
      Left = 128
      Top = 20
      Width = 185
      Height = 17
      Caption = 'Informar inclus�o de linha a linha.'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Importa��o de Tabelas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 788
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 792
    Height = 361
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'de arquivo de atualiza��o'
      object MeInfo: TMemo
        Left = 0
        Top = 0
        Width = 414
        Height = 333
        Align = alClient
        TabOrder = 0
      end
      object MeRegistros: TMemo
        Left = 414
        Top = 0
        Width = 370
        Height = 333
        Align = alRight
        TabOrder = 1
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'do Access'
      ImageIndex = 1
      object Splitter1: TSplitter
        Left = 177
        Top = 45
        Width = 3
        Height = 288
        Cursor = crHSplit
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 45
        Align = alTop
        TabOrder = 0
        object Label1: TLabel
          Left = 4
          Top = 28
          Width = 34
          Height = 13
          Caption = 'Avisos:'
        end
        object PB1: TProgressBar
          Left = 4
          Top = 4
          Width = 781
          Height = 17
          Min = 0
          Max = 100
          TabOrder = 0
        end
      end
      object CLTabelas: TCheckListBox
        Left = 0
        Top = 45
        Width = 177
        Height = 288
        Align = alLeft
        ItemHeight = 13
        Items.Strings = (
          'Acumuladores'
          'Admiss�es'
          'Categorias de funcion�rios'
          'C.B.O. 1994'
          'C.B.O. 2002'
          'Desligamentos'
          'Eventos'
          'Eventos.acumuladores'
          'Eventos.tipos'
          'Eventos.vari�veis'
          'Graus de instru��o'
          'Munic�pios'
          'Munic�pios - CEPs'
          'Nacionalidades'
          'Par�metros'
          'Situa��es'
          'Tipos de c�lculo'
          'Tipos de sal�rio'
          'Vari�veis'
          'V�nculos empregat�cios')
        TabOrder = 1
      end
      object Memo1: TMemo
        Left = 180
        Top = 45
        Width = 604
        Height = 288
        Align = alClient
        TabOrder = 2
      end
    end
  end
  object Database1: TDatabase
    AliasName = 'Banco de dados do MS Access'
    DatabaseName = 'Folha'
    LoginPrompt = False
    Params.Strings = (
      'USER NAME=ADMINISTRADOR'
      'PASSWORD=ADM')
    SessionName = 'Default'
    Left = 236
    Top = 132
  end
  object TbEventos: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Eventos'
    Left = 264
    Top = 132
    object TbEventosEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbEventosEvento: TSmallintField
      FieldName = 'Evento'
    end
    object TbEventosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object TbEventosNatureza: TWideStringField
      FieldName = 'Natureza'
      Size = 2
    end
    object TbEventosBase: TSmallintField
      FieldName = 'Base'
    end
    object TbEventosAuto: TBooleanField
      FieldName = 'Auto'
      Required = True
    end
    object TbEventosPercentual: TFloatField
      FieldName = 'Percentual'
    end
    object TbEventosPrioridade: TSmallintField
      FieldName = 'Prioridade'
    end
    object TbEventosVisivel: TBooleanField
      FieldName = 'Visivel'
      Required = True
    end
    object TbEventosInformacao: TBooleanField
      FieldName = 'Informacao'
      Required = True
    end
    object TbEventosSalvar: TBooleanField
      FieldName = 'Salvar'
      Required = True
    end
  end
  object TbAcumuladores: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Acumuladores'
    Left = 292
    Top = 132
    object TbAcumuladoresEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbAcumuladoresAcumulador: TSmallintField
      FieldName = 'Acumulador'
    end
    object TbAcumuladoresDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 60
    end
    object TbAcumuladoresChave: TWideStringField
      FieldName = 'Chave'
      Size = 40
    end
    object TbAcumuladoresVisivel: TBooleanField
      FieldName = 'Visivel'
      Required = True
    end
    object TbAcumuladoresRegimeCaixa: TBooleanField
      FieldName = 'RegimeCaixa'
      Required = True
    end
  end
  object TbVariaveis: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Variaveis'
    Left = 320
    Top = 132
    object TbVariaveisEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbVariaveisVariavel: TSmallintField
      FieldName = 'Variavel'
    end
    object TbVariaveisNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object TbVariaveisChave: TWideStringField
      FieldName = 'Chave'
      Size = 40
    end
    object TbVariaveisVisivel: TBooleanField
      FieldName = 'Visivel'
      Required = True
    end
    object TbVariaveisBase: TBooleanField
      FieldName = 'Base'
      Required = True
    end
    object TbVariaveisPrioridade: TSmallintField
      FieldName = 'Prioridade'
    end
  end
  object TbEventosAcu: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Eventos_AC'
    Left = 348
    Top = 132
    object TbEventosAcuEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbEventosAcuEvento: TSmallintField
      FieldName = 'Evento'
    end
    object TbEventosAcuAcumulador: TSmallintField
      FieldName = 'Acumulador'
    end
  end
  object TbEventosVar: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Eventos_VR'
    Left = 404
    Top = 132
    object TbEventosVarEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbEventosVarEvento: TSmallintField
      FieldName = 'Evento'
    end
    object TbEventosVarVariavel: TSmallintField
      FieldName = 'Variavel'
    end
  end
  object TbtINSS: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'INSS'
    Left = 432
    Top = 132
    object TbtINSSEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbtINSSFaixa: TFloatField
      FieldName = 'Faixa'
    end
    object TbtINSSAliquota: TFloatField
      FieldName = 'Aliquota'
    end
    object TbtINSSDeducao: TFloatField
      FieldName = 'Deducao'
    end
  end
  object TbtIRRF: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'IRRF'
    Left = 460
    Top = 132
    object TbtIRRFEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbtIRRFTeto: TFloatField
      FieldName = 'Teto'
    end
    object TbtIRRFAliquota: TFloatField
      FieldName = 'Aliquota'
    end
    object TbtIRRFDeducao: TFloatField
      FieldName = 'Deducao'
    end
  end
  object TbParam: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Parametros'
    Left = 236
    Top = 160
    object TbParamParametro: TSmallintField
      FieldName = 'Parametro'
    end
    object TbParamDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 60
    end
    object TbParamValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object TbCBO2002: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'CBO2002'
    Left = 292
    Top = 160
    object TbCBO2002EmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbCBO2002Codigo: TWideStringField
      FieldName = 'Codigo'
      Size = 12
    end
    object TbCBO2002Descricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 1
    end
  end
  object TbCategorias: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Categorias_Funcionarios'
    Left = 320
    Top = 160
    object TbCategoriasEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbCategoriasCategoria: TSmallintField
      FieldName = 'Categoria'
    end
    object TbCategoriasDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 50
    end
  end
  object TbVinculos: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Vinculos'
    Left = 348
    Top = 160
    object TbVinculosEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbVinculosVinculo: TSmallintField
      FieldName = 'Vinculo'
    end
    object TbVinculosDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 1
    end
  end
  object TbSituacoes: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Situacoes'
    Left = 404
    Top = 160
    object TbSituacoesEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbSituacoesSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object TbSituacoesDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 1
    end
  end
  object TbAdmissoes: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Admissoes'
    Left = 376
    Top = 160
    object TbAdmissoesEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbAdmissoesAdmissao: TSmallintField
      FieldName = 'Admissao'
    end
    object TbAdmissoesDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 1
    end
    object TbAdmissoesAdmCaged: TSmallintField
      FieldName = 'AdmCaged'
    end
  end
  object TbTiposSalario: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Tipos_Salario'
    Left = 432
    Top = 160
    object TbTiposSalarioEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbTiposSalarioTipoSalario: TSmallintField
      FieldName = 'TipoSalario'
    end
    object TbTiposSalarioDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 50
    end
  end
  object TbDesligamentos: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Desligamentos'
    Left = 460
    Top = 160
    object TbDesligamentosEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbDesligamentosDesligamento: TSmallintField
      FieldName = 'Desligamento'
    end
    object TbDesligamentosDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 1
    end
    object TbDesligamentosAvisoPrevio: TBooleanField
      FieldName = 'AvisoPrevio'
      Required = True
    end
    object TbDesligamentosFeriasVencidas: TBooleanField
      FieldName = 'FeriasVencidas'
      Required = True
    end
    object TbDesligamentosFeriasProporcionais: TBooleanField
      FieldName = 'FeriasProporcionais'
      Required = True
    end
    object TbDesligamentosRestringeFerias: TBooleanField
      FieldName = 'RestringeFerias'
      Required = True
    end
    object TbDesligamentosDecimoTerceiro: TBooleanField
      FieldName = 'DecimoTerceiro'
      Required = True
    end
    object TbDesligamentosMultaRecisao: TBooleanField
      FieldName = 'MultaRecisao'
      Required = True
    end
    object TbDesligamentosDesCaged: TSmallintField
      FieldName = 'DesCaged'
    end
  end
  object TbTiposCalculo: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Tipos_Calculo'
    Left = 544
    Top = 4
    object TbTiposCalculoEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbTiposCalculoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object TbTiposCalculoDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 70
    end
  end
  object TbEventosTip: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Eventos_Tipos'
    Left = 376
    Top = 132
    object TbEventosTipEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbEventosTipTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object TbEventosTipEvento: TSmallintField
      FieldName = 'Evento'
    end
  end
  object TbCBO1994: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Cargos'
    Left = 264
    Top = 160
    object TbCBO1994EmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbCBO1994Cargo: TWideStringField
      FieldName = 'Cargo'
      Size = 10
    end
    object TbCBO1994Descricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 1
    end
  end
  object TbMunicipios: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Municipios'
    Left = 572
    Top = 4
    object TbMunicipiosEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbMunicipiosMunicipio: TWideStringField
      FieldName = 'Municipio'
      Size = 14
    end
    object TbMunicipiosNome: TWideStringField
      FieldName = 'Nome'
      Size = 70
    end
    object TbMunicipiosFaixaCEP: TWideStringField
      FieldName = 'FaixaCEP'
      Size = 10
    end
  end
  object TbMunicipiosCEP: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Municipios_CEPs'
    Left = 600
    Top = 4
    object TbMunicipiosCEPEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbMunicipiosCEPMunicipio: TWideStringField
      FieldName = 'Municipio'
      Size = 14
    end
    object TbMunicipiosCEPFaixaIni: TWideStringField
      FieldName = 'FaixaIni'
      Size = 10
    end
    object TbMunicipiosCEPFaixaFin: TWideStringField
      FieldName = 'FaixaFin'
      Size = 10
    end
  end
  object TbNacionalidades: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Nacionalidades'
    Left = 628
    Top = 4
    object TbNacionalidadesEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbNacionalidadesNacionalidade: TSmallintField
      FieldName = 'Nacionalidade'
    end
    object TbNacionalidadesDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 60
    end
  end
  object TbGrausInstrucao: TTable
    DatabaseName = 'Banco de dados do MS Access'
    TableName = 'Graus_Instrucao'
    Left = 656
    Top = 4
    object TbGrausInstrucaoEmUso: TBooleanField
      FieldName = 'EmUso'
      Required = True
    end
    object TbGrausInstrucaoGrauInstrucao: TSmallintField
      FieldName = 'GrauInstrucao'
    end
    object TbGrausInstrucaoDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 1
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 236
    Top = 88
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 8
    Top = 8
  end
end
