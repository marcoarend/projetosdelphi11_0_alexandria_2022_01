unit FPFolhaImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Grids, DBGrids,
  dmkDBGrid, Db, mySQLDbTables, frxClass, frxDBSet, dmkEdit;

type
  TFmFPFolhaImp = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkDBGrid1: TdmkDBGrid;
    QrCalc: TmySQLQuery;
    DsCalc: TDataSource;
    dmkEdVias: TdmkEdit;
    Label1: TLabel;
    QrInsC: TmySQLQuery;
    QrInsI: TmySQLQuery;
    QrInsR: TmySQLQuery;
    QrCalcCODFUNCI: TIntegerField;
    QrCalcRegistro: TWideStringField;
    QrCalcDepto: TWideStringField;
    QrCalcFuncao: TWideStringField;
    QrCalcCBO2002: TWideStringField;
    QrCalcDataAdm: TDateField;
    QrCalcFilhos: TIntegerField;
    QrCalcDependent: TIntegerField;
    QrCalcBancoSal: TIntegerField;
    QrCalcContaSal: TWideStringField;
    QrCalcCodigo: TIntegerField;
    QrCalcPeriodo: TIntegerField;
    QrCalcEmpresa: TIntegerField;
    QrCalcEntidade: TIntegerField;
    QrCalcSemana: TSmallintField;
    QrCalcTipoCalc: TIntegerField;
    QrCalcDataC: TDateField;
    QrCalcValorP: TFloatField;
    QrCalcValorD: TFloatField;
    QrCalcValorL: TFloatField;
    QrCalcAtivo: TSmallintField;
    QrCalcNOMEFUNCI: TWideStringField;
    QrCalcNOMEEMPRE: TWideStringField;
    QrCalcNOMETIPOCALC: TWideStringField;
    QrCalcNOMEDEPTO: TWideStringField;
    QrCalcNOMEFUNCAO: TWideStringField;
    QrCalcPERIODO_TXT: TWideStringField;
    QrEveM: TmySQLQuery;
    QrEveMDescricao: TWideStringField;
    QrEveMCodigo: TIntegerField;
    QrEveMEvento: TIntegerField;
    QrEveMReferencia: TFloatField;
    QrEveMValor: TFloatField;
    QrEveMInfo: TSmallintField;
    QrEveMMostra: TSmallintField;
    QrEveMAtivo: TSmallintField;
    QrEveMValorP: TFloatField;
    QrEveMValorD: TFloatField;
    QrTotPD: TmySQLQuery;
    QrTotPDValorP: TFloatField;
    QrTotPDValorD: TFloatField;
    QrCab: TmySQLQuery;
    frxDsC: TfrxDBDataset;
    QrCabFolhaCal: TIntegerField;
    QrCabControle: TIntegerField;
    QrCabNumFolha: TIntegerField;
    QrCabEmpresa: TIntegerField;
    QrCabCodFunci: TIntegerField;
    QrCabEntidade: TIntegerField;
    QrCabRegistro: TWideStringField;
    QrCabCodINSS: TWideStringField;
    QrCabDataAdm: TDateField;
    QrCabCBO2002: TWideStringField;
    QrCabNomeEnt: TWideStringField;
    QrCabDepto: TWideStringField;
    QrCabNomeDepto: TWideStringField;
    QrCabFuncao: TWideStringField;
    QrCabNomeFuncao: TWideStringField;
    QrCabPeriodo: TIntegerField;
    QrCabAtivo: TSmallintField;
    QrIts: TmySQLQuery;
    frxDsI: TfrxDBDataset;
    frxDsR: TfrxDBDataset;
    QrRpe: TmySQLQuery;
    QrRpeControle: TIntegerField;
    QrRpeTotalP: TFloatField;
    QrRpeTotalD: TFloatField;
    QrRpeTotalL: TFloatField;
    QrRpeBase91: TFloatField;
    QrRpeBase93: TFloatField;
    QrRpeBase95: TFloatField;
    QrRpeBase96: TFloatField;
    QrRpeBase98: TFloatField;
    QrRpeBase99: TFloatField;
    QrRpeAtivo: TSmallintField;
    QrItsControle: TIntegerField;
    QrItsLinha: TIntegerField;
    QrItsCodiEvento: TIntegerField;
    QrItsNomeEvento: TWideStringField;
    QrItsReferencia: TFloatField;
    QrItsValorP: TFloatField;
    QrItsValorD: TFloatField;
    QrItsAtivo: TSmallintField;
    QrCabPERIODO_TXT: TWideStringField;
    frxFolha: TfrxReport;
    QrUpdC: TmySQLQuery;
    QrCabUltimFolha: TSmallintField;
    QrCalcFoto: TWideStringField;
    QrCabFoto: TWideStringField;
    QrAcum27: TmySQLQuery;
    QrAcum27Valor: TFloatField;
    QrRpeAcum03: TFloatField;
    QrRpeAcum17: TFloatField;
    QrRpeAcum25: TFloatField;
    QrRpeAcum26: TFloatField;
    QrRpeAcum27: TFloatField;
    QrRpeAcum28: TFloatField;
    QrAcum25: TmySQLQuery;
    QrAcum25Valor: TFloatField;
    QrAcum17: TmySQLQuery;
    QrAcum03: TmySQLQuery;
    QrAcum17Valor: TFloatField;
    QrAcum03Valor: TFloatField;
    QrAcum26: TmySQLQuery;
    QrAcum28: TmySQLQuery;
    QrAcum28Valor: TFloatField;
    QrAcum26Valor: TFloatField;
    QrFPFunci: TmySQLQuery;
    QrFPFunciNUMEFUNCI: TWideStringField;
    QrFPFunciNUMEEMPRESA: TWideStringField;
    QrFPFunciDepto: TWideStringField;
    QrFPFunciFuncao: TWideStringField;
    QrFPFunciNOMEFUNCAO: TWideStringField;
    QrFPFunciNOMEDEPTO: TWideStringField;
    QrFPFunciNOMECBO2002: TWideStringField;
    QrFPFunciCodigo: TIntegerField;
    QrFPFunciEmpresa: TIntegerField;
    QrFPFunciFunci: TIntegerField;
    QrFPFunciRegistro: TWideStringField;
    QrFPFunciCategoria: TIntegerField;
    QrFPFunciCBO2002: TWideStringField;
    QrFPFunciVinculo: TIntegerField;
    QrFPFunciAlvara: TSmallintField;
    QrFPFunciSituacao: TIntegerField;
    QrFPFunciAdmissao: TIntegerField;
    QrFPFunciDataAdm: TDateField;
    QrFPFunciHrSeman: TIntegerField;
    QrFPFunciTipoSal: TSmallintField;
    QrFPFunciSalario: TFloatField;
    QrFPFunciFilhos: TIntegerField;
    QrFPFunciDependent: TIntegerField;
    QrFPFunciPensaoAlm: TFloatField;
    QrFPFunciBancoSal: TIntegerField;
    QrFPFunciContaSal: TWideStringField;
    QrFPFunciOptaFGTS: TSmallintField;
    QrFPFunciBancoFGTS: TIntegerField;
    QrFPFunciContaFGTS: TWideStringField;
    QrFPFunciDataFGTS: TDateField;
    QrFPFunciDataExame: TDateField;
    QrFPFunciPerExame: TIntegerField;
    QrFPFunciDataIniAf: TDateField;
    QrFPFunciDataFinAf: TDateField;
    QrFPFunciMesesAf: TIntegerField;
    QrFPFunciDataAviso: TDateField;
    QrFPFunciAvisoInden: TSmallintField;
    QrFPFunciDiasAviso: TIntegerField;
    QrFPFunciDataDemiss: TDateField;
    QrFPFunciDemissao: TIntegerField;
    QrFPFunciCodigoAf: TWideStringField;
    QrFPFunciMotivoAf: TWideStringField;
    QrFPFunciObs: TWideMemoField;
    QrFPFunciGFIP: TSmallintField;
    QrFPFunciRAIS: TSmallintField;
    QrFPFunciProfessor: TSmallintField;
    QrFPFunciAtivo: TSmallintField;
    QrFPFunciLk: TIntegerField;
    QrFPFunciDataCad: TDateField;
    QrFPFunciDataAlt: TDateField;
    QrFPFunciUserCad: TIntegerField;
    QrFPFunciUserAlt: TIntegerField;
    QrFPFunciNOMECATEG: TWideStringField;
    QrFPFunciNOMEVINCU: TWideStringField;
    QrFPFunciNOMEADMIS: TWideStringField;
    QrFPFunciNOMESITUA: TWideStringField;
    QrFPFunciNOMETPSAL: TWideStringField;
    QrFPFunciNacionalid: TIntegerField;
    QrFPFunciAnoChegada: TIntegerField;
    QrFPFunciGrauInstru: TIntegerField;
    QrFPFunciEstCivil: TIntegerField;
    QrFPFunciCTPS: TWideStringField;
    QrFPFunciSerieCTPS: TWideStringField;
    QrFPFunciPIS: TWideStringField;
    QrFPFunciTitEleitor: TWideStringField;
    QrFPFunciReservista: TWideStringField;
    QrFPFunciRaca: TIntegerField;
    QrFPFunciDeficiente: TSmallintField;
    QrFPFunciFoto: TWideStringField;
    QrFPFunciCargo: TWideStringField;
    QrFPFunciLocalTrab: TWideStringField;
    QrFPFunciInsalubrid: TFloatField;
    QrFPFunciPericulosi: TFloatField;
    QrFPFunciNOMECARGO: TWideStringField;
    QrFPFunciNOMENACIONALID: TWideStringField;
    QrFPFunciNOMEECIVIL: TWideStringField;
    QrFPFunciNOMERACA: TWideStringField;
    QrFPFunciNOMEGRAUI: TWideStringField;
    QrFPFunciNOMELOCTRAB: TWideStringField;
    QrFPFunciPensaoLiq: TSmallintField;
    QrFPFunciComoCalcIR: TSmallintField;
    QrFPFunciFer: TmySQLQuery;
    QrFPFunciFerEmpresa: TIntegerField;
    QrFPFunciFerCodigo: TIntegerField;
    QrFPFunciFerCalculo: TIntegerField;
    QrFPFunciFerPecun: TSmallintField;
    QrFPFunciFerMeHEMan160: TFloatField;
    QrFPFunciFerMeHEMan200: TFloatField;
    QrFPFunciFerMediaCoMan: TFloatField;
    QrFPFunciFerMediaAdMan: TFloatField;
    QrFPFunciFerDtIniPA: TDateField;
    QrFPFunciFerDtFimPA: TDateField;
    QrFPFunciFerDiasFaltI: TIntegerField;
    QrFPFunciFerDiasFaltP: TIntegerField;
    QrFPFunciFerDiasGozar: TSmallintField;
    QrFPFunciFerDiasPecun: TSmallintField;
    QrFPFunciFerDtIniPecun: TDateField;
    QrFPFunciFerDtFimPecun: TDateField;
    QrFPFunciFerDtIniPG: TDateField;
    QrFPFunciFerDtFimFeria: TDateField;
    QrFPFunciFerDtAvisoFer: TDateField;
    QrFPFunciFerDtRecPgto: TDateField;
    QrFPFunciFerDtSolPecun: TDateField;
    QrFPFunciFerDtSolPrP13: TDateField;
    QrFPFunciFerDtRetorno: TDateField;
    QrFPFunciFerLk: TIntegerField;
    QrFPFunciFerDataCad: TDateField;
    QrFPFunciFerDataAlt: TDateField;
    QrFPFunciFerUserCad: TIntegerField;
    QrFPFunciFerUserAlt: TIntegerField;
    QrFPFunciFerDTINIPECUN_TXT: TWideStringField;
    QrFPFunciFerDTFIMPECUN_TXT: TWideStringField;
    QrFPFunciFerDTSOLPECUN_TXT: TWideStringField;
    QrFPFunciFerDTSOLPRP13_TXT: TWideStringField;
    QrFPFunciFerPECUNIO_TXT: TWideStringField;
    QrFPFunciFerDtFimPG: TDateField;
    QrFPFunciFerControle: TIntegerField;
    frxFeriasxxx: TfrxReport;
    frxDsFPFuncifer: TfrxDBDataset;
    frxDsFPFunci: TfrxDBDataset;
    frxFerias: TfrxReport;
    QrCalcPecun: TSmallintField;
    QrCalcDtIniPA: TDateField;
    QrCalcDtFimPA: TDateField;
    QrCalcDiasFaltI: TIntegerField;
    QrCalcDiasGozar: TSmallintField;
    QrCalcDiasPecun: TSmallintField;
    QrCalcDtIniPecun: TDateField;
    QrCalcDtFimPecun: TDateField;
    QrCalcDtIniPG: TDateField;
    QrCalcDtFimPG: TDateField;
    QrCalcDtRecPgto: TDateField;
    QrCalcDtRetorno: TDateField;
    QrCalcMediaHE: TFloatField;
    QrCalcMediaCo: TFloatField;
    QrCalcMediaAd: TFloatField;
    QrCabDiasPG: TIntegerField;
    QrCabDiasPP: TIntegerField;
    QrCabDiasFI: TIntegerField;
    QrCabMediaHE: TFloatField;
    QrCabMediaCo: TFloatField;
    QrCabMediaAd: TFloatField;
    QrCabSalFixo: TFloatField;
    QrCabBaseCal: TFloatField;
    QrCabDIASPG_TXT: TWideStringField;
    QrCabDIASPP_TXT: TWideStringField;
    QrCabDtIniPA: TDateField;
    QrCabDtFimPA: TDateField;
    QrCabDtIniPG: TDateField;
    QrCabDtFimPG: TDateField;
    QrCabDtIniPP: TDateField;
    QrCabDtFimPP: TDateField;
    QrCabPA_INI_DD: TWideStringField;
    QrCabPA_INI_MM: TWideStringField;
    QrCabPA_INI_AA: TWideStringField;
    QrCabPA_FIM_DD: TWideStringField;
    QrCabPA_FIM_MM: TWideStringField;
    QrCabPA_FIM_AA: TWideStringField;
    QrCabPG_INI_DD: TWideStringField;
    QrCabPG_INI_MM: TWideStringField;
    QrCabPG_INI_AA: TWideStringField;
    QrCabPG_FIM_DD: TWideStringField;
    QrCabPG_FIM_MM: TWideStringField;
    QrCabPG_FIM_AA: TWideStringField;
    QrCabPP_INI_DD: TWideStringField;
    QrCabPP_INI_MM: TWideStringField;
    QrCabPP_INI_AA: TWideStringField;
    QrCabPP_FIM_DD: TWideStringField;
    QrCabPP_FIM_MM: TWideStringField;
    QrCabPP_FIM_AA: TWideStringField;
    QrEvenXX: TmySQLQuery;
    QrEvenXXValor: TFloatField;
    QrRpeEXTENSO_TotalL: TWideStringField;
    QrCabDtRecPgto: TDateField;
    QrCabDTRECPGTO_TXT: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCalcCalcFields(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrCalcAfterScroll(DataSet: TDataSet);
    procedure QrTotPDAfterOpen(DataSet: TDataSet);
    procedure QrCabAfterScroll(DataSet: TDataSet);
    procedure QrCabCalcFields(DataSet: TDataSet);
    procedure frxFolhaGetValue(const VarName: String;
      var Value: Variant);
    procedure QrRpeCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FFolha: Integer;
  public
    { Public declarations }
    FArrCalcs: array of integer;
    function ReopenFPFolhaCal(): Integer;
    //procedure Calculo1;
    procedure Calculo3;
  end;

  var
  FmFPFolhaImp: TFmFPFolhaImp;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmFPFolhaImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPFolhaImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPFolhaImp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

function TFmFPFolhaImp.ReopenFPFolhaCal(): Integer;
var
  i: Integer;
  Itens: String;
begin
  Itens := '';
  for i := Low(FArrCalcs) to High(FArrCalcs) do
    Itens := Itens + ',' + IntToStr(FArrCalcs[i]);
  //
  QrCalc.Close;
  if Length(Itens) > 1 then
  begin
    Itens := Copy(Itens, 2, Length(Itens));
    QrCalc.SQL.Clear;
    //
    QrCalc.SQL.Add('SELECT fff.Pecun, fff.DtIniPA, fff.DtFimPA,');
    QrCalc.SQL.Add('fff.DiasFaltI, fff.DiasGozar, fff.DiasPecun,');
    QrCalc.SQL.Add('fff.DtIniPecun, fff.DtFimPecun, fff.DtIniPG,');
    QrCalc.SQL.Add('fff.DtFimPG, fff.DtRecPgto, fff.DtRetorno,');
    QrCalc.SQL.Add('fun.Funci CODFUNCI, fun.Registro, fun.Depto,');
    QrCalc.SQL.Add('fun.Funcao, fun.CBO2002, fun.DataAdm, fun.Filhos,');
    QrCalc.SQL.Add('fun.Dependent, fun.BancoSal, fun.ContaSal, fca.*,');
    QrCalc.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFUNCI,');
    QrCalc.SQL.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMPRE,');
    QrCalc.SQL.Add('tca.Descricao NOMETIPOCALC, dep.Descricao NOMEDEPTO,');
    QrCalc.SQL.Add('fpf.Descricao NOMEFUNCAO, fun.Foto');
    QrCalc.SQL.Add('');
    QrCalc.SQL.Add('FROM fpfolhacal fca');
    QrCalc.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=fca.Entidade');
    QrCalc.SQL.Add('LEFT JOIN entidades emp ON emp.Codigo=fca.Empresa');
    QrCalc.SQL.Add('LEFT JOIN fptpcal   tca ON tca.Codigo=fca.TipoCalc');
    QrCalc.SQL.Add('LEFT JOIN fpfunci   fun ON fun.Codigo=ent.Codigo');
    QrCalc.SQL.Add('LEFT JOIN fpdepto   dep ON dep.CodID=fun.Depto');
    QrCalc.SQL.Add('LEFT JOIN fpfunca   fpf ON fpf.CodID=fun.Funcao');
    QrCalc.SQL.Add('LEFT JOIN fpfuncifer fff ON fff.Calculo=fca.Codigo');
    QrCalc.SQL.Add('');
    QrCalc.SQL.Add('WHERE fca.Codigo in (' + Itens + ')');
    QrCalc.Open;
  end;
  Result := QrCalc.RecordCount;
end;

procedure TFmFPFolhaImp.QrCalcCalcFields(DataSet: TDataSet);
begin
  QrCalcPERIODO_TXT.Value := MLAGeral.MesEAnoDoPeriodoLongo(QrCalcPeriodo.Value);
end;

procedure TFmFPFolhaImp.BtOKClick(Sender: TObject);
begin
  {case QrCalcTipoCalc.Value of
    // Ferias
    5: Calculo5;
    // Mensal, Adiantamento
    else} Calculo3;
  //end;
end;

procedure TFmFPFolhaImp.Calculo3;
var
  i, j, k, n, Folha, Linha: Integer;
  ContinuaIts: Boolean;
  SalFixo, BaseCal: Double;
  ItensPorFolha: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
  case QrCalcTipoCalc.Value of
    // Ferias
    5:
    begin
      ItensPorFolha := 35; //  36 ok 37 pula
      {ItsFolha := IntToStr(ItensPorFolha);
      if InputQuery('Itens por folha', 'Informe os por folha:', ItsFolha) then
        ItensPorFolha := Geral.IMV(ItsFolha);}
    end;
    //1: Adiantamento, 3: Mensal
    else ItensPorFolha := 15;
  end;
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('DELETE FROM fpimpcab ');
  Dmod.QrUpdL.ExecSQL;
  //
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('DELETE FROM fpimpits ');
  Dmod.QrUpdL.ExecSQL;
  //
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('DELETE FROM fpimprpe ');
  Dmod.QrUpdL.ExecSQL;
  //
  QrUpdC.SQL.Clear;
  QrUpdC.SQL.Add('UPDATE fpimpcab SET ');
  QrUpdC.SQL.Add('UltimFolha = 0 ');
  QrUpdC.SQL.Add('WHERE Controle=:P0 ');
  //
  QrInsC.SQL.Clear;
  QrInsC.SQL.Add('INSERT INTO fpimpcab SET ');
  QrInsC.SQL.Add('FolhaCal   =:P00, ');
  QrInsC.SQL.Add('Controle   =:P01, ');
  QrInsC.SQL.Add('NumFolha   =:P02, ');
  QrInsC.SQL.Add('Empresa    =:P03, ');
  QrInsC.SQL.Add('CodFunci   =:P04, ');
  QrInsC.SQL.Add('Entidade   =:P05, ');
  QrInsC.SQL.Add('Registro   =:P06, ');
  QrInsC.SQL.Add('CodINSS    =:P07, ');
  QrInsC.SQL.Add('DataAdm    =:P08, ');
  QrInsC.SQL.Add('CBO2002    =:P09, ');
  QrInsC.SQL.Add('NomeEnt    =:P10, ');
  QrInsC.SQL.Add('Depto      =:P11, ');
  QrInsC.SQL.Add('NomeDepto  =:P12, ');
  QrInsC.SQL.Add('Funcao     =:P13, ');
  QrInsC.SQL.Add('NomeFuncao =:P14, ');
  QrInsC.SQL.Add('Periodo    =:P15, ');
  QrInsC.SQL.Add('UltimFolha =:P16, ');
  QrInsC.SQL.Add('DtIniPA    =:P17, ');
  QrInsC.SQL.Add('DtFimPA    =:P18, ');
  QrInsC.SQL.Add('DtIniPG    =:P19, ');
  QrInsC.SQL.Add('DtFimPG    =:P20, ');
  QrInsC.SQL.Add('DtIniPP    =:P21, ');
  QrInsC.SQL.Add('DtFimPP    =:P22, ');
  QrInsC.SQL.Add('DiasPG     =:P23, ');
  QrInsC.SQL.Add('DiasPP     =:P24, ');
  QrInsC.SQL.Add('DiasFI     =:P25, ');
  QrInsC.SQL.Add('MediaHE    =:P26, ');
  QrInsC.SQL.Add('MediaCo    =:P27, ');
  QrInsC.SQL.Add('MediaAd    =:P28, ');
  QrInsC.SQL.Add('SalFixo    =:P29, ');
  QrInsC.SQL.Add('BaseCal    =:P30, ');
  QrInsC.SQL.Add('DtRecPgto  =:P31, ');
  //
  QrInsC.SQL.Add('Foto       =:Pa, ');
  QrInsC.SQL.Add('Ativo      =:Pb');
  //
  QrInsI.SQL.Clear;
  QrInsI.SQL.Add('INSERT INTO fpimpits SET ');
  QrInsI.SQL.Add('Controle   =:P00, ');
  QrInsI.SQL.Add('Linha      =:P01, ');
  QrInsI.SQL.Add('CodiEvento =:P02, ');
  QrInsI.SQL.Add('NomeEvento =:P03, ');
  QrInsI.SQL.Add('Referencia =:P04, ');
  QrInsI.SQL.Add('ValorP     =:P05, ');
  QrInsI.SQL.Add('ValorD     =:P06, ');
  QrInsI.SQL.Add('Ativo      =:P07');
  //
  QrInsR.SQL.Clear;
  QrInsR.SQL.Add('INSERT INTO fpimprpe SET ');
  QrInsR.SQL.Add('Controle =:P00, ');
  QrInsR.SQL.Add('TotalP   =:P01, ');
  QrInsR.SQL.Add('TotalD   =:P02, ');
  QrInsR.SQL.Add('TotalL   =:P03, ');
  QrInsR.SQL.Add('Acum27   =:P04, ');
  QrInsR.SQL.Add('Acum25   =:P05, ');
  QrInsR.SQL.Add('Acum17   =:P06, ');
  QrInsR.SQL.Add('Acum03   =:P07, ');
  QrInsR.SQL.Add('Acum28   =:P08, ');
  QrInsR.SQL.Add('Acum26   =:P09, ');
  QrInsR.SQL.Add('Ativo    =:P10');
  //

  k := dmkEdVias.ValueVariant;
  if k < 1 then
  begin
    Application.MessageBox('Quantidade de vias inv�lida!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  // Sal�rio Fixo de f�rias
  if QrAcum27.State = dsInactive then
  begin
    QrAcum27.Close;
    QrAcum27.Params[0].AsInteger := QrCalcCodigo.Value;
    QrAcum27.Params[1].AsInteger := QrCalcCodigo.Value;
    QrAcum27.Open;
  end;  
  SalFixo := QrAcum27Valor.Value;
  // Sal�rio base de f�rias
  QrEvenXX.Close;
  QrEvenXX.Params[00].AsInteger := 78; // F�rias cheias (30 dias)
  QrEvenXX.Params[01].AsInteger := QrCalcCodigo.Value;
  QrEvenXX.Open;
  BaseCal := QrEvenXXValor.Value;
  n := 0;
  QrCalc.First;
  while not QrCalc.Eof do
  begin
    for i := 1 to k do
    begin
      // Parei Aqui
      // Ver
      Folha := 1;
      QrInsC.Params[00].AsInteger := QrCalcCodigo.Value;
      //QrInsC.Params[01].AsInteger := n;
      //QrInsC.Params[02].AsInteger := Folha;
      QrInsC.Params[03].AsInteger := QrCalcEmpresa.Value;
      QrInsC.Params[04].AsInteger := QrCalcCODFUNCI.Value;
      QrInsC.Params[05].AsInteger := QrCalcEntidade.Value;
      QrInsC.Params[06].AsString  := QrCalcRegistro.Value;
      QrInsC.Params[07].AsString  := '';//Parei Aqui QrCalcCodINSS   ;
      QrInsC.Params[08].AsString  := Geral.FDT(QrCalcDataAdm.Value, 1);
      QrInsC.Params[09].AsString  := MLAGeral.FormataCBO2002(QrCalcCBO2002.Value);
      QrInsC.Params[10].AsString  := QrCalcNOMEFUNCI.Value;
      QrInsC.Params[11].AsString  := QrCalcDepto.Value;
      QrInsC.Params[12].AsString  := QrCalcNOMEDEPTO.Value;
      QrInsC.Params[13].AsString  := QrCalcFuncao.Value;
      QrInsC.Params[14].AsString  := QrCalcNOMEFUNCAO.Value;
      QrInsC.Params[15].AsInteger := QrCalcPeriodo.Value;
      QrInsC.Params[16].AsInteger := 1;
      //
      QrInsC.Params[17].AsString  := Geral.FDT(QrCalcDtIniPA.Value, 1);
      QrInsC.Params[18].AsString  := Geral.FDT(QrCalcDtFimPA.Value, 1);
      QrInsC.Params[19].AsString  := Geral.FDT(QrCalcDtIniPG.Value, 1);
      QrInsC.Params[20].AsString  := Geral.FDT(QrCalcDtFimPG.Value, 1);
      QrInsC.Params[21].AsString  := Geral.FDT(QrCalcDtIniPecun.Value, 1);
      QrInsC.Params[22].AsString  := Geral.FDT(QrCalcDtFimPecun.Value, 1);
      QrInsC.Params[23].AsInteger := QrCalcDiasGozar.Value;
      QrInsC.Params[24].AsInteger := QrCalcDiasPecun.Value;
      QrInsC.Params[25].AsInteger := QrCalcDiasFaltI.Value;
      QrInsC.Params[26].AsFloat   := QrCalcMediaHE.Value;
      QrInsC.Params[27].AsFloat   := QrCalcMediaCo.Value;
      QrInsC.Params[28].AsFloat   := QrCalcMediaAd.Value;
      QrInsC.Params[29].AsFloat   := SalFixo;
      QrInsC.Params[30].AsFloat   := BaseCal;
      QrInsC.Params[31].AsString  := Geral.FDT(QrCalcDtRecPgto.Value, 1);
      //
      QrInsC.Params[32].AsString  := QrCalcFoto.Value;
      QrInsC.Params[33].AsInteger := 1;
      //QrInsC.ExecSQL;
      // Roda p�
      //QrInsR.Params[00].AsInteger := n;
      QrInsR.Params[01].AsFloat   := QrTotPDValorP.Value;
      QrInsR.Params[02].AsFloat   := QrTotPDValorD.Value;
      QrInsR.Params[03].AsFloat   := QrTotPDValorP.Value - QrTotPDValorD.Value;
      QrInsR.Params[04].AsFloat   := QrAcum27Valor.Value;
      QrInsR.Params[05].AsFloat   := QrAcum25Valor.Value;
      QrInsR.Params[06].AsFloat   := QrAcum17Valor.Value;
      QrInsR.Params[07].AsFloat   := QrAcum03Valor.Value;
      QrInsR.Params[08].AsFloat   := QrAcum28Valor.Value;
      QrInsR.Params[09].AsFloat   := QrAcum26Valor.Value;
      QrInsR.Params[10].AsInteger := 1;//Ativo
      //QrInsR.ExecSQL;
      // Itens
      ContinuaIts := True;
      while ContinuaIts do
      begin
        QrEveM.Close;
        QrEveM.Params[00].AsInteger := QrCalcCodigo.Value;
        QrEveM.Params[01].AsInteger := (Folha-1) * ItensPorFolha;
        QrEveM.Params[02].AsInteger := ItensPorFolha;
        QrEveM.Open;
        if (Folha = 1) or (QrEveM.RecordCount > 0) then
        begin
          Linha := 0;
          inc(n, 1);
          QrInsC.Params[01].AsInteger := n;
          QrInsC.Params[02].AsInteger := Folha;
          QrInsC.ExecSQL;
          //
          if Folha > 1 then
          begin
            QrUpdC.Params[00].AsInteger := n - 1;
            QrUpdC.ExecSQL;
          end;
          //
          QrInsR.Params[00].AsInteger := n;
          QrInsR.ExecSQL;
          //
          QrEveM.First;
          while not QrEveM.Eof do
          begin
            inc(linha, 1);
            QrInsI.Params[00].AsInteger := n; // Controle
            QrInsI.Params[01].AsInteger := Linha;
            QrInsI.Params[02].AsInteger := QrEveMEvento.Value;
            QrInsI.Params[03].AsString  := QrEveMDescricao.Value;
            QrInsI.Params[04].AsFloat   := QrEveMReferencia.Value;
            QrInsI.Params[05].AsFloat   := QrEveMValorP.Value;
            QrInsI.Params[06].AsFloat   := QrEveMValorD.Value;
            QrInsI.Params[07].AsInteger := 1;//Ativo
            QrInsI.ExecSQL;
            //
            QrEveM.Next;
          end;
          // Linhas em branco
          for j := Linha + 1 to ItensPorFolha do
          begin
            QrInsI.Params[00].AsInteger := n; // Controle
            QrInsI.Params[01].AsInteger := j;
            QrInsI.Params[02].AsInteger := 0;
            QrInsI.Params[03].AsString  := '';
            QrInsI.Params[04].AsFloat   := 0;
            QrInsI.Params[05].AsFloat   := 0;
            QrInsI.Params[06].AsFloat   := 0;
            QrInsI.Params[07].AsInteger := 0;//Inativo
            QrInsI.ExecSQL;
          end;
        end else ContinuaIts := False;
        inc(Folha, 1);
      end;
    end;
    QrCalc.Next;
  end;
  QrCab.Close;
  QrCab.Open;
  if QrCalcTipoCalc.Value = 5 then
    MyObjects.frxMostra(frxFerias, 'F�rias')
  else
    MyObjects.frxMostra(frxFolha, 'Folha de pagamento');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFPFolhaImp.FormCreate(Sender: TObject);
begin
  dmkEdVias.ValueVariant := 2;
end;

procedure TFmFPFolhaImp.QrCalcAfterScroll(DataSet: TDataSet);
begin
  QrTotPD.Close;
  QrTotPD.Params[0].AsInteger := QrCalcCodigo.Value;
  QrTotPD.Open;
  //
end;

procedure TFmFPFolhaImp.QrTotPDAfterOpen(DataSet: TDataSet);
begin
  // Sal�rio Base
  (*QrBase91.Close;
  QrBase91.Params[0].AsInteger := QrCalcCodigo.Value;
  QrBase91.Open;*)
  QrAcum27.Close;
  QrAcum27.Params[0].AsInteger := QrCalcCodigo.Value;
  QrAcum27.Params[1].AsInteger := QrCalcCodigo.Value;
  QrAcum27.Open;
  // Faixa IRRF (%)
  (*QrBase93.Close;
  QrBase93.Params[0].AsInteger := QrCalcCodigo.Value;
  QrBase93.Open;*)
  QrAcum25.Close;
  QrAcum25.Params[0].AsInteger := QrCalcCodigo.Value;
  QrAcum25.Params[1].AsInteger := QrCalcCodigo.Value;
  QrAcum25.Open;
  // Base FGTS
  (*QrBase95.Close;
  QrBase95.Params[0].AsInteger := QrCalcCodigo.Value;
  QrBase95.Open;*)
  QrAcum17.Close;
  QrAcum17.Params[0].AsInteger := QrCalcCodigo.Value;
  QrAcum17.Params[1].AsInteger := QrCalcCodigo.Value;
  QrAcum17.Open;
  // Valor FGTS
  (*QrBase96.Close;
  QrBase96.Params[0].AsInteger := QrCalcCodigo.Value;
  QrBase96.Open;*)
  QrAcum03.Close;
  QrAcum03.Params[0].AsInteger := QrCalcCodigo.Value;
  QrAcum03.Params[1].AsInteger := QrCalcCodigo.Value;
  QrAcum03.Open;
  // Base INSS
  (*QrBase98.Close;
  QrBase98.Params[0].AsInteger := QrCalcCodigo.Value;
  QrBase98.Open;*)
  QrAcum28.Close;
  QrAcum28.Params[0].AsInteger := QrCalcCodigo.Value;
  QrAcum28.Params[1].AsInteger := QrCalcCodigo.Value;
  QrAcum28.Open;
  // Base IRRF
  (*QrBase99.Close;
  QrBase99.Params[0].AsInteger := QrCalcCodigo.Value;
  QrBase99.Open;*)
  QrAcum26.Close;
  QrAcum26.Params[0].AsInteger := QrCalcCodigo.Value;
  QrAcum26.Params[1].AsInteger := QrCalcCodigo.Value;
  QrAcum26.Open;
  //
end;

procedure TFmFPFolhaImp.QrCabAfterScroll(DataSet: TDataSet);
begin
  Dmod.QrEndereco.Close;
  Dmod.QrEndereco.Params[0].AsInteger := QrCabEmpresa.Value;
  Dmod.QrEndereco.Open;
  //
  QrIts.Close;
  QrIts.Params[0].AsInteger := QrCabControle.Value;
  QrIts.Open;
  //
  QrRpe.Close;
  QrRpe.Params[0].AsInteger := QrCabControle.Value;
  QrRpe.Open;
  //
end;

procedure TFmFPFolhaImp.QrCabCalcFields(DataSet: TDataSet);
var
  Dia, Mes, Ano: Word;
begin
  QrCabPERIODO_TXT.Value := MLAGeral.MesEAnoDoPeriodoLongo(QrCabPeriodo.Value);
  //
  if QrCabDiasPG.Value = 0 then QrCabDIASPG_TXT.Value := ''
  else QrCabDIASPG_TXT.Value := '(' +
    FormatFloat('00', QrCabDiasPG.Value) + ' dias de gozo.)';
  //
  if QrCabDiasPP.Value = 0 then QrCabDIASPP_TXT.Value := ''
  else QrCabDIASPP_TXT.Value := '(' +
    FormatFloat('00', QrCabDiasPP.Value) + ' dias de abono.)';
  //
  DecodeDate(QrCabDtIniPA.Value, Ano, Mes, Dia);
  QrCabPA_INI_DD.Value := FormatFloat('00;-00; ', Dia);
  QrCabPA_INI_MM.Value := MLAGeral.VerificaMes(Mes, True);
  QrCabPA_INI_AA.Value := FormatFloat('0000;-0000; ', Ano);
  //
  DecodeDate(QrCabDtIniPG.Value, Ano, Mes, Dia);
  QrCabPG_INI_DD.Value := FormatFloat('00;-00; ', Dia);
  QrCabPG_INI_MM.Value := MLAGeral.VerificaMes(Mes, True);
  QrCabPG_INI_AA.Value := FormatFloat('0000;-0000; ', Ano);
  //
  DecodeDate(QrCabDtIniPP.Value, Ano, Mes, Dia);
  QrCabPP_INI_DD.Value := FormatFloat('00;-00; ', Dia);
  QrCabPP_INI_MM.Value := MLAGeral.VerificaMes(Mes, True);
  QrCabPP_INI_AA.Value := FormatFloat('0000;-0000; ', Ano);
  //
  DecodeDate(QrCabDtFimPA.Value, Ano, Mes, Dia);
  QrCabPA_FIM_DD.Value := FormatFloat('00;-00; ', Dia);
  QrCabPA_FIM_MM.Value := MLAGeral.VerificaMes(Mes, True);
  QrCabPA_FIM_AA.Value := FormatFloat('0000;-0000; ', Ano);
  //
  DecodeDate(QrCabDtFimPG.Value, Ano, Mes, Dia);
  QrCabPG_FIM_DD.Value := FormatFloat('00;-00; ', Dia);
  QrCabPG_FIM_MM.Value := MLAGeral.VerificaMes(Mes, True);
  QrCabPG_FIM_AA.Value := FormatFloat('0000;-0000; ', Ano);
  //
  DecodeDate(QrCabDtFimPP.Value, Ano, Mes, Dia);
  QrCabPP_FIM_DD.Value := FormatFloat('00;-00; ', Dia);
  QrCabPP_FIM_MM.Value := MLAGeral.VerificaMes(Mes, True);
  QrCabPP_FIM_AA.Value := FormatFloat('0000;-0000; ', Ano);
  //
  QrCabDTRECPGTO_TXT.Value := dmkPF.FDT_NULO(QrCabDtRecPgto.Value, 6);
end;

procedure TFmFPFolhaImp.frxFolhaGetValue(const VarName: String;
  var Value: Variant);
begin
 if AnsiCompareText(VarName, 'VARF_NUMFOLHA') = 0 then
 begin
   Inc(FFolha, 1);
   Value := IntToStr(FFolha);
 end else if AnsiCompareText(VarName, 'VARF_ULTIMAFOLHA') = 0 then
 begin
   if QrCabUltimFolha.Value = 1 then Value := 0 else
   Value := QrCabNumFolha.Value + 1;
 end else if AnsiCompareText(VarName, 'ImagemExiste') = 0 then
   Value := FileExists(QrCabFoto.Value)
 else if AnsiCompareText(VarName, 'ImagemCaminho') = 0 then
   Value := QrCabFoto.Value
end;

procedure TFmFPFolhaImp.QrRpeCalcFields(DataSet: TDataSet);
begin
  QrRpeEXTENSO_TotalL.Value := dmkPF.ExtensoMoney(
    Geral.FFT(QrRpeTotalL.Value, 2, siPositivo));
end;

end.

