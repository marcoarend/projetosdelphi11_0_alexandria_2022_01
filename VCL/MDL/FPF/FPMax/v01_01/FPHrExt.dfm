object FmFPHrExt: TFmFPHrExt
  Left = 339
  Top = 185
  Width = 907
  Height = 530
  Caption = 'Horas Extras'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 899
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Horas Extras'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 813
      Height = 44
      Align = alClient
      Transparent = True
    end
    object LaTipo: TLabel
      Left = 815
      Top = 2
      Width = 82
      Height = 44
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 899
    Height = 448
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnTudo: TPanel
      Left = 0
      Top = 148
      Width = 899
      Height = 300
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 341
        Height = 300
        Align = alLeft
        TabOrder = 0
        object Label4: TLabel
          Left = 12
          Top = 24
          Width = 154
          Height = 13
          Caption = 'Empresa (Cadastro de entidade):'
        end
        object Label5: TLabel
          Left = 12
          Top = 8
          Width = 280
          Height = 13
          Caption = 'OBS.: Selecione a empresa antes do funcion�rio!'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 12
          Top = 64
          Width = 168
          Height = 13
          Caption = 'Funcion�rio (Cadastro de entidade):'
        end
        object EdEmpresa: TLMDEdit
          Left = 12
          Top = 39
          Width = 48
          Height = 21
          
          Caret.BlinkRate = 530
          CtlXP = False
          TabOrder = 0
          OnChange = EdEmpresaChange
          OnExit = EdEmpresaExit
          AutoSelect = True
          Alignment = taRightJustify
          CustomButtons = <>
          PasswordChar = #0
        end
        object CBEmpresa: TDBLookupComboBox
          Left = 62
          Top = 39
          Width = 275
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEEMPRESA'
          ListSource = DsEmpresa
          TabOrder = 1
          OnClick = CBEmpresaClick
          OnDropDown = CBEmpresaDropDown
        end
        object EdCodigo: TLMDEdit
          Left = 12
          Top = 79
          Width = 48
          Height = 21
          
          Caret.BlinkRate = 530
          CtlXP = False
          TabOrder = 2
          OnChange = EdCodigoChange
          OnExit = EdCodigoExit
          AutoSelect = True
          Alignment = taRightJustify
          CustomButtons = <>
          PasswordChar = #0
        end
        object CBCodigo: TDBLookupComboBox
          Left = 62
          Top = 79
          Width = 271
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEFUNCI'
          ListSource = DsFunci
          TabOrder = 3
          OnClick = CBCodigoClick
          OnDropDown = CBCodigoDropDown
        end
      end
      object Panel6: TPanel
        Left = 341
        Top = 0
        Width = 558
        Height = 300
        Align = alClient
        TabOrder = 1
        object PnEdita: TPanel
          Left = 1
          Top = 121
          Width = 556
          Height = 178
          Align = alClient
          TabOrder = 1
          Visible = False
          object Label3: TLabel
            Left = 68
            Top = 8
            Width = 62
            Height = 13
            Caption = 'Horas extras:'
          end
          object Label1: TLabel
            Left = 140
            Top = 8
            Width = 26
            Height = 13
            Caption = 'Data:'
          end
          object Label8: TLabel
            Left = 8
            Top = 8
            Width = 42
            Height = 13
            Caption = 'Controle:'
          end
          object RGFator: TRadioGroup
            Left = 245
            Top = 5
            Width = 183
            Height = 40
            Caption = ' Tipo de hora extra:'
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Cadastro'
              '100%')
            TabOrder = 2
          end
          object dmkEdHoras: TdmkEdit
            Left = 68
            Top = 24
            Width = 68
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            DecimalSize = 2
            LeftZeros = 0
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object TPDataHE: TDateTimePicker
            Left = 140
            Top = 24
            Width = 100
            Height = 21
            CalAlignment = dtaLeft
            Date = 39481.4110429282
            Time = 39481.4110429282
            DateFormat = dfShort
            DateMode = dmComboBox
            Kind = dtkDate
            ParseInput = False
            TabOrder = 1
          end
          object Panel8: TPanel
            Left = 1
            Top = 129
            Width = 554
            Height = 48
            Align = alBottom
            TabOrder = 4
            object BtConfirma: TBitBtn
              Tag = 14
              Left = 20
              Top = 4
              Width = 90
              Height = 40
              Caption = '&Confirma'
              TabOrder = 0
              OnClick = BtConfirmaClick
              NumGlyphs = 2
            end
            object Panel3: TPanel
              Left = 442
              Top = 1
              Width = 111
              Height = 46
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object BtDesiste: TBitBtn
                Tag = 13
                Left = 2
                Top = 3
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Sai da janela atual'
                Caption = '&Desiste'
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtDesisteClick
                NumGlyphs = 2
              end
            end
          end
          object dmkEdControle: TdmkEdit
            Left = 8
            Top = 24
            Width = 57
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBackground
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 5
            FormatType = dmktfInteger
            DecimalSize = 0
            LeftZeros = 6
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object CkContinua: TCheckBox
            Left = 432
            Top = 28
            Width = 117
            Height = 17
            Caption = 'Continuar inserindo.'
            TabOrder = 3
          end
        end
        object PnConfig: TPanel
          Left = 1
          Top = 1
          Width = 556
          Height = 120
          Align = alTop
          TabOrder = 0
          object GroupBox1: TGroupBox
            Left = 1
            Top = 1
            Width = 220
            Height = 70
            Caption = ' Per�odo de pesquisa: '
            TabOrder = 0
            object TPIni: TDateTimePicker
              Left = 8
              Top = 37
              Width = 100
              Height = 21
              CalAlignment = dtaLeft
              Date = 39481.4110429282
              Time = 39481.4110429282
              DateFormat = dfShort
              DateMode = dmComboBox
              Kind = dtkDate
              ParseInput = False
              TabOrder = 1
            end
            object TPFim: TDateTimePicker
              Left = 112
              Top = 37
              Width = 100
              Height = 21
              CalAlignment = dtaLeft
              Date = 39481.4110429282
              Time = 39481.4110429282
              DateFormat = dfShort
              DateMode = dmComboBox
              Kind = dtkDate
              ParseInput = False
              TabOrder = 3
            end
            object CkIni: TCheckBox
              Left = 8
              Top = 16
              Width = 97
              Height = 17
              Caption = 'Data inicial:'
              TabOrder = 0
            end
            object CkFim: TCheckBox
              Left = 112
              Top = 16
              Width = 97
              Height = 17
              Caption = 'Data final:'
              TabOrder = 2
            end
          end
          object PainelConfirma: TPanel
            Left = 1
            Top = 71
            Width = 554
            Height = 48
            Align = alBottom
            TabOrder = 1
            object BtOK: TBitBtn
              Tag = 22
              Left = 20
              Top = 4
              Width = 90
              Height = 40
              Caption = '&Pesquisa'
              TabOrder = 0
              OnClick = BtOKClick
              NumGlyphs = 2
            end
            object Panel2: TPanel
              Left = 442
              Top = 1
              Width = 111
              Height = 46
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object BtSaida: TBitBtn
                Tag = 13
                Left = 2
                Top = 3
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Sai da janela atual'
                Caption = '&Sa�da'
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtSaidaClick
                NumGlyphs = 2
              end
            end
            object BtInclui: TBitBtn
              Tag = 10
              Left = 148
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Inclui'
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BtIncluiClick
              NumGlyphs = 2
            end
            object BtAltera: TBitBtn
              Tag = 11
              Left = 240
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Altera'
              Enabled = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 3
              OnClick = BtAlteraClick
              NumGlyphs = 2
            end
            object BtExclui: TBitBtn
              Tag = 12
              Left = 332
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Exclui'
              Enabled = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 4
              OnClick = BtExcluiClick
              NumGlyphs = 2
            end
          end
          object GroupBox2: TGroupBox
            Left = 221
            Top = 1
            Width = 204
            Height = 70
            Caption = ' Somas da pesquisa atual: '
            TabOrder = 2
            object Label6: TLabel
              Left = 8
              Top = 20
              Width = 62
              Height = 13
              Caption = 'Horas extras:'
              FocusControl = DBEdit1
            end
            object Label7: TLabel
              Left = 104
              Top = 20
              Width = 94
              Height = 13
              Caption = '= a x horas normais:'
              FocusControl = DBEdit2
            end
            object DBEdit1: TDBEdit
              Left = 8
              Top = 36
              Width = 92
              Height = 21
              DataField = 'Horas'
              DataSource = DsSumHr
              TabOrder = 0
            end
            object DBEdit2: TDBEdit
              Left = 104
              Top = 36
              Width = 92
              Height = 21
              DataField = 'FatHr'
              DataSource = DsSumHr
              TabOrder = 1
            end
          end
        end
      end
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 0
      Top = 0
      Width = 899
      Height = 148
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMEEMP'
          Title.Caption = 'Empresa'
          Width = 330
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFUN'
          Title.Caption = 'Funcion�rio'
          Width = 330
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHE'
          Title.Caption = 'Data'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Horas'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Fator'
          Visible = True
        end>
      Color = clWindow
      DataSource = DsFPHrExt
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMEEMP'
          Title.Caption = 'Empresa'
          Width = 330
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFUN'
          Title.Caption = 'Funcion�rio'
          Width = 330
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHE'
          Title.Caption = 'Data'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Horas'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Fator'
          Visible = True
        end>
    end
  end
  object QrFunci: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, fu.HEFator, fu.Funci,'
      'IF(en.Nome="", en.RazaoSocial, en.Nome) NOMEFUNCI'
      'FROM entidades en'
      'LEFT JOIN fpfunci fu ON fu.Codigo=en.Codigo'
      'WHERE (Fornece2="V" OR Fornece4="V")'
      'AND DataDemiss =0'
      'AND (fu.Codigo IN ('
      '  SELECT Codigo FROM fpfunci'
      '  WHERE Empresa=1'
      '  AND DataDemiss=0))'
      'ORDER BY NOMEFUNCI'
      '')
    Left = 73
    Top = 9
    object QrFunciCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFunciHEFator: TFloatField
      FieldName = 'HEFator'
    end
    object QrFunciFunci: TIntegerField
      FieldName = 'Funci'
    end
    object QrFunciNOMEFUNCI: TWideStringField
      FieldName = 'NOMEFUNCI'
      Required = True
      Size = 100
    end
  end
  object DsFunci: TDataSource
    DataSet = QrFunci
    Left = 101
    Top = 9
  end
  object QrEmpresa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, '
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEEMPRESA'
      'FROM entidades en'
      'WHERE ???'
      'ORDER BY NOMEEMPRESA')
    Left = 132
    Top = 8
  end
  object DsEmpresa: TDataSource
    DataSet = QrEmpresa
    Left = 160
    Top = 8
  end
  object QrFPHrExt: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrFPHrExtAfterOpen
    BeforeClose = QrFPHrExtBeforeClose
    SQL.Strings = (
      'SELECT hre.*, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,'
      'IF(fun.Tipo=0, fun.RazaoSocial, fun.Nome) NOMEFUN'
      ' '
      'FROM fphrext hre'
      'LEFT JOIN entidades emp ON emp.Codigo=hre.Empresa'
      'LEFT JOIN entidades fun ON fun.Codigo=hre.Codigo')
    Left = 13
    Top = 9
    object QrFPHrExtControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFPHrExtCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFPHrExtEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrFPHrExtDataHE: TDateField
      FieldName = 'DataHE'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPHrExtHoras: TFloatField
      FieldName = 'Horas'
      Required = True
      DisplayFormat = '0.00'
    end
    object QrFPHrExtLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFPHrExtDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFPHrExtDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFPHrExtUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFPHrExtUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFPHrExtFator: TFloatField
      FieldName = 'Fator'
      Required = True
    end
    object QrFPHrExtNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrFPHrExtNOMEFUN: TWideStringField
      FieldName = 'NOMEFUN'
      Size = 100
    end
  end
  object DsFPHrExt: TDataSource
    DataSet = QrFPHrExt
    Left = 41
    Top = 9
  end
  object QrSumHr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(hre.Horas) Horas, SUM(hre.Horas*hre.Fator/100) FatHr'
      ''
      'FROM fphrext hre'
      'LEFT JOIN entidades emp ON emp.Codigo=hre.Empresa'
      'LEFT JOIN entidades fun ON fun.Codigo=hre.Codigo'
      ''
      'WHERE DataHE  > -1000')
    Left = 192
    Top = 8
    object QrSumHrHoras: TFloatField
      FieldName = 'Horas'
      DisplayFormat = '0.00'
    end
    object QrSumHrFatHr: TFloatField
      FieldName = 'FatHr'
      DisplayFormat = '0.00'
    end
  end
  object DsSumHr: TDataSource
    DataSet = QrSumHr
    Left = 218
    Top = 9
  end
  object PMExclui: TPopupMenu
    Left = 710
    Top = 325
    object ExcluiitemAtual1: TMenuItem
      Caption = 'Exclui item &Atual'
      OnClick = ExcluiitemAtual1Click
    end
    object ExcluiitensSelecionados1: TMenuItem
      Caption = 'Exclui itens &Selecionados'
      OnClick = ExcluiitensSelecionados1Click
    end
    object ExcluiTodositens1: TMenuItem
      Caption = '&Exclui &Todos itens'
      OnClick = ExcluiTodositens1Click
    end
  end
end


