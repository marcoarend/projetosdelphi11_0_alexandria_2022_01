unit FPtSalr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Grids, DBGrids,
  dmkDBGridDAC, Db, mySQLDbTables, dmkDBGrid, Menus;

type
  TFmFPtSalr = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DsFPtSalr: TDataSource;
    QrFPtSalr: TmySQLQuery;
    QrFPtSalrDataV: TDateField;
    QrFPtSalrValor: TFloatField;
    QrFPtSalrAtivo: TSmallintField;
    dmkDBGridDAC1: TdmkDBGridDAC;
    BtAcao: TBitBtn;
    PMAcao: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure dmkDBGridDAC1CellClick(Column: TColumn);
  private
    { Private declarations }
    procedure ReopenQrFPSalr(Data: TDateTime);
  public
    { Public declarations }
  end;

  var
  FmFPtSalr: TFmFPtSalr;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmFPtSalr.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPtSalr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPtSalr.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFPtSalr.FormCreate(Sender: TObject);
begin
  ReopenQrFPSalr(0);
end;

procedure TFmFPtSalr.Inclui1Click(Sender: TObject);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fptsalr SET DataV=:P0');
  Dmod.QrUpd.Params[0].AsString := Geral.FDT(Date, 1);
  Dmod.QrUpd.ExecSQL;
  //
  ReopenQrFPSalr(Int(Date));
end;

procedure TFmFPtSalr.ReopenQrFPSalr(Data: TDateTime);
begin
  QrFPtSalr.Close;
  QrFPtSalr.Open;
  if Data > 0 then
    QrFPtSalr.Locate('DataV', Data, []);
end;

procedure TFmFPtSalr.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmFPtSalr.Exclui1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o do registro selecionado?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fptsalr WHERE DataV=:P0');
    Dmod.QrUpd.SQL.Add('AND Valor=:P1');
    Dmod.QrUpd.Params[00].AsString  := Geral.FDT(QrFPtSalrDataV.Value, 1);
    Dmod.QrUpd.Params[01].AsFloat   := QrFPtSalrValor.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPSalr(Int(Date));
  end;
end;

procedure TFmFPtSalr.dmkDBGridDAC1CellClick(Column: TColumn);
var
  Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if QrFPtSalrAtivo.Value = 1 then Ativo := 0 else Ativo := 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE fptsalr SET Ativo=:P0');
    Dmod.QrUpd.SQL.Add('WHERE DataV=:P1');
    //
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsString  := Geral.FDT(QrFPtSalrDataV.Value, 1);
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPSalr(QrFPtSalrDataV.Value);
  end;
end;

end.

