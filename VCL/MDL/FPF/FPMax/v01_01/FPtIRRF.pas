unit FPtIRRF;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Grids, DBGrids,
  dmkDBGridDAC, Db, mySQLDbTables, dmkDBGrid, Menus;

type
  TFmFPtIRRF = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DsFPtIRRF: TDataSource;
    QrFPtIRRF: TmySQLQuery;
    dmkDBGridDAC1: TdmkDBGridDAC;
    BtAcao: TBitBtn;
    PMAcao: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui1: TMenuItem;
    QrFPtIRRFTeto: TFloatField;
    QrFPtIRRFAliquota: TFloatField;
    QrFPtIRRFDeducao: TFloatField;
    QrFPtIRRFAtivo: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure dmkDBGridDAC1CellClick(Column: TColumn);
  private
    { Private declarations }
    procedure ReopenQrFPtIRRF(Teto: Double);
  public
    { Public declarations }
  end;

  var
  FmFPtIRRF: TFmFPtIRRF;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmFPtIRRF.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPtIRRF.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPtIRRF.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFPtIRRF.FormCreate(Sender: TObject);
begin
  ReopenQrFPtIRRF(0);
end;

procedure TFmFPtIRRF.Inclui1Click(Sender: TObject);
var
  Teto: String;
  LimVal: Double;
begin
  Teto := '0,00';
  if InputQuery('Novo teto de sal�rio', 'Informe o sal�rio teto:',
  Teto) then
  begin
    LimVal := Geral.DMV(Teto);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO fptirrf SET Teto=:P0');
    Dmod.QrUpd.Params[0].AsFloat  := LimVal;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPtIRRF(LimVal);
  end;
end;

procedure TFmFPtIRRF.ReopenQrFPtIRRF(Teto: Double);
begin
  QrFPtIRRF.Close;
  QrFPtIRRF.Open;
  QrFPtIRRF.Locate('Teto', Teto, []);
end;

procedure TFmFPtIRRF.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmFPtIRRF.Exclui1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o do registro selecionado?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fptirrf WHERE Teto=:P0');
    Dmod.QrUpd.Params[00].AsFloat   := QrFPtIRRFTeto.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPtIRRF(Int(Date));
  end;
end;

procedure TFmFPtIRRF.dmkDBGridDAC1CellClick(Column: TColumn);
var
  Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if QrFPtIRRFAtivo.Value = 1 then Ativo := 0 else Ativo := 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE fptirrf SET Ativo=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Teto=:P1');
    //
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsFloat   := QrFPtIRRFTeto.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPtIRRF(QrFPtIRRFTeto.Value);
  end;
end;

end.

