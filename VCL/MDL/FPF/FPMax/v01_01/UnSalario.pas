unit UnSalario;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts, UnInternalConsts2, UnMsgInt,
  Db, DbCtrls,   Mask, Buttons, ZCF2, (*DBTables,*) ComCtrls,
  dmkEdit;

type
  TUnSalario = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure ColoreTexto(Form: TForm; NomeComp: String);
    //
    function dmkEdTur(Form: TForm; SubNome: String): TdmkEdit;
    function LaTur(Form: TForm; SubNome: String): TLabel;
    function ConfereHoras(Form: TForm; Turno: Integer; TempoHrNoturna,
             IniHrNoturna, FimHrNoturna: TDateTime): Boolean;
  end;

var
  USalario: TUnSalario;

const
  SegundosNoDia = 86400;

implementation

uses UnMyObjects, UnMLAGeral;

function TUnSalario.dmkEdTur(Form: TForm; SubNome: String): TdmkEdit;
var
  I: Integer;
begin
  Result := nil;
  for i := 0 to Form.ComponentCount - 1 do
  begin
    if Form.Components[i] is TdmkEdit then
    begin
      if TdmkEdit(Form.Components[i]).Name = 'dmkEdTur' + SubNome then
      begin
        Result := TdmkEdit(Form.Components[i]);
        Break;
      end;
    end;
  end;
end;

function TUnSalario.LaTur(Form: TForm; SubNome: String): TLabel;
var
  I: Integer;
begin
  Result := nil;
  for i := 0 to Form.ComponentCount - 1 do
  begin
    if Form.Components[i] is TLabel then
    begin
      if TLabel(Form.Components[i]).Name = 'LaTur' + SubNome then
      begin
        Result := TLabel(Form.Components[i]);
        Break;
      end;
    end;
  end;
end;

procedure TUnSalario.ColoreTexto(Form: TForm; NomeComp: String);
var
  Hr1, Hr2: TTime;
  Cor: Integer;
  txt: String;
begin
  Hr1 := TdmkEdit(dmkEdTur(Form, NomeComp)).ValueVariant;
  txt := TLabel(LaTur(Form, NomeComp)).Caption;
  if txt = '' then txt := '00:00';
  Hr2 := StrToTime(txt);
  Cor := MLAGeral.EscolhaDe3Int(Hr1, Hr2, clGreen, clBlue, clRed);
  TdmkEdit(dmkEdTur(Form, NomeComp)).Font.Color := Cor;
  TLabel(LaTur(Form, NomeComp)).Font.Color := Cor;
end;

function TUnSalario.ConfereHoras(Form: TForm; Turno: Integer; TempoHrNoturna,
IniHrNoturna, FimHrNoturna: TDateTime): Boolean;
  function Tempos(const HoI1, HoI2, HoF1, HoF2, ANi0, ANf1, ANf2, HNo: Double;
  var HrTc, HrNc: Double): Boolean;
  var
    HFim: Double;
  begin
    // Acerta hor�rio final
    Result := True;
    if HoF1 < HoI1 then
      HFim := HoF2
    else
      HFim := HoF1;
    //
    // Caso A: Come�a depois do hor�rio noturno final
    // e come�a antes e termina antes do hor�rio noturno inicial
    // N�O TEM ADICIONAL NOTURNO
    if (HoI1 >= ANf1) and (HoI1 <= ANi0) and (HFim <= ANi0) then
    begin
      // Soma simples de total de horas trabalhadas
      HrTc := HFim - HoI1
    end else

    // Caso B: Come�a depois do final do hor�rio noturno
    // e come�a antes do in�cio do hor�rio noturno
    // e termina depois do in�cio do hor�rio noturno
    // e termina dentro do (ou final) do hor�rio noturno
    if ((HoI1 >= ANf1) and (HoI1 <= ANi0) and (HFim > ANi0) and (HFim <= ANf2))
    // Caso C: Come�a depois do final do hor�rio noturno
    // e come�a antes do in�cio do hor�rio noturno
    // e termina depois do in�cio do hor�rio noturno
    // e termina depois do final do hor�rio noturno
    // CASO IGUAL AO CASO B, POIS A HORA NOTURNA SE EXTENDE AT� A SAIDA DO TURNO
    or ((HoI1 >= ANf1) and (HoI1 <= ANi0) and (HFIm > ANi0) and (HFim > ANf2)) then
    begin
      // Soma simples de total de horas da parte que n�o tem adicional noturno
      HrTc := ANi0 - HoI1;
      // Tempo de Adicional notuno j� calculado pela hora noturna
      HrNc := ((HFim - ANi0) / HNo) / 24;
    end else
    // Caso D: Come�a durante o hor�rio noturno
    // e termina durante o hor�rio noturno
    if ( (HoI2 >= ANi0)  and (HFIm <= ANf2) )
    // Caso E: Come�a durante o hor�rio noturno
    // e termina depois do final do hor�rio noturno
    // CASO IGUAL AO CASO D, POIS A HORA NOTURNA SE EXTENDE AT� A SAIDA DO TURNO
    or ( (HoI2 >= ANi0) and (HFim >= ANf2) ) then
    begin
      // N�o h� horas simples
      HrTc := 0;
      // Tempo de Adicional notuno j� calculado pela hora noturna
      if HFim > HoI1 then
        HrNc := ((HFim - HoI1) / HNo) / 24
      else
        HrNc := ((HFim - HoI2) / HNo) / 24;
    end else
    // Parei aqui! Fazer outros casos adicional noturno!
    Application.MessageBox(PChar('Situa��o de hor�rio n�o prevista!  ' +
    'AVISE A DERMATEK!'), 'Erro', MB_OK+MB_ICONERROR);
  end;
var
  HNo,  // Hora noturna conforme cadastro
  Ent1, // Hora de entrada de 0 a 1 (0:00 a 23:59)
  Ent2, // Hora de entrada de 0 a 1 (0:00 a 23:59 do dia seguinte)
  DeI1, // Hora inicial do descanso no meio do turno de 0 a 1 (0:00 a 23:59)
  DeI2, // Hora inicial do descanso no meio do turno de 0 a 2 (0:00 a 23:59 do dia seguinte)
  DeF1, // Hora final do descanso no meio do turno  de 0 a 1 (0:00 a 23:59)
  DeF2, // Hora final do descanso no meio do turno de 0 a 2 (0:00 a 23:59 do dia seguinte)
  Sai1, // Hora do t�rmino do turno de 0 a 1 (0:00 a 23:59)
  Sai2, // Hora do t�rmino do turno de 0 a 2 (0:00 a 23:59 do dia seguinte)
  {
  HrT,  // Total de horas normais trabalhadas (no dmkEdit)
  HrE,  // Total de horas extras (no dmkEdit)
  HrA,  // Total de horas alimenta��o (no dmkEdit)
  HrN,  // Total de horas de adicional noturno (no dmkEdit)
  HnB,  // Total de horas para adicionar ao banco de horas (no dmkEdit)
  }
  ANi0, // In�cio do adicional noturno (no dmkEdit) de 0 a 1 (0:00 a 23:59)
  //ANi2, // In�cio do adicional noturno (no dmkEdit) 0 a 2 (0:00 a 23:59 do dia seguinte)
  ANf1, // Final do adicional noturno de 0 a 1 (0:00 a 23:59)
  ANf2, // Final do adicional noturno de 0 a 2 (0:00 a 23:59 do dia seguinte)
  //
  HrTc, // C�lculo te�rico do total de horas normais trabalhadas
  HrAc, // C�lculo te�rico do total de horas alimenta��o
  HrNc, // C�lculo te�rico do total de horas de adicional noturno
  HrSc, // Soma dos c�lculos te�ricos de horas trabalhadas e adicional noturno
  // HrBc n�o calcula?
  //HrBc, // C�lculo te�rico do total de horas a adicionar ao banco de horas
  Aux1, Aux2: Double;
  x{, Txt1, Txt2}: String;
  //
  TemIntervalo, Trabalhou: Boolean;
begin
  HNo := TempoHrNoturna; // QrFPFunciANMinHr.Value;
  if HNo = 0 then HNo := 1 / 24;
  x    := IntToStr(Turno);
  //
  Ent1 := TdmkEdit(dmkEdTur(Form, x+'Ent')).ValueVariant;
  DeI1 := TdmkEdit(dmkEdTur(Form, x+'DeI')).ValueVariant;
  DeF1 := TdmkEdit(dmkEdTur(Form, x+'DeF')).ValueVariant;
  Sai1 := TdmkEdit(dmkEdTur(Form, x+'Sai')).ValueVariant;
  ANi0 := IniHrNoturna; //QrFPFunciANIniHr.Value;
  ANf1 := FimHrNoturna; //QrFPFunciANFimHr.Value;
  //
  TemIntervalo := Int(DeI1 * SegundosNoDia) <> Int(DeF1 * SegundosNoDia);
  Trabalhou := (Int(Ent1 * SegundosNoDia) <> 0) or (Int(Sai1 * SegundosNoDia) <> 0) or
     (Int(DeI1 * SegundosNoDia) <> 0) or (Int(DeF1 * SegundosNoDia) <> 0);
  // Ajusta hor�rios quando hor�rio menor que etapa anterior (1h + 24h = 25h)
  if TemIntervalo then
  begin
    if DeI1 < Ent1 then DeI2 := DeI1 + 1 else DeI2 := DeI1;
    if DeF1 < DeI2 then DeF2 := DeF1 + 1 else DeF2 := DeF1;
    if Sai1 < DeF2 then Sai2 := Sai1 + 1 else Sai2 := Sai1;
  end else begin
    DeI2 := DeI1;
    DeF2 := DeF1;
    if Sai1 < Ent1 then Sai2 := Sai1 + 1 else Sai2 := Sai1;
  end;
  if ANf1 < ANi0 then
  begin
    ANf2 := ANf1 + 1;
    Ent2 := Ent1 + 1;
  end else begin
    ANf2 := ANf1;
    Ent2 := Ent1;
  end;
  //
  HrSc := 0;
  HrTc := 0;
  HrAc := 0;
  HrNc := 0;


  if Trabalhou then
  begin
    // caso n�o haja intervalo
    Tempos(Ent1, Ent2, Sai1, Sai2, ANi0, ANf1, ANf2, HNo, HrTc, HrNc);
    if TemIntervalo then
    begin
      Tempos(DeI1, DeI2, DeF1, DeF2, ANi0, ANf1, ANf2, HNo, Aux1, Aux2);
      HrTc := HrTc - Aux1;
      HrNc := HrNc - Aux2;
    end;
    // Soma as horas normais trabalhadas a as que tem adicional noturno j� calculando a hora noturna
    HrSc := HrTc + HrNc;
    //
    if not TemIntervalo then
    begin
      // Adicional alimenta��o por n�o haver intervalo
      // Verifica se tem hora alimenta��o
      // se o hor�rio for cont�nuo e superior a 6 horas ent�o tem uma "hora alimenta��o"
      if HrSc > 6.016/24 then
        HrAc := 1/24 else
      // se o hor�rio for cont�nuo e superior a 4 horas ent�o tem 15 min de "hora alimenta��o"
      if HrSc > 4.016/24 then
        HrAc := 0.25/24 // 15 minutos
      // caso contr�rio n�o tem "hora alimenta��o"
      else HrAc := 0;
    end;
  end;

  // Verifica se n�o extrapolou 24 horas
  if (HrSc > 1) or (HrSc < 0) then
  begin
    HrSc := 0;
    HrTc := 0;
    HrAc := 0;
    HrNc := 0;
  end;

  // Mostra os resultados nos labels
  // Horas trabalhadas
  TLabel(LaTur(Form, x+'HrT')).Caption := Geral.FDT(HrTc, 100);
  // Horas noturnas
  TLabel(LaTur(Form, x+'HrN')).Caption := Geral.FDT(HrNc, 100);
  // Horas somadas de trabalhadas e noturnas
  // testar
  TLabel(LaTur(Form, x+'HrS')).Caption := Geral.FDT(HrSc, 100);

  // Horas alimenta��o
  TLabel(LaTur(Form, x+'HrA')).Caption := Geral.FDT(HrAc, 100);

  //

  Result := True;
end;

end.
