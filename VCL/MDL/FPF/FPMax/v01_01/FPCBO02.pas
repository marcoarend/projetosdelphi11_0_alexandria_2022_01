unit FPCBO02;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Grids, DBGrids,
  dmkDBGridDAC, Db, mySQLDbTables, dmkDBGrid;

type
  TFmFPCBO02 = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    TbFPCBO02: TmySQLTable;
    TbFPCBO02CodID: TWideStringField;
    TbFPCBO02Descricao: TWideStringField;
    DsFPCBO02: TDataSource;
    DBGrid1: TDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TbFPCBO02BeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmFPCBO02: TFmFPCBO02;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmFPCBO02.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPCBO02.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPCBO02.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFPCBO02.FormCreate(Sender: TObject);
begin
  TbFPCBO02.Open;
end;

procedure TFmFPCBO02.TbFPCBO02BeforePost(DataSet: TDataSet);
begin
  TbFPCBO02CodID.Value := MLAGeral.SoNumeroELetraESeparadores_TT(TbFPCBO02CodID.Value);
end;

end.
