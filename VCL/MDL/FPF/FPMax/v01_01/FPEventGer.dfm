object FmFPEventGer: TFmFPEventGer
  Left = 339
  Top = 185
  Width = 800
  Height = 530
  Caption = 'Gerenciamento de Eventos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 169
    Width = 792
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtSalva: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = 'Sal&va'
      Enabled = False
      TabOrder = 0
      OnClick = BtSalvaClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 680
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa�da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object BtInclui: TBitBtn
      Tag = 10
      Left = 112
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Inclui'
      Enabled = False
      TabOrder = 2
      OnClick = BtIncluiClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Gerenciamento de Eventos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 788
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 121
    Align = alTop
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 108
      Height = 119
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'Panel3'
      TabOrder = 0
      object RGAuto: TRadioGroup
        Left = 0
        Top = 0
        Width = 108
        Height = 99
        Align = alTop
        Caption = ' Tipo de evento: '
        ItemIndex = 2
        Items.Strings = (
          'Autom�tico'
          'Do usu�rio'
          'Todos')
        TabOrder = 0
        OnClick = RGAutoClick
      end
      object CkAtivo: TCheckBox
        Left = 4
        Top = 100
        Width = 53
        Height = 17
        Caption = 'Ativo.'
        TabOrder = 1
        OnClick = CkAtivoClick
      end
    end
    object Panel4: TPanel
      Left = 109
      Top = 1
      Width = 682
      Height = 119
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object DBLookupListBox1: TDBLookupListBox
        Left = 0
        Top = 0
        Width = 376
        Height = 108
        Align = alClient
        KeyField = 'Codigo'
        ListField = 'EXIBICAO'
        ListSource = DsFPEvn
        TabOrder = 0
      end
      object Panel11: TPanel
        Left = 376
        Top = 0
        Width = 306
        Height = 119
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object GroupBox1: TGroupBox
          Left = 0
          Top = 39
          Width = 306
          Height = 80
          Align = alClient
          Caption = ' Pesquisa nos eventos (Conforme tipo selecionado): '
          TabOrder = 0
          object Label1: TLabel
            Left = 12
            Top = 20
            Width = 51
            Height = 13
            Caption = 'Descri��o:'
          end
          object EdPesqDescri: TEdit
            Left = 12
            Top = 36
            Width = 281
            Height = 21
            TabOrder = 0
            Text = '%%'
            OnChange = EdPesqDescriChange
          end
        end
        object RGOrdem: TRadioGroup
          Left = 0
          Top = 0
          Width = 306
          Height = 39
          Align = alTop
          Caption = ' Ordenar eventos por: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'C�digo'
            'Descri��o'
            'Natureza')
          TabOrder = 1
          OnClick = RGOrdemClick
        end
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 172
    Width = 792
    Height = 276
    Align = alClient
    TabOrder = 3
    object Panel6: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 48
      Align = alTop
      TabOrder = 0
      object LaCodigo: TLabel
        Left = 4
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C�digo:'
        Enabled = False
      end
      object LaDescricao: TLabel
        Left = 72
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri��o:'
      end
      object LaBaseCod: TLabel
        Left = 472
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Base:'
      end
      object LaPrioridade: TLabel
        Left = 732
        Top = 4
        Width = 50
        Height = 13
        Caption = 'Prioridade:'
      end
      object LaPercentual: TLabel
        Left = 664
        Top = 4
        Width = 54
        Height = 13
        Caption = 'Percentual:'
      end
      object EdDescricao: TEdit
        Left = 72
        Top = 20
        Width = 397
        Height = 21
        TabOrder = 0
        OnChange = EdDescricaoChange
      end
      object EdBaseCod: TLMDEdit
        Left = 472
        Top = 20
        Width = 33
        Height = 21
        
        Caret.BlinkRate = 530
        TabOrder = 1
        Visible = False
        OnChange = EdBaseCodChange
        OnExit = EdBaseCodExit
        Alignment = taRightJustify
        CustomButtons = <>
        PasswordChar = #0
      end
      object CBBaseCod: TDBLookupComboBox
        Left = 472
        Top = 20
        Width = 189
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsFPVaria
        TabOrder = 2
        OnClick = CBBaseCodClick
        OnDropDown = CBBaseCodDropDown
      end
      object dmkEdPercentual: TdmkEdit
        Left = 664
        Top = 20
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        DecimalSize = 2
        LeftZeros = 0
        ValMin = '0'
        ValMax = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        OnChange = dmkEdPercentualChange
      end
      object dmkEdPrioridade: TdmkEdit
        Left = 732
        Top = 20
        Width = 37
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        DecimalSize = 0
        LeftZeros = 0
        ValMin = '45'
        ValMax = '55'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        OnChange = dmkEdPrioridadeChange
      end
      object UdPrioridade: TUpDown
        Left = 769
        Top = 20
        Width = 16
        Height = 21
        Associate = dmkEdPrioridade
        Min = 45
        Max = 55
        Position = 45
        TabOrder = 5
        TabStop = True
        Wrap = False
        OnChanging = UdPrioridadeChanging
      end
      object EdCodigo: TLMDEdit
        Left = 4
        Top = 20
        Width = 65
        Height = 21
        
        Caret.BlinkRate = 530
        Enabled = False
        TabOrder = 6
        OnChange = EdBaseCodChange
        OnExit = EdBaseCodExit
        Alignment = taRightJustify
        CustomButtons = <>
        PasswordChar = #0
      end
    end
    object Panel7: TPanel
      Left = 1
      Top = 49
      Width = 790
      Height = 226
      Align = alClient
      TabOrder = 1
      object Panel8: TPanel
        Left = 1
        Top = 1
        Width = 108
        Height = 224
        Align = alLeft
        TabOrder = 0
        object RGNatureza: TRadioGroup
          Left = 1
          Top = 1
          Width = 106
          Height = 76
          Align = alTop
          Caption = ' Natureza: '
          Items.Strings = (
            'Provento'
            'Desconto')
          TabOrder = 0
          TabStop = True
          OnClick = RGNaturezaClick
          OnEnter = RGNaturezaEnter
          OnExit = RGNaturezaExit
        end
      end
      object Panel9: TPanel
        Left = 109
        Top = 1
        Width = 185
        Height = 224
        Align = alLeft
        TabOrder = 1
        object LBVaria: TCheckListBox
          Left = 1
          Top = 18
          Width = 119
          Height = 205
          OnClickCheck = LBVariaClickCheck
          Align = alClient
          ItemHeight = 13
          TabOrder = 0
        end
        object LBCodVaria: TListBox
          Left = 120
          Top = 18
          Width = 64
          Height = 205
          Align = alRight
          ItemHeight = 13
          TabOrder = 1
        end
        object StaticText1: TStaticText
          Left = 1
          Top = 1
          Width = 183
          Height = 17
          Align = alTop
          Alignment = taCenter
          Caption = ' Vari�veis de incid�ncia'
          TabOrder = 2
        end
      end
      object Panel10: TPanel
        Left = 294
        Top = 1
        Width = 495
        Height = 224
        Align = alClient
        TabOrder = 2
        object StaticText2: TStaticText
          Left = 1
          Top = 1
          Width = 493
          Height = 17
          Align = alTop
          Caption = '  Acumuladores'
          TabOrder = 0
        end
        object LBAcumu: TCheckListBox
          Left = 1
          Top = 18
          Width = 429
          Height = 205
          OnClickCheck = LBAcumuClickCheck
          Align = alClient
          ItemHeight = 13
          TabOrder = 1
        end
        object LBCodAcumu: TListBox
          Left = 430
          Top = 18
          Width = 64
          Height = 205
          Align = alRight
          ItemHeight = 13
          TabOrder = 2
        end
      end
    end
  end
  object QrFPEvn: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrFPEvnAfterScroll
    OnCalcFields = QrFPEvnCalcFields
    SQL.Strings = (
      'SELECT va.Nome NOMEVARI, ev.*'
      'FROM fpevent ev'
      'LEFT JOIN fpvaria va ON va.Codigo=ev.Base'
      'WHERE ev.Auto IN (:P0, :P1)')
    Left = 6
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFPEvnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPEvnDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 50
    end
    object QrFPEvnEXIBICAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EXIBICAO'
      Size = 100
      Calculated = True
    end
    object QrFPEvnBase: TIntegerField
      FieldName = 'Base'
    end
    object QrFPEvnPercentual: TFloatField
      FieldName = 'Percentual'
      DisplayFormat = '0.0000'
    end
    object QrFPEvnPrioridade: TSmallintField
      FieldName = 'Prioridade'
    end
    object QrFPEvnNOMEVARI: TWideStringField
      FieldName = 'NOMEVARI'
      Size = 30
    end
    object QrFPEvnAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrFPEvnNatureza: TWideStringField
      FieldName = 'Natureza'
      Size = 1
    end
    object QrFPEvnAuto: TSmallintField
      FieldName = 'Auto'
      Required = True
    end
    object QrFPEvnNOME_BASE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_BASE'
      Size = 50
      Calculated = True
    end
  end
  object DsFPEvn: TDataSource
    DataSet = QrFPEvn
    Left = 34
    Top = 9
  end
  object QrFPVaria: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrFPVariaAfterOpen
    BeforeClose = QrFPVariaBeforeClose
    SQL.Strings = (
      'SELECT Codigo, Nome, Visivel'
      'FROM fpvaria')
    Left = 97
    Top = 9
    object QrFPVariaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPVariaNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrFPVariaVisivel: TSmallintField
      FieldName = 'Visivel'
    end
    object QrFPVariaNOME_BASE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_BASE'
      Size = 50
      Calculated = True
    end
  end
  object QrFPEvnVar: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM fpeventvar'
      'WHERE Event=:P0')
    Left = 125
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFPEvnVarVaria: TIntegerField
      FieldName = 'Varia'
    end
    object QrFPEvnVarAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrFPEvnAcu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM fpeventacu'
      'WHERE Event=:P0')
    Left = 192
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFPEvnAcuAcumu: TIntegerField
      FieldName = 'Acumu'
    end
    object QrFPEvnAcuAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrFPAcumu: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrFPAcumuAfterOpen
    BeforeClose = QrFPAcumuBeforeClose
    SQL.Strings = (
      'SELECT Codigo, Descricao, Visivel'
      'FROM fpacumu')
    Left = 164
    Top = 9
    object QrFPAcumuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPAcumuDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 30
    end
    object QrFPAcumuVisivel: TSmallintField
      FieldName = 'Visivel'
    end
  end
  object QrLAcu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Ativo '
      'FROM fpeventacu'
      'WHERE Event=:P0'
      'AND Acumu=:P1')
    Left = 205
    Top = 69
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLAcuAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrLVar: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Ativo '
      'FROM fpeventvar'
      'WHERE Event=:P0'
      'AND Varia=:P1')
    Left = 233
    Top = 69
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLVarAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFPVaria: TDataSource
    DataSet = QrFPVaria
    Left = 69
    Top = 9
  end
end


