unit FPParam;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Grids, DBGrids,
  dmkDBGridDAC, Db, mySQLDbTables, dmkDBGrid, Menus;

type
  TFmFPParam = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DsFPParam: TDataSource;
    QrFPParam: TmySQLQuery;
    dmkDBGridDAC1: TdmkDBGridDAC;
    BtAcao: TBitBtn;
    PMAcao: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui1: TMenuItem;
    QrFPParamDescricao: TWideStringField;
    QrFPParamValor: TFloatField;
    QrFPParamParametro: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure dmkDBGridDAC1CellClick(Column: TColumn);
  private
    { Private declarations }
    procedure ReopenQrFPParam(Parametro: String);
  public
    { Public declarations }
  end;

  var
  FmFPParam: TFmFPParam;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmFPParam.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPParam.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPParam.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFPParam.FormCreate(Sender: TObject);
begin
  ReopenQrFPParam('');
end;

procedure TFmFPParam.Inclui1Click(Sender: TObject);
begin
  {Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpparam SET DataV=:P0');
  Dmod.QrUpd.Params[0].AsString := Geral.FDT(Date, 1);
  Dmod.QrUpd.ExecSQL;
  //
  ReopenQrFPParam(Int(Date));}
end;

procedure TFmFPParam.ReopenQrFPParam(Parametro: String);
begin
  QrFPParam.Close;
  QrFPParam.Open;
  if Parametro <> '' then
    QrFPParam.Locate('Parametro', Parametro, []);
end;

procedure TFmFPParam.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmFPParam.Exclui1Click(Sender: TObject);
begin
  {if Application.MessageBox('Confirma a exclus�o do registro selecionado?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fpparam WHERE DataV=:P0');
    Dmod.QrUpd.SQL.Add('AND Valor=:P1');
    Dmod.QrUpd.Params[00].AsString  := Geral.FDT(QrFPParamDataV.Value, 1);
    Dmod.QrUpd.Params[01].AsFloat   := QrFPParamValor.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPParam(Int(Date));
  end;}
end;

procedure TFmFPParam.dmkDBGridDAC1CellClick(Column: TColumn);
{var
  Ativo: Integer;}
begin
  {if Column.FieldName = 'Ativo' then
  begin
    if QrFPParamAtivo.Value = 1 then Ativo := 0 else Ativo := 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE fpparam SET Ativo=:P0');
    Dmod.QrUpd.SQL.Add('WHERE DataV=:P1');
    //
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsString  := Geral.FDT(QrFPParamDataV.Value, 1);
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPParam(QrFPParamDataV.Value);
  end;}
end;

end.

