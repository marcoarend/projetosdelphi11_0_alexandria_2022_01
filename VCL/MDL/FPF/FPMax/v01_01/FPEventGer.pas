unit FPEventGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Db,
  mySQLDbTables, DBCtrls, Mask,   
  dmkEdit, ComCtrls, CheckLst;

type
  TFmFPEventGer = class(TForm)
    PainelConfirma: TPanel;
    BtSalva: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    Panel4: TPanel;
    RGAuto: TRadioGroup;
    Panel5: TPanel;
    Splitter1: TSplitter;
    QrFPEvn: TmySQLQuery;
    DsFPEvn: TDataSource;
    DBLookupListBox1: TDBLookupListBox;
    QrFPEvnCodigo: TIntegerField;
    QrFPEvnDescricao: TWideStringField;
    QrFPEvnEXIBICAO: TWideStringField;
    QrFPEvnBase: TIntegerField;
    QrFPEvnPercentual: TFloatField;
    QrFPEvnPrioridade: TSmallintField;
    QrFPEvnNOMEVARI: TWideStringField;
    Panel6: TPanel;
    LaCodigo: TLabel;
    LaDescricao: TLabel;
    LaBaseCod: TLabel;
    LaPrioridade: TLabel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    RGNatureza: TRadioGroup;
    QrFPEvnAtivo: TSmallintField;
    QrFPEvnNatureza: TWideStringField;
    CkAtivo: TCheckBox;
    EdDescricao: TEdit;
    EdBaseCod: TLMDEdit;
    QrFPVaria: TmySQLQuery;
    QrFPVariaCodigo: TIntegerField;
    QrFPVariaNome: TWideStringField;
    CBBaseCod: TDBLookupComboBox;
    dmkEdPercentual: TdmkEdit;
    dmkEdPrioridade: TdmkEdit;
    UdPrioridade: TUpDown;
    LBVaria: TCheckListBox;
    LBCodVaria: TListBox;
    QrFPVariaVisivel: TSmallintField;
    QrFPEvnVar: TmySQLQuery;
    QrFPEvnVarVaria: TIntegerField;
    QrFPEvnVarAtivo: TSmallintField;
    EdCodigo: TLMDEdit;
    QrFPEvnAuto: TSmallintField;
    LaPercentual: TLabel;
    QrFPEvnNOME_BASE: TWideStringField;
    QrFPVariaNOME_BASE: TWideStringField;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    LBAcumu: TCheckListBox;
    LBCodAcumu: TListBox;
    QrFPEvnAcu: TmySQLQuery;
    QrFPAcumu: TmySQLQuery;
    QrFPAcumuCodigo: TIntegerField;
    QrFPAcumuDescricao: TWideStringField;
    QrFPAcumuVisivel: TSmallintField;
    QrFPEvnAcuAcumu: TIntegerField;
    QrFPEvnAcuAtivo: TSmallintField;
    BtInclui: TBitBtn;
    QrLAcu: TmySQLQuery;
    QrLVar: TmySQLQuery;
    QrLVarAtivo: TSmallintField;
    QrLAcuAtivo: TSmallintField;
    DsFPVaria: TDataSource;
    Panel11: TPanel;
    GroupBox1: TGroupBox;
    RGOrdem: TRadioGroup;
    Label1: TLabel;
    EdPesqDescri: TEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFPEvnCalcFields(DataSet: TDataSet);
    procedure RGAutoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrFPEvnAfterScroll(DataSet: TDataSet);
    procedure EdBaseCodChange(Sender: TObject);
    procedure EdBaseCodExit(Sender: TObject);
    procedure CBBaseCodClick(Sender: TObject);
    procedure CBBaseCodDropDown(Sender: TObject);
    procedure QrFPVariaBeforeClose(DataSet: TDataSet);
    procedure QrFPVariaAfterOpen(DataSet: TDataSet);
    procedure QrFPAcumuBeforeClose(DataSet: TDataSet);
    procedure QrFPAcumuAfterOpen(DataSet: TDataSet);
    procedure LBVariaClickCheck(Sender: TObject);
    procedure LBAcumuClickCheck(Sender: TObject);
    procedure RGNaturezaClick(Sender: TObject);
    procedure EdDescricaoChange(Sender: TObject);
    procedure dmkEdPercentualChange(Sender: TObject);
    procedure dmkEdPrioridadeChange(Sender: TObject);
    procedure UdPrioridadeChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure CkAtivoClick(Sender: TObject);
    procedure BtSalvaClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure RGNaturezaEnter(Sender: TObject);
    procedure RGNaturezaExit(Sender: TObject);
    procedure EdPesqDescriChange(Sender: TObject);
    procedure RGOrdemClick(Sender: TObject);
  private
    { Private declarations }
    FCompo: String;
    procedure ReopenEvn(Codigo: Integer);
    procedure SetaVaria();
    procedure SetaAcumu();
    procedure HabilitaComponentes();
  public
    { Public declarations }
  end;

  var
  FmFPEventGer: TFmFPEventGer;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts;

{$R *.DFM}

procedure TFmFPEventGer.BtSaidaClick(Sender: TObject);
begin
  VAR_FPEVENT := QrFPEvnCodigo.Value;
  Close;
end;

procedure TFmFPEventGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPEventGer.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFPEventGer.QrFPEvnCalcFields(DataSet: TDataSet);
begin
  QrFPEvnEXIBICAO.Value := IntToStr(QrFPEvnCodigo.Value) +
  ' - ' + QrFPEvnDescricao.Value;
  if QrFPEvnBase.Value = 0 then
    QrFPEvnNOME_BASE.Value := '(Sem base - Valor informado)' else
  QrFPEvnNOME_BASE.Value := QrFPEvnNOMEVARI.Value;
end;

procedure TFmFPEventGer.RGAutoClick(Sender: TObject);
begin
  ReopenEvn(0);
  BtInclui.Enabled := RGAuto.ItemIndex = 1;
end;

procedure TFmFPEventGer.ReopenEvn(Codigo: Integer);
const
  Ordem: array[0..2] of String = ('Codigo', 'Descricao', 'Natureza');
var
  Auto, Manu, Pesq: String;
begin
  Auto := MLAGeral.EscolhaDe2Str(RGAuto.Itemindex in ([0,2]), '1', '-1000');
  Manu := MLAGeral.EscolhaDe2Str(RGAuto.Itemindex in ([1,2]), '0', '-1000');
  //
  QrFPEvn.Close;
  QrFPEvn.SQL.Clear;
  QrFPEvn.SQL.Add('SELECT va.Nome NOMEVARI, ev.*');
  QrFPEvn.SQL.Add('FROM fpevent ev');
  QrFPEvn.SQL.Add('LEFT JOIN fpvaria va ON va.Codigo=ev.Base');
  QrFPEvn.SQL.Add('WHERE ev.Auto IN (:P0, :P1)');
  Pesq := Trim(EdPesqDescri.Text);
  if (Pesq <> '') and (Pesq <> '%%') then
    QrFPEvn.SQL.Add('AND ev.Descricao LIKE "' + Pesq + '"');
  QrFPEvn.SQL.Add('ORDER BY ' + Ordem[RGOrdem.ItemIndex]);
  //
  QrFPEvn.Params[00].AsString := Auto;
  QrFPEvn.Params[01].AsString := Manu;
  QrFPEvn.Open;
  //
  if Codigo > 0 then QrFPEvn.Locate('Codigo', Codigo, []);
end;

procedure TFmFPEventGer.FormCreate(Sender: TObject);
begin
  FCompo := '';
  QrFPVaria.Open;
  QrFPAcumu.Open;
  ReopenEvn(0);
end;

procedure TFmFPEventGer.EdBaseCodChange(Sender: TObject);
begin
  //MLAGeral.SincroI(FmFPEventGer, Sender, 0, siNegativo);
end;

procedure TFmFPEventGer.EdBaseCodExit(Sender: TObject);
begin
  //MLAGeral.SincroI(FmFPEventGer, Sender, 1, siNegativo);
end;

procedure TFmFPEventGer.CBBaseCodClick(Sender: TObject);
begin
  //MLAGeral.SincroI(FmFPEventGer, Sender, 2, siNegativo);
  if CBBaseCod.Focused then
    BtSalva.Enabled := True;
end;

procedure TFmFPEventGer.CBBaseCodDropDown(Sender: TObject);
begin
  //MLAGeral.SincroI(FmFPEventGer, Sender, 3, siNegativo);
end;

procedure TFmFPEventGer.QrFPEvnAfterScroll(DataSet: TDataSet);
begin
  if QrFPEvnNatureza.Value = 'D' then
    RGNatureza.ItemIndex := 1 else
    RGNatureza.ItemIndex := 0;
  EdCodigo.Text        := IntToStr(QrFPEvnCodigo.Value);
  CkAtivo.Checked      := Geral.IntToBool_0(QrFPEvnAtivo.Value);
  EdBaseCod.Text       := IntToStr(QrFPEvnBase.Value);
  CBBaseCod.KeyValue   := QrFPEvnBase.Value;
  dmkEdPercentual.Text := Geral.FFT(QrFPEvnPercentual.Value, 2, siPositivo);
  dmkEdPrioridade.Text := IntToStr(QrFPEvnPrioridade.Value);
  EdDescricao.Text     := QrFPEvnDescricao.Value;
  //
  HabilitaComponentes();
  SetaVaria;
  SetaAcumu;
  //
  BtSalva.Enabled := False;
end;

procedure TFmFPEventGer.QrFPVariaBeforeClose(DataSet: TDataSet);
begin
  LBVaria.Items.Clear;
  LBCodVaria.Items.Clear;
end;

procedure TFmFPEventGer.QrFPVariaAfterOpen(DataSet: TDataSet);
begin
  QrFPVaria.First;
  while not QrFPVaria.Eof do
  begin
    if QrFPVariaVisivel.Value = 1 then
    begin
      LBVaria.Items.Add(QrFPVariaNome.Value);
      LBCodVaria.Items.Add(IntToStr(QrFPVariaCodigo.Value));
    end;
    QrFPVaria.Next;
  end;
  SetaVaria;
end;

procedure TFmFPEventGer.HabilitaComponentes();
var
  Habilita: Boolean;
begin
  Habilita := not Geral.IntToBool_0(QrFPEvnAuto.Value);
  //
  LaBaseCod.Enabled       := Habilita;
  EdBaseCod.Enabled       := Habilita;
  CBBaseCod.Enabled       := Habilita;
  LaPercentual.Enabled    := Habilita;
  dmkEdPercentual.Enabled := Habilita;
  LaPrioridade.Enabled    := Habilita;
  dmkEdPrioridade.Enabled := Habilita;
  RGNatureza.Enabled      := Habilita;
  //
  UdPrioridade.Enabled    := Habilita;
  //
end;

procedure TFmFPEventGer.SetaVaria();
var
  i: Integer;
begin
  QrFPEvnVar.Close;
  QrFPEvnVar.Params[0].AsInteger := QrFPEvnCodigo.Value;
  QrFPEvnVar.Open;
  for i := 0 to LBVaria.Items.Count - 1 do
    LBVaria.Checked[i] := False;
  //
  QrFPEvnVar.Close;
  QrFPEvnVar.Params[0].AsInteger := QrFPEvnCodigo.Value;
  QrFPEvnVar.Open;
  while not QrFPEvnVar.Eof do
  begin
    if QrFPEvnVarAtivo.Value = 1 then
    begin
      for i := 0 to LBCodVaria.Items.Count - 1 do
      begin
        if StrToInt(LBCodVaria.Items[i]) = QrFPEvnVarVaria.Value then
        begin
          LBVaria.Checked[i] := True;
          Break;
        end;
      end;
    end;
    QrFPEvnVar.Next;
  end;
end;

procedure TFmFPEventGer.QrFPAcumuBeforeClose(DataSet: TDataSet);
begin
  LBAcumu.Items.Clear;
  LBCodAcumu.Items.Clear;
end;

procedure TFmFPEventGer.QrFPAcumuAfterOpen(DataSet: TDataSet);
begin
  QrFPAcumu.First;
  while not QrFPAcumu.Eof do
  begin
    if QrFPAcumuVisivel.Value = 1 then
    begin
      LBAcumu.Items.Add(QrFPAcumuDescricao.Value);
      LBCodAcumu.Items.Add(IntToStr(QrFPAcumuCodigo.Value));
    end;
    QrFPAcumu.Next;
  end;
  SetaAcumu;
end;

procedure TFmFPEventGer.SetaAcumu();
var
  i: Integer;
begin
  QrFPEvnAcu.Close;
  QrFPEvnAcu.Params[0].AsInteger := QrFPEvnCodigo.Value;
  QrFPEvnAcu.Open;
  for i := 0 to LBAcumu.Items.Count - 1 do
    LBAcumu.Checked[i] := False;
  //
  QrFPEvnAcu.Close;
  QrFPEvnAcu.Params[0].AsInteger := QrFPEvnCodigo.Value;
  QrFPEvnAcu.Open;
  while not QrFPEvnAcu.Eof do
  begin
    if QrFPEvnAcuAtivo.Value = 1 then
    begin
      for i := 0 to LBCodAcumu.Items.Count - 1 do
      begin
        if StrToInt(LBCodAcumu.Items[i]) = QrFPEvnAcuAcumu.Value then
        begin
          LBAcumu.Checked[i] := True;
          Break;
        end;
      end;
    end;
    QrFPEvnAcu.Next;
  end;
end;

procedure TFmFPEventGer.LBVariaClickCheck(Sender: TObject);
begin
  if LBVaria.Focused then
    BtSalva.Enabled := True;
end;

procedure TFmFPEventGer.LBAcumuClickCheck(Sender: TObject);
begin
  if LBAcumu.Focused then
    BtSalva.Enabled := True;
end;

procedure TFmFPEventGer.RGNaturezaClick(Sender: TObject);
begin
  if FCompo = RGNatureza.Name then
    BtSalva.Enabled := True;
end;

procedure TFmFPEventGer.EdDescricaoChange(Sender: TObject);
begin
  if EdDescricao.Focused then
    BtSalva.Enabled := True;
end;

procedure TFmFPEventGer.dmkEdPercentualChange(Sender: TObject);
begin
  if dmkEdPercentual.Focused then
    BtSalva.Enabled := True;
end;

procedure TFmFPEventGer.dmkEdPrioridadeChange(Sender: TObject);
begin
  if dmkEdPrioridade.Focused then
    BtSalva.Enabled := True;
end;

procedure TFmFPEventGer.UdPrioridadeChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  if UdPrioridade.Focused then
    BtSalva.Enabled := True;
end;

procedure TFmFPEventGer.CkAtivoClick(Sender: TObject);
begin
  if CkAtivo.Focused then
    BtSalva.Enabled := True;
end;

procedure TFmFPEventGer.BtSalvaClick(Sender: TObject);
var
  i, Codigo, Auto, Ativo, Base, Prioridade: Integer;
  Percentual: Double;
  Natureza: String;
begin
  Screen.Cursor := crHourGlass;
  Codigo := Geral.IMV(EdCodigo.Text);
  Ativo := MLAGeral.BoolToInt(CkAtivo.Checked);
  if CBBaseCod.KeyValue = NULL then Base := 0 else Base := CBBaseCod.KeyValue;
  Percentual := dmkEdPercentual.ValueVariant;
  Prioridade := dmkEdPrioridade.ValueVariant;
  Natureza := MLAGeral.EscolhaDe2Str(RGNatureza.ItemIndex = 1, 'D', 'P');
  //
  if Codigo = 0 then
  begin
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'FPEvent', 'FPEvent', 'Codigo');
    Auto  := 0;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO fpevent SET ');
    Dmod.QrUpd.SQL.Add('Descricao=:P0, Ativo=:P1, Natureza=:P2, Base=:P3, ');
    Dmod.QrUpd.SQL.Add('Auto=:P4, Percentual=:P5, Prioridade=:P6, ');
    Dmod.QrUpd.SQL.Add('Codigo=:Pa');
    //
    Dmod.QrUpd.Params[00].AsString  := EdDescricao.Text;
    Dmod.QrUpd.Params[01].AsInteger := Ativo;
    Dmod.QrUpd.Params[02].AsString  := Natureza;
    Dmod.QrUpd.Params[03].AsInteger := Base;
    Dmod.QrUpd.Params[04].AsInteger := Auto;
    Dmod.QrUpd.Params[05].AsFloat   := Percentual;
    Dmod.QrUpd.Params[06].AsFloat   := Prioridade;
    //
    Dmod.QrUpd.Params[07].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
  end else begin
    Auto := QrFPEvnAuto.Value;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE fpevent SET ');
    if not RGNatureza.Enabled then
    begin
      Dmod.QrUpd.SQL.Add('Descricao=:P0 WHERE Codigo=:P1');
      //
      Dmod.QrUpd.Params[00].AsString  := EdDescricao.Text;
      Dmod.QrUpd.Params[01].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
    end else begin
      Dmod.QrUpd.SQL.Add('Descricao=:P0, Ativo=:P1, Natureza=:P2, Base=:P3, ');
      Dmod.QrUpd.SQL.Add('Auto=:P4, Percentual=:P5, Prioridade=:P6 ');
      Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
      //
      Dmod.QrUpd.Params[00].AsString  := EdDescricao.Text;
      Dmod.QrUpd.Params[01].AsInteger := Ativo;
      Dmod.QrUpd.Params[02].AsString  := Natureza;
      Dmod.QrUpd.Params[03].AsInteger := Base;
      Dmod.QrUpd.Params[04].AsInteger := Auto;
      Dmod.QrUpd.Params[05].AsFloat   := Percentual;
      Dmod.QrUpd.Params[06].AsFloat   := Prioridade;
      //
      Dmod.QrUpd.Params[07].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
    end;
  end;

  // EventVar

  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('DELETE FROM fpeventvar WHERE Event=:P0 AND Varia=:P1 ');
  //
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO fpeventvar SET Event=:P0, Varia=:P1 ');
  //
  Dmod.QrUpdZ.SQL.Clear;
  Dmod.QrUpdZ.SQL.Add('UPDATE fpeventvar SET Ativo=1 WHERE Event=:P0 AND Varia=:P1 ');
  //
  for i := 0 to LBVaria.Items.Count - 1 do
  begin
    if LBVaria.Checked[i] = False then
    begin
      Dmod.QrUpdU.Params[00].AsInteger := QrFPEvnCodigo.Value;
      Dmod.QrUpdU.Params[01].AsString  := LBCodVaria.Items[i];
      Dmod.QrUpdU.ExecSQL;
    end else begin
      QrLVar.Close;
      QrLVar.Params[00].AsInteger := QrFPEvnCodigo.Value;
      QrLVar.Params[01].AsString  := LBCodVaria.Items[i];
      QrLVar.Open;
      if QrLVar.RecordCount > 0 then
      begin
        Dmod.QrUpdZ.Params[00].AsInteger := QrFPEvnCodigo.Value;
        Dmod.QrUpdZ.Params[01].AsString  := LBCodVaria.Items[i];
        Dmod.QrUpdZ.ExecSQL;
      end else begin
        Dmod.QrUpdM.Params[00].AsInteger := QrFPEvnCodigo.Value;
        Dmod.QrUpdM.Params[01].AsString  := LBCodVaria.Items[i];
        Dmod.QrUpdM.ExecSQL;
      end;
    end;
  end;

  // EventAcu

  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('DELETE FROM fpeventacu WHERE Event=:P0 AND Acumu=:P1 ');
  //
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO fpeventacu SET Event=:P0, Acumu=:P1 ');
  //
  Dmod.QrUpdZ.SQL.Clear;
  Dmod.QrUpdZ.SQL.Add('UPDATE fpeventacu SET Ativo=1 WHERE Event=:P0 AND Acumu=:P1 ');
  //
  for i := 0 to LBAcumu.Items.Count - 1 do
  begin
    if LBAcumu.Checked[i] = False then
    begin
      Dmod.QrUpdU.Params[00].AsInteger := QrFPEvnCodigo.Value;
      Dmod.QrUpdU.Params[01].AsString  := LBCodAcumu.Items[i];
      Dmod.QrUpdU.ExecSQL;
    end else begin
      QrLAcu.Close;
      QrLAcu.Params[00].AsInteger := QrFPEvnCodigo.Value;
      QrLAcu.Params[01].AsString  := LBCodAcumu.Items[i];
      QrLAcu.Open;
      if QrLAcu.RecordCount > 0 then
      begin
        Dmod.QrUpdZ.Params[00].AsInteger := QrFPEvnCodigo.Value;
        Dmod.QrUpdZ.Params[01].AsString  := LBCodAcumu.Items[i];
        Dmod.QrUpdZ.ExecSQL;
      end else begin
        Dmod.QrUpdM.Params[00].AsInteger := QrFPEvnCodigo.Value;
        Dmod.QrUpdM.Params[01].AsString  := LBCodAcumu.Items[i];
        Dmod.QrUpdM.ExecSQL;
      end;
    end;
  end;

  //
  ReopenEvn(Codigo);
  Screen.Cursor := crDefault;
end;

procedure TFmFPEventGer.BtIncluiClick(Sender: TObject);
var
  i: Integer;
begin
  // Inclui novo FPEvent
  EdCodigo.Text := '';
  EdDescricao.Text := '';
  CBBaseCod.KeyValue := NULL;
  dmkEdPercentual.Text := '';
  dmkEdPrioridade.Text := '50';
  for i := 0 to LBVaria.Items.Count - 1 do
    LBVaria.Checked[i] := False;
  for i := 0 to LBAcumu.Items.Count - 1 do
    LBAcumu.Checked[i] := False;
end;

procedure TFmFPEventGer.RGNaturezaEnter(Sender: TObject);
begin
  FCompo := RGNatureza.Name;
end;

procedure TFmFPEventGer.RGNaturezaExit(Sender: TObject);
begin
  FCompo := '';
end;

procedure TFmFPEventGer.EdPesqDescriChange(Sender: TObject);
begin
  ReopenEvn(QrFPEvnCodigo.Value);
end;

procedure TFmFPEventGer.RGOrdemClick(Sender: TObject);
begin
  ReopenEvn(QrFPEvnCodigo.Value);
end;

end.

