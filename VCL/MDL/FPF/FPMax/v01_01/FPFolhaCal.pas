unit FPFolhaCal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     DBCtrls,
     Db, mySQLDbTables, Grids, DBGrids,
  dmkDBGrid, dmkEdit, ComCtrls, Menus;

type
  TFmFPFolhaCal = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Painel3: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    Painel1: TPanel;
    Panel1: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdEmpresa: TLMDEdit;
    CBEmpresa: TDBLookupComboBox;
    EdDepto: TLMDEdit;
    CBDepto: TDBLookupComboBox;
    EdTipoCalc: TLMDEdit;
    CBTipoCalc: TDBLookupComboBox;
    TPDataC: TDateTimePicker;
    dmkEdPeriodo: TdmkEdit;
    Edit1: TEdit;
    Panel3: TPanel;
    BitBtn1: TBitBtn;
    BtOK: TBitBtn;
    Label16: TLabel;
    BtPrior: TBitBtn;
    BtNext: TBitBtn;
    Painel4: TPanel;
    PB2_1: TProgressBar;
    EdTpCalPer: TLMDEdit;
    CBTpCalPer: TDBLookupComboBox;
    PnAdiantamento: TPanel;
    RGAdianta: TRadioGroup;
    CkArredonda: TCheckBox;
    CkIRRF: TCheckBox;
    Label6: TLabel;
    dmkEdAdianta: TdmkEdit;
    dmkEdArredonda: TdmkEdit;
    Panel4: TPanel;
    dmkDBGrid2: TdmkDBGrid;
    Panel5: TPanel;
    Panel6: TPanel;
    Splitter1: TSplitter;
    Panel7: TPanel;
    Splitter2: TSplitter;
    Panel8: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    Painel2: TPanel;
    dmkDBGrid3: TdmkDBGrid;
    Panel9: TPanel;
    Panel10: TPanel;
    Label7: TLabel;
    Panel11: TPanel;
    BtSalva: TBitBtn;
    PMSalva: TPopupMenu;
    ClculoAtual1: TMenuItem;
    ClculosModificados1: TMenuItem;
    Todosclculos1: TMenuItem;
    BtImprime: TBitBtn;
    PMImprime: TPopupMenu;
    Panel12: TPanel;
    ItemAtual1: TMenuItem;
    ItensSelecionados1: TMenuItem;
    Todositens1: TMenuItem;
    Label63: TLabel;
    EdEvento: TLMDEdit;
    CBEvento: TDBLookupComboBox;
    dmkEdEve_ValPer: TdmkEdit;
    CkEve_Incid: TCheckBox;
    BtIncluir: TBitBtn;
    SpeedButton1: TSpeedButton;
    BitBtn2: TBitBtn;
    QrFPParam: TmySQLQuery;
    QrFPParamParametro: TIntegerField;
    QrFPParamValor: TFloatField;
    BitBtn3: TBitBtn;
    MeMsg: TMemo;
    Label8: TLabel;
    EdUnidade: TEdit;
    Button1: TButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdEmpresaExit(Sender: TObject);
    procedure CBEmpresaClick(Sender: TObject);
    procedure CBEmpresaDropDown(Sender: TObject);
    procedure EdDeptoChange(Sender: TObject);
    procedure EdDeptoExit(Sender: TObject);
    procedure CBDeptoClick(Sender: TObject);
    procedure CBDeptoDropDown(Sender: TObject);
    procedure dmkEdPeriodoChange(Sender: TObject);
    procedure dmkEdPeriodoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtNextClick(Sender: TObject);
    procedure BtPriorClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure dmkDBGrid1CellClick(Column: TColumn);
    procedure CkArredondaClick(Sender: TObject);
    procedure BtSalvaClick(Sender: TObject);
    procedure ClculoAtual1Click(Sender: TObject);
    procedure Todosclculos1Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure ItemAtual1Click(Sender: TObject);
    procedure ItensSelecionados1Click(Sender: TObject);
    procedure Todositens1Click(Sender: TObject);
    procedure BtIncluirClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure dmkEdEve_ValPerChange(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    FTemFerias, FCodCalFer, FDdFerAbo, FDdFerias, FDdPecun, FDdFerMes,
    FTipoCalc, FEmpreCod, FSemana, FPeriodo, FPanelIndex: Integer;
    FParam_DeduDepe, FParam_ReteMinm, FParam_PorcFGTS, FHorasTrab, FSalTrab,
    FParam_VMinIRRF, FParam_NaoSimFG, FParam_DeduAdIR, FHorasMax: Double;
    FDataIni, FDataFim, FNatureza: String;
    procedure AtualizaTodos(Status: Integer);
    procedure LimpaTabelasLocais(Calc, Its: Boolean);
    procedure MudaPeriodo(Key: Word);
    function  ReinsertFPC(): Integer;
    procedure ReopenDepto();
    procedure ReopenFPC(Entidade: Integer);
    function  ReopenFunci(): Integer;
    function  MostraPanel(Index: Integer; Mostra: Boolean): Boolean;
    function  Passo1a: Integer;
    function  Passo2a: Integer;
    function  Passo3a: Integer;
    function  Passo3b: Integer;
    function  CalculaFuncionarioAtual: Boolean;
    function  CalculaTodosFuncionarios: Boolean;
    function  CalculaFuncionarioTipoCalcT(): Boolean;
    procedure DefineItensDiversos(SalBase: Double);
    //function  CalculaFuncionarioTipoCalc1(): Boolean;
    //function  CalculaFuncionarioTipoCalc3(): Boolean;
    //
    function  DefineInfo(TipoCalc, Evento, Default: Integer): Integer;
    function  DefineSalva(TipoCalc, Evento, Default: Integer): Integer;
    function  DefineMostra(TipoCalc, Evento, Default: Integer): Integer;
    //function  DefineAcumula(TipoCalc, Evento, Default: Integer): Integer;
    //
    function  GeraDados(Referencia, Valor: Double; Alteracao: Integer; Unidade:
              String): Integer;
    function  GeraDadosPes(Evento: Integer; Referencia, Valor: Double;
              Alteracao: Integer; Unidade: String): Integer;
    function  EventosAutomaticos: Boolean;
    function  EventosPreDefinidos: Boolean;
    procedure SalvaCalculoAtual();
    procedure ImprimeItens(Tipo: TSelType);
    //function  VerificaEventosDoUsuario(Entidade: Integer; SalBase:
    //          Double): Boolean;
    procedure RedefineParametros;
    procedure VerificaBtIncluir;
    procedure DefineHorasMax;
    procedure ReopenIniFer;
  public
    { Public declarations }
  end;

  var
  FmFPFolhaCal: TFmFPFolhaCal;

implementation

uses UnMyObjects, Module, UCreate, ModuleSal, UMySQLModule, FPFolhaImp, FPEventGer,
  UnInternalConsts;

const
  FMaxPanel = 4;

{$R *.DFM}

function TFmFPFolhaCal.DefineInfo(TipoCalc, Evento, Default: Integer): Integer;
begin
  Result := Default;
  {case TipoCalc of
    1: Result := Default;//if Evento in ([91, 93..99]) then Result := 1;
    //3: if Evento in ([91..99]) then Result := 1;
    else Application.MessageBox(PChar('N�o foi definido para o tipo de c�lculo ' +
    IntToStr(TipoCalc) + 'se ele � informativo ou n�o!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;}
end;

function TFmFPFolhaCal.DefineSalva(TipoCalc, Evento, Default: Integer): Integer;
begin
  Result := Default;
  {case TipoCalc of
    1: Result := Default; //1: if Evento in ([2,40,41,59,81,88,89,91,93,94,99]) then Result := 1;
    else Application.MessageBox(PChar('N�o foi definido para o tipo de c�lculo ' +
    IntToStr(TipoCalc) + 'se ele � salvo ou n�o!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;}
end;

function TFmFPFolhaCal.DefineMostra(TipoCalc, Evento, Default: Integer): Integer;
begin
  Result := Default;
  {ase TipoCalc of
    1: Result := Default; //1: if Evento in ([2,59]) then Result := 1;
    else Application.MessageBox(PChar('N�o foi definido para o tipo de c�lculo ' +
    IntToStr(TipoCalc) + 'se ele � mostrado ou n�o!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;}
end;

{function TFmFPFolhaCal.DefineAcumula(TipoCalc, Evento, Default: Integer): Integer;
begin
  Result := 0;
  case TipoCalc of
    1: Result := Default; //1: if Evento in ([2,40,41,59,81,88,89,93,94,99]) then Result := 1;
    else Application.MessageBox(PChar('N�o foi definido para o tipo de c�lculo ' +
    IntToStr(TipoCalc) + 'se ele � acumulativo ou n�o!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;}

procedure TFmFPFolhaCal.LimpaTabelasLocais(Calc, Its: Boolean);
begin
  if Calc then
  begin
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM fpcalc');
    Dmod.QrUpdL.ExecSQL;
  end;
  //
  if Its then
  begin
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM fpcalceven');
    Dmod.QrUpdL.ExecSQL;
    //
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM fpcalcacum');
    Dmod.QrUpdL.ExecSQL;
    //
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM fpcalcferi');
    Dmod.QrUpdL.ExecSQL;
  end;
end;

procedure TFmFPFolhaCal.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPFolhaCal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if DmSal.QrEmpresas.State = dsInactive then
  begin
    DmSal.QrEmpresas.Close;
    DmSal.QrEmpresas.SQL.Clear;
    DmSal.QrEmpresas.SQL.Add('SELECT en.Codigo,');
    DmSal.QrEmpresas.SQL.Add('IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEEMPRESA');
    DmSal.QrEmpresas.SQL.Add('FROM entidades en');
    DmSal.QrEmpresas.SQL.Add('WHERE ' + VAR_FP_EMPRESA);
    DmSal.QrEmpresas.SQL.Add('ORDER BY NOMEEMPRESA');
    DmSal.QrEmpresas.Open;
  end;  
end;

procedure TFmFPFolhaCal.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFPFolhaCal.FormCreate(Sender: TObject);
begin
  DmSal.QrFPTpCal.Close;
  DmSal.QrFPTpCal.Open;

  // Lookup do DmSal.QrCalcM
  DmSal.QrFPEvent.Close;
  DmSal.QrFPEvent.Open;

  DmSal.QrUserEve.Close;
  DmSal.QrUserEve.Open;

  TPDataC.Date := Date;
  //
  Panel5.Align := alClient;
  //
  Painel1.Align := alClient;
  Painel2.Align := alClient;
  Painel3.Align := alClient;
  Painel4.Align := alClient;
  //
  FPanelIndex := 1;
  dmkDBGrid1.Align := alClient;
  dmkDBGrid3.Align := alClient;
  Panel1.Align     := alClient;
  //
  dmkEdAdianta.ValueVariant := 40;
  dmkEdArredonda.ValueVariant := 1;
  //
  DmSal.QrInsC.SQL.Clear;
  DmSal.QrInsC.SQL.Add('INSERT INTO fpfolhacal SET ');
  DmSal.QrInsC.SQL.Add('Codigo      =:P00,');
  DmSal.QrInsC.SQL.Add('Periodo     =:P01,');
  DmSal.QrInsC.SQL.Add('Empresa     =:P02,');
  DmSal.QrInsC.SQL.Add('Entidade    =:P03,');
  DmSal.QrInsC.SQL.Add('Semana      =:P04,');
  DmSal.QrInsC.SQL.Add('TipoCalc    =:P05,');
  DmSal.QrInsC.SQL.Add('DataC       =:P06,');
  DmSal.QrInsC.SQL.Add('ValorP      =:P07,');
  DmSal.QrInsC.SQL.Add('ValorD      =:P08,');
  DmSal.QrInsC.SQL.Add('ValorL      =:P09,');
  DmSal.QrInsC.SQL.Add('MediaHE     =:P10,');
  DmSal.QrInsC.SQL.Add('MediaCo     =:P11,');
  DmSal.QrInsC.SQL.Add('MediaAd     =:P12,');
  DmSal.QrInsC.SQL.Add('Ativo       =:P13');
  //
  DmSal.QrInsE.SQL.Clear;
  DmSal.QrInsE.SQL.Add('INSERT INTO fpfolhaeve SET ');
  DmSal.QrInsE.SQL.Add('Codigo      =:P00,');
  DmSal.QrInsE.SQL.Add('Evento      =:P01,');
  DmSal.QrInsE.SQL.Add('Referencia  =:P02,');
  DmSal.QrInsE.SQL.Add('Valor       =:P03,');
  DmSal.QrInsE.SQL.Add('Info        =:P04,');
  DmSal.QrInsE.SQL.Add('Mostra      =:P05,');
  DmSal.QrInsE.SQL.Add('Ativo       =:P06');
  //
  DmSal.QrInsA.SQL.Clear;
  DmSal.QrInsA.SQL.Add('INSERT INTO fpfolhaacu SET ');
  DmSal.QrInsA.SQL.Add('Codigo      =:P00, ');
  DmSal.QrInsA.SQL.Add('Evento      =:P01, ');
  DmSal.QrInsA.SQL.Add('Acumulador  =:P02 ');
  //
  DmSal.QrInsF.SQL.Clear;
  DmSal.QrInsF.SQL.Add('INSERT INTO fpfolhafer SET ');
  DmSal.QrInsF.SQL.Add('Codigo      =:P00, ');
  DmSal.QrInsF.SQL.Add('Item        =:P01, ');
  DmSal.QrInsF.SQL.Add('Tipo        =:P02, ');
  DmSal.QrInsF.SQL.Add('Auto        =:P03, ');
  DmSal.QrInsF.SQL.Add('Referencia  =:P04, ');
  DmSal.QrInsF.SQL.Add('Valor       =:P05 ');
  //
  DmSal.QrUpdC.SQL.Clear;
  DmSal.QrUpdC.SQL.Add('UPDATE fpcalc SET ');
  DmSal.QrUpdC.SQL.Add('Salvou = 1 ');
  DmSal.QrUpdC.SQL.Add('WHERE Entidade =:P0');
  //
  DmSal.QrUpdL0.SQL.Clear;
  DmSal.QrUpdL0.SQL.Add('INSERT INTO fpcalcbase SET Fonte=:P0, ');
  DmSal.QrUpdL0.SQL.Add('Entidade=:P1, Evento=:P2, Natureza=:P3, ');
  DmSal.QrUpdL0.SQL.Add('Prioridade=:P4, Informacao=:P5, Salvar=:P6, ');
  DmSal.QrUpdL0.SQL.Add('Visivel=:P7, Eve_ValPer=:P8, Eve_Meses=:P9, ');
  DmSal.QrUpdL0.SQL.Add('Eve_Incid=:P10, Conta=:P11, Cta_IncPer=:P12, ');
  DmSal.QrUpdL0.SQL.Add('Cta_MesesR=:P13, Incidencia=:P14, Auto=:P15, ');
  DmSal.QrUpdL0.SQL.Add('Eve_BasCod=:P16, Eve_BasPer=:P17, Referencia=:P18, ');
  DmSal.QrUpdL0.SQL.Add('ProporciHT=:P19, Unidade=:P20, Eve_Ciclo=:P21, ');
  DmSal.QrUpdL0.SQL.Add('Eve_DtCicl=:P22 ');
  //
  DmSal.QrUpdL1.SQL.Clear;
  DmSal.QrUpdL1.SQL.Add('INSERT INTO fpcalceven SET ');
  DmSal.QrUpdL1.SQL.Add('Entidade=:P0, Evento=:P1, Referencia=:P2, ');
  DmSal.QrUpdL1.SQL.Add('Valor=:P3, Alteracao=:P4, Info=:P5, Salva=:P6, ');
  DmSal.QrUpdL1.SQL.Add('Mostra=:P7, Prioridade=:P8, Auto=:P9, ');
  DmSal.QrUpdL1.SQL.Add('Unidade=:P10, Ativo=1');
  //
  DmSal.QrUpdL2.SQL.Clear;
  DmSal.QrUpdL2.SQL.Add('INSERT INTO fpcalcacum SET ');
  DmSal.QrUpdL2.SQL.Add('Entidade=:P0, Evento=:P1, Acumulador=:P2 ');
  DmSal.QrUpdL2.SQL.Add('');
  //

end;

procedure TFmFPFolhaCal.EdEmpresaChange(Sender: TObject);
var
  Ano, Mes, Dia: Word;
  TipoCalc: Integer;
  DataCalc: TDateTime;
begin
  MLAGeral.SincroI(FmFPFolhaCal, Sender, 0, siPositivo);
  //
  if TLMDEdit(Sender).Name = 'EdEVento' then
  begin
    VerificaBtIncluir;
  end;
  if TLMDEdit(Sender).Name = 'EdEmpresa' then
  begin
    ReopenDepto;
  end;
  if TLMDEdit(Sender).Name = 'EdTipoCalc' then
  begin
    TipoCalc := Geral.IMV(EdTipoCalc.Text);
    case TipoCalc of
      1: // Adiantamento salarial
      begin
        dmkEdPeriodo.ValueVariant := Geral.Periodo2000(Date);
        DecodeDate(Date, Ano, Mes, Dia);
        Dia := UMyMod.DiaUtilMes(EncodeDate(Ano, Mes, 1), 1, 18, 25);
        //DataCalc := EncodeDate(Ano, Mes, Dia);
        //if DataCalc > Date then
          DataCalc := Int(Date);
        TPDataC.Date := DataCalc;
      end;
      3: // Mensal
      begin
        dmkEdPeriodo.ValueVariant := Geral.Periodo2000(Date);
        DecodeDate(Date, Ano, Mes, Dia);
        Dia := UMyMod.DiaUtilMes(EncodeDate(Ano, Mes, 1), 1, 5, 10);
        //DataCalc := EncodeDate(Ano, Mes, Dia);
        //if DataCalc > Date then
          DataCalc := Int(Date);
        TPDataC.Date := DataCalc;
      end;
      else
      begin
        dmkEdPeriodo.ValueVariant := Geral.Periodo2000(Date);
        TPDataC.Date := Int(Date);
      end;
    end;
    //
    DmSal.ReopenTpCalPer(TipoCalc);
    if DmSal.QrTpCalPer.RecordCount > 0 then
    begin
      EdTpCalPer.Visible  := True;
      CBTpCalPer.Visible  := True;
      if DmSal.QrTpCalPer.Locate('Controle', 4, []) then
      begin
        EdTpCalPer.Text     := '4';
        CBTpCalPer.KeyValue := 4;
      end;
    end else begin
      EdTpCalPer.Visible  := False;
      CBTpCalPer.Visible  := False;
      EdTpCalPer.Text     := '';
      CBTpCalPer.KeyValue := NULL;
    end;
    //
    PnAdiantamento.Visible := Geral.IMV(EdTipoCalc.Text) = 1;
  end;
end;

procedure TFmFPFolhaCal.EdEmpresaExit(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPFolhaCal, Sender, 1, siPositivo);
end;

procedure TFmFPFolhaCal.CBEmpresaClick(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPFolhaCal, Sender, 2, siPositivo);
end;

procedure TFmFPFolhaCal.CBEmpresaDropDown(Sender: TObject);
begin
  MLAGeral.SincroI(FmFPFolhaCal, Sender, 3, siPositivo);
end;

procedure TFmFPFolhaCal.EdDeptoChange(Sender: TObject);
begin
  MLAGeral.SincroS(FmFPFolhaCal, Sender, 0);
end;

procedure TFmFPFolhaCal.EdDeptoExit(Sender: TObject);
begin
  MLAGeral.SincroS(FmFPFolhaCal, Sender, 1);
end;

procedure TFmFPFolhaCal.CBDeptoClick(Sender: TObject);
begin
  MLAGeral.SincroS(FmFPFolhaCal, Sender, 2);
end;

procedure TFmFPFolhaCal.CBDeptoDropDown(Sender: TObject);
begin
  MLAGeral.SincroS(FmFPFolhaCal, Sender, 3);
end;

procedure TFmFPFolhaCal.CkArredondaClick(Sender: TObject);
begin
  dmkEdArredonda.Enabled := CkArredonda.Checked;
end;

procedure TFmFPFolhaCal.ReopenDepto();
var
  Empresa: Integer;
begin
  Empresa := Geral.IMV(EdEmpresa.Text);
  //
  EdDepto.Text := '';
  CBDepto.KeyValue := NULL;
  DmSal.QrDepto.Close;
  if Empresa > 0 then
  begin
    DmSal.QrDepto.Close;
    DmSal.QrDepto.Params[0].AsInteger := Empresa;
    DmSal.QrDepto.Open;
  end;
end;

function TFmFPFolhaCal.ReopenFunci(): Integer;
var
  Empresa, TipoCalc, Categoria: Integer;
  Depto: String;
begin
  Empresa   := Geral.IMV(EdEmpresa.Text);
  TipoCalc  := Geral.IMV(EdTipoCalc.Text);
  Categoria := DmSal.QrTpCalPerCategoria.Value;
  //
  DmSal.QrFunci.Close;
  if Empresa > 0 then
  begin
    Depto := Trim(EdDepto.Text);
    //
    DmSal.QrFunci.Close;
    DmSal.QrFunci.SQL.Clear;
    DmSal.QrFunci.SQL.Add('SELECT IF(ent.Tipo=0, RazaoSocial, Nome) NOMEFUNCI,');
    DmSal.QrFunci.SQL.Add('ent.Codigo, fun.Funci, fun.Registro');
    // F�rias
    if TipoCalc = 5 then DmSal.QrFunci.SQL.Add(', fer.DtIniPA, fer.DtFimPA');
    //
    DmSal.QrFunci.SQL.Add('FROM fpfunci fun');
    DmSal.QrFunci.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=fun.Codigo');
    // F�rias
    if TipoCalc = 5 then DmSal.QrFunci.SQL.Add(
      'LEFT JOIN fpfuncifer fer ON fer.Codigo=fun.Codigo AND fer.Empresa=fun.Empresa');
    //
    DmSal.QrFunci.SQL.Add('');
    DmSal.QrFunci.SQL.Add('WHERE fun.Ativo=1');
    DmSal.QrFunci.SQL.Add('AND fun.Situacao=1');
    DmSal.QrFunci.SQL.Add('AND ((fun.DataDemiss=0) OR (SYSDATE() < fun.DataDemiss))');
    DmSal.QrFunci.SQL.Add('AND fun.Empresa=' + IntToStr(Empresa));
    case TipoCalc of
      // Adiantamento salarial
      1: DmSal.QrFunci.SQL.Add('AND fun.Categoria in (1,4)'); // Mensalista e Diarista
      // Mensal
      // Mensalista, Quinzenalista 1, 2 e Diarista
      //colocar categ no FPtpCalPer
      3: DmSal.QrFunci.SQL.Add('AND fun.Categoria = ' + IntToStr(Categoria));
      // F�rias
      5:
      begin
        //Fazer aqui s� quem tem f�rias
        DmSal.QrFunci.SQL.Add('AND fer.DtIniPG BETWEEN "' + FDataIni +
        '" AND "' + FDataFim + '"');
      end;
      //  Pro-labore
      8: DmSal.QrFunci.SQL.Add('AND fun.Categoria = 7'); // Categoria 7 => outros
      else begin
        DmSal.QrFunci.SQL.Add('AND fun.Categoria = -100000'); // n�o achar nada
        Application.MessageBox(PChar('Tipo de c�lculo n�o implementado!' +
        Chr(13) + Chr(10) + 'Solicite a implementa��o � Dermatek!'), 'Aviso',
        MB_OK+MB_ICONWARNING);
        Result := -1;
        Exit;
      end;
    end;
    if Depto <> '' then
      DmSal.QrFunci.SQL.Add('AND fun.Depto="' + Depto + '"');

    DmSal.QrFunci.SQL.Add('ORDER BY NOMEFUNCI');
    DmSal.QrFunci.SQL.Add('');
    //
    DmSal.QrFunci.Open;
    Result := DmSal.QrFunci.RecordCount;
  end else Result := 0;
end;

function TFmFPFolhaCal.ReinsertFPC(): Integer;
var
  Calcula: Integer;
begin
  LimpaTabelasLocais(True, True);
  //
  ReopenFunci;
  //
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO fpcalc SET ');
  Dmod.QrUpdL.SQL.Add('FPFolhaCal=0, Entidade=:P0, NomeEnti=:P1, ');
  Dmod.QrUpdL.SQL.Add('CodFunci=:P2, Calcula=:P3, Salvou=0');
  //
  DmSal.QrFunci.First;
  while not DmSal.QrFunci.Eof do
  begin
    Calcula := MLAGeral.BoolToInt(DmSal.QrFunciFunci.Value <> 0);
    Dmod.QrUpdL.Params[00].AsInteger := DmSal.QrFunciCodigo.Value;
    Dmod.QrUpdL.Params[01].AsString  := DmSal.QrFunciNOMEFUNCI.Value;
    Dmod.QrUpdL.Params[02].AsInteger := DmSal.QrFunciFunci.Value;
    Dmod.QrUpdL.Params[03].AsInteger := Calcula;
    Dmod.QrUpdL.ExecSQL;
    DmSal.QrFunci.Next;
  end;
  ReopenFPC(0);
  Result := DmSal.QrFPC.RecordCount;
end;

procedure TFmFPFolhaCal.dmkEdPeriodoChange(Sender: TObject);
begin
  Edit1.Text := MLAGeral.MesEAnoDoPeriodoLongo(dmkEdPeriodo.ValueVariant);
end;

procedure TFmFPFolhaCal.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MudaPeriodo(Key);
end;

procedure TFmFPFolhaCal.dmkEdPeriodoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MudaPeriodo(Key);
end;

procedure TFmFPFolhaCal.MudaPeriodo(Key: Word);
begin
  case key of
    VK_DOWN: dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant - 1;
    VK_UP: dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant + 1;
    VK_PRIOR: dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant - 12;
    VK_NEXT: dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant + 12;
  end;
end;

function TFmFPFolhaCal.MostraPanel(Index: Integer; Mostra: Boolean): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to ComponentCount - 1 do
  if Components[i] is TPanel then
    if TPanel(Components[i]).Name = 'Painel'+IntToStr(Index) then
    begin
      TPanel(Components[i]).Visible := Mostra;
      Result := True;
      Exit;
    end;
end;

procedure TFmFPFolhaCal.BtNextClick(Sender: TObject);
begin
  case FPanelIndex of
    1: if Passo1a < 1 then Exit;
    2:    Passo2a;
    3: if Passo3a < 1 then Exit;
    //3: if Passo2b < 1 then Exit;
    4: MeMsg.Lines.Clear;
  end;
  Inc(FPanelIndex, 1);
  BtPrior.Enabled := FPanelIndex > 1;
  BtNext.Enabled := FPanelIndex < FMaxPanel;
  if MostraPanel(FPanelIndex, True) then
    MostraPanel(FPanelIndex-1, False);
end;

procedure TFmFPFolhaCal.BtPriorClick(Sender: TObject);
begin
  Inc(FPanelIndex, -1);
  BtPrior.Enabled := FPanelIndex > 1;
  BtNext.Enabled := FPanelIndex < FMaxPanel;
  if MostraPanel(FPanelIndex, True) then
    MostraPanel(FPanelIndex+1, False);
  //
  case FPanelIndex of
    3: LimpaTabelasLocais(False, True);
  end;
  //
end;

procedure TFmFPFolhaCal.BtOKClick(Sender: TObject);
begin
  AtualizaTodos(1);
end;

procedure TFmFPFolhaCal.BitBtn1Click(Sender: TObject);
begin
  AtualizaTodos(0);
end;

procedure TFmFPFolhaCal.AtualizaTodos(Status: Integer);
begin
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE fpcalc SET Calcula=:P0');
  Dmod.QrUpdL.Params[00].AsInteger := Status;
  Dmod.QrUpdL.ExecSQL;
  //
  ReopenFPC(DmSal.QrFPCEntidade.Value);
end;

procedure TFmFPFolhaCal.ReopenFPC(Entidade: Integer);
begin
  DmSal.QrFPC.Close;
  DmSal.QrFPC.Open;
  //
  if Entidade > 0 then
    DmSal.QrFPC.Locate('Entidade', Entidade, []);
end;

procedure TFmFPFolhaCal.dmkDBGrid1CellClick(Column: TColumn);
var
  Calcula: Integer;
begin
  if Column.FieldName = 'Calcula' then
  begin
    if DmSal.QrFPCCalcula.Value = 1 then Calcula := 0 else Calcula := 1;
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('UPDATE fpcalc SET Calcula=:P0');
    Dmod.QrUpdL.SQL.Add('WHERE Entidade=:P1');
    //
    Dmod.QrUpdL.Params[00].AsInteger := Calcula;
    Dmod.QrUpdL.Params[01].AsInteger := DmSal.QrFPCEntidade.Value;
    Dmod.QrUpdL.ExecSQL;
    //
    ReopenFPC(DmSal.QrFPCEntidade.Value);
  end;
end;

function TFmFPFolhaCal.Passo1a: Integer;
var
  PerAtu: Integer;
  Continua: Boolean;
begin
  FTipoCalc := -1000;
  Result := 0;
  //
  FEmpreCod := Geral.IMV(EdEmpresa.Text);
  if FEmpreCod = 0 then
  begin
    FEmpreCod := -1000;
    Application.MessageBox('Informe a empresa!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdEmpresa.SetFocus;
    Exit;
  end;
  //
  FTipoCalc := Geral.IMV(EdTipoCalc.Text);
  if FTipoCalc = 0 then
  begin
    FTipoCalc := -1000;
    Application.MessageBox('Informe o tipo de c�lculo!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdTipoCalc.SetFocus;
    Exit;
  end;
  //
  FSemana := Geral.IMV(EdTpCalPer.Text);
  if (FSemana = 0) and (DmSal.QrTpCalPer.RecordCount > 0)  then
  begin
    Application.MessageBox('Informe o sub-per�odo da compet�ncia!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdTpCalPer.SetFocus;
    Exit;
  end;
  //
  FPeriodo := dmkEdPeriodo.ValueVariant;
  FDataIni := MLAGeral.PrimeiroDiaDoPeriodo(FPeriodo, dtSystem);
  FDataFim := MLAGeral.UltimoDiaDoPeriodo(FPeriodo, dtSystem);
  //
  PerAtu := Geral.Periodo2000(Date);
  if (FPeriodo > PerAtu) then
  begin
    Continua := Application.MessageBox(PChar('O per�odo informado: "' +
      Edit1.Text + '" ainda n�o passou, deseja continuar assim mesmo?'),
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES;
    if not Continua then
      Exit;
  end;
  //
  if (FPeriodo < (PerAtu-2)) then
  begin
    Continua := Application.MessageBox(PChar('O per�odo informado: "' +
      Edit1.Text + '" j� passou a mais de 2 meses, deseja continuar assim mesmo?'),
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES;
    if not Continua then
      Exit;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    //if
    DmSal.ReopenCdo(dmkEdPeriodo.ValueVariant, FEmpreCod, FTipoCalc, FSemana);
    Result := ReinsertFPC();
  finally
    Screen.Cursor := crDefault;
  end;
  //Result := ReopenFunci;
  if Result = 0 then Application.MessageBox(
    'N�o h� funcion�rios ativos para a configura��o selecionada!', 'Aviso',
    MB_OK+MB_ICONWARNING);
end;

function TFmFPFolhaCal.Passo2a: Integer;
begin
  Result := DmSal.QrCdo.RecordCount;
  //
end;

function TFmFPFolhaCal.Passo3a: Integer;
var
  Entidade: Integer;
begin
  //ReinsertFPC();
  if DmSal.QrCalc.State = dsBrowse then
    Entidade := DmSal.QrCalcEntidade.Value
  else Entidade := 0;
  //
  Result := DmSal.ReopenCalc(Entidade);
  //
  if Result < 1 then Application.MessageBox(
    'N�o h� funcion�rio selecionado para c�lculo!', 'Aviso', MB_OK+MB_ICONWARNING)
  else Result := Passo3b;
end;

function TFmFPFolhaCal.Passo3b: Integer;
var
  Entidade: Integer;
begin
  DmSal.ReopenEventip(Geral.IMV(EdTipoCalc.Text));
  //
  RedefineParametros;
  //
  Result := DmSal.QrEventip.RecordCount;
  //
  if Result < 1 then Application.MessageBox(
    'N�o h� eventos ativos para o tipo de c�lculo selecionado!',
    'Aviso', MB_OK+MB_ICONWARNING);

  if Result > 0 then
  begin
    if EventosAutomaticos then
      if EventosPreDefinidos then
        CalculaTodosFuncionarios;
  end;

  if DmSal.QrCalc.State = dsBrowse then
    Entidade := DmSal.QrCalcEntidade.Value
  else Entidade := 0;
  DmSal.ReopenCalc(Entidade);
end;

function TFmFPFolhaCal.EventosAutomaticos: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM fpcalcbase ');
    Dmod.QrUpdL.ExecSQL;
    //
    DmSal.QrCalc.First;
    while not DmSal.QrCalc.Eof do
    begin
      DmSal.QrEventip.First;
      while not DmSal.QrEventip.Eof do
      begin
        DmSal.QrUpdL0.Params[00].AsInteger := 1; // Automatico
        DmSal.QrUpdL0.Params[01].AsInteger := DmSal.QrCalcEntidade.Value;
        DmSal.QrUpdL0.Params[02].AsInteger := DmSal.QrEventipCodigo.Value;
        DmSal.QrUpdL0.Params[03].AsString  := DmSal.QrEventipNatureza.Value;
        DmSal.QrUpdL0.Params[04].AsInteger := DmSal.QrEventipPrioridade.Value;
        DmSal.QrUpdL0.Params[05].AsInteger := DmSal.QrEventipInformacao.Value;
        DmSal.QrUpdL0.Params[06].AsInteger := DmSal.QrEventipSalvar.Value;
        DmSal.QrUpdL0.Params[07].AsInteger := DmSal.QrEventipVisivel.Value;
        DmSal.QrUpdL0.Params[08].AsFloat   := 0;
        DmSal.QrUpdL0.Params[09].AsInteger := 0;
        DmSal.QrUpdL0.Params[10].AsInteger := 0;
        DmSal.QrUpdL0.Params[11].AsInteger := 0;
        DmSal.QrUpdL0.Params[12].AsFloat   := 0;
        DmSal.QrUpdL0.Params[13].AsInteger := 0;
        DmSal.QrUpdL0.Params[14].AsInteger := 0;
        DmSal.QrUpdL0.Params[15].AsInteger := DmSal.QrEventipAuto.Value;
        DmSal.QrUpdL0.Params[16].AsInteger := DmSal.QrEventipBase.Value;
        DmSal.QrUpdL0.Params[17].AsFloat   := DmSal.QrEventipPercentual.Value;
        DmSal.QrUpdL0.Params[18].AsFloat   := 0;
        DmSal.QrUpdL0.Params[19].AsInteger := 0; // proporcional as horas trabalhadas
        DmSal.QrUpdL0.Params[20].AsString  := ''; // autom�tico
        DmSal.QrUpdL0.Params[21].AsInteger := 0;
        DmSal.QrUpdL0.Params[22].AsString  := '';
        DmSal.QrUpdL0.ExecSQL;
        //
        DmSal.QrEventip.Next;
      end;
      //
      DmSal.QrCalc.Next;
    end;
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmFPFolhaCal.EventosPreDefinidos: Boolean;
var
  Prioridade: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    DmSal.QrCalc.First;
    while not DmSal.QrCalc.Eof do
    begin
      if DmSal.ReopenFunEve(DmSal.QrCalcEntidade.Value, FTipoCalc) > 0 then
      begin
        DmSal.QrFunEve.First;
        while not DmSal.QrFunEve.Eof do
        begin
          Prioridade := DmSal.QrFunEvePrioridade.Value;
          // Caso o tipo de c�lculo for f�rias e a natureza provento ent�o
          // fazer antes aumentar a prioridade para se tornar sal�rio base
          if (FTipoCalc = 5) and (DmSal.QrFunEveNatureza.Value = 'P') then
            Prioridade := Prioridade + 55;
          DmSal.QrUpdL0.Params[00].AsInteger := 2; // Cadastro do cliente
          DmSal.QrUpdL0.Params[01].AsInteger := DmSal.QrCalcEntidade.Value;
          DmSal.QrUpdL0.Params[02].AsInteger := DmSal.QrFunEveEvento.Value;
          DmSal.QrUpdL0.Params[03].AsString  := DmSal.QrFunEveNatureza.Value;
          DmSal.QrUpdL0.Params[04].AsInteger := Prioridade;
          DmSal.QrUpdL0.Params[05].AsInteger := DmSal.QrFunEveInformacao.Value;
          DmSal.QrUpdL0.Params[06].AsInteger := DmSal.QrFunEveSalvar.Value;
          DmSal.QrUpdL0.Params[07].AsInteger := DmSal.QrFunEveVisivel.Value;
          DmSal.QrUpdL0.Params[08].AsFloat   := DmSal.QrFunEveEve_ValPer.Value;
          DmSal.QrUpdL0.Params[09].AsInteger := DmSal.QrFunEveEve_Meses.Value;
          DmSal.QrUpdL0.Params[10].AsInteger := DmSal.QrFunEveEve_Incid.Value;
          DmSal.QrUpdL0.Params[11].AsInteger := DmSal.QrFunEveConta.Value;
          DmSal.QrUpdL0.Params[12].AsFloat   := DmSal.QrFunEveCta_IncPer.Value;
          DmSal.QrUpdL0.Params[13].AsInteger := DmSal.QrFunEveCta_MesesR.Value;
          DmSal.QrUpdL0.Params[14].AsInteger := DmSal.QrFunEveIncidencia.Value;
          DmSal.QrUpdL0.Params[15].AsInteger := DmSal.QrFunEveAuto.Value;
          DmSal.QrUpdL0.Params[16].AsInteger := DmSal.QrFunEveBase.Value;
          DmSal.QrUpdL0.Params[17].AsFloat   := DmSal.QrFunEvePercentual.Value;
          DmSal.QrUpdL0.Params[18].AsFloat   := -1;
          DmSal.QrUpdL0.Params[19].AsInteger := DmSal.QrFunEveProporciHT.Value; // proporcional as horas trabalhadas
          DmSal.QrUpdL0.Params[20].AsString  := DmSal.QrFunEveUnidade.Value;
          DmSal.QrUpdL0.Params[21].AsInteger := DmSal.QrFunEveEve_Ciclo.Value;
          DmSal.QrUpdL0.Params[22].AsString  := Geral.FDT(DmSal.QrFunEveEve_DtCicl.Value, 1);
          DmSal.QrUpdL0.ExecSQL;
          //
          DmSal.QrFunEve.Next;
        end;
      end;
      //
      DmSal.QrCalc.Next;
    end;
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmFPFolhaCal.CalculaTodosFuncionarios: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    DmSal.QrCalc.First;
    while not DmSal.QrCalc.Eof do
    begin
      CalculaFuncionarioAtual;
      //
      DmSal.QrCalc.Next;
    end;
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmFPFolhaCal.CalculaFuncionarioAtual: Boolean;
begin
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('DELETE FROM fpcalceven ');
  Dmod.QrUpdL.SQL.Add('WHERE Entidade=:P0 ');
  Dmod.QrUpdL.Params[0].AsInteger := DmSal.QrCalcEntidade.Value;
  Dmod.QrUpdL.ExecSQL;
  //
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('DELETE FROM fpcalcacum');
  Dmod.QrUpdL.SQL.Add('WHERE Entidade=:P0 ');
  Dmod.QrUpdL.Params[0].AsInteger := DmSal.QrCalcEntidade.Value;
  Dmod.QrUpdL.ExecSQL;
  //
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('DELETE FROM fpcalcferi');
  Dmod.QrUpdL.SQL.Add('WHERE Entidade=:P0 ');
  Dmod.QrUpdL.Params[0].AsInteger := DmSal.QrCalcEntidade.Value;
  Dmod.QrUpdL.ExecSQL;
  //
  {case FTipoCalc of
    1: CalculaFuncionarioTipoCalc1();
    3: CalculaFuncionarioTipoCalc3();
    else CalculaFuncionarioTipoCalcT();
  end;}
  CalculaFuncionarioTipoCalcT();
  Result := True;
end;

function TFmFPFolhaCal.GeraDados(Referencia, Valor: Double; Alteracao: Integer;
Unidade: String): Integer;
var
  Info, Salva, Mostra: Integer;
  ValReal: Double;
begin
  Info := DmSal.QrBaseInformacao.Value;
  Salva := DmSal.QrBaseSalvar.Value;
  Mostra := DmSal.QrBaseVisivel.Value;
  //Acumula := ?;
  //
  Info := DefineInfo(FTipoCalc, DmSal.QrBaseEvento.Value, Info);
  Salva := DefineSalva(FTipoCalc, DmSal.QrBaseEvento.Value, Salva);
  Mostra := DefineMostra(FTipoCalc, DmSal.QrBaseEvento.Value, Mostra);
  //Acumula := DefineAcumula(FTipoCalc, DmSal.QrBaseEvento.Value, Acumula);
  FNatureza := DmSal.QrBaseNatureza.Value;
  if FNatureza = 'P' then ValReal := Valor else
    if FNatureza = 'D' then ValReal := -Valor else
      ValReal := 0;
  //
  {Funci}DmSal.QrUpdL1.Params[00].AsInteger := DmSal.QrCalcEntidade.Value;
  {Event}DmSal.QrUpdL1.Params[01].AsInteger := DmSal.QrBaseEvento.Value;
  {Refer}DmSal.QrUpdL1.Params[02].AsFloat   := Referencia;
  {Valor}DmSal.QrUpdL1.Params[03].AsFloat   := ValReal;
         DmSal.QrUpdL1.Params[04].AsInteger := Alteracao;
         DmSal.QrUpdL1.Params[05].AsInteger := Info;
         DmSal.QrUpdL1.Params[06].AsInteger := Salva;
         DmSal.QrUpdL1.Params[07].AsInteger := Mostra;
         DmSal.QrUpdL1.Params[08].AsInteger := DmSal.QrBasePrioridade.Value;
         DmSal.QrUpdL1.Params[09].AsInteger := DmSal.QrBaseAuto.Value;
         DmSal.QrUpdL1.Params[10].AsString  := Unidade;
  DmSal.QrUpdL1.ExecSQL;
  Result := -2;
  if ValReal <> 0 then
  begin
    Result := Result + 2;
    DmSal.QrEvnAcum.Close;
    DmSal.QrEvnAcum.Params[0].AsInteger := DmSal.QrBaseEvento.Value;
    DmSal.QrEvnAcum.Open;
    //
    if DmSal.QrEvnAcum.RecordCount > 0 then
    begin
      while not DmSal.QrEvnAcum.Eof do
      begin
        DmSal.QrUpdL2.Params[0].AsInteger := DmSal.QrCalcEntidade.Value;
        DmSal.QrUpdL2.Params[1].AsInteger := DmSal.QrEvnAcumEvent.Value;
        DmSal.QrUpdL2.Params[2].AsInteger := DmSal.QrEvnAcumAcumu.Value;
        DmSal.QrUpdL2.ExecSQL;
        //
        DmSal.QrEvnAcum.Next;
      end;
    end;
    Result := Result + 1 + DmSal.QrEvnAcum.RecordCount;
  end else Result := -1;
end;

function TFmFPFolhaCal.GeraDadosPes(Evento: Integer; Referencia, Valor: Double;
Alteracao: Integer; Unidade: String): Integer;
var
  Info, Salva, Mostra: Integer;
  ValReal: Double;
begin
  Result := 0;
  DmSal.QrEvePes.Close;
  DmSal.QrEvePes.Params[0].AsInteger := Evento;
  DmSal.QrEvePes.Open;

  if Dmsal.QrEvePes.RecordCount = 0 then
  begin
    Application.MessageBox(PChar('ERRO! O evento ' + IntToStr(Evento) +
    ' n�o est� cadastrado! AVISE A DERMATEK'), 'ERRO', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Info := DmSal.QrEvePesInformacao.Value;
  Salva := DmSal.QrEvePesSalvar.Value;
  Mostra := DmSal.QrEvePesVisivel.Value;
  //
  Info := DefineInfo(FTipoCalc, DmSal.QrEvePesCodigo.Value, Info);
  Salva := DefineSalva(FTipoCalc, DmSal.QrEvePesCodigo.Value, Salva);
  Mostra := DefineMostra(FTipoCalc, DmSal.QrEvePesCodigo.Value, Mostra);
  //Acumula := DefineAcumula(FTipoCalc, DmSal.QrEvePesCodigo.Value, Acumula);
  FNatureza := DmSal.QrEvePesNatureza.Value;
  if ((FNatureza = 'P') and (Valor < 0)) then ValReal := -Valor else
    if ((FNatureza = 'D') and (Valor > 0)) then ValReal := -Valor else
      ValReal := Valor;
  //
  {Funci}DmSal.QrUpdL1.Params[00].AsInteger := DmSal.QrCalcEntidade.Value;
  {Event}DmSal.QrUpdL1.Params[01].AsInteger := DmSal.QrEvePesCodigo.Value;
  {Refer}DmSal.QrUpdL1.Params[02].AsFloat   := Referencia;
  {Valor}DmSal.QrUpdL1.Params[03].AsFloat   := ValReal;
         DmSal.QrUpdL1.Params[04].AsInteger := Alteracao;
         DmSal.QrUpdL1.Params[05].AsInteger := Info;
         DmSal.QrUpdL1.Params[06].AsInteger := Salva;
         DmSal.QrUpdL1.Params[07].AsInteger := Mostra;
         DmSal.QrUpdL1.Params[08].AsInteger := DmSal.QrEvePesPrioridade.Value;
         DmSal.QrUpdL1.Params[09].AsInteger := DmSal.QrEvePesAuto.Value;
         DmSal.QrUpdL1.Params[10].AsString  := Unidade;
  DmSal.QrUpdL1.ExecSQL;
  Result := -2;
  if ValReal <> 0 then
  begin
    Result := Result + 2;
    DmSal.QrEvnAcum.Close;
    DmSal.QrEvnAcum.Params[0].AsInteger := DmSal.QrEvePesCodigo.Value;
    DmSal.QrEvnAcum.Open;
    //
    if DmSal.QrEvnAcum.RecordCount > 0 then
    begin
      while not DmSal.QrEvnAcum.Eof do
      begin
        DmSal.QrUpdL2.Params[0].AsInteger := DmSal.QrCalcEntidade.Value;
        DmSal.QrUpdL2.Params[1].AsInteger := DmSal.QrEvnAcumEvent.Value;
        DmSal.QrUpdL2.Params[2].AsInteger := DmSal.QrEvnAcumAcumu.Value;
        DmSal.QrUpdL2.ExecSQL;
        //
        DmSal.QrEvnAcum.Next;
      end;
    end;
    Result := Result + 1 + DmSal.QrEvnAcum.RecordCount;
  end else Result := -1;
end;

function TFmFPFolhaCal.CalculaFuncionarioTipoCalcT(): Boolean;
var
  SalBase, BaseIRRF, IRRFval, DeduIRRF, AliqIRRF, Semanas, Adiant, BaseCalc,
  SalContr, IRRFbInt, DeduINSS, INSSbEmp, BaseFGTS, ValrFGTS, TotalDeduDepe,
  SalMinim, BaseINSS, InsalVal, PericVal, AdiantPg, InsalPer, PericPer,
  ReferG, Fator, Qtde, Valor, VlrFeria, BasePecu, AllFeri,
  FerMesFeri, FerMesAbon, FerMesFer3, FerMesAbo3, FerMesINSS, FerMesIRRF,
  FerMesLiq, SalFamil, ValrINSS, AliqINSS, PercPens, BasePens, PensFtor,
  PensIRRF, PensDfIR, PensValr, QtdHoras, BaseVal, HrReal, Refere, ValrPrev,
  Porc, Base091, HrExFat, HrExQtd, ValrHora, ValCalX, MediaCo, MediaAd,
  BaseFer3, ValrPecu, FerBase, AdiBase, FamBase, FatXX, ValIncid: Double;
  //
  PeriodoI, PeriodoT, QtdDepen, QtdFilho, PensLiqu, PeriAdmi: Integer;
  //
  EvenVar, DataV, DataI, DataF, DtIniPA, DtFimPA, Unidade: String;
begin
  FHorasMax  := 0;
  SalBase    := 0;
  AdiBase    := 0;
  FerBase    := 0;
  BaseCalc   := 0;
  SalContr   := 0;
  BaseIRRF   := 0;
  DeduIRRF   := 0;
  IRRFbInt   := 0;
  AliqIRRF   := 0;
  DeduINSS   := 0;
  INSSbEmp   := 0;
  BaseINSS   := 0;
  ValrINSS   := 0;
  //Adiant   := 0;
  BaseFGTS   := 0;
  //ValrFGTS := 0;
  InsalPer   := 0;
  InsalVal   := 0;
  PericPer   := 0;
  PericVal   := 0;
  //QtdDepen := 0;
  //QtdFilho := 0;
  //SalFamil := 0;
  //AliqINSS := 0;
  //ValrINSS := 0;
  //PensLiqu := 0;
  //PercPens := 0;
  BasePens   := 0;
  //QtdHoras := 0;
  ValrPrev   := 0;
  //Porc     := 0;
  //Base091  := 0;
  //HrExFat  := 0;
  ValrHora   := 0;
  AllFeri    := 0;
  FDdFerMes  := 0;
  FHorasTrab := 0;
  FSalTrab   := 0;
  FTemFerias := 0;
  FDdFerAbo  := 0;
  FDdFerias  := 0;
  FCodCalFer := 0;
  FDdPecun   := 0;
  FamBase    := 0;
  //ValIncid   := 0;
  //
  DataV := MLAGeral.UltimoDiaDoPeriodo(dmkEdPeriodo.ValueVariant, dtSystem);
  DmSal.QrSalMin.Close;
  DmSal.QrSalMin.Params[0].AsString := DataV;
  DmSal.QrSalMin.Open;
  SalMinim := DmSal.QrSalMinValor.Value;
  if SalMinim = 0 then
    MeMsg.Lines.Add('Sal�rio m�nimo indefinido!');
  // Dados do funcion�rio selecionado
  DmSal.QrFPFunci.Close;
  DmSal.QrFPFunci.Params[0].AsInteger := DmSal.QrCalcEntidade.Value;
  DmSal.QrFPFunci.Open;

  // eventos do funcion�rio selecionado
  DmSal.QrBase.Close;
  DmSal.QrBase.Params[0].AsInteger := DmSal.QrCalcEntidade.Value;
  DmSal.QrBase.Open;
  DmSal.QrBase.First;
  while not DmSal.QrBase.Eof do
  begin
    case DmSal.QrBaseFonte.Value of
      1: // Eventos autom�ticos
      begin
        case DmSal.QrBaseEvento.Value of
          // 001: Horas normais
          001:
          begin
            GeraDados(FHorasTrab, FSalTrab, 0, 'horas');
            if FTemFerias > 0 then
            begin
              // Parei Aqui
              // Colocar f�rias no pagamento mensal
              FerMesFeri := 0;
              FerMesAbon := 0;
              FerMesFer3 := 0;
              FerMesAbo3 := 0;
              FerMesIRRF := 0;
              FerMesINSS := 0;
              //
              if FCodCalFer > 0 then
              begin
                DmSal.QrItsCal.Close;
                DmSal.QrItsCal.Params[0].AsInteger := FCodCalFer;
                DmSal.QrItsCal.Open;
                DmSal.QrItsCal.First;
                while not DmSal.QrItsCal.Eof do
                begin
                  case DmSal.QrItsCalEvento.Value of
                    // Sal�rio Familia
                    //06: ; // Parei aqui. ver o que fazer
                    // F�rias normais
                    13: FerMesFeri := FerMesFeri + DmSal.QrItsCalValor.Value;
                    // Abono
                    14: FerMesAbon := FerMesAbon + DmSal.QrItsCalValor.Value;
                    // 1/3 s/ Valor F�rias
                    16: FerMesFer3 := FerMesFer3 + DmSal.QrItsCalValor.Value;
                    // 1/3 s/ Abono Pec.
                    23: FerMesAbo3 := FerMesAbo3 + DmSal.QrItsCalValor.Value;
                    // F�rias Venc. Var. H.E.
                    28: FerMesFeri := FerMesFeri + DmSal.QrItsCalValor.Value;
                    // F�rias Venc. Var. Comiss.
                    29: FerMesFeri := FerMesFeri + DmSal.QrItsCalValor.Value;
                    // F�rias Venc. Var. M�d. Adic.
                    30: FerMesFeri := FerMesFeri + DmSal.QrItsCalValor.Value;
                    // IRRF
                    50: FerMesIRRF := FerMesIRRF + DmSal.QrItsCalValor.Value;
                    // INSS
                    55: FerMesINSS := FerMesINSS + DmSal.QrItsCalValor.Value;

                    else begin
                      (*Application.MessageBox(PChar('ERRO. O evento de ' +
                      'f�rias n� ' + IntToStr(DmSal.QrItsCalEvento.Value) +
                      ' n�o est� implementado no sal�rio mensal! ' + Chr(13) +
                      Chr(10) + 'AVISE A DERMATEK!'), 'Erro', MB_OK+MB_ICONERROR);*)
                      MeMsg.Lines.Add('ERRO. O evento de ' +
                      'f�rias n� ' + IntToStr(DmSal.QrItsCalEvento.Value) +
                      ' n�o est� implementado no sal�rio mensal! AVISE A DERMATEK!');
                    end;
                  end;
                  DmSal.QrItsCal.Next;
                end;
              end else Application.MessageBox(PChar('M�s com f�rias ' +
              'sem c�lculo definido para o funcion�rio ' +
              DmSal.QrFunciNOMEFUNCI.Value + '!'), 'Aviso', MB_OK+MB_ICONWARNING);
              if FDdFerias > 0 then
              begin
                FerMesFeri := Round(FerMesFeri * FDdFerMes / FDdFerias * 100) / 100;
                FerMesFer3 := Round(FerMesFer3 * FDdFerMes / FDdFerias * 100) / 100;
                FerMesINSS := Round(FerMesINSS * FDdFerMes / FDdFerias * 100) / 100;
                FerMesIRRF := Round(FerMesIRRF * FDdFerMes / FDdFerias * 100) / 100;
                //
                GeraDadosPes(60, FDdFerMes, FerMesFeri, 0, 'dias');
                GeraDadosPes(62,        0, FerMesFer3, 0, '');
                if FTemFerias <> 1 then
                begin
                  FerMesAbon := 0;
                  FerMesAbo3 := 0;
                end else begin
                  if FerMesAbon > 0 then
                  begin
                    GeraDadosPes(61, FDdFerAbo, FerMesAbon, 0, 'dias');
                    GeraDadosPes(63,        0, FerMesAbo3, 0, '');
                  end;
                end;
                GeraDadosPes(64,        0, FerMesINSS, 0, '');
                GeraDadosPes(65,        0, FerMesIRRF, 0, '');
                FerMesLiq  := FerMesFeri + FerMesAbon + FerMesFer3 +
                              FerMesAbo3 + FerMesINSS + FerMesIRRF;
                GeraDadosPes(66,        0, -FerMesLiq, 0, '');
                FamBase := FamBase + FerMesFeri + FerMesFer3;
              end else Application.MessageBox(PChar('ERRO. Dias de f�rias ' +
              'dentro do m�s n�o definidos! AVISE A DERMATEK'), 'ERRO',
              MB_OK+MB_ICONERROR);
            end;
            //
          end;

          // 002: Adiantamento salarial
          002:
          begin
            case RGAdianta.ItemIndex of
              0: {Percentual}
              begin
                //  Verifica se inicia f�rias no m�s
                DefineItensDiversos(SalBase);

                (*ShowMessage('Adiantamento de sal�rio em m�s com f�rias em ' +
                'constru��o! Status f�rias: ' + IntToStr(FTemFerias));
                Application.MessageBox(PChar(
                'Base sal�rio' + FormatFloat('0.00', SalBase) + Chr(13) + Chr(10) +
                'Base Adiant.' + FormatFloat('0.00', AdiBase) + Chr(13) + Chr(10) +
                'Base f�rias ' + FormatFloat('0.00', FerBase) + Chr(13) + Chr(10) +
                'Base c�lculo' + FormatFloat('0.00', BaseCalc) + Chr(13) + Chr(10) +
                '-=-=-=-' + FormatFloat('0.00', 0) + Chr(13) + Chr(10) +
                ''), 'Itens', MB_OK+MB_ICONINFORMATION);*)

                ReferG  := dmkEdAdianta.ValueVariant;
                if FDdFerMes > 0 then
                  Adiant  := Round(ReferG * AdiBase * (30 - FDdFerMes) / 30) / 100
                else
                  Adiant  := Round(ReferG * AdiBase) / 100;
                Unidade := '%';
              end;
              1:
              begin
                ReferG  := 0;
                Adiant  := dmkEdAdianta.ValueVariant;
                Unidade := '';
              end;
              else begin // N�o definido
                ReferG  := 0;
                Adiant  := 0;
                Unidade := '';
              end;
            end;
            GeraDados(ReferG, Adiant, 0, Unidade);
          end;

          // 006: Sal�rio fam�lia
          006:
          begin
            QtdFilho := DmSal.QrFPFunciFilhos.Value;
            if QtdFilho > 0 then
            begin
              DmSal.QrSalFam.Close;
              DmSal.QrSalFam.Params[0].AsFloat := FamBase;
              DmSal.QrSalFam.Open;
              SalFamil := QtdFilho * DmSal.QrSalFamValor.Value;
              GeraDados(QtdFilho, SalFamil, 0, '');
            end;
          end;

          // 008: Pro-labore
          008:
          begin
            GeraDados(0, SalBase, 0, '');
          end;

          // 013: F�rias
          013:
          begin
            if FTipoCalc = 5 then
            begin
              ReopenIniFer;
              Dmod.QrUpdL.SQL.Clear;
              Dmod.QrUpdL.SQL.Add('UPDATE fpcalc SET FPFunciFer=:P0 ');
              Dmod.QrUpdL.SQL.Add('WHERE Entidade=:P1 ');
              Dmod.QrUpdL.Params[00].Asinteger := DmSal.QrIniFerControle.Value;
              Dmod.QrUpdL.Params[01].AsInteger := DmSal.QrCalcEntidade.Value;
              Dmod.QrUpdL.ExecSQL;
              //
              FDdFerias := DmSal.QrIniFerDiasGozar.Value;
              FDdPecun  := DmSal.QrIniFerDiasPecun.Value;
              AllFeri  := AllFeri + FerBase;
              VlrFeria := FerBase * FDdFerias / 30;
              GeraDados(FDdFerias, VlrFeria, 0, 'Dias');
            end else begin
              Application.MessageBox(PChar('O tipo de c�lculo ' + IntToStr(
              FTipoCalc) + ' n�o foi implementado para o evento ' + IntToStr(
              DmSal.QrBaseEvento.Value) + '.'), 'Aviso', MB_OK+MB_ICONWARNING);
              MeMsg.Lines.Add('O tipo de c�lculo ' + IntToStr(
              FTipoCalc) + ' n�o foi implementado para o evento ' + IntToStr(
              DmSal.QrBaseEvento.Value) + '.');
            end;
            //
          end;

          014: // Abono Pecuni�rio
          begin
            {
            BasePecu := DmSal.SomaValoresDeIncidencia(10);
            if (BasePecu > 0) and (FDdFerias > 0) and (FDdPecun > 0) then
              ValrPecu := BasePecu / FDdFerias * FDdPecun else ValrPecu := 0;}
            ValrPecu := FerBase * FDdPecun / 30;
            if ValrPecu > 0 then
              GeraDados(FDdPecun, ValrPecu, 0, 'Dias');
          end;

          016: // 1/3 s/ Valor F�rias
          begin
            BaseFer3 := DmSal.SomaValoresDeIncidencia(10, DmSal.QrCalcEntidade.Value);
            if BaseFer3 > 0 then
              GeraDados(0, BaseFer3 / 3, 0, '1/3');
          end;

          023: // 1/3 s/ Abono Pec.
          begin
            if ValrPecu > 0 then
              GeraDados(0, ValrPecu / 3, 0, '1/3');
          end;

          025: // F�rias Venc. Vari�vel
          begin
            // Anulado
            // 028: F�rias Venc. Var. H.E. (horas extras)
            // 029: F�rias Venc. Var. Comiss. (Comiss�es)
            // 030:
          end;

          028: //F�rias Venc. Var. H.E. (by Dermatek)
          begin
            // DmSal.QrIniFer j� est� aberto pelo c�lculo 13
            DmSal.QrHrE.Close;
            DmSal.QrHrE.Params[0].AsInteger := FEmpreCod;
            DmSal.QrHrE.Params[1].AsInteger := DmSal.QrCalcEntidade.Value;
            DmSal.QrHrE.Params[2].AsString  := Geral.FDT(DmSal.QrIniFerDtIniPA.Value, 1);
            DmSal.QrHrE.Params[3].AsString  := Geral.FDT(DmSal.QrIniFerDtFimPA.Value, 1);
            DmSal.QrHrE.Open;
            HrExFat := 0;
            HrExQtd := 0;
            DmSal.QrHrE.First;
            while not DmSal.QrHrE.Eof do
            begin
              HrExFat := HrExFat + (DmSal.QrHrEFatHr.Value / 12);
              HrExQtd := HrExQtd + (DmSal.QrHrEHoras.Value / 12);
              DmSal.QrHrE.Next;
            end;
            //
            FatXX := (DmSal.QrIniFerHEFator.Value / 100) + 1;
            HrExQtd := HrExQtd +
              DmSal.QrIniFerMeHEMan1xx.Value + DmSal.QrIniFerMeHEMan200.Value;

            HrExFat := Int((HrExFat +
              (DmSal.QrIniFerMeHEMan1xx.Value * FatXX) +
              (DmSal.QrIniFerMeHEMan200.Value * 2.0)) * 100) / 100;
            if HrExFat > 0 then
            begin
              Dmod.QrUpdL.SQL.Clear;
              Dmod.QrUpdL.SQL.Add('INSERT INTO fpcalcferi SET ');
              Dmod.QrUpdL.SQL.Add('Entidade=:P0, Item=:P1, Tipo=:P2, ');
              Dmod.QrUpdL.SQL.Add('Auto=:P3, Referencia=:P4, Valor=:P5 ');
              if DmSal.QrHrE.RecordCount > 0 then
              begin
                DmSal.QrHrE.First;
                while not DmSal.QrHrE.Eof do
                begin
                  ValCalX := (DmSal.QrHrEFatHr.Value * ValrHora) / 12;
                  Dmod.QrUpdL.Params[00].AsInteger := DmSal.QrCalcEntidade.Value;
                  Dmod.QrUpdL.Params[01].AsInteger := Round(DmSal.QrHrEFator.Value);
                  Dmod.QrUpdL.Params[02].AsInteger := 1; // Hora Extra
                  Dmod.QrUpdL.Params[03].AsInteger := 1; // Automatico
                  Dmod.QrUpdL.Params[04].AsFloat   := DmSal.QrHrEHoras.Value / 12;
                  Dmod.QrUpdL.Params[05].AsFloat   := ValCalX;
                  Dmod.QrUpdL.ExecSQL;
                  DmSal.QrHrE.Next;
                end;
              end;
              if DmSal.QrIniFerMeHEMan1xx.Value > 0 then
              begin
                FatXX := (DmSal.QrIniFerHEFator.Value / 100) + 1;
                ValCalX := DmSal.QrIniFerMeHEMan1xx.Value * FatXX * ValrHora;
                Dmod.QrUpdL.Params[00].AsInteger := DmSal.QrCalcEntidade.Value;
                Dmod.QrUpdL.Params[01].AsInteger := Round(FatXX * 100);
                Dmod.QrUpdL.Params[02].AsInteger := 1; // Hora Extra
                Dmod.QrUpdL.Params[03].AsInteger := 0; // Manual
                Dmod.QrUpdL.Params[04].AsFloat   := DmSal.QrIniFerMeHEMan1xx.Value;
                Dmod.QrUpdL.Params[05].AsFloat   := ValCalX;
                Dmod.QrUpdL.ExecSQL;
              end;
              if DmSal.QrIniFerMeHEMan200.Value > 0 then
              begin
                ValCalX := DmSal.QrIniFerMeHEMan200.Value * 2.0 * ValrHora;
                Dmod.QrUpdL.Params[00].AsInteger := DmSal.QrCalcEntidade.Value;
                Dmod.QrUpdL.Params[01].AsInteger := 200;
                Dmod.QrUpdL.Params[02].AsInteger := 1; // Hora Extra
                Dmod.QrUpdL.Params[03].AsInteger := 0; // Manual
                Dmod.QrUpdL.Params[04].AsFloat   := DmSal.QrIniFerMeHEMan200.Value;
                Dmod.QrUpdL.Params[05].AsFloat   := ValCalX;
                Dmod.QrUpdL.ExecSQL;
              end;
            end;
            //
            DmSal.QrVVF.Close;
            DmSal.QrVVF.Params[0].AsInteger := DmSal.QrCalcEntidade.Value;
            DmSal.QrVVF.Open;
            //
            if DmSal.QrVVFValor.Value > 0 then
            begin
              GeraDados(HrExQtd, DmSal.QrVVFValor.Value  * FDdFerias / 30, 0, 'horas');
              //
              Dmod.QrUpdL.SQL.Clear;
              Dmod.QrUpdL.SQL.Add('UPDATE fpcalc SET MediaHE=:P0 ');
              Dmod.QrUpdL.SQL.Add('WHERE Entidade=:P1 ');
              Dmod.QrUpdL.Params[00].AsFloat   := DmSal.QrVVFValor.Value;
              Dmod.QrUpdL.Params[01].AsInteger := DmSal.QrCalcEntidade.Value;
              Dmod.QrUpdL.ExecSQL;
              //
              AllFeri  := AllFeri + DmSal.QrVVFValor.Value;
            end;
            //
          end;

          029: //F�rias Venc. Var. Comiss. (by Dermatek)
          begin
            // DmSal.QrIniFer j� est� aberto pelo c�lculo 13
            if DmSal.QrIniFerMediaCoMan.Value > 0 then
            begin
              MediaCo := DmSal.QrIniFerMediaCoMan.Value;
              GeraDados(0, MediaCo * FDdFerias / 30, 0, '');
              //
              Dmod.QrUpdL.SQL.Clear;
              Dmod.QrUpdL.SQL.Add('UPDATE fpcalc SET MediaCo=:P0 ');
              Dmod.QrUpdL.SQL.Add('WHERE Entidade=:P1 ');
              Dmod.QrUpdL.Params[00].AsFloat   := MediaCo;
              Dmod.QrUpdL.Params[01].AsInteger := DmSal.QrCalcEntidade.Value;
              Dmod.QrUpdL.ExecSQL;
              //
              AllFeri  := AllFeri + MediaCo;
            end;
          end;

          030: //F�rias Venc. Var. Adici. (m�dia)(by Dermatek)
          begin
            // DmSal.QrIniFer j� est� aberto pelo c�lculo 13
            if DmSal.QrIniFerMediaAdMan.Value > 0 then
            begin
              MediaAd := DmSal.QrIniFerMediaAdMan.Value;
              GeraDados(0, MediaAd * FDdFerias / 30, 0, '');
              //
              Dmod.QrUpdL.SQL.Clear;
              Dmod.QrUpdL.SQL.Add('UPDATE fpcalc SET MediaAd=:P0 ');
              Dmod.QrUpdL.SQL.Add('WHERE Entidade=:P1 ');
              Dmod.QrUpdL.Params[00].AsFloat   := MediaAd;
              Dmod.QrUpdL.Params[01].AsInteger := DmSal.QrCalcEntidade.Value;
              Dmod.QrUpdL.ExecSQL;
              //
              AllFeri  := AllFeri + MediaAd;
            end;
          end;

          //////////////////////////////////////////////////////////////////////
                                                          //                  //
          // 040: Base adicional de insalubridade         //  Adicionado      //
          // calculado no c�digo 91                       //  DERMATEK        //
          040:                                            //                  //
          begin                                           //                  //
            if InsalVal > 0 then                          //                  //
              GeraDados(InsalPer, InsalVal, 0, '% S.M.'); //                  //
          end;                                            //                  //
          // 041: Base adicional de periculosidade        //                  //
          // calculado no c�digo 91                       //                  //
          041:                                            //                  //
          begin                                           //                  //
            if PericVal > 0 then                          //                  //
              GeraDados(PericPer, PericVal, 0, '%');      //                  //
          end;                                            //                  //
          // 042: Adicional de insalubridade              //                  //
          // calculado no c�digo 91                       //                  //
          042:                                            //                  //
          begin                                           //                  //
            if InsalVal > 0 then                          //                  //
            begin                                         //                  //
              GeraDados(InsalPer, InsalVal, 0, '% S.M.'); //                  //
              BaseCalc := BaseCalc + InsalVal;            //                  //
            end;                                          //                  //
          end;                                            //                  //
          // 043: Adicional de periculosidade             //                  //
          // calculado no c�digo 91                       //                  //
          043:                                            //                  //
          begin                                           //                  //
            if PericVal > 0 then                          //                  //
            begin                                         //                  //
              GeraDados(PericPer, PericVal, 0, '%');      //                  //
              BaseCalc := BaseCalc + PericVal;            //                  //
            end;                                          //                  //
          end;                                            //                  //
          //////////////////////////////////////////////////////////////////////


          // 050: IRRF
          050:
          begin
            // Parei Aqui
            // Falta fazer
            //BaseIRRF := BaseIRRF + BaseCalc;
            BaseIRRF := DmSal.SomaValoresDeIncidencia(1, DmSal.QrCalcEntidade.Value);

            // Descontos
            // - INSS
            BaseIRRF := BaseIRRF - ValrINSS;

            // - dependentes
            TotalDeduDepe := DmSal.QrFPFunciDependent.Value * FParam_DeduDepe;
            BaseIRRF := BaseIRRF - TotalDeduDepe;
            // - Previdenc. privada
            BaseIRRF := BaseIRRF - ValrPrev;
            if BaseIRRF < 0 then
              BaseIRRF := 0;
            if BaseIRRF > 0 then
            begin
              DmSal.QrPesqIRRF.Close;
              DmSal.QrPesqIRRF.Params[0].AsFloat  := BaseIRRF;
              DmSal.QrPesqIRRF.Open;
              if DmSal.QrPesqIRRF.RecordCount > 0 then
              begin
                DeduIRRF := DmSal.QrPesqIRRFDeducao.Value;
                AliqIRRF := DmSal.QrPesqIRRFAliquota.Value;
                IRRFval := (Round(BaseIRRF * AliqIRRF) / 100) - DeduIRRF;
                // verificar se IRRFVal � igual po maior que m�nimo
                if IRRFVal < FParam_VMinIRRF then
                begin
                  IRRFval  := 0;
                  AliqIRRF := 0;
                end;
              end else IRRFval := 0;
            end else IRRFval := 0;
            if IRRFval > 0 then
              GeraDados(0, IRRFval, 0, '');
          end;

          // 051: Arredondamento anterior
          051:
          begin
            // Parei Aqui
            // Falta fazer
          end;

          // 052: Pens�o aliment�cia
          052:
          begin
            PercPens := DmSal.QrFPFunciPensaoAlm.Value;
            PensLiqu := DmSal.QrFPFunciPensaoLiq.Value;
            if PercPens > 0 then
            begin
              case PensLiqu of
                // 0: Sobre o bruto
                0: BasePens := BaseCalc;
                1: // Sobre o sal�rio l�quido
                begin
// Como calcular a pens�o sobre o sal�rio l�quido:
// Lembrando que o rendimento l�quido � o resultado do Rendimento Bruto
// deduzido todos os descontos legais, tais como:
// INSS, IRRF, Contribui��es: Sindical, Assistencial, Confederativa, etc,
//e outros determinados na Conven��o/Acordo Coletivo da categoria.
                  BasePens := BaseCalc - ValrINSS;
                  //BasePens := BasePens * PensFtor;
                  // j� prever as dedu��es
                  //PensFtor := Round(BasePens * (100 - PercPens)) / 100;
                  PensFtor := BasePens;
                  DmSal.QrIRRF.Close;
                  DmSal.QrIRRF.Params[0].AsFloat := PensFtor;
                  DmSal.QrIRRF.Open;
                  PensIRRF := ((Round(BasePens * DmSal.QrIRRFAliquota.Value))) /
                    100 - DmSal.QrIRRFDeducao.Value;
                  PensDfIR := 1 - ((DmSal.QrIRRFAliquota.Value / 100) *
                              (PercPens / 100));
                  BasePens := Round(((BasePens - PensIRRF) / PensDfIR) * 100) / 100;
                  //
                end;
              end;
              PensValr := Round(BasePens * PercPens) / 100;
            end;
            //
            if PensValr > 0 then
              GeraDados(0, PensValr, 0, '');
            // Parei Aqui
          end;

          // 053: Adiantamento salarial pago no m�s
          053:
          begin
            {SELECT SUM(ValorL) ValorL
            FROM fpfolhacal
            WHERE TipoCalc=:P0
            AND Periodo=:P1
            AND Empresa=:P2
            AND Entidade=:P3}
            DmSal.QrSalPago.Close;
            DmSal.QrSalPago.Params[00].AsInteger := 1; // Adiantamento
            DmSal.QrSalPago.Params[01].AsInteger := FPeriodo;
            DmSal.QrSalPago.Params[02].AsInteger := FEmpreCod;
            DmSal.QrSalPago.Params[03].AsInteger := DmSal.QrCalcEntidade.Value;
            DmSal.QrSalPago.Open;
            AdiantPg := DmSal.QrSalPagoValorL.Value;
            //
            GeraDados(0, AdiantPg, 0, '');
          end;

          // 054: Desc. Compl. Sal�rio
          054:
          begin
            // Parei Aqui
            // Falta fazer
          end;

          // 055: INSS
          055:
          begin
            // Pro labore
            if FTipoCalc = 8 then
            begin
              BaseINSS := SalBase;
              DmSal.QrCond.Close;
              DmSal.QrCond.Params[0].AsInteger :=  DmSal.QrEmpresasCodigo.Value;
              DmSal.QrCond.Open;
              //
              AliqINSS := DmSal.QrCondProLaINSSr.Value;
            end else begin
            BaseINSS := DmSal.SomaValoresDeIncidencia(2, DmSal.QrCalcEntidade.Value);
              DmSal.QrINSS.Close;
              DmSal.QrINSS.Params[0].AsFloat := BaseINSS;
              DmSal.QrINSS.Open;
              if DmSal.QrINSS.RecordCount > 0 then
                AliqINSS := DmSal.QrINSSAliquota.Value
              else begin
                DmSal.QrINSS.Close;
                DmSal.QrINSS.Params[0].AsFloat := 0;
                DmSal.QrINSS.Open;
                // ir para a maior faixa
                DmSal.QrINSS.Last;
                BaseINSS := DmSal.QrINSSFaixa.Value;
                AliqINSS := DmSal.QrINSSAliquota.Value;
                if FTemFerias > 0 then
                begin
                  BaseINSS := BaseINSS + (FerMesINSS / AliqINSS * 100);
                end;
              end;
            end;
            ValrINSS := Round(AliqINSS * BaseINSS) / 100;
            GeraDados(AliqINSS, ValrINSS, 0, '%');
            // Parei Aqui
            //arredondamento ?
          end;

          // 059: IRRF s/ Adiantamento
          059:
          begin
            BaseIRRF := DmSal.SomaValoresDeIncidencia(1, DmSal.QrCalcEntidade.Value);
            //;
            // Parei Aqui
            // Falta fazer desconto de IRRF j� contribu�do no m�s de compet�ncia
            //
            //
{
Por outro lado, se o empregador efetua adiantamentos no m�s trabalhado
e quita a folha de pagamento somente no m�s seguinte,
o Imposto de Renda na Fonte incidir� sobre o somat�rio dos valores
dos adiantamentos e dos demais pagamentos efetuados dentro do m�s.

Exemplo:


Dia 05 - pagamento da folha do m�s anterior:  R$ 1.620,00
Dia 20 - adiantamento de 30% do m�s corrente: R$   700,00

No dia 20 (vinte), por ocasi�o da entrega do adiantamento, para fins de
reten��o, a fonte pagadora dever� considerar o somat�rio dos valores de
R$ 1.620,00 e de R$ 700,00 para a aplica��o da tabela progressiva.
O Imposto de Renda retido sobre o pagamento do dia 5 (cinco) ser� compensado
com o imposto apurado no dia 20 (vinte), e o saldo dever� ser recolhido at�
o terceiro dia �til da semana subseq�ente ao do fato gerador.
}
            //if (DmSal.QrFPtpCalCodigo.Value <> 1) or
            IRRFVal := 0;
            if CkIRRF.Checked then
            begin
              //descontar dependentes?
              // N�o
              DmSal.QrPesqIRRF.Close;
              DmSal.QrPesqIRRF.Params[0].AsFloat  := BaseIRRF;
              DmSal.QrPesqIRRF.Open;
              if DmSal.QrPesqIRRF.RecordCount > 0 then
              begin
                DeduIRRF := DmSal.QrPesqIRRFDeducao.Value;
                AliqIRRF := DmSal.QrPesqIRRFAliquota.Value;
                IRRFval := (Round(BaseIRRF * AliqIRRF) / 100) - DeduIRRF;
                // verificar se IRRFVal � igual po maior que m�nimo
                if IRRFVal < FParam_VMinIRRF then
                  IRRFval := 0;
              end else IRRFval := 0;
            end else begin
              // Acerto de IRRF pago dentro do m�s
              if DmSal.QrFPFunciComoCalcIR.Value = 1 then
              begin
                // 50 IRRF
                // 59 IRRF sobre adiantamento
{
SELECT SUM(fev.Valor) Valor
FROM fpfolhaeve fev
LEFT JOIN fpfolhacal fca ON fca.Codigo=fev.Codigo
WHERE (
  (fev.Evento = 50) OR
  ((fev.Evento = 59) AND (fev.Codigo<>:P0) )
   )
AND fca.Empresa=:P1
AND fca.Entidade=:P2
AND fca.DataC BETWEEN :P3 AND :P4
}
                DataI := Geral.FDT(MLAGeral.PrimeiroDiaDoMes(TPDataC.Date), 1);
                DataF := Geral.FDT(MLAGeral.UltimoDiaDoMes(TPDataC.Date), 1);
                DmSal.QrIRPg.Close;
                DmSal.QrIRPg.Params[00].AsInteger := DmSal.QrCalcCDO_COD.Value;
                DmSal.QrIRPg.Params[01].AsInteger := FEmpreCod;
                DmSal.QrIRPg.Params[02].AsInteger := DmSal.QrCalcEntidade.Value;
                DmSal.QrIRPg.Params[03].AsString  := DataI;
                DmSal.QrIRPg.Params[04].AsString  := DataF;
                DmSal.QrIRPg.Open;
                //
                // Parei Aqui
              end;
            end;
            if IRRFval > 0 then
              GeraDados(AliqIRRF, IRRFval, 0, '');
            //Parei aqui
          end;
          // 078: Base f�rias cheias (by Dermatek)
          078: if AllFeri > 0 then
              GeraDados(0, AllFeri, 0, '');
          // 080: Base de INSS Empresa
          080: if INSSbEmp > 0 then
              GeraDados(0, INSSbEmp, 0, '');
          // 081: Base de IR Adiantamento
          081: GeraDados(0, BaseIRRF, 0, '');
          // 082: Dedu��o do INSS
          082: if DeduINSS > 0 then
               GeraDados(0, DeduINSS, 0, '');
          // 088: Dedu��o de IRRF
          088: if DeduIRRF > 0 then
               GeraDados(0, DeduIRRF, 0, '');
          // 089: Sal�rio de Acordo com o C�lculo
          089: GeraDados(0, BaseCalc, 0, '');
          // 090: Base interna de IRRF
          090: if IRRFbInt > 0 then
               GeraDados(0, IRRFbInt, 0, '');
          // 091: Sal�rio Base para C�lculo do Usu�rio
          // Parei Aqui : Fazer sal�rio proporcional aos dias trabalhdos na admiss�o!
          091:
          begin
            SalContr := DmSal.QrFPFunciSalario.Value;

            // Horas cheias
            if FHorasMax = 0 then DefineHorasMax;
            PeriAdmi := Geral.Periodo2000(DmSal.QrFPFunciDataAdm.Value);
            if (PeriAdmi = FPeriodo) then
            begin
              // Parei Aqui
              ShowMessage('Per�odo parcial ainda n�o implementado');
              //calcular dias uteis dentro da semana para calcular total de horas de descanso
              // Falta fazer
              QtdHoras := 0;
            end else QtdHoras := FHorasMax;

            // Salario cheio
            case DmSal.QrFPFunciTipoSal.Value of
              1: Base091 := SalContr;
              4: Base091 := SalContr * 30;
              5:
              begin
               if DmSal.QrFPFunciProfessor.Value = 1 then
                 Semanas := 4.5
               else
                 Semanas := 5;
               Base091 := SalContr * Semanas * DmSal.QrFPFunciHrSeman.Value;
              end;
              else Base091 := 0;
            end;
            if QtdHoras > 0 then
              ValrHora := Base091 / QtdHoras
            else ValrHora := 0;
            SalBase := SalBase + Base091;
            FerBase := FerBase + Base091;
            AdiBase := AdiBase + Base091;
            FamBase := FamBase + Base091;
            DefineItensDiversos(SalBase);

            // Periculosidade e insalubridade
            // N�o pode acumular periculosidade mais insalubridade
            // o funcion�rio escolhe qual deseja
            // mas a periculosidade � mais vantajosa
            PericPer := DmSal.QrFPFunciPericulosi.Value;
            if PericPer > 0 then
            begin
              PericVal := Round(PericPer * SalBase) /  100;
              //TipoCalc1
              case FTipoCalc of
                // Adiantamento de sal�rio
                1: AdiBase := AdiBase + PericVal;
                // Mensal
                3: SalBase := SalBase + PericVal;
                // F�rias 
                5: FerBase := FerBase + PericVal;
              end;
              FamBase := FamBase + PericVal;
            end else
            InsalPer := DmSal.QrFPFunciInsalubrid.Value;
            if InsalPer > 0 then
            begin
              case FTipoCalc of
                1: // Adiantamento salarial
                begin
                  InsalVal := Round(InsalPer * SalMinim) /  100;
                  AdiBase := AdiBase + InsalVal;
                end;
                3: // Mensal
                begin
                  InsalVal := Round(InsalPer * SalMinim * FHorasTrab / FHorasMax) /  100;
                  //SalBase := SalBase + InsalVal;
                end;
                5: // F�rias ->
                begin
                  InsalVal := Round(InsalPer * SalMinim) /  100;
                  FerBase := FerBase + InsalVal;
                end;
              end;
              FamBase := FamBase + InsalVal;
            end;
            // Fim Periculosidade e insalubridade


            BaseCalc := BaseCalc + SalBase;
            GeraDados(QtdHoras, SalBase, 0, '');
          end;
          // 092: Dedu��o de Dependentes
          092: if TotalDeduDepe > 0 then
               GeraDados(0, TotalDeduDepe, 0, '');
          // 093: Faixa IRRF
          093: GeraDados(0, AliqIRRF, 0, '');
          // 094: Sal�rio Contratual
          094: GeraDados(0, SalContr, 0, '');
          // 095: Base para FGTS
          095:
          begin
//O porcentual de 8% do FGTS n�o � recolhido somente sobre o valor do sal�rio,
//mas incide tamb�m sobre o total do valor pago em horas extras,
//adicionais (noturno, periculosidade e insalubridade), 13� sal�rio,
//f�rias (sal�rio + 1/3) e aviso pr�vio (trabalhado ou indenizado).
//N�o h� desconto desse valor no sal�rio do trabalhador.
            BaseFGTS := DmSal.SomaValoresDeIncidencia(3, DmSal.QrCalcEntidade.Value);
            //
            GeraDados(0, BaseFGTS, 0, '');
          end;
          // 096: Valor do FGTS
          096:
          begin
            ValrFGTS := Round(BaseFGTS * FParam_PorcFGTS) / 100;
            GeraDados(FParam_PorcFGTS, ValrFGTS, 0, '');
          end;
          // 097: Sal�rio M�nimo
          097: GeraDados(0, SalMinim, 0, '');
          // 098: Base para INSS
          098: GeraDados(0, BaseINSS, 0, '');
          // 099: Base para IRRF
          099: GeraDados(0, BaseIRRF, 0, '');
          else
          begin
            DmSal.QrEvent.Close;
            DmSal.QrEvent.Params[0].AsInteger := DmSal.QrBaseEvento.Value;
            DmSal.QrEvent.Open;
            {Application.MessageBox(PChar('O evento n� ' + IntToStr(
            DmSal.QrBaseEvento.Value) + ' "' + DmSal.QrEventDescricao.Value +
            '" ainda n�o foi implementado'), 'Aviso', MB_OK+MB_ICONWARNING);}
            MeMsg.Lines.Add('O evento n� ' + IntToStr(
            DmSal.QrBaseEvento.Value) + ' "' + DmSal.QrEventDescricao.Value +
            '" ainda n�o foi implementado');
            DmSal.QrEvent.Close;
          end;
        end;
      end;
      2..3: // Eventos cadastrados no funcion�rio e adicionados pelo usu�rio
      begin
        //Fator  := 0;
        //Qtde   := 0;
        Valor  := 0;
        Refere := 0;
        Unidade := '';
        case DmSal.QrBaseIncidencia.Value of
          0: // Incid�ncia sobre o evento
          begin
            Fator := DmSal.QrBaseEve_ValPer.Value;
            if DmSal.QrBaseEve_BasCod.Value > 0 then
            begin

              ///////////////////////////////////
              //      Novo c�lculo 2008-02-10

              // se for pelo valor...
              if DmSal.QrBaseEve_Incid.Value = 0 then
              begin
                Refere := 0;
                Valor := DmSal.QrBaseEve_ValPer.Value;
              end
              // caso contr�rio � pela base selecionada (variavel de incid�ncia)
              else begin
                //incidencia 9 n�o d�
                if DmSal.QrBaseEve_BasCod.Value in ([7,9]) then
                  ShowMessage(IntToStr(DmSal.QrBaseEve_BasCod.Value));
                ValIncid := DmSal.SomaValoresDeIncidencia(
                  DmSal.QrBaseEve_BasCod.Value, DmSal.QrCalcEntidade.Value);
                // Parei aqui usar o ValIncid


                //BaseVal := SalBase;

                // Quantidade
                //Qtde  := DmSal.QrBaseEve_ValPer.Value;

                // porcentagem
                Porc := DmSal.QrBaseEve_BasPer.Value;

                // Propor��o de horas
                case DmSal.QrBaseFonte.Value of
                  2: HrReal := DmSal.QrBaseReferencia.Value;
                  3: HrReal := DmSal.QrBaseEve_ValPer.Value;
                  else HrReal := -1;
                end;
                if FHorasMax = 0 then DefineHorasMax;
                if HrReal = -1 then HrReal := FHorasMax;
                //Horas :=     $ 562,51  3,5     220     220
                Refere := HrReal;
                if FHorasMax > 0 then
                  Valor := Round(ValIncid * Porc * HrReal / FHorasMax) / 100
                else
                  Valor := 0;
              end;

              //      Fim novo c�lculo
              ///////////////////////////////////
              {
              case DmSal.QrBaseEve_BasCod.Value of
                //  Sal�rio base
                6..7:
                begin
                  if DmSal.QrBaseEve_Incid.Value = 0 then
                  begin
                    Refere := 0;
                    Valor := DmSal.QrBaseEve_ValPer.Value;

                  end else begin
                    BaseVal := SalBase;

                    // Quantidade
                    //Qtde  := DmSal.QrBaseEve_ValPer.Value;

                    // porcentagem
                    Porc := DmSal.QrBaseEve_BasPer.Value;

                    // Propor��o de horas
                    case DmSal.QrBaseFonte.Value of
                      2: HrReal := DmSal.QrBaseReferencia.Value;
                      3: HrReal := DmSal.QrBaseEve_ValPer.Value;
                      else HrReal := -1;
                    end;
                    if FHorasMax = 0 then DefineHorasMax;
                    if HrReal = -1 then HrReal := FHorasMax;
                    //Horas :=     $ 562,51  3,5     220     220
                    Refere := HrReal;
                    if FHorasMax > 0 then
                      Valor := Round(BaseVal * Porc * HrReal / FHorasMax) / 100
                    else
                      Valor := 0;
                  end;
                end;
                // Valor Hora
                //7: BaseVal := SalBase / HorasMax;
                // Parei Aqui falta fazer
                else
                begin
                  Application.MessageBox(PChar('A base de c�lculo ' +
                  IntToStr(DmSal.QrBaseEve_BasCod.Value) +
                  ' n�o tem implementa��o. O evento ' +
                  IntToStr(DmSal.QrBaseEvento.Value) + ' ficar� sem c�lculo!'),
                  'AVISE A DERMATEK!', MB_OK+MB_ICONWARNING);
                end;
              end;}
            end else begin
              if DmSal.QrBaseEve_Meses.Value > 0 then
              begin
                if DmSal.QrBaseEve_Ciclo.Value = 1 then
                  PeriodoI := Geral.Periodo2000(DmSal.QrBaseEve_DtCicl.Value)
                else
                  PeriodoI := Geral.Periodo2000(DmSal.QrFPFunciDataAdm.Value);
                PeriodoT := FPeriodo - PeriodoI;
                Qtde     := Int(PeriodoT / DmSal.QrBaseEve_Meses.Value);
                case DmSal.QrBaseEve_Incid.Value of
                  0: Valor := Fator * Qtde;
                  1: Valor := Round(Fator * Qtde * SalBase) / 100;
                  else Valor := 0;
                end;
                Refere := Qtde;
              end else begin
                case DmSal.QrBaseEve_Incid.Value of
                  0: Valor := Fator;
                  //220 * 562,51 / 100 =
                  1: Valor := Round(Fator * SalBase) / 100;
                  else Valor := 0;
                end;
                Refere := Fator;
              end;
            end;
          end;
          1: // Incid�ncia sobre a conta (do plano de contas)
          begin
            ShowMessage('A��o n�o implementada: FolhaCal 1411');
            // Parei Aqui
            // Falta fazer
          end;
          //else Fator := 0;
        end;
        if Trim(DmSal.QrBaseUnidade.Value) <> '' then
          Unidade := DmSal.QrBaseUnidade.Value;
        if (DmSal.QrBaseProporciHT.Value = 1) then
        begin
          if FTipoCalc = 5 then
          begin
            if (FDdFerias + FDdPecun) < 30 then
              Valor := Valor / 30 * (FDdFerias + FDdPecun);
          end else begin
            if FHorasTrab <> FHorasMax then
              Valor := Valor / FHorasMax * FHorasTrab;
          end;
        end;
        // se a prioridade for maior que 100 (Sal�rio base) ent�o somar ao
        // sal�rio base
        if DmSal.QrBasePrioridade.Value > 100 then
        begin
          SalBase := SalBase + Valor;
          FerBase := FerBase + Valor;
          AdiBase := AdiBase + Valor;
          FamBase := FamBase + Valor;
        end else
          // gerar mesmo quando valor = 0 para poder excluir
          GeraDados(Refere, Valor, 0, Unidade);
        if Valor > 0 then
        begin
          if FNatureza = 'P' then
            BaseCalc := BaseCalc + Valor;
          if DmSal.QrBaseEvento.Value = 498 then
            ValrPrev := ValrPrev + Valor;
        end;
        //
      end;
    end;
    DmSal.QrBase.Next;
  end;
  //
  Result := true;
end;

procedure TFmFPFolhaCal.BtSalvaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSalva, BtSalva);
end;

procedure TFmFPFolhaCal.ClculoAtual1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    SalvaCalculoAtual();
    DmSal.ReopenCalc(DmSal.QrCalcEntidade.Value);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFPFolhaCal.SalvaCalculoAtual();
var
  Codigo: Integer;
begin
  if DmSal.QrCalcCDO_COD.Value > 0 then
  begin
    Codigo := DmSal.QrCalcCDO_COD.Value;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE fpfuncifer SET Calculo = 0 WHERE Calculo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fpfolhacal WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := DmSal.QrCalcCDO_COD.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fpfolhaeve WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := DmSal.QrCalcCDO_COD.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fpfolhaacu WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := DmSal.QrCalcCDO_COD.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fpfolhafer WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := DmSal.QrCalcCDO_COD.Value;
    Dmod.QrUpd.ExecSQL;
    //
  end else Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
  'FPFolhaCal', 'FPFolhaCal', 'Codigo');
  //
  DmSal.QrInsC.Params[00].AsInteger := Codigo;
  DmSal.QrInsC.Params[01].AsInteger := FPeriodo;
  DmSal.QrInsC.Params[02].AsInteger := FEmpreCod;
  DmSal.QrInsC.Params[03].AsInteger := DmSal.QrCalcEntidade.Value;
  DmSal.QrInsC.Params[04].AsInteger := FSemana;
  DmSal.QrInsC.Params[05].AsInteger := FTipoCalc;
  DmSal.QrInsC.Params[06].AsString  := Geral.FDT(TPDataC.Date, 1);
  DmSal.QrInsC.Params[07].AsFloat   := DmSal.QrCalcValorP.Value;
  DmSal.QrInsC.Params[08].AsFloat   := DmSal.QrCalcValorD.Value;
  DmSal.QrInsC.Params[09].AsFloat   := DmSal.QrCalcValorL.Value;
  DmSal.QrInsC.Params[10].AsFloat   := DmSal.QrCalcMediaHE.Value;
  DmSal.QrInsC.Params[11].AsFloat   := DmSal.QrCalcMediaCo.Value;
  DmSal.QrInsC.Params[12].AsFloat   := DmSal.QrCalcMediaAd.Value;
  DmSal.QrInsC.Params[13].AsInteger := 1;
  //
  DmSal.QrInsC.ExecSQL;
  //
  DmSal.QrLEve.Close;
  DmSal.QrLEve.Params[0].AsInteger := DmSal.QrCalcEntidade.Value;
  DmSal.QrLEve.Open;
  while not DmSal.QrLEve.Eof do
  begin
    DmSal.QrInsE.Params[00].AsInteger := Codigo;
    DmSal.QrInsE.Params[01].AsInteger := DmSal.QrLEveEvento.Value;
    DmSal.QrInsE.Params[02].AsFloat   := DmSal.QrLEveReferencia.Value;
    DmSal.QrInsE.Params[03].AsFloat   := DmSal.QrLEveValor.Value;
    DmSal.QrInsE.Params[04].AsInteger := DmSal.QrLEveInfo.Value;
    DmSal.QrInsE.Params[05].AsInteger := DmSal.QrLEveMostra.Value;
    DmSal.QrInsE.Params[06].AsInteger := DmSal.QrLEveAtivo.Value;
    //
    DmSal.QrInsE.ExecSQL;
    //
    DmSal.QrLEve.Next;
  end;
  //
  DmSal.QrLAcu.Close;
  DmSal.QrLAcu.Params[0].AsInteger := DmSal.QrCalcEntidade.Value;
  DmSal.QrLAcu.Open;
  while not DmSal.QrLAcu.Eof do
  begin
    DmSal.QrInsA.Params[00].AsInteger := Codigo;
    DmSal.QrInsA.Params[01].AsInteger := DmSal.QrLAcuEvento.Value;
    DmSal.QrInsA.Params[02].AsInteger := DmSal.QrLAcuAcumulador.Value;
    //
    DmSal.QrInsA.ExecSQL;
    //
    DmSal.QrLAcu.Next;
  end;
  //
  DmSal.QrLFer.Close;
  DmSal.QrLFer.Params[0].AsInteger := DmSal.QrCalcEntidade.Value;
  DmSal.QrLFer.Open;
  while not DmSal.QrLFer.Eof do
  begin
    DmSal.QrInsF.Params[00].AsInteger := Codigo;
    DmSal.QrInsF.Params[01].AsInteger := DmSal.QrLFerItem.Value;
    DmSal.QrInsF.Params[02].AsInteger := DmSal.QrLFerTipo.Value;
    DmSal.QrInsF.Params[03].AsInteger := DmSal.QrLFerAuto.Value;
    DmSal.QrInsF.Params[04].AsFloat   := DmSal.QrLFerReferencia.Value;
    DmSal.QrInsF.Params[05].AsFloat   := DmSal.QrLFerValor.Value;
    //
    DmSal.QrInsF.ExecSQL;
    //
    DmSal.QrLFer.Next;
  end;
  //
  DmSal.QrUpdC.Params[0].AsInteger := DmSal.QrCalcEntidade.Value;
  DmSal.QrUpdC.ExecSQL;
  //
  // F�rias
  if FTipoCalc = 5 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE fpfuncifer SET Calculo=:P0 ');
    Dmod.QrUpd.SQL.Add('WHERE Empresa=:P1 AND Codigo=:P2 ');
    Dmod.QrUpd.SQL.Add('AND Controle=:P3 ');
    //
    Dmod.QrUpd.Params[00].AsInteger := Codigo;
    Dmod.QrUpd.Params[01].AsInteger := FEmpreCod;
    Dmod.QrUpd.Params[02].AsInteger := DmSal.QrCalcEntidade.Value;
    Dmod.QrUpd.Params[03].AsInteger := DmSal.QrCalcFPFunciFer.Value;
    //
    Dmod.QrUpd.ExecSQL;
  end;
end;

procedure TFmFPFolhaCal.Todosclculos1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    DmSal.QrCalc.First;
    while not DmSal.QrCalc.Eof do
    begin
      SalvaCalculoAtual();
      DmSal.QrCalc.Next;
    end;
    DmSal.ReopenCalc(DmSal.QrCalcEntidade.Value);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFPFolhaCal.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmFPFolhaCal.ImprimeItens(Tipo: TSelType);
var
  i, k: Integer;
begin
  if DmSal.QrCdo.RecordCount > 0 then
  begin
    Application.CreateForm(TFmFPFolhaImp, FmFPFolhaImp);
    k := 0;
    if Tipo = istSelecionados then
    begin
      if dmkDBGrid3.SelectedRows.Count > 1 then
      begin
        k := dmkDBGrid3.SelectedRows.Count;
        SetLength(FmFPFolhaImp.FArrCalcs, k);
        with dmkDBGrid3.DataSource.DataSet do
        for i:= 0 to k -1 do
        begin
          GotoBookmark(pointer(dmkDBGrid3.SelectedRows.Items[i]));
          FmFPFolhaImp.FArrCalcs[i] := DmSal.QrCdoCodigo.Value;
        end;
      end else begin
        if DmSal.QrCdo.RecordCount > 0 then
        begin
          k := 1;
          SetLength(FmFPFolhaImp.FArrCalcs, 1);
          FmFPFolhaImp.FArrCalcs[0] := DmSal.QrCdoCodigo.Value;
        end;
      end;
    end else
    if Tipo = istAtual then
    begin
      k := 1;
      SetLength(FmFPFolhaImp.FArrCalcs, 1);
      FmFPFolhaImp.FArrCalcs[0] := DmSal.QrCdoCodigo.Value;
    end else if Tipo = istTodos then
    begin
      k := DmSal.QrCdo.RecordCount;
      SetLength(FmFPFolhaImp.FArrCalcs, k);
      DmSal.QrCdo.First;
      i := 0;
      while not DmSal.QrCdo.Eof do
      begin
        FmFPFolhaImp.FArrCalcs[i] := DmSal.QrCdoCodigo.Value;
        DmSal.QrCdo.Next;
        inc(i, 1);
      end;
    end;
    if k > 0 then
    begin
      FmFPFolhaImp.ReopenFPFolhaCal;
      FmFPFolhaImp.ShowModal;
    end;
    FmFPFolhaImp.Destroy;
  end else Application.MessageBox('Nenhum item foi selecionado!', 'Aviso',
  MB_OK+MB_ICONWARNING);
end;

procedure TFmFPFolhaCal.ItemAtual1Click(Sender: TObject);
begin
  ImprimeItens(istAtual);
end;

procedure TFmFPFolhaCal.ItensSelecionados1Click(Sender: TObject);
begin
  ImprimeItens(istSelecionados);
end;

procedure TFmFPFolhaCal.Todositens1Click(Sender: TObject);
begin
  ImprimeItens(istTodos);
end;

procedure TFmFPFolhaCal.BtIncluirClick(Sender: TObject);
var
  Evento, Eve_Incid: Integer;
begin
  Evento := Geral.IMV(EdEvento.Text);
  if (Evento = 0) then
  begin
    Application.MessageBox('Informe o evento!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdEvento.SetFocus;
    Exit;
  end;
  {
  DmSal.QrUpdL0.SQL.Clear;
  DmSal.QrUpdL0.SQL.Add('INSERT INTO fpcalcbase SET Fonte=:P0, ');
  DmSal.QrUpdL0.SQL.Add('Entidade=:P1, Evento=:P2, Natureza=:P3, ');
  DmSal.QrUpdL0.SQL.Add('Prioridade=:P4, Informacao=:P5, Salvar=:P6, ');
  DmSal.QrUpdL0.SQL.Add('Visivel=:P7, Eve_ValPer=:P8, Eve_Meses=:P9, ');
  DmSal.QrUpdL0.SQL.Add('Eve_Incid=:P10, Conta=:P11, Cta_IncPer=:P12, ');
  DmSal.QrUpdL0.SQL.Add('Cta_MesesR=:P13, Incidencia=:P14, Auto=:P15, ');
  DmSal.QrUpdL0.SQL.Add('Eve_BasCod=:P16, Eve_BasPer=:P17, Referencia=:P18 ');
}
  //
  if DmSal.QrUserEvePercentual.Value > 0 then
    Eve_Incid := MLAGeral.BoolToInt(not CkEve_Incid.Checked)
  else
    Eve_Incid := 0;
  DmSal.QrUpdL0.Params[00].AsInteger := 3; // Adicionado pelo usu�rio
  DmSal.QrUpdL0.Params[01].AsInteger := DmSal.QrCalcEntidade.Value;
  DmSal.QrUpdL0.Params[02].AsInteger := Evento;
  DmSal.QrUpdL0.Params[03].AsString  := DmSal.QrUserEveNatureza.Value;
  DmSal.QrUpdL0.Params[04].AsInteger := DmSal.QrUserEvePrioridade.Value;
  DmSal.QrUpdL0.Params[05].AsInteger := DmSal.QrUserEveInformacao.Value;
  DmSal.QrUpdL0.Params[06].AsInteger := DmSal.QrUserEveSalvar.Value;
  DmSal.QrUpdL0.Params[07].AsInteger := DmSal.QrUserEveVisivel.Value;
  DmSal.QrUpdL0.Params[08].AsFloat   := dmkEdEve_ValPer.ValueVariant;
  DmSal.QrUpdL0.Params[09].AsInteger := 0;
  DmSal.QrUpdL0.Params[10].AsInteger := Eve_Incid;
  DmSal.QrUpdL0.Params[11].AsInteger := 0;
  DmSal.QrUpdL0.Params[12].AsFloat   := 0;
  DmSal.QrUpdL0.Params[13].AsInteger := 0;
  DmSal.QrUpdL0.Params[14].AsInteger := 0;
  DmSal.QrUpdL0.Params[15].AsInteger := 0;
  DmSal.QrUpdL0.Params[16].AsInteger := DmSal.QrUserEveBase.Value;
  DmSal.QrUpdL0.Params[17].AsFloat   := DmSal.QrUserEvePercentual.Value;
  DmSal.QrUpdL0.Params[18].AsFloat   := dmkEdEve_ValPer.ValueVariant;
  DmSal.QrUpdL0.Params[19].AsInteger := 0;
  DmSal.QrUpdL0.Params[20].AsString  := EdUnidade.Text;
  DmSal.QrUpdL0.Params[21].AsInteger := 0;
  DmSal.QrUpdL0.Params[22].AsString  := '';
  DmSal.QrUpdL0.ExecSQL;
  //
  CalculaFuncionarioAtual;
  DmSal.ReopenCalc(DmSal.QrCalcEntidade.Value);
  //
  EdEvento.Text := '';
  CBEvento.KeyValue := NULL;
  dmkEdEve_ValPer.Text := '';
  EdEvento.SetFocus;
end;

procedure TFmFPFolhaCal.SpeedButton1Click(Sender: TObject);
var
  MyCursor : TCursor;
begin
  //if UMyMod.AcessoNegadoAoForm('Perfis', 'Contas', 0) then Exit;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmFPEventGer, FmFPEventGer);
    FmFPEventGer.RGAuto.ItemIndex := 1;
  finally
    Screen.Cursor := MyCursor;
  end;
  FmFPEventGer.ShowModal;
  FmFPEventGer.Destroy;
  //
  // Lookup do DmSal.QrCalcM
  DmSal.QrFPEvent.Close;
  DmSal.QrFPEvent.Open;
  //
  DmSal.QrUserEve.Close;
  DmSal.QrUserEve.Open;
  if DmSal.QrUserEve.Locate('Codigo', VAR_FPEVENT, []) then
  begin
    EdEvento.Text := IntToStr(VAR_FPEVENT);
    CbEvento.KeyValue := VAR_FPEVENT;
  end;
end;

procedure TFmFPFolhaCal.BitBtn2Click(Sender: TObject);
begin
  if DmSal.QrCalcMAuto.Value = 0 then
  begin
    if Application.MessageBox('Confirma a exclus�o do evento seleciondo?!',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Dmod.QrUpdL.SQL.Clear;
      Dmod.QrUpdL.SQL.Add('DELETE FROM fpcalcbase ');
      Dmod.QrUpdL.SQL.Add('WHERE Entidade=:P0 AND Evento=:P1');
      //
      Dmod.QrUpdL.Params[00].AsInteger := DmSal.QrCalcMEntidade.Value;
      Dmod.QrUpdL.Params[01].AsInteger := DmSal.QrCalcMEvento.Value;
      Dmod.QrUpdL.ExecSQL;
      //
      Dmod.QrUpdL.SQL.Clear;
      Dmod.QrUpdL.SQL.Add('DELETE FROM fpcalceven ');
      Dmod.QrUpdL.SQL.Add('WHERE Entidade=:P0 AND Evento=:P1');
      //
      Dmod.QrUpdL.Params[00].AsInteger := DmSal.QrCalcMEntidade.Value;
      Dmod.QrUpdL.Params[01].AsInteger := DmSal.QrCalcMEvento.Value;
      Dmod.QrUpdL.ExecSQL;
      //
      CalculaFuncionarioAtual;
      DmSal.ReopenCalc(DmSal.QrCalcEntidade.Value);
    end;
  end else Application.MessageBox('O evento seleciondo n�o pode ser exclu�do!',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmFPFolhaCal.RedefineParametros;
begin
  QrFPParam.Close;
  QrFPParam.Open;
  QrFPParam.First;
  while not QrFPParam.Eof do
  begin
    case QrFPParamParametro.Value of
      1: FParam_DeduDepe := QrFPParamValor.Value; // Dedu��o por dependente
      2: FParam_ReteMinm := QrFPParamValor.Value; // Reten��o M�nima
      3: FParam_PorcFGTS := QrFPParamValor.Value; // % para FGTS
      4: FParam_VMinIRRF := QrFPParamValor.Value; // Recolhimento M�nimo de IRRF
      // Parei aqui ver se ainda existe (n�o existe mais)
      5: FParam_NaoSimFG := QrFPParamValor.Value; // % FGTS (N�o Optante Simples)
      6: FParam_DeduAdIR := QrFPParamValor.Value; // Dedu��o IRRF Adicional
    end;
    QrFPParam.Next;
  end;
end;

procedure TFmFPFolhaCal.dmkEdEve_ValPerChange(Sender: TObject);
begin
  VerificaBtIncluir;
end;

procedure TFmFPFolhaCal.VerificaBtIncluir;
begin
  BtIncluir.Enabled :=
    (Geral.IMV(EdEvento.Text) > 0) and (dmkEdEve_ValPer.ValueVariant > 0);
end;

procedure TFmFPFolhaCal.DefineHorasMax;
begin
  DmSal.QrFPFunci.Close;
  DmSal.QrFPFunci.Params[0].AsInteger := DmSal.QrCalcEntidade.Value;
  DmSal.QrFPFunci.Open;
  if (DmSal.QrFPFunciHrSeman.Value = 0) and (FTipoCalc <> 8)then
    Application.MessageBox(PChar('Horas semanais n�o definadas para o ' +
    'funcion�rio '+ IntToStr(DmSal.QrCalcEntidade.Value) + '!'), 'Aviso',
    MB_OK+MB_ICONWARNING);
  if DmSal.QrFPFunciProfessor.Value = 1 then
    FHorasMax := 4.5 * DmSal.QrFPFunciHrSeman.Value
  else
    FHorasMax := 5.0 * DmSal.QrFPFunciHrSeman.Value;
end;

procedure TFmFPFolhaCal.BitBtn3Click(Sender: TObject);
begin
  DmSal.ExcluiCalculoFolha(DmSal.QrCDOCodigo.Value);
end;

procedure TFmFPFolhaCal.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  if dmkEdPeriodo.Focused then
  begin
    if WheelDelta < 0 then
      dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant - 1
    else
      dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant + 1;
  end;
end;

Procedure TFmFPFolhaCal.DefineItensDiversos(SalBase: Double);
var
  PriDdMes, UltDdMes, UltDdFerMes, PriDdFerMes: TDateTime;
begin
  // Calcula dias f�rias, dias abono e horas trabalhadas
  //
  // Verifica se inicia f�rias no m�s
  ReopenIniFer;
  //
  if DmSal.QrIniFer.RecordCount > 0 then
  begin
    FTemFerias := 1;
    FCodCalFer := DmSal.QrIniFerCalculo.Value;
    FDdFerias := DmSal.QrIniFerDiasGozar.Value;
    FDdPecun  := DmSal.QrIniFerDiasPecun.Value;

    // Parei Aqui descontar feriados ?
    UltDdMes := MLAGeral.UltimoDiaDoMes(DmSal.QrIniFerDtIniPG.Value);
    if Round(DmSal.QrIniFerDtFimPG.Value) < Round(UltDdMes) then
      UltDdFerMes := DmSal.QrIniFerDtFimPG.Value
    else
      UltDdFerMes := UltDdMes;

    FDdFerMes := Round(UltDdFerMes) - Round(DmSal.QrIniFerDtIniPG.Value) + 1;
    FHorasTrab := Round((30 - FDdFerMes) / 30 * 100 * FHorasMax) / 100;
    FSalTrab   := Round(FHorasTrab * SalBase * 100 / FHorasMax) / 100;
    FDdFerAbo := DmSal.QrIniFerDiasPecun.Value;
  end else begin
    //  Verifica se existe f�rias durante todo m�s
    //  no case de f�rias de 30 dias que come�a 31 de janeiro
    //  e vai a 01 de mar�o
    DmSal.QrEmFer.Close;
    DmSal.QrEmFer.Params[0].AsInteger := FEmpreCod;
    DmSal.QrEmFer.Params[1].AsInteger := DmSal.QrCalcEntidade.Value;
    DmSal.QrEmFer.Params[2].AsString  := FDataIni;
    DmSal.QrEmFer.Params[3].AsString  := FDataFim;
    DmSal.QrEmFer.Open;
    //
    if DmSal.QrEmFer.RecordCount > 0 then
    begin
      FTemFerias := 2;
      FCodCalFer := DmSal.QrEmFerCalculo.Value;
      FDdFerias  := DmSal.QrEmFerDiasGozar.Value;
      FDdPecun   := DmSal.QrEmFerDiasPecun.Value;

      // N�o teve dias trabalhados
      FHorasTrab := 0;
      FSalTrab   := 0;
    end else begin


      //  Verifica se as f�rias terminam no m�s
      DmSal.QrFimFer.Close;
      DmSal.QrFimFer.Params[0].AsInteger := FEmpreCod;
      DmSal.QrFimFer.Params[1].AsInteger := DmSal.QrCalcEntidade.Value;
      DmSal.QrFimFer.Params[2].AsString  := FDataIni;
      DmSal.QrFimFer.Params[3].AsString  := FDataFim;
      DmSal.QrFimFer.Open;
      //
      if DmSal.QrFimFer.RecordCount > 0 then
      begin
        FTemFerias := 3;
        FCodCalFer := DmSal.QrFimFerCalculo.Value;
        FDdFerias  := DmSal.QrFimFerDiasGozar.Value;
        FDdPecun   := DmSal.QrFimFerDiasPecun.Value;

        // Parei Aqui descontar feriados ?
        PriDdMes := MLAGeral.PrimeiroDiaDoMes(DmSal.QrFimFerDtFimPG.Value);
        if Round(DmSal.QrFimFerDtFimPG.Value) < Round(PriDdMes) then
          PriDdFerMes := DmSal.QrFimFerDtFimPG.Value
        else
          PriDdFerMes := PriDdMes;

        FDdFerMes := Round(DmSal.QrFimFerDtFimPG.Value) - Round(PriDdFerMes) + 1;
        FHorasTrab := Round((30 - FDdFerMes) / 30 * 100 * FHorasMax) / 100;
        FSalTrab   := Round(FHorasTrab * SalBase * 100 / FHorasMax) / 100;
        // N�o precisa
        //FDdFerAbo := DmSal.QrFimFerDiasPecun.Value;
      end else begin
        FTemFerias := 0;
        FHorasTrab := FHorasMax;
        FSalTrab   := SalBase;
      end;
    end;
  end;
  // Fim c�lculo dias f�rias, dias abono e horas trabalhadas
end;

procedure TFmFPFolhaCal.ReopenIniFer;
begin
  DmSal.QrIniFer.Close;
  DmSal.QrIniFer.Params[0].AsInteger := FEmpreCod;
  DmSal.QrIniFer.Params[1].AsInteger := DmSal.QrCalcEntidade.Value;
  DmSal.QrIniFer.Params[2].AsString  := FDataIni;
  DmSal.QrIniFer.Params[3].AsString  := FDataFim;
  DmSal.QrIniFer.Open;
end;

// Parei Aqui - Fazer pro labore
// Adicional noturno
// DSR
// Parei Aqui - Sal�rio Fam�lia nas f�rias
// Parei Aqui - Fazer sal�rio categoria para insalubridade
// Parei Aqui - Agendamento de eventos
// Exporta�ao das tabelas FP
// colocar unidade nos itens de 100 a 500
// colocar unidade na impress�o do holerit



// DSR
//Para funcion�rios mensalista a DSR j� esta incluida na remunera��o do mesmo.
//E o anu�nio beneficio concedidos por alguns sindicatos aos profissionais
//de sua categoria n�o tem incid�ncia de DRS, assim para esta situa��o
//a menos que o funcion�rio tenha trabalhado no feriado(hora extra a 100%+DRS)
//a folha de pagamento s� constar� o salario 380,00+ anuenio 3,80
//dando um total de 383,80(bruto).

//varia 9 n�o coloca o anuenio
//deve ir no calculo do usuario
procedure TFmFPFolhaCal.Button1Click(Sender: TObject);
begin
  LimpaTabelasLocais(True, True);
end;

end.

