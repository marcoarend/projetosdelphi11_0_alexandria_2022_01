unit FPEvent;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral,  
      UnGOTOy,
   UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa,  
     
     LMDDBEdit;

type
  TFmCNAB400CaG = class(TForm)
    PainelDados: TPanel;
    DsCNAB400CaG: TDataSource;
    QrCNAB400CaG: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill002: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    PainelControle: TPanel;
    LaRegistro: TLMDLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TLMDEdit;
    EdNome: TEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TLMDDBEdit;
    DBEdNome: TLMDDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrCNAB400CaGCodigo: TIntegerField;
    QrCNAB400CaGNome: TWideStringField;
    QrCNAB400CaGLk: TIntegerField;
    QrCNAB400CaGDataCad: TDateField;
    QrCNAB400CaGDataAlt: TDateField;
    QrCNAB400CaGUserCad: TIntegerField;
    QrCNAB400CaGUserAlt: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCNAB400CaGAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrCNAB400CaGAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCNAB400CaGBeforeOpen(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmCNAB400CaG: TFmCNAB400CaG;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCNAB400CaG.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCNAB400CaG.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCNAB400CaGCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCNAB400CaG.DefParams;
begin
  VAR_GOTOTABELA := 'CNAB400CaG';
  VAR_GOTOMYSQLTABLE := QrCNAB400CaG;
  VAR_GOTONEG := True;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM cnab400cag');
  VAR_SQLx.Add('WHERE Codigo <> 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCNAB400CaG.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      EdNome.Text := CO_VAZIO;
      EdNome.Visible := True;
      PainelControle.Visible:=False;
      if Status = CO_INCLUSAO then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmCNAB400CaG.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCNAB400CaG.AlteraRegistro;
var
  CNAB400CaG : Integer;
begin
  CNAB400CaG := QrCNAB400CaGCodigo.Value;
  if QrCNAB400CaGCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(CNAB400CaG, Dmod.MyDB, 'CNAB400CaG', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(CNAB400CaG, Dmod.MyDB, 'CNAB400CaG', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCNAB400CaG.IncluiRegistro;
var
  Cursor : TCursor;
  CNAB400CaG : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    CNAB400CaG := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'CNAB400CaG', 'CNAB400CaG', 'Codigo');
    if Length(FormatFloat(FFormatFloat, CNAB400CaG))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, CO_INCLUSAO, CNAB400CaG);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmCNAB400CaG.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCNAB400CaG.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCNAB400CaG.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCNAB400CaG.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCNAB400CaG.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCNAB400CaG.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCNAB400CaG.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmCNAB400CaG.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmCNAB400CaG.BtSaidaClick(Sender: TObject);
begin
  VAR_INSTITUICAO := QrCNAB400CaGCodigo.Value;
  Close;
end;

procedure TFmCNAB400CaG.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('INSERT INTO cnab400cag SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE cnab400cag SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0,');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Codigo=:Pc');
  //
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  //
  Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[02].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[03].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CNAB400CaG', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmCNAB400CaG.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'CNAB400CaG', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CNAB400CaG', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CNAB400CaG', 'Codigo');
end;

procedure TFmCNAB400CaG.FormCreate(Sender: TObject);
begin
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmCNAB400CaG.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCNAB400CaGCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCNAB400CaG.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCNAB400CaG.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmCNAB400CaG.QrCNAB400CaGAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCNAB400CaG.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'CNAB400CaG', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmCNAB400CaG.QrCNAB400CaGAfterScroll(DataSet: TDataSet);
begin
  //BtAltera.Enabled := GOTOy.BtEnabled(QrCNAB400CaGCodigo.Value, False);
end;

procedure TFmCNAB400CaG.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCNAB400CaGCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'CNAB400CaG', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCNAB400CaG.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmCNAB400CaG.QrCNAB400CaGBeforeOpen(DataSet: TDataSet);
begin
  QrCNAB400CaGCodigo.DisplayFormat := FFormatFloat;
end;

end.

