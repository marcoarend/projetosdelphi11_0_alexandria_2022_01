unit FPFolha;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral;

type
  TFmFPFolha = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmFPFolha: TFmFPFolha;

implementation

uses FPFolhaCal;

{$R *.DFM}

procedure TFmFPFolha.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPFolha.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPFolha.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFPFolha.BtOKClick(Sender: TObject);
var
  MyCursor : TCursor;
begin
  //if UMyMod.AcessoNegadoAoForm('Perfis', 'Contas', 0) then Exit;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmFPFolhaCal, FmFPFolhaCal);
  finally
    Screen.Cursor := MyCursor;
  end;
  FmFPFolhaCal.ShowModal;
  FmFPFolhaCal.Destroy;
end;

end.
