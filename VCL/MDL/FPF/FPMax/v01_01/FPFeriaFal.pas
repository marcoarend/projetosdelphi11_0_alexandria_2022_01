unit FPFeriaFal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Grids, DBGrids,
  dmkDBGridDAC, Db, mySQLDbTables, dmkDBGrid, Menus;

type
  TFmFPFeraiFal = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DsFPFeriaFal: TDataSource;
    QrFPFeriaFal: TmySQLQuery;
    dmkDBGridDAC1: TdmkDBGridDAC;
    BtAcao: TBitBtn;
    PMAcao: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui1: TMenuItem;
    QrFPFeriaFalLimite: TSmallintField;
    QrFPFeriaFalDescricao: TWideStringField;
    QrFPFeriaFalDias: TSmallintField;
    QrFPFeriaFalAtivo: TSmallintField;
    QrFPFeriaFalLk: TIntegerField;
    QrFPFeriaFalDataCad: TDateField;
    QrFPFeriaFalDataAlt: TDateField;
    QrFPFeriaFalUserCad: TIntegerField;
    QrFPFeriaFalUserAlt: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure dmkDBGridDAC1CellClick(Column: TColumn);
  private
    { Private declarations }
    procedure ReopenQrFPFeriaFal(Limite: Double);
  public
    { Public declarations }
  end;

  var
  FmFPFeraiFal: TFmFPFeraiFal;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmFPFeraiFal.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPFeraiFal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPFeraiFal.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFPFeraiFal.FormCreate(Sender: TObject);
begin
  ReopenQrFPFeriaFal(0);
end;

procedure TFmFPFeraiFal.Inclui1Click(Sender: TObject);
var
  Limite: String;
  CodVal: Integer;
begin
  Limite := '0';
  if InputQuery('Novo limite de faltas injustificadas', 'Informe o limite de faltas:',
  Limite) then
  begin
    CodVal := Geral.IMV(Limite);
    if CodVal > 366 then CodVal := 366;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO fpferiafal SET Ativo=1, Limite=:P0');
    Dmod.QrUpd.Params[0].AsInteger := CodVal;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPFeriaFal(CodVal);
  end;
end;

procedure TFmFPFeraiFal.ReopenQrFPFeriaFal(Limite: Double);
begin
  QrFPFeriaFal.Close;
  QrFPFeriaFal.Open;
  QrFPFeriaFal.Locate('Limite', Limite, []);
end;

procedure TFmFPFeraiFal.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmFPFeraiFal.Exclui1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o do registro selecionado?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fpferiafal WHERE Limite=:P0');
    Dmod.QrUpd.Params[00].AsInteger := QrFPFeriaFalLimite.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPFeriaFal(0);
  end;
end;

procedure TFmFPFeraiFal.dmkDBGridDAC1CellClick(Column: TColumn);
var
  Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if QrFPFeriaFalAtivo.Value = 1 then Ativo := 0 else Ativo := 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE fpferiafal SET Ativo=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Limite=:P1');
    //
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsFloat   := QrFPFeriaFalLimite.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPFeriaFal(QrFPFeriaFalLimite.Value);
  end;
end;

end.

