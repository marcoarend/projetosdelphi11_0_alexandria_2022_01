unit FPAcumu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Grids, DBGrids,
  dmkDBGridDAC, Db, mySQLDbTables, dmkDBGrid, Menus;

type
  TFmFPAcumu = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DsFPAcumu: TDataSource;
    QrFPAcumu: TmySQLQuery;
    dmkDBGridDAC1: TdmkDBGridDAC;
    BtAcao: TBitBtn;
    PMAcao: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui1: TMenuItem;
    QrFPAcumuCodigo: TIntegerField;
    QrFPAcumuDescricao: TWideStringField;
    QrFPAcumuChave: TWideStringField;
    QrFPAcumuAtivo: TSmallintField;
    QrFPAcumuVisivel: TSmallintField;
    QrFPAcumuRegimeCxa: TSmallintField;
    QrFPAcumuLk: TIntegerField;
    QrFPAcumuDataCad: TDateField;
    QrFPAcumuDataAlt: TDateField;
    QrFPAcumuUserCad: TIntegerField;
    QrFPAcumuUserAlt: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure dmkDBGridDAC1CellClick(Column: TColumn);
  private
    { Private declarations }
    procedure ReopenQrFPAcumu(Codigo: Double);
  public
    { Public declarations }
  end;

  var
  FmFPAcumu: TFmFPAcumu;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmFPAcumu.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPAcumu.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPAcumu.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFPAcumu.FormCreate(Sender: TObject);
begin
  ReopenQrFPAcumu(0);
end;

procedure TFmFPAcumu.Inclui1Click(Sender: TObject);
var
  Codigo: String;
  CodVal: Integer;
begin
  Codigo := '0';
  if InputQuery('Novo item de acumulador', 'Informe o c�digo do acumulador:',
  Codigo) then
  begin
    CodVal := Geral.IMV(Codigo);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO fpacumu SET Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := CodVal;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPAcumu(CodVal);
  end;
end;

procedure TFmFPAcumu.ReopenQrFPAcumu(Codigo: Double);
begin
  QrFPAcumu.Close;
  QrFPAcumu.Open;
  QrFPAcumu.Locate('Codigo', Codigo, []);
end;

procedure TFmFPAcumu.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmFPAcumu.Exclui1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o do registro selecionado?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fpacumu WHERE Codigo=:P0');
    Dmod.QrUpd.Params[00].AsInteger := QrFPAcumuCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPAcumu(Int(Date));
  end;
end;

procedure TFmFPAcumu.dmkDBGridDAC1CellClick(Column: TColumn);
var
  Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if QrFPAcumuAtivo.Value = 1 then Ativo := 0 else Ativo := 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE fpacumu SET Ativo=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
    //
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsFloat   := QrFPAcumuCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrFPAcumu(QrFPAcumuCodigo.Value);
  end;
end;

end.

