unit ModuleSal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables;

type
  TDmSal = class(TDataModule)
    QrEmpresas: TmySQLQuery;
    QrEmpresasCodigo: TIntegerField;
    QrEmpresasNOMEEMPRESA: TWideStringField;
    DsEmpresas: TDataSource;
    QrDepto: TmySQLQuery;
    QrDeptoCodID: TWideStringField;
    QrDeptoDescricao: TWideStringField;
    DsDepto: TDataSource;
    QrFunci: TmySQLQuery;
    QrFunciNOMEFUNCI: TWideStringField;
    QrFunciCodigo: TIntegerField;
    DsFunci: TDataSource;
    QrFPtpCal: TmySQLQuery;
    QrFPtpCalCodigo: TSmallintField;
    QrFPtpCalDescricao: TWideStringField;
    DsFPtpCal: TDataSource;
    QrFPC: TmySQLQuery;
    DsFPC: TDataSource;
    QrCalc: TmySQLQuery;
    QrTpCalPer: TmySQLQuery;
    DsTpCalPer: TDataSource;
    QrEventip: TmySQLQuery;
    QrEventipCodigo: TIntegerField;
    QrEventipAtivo: TSmallintField;
    QrEventipNatureza: TWideStringField;
    QrEventipBase: TIntegerField;
    QrEventipAuto: TSmallintField;
    QrEventipPercentual: TFloatField;
    QrEventipPrioridade: TSmallintField;
    QrEventipVisivel: TSmallintField;
    QrEventipInformacao: TSmallintField;
    QrEventipSalvar: TSmallintField;
    QrFPFunci: TmySQLQuery;
    QrFPFunciHrSeman: TIntegerField;
    QrFPFunciTipoSal: TSmallintField;
    QrFPFunciSalario: TFloatField;
    QrFPFunciProfessor: TSmallintField;
    QrUpdL1: TmySQLQuery;
    QrUpdL2: TmySQLQuery;
    QrFPFunciDependent: TIntegerField;
    QrPesqIRRF: TmySQLQuery;
    QrPesqIRRFAliquota: TFloatField;
    QrPesqIRRFDeducao: TFloatField;
    QrEvnAcum: TmySQLQuery;
    QrEvnAcumEvent: TIntegerField;
    QrEvnAcumAcumu: TIntegerField;
    QrSalMin: TmySQLQuery;
    QrSalMinValor: TFloatField;
    QrSalMinDataV: TDateField;
    DsCalc: TDataSource;
    QrFunciFunci: TIntegerField;
    QrFunciRegistro: TWideStringField;
    QrFPCFPFolhaCal: TIntegerField;
    QrFPCEntidade: TIntegerField;
    QrFPCNomeEnti: TWideStringField;
    QrFPCCodFunci: TIntegerField;
    QrFPCCalcula: TSmallintField;
    QrCalcFPFolhaCal: TIntegerField;
    QrCalcEntidade: TIntegerField;
    QrCalcNomeEnti: TWideStringField;
    QrCalcCodFunci: TIntegerField;
    QrCalcCalcula: TSmallintField;
    QrCalcM: TmySQLQuery;
    QrFPEvent: TmySQLQuery;
    QrFPEventCodigo: TIntegerField;
    QrFPEventDescricao: TWideStringField;
    QrCalcMEntidade: TIntegerField;
    QrCalcMEvento: TIntegerField;
    QrCalcMReferencia: TFloatField;
    QrCalcMValor: TFloatField;
    QrCalcMAlteracao: TSmallintField;
    QrCalcMInfo: TSmallintField;
    QrCalcMSalva: TSmallintField;
    QrCalcMMostra: TSmallintField;
    QrCalcMAtivo: TSmallintField;
    QrCalcMNOMEEVENTO: TWideStringField;
    DsCalcM: TDataSource;
    QrCalcMValorP: TFloatField;
    QrCalcMValorD: TFloatField;
    QrCalcSumM: TmySQLQuery;
    DsCalcSumM: TDataSource;
    QrCalcSumMEntidade: TIntegerField;
    QrCalcSumMValorP: TFloatField;
    QrCalcSumMValorD: TFloatField;
    QrCalcValorP: TFloatField;
    QrCalcValorD: TFloatField;
    QrCalcSumMValorL: TFloatField;
    QrCalcValorL: TFloatField;
    QrCalcI: TmySQLQuery;
    DsCalcI: TDataSource;
    QrCalcIEntidade: TIntegerField;
    QrCalcIEvento: TIntegerField;
    QrCalcIReferencia: TFloatField;
    QrCalcIValor: TFloatField;
    QrCalcIAlteracao: TSmallintField;
    QrCalcIInfo: TSmallintField;
    QrCalcISalva: TSmallintField;
    QrCalcIMostra: TSmallintField;
    QrCalcIAtivo: TSmallintField;
    QrCalcINOMEEVENTO: TWideStringField;
    QrCdo: TmySQLQuery;
    QrCdoNOMEFUNCI: TWideStringField;
    QrCdoDataC: TDateField;
    QrCdoValorP: TFloatField;
    QrCdoValorD: TFloatField;
    QrCdoValorL: TFloatField;
    DsCdo: TDataSource;
    QrCdoEntidade: TIntegerField;
    QrFunciCDO_FUNCI: TIntegerField;
    QrCalcCDO_FUNCI: TIntegerField;
    QrCalcTemCalc: TSmallintField;
    QrCalcSalvou: TSmallintField;
    QrCdoCodigo: TIntegerField;
    QrCalcCDO_COD: TIntegerField;
    QrInsC: TmySQLQuery;
    QrInsA: TmySQLQuery;
    QrInsE: TmySQLQuery;
    QrLEve: TmySQLQuery;
    QrLEveEntidade: TIntegerField;
    QrLEveEvento: TIntegerField;
    QrLEveReferencia: TFloatField;
    QrLEveValor: TFloatField;
    QrLEveAlteracao: TSmallintField;
    QrLEveInfo: TSmallintField;
    QrLEveSalva: TSmallintField;
    QrLEveMostra: TSmallintField;
    QrLEveAtivo: TSmallintField;
    QrLAcu: TmySQLQuery;
    QrLAcuEntidade: TIntegerField;
    QrLAcuEvento: TIntegerField;
    QrLAcuAcumulador: TIntegerField;
    QrUpdC: TmySQLQuery;
    QrFPFunciInsalubrid: TFloatField;
    QrFPFunciPericulosi: TFloatField;
    QrTpCalPerControle: TSmallintField;
    QrTpCalPerTipoCalc: TSmallintField;
    QrTpCalPerCategoria: TSmallintField;
    QrTpCalPerSubPer: TSmallintField;
    QrTpCalPerDescricao: TWideStringField;
    QrFunEve: TmySQLQuery;
    QrFPFunciDataAdm: TDateField;
    QrFunEveIncidencia: TSmallintField;
    QrFunEveEvento: TIntegerField;
    QrFunEveEve_Incid: TSmallintField;
    QrFunEveEve_ValPer: TFloatField;
    QrFunEveEve_Meses: TSmallintField;
    QrFunEveConta: TIntegerField;
    QrFunEveCta_IncPer: TFloatField;
    QrFunEveCta_MesesR: TSmallintField;
    QrFunEveNatureza: TWideStringField;
    QrFunEveBase: TIntegerField;
    QrFunEvePercentual: TFloatField;
    QrFunEveVisivel: TSmallintField;
    QrFunEveInformacao: TSmallintField;
    QrFunEveSalvar: TSmallintField;
    QrSalPago: TmySQLQuery;
    QrSalPagoValorL: TFloatField;
    QrUserEve: TmySQLQuery;
    QrUserEveCodigo: TIntegerField;
    QrUserEveDescricao: TWideStringField;
    DsUserEve: TDataSource;
    QrFunEvePrioridade: TSmallintField;
    QrBase: TmySQLQuery;
    QrBaseEntidade: TIntegerField;
    QrBaseEvento: TIntegerField;
    QrBaseEve_ValPer: TFloatField;
    QrBaseEve_Meses: TSmallintField;
    QrBaseEve_Incid: TSmallintField;
    QrBaseConta: TIntegerField;
    QrBaseCta_IncPer: TFloatField;
    QrBaseCta_MesesR: TSmallintField;
    QrBaseNatureza: TWideStringField;
    QrBaseReferencia: TFloatField;
    QrBaseValor: TFloatField;
    QrBaseIncidencia: TSmallintField;
    QrBaseAlteracao: TSmallintField;
    QrBaseInformacao: TSmallintField;
    QrBaseSalvar: TSmallintField;
    QrBaseVisivel: TSmallintField;
    QrBasePrioridade: TIntegerField;
    QrBaseFonte: TSmallintField;
    QrBaseAtivo: TSmallintField;
    QrUpdL0: TmySQLQuery;
    QrUserEveNatureza: TWideStringField;
    QrUserEvePrioridade: TSmallintField;
    QrUserEveInformacao: TSmallintField;
    QrUserEveSalvar: TSmallintField;
    QrUserEveVisivel: TSmallintField;
    QrBaseAuto: TSmallintField;
    QrCalcMPrioridade: TIntegerField;
    QrCalcMAuto: TSmallintField;
    QrFunEveAuto: TSmallintField;
    QrSalFam: TmySQLQuery;
    QrSalFamValor: TFloatField;
    QrFPFunciFilhos: TIntegerField;
    QrINSS: TmySQLQuery;
    QrINSSAliquota: TFloatField;
    QrINSSDeducao: TFloatField;
    QrINSSFaixa: TFloatField;
    QrFPFunciPensaoAlm: TFloatField;
    QrIRRF: TmySQLQuery;
    QrIRRFTeto: TFloatField;
    QrIRRFAliquota: TFloatField;
    QrIRRFDeducao: TFloatField;
    QrFPFunciPensaoLiq: TSmallintField;
    QrFPFunciComoCalcIR: TSmallintField;
    QrIRPg: TmySQLQuery;
    QrIRPgValor: TFloatField;
    QrUserEvePercentual: TFloatField;
    QrUserEveBase: TIntegerField;
    QrBaseEve_BasCod: TIntegerField;
    QrBaseEve_BasPer: TFloatField;
    QrBasePercentual: TFloatField;
    QrIniFer: TmySQLQuery;
    QrHrE: TmySQLQuery;
    QrHrEHoras: TFloatField;
    QrHrEFatHr: TFloatField;
    Qrxxx: TmySQLQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    SmallintField1: TSmallintField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    DateField1: TDateField;
    DateField2: TDateField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    SmallintField2: TSmallintField;
    SmallintField3: TSmallintField;
    DateField3: TDateField;
    DateField4: TDateField;
    DateField5: TDateField;
    DateField6: TDateField;
    DateField7: TDateField;
    DateField8: TDateField;
    DateField9: TDateField;
    DateField10: TDateField;
    IntegerField6: TIntegerField;
    DateField11: TDateField;
    DateField12: TDateField;
    IntegerField7: TIntegerField;
    IntegerField8: TIntegerField;
    DateField13: TDateField;
    DateField14: TDateField;
    IntegerField9: TIntegerField;
    QrHrEFator: TFloatField;
    QrVVF: TmySQLQuery;
    QrVVFValor: TFloatField;
    QrSVI: TmySQLQuery;
    QrEVI: TmySQLQuery;
    QrEVIEvent: TIntegerField;
    QrSVIValor: TFloatField;
    QrInsF: TmySQLQuery;
    QrLFer: TmySQLQuery;
    QrLFerEntidade: TIntegerField;
    QrLFerItem: TIntegerField;
    QrLFerTipo: TIntegerField;
    QrLFerAuto: TIntegerField;
    QrLFerReferencia: TFloatField;
    QrLFerValor: TFloatField;
    QrLFerAtivo: TSmallintField;
    QrCalcFPFunciFer: TIntegerField;
    QrCalcMediaHE: TFloatField;
    QrCalcMediaCo: TFloatField;
    QrCalcMediaAd: TFloatField;
    QrEvent: TmySQLQuery;
    QrEventCodigo: TIntegerField;
    QrEventDescricao: TWideStringField;
    QrCalcMUnidade: TWideStringField;
    QrFunEveProporciHT: TSmallintField;
    QrBaseProporciHT: TSmallintField;
    QrEmFer: TmySQLQuery;
    QrFimFer: TmySQLQuery;
    QrIniFerDiasGozar: TSmallintField;
    QrIniFerDiasPecun: TSmallintField;
    QrIniFerControle: TIntegerField;
    QrIniFerMeHEMan200: TFloatField;
    QrIniFerDtIniPA: TDateField;
    QrIniFerDtFimPA: TDateField;
    QrIniFerMediaCoMan: TFloatField;
    QrIniFerMediaAdMan: TFloatField;
    QrIniFerDtIniPG: TDateField;
    QrIniFerDtFimPG: TDateField;
    QrFimFerDtIniPG: TDateField;
    QrFimFerDtFimPG: TDateField;
    QrIniFerCalculo: TIntegerField;
    QrEmFerCalculo: TIntegerField;
    QrFimFerCalculo: TIntegerField;
    QrItsCal: TmySQLQuery;
    QrItsCalCodigo: TIntegerField;
    QrItsCalEvento: TIntegerField;
    QrItsCalReferencia: TFloatField;
    QrItsCalValor: TFloatField;
    QrItsCalInfo: TSmallintField;
    QrItsCalMostra: TSmallintField;
    QrItsCalAtivo: TSmallintField;
    QrEvePes: TmySQLQuery;
    QrEvePesVisivel: TSmallintField;
    QrEvePesInformacao: TSmallintField;
    QrEvePesSalvar: TSmallintField;
    QrEvePesCodigo: TIntegerField;
    QrEvePesNatureza: TWideStringField;
    QrEvePesAuto: TSmallintField;
    QrEvePesPrioridade: TSmallintField;
    QrEmFerDiasGozar: TSmallintField;
    QrEmFerDiasPecun: TSmallintField;
    QrFimFerDiasGozar: TSmallintField;
    QrFimFerDiasPecun: TSmallintField;
    QrIniFerMeHEMan1xx: TFloatField;
    QrIniFerHEFator: TFloatField;
    QrBaseUnidade: TWideStringField;
    QrFunEveUnidade: TWideStringField;
    QrFunEveEve_Ciclo: TSmallintField;
    QrFunEveEve_DtCicl: TDateField;
    QrBaseEve_Ciclo: TIntegerField;
    QrBaseEve_DtCicl: TDateField;
    QrCond: TmySQLQuery;
    QrCondProLaINSSr: TFloatField;
    QrCondProLaINSSp: TFloatField;
    procedure QrCalcMCalcFields(DataSet: TDataSet);
    procedure QrCalcAfterScroll(DataSet: TDataSet);
    procedure QrCalcCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReopenTpCalPer(TipoCalc: Integer);
    procedure ReopenEventip(TipoCalc: Integer);
    procedure ReopenCalcIts;
    function  ExcluiCalculoFolha(Codigo: Integer): Boolean;
    function  ReopenCalc(Entidade: Integer): Integer;
    function  ReopenCdo(Periodo, Empresa, TipoCalc, Semana: Integer): Integer;
    function  ReopenFunEve(Entidade, TipoCalc: Integer): Integer;
    function  EventosDeVariavelDeIncidencia(Variavel: Integer): String;
    function  SomaValoresDeIncidencia(Variavel, Entidade: Integer): Double;
  end;

var
  DmSal: TDmSal;

implementation

uses UnMyObjects, Module, UnMLAGeral;

{$R *.DFM}

procedure TDmSal.ReopenTpCalPer(TipoCalc: Integer);
begin
  QrTpCalPer.Close;
  QrTpCalPer.Params[0].AsInteger := TipoCalc;
  QrTpCalPer.Open;
end;

procedure TDmSal.ReopenEventip(TipoCalc: Integer);
begin
  QrEventip.Close;
  QrEventip.Params[0].AsInteger := TipoCalc;
  QrEventip.Open;
end;

procedure TDmSal.QrCalcMCalcFields(DataSet: TDataSet);
begin
  if QrCalcMValor.Value > 0 then
    QrCalcMValorP.Value := QrCalcMValor.Value
  else
    QrCalcMValorP.Value := 0;
  //
  if QrCalcMValor.Value < 0 then
    QrCalcMValorD.Value := -QrCalcMValor.Value
  else
    QrCalcMValorD.Value := 0;
  //
end;

procedure TDmSal.QrCalcAfterScroll(DataSet: TDataSet);
begin
  ReopenCalcIts;
end;

function TDmSal.ReopenCalc(Entidade: Integer): Integer;
begin
  //
  QrCalcSumM.Close;
  QrCalcSumM.Open;
  //
  QrCdo.Close;
  QrCdo.Open;
  //
  QrCalc.Close;
  QrCalc.Open;
  Result := QrCalc.RecordCount;
  //
  if Entidade > 0 then
    QrCalc.Locate('Entidade', Entidade, []);
end;

procedure TDmSal.ReopenCalcIts();
begin
  QrCalcM.Close;
  QrCalcM.Params[0].AsInteger := QrCalcEntidade.Value;
  QrCalcM.Open;
  //
  QrCalcI.Close;
  QrCalcI.Params[0].AsInteger := QrCalcEntidade.Value;
  QrCalcI.Open;
  //
end;

function TDmSal.ReopenCdo(Periodo, Empresa, TipoCalc, Semana: Integer): Integer;
begin
  QrCdo.Close;
  QrCdo.Params[00].AsInteger := Periodo;
  QrCdo.Params[01].AsInteger := Empresa;
  QrCdo.Params[02].AsInteger := TipoCalc;
  QrCdo.Params[03].AsInteger := Semana;
  QrCdo.Open;
  //
  Result := QrCdo.RecordCount;
end;

procedure TDmSal.QrCalcCalcFields(DataSet: TDataSet);
begin
  QrCalcTemCalc.Value := MLAGeral.BoolToInt(QrCalcCDO_FUNCI.Value <> 0);
end;

function TDmSal.ReopenFunEve(Entidade, TipoCalc: Integer): Integer;
begin
  QrFunEve.Close;
  QrFunEve.Params[00].AsInteger := Entidade;
  QrFunEve.Params[01].AsInteger := TipoCalc;
  QrFunEve.Open;
  //
  Result := QrFunEve.RecordCount;
end;

function TDmSal.EventosDeVariavelDeIncidencia(Variavel: Integer): String;
var
  EvenVar: String;
begin
  QrEVI.Close;
  QrEVI.Params[0].AsInteger := Variavel;
  QrEVI.Open;
  //
  EvenVar := FormatFloat('0', QrEVIEvent.Value);
  QrEVI.Next;
  while not QrEVI.Eof do
  begin
    EvenVar := EvenVar + ', ' + FormatFloat('0', QrEVIEvent.Value);
    QrEVI.Next;
  end;
  Result := EvenVar;
end;

function TDmSal.SomaValoresDeIncidencia(Variavel, Entidade: Integer): Double;
begin
  QrSVI.Close;
  QrSVI.SQL.Clear;
  QrSVI.SQL.Add('SELECT SUM(Valor) Valor');
  QrSVI.SQL.Add('FROM fpcalceven');
  QrSVI.SQL.Add('WHERE Evento in (' +
    EventosDeVariavelDeIncidencia(Variavel) + ')');
  QrSVI.SQL.Add('AND Entidade=:P0');
  QrSVI.SQL.Add('');
  QrSVI.Params[0].AsInteger := Entidade;
  QrSVI.Open;
  //
  Result := QrSVIValor.Value;
end;

function TDmSal.ExcluiCalculoFolha(Codigo: Integer): Boolean;
begin
  if Application.MessageBox(PChar('Confirma a exclus�o do c�lculo n� ' +
  IntToStr(Codigo) + '?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    try
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE fpfuncifer SET Calculo = 0 WHERE Calculo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM fpfolhacal WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM fpfolhaeve WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM fpfolhaacu WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM fpfolhafer WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      QrCdo.Close;
      QrCdo.Open;
      //
      Result := True;
    except
      //Result := False;
      raise;
    end;
  end else Result := False;
end;

end.

