object FmFPFunciNew: TFmFPFunciNew
  Left = 339
  Top = 185
  Caption = 'FPG-FUNCI-002 :: Cadastro de Funcion'#225'rio'
  ClientHeight = 358
  ClientWidth = 613
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 613
    Height = 196
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label4: TLabel
      Left = 12
      Top = 8
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object Label5: TLabel
      Left = 12
      Top = 72
      Width = 280
      Height = 13
      Caption = 'OBS.: Selecione a empresa antes do funcion'#225'rio!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LaCadastro: TLabel
      Left = 12
      Top = 108
      Width = 168
      Height = 13
      Caption = 'Funcion'#225'rio (Cadastro de entidade):'
    end
    object Label6: TLabel
      Left = 532
      Top = 108
      Width = 57
      Height = 13
      Caption = 'C'#243'digo (F4):'
    end
    object Label1: TLabel
      Left = 12
      Top = 148
      Width = 34
      Height = 13
      Caption = 'Chapa:'
    end
    object SpeedButton1: TSpeedButton
      Left = 508
      Top = 124
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object EdEmpresa: TdmkEditCB
      Left = 12
      Top = 23
      Width = 53
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdEmpresaChange
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 68
      Top = 23
      Width = 533
      Height = 21
      Color = clWhite
      KeyField = 'CliInt'
      ListField = 'NOMEEMPRESA'
      ListSource = DsEmpresa
      TabOrder = 1
      dmkEditCB = EdEmpresa
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdCodigo: TdmkEditCB
      Left = 12
      Top = 124
      Width = 53
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdCodigoChange
      DBLookupComboBox = CBCodigo
      IgnoraDBLookupComboBox = False
    end
    object CBCodigo: TdmkDBLookupComboBox
      Left = 68
      Top = 124
      Width = 437
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'NOMEFUNCI'
      ListSource = DsFunci
      TabOrder = 3
      dmkEditCB = EdCodigo
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdFunci: TdmkEdit
      Left = 532
      Top = 124
      Width = 68
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnKeyDown = EdFunciKeyDown
    end
    object EdChapa: TdmkEdit
      Left = 12
      Top = 163
      Width = 589
      Height = 21
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 613
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 565
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 517
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 299
        Height = 32
        Caption = 'Cadastro de Funcion'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 299
        Height = 32
        Caption = 'Cadastro de Funcion'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 299
        Height = 32
        Caption = 'Cadastro de Funcion'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 244
    Width = 613
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 609
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 288
    Width = 613
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 609
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaNome: TLabel
        Left = 136
        Top = 4
        Width = 101
        Height = 13
        Caption = 'Nome do funcion'#225'rio:'
        Visible = False
      end
      object PnSaiDesis: TPanel
        Left = 484
        Top = 0
        Width = 125
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object EdNome: TdmkEdit
        Left = 136
        Top = 20
        Width = 345
        Height = 21
        TabOrder = 2
        Visible = False
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = EdNomeChange
      end
    end
  end
  object QrFunci: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, '
      'IF(en.Nome="", en.RazaoSocial, en.Nome) NOMEFUNCI'
      'FROM entidades en'
      'WHERE (en.Fornece1=:P0'
      'OR en.Fornece2=:P1'
      'OR en.Fornece3=:P2'
      'OR en.Fornece4=:P3'
      'OR en.Fornece5=:P4'
      'OR en.Fornece6=:P5'
      ')'
      'AND NOT (Codigo IN ('
      '  SELECT Codigo FROM fpfunci'
      '  WHERE Empresa=:P6'
      '  AND DataDemiss=0))'
      'ORDER BY NOMEFUNCI')
    Left = 61
    Top = 5
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end>
    object QrFunciCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFunciNOMEFUNCI: TWideStringField
      FieldName = 'NOMEFUNCI'
      Required = True
      Size = 100
    end
  end
  object DsFunci: TDataSource
    DataSet = QrFunci
    Left = 89
    Top = 5
  end
  object QrEmpresa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, CliInt,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEEMPRESA'
      'FROM entidades en'
      'WHERE CliInt <> 0'
      'ORDER BY NOMEEMPRESA')
    Left = 5
    Top = 5
    object QrEmpresaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmpresaCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEmpresaNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Required = True
      Size = 100
    end
  end
  object DsEmpresa: TDataSource
    DataSet = QrEmpresa
    Left = 33
    Top = 5
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Funci, Chapa'
      'FROM fpfunci'
      'WHERE Empresa=:P0'
      'AND Codigo=:P1')
    Left = 548
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPesqFunci: TIntegerField
      FieldName = 'Funci'
    end
    object QrPesqChapa: TWideStringField
      FieldName = 'Chapa'
      Size = 50
    end
  end
  object QrMax: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Funci) Funci'
      'FROM fpfunci'
      'WHERE Empresa=:P0'
      '')
    Left = 577
    Top = 5
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMaxFunci: TIntegerField
      FieldName = 'Funci'
    end
  end
end
