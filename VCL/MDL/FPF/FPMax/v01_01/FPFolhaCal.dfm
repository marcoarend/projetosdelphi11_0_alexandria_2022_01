object FmFPFolhaCal: TFmFPFolhaCal
  Left = 351
  Top = 184
  Width = 1024
  Height = 530
  Caption = 'C�lculo da Folha de Pagamento'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnMouseWheel = FormMouseWheel
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Painel4: TPanel
    Left = 0
    Top = 48
    Width = 1016
    Height = 400
    Align = alClient
    Color = clAppWorkSpace
    TabOrder = 5
    Visible = False
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 510
      Height = 398
      Align = alLeft
      Caption = 'Panel4'
      TabOrder = 0
      object dmkDBGrid2: TdmkDBGrid
        Left = 1
        Top = 17
        Width = 508
        Height = 243
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'TemCalc'
            Title.Alignment = taCenter
            Title.Caption = '*'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Salvou'
            Title.Alignment = taCenter
            Title.Caption = '**'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Entidade'
            Title.Caption = 'Entid.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NomeEnti'
            Title.Caption = 'Nome'
            Width = 206
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorP'
            Title.Caption = 'Proventos'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorD'
            Title.Caption = 'Descontos'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorL'
            Title.Caption = 'L�quido'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodFunci'
            Title.Caption = 'Funci.'
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FPFolhaCal'
            Title.Caption = 'C�culo n�'
            Visible = True
          end>
        Color = clWindow
        DataSource = DmSal.DsCalc
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'TemCalc'
            Title.Alignment = taCenter
            Title.Caption = '*'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Salvou'
            Title.Alignment = taCenter
            Title.Caption = '**'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Entidade'
            Title.Caption = 'Entid.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NomeEnti'
            Title.Caption = 'Nome'
            Width = 206
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorP'
            Title.Caption = 'Proventos'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorD'
            Title.Caption = 'Descontos'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorL'
            Title.Caption = 'L�quido'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodFunci'
            Title.Caption = 'Funci.'
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FPFolhaCal'
            Title.Caption = 'C�culo n�'
            Visible = True
          end>
      end
      object Panel10: TPanel
        Left = 1
        Top = 1
        Width = 508
        Height = 16
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label7: TLabel
          Left = 4
          Top = 0
          Width = 233
          Height = 13
          Caption = '* : J� tem c�lculo.        ** : O c�lculo salvo � este.'
        end
      end
      object Panel11: TPanel
        Left = 1
        Top = 349
        Width = 508
        Height = 48
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object BtSalva: TBitBtn
          Tag = 10035
          Left = 8
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Salva'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSalvaClick
          NumGlyphs = 2
        end
      end
      object MeMsg: TMemo
        Left = 1
        Top = 260
        Width = 508
        Height = 89
        Align = alBottom
        TabOrder = 3
      end
    end
    object Panel5: TPanel
      Left = 300
      Top = 45
      Width = 571
      Height = 340
      TabOrder = 1
      object Splitter1: TSplitter
        Left = 1
        Top = 89
        Width = 569
        Height = 3
        Cursor = crVSplit
        Align = alTop
      end
      object Splitter2: TSplitter
        Left = 1
        Top = 169
        Width = 569
        Height = 3
        Cursor = crVSplit
        Align = alBottom
      end
      object Panel6: TPanel
        Left = 1
        Top = 1
        Width = 569
        Height = 88
        Align = alTop
        TabOrder = 0
        object Label63: TLabel
          Left = 8
          Top = 4
          Width = 89
          Height = 13
          Caption = 'Evento do usu�rio:'
        end
        object SpeedButton1: TSpeedButton
          Left = 282
          Top = 20
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object Label8: TLabel
          Left = 392
          Top = 4
          Width = 43
          Height = 13
          Caption = 'Unidade:'
        end
        object EdEvento: TLMDEdit
          Left = 8
          Top = 20
          Width = 41
          Height = 21
          
          Caret.BlinkRate = 530
          CtlXP = False
          TabOrder = 0
          OnChange = EdEmpresaChange
          OnExit = EdEmpresaExit
          AutoSelect = True
          Alignment = taRightJustify
          CustomButtons = <>
          PasswordChar = #0
        end
        object CBEvento: TDBLookupComboBox
          Left = 52
          Top = 20
          Width = 230
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Descricao'
          ListSource = DmSal.DsUserEve
          TabOrder = 1
          OnClick = CBEmpresaClick
          OnDropDown = CBEmpresaDropDown
        end
        object dmkEdEve_ValPer: TdmkEdit
          Left = 308
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          DecimalSize = 2
          LeftZeros = 0
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          OnChange = dmkEdEve_ValPerChange
        end
        object CkEve_Incid: TCheckBox
          Left = 136
          Top = 60
          Width = 161
          Height = 17
          Caption = 'Valor ao inv�s de refer�ncia.'
          TabOrder = 4
        end
        object BtIncluir: TBitBtn
          Tag = 10
          Left = 300
          Top = 44
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Incluir'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtIncluirClick
          NumGlyphs = 2
        end
        object BitBtn2: TBitBtn
          Tag = 12
          Left = 392
          Top = 44
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Excluir'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = BitBtn2Click
          NumGlyphs = 2
        end
        object EdUnidade: TEdit
          Left = 392
          Top = 20
          Width = 89
          Height = 21
          MaxLength = 15
          TabOrder = 3
        end
      end
      object Panel7: TPanel
        Left = 1
        Top = 172
        Width = 569
        Height = 167
        Align = alBottom
        Caption = 'Panel7'
        TabOrder = 1
        object DBGrid2: TDBGrid
          Left = 1
          Top = 1
          Width = 567
          Height = 165
          Align = alClient
          DataSource = DmSal.DsCalcI
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Evento'
              Title.Caption = 'C�d.'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEEVENTO'
              Title.Caption = 'Descri��o'
              Width = 373
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end>
        end
      end
      object Panel8: TPanel
        Left = 1
        Top = 92
        Width = 569
        Height = 77
        Align = alClient
        TabOrder = 2
        object DBGrid1: TDBGrid
          Left = 1
          Top = 1
          Width = 567
          Height = 75
          Align = alClient
          DataSource = DmSal.DsCalcM
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Evento'
              Title.Caption = 'C�d.'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEEVENTO'
              Title.Caption = 'Descri��o'
              Width = 192
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Referencia'
              Title.Caption = 'Refer�ncia'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorP'
              Title.Caption = 'Proventos'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorD'
              Title.Caption = 'Descontos'
              Visible = True
            end>
        end
      end
    end
  end
  object Painel3: TPanel
    Left = 0
    Top = 48
    Width = 1016
    Height = 400
    Align = alClient
    Color = clAppWorkSpace
    TabOrder = 4
    Visible = False
    object dmkDBGrid1: TdmkDBGrid
      Left = 101
      Top = 1
      Width = 688
      Height = 140
      Columns = <
        item
          Expanded = False
          FieldName = 'Calcula'
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Entidade'
          Width = 47
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodFunci'
          Title.Caption = 'C�d.Funci.'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeEnti'
          Title.Caption = 'Nome do funcion�rio'
          Width = 597
          Visible = True
        end>
      Color = clWindow
      DataSource = DmSal.DsFPC
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dmkDBGrid1CellClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Calcula'
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Entidade'
          Width = 47
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodFunci'
          Title.Caption = 'C�d.Funci.'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeEnti'
          Title.Caption = 'Nome do funcion�rio'
          Width = 597
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 100
      Height = 398
      Align = alLeft
      TabOrder = 1
      object BitBtn1: TBitBtn
        Tag = 128
        Left = 3
        Top = 48
        Width = 90
        Height = 40
        Caption = '&Nenhum'
        TabOrder = 0
        OnClick = BitBtn1Click
        NumGlyphs = 2
      end
      object BtOK: TBitBtn
        Tag = 127
        Left = 3
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Todos'
        TabOrder = 1
        OnClick = BtOKClick
        NumGlyphs = 2
      end
      object PB2_1: TProgressBar
        Left = 1
        Top = 380
        Width = 98
        Height = 17
        Align = alBottom
        Min = 0
        Max = 100
        TabOrder = 2
      end
      object Button1: TButton
        Left = 13
        Top = 100
        Width = 75
        Height = 25
        Caption = 'Limpa'
        TabOrder = 3
        OnClick = Button1Click
      end
    end
  end
  object Painel2: TPanel
    Left = 0
    Top = 48
    Width = 1016
    Height = 400
    Align = alClient
    Color = clAppWorkSpace
    TabOrder = 3
    Visible = False
    object dmkDBGrid3: TdmkDBGrid
      Left = 1
      Top = 29
      Width = 1014
      Height = 220
      Columns = <
        item
          Expanded = False
          FieldName = 'Entidade'
          Title.Caption = 'Entid.'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFUNCI'
          Title.Caption = 'Funcion�rio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataC'
          Title.Caption = 'Data c�lc.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorP'
          Title.Caption = 'Proventos'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorD'
          Title.Caption = 'Descontos'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorL'
          Title.Caption = 'Valor l�quido'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C�culo n�'
          Visible = True
        end>
      Color = clWindow
      DataSource = DmSal.DsCdo
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Entidade'
          Title.Caption = 'Entid.'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFUNCI'
          Title.Caption = 'Funcion�rio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataC'
          Title.Caption = 'Data c�lc.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorP'
          Title.Caption = 'Proventos'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorD'
          Title.Caption = 'Descontos'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorL'
          Title.Caption = 'Valor l�quido'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C�culo n�'
          Visible = True
        end>
    end
    object Panel9: TPanel
      Left = 1
      Top = 351
      Width = 1014
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtImprime: TBitBtn
        Tag = 5
        Left = 10
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Imprime'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtImprimeClick
        NumGlyphs = 2
      end
      object BitBtn3: TBitBtn
        Tag = 12
        Left = 106
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BitBtn3Click
        NumGlyphs = 2
      end
    end
    object Panel12: TPanel
      Left = 1
      Top = 1
      Width = 1014
      Height = 27
      Align = alTop
      BevelOuter = bvNone
      Caption = 'C�lculos salvos do per�odo / tipo de pagamento'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 10649689
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
  end
  object Painel1: TPanel
    Left = 0
    Top = 48
    Width = 1016
    Height = 400
    Align = alClient
    Color = clAppWorkSpace
    TabOrder = 2
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 1014
      Height = 332
      Align = alTop
      TabOrder = 0
      object Label2: TLabel
        Left = 12
        Top = 8
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label3: TLabel
        Left = 12
        Top = 48
        Width = 157
        Height = 13
        Caption = 'Departamento (vazio para todos):'
      end
      object Label1: TLabel
        Left = 12
        Top = 88
        Width = 76
        Height = 13
        Caption = 'Tipo de c�lculo:'
      end
      object Label4: TLabel
        Left = 348
        Top = 128
        Width = 78
        Height = 13
        Caption = 'Data do c�lculo:'
      end
      object Label5: TLabel
        Left = 12
        Top = 128
        Width = 65
        Height = 13
        Caption = 'Compet�ncia:'
      end
      object Label16: TLabel
        Left = 80
        Top = 130
        Width = 22
        Height = 12
        Caption = 'pq'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Wingdings 3'
        Font.Style = []
        ParentFont = False
      end
      object EdEmpresa: TLMDEdit
        Left = 12
        Top = 24
        Width = 53
        Height = 21
        
        Caret.BlinkRate = 530
        CtlXP = False
        TabOrder = 0
        OnChange = EdEmpresaChange
        OnExit = EdEmpresaExit
        AutoSelect = True
        Alignment = taRightJustify
        CustomButtons = <>
        PasswordChar = #0
      end
      object CBEmpresa: TDBLookupComboBox
        Left = 68
        Top = 24
        Width = 373
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEEMPRESA'
        ListSource = DmSal.DsEmpresas
        TabOrder = 1
        OnClick = CBEmpresaClick
        OnDropDown = CBEmpresaDropDown
      end
      object EdDepto: TLMDEdit
        Left = 12
        Top = 64
        Width = 53
        Height = 21
        
        Caret.BlinkRate = 530
        CtlXP = False
        TabOrder = 2
        OnChange = EdDeptoChange
        OnExit = EdDeptoExit
        AutoSelect = True
        Alignment = taRightJustify
        CustomButtons = <>
        PasswordChar = #0
      end
      object CBDepto: TDBLookupComboBox
        Left = 68
        Top = 64
        Width = 373
        Height = 21
        Color = clWhite
        KeyField = 'CodID'
        ListField = 'Descricao'
        ListSource = DmSal.DsDepto
        TabOrder = 3
        OnClick = CBDeptoClick
        OnDropDown = CBDeptoDropDown
      end
      object EdTipoCalc: TLMDEdit
        Left = 12
        Top = 104
        Width = 53
        Height = 21
        
        Caret.BlinkRate = 530
        CtlXP = False
        TabOrder = 4
        OnChange = EdEmpresaChange
        OnExit = EdEmpresaExit
        AutoSelect = True
        Alignment = taRightJustify
        CustomButtons = <>
        PasswordChar = #0
      end
      object CBTipoCalc: TDBLookupComboBox
        Left = 68
        Top = 104
        Width = 373
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Descricao'
        ListSource = DmSal.DsFPtpCal
        TabOrder = 5
        OnClick = CBEmpresaClick
        OnDropDown = CBEmpresaDropDown
      end
      object TPDataC: TDateTimePicker
        Left = 348
        Top = 144
        Width = 93
        Height = 21
        CalAlignment = dtaLeft
        Date = 39455.6922898495
        Time = 39455.6922898495
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 10
      end
      object dmkEdPeriodo: TdmkEdit
        Left = 12
        Top = 144
        Width = 36
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        DecimalSize = 0
        LeftZeros = 0
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        OnChange = dmkEdPeriodoChange
        OnKeyDown = dmkEdPeriodoKeyDown
      end
      object Edit1: TEdit
        Left = 48
        Top = 144
        Width = 145
        Height = 21
        ReadOnly = True
        TabOrder = 7
        OnKeyDown = Edit1KeyDown
      end
      object EdTpCalPer: TLMDEdit
        Left = 196
        Top = 144
        Width = 36
        Height = 21
        
        Caret.BlinkRate = 530
        CtlXP = False
        TabOrder = 8
        Visible = False
        OnChange = EdEmpresaChange
        OnExit = EdEmpresaExit
        AutoSelect = True
        Alignment = taRightJustify
        CustomButtons = <>
        PasswordChar = #0
      end
      object CBTpCalPer: TDBLookupComboBox
        Left = 232
        Top = 144
        Width = 113
        Height = 21
        Color = clWhite
        KeyField = 'Controle'
        ListField = 'Descricao'
        ListSource = DmSal.DsTpCalPer
        TabOrder = 9
        Visible = False
        OnClick = CBEmpresaClick
        OnDropDown = CBEmpresaDropDown
      end
      object PnAdiantamento: TPanel
        Left = 12
        Top = 196
        Width = 429
        Height = 75
        BevelOuter = bvLowered
        TabOrder = 13
        Visible = False
        object Label6: TLabel
          Left = 204
          Top = 28
          Width = 102
          Height = 13
          Caption = '$ ou % adiantamento:'
        end
        object RGAdianta: TRadioGroup
          Left = 8
          Top = 3
          Width = 185
          Height = 64
          ItemIndex = 0
          Items.Strings = (
            'Adiantamento por percentual.'
            'Adiantamnto por valor.')
          TabOrder = 0
        end
        object CkIRRF: TCheckBox
          Left = 204
          Top = 8
          Width = 185
          Height = 17
          Caption = 'Calcula IRRF sobre adiantamento.'
          TabOrder = 1
        end
        object dmkEdAdianta: TdmkEdit
          Left = 204
          Top = 44
          Width = 105
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          DecimalSize = 2
          LeftZeros = 0
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
        end
      end
      object CkArredonda: TCheckBox
        Left = 12
        Top = 172
        Width = 185
        Height = 17
        Caption = 'C�lculo com arredondamento para:'
        Enabled = False
        TabOrder = 11
        Visible = False
        OnClick = CkArredondaClick
      end
      object dmkEdArredonda: TdmkEdit
        Left = 200
        Top = 168
        Width = 61
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 12
        Visible = False
        FormatType = dmktfDouble
        DecimalSize = 2
        LeftZeros = 0
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
      end
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 1016
    Height = 48
    Align = alBottom
    TabOrder = 0
    object Panel2: TPanel
      Left = 904
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      object BtSaida: TBitBtn
        Tag = 15
        Left = 2
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object BtPrior: TBitBtn
      Tag = 10033
      Left = 10
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Retorna'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtPriorClick
      NumGlyphs = 2
    end
    object BtNext: TBitBtn
      Tag = 10034
      Left = 106
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Avan�a'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtNextClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'C�lculo da Folha de Pagamento'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 1012
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object PMSalva: TPopupMenu
    Left = 41
    Top = 329
    object ClculoAtual1: TMenuItem
      Caption = 'C�lculo &Atual (Selecionado)'
      OnClick = ClculoAtual1Click
    end
    object ClculosModificados1: TMenuItem
      Caption = 'Todos c�lculos &Modificados'
      Enabled = False
    end
    object Todosclculos1: TMenuItem
      Caption = '&Todos c�lculos sem excess�o'
      OnClick = Todosclculos1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 41
    Top = 357
    object ItemAtual1: TMenuItem
      Caption = 'Item &Atual'
      OnClick = ItemAtual1Click
    end
    object ItensSelecionados1: TMenuItem
      Caption = 'Itens &Selecionados'
      OnClick = ItensSelecionados1Click
    end
    object Todositens1: TMenuItem
      Caption = '&Todos itens'
      OnClick = Todositens1Click
    end
  end
  object QrFPParam: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Parametro, Valor'
      'FROM fpparam')
    Left = 272
    Top = 176
    object QrFPParamParametro: TIntegerField
      FieldName = 'Parametro'
    end
    object QrFPParamValor: TFloatField
      FieldName = 'Valor'
    end
  end
end

