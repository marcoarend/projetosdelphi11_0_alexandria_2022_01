object FmFPFolhaImp: TFmFPFolhaImp
  Left = 370
  Top = 172
  Width = 1024
  Height = 530
  Caption = 'Impress�o de Recibo de Pagamento Salarial'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 1016
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Label1: TLabel
      Left = 132
      Top = 16
      Width = 23
      Height = 13
      Caption = 'Vias:'
    end
    object BtOK: TBitBtn
      Tag = 5
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 904
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa�da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object dmkEdVias: TdmkEdit
      Left = 156
      Top = 12
      Width = 25
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      DecimalSize = 0
      LeftZeros = 0
      ValMin = '1'
      ValMax = '9'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Impress�o de Recibo de Pagamento Salarial'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 1012
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1016
    Height = 400
    Align = alClient
    TabOrder = 0
    object dmkDBGrid1: TdmkDBGrid
      Left = 1
      Top = 1
      Width = 1014
      Height = 398
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'PERIODO_TXT'
          Title.Caption = 'Compet�ncia'
          Width = 133
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFUNCI'
          Title.Caption = 'Funcion�rio'
          Width = 267
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEEMPRE'
          Title.Caption = 'Empresa'
          Width = 212
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMETIPOCALC'
          Title.Caption = 'Pagamento'
          Width = 170
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorP'
          Title.Caption = 'Proventos'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorD'
          Title.Caption = 'D�bitos'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorL'
          Title.Caption = 'Valor l�quido'
          Visible = True
        end>
      Color = clWindow
      DataSource = DsCalc
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'PERIODO_TXT'
          Title.Caption = 'Compet�ncia'
          Width = 133
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFUNCI'
          Title.Caption = 'Funcion�rio'
          Width = 267
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEEMPRE'
          Title.Caption = 'Empresa'
          Width = 212
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMETIPOCALC'
          Title.Caption = 'Pagamento'
          Width = 170
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorP'
          Title.Caption = 'Proventos'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorD'
          Title.Caption = 'D�bitos'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorL'
          Title.Caption = 'Valor l�quido'
          Visible = True
        end>
    end
  end
  object QrCalc: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCalcAfterScroll
    OnCalcFields = QrCalcCalcFields
    SQL.Strings = (
      'SELECT fff.Pecun, fff.DtIniPA, fff.DtFimPA, '
      'fff.DiasFaltI, fff.DiasGozar, fff.DiasPecun,'
      'fff.DtIniPecun, fff.DtFimPecun, fff.DtIniPG, '
      'fff.DtFimPG, fff.DtRecPgto, fff.DtRetorno,'
      'fun.Funci CODFUNCI, fun.Registro, fun.Depto,'
      'fun.Funcao, fun.CBO2002, fun.DataAdm, fun.Filhos,'
      'fun.Dependent, fun.BancoSal, fun.ContaSal, fca.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFUNCI,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMPRE,'
      'tca.Descricao NOMETIPOCALC, dep.Descricao NOMEDEPTO,'
      'fpf.Descricao NOMEFUNCAO, fun.Foto, '
      'fca.MediaHE, fca.MediaCo, fca.MediaAd,'
      'fca.*'
      ''
      'FROM fpfolhacal fca'
      'LEFT JOIN entidades  ent ON ent.Codigo=fca.Entidade'
      'LEFT JOIN entidades  emp ON emp.Codigo=fca.Empresa'
      'LEFT JOIN fptpcal    tca ON tca.Codigo=fca.TipoCalc'
      'LEFT JOIN fpfunci    fun ON fun.Codigo=ent.Codigo'
      'LEFT JOIN fpdepto    dep ON dep.CodID=fun.Depto'
      'LEFT JOIN fpfunca    fpf ON fpf.CodID=fun.Funcao'
      'LEFT JOIN fpfuncifer fff ON fff.Calculo=fca.Codigo'
      ''
      'WHERE fca.Codigo in (9)')
    Left = 88
    Top = 92
    object QrCalcCODFUNCI: TIntegerField
      FieldName = 'CODFUNCI'
    end
    object QrCalcRegistro: TWideStringField
      FieldName = 'Registro'
      Size = 10
    end
    object QrCalcDepto: TWideStringField
      FieldName = 'Depto'
    end
    object QrCalcFuncao: TWideStringField
      FieldName = 'Funcao'
    end
    object QrCalcCBO2002: TWideStringField
      FieldName = 'CBO2002'
      Size = 7
    end
    object QrCalcDataAdm: TDateField
      FieldName = 'DataAdm'
    end
    object QrCalcFilhos: TIntegerField
      FieldName = 'Filhos'
    end
    object QrCalcDependent: TIntegerField
      FieldName = 'Dependent'
    end
    object QrCalcBancoSal: TIntegerField
      FieldName = 'BancoSal'
    end
    object QrCalcContaSal: TWideStringField
      FieldName = 'ContaSal'
      Size = 15
    end
    object QrCalcCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCalcPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrCalcEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrCalcEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrCalcSemana: TSmallintField
      FieldName = 'Semana'
      Required = True
    end
    object QrCalcTipoCalc: TIntegerField
      FieldName = 'TipoCalc'
      Required = True
    end
    object QrCalcDataC: TDateField
      FieldName = 'DataC'
      Required = True
    end
    object QrCalcValorP: TFloatField
      FieldName = 'ValorP'
      Required = True
    end
    object QrCalcValorD: TFloatField
      FieldName = 'ValorD'
      Required = True
    end
    object QrCalcValorL: TFloatField
      FieldName = 'ValorL'
      Required = True
    end
    object QrCalcAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrCalcNOMEFUNCI: TWideStringField
      FieldName = 'NOMEFUNCI'
      Size = 100
    end
    object QrCalcNOMEEMPRE: TWideStringField
      FieldName = 'NOMEEMPRE'
      Size = 100
    end
    object QrCalcNOMETIPOCALC: TWideStringField
      FieldName = 'NOMETIPOCALC'
      Size = 35
    end
    object QrCalcNOMEDEPTO: TWideStringField
      FieldName = 'NOMEDEPTO'
      Size = 50
    end
    object QrCalcNOMEFUNCAO: TWideStringField
      FieldName = 'NOMEFUNCAO'
      Size = 100
    end
    object QrCalcPERIODO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODO_TXT'
      Size = 50
      Calculated = True
    end
    object QrCalcFoto: TWideStringField
      FieldName = 'Foto'
      Size = 255
    end
    object QrCalcPecun: TSmallintField
      FieldName = 'Pecun'
    end
    object QrCalcDtIniPA: TDateField
      FieldName = 'DtIniPA'
    end
    object QrCalcDtFimPA: TDateField
      FieldName = 'DtFimPA'
    end
    object QrCalcDiasFaltI: TIntegerField
      FieldName = 'DiasFaltI'
    end
    object QrCalcDiasGozar: TSmallintField
      FieldName = 'DiasGozar'
    end
    object QrCalcDiasPecun: TSmallintField
      FieldName = 'DiasPecun'
    end
    object QrCalcDtIniPecun: TDateField
      FieldName = 'DtIniPecun'
    end
    object QrCalcDtFimPecun: TDateField
      FieldName = 'DtFimPecun'
    end
    object QrCalcDtIniPG: TDateField
      FieldName = 'DtIniPG'
    end
    object QrCalcDtFimPG: TDateField
      FieldName = 'DtFimPG'
    end
    object QrCalcDtRecPgto: TDateField
      FieldName = 'DtRecPgto'
    end
    object QrCalcDtRetorno: TDateField
      FieldName = 'DtRetorno'
    end
    object QrCalcMediaHE: TFloatField
      FieldName = 'MediaHE'
      Required = True
    end
    object QrCalcMediaCo: TFloatField
      FieldName = 'MediaCo'
      Required = True
    end
    object QrCalcMediaAd: TFloatField
      FieldName = 'MediaAd'
      Required = True
    end
  end
  object DsCalc: TDataSource
    DataSet = QrCalc
    Left = 116
    Top = 92
  end
  object QrInsC: TmySQLQuery
    Database = Dmod.MyLocDatabase
    Left = 148
    Top = 92
  end
  object QrInsI: TmySQLQuery
    Database = Dmod.MyLocDatabase
    Left = 148
    Top = 120
  end
  object QrInsR: TmySQLQuery
    Database = Dmod.MyLocDatabase
    Left = 148
    Top = 148
  end
  object QrEveM: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT eve.Descricao, foe.*,'
      'IF(eve.Natureza="P", foe.Valor, 0) ValorP,'
      'IF(eve.Natureza="D", -foe.Valor, 0) ValorD'
      'FROM fpfolhaeve foe'
      'LEFT JOIN fpevent eve ON eve.Codigo=foe.Evento'
      'WHERE foe.Valor <> 0'
      'AND foe.Mostra=1'
      'AND foe.Info=0'
      'AND foe.Codigo=:P0'
      'ORDER BY ValorP DESC, ValorD DESC'
      'LIMIT :P1, :P2')
    Left = 88
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEveMDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 50
    end
    object QrEveMCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEveMEvento: TIntegerField
      FieldName = 'Evento'
      Required = True
    end
    object QrEveMReferencia: TFloatField
      FieldName = 'Referencia'
      Required = True
    end
    object QrEveMValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrEveMInfo: TSmallintField
      FieldName = 'Info'
      Required = True
    end
    object QrEveMMostra: TSmallintField
      FieldName = 'Mostra'
      Required = True
    end
    object QrEveMAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEveMValorP: TFloatField
      FieldName = 'ValorP'
      Required = True
    end
    object QrEveMValorD: TFloatField
      FieldName = 'ValorD'
      Required = True
    end
  end
  object QrTotPD: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrTotPDAfterOpen
    SQL.Strings = (
      'SELECT '
      'SUM(IF(eve.Natureza="P", foe.Valor, 0)) ValorP,'
      'SUM(IF(eve.Natureza="D", -foe.Valor, 0)) ValorD'
      'FROM fpfolhaeve foe'
      'LEFT JOIN fpevent eve ON eve.Codigo=foe.Evento'
      'WHERE foe.Mostra=1'
      'AND Info=0'
      'AND foe.Codigo=:P0')
    Left = 88
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotPDValorP: TFloatField
      FieldName = 'ValorP'
    end
    object QrTotPDValorD: TFloatField
      FieldName = 'ValorD'
    end
  end
  object QrCab: TmySQLQuery
    Database = Dmod.MyLocDatabase
    AfterScroll = QrCabAfterScroll
    OnCalcFields = QrCabCalcFields
    SQL.Strings = (
      'SELECT * FROM '
      'FPImpCab')
    Left = 212
    Top = 92
    object QrCabFolhaCal: TIntegerField
      FieldName = 'FolhaCal'
      Origin = 'fpimpcab.FolhaCal'
    end
    object QrCabControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'fpimpcab.Controle'
    end
    object QrCabNumFolha: TIntegerField
      FieldName = 'NumFolha'
      Origin = 'fpimpcab.NumFolha'
    end
    object QrCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'fpimpcab.Empresa'
    end
    object QrCabCodFunci: TIntegerField
      FieldName = 'CodFunci'
      Origin = 'fpimpcab.CodFunci'
    end
    object QrCabEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'fpimpcab.Entidade'
    end
    object QrCabRegistro: TWideStringField
      FieldName = 'Registro'
      Origin = 'fpimpcab.Registro'
      Size = 10
    end
    object QrCabCodINSS: TWideStringField
      FieldName = 'CodINSS'
      Origin = 'fpimpcab.CodINSS'
    end
    object QrCabDataAdm: TDateField
      FieldName = 'DataAdm'
      Origin = 'fpimpcab.DataAdm'
    end
    object QrCabCBO2002: TWideStringField
      FieldName = 'CBO2002'
      Origin = 'fpimpcab.CBO2002'
      Size = 8
    end
    object QrCabNomeEnt: TWideStringField
      FieldName = 'NomeEnt'
      Origin = 'fpimpcab.NomeEnt'
      Size = 100
    end
    object QrCabDepto: TWideStringField
      FieldName = 'Depto'
      Origin = 'fpimpcab.Depto'
    end
    object QrCabNomeDepto: TWideStringField
      FieldName = 'NomeDepto'
      Origin = 'fpimpcab.NomeDepto'
      Size = 50
    end
    object QrCabFuncao: TWideStringField
      FieldName = 'Funcao'
      Origin = 'fpimpcab.Funcao'
    end
    object QrCabNomeFuncao: TWideStringField
      FieldName = 'NomeFuncao'
      Origin = 'fpimpcab.NomeFuncao'
      Size = 100
    end
    object QrCabPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'fpimpcab.Periodo'
    end
    object QrCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'fpimpcab.Ativo'
    end
    object QrCabPERIODO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODO_TXT'
      Size = 50
      Calculated = True
    end
    object QrCabUltimFolha: TSmallintField
      FieldName = 'UltimFolha'
      Origin = 'fpimpcab.UltimFolha'
    end
    object QrCabFoto: TWideStringField
      FieldName = 'Foto'
      Origin = 'fpimpcab.Foto'
      Size = 255
    end
    object QrCabDtIniPA: TDateField
      FieldName = 'DtIniPA'
      Origin = 'fpimpcab.DtIniPA'
    end
    object QrCabDtFimPA: TDateField
      FieldName = 'DtFimPA'
      Origin = 'fpimpcab.DtFimPA'
    end
    object QrCabDtIniPG: TDateField
      FieldName = 'DtIniPG'
      Origin = 'fpimpcab.DtIniPG'
    end
    object QrCabDtFimPG: TDateField
      FieldName = 'DtFimPG'
      Origin = 'fpimpcab.DtFimPG'
    end
    object QrCabDtIniPP: TDateField
      FieldName = 'DtIniPP'
      Origin = 'fpimpcab.DtIniPP'
    end
    object QrCabDtFimPP: TDateField
      FieldName = 'DtFimPP'
      Origin = 'fpimpcab.DtFimPP'
    end
    object QrCabDiasPG: TIntegerField
      FieldName = 'DiasPG'
      Origin = 'fpimpcab.DiasPG'
    end
    object QrCabDiasPP: TIntegerField
      FieldName = 'DiasPP'
      Origin = 'fpimpcab.DiasPP'
    end
    object QrCabDiasFI: TIntegerField
      FieldName = 'DiasFI'
      Origin = 'fpimpcab.DiasFI'
    end
    object QrCabMediaHE: TFloatField
      FieldName = 'MediaHE'
      Origin = 'fpimpcab.MediaHE'
    end
    object QrCabMediaCo: TFloatField
      FieldName = 'MediaCo'
      Origin = 'fpimpcab.MediaCo'
    end
    object QrCabMediaAd: TFloatField
      FieldName = 'MediaAd'
      Origin = 'fpimpcab.MediaAd'
    end
    object QrCabSalFixo: TFloatField
      FieldName = 'SalFixo'
      Origin = 'fpimpcab.SalFixo'
    end
    object QrCabBaseCal: TFloatField
      FieldName = 'BaseCal'
      Origin = 'fpimpcab.BaseCal'
    end
    object QrCabDIASPG_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DIASPG_TXT'
      Size = 100
      Calculated = True
    end
    object QrCabDIASPP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DIASPP_TXT'
      Size = 100
      Calculated = True
    end
    object QrCabPA_INI_DD: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PA_INI_DD'
      Size = 2
      Calculated = True
    end
    object QrCabPA_INI_MM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PA_INI_MM'
      Size = 10
      Calculated = True
    end
    object QrCabPA_INI_AA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PA_INI_AA'
      Size = 4
      Calculated = True
    end
    object QrCabPA_FIM_DD: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PA_FIM_DD'
      Size = 2
      Calculated = True
    end
    object QrCabPA_FIM_MM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PA_FIM_MM'
      Size = 10
      Calculated = True
    end
    object QrCabPA_FIM_AA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PA_FIM_AA'
      Size = 4
      Calculated = True
    end
    object QrCabPG_INI_DD: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PG_INI_DD'
      Size = 2
      Calculated = True
    end
    object QrCabPG_INI_MM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PG_INI_MM'
      Size = 10
      Calculated = True
    end
    object QrCabPG_INI_AA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PG_INI_AA'
      Size = 4
      Calculated = True
    end
    object QrCabPG_FIM_DD: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PG_FIM_DD'
      Size = 2
      Calculated = True
    end
    object QrCabPG_FIM_MM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PG_FIM_MM'
      Size = 10
      Calculated = True
    end
    object QrCabPG_FIM_AA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PG_FIM_AA'
      Size = 4
      Calculated = True
    end
    object QrCabPP_INI_DD: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PP_INI_DD'
      Size = 2
      Calculated = True
    end
    object QrCabPP_INI_MM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PP_INI_MM'
      Size = 10
      Calculated = True
    end
    object QrCabPP_INI_AA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PP_INI_AA'
      Size = 4
      Calculated = True
    end
    object QrCabPP_FIM_DD: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PP_FIM_DD'
      Size = 2
      Calculated = True
    end
    object QrCabPP_FIM_MM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PP_FIM_MM'
      Size = 10
      Calculated = True
    end
    object QrCabPP_FIM_AA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PP_FIM_AA'
      Size = 4
      Calculated = True
    end
    object QrCabDtRecPgto: TDateField
      FieldName = 'DtRecPgto'
      Origin = 'fpimpcab.DtRecPgto'
    end
    object QrCabDTRECPGTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTRECPGTO_TXT'
      Size = 100
      Calculated = True
    end
  end
  object frxDsC: TfrxDBDataset
    UserName = 'frxDsC'
    CloseDataSource = False
    DataSet = QrCab
    Left = 240
    Top = 92
  end
  object QrIts: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT * FROM '
      'FPImpIts'
      'WHERE Controle=:P0'
      '')
    Left = 212
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'fpimpits.Controle'
    end
    object QrItsLinha: TIntegerField
      FieldName = 'Linha'
      Origin = 'fpimpits.Linha'
    end
    object QrItsCodiEvento: TIntegerField
      FieldName = 'CodiEvento'
      Origin = 'fpimpits.CodiEvento'
    end
    object QrItsNomeEvento: TWideStringField
      FieldName = 'NomeEvento'
      Origin = 'fpimpits.NomeEvento'
      Size = 50
    end
    object QrItsReferencia: TFloatField
      FieldName = 'Referencia'
      Origin = 'fpimpits.Referencia'
    end
    object QrItsValorP: TFloatField
      FieldName = 'ValorP'
      Origin = 'fpimpits.ValorP'
    end
    object QrItsValorD: TFloatField
      FieldName = 'ValorD'
      Origin = 'fpimpits.ValorD'
    end
    object QrItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'fpimpits.Ativo'
    end
  end
  object frxDsI: TfrxDBDataset
    UserName = 'frxDsI'
    CloseDataSource = False
    DataSet = QrIts
    Left = 240
    Top = 120
  end
  object frxDsR: TfrxDBDataset
    UserName = 'frxDsR'
    CloseDataSource = False
    DataSet = QrRpe
    Left = 240
    Top = 148
  end
  object QrRpe: TmySQLQuery
    Database = Dmod.MyLocDatabase
    OnCalcFields = QrRpeCalcFields
    SQL.Strings = (
      'SELECT * FROM '
      'FPImpRPe'
      'WHERE Controle=:P0'
      '')
    Left = 212
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRpeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrRpeTotalP: TFloatField
      FieldName = 'TotalP'
    end
    object QrRpeTotalD: TFloatField
      FieldName = 'TotalD'
    end
    object QrRpeTotalL: TFloatField
      FieldName = 'TotalL'
    end
    object QrRpeBase91: TFloatField
      FieldName = 'Base91'
    end
    object QrRpeBase93: TFloatField
      FieldName = 'Base93'
    end
    object QrRpeBase95: TFloatField
      FieldName = 'Base95'
    end
    object QrRpeBase96: TFloatField
      FieldName = 'Base96'
    end
    object QrRpeBase98: TFloatField
      FieldName = 'Base98'
    end
    object QrRpeBase99: TFloatField
      FieldName = 'Base99'
    end
    object QrRpeAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrRpeAcum03: TFloatField
      FieldName = 'Acum03'
    end
    object QrRpeAcum17: TFloatField
      FieldName = 'Acum17'
    end
    object QrRpeAcum25: TFloatField
      FieldName = 'Acum25'
    end
    object QrRpeAcum26: TFloatField
      FieldName = 'Acum26'
    end
    object QrRpeAcum27: TFloatField
      FieldName = 'Acum27'
    end
    object QrRpeAcum28: TFloatField
      FieldName = 'Acum28'
    end
    object QrRpeEXTENSO_TotalL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EXTENSO_TotalL'
      Size = 255
      Calculated = True
    end
  end
  object frxFolha: TfrxReport
    Version = '4.0.12'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39458.8377247801
    ReportOptions.LastChange = 39459.8728255208
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo58OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Memo58.Visible := <VARF_ULTIMAFOLHA> > 0;'
      
        '  Memo58.Text := '#39'CONTINUA NA FOLHA N� '#39' + IntToStr(<VARF_ULTIMA' +
        'FOLHA>);'
      'end;'
      ''
      'procedure Picture1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <ImagemExiste> = True then'
      '  begin'
      '    Picture1.Visible := True;'
      '    Picture1.LoadFromFile(<ImagemCaminho>);'
      '  end else Picture1.Visible := False;'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxFolhaGetValue
    Left = 180
    Top = 92
    Datasets = <
      item
        DataSet = frxDsC
        DataSetName = 'frxDsC'
      end
      item
        DataSet = Dmod.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = frxDsI
        DataSetName = 'frxDsI'
      end
      item
        DataSet = frxDsR
        DataSetName = 'frxDsR'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000
      Width = 1000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210
      PaperHeight = 297
      PaperSize = 9
      LeftMargin = 10
      RightMargin = 10
      BottomMargin = 10
      object MasterData1: TfrxMasterData
        Height = 137.95283646
        Top = 98.26778
        Width = 718.1107
        DataSet = frxDsC
        DataSetName = 'frxDsC'
        RowCount = 0
        object Shape1: TfrxShapeView
          Left = 3.77953
          Width = 710.55164
          Height = 120.944881889764
          Frame.Width = 0.1
        end
        object Memo1: TfrxMemoView
          Left = 3.77953
          Width = 294.80334
          Height = 22.67718
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'Recibo de Pagamento de Salário')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 7.55906
          Top = 22.67718
          Width = 419.52783
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsC."Empresa"] - [frxDsEndereco."NOMEDONO"]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 623.62245
          Width = 90.70867118
          Height = 120.94491118
          OnBeforePrint = 'Picture1OnBeforePrint'
          Frame.Width = 0.1
        end
        object Memo4: TfrxMemoView
          Left = 302.3624
          Width = 264.5671
          Height = 22.67718
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsC."PERIODO_TXT"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 623.62245
          Height = 120.94496
          Frame.Typ = [ftLeft]
          Frame.Width = 0.1
        end
        object Line2: TfrxLineView
          Left = 3.77953
          Top = 60.4724799999999
          Width = 619.84292
          Frame.Typ = [ftTop]
          Frame.Width = 0.1
        end
        object Memo2: TfrxMemoView
          Left = 7.55906
          Top = 41.57483
          Width = 612.28386
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 7.55906
          Top = 60.47248
          Width = 41.57483
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Código:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 430.86642
          Top = 22.6771799999999
          Width = 188.9765
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'CNPJ: [frxDsEndereco."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 7.55906
          Top = 71.81107
          Width = 41.57483
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[FormatFloat('#39'0000'#39',<frxDsC."CodFunci">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 113.3859
          Top = 60.4724799999999
          Width = 102.04731
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Registro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 113.3859
          Top = 71.81107
          Width = 102.04731
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsC."Registro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 7.55906
          Top = 90.70872
          Width = 151.1812
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Nome do funcionário:')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 7.55906
          Top = 102.04731
          Width = 427.08689
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsC."NomeEnt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 377.953
          Top = 60.4724799999999
          Width = 52.91342
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'C.B.O.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 377.953
          Top = 71.81107
          Width = 52.91342
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsC."CBO2002"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 434.64595
          Top = 60.4724799999999
          Width = 185.19678449
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Departamento:')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 434.64595
          Top = 71.81107
          Width = 185.19678449
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsC."Depto"] - [frxDsC."NomeDepto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 434.64595
          Top = 90.70872
          Width = 185.19678449
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Função:')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 434.64595
          Top = 102.04731
          Width = 185.19678449
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsC."Funcao"] - [frxDsC."NomeFuncao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 52.91342
          Top = 60.4724799999999
          Width = 56.69295
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Identificador:')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 52.91342
          Top = 71.81107
          Width = 56.69295
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[FormatFloat('#39'0000'#39',<frxDsC."Entidade">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 313.70099
          Top = 60.4724799999999
          Width = 60.47248
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Admissão:')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 313.70099
          Top = 71.81107
          Width = 60.47248
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsC."DataAdm"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 215.43321
          Top = 60.4724799999999
          Width = 94.48825
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Inscrição INSS:')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 215.43321
          Top = 71.81107
          Width = 94.48825
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsC."CodINSS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 566.9295
          Top = 3.77952999999997
          Width = 52.91342
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Folha:[frxDsC."NumFolha"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 3.77953
          Top = 124.72449
          Width = 52.9133858267717
          Height = 13.22834646
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Código')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo31: TfrxMemoView
          Left = 56.69295
          Top = 124.72449
          Width = 317.48052
          Height = 13.22834646
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          Memo.UTF8 = (
            'Descrição')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo32: TfrxMemoView
          Left = 374.17347
          Top = 124.72449
          Width = 113.38582677
          Height = 13.22834646
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Referência')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo33: TfrxMemoView
          Left = 487.55937
          Top = 124.72449
          Width = 113.38582677
          Height = 13.22834646
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Proventos')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo34: TfrxMemoView
          Left = 600.94527
          Top = 124.72449
          Width = 113.38582677
          Height = 13.22834646
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Descontos')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Line9: TfrxLineView
          Left = 623.62245
          Width = 90.70872
          Frame.Typ = [ftTop]
          Frame.Width = 0.1
        end
      end
      object DetailData1: TfrxDetailData
        Height = 13.22834646
        Top = 260.78757
        Width = 718.1107
        DataSet = frxDsI
        DataSetName = 'frxDsI'
        RowCount = 0
        object Memo5: TfrxMemoView
          Left = 3.77953
          Width = 52.91338583
          Height = 13.22834646
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[FormatFloat('#39'0000;-0000; '#39',<frxDsI."CodiEvento">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          Left = 56.69295
          Width = 317.48052
          Height = 13.22834646
          DataField = 'NomeEvento'
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.1
          Memo.UTF8 = (
            '[frxDsI."NomeEvento"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo27: TfrxMemoView
          Left = 374.17347
          Width = 113.38582677
          Height = 13.22834646
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            
              '[FormatFloat('#39'#,###,#0.00;-#,###,#0.00; '#39',<frxDsI."Referencia">)' +
              ']')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo28: TfrxMemoView
          Left = 487.55937
          Width = 113.38582677
          Height = 13.22834646
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[FormatFloat('#39'#,###,#0.00;-#,###,#0.00; '#39',<frxDsI."ValorP">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo29: TfrxMemoView
          Left = 600.94527
          Width = 113.38582677
          Height = 13.22834646
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[FormatFloat('#39'#,###,#0.00;-#,###,#0.00; '#39',<frxDsI."ValorD">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 56.69294756
        Top = 18.89765
        Width = 718.1107
        Condition = 'frxDsC."Controle"'
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 139.84261
        Top = 298.58287
        Width = 718.1107
        object Memo35: TfrxMemoView
          Left = 306.14193
          Width = 98.26778
          Height = 17.00787402
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            'Descontos:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 404.40971
          Width = 105.82679118
          Height = 17.00787402
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsR."TotalD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 608.50433
          Width = 105.82679118
          Height = 17.00787402
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsR."TotalL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 3.77953
          Width = 98.26778
          Height = 17.00787402
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          Memo.UTF8 = (
            ' TOTAIS')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 102.04731
          Width = 98.26778
          Height = 17.00787402
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            'Vencimentos:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 200.31509
          Width = 105.82679118
          Height = 17.00787402
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsR."TotalP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 510.23655
          Width = 98.26778
          Height = 17.00787402
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            'Valor líquido:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 3.77953
          Top = 22.6771800000001
          Width = 120.944881889764
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Salário Base:')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          Left = 3.77953
          Top = 34.01577
          Width = 120.94488189
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsR."Acum27"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          Left = 124.72449
          Top = 22.6771800000001
          Width = 120.94488189
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Salário contibuição INSS:')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          Left = 124.72449
          Top = 34.01577
          Width = 120.94488189
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsR."Acum28"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          Left = 245.66945
          Top = 22.6771800000001
          Width = 120.94488189
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Salário cálculo FGTS:')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          Left = 245.66945
          Top = 34.01577
          Width = 120.94488189
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsR."Acum17"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          Left = 366.61441
          Top = 22.6771800000001
          Width = 120.94488189
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'FGTS do mês:')
          ParentFont = False
          WordWrap = False
        end
        object Memo49: TfrxMemoView
          Left = 366.61441
          Top = 34.01577
          Width = 120.94488189
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsR."Acum03"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          Left = 487.55937
          Top = 22.6771800000001
          Width = 120.94488189
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Base cálculo IRRF:')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          Left = 487.55937
          Top = 34.01577
          Width = 120.94488189
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsR."Acum26"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          Left = 608.50433
          Top = 22.67718
          Width = 105.8267863
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Faixa IRRF:')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          Left = 608.50433
          Top = 34.01577
          Width = 105.8267863
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsR."Acum25"]')
          ParentFont = False
          WordWrap = False
        end
        object Shape2: TfrxShapeView
          Left = 3.77953
          Top = 56.69295
          Width = 710.55139591
          Height = 52.91342
          Frame.Width = 0.1
        end
        object Memo54: TfrxMemoView
          Left = 3.77953
          Top = 56.69295
          Width = 710.55139591
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            
              'Declaro ter recebido a importância líquida discriminada neste ' +
              'recibo.')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          Left = 34.01577
          Top = 75.5906
          Width = 192.75603
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.1
          Memo.UTF8 = (
            '                /                /          ')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 34.01577
          Top = 94.48825
          Width = 192.75603
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Data')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          Left = 264.5671
          Top = 94.48825
          Width = 415.7483
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Assinatura do funcionário')
          ParentFont = False
          WordWrap = False
        end
        object Line3: TfrxLineView
          Left = -75.59055118
          Top = 139.84261
          Width = 907.08661417
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
          Frame.Width = 0.1
        end
        object Memo58: TfrxMemoView
          Left = 3.77953
          Width = 710.55164
          Height = 52.91342
          Visible = False
          OnBeforePrint = 'Memo58OnBeforePrint'
          Color = clWhite
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Continua na próxima folha')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrUpdC: TmySQLQuery
    Database = Dmod.MyLocDatabase
    Left = 148
    Top = 176
  end
  object QrAcum27: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(fev.Valor) Valor'
      'FROM fpfolhaeve fev'
      'LEFT JOIN fpfolhaacu fac ON fac.Evento=fev.Evento'
      'WHERE fac.Acumulador=27'
      'AND fac.Codigo=:P0'
      'AND fev.Codigo=:P1')
    Left = 296
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAcum27Valor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrAcum25: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(fev.Valor) Valor'
      'FROM fpfolhaeve fev'
      'LEFT JOIN fpfolhaacu fac ON fac.Evento=fev.Evento'
      'WHERE fac.Acumulador=25'
      'AND fac.Codigo=:P0'
      'AND fev.Codigo=:P1')
    Left = 296
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAcum25Valor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrAcum17: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(fev.Valor) Valor'
      'FROM fpfolhaeve fev'
      'LEFT JOIN fpfolhaacu fac ON fac.Evento=fev.Evento'
      'WHERE fac.Acumulador=17'
      'AND fac.Codigo=:P0'
      'AND fev.Codigo=:P1')
    Left = 296
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAcum17Valor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrAcum03: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(fev.Valor) Valor'
      'FROM fpfolhaeve fev'
      'LEFT JOIN fpfolhaacu fac ON fac.Evento=fev.Evento'
      'WHERE fac.Acumulador=03'
      'AND fac.Codigo=:P0'
      'AND fev.Codigo=:P1')
    Left = 296
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAcum03Valor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrAcum26: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(fev.Valor) Valor'
      'FROM fpfolhaeve fev'
      'LEFT JOIN fpfolhaacu fac ON fac.Evento=fev.Evento'
      'WHERE fac.Acumulador=26'
      'AND fac.Codigo=:P0'
      'AND fev.Codigo=:P1')
    Left = 296
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAcum26Valor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrAcum28: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(fev.Valor) Valor'
      'FROM fpfolhaeve fev'
      'LEFT JOIN fpfolhaacu fac ON fac.Evento=fev.Evento'
      'WHERE fac.Acumulador=28'
      'AND fac.Codigo=:P0'
      'AND fev.Codigo=:P1')
    Left = 296
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAcum28Valor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrFPFunci: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fpf.Descricao NOMEFUNCAO, fpd.Descricao NOMEDEPTO,'
      'fp2.Descricao NOMECBO2002, fpc.Descricao NOMECATEG,'
      'fpv.Descricao NOMEVINCU, fpa.Descricao NOMEADMIS,'
      'fps.Descricao NOMESITUA, fpl.Descricao NOMETPSAL, '
      'IF(efu.Tipo=0, efu.RazaoSocial, efu.Nome) NUMEFUNCI, '
      'IF(epr.Tipo=0, epr.RazaoSocial, epr.Nome) NUMEEMPRESA, '
      'fp4.Descricao NOMECARGO, fpn.Descricao NOMENACIONALID, '
      'fpe.Descricao NOMEECIVIL, fpr.Descricao NOMERACA,'
      'fpg.Descricao NOMEGRAUI, fpm.Nome NOMELOCTRAB, fun.* '
      'FROM fpfunci fun'
      'LEFT JOIN entidades efu ON efu.Codigo=fun.Codigo'
      'LEFT JOIN entidades epr ON epr.Codigo=fun.Empresa'
      'LEFT JOIN fpfunca   fpf ON fpf.CodID=fun.Funcao'
      'LEFT JOIN fpdepto   fpd ON fpd.CodID=fun.Depto'
      'LEFT JOIN fpcbo02   fp2 ON fp2.CodID=fun.CBO2002'
      'LEFT JOIN fpcbo94   fp4 ON fp4.CodID=fun.Cargo'
      'LEFT JOIN fpcateg   fpc ON fpc.Codigo=fun.Categoria'
      'LEFT JOIN fpvincu   fpv ON fpv.Codigo=fun.Vinculo'
      'LEFT JOIN fpadmis   fpa ON fpa.Codigo=fun.Admissao'
      'LEFT JOIN fpsitua   fps ON fps.Codigo=fun.Situacao'
      'LEFT JOIN fptpsal   fpl ON fpl.Codigo=fun.TipoSal'
      'LEFT JOIN fpnacio   fpn ON fpn.Codigo=fun.Nacionalid'
      'LEFT JOIN fpcivil   fpe ON fpe.Codigo=fun.EstCivil'
      'LEFT JOIN fpracas   fpr ON fpr.Codigo=fun.Raca'
      'LEFT JOIN fpgraui   fpg ON fpg.Codigo=fun.GrauInstru'
      'LEFT JOIN fpmunic   fpm ON fpm.CodID=fun.LocalTrab'
      'WHERE fun.Codigo > 0')
    Left = 336
    Top = 93
    object QrFPFunciNUMEFUNCI: TWideStringField
      FieldName = 'NUMEFUNCI'
      Size = 100
    end
    object QrFPFunciNUMEEMPRESA: TWideStringField
      FieldName = 'NUMEEMPRESA'
      Size = 100
    end
    object QrFPFunciDepto: TWideStringField
      FieldName = 'Depto'
      Origin = 'fpfunci.Depto'
      Required = True
    end
    object QrFPFunciFuncao: TWideStringField
      FieldName = 'Funcao'
      Origin = 'fpfunci.Funcao'
      Required = True
    end
    object QrFPFunciNOMEFUNCAO: TWideStringField
      FieldName = 'NOMEFUNCAO'
      Origin = 'fpfunca.Descricao'
      Size = 100
    end
    object QrFPFunciNOMEDEPTO: TWideStringField
      FieldName = 'NOMEDEPTO'
      Origin = 'fpdepto.Descricao'
      Size = 50
    end
    object QrFPFunciNOMECBO2002: TWideStringField
      FieldName = 'NOMECBO2002'
      Origin = 'fpcbo02.Descricao'
      Size = 255
    end
    object QrFPFunciCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fpfunci.Codigo'
    end
    object QrFPFunciEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'fpfunci.Empresa'
    end
    object QrFPFunciFunci: TIntegerField
      FieldName = 'Funci'
      Origin = 'fpfunci.Funci'
    end
    object QrFPFunciRegistro: TWideStringField
      FieldName = 'Registro'
      Origin = 'fpfunci.Registro'
      Size = 10
    end
    object QrFPFunciCategoria: TIntegerField
      FieldName = 'Categoria'
      Origin = 'fpfunci.Categoria'
    end
    object QrFPFunciCBO2002: TWideStringField
      DisplayWidth = 7
      FieldName = 'CBO2002'
      Origin = 'fpfunci.CBO2002'
      Size = 7
    end
    object QrFPFunciVinculo: TIntegerField
      FieldName = 'Vinculo'
      Origin = 'fpfunci.Vinculo'
    end
    object QrFPFunciAlvara: TSmallintField
      FieldName = 'Alvara'
      Origin = 'fpfunci.Alvara'
    end
    object QrFPFunciSituacao: TIntegerField
      FieldName = 'Situacao'
      Origin = 'fpfunci.Situacao'
    end
    object QrFPFunciAdmissao: TIntegerField
      FieldName = 'Admissao'
      Origin = 'fpfunci.Admissao'
    end
    object QrFPFunciDataAdm: TDateField
      FieldName = 'DataAdm'
      Origin = 'fpfunci.DataAdm'
    end
    object QrFPFunciHrSeman: TIntegerField
      FieldName = 'HrSeman'
      Origin = 'fpfunci.HrSeman'
    end
    object QrFPFunciTipoSal: TSmallintField
      FieldName = 'TipoSal'
      Origin = 'fpfunci.TipoSal'
    end
    object QrFPFunciSalario: TFloatField
      FieldName = 'Salario'
      Origin = 'fpfunci.Salario'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrFPFunciFilhos: TIntegerField
      FieldName = 'Filhos'
      Origin = 'fpfunci.Filhos'
    end
    object QrFPFunciDependent: TIntegerField
      FieldName = 'Dependent'
      Origin = 'fpfunci.Dependent'
    end
    object QrFPFunciPensaoAlm: TFloatField
      FieldName = 'PensaoAlm'
      Origin = 'fpfunci.PensaoAlm'
      Required = True
      DisplayFormat = '#,###,##0.000000'
    end
    object QrFPFunciBancoSal: TIntegerField
      FieldName = 'BancoSal'
      Origin = 'fpfunci.BancoSal'
    end
    object QrFPFunciContaSal: TWideStringField
      FieldName = 'ContaSal'
      Origin = 'fpfunci.ContaSal'
      Size = 15
    end
    object QrFPFunciOptaFGTS: TSmallintField
      FieldName = 'OptaFGTS'
      Origin = 'fpfunci.OptaFGTS'
    end
    object QrFPFunciBancoFGTS: TIntegerField
      FieldName = 'BancoFGTS'
      Origin = 'fpfunci.BancoFGTS'
    end
    object QrFPFunciContaFGTS: TWideStringField
      FieldName = 'ContaFGTS'
      Origin = 'fpfunci.ContaFGTS'
      Size = 15
    end
    object QrFPFunciDataFGTS: TDateField
      FieldName = 'DataFGTS'
      Origin = 'fpfunci.DataFGTS'
    end
    object QrFPFunciDataExame: TDateField
      FieldName = 'DataExame'
      Origin = 'fpfunci.DataExame'
    end
    object QrFPFunciPerExame: TIntegerField
      FieldName = 'PerExame'
      Origin = 'fpfunci.PerExame'
    end
    object QrFPFunciDataIniAf: TDateField
      FieldName = 'DataIniAf'
      Origin = 'fpfunci.DataIniAf'
    end
    object QrFPFunciDataFinAf: TDateField
      FieldName = 'DataFinAf'
      Origin = 'fpfunci.DataFinAf'
    end
    object QrFPFunciMesesAf: TIntegerField
      FieldName = 'MesesAf'
      Origin = 'fpfunci.MesesAf'
    end
    object QrFPFunciDataAviso: TDateField
      FieldName = 'DataAviso'
      Origin = 'fpfunci.DataAviso'
    end
    object QrFPFunciAvisoInden: TSmallintField
      FieldName = 'AvisoInden'
      Origin = 'fpfunci.AvisoInden'
    end
    object QrFPFunciDiasAviso: TIntegerField
      FieldName = 'DiasAviso'
      Origin = 'fpfunci.DiasAviso'
    end
    object QrFPFunciDataDemiss: TDateField
      FieldName = 'DataDemiss'
      Origin = 'fpfunci.DataDemiss'
    end
    object QrFPFunciDemissao: TIntegerField
      FieldName = 'Demissao'
      Origin = 'fpfunci.Demissao'
    end
    object QrFPFunciCodigoAf: TWideStringField
      FieldName = 'CodigoAf'
      Origin = 'fpfunci.CodigoAf'
      Size = 10
    end
    object QrFPFunciMotivoAf: TWideStringField
      FieldName = 'MotivoAf'
      Origin = 'fpfunci.MotivoAf'
      Size = 50
    end
    object QrFPFunciObs: TWideMemoField
      FieldName = 'Obs'
      Origin = 'fpfunci.Obs'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFPFunciGFIP: TSmallintField
      FieldName = 'GFIP'
      Origin = 'fpfunci.GFIP'
    end
    object QrFPFunciRAIS: TSmallintField
      FieldName = 'RAIS'
      Origin = 'fpfunci.RAIS'
    end
    object QrFPFunciProfessor: TSmallintField
      FieldName = 'Professor'
      Origin = 'fpfunci.Professor'
    end
    object QrFPFunciAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'fpfunci.Ativo'
    end
    object QrFPFunciLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'fpfunci.Lk'
    end
    object QrFPFunciDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'fpfunci.DataCad'
    end
    object QrFPFunciDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'fpfunci.DataAlt'
    end
    object QrFPFunciUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'fpfunci.UserCad'
    end
    object QrFPFunciUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'fpfunci.UserAlt'
    end
    object QrFPFunciNOMECATEG: TWideStringField
      FieldName = 'NOMECATEG'
      Origin = 'fpcateg.Descricao'
      Size = 30
    end
    object QrFPFunciNOMEVINCU: TWideStringField
      FieldName = 'NOMEVINCU'
      Origin = 'fpvincu.Descricao'
      Size = 200
    end
    object QrFPFunciNOMEADMIS: TWideStringField
      FieldName = 'NOMEADMIS'
      Origin = 'fpadmis.Descricao'
      Size = 200
    end
    object QrFPFunciNOMESITUA: TWideStringField
      FieldName = 'NOMESITUA'
      Origin = 'fpsitua.Descricao'
      Size = 250
    end
    object QrFPFunciNOMETPSAL: TWideStringField
      FieldName = 'NOMETPSAL'
      Origin = 'fptpsal.Descricao'
      Size = 25
    end
    object QrFPFunciNacionalid: TIntegerField
      FieldName = 'Nacionalid'
      Origin = 'fpfunci.Nacionalid'
      Required = True
    end
    object QrFPFunciAnoChegada: TIntegerField
      FieldName = 'AnoChegada'
      Origin = 'fpfunci.AnoChegada'
      Required = True
    end
    object QrFPFunciGrauInstru: TIntegerField
      FieldName = 'GrauInstru'
      Origin = 'fpfunci.GrauInstru'
      Required = True
    end
    object QrFPFunciEstCivil: TIntegerField
      FieldName = 'EstCivil'
      Origin = 'fpfunci.EstCivil'
      Required = True
    end
    object QrFPFunciCTPS: TWideStringField
      FieldName = 'CTPS'
      Origin = 'fpfunci.CTPS'
      Size = 15
    end
    object QrFPFunciSerieCTPS: TWideStringField
      FieldName = 'SerieCTPS'
      Origin = 'fpfunci.SerieCTPS'
      Size = 10
    end
    object QrFPFunciPIS: TWideStringField
      FieldName = 'PIS'
      Origin = 'fpfunci.PIS'
    end
    object QrFPFunciTitEleitor: TWideStringField
      FieldName = 'TitEleitor'
      Origin = 'fpfunci.TitEleitor'
    end
    object QrFPFunciReservista: TWideStringField
      FieldName = 'Reservista'
      Origin = 'fpfunci.Reservista'
    end
    object QrFPFunciRaca: TIntegerField
      FieldName = 'Raca'
      Origin = 'fpfunci.Raca'
      Required = True
    end
    object QrFPFunciDeficiente: TSmallintField
      FieldName = 'Deficiente'
      Origin = 'fpfunci.Deficiente'
      Required = True
    end
    object QrFPFunciFoto: TWideStringField
      FieldName = 'Foto'
      Origin = 'fpfunci.Foto'
      Size = 255
    end
    object QrFPFunciCargo: TWideStringField
      FieldName = 'Cargo'
      Origin = 'fpfunci.Cargo'
      Required = True
      Size = 5
    end
    object QrFPFunciLocalTrab: TWideStringField
      FieldName = 'LocalTrab'
      Origin = 'fpfunci.LocalTrab'
      Size = 7
    end
    object QrFPFunciInsalubrid: TFloatField
      FieldName = 'Insalubrid'
      Origin = 'fpfunci.Insalubrid'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFPFunciPericulosi: TFloatField
      FieldName = 'Periculosi'
      Origin = 'fpfunci.Periculosi'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFPFunciNOMECARGO: TWideStringField
      FieldName = 'NOMECARGO'
      Origin = 'fpcbo94.Descricao'
      Size = 200
    end
    object QrFPFunciNOMENACIONALID: TWideStringField
      FieldName = 'NOMENACIONALID'
      Origin = 'fpnacio.Descricao'
      Size = 30
    end
    object QrFPFunciNOMEECIVIL: TWideStringField
      FieldName = 'NOMEECIVIL'
      Origin = 'fpcivil.Descricao'
      Size = 15
    end
    object QrFPFunciNOMERACA: TWideStringField
      FieldName = 'NOMERACA'
      Origin = 'fpracas.Descricao'
      Size = 15
    end
    object QrFPFunciNOMEGRAUI: TWideStringField
      FieldName = 'NOMEGRAUI'
      Origin = 'fpgraui.Descricao'
      Size = 200
    end
    object QrFPFunciNOMELOCTRAB: TWideStringField
      FieldName = 'NOMELOCTRAB'
      Origin = 'fpmunic.Nome'
      Size = 35
    end
    object QrFPFunciPensaoLiq: TSmallintField
      FieldName = 'PensaoLiq'
      Origin = 'fpfunci.PensaoLiq'
      Required = True
    end
    object QrFPFunciComoCalcIR: TSmallintField
      FieldName = 'ComoCalcIR'
      Origin = 'fpfunci.ComoCalcIR'
      Required = True
    end
  end
  object QrFPFunciFer: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM fpfuncifer'
      'WHERE Empresa=:P0'
      'AND Codigo=:P1'
      'ORDER BY DtIniPA DESC')
    Left = 336
    Top = 121
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFPFunciFerEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'fpfuncifer.Empresa'
    end
    object QrFPFunciFerCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fpfuncifer.Codigo'
    end
    object QrFPFunciFerCalculo: TIntegerField
      FieldName = 'Calculo'
      Origin = 'fpfuncifer.Calculo'
    end
    object QrFPFunciFerPecun: TSmallintField
      FieldName = 'Pecun'
      Origin = 'fpfuncifer.Pecun'
    end
    object QrFPFunciFerMeHEMan160: TFloatField
      FieldName = 'MeHEMan160'
    end
    object QrFPFunciFerMeHEMan200: TFloatField
      FieldName = 'MeHEMan200'
      DisplayFormat = '#,###,##0.00'
    end
    object QrFPFunciFerMediaCoMan: TFloatField
      FieldName = 'MediaCoMan'
      Origin = 'fpfuncifer.MediaCoMan'
      DisplayFormat = '#,###,##0.00'
    end
    object QrFPFunciFerMediaAdMan: TFloatField
      FieldName = 'MediaAdMan'
      Origin = 'fpfuncifer.MediaAdMan'
      DisplayFormat = '#,###,##0.00'
    end
    object QrFPFunciFerDtIniPA: TDateField
      FieldName = 'DtIniPA'
      Origin = 'fpfuncifer.DtIniPA'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtFimPA: TDateField
      FieldName = 'DtFimPA'
      Origin = 'fpfuncifer.DtFimPA'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDiasFaltI: TIntegerField
      FieldName = 'DiasFaltI'
      Origin = 'fpfuncifer.DiasFaltI'
    end
    object QrFPFunciFerDiasFaltP: TIntegerField
      FieldName = 'DiasFaltP'
      Origin = 'fpfuncifer.DiasFaltP'
    end
    object QrFPFunciFerDiasGozar: TSmallintField
      FieldName = 'DiasGozar'
      Origin = 'fpfuncifer.DiasGozar'
    end
    object QrFPFunciFerDiasPecun: TSmallintField
      FieldName = 'DiasPecun'
      Origin = 'fpfuncifer.DiasPecun'
    end
    object QrFPFunciFerDtIniPecun: TDateField
      FieldName = 'DtIniPecun'
      Origin = 'fpfuncifer.DtIniPecun'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtFimPecun: TDateField
      FieldName = 'DtFimPecun'
      Origin = 'fpfuncifer.DtFimPecun'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtIniPG: TDateField
      FieldName = 'DtIniPG'
      Origin = 'fpfuncifer.DtIniPG'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtFimFeria: TDateField
      FieldName = 'DtFimFeria'
      Origin = 'fpfuncifer.DtFimFeria'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtAvisoFer: TDateField
      FieldName = 'DtAvisoFer'
      Origin = 'fpfuncifer.DtAvisoFer'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtRecPgto: TDateField
      FieldName = 'DtRecPgto'
      Origin = 'fpfuncifer.DtRecPgto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtSolPecun: TDateField
      FieldName = 'DtSolPecun'
      Origin = 'fpfuncifer.DtSolPecun'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtSolPrP13: TDateField
      FieldName = 'DtSolPrP13'
      Origin = 'fpfuncifer.DtSolPrP13'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtRetorno: TDateField
      FieldName = 'DtRetorno'
      Origin = 'fpfuncifer.DtRetorno'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'fpfuncifer.Lk'
    end
    object QrFPFunciFerDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'fpfuncifer.DataCad'
    end
    object QrFPFunciFerDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'fpfuncifer.DataAlt'
    end
    object QrFPFunciFerUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'fpfuncifer.UserCad'
    end
    object QrFPFunciFerUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'fpfuncifer.UserAlt'
    end
    object QrFPFunciFerDTINIPECUN_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTINIPECUN_TXT'
      Size = 8
      Calculated = True
    end
    object QrFPFunciFerDTFIMPECUN_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTFIMPECUN_TXT'
      Size = 8
      Calculated = True
    end
    object QrFPFunciFerDTSOLPECUN_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTSOLPECUN_TXT'
      Size = 8
      Calculated = True
    end
    object QrFPFunciFerDTSOLPRP13_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTSOLPRP13_TXT'
      Size = 8
      Calculated = True
    end
    object QrFPFunciFerPECUNIO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PECUNIO_TXT'
      Size = 1
      Calculated = True
    end
    object QrFPFunciFerDtFimPG: TDateField
      FieldName = 'DtFimPG'
      Origin = 'fpfuncifer.DtFimPG'
    end
    object QrFPFunciFerControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'fpfuncifer.Controle'
    end
  end
  object frxFeriasxxx: TfrxReport
    Version = '4.0.12'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39458.8377247801
    ReportOptions.LastChange = 39459.8728255208
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Picture1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <ImagemExiste> = True then'
      '  begin'
      '    Picture1.Visible := True;'
      '    Picture1.LoadFromFile(<ImagemCaminho>);'
      '  end else Picture1.Visible := False;'
      'end;'
      ''
      'procedure Line3OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure Memo54OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  Memo54.Visible := False;'
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 392
    Top = 121
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = frxDsFPFunci
        DataSetName = 'frxDsFPFunci'
      end
      item
        DataSet = frxDsFPFuncifer
        DataSetName = 'frxDsFPFuncifer'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000
      Width = 1000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210
      PaperHeight = 297
      PaperSize = 9
      LeftMargin = 10
      RightMargin = 10
      BottomMargin = 10
      object MasterData1: TfrxMasterData
        Height = 540.47243606
        Top = 18.89765
        Width = 718.1107
        RowCount = 2
        object Shape1: TfrxShapeView
          Left = 3.77948118
          Top = 34.01577
          Width = 710.55164
          Height = 120.94488189
          Frame.Width = 0.1
        end
        object Memo1: TfrxMemoView
          Left = 3.77948118
          Top = 34.01577
          Width = 616.06339
          Height = 22.67718
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'RECIBO DE FÉRIAS')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 7.55901118
          Top = 117.16543
          Width = 419.52783
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsFPFunci."Empresa"] - [frxDsFPFunci."NUMEEMPRESA"]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 623.62240118
          Top = 34.01577
          Width = 90.70867118
          Height = 120.94491118
          OnBeforePrint = 'Picture1OnBeforePrint'
          Frame.Width = 0.1
        end
        object Line1: TfrxLineView
          Left = 623.62240118
          Top = 34.01577
          Height = 120.94496
          Frame.Typ = [ftLeft]
          Frame.Width = 0.1
        end
        object Line2: TfrxLineView
          Left = 3.77948118
          Top = 117.16543
          Width = 619.84292
          Frame.Typ = [ftTop]
          Frame.Width = 0.1
        end
        object Memo2: TfrxMemoView
          Left = 7.55901118
          Top = 136.06308
          Width = 612.28386
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 7.55901118
          Top = 86.92919
          Width = 41.57483
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Código:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 430.86637118
          Top = 117.16543
          Width = 188.9765
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'CNPJ: [frxDsEndereco."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 7.55901118
          Top = 98.26778
          Width = 41.57483
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[FormatFloat('#39'0000'#39',<frxDsFPFunci."Funci">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 113.38585118
          Top = 86.92919
          Width = 102.04731
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Registro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 113.38585118
          Top = 98.26778
          Width = 102.04731
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsFPFunci."Registro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 7.55901118
          Top = 56.69295
          Width = 151.1812
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Nome do funcionário:')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 7.55901118
          Top = 68.03154
          Width = 427.08689
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsFPFunci."NUMEFUNCI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 377.95295118
          Top = 86.92919
          Width = 52.91342
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'C.B.O.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 377.95295118
          Top = 98.26778
          Width = 52.91342
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsFPFunci."CBO2002"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 434.64590118
          Top = 86.92919
          Width = 185.19678449
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Departamento:')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 434.64590118
          Top = 98.26778
          Width = 185.19678449
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsFPFunci."Depto"] - [frxDsFPFunci."NOMEDEPTO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 434.64590118
          Top = 56.69295
          Width = 185.19678449
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Função:')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 434.64590118
          Top = 68.03154
          Width = 185.19678449
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsFPFunci."Funcao"] - [frxDsFPFunci."NOMEFUNCAO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 52.91337118
          Top = 86.92919
          Width = 56.69295
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Identificador:')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 52.91337118
          Top = 98.26778
          Width = 56.69295
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[FormatFloat('#39'0000'#39',<frxDsFPFunci."Codigo">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 313.70094118
          Top = 86.92919
          Width = 60.47248
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Série:')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 313.70094118
          Top = 98.26778
          Width = 60.47248
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsFPFunci."SerieCTPS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 215.43316118
          Top = 86.92919
          Width = 94.48825
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Carteira de trabalho:')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 215.43316118
          Top = 98.26778
          Width = 94.48825
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsFPFunci."CTPS"]')
          ParentFont = False
          WordWrap = False
        end
        object Line9: TfrxLineView
          Left = 623.62240118
          Top = 34.01577
          Width = 90.70872
          Frame.Typ = [ftTop]
          Frame.Width = 0.1
        end
        object Shape2: TfrxShapeView
          Left = 3.77948118
          Top = 291.02381
          Width = 710.55139591
          Height = 219.21274
          Frame.Width = 0.1
        end
        object Memo56: TfrxMemoView
          Left = 11.33854118
          Top = 313.70099
          Width = 695.43352
          Height = 22.67718
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            
              '[frxDsEndereco."CIDADE"], [FormatDateTime('#39'dd" de "mmmm" de "yyy' +
              'y'#39',<frxDsFPFuncifer."DtSolPecun">)].')
          ParentFont = False
          WordWrap = False
        end
        object Shape3: TfrxShapeView
          Left = 3.77953
          Top = 154.96073
          Width = 710.55164
          Height = 136.06308
          Frame.Width = 0.1
        end
        object Memo4: TfrxMemoView
          Left = 11.33859
          Top = 162.51979
          Width = 695.43352
          Height = 75.5906
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haBlock
          Memo.UTF8 = (
            
              'O abaixo assinado requer a concessão do abono pecuniário de 1/' +
              '3 de suas férias, referente ao período aquisitivo de [frxDsFPF' +
              'uncifer."DtIniPA"] a [frxDsFPFuncifer."DtFimPA"] conforme facult' +
              'a o Artigo 143 da CLT - Consolidação das Leis do Trabalho.')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Top = 529.13384606
          Width = 718.11045591
          Height = 11.33859
          OnAfterPrint = 'Memo54OnAfterPrint'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDot
          Frame.Typ = [ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            
              'Deverá ser solicitado 15 dias antes do término do período aqu' +
              'isitivo.')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          Left = 396.8503937
          Top = 472.44125
          Width = 279.68503937
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Assinatura do empregador')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 37.79534882
          Top = 472.44125
          Width = 279.68503937
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Assinatura do funcionário')
          ParentFont = False
          WordWrap = False
        end
        object Line4: TfrxLineView
          Left = 3.77953
          Top = 359.05535
          Width = 710.55164
          Frame.Typ = [ftTop]
          Frame.Width = 0.1
        end
        object Line5: TfrxLineView
          Left = 359.055118110236
          Top = 359.05511811
          Height = 136.06308
          Frame.Typ = [ftLeft]
          Frame.Width = 0.1
        end
        object Memo6: TfrxMemoView
          Left = 396.85065
          Top = 377.953
          Width = 279.68522
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            
              'Ciente em [FormatDateTime('#39'dd/mm/yyyy'#39',<frxDsFPFuncifer."DtSolPe' +
              'cun">)].')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Left = 3.77953
          Top = 245.66945
          Width = 710.55164
          Frame.Typ = [ftTop]
        end
        object Memo26: TfrxMemoView
          Left = 11.33859
          Top = 260.78757
          Width = 695.43352
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            
              'PERÍODO DO ABONO PECUNIÁRIO: [frxDsFPFuncifer."DtIniPecun"] a ' +
              '[frxDsFPFuncifer."DtFimPecun"].')
          ParentFont = False
        end
      end
    end
  end
  object frxDsFPFuncifer: TfrxDBDataset
    UserName = 'frxDsFPFuncifer'
    CloseDataSource = False
    DataSet = QrFPFunciFer
    Left = 364
    Top = 121
  end
  object frxDsFPFunci: TfrxDBDataset
    UserName = 'frxDsFPFunci'
    CloseDataSource = False
    DataSet = QrFPFunci
    Left = 364
    Top = 93
  end
  object frxFerias: TfrxReport
    Version = '4.0.12'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39458.8377247801
    ReportOptions.LastChange = 39459.8728255208
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo58OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  Memo58.Visible := <VARF_ULTIMAFOLHA> > 0;'
      
        '  Memo58.Text := '#39'CONTINUA NA FOLHA N� '#39' + IntToStr(<VARF_ULTIMA' +
        'FOLHA>);'
      'end;'
      ''
      'procedure Picture1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <ImagemExiste> = True then'
      '  begin'
      '    Picture1.Visible := True;'
      '    Picture1.LoadFromFile(<ImagemCaminho>);'
      '  end else Picture1.Visible := False;'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxFolhaGetValue
    Left = 180
    Top = 120
    Datasets = <
      item
        DataSet = frxDsC
        DataSetName = 'frxDsC'
      end
      item
        DataSet = Dmod.frxDsControle
        DataSetName = 'frxDsControle'
      end
      item
        DataSet = Dmod.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = frxDsI
        DataSetName = 'frxDsI'
      end
      item
        DataSet = frxDsR
        DataSetName = 'frxDsR'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000
      Width = 1000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210
      PaperHeight = 297
      PaperSize = 9
      LeftMargin = 10
      RightMargin = 10
      BottomMargin = 10
      object MasterData1: TfrxMasterData
        Height = 289.13403646
        Top = 158.74026
        Width = 718.1107
        DataSet = frxDsC
        DataSetName = 'frxDsC'
        RowCount = 0
        object Shape1: TfrxShapeView
          Left = 3.77953
          Width = 710.55164
          Height = 120.944881889764
          Frame.Width = 0.1
        end
        object Memo1: TfrxMemoView
          Left = 3.77953
          Width = 563.14997
          Height = 22.67718
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'RECIBO DE FÉRIAS')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 7.55906
          Top = 22.6771800000001
          Width = 419.52783
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsC."Empresa"] - [frxDsEndereco."NOMEDONO"]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 623.62245
          Width = 90.70867118
          Height = 120.94491118
          OnBeforePrint = 'Picture1OnBeforePrint'
          Frame.Width = 0.1
        end
        object Line1: TfrxLineView
          Left = 623.62245
          Height = 120.94496
          Frame.Typ = [ftLeft]
          Frame.Width = 0.1
        end
        object Line2: TfrxLineView
          Left = 3.77953
          Top = 60.4724799999999
          Width = 619.84292
          Frame.Typ = [ftTop]
          Frame.Width = 0.1
        end
        object Memo2: TfrxMemoView
          Left = 7.55906
          Top = 41.5748300000001
          Width = 612.28386
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 7.55906
          Top = 60.4724800000001
          Width = 41.57483
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Código:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 430.86642
          Top = 22.6771799999999
          Width = 188.9765
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'CNPJ: [frxDsEndereco."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 7.55906
          Top = 71.8110699999999
          Width = 41.57483
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[FormatFloat('#39'0000'#39',<frxDsC."CodFunci">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 113.3859
          Top = 60.4724799999999
          Width = 102.04731
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Registro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 113.3859
          Top = 71.8110699999999
          Width = 102.04731
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsC."Registro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 7.55906
          Top = 90.7087199999999
          Width = 151.1812
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Nome do funcionário:')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 7.55906
          Top = 102.04731
          Width = 427.08689
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[frxDsC."NomeEnt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 377.953
          Top = 60.4724799999999
          Width = 52.91342
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'C.B.O.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 377.953
          Top = 71.8110699999999
          Width = 52.91342
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsC."CBO2002"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 434.64595
          Top = 60.4724799999999
          Width = 185.19678449
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Departamento:')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 434.64595
          Top = 71.8110699999999
          Width = 185.19678449
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsC."Depto"] - [frxDsC."NomeDepto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 434.64595
          Top = 90.7087199999999
          Width = 185.19678449
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Função:')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 434.64595
          Top = 102.04731
          Width = 185.19678449
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsC."Funcao"] - [frxDsC."NomeFuncao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 52.91342
          Top = 60.4724799999999
          Width = 56.69295
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Identificador:')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 52.91342
          Top = 71.8110699999999
          Width = 56.69295
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[FormatFloat('#39'0000'#39',<frxDsC."Entidade">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 313.70099
          Top = 60.4724799999999
          Width = 60.47248
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Admissão:')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 313.70099
          Top = 71.8110699999999
          Width = 60.47248
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsC."DataAdm"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 215.43321
          Top = 60.4724799999999
          Width = 94.48825
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Inscrição INSS:')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 215.43321
          Top = 71.8110699999999
          Width = 94.48825
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsC."CodINSS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 566.9295
          Top = 3.77952999999997
          Width = 52.91342
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Folha:[frxDsC."NumFolha"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 3.77953
          Top = 275.90569
          Width = 52.91338583
          Height = 13.22834646
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Código')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo31: TfrxMemoView
          Left = 56.69295
          Top = 275.90569
          Width = 317.48052
          Height = 13.22834646
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          Memo.UTF8 = (
            'Descrição')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo32: TfrxMemoView
          Left = 374.17347
          Top = 275.90569
          Width = 113.38582677
          Height = 13.22834646
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Referência')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo33: TfrxMemoView
          Left = 487.55937
          Top = 275.90569
          Width = 113.38582677
          Height = 13.22834646
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Bruto das férias')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo34: TfrxMemoView
          Left = 600.94527
          Top = 275.90569
          Width = 113.38582677
          Height = 13.22834646
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Descontos')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Line9: TfrxLineView
          Left = 623.62245
          Width = 90.70872
          Frame.Typ = [ftTop]
          Frame.Width = 0.1
        end
        object Shape3: TfrxShapeView
          Left = 3.77953
          Top = 124.72449
          Width = 710.55164
          Height = 86.92919
          Frame.Width = 0.1
        end
        object Memo4: TfrxMemoView
          Left = 3.77953
          Top = 124.72449
          Width = 710.55164
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'PERÍODOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Left = 3.77953
          Top = 143.62214
          Width = 109.60637
          Height = 22.67716535
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            'Período aquisitivo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 3.77953
          Top = 188.9765
          Width = 109.60637
          Height = 22.67716535
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            'Período do abono')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 3.77953
          Top = 166.29932
          Width = 109.60637
          Height = 22.67716535
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            'Período de gozo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 136.06308
          Top = 143.62214
          Width = 22.67705063
          Height = 22.67716535
          DataField = 'PA_INI_DD'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsC."PA_INI_DD"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 336.37817
          Top = 143.62214
          Width = 22.67716535
          Height = 22.67716535
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'a')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 336.37817
          Top = 166.29932
          Width = 22.67716535
          Height = 22.67716535
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'a')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 336.37817
          Top = 188.9765
          Width = 22.67716535
          Height = 22.67716535
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'a')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 578.26809
          Top = 143.62214
          Width = 136.06308
          Height = 22.67716535
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 578.26809
          Top = 188.9765
          Width = 136.06308
          Height = 22.67716535
          DataField = 'DIASPP_TXT'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            '[frxDsC."DIASPP_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 578.26809
          Top = 166.29932
          Width = 136.06308
          Height = 22.67716535
          DataField = 'DIASPG_TXT'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            '[frxDsC."DIASPG_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Left = 113.3859
          Top = 143.62214
          Width = 22.67716535
          Height = 22.67716535
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'de')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Left = 113.3859
          Top = 166.29932
          Width = 22.67716535
          Height = 22.67716535
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'de')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 113.3859
          Top = 188.9765
          Width = 22.67716535
          Height = 22.67716535
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'de')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 181.41744
          Top = 143.62214
          Width = 98.26765063
          Height = 22.67716535
          DataField = 'PA_INI_MM'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsC."PA_INI_MM"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Left = 302.3624
          Top = 143.62214
          Width = 34.01564063
          Height = 22.67716535
          DataField = 'PA_INI_AA'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            '[frxDsC."PA_INI_AA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 359.05535
          Top = 143.62214
          Width = 22.67705063
          Height = 22.67716535
          DataField = 'PA_FIM_DD'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsC."PA_FIM_DD"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Left = 404.40971
          Top = 143.62214
          Width = 98.26765063
          Height = 22.67716535
          DataField = 'PA_FIM_MM'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsC."PA_FIM_MM"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 525.35467
          Top = 143.62214
          Width = 34.01564063
          Height = 22.67716535
          DataField = 'PA_FIM_AA'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            '[frxDsC."PA_FIM_AA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 136.06308
          Top = 166.29932
          Width = 22.67705063
          Height = 22.67716535
          DataField = 'PG_INI_DD'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsC."PG_INI_DD"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 181.41744
          Top = 166.29932
          Width = 98.26765063
          Height = 22.67716535
          DataField = 'PG_INI_MM'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsC."PG_INI_MM"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          Left = 302.3624
          Top = 166.29932
          Width = 34.01564063
          Height = 22.67716535
          DataField = 'PG_INI_AA'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            '[frxDsC."PG_INI_AA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          Left = 359.05535
          Top = 166.29932
          Width = 22.67705063
          Height = 22.67716535
          DataField = 'PG_FIM_DD'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsC."PG_FIM_DD"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          Left = 404.40971
          Top = 166.29932
          Width = 98.26765063
          Height = 22.67716535
          DataField = 'PG_FIM_MM'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsC."PG_FIM_MM"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          Left = 525.35467
          Top = 166.29932
          Width = 34.01564063
          Height = 22.67716535
          DataField = 'PG_FIM_AA'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            '[frxDsC."PG_FIM_AA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          Left = 136.06308
          Top = 188.9765
          Width = 22.67705063
          Height = 22.67716535
          DataField = 'PP_INI_DD'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsC."PP_INI_DD"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          Left = 181.41744
          Top = 188.9765
          Width = 98.26765063
          Height = 22.67716535
          DataField = 'PP_INI_MM'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsC."PP_INI_MM"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          Left = 302.3624
          Top = 188.9765
          Width = 34.01564063
          Height = 22.67716535
          DataField = 'PP_INI_AA'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            '[frxDsC."PP_INI_AA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          Left = 359.05535
          Top = 188.9765
          Width = 22.67705063
          Height = 22.67716535
          DataField = 'PP_FIM_DD'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsC."PP_FIM_DD"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          Left = 404.40971
          Top = 188.9765
          Width = 98.26765063
          Height = 22.67716535
          DataField = 'PP_FIM_MM'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsC."PP_FIM_MM"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          Left = 525.35467
          Top = 188.9765
          Width = 34.01564063
          Height = 22.67716535
          DataField = 'PP_FIM_AA'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            '[frxDsC."PP_FIM_AA"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Left = 3.77953
          Top = 188.9765
          Width = 710.55164
          Frame.Typ = [ftTop]
          Frame.Width = 0.1
        end
        object Line5: TfrxLineView
          Left = 3.77953
          Top = 166.29932
          Width = 710.55164
          Frame.Typ = [ftTop]
          Frame.Width = 0.1
        end
        object Memo89: TfrxMemoView
          Left = 158.74026
          Top = 143.62214
          Width = 22.67716535
          Height = 22.67716535
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'de')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          Left = 158.74026
          Top = 166.29932
          Width = 22.67716535
          Height = 22.67716535
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'de')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          Left = 158.74026
          Top = 188.9765
          Width = 22.67716535
          Height = 22.67716535
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'de')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          Left = 279.68522
          Top = 143.62214
          Width = 22.67716535
          Height = 22.67716535
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'de')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo93: TfrxMemoView
          Left = 279.68522
          Top = 166.29932
          Width = 22.67716535
          Height = 22.67716535
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'de')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          Left = 279.68522
          Top = 188.9765
          Width = 22.67716535
          Height = 22.67716535
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'de')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          Left = 381.73253
          Top = 143.62214
          Width = 22.67716535
          Height = 22.67716535
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'de')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          Left = 381.73253
          Top = 166.29932
          Width = 22.67716535
          Height = 22.67716535
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'de')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          Left = 381.73253
          Top = 188.9765
          Width = 22.67716535
          Height = 22.67716535
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'de')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          Left = 502.67749
          Top = 143.62214
          Width = 22.67716535
          Height = 22.67716535
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'de')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo99: TfrxMemoView
          Left = 502.67749
          Top = 166.29932
          Width = 22.67716535
          Height = 22.67716535
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'de')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          Left = 502.67749
          Top = 188.9765
          Width = 22.67716535
          Height = 22.67716535
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'de')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          Left = 3.77953
          Top = 215.43321
          Width = 710.55164
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'BASE PARA CÁLCULO DA REMUNERAÇÃO DAS FÉRIAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          Left = 3.77953
          Top = 234.33086
          Width = 117.165354330709
          Height = 13.22834646
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Faltas não justificadas')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo103: TfrxMemoView
          Left = 120.94496
          Top = 234.33086
          Width = 117.16535433
          Height = 13.22834646
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Salário fixo')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo104: TfrxMemoView
          Left = 589.60668
          Top = 234.33086
          Width = 124.72441433
          Height = 13.22834646
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Salário base férias')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo105: TfrxMemoView
          Left = 238.11039
          Top = 234.33086
          Width = 117.16535433
          Height = 13.22834646
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Média horas extras')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo106: TfrxMemoView
          Left = 472.44125
          Top = 234.33086
          Width = 117.16535433
          Height = 13.22834646
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Média adicionais')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo107: TfrxMemoView
          Left = 355.27582
          Top = 234.33086
          Width = 117.16535433
          Height = 13.22834646
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Média comissões')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo108: TfrxMemoView
          Left = 3.77953
          Top = 247.55905512
          Width = 117.16535433
          Height = 13.22834646
          DataField = 'DiasFI'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsC."DiasFI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo109: TfrxMemoView
          Left = 589.60668
          Top = 247.55905512
          Width = 124.72441433
          Height = 13.22834646
          DataField = 'BaseCal'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsC."BaseCal"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo110: TfrxMemoView
          Left = 238.11039
          Top = 247.55905512
          Width = 117.16535433
          Height = 13.22834646
          DataField = 'MediaHE'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsC."MediaHE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo111: TfrxMemoView
          Left = 472.44125
          Top = 247.55905512
          Width = 117.16535433
          Height = 13.22834646
          DataField = 'MediaAd'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsC."MediaAd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo112: TfrxMemoView
          Left = 355.27582
          Top = 247.55905512
          Width = 117.16535433
          Height = 13.22834646
          DataField = 'MediaCo'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsC."MediaCo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo113: TfrxMemoView
          Left = 120.94496
          Top = 247.55905512
          Width = 117.16535433
          Height = 13.22834646
          DataField = 'SalFixo'
          DataSet = frxDsC
          DataSetName = 'frxDsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsC."SalFixo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object DetailData1: TfrxDetailData
        Height = 13.22834646
        Top = 472.44125
        Width = 718.1107
        DataSet = frxDsI
        DataSetName = 'frxDsI'
        RowCount = 0
        object Memo5: TfrxMemoView
          Left = 3.77953
          Width = 52.91338583
          Height = 13.22834646
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[FormatFloat('#39'0000;-0000; '#39',<frxDsI."CodiEvento">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          Left = 56.69295
          Width = 317.48052
          Height = 13.22834646
          DataField = 'NomeEvento'
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.1
          Memo.UTF8 = (
            '[frxDsI."NomeEvento"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo27: TfrxMemoView
          Left = 374.17347
          Width = 113.38582677
          Height = 13.22834646
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            
              '[FormatFloat('#39'#,###,#0.00;-#,###,#0.00; '#39',<frxDsI."Referencia">)' +
              ']')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo28: TfrxMemoView
          Left = 487.55937
          Width = 113.38582677
          Height = 13.22834646
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[FormatFloat('#39'#,###,#0.00;-#,###,#0.00; '#39',<frxDsI."ValorP">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo29: TfrxMemoView
          Left = 600.94527
          Width = 113.38582677
          Height = 13.22834646
          DataSet = frxDsI
          DataSetName = 'frxDsI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[FormatFloat('#39'#,###,#0.00;-#,###,#0.00; '#39',<frxDsI."ValorD">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 2.44E-6
        Top = 136.06308
        Width = 718.1107
        Condition = 'frxDsC."Controle"'
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 260.78757
        Top = 510.23655
        Width = 718.1107
        object Memo36: TfrxMemoView
          Left = 600.94527
          Width = 113.38585118
          Height = 17.00787402
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsR."TotalD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 600.94527
          Top = 17.00787402
          Width = 113.38585118
          Height = 17.00787402
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsR."TotalL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 3.77953
          Width = 483.77984
          Height = 34.01574803
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 487.55937
          Width = 113.38585118
          Height = 17.00787402
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsR."TotalP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 487.55937
          Top = 17.0078740157481
          Width = 113.3859
          Height = 17.00787402
          DataSetName = 'frxDsEve'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            'Valor líquido:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 3.77953
          Top = 41.5748300000001
          Width = 120.94488189
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Salário Base:')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          Left = 3.77953
          Top = 52.9134200000001
          Width = 120.94488189
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsR."Acum27"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo42: TfrxMemoView
          Left = 124.72449
          Top = 41.5748300000001
          Width = 120.94488189
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Salário contibuição INSS:')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          Left = 124.72449
          Top = 52.9134200000001
          Width = 120.94488189
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsR."Acum28"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          Left = 245.66945
          Top = 41.5748300000001
          Width = 120.94488189
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Salário cálculo FGTS:')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          Left = 245.66945
          Top = 52.9134200000001
          Width = 120.94488189
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsR."Acum17"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          Left = 366.61441
          Top = 41.5748300000001
          Width = 120.94488189
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Valor do FGTS:')
          ParentFont = False
          WordWrap = False
        end
        object Memo49: TfrxMemoView
          Left = 366.61441
          Top = 52.9134200000001
          Width = 120.94488189
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsR."Acum03"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          Left = 487.55937
          Top = 41.5748300000001
          Width = 120.94488189
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Base cálculo IRRF:')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          Left = 487.55937
          Top = 52.9134200000001
          Width = 120.94488189
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsR."Acum26"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          Left = 608.50433
          Top = 41.5748300000001
          Width = 105.8267863
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Faixa IRRF:')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          Left = 608.50433
          Top = 52.9134200000001
          Width = 105.8267863
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsR."Acum25"]')
          ParentFont = False
          WordWrap = False
        end
        object Shape2: TfrxShapeView
          Left = 3.77953
          Top = 75.5906000000001
          Width = 710.55139591
          Height = 109.60637
          Frame.Width = 0.1
        end
        object Memo54: TfrxMemoView
          Left = 3.77953
          Top = 75.5906000000001
          Width = 710.55139591
          Height = 75.5906
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            
              'Declaro ter recebido de [frxDsEndereco."NOMEDONO"],  estabelecid' +
              'a no endereço [frxDsEndereco."E_ALL"], a importância de [frxDs' +
              'Controle."Moeda"] [FormatFloat('#39'#,###,##0.00'#39',<frxDsR."TotalL">)' +
              '] ([frxDsR."EXTENSO_TotalL"]) que me é paga antecipadamente por' +
              ' motivo das minhas férias regulamentares, ora concedidas e que ' +
              'vou gozar de acordo com a descrição acima, tudo conforme o avi' +
              'so que recebi em tempo, no qual dei o meu "CIENTE".'
            
              '          Para clareza e documento, firmo o presente recibo, dan' +
              'do plena e geral quitação.')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 37.7953
          Top = 151.1812
          Width = 302.3624
          Height = 18.89765
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.1
          Memo.UTF8 = (
            '[frxDsEndereco."CIDADE"], [frxDsC."DTRECPGTO_TXT"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 37.7953
          Top = 170.07885
          Width = 302.3624
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            '')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          Left = 359.05535
          Top = 170.07885
          Width = 325.03958
          Height = 11.33859
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Assinatura do funcionário')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          Left = 3.77953
          Width = 710.55164
          Height = 185.19697
          Visible = False
          OnBeforePrint = 'Memo58OnBeforePrint'
          Color = clWhite
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'Continua na próxima folha')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 56.69295
        Top = 18.89765
        Width = 718.1107
      end
    end
  end
  object QrEvenXX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor '
      'FROM fpfolhaeve'
      'WHERE Evento=:P0'
      'AND Codigo=:P1 '
      '')
    Left = 296
    Top = 260
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEvenXXValor: TFloatField
      FieldName = 'Valor'
    end
  end
end


