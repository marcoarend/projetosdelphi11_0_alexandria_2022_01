object FmFPAcumu: TFmFPAcumu
  Left = 510
  Top = 211
  Caption = 'Acumuladores'
  ClientHeight = 283
  ClientWidth = 343
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 235
    Width = 343
    Height = 48
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 237
    ExplicitWidth = 351
    object Panel2: TPanel
      Left = 239
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object BtAcao: TBitBtn
      Tag = 294
      Left = 2
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&A'#231#227'o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtAcaoClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 343
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Acumuladores'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    ExplicitWidth = 351
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 347
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 343
    Height = 187
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 351
    ExplicitHeight = 189
    object dmkDBGridDAC1: TdmkDBGridDAC
      Left = 1
      Top = 1
      Width = 349
      Height = 187
      SQLFieldsToChange.Strings = (
        'Descricao')
      SQLIndexesOnUpdate.Strings = (
        'Codigo')
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ativo'
          Width = 17
          Visible = True
        end>
      Color = clWindow
      DataSource = DsFPAcumu
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dmkDBGridDAC1CellClick
      SQLTable = 'FPAcumu'
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ativo'
          Width = 17
          Visible = True
        end>
    end
  end
  object DsFPAcumu: TDataSource
    DataSet = QrFPAcumu
    Left = 168
    Top = 176
  end
  object QrFPAcumu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM fpacumu'
      '')
    Left = 140
    Top = 176
    object QrFPAcumuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPAcumuDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 30
    end
    object QrFPAcumuChave: TWideStringField
      FieldName = 'Chave'
    end
    object QrFPAcumuAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrFPAcumuVisivel: TSmallintField
      FieldName = 'Visivel'
    end
    object QrFPAcumuRegimeCxa: TSmallintField
      FieldName = 'RegimeCxa'
    end
    object QrFPAcumuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFPAcumuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFPAcumuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFPAcumuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFPAcumuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object PMAcao: TPopupMenu
    Left = 104
    Top = 249
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
  end
end

