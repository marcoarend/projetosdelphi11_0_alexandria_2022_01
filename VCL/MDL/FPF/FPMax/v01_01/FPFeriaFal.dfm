object FmFPFeraiFal: TFmFPFeraiFal
  Left = 510
  Top = 211
  Width = 359
  Height = 319
  Caption = 'Acumuladores'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 237
    Width = 351
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 239
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa�da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object BtAcao: TBitBtn
      Tag = 294
      Left = 2
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&A��o'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtAcaoClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 351
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Acumuladores'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 347
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 351
    Height = 189
    Align = alClient
    TabOrder = 0
    object dmkDBGridDAC1: TdmkDBGridDAC
      Left = 1
      Top = 1
      Width = 349
      Height = 187
      SQLFieldsToChange.Strings = (
        'Descricao'
        'Dias')
      SQLIndexesOnUpdate.Strings = (
        'Limite')
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Limite'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri��o'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Dias'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ativo'
          Width = 17
          Visible = True
        end>
      Color = clWindow
      DataSource = DsFPFeriaFal
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dmkDBGridDAC1CellClick
      SQLTable = 'FPFeriaFal'
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Limite'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri��o'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Dias'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ativo'
          Width = 17
          Visible = True
        end>
    end
  end
  object DsFPFeriaFal: TDataSource
    DataSet = QrFPFeriaFal
    Left = 168
    Top = 176
  end
  object QrFPFeriaFal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM fpferiafal'
      'ORDER BY Limite')
    Left = 140
    Top = 176
    object QrFPFeriaFalLimite: TSmallintField
      FieldName = 'Limite'
    end
    object QrFPFeriaFalDescricao: TWideStringField
      FieldName = 'Descricao'
    end
    object QrFPFeriaFalDias: TSmallintField
      FieldName = 'Dias'
    end
    object QrFPFeriaFalAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrFPFeriaFalLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFPFeriaFalDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFPFeriaFalDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFPFeriaFalUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFPFeriaFalUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object PMAcao: TPopupMenu
    Left = 108
    Top = 249
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
  end
end

