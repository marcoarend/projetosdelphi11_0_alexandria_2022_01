object FmFPPonto: TFmFPPonto
  Left = 241
  Top = 167
  Width = 1024
  Height = 546
  Caption = 'Ponto de Funcion�rios'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Ponto de Funcion�rios'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 930
      Height = 44
      Align = alClient
      Transparent = True
    end
    object LaTipo: TLabel
      Left = 932
      Top = 2
      Width = 82
      Height = 44
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1016
    Height = 464
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object dmkDBGrid1: TdmkDBGrid
      Left = 0
      Top = 0
      Width = 1016
      Height = 160
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMEEMP'
          Title.Caption = 'Empresa'
          Width = 330
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFUN'
          Title.Caption = 'Funcion�rio'
          Width = 330
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHE'
          Title.Caption = 'Data'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Horas'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Fator'
          Visible = True
        end>
      Color = clWindow
      DataSource = DsFPPontoHor
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMEEMP'
          Title.Caption = 'Empresa'
          Width = 330
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFUN'
          Title.Caption = 'Funcion�rio'
          Width = 330
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataHE'
          Title.Caption = 'Data'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Horas'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Fator'
          Visible = True
        end>
    end
    object PnTudo: TPanel
      Left = 0
      Top = 160
      Width = 1016
      Height = 304
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object Panel4: TPanel
        Left = 4
        Top = 328
        Width = 341
        Height = 89
        BevelOuter = bvNone
        TabOrder = 0
      end
      object Panel6: TPanel
        Left = 345
        Top = 336
        Width = 675
        Height = 81
        BevelOuter = bvNone
        TabOrder = 1
      end
      object Panel16: TPanel
        Left = 0
        Top = 0
        Width = 1016
        Height = 196
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        object Panel17: TPanel
          Left = 0
          Top = 0
          Width = 341
          Height = 196
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 341
            Height = 196
            Align = alClient
            TabOrder = 0
            object Label5: TLabel
              Left = 8
              Top = 8
              Width = 280
              Height = 13
              Caption = 'OBS.: Selecione a empresa antes do funcion�rio!'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label4: TLabel
              Left = 8
              Top = 24
              Width = 154
              Height = 13
              Caption = 'Empresa (Cadastro de entidade):'
            end
            object Label2: TLabel
              Left = 8
              Top = 64
              Width = 168
              Height = 13
              Caption = 'Funcion�rio (Cadastro de entidade):'
            end
            object EdEmpresa: TLMDEdit
              Left = 8
              Top = 39
              Width = 48
              Height = 21
              
              Caret.BlinkRate = 530
              CtlXP = False
              TabOrder = 0
              OnChange = EdEmpresaChange
              OnExit = EdEmpresaExit
              AutoSelect = True
              Alignment = taRightJustify
              CustomButtons = <>
              PasswordChar = #0
            end
            object CBEmpresa: TDBLookupComboBox
              Left = 58
              Top = 39
              Width = 275
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'NOMEEMPRESA'
              ListSource = DsEmpresa
              TabOrder = 1
              OnClick = CBEmpresaClick
              OnDropDown = CBEmpresaDropDown
            end
            object CBCodigo: TDBLookupComboBox
              Left = 58
              Top = 79
              Width = 275
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'NOMEFUNCI'
              ListSource = DsFunci
              TabOrder = 2
              OnClick = CBCodigoClick
              OnDropDown = CBCodigoDropDown
            end
            object EdCodigo: TLMDEdit
              Left = 8
              Top = 79
              Width = 48
              Height = 21
              
              Caret.BlinkRate = 530
              CtlXP = False
              TabOrder = 3
              OnChange = EdCodigoChange
              OnExit = EdCodigoExit
              AutoSelect = True
              Alignment = taRightJustify
              CustomButtons = <>
              PasswordChar = #0
            end
            object CkPonto: TCheckBox
              Left = 8
              Top = 104
              Width = 240
              Height = 17
              Caption = 'Somente funcion�rios com ponto controlado.'
              Checked = True
              State = cbChecked
              TabOrder = 4
              OnClick = CkPontoClick
            end
            object Panel7: TPanel
              Left = 1
              Top = 127
              Width = 339
              Height = 68
              Align = alBottom
              Caption = 'Panel7'
              TabOrder = 5
              object GroupBox1: TGroupBox
                Left = 1
                Top = 1
                Width = 192
                Height = 66
                Align = alLeft
                Caption = ' Per�odo de pesquisa: '
                TabOrder = 0
                object TPIni: TDateTimePicker
                  Left = 6
                  Top = 37
                  Width = 90
                  Height = 21
                  CalAlignment = dtaLeft
                  Date = 39481.4110429282
                  Time = 39481.4110429282
                  DateFormat = dfShort
                  DateMode = dmComboBox
                  Kind = dtkDate
                  ParseInput = False
                  TabOrder = 1
                end
                object TPFim: TDateTimePicker
                  Left = 98
                  Top = 37
                  Width = 90
                  Height = 21
                  CalAlignment = dtaLeft
                  Date = 39481.4110429282
                  Time = 39481.4110429282
                  DateFormat = dfShort
                  DateMode = dmComboBox
                  Kind = dtkDate
                  ParseInput = False
                  TabOrder = 3
                end
                object CkIni: TCheckBox
                  Left = 6
                  Top = 16
                  Width = 97
                  Height = 17
                  Caption = 'Data inicial:'
                  TabOrder = 0
                end
                object CkFim: TCheckBox
                  Left = 98
                  Top = 16
                  Width = 79
                  Height = 17
                  Caption = 'Data final:'
                  TabOrder = 2
                end
              end
              object GroupBox2: TGroupBox
                Left = 193
                Top = 1
                Width = 145
                Height = 66
                Align = alClient
                Caption = ' Somas da pesquisa atual: '
                TabOrder = 1
                object Label6: TLabel
                  Left = 6
                  Top = 20
                  Width = 62
                  Height = 13
                  Caption = 'Horas extras:'
                  FocusControl = DBEdit1
                end
                object Label7: TLabel
                  Left = 74
                  Top = 20
                  Width = 64
                  Height = 13
                  Caption = '= a x h norm.:'
                  FocusControl = DBEdit2
                end
                object DBEdit1: TDBEdit
                  Left = 6
                  Top = 36
                  Width = 64
                  Height = 21
                  DataField = 'Horas'
                  DataSource = DsSumHr
                  TabOrder = 0
                end
                object DBEdit2: TDBEdit
                  Left = 74
                  Top = 36
                  Width = 64
                  Height = 21
                  DataField = 'FatHr'
                  DataSource = DsSumHr
                  TabOrder = 1
                end
              end
            end
          end
        end
        object PnEdita1: TPanel
          Left = 341
          Top = 0
          Width = 675
          Height = 196
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          Visible = False
          object PnAlteracao: TPanel
            Left = 0
            Top = 0
            Width = 675
            Height = 45
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            Visible = False
            object Panel8: TPanel
              Left = 0
              Top = 0
              Width = 73
              Height = 45
              Align = alLeft
              TabOrder = 0
              object Label8: TLabel
                Left = 8
                Top = 4
                Width = 42
                Height = 13
                Caption = 'Controle:'
              end
              object dmkEdControle: TdmkEdit
                Left = 8
                Top = 20
                Width = 57
                Height = 21
                Alignment = taRightJustify
                Color = clInfoBk
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBackground
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ReadOnly = True
                TabOrder = 0
                FormatType = dmktfInteger
                DecimalSize = 0
                LeftZeros = 6
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
              end
            end
            object Panel12: TPanel
              Left = 73
              Top = 0
              Width = 602
              Height = 45
              Align = alClient
              TabOrder = 1
              object Label3: TLabel
                Left = 320
                Top = 4
                Width = 62
                Height = 13
                Caption = 'Horas extras:'
              end
              object dmkEdHoras: TdmkEdit
                Left = 320
                Top = 20
                Width = 68
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                DecimalSize = 2
                LeftZeros = 0
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
              end
              object RGFator: TRadioGroup
                Left = 133
                Top = 1
                Width = 183
                Height = 40
                Caption = ' Tipo de hora extra:'
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'Cadastro'
                  '100%')
                TabOrder = 1
              end
              object CkContinua: TCheckBox
                Left = 4
                Top = 4
                Width = 117
                Height = 17
                Caption = 'Continuar inserindo.'
                TabOrder = 2
              end
            end
          end
          object PnInclusao: TPanel
            Left = 0
            Top = 45
            Width = 675
            Height = 151
            Align = alClient
            Caption = '�rea de inclus�o / altera��o'
            TabOrder = 1
            Visible = False
            object Panel14: TPanel
              Left = 1
              Top = 1
              Width = 373
              Height = 149
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object LBTurnos: TDBLookupListBox
                Left = 0
                Top = 45
                Width = 373
                Height = 95
                Align = alClient
                KeyField = 'Controle'
                ListField = 'HORARIO'
                ListSource = DsFPFunciHor
                TabOrder = 0
                OnClick = LBTurnosClick
              end
              object Panel15: TPanel
                Left = 0
                Top = 0
                Width = 373
                Height = 45
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                object Label9: TLabel
                  Left = 188
                  Top = 4
                  Width = 55
                  Height = 13
                  Caption = 'Data inicial:'
                end
                object Label1: TLabel
                  Left = 4
                  Top = 4
                  Width = 65
                  Height = 13
                  Caption = 'Compet�ncia:'
                end
                object Label16: TLabel
                  Left = 72
                  Top = 5
                  Width = 22
                  Height = 12
                  Caption = 'pq'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Wingdings 3'
                  Font.Style = []
                  ParentFont = False
                end
                object TPDataIni: TDateTimePicker
                  Left = 188
                  Top = 20
                  Width = 85
                  Height = 21
                  CalAlignment = dtaLeft
                  Date = 39481.4110429282
                  Time = 39481.4110429282
                  DateFormat = dfShort
                  DateMode = dmComboBox
                  Kind = dtkDate
                  ParseInput = False
                  TabOrder = 0
                  OnChange = TPDataIniChange
                end
                object dmkEdPeriodo: TdmkEdit
                  Left = 4
                  Top = 20
                  Width = 36
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  DecimalSize = 0
                  LeftZeros = 0
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  OnChange = dmkEdPeriodoChange
                  OnKeyDown = dmkEdPeriodoKeyDown
                end
                object Edit1: TEdit
                  Left = 40
                  Top = 20
                  Width = 145
                  Height = 21
                  ReadOnly = True
                  TabOrder = 2
                  OnKeyDown = Edit1KeyDown
                end
              end
            end
            object Panel11: TPanel
              Left = 374
              Top = 1
              Width = 300
              Height = 149
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object Panel13: TPanel
                Left = 0
                Top = 0
                Width = 300
                Height = 149
                Align = alClient
                BevelOuter = bvNone
                Caption = 'Panel13'
                TabOrder = 0
                object PnCalenTitulo: TPanel
                  Left = 0
                  Top = 0
                  Width = 300
                  Height = 17
                  Align = alTop
                  BevelOuter = bvNone
                  Caption = 'M�s/Ano'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 0
                end
                object PnCalendar: TPanel
                  Left = 0
                  Top = 17
                  Width = 300
                  Height = 132
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Grade: TDrawGrid
                    Left = 0
                    Top = 0
                    Width = 300
                    Height = 132
                    Align = alRight
                    ColCount = 8
                    DefaultColWidth = 36
                    DefaultRowHeight = 17
                    FixedCols = 0
                    RowCount = 7
                    FixedRows = 0
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentFont = False
                    ScrollBars = ssNone
                    TabOrder = 0
                  end
                end
              end
            end
          end
        end
      end
      object Panel18: TPanel
        Left = 0
        Top = 256
        Width = 1016
        Height = 48
        Align = alBottom
        TabOrder = 3
        object Panel9: TPanel
          Left = 1
          Top = 1
          Width = 341
          Height = 46
          Align = alLeft
          TabOrder = 0
          object BtOK: TBitBtn
            Tag = 22
            Left = 16
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Pesquisa'
            TabOrder = 0
            OnClick = BtOKClick
            NumGlyphs = 2
          end
        end
        object Panel2: TPanel
          Left = 342
          Top = 1
          Width = 673
          Height = 46
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object PnConfirma: TPanel
            Left = 491
            Top = 0
            Width = 182
            Height = 46
            Align = alRight
            TabOrder = 0
            Visible = False
            object BtConfirma: TBitBtn
              Tag = 14
              Left = 20
              Top = 4
              Width = 90
              Height = 40
              Caption = '&Confirma'
              TabOrder = 0
              OnClick = BtConfirmaClick
              NumGlyphs = 2
            end
            object Panel3: TPanel
              Left = 70
              Top = 1
              Width = 111
              Height = 44
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object BtDesiste: TBitBtn
                Tag = 13
                Left = 2
                Top = 3
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Sai da janela atual'
                Caption = '&Desiste'
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtDesisteClick
                NumGlyphs = 2
              end
            end
          end
          object PnInclui: TPanel
            Left = 0
            Top = 0
            Width = 491
            Height = 46
            Align = alClient
            TabOrder = 1
            object BtInclui: TBitBtn
              Tag = 10
              Left = 16
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Inclui'
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtIncluiClick
              NumGlyphs = 2
            end
            object BtAltera: TBitBtn
              Tag = 11
              Left = 122
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Altera'
              Enabled = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtAlteraClick
              NumGlyphs = 2
            end
            object BtExclui: TBitBtn
              Tag = 12
              Left = 228
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Exclui'
              Enabled = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BtExcluiClick
              NumGlyphs = 2
            end
            object Panel10: TPanel
              Left = 379
              Top = 1
              Width = 111
              Height = 44
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 3
              object BtSaida: TBitBtn
                Tag = 13
                Left = 2
                Top = 3
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Sai da janela atual'
                Caption = '&Sa�da'
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtSaidaClick
                NumGlyphs = 2
              end
            end
          end
        end
      end
      object PnEdita2: TPanel
        Left = 0
        Top = 196
        Width = 1016
        Height = 60
        Align = alBottom
        TabOrder = 4
        object Panel36: TPanel
          Left = 1
          Top = 1
          Width = 1014
          Height = 60
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object GroupBox12: TGroupBox
            Left = 1
            Top = 1
            Width = 252
            Height = 58
            Align = alLeft
            Caption = ' Turno 1: '
            TabOrder = 0
            object Label133: TLabel
              Left = 8
              Top = 16
              Width = 55
              Height = 13
              Caption = 'Hora inicial:'
            end
            object Label134: TLabel
              Left = 68
              Top = 16
              Width = 55
              Height = 13
              Caption = 'Desc.inicio:'
            end
            object Label135: TLabel
              Left = 128
              Top = 16
              Width = 44
              Height = 13
              Caption = 'Desc.fim:'
            end
            object Label136: TLabel
              Left = 188
              Top = 16
              Width = 48
              Height = 13
              Caption = 'Hora final:'
            end
            object dmkEdTur1Ent: TdmkEdit
              Left = 8
              Top = 32
              Width = 57
              Height = 21
              TabOrder = 0
              FormatType = dmktfTime
              DecimalSize = 0
              LeftZeros = 0
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0d
              OnExit = dmkEdTur1EntExit
            end
            object dmkEdTur1DeI: TdmkEdit
              Left = 68
              Top = 32
              Width = 57
              Height = 21
              TabOrder = 1
              FormatType = dmktfTime
              DecimalSize = 0
              LeftZeros = 0
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0d
              OnExit = dmkEdTur1EntExit
            end
            object dmkEdTur1DeF: TdmkEdit
              Left = 128
              Top = 32
              Width = 57
              Height = 21
              TabOrder = 2
              FormatType = dmktfTime
              DecimalSize = 0
              LeftZeros = 0
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0d
              OnExit = dmkEdTur1EntExit
            end
            object dmkEdTur1Sai: TdmkEdit
              Left = 188
              Top = 32
              Width = 57
              Height = 21
              TabOrder = 3
              FormatType = dmktfTime
              DecimalSize = 0
              LeftZeros = 0
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0d
              OnExit = dmkEdTur1EntExit
            end
          end
          object Panel37: TPanel
            Left = 253
            Top = 1
            Width = 760
            Height = 58
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object Label142: TLabel
              Left = 8
              Top = 4
              Width = 49
              Height = 13
              Caption = 'H.trab.dia:'
            end
            object Label143: TLabel
              Left = 312
              Top = 4
              Width = 45
              Height = 13
              Caption = 'H. extras:'
            end
            object Label148: TLabel
              Left = 84
              Top = 4
              Width = 58
              Height = 13
              Caption = 'H.trab.noite:'
            end
            object Label149: TLabel
              Left = 236
              Top = 4
              Width = 59
              Height = 13
              Caption = 'H. banco h.:'
            end
            object LaTur1HrT: TLabel
              Left = 8
              Top = 44
              Width = 64
              Height = 13
              Alignment = taCenter
              AutoSize = False
              Caption = '00:00:00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object LaTur1HrE: TLabel
              Left = 312
              Top = 44
              Width = 64
              Height = 13
              Alignment = taCenter
              AutoSize = False
              Caption = '00:00:00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              Visible = False
            end
            object LaTur1HrN: TLabel
              Left = 84
              Top = 44
              Width = 64
              Height = 13
              Alignment = taCenter
              AutoSize = False
              Caption = '00:00:00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object LaTur1HrB: TLabel
              Left = 236
              Top = 44
              Width = 64
              Height = 13
              Alignment = taCenter
              AutoSize = False
              Caption = '00:00:00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              Visible = False
            end
            object Label152: TLabel
              Left = 74
              Top = 24
              Width = 6
              Height = 13
              Caption = '+'
            end
            object Label154: TLabel
              Left = 150
              Top = 24
              Width = 6
              Height = 13
              Caption = '='
            end
            object Label156: TLabel
              Left = 160
              Top = 4
              Width = 60
              Height = 13
              Caption = 'Total h.trab.:'
            end
            object LaTur1HrS: TLabel
              Left = 160
              Top = 44
              Width = 64
              Height = 13
              Alignment = taCenter
              AutoSize = False
              Caption = '00:00:00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label158: TLabel
              Left = 388
              Top = 4
              Width = 53
              Height = 13
              Caption = 'H. normais:'
            end
            object LaTur1HrC: TLabel
              Left = 388
              Top = 44
              Width = 64
              Height = 13
              Alignment = taCenter
              AutoSize = False
              Caption = '00:00:00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              Visible = False
            end
            object Label159: TLabel
              Left = 226
              Top = 24
              Width = 6
              Height = 13
              Caption = '='
            end
            object Label162: TLabel
              Left = 302
              Top = 24
              Width = 6
              Height = 13
              Caption = '+'
            end
            object Label163: TLabel
              Left = 378
              Top = 24
              Width = 6
              Height = 13
              Caption = '+'
            end
            object dmkEdTur1HrT: TdmkEdit
              Left = 8
              Top = 20
              Width = 64
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
              FormatType = dmktfTime
              DecimalSize = 0
              LeftZeros = 0
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfLong
              Texto = '00:00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0d
            end
            object dmkEdTur1HrE: TdmkEdit
              Left = 312
              Top = 20
              Width = 64
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 4
              FormatType = dmktfTime
              DecimalSize = 0
              LeftZeros = 0
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfLong
              Texto = '00:00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0d
            end
            object dmkEdTur1HrN: TdmkEdit
              Left = 84
              Top = 20
              Width = 64
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              FormatType = dmktfTime
              DecimalSize = 0
              LeftZeros = 0
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfLong
              Texto = '00:00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0d
            end
            object dmkEdTur1HrB: TdmkEdit
              Left = 236
              Top = 20
              Width = 64
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 3
              FormatType = dmktfTime
              DecimalSize = 0
              LeftZeros = 0
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfLong
              Texto = '00:00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0d
            end
            object dmkEdTur1HrS: TdmkEdit
              Left = 160
              Top = 20
              Width = 64
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
              FormatType = dmktfTime
              DecimalSize = 0
              LeftZeros = 0
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfLong
              Texto = '00:00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0d
            end
            object dmkEdTur1HrC: TdmkEdit
              Left = 388
              Top = 20
              Width = 64
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 5
              FormatType = dmktfTime
              DecimalSize = 0
              LeftZeros = 0
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfLong
              Texto = '00:00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0d
            end
            object Panel38: TPanel
              Left = 680
              Top = 0
              Width = 80
              Height = 58
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 6
              object Label144: TLabel
                Left = 8
                Top = 4
                Width = 53
                Height = 13
                Caption = 'H. aliment.:'
              end
              object LaTur1HrA: TLabel
                Left = 8
                Top = 44
                Width = 64
                Height = 13
                Alignment = taCenter
                AutoSize = False
                Caption = '00:00:00'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object dmkEdTur1HrA: TdmkEdit
                Left = 8
                Top = 20
                Width = 64
                Height = 21
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                FormatType = dmktfTime
                DecimalSize = 0
                LeftZeros = 0
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfLong
                Texto = '00:00:00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0d
              end
            end
            object Memo1: TMemo
              Left = 484
              Top = 0
              Width = 185
              Height = 57
              Lines.Strings = (
                'Memo1')
              TabOrder = 7
            end
          end
        end
      end
    end
  end
  object QrFunci: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, fu.HEFator, fu.Funci, fu.PriDdPonto,'
      'IF(en.Nome="", en.RazaoSocial, en.Nome) NOMEFUNCI,'
      'fu.ANMinHr, fu.ANIniHr, fu.ANFimHr'
      'FROM entidades en'
      'LEFT JOIN fpfunci fu ON fu.Codigo=en.Codigo'
      'WHERE (Fornece2="V" OR Fornece4="V")'
      'AND DataDemiss =0'
      'AND PriDdPonto>0'
      'AND (fu.Codigo IN ('
      '  SELECT Codigo FROM fpfunci'
      '  WHERE Empresa=1'
      '  AND DataDemiss=0))'
      'ORDER BY NOMEFUNCI')
    Left = 73
    Top = 9
    object QrFunciCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFunciHEFator: TFloatField
      FieldName = 'HEFator'
    end
    object QrFunciFunci: TIntegerField
      FieldName = 'Funci'
    end
    object QrFunciNOMEFUNCI: TWideStringField
      FieldName = 'NOMEFUNCI'
      Required = True
      Size = 100
    end
    object QrFunciPriDdPonto: TSmallintField
      FieldName = 'PriDdPonto'
    end
    object QrFunciANMinHr: TTimeField
      FieldName = 'ANMinHr'
    end
    object QrFunciANIniHr: TTimeField
      FieldName = 'ANIniHr'
    end
    object QrFunciANFimHr: TTimeField
      FieldName = 'ANFimHr'
    end
  end
  object DsFunci: TDataSource
    DataSet = QrFunci
    Left = 101
    Top = 9
  end
  object QrEmpresa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, '
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEEMPRESA'
      'FROM entidades en'
      'WHERE ???'
      'ORDER BY NOMEEMPRESA')
    Left = 132
    Top = 8
  end
  object DsEmpresa: TDataSource
    DataSet = QrEmpresa
    Left = 160
    Top = 8
  end
  object QrFPPontoHor: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrFPPontoHorAfterOpen
    BeforeClose = QrFPPontoHorBeforeClose
    SQL.Strings = (
      'SELECT pon.*, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,'
      'IF(fun.Tipo=0, fun.RazaoSocial, fun.Nome) NOMEFUN'
      ' '
      'FROM fppontohor pon'
      'LEFT JOIN entidades emp ON emp.Codigo=pon.Empresa'
      'LEFT JOIN entidades fun ON fun.Codigo=pon.Codigo')
    Left = 13
    Top = 9
    object QrFPPontoHorControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFPPontoHorCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFPPontoHorEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrFPPontoHorDataP: TDateField
      FieldName = 'DataP'
      Required = True
    end
    object QrFPPontoHorTur1Ent: TTimeField
      FieldName = 'Tur1Ent'
      Required = True
    end
    object QrFPPontoHorTur1DeI: TTimeField
      FieldName = 'Tur1DeI'
      Required = True
    end
    object QrFPPontoHorTur1DeF: TTimeField
      FieldName = 'Tur1DeF'
      Required = True
    end
    object QrFPPontoHorTur1Sai: TTimeField
      FieldName = 'Tur1Sai'
      Required = True
    end
    object QrFPPontoHorTur2Ent: TTimeField
      FieldName = 'Tur2Ent'
      Required = True
    end
    object QrFPPontoHorTur2DeI: TTimeField
      FieldName = 'Tur2DeI'
      Required = True
    end
    object QrFPPontoHorTur2DeF: TTimeField
      FieldName = 'Tur2DeF'
      Required = True
    end
    object QrFPPontoHorTur2Sai: TTimeField
      FieldName = 'Tur2Sai'
      Required = True
    end
    object QrFPPontoHorHorasANCar: TFloatField
      FieldName = 'HorasANCar'
      Required = True
    end
    object QrFPPontoHorHorasANCal: TFloatField
      FieldName = 'HorasANCal'
      Required = True
    end
    object QrFPPontoHorHorasHETot: TFloatField
      FieldName = 'HorasHETot'
      Required = True
    end
    object QrFPPontoHorHorasHEDia: TFloatField
      FieldName = 'HorasHEDia'
      Required = True
    end
    object QrFPPontoHorHorasHENot: TFloatField
      FieldName = 'HorasHENot'
      Required = True
    end
    object QrFPPontoHorHorasHEDom: TFloatField
      FieldName = 'HorasHEDom'
      Required = True
    end
    object QrFPPontoHorHorasHEFer: TFloatField
      FieldName = 'HorasHEFer'
      Required = True
    end
    object QrFPPontoHorHrBancoTra: TFloatField
      FieldName = 'HrBancoTra'
      Required = True
    end
    object QrFPPontoHorHrBancoGoz: TFloatField
      FieldName = 'HrBancoGoz'
      Required = True
    end
    object QrFPPontoHorHrFaltaJust: TFloatField
      FieldName = 'HrFaltaJust'
      Required = True
    end
    object QrFPPontoHorHrFaltaInju: TFloatField
      FieldName = 'HrFaltaInju'
      Required = True
    end
    object QrFPPontoHorFator: TFloatField
      FieldName = 'Fator'
      Required = True
    end
    object QrFPPontoHorLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFPPontoHorDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFPPontoHorDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFPPontoHorUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFPPontoHorUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFPPontoHorNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrFPPontoHorNOMEFUN: TWideStringField
      FieldName = 'NOMEFUN'
      Size = 100
    end
    object QrFPPontoHorPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrFPPontoHorHrAlimenta: TFloatField
      FieldName = 'HrAlimenta'
      Required = True
    end
  end
  object DsFPPontoHor: TDataSource
    DataSet = QrFPPontoHor
    Left = 41
    Top = 9
  end
  object QrSumHr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(pon.Horas) Horas, SUM(pon.Horas*pon.Fator/100) FatHr'
      ''
      'FROM fpponto pon'
      'LEFT JOIN entidades emp ON emp.Codigo=pon.Empresa'
      'LEFT JOIN entidades fun ON fun.Codigo=pon.Codigo'
      ''
      'WHERE DataHE  > -1000')
    Left = 192
    Top = 8
    object QrSumHrHoras: TFloatField
      FieldName = 'Horas'
      DisplayFormat = '0.00'
    end
    object QrSumHrFatHr: TFloatField
      FieldName = 'FatHr'
      DisplayFormat = '0.00'
    end
  end
  object DsSumHr: TDataSource
    DataSet = QrSumHr
    Left = 218
    Top = 9
  end
  object PMExclui: TPopupMenu
    Left = 638
    Top = 117
    object ExcluiitemAtual1: TMenuItem
      Caption = 'Exclui item &Atual'
      OnClick = ExcluiitemAtual1Click
    end
    object ExcluiitensSelecionados1: TMenuItem
      Caption = 'Exclui itens &Selecionados'
      OnClick = ExcluiitensSelecionados1Click
    end
    object ExcluiTodositens1: TMenuItem
      Caption = '&Exclui &Todos itens'
      OnClick = ExcluiTodositens1Click
    end
  end
  object QrFPFunciHor: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFPFunciHorCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM fpfuncihor'
      'WHERE Codigo=:P0')
    Left = 581
    Top = 115
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFPFunciHorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPFunciHorControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFPFunciHorTur1Ent: TTimeField
      FieldName = 'Tur1Ent'
    end
    object QrFPFunciHorTur1DeI: TTimeField
      FieldName = 'Tur1DeI'
    end
    object QrFPFunciHorTur1DeF: TTimeField
      FieldName = 'Tur1DeF'
    end
    object QrFPFunciHorTur1Sai: TTimeField
      FieldName = 'Tur1Sai'
    end
    object QrFPFunciHorTur2Ent: TTimeField
      FieldName = 'Tur2Ent'
    end
    object QrFPFunciHorTur2DeI: TTimeField
      FieldName = 'Tur2DeI'
    end
    object QrFPFunciHorTur2DeF: TTimeField
      FieldName = 'Tur2DeF'
    end
    object QrFPFunciHorTur2Sai: TTimeField
      FieldName = 'Tur2Sai'
    end
    object QrFPFunciHorLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFPFunciHorDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFPFunciHorDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFPFunciHorUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFPFunciHorUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFPFunciHorHORARIO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'HORARIO'
      Size = 100
      Calculated = True
    end
    object QrFPFunciHorTur1HrN: TTimeField
      FieldName = 'Tur1HrN'
    end
    object QrFPFunciHorTur1HrE: TTimeField
      FieldName = 'Tur1HrE'
    end
    object QrFPFunciHorTur1HrA: TTimeField
      FieldName = 'Tur1HrA'
    end
    object QrFPFunciHorTur2HrN: TTimeField
      FieldName = 'Tur2HrN'
    end
    object QrFPFunciHorTur2HrE: TTimeField
      FieldName = 'Tur2HrE'
    end
    object QrFPFunciHorTur2HrA: TTimeField
      FieldName = 'Tur2HrA'
    end
    object QrFPFunciHorTur1HrT: TTimeField
      FieldName = 'Tur1HrT'
    end
    object QrFPFunciHorTur1HrS: TTimeField
      FieldName = 'Tur1HrS'
    end
    object QrFPFunciHorTur1HrB: TTimeField
      FieldName = 'Tur1HrB'
    end
    object QrFPFunciHorTur2HrT: TTimeField
      FieldName = 'Tur2HrT'
    end
    object QrFPFunciHorTur2HrS: TTimeField
      FieldName = 'Tur2HrS'
    end
    object QrFPFunciHorTur2HrB: TTimeField
      FieldName = 'Tur2HrB'
    end
    object QrFPFunciHorTur1HrC: TTimeField
      FieldName = 'Tur1HrC'
    end
    object QrFPFunciHorTur2HrC: TTimeField
      FieldName = 'Tur2HrC'
    end
  end
  object DsFPFunciHor: TDataSource
    DataSet = QrFPFunciHor
    Left = 609
    Top = 115
  end
  object QrFeriados: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data '
      'FROM feriados'
      'WHERE Data BETWEEN :P0 AND :P1')
    Left = 248
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFeriadosData: TDateField
      FieldName = 'Data'
    end
  end
end


