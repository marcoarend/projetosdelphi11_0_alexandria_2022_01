object FmFPFolha: TFmFPFolha
  Left = 339
  Top = 185
  Caption = 'Folha de Pagamento'
  ClientHeight = 492
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 444
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 448
    ExplicitWidth = 792
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Inclui'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 680
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Folha de Pagamento'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    ExplicitWidth = 792
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 788
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 396
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 792
    ExplicitHeight = 400
  end
end
