unit FPImporta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Db, (*DBTables,*)
  ComCtrls, CheckLst, FileCtrl, IniFiles;

type
  TFmFPImporta = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Database1: TDatabase;
    TbEventos: TTable;
    TbEventosEmUso: TBooleanField;
    TbEventosEvento: TSmallintField;
    TbEventosDescricao: TWideStringField;
    TbEventosNatureza: TWideStringField;
    TbEventosBase: TSmallintField;
    TbEventosAuto: TBooleanField;
    TbEventosPercentual: TFloatField;
    TbEventosPrioridade: TSmallintField;
    TbEventosVisivel: TBooleanField;
    TbEventosInformacao: TBooleanField;
    TbEventosSalvar: TBooleanField;
    TbAcumuladores: TTable;
    TbAcumuladoresEmUso: TBooleanField;
    TbAcumuladoresAcumulador: TSmallintField;
    TbAcumuladoresDescricao: TWideStringField;
    TbAcumuladoresChave: TWideStringField;
    TbAcumuladoresVisivel: TBooleanField;
    TbAcumuladoresRegimeCaixa: TBooleanField;
    TbVariaveis: TTable;
    TbVariaveisEmUso: TBooleanField;
    TbVariaveisVariavel: TSmallintField;
    TbVariaveisNome: TWideStringField;
    TbVariaveisChave: TWideStringField;
    TbVariaveisVisivel: TBooleanField;
    TbVariaveisBase: TBooleanField;
    TbVariaveisPrioridade: TSmallintField;
    TbEventosAcu: TTable;
    TbEventosAcuEmUso: TBooleanField;
    TbEventosAcuEvento: TSmallintField;
    TbEventosAcuAcumulador: TSmallintField;
    TbEventosVar: TTable;
    TbEventosVarEmUso: TBooleanField;
    TbEventosVarEvento: TSmallintField;
    TbEventosVarVariavel: TSmallintField;
    TbtINSS: TTable;
    TbtINSSEmUso: TBooleanField;
    TbtINSSFaixa: TFloatField;
    TbtINSSAliquota: TFloatField;
    TbtINSSDeducao: TFloatField;
    TbtIRRF: TTable;
    TbtIRRFEmUso: TBooleanField;
    TbtIRRFTeto: TFloatField;
    TbtIRRFAliquota: TFloatField;
    TbtIRRFDeducao: TFloatField;
    TbParam: TTable;
    TbParamParametro: TSmallintField;
    TbParamDescricao: TWideStringField;
    TbParamValor: TFloatField;
    TbCBO2002: TTable;
    TbCBO2002EmUso: TBooleanField;
    TbCBO2002Codigo: TWideStringField;
    TbCBO2002Descricao: TWideMemoField;
    TbCategorias: TTable;
    TbCategoriasEmUso: TBooleanField;
    TbCategoriasCategoria: TSmallintField;
    TbCategoriasDescricao: TWideStringField;
    TbVinculos: TTable;
    TbVinculosEmUso: TBooleanField;
    TbVinculosVinculo: TSmallintField;
    TbVinculosDescricao: TWideMemoField;
    TbSituacoes: TTable;
    TbAdmissoes: TTable;
    TbAdmissoesEmUso: TBooleanField;
    TbAdmissoesAdmissao: TSmallintField;
    TbAdmissoesDescricao: TWideMemoField;
    TbAdmissoesAdmCaged: TSmallintField;
    TbSituacoesEmUso: TBooleanField;
    TbSituacoesSituacao: TSmallintField;
    TbSituacoesDescricao: TWideMemoField;
    TbTiposSalario: TTable;
    TbTiposSalarioEmUso: TBooleanField;
    TbTiposSalarioTipoSalario: TSmallintField;
    TbTiposSalarioDescricao: TWideStringField;
    TbDesligamentos: TTable;
    TbDesligamentosEmUso: TBooleanField;
    TbDesligamentosDesligamento: TSmallintField;
    TbDesligamentosDescricao: TWideMemoField;
    TbDesligamentosAvisoPrevio: TBooleanField;
    TbDesligamentosFeriasVencidas: TBooleanField;
    TbDesligamentosFeriasProporcionais: TBooleanField;
    TbDesligamentosRestringeFerias: TBooleanField;
    TbDesligamentosDecimoTerceiro: TBooleanField;
    TbDesligamentosMultaRecisao: TBooleanField;
    TbDesligamentosDesCaged: TSmallintField;
    TbTiposCalculo: TTable;
    TbTiposCalculoEmUso: TBooleanField;
    TbTiposCalculoTipo: TSmallintField;
    TbTiposCalculoDescricao: TWideStringField;
    TbEventosTip: TTable;
    TbEventosTipEmUso: TBooleanField;
    TbEventosTipTipo: TSmallintField;
    TbEventosTipEvento: TSmallintField;
    TbCBO1994: TTable;
    TbCBO1994EmUso: TBooleanField;
    TbCBO1994Cargo: TWideStringField;
    TbCBO1994Descricao: TWideMemoField;
    TbMunicipios: TTable;
    TbMunicipiosEmUso: TBooleanField;
    TbMunicipiosMunicipio: TWideStringField;
    TbMunicipiosNome: TWideStringField;
    TbMunicipiosFaixaCEP: TWideStringField;
    TbMunicipiosCEP: TTable;
    TbMunicipiosCEPEmUso: TBooleanField;
    TbMunicipiosCEPMunicipio: TWideStringField;
    TbMunicipiosCEPFaixaIni: TWideStringField;
    TbMunicipiosCEPFaixaFin: TWideStringField;
    TbNacionalidades: TTable;
    TbNacionalidadesEmUso: TBooleanField;
    TbNacionalidadesNacionalidade: TSmallintField;
    TbNacionalidadesDescricao: TWideStringField;
    TbGrausInstrucao: TTable;
    TbGrausInstrucaoEmUso: TBooleanField;
    TbGrausInstrucaoGrauInstrucao: TSmallintField;
    TbGrausInstrucaoDescricao: TWideMemoField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    PB1: TProgressBar;
    CLTabelas: TCheckListBox;
    Memo1: TMemo;
    Splitter1: TSplitter;
    MeInfo: TMemo;
    MeRegistros: TMemo;
    BtImportar: TBitBtn;
    OpenDialog1: TOpenDialog;
    Timer1: TTimer;
    CkLinha: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtImportarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    procedure ImportaDoAccess;
    procedure ImportaAcumuladores();
    procedure ImportaAdmissoes();
    procedure ImportaCategorias();
    procedure ImportaCBO1994();
    procedure ImportaCBO2002();
    procedure ImportaDesligamentos();
    procedure ImportaEventos();
    procedure ImportaEventosAcu();
    procedure ImportaEventosTip();
    procedure ImportaEventosVar();
    procedure ImportaVariaveis();
    procedure ImportaMunicipios();
    procedure ImportaGrausInstrucao();
    procedure ImportaMunicipiosCEPs();
    procedure ImportaNacionalidades();
    procedure ImportaTiposCalculo();
    procedure ImportaTiposSalario();
    procedure ImportaParametros();
    procedure ImportaSituacoes();
    procedure ImportaVinculos();
    //
    procedure ImportaDeArquivo(Fonte: String);
    procedure AtualizaFPAcumu;
    procedure AtualizaFPEvent;
    procedure AtualizaFPEventAcu;
    procedure AtualizaFPEventTip;
    procedure AtualizaFPEventVar;

  public
    { Public declarations }
    FExecutouAuto, FImportaFPImporta: Boolean;
  end;

  var
  FmFPImporta: TFmFPImporta;


  resourcestring
  sCaminho = 'C:\Dermatek\Temp\Folha';
  sArqTemp = 'C:\Dermatek\Temp\Folha\Importa.Txt';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmFPImporta.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPImporta.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if FImportaFPImporta then
  begin
    FImportaFPImporta := False;
    Timer1.Enabled := true;
  end;
end;

procedure TFmFPImporta.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFPImporta.ImportaAcumuladores();
begin
  Memo1.Text := 'Deletando acumuladores...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpacumu');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de acumuladores...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpacumu SET Descricao=:P0, Chave=:P1, ');
  Dmod.QrUpd.SQL.Add('Ativo=:P2, Visivel=:P3, RegimeCxa=:P4, ');
  Dmod.QrUpd.SQL.Add('Codigo=:Pa');
  Dmod.QrUpd.SQL.Add('');
  TbAcumuladores.Close;
  TbAcumuladores.Open;
  TbAcumuladores.First;
  PB1.Max := TbAcumuladores.RecordCount;
  while not TbAcumuladores.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsString  := TbAcumuladoresDescricao.Value;
    Dmod.QrUpd.Params[01].AsString  := TbAcumuladoresChave.Value;
    Dmod.QrUpd.Params[02].AsInteger := MLAGeral.BoolToInt(TbAcumuladoresEmUso.Value);
    Dmod.QrUpd.Params[03].AsInteger := MLAGeral.BoolToInt(TbAcumuladoresVisivel.Value);
    Dmod.QrUpd.Params[04].AsInteger := MLAGeral.BoolToInt(TbAcumuladoresRegimeCaixa.Value);
    Dmod.QrUpd.Params[05].AsInteger := TbAcumuladoresAcumulador.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbAcumuladores.Next;
  end;
  Memo1.Text := 'Importa��o de acumuladores finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaAdmissoes();
begin
  Memo1.Text := 'Deletando admiss�es...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpadmis');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de Admiss�es...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpadmis SET Descricao=:P0, Ativo=:P1, ');
  Dmod.QrUpd.SQL.Add('AdmCAGED=:P2, Codigo=:Pa');
  Dmod.QrUpd.SQL.Add('');
  TbAdmissoes.Close;
  TbAdmissoes.Open;
  TbAdmissoes.First;
  PB1.Max := TbAdmissoes.RecordCount;
  while not TbAdmissoes.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsString  := TbAdmissoesDescricao.Value;
    Dmod.QrUpd.Params[01].AsInteger := MLAGeral.BoolToInt(TbAdmissoesEmUso.Value);
    Dmod.QrUpd.Params[02].AsInteger := TbAdmissoesAdmCaged.Value;
    Dmod.QrUpd.Params[03].AsInteger := TbAdmissoesAdmissao.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbAdmissoes.Next;
  end;
  Memo1.Text := 'Importa��o de Admiss�es finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaCategorias();
begin
  Memo1.Text := 'Deletando categorias de funcion�rios...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpcateg');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de categorias de funcion�rios...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpcateg SET Descricao=:P0, Ativo=:P2, ');
  Dmod.QrUpd.SQL.Add('Codigo=:Pa');
  Dmod.QrUpd.SQL.Add('');
  TbCategorias.Close;
  TbCategorias.Open;
  TbCategorias.First;
  PB1.Max := TbCategorias.RecordCount;
  while not TbCategorias.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsString  := TbCategoriasDescricao.Value;
    Dmod.QrUpd.Params[01].AsInteger := MLAGeral.BoolToInt(TbCategoriasEmUso.Value);
    Dmod.QrUpd.Params[02].AsInteger := TbCategoriasCategoria.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbCategorias.Next;
  end;
  Memo1.Text := 'Importa��o de categorias de funcion�rios finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaCBO1994();
begin
  Memo1.Text := 'Deletando C.B.O. 1994...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpcbo94');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de C.B.O. 1994...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpcbo94 SET Descricao=:P0, Ativo=:P1, ');
  Dmod.QrUpd.SQL.Add('CodID=:Pa');
  Dmod.QrUpd.SQL.Add('');
  TbCBO1994.Close;
  TbCBO1994.Open;
  TbCBO1994.First;
  PB1.Max := TbCBO1994.RecordCount;
  while not TbCBO1994.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsString  := TbCBO1994Descricao.Value;
    Dmod.QrUpd.Params[01].AsInteger := MLAGeral.BoolToInt(TbCBO1994EmUso.Value);
    Dmod.QrUpd.Params[02].AsString  := TbCBO1994Cargo.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbCBO1994.Next;
  end;
  Memo1.Text := 'Importa��o de C.B.O. 1994 finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaCBO2002();
begin
  Memo1.Text := 'Deletando C.B.O. 2002...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpcbo02');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de C.B.O. 2002...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpcbo02 SET Descricao=:P0, Ativo=:P1, ');
  Dmod.QrUpd.SQL.Add('CodID=:Pa');
  Dmod.QrUpd.SQL.Add('');
  TbCBO2002.Close;
  TbCBO2002.Open;
  TbCBO2002.First;
  PB1.Max := TbCBO2002.RecordCount;
  while not TbCBO2002.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsString  := TbCBO2002Descricao.Value;
    Dmod.QrUpd.Params[01].AsInteger := MLAGeral.BoolToInt(TbCBO2002EmUso.Value);
    Dmod.QrUpd.Params[02].AsString  := TbCBO2002Codigo.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbCBO2002.Next;
  end;
  Memo1.Text := 'Importa��o de C.B.O. 2002 finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaDesligamentos();
begin
  Memo1.Text := 'Deletando dados de desligamentos...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpdeslg');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de deligamentos...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpdeslg SET Descricao=:P0, Ativo=:P1, ');
  Dmod.QrUpd.SQL.Add('AvisoPrevi=:P2, FeriasVenc=:P3, FeriasProp=:P4, ');
  Dmod.QrUpd.SQL.Add('FeriasRest=:P5, DecimoTerc=:P6, MultaResci=:P7, ');
  Dmod.QrUpd.SQL.Add('CodCAGEG=:P8, ');
  Dmod.QrUpd.SQL.Add('Codigo=:Pa');
  Dmod.QrUpd.SQL.Add('');
  TbDesligamentos.Close;
  TbDesligamentos.Open;
  TbDesligamentos.First;
  PB1.Max := TbDesligamentos.RecordCount;
  while not TbDesligamentos.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsString  := TbDesligamentosDescricao.Value;
    Dmod.QrUpd.Params[01].AsInteger := MLAGeral.BoolToInt(TbDesligamentosEmUso.Value);
    Dmod.QrUpd.Params[02].AsInteger := MLAGeral.BoolToInt(TbDesligamentosAvisoPrevio.Value);
    Dmod.QrUpd.Params[03].AsInteger := MLAGeral.BoolToInt(TbDesligamentosFeriasVencidas.Value);
    Dmod.QrUpd.Params[04].AsInteger := MLAGeral.BoolToInt(TbDesligamentosFeriasProporcionais.Value);
    Dmod.QrUpd.Params[05].AsInteger := MLAGeral.BoolToInt(TbDesligamentosRestringeFerias.Value);
    Dmod.QrUpd.Params[06].AsInteger := MLAGeral.BoolToInt(TbDesligamentosDecimoTerceiro.Value);
    Dmod.QrUpd.Params[07].AsInteger := MLAGeral.BoolToInt(TbDesligamentosMultaRecisao.Value);
    Dmod.QrUpd.Params[08].AsInteger := TbDesligamentosDesCaged.Value;
    Dmod.QrUpd.Params[09].AsInteger := TbDesligamentosDesligamento.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbDesligamentos.Next;
  end;
  Memo1.Text := 'Importa��o de Situa��es finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaEventos();
begin
  Memo1.Text := 'Deletando dados de eventos...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpevent');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de eventos...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpevent SET Descricao=:P0, Ativo=:P1, ');
  Dmod.QrUpd.SQL.Add('Natureza=:P2, Base=:P3, Auto=:P4, Percentual=:P5, ');
  Dmod.QrUpd.SQL.Add('Prioridade=:P6, Visivel=:P7, Informacao=:P8, ');
  Dmod.QrUpd.SQL.Add('Salvar=:P9, ');
  Dmod.QrUpd.SQL.Add('Codigo=:Pa');
  Dmod.QrUpd.SQL.Add('');
  TbEventos.Close;
  TbEventos.Open;
  TbEventos.First;
  PB1.Max := TbEventos.RecordCount;
  while not TbEventos.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsString  := TbEventosDescricao.Value;
    Dmod.QrUpd.Params[01].AsInteger := MLAGeral.BoolToInt(TbEventosEmUso.Value);
    Dmod.QrUpd.Params[02].AsString  := TbEventosNatureza.Value;
    Dmod.QrUpd.Params[03].AsInteger := TbEventosBase.Value;
    Dmod.QrUpd.Params[04].AsInteger := MLAGeral.BoolToInt(TbEventosAuto.Value);
    Dmod.QrUpd.Params[05].AsFloat   := TbEventosPercentual.Value;
    Dmod.QrUpd.Params[06].AsInteger := TbEventosPrioridade.Value;
    Dmod.QrUpd.Params[07].AsInteger := MLAGeral.BoolToInt(TbEventosVisivel.Value);
    Dmod.QrUpd.Params[08].AsInteger := MLAGeral.BoolToInt(TbEventosInformacao.Value);
    Dmod.QrUpd.Params[09].AsInteger := MLAGeral.BoolToInt(TbEventosSalvar.Value);
    Dmod.QrUpd.Params[10].AsInteger := TbEventosEvento.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbEventos.Next;
  end;
  Memo1.Text := 'Importa��o de eventos finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaEventosAcu();
var
  Cod_ID: String;
begin
  Memo1.Text := 'Deletando dados de eventos.acumuladores...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpeventacu');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de eventos.acumuladores...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpeventacu SET Event=:P0, Acumu=:P1, ');
  Dmod.QrUpd.SQL.Add('Ativo=:P2, Cod_ID=:P3 ');
  Dmod.QrUpd.SQL.Add('');
  TbEventosAcu.Close;
  TbEventosAcu.Open;
  TbEventosAcu.First;
  PB1.Max := TbEventosAcu.RecordCount;
  while not TbEventosAcu.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Cod_ID := FormatFloat('0', TbEventosAcuEvento.Value) + '.' +
      FormatFloat('0', TbEventosAcuAcumulador.Value);
    Dmod.QrUpd.Params[00].AsInteger := TbEventosAcuEvento.Value;
    Dmod.QrUpd.Params[01].AsInteger := TbEventosAcuAcumulador.Value;
    Dmod.QrUpd.Params[02].AsInteger := MLAGeral.BoolToInt(TbEventosAcuEmUso.Value);
    Dmod.QrUpd.Params[03].AsString  := Cod_ID;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbEventosAcu.Next;
  end;
  Memo1.Text := 'Importa��o de eventos.acumuladores finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaEventosTip();
var
  Cod_ID: String;
begin
  Memo1.Text := 'Deletando dados de eventos.tipos...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpeventtip');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de eventos.tipos...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpeventtip SET Event=:P0, Tipo=:P1, ');
  Dmod.QrUpd.SQL.Add('Ativo=:P2, Cod_ID=:P3 ');
  Dmod.QrUpd.SQL.Add('');
  TbEventosTip.Close;
  TbEventosTip.Open;
  TbEventosTip.First;
  PB1.Max := TbEventosTip.RecordCount;
  while not TbEventosTip.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Cod_ID := FormatFloat('0', TbEventosTipTipo.Value) + '.' +
      FormatFloat('0', TbEventosTipEvento.Value);
    Dmod.QrUpd.Params[00].AsInteger := TbEventosTipEvento.Value;
    Dmod.QrUpd.Params[01].AsInteger := TbEventosTipTipo.Value;
    Dmod.QrUpd.Params[02].AsInteger := MLAGeral.BoolToInt(TbEventosTipEmUso.Value);
    Dmod.QrUpd.Params[03].AsString  := Cod_ID;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbEventosTip.Next;
  end;
  Memo1.Text := 'Importa��o de eventos.acumuladores finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaEventosVar();
var
  Cod_ID: String;
begin
  Memo1.Text := 'Deletando dados de eventos.vari�veis...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpeventvar');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de eventos.vari�veis...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpeventvar SET Event=:P0, Varia=:P1, ');
  Dmod.QrUpd.SQL.Add('Ativo=:P2, Cod_ID=:P3 ');
  Dmod.QrUpd.SQL.Add('');
  TbEventosVar.Close;
  TbEventosVar.Open;
  TbEventosVar.First;
  PB1.Max := TbEventosVar.RecordCount;
  while not TbEventosVar.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Cod_ID := FormatFloat('0', TbEventosVarEvento.Value) + '.' +
      FormatFloat('0', TbEventosVarVariavel.Value);
    Dmod.QrUpd.Params[00].AsInteger := TbEventosVarEvento.Value;
    Dmod.QrUpd.Params[01].AsInteger := TbEventosVarVariavel.Value;
    Dmod.QrUpd.Params[02].AsInteger := MLAGeral.BoolToInt(TbEventosVarEmUso.Value);
    Dmod.QrUpd.Params[03].AsString  := Cod_ID;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbEventosVar.Next;
  end;
  Memo1.Text := 'Importa��o de eventos.vari�veis finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaGrausInstrucao();
begin
  Memo1.Text := 'Deletando dados de graus de instru��o...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpgraui');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de graus de instru��o...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpgraui SET Descricao=:P0, Ativo=:P1, ');
  Dmod.QrUpd.SQL.Add('Codigo=:Pa');
  Dmod.QrUpd.SQL.Add('');
  TbGrausInstrucao.Close;
  TbGrausInstrucao.Open;
  TbGrausInstrucao.First;
  PB1.Max := TbGrausInstrucao.RecordCount;
  while not TbGrausInstrucao.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsString  := TbGrausInstrucaoDescricao.Value;
    Dmod.QrUpd.Params[01].AsInteger := MLAGeral.BoolToInt(TbGrausInstrucaoEmUso.Value);
    Dmod.QrUpd.Params[02].AsInteger := TbGrausInstrucaoGrauInstrucao.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbGrausInstrucao.Next;
  end;
  Memo1.Text := 'Importa��o de graus de instru��o finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaMunicipios();
begin
  Memo1.Text := 'Deletando dados de munic�pios...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpmunic');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de munic�pios...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpmunic SET Nome=:P0, FaixaCEP=:P1, ');
  Dmod.QrUpd.SQL.Add('Ativo=:P2, CodID=:Pa');
  Dmod.QrUpd.SQL.Add('');
  TbMunicipios.Close;
  TbMunicipios.Open;
  TbMunicipios.First;
  PB1.Max := TbMunicipios.RecordCount;
  while not TbMunicipios.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsString  := TbMunicipiosNome.Value;
    Dmod.QrUpd.Params[01].AsString  := TbMunicipiosFaixaCEP.Value;
    Dmod.QrUpd.Params[02].AsInteger := MLAGeral.BoolToInt(TbMunicipiosEmUso.Value);
    Dmod.QrUpd.Params[03].AsString  := TbMunicipiosMunicipio.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbMunicipios.Next;
  end;
  Memo1.Text := 'Importa��o de munic�pios finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaMunicipiosCEPs();
begin
  Memo1.Text := 'Deletando dados de faixa de CEPs de munic�pios...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpmuniccep');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de faixa de CEPs de munic�pios...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpmuniccep SET FaixaIni=:P0, FaixaFim=:P1, ');
  Dmod.QrUpd.SQL.Add('Ativo=:P2, CodID=:Pa');
  Dmod.QrUpd.SQL.Add('');
  TbMunicipiosCEP.Close;
  TbMunicipiosCEP.Open;
  TbMunicipiosCEP.First;
  PB1.Max := TbMunicipiosCEP.RecordCount;
  while not TbMunicipiosCEP.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsString  := TbMunicipiosCEPFaixaIni.Value;
    Dmod.QrUpd.Params[01].AsString  := TbMunicipiosCEPFaixaFin.Value;
    Dmod.QrUpd.Params[02].AsInteger := MLAGeral.BoolToInt(TbMunicipiosCEPEmUso.Value);
    Dmod.QrUpd.Params[03].AsString  := TbMunicipiosCEPMunicipio.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbMunicipiosCEP.Next;
  end;
  Memo1.Text := 'Importa��o de faixa de CEPs de munic�pios finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaNacionalidades();
begin
  Memo1.Text := 'Deletando dados de nacionalidades...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpnacio');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de nacionalidades...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpnacio SET Descricao=:P0, Ativo=:P1,  ');
  Dmod.QrUpd.SQL.Add('Codigo=:Pa');
  Dmod.QrUpd.SQL.Add('');
  TbNacionalidades.Close;
  TbNacionalidades.Open;
  TbNacionalidades.First;
  PB1.Max := TbNacionalidades.RecordCount;
  while not TbNacionalidades.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsString  := TbNacionalidadesDescricao.Value;
    Dmod.QrUpd.Params[01].AsInteger := MLAGeral.BoolToInt(TbNacionalidadesEmUso.Value);
    Dmod.QrUpd.Params[02].AsInteger := TbNacionalidadesNacionalidade.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbNacionalidades.Next;
  end;
  Memo1.Text := 'Importa��o de nacionalidades finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaParametros();
begin
  Memo1.Text := 'Deletando dados de par�metros...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpparam');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando par�metros...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpparam SET Parametro=:P0, Descricao=:P1, ');
  Dmod.QrUpd.SQL.Add('Valor=:P2');
  Dmod.QrUpd.SQL.Add('');
  TbParam.Close;
  TbParam.Open;
  TbParam.First;
  PB1.Max := TbParam.RecordCount;
  while not TbParam.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsInteger := TbParamParametro.Value;
    Dmod.QrUpd.Params[01].AsString  := TbParamDescricao.Value;
    Dmod.QrUpd.Params[02].AsFloat   := TbParamValor.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbParam.Next;
  end;
  Memo1.Text := 'Importa��o de par�metros finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaSituacoes();
begin
  Memo1.Text := 'Deletando dados de situa��es...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpsitua');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de Situa��es...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpsitua SET Descricao=:P0, Ativo=:P1, ');
  Dmod.QrUpd.SQL.Add('Codigo=:Pa');
  Dmod.QrUpd.SQL.Add('');
  TbSituacoes.Close;
  TbSituacoes.Open;
  TbSituacoes.First;
  PB1.Max := TbSituacoes.RecordCount;
  while not TbSituacoes.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsString  := TbSituacoesDescricao.Value;
    Dmod.QrUpd.Params[01].AsInteger := MLAGeral.BoolToInt(TbSituacoesEmUso.Value);
    Dmod.QrUpd.Params[02].AsInteger := TbSituacoesSituacao.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbSituacoes.Next;
  end;
  Memo1.Text := 'Importa��o de Situa��es finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

{procedure TFmFPImporta.ImportaTabelaINSS();
begin
  Memo1.Text := 'Importando dados do INSS...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fptinss SET Faixa=:P0, Aliquota=:P1, ');
  Dmod.QrUpd.SQL.Add('Deducao=:P2, Ativo=:P3');
  Dmod.QrUpd.SQL.Add('');
  TbtINSS.Close;
  TbtINSS.Open;
  TbtINSS.First;
  PB1.Max := TbtINSS.RecordCount;
  while not TbtINSS.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsFloat   := TbtINSSFaixa.Value;
    Dmod.QrUpd.Params[01].AsFloat   := TbtINSSAliquota.Value;
    Dmod.QrUpd.Params[02].AsFloat   := TbtINSSDeducao.Value;
    Dmod.QrUpd.Params[03].AsInteger := MLAGeral.BoolToInt(TbtINSSEmUso.Value);
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbtINSS.Next;
  end;
  Memo1.Text := 'Importa��o de dados do INSS finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaTabelaIRRF();
begin
  Memo1.Text := 'Importando dados do IRRF...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fptirrf SET Teto=:P0, Aliquota=:P1, ');
  Dmod.QrUpd.SQL.Add('Deducao=:P2, Ativo=:P3');
  Dmod.QrUpd.SQL.Add('');
  TbtIRRF.Close;
  TbtIRRF.Open;
  TbtIRRF.First;
  PB1.Max := TbtIRRF.RecordCount;
  while not TbtIRRF.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsFloat   := TbtIRRFTeto.Value;
    Dmod.QrUpd.Params[01].AsFloat   := TbtIRRFAliquota.Value;
    Dmod.QrUpd.Params[02].AsFloat   := TbtIRRFDeducao.Value;
    Dmod.QrUpd.Params[03].AsInteger := MLAGeral.BoolToInt(TbtIRRFEmUso.Value);
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbtIRRF.Next;
  end;
  Memo1.Text := 'Importa��o de dados do IRRF finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;}

procedure TFmFPImporta.ImportaTiposCalculo();
begin
  Memo1.Text := 'Deletando dados de tipos de c�lculo...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fptpcal');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de tipos de c�lculo...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fptpcal SET Descricao=:P0, Ativo=:P1, ');
  Dmod.QrUpd.SQL.Add('Codigo=:P2 ');
  Dmod.QrUpd.SQL.Add('');
  TbTiposCalculo.Close;
  TbTiposCalculo.Open;
  TbTiposCalculo.First;
  PB1.Max := TbTiposCalculo.RecordCount;
  while not TbTiposCalculo.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsString  := TbTiposCalculoDescricao.Value;
    Dmod.QrUpd.Params[01].AsInteger := MLAGeral.BoolToInt(TbTiposCalculoEmUso.Value);
    Dmod.QrUpd.Params[02].AsInteger := TbTiposCalculoTipo.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbTiposCalculo.Next;
  end;
  Memo1.Text := 'Importa��o de dados de tipos de c�lculo finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaTiposSalario();
begin
  Memo1.Text := 'Deletando dados de sal�rio...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fptpsal');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de tipos de sal�rio...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fptpsal SET Descricao=:P0, Ativo=:P1, ');
  Dmod.QrUpd.SQL.Add('Codigo=:P2 ');
  Dmod.QrUpd.SQL.Add('');
  TbTiposSalario.Close;
  TbTiposSalario.Open;
  TbTiposSalario.First;
  PB1.Max := TbTiposSalario.RecordCount;
  while not TbTiposSalario.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsString  := TbTiposSalarioDescricao.Value;
    Dmod.QrUpd.Params[01].AsInteger := MLAGeral.BoolToInt(TbTiposSalarioEmUso.Value);
    Dmod.QrUpd.Params[02].AsInteger := TbTiposSalarioTipoSalario.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbTiposSalario.Next;
  end;
  Memo1.Text := 'Importa��o de dados de tipos de salario finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaVariaveis();
begin
  Memo1.Text := 'Deletando dados de vari�veis...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpvaria');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de vari�veis...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpvaria SET Nome=:P0, Chave=:P1, ');
  Dmod.QrUpd.SQL.Add('Ativo=:P2, Visivel=:P3, Base=:P4, Prioridade=:P5, ');
  Dmod.QrUpd.SQL.Add('Codigo=:Pa');
  Dmod.QrUpd.SQL.Add('');
  TbVariaveis.Close;
  TbVariaveis.Open;
  TbVariaveis.First;
  PB1.Max := TbVariaveis.RecordCount;
  while not TbVariaveis.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsString  := TbVariaveisNome.Value;
    Dmod.QrUpd.Params[01].AsString  := TbVariaveisChave.Value;
    Dmod.QrUpd.Params[02].AsInteger := MLAGeral.BoolToInt(TbVariaveisEmUso.Value);
    Dmod.QrUpd.Params[03].AsInteger := MLAGeral.BoolToInt(TbVariaveisVisivel.Value);
    Dmod.QrUpd.Params[04].AsInteger := MLAGeral.BoolToInt(TbVariaveisBase.Value);
    Dmod.QrUpd.Params[05].AsInteger := TbVariaveisPrioridade.Value;
    Dmod.QrUpd.Params[06].AsInteger := TbVariaveisVariavel.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbVariaveis.Next;
  end;
  Memo1.Text := 'Importa��o de vari�veis finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.ImportaVinculos();
begin
  Memo1.Text := 'Deletando dados de v�nculos empregat�cios...' + chr(13) + Chr(10) + Memo1.Text;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpvincu');
  Dmod.QrUpd.ExecSQL;
  //
  Memo1.Text := 'Importando dados de v�nculos empregat�cios...' + chr(13) + Chr(10) + Memo1.Text;
  PB1.Position := 0;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpvincu SET Descricao=:P0, Ativo=:P1, ');
  Dmod.QrUpd.SQL.Add('Codigo=:Pa');
  Dmod.QrUpd.SQL.Add('');
  TbVinculos.Close;
  TbVinculos.Open;
  TbVinculos.First;
  PB1.Max := TbVinculos.RecordCount;
  while not TbVinculos.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    Dmod.QrUpd.Params[00].AsString  := TbVinculosDescricao.Value;
    Dmod.QrUpd.Params[01].AsInteger := MLAGeral.BoolToInt(TbVinculosEmUso.Value);
    Dmod.QrUpd.Params[02].AsInteger := TbVinculosVinculo.Value;
    try
      Dmod.QrUpd.ExecSQL;
    finally
      ;
    end;
    TbVinculos.Next;
  end;
  Memo1.Text := 'Importa��o de vari�veis finalizada.' + chr(13) + Chr(10) + Memo1.Text;
end;

procedure TFmFPImporta.BtImportarClick(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0: if OpenDialog1.Execute then ImportaDeArquivo(OpenDialog1.FileName);
    1: ImportaDoAccess;
    else
  end;
end;

procedure TFmFPImporta.ImportaDoAccess;
var
  i: Integer;
begin
  for i := 0 to CLTabelas.Items.Count -1 do
  begin
    if CLTabelas.Checked[i] then
    begin
      if CLTabelas.Items[i] = 'Acumuladores'               then ImportaAcumuladores()   else
      if CLTabelas.Items[i] = 'Admiss�es'                  then ImportaAdmissoes()      else
      if CLTabelas.Items[i] = 'Categorias de funcion�rios' then ImportaCategorias()     else
      if CLTabelas.Items[i] = 'C.B.O. 2002'                then ImportaCBO2002()        else
      if CLTabelas.Items[i] = 'C.B.O. 1994'                then ImportaCBO1994()        else
      if CLTabelas.Items[i] = 'Desligamentos'              then ImportaDesligamentos()  else
      if CLTabelas.Items[i] = 'Eventos'                    then ImportaEventos()        else
      if CLTabelas.Items[i] = 'Eventos.acumuladores'       then ImportaEventosAcu()     else
      if CLTabelas.Items[i] = 'Eventos.tipos'              then ImportaEventosTip()     else
      if CLTabelas.Items[i] = 'Eventos.vari�veis'          then ImportaEventosVar()     else
      if CLTabelas.Items[i] = 'Graus de instru��o'         then ImportaGrausInstrucao() else
      if CLTabelas.Items[i] = 'Munic�pios'                 then ImportaMunicipios()     else
      if CLTabelas.Items[i] = 'Munic�pios - CEPs'          then ImportaMunicipiosCEPs() else
      if CLTabelas.Items[i] = 'Nacionalidades'             then ImportaNacionalidades() else
      if CLTabelas.Items[i] = 'Par�metros'                 then ImportaParametros()     else
      if CLTabelas.Items[i] = 'Situa��es'                  then ImportaSituacoes()      else
      //if CLTabelas.Items[i] = 'Tabela INSS'                then ImportaTabelaINSS()     else
      //if CLTabelas.Items[i] = 'Tabela IRRF'                then ImportaTabelaIRRF()     else
      if CLTabelas.Items[i] = 'Tipos de c�lculo'           then ImportaTiposCalculo()   else
      if CLTabelas.Items[i] = 'Tipos de sal�rio'           then ImportaTiposSalario()   else
      if CLTabelas.Items[i] = 'Vari�veis'                  then ImportaVariaveis()      else
      if CLTabelas.Items[i] = 'V�nculos empregat�cios'     then ImportaVinculos()       else
    end;
  end;
end;

procedure TFmFPImporta.ImportaDeArquivo(Fonte: String);
  procedure ImportaTabela(const LinhaIni: Integer; const Arquivo: TStringList;
    var Registros: TStringList);
  var
    k, z: Integer;
    x: String;
  begin
    x := '';
    z := LinhaIni;
    while x <> '<' do
    begin
      inc(z, 1);
      if z >= Arquivo.Count then Break;
      //if z >= MeImporta.Lines.Count then Break;
      x := Arquivo[z][1];
      //x := MeImporta.Lines[z][1];
    end;
    inc(z, -1);
    //MeRegistros.Lines.Clear;
    for k := LinhaIni+1 to z do
      //MeRegistros.Lines.Add(Arquivo[k]);
      Registros.Add(Arquivo[k]);
    if FileExists(sArqTemp) then
      DeleteFile(sArqTemp);
    Registros.SaveToFile(sArqTemp);
  end;
  //
var
  i, n, k, Versao: Integer;
  Tabela, x: String;
  Arquivo, Tabelas, Registros: TStringList;
  Continua: Boolean;
begin
  if not FileExists(Fonte) then
  begin
    Application.MessageBox(PChar('O arquivo "' + Fonte +
    '" n�o pode ser localizado!'), 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    ForceDirectories(sCaminho);
    Arquivo := TStringList.Create;
    try
      //MeImporta.Lines.LoadFromFile(Fonte);
      Arquivo.LoadFromFile(Fonte);
      Continua := False;
      k := pos('=', Arquivo[0]);
      if k > 2 then
      begin
        x := Copy(Arquivo[0], 1, k-1);
        Continua := Uppercase(x) = 'FOLHADEPAGAMENTO';
      end;
      if not Continua then
      begin
        //Arquivo.Free;
        Application.MessageBox('O arquivo selecionado n�o � v�lido!',
        'Erro', MB_OK+MB_ICONERROR);
        Screen.Cursor := crDefault;
        Exit;
      end else begin
        Versao := Geral.IMV(Copy(Arquivo[0], 8, Length(Arquivo[0])-7));
        if Versao < Dmod.QrControleVerBcoTabs.Value then
        begin
          if Application.MessageBox(PChar('A vers�o do arquivo selecionado � ' +
          'anterior ao da instalada! Deseja continuar assim mesmo?'),
          'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
          begin
            Screen.Cursor := crDefault;
            Exit;
          end;
        end;
      end;

      Tabelas := TStringList.Create;
      //MeTabelas.Lines.Clear;
      try
        //for i := 1 to MeImporta.Lines.Count -1 do
        for i := 1 to Arquivo.Count -1 do
        // Arquivo[0] => Versao do arquivo
        begin
          //if MeImporta.Lines[i][1] = '<' then
          if Arquivo[i][1] = '<' then
          begin
            //Tabela := Copy(MeImporta.Lines[i], 2, Length(MeImporta.Lines[i])-2);
            Tabela := Copy(Arquivo[i], 2, Length(Arquivo[i])-2);
            Tabelas.Add(Tabela+'='+FormatFloat('0', i));
          end;
        end;
        if Tabelas.Count > 0 then
        begin
          Registros := TStringList.Create;
          try
            MeInfo.Lines.Clear;
            for i := 0 to Tabelas.Count -1 do
            begin
              n := pos('=', Tabelas[i]);
              Tabela := Copy(Tabelas[i], 1, n-1);
              n := StrToInt(Copy(Tabelas[i], n+1, Length(Tabelas[i])-n));
              //ShowMessage(Tabela + ' - ' +IntToStr(n));
              //
              Registros.Clear;
              ImportaTabela(n, Arquivo, Registros);
              //
              if Uppercase(Tabela) = 'FPACUMU'    then AtualizaFPAcumu    else
              if Uppercase(Tabela) = 'FPEVENT'    then AtualizaFPEvent    else
              if Uppercase(Tabela) = 'FPEVENTACU' then AtualizaFPEventAcu else
              if Uppercase(Tabela) = 'FPEVENTTIP' then AtualizaFPEventTip else
              if Uppercase(Tabela) = 'FPEVENTVAR' then AtualizaFPEventVar else
              Application.MessageBox(PChar('A importa��o da tabela "' + Tabela +
              '" n�o est� implementada!'), 'Aviso', MB_OK+MB_ICONWARNING);
              //
            end;
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('UPDATE controle SET VerSalTabs=:P0');
            Dmod.QrUpd.Params[0].AsInteger := Versao;
            Dmod.QrUpd.ExecSQL;
            Dmod.QrControle.Close;
            Dmod.QrControle.Open;
            //
            Application.MessageBox(
            'Fim da importa��o de dados de folha de pagamento!', 'Informa��o',
            MB_OK+MB_ICONINFORMATION);
          finally
            Registros.Free;
          end;
        end;
      finally
        Tabelas.Free;
      end;
    finally
      Arquivo.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  if FExecutouAuto then Close;
end;

procedure TFmFPImporta.AtualizaFPAcumu;
var
  MemFile: TMemIniFile;
  RegItens: TStringList;
  i: Integer;
  //
  Codigo, Ativo, Visivel, RegimeCxa: Integer;
  Descricao, Chave: String;
  //Percentual: Double;
begin
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('DELETE FROM fpacumu WHERE Codigo <= 500');
  Dmod.QrUpdU.ExecSQL;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO fpacumu SET ');
  Dmod.QrUpdU.SQL.Add('Codigo=:P0, Descricao=:P1, Ativo=:P2, Chave=:P3, ');
  Dmod.QrUpdU.SQL.Add('Visivel=:P4, RegimeCxa=:P5');
  //
  MemFile := TMemIniFile.Create(sArqTemp);
  try
    RegItens := TStringList.Create;
    try
      MemFile.ReadSections(RegItens);
      //MemFile.ReadSections(MeTeste.Lines);
      for i := 0 to RegItens.Count - 1 do
      begin
        //MemFile.ReadSection(RegItens[i], MeTabelas.Lines);
        if CkLinha.Checked then
          MemFile.ReadSectionValues(RegItens[i], MeRegistros.Lines);
        Codigo     := MemFile.ReadInteger(RegItens[i], 'Codigo'     , 0);
        Ativo      := MemFile.ReadInteger(RegItens[i], 'Ativo'      , 0);
        Visivel    := MemFile.ReadInteger(RegItens[i], 'Visivel'    , 0);
        RegimeCxa  := MemFile.ReadInteger(RegItens[i], 'RegimeCxa'  , 0);
        //
        Descricao  := MemFile.ReadString (RegItens[i], 'Descricao'  , '');
        Chave      := MemFile.ReadString (RegItens[i], 'Chave'      , '');
        //
        Dmod.QrUpdU.Params[00].AsInteger := Codigo;
        Dmod.QrUpdU.Params[01].AsString  := Descricao;
        Dmod.QrUpdU.Params[02].AsInteger := Ativo;
        Dmod.QrUpdU.Params[03].AsString  := Chave;
        Dmod.QrUpdU.Params[04].AsInteger := Visivel;
        Dmod.QrUpdU.Params[05].AsInteger := RegimeCxa;
        //
        Dmod.QrUpdU.ExecSQL;
        //
        if CkLinha.Checked then
        MeInfo.Lines.Add('O acumulador  ' +
        FormatFloat('000', Codigo) + '.' + Descricao + ' foi inclu�do com sucesso');
      end;
      if not CkLinha.Checked then
        MeInfo.Lines.Add('Tabela de acumuladores importada com sucesso');
    finally
      RegItens.Free;
    end;
  finally
    MemFile.Free;
  end;
end;

procedure TFmFPImporta.AtualizaFPEvent;
var
  MemFile: TMemIniFile;
  RegItens: TStringList;
  i: Integer;
  //
  Codigo, Ativo, Base, Auto, Prioridade, Visivel, Informacao, Salvar: Integer;
  Percentual, Descricao, Natureza: String;
  //Percentual: Double;
begin
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('DELETE FROM fpevent WHERE Codigo <= 500');
  Dmod.QrUpdU.ExecSQL;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO fpevent SET ');
  Dmod.QrUpdU.SQL.Add('Codigo=:P0, Descricao=:P1, Ativo=:P2, Natureza=:P3, ');
  Dmod.QrUpdU.SQL.Add('Base=:P4, Auto=:P5, Percentual=:P6, Prioridade=:P7, ');
  Dmod.QrUpdU.SQL.Add('Visivel=:P8, Informacao=:P9, Salvar=:P10');
  //
  MemFile := TMemIniFile.Create(sArqTemp);
  try
    RegItens := TStringList.Create;
    try
      MemFile.ReadSections(RegItens);
      //MemFile.ReadSections(MeTeste.Lines);
      for i := 0 to RegItens.Count - 1 do
      begin
        //MemFile.ReadSection(RegItens[i], MeTabelas.Lines);
        if CkLinha.Checked then
          MemFile.ReadSectionValues(RegItens[i], MeRegistros.Lines);
        Codigo     := MemFile.ReadInteger(RegItens[i], 'Codigo'     , 0);
        Ativo      := MemFile.ReadInteger(RegItens[i], 'Ativo'      , 0);
        Base       := MemFile.ReadInteger(RegItens[i], 'Base'       , 0);
        Auto       := MemFile.ReadInteger(RegItens[i], 'Auto'       , 0);
        Prioridade := MemFile.ReadInteger(RegItens[i], 'Prioridade' , 0);
        Visivel    := MemFile.ReadInteger(RegItens[i], 'Visivel'    , 0);
        Informacao := MemFile.ReadInteger(RegItens[i], 'Informacao' , 0);
        Salvar     := MemFile.ReadInteger(RegItens[i], 'Salvar'     , 0);
        //
        Descricao  := MemFile.ReadString (RegItens[i], 'Descricao'  , '');
        Natureza   := MemFile.ReadString (RegItens[i], 'Natureza'   , '');
        Percentual := MemFile.ReadString (RegItens[i], 'Percentual' , '');
        //
        Dmod.QrUpdU.Params[00].AsInteger := Codigo;
        Dmod.QrUpdU.Params[01].AsString  := Descricao;
        Dmod.QrUpdU.Params[02].AsInteger := Ativo;
        Dmod.QrUpdU.Params[03].AsString  := Natureza;
        Dmod.QrUpdU.Params[04].AsInteger := Base;
        Dmod.QrUpdU.Params[05].AsInteger := Auto;
        Dmod.QrUpdU.Params[06].AsString  := Percentual;
        Dmod.QrUpdU.Params[07].AsInteger := Prioridade;
        Dmod.QrUpdU.Params[08].AsInteger := Visivel;
        Dmod.QrUpdU.Params[09].AsInteger := Informacao;
        Dmod.QrUpdU.Params[10].AsInteger := Salvar;
        //
        Dmod.QrUpdU.ExecSQL;
        //
        if CkLinha.Checked then
        MeInfo.Lines.Add('O evento  ' +
        FormatFloat('000', Codigo) + '.' + Descricao + ' foi inclu�do com sucesso');
      end;
      if not CkLinha.Checked then
        MeInfo.Lines.Add('Tabela de eventos importada com sucesso');
    finally
      RegItens.Free;
    end;
  finally
    MemFile.Free;
  end;
end;

procedure TFmFPImporta.AtualizaFPEventAcu;
var
  MemFile: TMemIniFile;
  RegItens: TStringList;
  i: Integer;
  //
  Event, Acumu, Ativo: Integer;
  Cod_ID: String;
begin
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('DELETE FROM fpeventacu ');
  Dmod.QrUpdU.ExecSQL;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO fpeventacu SET ');
  Dmod.QrUpdU.SQL.Add('Event=:P0, Acumu=:P1, Ativo=:P2, Cod_ID=:P3');
  //
  MemFile := TMemIniFile.Create(sArqTemp);
  try
    RegItens := TStringList.Create;
    try
      MemFile.ReadSections(RegItens);
      //MemFile.ReadSections(MeTeste.Lines);
      for i := 0 to RegItens.Count - 1 do
      begin
        //MemFile.ReadSection(RegItens[i], MeTabelas.Lines);
        if CkLinha.Checked then
          MemFile.ReadSectionValues(RegItens[i], MeRegistros.Lines);
        Event      := MemFile.ReadInteger(RegItens[i], 'Event'      , 0);
        Acumu      := MemFile.ReadInteger(RegItens[i], 'Acumu'       , 0);
        Ativo      := MemFile.ReadInteger(RegItens[i], 'Ativo'      , 0);
        //
        Cod_ID     := MemFile.ReadString (RegItens[i], 'Cod_ID'  , '');
        //
        Dmod.QrUpdU.Params[00].AsInteger := Event;
        Dmod.QrUpdU.Params[01].AsInteger := Acumu;
        Dmod.QrUpdU.Params[02].AsInteger := Ativo;
        Dmod.QrUpdU.Params[03].AsString  := Cod_ID;
        //
        Dmod.QrUpdU.ExecSQL;
        //
        if CkLinha.Checked then
        MeInfo.Lines.Add('O acumulador de evento  ' +
        Cod_ID + ' foi inclu�do com sucesso');
      end;
      if not CkLinha.Checked then
        MeInfo.Lines.Add('Tabela de acumuladores de eventos importada com sucesso');
    finally
      RegItens.Free;
    end;
  finally
    MemFile.Free;
  end;
end;

procedure TFmFPImporta.AtualizaFPEventTip;
var
  MemFile: TMemIniFile;
  RegItens: TStringList;
  i: Integer;
  //
  Event, Tipo, Ativo: Integer;
  Cod_ID: String;
begin
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('DELETE FROM fpeventtip ');
  Dmod.QrUpdU.ExecSQL;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO fpeventtip SET ');
  Dmod.QrUpdU.SQL.Add('Event=:P0, Tipo=:P1, Ativo=:P2, Cod_ID=:P3');
  //
  MemFile := TMemIniFile.Create(sArqTemp);
  try
    RegItens := TStringList.Create;
    try
      MemFile.ReadSections(RegItens);
      //MemFile.ReadSections(MeTeste.Lines);
      for i := 0 to RegItens.Count - 1 do
      begin
        //MemFile.ReadSection(RegItens[i], MeTabelas.Lines);
        if CkLinha.Checked then
          MemFile.ReadSectionValues(RegItens[i], MeRegistros.Lines);
        Event      := MemFile.ReadInteger(RegItens[i], 'Event'      , 0);
        Tipo       := MemFile.ReadInteger(RegItens[i], 'Tipo'       , 0);
        Ativo      := MemFile.ReadInteger(RegItens[i], 'Ativo'      , 0);
        //
        Cod_ID     := MemFile.ReadString (RegItens[i], 'Cod_ID'  , '');
        //
        Dmod.QrUpdU.Params[00].AsInteger := Event;
        Dmod.QrUpdU.Params[01].AsInteger := Tipo;
        Dmod.QrUpdU.Params[02].AsInteger := Ativo;
        Dmod.QrUpdU.Params[03].AsString  := Cod_ID;
        //
        Dmod.QrUpdU.ExecSQL;
        //
        if CkLinha.Checked then
        MeInfo.Lines.Add('O tipo de evento  ' +
        Cod_ID + ' foi inclu�do com sucesso');
      end;
      if not CkLinha.Checked then
        MeInfo.Lines.Add('Tabela de tipos de eventos importada com sucesso');
    finally
      RegItens.Free;
    end;
  finally
    MemFile.Free;
  end;
end;

procedure TFmFPImporta.AtualizaFPEventVar;
var
  MemFile: TMemIniFile;
  RegItens: TStringList;
  i: Integer;
  //
  Event, Varia, Ativo: Integer;
  Cod_ID: String;
begin
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('DELETE FROM fpeventvar ');
  Dmod.QrUpdU.ExecSQL;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO fpeventvar SET ');
  Dmod.QrUpdU.SQL.Add('Event=:P0, Varia=:P1, Ativo=:P2, Cod_ID=:P3');
  //
  MemFile := TMemIniFile.Create(sArqTemp);
  try
    RegItens := TStringList.Create;
    try
      MemFile.ReadSections(RegItens);
      //MemFile.ReadSections(MeTeste.Lines);
      for i := 0 to RegItens.Count - 1 do
      begin
        //MemFile.ReadSection(RegItens[i], MeTabelas.Lines);
        if CkLinha.Checked then
          MemFile.ReadSectionValues(RegItens[i], MeRegistros.Lines);
        Event      := MemFile.ReadInteger(RegItens[i], 'Event'      , 0);
        Varia      := MemFile.ReadInteger(RegItens[i], 'Varia'       , 0);
        Ativo      := MemFile.ReadInteger(RegItens[i], 'Ativo'      , 0);
        //
        Cod_ID     := MemFile.ReadString (RegItens[i], 'Cod_ID'  , '');
        //
        Dmod.QrUpdU.Params[00].AsInteger := Event;
        Dmod.QrUpdU.Params[01].AsInteger := Varia;
        Dmod.QrUpdU.Params[02].AsInteger := Ativo;
        Dmod.QrUpdU.Params[03].AsString  := Cod_ID;
        //
        Dmod.QrUpdU.ExecSQL;
        //
        if CkLinha.Checked then
        MeInfo.Lines.Add('A vari�vel de evento  ' +
        Cod_ID + ' foi inclu�da com sucesso');
      end;
      if not CkLinha.Checked then
        MeInfo.Lines.Add('Tabela de vari�veis de eventos importada com sucesso');
    finally
      RegItens.Free;
    end;
  finally
    MemFile.Free;
  end;
end;

procedure TFmFPImporta.FormCreate(Sender: TObject);
begin
  FExecutouAuto     := False;
  FImportaFPImporta := False;
end;

procedure TFmFPImporta.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FExecutouAuto  := True;
  ImportaDeArquivo(ExtractFilePath(Application.ExeName) +'Folha.txt');
end;

end.

