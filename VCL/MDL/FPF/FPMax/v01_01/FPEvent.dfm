object FmCNAB400CaG: TFmCNAB400CaG
  Left = 368
  Top = 194
  Caption = 'Registros CNAB'
  ClientHeight = 346
  ClientWidth = 679
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label14: TLabel
    Left = 16
    Top = 140
    Width = 33
    Height = 13
    Caption = 'Tam 2:'
  end
  object Label15: TLabel
    Left = 16
    Top = 184
    Width = 33
    Height = 13
    Caption = 'Tam 3:'
  end
  object Label16: TLabel
    Left = 16
    Top = 228
    Width = 33
    Height = 13
    Caption = 'Tam 4:'
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 679
    Height = 298
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 249
      Width = 677
      Height = 48
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 251
      ExplicitWidth = 685
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object Panel1: TPanel
        Left = 576
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
          NumGlyphs = 2
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 820
      Height = 196
      TabOrder = 1
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 68
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object EdCodigo: TLMDEdit
        Left = 16
        Top = 24
        Width = 48
        Height = 21
        
        Caret.BlinkRate = 530
        CtlXP = False
        TabOrder = 0
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        AutoSelect = True
        ParentFont = False
        Alignment = taRightJustify
        CustomButtons = <>
        PasswordChar = #0
        ReadOnly = True
        Text = '00'
      end
      object EdNome: TEdit
        Left = 68
        Top = 24
        Width = 600
        Height = 21
        MaxLength = 255
        TabOrder = 1
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 679
    Height = 298
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 249
      Width = 677
      Height = 48
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 251
      ExplicitWidth = 685
      object LaRegistro: TLMDLabel
        Left = 173
        Top = 1
        Width = 42
        Height = 46
        
        Bevel.StyleOuter = bvRaised
        
        Align = alClient
        Alignment = agBottomLeft
        Options = []
        Caption = '[N]: 0'
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
          NumGlyphs = 2
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
          NumGlyphs = 2
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
          NumGlyphs = 2
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
          NumGlyphs = 2
        end
      end
      object Panel3: TPanel
        Left = 215
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          NumGlyphs = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
          NumGlyphs = 2
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtIncluiClick
          NumGlyphs = 2
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 820
      Height = 188
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 68
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TLMDDBEdit
        Left = 16
        Top = 24
        Width = 48
        Height = 21
        Hint = 'N'#186' do banco'
        
        Caret.BlinkRate = 530
        CtlXP = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Alignment = taRightJustify
        CustomButtons = <>
        ReadOnly = True
        DataField = 'Codigo'
        DataSource = DsCNAB400CaG
      end
      object DBEdNome: TLMDDBEdit
        Left = 68
        Top = 24
        Width = 600
        Height = 21
        Hint = 'Nome do banco'
        
        Caret.BlinkRate = 530
        Color = clWhite
        CtlXP = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        CustomButtons = <>
        DataField = 'Nome'
        DataSource = DsCNAB400CaG
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 679
    Height = 48
    Align = alTop
    Alignment = agCenterLeft
    
    
    
    
    Caption = '                              Registros CNAB'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1

    ExplicitWidth = 687
    object LaTipo: TLabel
      Left = 603
      Top = 2
      Width = 82
      Height = 44
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Image1: TImage
      Left = 227
      Top = 2
      Width = 376
      Height = 44
      Align = alClient
      Transparent = True
    end
    object PanelFill002: TPanel
      Left = 2
      Top = 2
      Width = 225
      Height = 44
      Align = alLeft
      
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0

      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 1
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
  end
  object DsCNAB400CaG: TDataSource
    DataSet = QrCNAB400CaG
    Left = 488
    Top = 161
  end
  object QrCNAB400CaG: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCNAB400CaGBeforeOpen
    AfterOpen = QrCNAB400CaGAfterOpen
    AfterScroll = QrCNAB400CaGAfterScroll
    SQL.Strings = (
      'SELECT * FROM cnab400cag'
      'WHERE Codigo > 0')
    Left = 460
    Top = 161
    object QrCNAB400CaGCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAB400CaGNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCNAB400CaGLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCNAB400CaGDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCNAB400CaGDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCNAB400CaGUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCNAB400CaGUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
end

