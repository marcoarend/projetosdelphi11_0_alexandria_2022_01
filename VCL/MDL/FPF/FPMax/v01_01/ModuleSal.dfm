object DmSal: TDmSal
  OldCreateOrder = False
  Left = 345
  Top = 165
  Height = 575
  Width = 844
  object QrEmpresas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, '
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEEMPRESA'
      'FROM entidades en'
      'ORDER BY NOMEEMPRESA')
    Left = 24
    object QrEmpresasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmpresasNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Required = True
      Size = 100
    end
  end
  object DsEmpresas: TDataSource
    DataSet = QrEmpresas
    Left = 92
  end
  object QrDepto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT fun.Depto CodID, dep.Descricao'
      'FROM fpdepto dep'
      'LEFT JOIN fpfunci fun ON fun.Depto=dep.CodID'
      'WHERE fun.Depto IS NOT NULL'
      'AND fun.Empresa=:P0'
      'ORDER BY dep.Descricao')
    Left = 24
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDeptoCodID: TWideStringField
      FieldName = 'CodID'
    end
    object QrDeptoDescricao: TWideStringField
      FieldName = 'Descricao'
      Required = True
      Size = 50
    end
  end
  object DsDepto: TDataSource
    DataSet = QrDepto
    Left = 92
    Top = 44
  end
  object QrFunci: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, RazaoSocial, Nome) NOMEFUNCI, '
      'ent.Codigo, fun.Funci, fun.Registro'
      'FROM fpfunci fun'
      'LEFT JOIN entidades ent ON ent.Codigo=fun.Codigo'
      'WHERE fun.Ativo=1'
      'AND fun.Empresa=1'
      'ORDER BY NOMEFUNCI')
    Left = 25
    Top = 88
    object QrFunciNOMEFUNCI: TWideStringField
      FieldName = 'NOMEFUNCI'
      Size = 100
    end
    object QrFunciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFunciFunci: TIntegerField
      FieldName = 'Funci'
      Required = True
    end
    object QrFunciRegistro: TWideStringField
      FieldName = 'Registro'
      Size = 10
    end
    object QrFunciCDO_FUNCI: TIntegerField
      FieldKind = fkLookup
      FieldName = 'CDO_FUNCI'
      LookupDataSet = QrCdo
      LookupKeyFields = 'Entidade'
      LookupResultField = 'Entidade'
      KeyFields = 'Funci'
      Lookup = True
    end
  end
  object DsFunci: TDataSource
    DataSet = QrFunci
    Left = 93
    Top = 88
  end
  object QrFPtpCal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao'
      'FROM fptpcal'
      'ORDER BY Descricao'
      '')
    Left = 25
    Top = 132
    object QrFPtpCalCodigo: TSmallintField
      FieldName = 'Codigo'
    end
    object QrFPtpCalDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 35
    end
  end
  object DsFPtpCal: TDataSource
    DataSet = QrFPtpCal
    Left = 93
    Top = 132
  end
  object QrFPC: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT *'
      'FROM fpcalc')
    Left = 25
    Top = 177
    object QrFPCFPFolhaCal: TIntegerField
      FieldName = 'FPFolhaCal'
    end
    object QrFPCEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrFPCNomeEnti: TWideStringField
      FieldName = 'NomeEnti'
      Size = 100
    end
    object QrFPCCodFunci: TIntegerField
      FieldName = 'CodFunci'
    end
    object QrFPCCalcula: TSmallintField
      FieldName = 'Calcula'
      MaxValue = 1
    end
  end
  object DsFPC: TDataSource
    DataSet = QrFPC
    Left = 93
    Top = 177
  end
  object QrCalc: TmySQLQuery
    Database = Dmod.MyLocDatabase
    AfterScroll = QrCalcAfterScroll
    OnCalcFields = QrCalcCalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM fpcalc'
      'WHERE Calcula=1')
    Left = 24
    Top = 264
    object QrCalcFPFolhaCal: TIntegerField
      FieldName = 'FPFolhaCal'
    end
    object QrCalcEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrCalcNomeEnti: TWideStringField
      FieldName = 'NomeEnti'
      Size = 100
    end
    object QrCalcCodFunci: TIntegerField
      FieldName = 'CodFunci'
    end
    object QrCalcCalcula: TSmallintField
      FieldName = 'Calcula'
    end
    object QrCalcValorP: TFloatField
      FieldKind = fkLookup
      FieldName = 'ValorP'
      LookupDataSet = QrCalcSumM
      LookupKeyFields = 'Entidade'
      LookupResultField = 'ValorP'
      KeyFields = 'Entidade'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Lookup = True
    end
    object QrCalcValorD: TFloatField
      FieldKind = fkLookup
      FieldName = 'ValorD'
      LookupDataSet = QrCalcSumM
      LookupKeyFields = 'Entidade'
      LookupResultField = 'ValorD'
      KeyFields = 'Entidade'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Lookup = True
    end
    object QrCalcValorL: TFloatField
      FieldKind = fkLookup
      FieldName = 'ValorL'
      LookupDataSet = QrCalcSumM
      LookupKeyFields = 'Entidade'
      LookupResultField = 'ValorL'
      KeyFields = 'Entidade'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Lookup = True
    end
    object QrCalcCDO_FUNCI: TIntegerField
      FieldKind = fkLookup
      FieldName = 'CDO_FUNCI'
      LookupDataSet = QrCdo
      LookupKeyFields = 'Entidade'
      LookupResultField = 'Entidade'
      KeyFields = 'Entidade'
      Lookup = True
    end
    object QrCalcCDO_COD: TIntegerField
      FieldKind = fkLookup
      FieldName = 'CDO_COD'
      LookupDataSet = QrCdo
      LookupKeyFields = 'Entidade'
      LookupResultField = 'Codigo'
      KeyFields = 'Entidade'
      Lookup = True
    end
    object QrCalcTemCalc: TSmallintField
      FieldKind = fkCalculated
      FieldName = 'TemCalc'
      MaxValue = 1
      Calculated = True
    end
    object QrCalcSalvou: TSmallintField
      FieldName = 'Salvou'
      MaxValue = 1
    end
    object QrCalcFPFunciFer: TIntegerField
      FieldName = 'FPFunciFer'
    end
    object QrCalcMediaHE: TFloatField
      FieldName = 'MediaHE'
    end
    object QrCalcMediaCo: TFloatField
      FieldName = 'MediaCo'
    end
    object QrCalcMediaAd: TFloatField
      FieldName = 'MediaAd'
    end
  end
  object QrTpCalPer: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM fptpcalper'
      'WHERE TipoCalc=:P0')
    Left = 24
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTpCalPerControle: TSmallintField
      FieldName = 'Controle'
    end
    object QrTpCalPerTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
    end
    object QrTpCalPerCategoria: TSmallintField
      FieldName = 'Categoria'
    end
    object QrTpCalPerSubPer: TSmallintField
      FieldName = 'SubPer'
    end
    object QrTpCalPerDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 30
    end
  end
  object DsTpCalPer: TDataSource
    DataSet = QrTpCalPer
    Left = 92
    Top = 220
  end
  object QrEventip: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ev.*'
      'FROM fpeventtip et'
      'LEFT JOIN fpevent ev ON ev.Codigo=et.Event'
      'WHERE et.Ativo=1'
      'AND ev.Ativo=1'
      'AND et.Tipo=:P0'
      'ORDER BY ev.Prioridade DESC, ev.Codigo')
    Left = 160
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEventipCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEventipAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEventipNatureza: TWideStringField
      FieldName = 'Natureza'
      Size = 1
    end
    object QrEventipBase: TIntegerField
      FieldName = 'Base'
    end
    object QrEventipAuto: TSmallintField
      FieldName = 'Auto'
    end
    object QrEventipPercentual: TFloatField
      FieldName = 'Percentual'
    end
    object QrEventipPrioridade: TSmallintField
      FieldName = 'Prioridade'
    end
    object QrEventipVisivel: TSmallintField
      FieldName = 'Visivel'
    end
    object QrEventipInformacao: TSmallintField
      FieldName = 'Informacao'
    end
    object QrEventipSalvar: TSmallintField
      FieldName = 'Salvar'
    end
  end
  object QrFPFunci: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM fpfunci'
      'WHERE Codigo=:P0')
    Left = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFPFunciHrSeman: TIntegerField
      FieldName = 'HrSeman'
      Origin = 'fpfunci.HrSeman'
    end
    object QrFPFunciTipoSal: TSmallintField
      FieldName = 'TipoSal'
      Origin = 'fpfunci.TipoSal'
    end
    object QrFPFunciSalario: TFloatField
      FieldName = 'Salario'
      Origin = 'fpfunci.Salario'
    end
    object QrFPFunciProfessor: TSmallintField
      FieldName = 'Professor'
      Origin = 'fpfunci.Professor'
    end
    object QrFPFunciDependent: TIntegerField
      FieldName = 'Dependent'
      Origin = 'fpfunci.Dependent'
    end
    object QrFPFunciInsalubrid: TFloatField
      FieldName = 'Insalubrid'
      Origin = 'fpfunci.Insalubrid'
    end
    object QrFPFunciPericulosi: TFloatField
      FieldName = 'Periculosi'
      Origin = 'fpfunci.Periculosi'
    end
    object QrFPFunciDataAdm: TDateField
      FieldName = 'DataAdm'
      Origin = 'fpfunci.DataAdm'
    end
    object QrFPFunciFilhos: TIntegerField
      FieldName = 'Filhos'
      Origin = 'fpfunci.Filhos'
    end
    object QrFPFunciPensaoAlm: TFloatField
      FieldName = 'PensaoAlm'
      Origin = 'fpfunci.PensaoAlm'
    end
    object QrFPFunciPensaoLiq: TSmallintField
      FieldName = 'PensaoLiq'
      Origin = 'fpfunci.PensaoLiq'
    end
    object QrFPFunciComoCalcIR: TSmallintField
      FieldName = 'ComoCalcIR'
      Origin = 'fpfunci.ComoCalcIR'
    end
  end
  object QrUpdL1: TmySQLQuery
    Database = Dmod.MyLocDatabase
    Left = 368
    Top = 308
  end
  object QrUpdL2: TmySQLQuery
    Database = Dmod.MyLocDatabase
    Left = 368
    Top = 352
  end
  object QrPesqIRRF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM fptirrf'
      'WHERE Teto >=:P0')
    Left = 160
    Top = 88
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqIRRFAliquota: TFloatField
      FieldName = 'Aliquota'
    end
    object QrPesqIRRFDeducao: TFloatField
      FieldName = 'Deducao'
    end
  end
  object QrEvnAcum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Event, Acumu'
      'FROM fpeventacu'
      'WHERE Ativo=1'
      'AND Event=:P0')
    Left = 160
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEvnAcumEvent: TIntegerField
      FieldName = 'Event'
    end
    object QrEvnAcumAcumu: TIntegerField
      FieldName = 'Acumu'
    end
  end
  object QrSalMin: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DataV, Valor'
      'FROM fptsalr'
      'WHERE Ativo=1'
      'AND DataV <=:P0'
      'ORDER BY DataV DESC')
    Left = 160
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSalMinValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSalMinDataV: TDateField
      FieldName = 'DataV'
    end
  end
  object DsCalc: TDataSource
    DataSet = QrCalc
    Left = 92
    Top = 264
  end
  object QrCalcM: TmySQLQuery
    Database = Dmod.MyLocDatabase
    OnCalcFields = QrCalcMCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM fpcalceven'
      'WHERE Mostra=1'
      'AND Info=0'
      'AND Entidade=:P0'
      'ORDER BY Valor DESC')
    Left = 24
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCalcMEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrCalcMEvento: TIntegerField
      FieldName = 'Evento'
    end
    object QrCalcMReferencia: TFloatField
      FieldName = 'Referencia'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCalcMValor: TFloatField
      FieldName = 'Valor'
    end
    object QrCalcMAlteracao: TSmallintField
      FieldName = 'Alteracao'
    end
    object QrCalcMInfo: TSmallintField
      FieldName = 'Info'
    end
    object QrCalcMSalva: TSmallintField
      FieldName = 'Salva'
    end
    object QrCalcMMostra: TSmallintField
      FieldName = 'Mostra'
    end
    object QrCalcMAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCalcMNOMEEVENTO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMEEVENTO'
      LookupDataSet = QrFPEvent
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Descricao'
      KeyFields = 'Evento'
      Size = 50
      Lookup = True
    end
    object QrCalcMValorP: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ValorP'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrCalcMValorD: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ValorD'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrCalcMPrioridade: TIntegerField
      FieldName = 'Prioridade'
    end
    object QrCalcMAuto: TSmallintField
      FieldName = 'Auto'
    end
    object QrCalcMUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 15
    end
  end
  object QrFPEvent: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM fpevent')
    Left = 160
    Top = 220
    object QrFPEventCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPEventDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 50
    end
  end
  object DsCalcM: TDataSource
    DataSet = QrCalcM
    Left = 92
    Top = 352
  end
  object QrCalcSumM: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT Entidade, SUM(IF(Valor > 0, Valor, 0)) ValorP,'
      'SUM(IF(Valor < 0, -Valor, 0)) ValorD, SUM(Valor) ValorL'
      'FROM fpcalceven'
      'WHERE Mostra=1'
      'AND Info=0'
      'GROUP BY Entidade')
    Left = 24
    Top = 308
    object QrCalcSumMEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrCalcSumMValorP: TFloatField
      FieldName = 'ValorP'
    end
    object QrCalcSumMValorD: TFloatField
      FieldName = 'ValorD'
    end
    object QrCalcSumMValorL: TFloatField
      FieldName = 'ValorL'
    end
  end
  object DsCalcSumM: TDataSource
    DataSet = QrCalcSumM
    Left = 92
    Top = 308
  end
  object QrCalcI: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT * '
      'FROM fpcalceven'
      'WHERE Info=1'
      'AND Entidade=:P0')
    Left = 24
    Top = 396
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCalcINOMEEVENTO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMEEVENTO'
      LookupDataSet = QrFPEvent
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Descricao'
      KeyFields = 'Evento'
      Size = 50
      Lookup = True
    end
    object QrCalcIEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrCalcIEvento: TIntegerField
      FieldName = 'Evento'
    end
    object QrCalcIReferencia: TFloatField
      FieldName = 'Referencia'
    end
    object QrCalcIValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCalcIAlteracao: TSmallintField
      FieldName = 'Alteracao'
    end
    object QrCalcIInfo: TSmallintField
      FieldName = 'Info'
    end
    object QrCalcISalva: TSmallintField
      FieldName = 'Salva'
    end
    object QrCalcIMostra: TSmallintField
      FieldName = 'Mostra'
    end
    object QrCalcIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsCalcI: TDataSource
    DataSet = QrCalcI
    Left = 92
    Top = 396
  end
  object QrCdo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFUNCI,'
      
        'foc.Entidade, foc.DataC, foc.ValorP, foc.ValorD, foc.ValorL, foc' +
        '.Codigo'
      'FROM fpfolhacal foc'
      'LEFT JOIN entidades ent ON ent.Codigo=foc.Entidade'
      'WHERE foc.Ativo=1 '
      'AND foc.Periodo=:P0'
      'AND foc.Empresa=:P1'
      'AND foc.TipoCalc=:P2'
      'AND foc.Semana=:P3')
    Left = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrCdoNOMEFUNCI: TWideStringField
      FieldName = 'NOMEFUNCI'
      Size = 100
    end
    object QrCdoEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrCdoDataC: TDateField
      FieldName = 'DataC'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrCdoValorP: TFloatField
      FieldName = 'ValorP'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCdoValorD: TFloatField
      FieldName = 'ValorD'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCdoValorL: TFloatField
      FieldName = 'ValorL'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCdoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsCdo: TDataSource
    DataSet = QrCdo
    Left = 296
  end
  object QrInsC: TmySQLQuery
    Database = Dmod.MyDB
    Left = 368
  end
  object QrInsA: TmySQLQuery
    Database = Dmod.MyDB
    Left = 368
    Top = 44
  end
  object QrInsE: TmySQLQuery
    Database = Dmod.MyDB
    Left = 368
    Top = 88
  end
  object QrLEve: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT * '
      'FROM fpcalceven'
      'WHERE Salva=1'
      'AND Ativo=1'
      'AND Entidade=:P0')
    Left = 368
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLEveEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrLEveEvento: TIntegerField
      FieldName = 'Evento'
    end
    object QrLEveReferencia: TFloatField
      FieldName = 'Referencia'
    end
    object QrLEveValor: TFloatField
      FieldName = 'Valor'
    end
    object QrLEveAlteracao: TSmallintField
      FieldName = 'Alteracao'
    end
    object QrLEveInfo: TSmallintField
      FieldName = 'Info'
    end
    object QrLEveSalva: TSmallintField
      FieldName = 'Salva'
    end
    object QrLEveMostra: TSmallintField
      FieldName = 'Mostra'
    end
    object QrLEveAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrLAcu: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT * '
      'FROM fpcalcacum'
      'WHERE Entidade=:P0'
      '')
    Left = 368
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLAcuEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrLAcuEvento: TIntegerField
      FieldName = 'Evento'
    end
    object QrLAcuAcumulador: TIntegerField
      FieldName = 'Acumulador'
    end
  end
  object QrUpdC: TmySQLQuery
    Database = Dmod.MyLocDatabase
    Left = 368
    Top = 220
  end
  object QrFunEve: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fue.Incidencia, fue.Evento, fue.Eve_Incid, '
      'fue.Eve_ValPer, fue.Eve_Meses, fue.Conta, '
      'fue.Cta_IncPer, fue.Cta_MesesR,'
      'eve.Natureza, eve.Base, eve.Percentual, '
      'eve.Visivel, eve.Informacao, eve.Salvar,'
      'eve.Prioridade, eve.Auto, '
      'fue.Unidade, fue.ProporciHT, fue.Eve_Ciclo, fue.Eve_DtCicl'
      'FROM fpfuncieve fue'
      'LEFT JOIN fpevent eve ON eve.Codigo=fue.Evento'
      'LEFT JOIN fpfuncical fuc ON fue.Controle=fuc.Controle'
      'WHERE fue.Codigo=:P0'
      'AND fuc.TipoCalc=:P1'
      ''
      'ORDER BY eve.Prioridade DESC')
    Left = 160
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFunEveIncidencia: TSmallintField
      FieldName = 'Incidencia'
      Required = True
    end
    object QrFunEveEvento: TIntegerField
      FieldName = 'Evento'
      Required = True
    end
    object QrFunEveEve_Incid: TSmallintField
      FieldName = 'Eve_Incid'
      Required = True
    end
    object QrFunEveEve_ValPer: TFloatField
      FieldName = 'Eve_ValPer'
      Required = True
    end
    object QrFunEveEve_Meses: TSmallintField
      FieldName = 'Eve_Meses'
      Required = True
    end
    object QrFunEveConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrFunEveCta_IncPer: TFloatField
      FieldName = 'Cta_IncPer'
      Required = True
    end
    object QrFunEveCta_MesesR: TSmallintField
      FieldName = 'Cta_MesesR'
      Required = True
    end
    object QrFunEveNatureza: TWideStringField
      FieldName = 'Natureza'
      Size = 1
    end
    object QrFunEveBase: TIntegerField
      FieldName = 'Base'
    end
    object QrFunEvePercentual: TFloatField
      FieldName = 'Percentual'
    end
    object QrFunEveVisivel: TSmallintField
      FieldName = 'Visivel'
    end
    object QrFunEveInformacao: TSmallintField
      FieldName = 'Informacao'
    end
    object QrFunEveSalvar: TSmallintField
      FieldName = 'Salvar'
    end
    object QrFunEvePrioridade: TSmallintField
      FieldName = 'Prioridade'
    end
    object QrFunEveAuto: TSmallintField
      FieldName = 'Auto'
    end
    object QrFunEveProporciHT: TSmallintField
      FieldName = 'ProporciHT'
      Required = True
    end
    object QrFunEveUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 15
    end
    object QrFunEveEve_Ciclo: TSmallintField
      FieldName = 'Eve_Ciclo'
      Required = True
    end
    object QrFunEveEve_DtCicl: TDateField
      FieldName = 'Eve_DtCicl'
      Required = True
    end
  end
  object QrSalPago: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ValorL) ValorL'
      'FROM fpfolhacal'
      'WHERE TipoCalc=:P0'
      'AND Periodo=:P1'
      'AND Empresa=:P2'
      'AND Entidade=:P3')
    Left = 160
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrSalPagoValorL: TFloatField
      FieldName = 'ValorL'
    end
  end
  object QrUserEve: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao, Natureza, Prioridade,'
      'Informacao, Salvar, Visivel, Percentual, Base'
      'FROM fpevent'
      'WHERE Auto=0'
      'ORDER BY Descricao')
    Left = 230
    Top = 46
    object QrUserEveCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUserEveDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 50
    end
    object QrUserEveNatureza: TWideStringField
      FieldName = 'Natureza'
      Size = 1
    end
    object QrUserEvePrioridade: TSmallintField
      FieldName = 'Prioridade'
    end
    object QrUserEveInformacao: TSmallintField
      FieldName = 'Informacao'
    end
    object QrUserEveSalvar: TSmallintField
      FieldName = 'Salvar'
    end
    object QrUserEveVisivel: TSmallintField
      FieldName = 'Visivel'
    end
    object QrUserEveBase: TIntegerField
      FieldName = 'Base'
    end
    object QrUserEvePercentual: TFloatField
      FieldName = 'Percentual'
    end
  end
  object DsUserEve: TDataSource
    DataSet = QrUserEve
    Left = 298
    Top = 46
  end
  object QrBase: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT * '
      'FROM fpcalcbase'
      'WHERE Entidade=:P0'
      'ORDER BY Prioridade DESC, Evento')
    Left = 160
    Top = 308
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBaseEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'fpcalcbase.Entidade'
    end
    object QrBaseEvento: TIntegerField
      FieldName = 'Evento'
      Origin = 'fpcalcbase.Evento'
    end
    object QrBaseEve_ValPer: TFloatField
      FieldName = 'Eve_ValPer'
      Origin = 'fpcalcbase.Eve_ValPer'
    end
    object QrBaseEve_Meses: TSmallintField
      FieldName = 'Eve_Meses'
      Origin = 'fpcalcbase.Eve_Meses'
    end
    object QrBaseEve_Incid: TSmallintField
      FieldName = 'Eve_Incid'
      Origin = 'fpcalcbase.Eve_Incid'
    end
    object QrBaseConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'fpcalcbase.Conta'
    end
    object QrBaseCta_IncPer: TFloatField
      FieldName = 'Cta_IncPer'
      Origin = 'fpcalcbase.Cta_IncPer'
    end
    object QrBaseCta_MesesR: TSmallintField
      FieldName = 'Cta_MesesR'
      Origin = 'fpcalcbase.Cta_MesesR'
    end
    object QrBaseNatureza: TWideStringField
      FieldName = 'Natureza'
      Origin = 'fpcalcbase.Natureza'
      Size = 1
    end
    object QrBaseReferencia: TFloatField
      FieldName = 'Referencia'
      Origin = 'fpcalcbase.Referencia'
    end
    object QrBaseValor: TFloatField
      FieldName = 'Valor'
      Origin = 'fpcalcbase.Valor'
    end
    object QrBaseIncidencia: TSmallintField
      FieldName = 'Incidencia'
      Origin = 'fpcalcbase.Incidencia'
    end
    object QrBaseAlteracao: TSmallintField
      FieldName = 'Alteracao'
      Origin = 'fpcalcbase.Alteracao'
    end
    object QrBaseInformacao: TSmallintField
      FieldName = 'Informacao'
      Origin = 'fpcalcbase.Informacao'
    end
    object QrBaseSalvar: TSmallintField
      FieldName = 'Salvar'
      Origin = 'fpcalcbase.Salvar'
    end
    object QrBaseVisivel: TSmallintField
      FieldName = 'Visivel'
      Origin = 'fpcalcbase.Visivel'
    end
    object QrBasePrioridade: TIntegerField
      FieldName = 'Prioridade'
      Origin = 'fpcalcbase.Prioridade'
    end
    object QrBaseFonte: TSmallintField
      FieldName = 'Fonte'
      Origin = 'fpcalcbase.Fonte'
    end
    object QrBaseAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'fpcalcbase.Ativo'
    end
    object QrBaseAuto: TSmallintField
      FieldName = 'Auto'
      Origin = 'fpcalcbase.Auto'
    end
    object QrBaseEve_BasCod: TIntegerField
      FieldName = 'Eve_BasCod'
      Origin = 'fpcalcbase.Eve_BasCod'
    end
    object QrBaseEve_BasPer: TFloatField
      FieldName = 'Eve_BasPer'
      Origin = 'fpcalcbase.Eve_BasPer'
    end
    object QrBasePercentual: TFloatField
      FieldName = 'Percentual'
    end
    object QrBaseProporciHT: TSmallintField
      FieldName = 'ProporciHT'
    end
    object QrBaseUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 15
    end
    object QrBaseEve_Ciclo: TIntegerField
      FieldName = 'Eve_Ciclo'
    end
    object QrBaseEve_DtCicl: TDateField
      FieldName = 'Eve_DtCicl'
    end
  end
  object QrUpdL0: TmySQLQuery
    Database = Dmod.MyLocDatabase
    Left = 368
    Top = 264
  end
  object QrSalFam: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Valor '
      'FROM fptsfam'
      'WHERE Limite >= :P0'
      'ORDER BY Limite')
    Left = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSalFamValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrINSS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Faixa, Aliquota, Deducao'
      'FROM fptinss'
      'WHERE Faixa >= :P0'
      'ORDER BY Faixa')
    Left = 440
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrINSSAliquota: TFloatField
      FieldName = 'Aliquota'
    end
    object QrINSSDeducao: TFloatField
      FieldName = 'Deducao'
    end
    object QrINSSFaixa: TFloatField
      FieldName = 'Faixa'
    end
  end
  object QrIRRF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Teto, Aliquota, Deducao'
      'FROM fptirrf'
      'WHERE Teto>=:P0'
      'ORDER BY Teto')
    Left = 440
    Top = 88
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrIRRFTeto: TFloatField
      FieldName = 'Teto'
    end
    object QrIRRFAliquota: TFloatField
      FieldName = 'Aliquota'
    end
    object QrIRRFDeducao: TFloatField
      FieldName = 'Deducao'
    end
  end
  object QrIRPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(fev.Valor) Valor'
      'FROM fpfolhaeve fev'
      'LEFT JOIN fpfolhacal fca ON fca.Codigo=fev.Codigo'
      'WHERE ( '
      '  (fev.Evento = 50) OR '
      '  ((fev.Evento = 59) AND (fev.Codigo<>:P0) )'
      '   ) '
      'AND fca.Empresa=:P1'
      'AND fca.Entidade=:P2'
      'AND fca.DataC BETWEEN :P3 AND :P4')
    Left = 440
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrIRPgValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrIniFer: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fun.HEFator, fer.*'
      'FROM fpfuncifer fer'
      'LEFT JOIN fpfunci fun ON '
      '  fun.Codigo=fer.Codigo AND fun.Empresa=fer.Empresa'
      'WHERE fun.DataDemiss = 0'
      'AND fer.Empresa=:P0'
      'AND fer.Codigo=:P1'
      'AND fer.DtIniPG BETWEEN :P2 AND :P3')
    Left = 440
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrIniFerDiasGozar: TSmallintField
      FieldName = 'DiasGozar'
      Required = True
    end
    object QrIniFerDiasPecun: TSmallintField
      FieldName = 'DiasPecun'
      Required = True
    end
    object QrIniFerControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrIniFerMeHEMan1xx: TFloatField
      FieldName = 'MeHEMan1xx'
      Required = True
    end
    object QrIniFerMeHEMan200: TFloatField
      FieldName = 'MeHEMan200'
      Required = True
    end
    object QrIniFerDtIniPA: TDateField
      FieldName = 'DtIniPA'
      Required = True
    end
    object QrIniFerDtFimPA: TDateField
      FieldName = 'DtFimPA'
      Required = True
    end
    object QrIniFerMediaCoMan: TFloatField
      FieldName = 'MediaCoMan'
      Required = True
    end
    object QrIniFerMediaAdMan: TFloatField
      FieldName = 'MediaAdMan'
      Required = True
    end
    object QrIniFerDtIniPG: TDateField
      FieldName = 'DtIniPG'
      Required = True
    end
    object QrIniFerDtFimPG: TDateField
      FieldName = 'DtFimPG'
      Required = True
    end
    object QrIniFerCalculo: TIntegerField
      FieldName = 'Calculo'
      Required = True
    end
    object QrIniFerHEFator: TFloatField
      FieldName = 'HEFator'
    end
  end
  object QrHrE: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT hre.Fator, SUM(hre.Horas) Horas, '
      'SUM(hre.Horas*hre.Fator/100) FatHr'
      ''
      'FROM fphrext hre'
      ''
      'WHERE hre.Empresa=:P0'
      'AND hre.Codigo=:P1'
      'AND hre.DataHE BETWEEN :P2 AND :P3'
      ''
      'GROUP BY hre.Fator')
    Left = 440
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrHrEHoras: TFloatField
      FieldName = 'Horas'
    end
    object QrHrEFatHr: TFloatField
      FieldName = 'FatHr'
    end
    object QrHrEFator: TFloatField
      FieldName = 'Fator'
      Required = True
    end
  end
  object Qrxxx: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fer.*'
      'FROM fpfuncifer fer'
      'WHERE Empresa=:P0'
      'AND Codigo=:P1'
      'AND DtIniPG BETWEEN :P2 AND :P3')
    Left = 440
    Top = 308
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object IntegerField3: TIntegerField
      FieldName = 'Calculo'
      Required = True
    end
    object SmallintField1: TSmallintField
      FieldName = 'Pecun'
      Required = True
    end
    object FloatField1: TFloatField
      FieldName = 'MediaHEMan'
      Required = True
    end
    object FloatField2: TFloatField
      FieldName = 'MediaCoMan'
      Required = True
    end
    object FloatField3: TFloatField
      FieldName = 'MediaAdMan'
      Required = True
    end
    object DateField1: TDateField
      FieldName = 'DtIniPA'
      Required = True
    end
    object DateField2: TDateField
      FieldName = 'DtFimPA'
      Required = True
    end
    object IntegerField4: TIntegerField
      FieldName = 'DiasFaltI'
      Required = True
    end
    object IntegerField5: TIntegerField
      FieldName = 'DiasFaltP'
      Required = True
    end
    object SmallintField2: TSmallintField
      FieldName = 'DiasGozar'
      Required = True
    end
    object SmallintField3: TSmallintField
      FieldName = 'DiasPecun'
      Required = True
    end
    object DateField3: TDateField
      FieldName = 'DtIniPecun'
      Required = True
    end
    object DateField4: TDateField
      FieldName = 'DtFimPecun'
      Required = True
    end
    object DateField5: TDateField
      FieldName = 'DtIniPG'
      Required = True
    end
    object DateField6: TDateField
      FieldName = 'DtFimFeria'
      Required = True
    end
    object DateField7: TDateField
      FieldName = 'DtAvisoFer'
      Required = True
    end
    object DateField8: TDateField
      FieldName = 'DtRecPgto'
      Required = True
    end
    object DateField9: TDateField
      FieldName = 'DtSolPecun'
      Required = True
    end
    object DateField10: TDateField
      FieldName = 'DtSolPrP13'
      Required = True
    end
    object IntegerField6: TIntegerField
      FieldName = 'Lk'
    end
    object DateField11: TDateField
      FieldName = 'DataCad'
    end
    object DateField12: TDateField
      FieldName = 'DataAlt'
    end
    object IntegerField7: TIntegerField
      FieldName = 'UserCad'
    end
    object IntegerField8: TIntegerField
      FieldName = 'UserAlt'
    end
    object DateField13: TDateField
      FieldName = 'DtRetorno'
      Required = True
    end
    object DateField14: TDateField
      FieldName = 'DtFimPG'
      Required = True
    end
    object IntegerField9: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object QrVVF: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT SUM(Valor) Valor'
      ''
      'FROM fpcalcferi'
      ''
      'WHERE Entidade=:P0')
    Left = 440
    Top = 396
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVVFValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrSVI: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT SUM(Valor) Valor'
      'FROM fpcalceven'
      'WHERE Evento in (13,28)')
    Left = 440
    Top = 440
    object QrSVIValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrEVI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Event'
      'FROM fpeventvar'
      'WHERE Varia=:P0')
    Left = 516
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEVIEvent: TIntegerField
      FieldName = 'Event'
    end
  end
  object QrInsF: TmySQLQuery
    Database = Dmod.MyDB
    Left = 368
    Top = 396
  end
  object QrLFer: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT * '
      'FROM fpcalcferi'
      'WHERE Entidade=:P0'
      '')
    Left = 368
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLFerEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrLFerItem: TIntegerField
      FieldName = 'Item'
    end
    object QrLFerTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrLFerAuto: TIntegerField
      FieldName = 'Auto'
    end
    object QrLFerReferencia: TFloatField
      FieldName = 'Referencia'
    end
    object QrLFerValor: TFloatField
      FieldName = 'Valor'
    end
    object QrLFerAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrEvent: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao'
      'FROM fpevent'
      'WHERE Codigo=:P0'
      '')
    Left = 516
    Top = 44
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEventCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEventDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 50
    end
  end
  object QrEmFer: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fer.*'
      'FROM fpfuncifer fer'
      'WHERE Empresa=:P0'
      'AND Codigo=:P1'
      'AND DtIniPG < :P2 '
      'AND DtFimPG > :P3')
    Left = 440
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrEmFerCalculo: TIntegerField
      FieldName = 'Calculo'
      Required = True
    end
    object QrEmFerDiasGozar: TSmallintField
      FieldName = 'DiasGozar'
      Required = True
    end
    object QrEmFerDiasPecun: TSmallintField
      FieldName = 'DiasPecun'
      Required = True
    end
  end
  object QrFimFer: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fer.*'
      'FROM fpfuncifer fer'
      'WHERE Empresa=:P0'
      'AND Codigo=:P1'
      'AND DtFimPG BETWEEN :P2 AND :P3')
    Left = 440
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrFimFerDtIniPG: TDateField
      FieldName = 'DtIniPG'
      Required = True
    end
    object QrFimFerDtFimPG: TDateField
      FieldName = 'DtFimPG'
      Required = True
    end
    object QrFimFerCalculo: TIntegerField
      FieldName = 'Calculo'
      Required = True
    end
    object QrFimFerDiasGozar: TSmallintField
      FieldName = 'DiasGozar'
      Required = True
    end
    object QrFimFerDiasPecun: TSmallintField
      FieldName = 'DiasPecun'
      Required = True
    end
  end
  object QrItsCal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM fpfolhaeve'
      'WHERE Info=0'
      'AND Mostra=1'
      'AND Ativo=1'
      'AND Codigo=:P0')
    Left = 516
    Top = 88
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItsCalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrItsCalEvento: TIntegerField
      FieldName = 'Evento'
    end
    object QrItsCalReferencia: TFloatField
      FieldName = 'Referencia'
    end
    object QrItsCalValor: TFloatField
      FieldName = 'Valor'
    end
    object QrItsCalInfo: TSmallintField
      FieldName = 'Info'
    end
    object QrItsCalMostra: TSmallintField
      FieldName = 'Mostra'
    end
    object QrItsCalAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrEvePes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM fpevent'
      'WHERE Codigo=:P0')
    Left = 160
    Top = 396
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEvePesVisivel: TSmallintField
      FieldName = 'Visivel'
    end
    object QrEvePesInformacao: TSmallintField
      FieldName = 'Informacao'
    end
    object QrEvePesSalvar: TSmallintField
      FieldName = 'Salvar'
    end
    object QrEvePesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEvePesNatureza: TWideStringField
      FieldName = 'Natureza'
      Size = 1
    end
    object QrEvePesAuto: TSmallintField
      FieldName = 'Auto'
    end
    object QrEvePesPrioridade: TSmallintField
      FieldName = 'Prioridade'
    end
  end
  object QrCond: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cnd.ProLaINSSr, cnd.ProLaINSSp'
      'FROM cond cnd'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      'WHERE ent.Codigo=:P0'
      '')
    Left = 516
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondProLaINSSr: TFloatField
      FieldName = 'ProLaINSSr'
    end
    object QrCondProLaINSSp: TFloatField
      FieldName = 'ProLaINSSp'
    end
  end
end


