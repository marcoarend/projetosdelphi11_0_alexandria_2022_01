object FmFPFunci: TFmFPFunci
  Left = 334
  Top = 166
  Caption = 'FPG-FUNCI-001 :: Funcion'#225'rios'
  ClientHeight = 642
  ClientWidth = 924
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 924
    Height = 546
    Align = alClient
    BevelOuter = bvNone
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PnEditTop: TPanel
      Left = 0
      Top = 0
      Width = 924
      Height = 106
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel14: TPanel
        Left = 0
        Top = 0
        Width = 844
        Height = 106
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label3: TLabel
          Left = 8
          Top = 8
          Width = 104
          Height = 13
          Caption = 'Cadastro de entidade:'
          FocusControl = DBEdit001
        end
        object DBEdit001: TDBEdit
          Left = 8
          Top = 24
          Width = 48
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsFPFunci
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdit002: TDBEdit
          Left = 56
          Top = 24
          Width = 648
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'NUMEFUNCI'
          DataSource = DsFPFunci
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object DBEdit10: TDBEdit
          Left = 8
          Top = 48
          Width = 696
          Height = 21
          DataField = 'NUMEEMPRESA'
          DataSource = DsFPFunci
          TabOrder = 2
        end
      end
      object Panel13: TPanel
        Left = 844
        Top = 0
        Width = 80
        Height = 106
        Align = alRight
        TabOrder = 0
        object LaFotoEd: TLabel
          Left = 1
          Top = 1
          Width = 39
          Height = 78
          Align = alClient
          Alignment = taCenter
          Caption = #13#10'Foto'#13#10#13#10'N'#227'o'#13#10#13#10'Definida'
        end
        object Image2: TImage
          Left = 1
          Top = 1
          Width = 78
          Height = 104
          Align = alClient
          Center = True
          Stretch = True
          ExplicitHeight = 102
        end
      end
    end
    object PainelEdit: TPanel
      Left = 0
      Top = 106
      Width = 924
      Height = 346
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 924
        Height = 346
        ActivePage = TabSheet5
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Informa'#231#245'es sal'#225'rio / recibo'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 916
            Height = 318
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label7: TLabel
              Left = 8
              Top = 4
              Width = 39
              Height = 13
              Caption = 'Fun'#231#227'o:'
            end
            object Label9: TLabel
              Left = 8
              Top = 44
              Width = 61
              Height = 13
              Caption = 'C.B.O. 2002:'
            end
            object Label8: TLabel
              Left = 396
              Top = 4
              Width = 70
              Height = 13
              Caption = 'Departamento:'
            end
            object Label13: TLabel
              Left = 8
              Top = 124
              Width = 42
              Height = 13
              Caption = 'Registro:'
            end
            object Label14: TLabel
              Left = 96
              Top = 124
              Width = 48
              Height = 13
              Caption = 'Categoria:'
            end
            object Label22: TLabel
              Left = 288
              Top = 124
              Width = 78
              Height = 13
              Caption = 'Horas semanais:'
            end
            object Label27: TLabel
              Left = 372
              Top = 124
              Width = 72
              Height = 13
              Caption = 'Tipo de sal'#225'rio:'
            end
            object Label28: TLabel
              Left = 564
              Top = 124
              Width = 35
              Height = 13
              Caption = 'Sal'#225'rio:'
            end
            object Label29: TLabel
              Left = 648
              Top = 124
              Width = 42
              Height = 13
              Caption = 'N'#186' filhos:'
            end
            object Label30: TLabel
              Left = 712
              Top = 124
              Width = 57
              Height = 13
              Caption = 'N'#186' depend.:'
            end
            object Label31: TLabel
              Left = 112
              Top = 164
              Width = 103
              Height = 13
              Caption = '% pens'#227'o aliment'#237'cia:'
            end
            object Label36: TLabel
              Left = 220
              Top = 164
              Width = 148
              Height = 13
              Caption = 'Banco e conta corrente sal'#225'rio:'
            end
            object Label68: TLabel
              Left = 8
              Top = 84
              Width = 61
              Height = 13
              Caption = 'C.B.O. 1994:'
            end
            object Label71: TLabel
              Left = 372
              Top = 164
              Width = 76
              Height = 13
              Caption = '% insalubridade:'
            end
            object Label72: TLabel
              Left = 456
              Top = 164
              Width = 82
              Height = 13
              Caption = '% periculosidade:'
            end
            object SpeedButton6: TSpeedButton
              Left = 366
              Top = 19
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton6Click
            end
            object SpeedButton7: TSpeedButton
              Left = 754
              Top = 19
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton7Click
            end
            object SpeedButton8: TSpeedButton
              Left = 754
              Top = 59
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton8Click
            end
            object SpeedButton9: TSpeedButton
              Left = 754
              Top = 99
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton9Click
            end
            object Label101: TLabel
              Left = 544
              Top = 164
              Width = 82
              Height = 13
              Caption = '% H.E semanais'#185':'
            end
            object Label94: TLabel
              Left = 632
              Top = 164
              Width = 38
              Height = 13
              Caption = 'PDCP '#178':'
            end
            object Label138: TLabel
              Left = 12
              Top = 300
              Width = 347
              Height = 13
              Caption = 
                #178': Primeiro dia (do m'#234's) do cart'#227'o ponto. (zero quando n'#227'o contr' +
                'ola ponto)'
            end
            object Label141: TLabel
              Left = 12
              Top = 280
              Width = 674
              Height = 13
              Caption = 
                #185': Porcentagem agregada a hora normal. Ex.: 50, 60 ou 70%  para ' +
                'horas extras em dias de trabalho normais (n'#227'o no descanso quando' +
                ' '#233' 100%).'
            end
            object Label166: TLabel
              Left = 8
              Top = 164
              Width = 34
              Height = 13
              Caption = 'Chapa:'
            end
            object EdFuncao: TdmkEditCB
              Left = 8
              Top = 19
              Width = 41
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              DBLookupComboBox = CBFuncao
              IgnoraDBLookupComboBox = False
            end
            object EdCBO2002: TdmkEditCB
              Left = 8
              Top = 59
              Width = 57
              Height = 21
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              DBLookupComboBox = CBCBO2002
              IgnoraDBLookupComboBox = False
            end
            object CBCBO2002: TdmkDBLookupComboBox
              Left = 68
              Top = 59
              Width = 685
              Height = 21
              Color = clWhite
              KeyField = 'CodID'
              ListField = 'Descricao'
              ListSource = DsFPCBO02
              TabOrder = 5
              dmkEditCB = EdCBO2002
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object CBFuncao: TdmkDBLookupComboBox
              Left = 52
              Top = 19
              Width = 313
              Height = 21
              Color = clWhite
              KeyField = 'CodID'
              ListField = 'Descricao'
              ListSource = DsFPFunca
              TabOrder = 1
              dmkEditCB = EdFuncao
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdDepto: TdmkEditCB
              Left = 396
              Top = 19
              Width = 41
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBDepto
              IgnoraDBLookupComboBox = False
            end
            object CBDepto: TdmkDBLookupComboBox
              Left = 438
              Top = 19
              Width = 315
              Height = 21
              Color = clWhite
              KeyField = 'CodID'
              ListField = 'Descricao'
              ListSource = DsFPDepto
              TabOrder = 3
              dmkEditCB = EdDepto
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdRegistro: TdmkEdit
              Left = 8
              Top = 140
              Width = 85
              Height = 21
              MaxLength = 10
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCategoria: TdmkEditCB
              Left = 96
              Top = 140
              Width = 41
              Height = 21
              Alignment = taRightJustify
              TabOrder = 9
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdCodigoChange
              DBLookupComboBox = CBCategoria
              IgnoraDBLookupComboBox = False
            end
            object CBCategoria: TdmkDBLookupComboBox
              Left = 140
              Top = 140
              Width = 145
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Descricao'
              ListSource = DsFPCateg
              TabOrder = 10
              dmkEditCB = EdCategoria
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object dmkEdHrSeman: TdmkEdit
              Left = 288
              Top = 140
              Width = 81
              Height = 21
              Alignment = taRightJustify
              TabOrder = 11
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdTipoSal: TdmkEditCB
              Left = 372
              Top = 140
              Width = 41
              Height = 21
              Alignment = taRightJustify
              TabOrder = 12
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdCodigoChange
              DBLookupComboBox = CBTipoSal
              IgnoraDBLookupComboBox = False
            end
            object CBTipoSal: TdmkDBLookupComboBox
              Left = 416
              Top = 140
              Width = 145
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Descricao'
              ListSource = DsFPTpSal
              TabOrder = 13
              dmkEditCB = EdTipoSal
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object dmkEdSalario: TdmkEdit
              Left = 564
              Top = 140
              Width = 81
              Height = 21
              Alignment = taRightJustify
              TabOrder = 14
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdFilhos: TdmkEdit
              Left = 648
              Top = 140
              Width = 61
              Height = 21
              Alignment = taRightJustify
              TabOrder = 15
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object dmkEdDependent: TdmkEdit
              Left = 712
              Top = 140
              Width = 61
              Height = 21
              Alignment = taRightJustify
              TabOrder = 16
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object dmkEdPensaoAlm: TdmkEdit
              Left = 112
              Top = 180
              Width = 105
              Height = 21
              Alignment = taRightJustify
              TabOrder = 18
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 6
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdBancoSal: TdmkEdit
              Left = 220
              Top = 180
              Width = 33
              Height = 21
              Alignment = taRightJustify
              TabOrder = 19
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdContaSal: TEdit
              Left = 252
              Top = 180
              Width = 117
              Height = 21
              TabOrder = 20
            end
            object EdCBO1994: TdmkEditCB
              Left = 8
              Top = 99
              Width = 57
              Height = 21
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              DBLookupComboBox = CBCBO1994
              IgnoraDBLookupComboBox = False
            end
            object CBCBO1994: TdmkDBLookupComboBox
              Left = 68
              Top = 99
              Width = 685
              Height = 21
              Color = clWhite
              KeyField = 'CodID'
              ListField = 'Descricao'
              ListSource = DsFPCBO94
              TabOrder = 7
              dmkEditCB = EdCBO1994
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object dmkEdInsalubrid: TdmkEdit
              Left = 372
              Top = 180
              Width = 81
              Height = 21
              Alignment = taRightJustify
              TabOrder = 21
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdPericulosi: TdmkEdit
              Left = 456
              Top = 180
              Width = 85
              Height = 21
              Alignment = taRightJustify
              TabOrder = 22
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object CkPensaoLiq: TCheckBox
              Left = 680
              Top = 168
              Width = 101
              Height = 37
              Caption = 'Calcular sobre o '#13#10'sal'#225'rio l'#237'quido.'
              TabOrder = 25
              WordWrap = True
            end
            object RgComoCalcIR: TRadioGroup
              Left = 8
              Top = 202
              Width = 381
              Height = 73
              Caption = ' Como '#233' pago o sal'#225'rio (para c'#225'lculo do IRRF): '
              ItemIndex = 1
              Items.Strings = (
                
                  'Dentro do m'#234's (quando o adiantamento e a quita'#231#227'o s'#227'o dentro do ' +
                  'm'#234's).'
                'Quita no m'#234's seguinte ao da compet'#234'ncia como explicado ao lado.'
                'Quita depois mas calcula o IRRF pela compet'#234'ncia (incorreto).')
              TabOrder = 26
            end
            object StaticText1: TStaticText
              Left = 392
              Top = 207
              Width = 381
              Height = 68
              AutoSize = False
              BorderStyle = sbsSunken
              Caption = 
                'COMO CALCULAR O IRRF:'#13#10'Se o empregador efetua adiantamentos no m' +
                #234's trabalhado e quita a folha de pagamento somente no m'#234's seguin' +
                'te, o Imposto de Renda na Fonte incidir'#225' sobre o somat'#243'rio dos v' +
                'alores dos adiantamentos e dos demais pagamentos efetuados dentr' +
                'o do m'#234's.'
              TabOrder = 27
            end
            object dmkEdHEFator: TdmkEdit
              Left = 544
              Top = 180
              Width = 85
              Height = 21
              Alignment = taRightJustify
              TabOrder = 23
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '100'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdPriDdPonto: TdmkEdit
              Left = 632
              Top = 180
              Width = 37
              Height = 21
              Alignment = taRightJustify
              TabOrder = 24
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '28'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdChapa: TdmkEdit
              Left = 8
              Top = 180
              Width = 101
              Height = 21
              TabOrder = 17
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Informa'#231#245'es trabalhistas'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label17: TLabel
            Left = 8
            Top = 4
            Width = 105
            Height = 13
            Caption = 'V'#237'nculo empregat'#237'cio:'
          end
          object Label19: TLabel
            Left = 8
            Top = 44
            Width = 45
            Height = 13
            Caption = 'Situa'#231#227'o:'
          end
          object Label20: TLabel
            Left = 8
            Top = 84
            Width = 86
            Height = 13
            Caption = 'Tipo de admiss'#227'o:'
          end
          object Label21: TLabel
            Left = 684
            Top = 84
            Width = 88
            Height = 13
            Caption = 'Data da admiss'#227'o:'
          end
          object Label123: TLabel
            Left = 312
            Top = 164
            Width = 68
            Height = 13
            Caption = '% lei 8923/94:'
          end
          object EdVinculo: TdmkEditCB
            Left = 8
            Top = 20
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCodigoChange
            DBLookupComboBox = CBVinculo
            IgnoraDBLookupComboBox = False
          end
          object CBVinculo: TdmkDBLookupComboBox
            Left = 68
            Top = 20
            Width = 708
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Descricao'
            ListSource = DsFPVincu
            TabOrder = 1
            dmkEditCB = EdVinculo
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CkAlvara: TCheckBox
            Left = 176
            Top = 124
            Width = 329
            Height = 17
            Caption = 'Tem alvar'#225' para o caso de menor n'#227'o aprendiz conforme a RAIS.'
            TabOrder = 8
          end
          object EdSituacao: TdmkEditCB
            Left = 8
            Top = 60
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCodigoChange
            DBLookupComboBox = CBSituacao
            IgnoraDBLookupComboBox = False
          end
          object CBSituacao: TdmkDBLookupComboBox
            Left = 68
            Top = 60
            Width = 708
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Descricao'
            ListSource = DsFPSitua
            TabOrder = 3
            dmkEditCB = EdSituacao
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdAdmissao: TdmkEditCB
            Left = 8
            Top = 100
            Width = 57
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdCodigoChange
            DBLookupComboBox = CBAdmissao
            IgnoraDBLookupComboBox = False
          end
          object CBAdmissao: TdmkDBLookupComboBox
            Left = 68
            Top = 100
            Width = 613
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Descricao'
            ListSource = DsFPAdmis
            TabOrder = 5
            dmkEditCB = EdAdmissao
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CkProfessor: TCheckBox
            Left = 176
            Top = 140
            Width = 97
            Height = 17
            Caption = #201' professor.'
            TabOrder = 9
          end
          object CkRAIS: TCheckBox
            Left = 8
            Top = 124
            Width = 165
            Height = 17
            Caption = 'Participa da rela'#231#227'o da RAIS.'
            TabOrder = 6
          end
          object CkGFIP: TCheckBox
            Left = 8
            Top = 140
            Width = 165
            Height = 17
            Caption = 'Participa da rela'#231#227'o da GFIP.'
            TabOrder = 7
          end
          object TPDataAdm: TDateTimePicker
            Left = 684
            Top = 100
            Width = 89
            Height = 21
            Date = 39470.691262291700000000
            Time = 39470.691262291700000000
            TabOrder = 10
          end
          object GroupBox9: TGroupBox
            Left = 8
            Top = 160
            Width = 301
            Height = 105
            Caption = ' Adicinal noturno: '
            TabOrder = 11
            object Label119: TLabel
              Left = 8
              Top = 16
              Width = 55
              Height = 13
              Caption = 'Hora inicial:'
            end
            object Label120: TLabel
              Left = 68
              Top = 16
              Width = 48
              Height = 13
              Caption = 'Hora final:'
            end
            object Label121: TLabel
              Left = 128
              Top = 16
              Width = 60
              Height = 13
              Caption = 'Tempo hora:'
            end
            object Label122: TLabel
              Left = 8
              Top = 56
              Width = 76
              Height = 13
              Caption = '% adic. noturno:'
            end
            object RGANTipo: TRadioGroup
              Left = 208
              Top = 15
              Width = 91
              Height = 88
              Align = alRight
              Caption = ' Atividade: '
              ItemIndex = 0
              Items.Strings = (
                'Urbana'
                'Agr'#237'cola'
                'Pecu'#225'ria')
              TabOrder = 0
              OnClick = RGANTipoClick
            end
            object dmkEdANIniHr: TdmkEdit
              Left = 8
              Top = 32
              Width = 57
              Height = 21
              TabOrder = 1
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdANFimHr: TdmkEdit
              Left = 68
              Top = 32
              Width = 57
              Height = 21
              TabOrder = 2
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdANMinHr: TdmkEdit
              Left = 128
              Top = 32
              Width = 65
              Height = 21
              TabOrder = 3
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfLong
              Texto = '00:00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEdAdiciNotur: TdmkEdit
              Left = 8
              Top = 72
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object dmkEdLei8923_94: TdmkEdit
            Left = 312
            Top = 180
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 12
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'Informa'#231#245'es pessoais'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label59: TLabel
            Left = 8
            Top = 4
            Width = 71
            Height = 13
            Caption = 'Nacionalidade:'
          end
          object LaAnoChegada: TLabel
            Left = 256
            Top = 4
            Width = 67
            Height = 13
            Caption = 'Ano chegada:'
            Enabled = False
          end
          object Label60: TLabel
            Left = 8
            Top = 44
            Width = 87
            Height = 13
            Caption = 'Grau de instru'#231#227'o:'
          end
          object Label61: TLabel
            Left = 328
            Top = 4
            Width = 57
            Height = 13
            Caption = 'Estado civil:'
          end
          object Label67: TLabel
            Left = 576
            Top = 4
            Width = 29
            Height = 13
            Caption = 'Ra'#231'a:'
          end
          object Label69: TLabel
            Left = 8
            Top = 84
            Width = 209
            Height = 13
            Caption = 'Local de trabalho (quando em outra cidade):'
          end
          object Label70: TLabel
            Left = 8
            Top = 124
            Width = 264
            Height = 13
            Caption = 'Arquivo da foto (Formato bitmap (BMP):  (Formato 3 x 4):'
          end
          object SpeedButton5: TSpeedButton
            Left = 752
            Top = 140
            Width = 23
            Height = 22
            OnClick = SpeedButton5Click
          end
          object EdNacionalid: TdmkEditCB
            Left = 8
            Top = 20
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCodigoChange
            DBLookupComboBox = CBNacionalid
            IgnoraDBLookupComboBox = False
          end
          object CBNacionalid: TdmkDBLookupComboBox
            Left = 68
            Top = 20
            Width = 185
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Descricao'
            ListSource = DsFPNacio
            TabOrder = 1
            dmkEditCB = EdNacionalid
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object dmkEdAnoChegada: TdmkEdit
            Left = 256
            Top = 20
            Width = 69
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdGrauInstru: TdmkEditCB
            Left = 8
            Top = 60
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCodigoChange
            DBLookupComboBox = CBGrauInstru
            IgnoraDBLookupComboBox = False
          end
          object CBGrauInstru: TdmkDBLookupComboBox
            Left = 68
            Top = 60
            Width = 708
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Descricao'
            ListSource = DsFPGrauI
            TabOrder = 8
            dmkEditCB = EdGrauInstru
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdEstCivil: TdmkEditCB
            Left = 328
            Top = 20
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCodigoChange
            DBLookupComboBox = CBEstCivil
            IgnoraDBLookupComboBox = False
          end
          object CBEstCivil: TdmkDBLookupComboBox
            Left = 388
            Top = 20
            Width = 185
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Descricao'
            ListSource = DsFPCivil
            TabOrder = 4
            dmkEditCB = EdEstCivil
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdRaca: TdmkEditCB
            Left = 576
            Top = 20
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCodigoChange
            DBLookupComboBox = CBRaca
            IgnoraDBLookupComboBox = False
          end
          object CBRaca: TdmkDBLookupComboBox
            Left = 636
            Top = 20
            Width = 137
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Descricao'
            ListSource = DsFPRacas
            TabOrder = 6
            dmkEditCB = EdRaca
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdLocalTrab: TdmkEditCB
            Left = 8
            Top = 100
            Width = 57
            Height = 21
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            DBLookupComboBox = CBLocalTrab
            IgnoraDBLookupComboBox = False
          end
          object CBLocalTrab: TdmkDBLookupComboBox
            Left = 68
            Top = 100
            Width = 185
            Height = 21
            Color = clWhite
            KeyField = 'CodID'
            ListField = 'Nome'
            ListSource = DsFPMunic
            TabOrder = 10
            dmkEditCB = EdLocalTrab
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdFoto: TEdit
            Left = 8
            Top = 140
            Width = 741
            Height = 21
            TabOrder = 11
            OnChange = EdFotoChange
          end
        end
        object TabSheet8: TTabSheet
          Caption = 'Documenta'#231#227'o'
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label88: TLabel
            Left = 8
            Top = 8
            Width = 31
            Height = 13
            Caption = 'CTPS:'
          end
          object Label89: TLabel
            Left = 132
            Top = 8
            Width = 58
            Height = 13
            Caption = 'S'#233'rie CTPS:'
          end
          object Label90: TLabel
            Left = 216
            Top = 8
            Width = 20
            Height = 13
            Caption = 'PIS:'
          end
          object Label91: TLabel
            Left = 360
            Top = 8
            Width = 77
            Height = 13
            Caption = 'T'#237'tulo de eleitor:'
          end
          object Label92: TLabel
            Left = 504
            Top = 8
            Width = 53
            Height = 13
            Caption = 'Reservista:'
          end
          object EdCTPS: TEdit
            Left = 8
            Top = 24
            Width = 120
            Height = 21
            TabOrder = 0
          end
          object EdSerieCTPS: TEdit
            Left = 132
            Top = 24
            Width = 80
            Height = 21
            TabOrder = 1
          end
          object EdPIS: TEdit
            Left = 216
            Top = 24
            Width = 140
            Height = 21
            TabOrder = 2
          end
          object EdTitEleitor: TEdit
            Left = 360
            Top = 24
            Width = 140
            Height = 21
            TabOrder = 3
          end
          object EdReservista: TEdit
            Left = 504
            Top = 24
            Width = 140
            Height = 21
            TabOrder = 4
          end
          object GroupBox2: TGroupBox
            Left = 8
            Top = 76
            Width = 261
            Height = 81
            Caption = ' Fundo de garantia por tempo de servi'#231'o: '
            TabOrder = 5
            object Label40: TLabel
              Left = 12
              Top = 36
              Width = 74
              Height = 13
              Caption = 'Data da op'#231#227'o:'
            end
            object Label41: TLabel
              Left = 108
              Top = 36
              Width = 127
              Height = 13
              Caption = 'Banco e c/corrente FGTS:'
            end
            object CkOptaFGTS: TCheckBox
              Left = 12
              Top = 16
              Width = 109
              Height = 17
              Caption = 'Optante do FGTS.'
              TabOrder = 0
            end
            object dmkEdBancoFGTS: TdmkEdit
              Left = 108
              Top = 52
              Width = 33
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdContaFGTS: TEdit
              Left = 140
              Top = 52
              Width = 94
              Height = 21
              TabOrder = 2
            end
            object TPDataFGTS: TDateTimePicker
              Left = 12
              Top = 52
              Width = 93
              Height = 21
              Date = 39470.698853437500000000
              Time = 39470.698853437500000000
              TabOrder = 3
            end
          end
          object GroupBox3: TGroupBox
            Left = 272
            Top = 76
            Width = 225
            Height = 81
            Caption = ' Exames m'#233'dicos: '
            TabOrder = 6
            object Label43: TLabel
              Left = 12
              Top = 36
              Width = 88
              Height = 13
              Caption = 'Data da 1'#186' exame:'
            end
            object Label44: TLabel
              Left = 108
              Top = 36
              Width = 26
              Height = 13
              Caption = 'Ciclo:'
            end
            object Label45: TLabel
              Left = 132
              Top = 56
              Width = 33
              Height = 13
              Caption = 'meses.'
            end
            object dmkEdPerExame: TdmkEdit
              Left = 108
              Top = 52
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object TPDataExame: TDateTimePicker
              Left = 12
              Top = 52
              Width = 93
              Height = 21
              Date = 39470.698636967600000000
              Time = 39470.698636967600000000
              TabOrder = 1
            end
          end
          object GroupBox4: TGroupBox
            Left = 500
            Top = 76
            Width = 275
            Height = 81
            Caption = ' Afastamento: '
            TabOrder = 7
            object Label46: TLabel
              Left = 12
              Top = 36
              Width = 87
              Height = 13
              Caption = 'Data afastamento:'
            end
            object Label47: TLabel
              Left = 108
              Top = 36
              Width = 77
              Height = 13
              Caption = 'Data do retorno:'
            end
            object Label49: TLabel
              Left = 204
              Top = 36
              Width = 34
              Height = 13
              Caption = 'Meses:'
            end
            object dmkEdMesesAf: TdmkEdit
              Left = 204
              Top = 52
              Width = 33
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object TPDataIniAf: TDateTimePicker
              Left = 12
              Top = 52
              Width = 93
              Height = 21
              Date = 39470.698122280100000000
              Time = 39470.698122280100000000
              TabOrder = 1
            end
            object TPDataFinAf: TDateTimePicker
              Left = 108
              Top = 52
              Width = 89
              Height = 21
              Date = 39470.698400787000000000
              Time = 39470.698400787000000000
              TabOrder = 2
            end
          end
          object CkDeficiente: TCheckBox
            Left = 8
            Top = 56
            Width = 193
            Height = 17
            Caption = #201' deficiente f'#237'sico conforme RAIS.'
            TabOrder = 8
          end
        end
        object TabSheet9: TTabSheet
          Caption = 'Informa'#231#245'es da entidade'
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
        object TabSheet14: TTabSheet
          Caption = 'Obs.'
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
        object TabSheet16: TTabSheet
          Caption = 'Demiss'#227'o'
          ImageIndex = 7
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 916
            Height = 318
            Align = alClient
            TabOrder = 0
            Visible = False
            object Label48: TLabel
              Left = 8
              Top = 4
              Width = 54
              Height = 13
              Caption = 'Data aviso:'
            end
            object Label50: TLabel
              Left = 212
              Top = 4
              Width = 52
              Height = 13
              Caption = 'Dias aviso:'
            end
            object Label51: TLabel
              Left = 8
              Top = 44
              Width = 73
              Height = 13
              Caption = 'Data demiss'#227'o:'
            end
            object Label52: TLabel
              Left = 100
              Top = 44
              Width = 97
              Height = 13
              Caption = 'Motivo da demiss'#227'o:'
            end
            object CheckBox1: TCheckBox
              Left = 104
              Top = 24
              Width = 105
              Height = 17
              Caption = 'Aviso indenizado.'
              TabOrder = 0
            end
            object dmkEdit2: TdmkEdit
              Left = 212
              Top = 20
              Width = 65
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdDemissao: TdmkEditCB
              Left = 100
              Top = 60
              Width = 57
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdCodigoChange
              DBLookupComboBox = CBDemissao
              IgnoraDBLookupComboBox = False
            end
            object CBDemissao: TdmkDBLookupComboBox
              Left = 160
              Top = 60
              Width = 613
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Descricao'
              ListSource = DsFPDeslg
              TabOrder = 3
              dmkEditCB = EdDemissao
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 483
      Width = 924
      Height = 63
      Align = alBottom
      TabOrder = 2
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 814
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 924
    Height = 546
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PnDataTop: TPanel
      Left = 0
      Top = 0
      Width = 924
      Height = 106
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Panel11: TPanel
        Left = 844
        Top = 0
        Width = 80
        Height = 106
        Align = alRight
        TabOrder = 0
        object LaFotoDB: TLabel
          Left = 1
          Top = 1
          Width = 39
          Height = 78
          Align = alClient
          Alignment = taCenter
          Caption = #13#10'Foto'#13#10#13#10'N'#227'o'#13#10#13#10'Definida'
        end
        object Image3: TImage
          Left = 1
          Top = 1
          Width = 78
          Height = 104
          Align = alClient
          Center = True
          Stretch = True
          ExplicitHeight = 102
        end
      end
      object Panel12: TPanel
        Left = 0
        Top = 0
        Width = 844
        Height = 106
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label1: TLabel
          Left = 12
          Top = 4
          Width = 104
          Height = 13
          Caption = 'Cadastro de entidade:'
        end
        object DBEdCodigo: TDBEdit
          Left = 12
          Top = 20
          Width = 48
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsFPFunci
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdNome: TDBEdit
          Left = 64
          Top = 20
          Width = 640
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'NUMEFUNCI'
          DataSource = DsFPFunci
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object DBEdit9: TDBEdit
          Left = 12
          Top = 44
          Width = 692
          Height = 21
          DataField = 'NUMEEMPRESA'
          DataSource = DsFPFunci
          TabOrder = 2
        end
      end
    end
    object PainelData: TPanel
      Left = 0
      Top = 106
      Width = 924
      Height = 350
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object PageControl2: TPageControl
        Left = 0
        Top = 0
        Width = 924
        Height = 350
        ActivePage = TabSheet4
        Align = alClient
        TabOrder = 0
        OnChange = PageControl2Change
        object TabSheet2: TTabSheet
          Caption = 'Informa'#231#245'es sal'#225'rio / recibo'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel10: TPanel
            Left = 0
            Top = 0
            Width = 916
            Height = 322
            Align = alClient
            BevelOuter = bvNone
            Enabled = False
            ParentBackground = False
            TabOrder = 0
            object Label10: TLabel
              Left = 8
              Top = 4
              Width = 39
              Height = 13
              Caption = 'Fun'#231#227'o:'
            end
            object Label11: TLabel
              Left = 8
              Top = 44
              Width = 61
              Height = 13
              Caption = 'C.B.O. 2002:'
            end
            object Label12: TLabel
              Left = 392
              Top = 4
              Width = 70
              Height = 13
              Caption = 'Departamento:'
            end
            object Label15: TLabel
              Left = 8
              Top = 124
              Width = 42
              Height = 13
              Caption = 'Registro:'
            end
            object Label16: TLabel
              Left = 96
              Top = 124
              Width = 48
              Height = 13
              Caption = 'Categoria:'
            end
            object Label26: TLabel
              Left = 288
              Top = 124
              Width = 78
              Height = 13
              Caption = 'Horas semanais:'
            end
            object Label32: TLabel
              Left = 372
              Top = 124
              Width = 72
              Height = 13
              Caption = 'Tipo de sal'#225'rio:'
            end
            object Label33: TLabel
              Left = 564
              Top = 124
              Width = 35
              Height = 13
              Caption = 'Sal'#225'rio:'
            end
            object Label34: TLabel
              Left = 648
              Top = 124
              Width = 42
              Height = 13
              Caption = 'N'#186' filhos:'
            end
            object Label35: TLabel
              Left = 712
              Top = 124
              Width = 57
              Height = 13
              Caption = 'N'#186' depend.:'
            end
            object Label37: TLabel
              Left = 112
              Top = 164
              Width = 103
              Height = 13
              Caption = '% pens'#227'o aliment'#237'cia:'
            end
            object Label38: TLabel
              Left = 220
              Top = 164
              Width = 127
              Height = 13
              Caption = 'Banco e c.corrente sal'#225'rio:'
            end
            object Label75: TLabel
              Left = 372
              Top = 164
              Width = 76
              Height = 13
              Caption = '% insalubridade:'
            end
            object Label76: TLabel
              Left = 456
              Top = 164
              Width = 82
              Height = 13
              Caption = '% periculosidade:'
            end
            object Label73: TLabel
              Left = 8
              Top = 84
              Width = 61
              Height = 13
              Caption = 'C.B.O. 1994:'
            end
            object Label117: TLabel
              Left = 544
              Top = 164
              Width = 82
              Height = 13
              Caption = '% H.E semanais'#185':'
            end
            object Label118: TLabel
              Left = 12
              Top = 280
              Width = 670
              Height = 13
              Caption = 
                #185' porcentagem agregada a hora normal. Ex.: 50, 60 ou 70%  para h' +
                'oras extras em dias de trabalho normais (n'#227'o no descanso quando ' +
                #233' 100%).'
            end
            object Label139: TLabel
              Left = 632
              Top = 164
              Width = 38
              Height = 13
              Caption = 'PDCP '#178':'
              FocusControl = DBEdit53
            end
            object Label140: TLabel
              Left = 12
              Top = 300
              Width = 347
              Height = 13
              Caption = 
                #178': Primeiro dia (do m'#234's) do cart'#227'o ponto. (zero quando n'#227'o contr' +
                'ola ponto)'
            end
            object Label167: TLabel
              Left = 8
              Top = 164
              Width = 34
              Height = 13
              Caption = 'Chapa:'
            end
            object DBEdit1: TDBEdit
              Left = 44
              Top = 20
              Width = 345
              Height = 21
              DataField = 'NOMEFUNCAO'
              DataSource = DsFPFunci
              TabOrder = 0
            end
            object DBEdit2: TDBEdit
              Left = 428
              Top = 20
              Width = 345
              Height = 21
              DataField = 'NOMEDEPTO'
              DataSource = DsFPFunci
              TabOrder = 1
            end
            object DBEdit3: TDBEdit
              Left = 56
              Top = 60
              Width = 717
              Height = 21
              DataField = 'NOMECBO2002'
              DataSource = DsFPFunci
              TabOrder = 2
            end
            object DBEdit4: TDBEdit
              Left = 392
              Top = 20
              Width = 37
              Height = 21
              DataField = 'Depto'
              DataSource = DsFPFunci
              TabOrder = 3
            end
            object DBEdit5: TDBEdit
              Left = 8
              Top = 20
              Width = 37
              Height = 21
              DataField = 'Funcao'
              DataSource = DsFPFunci
              TabOrder = 4
            end
            object DBEdit6: TDBEdit
              Left = 8
              Top = 60
              Width = 49
              Height = 21
              DataField = 'CBO2002'
              DataSource = DsFPFunci
              TabOrder = 5
            end
            object DBEdit7: TDBEdit
              Left = 8
              Top = 140
              Width = 85
              Height = 21
              DataField = 'Registro'
              DataSource = DsFPFunci
              TabOrder = 6
            end
            object DBEdit8: TDBEdit
              Left = 96
              Top = 140
              Width = 189
              Height = 21
              DataField = 'NOMECATEG'
              DataSource = DsFPFunci
              TabOrder = 7
            end
            object DBEdit15: TDBEdit
              Left = 288
              Top = 140
              Width = 81
              Height = 21
              DataField = 'HrSeman'
              DataSource = DsFPFunci
              TabOrder = 8
            end
            object DBEdit16: TDBEdit
              Left = 372
              Top = 140
              Width = 189
              Height = 21
              DataField = 'NOMETPSAL'
              DataSource = DsFPFunci
              TabOrder = 9
            end
            object DBEdit17: TDBEdit
              Left = 564
              Top = 140
              Width = 81
              Height = 21
              DataField = 'Salario'
              DataSource = DsFPFunci
              TabOrder = 10
            end
            object DBEdit18: TDBEdit
              Left = 648
              Top = 140
              Width = 61
              Height = 21
              DataField = 'Filhos'
              DataSource = DsFPFunci
              TabOrder = 11
            end
            object DBEdit19: TDBEdit
              Left = 712
              Top = 140
              Width = 61
              Height = 21
              DataField = 'Dependent'
              DataSource = DsFPFunci
              TabOrder = 12
            end
            object DBEdit20: TDBEdit
              Left = 112
              Top = 180
              Width = 105
              Height = 21
              DataField = 'PensaoAlm'
              DataSource = DsFPFunci
              TabOrder = 13
            end
            object DBEdit21: TDBEdit
              Left = 220
              Top = 180
              Width = 37
              Height = 21
              DataField = 'BancoSal'
              DataSource = DsFPFunci
              TabOrder = 14
            end
            object DBEdit22: TDBEdit
              Left = 256
              Top = 180
              Width = 113
              Height = 21
              DataField = 'ContaSal'
              DataSource = DsFPFunci
              TabOrder = 15
            end
            object DBEdit31: TDBEdit
              Left = 372
              Top = 180
              Width = 81
              Height = 21
              DataField = 'Insalubrid'
              DataSource = DsFPFunci
              TabOrder = 16
            end
            object DBEdit32: TDBEdit
              Left = 456
              Top = 180
              Width = 85
              Height = 21
              DataField = 'Periculosi'
              DataSource = DsFPFunci
              TabOrder = 17
            end
            object DBEdit33: TDBEdit
              Left = 8
              Top = 100
              Width = 49
              Height = 21
              DataField = 'Cargo'
              DataSource = DsFPFunci
              TabOrder = 18
            end
            object DBEdit34: TDBEdit
              Left = 56
              Top = 100
              Width = 717
              Height = 21
              DataField = 'NOMECARGO'
              DataSource = DsFPFunci
              TabOrder = 19
            end
            object DBCheckBox6: TDBCheckBox
              Left = 680
              Top = 172
              Width = 97
              Height = 33
              Caption = 'Calcular sobre o sal'#225'rio l'#237'quido.'
              DataField = 'PensaoLiq'
              DataSource = DsFPFunci
              TabOrder = 20
              ValueChecked = '1'
              ValueUnchecked = '0'
              WordWrap = True
            end
            object StaticText2: TStaticText
              Left = 392
              Top = 207
              Width = 381
              Height = 68
              AutoSize = False
              BorderStyle = sbsSunken
              Caption = 
                'COMO CALCULAR O IRRF:'#13#10'Se o empregador efetua adiantamentos no m' +
                #234's trabalhado e quita a folha de pagamento somente no m'#234's seguin' +
                'te, o Imposto de Renda na Fonte incidir'#225' sobre o somat'#243'rio dos v' +
                'alores dos adiantamentos e dos demais pagamentos efetuados dentr' +
                'o do m'#234's.'
              TabOrder = 21
            end
            object DBRadioGroup1: TDBRadioGroup
              Left = 8
              Top = 202
              Width = 381
              Height = 73
              Caption = ' Como '#233' pago o sal'#225'rio (para c'#225'lculo do IRRF): '
              DataField = 'ComoCalcIR'
              DataSource = DsFPFunci
              Items.Strings = (
                
                  'Dentro do m'#234's (quando o adiantamento e a quita'#231#227'o s'#227'o dentro do ' +
                  'm'#234's).'
                'Quita no m'#234's seguinte ao da compet'#234'ncia como explicado ao lado.'
                'Quita depois mas calcula o IRRF pela compet'#234'ncia (incorreto).')
              ParentBackground = True
              TabOrder = 22
              Values.Strings = (
                '0'
                '1'
                '2')
            end
            object DBEdit47: TDBEdit
              Left = 544
              Top = 180
              Width = 85
              Height = 21
              DataField = 'HEFator'
              DataSource = DsFPFunci
              TabOrder = 23
            end
            object DBEdit53: TDBEdit
              Left = 632
              Top = 180
              Width = 41
              Height = 21
              DataField = 'PriDdPonto'
              DataSource = DsFPFunci
              TabOrder = 24
            end
            object DBEdit54: TDBEdit
              Left = 8
              Top = 180
              Width = 101
              Height = 21
              DataField = 'Chapa'
              DataSource = DsFPFunci
              TabOrder = 25
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Informa'#231#245'es trabalhistas'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel27: TPanel
            Left = 0
            Top = 0
            Width = 916
            Height = 125
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label18: TLabel
              Left = 8
              Top = 4
              Width = 105
              Height = 13
              Caption = 'V'#237'nculo empregat'#237'cio:'
              FocusControl = DBEdit11
            end
            object Label23: TLabel
              Left = 8
              Top = 44
              Width = 45
              Height = 13
              Caption = 'Situa'#231#227'o:'
            end
            object Label24: TLabel
              Left = 8
              Top = 84
              Width = 86
              Height = 13
              Caption = 'Tipo de admiss'#227'o:'
            end
            object Label25: TLabel
              Left = 684
              Top = 84
              Width = 88
              Height = 13
              Caption = 'Data da admiss'#227'o:'
            end
            object DBEdit11: TDBEdit
              Left = 8
              Top = 20
              Width = 764
              Height = 21
              DataField = 'NOMEVINCU'
              DataSource = DsFPFunci
              TabOrder = 0
            end
            object DBEdit12: TDBEdit
              Left = 8
              Top = 60
              Width = 764
              Height = 21
              DataField = 'NOMESITUA'
              DataSource = DsFPFunci
              TabOrder = 1
            end
            object DBEdit13: TDBEdit
              Left = 8
              Top = 100
              Width = 673
              Height = 21
              DataField = 'NOMEADMIS'
              DataSource = DsFPFunci
              TabOrder = 2
            end
            object DBEdit14: TDBEdit
              Left = 684
              Top = 100
              Width = 89
              Height = 21
              DataField = 'DataAdm'
              DataSource = DsFPFunci
              TabOrder = 3
            end
          end
          object Panel40: TPanel
            Left = 0
            Top = 125
            Width = 916
            Height = 197
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Label128: TLabel
              Left = 656
              Top = 12
              Width = 68
              Height = 13
              Caption = '% lei 8923/94:'
            end
            object DBCheckBox4: TDBCheckBox
              Left = 8
              Top = 4
              Width = 169
              Height = 17
              Caption = 'Participa da rela'#231#227'o da RAIS.'
              DataField = 'RAIS'
              DataSource = DsFPFunci
              TabOrder = 0
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBCheckBox5: TDBCheckBox
              Left = 8
              Top = 20
              Width = 165
              Height = 17
              Caption = 'Participa da rela'#231#227'o da GFIP.'
              DataField = 'GFIP'
              DataSource = DsFPFunci
              TabOrder = 1
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBCheckBox1: TDBCheckBox
              Left = 8
              Top = 36
              Width = 337
              Height = 17
              Caption = 'Tem alvar'#225' para o caso de menor n'#227'o aprendiz conforme a RAIS.'
              DataField = 'Alvara'
              DataSource = DsFPFunci
              TabOrder = 2
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBCheckBox2: TDBCheckBox
              Left = 184
              Top = 20
              Width = 97
              Height = 17
              Caption = #201' professor.'
              DataField = 'Professor'
              DataSource = DsFPFunci
              TabOrder = 3
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object GroupBox10: TGroupBox
              Left = 348
              Top = 8
              Width = 301
              Height = 105
              Caption = ' Adicinal noturno: '
              TabOrder = 4
              object Label124: TLabel
                Left = 8
                Top = 16
                Width = 55
                Height = 13
                Caption = 'Hora inicial:'
              end
              object Label125: TLabel
                Left = 68
                Top = 16
                Width = 48
                Height = 13
                Caption = 'Hora final:'
              end
              object Label126: TLabel
                Left = 128
                Top = 16
                Width = 60
                Height = 13
                Caption = 'Tempo hora:'
              end
              object Label127: TLabel
                Left = 8
                Top = 56
                Width = 76
                Height = 13
                Caption = '% adic. noturno:'
              end
              object DBEdit49: TDBEdit
                Left = 8
                Top = 32
                Width = 57
                Height = 21
                DataField = 'ANIniHr'
                DataSource = DsFPFunci
                TabOrder = 0
              end
              object DBEdit50: TDBEdit
                Left = 68
                Top = 32
                Width = 57
                Height = 21
                DataField = 'ANFimHr'
                DataSource = DsFPFunci
                TabOrder = 1
              end
              object DBEdit51: TDBEdit
                Left = 128
                Top = 32
                Width = 57
                Height = 21
                DataField = 'ANMinHr'
                DataSource = DsFPFunci
                TabOrder = 2
              end
              object DBEdit52: TDBEdit
                Left = 8
                Top = 72
                Width = 80
                Height = 21
                DataField = 'AdiciNotur'
                DataSource = DsFPFunci
                TabOrder = 3
              end
              object DBRadioGroup2: TDBRadioGroup
                Left = 208
                Top = 15
                Width = 91
                Height = 88
                Align = alRight
                Caption = ' Atividade: '
                DataField = 'ANTipo'
                DataSource = DsFPFunci
                Items.Strings = (
                  'Urbana'
                  'Agr'#237'cola'
                  'Pecu'#225'ria')
                ParentBackground = True
                TabOrder = 4
                Values.Strings = (
                  '0'
                  '1'
                  '2')
              end
            end
            object DBEdit48: TDBEdit
              Left = 656
              Top = 28
              Width = 80
              Height = 21
              DataField = 'Lei8923_94'
              DataSource = DsFPFunci
              TabOrder = 5
            end
          end
        end
        object TabSheet6: TTabSheet
          Caption = 'Informa'#231#245'es pessoais'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel41: TPanel
            Left = 0
            Top = 0
            Width = 916
            Height = 322
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label74: TLabel
              Left = 8
              Top = 4
              Width = 71
              Height = 13
              Caption = 'Nacionalidade:'
            end
            object Label80: TLabel
              Left = 8
              Top = 44
              Width = 87
              Height = 13
              Caption = 'Grau de instru'#231#227'o:'
            end
            object Label86: TLabel
              Left = 8
              Top = 84
              Width = 209
              Height = 13
              Caption = 'Local de trabalho (quando em outra cidade):'
            end
            object Label87: TLabel
              Left = 8
              Top = 124
              Width = 264
              Height = 13
              Caption = 'Arquivo da foto (Formato bitmap (BMP):  (Formato 3 x 4):'
            end
            object Label77: TLabel
              Left = 256
              Top = 4
              Width = 67
              Height = 13
              Caption = 'Ano chegada:'
              Enabled = False
            end
            object Label78: TLabel
              Left = 328
              Top = 4
              Width = 57
              Height = 13
              Caption = 'Estado civil:'
            end
            object Label79: TLabel
              Left = 576
              Top = 4
              Width = 29
              Height = 13
              Caption = 'Ra'#231'a:'
            end
            object DBEdit35: TDBEdit
              Left = 8
              Top = 20
              Width = 245
              Height = 21
              DataField = 'NOMENACIONALID'
              DataSource = DsFPFunci
              TabOrder = 0
            end
            object DBEdit38: TDBEdit
              Left = 8
              Top = 60
              Width = 768
              Height = 21
              DataField = 'NOMEGRAUI'
              DataSource = DsFPFunci
              TabOrder = 1
            end
            object DBEdit46: TDBEdit
              Left = 8
              Top = 100
              Width = 214
              Height = 21
              DataField = 'NOMELOCTRAB'
              DataSource = DsFPFunci
              TabOrder = 2
            end
            object DBEdit45: TDBEdit
              Left = 8
              Top = 140
              Width = 768
              Height = 21
              DataField = 'Foto'
              DataSource = DsFPFunci
              TabOrder = 3
            end
            object DBEdit44: TDBEdit
              Left = 256
              Top = 20
              Width = 69
              Height = 21
              DataField = 'AnoChegada'
              DataSource = DsFPFunci
              TabOrder = 4
            end
            object DBEdit36: TDBEdit
              Left = 328
              Top = 20
              Width = 245
              Height = 21
              DataField = 'NOMEECIVIL'
              DataSource = DsFPFunci
              TabOrder = 5
            end
            object DBEdit37: TDBEdit
              Left = 576
              Top = 20
              Width = 201
              Height = 21
              DataField = 'NOMERACA'
              DataSource = DsFPFunci
              TabOrder = 6
            end
          end
        end
        object TabSheet7: TTabSheet
          Caption = 'Documenta'#231#227'o'
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel42: TPanel
            Left = 0
            Top = 0
            Width = 916
            Height = 322
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label81: TLabel
              Left = 8
              Top = 8
              Width = 31
              Height = 13
              Caption = 'CTPS:'
            end
            object Label82: TLabel
              Left = 132
              Top = 8
              Width = 58
              Height = 13
              Caption = 'S'#233'rie CTPS:'
            end
            object Label83: TLabel
              Left = 216
              Top = 8
              Width = 20
              Height = 13
              Caption = 'PIS:'
            end
            object Label84: TLabel
              Left = 360
              Top = 8
              Width = 77
              Height = 13
              Caption = 'T'#237'tulo de eleitor:'
            end
            object Label85: TLabel
              Left = 504
              Top = 8
              Width = 53
              Height = 13
              Caption = 'Reservista:'
            end
            object DBEdit39: TDBEdit
              Left = 8
              Top = 24
              Width = 121
              Height = 21
              DataField = 'CTPS'
              DataSource = DsFPFunci
              TabOrder = 0
            end
            object DBEdit40: TDBEdit
              Left = 132
              Top = 24
              Width = 81
              Height = 21
              DataField = 'SerieCTPS'
              DataSource = DsFPFunci
              TabOrder = 1
            end
            object DBEdit41: TDBEdit
              Left = 216
              Top = 24
              Width = 141
              Height = 21
              DataField = 'PIS'
              DataSource = DsFPFunci
              TabOrder = 2
            end
            object DBEdit42: TDBEdit
              Left = 360
              Top = 24
              Width = 141
              Height = 21
              DataField = 'TitEleitor'
              DataSource = DsFPFunci
              TabOrder = 3
            end
            object DBEdit43: TDBEdit
              Left = 504
              Top = 24
              Width = 140
              Height = 21
              DataField = 'Reservista'
              DataSource = DsFPFunci
              TabOrder = 4
            end
            object CheckBox2: TCheckBox
              Left = 8
              Top = 56
              Width = 193
              Height = 17
              Caption = #201' deficiente f'#237'sico conforme RAIS.'
              TabOrder = 5
            end
            object GroupBox1: TGroupBox
              Left = 8
              Top = 76
              Width = 261
              Height = 81
              Caption = ' Fundo de garantia por tempo de servi'#231'o: '
              TabOrder = 6
              object Label39: TLabel
                Left = 12
                Top = 36
                Width = 74
                Height = 13
                Caption = 'Data da op'#231#227'o:'
              end
              object Label42: TLabel
                Left = 108
                Top = 36
                Width = 125
                Height = 13
                Caption = 'Banco e c.corrente FGTS:'
              end
              object DBCheckBox3: TDBCheckBox
                Left = 16
                Top = 16
                Width = 109
                Height = 17
                Caption = 'Optante do FGTS.'
                DataField = 'OptaFGTS'
                DataSource = DsFPFunci
                TabOrder = 0
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object DBEdit23: TDBEdit
                Left = 12
                Top = 52
                Width = 90
                Height = 21
                DataField = 'DataFGTS'
                DataSource = DsFPFunci
                TabOrder = 1
              end
              object DBEdit24: TDBEdit
                Left = 108
                Top = 52
                Width = 33
                Height = 21
                DataField = 'BancoFGTS'
                DataSource = DsFPFunci
                TabOrder = 2
              end
              object DBEdit25: TDBEdit
                Left = 140
                Top = 52
                Width = 94
                Height = 21
                DataField = 'ContaFGTS'
                DataSource = DsFPFunci
                TabOrder = 3
              end
            end
            object GroupBox5: TGroupBox
              Left = 272
              Top = 76
              Width = 225
              Height = 81
              Caption = ' Exames m'#233'dicos: '
              TabOrder = 7
              object Label53: TLabel
                Left = 12
                Top = 36
                Width = 88
                Height = 13
                Caption = 'Data da 1'#186' exame:'
              end
              object Label54: TLabel
                Left = 108
                Top = 36
                Width = 26
                Height = 13
                Caption = 'Ciclo:'
              end
              object Label55: TLabel
                Left = 132
                Top = 56
                Width = 33
                Height = 13
                Caption = 'meses.'
              end
              object DBEdit26: TDBEdit
                Left = 12
                Top = 52
                Width = 90
                Height = 21
                DataField = 'DataExame'
                DataSource = DsFPFunci
                TabOrder = 0
              end
              object DBEdit27: TDBEdit
                Left = 108
                Top = 52
                Width = 21
                Height = 21
                DataField = 'PerExame'
                DataSource = DsFPFunci
                TabOrder = 1
              end
            end
            object GroupBox6: TGroupBox
              Left = 500
              Top = 76
              Width = 275
              Height = 81
              Caption = ' Afastamento: '
              TabOrder = 8
              object Label56: TLabel
                Left = 12
                Top = 36
                Width = 87
                Height = 13
                Caption = 'Data afastamento:'
              end
              object Label57: TLabel
                Left = 108
                Top = 36
                Width = 77
                Height = 13
                Caption = 'Data do retorno:'
              end
              object Label58: TLabel
                Left = 204
                Top = 36
                Width = 34
                Height = 13
                Caption = 'Meses:'
              end
              object DBEdit28: TDBEdit
                Left = 12
                Top = 52
                Width = 90
                Height = 21
                DataField = 'DataIniAf'
                DataSource = DsFPFunci
                TabOrder = 0
              end
              object DBEdit29: TDBEdit
                Left = 108
                Top = 52
                Width = 90
                Height = 21
                DataField = 'DataFinAf'
                DataSource = DsFPFunci
                TabOrder = 1
              end
              object DBEdit30: TDBEdit
                Left = 204
                Top = 52
                Width = 33
                Height = 21
                DataField = 'MesesAf'
                DataSource = DsFPFunci
                TabOrder = 2
              end
            end
          end
        end
        object TabSheet11: TTabSheet
          Caption = 'Dados entidade'
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
        object TabSheet12: TTabSheet
          Caption = 'Obs.'
          ImageIndex = 5
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
        object TabSheet13: TTabSheet
          Caption = 'F'#233'rias'
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel22: TPanel
            Left = 0
            Top = 0
            Width = 916
            Height = 322
            Align = alClient
            TabOrder = 0
            object PnFerias: TPanel
              Left = 1
              Top = 58
              Width = 914
              Height = 215
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              Visible = False
              object Panel21: TPanel
                Left = 0
                Top = 0
                Width = 85
                Height = 215
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 0
                object LaTipoFer: TLabel
                  Left = 0
                  Top = 0
                  Width = 85
                  Height = 25
                  Align = alTop
                  Alignment = taCenter
                  AutoSize = False
                  Caption = 'Travado'
                  Font.Charset = ANSI_CHARSET
                  Font.Color = 8281908
                  Font.Height = -15
                  Font.Name = 'Times New Roman'
                  Font.Style = [fsBold]
                  ParentFont = False
                  Transparent = True
                end
                object Panel25: TPanel
                  Left = 0
                  Top = 25
                  Width = 85
                  Height = 190
                  Align = alClient
                  BevelOuter = bvLowered
                  Enabled = False
                  ParentBackground = False
                  TabOrder = 0
                  object Label109: TLabel
                    Left = 6
                    Top = 4
                    Width = 62
                    Height = 13
                    Caption = 'Dias a gozar:'
                  end
                  object Label113: TLabel
                    Left = 6
                    Top = 44
                    Width = 57
                    Height = 13
                    Caption = 'Dias abono:'
                  end
                  object Label114: TLabel
                    Left = 6
                    Top = 84
                    Width = 63
                    Height = 13
                    Caption = 'In'#237'cio pecun:'
                  end
                  object Label115: TLabel
                    Left = 6
                    Top = 124
                    Width = 52
                    Height = 13
                    Caption = 'Fim pecun:'
                  end
                  object dmkEdDiasGozar: TdmkEdit
                    Left = 6
                    Top = 20
                    Width = 72
                    Height = 21
                    TabStop = False
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                  object dmkEdDiasPecun: TdmkEdit
                    Left = 6
                    Top = 60
                    Width = 72
                    Height = 21
                    TabStop = False
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                  object dmkEdDtIniPecun: TdmkEdit
                    Left = 6
                    Top = 100
                    Width = 72
                    Height = 21
                    TabStop = False
                    TabOrder = 2
                    FormatType = dmktfDate
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0d
                    ValWarn = False
                  end
                  object dmkEdDtFimPecun: TdmkEdit
                    Left = 6
                    Top = 140
                    Width = 72
                    Height = 21
                    TabStop = False
                    TabOrder = 3
                    FormatType = dmktfDate
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0d
                    ValWarn = False
                  end
                end
              end
              object Panel24: TPanel
                Left = 85
                Top = 0
                Width = 829
                Height = 215
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                object Label102: TLabel
                  Left = 8
                  Top = 8
                  Width = 161
                  Height = 13
                  Caption = 'Media de horas extras semanais'#185'*:'
                end
                object Label104: TLabel
                  Left = 8
                  Top = 56
                  Width = 121
                  Height = 13
                  Caption = 'M'#233'dia de comiss'#245'es ($) *:'
                end
                object Label105: TLabel
                  Left = 8
                  Top = 80
                  Width = 119
                  Height = 13
                  Caption = 'M'#233'dia de adicionais ($) *:'
                end
                object Label110: TLabel
                  Left = 8
                  Top = 104
                  Width = 101
                  Height = 13
                  Caption = 'Faltas injustificadas *:'
                end
                object Label103: TLabel
                  Left = 268
                  Top = 8
                  Width = 188
                  Height = 13
                  Caption = 'Data inicial e final do per'#237'odo aquisitivo:'
                end
                object Label106: TLabel
                  Left = 268
                  Top = 32
                  Width = 134
                  Height = 13
                  Caption = 'Data inicial e final das f'#233'rias:'
                end
                object Label107: TLabel
                  Left = 268
                  Top = 80
                  Width = 144
                  Height = 13
                  Caption = 'Data do aviso pr'#233'vio de f'#233'rias:'
                end
                object Label108: TLabel
                  Left = 268
                  Top = 104
                  Width = 116
                  Height = 13
                  Caption = 'Data do recibo de f'#233'rias:'
                end
                object Label111: TLabel
                  Left = 268
                  Top = 56
                  Width = 164
                  Height = 13
                  Caption = 'Data solicita'#231#227'o abono pecuni'#225'rio:'
                end
                object Label112: TLabel
                  Left = 268
                  Top = 128
                  Width = 162
                  Height = 13
                  Caption = 'Data da solicit. da 1'#170' parc. do 13'#186':'
                end
                object Label116: TLabel
                  Left = 268
                  Top = 152
                  Width = 133
                  Height = 13
                  Caption = 'Data do retorno ao trabalho:'
                end
                object Label96: TLabel
                  Left = 8
                  Top = 32
                  Width = 140
                  Height = 13
                  Caption = 'Media de horas extras 100%*:'
                end
                object RGPecun: TRadioGroup
                  Left = 8
                  Top = 124
                  Width = 241
                  Height = 41
                  Caption = ' 1/3 de f'#233'rias em pec'#250'nia?:  '
                  Columns = 2
                  ItemIndex = 0
                  Items.Strings = (
                    'N'#227'o'
                    'Sim')
                  TabOrder = 5
                  OnClick = RGPecunClick
                end
                object dmkEdDiasFaltI: TdmkEdit
                  Left = 176
                  Top = 100
                  Width = 72
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 4
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = dmkEdDiasFaltIChange
                end
                object dmkEdMediaAdMan: TdmkEdit
                  Left = 176
                  Top = 76
                  Width = 72
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object dmkEdMediaCoMan: TdmkEdit
                  Left = 176
                  Top = 52
                  Width = 72
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object dmkEdMeHEMan1xx: TdmkEdit
                  Left = 176
                  Top = 4
                  Width = 72
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object dmkEdDtIniPA: TdmkEdit
                  Left = 468
                  Top = 4
                  Width = 72
                  Height = 21
                  TabOrder = 6
                  FormatType = dmktfDate
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0d
                  ValWarn = False
                  OnChange = dmkEdDtIniPAChange
                end
                object dmkEdDtFimPA: TdmkEdit
                  Left = 544
                  Top = 4
                  Width = 72
                  Height = 21
                  TabOrder = 7
                  FormatType = dmktfDate
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0d
                  ValWarn = False
                  OnChange = dmkEdDtFimPAChange
                end
                object dmkEdDtFimPG: TdmkEdit
                  Left = 544
                  Top = 28
                  Width = 72
                  Height = 21
                  TabStop = False
                  ReadOnly = True
                  TabOrder = 9
                  FormatType = dmktfDate
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0d
                  ValWarn = False
                end
                object dmkEdDtIniPG: TdmkEdit
                  Left = 468
                  Top = 28
                  Width = 72
                  Height = 21
                  TabOrder = 8
                  FormatType = dmktfDate
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0d
                  ValWarn = False
                  OnExit = dmkEdDtIniPGExit
                end
                object dmkEdDtSolPrP13: TdmkEdit
                  Left = 468
                  Top = 124
                  Width = 72
                  Height = 21
                  TabOrder = 13
                  FormatType = dmktfDate
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0d
                  ValWarn = False
                end
                object dmkEdDtSolPecun: TdmkEdit
                  Left = 468
                  Top = 52
                  Width = 72
                  Height = 21
                  TabOrder = 10
                  FormatType = dmktfDate
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0d
                  ValWarn = False
                end
                object dmkEdDtRecPgto: TdmkEdit
                  Left = 468
                  Top = 100
                  Width = 72
                  Height = 21
                  TabOrder = 12
                  FormatType = dmktfDate
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0d
                  ValWarn = False
                end
                object dmkEdDtAvisoFer: TdmkEdit
                  Left = 468
                  Top = 76
                  Width = 72
                  Height = 21
                  TabOrder = 11
                  FormatType = dmktfDate
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0d
                  ValWarn = False
                end
                object dmkEdDtRetorno: TdmkEdit
                  Left = 468
                  Top = 148
                  Width = 72
                  Height = 21
                  TabStop = False
                  ReadOnly = True
                  TabOrder = 14
                  FormatType = dmktfDate
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0d
                  ValWarn = False
                end
                object StaticText3: TStaticText
                  Left = 8
                  Top = 172
                  Width = 617
                  Height = 33
                  AutoSize = False
                  BorderStyle = sbsSunken
                  Caption = 
                    '*: Informe somente dados do per'#237'odo aquisitivo que n'#227'o constam e' +
                    'm c'#225'lculos neste aplicativo, pois os dados ser'#227'o somados aos c'#225'l' +
                    'culos existentes.             '#185' = % do cadastro.'#13#10
                  TabOrder = 15
                end
                object dmkEdMeHEMan200: TdmkEdit
                  Left = 176
                  Top = 28
                  Width = 72
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
            object DBGFerias: TDBGrid
              Left = 1
              Top = 1
              Width = 914
              Height = 57
              Align = alClient
              DataSource = DsFPFunciFer
              TabOrder = 2
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Calculo'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  Title.Caption = 'C'#225'lculo'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtIniPA'
                  Title.Caption = 'In'#237'cio PA'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtFimPA'
                  Title.Caption = 'Fim PA'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DTSOLPRP13_TXT'
                  Title.Caption = 'Solic. 13'#186
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtAvisoFer'
                  Title.Caption = 'Aviso'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PECUNIO_TXT'
                  Title.Caption = 'Pec'#250'nio'
                  Width = 18
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DTSOLPECUN_TXT'
                  Title.Caption = 'Sol. pecun.'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DiasFaltI'
                  Title.Caption = 'DFI'
                  Width = 24
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DiasFaltP'
                  Title.Caption = 'DDF'
                  Width = 24
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MeHEMan1xx'
                  Title.Caption = 'M'#233'd. HE XX'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MeHEMan200'
                  Title.Caption = 'M'#233'd.HE 100'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MediaCoMan'
                  Title.Caption = 'M'#233'd. Comis.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MediaAdMan'
                  Title.Caption = 'M'#233'd. adicio.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DiasGozar'
                  Title.Caption = 'DFG'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtRecPgto'
                  Title.Caption = 'Rec. pgto'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtIniPG'
                  Title.Caption = 'Inicio gozo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DiasPecun'
                  Title.Caption = 'DFP'
                  Width = 24
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DTINIPECUN_TXT'
                  Title.Caption = 'In'#237'cio pecun.'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DTFIMPECUN_TXT'
                  Title.Caption = 'Fim pecun'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtFimFeria'
                  Title.Caption = 'Fim f'#233'rias'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DtRetorno'
                  Title.Caption = 'Retorno'
                  Width = 56
                  Visible = True
                end>
            end
            object Panel23: TPanel
              Left = 1
              Top = 273
              Width = 914
              Height = 48
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 1
              object PnFerConf: TPanel
                Left = 530
                Top = 0
                Width = 384
                Height = 48
                Align = alRight
                ParentBackground = False
                TabOrder = 0
                Visible = False
                object BitBtn3: TBitBtn
                  Tag = 14
                  Left = 188
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Confirma'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BitBtn3Click
                end
                object BitBtn4: TBitBtn
                  Tag = 15
                  Left = 280
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Desiste'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                  OnClick = BitBtn4Click
                end
              end
              object PnFerEdit: TPanel
                Left = 0
                Top = 0
                Width = 530
                Height = 48
                Align = alClient
                ParentBackground = False
                TabOrder = 1
                object BtExcluiFer: TBitBtn
                  Tag = 12
                  Left = 188
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Exclui'
                  Enabled = False
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtExcluiFerClick
                end
                object BtAlteraFer: TBitBtn
                  Tag = 11
                  Left = 96
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Altera'
                  Enabled = False
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                  OnClick = BtAlteraFerClick
                end
                object BitBtn9: TBitBtn
                  Tag = 10
                  Left = 4
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Inclui'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                  OnClick = BitBtn9Click
                end
              end
            end
          end
        end
        object TabSheet15: TTabSheet
          Caption = 'Eventos'
          ImageIndex = 7
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel15: TPanel
            Left = 0
            Top = 274
            Width = 916
            Height = 48
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object PnEveConf: TPanel
              Left = 532
              Top = 0
              Width = 384
              Height = 48
              Align = alRight
              TabOrder = 0
              Visible = False
              object BitBtn6: TBitBtn
                Tag = 14
                Left = 188
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Altera banco atual'
                Caption = '&Confirma'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BitBtn6Click
              end
              object BitBtn7: TBitBtn
                Tag = 15
                Left = 280
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Exclui banco atual'
                Caption = '&Desiste'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BitBtn7Click
              end
            end
            object PnEveEdit: TPanel
              Left = 0
              Top = 0
              Width = 532
              Height = 48
              Align = alClient
              TabOrder = 1
              object Label97: TLabel
                Left = 292
                Top = 5
                Width = 251
                Height = 13
                Caption = 'PAI* : Per'#237'odo de ac'#250'mulo da incid'#234'ncia (em meses).'
              end
              object Label98: TLabel
                Left = 292
                Top = 18
                Width = 310
                Height = 13
                Caption = 
                  'IMR*: Incremento de meses de refer'#234'ncia da conta ( Ex:. 0 ou -1)' +
                  '.'
              end
              object Label100: TLabel
                Left = 292
                Top = 31
                Width = 235
                Height = 13
                Caption = 'PVI* : Porcentagem (%) ou valor ($) de incid'#234'ncia.'
              end
              object BtExcluiIts: TBitBtn
                Tag = 12
                Left = 188
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Exclui banco atual'
                Caption = '&Exclui'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtExcluiItsClick
              end
              object BtAlteraIts: TBitBtn
                Tag = 11
                Left = 96
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Altera banco atual'
                Caption = '&Altera'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtAlteraItsClick
              end
              object BtIncluiIts: TBitBtn
                Tag = 10
                Left = 4
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Inclui novo banco'
                Caption = '&Inclui'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BtIncluiItsClick
              end
            end
          end
          object PnEventos: TPanel
            Left = 0
            Top = 41
            Width = 916
            Height = 233
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            Visible = False
            object Panel17: TPanel
              Left = 0
              Top = 0
              Width = 72
              Height = 233
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              ExplicitLeft = 1
              ExplicitTop = 1
              ExplicitHeight = 231
              object LaTipoEve: TLabel
                Left = 0
                Top = 0
                Width = 72
                Height = 49
                Align = alTop
                Alignment = taCenter
                AutoSize = False
                Caption = 'Travado'
                Font.Charset = ANSI_CHARSET
                Font.Color = 8281908
                Font.Height = -15
                Font.Name = 'Times New Roman'
                Font.Style = [fsBold]
                ParentFont = False
                Transparent = True
              end
              object RGIncidencia: TRadioGroup
                Left = 0
                Top = 49
                Width = 72
                Height = 84
                Align = alTop
                Caption = ' Incid'#234'ncia: '
                ItemIndex = 0
                Items.Strings = (
                  'Evento'
                  'Conta')
                TabOrder = 0
              end
            end
            object Panel18: TPanel
              Left = 72
              Top = 0
              Width = 844
              Height = 233
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object Panel16: TPanel
                Left = 0
                Top = 0
                Width = 321
                Height = 233
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 0
                object Panel19: TPanel
                  Left = 0
                  Top = 45
                  Width = 321
                  Height = 188
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  object GroupBox8: TGroupBox
                    Left = 153
                    Top = 0
                    Width = 168
                    Height = 188
                    Align = alRight
                    Caption = ' Incid'#234'ncia sobre o evento: '
                    TabOrder = 0
                    ExplicitHeight = 186
                    object RGEve_Incid: TRadioGroup
                      Left = 2
                      Top = 15
                      Width = 164
                      Height = 58
                      Align = alTop
                      Caption = ' Forma de incid'#234'ncia: '
                      ItemIndex = 0
                      Items.Strings = (
                        'Valor direto ($)'
                        'Refer'#234'ncia (%)')
                      TabOrder = 0
                    end
                    object TPanel
                      Left = 2
                      Top = 73
                      Width = 164
                      Height = 113
                      Align = alClient
                      BevelOuter = bvNone
                      TabOrder = 1
                      ExplicitHeight = 111
                      object Panel31: TPanel
                        Left = 0
                        Top = 0
                        Width = 164
                        Height = 45
                        Align = alTop
                        BevelOuter = bvNone
                        TabOrder = 0
                        object Label93: TLabel
                          Left = 4
                          Top = 4
                          Width = 46
                          Height = 13
                          Caption = 'Valor / %:'
                        end
                        object Label137: TLabel
                          Left = 89
                          Top = 4
                          Width = 43
                          Height = 13
                          Caption = 'Unidade:'
                        end
                        object dmkEdEve_ValPer: TdmkEdit
                          Left = 4
                          Top = 20
                          Width = 80
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfDouble
                          MskType = fmtNone
                          DecimalSize = 2
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0,00'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0.000000000000000000
                          ValWarn = False
                        end
                        object EdUnidade: TEdit
                          Left = 88
                          Top = 20
                          Width = 72
                          Height = 21
                          MaxLength = 15
                          TabOrder = 1
                        end
                      end
                      object GroupBox13: TGroupBox
                        Left = 0
                        Top = 45
                        Width = 164
                        Height = 68
                        Align = alClient
                        Caption = ' PAI*: '
                        TabOrder = 1
                        ExplicitHeight = 66
                        object Label95: TLabel
                          Left = 8
                          Top = 15
                          Width = 34
                          Height = 13
                          Caption = 'Meses:'
                        end
                        object dmkEdEve_Meses: TdmkEdit
                          Left = 8
                          Top = 31
                          Width = 37
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                        end
                        object CkEve_Ciclo: TCheckBox
                          Left = 56
                          Top = 12
                          Width = 97
                          Height = 17
                          Caption = 'Desde a data:'
                          TabOrder = 1
                        end
                        object dmkEdEve_DtCicl: TdmkEdit
                          Left = 56
                          Top = 32
                          Width = 80
                          Height = 21
                          TabOrder = 2
                          FormatType = dmktfDate
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0d
                          ValWarn = False
                        end
                      end
                    end
                  end
                  object Panel28: TPanel
                    Left = 0
                    Top = 0
                    Width = 153
                    Height = 188
                    Align = alClient
                    BevelOuter = bvNone
                    Caption = 'Panel28'
                    TabOrder = 1
                    object StaticText4: TStaticText
                      Left = 0
                      Top = 0
                      Width = 153
                      Height = 17
                      Align = alTop
                      Alignment = taCenter
                      Caption = ' Tipos de c'#225'lculo:'
                      TabOrder = 0
                      ExplicitWidth = 88
                    end
                    object CLTipoCalc: TCheckListBox
                      Left = 0
                      Top = 17
                      Width = 153
                      Height = 171
                      Align = alClient
                      ItemHeight = 13
                      Items.Strings = (
                        'Adiant. de Sal'#225'rio'
                        'Adiant. de 13'#186' Sal'#225'rio'
                        'Mensal'
                        'Semanal'
                        'F'#233'rias'
                        'Afastamento'
                        'Rescis'#227'o'
                        'Pr'#243'-Labore'
                        '13'#186' Sal'#225'rio')
                      TabOrder = 1
                    end
                  end
                end
                object Panel26: TPanel
                  Left = 0
                  Top = 0
                  Width = 321
                  Height = 45
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label63: TLabel
                    Left = 4
                    Top = 4
                    Width = 37
                    Height = 13
                    Caption = 'Evento:'
                  end
                  object EdEvento: TdmkEditCB
                    Left = 4
                    Top = 20
                    Width = 41
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBEvento
                    IgnoraDBLookupComboBox = False
                  end
                  object CBEvento: TdmkDBLookupComboBox
                    Left = 48
                    Top = 20
                    Width = 265
                    Height = 21
                    Color = clWhite
                    KeyField = 'Codigo'
                    ListField = 'Descricao'
                    ListSource = DsEventos
                    TabOrder = 1
                    dmkEditCB = EdEvento
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                end
              end
              object Panel20: TPanel
                Left = 321
                Top = 0
                Width = 523
                Height = 233
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                ExplicitWidth = 521
                ExplicitHeight = 231
                object Label66: TLabel
                  Left = 4
                  Top = 191
                  Width = 310
                  Height = 13
                  Caption = 
                    'IMR*: Incremento de meses de refer'#234'ncia da conta ( Ex:. 0 ou -1)' +
                    '.'
                end
                object Label99: TLabel
                  Left = 5
                  Top = 208
                  Width = 364
                  Height = 13
                  Caption = 
                    'PAI** : Per'#237'odo de ac'#250'mulo da incid'#234'ncia (em meses) (zero para c' +
                    #225'lculo fixo).'
                end
                object GroupBox7: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 523
                  Height = 104
                  Align = alTop
                  Caption = ' Incid'#234'ncia sobre conta corrente: '
                  TabOrder = 0
                  Visible = False
                  ExplicitWidth = 521
                  object Label62: TLabel
                    Left = 8
                    Top = 56
                    Width = 54
                    Height = 13
                    Caption = 'Percentual:'
                  end
                  object Label65: TLabel
                    Left = 68
                    Top = 56
                    Width = 27
                    Height = 13
                    Caption = 'IMR*:'
                  end
                  object Label64: TLabel
                    Left = 8
                    Top = 16
                    Width = 116
                    Height = 13
                    Caption = 'Conta (plano de contas):'
                  end
                  object dmkEdCta_IncPer: TdmkEdit
                    Left = 8
                    Top = 72
                    Width = 57
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 4
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,0000'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdCta_MesesR: TdmkEdit
                    Left = 68
                    Top = 72
                    Width = 33
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 3
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-3'
                    ValMax = '0'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                  object EdConta: TdmkEditCB
                    Left = 8
                    Top = 32
                    Width = 41
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBConta
                    IgnoraDBLookupComboBox = False
                  end
                  object CBConta: TdmkDBLookupComboBox
                    Left = 52
                    Top = 32
                    Width = 321
                    Height = 21
                    Color = clWhite
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsContas
                    TabOrder = 1
                    dmkEditCB = EdConta
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                end
                object CkDesBasIRRF: TCheckBox
                  Left = 4
                  Top = 108
                  Width = 97
                  Height = 17
                  Caption = 'CkDesBasIRRF'
                  TabOrder = 1
                  Visible = False
                end
                object CkProporciHT: TCheckBox
                  Left = 4
                  Top = 164
                  Width = 373
                  Height = 17
                  Caption = 'Proporcional '#224's horas trabalhadas.'
                  TabOrder = 2
                end
              end
            end
          end
          object PnGrades: TPanel
            Left = 0
            Top = 0
            Width = 916
            Height = 41
            Align = alTop
            TabOrder = 2
            object DBGEventos: TDBGrid
              Left = 1
              Top = 1
              Width = 751
              Height = 39
              Align = alClient
              DataSource = DsFPFunciEve
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDrawColumnCell = DBGEventosDrawColumnCell
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NOME_INCIDENCIA'
                  Title.Caption = 'Incid'#234'ncia'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Evento'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEEVENTO'
                  Title.Caption = 'Descri'#231#227'o do evento'
                  Width = 104
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_EVE_INCID'
                  Title.Caption = 'Forma de incid.'
                  Width = 78
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Eve_ValPer'
                  Title.Caption = 'PVI*'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Eve_Meses'
                  Title.Caption = 'PAI**'
                  Width = 33
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECONTA'
                  Title.Caption = 'Conta de incid'#234'ncia'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Cta_IncPer'
                  Title.Caption = 'PVI*'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Cta_MesesR'
                  Title.Caption = 'IMR*'
                  Width = 30
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ProporciHT'
                  Title.Caption = 'pht'
                  Width = 17
                  Visible = True
                end>
            end
            object DBGrid1: TDBGrid
              Left = 752
              Top = 1
              Width = 163
              Height = 39
              Align = alRight
              DataSource = DsFPFunciCal
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NOMETIPOCALC'
                  Title.Caption = 'Tipo de c'#225'lculo'
                  Width = 129
                  Visible = True
                end>
            end
          end
        end
        object TabSheet10: TTabSheet
          Caption = 'Hor'#225'rios'
          ImageIndex = 8
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel30: TPanel
            Left = 0
            Top = 0
            Width = 916
            Height = 322
            Align = alClient
            ParentBackground = False
            TabOrder = 0
            object Panel32: TPanel
              Left = 1
              Top = 1
              Width = 914
              Height = 48
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object Panel33: TPanel
                Left = 0
                Top = 0
                Width = 914
                Height = 48
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 0
                object PnHorCtrl: TPanel
                  Left = 0
                  Top = 0
                  Width = 592
                  Height = 48
                  Align = alClient
                  TabOrder = 1
                  object BtExcluiHor: TBitBtn
                    Tag = 12
                    Left = 184
                    Top = 4
                    Width = 90
                    Height = 40
                    Cursor = crHandPoint
                    Hint = 'Exclui banco atual'
                    Caption = '&Exclui'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtExcluiHorClick
                  end
                  object BtAlteraHor: TBitBtn
                    Tag = 11
                    Left = 92
                    Top = 4
                    Width = 90
                    Height = 40
                    Cursor = crHandPoint
                    Hint = 'Altera banco atual'
                    Caption = '&Altera'
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 1
                    OnClick = BtAlteraHorClick
                  end
                  object BitBtn12: TBitBtn
                    Tag = 10
                    Left = 0
                    Top = 4
                    Width = 90
                    Height = 40
                    Cursor = crHandPoint
                    Hint = 'Inclui novo banco'
                    Caption = '&Inclui'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 2
                  end
                end
                object PnHorConf: TPanel
                  Left = 592
                  Top = 0
                  Width = 322
                  Height = 48
                  Align = alRight
                  TabOrder = 0
                  Visible = False
                  object Panel999: TPanel
                    Left = 1
                    Top = 1
                    Width = 192
                    Height = 46
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 0
                    object BitBtn5: TBitBtn
                      Tag = 14
                      Left = 4
                      Top = 4
                      Width = 90
                      Height = 40
                      Cursor = crHandPoint
                      Hint = 'Altera banco atual'
                      Caption = '&Confirma'
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 0
                      OnClick = BitBtn5Click
                    end
                    object BitBtn8: TBitBtn
                      Tag = 15
                      Left = 96
                      Top = 4
                      Width = 90
                      Height = 40
                      Cursor = crHandPoint
                      Hint = 'Exclui banco atual'
                      Caption = '&Desiste'
                      NumGlyphs = 2
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 1
                      OnClick = BitBtn8Click
                    end
                  end
                end
              end
            end
            object DBGHor: TDBGrid
              Left = 1
              Top = 49
              Width = 914
              Height = 152
              Align = alClient
              DataSource = DsFPFunciHor
              TabOrder = 2
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'Tur1Ent'
                  Title.Alignment = taCenter
                  Title.Caption = 'Entrada'
                  Width = 52
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'Tur1DeI'
                  Title.Alignment = taCenter
                  Title.Caption = 'Ini.interv.'
                  Width = 52
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'Tur1DeF'
                  Title.Alignment = taCenter
                  Title.Caption = 'Fim interv.'
                  Width = 52
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'Tur1Sai'
                  Title.Alignment = taCenter
                  Title.Caption = 'Sa'#237'da'
                  Width = 52
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Color = clInactiveBorder
                  Expanded = False
                  Title.Alignment = taCenter
                  Width = 3
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'TXT1HrT'
                  Title.Alignment = taCenter
                  Title.Caption = 'H. trab.dia'
                  Width = 72
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'TXT1HrN'
                  Title.Alignment = taCenter
                  Title.Caption = 'H. trab.noite'
                  Width = 72
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'TXT1HrS'
                  Title.Alignment = taCenter
                  Title.Caption = 'Total h. trab.'
                  Width = 72
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'TXT1HrA'
                  Title.Alignment = taCenter
                  Title.Caption = 'H.alimenta'#231#227'o'
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'TXT1HrB'
                  Title.Alignment = taCenter
                  Title.Caption = 'H.banco horas'
                  Width = 72
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'TXT1HrE'
                  Title.Alignment = taCenter
                  Title.Caption = 'Horas extras'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TXT1HrC'
                  Title.Caption = 'Horas normais'
                  Width = 72
                  Visible = True
                end>
            end
            object PnHorEdit: TPanel
              Left = 1
              Top = 201
              Width = 914
              Height = 120
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              Visible = False
              object Panel34: TPanel
                Left = 0
                Top = 60
                Width = 914
                Height = 58
                Align = alTop
                BevelOuter = bvLowered
                TabOrder = 0
                object GroupBox11: TGroupBox
                  Left = 1
                  Top = 1
                  Width = 252
                  Height = 56
                  Align = alLeft
                  Caption = ' Turno 2: '
                  TabOrder = 0
                  object Label129: TLabel
                    Left = 8
                    Top = 16
                    Width = 55
                    Height = 13
                    Caption = 'Hora inicial:'
                  end
                  object Label130: TLabel
                    Left = 68
                    Top = 16
                    Width = 55
                    Height = 13
                    Caption = 'Desc.inicio:'
                  end
                  object Label131: TLabel
                    Left = 128
                    Top = 16
                    Width = 44
                    Height = 13
                    Caption = 'Desc.fim:'
                  end
                  object Label132: TLabel
                    Left = 188
                    Top = 16
                    Width = 48
                    Height = 13
                    Caption = 'Hora final:'
                  end
                  object dmkEdTur2Ent: TdmkEdit
                    Left = 8
                    Top = 32
                    Width = 57
                    Height = 21
                    TabOrder = 0
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdTur2DeI: TdmkEdit
                    Left = 68
                    Top = 32
                    Width = 57
                    Height = 21
                    TabOrder = 1
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdTur2DeF: TdmkEdit
                    Left = 128
                    Top = 32
                    Width = 57
                    Height = 21
                    TabOrder = 2
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdTur2Sai: TdmkEdit
                    Left = 188
                    Top = 32
                    Width = 57
                    Height = 21
                    TabOrder = 3
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                end
                object Panel35: TPanel
                  Left = 253
                  Top = 1
                  Width = 660
                  Height = 56
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label146: TLabel
                    Left = 312
                    Top = 4
                    Width = 45
                    Height = 13
                    Caption = 'H. extras:'
                  end
                  object Label151: TLabel
                    Left = 236
                    Top = 4
                    Width = 59
                    Height = 13
                    Caption = 'H. banco h.:'
                  end
                  object LaTur2HrT: TLabel
                    Left = 8
                    Top = 43
                    Width = 64
                    Height = 13
                    Alignment = taCenter
                    AutoSize = False
                    Caption = '00:00:00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                  object LaTur2HrE: TLabel
                    Left = 312
                    Top = 43
                    Width = 64
                    Height = 13
                    Alignment = taCenter
                    AutoSize = False
                    Caption = '00:00:00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    Visible = False
                  end
                  object LaTur2HrN: TLabel
                    Left = 84
                    Top = 43
                    Width = 64
                    Height = 13
                    Alignment = taCenter
                    AutoSize = False
                    Caption = '00:00:00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                  object LaTur2HrB: TLabel
                    Left = 236
                    Top = 43
                    Width = 64
                    Height = 13
                    Alignment = taCenter
                    AutoSize = False
                    Caption = '00:00:00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    Visible = False
                  end
                  object Label153: TLabel
                    Left = 74
                    Top = 24
                    Width = 6
                    Height = 13
                    Caption = '+'
                  end
                  object Label155: TLabel
                    Left = 150
                    Top = 24
                    Width = 6
                    Height = 13
                    Caption = '='
                  end
                  object LaTur2HrS: TLabel
                    Left = 160
                    Top = 44
                    Width = 64
                    Height = 13
                    Alignment = taCenter
                    AutoSize = False
                    Caption = '00:00:00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                  object Label145: TLabel
                    Left = 8
                    Top = 4
                    Width = 49
                    Height = 13
                    Caption = 'H.trab.dia:'
                  end
                  object Label150: TLabel
                    Left = 84
                    Top = 4
                    Width = 58
                    Height = 13
                    Caption = 'H.trab.noite:'
                  end
                  object Label157: TLabel
                    Left = 160
                    Top = 4
                    Width = 60
                    Height = 13
                    Caption = 'Total h.trab.:'
                  end
                  object Label160: TLabel
                    Left = 388
                    Top = 4
                    Width = 53
                    Height = 13
                    Caption = 'H. normais:'
                  end
                  object LaTur2HrC: TLabel
                    Left = 388
                    Top = 44
                    Width = 64
                    Height = 13
                    Alignment = taCenter
                    AutoSize = False
                    Caption = '00:00:00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    Visible = False
                  end
                  object Label161: TLabel
                    Left = 226
                    Top = 24
                    Width = 6
                    Height = 13
                    Caption = '='
                  end
                  object Label164: TLabel
                    Left = 302
                    Top = 24
                    Width = 6
                    Height = 13
                    Caption = '+'
                  end
                  object Label165: TLabel
                    Left = 378
                    Top = 24
                    Width = 6
                    Height = 13
                    Caption = '+'
                  end
                  object dmkEdTur2HrT: TdmkEdit
                    Left = 8
                    Top = 20
                    Width = 64
                    Height = 21
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 0
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfLong
                    Texto = '00:00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdTur2HrE: TdmkEdit
                    Left = 312
                    Top = 20
                    Width = 64
                    Height = 21
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 4
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfLong
                    Texto = '00:00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdTur2HrN: TdmkEdit
                    Left = 84
                    Top = 20
                    Width = 64
                    Height = 21
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 1
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfLong
                    Texto = '00:00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdTur2HrB: TdmkEdit
                    Left = 236
                    Top = 20
                    Width = 64
                    Height = 21
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 3
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfLong
                    Texto = '00:00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdTur2HrS: TdmkEdit
                    Left = 160
                    Top = 20
                    Width = 64
                    Height = 21
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 2
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfLong
                    Texto = '00:00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object dmkEdTur2HrC: TdmkEdit
                    Left = 388
                    Top = 20
                    Width = 64
                    Height = 21
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 5
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfLong
                    Texto = '00:00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object Panel39: TPanel
                    Left = 580
                    Top = 0
                    Width = 80
                    Height = 56
                    Align = alRight
                    BevelOuter = bvNone
                    TabOrder = 6
                    object Label147: TLabel
                      Left = 8
                      Top = 4
                      Width = 53
                      Height = 13
                      Caption = 'H. aliment.:'
                    end
                    object LaTur2HrA: TLabel
                      Left = 8
                      Top = 43
                      Width = 64
                      Height = 13
                      Alignment = taCenter
                      AutoSize = False
                      Caption = '00:00:00'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clRed
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object dmkEdTur2HrA: TdmkEdit
                      Left = 8
                      Top = 20
                      Width = 64
                      Height = 21
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                      TabOrder = 0
                      FormatType = dmktfTime
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfLong
                      Texto = '00:00:00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                  end
                end
              end
              object Panel36: TPanel
                Left = 0
                Top = 0
                Width = 914
                Height = 60
                Align = alTop
                BevelOuter = bvLowered
                TabOrder = 1
                object GroupBox12: TGroupBox
                  Left = 1
                  Top = 1
                  Width = 252
                  Height = 58
                  Align = alLeft
                  Caption = ' Turno 1: '
                  TabOrder = 0
                  object Label133: TLabel
                    Left = 8
                    Top = 16
                    Width = 55
                    Height = 13
                    Caption = 'Hora inicial:'
                  end
                  object Label134: TLabel
                    Left = 68
                    Top = 16
                    Width = 55
                    Height = 13
                    Caption = 'Desc.inicio:'
                  end
                  object Label135: TLabel
                    Left = 128
                    Top = 16
                    Width = 44
                    Height = 13
                    Caption = 'Desc.fim:'
                  end
                  object Label136: TLabel
                    Left = 188
                    Top = 16
                    Width = 48
                    Height = 13
                    Caption = 'Hora final:'
                  end
                  object dmkEdTur1Ent: TdmkEdit
                    Left = 8
                    Top = 32
                    Width = 57
                    Height = 21
                    TabOrder = 0
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnExit = dmkEdTur1EntExit
                  end
                  object dmkEdTur1DeI: TdmkEdit
                    Left = 68
                    Top = 32
                    Width = 57
                    Height = 21
                    TabOrder = 1
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnExit = dmkEdTur1DeIExit
                  end
                  object dmkEdTur1DeF: TdmkEdit
                    Left = 128
                    Top = 32
                    Width = 57
                    Height = 21
                    TabOrder = 2
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnExit = dmkEdTur1DeFExit
                  end
                  object dmkEdTur1Sai: TdmkEdit
                    Left = 188
                    Top = 32
                    Width = 57
                    Height = 21
                    TabOrder = 3
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnExit = dmkEdTur1SaiExit
                  end
                end
                object Panel37: TPanel
                  Left = 253
                  Top = 1
                  Width = 660
                  Height = 58
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label142: TLabel
                    Left = 8
                    Top = 4
                    Width = 49
                    Height = 13
                    Caption = 'H.trab.dia:'
                  end
                  object Label143: TLabel
                    Left = 312
                    Top = 4
                    Width = 45
                    Height = 13
                    Caption = 'H. extras:'
                  end
                  object Label148: TLabel
                    Left = 84
                    Top = 4
                    Width = 58
                    Height = 13
                    Caption = 'H.trab.noite:'
                  end
                  object Label149: TLabel
                    Left = 236
                    Top = 4
                    Width = 59
                    Height = 13
                    Caption = 'H. banco h.:'
                  end
                  object LaTur1HrT: TLabel
                    Left = 8
                    Top = 44
                    Width = 64
                    Height = 13
                    Alignment = taCenter
                    AutoSize = False
                    Caption = '00:00:00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                  object LaTur1HrE: TLabel
                    Left = 312
                    Top = 44
                    Width = 64
                    Height = 13
                    Alignment = taCenter
                    AutoSize = False
                    Caption = '00:00:00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    Visible = False
                  end
                  object LaTur1HrN: TLabel
                    Left = 84
                    Top = 44
                    Width = 64
                    Height = 13
                    Alignment = taCenter
                    AutoSize = False
                    Caption = '00:00:00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                  object LaTur1HrB: TLabel
                    Left = 236
                    Top = 44
                    Width = 64
                    Height = 13
                    Alignment = taCenter
                    AutoSize = False
                    Caption = '00:00:00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    Visible = False
                  end
                  object Label152: TLabel
                    Left = 74
                    Top = 24
                    Width = 6
                    Height = 13
                    Caption = '+'
                  end
                  object Label154: TLabel
                    Left = 150
                    Top = 24
                    Width = 6
                    Height = 13
                    Caption = '='
                  end
                  object Label156: TLabel
                    Left = 160
                    Top = 4
                    Width = 60
                    Height = 13
                    Caption = 'Total h.trab.:'
                  end
                  object LaTur1HrS: TLabel
                    Left = 160
                    Top = 44
                    Width = 64
                    Height = 13
                    Alignment = taCenter
                    AutoSize = False
                    Caption = '00:00:00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                  end
                  object Label158: TLabel
                    Left = 388
                    Top = 4
                    Width = 53
                    Height = 13
                    Caption = 'H. normais:'
                  end
                  object LaTur1HrC: TLabel
                    Left = 388
                    Top = 44
                    Width = 64
                    Height = 13
                    Alignment = taCenter
                    AutoSize = False
                    Caption = '00:00:00'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    Visible = False
                  end
                  object Label159: TLabel
                    Left = 226
                    Top = 24
                    Width = 6
                    Height = 13
                    Caption = '='
                  end
                  object Label162: TLabel
                    Left = 302
                    Top = 24
                    Width = 6
                    Height = 13
                    Caption = '+'
                  end
                  object Label163: TLabel
                    Left = 378
                    Top = 24
                    Width = 6
                    Height = 13
                    Caption = '+'
                  end
                  object dmkEdTur1HrT: TdmkEdit
                    Left = 8
                    Top = 20
                    Width = 64
                    Height = 21
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 0
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfLong
                    Texto = '00:00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnExit = dmkEdTur1HrTExit
                  end
                  object dmkEdTur1HrE: TdmkEdit
                    Left = 312
                    Top = 20
                    Width = 64
                    Height = 21
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 4
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfLong
                    Texto = '00:00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnExit = dmkEdTur1HrEExit
                  end
                  object dmkEdTur1HrN: TdmkEdit
                    Left = 84
                    Top = 20
                    Width = 64
                    Height = 21
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 1
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfLong
                    Texto = '00:00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnExit = dmkEdTur1HrNExit
                  end
                  object dmkEdTur1HrB: TdmkEdit
                    Left = 236
                    Top = 20
                    Width = 64
                    Height = 21
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 3
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfLong
                    Texto = '00:00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnExit = dmkEdTur1HrBExit
                  end
                  object dmkEdTur1HrS: TdmkEdit
                    Left = 160
                    Top = 20
                    Width = 64
                    Height = 21
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 2
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfLong
                    Texto = '00:00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnExit = dmkEdTur1HrSExit
                  end
                  object dmkEdTur1HrC: TdmkEdit
                    Left = 388
                    Top = 20
                    Width = 64
                    Height = 21
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 5
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfLong
                    Texto = '00:00:00'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                    OnExit = dmkEdTur1HrCExit
                  end
                  object Panel38: TPanel
                    Left = 580
                    Top = 0
                    Width = 80
                    Height = 58
                    Align = alRight
                    BevelOuter = bvNone
                    TabOrder = 6
                    object Label144: TLabel
                      Left = 8
                      Top = 4
                      Width = 53
                      Height = 13
                      Caption = 'H. aliment.:'
                    end
                    object LaTur1HrA: TLabel
                      Left = 8
                      Top = 44
                      Width = 64
                      Height = 13
                      Alignment = taCenter
                      AutoSize = False
                      Caption = '00:00:00'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clRed
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object dmkEdTur1HrA: TdmkEdit
                      Left = 8
                      Top = 20
                      Width = 64
                      Height = 21
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -11
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                      TabOrder = 0
                      FormatType = dmktfTime
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfLong
                      Texto = '00:00:00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                      OnExit = dmkEdTur1HrAExit
                    end
                  end
                end
              end
            end
          end
        end
        object TabSheet17: TTabSheet
          Caption = ' Treinamentos '
          ImageIndex = 9
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 916
            Height = 322
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object DBGrid2: TDBGrid
              Left = 0
              Top = 48
              Width = 916
              Height = 274
              Align = alClient
              DataSource = DsFPFunciTre
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnCellClick = DBGrid2CellClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 300
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CargaHoras'
                  Title.Caption = 'Horas'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataIni'
                  Title.Caption = 'In'#237'cio'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataFim'
                  Title.Caption = 'Final'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PathCertif'
                  Title.Caption = 'Arquivo do certificado'
                  Width = 600
                  Visible = True
                end>
            end
            object Panel29: TPanel
              Left = 0
              Top = 0
              Width = 916
              Height = 48
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object Label168: TLabel
                Left = 285
                Top = 18
                Width = 457
                Height = 16
                Caption = 
                  'Clique no "Arquivo do certificado" desejado para abrir em seu pr' +
                  'ograma padr'#227'o.'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clSilver
                Font.Height = -13
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object Label169: TLabel
                Left = 284
                Top = 17
                Width = 457
                Height = 16
                Caption = 
                  'Clique no "Arquivo do certificado" desejado para abrir em seu pr' +
                  'ograma padr'#227'o.'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clPurple
                Font.Height = -13
                Font.Name = 'Arial'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object BtFPFunciTreIns: TBitBtn
                Tag = 10
                Left = 0
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Inclui novo banco'
                Caption = '&Inclui'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtFPFunciTreInsClick
              end
              object BtFPFunciTreUpd: TBitBtn
                Tag = 11
                Left = 92
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Altera banco atual'
                Caption = '&Altera'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtFPFunciTreUpdClick
              end
              object BtFPFunciTreDel: TBitBtn
                Tag = 12
                Left = 184
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Exclui banco atual'
                Caption = '&Exclui'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BtFPFunciTreDelClick
              end
            end
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 482
      Width = 924
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 401
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 412
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          Visible = False
          OnClick = BtExcluiClick
        end
      end
    end
  end
  object PnInclui: TPanel
    Left = 0
    Top = 96
    Width = 924
    Height = 546
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    Visible = False
    object Panel8: TPanel
      Left = 1
      Top = 1
      Width = 908
      Height = 160
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 12
        Top = 108
        Width = 168
        Height = 13
        Caption = 'Funcion'#225'rio (Cadastro de entidade):'
      end
      object Label4: TLabel
        Left = 12
        Top = 8
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label5: TLabel
        Left = 12
        Top = 72
        Width = 280
        Height = 13
        Caption = 'OBS.: Selecione a empresa antes do funcion'#225'rio!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 532
        Top = 108
        Width = 57
        Height = 13
        Caption = 'C'#243'digo (F4):'
      end
      object EdCodigo: TdmkEditCB
        Left = 12
        Top = 123
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCodigoChange
        DBLookupComboBox = CBCodigo
        IgnoraDBLookupComboBox = False
      end
      object CBCodigo: TdmkDBLookupComboBox
        Left = 68
        Top = 123
        Width = 461
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEFUNCI'
        ListSource = DsFunci
        TabOrder = 3
        dmkEditCB = EdCodigo
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEmpresa: TdmkEditCB
        Left = 12
        Top = 23
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 68
        Top = 23
        Width = 533
        Height = 21
        Color = clWhite
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdFunci: TdmkEdit
        Left = 532
        Top = 123
        Width = 68
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdFunciKeyDown
      end
    end
    object GroupBox14: TGroupBox
      Left = 0
      Top = 483
      Width = 924
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BitBtn1Click
      end
      object Panel6: TPanel
        Left = 814
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn2: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 924
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 876
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 660
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 155
        Height = 32
        Caption = 'Funcion'#225'rios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 155
        Height = 32
        Caption = 'Funcion'#225'rios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 155
        Height = 32
        Caption = 'Funcion'#225'rios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 924
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel43: TPanel
      Left = 2
      Top = 15
      Width = 920
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsFPFunci: TDataSource
    DataSet = QrFPFunci
    Left = 444
    Top = 13
  end
  object QrFPFunci: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFPFunciBeforeOpen
    AfterOpen = QrFPFunciAfterOpen
    BeforeClose = QrFPFunciBeforeClose
    BeforeScroll = QrFPFunciBeforeScroll
    AfterScroll = QrFPFunciAfterScroll
    SQL.Strings = (
      'SELECT fpf.Descricao NOMEFUNCAO, fpd.Descricao NOMEDEPTO,'
      'fp2.Descricao NOMECBO2002, fpc.Descricao NOMECATEG,'
      'fpv.Descricao NOMEVINCU, fpa.Descricao NOMEADMIS,'
      'fps.Descricao NOMESITUA, fpl.Descricao NOMETPSAL, '
      'IF(efu.Tipo=0, efu.RazaoSocial, efu.Nome) NUMEFUNCI, '
      'IF(epr.Tipo=0, epr.RazaoSocial, epr.Nome) NUMEEMPRESA, '
      'fp4.Descricao NOMECARGO, fpn.Descricao NOMENACIONALID, '
      'fpe.Descricao NOMEECIVIL, fpr.Descricao NOMERACA,'
      'fpg.Descricao NOMEGRAUI, fpm.Nome NOMELOCTRAB, fun.* '
      'FROM fpfunci fun'
      'LEFT JOIN entidades efu ON efu.Codigo=fun.Codigo'
      'LEFT JOIN entidades epr ON epr.Codigo=fun.Empresa'
      'LEFT JOIN fpfunca   fpf ON fpf.CodID=fun.Funcao'
      'LEFT JOIN fpdepto   fpd ON fpd.CodID=fun.Depto'
      'LEFT JOIN fpcbo02   fp2 ON fp2.CodID=fun.CBO2002'
      'LEFT JOIN fpcbo94   fp4 ON fp4.CodID=fun.Cargo'
      'LEFT JOIN fpcateg   fpc ON fpc.Codigo=fun.Categoria'
      'LEFT JOIN fpvincu   fpv ON fpv.Codigo=fun.Vinculo'
      'LEFT JOIN fpadmis   fpa ON fpa.Codigo=fun.Admissao'
      'LEFT JOIN fpsitua   fps ON fps.Codigo=fun.Situacao'
      'LEFT JOIN fptpsal   fpl ON fpl.Codigo=fun.TipoSal'
      'LEFT JOIN fpnacio   fpn ON fpn.Codigo=fun.Nacionalid'
      'LEFT JOIN fpcivil   fpe ON fpe.Codigo=fun.EstCivil'
      'LEFT JOIN fpracas   fpr ON fpr.Codigo=fun.Raca'
      'LEFT JOIN fpgraui   fpg ON fpg.Codigo=fun.GrauInstru'
      'LEFT JOIN fpmunic   fpm ON fpm.CodID=fun.LocalTrab'
      'WHERE fun.Codigo > 0')
    Left = 416
    Top = 13
    object QrFPFunciNUMEFUNCI: TWideStringField
      FieldName = 'NUMEFUNCI'
      Size = 100
    end
    object QrFPFunciNUMEEMPRESA: TWideStringField
      FieldName = 'NUMEEMPRESA'
      Size = 100
    end
    object QrFPFunciDepto: TWideStringField
      FieldName = 'Depto'
      Origin = 'fpfunci.Depto'
      Required = True
    end
    object QrFPFunciFuncao: TWideStringField
      FieldName = 'Funcao'
      Origin = 'fpfunci.Funcao'
      Required = True
    end
    object QrFPFunciNOMEFUNCAO: TWideStringField
      FieldName = 'NOMEFUNCAO'
      Origin = 'fpfunca.Descricao'
      Size = 100
    end
    object QrFPFunciNOMEDEPTO: TWideStringField
      FieldName = 'NOMEDEPTO'
      Origin = 'fpdepto.Descricao'
      Size = 50
    end
    object QrFPFunciNOMECBO2002: TWideStringField
      FieldName = 'NOMECBO2002'
      Origin = 'fpcbo02.Descricao'
      Size = 255
    end
    object QrFPFunciCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fpfunci.Codigo'
    end
    object QrFPFunciEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'fpfunci.Empresa'
    end
    object QrFPFunciFunci: TIntegerField
      FieldName = 'Funci'
      Origin = 'fpfunci.Funci'
    end
    object QrFPFunciRegistro: TWideStringField
      FieldName = 'Registro'
      Origin = 'fpfunci.Registro'
      Size = 10
    end
    object QrFPFunciCategoria: TIntegerField
      FieldName = 'Categoria'
      Origin = 'fpfunci.Categoria'
    end
    object QrFPFunciCBO2002: TWideStringField
      DisplayWidth = 7
      FieldName = 'CBO2002'
      Origin = 'fpfunci.CBO2002'
      Size = 7
    end
    object QrFPFunciVinculo: TIntegerField
      FieldName = 'Vinculo'
      Origin = 'fpfunci.Vinculo'
    end
    object QrFPFunciAlvara: TSmallintField
      FieldName = 'Alvara'
      Origin = 'fpfunci.Alvara'
    end
    object QrFPFunciSituacao: TIntegerField
      FieldName = 'Situacao'
      Origin = 'fpfunci.Situacao'
    end
    object QrFPFunciAdmissao: TIntegerField
      FieldName = 'Admissao'
      Origin = 'fpfunci.Admissao'
    end
    object QrFPFunciDataAdm: TDateField
      FieldName = 'DataAdm'
      Origin = 'fpfunci.DataAdm'
    end
    object QrFPFunciHrSeman: TIntegerField
      FieldName = 'HrSeman'
      Origin = 'fpfunci.HrSeman'
    end
    object QrFPFunciTipoSal: TSmallintField
      FieldName = 'TipoSal'
      Origin = 'fpfunci.TipoSal'
    end
    object QrFPFunciSalario: TFloatField
      FieldName = 'Salario'
      Origin = 'fpfunci.Salario'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrFPFunciFilhos: TIntegerField
      FieldName = 'Filhos'
      Origin = 'fpfunci.Filhos'
    end
    object QrFPFunciDependent: TIntegerField
      FieldName = 'Dependent'
      Origin = 'fpfunci.Dependent'
    end
    object QrFPFunciPensaoAlm: TFloatField
      FieldName = 'PensaoAlm'
      Origin = 'fpfunci.PensaoAlm'
      Required = True
      DisplayFormat = '#,###,##0.000000'
    end
    object QrFPFunciBancoSal: TIntegerField
      FieldName = 'BancoSal'
      Origin = 'fpfunci.BancoSal'
    end
    object QrFPFunciContaSal: TWideStringField
      FieldName = 'ContaSal'
      Origin = 'fpfunci.ContaSal'
      Size = 15
    end
    object QrFPFunciOptaFGTS: TSmallintField
      FieldName = 'OptaFGTS'
      Origin = 'fpfunci.OptaFGTS'
    end
    object QrFPFunciBancoFGTS: TIntegerField
      FieldName = 'BancoFGTS'
      Origin = 'fpfunci.BancoFGTS'
    end
    object QrFPFunciContaFGTS: TWideStringField
      FieldName = 'ContaFGTS'
      Origin = 'fpfunci.ContaFGTS'
      Size = 15
    end
    object QrFPFunciDataFGTS: TDateField
      FieldName = 'DataFGTS'
      Origin = 'fpfunci.DataFGTS'
    end
    object QrFPFunciDataExame: TDateField
      FieldName = 'DataExame'
      Origin = 'fpfunci.DataExame'
    end
    object QrFPFunciPerExame: TIntegerField
      FieldName = 'PerExame'
      Origin = 'fpfunci.PerExame'
    end
    object QrFPFunciDataIniAf: TDateField
      FieldName = 'DataIniAf'
      Origin = 'fpfunci.DataIniAf'
    end
    object QrFPFunciDataFinAf: TDateField
      FieldName = 'DataFinAf'
      Origin = 'fpfunci.DataFinAf'
    end
    object QrFPFunciMesesAf: TIntegerField
      FieldName = 'MesesAf'
      Origin = 'fpfunci.MesesAf'
    end
    object QrFPFunciDataAviso: TDateField
      FieldName = 'DataAviso'
      Origin = 'fpfunci.DataAviso'
    end
    object QrFPFunciAvisoInden: TSmallintField
      FieldName = 'AvisoInden'
      Origin = 'fpfunci.AvisoInden'
    end
    object QrFPFunciDiasAviso: TIntegerField
      FieldName = 'DiasAviso'
      Origin = 'fpfunci.DiasAviso'
    end
    object QrFPFunciDataDemiss: TDateField
      FieldName = 'DataDemiss'
      Origin = 'fpfunci.DataDemiss'
    end
    object QrFPFunciDemissao: TIntegerField
      FieldName = 'Demissao'
      Origin = 'fpfunci.Demissao'
    end
    object QrFPFunciCodigoAf: TWideStringField
      FieldName = 'CodigoAf'
      Origin = 'fpfunci.CodigoAf'
      Size = 10
    end
    object QrFPFunciMotivoAf: TWideStringField
      FieldName = 'MotivoAf'
      Origin = 'fpfunci.MotivoAf'
      Size = 50
    end
    object QrFPFunciObs: TWideMemoField
      FieldName = 'Obs'
      Origin = 'fpfunci.Obs'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFPFunciGFIP: TSmallintField
      FieldName = 'GFIP'
      Origin = 'fpfunci.GFIP'
    end
    object QrFPFunciRAIS: TSmallintField
      FieldName = 'RAIS'
      Origin = 'fpfunci.RAIS'
    end
    object QrFPFunciProfessor: TSmallintField
      FieldName = 'Professor'
      Origin = 'fpfunci.Professor'
    end
    object QrFPFunciAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'fpfunci.Ativo'
    end
    object QrFPFunciLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'fpfunci.Lk'
    end
    object QrFPFunciDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'fpfunci.DataCad'
    end
    object QrFPFunciDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'fpfunci.DataAlt'
    end
    object QrFPFunciUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'fpfunci.UserCad'
    end
    object QrFPFunciUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'fpfunci.UserAlt'
    end
    object QrFPFunciNOMECATEG: TWideStringField
      FieldName = 'NOMECATEG'
      Origin = 'fpcateg.Descricao'
      Size = 30
    end
    object QrFPFunciNOMEVINCU: TWideStringField
      FieldName = 'NOMEVINCU'
      Origin = 'fpvincu.Descricao'
      Size = 200
    end
    object QrFPFunciNOMEADMIS: TWideStringField
      FieldName = 'NOMEADMIS'
      Origin = 'fpadmis.Descricao'
      Size = 200
    end
    object QrFPFunciNOMESITUA: TWideStringField
      FieldName = 'NOMESITUA'
      Origin = 'fpsitua.Descricao'
      Size = 250
    end
    object QrFPFunciNOMETPSAL: TWideStringField
      FieldName = 'NOMETPSAL'
      Origin = 'fptpsal.Descricao'
      Size = 25
    end
    object QrFPFunciNacionalid: TIntegerField
      FieldName = 'Nacionalid'
      Origin = 'fpfunci.Nacionalid'
      Required = True
    end
    object QrFPFunciAnoChegada: TIntegerField
      FieldName = 'AnoChegada'
      Origin = 'fpfunci.AnoChegada'
      Required = True
    end
    object QrFPFunciGrauInstru: TIntegerField
      FieldName = 'GrauInstru'
      Origin = 'fpfunci.GrauInstru'
      Required = True
    end
    object QrFPFunciEstCivil: TIntegerField
      FieldName = 'EstCivil'
      Origin = 'fpfunci.EstCivil'
      Required = True
    end
    object QrFPFunciCTPS: TWideStringField
      FieldName = 'CTPS'
      Origin = 'fpfunci.CTPS'
      Size = 15
    end
    object QrFPFunciSerieCTPS: TWideStringField
      FieldName = 'SerieCTPS'
      Origin = 'fpfunci.SerieCTPS'
      Size = 10
    end
    object QrFPFunciPIS: TWideStringField
      FieldName = 'PIS'
      Origin = 'fpfunci.PIS'
    end
    object QrFPFunciTitEleitor: TWideStringField
      FieldName = 'TitEleitor'
      Origin = 'fpfunci.TitEleitor'
    end
    object QrFPFunciReservista: TWideStringField
      FieldName = 'Reservista'
      Origin = 'fpfunci.Reservista'
    end
    object QrFPFunciRaca: TIntegerField
      FieldName = 'Raca'
      Origin = 'fpfunci.Raca'
      Required = True
    end
    object QrFPFunciDeficiente: TSmallintField
      FieldName = 'Deficiente'
      Origin = 'fpfunci.Deficiente'
      Required = True
    end
    object QrFPFunciFoto: TWideStringField
      FieldName = 'Foto'
      Origin = 'fpfunci.Foto'
      Size = 255
    end
    object QrFPFunciCargo: TWideStringField
      FieldName = 'Cargo'
      Origin = 'fpfunci.Cargo'
      Required = True
      Size = 5
    end
    object QrFPFunciLocalTrab: TWideStringField
      FieldName = 'LocalTrab'
      Origin = 'fpfunci.LocalTrab'
      Size = 7
    end
    object QrFPFunciInsalubrid: TFloatField
      FieldName = 'Insalubrid'
      Origin = 'fpfunci.Insalubrid'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFPFunciPericulosi: TFloatField
      FieldName = 'Periculosi'
      Origin = 'fpfunci.Periculosi'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFPFunciNOMECARGO: TWideStringField
      FieldName = 'NOMECARGO'
      Origin = 'fpcbo94.Descricao'
      Size = 200
    end
    object QrFPFunciNOMENACIONALID: TWideStringField
      FieldName = 'NOMENACIONALID'
      Origin = 'fpnacio.Descricao'
      Size = 30
    end
    object QrFPFunciNOMEECIVIL: TWideStringField
      FieldName = 'NOMEECIVIL'
      Origin = 'fpcivil.Descricao'
      Size = 15
    end
    object QrFPFunciNOMERACA: TWideStringField
      FieldName = 'NOMERACA'
      Origin = 'fpracas.Descricao'
      Size = 15
    end
    object QrFPFunciNOMEGRAUI: TWideStringField
      FieldName = 'NOMEGRAUI'
      Origin = 'fpgraui.Descricao'
      Size = 200
    end
    object QrFPFunciNOMELOCTRAB: TWideStringField
      FieldName = 'NOMELOCTRAB'
      Origin = 'fpmunic.Nome'
      Size = 35
    end
    object QrFPFunciPensaoLiq: TSmallintField
      FieldName = 'PensaoLiq'
      Origin = 'fpfunci.PensaoLiq'
      Required = True
    end
    object QrFPFunciComoCalcIR: TSmallintField
      FieldName = 'ComoCalcIR'
      Origin = 'fpfunci.ComoCalcIR'
      Required = True
    end
    object QrFPFunciHEFator: TFloatField
      FieldName = 'HEFator'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrFPFunciANTipo: TSmallintField
      FieldName = 'ANTipo'
      Required = True
    end
    object QrFPFunciANIniHr: TTimeField
      FieldName = 'ANIniHr'
      Required = True
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciANFimHr: TTimeField
      FieldName = 'ANFimHr'
      Required = True
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciANMinHr: TTimeField
      FieldName = 'ANMinHr'
      Required = True
      DisplayFormat = 'hh:nn:ss'
    end
    object QrFPFunciLei8923_94: TFloatField
      FieldName = 'Lei8923_94'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrFPFunciAdiciNotur: TFloatField
      FieldName = 'AdiciNotur'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrFPFunciPriDdPonto: TSmallintField
      FieldName = 'PriDdPonto'
      Required = True
    end
    object QrFPFunciAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFPFunciChapa: TWideStringField
      FieldName = 'Chapa'
      Size = 50
    end
  end
  object QrFunci: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, '
      'IF(en.Nome="", en.RazaoSocial, en.Nome) NOMEFUNCI'
      'FROM entidades en'
      'WHERE (en.Fornece1=:P0'
      'OR en.Fornece2=:P1'
      'OR en.Fornece3=:P2'
      'OR en.Fornece4=:P3'
      'OR en.Fornece5=:P4'
      'OR en.Fornece6=:P5'
      ')'
      'AND NOT (Codigo IN ('
      '  SELECT Codigo FROM fpfunci'
      '  WHERE Empresa=:P6'
      '  AND DataDemiss=0))'
      'ORDER BY NOMEFUNCI')
    Left = 473
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end>
    object QrFunciCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFunciNOMEFUNCI: TWideStringField
      FieldName = 'NOMEFUNCI'
      Required = True
      Size = 100
    end
  end
  object DsFunci: TDataSource
    DataSet = QrFunci
    Left = 501
    Top = 13
  end
  object QrMax: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Funci) Funci'
      'FROM fpfunci'
      'WHERE Empresa=:P0'
      '')
    Left = 585
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMaxFunci: TIntegerField
      FieldName = 'Funci'
    end
  end
  object QrPesq1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM fpfunci'
      'WHERE Empresa=:P0'
      'AND Funci=:P1')
    Left = 616
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPesq1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrFPFunca: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodID, Descricao'
      'FROM fpfunca'
      'ORDER BY Descricao')
    Left = 417
    Top = 85
    object QrFPFuncaCodID: TWideStringField
      FieldName = 'CodID'
    end
    object QrFPFuncaDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
  end
  object DsFPFunca: TDataSource
    DataSet = QrFPFunca
    Left = 445
    Top = 85
  end
  object QrFPDepto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodID, Descricao'
      'FROM fpdepto'
      'ORDER BY Descricao')
    Left = 473
    Top = 85
    object QrFPDeptoCodID: TWideStringField
      FieldName = 'CodID'
    end
    object QrFPDeptoDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 50
    end
  end
  object DsFPDepto: TDataSource
    DataSet = QrFPDepto
    Left = 501
    Top = 85
  end
  object QrFPCBO02: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodID, Descricao'
      'FROM fpcbo02'
      'ORDER BY Descricao')
    Left = 529
    Top = 85
    object QrFPCBO02CodID: TWideStringField
      FieldName = 'CodID'
      Size = 7
    end
    object QrFPCBO02Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
  end
  object DsFPCBO02: TDataSource
    DataSet = QrFPCBO02
    Left = 557
    Top = 85
  end
  object QrFPCateg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao'
      'FROM fpcateg'
      'ORDER BY Descricao')
    Left = 585
    Top = 85
    object QrFPCategCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPCategDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 30
    end
  end
  object DsFPCateg: TDataSource
    DataSet = QrFPCateg
    Left = 613
    Top = 85
  end
  object QrFPVincu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao'
      'FROM fpvincu'
      'ORDER BY Descricao')
    Left = 417
    Top = 113
    object QrFPVincuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPVincuDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 200
    end
  end
  object DsFPVincu: TDataSource
    DataSet = QrFPVincu
    Left = 445
    Top = 113
  end
  object QrFPAdmis: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao'
      'FROM fpadmis'
      'ORDER BY Descricao')
    Left = 473
    Top = 113
    object QrFPAdmisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPAdmisDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 200
    end
  end
  object DsFPAdmis: TDataSource
    DataSet = QrFPAdmis
    Left = 501
    Top = 113
  end
  object QrFPSitua: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao'
      'FROM fpsitua'
      'ORDER BY Descricao')
    Left = 529
    Top = 113
    object QrFPSituaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPSituaDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 200
    end
  end
  object DsFPSitua: TDataSource
    DataSet = QrFPSitua
    Left = 557
    Top = 113
  end
  object QrFPDeslg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao'
      'FROM fpdeslg'
      'ORDER BY Descricao')
    Left = 585
    Top = 113
    object QrFPDeslgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPDeslgDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 200
    end
  end
  object DsFPDeslg: TDataSource
    DataSet = QrFPDeslg
    Left = 613
    Top = 113
  end
  object QrFPTpSal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao'
      'FROM fptpsal'
      'WHERE Ativo=1'
      'ORDER BY Descricao')
    Left = 648
    Top = 12
    object QrFPTpSalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPTpSalDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 25
    end
  end
  object DsFPTpSal: TDataSource
    DataSet = QrFPTpSal
    Left = 676
    Top = 12
  end
  object QrFPNacio: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao'
      'FROM fpnacio'
      'ORDER BY Descricao')
    Left = 277
    Top = 53
    object QrFPNacioCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPNacioDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 30
    end
  end
  object DsFPNacio: TDataSource
    DataSet = QrFPNacio
    Left = 305
    Top = 53
  end
  object QrFPGrauI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao'
      'FROM fpgraui'
      'ORDER BY Descricao')
    Left = 277
    Top = 81
    object QrFPGrauICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPGrauIDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 200
    end
  end
  object DsFPGrauI: TDataSource
    DataSet = QrFPGrauI
    Left = 305
    Top = 81
  end
  object QrFPCivil: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao'
      'FROM fpcivil'
      'ORDER BY Descricao')
    Left = 333
    Top = 53
    object QrFPCivilCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPCivilDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 15
    end
  end
  object DsFPCivil: TDataSource
    DataSet = QrFPCivil
    Left = 361
    Top = 53
  end
  object QrFPRacas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao'
      'FROM fpracas'
      'ORDER BY Descricao')
    Left = 333
    Top = 81
    object QrFPRacasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPRacasDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 15
    end
  end
  object DsFPRacas: TDataSource
    DataSet = QrFPRacas
    Left = 361
    Top = 81
  end
  object QrFPCBO94: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodID, Descricao'
      'FROM fpcbo94'
      'ORDER BY Descricao')
    Left = 333
    Top = 109
    object QrFPCBO94CodID: TWideStringField
      FieldName = 'CodID'
      Size = 7
    end
    object QrFPCBO94Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 200
    end
  end
  object DsFPCBO94: TDataSource
    DataSet = QrFPCBO94
    Left = 361
    Top = 109
  end
  object QrFPMunic: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM fpmunic'
      'ORDER BY Nome')
    Left = 277
    Top = 109
    object QrFPMunicCodID: TWideStringField
      FieldName = 'CodID'
      Size = 7
    end
    object QrFPMunicNome: TWideStringField
      FieldName = 'Nome'
      Size = 35
    end
    object QrFPMunicFaixaCEP: TWideStringField
      FieldName = 'FaixaCEP'
      Size = 5
    end
    object QrFPMunicAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsFPMunic: TDataSource
    DataSet = QrFPMunic
    Left = 305
    Top = 109
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Left = 630
    Top = 194
  end
  object QrFPFunciEve: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrFPFunciEveAfterOpen
    BeforeClose = QrFPFunciEveBeforeClose
    AfterScroll = QrFPFunciEveAfterScroll
    OnCalcFields = QrFPFunciEveCalcFields
    SQL.Strings = (
      'SELECT fue.*, eve.Descricao NOMEEVENTO,'
      'con.Nome NOMECONTA'
      'FROM fpfuncieve fue'
      'LEFT JOIN fpevent eve ON eve.Codigo=fue.Evento'
      'LEFT JOIN contas  con ON con.Codigo=fue.Conta'
      'WHERE fue.Codigo=:P0')
    Left = 418
    Top = 54
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFPFunciEveCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fpfuncieve.Codigo'
    end
    object QrFPFunciEveControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'fpfuncieve.Controle'
    end
    object QrFPFunciEveEvento: TIntegerField
      FieldName = 'Evento'
      Origin = 'fpfuncieve.Evento'
    end
    object QrFPFunciEveConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'fpfuncieve.Conta'
    end
    object QrFPFunciEveLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'fpfuncieve.Lk'
    end
    object QrFPFunciEveDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'fpfuncieve.DataCad'
    end
    object QrFPFunciEveDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'fpfuncieve.DataAlt'
    end
    object QrFPFunciEveUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'fpfuncieve.UserCad'
    end
    object QrFPFunciEveUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'fpfuncieve.UserAlt'
    end
    object QrFPFunciEveIncidencia: TSmallintField
      FieldName = 'Incidencia'
      Origin = 'fpfuncieve.Incidencia'
    end
    object QrFPFunciEveEve_Incid: TSmallintField
      FieldName = 'Eve_Incid'
      Origin = 'fpfuncieve.Eve_Incid'
    end
    object QrFPFunciEveEve_ValPer: TFloatField
      FieldName = 'Eve_ValPer'
      Origin = 'fpfuncieve.Eve_ValPer'
      DisplayFormat = '#,###,##0.00'
    end
    object QrFPFunciEveEve_Meses: TSmallintField
      FieldName = 'Eve_Meses'
      Origin = 'fpfuncieve.Eve_Meses'
    end
    object QrFPFunciEveCta_IncPer: TFloatField
      FieldName = 'Cta_IncPer'
      Origin = 'fpfuncieve.Cta_IncPer'
      DisplayFormat = '#,###,##0.00'
    end
    object QrFPFunciEveCta_MesesR: TSmallintField
      FieldName = 'Cta_MesesR'
      Origin = 'fpfuncieve.Cta_MesesR'
    end
    object QrFPFunciEveNOMEEVENTO: TWideStringField
      FieldName = 'NOMEEVENTO'
      Size = 50
    end
    object QrFPFunciEveNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrFPFunciEveNOME_INCIDENCIA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_INCIDENCIA'
      Size = 8
      Calculated = True
    end
    object QrFPFunciEveNOME_EVE_INCID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_EVE_INCID'
      Calculated = True
    end
    object QrFPFunciEveDesBasIRRF: TSmallintField
      FieldName = 'DesBasIRRF'
      Required = True
    end
    object QrFPFunciEveProporciHT: TSmallintField
      FieldName = 'ProporciHT'
      Required = True
    end
    object QrFPFunciEveUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 15
    end
    object QrFPFunciEveEve_Ciclo: TSmallintField
      FieldName = 'Eve_Ciclo'
    end
    object QrFPFunciEveEve_DtCicl: TDateField
      FieldName = 'Eve_DtCicl'
    end
  end
  object DsFPFunciEve: TDataSource
    DataSet = QrFPFunciEve
    Left = 446
    Top = 54
  end
  object QrEventos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao'
      'FROM fpevent'
      'WHERE Auto=0'
      'ORDER BY Descricao')
    Left = 474
    Top = 54
    object QrEventosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEventosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 50
    end
  end
  object DsEventos: TDataSource
    DataSet = QrEventos
    Left = 502
    Top = 54
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas')
    Left = 530
    Top = 54
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 558
    Top = 54
  end
  object QrFPtpCal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descricao'
      'FROM fptpcal'
      'ORDER BY Descricao'
      '')
    Left = 586
    Top = 54
    object QrFPtpCalCodigo: TSmallintField
      FieldName = 'Codigo'
    end
    object QrFPtpCalDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 35
    end
  end
  object DsFPtpCal: TDataSource
    DataSet = QrFPtpCal
    Left = 614
    Top = 54
  end
  object QrFPFunciFer: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrFPFunciFerAfterOpen
    BeforeClose = QrFPFunciFerBeforeClose
    OnCalcFields = QrFPFunciFerCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM fpfuncifer'
      'WHERE Empresa=:P0'
      'AND Codigo=:P1'
      'ORDER BY DtIniPA DESC')
    Left = 651
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFPFunciFerEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'fpfuncifer.Empresa'
    end
    object QrFPFunciFerCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fpfuncifer.Codigo'
    end
    object QrFPFunciFerCalculo: TIntegerField
      FieldName = 'Calculo'
      Origin = 'fpfuncifer.Calculo'
    end
    object QrFPFunciFerPecun: TSmallintField
      FieldName = 'Pecun'
      Origin = 'fpfuncifer.Pecun'
    end
    object QrFPFunciFerMeHEMan1xx: TFloatField
      FieldName = 'MeHEMan1xx'
      DisplayFormat = '#,###,##0.00'
    end
    object QrFPFunciFerMeHEMan200: TFloatField
      FieldName = 'MeHEMan200'
      DisplayFormat = '#,###,##0.00'
    end
    object QrFPFunciFerMediaCoMan: TFloatField
      FieldName = 'MediaCoMan'
      Origin = 'fpfuncifer.MediaCoMan'
      DisplayFormat = '#,###,##0.00'
    end
    object QrFPFunciFerMediaAdMan: TFloatField
      FieldName = 'MediaAdMan'
      Origin = 'fpfuncifer.MediaAdMan'
      DisplayFormat = '#,###,##0.00'
    end
    object QrFPFunciFerDtIniPA: TDateField
      FieldName = 'DtIniPA'
      Origin = 'fpfuncifer.DtIniPA'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtFimPA: TDateField
      FieldName = 'DtFimPA'
      Origin = 'fpfuncifer.DtFimPA'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDiasFaltI: TIntegerField
      FieldName = 'DiasFaltI'
      Origin = 'fpfuncifer.DiasFaltI'
    end
    object QrFPFunciFerDiasFaltP: TIntegerField
      FieldName = 'DiasFaltP'
      Origin = 'fpfuncifer.DiasFaltP'
    end
    object QrFPFunciFerDiasGozar: TSmallintField
      FieldName = 'DiasGozar'
      Origin = 'fpfuncifer.DiasGozar'
    end
    object QrFPFunciFerDiasPecun: TSmallintField
      FieldName = 'DiasPecun'
      Origin = 'fpfuncifer.DiasPecun'
    end
    object QrFPFunciFerDtIniPecun: TDateField
      FieldName = 'DtIniPecun'
      Origin = 'fpfuncifer.DtIniPecun'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtFimPecun: TDateField
      FieldName = 'DtFimPecun'
      Origin = 'fpfuncifer.DtFimPecun'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtIniPG: TDateField
      FieldName = 'DtIniPG'
      Origin = 'fpfuncifer.DtIniPG'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtFimFeria: TDateField
      FieldName = 'DtFimFeria'
      Origin = 'fpfuncifer.DtFimFeria'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtAvisoFer: TDateField
      FieldName = 'DtAvisoFer'
      Origin = 'fpfuncifer.DtAvisoFer'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtRecPgto: TDateField
      FieldName = 'DtRecPgto'
      Origin = 'fpfuncifer.DtRecPgto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtSolPecun: TDateField
      FieldName = 'DtSolPecun'
      Origin = 'fpfuncifer.DtSolPecun'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtSolPrP13: TDateField
      FieldName = 'DtSolPrP13'
      Origin = 'fpfuncifer.DtSolPrP13'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerDtRetorno: TDateField
      FieldName = 'DtRetorno'
      Origin = 'fpfuncifer.DtRetorno'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrFPFunciFerLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'fpfuncifer.Lk'
    end
    object QrFPFunciFerDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'fpfuncifer.DataCad'
    end
    object QrFPFunciFerDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'fpfuncifer.DataAlt'
    end
    object QrFPFunciFerUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'fpfuncifer.UserCad'
    end
    object QrFPFunciFerUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'fpfuncifer.UserAlt'
    end
    object QrFPFunciFerDTINIPECUN_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTINIPECUN_TXT'
      Size = 8
      Calculated = True
    end
    object QrFPFunciFerDTFIMPECUN_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTFIMPECUN_TXT'
      Size = 8
      Calculated = True
    end
    object QrFPFunciFerDTSOLPECUN_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTSOLPECUN_TXT'
      Size = 8
      Calculated = True
    end
    object QrFPFunciFerDTSOLPRP13_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTSOLPRP13_TXT'
      Size = 8
      Calculated = True
    end
    object QrFPFunciFerPECUNIO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PECUNIO_TXT'
      Size = 1
      Calculated = True
    end
    object QrFPFunciFerDtFimPG: TDateField
      FieldName = 'DtFimPG'
      Origin = 'fpfuncifer.DtFimPG'
    end
    object QrFPFunciFerControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'fpfuncifer.Controle'
    end
  end
  object DsFPFunciFer: TDataSource
    DataSet = QrFPFunciFer
    Left = 679
    Top = 56
  end
  object QrFaltas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM fpferiafal'
      'WHERE Limite >=:P0'
      'ORDER BY Limite')
    Left = 650
    Top = 86
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFaltasDias: TSmallintField
      FieldName = 'Dias'
    end
  end
  object frxAvisoFerias: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39458.837724780100000000
    ReportOptions.LastChange = 39459.872825520800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Picture1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <ImagemExiste> = True then'
      '  begin'
      '    Picture1.Visible := True;'
      '    Picture1.LoadFromFile(<ImagemCaminho>);'
      '  end else Picture1.Visible := False;'
      'end;'
      ''
      'procedure Line3OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure Memo54OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  Memo54.Visible := False;'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxAvisoFeriasGetValue
    Left = 650
    Top = 114
    Datasets = <
      item
      end
      item
      end
      item
        DataSet = frxDsFPFunci
        DataSetName = 'frxDsFPFunci'
      end
      item
        DataSet = frxDsFPFuncifer
        DataSetName = 'frxDsFPFuncifer'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 540.472436060000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        RowCount = 2
        object Shape1: TfrxShapeView
          Left = 3.779481180000000000
          Top = 34.015770000000000000
          Width = 710.551640000000000000
          Height = 120.944881890000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo1: TfrxMemoView
          Left = 3.779481180000000000
          Top = 34.015770000000000000
          Width = 616.063390000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'AVISO DE F'#201'RIAS')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 7.559011180000000000
          Top = 56.692950000000000000
          Width = 419.527830000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsFPFunci."Empresa"] - [frxDsFPFunci."NUMEEMPRESA"]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 623.622401180000000000
          Top = 34.015770000000000000
          Width = 90.708671180000000000
          Height = 120.944911180000000000
          OnBeforePrint = 'Picture1OnBeforePrint'
          ShowHint = False
          Frame.Width = 0.100000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Line1: TfrxLineView
          Left = 623.622401180000000000
          Top = 34.015770000000000000
          Height = 120.944960000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line2: TfrxLineView
          Left = 3.779481180000000000
          Top = 94.488250000000000000
          Width = 619.842920000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo2: TfrxMemoView
          Left = 7.559011180000000000
          Top = 75.590600000000000000
          Width = 612.283860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 7.559011180000000000
          Top = 94.488250000000000000
          Width = 41.574830000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'C'#243'digo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 430.866371180000000000
          Top = 56.692950000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CNPJ: [frxDsEndereco."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 7.559011180000000000
          Top = 105.826840000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[FormatFloat('#39'0000'#39',<frxDsFPFunci."Funci">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 113.385851180000000000
          Top = 94.488250000000000000
          Width = 102.047310000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Registro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 113.385851180000000000
          Top = 105.826840000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."Registro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 7.559011180000000000
          Top = 124.724490000000000000
          Width = 151.181200000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Nome do funcion'#225'rio:')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 7.559011180000000000
          Top = 136.063080000000000000
          Width = 427.086890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsFPFunci."NUMEFUNCI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 377.952951180000000000
          Top = 94.488250000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'C.B.O.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 377.952951180000000000
          Top = 105.826840000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."CBO2002"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 434.645901180000000000
          Top = 94.488250000000000000
          Width = 185.196784490000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Departamento:')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 434.645901180000000000
          Top = 105.826840000000000000
          Width = 185.196784490000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."Depto"] - [frxDsFPFunci."NOMEDEPTO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 434.645901180000000000
          Top = 124.724490000000000000
          Width = 185.196784490000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Fun'#231#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 434.645901180000000000
          Top = 136.063080000000000000
          Width = 185.196784490000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."Funcao"] - [frxDsFPFunci."NOMEFUNCAO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 52.913371180000000000
          Top = 94.488250000000000000
          Width = 56.692950000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Identificador:')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 52.913371180000000000
          Top = 105.826840000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[FormatFloat('#39'0000'#39',<frxDsFPFunci."Codigo">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 313.700941180000000000
          Top = 94.488250000000000000
          Width = 60.472480000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'S'#233'rie:')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 313.700941180000000000
          Top = 105.826840000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."SerieCTPS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 215.433161180000000000
          Top = 94.488250000000000000
          Width = 94.488250000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Carteira de trabalho:')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 215.433161180000000000
          Top = 105.826840000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."CTPS"]')
          ParentFont = False
          WordWrap = False
        end
        object Line9: TfrxLineView
          Left = 623.622401180000000000
          Top = 34.015770000000000000
          Width = 90.708720000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Shape2: TfrxShapeView
          Left = 3.779481180000000000
          Top = 291.023810000000000000
          Width = 710.551395910000000000
          Height = 219.212740000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo56: TfrxMemoView
          Left = 11.338541180000000000
          Top = 313.700990000000000000
          Width = 695.433520000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[frxDsEndereco."CIDADE"], [FormatDateTime('#39'dd" de "mmmm" de "yyy' +
              'y'#39',<frxDsFPFuncifer."DtAvisoFer">)].')
          ParentFont = False
          WordWrap = False
        end
        object Shape3: TfrxShapeView
          Left = 3.779530000000000000
          Top = 154.960730000000000000
          Width = 710.551640000000000000
          Height = 136.063080000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo4: TfrxMemoView
          Left = 11.338590000000000000
          Top = 162.519790000000000000
          Width = 695.433520000000000000
          Height = 117.165430000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haBlock
          Memo.UTF8W = (
            
              'Comunicamos '#224' V. S'#170', que ser-lhe-'#227'o concedidas f'#233'rias a partir d' +
              'o dia [frxDsFPFuncifer."DtIniPG"] ao dia [frxDsFPFuncifer."DtFim' +
              'PG"], relativas ao per'#237'odo aquisitivo de [frxDsFPFuncifer."DtIni' +
              'PA"] a [frxDsFPFuncifer."DtFimPA"]. A import'#226'ncia relativa as f'#233 +
              'rias e o abono pecuni'#225'rio, se for o caso, ficar'#225' a sua disposi'#231#227 +
              'o no dia [frxDsFPFuncifer."DtRecPgto"].'
            ''
            'Retorno ao trabalho em [frxDsFPFuncifer."DtRetorno"].')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Top = 529.133846060000000000
          Width = 718.110455910000000000
          Height = 11.338590000000000000
          OnAfterPrint = 'Memo54OnAfterPrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDot
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Comunicar as f'#233'rias com anteced'#234'ncia m'#237'nima de 30 dias.')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          Left = 396.850393700787000000
          Top = 472.441250000000000000
          Width = 279.685039370079000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Assinatura do funcion'#225'rio')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 37.795348820000000000
          Top = 472.441250000000000000
          Width = 279.685039370079000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Assinatura do empregador')
          ParentFont = False
          WordWrap = False
        end
        object Line4: TfrxLineView
          Left = 3.779530000000000000
          Top = 359.055350000000000000
          Width = 710.551640000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line5: TfrxLineView
          Left = 359.055118110236000000
          Top = 359.055118110000000000
          Height = 136.063080000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Memo6: TfrxMemoView
          Left = 396.850650000000000000
          Top = 377.953000000000000000
          Width = 279.685220000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              'Ciente em [FormatDateTime('#39'dd/mm/yyyy'#39',<frxDsFPFuncifer."DtAviso' +
              'Fer">)].')
          ParentFont = False
        end
      end
    end
  end
  object PMImprime: TPopupMenu
    OnPopup = PMImprimePopup
    Left = 34
    Top = 22
    object Avisodefrias1: TMenuItem
      Caption = 'Aviso de &F'#233'rias'
      OnClick = Avisodefrias1Click
    end
    object Solicitaodeabonopecunirio1: TMenuItem
      Caption = 'Solicita'#231#227'o de &Abono pecuni'#225'rio'
      OnClick = Solicitaodeabonopecunirio1Click
    end
    object Solicitaoda1parcelado131: TMenuItem
      Caption = 'Solicita'#231#227'o da 1'#170' &Parcela do 13'#186
      OnClick = Solicitaoda1parcelado131Click
    end
  end
  object frxDsFPFuncifer: TfrxDBDataset
    UserName = 'frxDsFPFuncifer'
    CloseDataSource = False
    DataSet = QrFPFunciFer
    BCDToCurrency = False
    Left = 678
    Top = 114
  end
  object frxDsFPFunci: TfrxDBDataset
    UserName = 'frxDsFPFunci'
    CloseDataSource = False
    DataSet = QrFPFunci
    BCDToCurrency = False
    Left = 678
    Top = 86
  end
  object frxSolicitaAbono: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39458.837724780100000000
    ReportOptions.LastChange = 39459.872825520800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Picture1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <ImagemExiste> = True then'
      '  begin'
      '    Picture1.Visible := True;'
      '    Picture1.LoadFromFile(<ImagemCaminho>);'
      '  end else Picture1.Visible := False;'
      'end;'
      ''
      'procedure Line3OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure Memo54OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  Memo54.Visible := False;'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxAvisoFeriasGetValue
    Left = 706
    Top = 114
    Datasets = <
      item
      end
      item
      end
      item
        DataSet = frxDsFPFunci
        DataSetName = 'frxDsFPFunci'
      end
      item
        DataSet = frxDsFPFuncifer
        DataSetName = 'frxDsFPFuncifer'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 540.472436060000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        RowCount = 2
        object Shape1: TfrxShapeView
          Left = 3.779481180000000000
          Top = 34.015770000000000000
          Width = 710.551640000000000000
          Height = 120.944881890000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo1: TfrxMemoView
          Left = 3.779481180000000000
          Top = 34.015770000000000000
          Width = 616.063390000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'SOLICITA'#199#195'O DE F'#201'RIAS EM ABONO PECUNI'#193'RIO')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 7.559011180000000000
          Top = 117.165430000000000000
          Width = 419.527830000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsFPFunci."Empresa"] - [frxDsFPFunci."NUMEEMPRESA"]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 623.622401180000000000
          Top = 34.015770000000000000
          Width = 90.708671180000000000
          Height = 120.944911180000000000
          OnBeforePrint = 'Picture1OnBeforePrint'
          ShowHint = False
          Frame.Width = 0.100000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Line1: TfrxLineView
          Left = 623.622401180000000000
          Top = 34.015770000000000000
          Height = 120.944960000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line2: TfrxLineView
          Left = 3.779481180000000000
          Top = 117.165430000000000000
          Width = 619.842920000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo2: TfrxMemoView
          Left = 7.559011180000000000
          Top = 136.063080000000000000
          Width = 612.283860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 7.559011180000000000
          Top = 86.929190000000000000
          Width = 41.574830000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'C'#243'digo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 430.866371180000000000
          Top = 117.165430000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CNPJ: [frxDsEndereco."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 7.559011180000000000
          Top = 98.267780000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[FormatFloat('#39'0000'#39',<frxDsFPFunci."Funci">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 113.385851180000000000
          Top = 86.929190000000000000
          Width = 102.047310000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Registro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 113.385851180000000000
          Top = 98.267780000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."Registro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 7.559011180000000000
          Top = 56.692950000000000000
          Width = 151.181200000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Nome do funcion'#225'rio:')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 7.559011180000000000
          Top = 68.031540000000000000
          Width = 427.086890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsFPFunci."NUMEFUNCI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 377.952951180000000000
          Top = 86.929190000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'C.B.O.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 377.952951180000000000
          Top = 98.267780000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."CBO2002"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 434.645901180000000000
          Top = 86.929190000000000000
          Width = 185.196784490000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Departamento:')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 434.645901180000000000
          Top = 98.267780000000000000
          Width = 185.196784490000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."Depto"] - [frxDsFPFunci."NOMEDEPTO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 434.645901180000000000
          Top = 56.692950000000000000
          Width = 185.196784490000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Fun'#231#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 434.645901180000000000
          Top = 68.031540000000000000
          Width = 185.196784490000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."Funcao"] - [frxDsFPFunci."NOMEFUNCAO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 52.913371180000000000
          Top = 86.929190000000000000
          Width = 56.692950000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Identificador:')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 52.913371180000000000
          Top = 98.267780000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[FormatFloat('#39'0000'#39',<frxDsFPFunci."Codigo">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 313.700941180000000000
          Top = 86.929190000000000000
          Width = 60.472480000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'S'#233'rie:')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 313.700941180000000000
          Top = 98.267780000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."SerieCTPS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 215.433161180000000000
          Top = 86.929190000000000000
          Width = 94.488250000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Carteira de trabalho:')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 215.433161180000000000
          Top = 98.267780000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."CTPS"]')
          ParentFont = False
          WordWrap = False
        end
        object Line9: TfrxLineView
          Left = 623.622401180000000000
          Top = 34.015770000000000000
          Width = 90.708720000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Shape2: TfrxShapeView
          Left = 3.779481180000000000
          Top = 291.023810000000000000
          Width = 710.551395910000000000
          Height = 219.212740000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo56: TfrxMemoView
          Left = 11.338541180000000000
          Top = 313.700990000000000000
          Width = 695.433520000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[frxDsEndereco."CIDADE"], [FormatDateTime('#39'dd" de "mmmm" de "yyy' +
              'y'#39',<frxDsFPFuncifer."DtSolPecun">)].')
          ParentFont = False
          WordWrap = False
        end
        object Shape3: TfrxShapeView
          Left = 3.779530000000000000
          Top = 154.960730000000000000
          Width = 710.551640000000000000
          Height = 136.063080000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo4: TfrxMemoView
          Left = 11.338590000000000000
          Top = 162.519790000000000000
          Width = 695.433520000000000000
          Height = 75.590600000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haBlock
          Memo.UTF8W = (
            
              'O abaixo assinado requer a concess'#227'o do abono pecuni'#225'rio de 1/3 ' +
              'de suas f'#233'rias, referente ao per'#237'odo aquisitivo de [frxDsFPFunci' +
              'fer."DtIniPA"] a [frxDsFPFuncifer."DtFimPA"] conforme faculta o ' +
              'Artigo 143 da CLT - Consolida'#231#227'o das Leis do Trabalho.')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Top = 529.133846060000000000
          Width = 718.110455910000000000
          Height = 11.338590000000000000
          OnAfterPrint = 'Memo54OnAfterPrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDot
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Dever'#225' ser solicitado 15 dias antes do t'#233'rmino do per'#237'odo aquisi' +
              'tivo.')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          Left = 396.850393700000000000
          Top = 472.441250000000000000
          Width = 279.685039370000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Assinatura do empregador')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 37.795348820000000000
          Top = 472.441250000000000000
          Width = 279.685039370000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Assinatura do funcion'#225'rio')
          ParentFont = False
          WordWrap = False
        end
        object Line4: TfrxLineView
          Left = 3.779530000000000000
          Top = 359.055350000000000000
          Width = 710.551640000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line5: TfrxLineView
          Left = 359.055118110236000000
          Top = 359.055118110000000000
          Height = 136.063080000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Memo6: TfrxMemoView
          Left = 396.850650000000000000
          Top = 377.953000000000000000
          Width = 279.685220000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            
              'Ciente em [FormatDateTime('#39'dd/mm/yyyy'#39',<frxDsFPFuncifer."DtSolPe' +
              'cun">)].')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Left = 3.779530000000000000
          Top = 245.669450000000000000
          Width = 710.551640000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Memo26: TfrxMemoView
          Left = 11.338590000000000000
          Top = 260.787570000000000000
          Width = 695.433520000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'PER'#205'ODO DO ABONO PECUNI'#193'RIO: [frxDsFPFuncifer."DtIniPecun"] a [f' +
              'rxDsFPFuncifer."DtFimPecun"].')
          ParentFont = False
        end
      end
    end
  end
  object frxSolicita1a_13o: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39458.837724780100000000
    ReportOptions.LastChange = 39459.872825520800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Picture1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <ImagemExiste> = True then'
      '  begin'
      '    Picture1.Visible := True;'
      '    Picture1.LoadFromFile(<ImagemCaminho>);'
      '  end else Picture1.Visible := False;'
      'end;'
      ''
      'procedure Line3OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure Memo54OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  Memo54.Visible := False;'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxAvisoFeriasGetValue
    Left = 706
    Top = 86
    Datasets = <
      item
      end
      item
      end
      item
        DataSet = frxDsFPFunci
        DataSetName = 'frxDsFPFunci'
      end
      item
        DataSet = frxDsFPFuncifer
        DataSetName = 'frxDsFPFuncifer'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 540.472436060000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        RowCount = 2
        object Shape1: TfrxShapeView
          Left = 3.779481180000000000
          Top = 34.015770000000000000
          Width = 710.551640000000000000
          Height = 120.944881890000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo1: TfrxMemoView
          Left = 3.779481180000000000
          Top = 34.015770000000000000
          Width = 616.063390000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'SOLICITA'#199#195'O DA 1'#170' PARCELA DO 13'#186' SAL'#193'RIO')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 7.559011180000000000
          Top = 117.165430000000000000
          Width = 419.527830000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsFPFunci."Empresa"] - [frxDsFPFunci."NUMEEMPRESA"]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 623.622401180000000000
          Top = 34.015770000000000000
          Width = 90.708671180000000000
          Height = 120.944911180000000000
          OnBeforePrint = 'Picture1OnBeforePrint'
          ShowHint = False
          Frame.Width = 0.100000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Line1: TfrxLineView
          Left = 623.622401180000000000
          Top = 34.015770000000000000
          Height = 120.944960000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Line2: TfrxLineView
          Left = 3.779481180000000000
          Top = 117.165430000000000000
          Width = 619.842920000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo2: TfrxMemoView
          Left = 7.559011180000000000
          Top = 136.063080000000000000
          Width = 612.283860000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 7.559011180000000000
          Top = 86.929190000000000000
          Width = 41.574830000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'C'#243'digo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 430.866371180000000000
          Top = 117.165430000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CNPJ: [frxDsEndereco."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 7.559011180000000000
          Top = 98.267780000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[FormatFloat('#39'0000'#39',<frxDsFPFunci."Funci">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 113.385851180000000000
          Top = 86.929190000000000000
          Width = 102.047310000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Registro:')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 113.385851180000000000
          Top = 98.267780000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."Registro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 7.559011180000000000
          Top = 56.692950000000000000
          Width = 151.181200000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Nome do funcion'#225'rio:')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 7.559011180000000000
          Top = 68.031540000000000000
          Width = 427.086890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsFPFunci."NUMEFUNCI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 377.952951180000000000
          Top = 86.929190000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'C.B.O.:')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 377.952951180000000000
          Top = 98.267780000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."CBO2002"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 434.645901180000000000
          Top = 86.929190000000000000
          Width = 185.196784490000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Departamento:')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 434.645901180000000000
          Top = 98.267780000000000000
          Width = 185.196784490000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."Depto"] - [frxDsFPFunci."NOMEDEPTO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 434.645901180000000000
          Top = 56.692950000000000000
          Width = 185.196784490000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Fun'#231#227'o:')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 434.645901180000000000
          Top = 68.031540000000000000
          Width = 185.196784490000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."Funcao"] - [frxDsFPFunci."NOMEFUNCAO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 52.913371180000000000
          Top = 86.929190000000000000
          Width = 56.692950000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Identificador:')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 52.913371180000000000
          Top = 98.267780000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[FormatFloat('#39'0000'#39',<frxDsFPFunci."Codigo">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 313.700941180000000000
          Top = 86.929190000000000000
          Width = 60.472480000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'S'#233'rie:')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 313.700941180000000000
          Top = 98.267780000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."SerieCTPS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 215.433161180000000000
          Top = 86.929190000000000000
          Width = 94.488250000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Carteira de trabalho:')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 215.433161180000000000
          Top = 98.267780000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFPFunci."CTPS"]')
          ParentFont = False
          WordWrap = False
        end
        object Line9: TfrxLineView
          Left = 623.622401180000000000
          Top = 34.015770000000000000
          Width = 90.708720000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Shape2: TfrxShapeView
          Left = 3.779481180000000000
          Top = 291.023810000000000000
          Width = 710.551395910000000000
          Height = 219.212740000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo56: TfrxMemoView
          Left = 11.338541180000000000
          Top = 313.700990000000000000
          Width = 695.433520000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEndereco."CIDADE"],  _______/_______/___________')
          ParentFont = False
          WordWrap = False
        end
        object Shape3: TfrxShapeView
          Left = 3.779530000000000000
          Top = 154.960730000000000000
          Width = 710.551640000000000000
          Height = 136.063080000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo4: TfrxMemoView
          Left = 11.338590000000000000
          Top = 162.519790000000000000
          Width = 695.433520000000000000
          Height = 113.385900000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haBlock
          Memo.UTF8W = (
            '     Prezados senhores,'
            '     '
            
              '     Nos termos da legisla'#231#227'o vigente, solicito o pagamento da p' +
              'rimeira parcela do d'#233'cimo terceiro sal'#225'rio, por ocasi'#227'o do gozo ' +
              'das minhas f'#233'rias.'
            '     '
            '     Solicito apor o seu ciente na c'#243'pia desta.')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Top = 529.133846060000000000
          Width = 718.110455910000000000
          Height = 11.338590000000000000
          OnAfterPrint = 'Memo54OnAfterPrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Style = fsDot
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Dever'#225' ser solicitado 15 dias antes do t'#233'rmino do per'#237'odo aquisi' +
              'tivo.')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          Left = 396.850393700000000000
          Top = 472.441250000000000000
          Width = 279.685039370000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Assinatura do empregador')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 37.795348820000000000
          Top = 472.441250000000000000
          Width = 279.685039370000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Assinatura do funcion'#225'rio')
          ParentFont = False
          WordWrap = False
        end
        object Line4: TfrxLineView
          Left = 3.779530000000000000
          Top = 359.055350000000000000
          Width = 710.551640000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Line5: TfrxLineView
          Left = 359.055118110236000000
          Top = 359.055118110000000000
          Height = 136.063080000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Memo6: TfrxMemoView
          Left = 396.850650000000000000
          Top = 377.953000000000000000
          Width = 279.685220000000000000
          Height = 34.015770000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Ciente em _______/_______/___________')
          ParentFont = False
        end
      end
    end
  end
  object QrFPFunciCal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fuc.*, tpc.Descricao NOMETIPOCALC '
      'FROM fpfuncical fuc'
      'LEFT JOIN fptpcal tpc ON tpc.Codigo=fuc.TipoCalc'
      'WHERE fuc.Controle=:P0')
    Left = 744
    Top = 88
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFPFunciCalControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFPFunciCalTipoCalc: TIntegerField
      FieldName = 'TipoCalc'
      Required = True
    end
    object QrFPFunciCalLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFPFunciCalDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFPFunciCalDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFPFunciCalUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFPFunciCalUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFPFunciCalNOMETIPOCALC: TWideStringField
      FieldName = 'NOMETIPOCALC'
      Size = 35
    end
  end
  object DsFPFunciCal: TDataSource
    DataSet = QrFPFunciCal
    Left = 772
    Top = 88
  end
  object QrFPFunciHor: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrFPFunciHorAfterOpen
    BeforeClose = QrFPFunciHorBeforeClose
    OnCalcFields = QrFPFunciHorCalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM fpfuncihor'
      'WHERE Codigo=:P0')
    Left = 744
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFPFunciHorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPFunciHorControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFPFunciHorTur1Ent: TTimeField
      FieldName = 'Tur1Ent'
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciHorTur1DeI: TTimeField
      FieldName = 'Tur1DeI'
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciHorTur1DeF: TTimeField
      FieldName = 'Tur1DeF'
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciHorTur1Sai: TTimeField
      FieldName = 'Tur1Sai'
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciHorTur1HrT: TTimeField
      FieldName = 'Tur1HrT'
      DisplayFormat = 'hh:nn:ss'
    end
    object QrFPFunciHorTur1HrN: TTimeField
      FieldName = 'Tur1HrN'
      DisplayFormat = 'hh:nn:ss'
    end
    object QrFPFunciHorTur1HrS: TTimeField
      FieldName = 'Tur1HrS'
      DisplayFormat = 'hh:nn:ss'
    end
    object QrFPFunciHorTur1HrA: TTimeField
      FieldName = 'Tur1HrA'
      DisplayFormat = 'hh:nn:ss'
    end
    object QrFPFunciHorTur1HrB: TTimeField
      FieldName = 'Tur1HrB'
      DisplayFormat = 'hh:nn:ss'
    end
    object QrFPFunciHorTur2Ent: TTimeField
      FieldName = 'Tur2Ent'
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciHorTur1HrE: TTimeField
      FieldName = 'Tur1HrE'
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciHorTur1HrC: TTimeField
      FieldName = 'Tur1HrC'
    end
    object QrFPFunciHorTur2DeI: TTimeField
      FieldName = 'Tur2DeI'
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciHorTur2DeF: TTimeField
      FieldName = 'Tur2DeF'
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciHorTur2Sai: TTimeField
      FieldName = 'Tur2Sai'
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciHorTur2HrT: TTimeField
      FieldName = 'Tur2HrT'
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciHorTur2HrN: TTimeField
      FieldName = 'Tur2HrN'
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciHorTur2HrS: TTimeField
      FieldName = 'Tur2HrS'
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciHorTur2HrA: TTimeField
      FieldName = 'Tur2HrA'
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciHorTur2HrB: TTimeField
      FieldName = 'Tur2HrB'
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciHorTur2HrE: TTimeField
      FieldName = 'Tur2HrE'
      DisplayFormat = 'hh:nn'
    end
    object QrFPFunciHorTur2HrC: TTimeField
      FieldName = 'Tur2HrC'
    end
    object QrFPFunciHorLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFPFunciHorDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFPFunciHorDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFPFunciHorUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFPFunciHorUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFPFunciHorTXT1HrT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT1HrT'
      Size = 8
      Calculated = True
    end
    object QrFPFunciHorTXT1HrN: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT1HrN'
      Size = 8
      Calculated = True
    end
    object QrFPFunciHorTXT1HrS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT1HrS'
      Size = 8
      Calculated = True
    end
    object QrFPFunciHorTXT1HrA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT1HrA'
      Size = 8
      Calculated = True
    end
    object QrFPFunciHorTXT1HrB: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT1HrB'
      Size = 8
      Calculated = True
    end
    object QrFPFunciHorTXT1HrE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT1HrE'
      Size = 8
      Calculated = True
    end
    object QrFPFunciHorTXT1HrC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT1HrC'
      Size = 8
      Calculated = True
    end
  end
  object DsFPFunciHor: TDataSource
    DataSet = QrFPFunciHor
    Left = 772
    Top = 60
  end
  object QrFPFunciTre: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrFPFunciTreAfterOpen
    BeforeClose = QrFPFunciTreBeforeClose
    SQL.Strings = (
      'SELECT *'
      'FROM fpfuncitre'
      'WHERE Codigo=:P0')
    Left = 744
    Top = 116
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFPFunciTreCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFPFunciTreControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFPFunciTreNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrFPFunciTreCargaHoras: TFloatField
      FieldName = 'CargaHoras'
    end
    object QrFPFunciTreDataIni: TDateField
      FieldName = 'DataIni'
    end
    object QrFPFunciTreDataFim: TDateField
      FieldName = 'DataFim'
    end
    object QrFPFunciTrePathCertif: TWideStringField
      FieldName = 'PathCertif'
      Size = 255
    end
  end
  object DsFPFunciTre: TDataSource
    DataSet = QrFPFunciTre
    Left = 772
    Top = 116
  end
end
