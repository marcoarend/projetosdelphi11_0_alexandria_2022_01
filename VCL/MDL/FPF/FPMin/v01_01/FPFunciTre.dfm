object FmFPFunciTre: TFmFPFunciTre
  Left = 339
  Top = 185
  Caption = 'FPG-FUNCI-003 :: Treinamentos de Funcion'#225'rio'
  ClientHeight = 304
  ClientWidth = 647
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 647
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 599
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 551
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 351
        Height = 32
        Caption = 'Treinamentos de Funcion'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 351
        Height = 32
        Caption = 'Treinamentos de Funcion'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 351
        Height = 32
        Caption = 'Treinamentos de Funcion'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 647
    Height = 142
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 647
      Height = 142
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 647
        Height = 142
        Align = alClient
        TabOrder = 0
        object Label1: TLabel
          Left = 12
          Top = 12
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label2: TLabel
          Left = 12
          Top = 52
          Width = 66
          Height = 13
          Caption = 'Carga hor'#225'ria:'
        end
        object Label3: TLabel
          Left = 116
          Top = 52
          Width = 55
          Height = 13
          Caption = 'Data inicial:'
        end
        object Label4: TLabel
          Left = 232
          Top = 52
          Width = 48
          Height = 13
          Caption = 'Data final:'
        end
        object Label5: TLabel
          Left = 12
          Top = 92
          Width = 106
          Height = 13
          Caption = 'Arquivo do certificado:'
        end
        object SpeedButton1: TSpeedButton
          Left = 608
          Top = 108
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object CkContinuar: TCheckBox
          Left = 8
          Top = 224
          Width = 213
          Height = 17
          Caption = 'Continuar inserindo ap'#243's a confirma'#231#227'o.'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object EdNome: TdmkEdit
          Left = 12
          Top = 28
          Width = 621
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCargaHoras: TdmkEdit
          Left = 12
          Top = 68
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'CargaHoras'
          UpdCampo = 'CargaHoras'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object TPDataIni: TdmkEditDateTimePicker
          Left = 116
          Top = 68
          Width = 112
          Height = 21
          Date = 41058.735317326390000000
          Time = 41058.735317326390000000
          TabOrder = 3
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DataIni'
          UpdCampo = 'DataIni'
          UpdType = utYes
        end
        object TPDataFim: TdmkEditDateTimePicker
          Left = 232
          Top = 68
          Width = 112
          Height = 21
          Date = 41058.735373634260000000
          Time = 41058.735373634260000000
          TabOrder = 4
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DataFim'
          UpdCampo = 'DataFim'
          UpdType = utYes
        end
        object EdPathCertif: TdmkEdit
          Left = 12
          Top = 108
          Width = 593
          Height = 21
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'PathCertif'
          UpdCampo = 'PathCertif'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 190
    Width = 647
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 643
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 234
    Width = 647
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 501
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 499
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
end
