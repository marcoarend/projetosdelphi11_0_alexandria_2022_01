unit FPFunciTre;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask;

type
  TFmFPFunciTre = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    CkContinuar: TCheckBox;
    EdNome: TdmkEdit;
    Label1: TLabel;
    EdCargaHoras: TdmkEdit;
    Label2: TLabel;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdPathCertif: TdmkEdit;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FQryFPFunciTre: TmySQLQuery;
    FCodiAtual, FCtrlAtual: Integer;
  end;

  var
  FmFPFunciTre: TFmFPFunciTre;

implementation

uses UnMyObjects, Module, UMySQLModule;

{$R *.DFM}

procedure TFmFPFunciTre.BtOKClick(Sender: TObject);
var
  Nome, DataIni, DataFim, PathCertif: String;
  Codigo, Controle: Integer;
  CargaHoras: Double;
begin
  Codigo         := FCodiAtual;
  Controle       := FCtrlAtual;
  Nome           := EdNome.Text;
  CargaHoras     := EdCargaHoras.ValueVariant;
  DataIni        := Geral.FDT(TPDataIni.Date, 1);
  DataFim        := Geral.FDT(TPDataIni.Date, 1);
  PathCertif     := EdPathCertif.Text;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('fpfuncitre', 'Controle', ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'fpfuncitre', False, [
  'Codigo', 'Nome', 'CargaHoras',
  'DataIni', 'DataFim', 'PathCertif'], [
  'Controle'], [
  Codigo, Nome, CargaHoras,
  DataIni, DataFim, PathCertif], [
  Controle], True) then
  begin
    if FQryFPFunciTre <> nil then
    begin
      FQryFPFunciTre.Close;
      FQryFPFunciTre.Open;
      FQryFPFunciTre.Locate('Controle', Controle, []);
    end;
    if CkContinuar.Checked then
    begin
      EdNome.Text := '';
      EdCargaHoras.ValueVariant := 0;
      TPDataIni.Date := 0;
      TPDataFim.Date := 0;
      EdPathCertif.Text := '';
      //
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmFPFunciTre.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPFunciTre.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPFunciTre.FormCreate(Sender: TObject);
begin
  TPDataIni.Date := 0;
  TPDataFim.Date := 0;
end;

procedure TFmFPFunciTre.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFPFunciTre.SpeedButton1Click(Sender: TObject);
begin
  MyObjects.DefineArquivo1(Self, EdPathCertif);
end;

end.
