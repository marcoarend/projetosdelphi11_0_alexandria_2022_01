unit FPMin_Tabs;
{ Colocar no MyListas:

Uses FPMin_Tabs;


//
function TMyListas.CriaListaTabelas(:
      UnFPMin_Tabs.CarregaListaTabelas(Lista);
//
function TMyListas.CriaListaSQL:
    UnFPMin_Tabs.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:
    UnFPMin_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      UnFPMin_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      UnFPMin_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    UnFPMin_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  UnFPMin_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms, UnInternalConsts, dmkGeral, UnDmkEnums;

type
  TFPMin_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(TabelaBase: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  UnFPMin_Tabs: TFPMin_Tabs;

implementation

uses UMySQLModule;

function TFPMin_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
{
  if Uppercase(Tabela) = Uppercase('Controle') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo');
    FListaSQL.Add('1');
  end else
  if Uppercase(Tabela) = Uppercase('OpcoesGerl') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo|SloganFoot');
    FListaSQL.Add('1|"O justo viver� por f�! Rm 1.17"');
  end else
  if Uppercase(Tabela) = Uppercase('Senhas') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Numero|Login');
    FListaSQL.Add('0|""');
  end;
}
end;

function TFPMin_Tabs.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  try
    MyLinguas.AdTbLst(Lista, False, Lowercase('FPFunca'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('FPFunci'), '');
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TFPMin_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('') then
    begin
    {
    end else
    if Uppercase(Tabela) = Uppercase('ProtocoOco') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"Lan�amentos"');
    }
    end else
    ;
  except
    raise;
    Result := False;
  end;
end;


function TFPMin_Tabs.CarregaListaFRIndices(TabelaBase: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  //
  if Uppercase(TabelaBase) = Uppercase('FPFunca') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'CodID';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('FPFunci') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  ;
end;

function TFPMin_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    if Uppercase(Tabela) = Uppercase('FPFunca') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'CodID';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??????';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('FPFunci') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Codigo do funcionario dentro da empresa (informativo)
      New(FRCampos);
      FRCampos.Field      := 'Funci';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // N�mero do registro do funcion�rio no livro de empregados
      New(FRCampos);
      FRCampos.Field      := 'Registro';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Departamento em que o funcion�rio trabalha
      New(FRCampos);
      FRCampos.Field      := 'Depto';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Categoria de acordo com a periodicidade de pagamento
      New(FRCampos);
      FRCampos.Field      := 'Categoria';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Fun��o do Funcion�rio dentro da empresa
      New(FRCampos);
      FRCampos.Field      := 'Funcao';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Cargo ou Ocupa��o conforme a RAIS ( C.B.O. 94)
      New(FRCampos);
      FRCampos.Field      := 'Cargo';
      FRCampos.Tipo       := 'varchar(5)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // C�digo de Ocupa��o conforme a CBO2002
      New(FRCampos);
      FRCampos.Field      := 'CBO2002';
      FRCampos.Tipo       := 'varchar(7)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '??????';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Tipo de v�nculo empregat�cio conforme a RAIS
      New(FRCampos);
      FRCampos.Field      := 'Vinculo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Indicador de alvar� para o caso de menor n�o aprendiz conforme a RAIS
      New(FRCampos);
      FRCampos.Field      := 'Alvara';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Situa��o do funcion�rio conforme a RAIS
      New(FRCampos);
      FRCampos.Field      := 'Situacao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Tipo de admiss�o do funcion�rio conforme a RAIS
      New(FRCampos);
      FRCampos.Field      := 'Admissao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Data de admiss�o do funcion�rio
      New(FRCampos);
      FRCampos.Field      := 'DataAdm';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // N�mero de horas semanais do funcion�rio
      New(FRCampos);
      FRCampos.Field      := 'HrSeman';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Tipo de Sal�rio recebido pelo funcion�rio
      New(FRCampos);
      FRCampos.Field      := 'TipoSal';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Salario';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // N�mero de filhos para efeito de sal�rio fam�lia
      New(FRCampos);
      FRCampos.Field      := 'Filhos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // N�mero de dependentes para efeito de imposto de renda
      New(FRCampos);
      FRCampos.Field      := 'Dependent';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Percentual para a pens�o aliment�cia, se houver
      New(FRCampos);
      FRCampos.Field      := 'PensaoAlm';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Calcular a pens�o sobre o sal�rio l�quido?
      // Lembrando que o rendimento l�quido � o resultado do Rendimento Bruto
      // deduzido todos os descontos legais, tais como:
      // INSS, IRRF, Contribui��es: Sindical, Assistencial, Confederativa, etc,
      //e outros determinados na Conven��o/Acordo Coletivo da categoria.
      New(FRCampos);
      FRCampos.Field      := 'PensaoLiq';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // COMO CALCULAR O IRRF
      // Se o empregador efetua adiantamentos no m�s trabalhado e quita a folha
      // de pagamento somente no m�s seguinte, o Imposto de Renda na Fonte
      // incidir� sobre o somat�rio dos valores dos adiantamentos e dos demais
      // pagamentos efetuados dentro do m�s.
      //
      // 0: Dentro do m�s (quando o adiantamento e a quita��o s�o dentro do m�s).
      // 1: Quita no m�s seguinte ao da compet�ncia como explicado acima.
      // 2: Quita depois mas calcula o IRRF pela compet�ncia (incorreto).
      New(FRCampos);
      FRCampos.Field      := 'ComoCalcIR';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Banco para dep�sito do sal�rio
      New(FRCampos);
      FRCampos.Field      := 'BancoSal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // N�mero da conta corrente para dep�sito do sal�rio
      New(FRCampos);
      FRCampos.Field      := 'ContaSal';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Indica se o funcion�rio � ou n�o optante do FGTS
      New(FRCampos);
      FRCampos.Field      := 'OptaFGTS';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Banco para dep�sito do FGTS
      New(FRCampos);
      FRCampos.Field      := 'BancoFGTS';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // N�mero da conta corrente para dep�sito do FGTS
      New(FRCampos);
      FRCampos.Field      := 'ContaFGTS';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Data em que o funcion�rio fez a op��o pelo FGTS
      New(FRCampos);
      FRCampos.Field      := 'DataFGTS';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Data do primeiro exame m�dico
      New(FRCampos);
      FRCampos.Field      := 'DataExame';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Per�odo de tempo em meses para o pr�ximo exame m�dico
      New(FRCampos);
      FRCampos.Field      := 'PerExame';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Data do afastamento do funcion�rio
      New(FRCampos);
      FRCampos.Field      := 'DataIniAf';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Data do retorno do afastamento do funcion�rio
      New(FRCampos);
      FRCampos.Field      := 'DataFinAf';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // N�mero de meses no ano base em que o funcion�rio ficou afastado
      New(FRCampos);
      FRCampos.Field      := 'MesesAf';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Data do aviso pr�vio
      New(FRCampos);
      FRCampos.Field      := 'DataAviso';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Indica se o aviso pr�vio ser� indenizado pela empresa (se o tipo de demiss�o permitir)
      New(FRCampos);
      FRCampos.Field      := 'AvisoInden';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Dias que o funcion�rio cumpriu de aviso pr�vio
      New(FRCampos);
      FRCampos.Field      := 'DiasAviso';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Data de demiss�o do funcion�rio
      New(FRCampos);
      FRCampos.Field      := 'DataDemiss';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Motivo para a demiss�o do funcion�rio conforme a RAIS
      New(FRCampos);
      FRCampos.Field      := 'Demissao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // C�digo de afastamento do funcion�rio conforme a CAIXA para Rescis�o
      New(FRCampos);
      FRCampos.Field      := 'CodigoAf';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Descri��o do motivo de afastamento conforme a CAIXA para Rescis�o
      New(FRCampos);
      FRCampos.Field      := 'MotivoAf';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Obs';
      FRCampos.Tipo       := 'mediumtext';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Indica se o funcion�rio ir� participar da rela��o da GFIP
      New(FRCampos);
      FRCampos.Field      := 'GFIP';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Indica se o funcion�rio ir� participar da rela��o da RAIS
      New(FRCampos);
      FRCampos.Field      := 'RAIS';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Indica se o c�lculo deve usar o valor de 4 semanas e meia
      // para as refer�ncias de semana ao inv�s de 5.
      New(FRCampos);
      FRCampos.Field      := 'Professor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // Nacionalidade
      New(FRCampos);
      FRCampos.Field      := 'Nacionalid';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '10';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Ano em que o funcion�rio chegou ao Pa�s (apenas para estrangeiros).
      New(FRCampos);
      FRCampos.Field      := 'AnoChegada';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Grau de instru��o
      New(FRCampos);
      FRCampos.Field      := 'GrauInstru';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstCivil';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // N�mero da carteira de trabalho do funcion�rio
      New(FRCampos);
      FRCampos.Field      := 'CTPS';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // S�rie da carteira de trabalho do funcion�rio
      New(FRCampos);
      FRCampos.Field      := 'SerieCTPS';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // N�mero de inscri��o no P.I.S.
      New(FRCampos);
      FRCampos.Field      := 'PIS';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // N�mero do t�tulo de eleitor
      New(FRCampos);
      FRCampos.Field      := 'TitEleitor';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // N�mero da carteira de reservista
      New(FRCampos);
      FRCampos.Field      := 'Reservista';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Informa��o sobre a ra�a e cor do funcion�rio de acordo com a RAIS
      New(FRCampos);
      FRCampos.Field      := 'Raca';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Indica se o funcion�rio � portador de defici�ncia f�sica de acordo com a RAIS
      New(FRCampos);
      FRCampos.Field      := 'Deficiente';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Foto';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // C�digo do Munic�pio onde o trabalhador exerce atividade,
      // caso n�o for o mesmo mun�cipio onde est� localizada a empresa.
      New(FRCampos);
      FRCampos.Field      := 'LocalTrab';
      FRCampos.Tipo       := 'varchar(7)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Insalubrid';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Periculosi';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // % a mais que 100 nas horas extras de semana
      New(FRCampos);
      FRCampos.Field      := 'HEFator';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '50.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Adicional noturno: 0 - Urbano, 1 - Agr�cola, 2- Pecu�ria, 3- Manual
      //                    22:00-05:00 21:00-05:00   20:00-04:00
      //                    Hora 52,5 min        60 min
      New(FRCampos);
      FRCampos.Field      := 'ANTipo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Hora inicial do adicional noturno Manual
      New(FRCampos);
      FRCampos.Field      := 'ANIniHr';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '22:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Hora final do adicional noturno Manual
      New(FRCampos);
      FRCampos.Field      := 'ANFimHr';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '05:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // tempo da hora noturna Manual
      New(FRCampos);
      FRCampos.Field      := 'ANMinHr';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '00:52:30';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // % de adicional noturno
      New(FRCampos);
      FRCampos.Field      := 'AdiciNotur';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '25.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Lei que obriga o empregador pagar a mais quando n�o concede descanso no
      // turno de trabalho
      New(FRCampos);
      FRCampos.Field      := 'Lei8923_94';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '50.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // Primeiro dia do cart�o ponto
      New(FRCampos);
      FRCampos.Field      := 'PriDdPonto';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // zero quando n�o controla ponto
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // N�mero da chapa do funcion�rio (cart�o ponto?)
      New(FRCampos);
      FRCampos.Field      := 'Chapa';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    ;
  except
    raise;
    Result := False;
  end;
end;

function TFPMin_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
{
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      New(FRCampos);
      FRCampos.Field      := 'Perfis';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Senhas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    ;
}
  except
    raise;
    Result := False;
  end;
end;

function TFPMin_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  (* Estas janelas j� s�o criadas no FPMax_Tabs*)
  //
  // FPG-FUNCI-001 :: Funcion�rios
  New(FRJanelas);
  FRJanelas.ID        := 'FPG-FUNCI-001';
  FRJanelas.Nome      := 'FmFPFunci';
  FRJanelas.Descricao := 'Funcion�rios';
  FLJanelas.Add(FRJanelas);
  //
  // FPG-FUNCI-002 :: Cadastro de Funcion�rio
  New(FRJanelas);
  FRJanelas.ID        := 'FPG-FUNCI-002';
  FRJanelas.Nome      := 'FmFPFunciNew';
  FRJanelas.Descricao := 'Cadastro de Funcion�rio';
  FLJanelas.Add(FRJanelas);
  //
  // FPG-FUNCI-003 :: Fun��es de Funcion�rios
  New(FRJanelas);
  FRJanelas.ID        := 'FPG-FUNCI-003';
  FRJanelas.Nome      := 'FmFPFunca';
  FRJanelas.Descricao := 'Fun��es de Funcion�rios';
  FLJanelas.Add(FRJanelas);
  //

  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
