unit FPFunca;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, UnDmkProcFunc,
  dmkDBGridDAC, Db, mySQLDbTables, dmkDBGrid, dmkImage, UnDmkEnums;

type
  TFmFPFunca = class(TForm)
    Panel1: TPanel;
    TbFPFunca: TmySQLTable;
    TbFPFuncaCodID: TWideStringField;
    TbFPFuncaDescricao: TWideStringField;
    DsFPFunca: TDataSource;
    DBGrid1: TDBGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TbFPFuncaBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmFPFunca: TFmFPFunca;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmFPFunca.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFPFunca.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPFunca.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFPFunca.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TbFPFunca.Open;
end;

procedure TFmFPFunca.TbFPFuncaBeforePost(DataSet: TDataSet);
begin
  TbFPFuncaCodID.Value := dmkPF.SoNumeroELetraESeparadores_TT(TbFPFuncaCodID.Value);
end;

end.
