unit FPFunci;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  dmkEdit, Grids, DBGrids, frxClass, Menus, frxDBSet, CheckLst, Variants,
  dmkDBLookupComboBox, dmkEditCB, dmkGeral, UnDmkProcFunc, ShellAPI,
  dmkImage, UnDmkEnums;

type
  TFmFPFunci = class(TForm)
    PainelDados: TPanel;
    DsFPFunci: TDataSource;
    QrFPFunci: TmySQLQuery;
    PainelEdita: TPanel;
    PnEditTop: TPanel;
    PnDataTop: TPanel;
    PnInclui: TPanel;
    Panel8: TPanel;
    Label2: TLabel;
    EdCodigo: TdmkEditCB;
    CBCodigo: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrFunci: TmySQLQuery;
    DsFunci: TDataSource;
    QrFunciCodigo: TIntegerField;
    QrFunciNOMEFUNCI: TWideStringField;
    Label5: TLabel;
    Label6: TLabel;
    EdFunci: TdmkEdit;
    QrMax: TmySQLQuery;
    QrMaxFunci: TIntegerField;
    QrPesq1: TmySQLQuery;
    QrFPFunciEmpresa: TIntegerField;
    QrFPFunciFunci: TIntegerField;
    QrFPFunciRegistro: TWideStringField;
    QrFPFunciCategoria: TIntegerField;
    QrFPFunciCBO2002: TWideStringField;
    QrFPFunciVinculo: TIntegerField;
    QrFPFunciAlvara: TSmallintField;
    QrFPFunciSituacao: TIntegerField;
    QrFPFunciAdmissao: TIntegerField;
    QrFPFunciDataAdm: TDateField;
    QrFPFunciHrSeman: TIntegerField;
    QrFPFunciTipoSal: TSmallintField;
    QrFPFunciSalario: TFloatField;
    QrFPFunciFilhos: TIntegerField;
    QrFPFunciDependent: TIntegerField;
    QrFPFunciBancoSal: TIntegerField;
    QrFPFunciContaSal: TWideStringField;
    QrFPFunciOptaFGTS: TSmallintField;
    QrFPFunciBancoFGTS: TIntegerField;
    QrFPFunciContaFGTS: TWideStringField;
    QrFPFunciDataFGTS: TDateField;
    QrFPFunciDataExame: TDateField;
    QrFPFunciPerExame: TIntegerField;
    QrFPFunciDataIniAf: TDateField;
    QrFPFunciDataFinAf: TDateField;
    QrFPFunciMesesAf: TIntegerField;
    QrFPFunciDataAviso: TDateField;
    QrFPFunciAvisoInden: TSmallintField;
    QrFPFunciDiasAviso: TIntegerField;
    QrFPFunciDataDemiss: TDateField;
    QrFPFunciDemissao: TIntegerField;
    QrFPFunciCodigoAf: TWideStringField;
    QrFPFunciMotivoAf: TWideStringField;
    QrFPFunciObs: TWideMemoField;
    QrFPFunciGFIP: TSmallintField;
    QrFPFunciRAIS: TSmallintField;
    QrFPFunciProfessor: TSmallintField;
    QrFPFunciAtivo: TSmallintField;
    QrFPFunciLk: TIntegerField;
    QrFPFunciDataCad: TDateField;
    QrFPFunciDataAlt: TDateField;
    QrFPFunciUserCad: TIntegerField;
    QrFPFunciUserAlt: TIntegerField;
    QrFPFunciCodigo: TIntegerField;
    QrFPFunciNUMEFUNCI: TWideStringField;
    QrFPFunciNUMEEMPRESA: TWideStringField;
    PainelEdit: TPanel;
    QrFPFunca: TmySQLQuery;
    DsFPFunca: TDataSource;
    QrFPFuncaCodID: TWideStringField;
    QrFPFuncaDescricao: TWideStringField;
    QrFPDepto: TmySQLQuery;
    QrFPDeptoCodID: TWideStringField;
    QrFPDeptoDescricao: TWideStringField;
    DsFPDepto: TDataSource;
    QrFPFunciDepto: TWideStringField;
    QrFPFunciFuncao: TWideStringField;
    QrFPCBO02: TmySQLQuery;
    DsFPCBO02: TDataSource;
    QrFPCBO02CodID: TWideStringField;
    QrFPCBO02Descricao: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel4: TPanel;
    Label7: TLabel;
    EdFuncao: TdmkEditCB;
    Label9: TLabel;
    EdCBO2002: TdmkEditCB;
    CBCBO2002: TdmkDBLookupComboBox;
    CBFuncao: TdmkDBLookupComboBox;
    EdDepto: TdmkEditCB;
    Label8: TLabel;
    CBDepto: TdmkDBLookupComboBox;
    PainelData: TPanel;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    Panel10: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    QrFPFunciNOMEFUNCAO: TWideStringField;
    QrFPFunciNOMEDEPTO: TWideStringField;
    QrFPFunciNOMECBO2002: TWideStringField;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    TabSheet3: TTabSheet;
    Label13: TLabel;
    EdRegistro: TdmkEdit;
    Label14: TLabel;
    EdCategoria: TdmkEditCB;
    CBCategoria: TdmkDBLookupComboBox;
    QrFPCateg: TmySQLQuery;
    DsFPCateg: TDataSource;
    QrFPCategCodigo: TIntegerField;
    QrFPCategDescricao: TWideStringField;
    Label15: TLabel;
    Label16: TLabel;
    DBEdit7: TDBEdit;
    QrFPFunciNOMECATEG: TWideStringField;
    DBEdit8: TDBEdit;
    QrPesq1Codigo: TIntegerField;
    TabSheet4: TTabSheet;
    Label17: TLabel;
    EdVinculo: TdmkEditCB;
    CBVinculo: TdmkDBLookupComboBox;
    CkAlvara: TCheckBox;
    QrFPVincu: TmySQLQuery;
    DsFPVincu: TDataSource;
    QrFPVincuCodigo: TIntegerField;
    QrFPVincuDescricao: TWideStringField;
    QrFPFunciNOMEVINCU: TWideStringField;
    Label19: TLabel;
    EdSituacao: TdmkEditCB;
    CBSituacao: TdmkDBLookupComboBox;
    Label20: TLabel;
    EdAdmissao: TdmkEditCB;
    CBAdmissao: TdmkDBLookupComboBox;
    Label21: TLabel;
    QrFPAdmis: TmySQLQuery;
    DsFPAdmis: TDataSource;
    QrFPSitua: TmySQLQuery;
    DsFPSitua: TDataSource;
    QrFPAdmisCodigo: TIntegerField;
    QrFPAdmisDescricao: TWideStringField;
    QrFPSituaCodigo: TIntegerField;
    QrFPSituaDescricao: TWideStringField;
    QrFPFunciNOMEADMIS: TWideStringField;
    QrFPFunciNOMESITUA: TWideStringField;
    Label26: TLabel;
    DBEdit15: TDBEdit;
    Label22: TLabel;
    dmkEdHrSeman: TdmkEdit;
    CkProfessor: TCheckBox;
    Label27: TLabel;
    EdTipoSal: TdmkEditCB;
    CBTipoSal: TdmkDBLookupComboBox;
    QrFPDeslg: TmySQLQuery;
    DsFPDeslg: TDataSource;
    dmkEdSalario: TdmkEdit;
    Label28: TLabel;
    Label29: TLabel;
    dmkEdFilhos: TdmkEdit;
    dmkEdDependent: TdmkEdit;
    Label30: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    QrFPFunciNOMETPSAL: TWideStringField;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    dmkEdPensaoAlm: TdmkEdit;
    Label31: TLabel;
    dmkEdBancoSal: TdmkEdit;
    Label36: TLabel;
    EdContaSal: TEdit;
    Label37: TLabel;
    Label38: TLabel;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    QrFPFunciPensaoAlm: TFloatField;
    TabSheet5: TTabSheet;
    QrFPDeslgCodigo: TIntegerField;
    QrFPDeslgDescricao: TWideStringField;
    CkRAIS: TCheckBox;
    CkGFIP: TCheckBox;
    QrFPTpSal: TmySQLQuery;
    DsFPTpSal: TDataSource;
    QrFPTpSalCodigo: TIntegerField;
    QrFPTpSalDescricao: TWideStringField;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    TabSheet9: TTabSheet;
    EdNacionalid: TdmkEditCB;
    Label59: TLabel;
    CBNacionalid: TdmkDBLookupComboBox;
    QrFPNacio: TmySQLQuery;
    QrFPNacioCodigo: TIntegerField;
    QrFPNacioDescricao: TWideStringField;
    DsFPNacio: TDataSource;
    dmkEdAnoChegada: TdmkEdit;
    LaAnoChegada: TLabel;
    QrFPFunciNacionalid: TIntegerField;
    QrFPFunciAnoChegada: TIntegerField;
    QrFPFunciGrauInstru: TIntegerField;
    QrFPFunciEstCivil: TIntegerField;
    QrFPFunciCTPS: TWideStringField;
    QrFPFunciSerieCTPS: TWideStringField;
    QrFPFunciPIS: TWideStringField;
    QrFPFunciTitEleitor: TWideStringField;
    QrFPFunciReservista: TWideStringField;
    QrFPFunciRaca: TIntegerField;
    QrFPFunciDeficiente: TSmallintField;
    QrFPFunciFoto: TWideStringField;
    QrFPFunciCargo: TWideStringField;
    QrFPFunciLocalTrab: TWideStringField;
    Label60: TLabel;
    EdGrauInstru: TdmkEditCB;
    CBGrauInstru: TdmkDBLookupComboBox;
    QrFPGrauI: TmySQLQuery;
    DsFPGrauI: TDataSource;
    QrFPGrauICodigo: TIntegerField;
    QrFPGrauIDescricao: TWideStringField;
    Label61: TLabel;
    EdEstCivil: TdmkEditCB;
    CBEstCivil: TdmkDBLookupComboBox;
    QrFPCivil: TmySQLQuery;
    DsFPCivil: TDataSource;
    QrFPCivilCodigo: TIntegerField;
    QrFPCivilDescricao: TWideStringField;
    Label67: TLabel;
    EdRaca: TdmkEditCB;
    CBRaca: TdmkDBLookupComboBox;
    QrFPRacas: TmySQLQuery;
    DsFPRacas: TDataSource;
    QrFPRacasCodigo: TIntegerField;
    QrFPRacasDescricao: TWideStringField;
    Label68: TLabel;
    EdCBO1994: TdmkEditCB;
    CBCBO1994: TdmkDBLookupComboBox;
    QrFPCBO94: TmySQLQuery;
    DsFPCBO94: TDataSource;
    QrFPCBO94CodID: TWideStringField;
    QrFPCBO94Descricao: TWideStringField;
    Label69: TLabel;
    EdLocalTrab: TdmkEditCB;
    CBLocalTrab: TdmkDBLookupComboBox;
    QrFPMunic: TmySQLQuery;
    DsFPMunic: TDataSource;
    QrFPMunicCodID: TWideStringField;
    QrFPMunicNome: TWideStringField;
    QrFPMunicFaixaCEP: TWideStringField;
    QrFPMunicAtivo: TSmallintField;
    Label70: TLabel;
    EdFoto: TEdit;
    SpeedButton5: TSpeedButton;
    OpenPictureDialog1: TOpenPictureDialog;
    Label71: TLabel;
    dmkEdInsalubrid: TdmkEdit;
    Label72: TLabel;
    dmkEdPericulosi: TdmkEdit;
    QrFPFunciInsalubrid: TFloatField;
    QrFPFunciPericulosi: TFloatField;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    Label75: TLabel;
    Label76: TLabel;
    Label73: TLabel;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    QrFPFunciNOMECARGO: TWideStringField;
    QrFPFunciNOMENACIONALID: TWideStringField;
    QrFPFunciNOMEECIVIL: TWideStringField;
    QrFPFunciNOMERACA: TWideStringField;
    QrFPFunciNOMEGRAUI: TWideStringField;
    QrFPFunciNOMELOCTRAB: TWideStringField;
    TabSheet11: TTabSheet;
    TabSheet12: TTabSheet;
    TabSheet13: TTabSheet;
    Panel11: TPanel;
    Panel12: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit9: TDBEdit;
    LaFotoDB: TLabel;
    Image3: TImage;
    TabSheet14: TTabSheet;
    Label88: TLabel;
    Label89: TLabel;
    Label90: TLabel;
    Label91: TLabel;
    Label92: TLabel;
    EdCTPS: TEdit;
    EdSerieCTPS: TEdit;
    EdPIS: TEdit;
    EdTitEleitor: TEdit;
    EdReservista: TEdit;
    GroupBox2: TGroupBox;
    Label40: TLabel;
    Label41: TLabel;
    CkOptaFGTS: TCheckBox;
    dmkEdBancoFGTS: TdmkEdit;
    EdContaFGTS: TEdit;
    GroupBox3: TGroupBox;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    dmkEdPerExame: TdmkEdit;
    GroupBox4: TGroupBox;
    Label46: TLabel;
    Label47: TLabel;
    Label49: TLabel;
    dmkEdMesesAf: TdmkEdit;
    CkDeficiente: TCheckBox;
    Panel13: TPanel;
    LaFotoEd: TLabel;
    Panel14: TPanel;
    Label3: TLabel;
    DBEdit001: TDBEdit;
    DBEdit002: TDBEdit;
    DBEdit10: TDBEdit;
    Image2: TImage;
    TabSheet15: TTabSheet;
    Panel15: TPanel;
    QrFPFunciEve: TmySQLQuery;
    DsFPFunciEve: TDataSource;
    PnEventos: TPanel;
    Panel17: TPanel;
    LaTipoEve: TLabel;
    QrEventos: TmySQLQuery;
    DsEventos: TDataSource;
    QrEventosCodigo: TIntegerField;
    QrEventosDescricao: TWideStringField;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    Panel18: TPanel;
    Panel16: TPanel;
    RGIncidencia: TRadioGroup;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    PnEveConf: TPanel;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    PnEveEdit: TPanel;
    BtExcluiIts: TBitBtn;
    BtAlteraIts: TBitBtn;
    BtIncluiIts: TBitBtn;
    QrFPFunciEveCodigo: TIntegerField;
    QrFPFunciEveControle: TIntegerField;
    QrFPFunciEveEvento: TIntegerField;
    QrFPFunciEveConta: TIntegerField;
    QrFPFunciEveLk: TIntegerField;
    QrFPFunciEveDataCad: TDateField;
    QrFPFunciEveDataAlt: TDateField;
    QrFPFunciEveUserCad: TIntegerField;
    QrFPFunciEveUserAlt: TIntegerField;
    QrFPFunciEveIncidencia: TSmallintField;
    QrFPFunciEveEve_Incid: TSmallintField;
    QrFPFunciEveEve_ValPer: TFloatField;
    QrFPFunciEveEve_Meses: TSmallintField;
    QrFPFunciEveCta_IncPer: TFloatField;
    QrFPFunciEveCta_MesesR: TSmallintField;
    QrFPFunciEveNOMEEVENTO: TWideStringField;
    QrFPFunciEveNOMECONTA: TWideStringField;
    QrFPFunciEveNOME_INCIDENCIA: TWideStringField;
    QrFPFunciEveNOME_EVE_INCID: TWideStringField;
    Label97: TLabel;
    Label98: TLabel;
    Panel20: TPanel;
    GroupBox7: TGroupBox;
    Label62: TLabel;
    Label65: TLabel;
    Label64: TLabel;
    dmkEdCta_IncPer: TdmkEdit;
    dmkEdCta_MesesR: TdmkEdit;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    Label66: TLabel;
    Label99: TLabel;
    QrFPtpCal: TmySQLQuery;
    DsFPtpCal: TDataSource;
    QrFPtpCalCodigo: TSmallintField;
    QrFPtpCalDescricao: TWideStringField;
    Label100: TLabel;
    CkPensaoLiq: TCheckBox;
    QrFPFunciPensaoLiq: TSmallintField;
    DBCheckBox6: TDBCheckBox;
    RgComoCalcIR: TRadioGroup;
    StaticText1: TStaticText;
    QrFPFunciComoCalcIR: TSmallintField;
    StaticText2: TStaticText;
    DBRadioGroup1: TDBRadioGroup;
    CkDesBasIRRF: TCheckBox;
    TPDataAdm: TDateTimePicker;
    TPDataIniAf: TDateTimePicker;
    TPDataFinAf: TDateTimePicker;
    TPDataExame: TDateTimePicker;
    TPDataFGTS: TDateTimePicker;
    TabSheet16: TTabSheet;
    Panel9: TPanel;
    Label48: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    CheckBox1: TCheckBox;
    dmkEdit2: TdmkEdit;
    EdDemissao: TdmkEditCB;
    CBDemissao: TdmkDBLookupComboBox;
    Panel22: TPanel;
    PnFerias: TPanel;
    DBGFerias: TDBGrid;
    QrFPFunciFer: TmySQLQuery;
    DsFPFunciFer: TDataSource;
    QrFPFunciFerEmpresa: TIntegerField;
    QrFPFunciFerCodigo: TIntegerField;
    QrFPFunciFerCalculo: TIntegerField;
    QrFPFunciFerPecun: TSmallintField;
    QrFPFunciFerMediaCoMan: TFloatField;
    QrFPFunciFerMediaAdMan: TFloatField;
    QrFPFunciFerDtIniPA: TDateField;
    QrFPFunciFerDtFimPA: TDateField;
    QrFPFunciFerDiasFaltI: TIntegerField;
    QrFPFunciFerDiasFaltP: TIntegerField;
    QrFPFunciFerDiasGozar: TSmallintField;
    QrFPFunciFerDiasPecun: TSmallintField;
    QrFPFunciFerDtIniPecun: TDateField;
    QrFPFunciFerDtFimPecun: TDateField;
    QrFPFunciFerDtIniPG: TDateField;
    QrFPFunciFerDtFimFeria: TDateField;
    QrFPFunciFerDtAvisoFer: TDateField;
    QrFPFunciFerDtRecPgto: TDateField;
    QrFPFunciFerDtSolPecun: TDateField;
    QrFPFunciFerDtSolPrP13: TDateField;
    QrFPFunciFerLk: TIntegerField;
    QrFPFunciFerDataCad: TDateField;
    QrFPFunciFerDataAlt: TDateField;
    QrFPFunciFerUserCad: TIntegerField;
    QrFPFunciFerUserAlt: TIntegerField;
    Panel23: TPanel;
    PnFerConf: TPanel;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    PnFerEdit: TPanel;
    BtExcluiFer: TBitBtn;
    BtAlteraFer: TBitBtn;
    BitBtn9: TBitBtn;
    Panel21: TPanel;
    LaTipoFer: TLabel;
    Panel24: TPanel;
    Label102: TLabel;
    Label104: TLabel;
    Label105: TLabel;
    Label110: TLabel;
    RGPecun: TRadioGroup;
    dmkEdDiasFaltI: TdmkEdit;
    dmkEdMediaAdMan: TdmkEdit;
    dmkEdMediaCoMan: TdmkEdit;
    dmkEdMeHEMan1xx: TdmkEdit;
    Label103: TLabel;
    Label106: TLabel;
    Label107: TLabel;
    Label108: TLabel;
    Label111: TLabel;
    Label112: TLabel;
    QrFaltas: TmySQLQuery;
    QrFaltasDias: TSmallintField;
    QrFPFunciFerDTINIPECUN_TXT: TWideStringField;
    QrFPFunciFerDTFIMPECUN_TXT: TWideStringField;
    Panel25: TPanel;
    Label109: TLabel;
    dmkEdDiasGozar: TdmkEdit;
    Label113: TLabel;
    dmkEdDiasPecun: TdmkEdit;
    Label114: TLabel;
    Label115: TLabel;
    dmkEdDtIniPA: TdmkEdit;
    dmkEdDtFimPA: TdmkEdit;
    dmkEdDtFimPG: TdmkEdit;
    dmkEdDtIniPG: TdmkEdit;
    dmkEdDtSolPrP13: TdmkEdit;
    dmkEdDtSolPecun: TdmkEdit;
    dmkEdDtRecPgto: TdmkEdit;
    dmkEdDtAvisoFer: TdmkEdit;
    dmkEdDtIniPecun: TdmkEdit;
    dmkEdDtFimPecun: TdmkEdit;
    Label116: TLabel;
    dmkEdDtRetorno: TdmkEdit;
    QrFPFunciFerDtRetorno: TDateField;
    StaticText3: TStaticText;
    QrFPFunciFerDTSOLPECUN_TXT: TWideStringField;
    QrFPFunciFerDTSOLPRP13_TXT: TWideStringField;
    QrFPFunciFerPECUNIO_TXT: TWideStringField;
    frxAvisoFerias: TfrxReport;
    PMImprime: TPopupMenu;
    Avisodefrias1: TMenuItem;
    frxDsFPFuncifer: TfrxDBDataset;
    frxDsFPFunci: TfrxDBDataset;
    QrFPFunciFerDtFimPG: TDateField;
    Solicitaodeabonopecunirio1: TMenuItem;
    frxSolicitaAbono: TfrxReport;
    frxSolicita1a_13o: TfrxReport;
    Solicitaoda1parcelado131: TMenuItem;
    QrFPFunciFerControle: TIntegerField;
    Panel19: TPanel;
    GroupBox8: TGroupBox;
    RGEve_Incid: TRadioGroup;
    Panel26: TPanel;
    Label63: TLabel;
    EdEvento: TdmkEditCB;
    CBEvento: TdmkDBLookupComboBox;
    Panel28: TPanel;
    StaticText4: TStaticText;
    CLTipoCalc: TCheckListBox;
    QrFPFunciCal: TmySQLQuery;
    DsFPFunciCal: TDataSource;
    QrFPFunciCalControle: TIntegerField;
    QrFPFunciCalTipoCalc: TIntegerField;
    QrFPFunciCalLk: TIntegerField;
    QrFPFunciCalDataCad: TDateField;
    QrFPFunciCalDataAlt: TDateField;
    QrFPFunciCalUserCad: TIntegerField;
    QrFPFunciCalUserAlt: TIntegerField;
    QrFPFunciCalNOMETIPOCALC: TWideStringField;
    QrFPFunciEveDesBasIRRF: TSmallintField;
    PnGrades: TPanel;
    DBGEventos: TDBGrid;
    DBGrid1: TDBGrid;
    Label96: TLabel;
    dmkEdMeHEMan200: TdmkEdit;
    QrFPFunciFerMeHEMan200: TFloatField;
    CkProporciHT: TCheckBox;
    QrFPFunciEveProporciHT: TSmallintField;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    Label101: TLabel;
    dmkEdHEFator: TdmkEdit;
    QrFPFunciFerMeHEMan1xx: TFloatField;
    QrFPFunciHEFator: TFloatField;
    DBEdit47: TDBEdit;
    Label117: TLabel;
    Label118: TLabel;
    GroupBox9: TGroupBox;
    RGANTipo: TRadioGroup;
    dmkEdANIniHr: TdmkEdit;
    Label119: TLabel;
    Label120: TLabel;
    dmkEdANFimHr: TdmkEdit;
    Label121: TLabel;
    dmkEdANMinHr: TdmkEdit;
    dmkEdAdiciNotur: TdmkEdit;
    Label122: TLabel;
    Label123: TLabel;
    dmkEdLei8923_94: TdmkEdit;
    QrFPFunciANTipo: TSmallintField;
    QrFPFunciANIniHr: TTimeField;
    QrFPFunciANFimHr: TTimeField;
    QrFPFunciANMinHr: TTimeField;
    QrFPFunciLei8923_94: TFloatField;
    QrFPFunciAdiciNotur: TFloatField;
    Panel27: TPanel;
    QrFPFunciHor: TmySQLQuery;
    DsFPFunciHor: TDataSource;
    QrFPFunciHorCodigo: TIntegerField;
    QrFPFunciHorControle: TIntegerField;
    QrFPFunciHorTur1Ent: TTimeField;
    QrFPFunciHorTur1DeI: TTimeField;
    QrFPFunciHorTur1DeF: TTimeField;
    QrFPFunciHorTur1Sai: TTimeField;
    QrFPFunciHorTur2Ent: TTimeField;
    QrFPFunciHorTur2DeI: TTimeField;
    QrFPFunciHorTur2DeF: TTimeField;
    QrFPFunciHorTur2Sai: TTimeField;
    QrFPFunciHorLk: TIntegerField;
    QrFPFunciHorDataCad: TDateField;
    QrFPFunciHorDataAlt: TDateField;
    QrFPFunciHorUserCad: TIntegerField;
    QrFPFunciHorUserAlt: TIntegerField;
    QrFPFunciEveUnidade: TWideStringField;
    Panel31: TPanel;
    Label93: TLabel;
    dmkEdEve_ValPer: TdmkEdit;
    EdUnidade: TEdit;
    Label137: TLabel;
    GroupBox13: TGroupBox;
    Label95: TLabel;
    dmkEdEve_Meses: TdmkEdit;
    CkEve_Ciclo: TCheckBox;
    dmkEdEve_DtCicl: TdmkEdit;
    QrFPFunciEveEve_Ciclo: TSmallintField;
    QrFPFunciEveEve_DtCicl: TDateField;
    dmkEdPriDdPonto: TdmkEdit;
    Label94: TLabel;
    QrFPFunciPriDdPonto: TSmallintField;
    Label139: TLabel;
    DBEdit53: TDBEdit;
    Label138: TLabel;
    Label141: TLabel;
    Label140: TLabel;
    QrFPFunciHorTur1HrN: TTimeField;
    QrFPFunciHorTur1HrE: TTimeField;
    QrFPFunciHorTur1HrA: TTimeField;
    QrFPFunciHorTur2HrN: TTimeField;
    QrFPFunciHorTur2HrE: TTimeField;
    QrFPFunciHorTur2HrA: TTimeField;
    TabSheet10: TTabSheet;
    Panel30: TPanel;
    Panel32: TPanel;
    Panel33: TPanel;
    PnHorCtrl: TPanel;
    BtExcluiHor: TBitBtn;
    BtAlteraHor: TBitBtn;
    BitBtn12: TBitBtn;
    PnHorConf: TPanel;
    Panel999: TPanel;
    BitBtn5: TBitBtn;
    BitBtn8: TBitBtn;
    DBGHor: TDBGrid;
    PnHorEdit: TPanel;
    Panel34: TPanel;
    GroupBox11: TGroupBox;
    Label129: TLabel;
    Label130: TLabel;
    Label131: TLabel;
    Label132: TLabel;
    dmkEdTur2Ent: TdmkEdit;
    dmkEdTur2DeI: TdmkEdit;
    dmkEdTur2DeF: TdmkEdit;
    dmkEdTur2Sai: TdmkEdit;
    Panel35: TPanel;
    Panel36: TPanel;
    GroupBox12: TGroupBox;
    Label133: TLabel;
    Label134: TLabel;
    Label135: TLabel;
    Label136: TLabel;
    dmkEdTur1Ent: TdmkEdit;
    dmkEdTur1DeI: TdmkEdit;
    dmkEdTur1DeF: TdmkEdit;
    dmkEdTur1Sai: TdmkEdit;
    Panel37: TPanel;
    Label142: TLabel;
    Label143: TLabel;
    dmkEdTur1HrT: TdmkEdit;
    dmkEdTur1HrE: TdmkEdit;
    dmkEdTur2HrT: TdmkEdit;
    Label146: TLabel;
    dmkEdTur2HrE: TdmkEdit;
    Label148: TLabel;
    dmkEdTur1HrN: TdmkEdit;
    dmkEdTur1HrB: TdmkEdit;
    Label149: TLabel;
    dmkEdTur2HrN: TdmkEdit;
    Label151: TLabel;
    dmkEdTur2HrB: TdmkEdit;
    LaTur1HrT: TLabel;
    LaTur1HrE: TLabel;
    LaTur1HrN: TLabel;
    LaTur1HrB: TLabel;
    LaTur2HrT: TLabel;
    LaTur2HrE: TLabel;
    LaTur2HrN: TLabel;
    LaTur2HrB: TLabel;
    Label152: TLabel;
    Label153: TLabel;
    Label154: TLabel;
    Label155: TLabel;
    dmkEdTur1HrS: TdmkEdit;
    Label156: TLabel;
    LaTur1HrS: TLabel;
    dmkEdTur2HrS: TdmkEdit;
    LaTur2HrS: TLabel;
    QrFPFunciHorTur1HrT: TTimeField;
    QrFPFunciHorTur1HrS: TTimeField;
    QrFPFunciHorTur1HrB: TTimeField;
    QrFPFunciHorTur2HrT: TTimeField;
    QrFPFunciHorTur2HrS: TTimeField;
    QrFPFunciHorTur2HrB: TTimeField;
    Label145: TLabel;
    Label150: TLabel;
    Label157: TLabel;
    Label158: TLabel;
    dmkEdTur1HrC: TdmkEdit;
    LaTur1HrC: TLabel;
    Label160: TLabel;
    dmkEdTur2HrC: TdmkEdit;
    LaTur2HrC: TLabel;
    QrFPFunciHorTXT1HrT: TWideStringField;
    QrFPFunciHorTXT1HrN: TWideStringField;
    QrFPFunciHorTXT1HrS: TWideStringField;
    QrFPFunciHorTXT1HrA: TWideStringField;
    QrFPFunciHorTXT1HrB: TWideStringField;
    QrFPFunciHorTXT1HrE: TWideStringField;
    Label159: TLabel;
    Panel38: TPanel;
    Label144: TLabel;
    dmkEdTur1HrA: TdmkEdit;
    LaTur1HrA: TLabel;
    Panel39: TPanel;
    Label147: TLabel;
    dmkEdTur2HrA: TdmkEdit;
    LaTur2HrA: TLabel;
    Label161: TLabel;
    Label162: TLabel;
    Label163: TLabel;
    Label164: TLabel;
    Label165: TLabel;
    QrFPFunciHorTur1HrC: TTimeField;
    QrFPFunciHorTur2HrC: TTimeField;
    QrFPFunciHorTXT1HrC: TWideStringField;
    Panel40: TPanel;
    Label18: TLabel;
    DBEdit11: TDBEdit;
    Label23: TLabel;
    DBEdit12: TDBEdit;
    Label24: TLabel;
    DBEdit13: TDBEdit;
    Label25: TLabel;
    DBEdit14: TDBEdit;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    GroupBox10: TGroupBox;
    Label124: TLabel;
    Label125: TLabel;
    Label126: TLabel;
    Label127: TLabel;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    DBEdit51: TDBEdit;
    DBEdit52: TDBEdit;
    DBRadioGroup2: TDBRadioGroup;
    Label128: TLabel;
    DBEdit48: TDBEdit;
    Panel41: TPanel;
    Label74: TLabel;
    DBEdit35: TDBEdit;
    Label80: TLabel;
    DBEdit38: TDBEdit;
    Label86: TLabel;
    DBEdit46: TDBEdit;
    Label87: TLabel;
    DBEdit45: TDBEdit;
    Label77: TLabel;
    DBEdit44: TDBEdit;
    Label78: TLabel;
    DBEdit36: TDBEdit;
    Label79: TLabel;
    DBEdit37: TDBEdit;
    Panel42: TPanel;
    Label81: TLabel;
    DBEdit39: TDBEdit;
    Label82: TLabel;
    DBEdit40: TDBEdit;
    Label83: TLabel;
    DBEdit41: TDBEdit;
    Label84: TLabel;
    DBEdit42: TDBEdit;
    Label85: TLabel;
    DBEdit43: TDBEdit;
    CheckBox2: TCheckBox;
    GroupBox1: TGroupBox;
    Label39: TLabel;
    Label42: TLabel;
    DBCheckBox3: TDBCheckBox;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    GroupBox5: TGroupBox;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    GroupBox6: TGroupBox;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    EdChapa: TdmkEdit;
    Label166: TLabel;
    QrFPFunciAlterWeb: TSmallintField;
    QrFPFunciChapa: TWideStringField;
    Label167: TLabel;
    DBEdit54: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel43: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GroupBox14: TGroupBox;
    BitBtn1: TBitBtn;
    Panel6: TPanel;
    BitBtn2: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    QrFPFunciTre: TmySQLQuery;
    DsFPFunciTre: TDataSource;
    QrFPFunciTreCodigo: TIntegerField;
    QrFPFunciTreControle: TIntegerField;
    QrFPFunciTreNome: TWideStringField;
    QrFPFunciTreCargaHoras: TFloatField;
    QrFPFunciTreDataIni: TDateField;
    QrFPFunciTreDataFim: TDateField;
    QrFPFunciTrePathCertif: TWideStringField;
    TabSheet17: TTabSheet;
    Panel7: TPanel;
    DBGrid2: TDBGrid;
    Panel29: TPanel;
    BtFPFunciTreIns: TBitBtn;
    BtFPFunciTreUpd: TBitBtn;
    BtFPFunciTreDel: TBitBtn;
    Label168: TLabel;
    Label169: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure QrFPFunciAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFPFunciBeforeOpen(DataSet: TDataSet);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdCodigoChange(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure EdFunciKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrFPFunciAfterOpen(DataSet: TDataSet);
    procedure QrFPFunciBeforeClose(DataSet: TDataSet);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure EdFotoChange(Sender: TObject);
    procedure BtIncluiItsClick(Sender: TObject);
    procedure QrFPFunciBeforeScroll(DataSet: TDataSet);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BtAlteraItsClick(Sender: TObject);
    procedure QrFPFunciEveCalcFields(DataSet: TDataSet);
    procedure QrFPFunciEveBeforeClose(DataSet: TDataSet);
    procedure QrFPFunciEveAfterOpen(DataSet: TDataSet);
    procedure BtExcluiItsClick(Sender: TObject);
    procedure PageControl2Change(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure QrFPFunciFerBeforeClose(DataSet: TDataSet);
    procedure QrFPFunciFerAfterOpen(DataSet: TDataSet);
    procedure QrFPFunciFerCalcFields(DataSet: TDataSet);
    procedure BtAlteraFerClick(Sender: TObject);
    procedure BtExcluiFerClick(Sender: TObject);
    procedure dmkEdDiasFaltIChange(Sender: TObject);
    procedure RGPecunClick(Sender: TObject);
    procedure dmkEdDtIniPAChange(Sender: TObject);
    procedure dmkEdDtIniPGExit(Sender: TObject);
    procedure dmkEdDtFimPAChange(Sender: TObject);
    procedure Avisodefrias1Click(Sender: TObject);
    procedure frxAvisoFeriasGetValue(const VarName: String;
      var Value: Variant);
    procedure PMImprimePopup(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Solicitaodeabonopecunirio1Click(Sender: TObject);
    procedure Solicitaoda1parcelado131Click(Sender: TObject);
    procedure QrFPFunciEveAfterScroll(DataSet: TDataSet);
    procedure DBGEventosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure RGANTipoClick(Sender: TObject);
    procedure BtAlteraHorClick(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure QrFPFunciHorBeforeClose(DataSet: TDataSet);
    procedure QrFPFunciHorAfterOpen(DataSet: TDataSet);
    procedure BtExcluiHorClick(Sender: TObject);
    procedure dmkEdTur1EntExit(Sender: TObject);
    procedure dmkEdTur1DeIExit(Sender: TObject);
    procedure dmkEdTur1DeFExit(Sender: TObject);
    procedure dmkEdTur1SaiExit(Sender: TObject);
    procedure QrFPFunciHorCalcFields(DataSet: TDataSet);
    procedure dmkEdTur1HrTExit(Sender: TObject);
    procedure dmkEdTur1HrNExit(Sender: TObject);
    procedure dmkEdTur1HrSExit(Sender: TObject);
    procedure dmkEdTur1HrAExit(Sender: TObject);
    procedure dmkEdTur1HrBExit(Sender: TObject);
    procedure dmkEdTur1HrEExit(Sender: TObject);
    procedure dmkEdTur1HrCExit(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtFPFunciTreInsClick(Sender: TObject);
    procedure BtFPFunciTreUpdClick(Sender: TObject);
    procedure BtFPFunciTreDelClick(Sender: TObject);
    procedure QrFPFunciTreBeforeClose(DataSet: TDataSet);
    procedure QrFPFunciTreAfterOpen(DataSet: TDataSet);
    procedure DBGrid2CellClick(Column: TColumn);
  private
    FDiasFaltI, FDiasFaltP, FDiasGozar, FDiasPecun: Integer;
    procedure CriaOForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure MostraEdicaoIts(Mostra: Integer; SQLType: TSQLType);
    procedure MostraFormFPFunciTre(SQLType: TSQLType);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ReopenFPFunciEve(Controle: Integer);
    procedure ReopenFPFunciCal(TipoCalc: Integer);
    procedure ReopenFPFunciFer(Calculo: Integer);
    procedure ReopenFPFunciHor(Controle: Integer);
    procedure ReopenFPFunciTre(Controle: Integer);
    procedure CalculaDiasFerias();
    procedure ColoreTexto(NomeComp: String);
    //
    function dmkEdTur(SubNome: String): TdmkEdit;
    function LaTur(SubNome: String): TLabel;
    function ConfereHoras(Turno: Integer): Boolean;
  public
    { Public declarations }
  end;

var
  FmFPFunci: TFmFPFunci;

const
  FFormatFloat = '00000';
  SegundosNoDia = 86400;
  FFotoN = Chr(13)+Chr(10)+'Foto'+Chr(13)+Chr(10)+Chr(13)+Chr(10)+
                           'N�o'+Chr(13)+Chr(10)+Chr(13)+Chr(10);

implementation

uses UnMyObjects,
{$IFDEF FOLHA_PAGAMENTO} FPFeriaFal, FPFunca, FPDepto, ModuleSal, {$ENDIF}
Module, MyVCLSkin, ModuleGeral, FPFunciTre, MyDBCheck;

{$R *.DFM}

//itens autom�ticos de event em funci por tipocalc

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFPFunci.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFPFunci.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFPFunciCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFPFunci.DefParams;
begin
  VAR_GOTOTABELA := 'FPFunci';
  VAR_GOTOMYSQLTABLE := QrFPFunci;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;
  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT fpf.Descricao NOMEFUNCAO, ');
{$IFDEF FOLHA_PAGAMENTO}
  VAR_SQLx.Add('fpd.Descricao NOMEDEPTO, fp2.Descricao NOMECBO2002, ');
  VAR_SQLx.Add('fpc.Descricao NOMECATEG,');
  VAR_SQLx.Add('fpv.Descricao NOMEVINCU, fpa.Descricao NOMEADMIS,');
  VAR_SQLx.Add('fps.Descricao NOMESITUA, fpl.Descricao NOMETPSAL,');
  VAR_SQLx.Add('fp4.Descricao NOMECARGO, fpn.Descricao NOMENACIONALID,');
  VAR_SQLx.Add('fpe.Descricao NOMEECIVIL, fpr.Descricao NOMERACA,');
  VAR_SQLx.Add('fpg.Descricao NOMEGRAUI, fpm.Nome NOMELOCTRAB, ');
{$ELSE}
  VAR_SQLx.Add('"" NOMEDEPTO, "" NOMECBO2002, ');
  VAR_SQLx.Add('"" NOMECATEG,');
  VAR_SQLx.Add('"" NOMEVINCU, "" NOMEADMIS,');
  VAR_SQLx.Add('"" NOMESITUA, "" NOMETPSAL,');
  VAR_SQLx.Add('"" NOMECARGO, "" NOMENACIONALID,');
  VAR_SQLx.Add('"" NOMEECIVIL, "" NOMERACA,');
  VAR_SQLx.Add('"" NOMEGRAUI, "" NOMELOCTRAB, ');
{$ENDIF}
  VAR_SQLx.Add('IF(efu.Tipo=0, efu.RazaoSocial, efu.Nome) NUMEFUNCI,');
  VAR_SQLx.Add('IF(epr.Tipo=0, epr.RazaoSocial, epr.Nome) NUMEEMPRESA,');
  VAR_SQLx.Add('fun.*');
  VAR_SQLx.Add('FROM fpfunci fun');
  VAR_SQLx.Add('LEFT JOIN entidades efu ON efu.Codigo=fun.Codigo');
  VAR_SQLx.Add('LEFT JOIN entidades epr ON epr.Codigo=fun.Empresa');
  VAR_SQLx.Add('LEFT JOIN fpfunca   fpf ON fpf.CodID=fun.Funcao');
{$IFDEF FOLHA_PAGAMENTO}
  VAR_SQLx.Add('LEFT JOIN fpdepto   fpd ON fpd.CodID=fun.Depto');
  VAR_SQLx.Add('LEFT JOIN fpcbo02   fp2 ON fp2.CodID=fun.CBO2002');
  VAR_SQLx.Add('LEFT JOIN fpcbo94   fp4 ON fp4.CodID=fun.Cargo');
  VAR_SQLx.Add('LEFT JOIN fpcateg   fpc ON fpc.Codigo=fun.Categoria');
  VAR_SQLx.Add('LEFT JOIN fpvincu   fpv ON fpv.Codigo=fun.Vinculo');
  VAR_SQLx.Add('LEFT JOIN fpadmis   fpa ON fpa.Codigo=fun.Admissao');
  VAR_SQLx.Add('LEFT JOIN fpsitua   fps ON fps.Codigo=fun.Situacao');
  VAR_SQLx.Add('LEFT JOIN fptpsal   fpl ON fpl.Codigo=fun.TipoSal');
  VAR_SQLx.Add('LEFT JOIN fpnacio   fpn ON fpn.Codigo=fun.Nacionalid');
  VAR_SQLx.Add('LEFT JOIN fpcivil   fpe ON fpe.Codigo=fun.EstCivil');
  VAR_SQLx.Add('LEFT JOIN fpracas   fpr ON fpr.Codigo=fun.Raca');
  VAR_SQLx.Add('LEFT JOIN fpgraui   fpg ON fpg.Codigo=fun.GrauInstru');
  VAR_SQLx.Add('LEFT JOIN fpmunic   fpm ON fpm.CodID=fun.LocalTrab');
{$ENDIF}
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE fun.Codigo > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND fun.Codigo=:P0');
  //
  VAR_SQLa.Add('AND efu.Nome Like :P0');
  //
end;

procedure TFmFPFunci.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
      PnInclui.Visible       := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        // Parei aqui
        // N�o tem
      end else begin
        EdFuncao.Text := QrFPFunciFuncao.Value;
        CBFuncao.KeyValue := QrFPFunciFuncao.Value;
        //
        EdDepto.Text := QrFPFunciDepto.Value;
        CBDepto.KeyValue := QrFPFunciDepto.Value;
        //
        EdCBO2002.Text := QrFPFunciCBO2002.Value;
        CBCBO2002.KeyValue := QrFPFunciCBO2002.Value;
        //
        EdCBO1994.Text := QrFPFunciCargo.Value;
        CBCBO1994.KeyValue := QrFPFunciCargo.Value;
        //
        EdLocalTrab.Text := QrFPFunciLocalTrab.Value;
        CBLocalTrab.KeyValue := QrFPFunciLocalTrab.Value;
        //
        EdCategoria.Text := IntToStr(QrFPFunciCategoria.Value);
        CBCategoria.KeyValue := QrFPFunciCategoria.Value;
        //
        EdVinculo.Text := IntToStr(QrFPFunciVinculo.Value);
        CBVinculo.KeyValue := QrFPFunciVinculo.Value;
        //
        EdAdmissao.Text := IntToStr(QrFPFunciAdmissao.Value);
        CBAdmissao.KeyValue := QrFPFunciAdmissao.Value;
        //
        EdSituacao.Text := IntToStr(QrFPFunciSituacao.Value);
        CBSituacao.KeyValue := QrFPFunciSituacao.Value;
        //
        EdTipoSal.Text := IntToStr(QrFPFunciTipoSal.Value);
        CBTipoSal.KeyValue := QrFPFunciTipoSal.Value;
        //
        EdRegistro.Text              := QrFPFunciRegistro.Value;
        CkAlvara.Checked             := Geral.IntToBool_0(QrFPFunciAlvara.Value);
        CkProfessor.Checked          := Geral.IntToBool_0(QrFPFunciProfessor.Value);
        TPDataAdm.Date               := QrFPFunciDataAdm.Value;
        dmkEdHrSeman.ValueVariant    := QrFPFunciHrSeman.Value;
        dmkEdSalario.ValueVariant    := QrFPFunciSalario.Value;
        dmkEdFilhos.ValueVariant     := QrFPFunciFilhos.Value;
        dmkEdDependent.ValueVariant  := QrFPFunciDependent.Value;
        dmkEdHrSeman.ValueVariant    := QrFPFunciHrSeman.Value;
        dmkEdPensaoAlm.ValueVariant  := QrFPFunciPensaoAlm.Value;
        dmkEdBancoSal.ValueVariant   := QrFPFunciBancoSal.Value;
        EdContaSal.Text              := QrFPFunciContaSal.Value;
        CkOptaFGTS.Checked           := Geral.IntToBool_0(QrFPFunciOptaFGTS.Value);
        TPDataFGTS.Date              := QrFPFunciDataFGTS.Value;
        dmkEdBancoFGTS.ValueVariant  := QrFPFunciBancoFGTS.Value;
        EdContaFGTS.Text             := QrFPFunciContaFGTS.Value;
        TPDataExame.date             := QrFPFunciDataExame.Value;
        dmkEdPerExame.ValueVariant   := QrFPFunciPerExame.Value;
        TPDataIniAf.Date             := QrFPFunciDataIniAf.Value;
        TPDataFinAf.Date             := QrFPFunciDataFinAf.Value;
        dmkEdMesesAf.ValueVariant    := QrFPFunciMesesAf.Value;
        CkRAIS.Checked               := Geral.IntToBool_0(QrFPFunciRAIS.Value);
        CkGFIP.Checked               := Geral.IntToBool_0(QrFPFunciGFIP.Value);
        EdNacionalid.Text            := IntToStr(QrFPFunciNacionalid.Value);
        CBNacionalid.KeyValue        := QrFPFunciNacionalid.Value;
        dmkEdAnoChegada.ValueVariant := QrFPFunciAnoChegada.Value;
        EdGrauInstru.Text            := IntToStr(QrFPFunciGrauInstru.Value);
        CBGrauInstru.KeyValue        := QrFPFunciGrauInstru.Value;
        EdEstCivil.Text              := IntToStr(QrFPFunciEstCivil.Value);
        CBEstCivil.KeyValue          := QrFPFunciEstCivil.Value;
        EdCTPS.Text                  := QrFPFunciCTPS.Value;
        EdSerieCTPS.Text             := QrFPFunciSerieCTPS.Value;
        EdPIS.Text                   := QrFPFunciPIS.Value;
        EdTitEleitor.Text            := QrFPFunciTitEleitor.Value;
        EdReservista.Text            := QrFPFunciReservista.Value;
        EdRaca.Text                  := IntToStr(QrFPFunciRaca.Value);
        CBRaca.KeyValue              := QrFPFunciRaca.Value;
        CkDeficiente.Checked         := Geral.IntToBool_0(QrFPFunciDeficiente.Value);
        EdFoto.Text                  := QrFPFunciFoto.Value;
        dmkEdInsalubrid.ValueVariant := QrFPFunciInsalubrid.Value;
        dmkEdPericulosi.ValueVariant := QrFPFunciPericulosi.Value;
        dmkEdHEFator.ValueVariant    := QrFPFunciHEFator.Value;
        CkPensaoLiq.Checked          := Geral.IntToBool_0(QrFPFunciPensaoLiq.Value);
        RgComoCalcIR.ItemIndex       := QrFPFunciComoCalcIR.Value;
        RGANTipo.ItemIndex           := QrFPFunciANTipo.Value;
        dmkEdAdiciNotur.ValueVariant := QrFPFunciAdiciNotur.Value;
        dmkEdLei8923_94.ValueVariant := QrFPFunciLei8923_94.Value;
        dmkEdANIniHr.ValueVariant    := QrFPFunciANIniHr.Value;
        dmkEdANFimHr.ValueVariant    := QrFPFunciANFimHr.Value;
        dmkEdANMinHr.ValueVariant    := QrFPFunciANMinHr.Value;
        dmkEdPriDdPonto.ValueVariant := QrFPFunciPriDdPonto.Value;
        //
        EdChapa.ValueVariant         := QrFPFunciChapa.Value;
        //
        PageControl1.ActivePageIndex := 0;
        EdFuncao.SetFocus;
      end;
      //Ed.SetFocus;
    end;
    2:
    begin
      EdEmpresa.Text     := '';
      CBEmpresa.KeyValue := NULL;
      EdFunci.Text       := '';
      EdCodigo.Text      := '';
      CBCodigo.KeyValue  := NULL;
      //EdChapa_1.Text     := '';
      QrFunci.Close;
      //
{     Ver o que fazer!
      QrEmpresa.Close;
      QrEmpresa.SQL.Clear;
      QrEmpresa.SQL.Add('SELECT en.Codigo, en.CliInt, ');
      QrEmpresa.SQL.Add('IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEEMPRESA');
      QrEmpresa.SQL.Add('FROM entidades en');
      QrEmpresa.SQL.Add('WHERE ' + VAR_FP_EMPRESA);
      QrEmpresa.SQL.Add('ORDER BY NOMEEMPRESA');
      QrEmpresa.Open;
}
      //
      PnInclui.Visible := True;
      PainelDados.Visible := False;
      //
      EdEmpresa.SetFocus;
    end;
    else Geral.MB_Erro('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmFPFunci.CriaOForm;
begin
  DefParams;
  Va(vpLast);
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFPFunci.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFPFunci.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFPFunci.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFPFunci.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFPFunci.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFPFunciCodigo.Value;
  Close;
end;

procedure TFmFPFunci.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmFPFunci.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  TPDataExame.Date := Date;
  TPDataFGTS.Date := Date;
  TPDataFinAf.Date := Date;
  TPDataIniAf.Date := Date;
  TPDataAdm.Date := Date;
  //
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  PnGrades.Align    := alClient;
  PnEveConf.Align   := alClient;
  PnFerConf.Align   := alClient;
  PnFerias.Align    := alClient;
  PnHorEdit.Align   := alClient;
  PnHorConf.Align   := alClient;
  PnEventos.Align   := alClient;
  CriaOForm;
  //
  QrFPFunca.Open;
{$IFDEF FOLHA_PAGAMENTO}
  QrFPDepto.Open;
  QrFPCBO94.Open;
  QrFPCBO02.Open;
  QrFPCateg.Open;
  QrFPVincu.Open;
  QrFPAdmis.Open;
  QrFPSitua.Open;
  QrFPDeslg.Open;
  QrFPTpSal.Open;
  QrFPNacio.Open;
  QrFPGrauI.Open;
  QrFPCivil.Open;
  QrFPRacas.Open;
  QrFPMunic.Open;
  QrFPtpCal.Open;
  QrEventos.Open;
{$ENDIF}
  QrContas.Open;
  //
  PageControl2.ActivePageIndex := 0;
  //
  dmkEdPriDdPonto.ValueVariant := 1;
end;

procedure TFmFPFunci.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFPFunciCodigo.Value,LaRegistro.Caption);
end;

procedure TFmFPFunci.SbNomeClick(Sender: TObject);
begin
  SbQueryClick(Self);
end;

procedure TFmFPFunci.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmFPFunci.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFPFunci.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFPFunci.QrFPFunciAfterScroll(DataSet: TDataSet);
begin
  MostraEdicaoIts(0, stLok);
  if QrFPFunciFoto.Value <> '' then
  begin
    if FileExists(QrFPFunciFoto.Value) then
    begin
      Image3.Picture.LoadFromFile(QrFPFunciFoto.Value);
      LaFotoDB.Caption := '';
    end else begin
      Image3.Picture := nil;
      LaFotoDB.Caption := FFotoN + 'Localizada';
    end;
  end else begin
    Image3.Picture := nil;
    LaFotoDB.Caption := FFotoN + 'Definida';
  end;
{$IFDEF FOLHA_PAGAMENTO}
  ReopenFPFunciEve(0);
  ReopenFPFunciFer(0);
  ReopenFPFunciHor(0);
  ReopenFPFunciTre(0);
{$ENDIF}
end;

procedure TFmFPFunci.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFPFunciCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Entidades', Dmod.MyDB,
    'AND ' + VAR_FP_FUNCION));
end;

procedure TFmFPFunci.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFPFunci.QrFPFunciBeforeOpen(DataSet: TDataSet);
begin
  QrFPFunciCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmFPFunci.EdEmpresaChange(Sender: TObject);
var
  Empresa: Integer;
begin
  Empresa := Geral.IMV(EdEmpresa.Text);
  if Empresa <> 0 then Empresa := DmodG.QrEmpresasCodigo.Value;
  //
  EdCodigo.Text := '';
  CBCodigo.KeyValue := NULL;
  QrFunci.Close;
  if Empresa <> 0 then
  begin
    QrFunci.Close;
    QrFunci.SQL.Clear;
    QrFunci.SQL.Add('SELECT en.Codigo,');
    QrFunci.SQL.Add('IF(en.Nome="", en.RazaoSocial, en.Nome) NOMEFUNCI');
    QrFunci.SQL.Add('FROM entidades en');
    QrFunci.SQL.Add('WHERE ' + VAR_FP_FUNCION);
    QrFunci.SQL.Add('AND NOT (Codigo IN (');
    QrFunci.SQL.Add('  SELECT Codigo FROM fpfunci');
    QrFunci.SQL.Add('  WHERE Empresa=' + IntToStr(Empresa));
    QrFunci.SQL.Add('  AND DataDemiss=0))');
    QrFunci.SQL.Add('ORDER BY NOMEFUNCI');
    //
    QrFunci.Open;
  end;  
end;

procedure TFmFPFunci.EdCodigoChange(Sender: TObject);
begin
  if TdmkEdit(Sender).Name = 'EdNacionalid' then
  begin
    dmkEdAnoChegada.Enabled := Geral.IMV(EdNacionalid.Text) <> 10;
    LaAnoChegada.Enabled := dmkEdAnoChegada.Enabled;
  end;
end;

procedure TFmFPFunci.BtIncluiClick(Sender: TObject);
begin
  MostraEdicaoIts(0, stLok);
  MostraEdicao(2, stIns, 0);
end;

procedure TFmFPFunci.EdFunciKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Empresa: Integer;
begin
  if Key = VK_F4 then
  begin
    Empresa := Geral.IMV(EdEmpresa.Text);
    if Empresa = 0 then
    begin
      Geral.MB_Aviso('Informe a empresa!');
      Exit;
    end else Empresa := DModG.QrEmpresasCodigo.Value;
    QrMax.Close;
    QrMax.Params[0].AsInteger := Empresa;
    QrMax.Open;
    EdFunci.Text := IntToStr(QrMaxFunci.Value + 1);
  end;
end;

procedure TFmFPFunci.BitBtn1Click(Sender: TObject);
var
  Funci, Empresa, Codigo: Integer;
begin
  Funci   := Geral.IMV(EdFunci.Text);
  Codigo  := Geral.IMV(EdCodigo.Text);
  Empresa := Geral.IMV(EdEmpresa.Text);
  if Empresa <> 0 then
    Empresa := DModG.QrEmpresasCodigo.Value;
  //
  if (Funci=0) or (Empresa=0) or (Codigo=0) then
  begin
    Geral.MB_Aviso(
    'A empresa, o funcion�rio e o c�digo do funcion�rio devem ser informados!');
    if Empresa = 0 then EdEmpresa.SetFocus else
    if Codigo = 0 then EdCodigo.SetFocus else
    if Funci = 0 then EdFunci.SetFocus;
    Exit;
  end;
  //
  QrPesq1.Close;
  QrPesq1.Params[00].AsInteger := Empresa;
  QrPesq1.Params[01].AsInteger := Funci;
  QrPesq1.Open;
  if QrPesq1.RecordCount > 0 then
  begin
    Geral.MB_Aviso('O c�digo "' + IntToStr(Funci) +
    '" j� est� cadastrado para o funcion�rio n�mero ' +
    IntToStr(QrPesq1Codigo.Value) + '!');
    Exit;
  end;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'FPFunci', False,
  ['Funci'],
  ['Empresa', 'Codigo'],
  [Funci],
  [Empresa, Codigo], True);
  //
  LocCod(Codigo, Codigo);
  MostraEdicao(0, stLok, 0);
end;

procedure TFmFPFunci.QrFPFunciAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrFPFunci.RecordCount > 0;
  //
  BtAltera.Enabled := Habilita;
  BtFPFunciTreIns.Enabled := Habilita;
end;

procedure TFmFPFunci.QrFPFunciBeforeClose(DataSet: TDataSet);
begin
  MostraEdicaoIts(0, stLok);
  BtAltera.Enabled := False;
  BtFPFunciTreIns.Enabled := False;
{$IFDEF FOLHA_PAGAMENTO}
  QrFPFunciEve.Close;
  QrFPFunciFer.Close;
  QrFPFunciHor.Close;
  QrFPFunciTre.Close;
{$ENDIF}  
end;

procedure TFmFPFunci.BtAlteraClick(Sender: TObject);
begin
  MostraEdicaoIts(0, stLok);
  MostraEdicao(1, stUpd, 0);
end;

procedure TFmFPFunci.BtConfirmaClick(Sender: TObject);
var
  Funcao, Depto, CBO2002, CBO1994, DataAdm, ContaSal, DataFGTS, ContaFGTS,
  DataExame, DataIniAf, DataFinAf, Foto, FeriaDtPrx, FeriaDtIni, FeriaDtAvi,
  FeriaDtRec, ANIniHr, ANFimHr, ANMinHr, Chapa: String;
  Categoria, Vinculo, Alvara, Situacao, Admissao, Professor, TipoSal, Filhos,
  Dependent, BancoSal, OptaFGTS, BancoFGTS, PerExame, MesesAf, RAIS, GFIP,
  Nacionalid, AnoChegada, GrauInstru, EstCivil, Raca, Deficiente, PensaoLiq,
  ComoCalcIR, FeriaNrGoz, FeriaFaltI, FeriaFaltP, FeriaPecun, ANTipo,
  PriDdPonto: Integer;
  Salario, PensaoAlm, Insalubrid, Periculosi, HEFator, AdiciNotur,
  Lei8923_94: Double;
begin
  Funcao     := Trim(EdFuncao.Text);
  Depto      := Trim(EdDepto.Text);
  CBO2002    := Trim(EdCBO2002.Text);
  CBO1994    := Trim(EdCBO1994.Text);
  Categoria  := Geral.IMV(EdCategoria.Text);
  Vinculo    := Geral.IMV(EdVinculo.Text);
  Admissao   := Geral.IMV(EdAdmissao.Text);
  Situacao   := Geral.IMV(EdSituacao.Text);
  TipoSal    := Geral.IMV(EdTipoSal.Text);
  DataAdm    := Geral.FDT(TPDataAdm.Date, 1);
  Filhos     := dmkEdFilhos.ValueVariant;
  Dependent  := dmkEdDependent.ValueVariant;
  Salario    := dmkEdSalario.ValueVariant;
  PensaoAlm  := dmkEdPensaoAlm.ValueVariant;
  BancoSal   := dmkEdBancoSal.ValueVariant;
  ContaSal   := EdContaSal.Text;
  OptaFGTS   := Geral.BoolToInt(CkOptaFGTS.Checked);
  DataFGTS   := Geral.FDT(TPDataFGTS.Date, 1);
  BancoFGTS  := dmkEdBancoFGTS.ValueVariant;
  ContaFGTS  := EdContaFGTS.Text;
  DataExame  := Geral.FDT(TPDataExame.Date, 1);
  PerExame   := dmkEdPerExame.ValueVariant;
  DataIniAf  := Geral.FDT(TPDataIniAf.Date, 1);
  DataFinAf  := Geral.FDT(TPDataFinAf.Date, 1);
  MesesAf    := dmkEdMesesAf.ValueVariant;
  RAIS       := Geral.BoolToInt(CkRAIS.Checked);
  GFIP       := Geral.BoolToInt(CkGFIP.Checked);
  Nacionalid := Geral.IMV(EdNacionalid.Text);
  AnoChegada := dmkEdAnoChegada.ValueVariant;
  GrauInstru := Geral.IMV(EdGrauInstru.Text);
  EstCivil   := Geral.IMV(EdEstCivil.Text);
  Raca       := Geral.IMV(EdRaca.Text);
  Deficiente := Geral.BoolToInt(CkDeficiente.Checked);
  Foto       := DmkPF.DuplicaSlash(EdFoto.Text);
  Insalubrid := dmkEdInsalubrid.ValueVariant;
  Periculosi := dmkEdPericulosi.ValueVariant;
  PensaoLiq  := Geral.BoolToInt(CkPensaoLiq.Checked);
  ComoCalcIR := RgComoCalcIR.ItemIndex;
  HEFator    := dmkEdHEFator.ValueVariant;
  //
  ANTipo     := RGANTipo.ItemIndex;
  AdiciNotur := dmkEdAdiciNotur.ValueVariant;
  Lei8923_94 := dmkEdLei8923_94.ValueVariant;
  ANIniHr    := dmkEdANIniHr.Text;
  ANFimHr    := dmkEdANFimHr.Text;
  ANMinHr    := dmkEdANMinHr.Text;
  PriDdPonto := dmkEdPriDdPonto.ValueVariant;
  //
  Chapa      := EdChapa.Text;
  //
{$IFDEF FOLHA_PAGAMENTO}
  if (Funcao='') or (Depto='') or (CBO2002='') or (Categoria=0) then
  begin
    PageControl1.ActivePageIndex := 0;
    Geral.MB_Aviso(
    'A fun��o, o departamento e o CBO2002 do funcion�rio devem ser informados!');
    if Funcao    = '' then EdFuncao.SetFocus else
    if Depto     = '' then EdDepto.SetFocus else
    if CBO2002   = '' then EdCBO2002.SetFocus else
    if Categoria =  0 then EdCategoria.SetFocus else
    if TipoSal   =  0 then EdTipoSal.SetFocus else
    ;
    Exit;
  end;
{$ENDIF}  
  (*if (Vinculo=0) or (Admissao=0) or (Situacao=0) then
  begin
    O s�ndico tem v�nculo empregat�cio ?
    PageControl1.ActivePageIndex := 1;
    Geral.MB_Aviso('O v�nculo empregat�cio do ' +
    'funcion�rio deve ser informado!');
    if Vinculo   =  0 then EdVinculo.SetFocus else
    if Situacao  =  0 then EdSituacao.SetFocus else
    if Admissao  =  0 then EdAdmissao.SetFocus else
    ;
    Exit;
  end;*)
  if TPDataAdm.Date < 15462 then // 01/05/1942
  begin
    Geral.MB_Aviso('Data de admiss�o inv�lida!');
    // Erro ? TPDataAdm.SetFocus;
    Pagecontrol1.ActivePageIndex := 1;
    TPDataAdm.SetFocus;
    Exit;
  end;
  if (Insalubrid > 0) and (Periculosi > 0) then
  begin
    Geral.MB_Aviso('N�o � permitido acumulo de insalubridade e ' +
    'periculosidade. Se o trabalhador trabalha em local considerado ' +
    'insalubre e perigoso, ele deve optar apenas por um dos adicionais.!');
    dmkEdPericulosi.SetFocus;
    Exit;
  end;
  //
  Alvara := Geral.BoolToInt(CkAlvara.Checked);
  Professor := Geral.BoolToInt(CkProfessor.Checked);
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'FPFunci', False,
  ['Funcao', 'Depto', 'CBO2002', 'Registro', 'Categoria', 'Vinculo', 'Alvara',
  'Admissao', 'Situacao', 'DataAdm', 'HrSeman', 'Professor', 'TipoSal',
  'Salario', 'Filhos', 'Dependent', 'PensaoAlm', 'BancoSal', 'ContaSal',
  'OptaFGTS', 'DataFGTS', 'BancoFGTS', 'ContaFGTS', 'DataExame', 'PerExame',
  'DataIniAf', 'DataFinAf', 'MesesAf', 'RAIS', 'GFIP', 'Nacionalid',
  'AnoChegada', 'GrauInstru', 'EstCivil', 'CTPS', 'SerieCTPS', 'PIS',
  'TitEleitor', 'Reservista', 'Raca', 'Deficiente', 'Cargo', 'HEFator',
  'LocalTrab', 'Foto', 'Insalubrid', 'Periculosi', 'PensaoLiq', 'ComoCalcIR',
  'ANTipo', 'AdiciNotur', 'Lei8923_94', 'ANIniHr', 'ANFimHr', 'ANMinHr',
  'PriDdPonto', 'Chapa'
  ],
  ['Empresa', 'Codigo'],
  [Funcao, Depto, CBO2002, EdRegistro.Text, Categoria, Vinculo, Alvara,
  Admissao, Situacao, DataAdm, dmkEdHrSeman.Text, Professor, TipoSal,
  Salario, Filhos, Dependent, PensaoAlm, BancoSal, ContaSal,
  OptaFGTS, DataFGTS, BancoFGTS, ContaFGTS, DataExame, PerExame,
  DataIniAf, DataFinAf, MesesAf, RAIS, GFIP, Nacionalid,
  AnoChegada, GrauInstru, EstCivil, EdCTPS.Text, EdSerieCTPS.Text, EdPIS.Text,
  EdTitEleitor.Text, EdReservista.Text, Raca, Deficiente, CBO1994, HEFator,
  EdLocalTrab.Text, Foto, Insalubrid, Periculosi, PensaoLiq, ComoCalcIR,
  ANTipo, AdiciNotur, Lei8923_94, ANIniHr, ANFimHr, ANMinHr,
  PriDdPonto, Chapa
   ],
  [QrFPFunciEmpresa.Value, QrFPFunciCodigo.Value], True);
  //
  LocCod(QrFPFunciCodigo.Value, QrFPFunciCodigo.Value);
  MostraEdicao(0, stLok, 0);
end;

procedure TFmFPFunci.SpeedButton5Click(Sender: TObject);
begin
  if OpenPictureDialog1.Execute then
    EdFoto.Text := OpenPictureDialog1.FileName;
end;

procedure TFmFPFunci.EdFotoChange(Sender: TObject);
begin
  if EdFoto.Text <> '' then
  begin
    if FileExists(EdFoto.Text) then
    begin
      Image2.Picture.LoadFromFile(EdFoto.Text);
      LaFotoEd.Caption := '';
    end else begin
      Image2.Picture := nil;
      LaFotoEd.Caption := FFotoN + 'Localizada';
    end;
  end else begin
    Image2.Picture := nil;
    LaFotoEd.Caption := FFotoN + 'Definida';
  end;
end;

procedure TFmFPFunci.ReopenFPFunciEve(Controle: Integer);
begin
  QrFPFunciEve.Close;
  QrFPFunciEve.Params[0].AsInteger := QrFPFunciCodigo.Value;
  QrFPFunciEve.Open;
  //
  if Controle > 0 then
    QrFPFunciEve.Locate('Controle', Controle, []);
end;

procedure TFmFPFunci.ReopenFPFunciCal(TipoCalc: Integer);
begin
  QrFPFunciCal.Close;
  QrFPFunciCal.Params[0].AsInteger := QrFPFunciEveControle.Value;
  QrFPFunciCal.Open;
  //
  if TipoCalc > 0 then
    QrFPFunciCal.Locate('TipoCalc', TipoCalc, []);
end;

procedure TFmFPFunci.ReopenFPFunciFer(Calculo: Integer);
begin
  QrFPFunciFer.Close;
  QrFPFunciFer.Params[00].AsInteger := QrFPFunciEmpresa.Value;
  QrFPFunciFer.Params[01].AsInteger := QrFPFunciCodigo.Value;
  QrFPFunciFer.Open;
  //
  if Calculo > 0 then
    QrFPFunciFer.Locate('Calculo', Calculo, []);
end;

procedure TFmFPFunci.BtIncluiItsClick(Sender: TObject);
begin
  MostraEdicaoIts(1, stIns);
end;

procedure TFmFPFunci.QrFPFunciBeforeScroll(DataSet: TDataSet);
begin
  MostraEdicaoIts(0, stLok);
end;

procedure TFmFPFunci.MostraEdicaoIts(Mostra: Integer; SQLType: TSQLType);
var
  i: Integer;
begin
  case Mostra of
    0:
    begin
      PnEveEdit.Visible  := True;
      PnGrades.Visible   := True;
      PnEventos.Visible  := False;
      PnEveConf.Visible  := False;
      ImgTipo.SQLType  := SQLType;
      //
      DBGFerias.Visible  := True;
      PnFerEdit.Visible  := True;
      PnFerias.Visible   := False;
      PnFerConf.Visible  := False;
      ImgTipo.SQLType  := SQLType;
      //
      PnHorCtrl.Visible  := True;
      PnHorConf.Visible  := False;
      DBGHor.Visible     := True;
      PnHorEdit.Visible  := False;
      ImgTipo.SQLType  := SQLType;
    end;
    1:
    begin
      PnEventos.Visible  := True;
      PnGrades.Visible   := False;
      PnEveConf.Visible  := True;
      PnEveEdit.Visible  := False;
      // Limpar tudo
      for i := 0 to CLTipoCalc.Items.Count -1 do
        CLTipoCalc.Checked[i] := False;
      if SQLType = stIns then
      begin
        RGIncidencia.ItemIndex       := 0;
        //
        EdEvento.Text                := '';
        CBEvento.KeyValue            := NULL;
        RGEve_Incid.ItemIndex        := 0;
        dmkEdEve_ValPer.ValueVariant := 0;
        dmkEdEve_Meses.ValueVariant  := 0;
        //
        EdConta.Text                 := '';
        CBConta.KeyValue             := NULL;
        dmkEdCta_IncPer.ValueVariant := 0;
        dmkEdCta_MesesR.ValueVariant := 0;
        EdUnidade.Text               := '';
        //
        CkProporciHT.Checked         := True;
        CkEve_Ciclo.Checked          := False;
        dmkEdEve_DtCicl.Text         := '';
      end else begin
        RGIncidencia.ItemIndex       := QrFPFunciEveIncidencia.Value;
        //
        EdEvento.Text                := IntToStr(QrFPFunciEveEvento.Value);
        CBEvento.KeyValue            := QrFPFunciEveEvento.Value;
        RGEve_Incid.ItemIndex        := QrFPFunciEveEve_Incid.Value;
        dmkEdEve_ValPer.ValueVariant := QrFPFunciEveEve_ValPer.Value;
        dmkEdEve_Meses.ValueVariant  := QrFPFunciEveEve_Meses.Value;
        EdUnidade.Text               := QrFPFunciEveUnidade.Value;
        //
        EdConta.Text                 := IntToStr(QrFPFunciEveConta.Value);
        CBConta.KeyValue             := QrFPFunciEveConta.Value;
        dmkEdCta_IncPer.ValueVariant := QrFPFunciEveCta_IncPer.Value;
        dmkEdCta_MesesR.ValueVariant := QrFPFunciEveCta_MesesR.Value;
        //
        CkProporciHT.Checked         := Geral.IntToBool(QrFPFunciEveProporciHT.Value);
        //
        CkEve_Ciclo.Checked          := Geral.IntToBool(QrFPFunciEveEve_Ciclo.Value);
        dmkEdEve_DtCicl.ValueVariant := QrFPFunciEveEve_DtCicl.Value;
        //
        QrFPFuncical.First;
        while not QrFPFuncical.Eof do
        begin
          CLTipoCalc.Checked[QrFPFunciCalTipoCalc.Value -1] := True;
          // Parei aqui
          QrFPFuncical.Next;
        end;
        //
      end;
      EdEvento.SetFocus;
      ImgTipo.SQLType := SQLType;
    end;
    2:
    begin
      PnFerias.Visible   := True;
      DBGFerias.Visible  := False;
      PnFerConf.Visible  := True;
      PnFerEdit.Visible  := False;
      //
      if SQLType = stIns then
      begin
        dmkEdMeHEMan1xx.ValueVariant := 0;
        dmkEdMeHEMan200.ValueVariant := 0;
        dmkEdMediaCoMan.ValueVariant := 0;
        dmkEdMediaAdMan.ValueVariant := 0;
        dmkEdDiasFaltI.ValueVariant  := 0;
        //
        RGPecun.ItemIndex            := 0;
        //
        if QrFPFunciFer.RecordCount = 0 then
        begin
          dmkEdDtIniPA.ValueVariant     := QrFPFunciDataAdm.Value;
          dmkEdDtIniPG.ValueVariant     := Date + 30;
          dmkEdDtRetorno.ValueVariant   := Date + 60;
          dmkEdDtRecPgto.ValueVariant   := Date;
          dmkEdDtAvisoFer.ValueVariant  := Date;
          //DecodeDate(Date, Ano, Mes, Dia);
          dmkEdDtSolPrP13.Text          := '';
          dmkEdDtSolPecun.Text          := '';
        end else begin
          dmkEdDtIniPA.ValueVariant     := QrFPFunciFerDtIniPA.Value;
          dmkEdDtIniPG.ValueVariant     := QrFPFunciFerDtIniPG.Value;
          dmkEdDtRecPgto.ValueVariant   := QrFPFunciFerDtRecPgto.Value;
          dmkEdDtSolPecun.ValueVariant  := QrFPFunciFerDtSolPecun.Value;
          dmkEdDtSolPrP13.ValueVariant  := QrFPFunciFerDtSolPrP13.Value;
          dmkEdDtAvisoFer.ValueVariant  := QrFPFunciFerDtAvisoFer.Value;
          dmkEdDtRetorno.ValueVariant   := QrFPFunciFerDtRetorno.Value;
        end;
        //
      end else begin
        dmkEdMeHEMan1xx.ValueVariant := QrFPFunciFerMeHEMan1xx.Value;
        dmkEdMeHEMan200.ValueVariant := QrFPFunciFerMeHEMan200.Value;
        dmkEdMediaCoMan.ValueVariant := QrFPFunciFerMediaCoMan.Value;
        dmkEdMediaAdMan.ValueVariant := QrFPFunciFerMediaAdMan.Value;
        dmkEdDiasFaltI.ValueVariant  := QrFPFunciFerDiasFaltI.Value;
        //
        RGPecun.ItemIndex            := QrFPFunciFerPecun.Value;
        //
        dmkEdDtIniPA.ValueVariant     := QrFPFunciFerDtIniPA.Value;
        dmkEdDtIniPG.ValueVariant     := QrFPFunciFerDtIniPG.Value;
        dmkEdDtRecPgto.ValueVariant   := QrFPFunciFerDtRecPgto.Value;
        dmkEdDtSolPecun.ValueVariant  := QrFPFunciFerDtSolPecun.Value;
        dmkEdDtSolPrP13.ValueVariant  := QrFPFunciFerDtSolPrP13.Value;
        //
      end;
      ImgTipo.SQLType := SQLType;
      CalculaDiasFerias();
      dmkEdMeHEMan1xx.SetFocus;
    end;
    3:
    begin
      PnHorConf.Visible  := True;
      PnHorCtrl.Visible  := False;
      PnHorEdit.Visible  := True;
      DBGHor.Visible     := False;
      //
      if SQLType = stIns then
      begin
        dmkEdTur1Ent.Text := '00:00';
        dmkEdTur1DeI.Text := '00:00';
        dmkEdTur1DeF.Text := '00:00';
        dmkEdTur1Sai.Text := '00:00';
        dmkEdTur1HrT.Text := '00:00:00';
        dmkEdTur1HrN.Text := '00:00:00';
        dmkEdTur1HrS.Text := '00:00:00';
        dmkEdTur1HrA.Text := '00:00:00';
        dmkEdTur1HrB.Text := '00:00:00';
        dmkEdTur1HrE.Text := '00:00:00';
        dmkEdTur1HrC.Text := '00:00:00';
        //
        dmkEdTur2Ent.Text := '00:00';
        dmkEdTur2DeI.Text := '00:00';
        dmkEdTur2DeF.Text := '00:00';
        dmkEdTur2Sai.Text := '00:00';
        dmkEdTur2HrT.Text := '00:00:00';
        dmkEdTur2HrN.Text := '00:00:00';
        dmkEdTur2HrS.Text := '00:00:00';
        dmkEdTur2HrA.Text := '00:00:00';
        dmkEdTur2HrB.Text := '00:00:00';
        dmkEdTur2HrE.Text := '00:00:00';
        dmkEdTur2HrC.Text := '00:00:00';
        //
      end else begin
        dmkEdTur1Ent.ValueVariant := QrFPFunciHorTur1Ent.Value;
        dmkEdTur1DeI.ValueVariant := QrFPFunciHorTur1DeI.Value;
        dmkEdTur1DeF.ValueVariant := QrFPFunciHorTur1DeF.Value;
        dmkEdTur1Sai.ValueVariant := QrFPFunciHorTur1Sai.Value;
        dmkEdTur1HrT.ValueVariant := QrFPFunciHorTur1HrT.Value;
        dmkEdTur1HrN.ValueVariant := QrFPFunciHorTur1HrN.Value;
        dmkEdTur1HrS.ValueVariant := QrFPFunciHorTur1HrS.Value;
        dmkEdTur1HrA.ValueVariant := QrFPFunciHorTur1HrA.Value;
        dmkEdTur1HrB.ValueVariant := QrFPFunciHorTur1HrB.Value;
        dmkEdTur1HrE.ValueVariant := QrFPFunciHorTur1HrE.Value;
        dmkEdTur1HrC.ValueVariant := QrFPFunciHorTur1HrC.Value;
        //
        dmkEdTur2Ent.ValueVariant := QrFPFunciHorTur2Ent.Value;
        dmkEdTur2DeI.ValueVariant := QrFPFunciHorTur2DeI.Value;
        dmkEdTur2DeF.ValueVariant := QrFPFunciHorTur2DeF.Value;
        dmkEdTur2Sai.ValueVariant := QrFPFunciHorTur2Sai.Value;
        dmkEdTur2HrT.ValueVariant := QrFPFunciHorTur2HrT.Value;
        dmkEdTur2HrN.ValueVariant := QrFPFunciHorTur2HrN.Value;
        dmkEdTur2HrS.ValueVariant := QrFPFunciHorTur2HrS.Value;
        dmkEdTur2HrA.ValueVariant := QrFPFunciHorTur2HrA.Value;
        dmkEdTur2HrB.ValueVariant := QrFPFunciHorTur2HrB.Value;
        dmkEdTur2HrE.ValueVariant := QrFPFunciHorTur2HrE.Value;
        dmkEdTur2HrC.ValueVariant := QrFPFunciHorTur2HrC.Value;
        //
      end;
      ImgTipo.SQLType  := SQLType;
      ConfereHoras(1);
      ConfereHoras(2);
      dmkEdTur1Ent.SetFocus;
    end;
  end;
end;

procedure TFmFPFunci.MostraFormFPFunciTre(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmFPFunciTre, FmFPFunciTre, afmoNegarComAviso) then
  begin
    FmFPFunciTre.ImgTipo.SQLType := SQLType;
    FmFPFunciTre.FCodiAtual := QrFPFunciCodigo.Value;
    FmFPFunciTre.FQryFPFunciTre := QrFPFunciTre;
    if SQLType = stUpd then
    begin
      FmFPFunciTre.FCtrlAtual := QrFPFunciTreControle.Value;
      FmFPFunciTre.EdNome.Text := QrFPFunciTreNome.Value;
      FmFPFunciTre.EdCargaHoras.ValueVariant := QrFPFunciTreCargaHoras.Value;
      FmFPFunciTre.TPDataIni.Date := QrFPFunciTreDataIni.Value;
      FmFPFunciTre.TPDataFIm.Date := QrFPFunciTreDataFim.Value;
      FmFPFunciTre.EdPathCertif.Text := QrFPFunciTrePathCertif.Value;
    end else begin
      FmFPFunciTre.FCtrlAtual := 0;
    end;
    FmFPFunciTre.ShowModal;
    FmFPFunciTre.Destroy;
  end;
end;

procedure TFmFPFunci.BitBtn7Click(Sender: TObject);
begin
  MostraEdicaoIts(0, stLok);
end;

procedure TFmFPFunci.BitBtn6Click(Sender: TObject);
var
  i, Incidencia, Evento, EveMeses, Conta, CtaMesesR, Codigo, Controle,
  ProporciHT, Eve_Ciclo: Integer;
  EveValPer, CtaIncPer: Double;
  Eve_DtCicl: String;
begin
  Incidencia := RGIncidencia.ItemIndex;
  //
  Evento := Geral.IMV(EdEvento.Text);
  if (Incidencia = 0) and (Evento = 0) then
  begin
    Geral.MB_Aviso(
    'Ao selecionar a incid�ncia "Evento" � obrigat�ria a sele��o do evento!');
    EdEvento.SetFocus;
    Exit;
  end;
  //
  Conta := Geral.IMV(EdConta.Text);
  if (Incidencia = 1) and (Conta = 0) then
  begin
    Geral.MB_Aviso(
    'Ao selecionar a incid�ncia "Conta" � obrigat�ria a sele��o da conta (plano de contas)!');
    EdConta.SetFocus;
    Exit;
  end;
  //
  EveMeses   := dmkEdEve_Meses.ValueVariant;
  CtaMesesR  := dmkEdCta_MesesR.ValueVariant;
  //
  EveValPer  := dmkEdEve_ValPer.ValueVariant;
  CtaIncPer  := dmkEdCta_IncPer.ValueVariant;
          //
  ProporciHT := Geral.BoolToInt(CkProporciHT.Checked);
  //
  if CkEve_Ciclo.Checked and (dmkEdEve_DtCicl.ValueVariant < 15462) then // 01/05/1942
  begin
    Geral.MB_Aviso(
    'Data inicial inv�lida para o per�odo de ac�mulo de incid�ncia (PAI)!');
    dmkEdEve_DtCicl.SetFocus;
    Exit;
  end;
  Eve_Ciclo  := Geral.BoolToInt(CkEve_Ciclo.Checked);
  Eve_DtCicl := Geral.FDT(dmkEdEve_DtCicl.ValueVariant, 1);
  //
  Codigo     := QrFPFunciCodigo.Value;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('FPFunciEve', 'Controle',
    ImgTipo.SQLType, QrFPFunciEveControle.Value);
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'FPFunciEve', False, [
    'Incidencia', 'Evento', 'Eve_Incid', 'Eve_ValPer','Eve_Meses',
    'Conta', 'Cta_IncPer', 'Cta_MesesR', 'ProporciHT', 'Unidade',
    'Eve_Ciclo', 'Eve_DtCicl'
    ],[ 'Codigo', 'Controle'], [
    Incidencia, Evento, RGEve_Incid.ItemIndex, EveValPer, EveMeses,
    Conta, CtaIncPer, CtaMesesR, ProporciHT, EdUnidade.Text,
    Eve_Ciclo, Eve_DtCicl
    ], [Codigo, Controle], True);
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM fpfuncical WHERE Controle=:P0');
  Dmod.QrUpd.Params[0].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO fpfuncical SET Controle=:P0, TipoCalc=:P1');
  for i := 0 to CLTipoCalc.Items.Count -1 do
  begin
    if CLTipoCalc.Checked[i] then
    begin
      Dmod.QrUpd.Params[00].AsInteger := Controle;
      Dmod.QrUpd.Params[01].AsInteger := i + 1;
      Dmod.QrUpd.ExecSQL;
    end;
  end;
  //
  ReopenFPFunciEve(Controle);
  MostraEdicaoIts(0, stLok);
end;

procedure TFmFPFunci.BtAlteraItsClick(Sender: TObject);
begin
  MostraEdicaoIts(1, stUpd);
end;

procedure TFmFPFunci.QrFPFunciEveCalcFields(DataSet: TDataSet);
begin
  QrFPFunciEveNOME_EVE_INCID.Value :=
    RGEve_Incid.Items[QrFPFunciEveEve_Incid.Value];
  QrFPFunciEveNOME_INCIDENCIA.Value :=
    RGIncidencia.Items[QrFPFunciEveIncidencia.Value];
end;

procedure TFmFPFunci.QrFPFunciEveBeforeClose(DataSet: TDataSet);
begin
  BtAlteraIts.Enabled := False;
  BtExcluiIts.Enabled := False;
  QrFPFunciCal.Close;
end;

procedure TFmFPFunci.QrFPFunciEveAfterOpen(DataSet: TDataSet);
begin
  BtAlteraIts.Enabled := QrFPFunciEve.RecordCount > 0;
  BtExcluiIts.Enabled := QrFPFunciEve.RecordCount > 0;
end;

procedure TFmFPFunci.BtExcluiItsClick(Sender: TObject);
begin
  //if
  UMyMod.SQLDel1(Dmod.QrUpd, QrFPFunciEve, 'FPFunciEve', 'Controle',
    QrFPFunciEveControle.Value, True,
    'Confirma a exclus�o do evento selecionado?', True);
end;

procedure TFmFPFunci.BtFPFunciTreDelClick(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do treinamento selecionado?',
  'fpfuncitre', 'Controle', QrFPFunciTreControle.Value, DMod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrFPFunciTre, QrFPFunciTreCodigo,
      QrFPFunciTreControle.Value);
    ReopenFPFunciTre(Controle);
  end;
end;

procedure TFmFPFunci.BtFPFunciTreInsClick(Sender: TObject);
begin
  MostraFormFPFunciTre(stIns);
end;

procedure TFmFPFunci.BtFPFunciTreUpdClick(Sender: TObject);
begin
  MostraFormFPFunciTre(stUpd);
end;

procedure TFmFPFunci.PageControl2Change(Sender: TObject);
begin
  MostraEdicaoIts(0, stLok);
  GBCntrl.Visible :=
    (PageControl2.ActivePageIndex < 6) and (ImgTipo.SQLType = stLok);
end;

procedure TFmFPFunci.BtExcluiClick(Sender: TObject);
begin
  MostraEdicaoIts(0, stLok);
end;

procedure TFmFPFunci.BitBtn9Click(Sender: TObject);
begin
  MostraEdicaoIts(2, stIns);
end;

procedure TFmFPFunci.BitBtn4Click(Sender: TObject);
begin
  // reabrir pois o c�culo pode ter sido excluido na altera��o
  if ImgTipo.SQLType = stUpd then
    ReopenFPFunciFer(QrFPFunciFerCalculo.Value);
  MostraEdicaoIts(0, stLok);
end;

procedure TFmFPFunci.BitBtn3Click(Sender: TObject);
var
  Calculo, Empresa, Codigo, Controle: Integer;
  MeHEMan1xx, MeHEMan200, MediaCoMan, MediaAdMan: Double;
  DtIniPA, DtFimPA, DtIniPecun, DtFimPecun, DtIniPG, DtFimPG, DtFimFeria,
  DtAvisoFer, DtRecPgto, DtSolPecun, DtSolPrP13, DtRetorno: String;
begin
  CalculaDiasFerias();
  Empresa := QrFPFunciEmpresa.Value;
  Codigo  := QrFPFunciCodigo.Value;
  if ImgTipo.SQLType = stIns then
  begin
    Calculo := 0;
  end else begin
    Calculo := QrFPFunciFerCalculo.Value;
  end;
  MeHEMan1xx := dmkEdMeHEMan1xx.ValueVariant;
  MeHEMan200 := dmkEdMeHEMan200.ValueVariant;
  MediaCoMan := dmkEdMediaCoMan.ValueVariant;
  MediaAdMan := dmkEdMediaAdMan.ValueVariant;
  //
  DtIniPA    := Geral.FDT(dmkEdDtIniPA.ValueVariant, 1);
  DtFimPA    := Geral.FDT(IncMonth(dmkEdDtIniPA.ValueVariant, 12) -1, 1);
  DtIniPG    := Geral.FDT(dmkEdDtIniPG.ValueVariant, 1);
  DtFimPG    := Geral.FDT(dmkEdDtFimPG.ValueVariant, 1);
  //
  DtIniPecun := Geral.FDT(dmkEdDtIniPecun.ValueVariant, 1);
  DtFimPecun := Geral.FDT(dmkEdDtFimPecun.ValueVariant, 1);
  DtFimFeria := Geral.FDT(dmkEdDtIniPG.ValueVariant + 29, 1);
  DtAvisoFer := Geral.FDT(dmkEdDtAvisoFer.ValueVariant, 1);
  DtRecPgto  := Geral.FDT(dmkEdDtRecPgto.ValueVariant, 1);
  DtSolPecun := Geral.FDT(dmkEdDtSolPecun.ValueVariant, 1);
  DtSolPrP13 := Geral.FDT(dmkEdDtSolPrP13.ValueVariant, 1);
  DtRetorno  := Geral.FDT(dmkEdDtRetorno.ValueVariant, 1);
  //
  //
  Controle := UMyMod.BuscaEmLivreY_Def('FPFunciFer', 'Controle',
    ImgTipo.SQLType, QrFPFunciFerControle.Value);
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'FPFunciFer', False, [
    'MeHEMan1xx', 'MeHEMan200', 'MediaCoMan', 'MediaAdMan', 'DtIniPA',
    'DtFimPA', 'DiasFaltI', 'DiasFaltP', 'DiasGozar', 'DiasPecun',
    'DtIniPecun', 'DtFimPecun', 'DtIniPG', 'DtFimFeria', 'DtAvisoFer',
    'DtRecPgto', 'DtSolPecun', 'DtSolPrP13', 'DtRetorno', 'Pecun',
    'DtFimPG', 'Empresa', 'Codigo', 'Calculo'
    ],['Controle' ], [
    MeHEMan1xx, MeHEMan200, MediaCoMan, MediaAdMan, DtIniPA,
    DtFimPA, FDiasFaltI, FDiasFaltP, FDiasGozar, FDiasPecun,
    DtIniPecun, DtFimPecun, DtIniPG, DtFimFeria, DtAvisoFer,
    DtRecPgto, DtSolPecun, DtSolPrP13, DtRetorno, RGPecun.Itemindex,
    DtFimPG, Empresa, Codigo, Calculo
    ], [Controle], True);
  //
  ReopenFPFunciFer(Calculo);
  MostraEdicaoIts(0, stLok);
end;

procedure TFmFPFunci.QrFPFunciFerBeforeClose(DataSet: TDataSet);
begin
  BtAlteraFer.Enabled := False;
  BtExcluiFer.Enabled := False;
end;

procedure TFmFPFunci.QrFPFunciFerAfterOpen(DataSet: TDataSet);
begin
  BtAlteraFer.Enabled := (QrFPFunciFer.RecordCount > 0);// and (QrFPFunciFerCalculo.Value = 0);
  BtExcluiFer.Enabled := (QrFPFunciFer.RecordCount > 0);// and (QrFPFunciFerCalculo.Value = 0);
end;

procedure TFmFPFunci.QrFPFunciFerCalcFields(DataSet: TDataSet);
begin
  QrFPFunciFerDTINIPECUN_TXT.Value := Geral.FDT(QrFPFunciFerDtIniPecun.Value, 3);
  QrFPFunciFerDTFIMPECUN_TXT.Value := Geral.FDT(QrFPFunciFerDtFimPecun.Value, 3);
  QrFPFunciFerDTSOLPECUN_TXT.Value := Geral.FDT(QrFPFunciFerDtSolPecun.Value, 3);
  QrFPFunciFerDTSOLPRP13_TXT.Value := Geral.FDT(QrFPFunciFerDtSolPrP13.Value, 3);
  QrFPFunciFerPECUNIO_TXT.Value    :=
    dmkPF.EscolhaDe2Str(QrFPFunciFerPecun.Value > 0, 'S', 'N');
end;

procedure TFmFPFunci.BtAlteraFerClick(Sender: TObject);
{$IFDEF FOLHA_PAGAMENTO}
var
  Calculo: Integer;
{$ENDIF}
begin
{$IFDEF FOLHA_PAGAMENTO}
  Calculo := QrFPFunciFerCalculo.Value;
  if Calculo > 0 then
  begin
    if Geral.MB_Pergunta('Para alterar as configura��es das f�rias ' +
    'selecionadas o c�lculo j� existenete (n� ' + IntToStr(Calculo) +
    ') deve ser exclu�do! Deseja excluir este c�culo para efetuar a altera��o ' +
    ' na configura��o das f�rias selecionadas?') = ID_YES then
    begin
      DmSal.ExcluiCalculoFolha(Calculo);
      MostraEdicaoIts(2, stUpd);
    end;
  end else MostraEdicaoIts(2, stUpd);
{$ENDIF}
end;

procedure TFmFPFunci.BtExcluiFerClick(Sender: TObject);
{$IFDEF FOLHA_PAGAMENTO}
var
  Calculo: Integer;
  Continua: Boolean;
{$ENDIF}
begin
{$IFDEF FOLHA_PAGAMENTO}
  if Geral.MB_Pergunta(
  'Confirma a exclus�o da programa��o de f�rias selecionada?') = ID_YES then
  begin
    Continua := False;
    Calculo := QrFPFunciFerCalculo.Value;
    if Calculo > 0 then
    begin
      if Geral.MB_Pergunta('Para excluir as configura��es das f�rias ' +
      'selecionadas o c�lculo j� existenete (n� ' + IntToStr(Calculo) +
      ') deve ser exclu�do! Deseja excluir este c�culo para excluir a ' +
      'configura��o das f�rias selecionadas?') = ID_YES then
      Continua := DmSal.ExcluiCalculoFolha(Calculo);
    end else Continua := True;
    if Continua then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM fpfuncifer WHERE ');
      Dmod.QrUpd.SQL.Add('Empresa=:P0 AND Codigo=:P1 AND Calculo=:P2');
      Dmod.QrUpd.Params[00].AsInteger := QrFPFunciFerEmpresa.Value;
      Dmod.QrUpd.Params[01].AsInteger := QrFPFunciFerCodigo.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrFPFunciFerCalculo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      ReopenFPFunciFer(0);
    end;
  end;
{$ENDIF}
end;

procedure TFmFPFunci.dmkEdDiasFaltIChange(Sender: TObject);
begin
  CalculaDiasFerias();
end;

procedure TFmFPFunci.RGPecunClick(Sender: TObject);
begin
  CalculaDiasFerias();
end;

procedure TFmFPFunci.dmkEdDtIniPGExit(Sender: TObject);
begin
  CalculaDiasFerias();
end;

procedure TFmFPFunci.CalculaDiasFerias();
{$IFDEF FOLHA_PAGAMENTO}
var
  Resposta: Integer;
{$ENDIF}
begin
{$IFDEF FOLHA_PAGAMENTO}
  FDiasFaltI := dmkEdDiasFaltI.ValueVariant;
  QrFaltas.Close;
  QrFaltas.Params[0].AsInteger := FDiasFaltI;
  QrFaltas.Open;
  if QrFaltas.RecordCount = 0 then
  begin
    Resposta := Geral.MB_Pergunta('N�o foi localizado nenhum registro ' +
    'contendo informa��es de dias a reduzir do per�odo de gozo das f�rias.' +
    PChar(13)+PChar(10)+'Deseja verificar a tabela de faltas injustificadas ' +
    'que determinam esta redu��o?');
    case Resposta of
      ID_YES:
      begin
        Application.CreateForm(TFmFPFeraiFal, FmFPFeraiFal);
        FmFPFeraiFal.ShowModal;
        FmFPFeraiFal.Destroy;
        //
        BitBtn3Click(Self);
        Exit;
      end;
      ID_CANCEL: Exit;
    end;
  end;
  FDiasFaltP := 30 - QrFaltasDias.Value;
  FDiasGozar := 30 - FDiasFaltP;
  //
  if RGPecun.ItemIndex = 0 then
  begin
    // N�o vende 10 dias
    FDiasPecun  := 0;
    dmkEdDtIniPecun.Text := '';
    dmkEdDtFimPecun.Text := '';
  end else begin
    // Vende 1/3 (Pec�nio)
    FDiasPecun  := Trunc(FDiasGozar / 3);
    FDiasGozar  := FDiasGozar - FDiasPecun;
    dmkEdDtIniPecun.ValueVariant := dmkEdDtIniPG.ValueVariant + FDiasGozar;
    dmkEdDtFimPecun.ValueVariant := dmkEdDtIniPecun.ValueVariant + FDiasPecun -1;
  end;
  dmkEdDiasGozar.ValueVariant  := FDiasGozar;
  dmkEdDiasPecun.ValueVariant  := FDiasPecun;
  dmkEdDtFimPG.ValueVariant    := dmkEdDtIniPG.ValueVariant + FDiasGozar - 1;
  dmkEdDtAvisoFer.ValueVariant := dmkEdDtIniPG.ValueVariant - 30;
  dmkEdDtRecPgto.ValueVariant  := dmkEdDtIniPG.ValueVariant - 2;
  dmkEdDtRetorno.ValueVariant  := dmkEdDtIniPG.ValueVariant + FDiasGozar;
  case RGPecun.ItemIndex of
    0: dmkEdDtSolPecun.Text := '';
    1: dmkEdDtSolPecun.ValueVariant := dmkEdDtFimPA.ValueVariant -15;
  end;
{$ENDIF}  
end;

procedure TFmFPFunci.dmkEdDtIniPAChange(Sender: TObject);
begin
  dmkEdDtFimPA.ValueVariant := IncMonth(dmkEdDtIniPA.ValueVariant, 12) -1;
end;

procedure TFmFPFunci.dmkEdDtFimPAChange(Sender: TObject);
begin
  case RGPecun.ItemIndex of
    0: dmkEdDtSolPecun.Text := '';
    1: dmkEdDtSolPecun.ValueVariant := dmkEdDtFimPA.ValueVariant -15;
  end;
end;

procedure TFmFPFunci.frxAvisoFeriasGetValue(const VarName: String;
  var Value: Variant);
begin
 if AnsiCompareText(VarName, 'ImagemExiste') = 0 then
   Value := FileExists(QrFPFunciFoto.Value)
 else if AnsiCompareText(VarName, 'ImagemCaminho') = 0 then
   Value := QrFPFunciFoto.Value

end;

procedure TFmFPFunci.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmFPFunci.PMImprimePopup(Sender: TObject);
begin
  Avisodefrias1.Enabled              := QrFPFunciFer.RecordCount > 0;
  Solicitaodeabonopecunirio1.Enabled := QrFPFunciFer.RecordCount > 0;
  Solicitaoda1parcelado131.Enabled   := QrFPFunciFer.RecordCount > 0;
end;

procedure TFmFPFunci.Avisodefrias1Click(Sender: TObject);
begin
  DModG.ReopenEndereco(QrFPFunciEmpresa.Value);
  //
  MyObjects.frxMostra(frxAvisoFerias, 'Aviso de f�rias');
end;

procedure TFmFPFunci.Solicitaodeabonopecunirio1Click(Sender: TObject);
begin
  DModG.ReopenEndereco(QrFPFunciEmpresa.Value);
  //
  MyObjects.frxMostra(frxSolicitaAbono, 'Solicita��o de abono pecuni�rio');
end;

procedure TFmFPFunci.Solicitaoda1parcelado131Click(Sender: TObject);
begin
  DModG.ReopenEndereco(QrFPFunciEmpresa.Value);
  //
  MyObjects.frxMostra(frxSolicita1a_13o, 'Solicita��o da 1� parcela do 13�');
end;

procedure TFmFPFunci.QrFPFunciEveAfterScroll(DataSet: TDataSet);
begin
  ReopenFPFunciCal(0);
end;

procedure TFmFPFunci.DBGEventosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'ProporciHT' then
    MeuVCLSkin.DrawGrid(DBGEventos, Rect, 1, QrFPFunciEveProporciHT.Value);
end;

procedure TFmFPFunci.DBGrid2CellClick(Column: TColumn);
var
  Nome: String;
begin
  Screen.Cursor := crHourGlass;
  try
    if (QrFPFunciTre.State <> dsInactive) and (QrFPFunciTre.RecordCount > 0) then
    begin
      if Column.FieldName = 'PathCertif' then
      begin
        Nome := QrFPFunciTrePathCertif.Value;
        if FileExists(Nome) then
          ShellExecute(Application.Handle, PChar('open'), PChar(Nome), PChar(''), nil, SW_NORMAL)
        else
          Geral.MB_Aviso('O arquivo n�o foi localizado!');
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFPFunci.SpeedButton6Click(Sender: TObject);
{$IFDEF FOLHA_PAGAMENTO}
var
  MyCursor : TCursor;
{$ENDIF}
begin
{$IFDEF FOLHA_PAGAMENTO}
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmFPFunca, FmFPFunca);
  finally
    Screen.Cursor := MyCursor;
  end;
  FmFPFunca.ShowModal;
  FmFPFunca.Destroy;
  //
  QrFPFunca.Close;
  QrFPFunca.Open;
{$ENDIF}  
end;

procedure TFmFPFunci.SpeedButton7Click(Sender: TObject);
{$IFDEF FOLHA_PAGAMENTO}
var
  MyCursor : TCursor;
{$ENDIF}
begin
{$IFDEF FOLHA_PAGAMENTO}
  //if UMyMod.AcessoNegadoAoForm('Perfis', 'Contas', 0) then Exit;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmFPDepto, FmFPDepto);
  finally
    Screen.Cursor := MyCursor;
  end;
  FmFPDepto.ShowModal;
  FmFPDepto.Destroy;
  //
  QrFPDepto.Close;
  QrFPDepto.Open;
{$ENDIF}  
end;

procedure TFmFPFunci.SpeedButton8Click(Sender: TObject);
begin
  EdCBO2002.Text :=
  CuringaLoc.CriaForm4('CodID', 'Descricao', 'FPCBO02', Dmod.MyDB, CO_VAZIO);
end;

procedure TFmFPFunci.SpeedButton9Click(Sender: TObject);
begin
  EdCBO1994.Text :=
  CuringaLoc.CriaForm4('CodID', 'Descricao', 'FPCBO94', Dmod.MyDB, CO_VAZIO);
end;

procedure TFmFPFunci.RGANTipoClick(Sender: TObject);
begin
  case RGANTipo.Itemindex of
    0:
    begin
      dmkEdANIniHr.Text := '22:00';
      dmkEdANFimHr.Text := '05:00';
      dmkEdANMinHr.Text := '00:52:30';
    end;
    1:
    begin
      dmkEdANIniHr.Text := '21:00';
      dmkEdANFimHr.Text := '05:00';
      dmkEdANMinHr.Text := '01:00:00';
    end;
    2:
    begin
      dmkEdANIniHr.Text := '20:00';
      dmkEdANFimHr.Text := '04:00';
      dmkEdANMinHr.Text := '01:00:00';
    end;
  end;
end;

procedure TFmFPFunci.BtAlteraHorClick(Sender: TObject);
begin
  MostraEdicaoIts(3, stUpd)
end;

procedure TFmFPFunci.BitBtn8Click(Sender: TObject);
begin
  MostraEdicaoIts(0, stLok)
end;

procedure TFmFPFunci.BitBtn5Click(Sender: TObject);
var
  Tur1Ent, Tur1DeI, Tur1DeF, Tur1Sai,
  Tur1HrT, Tur1HrN, Tur1HrS, Tur1HrA, Tur1HrB, Tur1HrE, Tur1HrC,
  //
  Tur2Ent, Tur2DeI, Tur2DeF, Tur2Sai,
  Tur2HrT, Tur2HrN, Tur2HrS, Tur2HrA, Tur2HrB, Tur2HrE, Tur2HrC,
  //
  Tur1HrZ: String;
  Codigo, Controle: Integer;
begin
  if not ConfereHoras(1) then Exit;
  if not ConfereHoras(2) then Exit;
  Tur1Ent := {Geral.FDT(}dmkEdTur1Ent.Text{, 102)};
  Tur1DeI := {Geral.FDT(}dmkEdTur1DeI.Text{, 102)};
  Tur1DeF := {Geral.FDT(}dmkEdTur1DeF.Text{, 102)};
  Tur1Sai := {Geral.FDT(}dmkEdTur1Sai.Text{, 102)};

  Tur1HrT := {Geral.FDT(}dmkEdTur1HrT.Text{, 102)};
  Tur1HrN := {Geral.FDT(}dmkEdTur1HrN.Text{, 102)};
  Tur1HrS := {Geral.FDT(}dmkEdTur1HrS.Text{, 102)};
  Tur1HrA := {Geral.FDT(}dmkEdTur1HrA.Text{, 102)};
  Tur1HrB := {Geral.FDT(}dmkEdTur1HrB.Text{, 102)};
  Tur1HrE := {Geral.FDT(}dmkEdTur1HrE.Text{, 102)};
  Tur1HrC := {Geral.FDT(}dmkEdTur1HrC.Text{, 102)};
  //
  Tur2Ent := {Geral.FDT(}dmkEdTur2Ent.Text{, 102)};
  Tur2DeI := {Geral.FDT(}dmkEdTur2DeI.Text{, 102)};
  Tur2DeF := {Geral.FDT(}dmkEdTur2DeF.Text{, 102)};
  Tur2Sai := {Geral.FDT(}dmkEdTur2Sai.Text{, 102)};

  Tur2HrT := {Geral.FDT(}dmkEdTur2HrT.Text{, 102)};
  Tur2HrN := {Geral.FDT(}dmkEdTur2HrN.Text{, 102)};
  Tur2HrS := {Geral.FDT(}dmkEdTur2HrS.Text{, 102)};
  Tur2HrA := {Geral.FDT(}dmkEdTur2HrA.Text{, 102)};
  Tur2HrB := {Geral.FDT(}dmkEdTur2HrB.Text{, 102)};
  Tur2HrE := {Geral.FDT(}dmkEdTur2HrE.Text{, 102)};
  Tur2HrC := {Geral.FDT(}dmkEdTur2HrC.Text{, 102)};
  //
  Tur1HrZ := Geral.FDT(StrToTime(Tur1HrB) + StrToTime(Tur1HrC) + StrToTime(Tur1HrE), 100);
  if Int(StrToTime(Tur1HrS) * SegundosNoDia) <>
     Int(StrToTime(Tur1HrZ) * SegundosNoDia) then
  begin
    if Geral.MB_Pergunta('A soma dos tempos de horas normais + ' +
    'horas extras e horas de banco de horas (' + Tur1HrZ +
    ') n�o confere com o tempo de horas trabalhadas (' + Tur1HrS +
    ') no turno 1. Deseja continuar assim mesmo?') <> ID_YES then
      Exit;
  end;

  Codigo     := QrFPFunciCodigo.Value;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('FPFunciHor', 'Controle',
    ImgTipo.SQLType, QrFPFunciHorControle.Value);
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'FPFunciHor', False, [
    'Tur1Ent', 'Tur1DeI', 'Tur1DeF', 'Tur1Sai',
    'Tur1HrT', 'Tur1HrN', 'Tur1HrS', 'Tur1HrA', 'Tur1HrB', 'Tur1HrE', 'Tur1HrC',

    'Tur2Ent', 'Tur2DeI', 'Tur2DeF', 'Tur2Sai',
    'Tur2HrT', 'Tur2HrN', 'Tur2HrS', 'Tur2HrA', 'Tur2HrB', 'Tur2HrE', 'Tur2HrC'

    ],[ 'Codigo', 'Controle'], [
    Tur1Ent, Tur1DeI, Tur1DeF, Tur1Sai,
    Tur1HrT, Tur1HrN, Tur1HrS, Tur1HrA, Tur1HrB, Tur1HrE, Tur1HrC,

    Tur2Ent, Tur2DeI, Tur2DeF, Tur2Sai,
    Tur2HrT, Tur2HrN, Tur2HrS, Tur2HrA, Tur2HrB, Tur2HrE, Tur2HrC

    ], [Codigo, Controle], True);
  //
  ReopenFPFunciHor(Controle);
  MostraEdicaoIts(0, stLok)
end;

procedure TFmFPFunci.ReopenFPFunciHor(Controle: Integer);
begin
  QrFPFunciHor.Close;
  QrFPFunciHor.Params[0].AsInteger := QrFPFunciCodigo.Value;
  QrFPFunciHor.Open;
  //
  if Controle > 0 then
    QrFPFunciHor.Locate('Controle', Controle, []);
end;

procedure TFmFPFunci.ReopenFPFunciTre(Controle: Integer);
begin
  QrFPFunciTre.Close;
  QrFPFunciTre.Params[0].AsInteger := QrFPFunciCodigo.Value;
  QrFPFunciTre.Open;
  //
  if Controle > 0 then
    QrFPFunciTre.Locate('Controle', Controle, []);
end;

procedure TFmFPFunci.QrFPFunciHorBeforeClose(DataSet: TDataSet);
begin
  BtAlteraHor.Enabled := False;
  BtExcluiHor.Enabled := False;
end;

procedure TFmFPFunci.QrFPFunciHorAfterOpen(DataSet: TDataSet);
begin
  BtAlteraHor.Enabled := QrFPFunciHor.RecordCount > 0;
  BtExcluiHor.Enabled := QrFPFunciHor.RecordCount > 0;
end;

procedure TFmFPFunci.BtExcluiHorClick(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o do registro selecionado?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fpfuncihor ');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrFPFunciHorControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenFPFunciHor(0);
  end;
end;

procedure TFmFPFunci.dmkEdTur1EntExit(Sender: TObject);
begin
  ConfereHoras(1);
end;

procedure TFmFPFunci.dmkEdTur1DeIExit(Sender: TObject);
begin
  ConfereHoras(1);
end;

procedure TFmFPFunci.dmkEdTur1DeFExit(Sender: TObject);
begin
  ConfereHoras(1);
end;

procedure TFmFPFunci.dmkEdTur1SaiExit(Sender: TObject);
begin
  ConfereHoras(1);
end;

function TFmFPFunci.ConfereHoras(Turno: Integer): Boolean;
  function Tempos(const HoI1, HoI2, HoF1, HoF2, ANi0, ANf1, ANf2, HNo: Double;
  var HrTc, HrNc: Double): Boolean;
  var
    HFim: Double;
  begin
    // Acerta hor�rio final
    Result := True;
    if HoF1 < HoI1 then
      HFim := HoF2
    else
      HFim := HoF1;
    //
    // Caso A: Come�a depois do hor�rio noturno final
    // e come�a antes e termina antes do hor�rio noturno inicial
    // N�O TEM ADICIONAL NOTURNO
    if (HoI1 >= ANf1) and (HoI1 <= ANi0) and (HFim <= ANi0) then
    begin
      // Soma simples de total de horas trabalhadas
      HrTc := HFim - HoI1
    end else

    // Caso B: Come�a depois do final do hor�rio noturno
    // e come�a antes do in�cio do hor�rio noturno
    // e termina depois do in�cio do hor�rio noturno
    // e termina dentro do (ou final) do hor�rio noturno
    if ((HoI1 >= ANf1) and (HoI1 <= ANi0) and (HFim > ANi0) and (HFim <= ANf2))
    // Caso C: Come�a depois do final do hor�rio noturno
    // e come�a antes do in�cio do hor�rio noturno
    // e termina depois do in�cio do hor�rio noturno
    // e termina depois do final do hor�rio noturno
    // CASO IGUAL AO CASO B, POIS A HORA NOTURNA SE EXTENDE AT� A SAIDA DO TURNO
    or ((HoI1 >= ANf1) and (HoI1 <= ANi0) and (HFIm > ANi0) and (HFim > ANf2)) then
    begin
      // Soma simples de total de horas da parte que n�o tem adicional noturno
      HrTc := ANi0 - HoI1;
      // Tempo de Adicional notuno j� calculado pela hora noturna
      HrNc := ((HFim - ANi0) / HNo) / 24;
    end else
    // Caso D: Come�a durante o hor�rio noturno
    // e termina durante o hor�rio noturno
    if ( (HoI2 >= ANi0)  and (HFIm <= ANf2) )
    // Caso E: Come�a durante o hor�rio noturno
    // e termina depois do final do hor�rio noturno
    // CASO IGUAL AO CASO D, POIS A HORA NOTURNA SE EXTENDE AT� A SAIDA DO TURNO
    or ( (HoI2 >= ANi0) and (HFim >= ANf2) ) then
    begin
      // N�o h� horas simples
      HrTc := 0;
      // Tempo de Adicional notuno j� calculado pela hora noturna
      if HFim > HoI1 then
        HrNc := ((HFim - HoI1) / HNo) / 24
      else
        HrNc := ((HFim - HoI2) / HNo) / 24;
    end else
    // Parei aqui! Fazer outros casos adicional noturno!
    Geral.MB_Erro('Situa��o de hor�rio n�o prevista!  ' + sLineBreak +
    'AVISE A DERMATEK!');
  end;
var
  HNo,  // Hora noturna conforme cadastro
  Ent1, // Hora de entrada de 0 a 1 (0:00 a 23:59)
  Ent2, // Hora de entrada de 0 a 1 (0:00 a 23:59 do dia seguinte)
  DeI1, // Hora inicial do descanso no meio do turno de 0 a 1 (0:00 a 23:59)
  DeI2, // Hora inicial do descanso no meio do turno de 0 a 2 (0:00 a 23:59 do dia seguinte)
  DeF1, // Hora final do descanso no meio do turno  de 0 a 1 (0:00 a 23:59)
  DeF2, // Hora final do descanso no meio do turno de 0 a 2 (0:00 a 23:59 do dia seguinte)
  Sai1, // Hora do t�rmino do turno de 0 a 1 (0:00 a 23:59)
  Sai2, // Hora do t�rmino do turno de 0 a 2 (0:00 a 23:59 do dia seguinte)
  {
  HrT,  // Total de horas normais trabalhadas (no dmkEdit)
  HrE,  // Total de horas extras (no dmkEdit)
  HrA,  // Total de horas alimenta��o (no dmkEdit)
  HrN,  // Total de horas de adicional noturno (no dmkEdit)
  HnB,  // Total de horas para adicionar ao banco de horas (no dmkEdit)
  }
  ANi0, // In�cio do adicional noturno (no dmkEdit) de 0 a 1 (0:00 a 23:59)
  //ANi2, // In�cio do adicional noturno (no dmkEdit) 0 a 2 (0:00 a 23:59 do dia seguinte)
  ANf1, // Final do adicional noturno de 0 a 1 (0:00 a 23:59)
  ANf2, // Final do adicional noturno de 0 a 2 (0:00 a 23:59 do dia seguinte)
  //
  HrTc, // C�lculo te�rico do total de horas normais trabalhadas
  HrAc, // C�lculo te�rico do total de horas alimenta��o
  HrNc, // C�lculo te�rico do total de horas de adicional noturno
  HrSc, // Soma dos c�lculos te�ricos de horas trabalhadas e adicional noturno
  // HrBc n�o calcula?
  //HrBc, // C�lculo te�rico do total de horas a adicionar ao banco de horas
  Aux1, Aux2: Double;
  x{, Txt1, Txt2}: String;
  //
  TemIntervalo, Trabalhou: Boolean;
begin
  HNo := QrFPFunciANMinHr.Value;
  if HNo = 0 then HNo := 1 / 24;
  x    := IntToStr(Turno);
  //
  Ent1 := TdmkEdit(dmkEdTur(x+'Ent')).ValueVariant;
  DeI1 := TdmkEdit(dmkEdTur(x+'DeI')).ValueVariant;
  DeF1 := TdmkEdit(dmkEdTur(x+'DeF')).ValueVariant;
  Sai1 := TdmkEdit(dmkEdTur(x+'Sai')).ValueVariant;
  ANi0 := QrFPFunciANIniHr.Value;
  ANf1 := QrFPFunciANFimHr.Value;
  //
  TemIntervalo := Int(DeI1 * SegundosNoDia) <> Int(DeF1 * SegundosNoDia);
  Trabalhou := (Int(Ent1 * SegundosNoDia) <> 0) or (Int(Sai1 * SegundosNoDia) <> 0) or
     (Int(DeI1 * SegundosNoDia) <> 0) or (Int(DeF1 * SegundosNoDia) <> 0);
  // Ajusta hor�rios quando hor�rio menor que etapa anterior (1h + 24h = 25h)
  if TemIntervalo then
  begin
    if DeI1 < Ent1 then DeI2 := DeI1 + 1 else DeI2 := DeI1;
    if DeF1 < DeI2 then DeF2 := DeF1 + 1 else DeF2 := DeF1;
    if Sai1 < DeF2 then Sai2 := Sai1 + 1 else Sai2 := Sai1;
  end else begin
    DeI2 := DeI1;
    DeF2 := DeF1;
    if Sai1 < Ent1 then Sai2 := Sai1 + 1 else Sai2 := Sai1;
  end;
  if ANf1 < ANi0 then
  begin
    ANf2 := ANf1 + 1;
    Ent2 := Ent1 + 1;
  end else begin
    ANf2 := ANf1;
    Ent2 := Ent1;
  end;
  //
  HrSc := 0;
  HrTc := 0;
  HrAc := 0;
  HrNc := 0;


  if Trabalhou then
  begin
    // caso n�o haja intervalo
    Tempos(Ent1, Ent2, Sai1, Sai2, ANi0, ANf1, ANf2, HNo, HrTc, HrNc);
    if TemIntervalo then
    begin
      Tempos(DeI1, DeI2, DeF1, DeF2, ANi0, ANf1, ANf2, HNo, Aux1, Aux2);
      HrTc := HrTc - Aux1;
      HrNc := HrNc - Aux2;
    end;
    // Soma as horas normais trabalhadas a as que tem adicional noturno j� calculando a hora noturna
    HrSc := HrTc + HrNc;
    //
    if not TemIntervalo then
    begin
      // Adicional alimenta��o por n�o haver intervalo
      // Verifica se tem hora alimenta��o
      // se o hor�rio for cont�nuo e superior a 6 horas ent�o tem uma "hora alimenta��o"
      if HrSc > 6.016/24 then
        HrAc := 1/24 else
      // se o hor�rio for cont�nuo e superior a 4 horas ent�o tem 15 min de "hora alimenta��o"
      if HrSc > 4.016/24 then
        HrAc := 0.25/24 // 15 minutos
      // caso contr�rio n�o tem "hora alimenta��o"
      else HrAc := 0;
    end;
  end;

  // Verifica se n�o extrapolou 24 horas
  if (HrSc > 1) or (HrSc < 0) then
  begin
    HrSc := 0;
    HrTc := 0;
    HrAc := 0;
    HrNc := 0;
  end;

  // Mostra os resultados nos labels
  // Horas trabalhadas
  TLabel(LaTur(x+'HrT')).Caption := Geral.FDT(HrTc, 100);
  // Horas noturnas
  TLabel(LaTur(x+'HrN')).Caption := Geral.FDT(HrNc, 100);
  // Horas somadas de trabalhadas e noturnas
  // testar
  TLabel(LaTur(x+'HrS')).Caption := Geral.FDT(HrSc, 100);

  // Horas alimenta��o
  TLabel(LaTur(x+'HrA')).Caption := Geral.FDT(HrAc, 100);

  //

  Result := True;
end;

procedure TFmFPFunci.QrFPFunciHorCalcFields(DataSet: TDataSet);
begin
  QrFPFunciHorTXT1HrT.Value := Geral.FDT(QrFPFunciHorTur1HrT.Value, 100);
  QrFPFunciHorTXT1HrN.Value := Geral.FDT(QrFPFunciHorTur1HrN.Value, 100);
  QrFPFunciHorTXT1HrS.Value := Geral.FDT(QrFPFunciHorTur1HrS.Value, 100);
  QrFPFunciHorTXT1HrA.Value := Geral.FDT(QrFPFunciHorTur1HrA.Value, 100);
  QrFPFunciHorTXT1HrB.Value := Geral.FDT(QrFPFunciHorTur1HrB.Value, 100);
  QrFPFunciHorTXT1HrE.Value := Geral.FDT(QrFPFunciHorTur1HrE.Value, 100);
  QrFPFunciHorTXT1HrC.Value := Geral.FDT(QrFPFunciHorTur1HrC.Value, 100);
end;

procedure TFmFPFunci.QrFPFunciTreAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrFPFunciTre.RecordCount > 0;
  //
  BtFPFunciTreUpd.Enabled := Habilita;
  BtFPFunciTreDel.Enabled := Habilita;
end;

procedure TFmFPFunci.QrFPFunciTreBeforeClose(DataSet: TDataSet);
begin
  BtFPFunciTreUpd.Enabled := False;
  BtFPFunciTreDel.Enabled := False;
end;

function TFmFPFunci.dmkEdTur(SubNome: String): TdmkEdit;
var
  I: Integer;
begin
  Result := nil;
  for i := 0 to FmFPFunci.ComponentCount - 1 do
  begin
    if Components[i] is TdmkEdit then
    begin
      if TdmkEdit(Components[i]).Name = 'dmkEdTur' + SubNome then
      begin
        Result := TdmkEdit(FmFPFunci.Components[i]);
        Break;
      end;
    end;
  end;
end;

function TFmFPFunci.LaTur(SubNome: String): TLabel;
var
  I: Integer;
begin
  Result := nil;
  for i := 0 to FmFPFunci.ComponentCount - 1 do
  begin
    if Components[i] is TLabel then
    begin
      if TLabel(Components[i]).Name = 'LaTur' + SubNome then
      begin
        Result := TLabel(FmFPFunci.Components[i]);
        Break;
      end;
    end;
  end;
end;

procedure TFmFPFunci.ColoreTexto(NomeComp: String);
var
  Hr1, Hr2: TTime;
  Cor: Integer;
  txt: String;
begin
  Hr1 := TdmkEdit(dmkEdTur(NomeComp)).ValueVariant;
  txt := TLabel(LaTur(NomeComp)).Caption;
  if txt = '' then txt := '00:00';
  Hr2 := StrToTime(txt);
  Cor := dmkPF.EscolhaDe3Int(Hr1, Hr2, clGreen, clBlue, clRed);
  TdmkEdit(dmkEdTur(NomeComp)).Font.Color := Cor;
  TLabel(LaTur(NomeComp)).Font.Color := Cor;
end;

procedure TFmFPFunci.dmkEdTur1HrTExit(Sender: TObject);
begin
  ColoreTexto('1HrT');
end;

procedure TFmFPFunci.dmkEdTur1HrNExit(Sender: TObject);
begin
  ColoreTexto('1HrN');
end;

procedure TFmFPFunci.dmkEdTur1HrSExit(Sender: TObject);
begin
  ColoreTexto('1HrS');
end;

procedure TFmFPFunci.dmkEdTur1HrAExit(Sender: TObject);
begin
  ColoreTexto('1HrA');
end;

procedure TFmFPFunci.dmkEdTur1HrBExit(Sender: TObject);
begin
  ColoreTexto('1HrB');
end;

procedure TFmFPFunci.dmkEdTur1HrEExit(Sender: TObject);
begin
  ColoreTexto('1HrE');
end;

procedure TFmFPFunci.dmkEdTur1HrCExit(Sender: TObject);
begin
  ColoreTexto('1HrC');
end;

end.

