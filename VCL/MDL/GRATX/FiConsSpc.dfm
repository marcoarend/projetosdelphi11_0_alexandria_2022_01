object FmFiConsSpc: TFmFiConsSpc
  Left = 339
  Top = 185
  Caption = 'PRD-FICSM-002 :: Ficha de Consumo - Itens N'#227'o B'#225'sicos'
  ClientHeight = 624
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnBase: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 462
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 29
      Align = alTop
      TabOrder = 0
      object Label5: TLabel
        Left = 12
        Top = 8
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label3: TLabel
        Left = 112
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object EdGG1Dest: TdmkEdit
        Left = 28
        Top = 4
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNomeGG1Dest: TdmkEdit
        Left = 168
        Top = 4
        Width = 829
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object PnLeft: TPanel
      Left = 0
      Top = 29
      Width = 1008
      Height = 433
      Align = alClient
      TabOrder = 1
      object PCOrigens: TPageControl
        Left = 1
        Top = 1
        Width = 1006
        Height = 431
        ActivePage = TabSheet5
        Align = alClient
        TabOrder = 0
        object TabSheet5: TTabSheet
          Caption = ' Adicionados '
          object Splitter1: TSplitter
            Left = 509
            Top = 0
            Width = 5
            Height = 403
            ExplicitLeft = 448
            ExplicitTop = 12
          end
          object DBGSrcArt: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 509
            Height = 403
            Align = alLeft
            DataSource = DsSrcArt
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            OnDblClick = DBGSrcArtDblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Nivel1'
                Title.Caption = 'Produto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_GG1_Sorc'
                Title.Caption = 'Descri'#231#227'o do produto'
                Width = 236
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ITENS'
                Title.Caption = 'Itens'
                Width = 28
                Visible = True
              end>
          end
          object Panel8: TPanel
            Left = 514
            Top = 0
            Width = 484
            Height = 403
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            ExplicitLeft = 370
            ExplicitWidth = 628
            object DBGSrcTeC: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 484
              Height = 403
              Align = alClient
              DataSource = DsSrcTeC
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              OnCellClick = DBGSrcTeCCellClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_COR'
                  Title.Caption = 'Cor'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_TAM'
                  Title.Caption = 'Tamanho'
                  Width = 55
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ITENS'
                  Title.Caption = 'Itens'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
        object TabSheet6: TTabSheet
          Caption = ' Adicionar '
          ImageIndex = 1
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 700
            Height = 403
            Align = alLeft
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 700
              Height = 25
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 621
              object Label1: TLabel
                Left = 4
                Top = 7
                Width = 40
                Height = 13
                Caption = 'Produto:'
              end
              object EdGraGru1: TdmkEditCB
                Left = 48
                Top = 3
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnRedefinido = EdGraGru1Redefinido
                DBLookupComboBox = CBGraGru1
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBGraGru1: TdmkDBLookupComboBox
                Left = 107
                Top = 3
                Width = 458
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'Nome'
                ListSource = DsGraGru1
                TabOrder = 1
                dmkEditCB = EdGraGru1
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
            object Panel3: TPanel
              Left = 0
              Top = 25
              Width = 700
              Height = 378
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              ExplicitWidth = 621
              object PnVV: TPanel
                Left = 0
                Top = 0
                Width = 645
                Height = 378
                Align = alClient
                TabOrder = 0
                ExplicitWidth = 566
                object DBGFiConsOri: TdmkDBGridZTO
                  Left = 1
                  Top = 1
                  Width = 643
                  Height = 376
                  Align = alClient
                  DataSource = DsFiConsOri
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                  OnCellClick = DBGFiConsOriCellClick
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NO_COR'
                      Title.Caption = 'Cor'
                      Width = 140
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_TAM'
                      Title.Caption = 'Tamanho'
                      Width = 55
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SIGLAUNIDMED'
                      Title.Caption = 'Un.med.'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_Obrigatorio'
                      Title.Caption = 'Ob?'
                      Width = 30
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PARTE'
                      Title.Caption = 'Parte'
                      Width = 100
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_PRD_TAM_COR_SUBS'
                      Title.Caption = 'Reduzido substituto'
                      Width = 179
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SIGLAUNIDMED_s'
                      Title.Caption = 'Un.med.'
                      Visible = True
                    end>
                end
              end
              object Panel5: TPanel
                Left = 645
                Top = 0
                Width = 55
                Height = 378
                Align = alRight
                TabOrder = 1
                ExplicitLeft = 566
                object Sb_NtoN: TSpeedButton
                  Left = 4
                  Top = 20
                  Width = 45
                  Height = 25
                  Caption = 'N >> N'
                  OnClick = Sb_NtoNClick
                end
                object Sb_DelN: TSpeedButton
                  Left = 4
                  Top = 136
                  Width = 45
                  Height = 25
                  Caption = '0 << N'
                end
                object Sb_1toN: TSpeedButton
                  Left = 4
                  Top = 48
                  Width = 45
                  Height = 25
                  Caption = '1 > N'
                  OnClick = Sb_1toNClick
                end
                object Sb_Del1: TSpeedButton
                  Left = 4
                  Top = 164
                  Width = 45
                  Height = 25
                  Caption = '0 < 1'
                end
                object Sb_1to1: TSpeedButton
                  Left = 4
                  Top = 76
                  Width = 45
                  Height = 25
                  Caption = '1 > 1'
                  OnClick = Sb_1to1Click
                end
              end
            end
          end
          object PnDest: TPanel
            Left = 700
            Top = 0
            Width = 298
            Height = 403
            Align = alClient
            TabOrder = 1
            ExplicitLeft = 621
            ExplicitWidth = 377
            object PCGrades: TPageControl
              Left = 1
              Top = 1
              Width = 296
              Height = 401
              ActivePage = TabSheet2
              Align = alClient
              TabOrder = 0
              ExplicitWidth = 375
              object TabSheet1: TTabSheet
                Caption = ' Quantidades '
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object GradeK: TStringGrid
                  Left = 0
                  Top = 0
                  Width = 367
                  Height = 373
                  Align = alClient
                  ColCount = 2
                  DefaultColWidth = 65
                  DefaultRowHeight = 18
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goFixedColClick]
                  TabOrder = 0
                  OnDrawCell = GradeKDrawCell
                  OnFixedCellClick = GradeKFixedCellClick
                  RowHeights = (
                    18
                    18)
                end
              end
              object TabSheet2: TTabSheet
                Caption = ' Ativos '
                ImageIndex = 1
                ExplicitWidth = 367
                object GradeA: TStringGrid
                  Left = 0
                  Top = 0
                  Width = 288
                  Height = 373
                  Align = alClient
                  ColCount = 2
                  DefaultColWidth = 65
                  DefaultRowHeight = 18
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  TabOrder = 0
                  ExplicitWidth = 367
                  RowHeights = (
                    18
                    18)
                end
              end
              object TabSheet3: TTabSheet
                Caption = ' Reduzidos '
                ImageIndex = 2
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object GradeC: TStringGrid
                  Left = 0
                  Top = 0
                  Width = 621
                  Height = 403
                  Align = alClient
                  ColCount = 2
                  DefaultColWidth = 65
                  DefaultRowHeight = 18
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  TabOrder = 0
                  RowHeights = (
                    18
                    18)
                end
              end
              object TabSheet4: TTabSheet
                Caption = ' X '
                ImageIndex = 3
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object GradeX: TStringGrid
                  Left = 0
                  Top = 0
                  Width = 369
                  Height = 403
                  Align = alClient
                  ColCount = 2
                  DefaultColWidth = 65
                  DefaultRowHeight = 18
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  TabOrder = 0
                  RowHeights = (
                    18
                    18)
                end
              end
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 483
        Height = 32
        Caption = ' Ficha de Consumo - Itens N'#227'o B'#225'sicos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 483
        Height = 32
        Caption = ' Ficha de Consumo - Itens N'#227'o B'#225'sicos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 483
        Height = 32
        Caption = ' Ficha de Consumo - Itens N'#227'o B'#225'sicos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 510
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 554
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label4: TLabel
        Left = 180
        Top = 2
        Width = 9
        Height = 13
        Caption = '...'
      end
      object Label6: TLabel
        Left = 180
        Top = 24
        Width = 9
        Height = 13
        Caption = '...'
      end
      object Label7: TLabel
        Left = 364
        Top = 2
        Width = 9
        Height = 13
        Caption = '...'
      end
      object Label8: TLabel
        Left = 364
        Top = 24
        Width = 9
        Height = 13
        Caption = '...'
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        Visible = False
      end
    end
  end
  object QrGraGru1: TMySQLQuery
    Database = FmTempDB.DBTeste
    SQL.Strings = (
      'SELECT Nivel1, CodUsu, Nome, GraTamCad, GG1Subst'
      'FROM gragru1'
      'ORDER BY Nome')
    Left = 780
    Top = 120
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrGraGru1GraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrGraGru1GG1Subst: TIntegerField
      FieldName = 'GG1Subst'
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 780
    Top = 168
  end
  object QrFiConsOri: TMySQLQuery
    Database = FmTempDB.DBTeste
    AfterScroll = QrFiConsOriAfterScroll
    SQL.Strings = (
      'SELECT ggx.Controle GraGruX, gti.Nome NO_TAM,'
      'gcc.Nome NO_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED,'
      'ggc.GraCorCad, ggx.GraGruC,'
      'ggx.GraGru1, ggx.GraTamI , gti.Codigo GraTamCad,'
      'fco.Controle FiConsOri, fco.GGXSubs, fco.Parte, fco.Obrigatorio,'
      'IF(fco.Obrigatorio IS NULL, "",'
      '  IF(fco.Obrigatorio=1, "Sim", "N'#227'o")) NO_Obrigatorio,'
      'gpc.Nome NO_PARTE,'
      'CONCAT(gg1s.Nome,  '
      
        '  IF(gtis.PrintTam=0 OR gtis.Codigo IS NULL, "", CONCAT(" ", gti' +
        's.Nome)),'
      
        '  IF(gccs.PrintCor=0 OR gccs.Codigo IS NULL, "", CONCAT(" ", gcc' +
        's.Nome)))  '
      '  NO_PRD_TAM_COR_SUBS, unms.Sigla SIGLAUNIDMED_s,'
      'unms.CodUsu CODUSUUNIDMED_s, unms.Nome NOMEUNIDMED_s'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      ''
      'LEFT JOIN ficonsori  fco ON fco.GGXSorc=ggx.Controle'
      '                        AND fco.GG1Dest>0'
      'LEFT JOIN graparcad  gpc ON gpc.Codigo=fco.Parte'
      'LEFT JOIN gragrux    ggxs ON ggxs.Controle=fco.GGXSubs'
      'LEFT JOIN gragruc    ggcs ON ggcs.Controle=ggxs.GraGruC'
      'LEFT JOIN gracorcad  gccs ON gccs.Codigo=ggcs.GraCorCad  '
      'LEFT JOIN gratamits  gtis ON gtis.Controle=ggxs.GraTamI'
      'LEFT JOIN gragru1    gg1s ON gg1s.Nivel1=ggxs.GraGru1'
      'LEFT JOIN unidmed    unms ON unms.Codigo=gg1s.UnidMed'
      ''
      'WHERE ggx.GraGru1>0'
      'ORDER BY ggx.Controle')
    Left = 77
    Top = 235
    object QrFiConsOriGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrFiConsOriNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrFiConsOriNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrFiConsOriGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrFiConsOriGraGruC: TIntegerField
      FieldName = 'GraGruC'
      Required = True
    end
    object QrFiConsOriGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Required = True
    end
    object QrFiConsOriGraTamI: TIntegerField
      FieldName = 'GraTamI'
      Required = True
    end
    object QrFiConsOriGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrFiConsOriFiConsOri: TIntegerField
      FieldName = 'FiConsOri'
    end
    object QrFiConsOriGGXSubs: TIntegerField
      FieldName = 'GGXSubs'
    end
    object QrFiConsOriParte: TIntegerField
      FieldName = 'Parte'
    end
    object QrFiConsOriObrigatorio: TSmallintField
      FieldName = 'Obrigatorio'
    end
    object QrFiConsOriNO_Obrigatorio: TWideStringField
      FieldName = 'NO_Obrigatorio'
      Required = True
      Size = 3
    end
    object QrFiConsOriNO_PARTE: TWideStringField
      FieldName = 'NO_PARTE'
      Size = 60
    end
    object QrFiConsOriNO_PRD_TAM_COR_SUBS: TWideStringField
      FieldName = 'NO_PRD_TAM_COR_SUBS'
      Size = 157
    end
    object QrFiConsOriSIGLAUNIDMED: TStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrFiConsOriCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrFiConsOriNOMEUNIDMED: TStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrFiConsOriSIGLAUNIDMED_s: TStringField
      FieldName = 'SIGLAUNIDMED_s'
      Size = 6
    end
    object QrFiConsOriCODUSUUNIDMED_s: TIntegerField
      FieldName = 'CODUSUUNIDMED_s'
    end
    object QrFiConsOriNOMEUNIDMED_s: TStringField
      FieldName = 'NOMEUNIDMED_s'
      Size = 30
    end
  end
  object DsFiConsOri: TDataSource
    DataSet = QrFiConsOri
    Left = 77
    Top = 283
  end
  object DqAux: TMySQLDirectQuery
    Database = FmTempDB.DBTeste
    Left = 205
    Top = 275
  end
  object QrSrcArt: TMySQLQuery
    Database = FmTempDB.DBTeste
    BeforeClose = QrSrcArtBeforeClose
    AfterScroll = QrSrcArtAfterScroll
    SQL.Strings = (
      'SELECT gg11.Nivel1, gg11.Nome NO_GG1_Sorc, '
      'COUNT(fcs.Controle) ITENS  '
      'FROM ficonsspc fcs '
      'LEFT JOIN gragrux    ggx1 ON ggx1.Controle=fcs.GGXSorc '
      'LEFT JOIN gragru1    gg11 ON gg11.Nivel1=ggx1.GraGru1  '
      'WHERE fcs.GG1Dest=25'
      'GROUP BY gg11.Nivel1'
      'ORDER BY gg11.Nome ')
    Left = 418
    Top = 242
    object QrSrcArtNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrSrcArtNO_GG1_Sorc: TWideStringField
      FieldName = 'NO_GG1_Sorc'
      Size = 120
    end
    object QrSrcArtITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsSrcArt: TDataSource
    DataSet = QrSrcArt
    Left = 418
    Top = 290
  end
  object QrSrcTeC: TMySQLQuery
    Database = FmTempDB.DBTeste
    SQL.Strings = (
      'SELECT ggxa.GraTamI, gtia.Nome NO_TAM, ggxa.GraGruC, '
      'ggca.GraCorCad, gcca.Nome NO_COR, ggxa.Controle GraGruX,'
      'COUNT(fcb.Controle) ITENS'
      'FROM ficonsspc fcb'
      ''
      'LEFT JOIN gragrux    ggxa ON ggxa.Controle=fcb.GGXSorc'
      'LEFT JOIN gragruc    ggca ON ggca.Controle=ggxa.GraGruC '
      'LEFT JOIN gracorcad  gcca ON gcca.Codigo=ggca.GraCorCad '
      'LEFT JOIN gratamits  gtia ON gtia.Controle=ggxa.GraTamI '
      'LEFT JOIN gragru1    gg1a ON gg1a.Nivel1=ggxa.GraGru1'
      ''
      'LEFT JOIN gragrux    ggxb ON ggxb.Controle=fcb.GGXSubs'
      'LEFT JOIN gragruc    ggcb ON ggcb.Controle=ggxb.GraGruC '
      'LEFT JOIN gracorcad  gccb ON gccb.Codigo=ggcb.GraCorCad '
      'LEFT JOIN gratamits  gtib ON gtib.Controle=ggxb.GraTamI '
      'LEFT JOIN gragru1    gg1b ON gg1b.Nivel1=ggxb.GraGru1'
      ''
      'LEFT JOIN graparcad  gpc  ON gpc.Codigo=fcb.Parte '
      'WHERE fcb.GG1Dest=25'
      'AND ggxa.GraGru1=429'
      'GROUP BY ggxa.GraTamI, ggxa.GraGruC, ggca.GraCorCad')
    Left = 478
    Top = 242
    object QrSrcTeCGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrSrcTeCNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrSrcTeCGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrSrcTeCGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrSrcTeCNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrSrcTeCITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrSrcTeCGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
  end
  object DsSrcTeC: TDataSource
    DataSet = QrSrcTeC
    Left = 478
    Top = 290
  end
end
