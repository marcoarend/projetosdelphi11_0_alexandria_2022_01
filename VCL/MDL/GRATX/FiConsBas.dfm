object FmFiConsBas: TFmFiConsBas
  Left = 339
  Top = 185
  Caption = 'PRD-FICSM-001 :: Ficha de Consumo - Itens B'#225'sicos'
  ClientHeight = 435
  ClientWidth = 650
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 293
    Width = 650
    Height = 28
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 617
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 650
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    ExplicitWidth = 617
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label3: TLabel
      Left = 100
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object EdGG1Dest: TdmkEdit
      Left = 12
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNomeGG1Dest: TdmkEdit
      Left = 100
      Top = 36
      Width = 509
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 650
    Height = 181
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    ExplicitWidth = 617
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label1: TLabel
      Left = 12
      Top = 56
      Width = 106
      Height = 13
      Caption = 'Reduzido preferencial:'
    end
    object SbGGXSrc: TSpeedButton
      Left = 512
      Top = 72
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbGGXSrcClick
    end
    object Label2: TLabel
      Left = 12
      Top = 96
      Width = 96
      Height = 13
      Caption = 'Reduzido substituto:'
    end
    object SbGGXSorc: TSpeedButton
      Left = 512
      Top = 112
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbGGXSorcClick
    end
    object Label4: TLabel
      Left = 12
      Top = 136
      Width = 150
      Height = 13
      Caption = 'Parte do Artigo ('#193'rea da Pe'#231'a) :'
    end
    object SbParte: TSpeedButton
      Left = 408
      Top = 152
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbParteClick
    end
    object Label8: TLabel
      Left = 432
      Top = 136
      Width = 58
      Height = 13
      Caption = 'Quantidade:'
    end
    object Label7: TLabel
      Left = 540
      Top = 56
      Width = 95
      Height = 13
      Caption = 'Unidade de medida:'
      FocusControl = DBEdit1
    end
    object Label9: TLabel
      Left = 540
      Top = 96
      Width = 95
      Height = 13
      Caption = 'Unidade de medida:'
      FocusControl = DBEdit2
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object CBGGXSorc: TdmkDBLookupComboBox
      Left = 68
      Top = 72
      Width = 441
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGGXSorc
      TabOrder = 2
      dmkEditCB = EdGGXSorc
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdGGXSorc: TdmkEditCB
      Left = 12
      Top = 72
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBGGXSorc
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdGGXSubs: TdmkEditCB
      Left = 12
      Top = 112
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBGGXSubs
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBGGXSubs: TdmkDBLookupComboBox
      Left = 68
      Top = 112
      Width = 441
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGGXSubs
      TabOrder = 4
      dmkEditCB = EdGGXSubs
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdParte: TdmkEditCB
      Left = 12
      Top = 152
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBParte
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBParte: TdmkDBLookupComboBox
      Left = 68
      Top = 152
      Width = 337
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsGraParCad
      TabOrder = 6
      dmkEditCB = EdParte
      QryCampo = 'Cargo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdQtdUso: TdmkEdit
      Left = 432
      Top = 152
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object CkObrigatorio: TCheckBox
      Left = 544
      Top = 156
      Width = 85
      Height = 17
      Caption = 'Obrigat'#243'rio.'
      Checked = True
      State = cbChecked
      TabOrder = 8
    end
    object DBEdit1: TDBEdit
      Left = 540
      Top = 72
      Width = 96
      Height = 21
      TabStop = False
      DataField = 'SIGLAUNIDMED'
      DataSource = DsGGXSorc
      TabOrder = 9
    end
    object DBEdit2: TDBEdit
      Left = 540
      Top = 112
      Width = 96
      Height = 21
      TabStop = False
      DataField = 'SIGLAUNIDMED'
      DataSource = DsGGXSubs
      TabOrder = 10
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 650
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 617
    object GB_R: TGroupBox
      Left = 602
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 569
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 554
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 521
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 418
        Height = 32
        Caption = 'Ficha de Consumo - Itens B'#225'sicos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 418
        Height = 32
        Caption = 'Ficha de Consumo - Itens B'#225'sicos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 418
        Height = 32
        Caption = 'Ficha de Consumo - Itens B'#225'sicos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 321
    Width = 650
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    ExplicitWidth = 617
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 646
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 613
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 365
    Width = 650
    Height = 70
    Align = alBottom
    TabOrder = 5
    ExplicitWidth = 617
    object PnSaiDesis: TPanel
      Left = 504
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 471
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 502
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 469
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrGGXSorc: TMySQLQuery
    Database = FmTempDB.DBTeste
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)), '
      
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nom' +
        'e))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED '
      'FROM gragrux ggx'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.Controle'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed '
      'WHERE ggx.Ativo=1'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 160
    Top = 47
    object QrGGXSorcGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Required = True
    end
    object QrGGXSorcControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGGXSorcNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXSorcSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXSorcCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXSorcNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXSorc: TDataSource
    DataSet = QrGGXSorc
    Left = 162
    Top = 94
  end
  object QrGGXSubs: TMySQLQuery
    Database = FmTempDB.DBTeste
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)), '
      
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nom' +
        'e))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED '
      'FROM gragrux ggx'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.Controle'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE ggx.Ativo=1'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 232
    Top = 47
    object QrGGXSubsGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Required = True
    end
    object QrGGXSubsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGGXSubsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXSubsSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXSubsCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXSubsNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXSubs: TDataSource
    DataSet = QrGGXSubs
    Left = 234
    Top = 94
  end
  object QrGraParCad: TMySQLQuery
    Database = FmTempDB.DBTeste
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM graparcad'
      'ORDER BY Nome')
    Left = 312
    Top = 48
    object QrGraParCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraParCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsGraParCad: TDataSource
    DataSet = QrGraParCad
    Left = 310
    Top = 94
  end
end
