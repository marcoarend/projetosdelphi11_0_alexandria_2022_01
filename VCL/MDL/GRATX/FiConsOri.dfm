object FmFiConsOri: TFmFiConsOri
  Left = 339
  Top = 185
  Caption = 'PRD-FICSM-003 :: Ficha de Consumo - Dados Gerais N'#227'o B'#225'sicos'
  ClientHeight = 394
  ClientWidth = 706
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 706
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 699
    object GB_R: TGroupBox
      Left = 658
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 651
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 610
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 603
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 582
        Height = 32
        Caption = 'Ficha de Consumo - Dados Gerais N'#227'o B'#225'sicos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 582
        Height = 32
        Caption = 'Ficha de Consumo - Dados Gerais N'#227'o B'#225'sicos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 582
        Height = 32
        Caption = 'Ficha de Consumo - Dados Gerais N'#227'o B'#225'sicos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 706
    Height = 232
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 699
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 706
      Height = 232
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 699
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 706
        Height = 232
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 699
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 702
          Height = 106
          Align = alTop
          Caption = ' Dados do cabe'#231'alho:'
          Enabled = False
          TabOrder = 0
          ExplicitWidth = 695
          object Label5: TLabel
            Left = 12
            Top = 20
            Width = 79
            Height = 13
            Caption = 'Produto Destino:'
          end
          object Label3: TLabel
            Left = 12
            Top = 60
            Width = 82
            Height = 13
            Caption = 'Reduzido origem:'
          end
          object EdGG1Dest: TdmkEdit
            Left = 12
            Top = 36
            Width = 57
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdGG1NomeDest: TdmkEdit
            Left = 72
            Top = 36
            Width = 613
            Height = 21
            Enabled = False
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdGGXSorc: TdmkEdit
            Left = 12
            Top = 76
            Width = 57
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdGGXPRD_TAM_CORSorc: TdmkEdit
            Left = 72
            Top = 76
            Width = 613
            Height = 21
            Enabled = False
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object GroupBox3: TGroupBox
          Left = 2
          Top = 121
          Width = 702
          Height = 109
          Align = alClient
          Caption = ' Dados Gerais do Grupo: '
          TabOrder = 1
          ExplicitWidth = 695
          object Panel5: TPanel
            Left = 2
            Top = 15
            Width = 698
            Height = 92
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            ExplicitWidth = 691
            object Label2: TLabel
              Left = 8
              Top = 44
              Width = 28
              Height = 13
              Caption = 'Parte:'
            end
            object Label1: TLabel
              Left = 92
              Top = 4
              Width = 96
              Height = 13
              Caption = 'Reduzido substituto:'
            end
            object Label6: TLabel
              Left = 8
              Top = 4
              Width = 14
              Height = 13
              Caption = 'ID:'
            end
            object Label4: TLabel
              Left = 588
              Top = 44
              Width = 58
              Height = 13
              Caption = 'Quantidade:'
            end
            object Label7: TLabel
              Left = 588
              Top = 4
              Width = 95
              Height = 13
              Caption = 'Unidade de medida:'
              FocusControl = DBEdit1
            end
            object CBParte: TdmkDBLookupComboBox
              Left = 64
              Top = 60
              Width = 405
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsGraParCad
              TabOrder = 0
              dmkEditCB = EdParte
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object CkObrigatorio: TdmkCheckBox
              Left = 488
              Top = 60
              Width = 97
              Height = 17
              Caption = 'Obrigat'#243'rio.'
              Checked = True
              State = cbChecked
              TabOrder = 1
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
            object EdParte: TdmkEditCB
              Left = 8
              Top = 60
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBParte
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBGGXSubs: TdmkDBLookupComboBox
              Left = 148
              Top = 20
              Width = 433
              Height = 21
              KeyField = 'Controle'
              ListField = 'NO_PRD_TAM_COR'
              ListSource = DsGraGruX
              TabOrder = 3
              dmkEditCB = EdGGXSubs
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdGGXSubs: TdmkEditCB
              Left = 92
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBGGXSubs
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object EdControle: TdmkEdit
              Left = 8
              Top = 20
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Controle'
              UpdCampo = 'Controle'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdQtdUso: TdmkEdit
              Left = 588
              Top = 60
              Width = 97
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              QryCampo = 'Controle'
              UpdCampo = 'Controle'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object DBEdit1: TDBEdit
              Left = 588
              Top = 20
              Width = 96
              Height = 21
              TabStop = False
              DataField = 'SIGLAUNIDMED'
              DataSource = DsGraGruX
              TabOrder = 7
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 280
    Width = 706
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 699
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 702
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 695
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 324
    Width = 706
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitWidth = 699
    object PnSaiDesis: TPanel
      Left = 560
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 553
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 558
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 551
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 65527
  end
  object QrGraGruX: TMySQLQuery
    Database = FmTempDB.DBTeste
    SQL.Strings = (
      'SELECT ggx.Controle, CONCAT(gg1.Nome,  '
      
        '  IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.N' +
        'ome)),'
      
        '  IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.N' +
        'ome)))'
      '  NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR')
    Left = 172
    Top = 52
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 172
    Top = 100
  end
  object QrGraParCad: TMySQLQuery
    Database = FmTempDB.DBTeste
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM GraParCad'
      'ORDER BY Nome')
    Left = 252
    Top = 52
  end
  object DsGraParCad: TDataSource
    DataSet = QrGraParCad
    Left = 252
    Top = 100
  end
end
