unit GraPckCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, dmkEditCB, dmkDBLookupComboBox;

type
  TFmGraPckCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrGraPckCad: TMySQLQuery;
    DsGraPckCad: TDataSource;
    QrGraPckQtd: TMySQLQuery;
    DsGraPckQtd: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrGraGru1: TMySQLQuery;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    DsGraGru1: TDataSource;
    SbGraGru1: TSpeedButton;
    Label3: TLabel;
    Label4: TLabel;
    QrGraPckCadCodigo: TIntegerField;
    QrGraPckCadCodUsu: TIntegerField;
    QrGraPckCadNome: TWideStringField;
    QrGraPckCadGraGru1: TIntegerField;
    QrGraPckCadLk: TIntegerField;
    QrGraPckCadDataCad: TDateField;
    QrGraPckCadDataAlt: TDateField;
    QrGraPckCadUserCad: TIntegerField;
    QrGraPckCadUserAlt: TIntegerField;
    QrGraPckCadAlterWeb: TSmallintField;
    QrGraPckCadAWServerID: TIntegerField;
    QrGraPckCadAWStatSinc: TSmallintField;
    QrGraPckCadAtivo: TSmallintField;
    QrGraPckCadNO_GraGru1: TWideStringField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    QrGraPckCadGraTamCad: TIntegerField;
    GradeK: TStringGrid;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraPckCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraPckCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrGraPckCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrGraPckCadBeforeClose(DataSet: TDataSet);
    procedure SbGraGru1Click(Sender: TObject);
    procedure GradeKDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure EdCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGru1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraGraPckQtd(SQLType: TSQLType);
    procedure CopiaNome(Sender: TObject; var Key: Word; Shift: TShiftState);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenGraPckQtd(Controle: Integer);

  end;

var
  FmGraPckCad: TFmGraPckCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModProd, GraGruN, GraPckQtd;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraPckCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraPckCad.MostraGraPckQtd(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmGraPckQtd, FmGraPckQtd, afmoNegarComAviso) then
  begin
    FmGraPckQtd.ImgTipo.SQLType := SQLType;
    //FmGraPckQtd.FQrCab := QrGraPckCad;
    FmGraPckQtd.FDsCab := DsGraPckCad;
    //FmGraPckQtd.FQrIts := QrGraPckQtd;
    //
    FmGraPckQtd.FNivel1    := QrGraPckCadGraGru1.Value;
    FmGraPckQtd.FGraTamCad := QrGraPckCadGraTamCad.Value;
    FmGraPckQtd.FGraPckCad := QrGraPckCadCodigo.Value;
    //
    if SQLType = stIns then
      //
    else
    begin
(*
      FmGraPckQtd.EdControle.ValueVariant := QrGraPckQtdControle.Value;
      //
      FmGraPckQtd.EdNome.ValueVariant := MLAGeral.FormataCNPJ_TFT(QrGraPckQtdCNPJ_CPF.Value);
      FmGraPckQtd.EdNomeEmiSac.Text := QrGraPckQtdNome.Value;
      FmGraPckQtd.EdCPF1.ReadOnly := True;
*)
    end;
    FmGraPckQtd.PreparaGrades();
    //
    FmGraPckQtd.ShowModal;
    FmGraPckQtd.Destroy;
  end;
end;

procedure TFmGraPckCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrGraPckCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrGraPckCad, QrGraPckQtd);
end;

procedure TFmGraPckCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrGraPckCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrGraPckQtd);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrGraPckQtd);
end;

procedure TFmGraPckCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraPckCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraPckCad.DefParams;
begin
  VAR_GOTOTABELA := 'grapckcad';
  VAR_GOTOMYSQLTABLE := QrGraPckCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT gpk.*, gg1.GraTamCad, gg1.Nome NO_GraGru1');
  VAR_SQLx.Add('FROM grapckcad gpk');
  VAR_SQLx.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=gpk.GraGru1');
  VAR_SQLx.Add('WHERE gpk.Codigo > 0');
  //
  VAR_SQL1.Add('AND gpk.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND gpk.Nome Like :P0');
  VAR_SQLa.Add('AND (');
  VAR_SQLa.Add('  gpk.Nome LIKE :P0');
  VAR_SQLa.Add('  OR');
  VAR_SQLa.Add('  gg1.Nome LIKE :P0');
  VAR_SQLa.Add(')');
  //
end;

procedure TFmGraPckCad.EdCodigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  CopiaNome(Sender, Key, Shift);
end;

procedure TFmGraPckCad.ItsAltera1Click(Sender: TObject);
begin
  MostraGraPckQtd(stUpd);
end;

procedure TFmGraPckCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmGraPckCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmGraPckCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraPckCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'GraPckQtd', 'Controle', QrGraPckQtdControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrGraPckQtd,
      QrGraPckQtdControle, QrGraPckQtdControle.Value);
    ReopenGraPckQtd(Controle);
  end;
}
end;

procedure TFmGraPckCad.ReopenGraPckQtd(Controle: Integer);
begin
  DmProd.ConfigGradesPack1(QrGraPckCadGraGru1.Value, QrGraPckCadGraTamCad.Value,
  QrGraPckCadCodigo.Value, nil, nil, nil, GradeK);
  (*
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraPckQtd, Dmod.MyDB, [
  'SELECT * ',
  'FROM grapckqtd ',
  'WHERE Codigo=' + Geral.FF0(QrGraPckCadCodigo.Value),
  '']);
  //
  QrGraPckQtd.Locate('Controle?, Controle, []);
*)
end;


procedure TFmGraPckCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraPckCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraPckCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraPckCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraPckCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraPckCad.SbGraGru1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
  end;
  UMyMod.SetaCodigoPesquisado(EdGraGru1, CBGraGru1, QrGraGru1, VAR_CADASTRO);
end;

procedure TFmGraPckCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraPckCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGraPckCadCodigo.Value;
  Close;
end;

procedure TFmGraPckCad.ItsInclui1Click(Sender: TObject);
begin
  MostraGraPckQtd(stIns);
end;

procedure TFmGraPckCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraPckCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'grapckcad');
end;

procedure TFmGraPckCad.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, CodUsu, GraGru1: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  //CodUsu         := ;
  Nome           := EdNome.ValueVariant;
  GraGru1        := EdGraGru1.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(GraGru1 = 0, EdGraGru1, 'Defina o Artigo!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('grapckcad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  CodUsu := Codigo;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'grapckcad', False, [
  'CodUsu', 'Nome', 'GraGru1'], [
  'Codigo'], [
  CodUsu, Nome, GraGru1], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmGraPckCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'grapckcad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'grapckcad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmGraPckCad.BtItsClick(Sender: TObject);
begin
//  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
  MostraGraPckQtd(stIns);
end;

procedure TFmGraPckCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmGraPckCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GradeK.Align := alClient;
  GradeK.ColWidths[0] := 192;
  CriaOForm;
  FSeq := 0;
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
end;

procedure TFmGraPckCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraPckCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraPckCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGraPckCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrGraPckCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraPckCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGraPckCad.QrGraPckCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraPckCad.QrGraPckCadAfterScroll(DataSet: TDataSet);
begin
  ReopenGraPckQtd(0);
end;

procedure TFmGraPckCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrGraPckCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmGraPckCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGraPckCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'grapckcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGraPckCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraPckCad.GradeKDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
  Qtde: Double;
  Txto: String;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := Panel2.Color
  else if GradeK.Cells[ACol, ARow] <> '' then
    Cor := clWindow
  else
    Cor := clMenu;

  //

  if (ACol = 0) or (ARow = 0) then
(*
    MyObjects.DesenhaTextoEmStringGrid(GradeK, Rect, clBlack,
      Cor, taLeftJustify,
      GradeK.Cells[Acol, ARow], 0, 0, False)
*)
  else
  begin
    Qtde := Geral.DMV(GradeK.Cells[Acol, ARow]);
    if Qtde > 0 then
      Txto := FormatFloat(Dmod.FStrFmtQtd, Qtde)
    else
      Txto := '';
    MyObjects.DesenhaTextoEmStringGrid(GradeK, Rect, clBlack, Cor,
      taRightJustify, Txto, 0, 0, False);
  end;
end;

procedure TFmGraPckCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrGraPckCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'grapckcad');
end;

procedure TFmGraPckCad.CBGraGru1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  CopiaNome(Sender, Key, Shift);
end;

procedure TFmGraPckCad.CopiaNome(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdNome.Text := CBGraGru1.Text;
end;

procedure TFmGraPckCad.QrGraPckCadBeforeClose(
  DataSet: TDataSet);
begin
  QrGraPckQtd.Close;
end;

procedure TFmGraPckCad.QrGraPckCadBeforeOpen(DataSet: TDataSet);
begin
  QrGraPckCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

