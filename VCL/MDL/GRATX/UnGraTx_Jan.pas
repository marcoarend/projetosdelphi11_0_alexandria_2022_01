unit UnGraTX_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, mySQLDbTables, DmkGeral, DmkDAC_PF,
  UnInternalConsts, UnDmkProcFunc, frxClass, frxDBSet, Menus, Variants,
  dmkDBLookupComboBox, dmkEditCB, UnDmkEnums, ModProd, UnGrl_Vars,
  mySQLDirectQuery;

type
  TUnGraTX_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure MostraFormGraFibCad(Codigo: Integer);
    procedure MostraFormGraParCad();
    procedure MostraFormGraCmpCad(Codigo: Integer);
    procedure MostraFormGraPckCad(Codigo: Integer);
    procedure MostraFormFiConsBas(QrFiConsBas: TmysqlQuery; SQLType: TSQLType;
              Nivel1, Controle: Integer; GG1_Nome: String);
    procedure MostraFormFiConsSpc(Nivel1, GraTamCad: Integer; GG1_Nome: String);

  end;

var
  GraTX_Jan: TUnGraTX_Jan;

implementation

uses MyDBCheck, ModuleGeral, UnMyObjects, Module, CfgCadLista,
  GraCmpCad, GraFibCad, GraPckCad, FiConsBas, FiConsSpc,
  UMySQLDB;

{ TUnGraTX_Jan }

procedure TUnGraTX_Jan.MostraFormFiConsBas(QrFiConsBas: TmysqlQuery; SQLType:
  TSQLType; Nivel1, Controle: Integer; GG1_Nome: String);
var
  Dq: TmySQLDirectQuery;
begin
  if DBCheck.CriaFm(TFmFiConsBas, FmFiConsBas, afmoNegarComAviso) then
  begin
    FmFiConsBas.ImgTipo.SQLType := SQLType;
    //
    FmFiConsBas.EdControle.ValueVariant      := Controle;
    FmFiConsBas.EdGG1Dest.ValueVariant       := Nivel1;
    FmFiConsBas.EdNomeGG1Dest.ValueVariant   := GG1_Nome;
    FmFiConsBas.FQrIts                       := QrFiConsBas;
    //
    if SQLType = stUpd then
    begin
      Dq := TmySQLDirectQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLDirectQuery0(Dq, DModG.MyCompressDB, [
        'SELECT * ',
        'FROM ficonsbas ',
        'WHERE Controle=' + Geral.FF0(Controle),
        EmptyStr]);
        //
        FmFiConsBas.EdGGXSorc.ValueVariant := USQLDB.Int(Dq, 'GGXSorc');
        FmFiConsBas.EdGGXSubs.ValueVariant := USQLDB.Int(Dq, 'GGXSubs');
        FmFiConsBas.EdParte.ValueVariant   := USQLDB.Int(Dq, 'Parte');
        FmFiConsBas.EdQtdUso.ValueVariant  := USQLDB.Flu(Dq, 'QtdUso');
        FmFiConsBas.CkObrigatorio.Checked  := Geral.IntToBool(USQLDB.Int(Dq, 'Obrigatorio'));
      finally
        Dq.Free;
      end;
    end;
    FmFiConsBas.ShowModal;
    FmFiConsBas.Destroy;
  end;
end;

procedure TUnGraTX_Jan.MostraFormFiConsSpc(Nivel1, GraTamCad: Integer;
  GG1_Nome: String);
begin
  if DBCheck.CriaFm(TFmFiConsSpc, FmFiConsSpc, afmoNegarComAviso) then
  begin
    FmFiConsSpc.ImgTipo.SQLType := stNil;
    //
    FmFiConsSpc.FGG1Dest       := Nivel1;
    FmFiConsSpc.FGraTamCadDest := GraTamCad;
    //
    FmFiConsSpc.EdGG1Dest.ValueVariant     := Nivel1;
    FmFiConsSpc.EdNomeGG1Dest.ValueVariant := GG1_Nome;
    //
    FmFiConsSpc.PreparaGrades();
    FmFiConsSpc.ReopenSrcArt();
    //
    FmFiConsSpc.ShowModal;
    FmFiConsSpc.Destroy;
    //
  end;
end;

procedure TUnGraTX_Jan.MostraFormGraCmpCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmGraCmpCad, FmGraCmpCad, afmoNegarComAviso) then
  begin
    FmGraCmpCad.ShowModal;
    if Codigo <> 0 then
      FmGraCmpCad.LocCod(Codigo, Codigo);
    FmGraCmpCad.Destroy;
  end;
end;

procedure TUnGraTX_Jan.MostraFormGraFibCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmGraFibCad, FmGraFibCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmGraFibCad.LocCod(Codigo, Codigo);
    FmGraFibCad.ShowModal;
    FmGraFibCad.Destroy;
  end;
end;

procedure TUnGraTX_Jan.MostraFormGraParCad();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'graparcad', 60, ncControle,
  'Partes de Artigos (�reas de Pe�as)',
  [], False, Null, [], [], False);
end;

procedure TUnGraTX_Jan.MostraFormGraPckCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmGraPckCad, FmGraPckCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmGraPckCad.LocCod(Codigo, Codigo);
    FmGraPckCad.ShowModal;
    FmGraPckCad.Destroy;
  end;
end;

end.
