unit GraCmpFib;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmGraCmpFib = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdConta: TdmkEdit;
    Label6: TLabel;
    CBFibra: TdmkDBLookupComboBox;
    EdFibra: TdmkEditCB;
    Label1: TLabel;
    SbFibra: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraFibCad: TMySQLQuery;
    QrGraFibCadCodigo: TIntegerField;
    QrGraFibCadNome: TWideStringField;
    DsGraFibCad: TDataSource;
    Label2: TLabel;
    DBEdControle: TdmkDBEdit;
    DBEdCtrlNome: TDBEdit;
    Label4: TLabel;
    EdPercComp: TdmkEdit;
    Label7: TLabel;
    CkImprime: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbFibraClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenFib(Conta: Integer);
  public
    { Public declarations }
    FQrCab, FQrPar, FQrFib: TmySQLQuery;
    FDsPar, FDsCab: TDataSource;
    FContexRef: Integer;
  end;

  var
  FmGraCmpFib: TFmGraCmpFib;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, DmkDAC_PF,
  UnGraTX_Jan, UnGraTX_PF;

{$R *.DFM}

procedure TFmGraCmpFib.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Conta, Fibra, Imprime: Integer;
  PercComp: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := Geral.IMV(DBEdControle.Text);
  Conta          := EdConta.ValueVariant;
  Fibra          := EdFibra.ValueVariant;
  PercComp       := EdPercComp.ValueVariant;
  Imprime        := Geral.BoolToInt(CkImprime.Checked);
  //
  //Evitar duplicação
  if UMyMod.VerificaDuplicadoNovo(SQLType, Dmod.QrAux, Dmod.MyDB, 'gracmpfib',
  'Controle', 'Conta', Controle, Conta, 'Fibra', Fibra, (*ShowMsgDupl*)True) then Exit;
  //
  Conta := UMyMod.BPGS1I32('gracmpfib', 'Conta', '', '', tsPos, SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gracmpfib', False,[
  'Codigo', 'Controle', 'Fibra',
  'PercComp', 'Imprime'], [
  'Conta'], [
  Codigo, Controle, Fibra,
  PercComp, Imprime], [
  Conta], True) then
  begin
    GraTX_PF.CalculaTotaisGraCmpPar(Controle, FQrPar);
    ReopenFib(Conta);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdConta.ValueVariant      := 0;
      EdFibra.ValueVariant      := 0;
      CBFibra.KeyValue          := Null;
      //EdNome.SetFocus;
      EdFibra.SetFocus;
    end else Close;
  end;
end;

procedure TFmGraCmpFib.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraCmpFib.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource   := FDsCab;
  DBEdNome.DataSource     := FDsCab;
  //
  DBEdControle.DataSource := FDsPar;
  DBEdCtrlNome.DataSource := FDsPar;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmGraCmpFib.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDMkDAC_PF.AbreQuery(QrGraFibCad, DMod.MyDB);
end;

procedure TFmGraCmpFib.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraCmpFib.ReopenFib(Conta: Integer);
begin
  if FQrFib <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrFib, FQrFib.Database);
    //
    if Conta <> 0 then
      FQrFib.Locate('Conta', Conta, []);
  end;
end;

procedure TFmGraCmpFib.SbFibraClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  GraTX_Jan.MostraFormGraFibCad(0);
  UMyMod.SetaCodigoPesquisado(EdFIbra, CBFibra, QrGraFibCad, VAR_CADASTRO);
end;

end.
