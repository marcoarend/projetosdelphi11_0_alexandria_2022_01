unit FiConsOri;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, dmkDBLookupComboBox, dmkEditCB, dmkCheckBox, Vcl.Mask,
  dmkDBEdit;

type
  TFmFiConsOri = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrGraGruX: TMySQLQuery;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    DsGraGruX: TDataSource;
    QrGraParCad: TMySQLQuery;
    DsGraParCad: TDataSource;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    GroupBox3: TGroupBox;
    Panel5: TPanel;
    CBParte: TdmkDBLookupComboBox;
    CkObrigatorio: TdmkCheckBox;
    EdParte: TdmkEditCB;
    Label2: TLabel;
    CBGGXSubs: TdmkDBLookupComboBox;
    EdGGXSubs: TdmkEditCB;
    Label1: TLabel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    EdGG1Dest: TdmkEdit;
    EdGG1NomeDest: TdmkEdit;
    Label3: TLabel;
    EdGGXSorc: TdmkEdit;
    EdGGXPRD_TAM_CORSorc: TdmkEdit;
    EdQtdUso: TdmkEdit;
    Label4: TLabel;
    QrGraGruXSIGLAUNIDMED: TStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TStringField;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FAlterou: Boolean;
    (*
    FGGXSubs,
    FParte,
    FObrigatorio: Integer;
    *)
    FQtdUso: Double;
  end;

  var
  FmFiConsOri: TFmFiConsOri;

implementation

uses UnMyObjects, DmkDAC_PF, Module, UMySQLModule;

{$R *.DFM}

procedure TFmFiConsOri.BtOKClick(Sender: TObject);
var
  GG1Dest, Controle, GGXSorc, GGXSubs, Parte, Obrigatorio: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  GG1Dest        := EdGG1Dest.ValueVariant;
  Controle       := EdControle.ValueVariant;
  GGXSorc        := EdGGXSorc.ValueVariant;
  GGXSubs        := EdGGXSubs.ValueVariant;
  Parte          := EdParte.ValueVariant;
  Obrigatorio    := Geral.BoolToInt(CkObrigatorio.Checked);
  //
  Controle := UMyMod.BPGS1I32('ficonsori', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ficonsori', False, [
  'GG1Dest', 'GGXSorc', 'GGXSubs',
  'Parte', 'Obrigatorio'], [
  'Controle'], [
  GG1Dest, GGXSorc, GGXSubs,
  Parte, Obrigatorio], [
  Controle], True) then
  begin
    FAlterou     := True;
    FQtdUso      := EdQtdUso.ValueVariant;
    //
    (*
    FGGXSubs     := GGXSubs;
    FParte       := Parte;
    FObrigatorio := Obrigatorio;
    *)
    //
    Close;
  end;
end;

procedure TFmFiConsOri.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFiConsOri.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFiConsOri.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FAlterou     := False;
  (*
  FGGXSubs     := 0;
  FParte       := 0;
  FObrigatorio := 1;
  *)
  FQtdUso      := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrGraParCad, Dmod.MyDB);
end;

procedure TFmFiConsOri.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
