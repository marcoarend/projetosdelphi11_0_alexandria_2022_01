unit UnGraTX_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Buttons, ComCtrls, CommCtrl, Consts,
  Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums, dmkEditCB,
  dmkEdit, dmkDBLookupComboBox, mySQLDbTables, Data.Db, DBGrids, AppListas,
  dmkDBGridZTO, UnDmkProcFunc, UnProjGroup_Vars, TypInfo, System.Math,
  UnProjGroup_Consts, UnEntities, DBCtrls, Grids, Mask, UnTX_Tabs,
  dmkEditDateTimePicker, UnGrl_Consts, UnGrl_Geral, UnTX_PF, UnAppEnums;

type
  TUnGraTX_PF = class(TObject)
  private
    { Private declarations }
     //
  public
    { Public declarations }
    //
    procedure CalculaTotaisGraCmpPar(GraCmpPar: Integer; QryToReopen: TmySQLQuery);
  end;

var
  GraTX_PF: TUnGraTX_PF;

implementation

uses DmkDAC_PF, Module, UMySQLModule;

{ TUnGraTX_PF }

procedure TUnGraTX_PF.CalculaTotaisGraCmpPar(GraCmpPar: Integer; QryToReopen:
  TmySQLQuery);
var
  Controle, QtdeTot, QtdeImp, QtdeNoI: Integer;
  PercTot, PercImp, PercNoI: Double;
  SQLType: TSQLType;
begin
  SQLType   := stUpd;
  Controle  := GraCmpPar;
  QtdeTot   := 0;
  QtdeImp   := 0;
  QtdeNoI   := 0;
  PercTot   := 0;
  PercImp   := 0;
  PercNoI   := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT  ',
  'COUNT(Conta) QtdeTot, ',
  'SUM(IF(Imprime=1, 1, 0)) QtdeImp,  ',
  'SUM(PercComp) PercTot, ',
  'SUM(IF(Imprime=1, PercComp, 0)) PercImp ',
  'FROM gracmpfib ',
  'WHERE Controle=' + Geral.FF0(GraCmpPar),
  EmptyStr]);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    QtdeTot := Dmod.QrAux.FieldByName('QtdeTot').AsInteger;
    QtdeImp := Dmod.QrAux.FieldByName('QtdeImp').AsInteger;
    QtdeNoI := QtdeTot - QtdeImp;
    PercTot := Dmod.QrAux.FieldByName('PercTot').AsFloat;
    PercImp := Dmod.QrAux.FieldByName('PercImp').AsFloat;
    PercNoI := PercTot - PercImp;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gracmppar', False, [
  'PercTot', 'PercImp', 'PercNoI',
  'QtdeTot', 'QtdeImp', 'QtdeNoI'], [
  'Controle'], [
  PercTot, PercImp, PercNoI,
  QtdeTot, QtdeImp, QtdeNoI], [
  Controle], True) then
  begin
    if QryToReopen <> nil then
    begin
      UnDmkDAC_PF.AbreQueryApenas(QryToReopen);
      QryToReopen.Locate('Controle', Controle, []);
    end;
  end;
end;

end.
