unit GraCmpCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums;

type
  TFmGraCmpCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrGraCmpCad: TMySQLQuery;
    DsGraCmpCad: TDataSource;
    QrGraCmpPar: TMySQLQuery;
    DsGraCmpPar: TDataSource;
    PMPar: TPopupMenu;
    ParInclui1: TMenuItem;
    ParExclui1: TMenuItem;
    ParAltera1: TMenuItem;
    PMCad: TPopupMenu;
    CadInclui1: TMenuItem;
    CadAltera1: TMenuItem;
    CadExclui1: TMenuItem;
    BtCad: TBitBtn;
    BtPar: TBitBtn;
    DBTeste: TMySQLDatabase;
    QrGraCmpCadCodigo: TIntegerField;
    QrGraCmpCadCodUsu: TIntegerField;
    QrGraCmpCadNome: TWideStringField;
    QrGraCmpCadLk: TIntegerField;
    QrGraCmpCadDataCad: TDateField;
    QrGraCmpCadDataAlt: TDateField;
    QrGraCmpCadUserCad: TIntegerField;
    QrGraCmpCadUserAlt: TIntegerField;
    QrGraCmpCadAlterWeb: TSmallintField;
    QrGraCmpCadAWServerID: TIntegerField;
    QrGraCmpCadAWStatSinc: TSmallintField;
    QrGraCmpCadAtivo: TSmallintField;
    QrGraCmpParNO_Parte: TWideStringField;
    QrGraCmpParCodigo: TIntegerField;
    QrGraCmpParControle: TIntegerField;
    QrGraCmpParParte: TIntegerField;
    QrGraCmpParLk: TIntegerField;
    QrGraCmpParDataCad: TDateField;
    QrGraCmpParDataAlt: TDateField;
    QrGraCmpParUserCad: TIntegerField;
    QrGraCmpParUserAlt: TIntegerField;
    QrGraCmpParAlterWeb: TSmallintField;
    QrGraCmpParAWServerID: TIntegerField;
    QrGraCmpParAWStatSinc: TSmallintField;
    QrGraCmpParAtivo: TSmallintField;
    QrGraCmpFib: TMySQLQuery;
    DsGraCmpFib: TDataSource;
    QrGraCmpFibNO_Fibra: TWideStringField;
    QrGraCmpFibCodigo: TIntegerField;
    QrGraCmpFibControle: TIntegerField;
    QrGraCmpFibConta: TIntegerField;
    QrGraCmpFibFibra: TIntegerField;
    QrGraCmpFibPercComp: TFloatField;
    QrGraCmpFibImprime: TSmallintField;
    QrGraCmpFibLk: TIntegerField;
    QrGraCmpFibDataCad: TDateField;
    QrGraCmpFibDataAlt: TDateField;
    QrGraCmpFibUserCad: TIntegerField;
    QrGraCmpFibUserAlt: TIntegerField;
    QrGraCmpFibAlterWeb: TSmallintField;
    QrGraCmpFibAWServerID: TIntegerField;
    QrGraCmpFibAWStatSinc: TSmallintField;
    QrGraCmpFibAtivo: TSmallintField;
    PnPar: TPanel;
    DBGPar: TDBGrid;
    Splitter1: TSplitter;
    PnFib: TPanel;
    DBGFib: TDBGrid;
    Label3: TLabel;
    Label4: TLabel;
    QrGraCmpParPercTot: TFloatField;
    QrGraCmpParPercImp: TFloatField;
    QrGraCmpParPercNoI: TFloatField;
    QrGraCmpParQtdeTot: TIntegerField;
    QrGraCmpParQtdeImp: TIntegerField;
    QrGraCmpParQtdeNoI: TIntegerField;
    QrGraCmpFibNO_Imprime: TWideStringField;
    BtFib: TBitBtn;
    PMFib: TPopupMenu;
    FibInclui1: TMenuItem;
    FIbAltera1: TMenuItem;
    FibExclui1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraCmpCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraCmpCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrGraCmpCadAfterScroll(DataSet: TDataSet);
    procedure CadInclui1Click(Sender: TObject);
    procedure CadAltera1Click(Sender: TObject);
    procedure CadExclui1Click(Sender: TObject);
    procedure BtCadClick(Sender: TObject);
    procedure BtParClick(Sender: TObject);
    procedure ParInclui1Click(Sender: TObject);
    procedure ParExclui1Click(Sender: TObject);
    procedure ParAltera1Click(Sender: TObject);
    procedure PMCadPopup(Sender: TObject);
    procedure PMParPopup(Sender: TObject);
    procedure QrGraCmpCadBeforeClose(DataSet: TDataSet);
    procedure QrGraCmpParAfterScroll(DataSet: TDataSet);
    procedure QrGraCmpParBeforeClose(DataSet: TDataSet);
    procedure PMFibPopup(Sender: TObject);
    procedure FibInclui1Click(Sender: TObject);
    procedure FIbAltera1Click(Sender: TObject);
    procedure FibExclui1Click(Sender: TObject);
    procedure BtFibClick(Sender: TObject);
    procedure DBGParDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGFibDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormGraCmpPar(SQLType: TSQLType);
    procedure MostraFormGraCmpFib(SQLType: TSQLType);
    procedure ReopenGraCmpPar(Controle: Integer);
    procedure ReopenGraCmpFib(Conta: Integer);
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);


  end;

var
  FmGraCmpCad: TFmGraCmpCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, GraCmpPar, GraCmpFib,
  UnGraTX_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraCmpCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraCmpCad.MostraFormGraCmpFib(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmGraCmpFib, FmGraCmpFib, afmoNegarComAviso) then
  begin
    FmGraCmpFib.ImgTipo.SQLType := SQLType;
    FmGraCmpFib.FQrCab := QrGraCmpCad;
    FmGraCmpFib.FDsCab := DsGraCmpCad;
    FmGraCmpFib.FQrPar := QrGraCmpPar;
    FmGraCmpFib.FDsPar := DsGraCmpPar;
    FmGraCmpFib.FQrFib := QrGraCmpFib;
    //
    if SQLType = stIns then
      //
    else
    begin
      FmGraCmpFib.EdConta.ValueVariant := QrGraCmpFibConta.Value;
      //
      FmGraCmpFib.EdFibra.ValueVariant    := QrGraCmpFibFibra.Value;
      FmGraCmpFib.CBFibra.KeyValue        := QrGraCmpFibFibra.Value;
      FmGraCmpFib.EdPercComp.ValueVariant := QrGraCmpFibPercComp.Value;
      FmGraCmpFib.CkImprime.Checked       := Geral.IntToBool(QrGraCmpFibImprime.Value);
    end;
    FmGraCmpFib.ShowModal;
    FmGraCmpFib.Destroy;
  end;
end;

procedure TFmGraCmpCad.MostraFormGraCmpPar(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmGraCmpPar, FmGraCmpPar, afmoNegarComAviso) then
  begin
    FmGraCmpPar.ImgTipo.SQLType := SQLType;
    FmGraCmpPar.FQrCab := QrGraCmpCad;
    FmGraCmpPar.FDsCab := DsGraCmpCad;
    FmGraCmpPar.FQrIts := QrGraCmpPar;
    if SQLType = stIns then
      //
    else
    begin
      FmGraCmpPar.EdControle.ValueVariant := QrGraCmpParControle.Value;
      //
      FmGraCmpPar.EdParte.ValueVariant := QrGraCmpParParte.Value;
      FmGraCmpPar.CBParte.KeyValue     := QrGraCmpParParte.Value;
    end;
    FmGraCmpPar.ShowModal;
    FmGraCmpPar.Destroy;
  end;
end;

procedure TFmGraCmpCad.PMCadPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CadAltera1, QrGraCmpCad);
  MyObjects.HabilitaMenuItemCabDel(CadExclui1, QrGraCmpCad, QrGraCmpPar);
end;

procedure TFmGraCmpCad.PMFibPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(FibInclui1, QrGraCmpPar);
  MyObjects.HabilitaMenuItemItsUpd(FibAltera1, QrGraCmpFib);
  MyObjects.HabilitaMenuItemItsDel(FibExclui1, QrGraCmpFib);
end;

procedure TFmGraCmpCad.PMParPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ParInclui1, QrGraCmpCad);
  MyObjects.HabilitaMenuItemItsUpd(ParAltera1, QrGraCmpPar);
  MyObjects.HabilitaMenuItemItsDel(ParExclui1, QrGraCmpPar);
end;

procedure TFmGraCmpCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraCmpCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraCmpCad.DefParams;
begin
  VAR_GOTOTABELA := 'gracmpcad';
  VAR_GOTOMYSQLTABLE := QrGraCmpCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM gracmpcad');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmGraCmpCad.ParAltera1Click(Sender: TObject);
begin
  MostraFormGraCmpPar(stUpd);
end;

procedure TFmGraCmpCad.CadExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmGraCmpCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmGraCmpCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraCmpCad.ParExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'GraCmpPar', 'Controle', QrGraCmpParControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrGraCmpPar,
      QrGraCmpParControle, QrGraCmpParControle.Value);
    ReopenGraCmpPar(Controle);
  end;
end;

procedure TFmGraCmpCad.ReopenGraCmpFib(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraCmpFib, Dmod.MyDB, [
  'SELECT if(gcf.Imprime=1, "Sim", "N�o") NO_Imprime, gfc.Nome NO_Fibra, gcf.* ',
  'FROM gracmpfib gcf ',
  'LEFT JOIN grafibcad gfc ON gfc.Codigo=gcf.Fibra ',
  'WHERE gcf.Controle=' + Geral.FF0(QrGraCmpParControle.Value),
  'ORDER BY gcf.Controle ',
  '']);
  //
  QrGraCmpFib.Locate('Conta', Conta, []);
end;

procedure TFmGraCmpCad.ReopenGraCmpPar(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraCmpPar, Dmod.MyDB, [
  'SELECT gpc.Nome NO_Parte, gcp.* ',
  'FROM gracmppar gcp ',
  'LEFT JOIN GraParCad gpc ON gpc.Codigo=gcp.Parte ',
  'WHERE gcp.Codigo=' + Geral.FF0(QrGraCmpCadCodigo.Value),
  'ORDER BY gcp.Controle ',
  '']);
  //
  QrGraCmpPar.Locate('Controle', Controle, []);
end;


procedure TFmGraCmpCad.DBGFibDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  CorTexto, CorFundo: TColor;
begin
  if Column.FieldName = 'NO_Imprime' then
  begin
    CorFundo := clWhite;
    if (QrGraCmpFibImprime.Value = 1) then
      CorTexto := clBlue
    else
      CorTexto := clGrayText;
    MyObjects.DesenhaTextoEmDBGrid(TDBGrid(DBGFib), Rect, CorTexto, CorFundo,
      Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TFmGraCmpCad.DBGParDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  CorTexto, CorFundo: TColor;
begin
  if Column.FieldName = 'PercTot' then
  begin
    CorFundo := clWhite;
    if (QrGraCmpParPercTot.Value > 99.99) then
      CorTexto := clBlue
    else
      CorTexto := clRed;
    MyObjects.DesenhaTextoEmDBGrid(TDBGrid(DBGPar), Rect, CorTexto, CorFundo,
      Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TFmGraCmpCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGraCmpCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraCmpCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraCmpCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraCmpCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraCmpCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraCmpCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGraCmpCadCodigo.Value;
  Close;
end;

procedure TFmGraCmpCad.ParInclui1Click(Sender: TObject);
begin
  MostraFormGraCmpPar(stIns);
end;

procedure TFmGraCmpCad.CadAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraCmpCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'gracmpcad');
end;

procedure TFmGraCmpCad.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, CodUsu: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  //CodUsu         := Codigo;
  Nome           := EdNome.ValueVariant;
  //
  Codigo := UMyMod.BPGS1I32('gracmpcad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  CodUsu := Codigo;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gracmpcad', False, [
  'CodUsu', 'Nome'], [
  'Codigo'], [
  CodUsu, Nome], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmGraCmpCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'gracmpcad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'gracmpcad', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmGraCmpCad.BtFibClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFib, BtFib);
end;

procedure TFmGraCmpCad.BtParClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPar, BtPar);
end;

procedure TFmGraCmpCad.BtCadClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCad, BtCad);
end;

procedure TFmGraCmpCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  PnPar.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmGraCmpCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraCmpCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraCmpCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGraCmpCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrGraCmpCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGraCmpCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGraCmpCad.QrGraCmpCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraCmpCad.QrGraCmpCadAfterScroll(DataSet: TDataSet);
begin
  ReopenGraCmpPar(0);
end;

procedure TFmGraCmpCad.FIbAltera1Click(Sender: TObject);
begin
  MostraFormGraCmpFib(stUpd);
end;

procedure TFmGraCmpCad.FibExclui1Click(Sender: TObject);
var
  Conta, Controle: Integer;
begin
  Controle := QrGraCmpFibControle.Value;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'GraCmpFibra', 'Controle', QrGraCmpFibControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Conta := GOTOy.LocalizaPriorNextIntQr(QrGraCmpFib,
      QrGraCmpFibControle, QrGraCmpFibConta.Value);
    GraTX_PF.CalculaTotaisGraCmpPar(Controle, nil);
    ReopenGraCmpPar(Controle);
    ReopenGraCmpFib(Conta);
  end;
end;

procedure TFmGraCmpCad.FibInclui1Click(Sender: TObject);
begin
  MostraFormGraCmpFib(stIns);
end;

procedure TFmGraCmpCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CadInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrGraCmpCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmGraCmpCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGraCmpCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'gracmpcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGraCmpCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraCmpCad.CadInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrGraCmpCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'gracmpcad');
end;

procedure TFmGraCmpCad.QrGraCmpCadBeforeClose(
  DataSet: TDataSet);
begin
  QrGraCmpPar.Close;
end;

procedure TFmGraCmpCad.QrGraCmpCadBeforeOpen(DataSet: TDataSet);
begin
  QrGraCmpCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmGraCmpCad.QrGraCmpParAfterScroll(DataSet: TDataSet);
begin
  ReopenGraCmpFib(0);
end;

procedure TFmGraCmpCad.QrGraCmpParBeforeClose(DataSet: TDataSet);
begin
  QrGraCmpFib.Close;
end;

end.

