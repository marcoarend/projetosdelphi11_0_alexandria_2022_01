object FmGraCmpCad: TFmGraCmpCad
  Left = 368
  Top = 194
  Caption = 'MAT-COMPO-001 :: Composi'#231#227'o de Material'
  ClientHeight = 563
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 404
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 649
      Top = 61
      Height = 342
      Align = alRight
      ExplicitLeft = 608
      ExplicitTop = 136
      ExplicitHeight = 100
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsGraCmpCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 689
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsGraCmpCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 403
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCad: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Composite'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCadClick
        end
        object BtPar: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Parte'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtParClick
        end
        object BtFib: TBitBtn
          Tag = 110
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Fibra'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtFibClick
        end
      end
    end
    object PnPar: TPanel
      Left = 0
      Top = 73
      Width = 513
      Height = 216
      BevelOuter = bvNone
      TabOrder = 2
      object Label3: TLabel
        Left = 0
        Top = 0
        Width = 513
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Partes do material:'
        ExplicitWidth = 87
      end
      object DBGPar: TDBGrid
        Left = 0
        Top = 13
        Width = 513
        Height = 203
        Align = alClient
        DataSource = DsGraCmpPar
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = DBGParDrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Parte'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Parte'
            Title.Caption = 'Descri'#231#227'o da Parte'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QtdeImp'
            Title.Caption = 'Qtd Imp.'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QtdeTot'
            Title.Caption = 'Qtd Tot.'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PercImp'
            Title.Caption = '% Imp'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PercTot'
            Title.Caption = '% Tot.'
            Width = 48
            Visible = True
          end>
      end
    end
    object PnFib: TPanel
      Left = 652
      Top = 61
      Width = 356
      Height = 342
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 3
      object Label4: TLabel
        Left = 0
        Top = 0
        Width = 356
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Fibras componentes da parte selecinada:'
        ExplicitWidth = 194
      end
      object DBGFib: TDBGrid
        Left = 0
        Top = 13
        Width = 356
        Height = 329
        Align = alClient
        DataSource = DsGraCmpFib
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = DBGFibDrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'Conta'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Fibra'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Fibra'
            Title.Caption = 'Nome da Fibra'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PercComp'
            Title.Caption = '% Comp.'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Imprime'
            Title.Caption = 'Imp?'
            Width = 28
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 294
        Height = 32
        Caption = 'Composi'#231#227'o de Material'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 294
        Height = 32
        Caption = 'Composi'#231#227'o de Material'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 294
        Height = 32
        Caption = 'Composi'#231#227'o de Material'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCad
    Left = 120
    Top = 64
  end
  object QrGraCmpCad: TMySQLQuery
    Database = DBTeste
    BeforeOpen = QrGraCmpCadBeforeOpen
    AfterOpen = QrGraCmpCadAfterOpen
    BeforeClose = QrGraCmpCadBeforeClose
    AfterScroll = QrGraCmpCadAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM gracmpcad'
      'WHERE Codigo > 0')
    Left = 92
    Top = 233
    object QrGraCmpCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGraCmpCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrGraCmpCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrGraCmpCadLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrGraCmpCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraCmpCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraCmpCadUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrGraCmpCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrGraCmpCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrGraCmpCadAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrGraCmpCadAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrGraCmpCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsGraCmpCad: TDataSource
    DataSet = QrGraCmpCad
    Left = 92
    Top = 277
  end
  object QrGraCmpPar: TMySQLQuery
    Database = DBTeste
    BeforeClose = QrGraCmpParBeforeClose
    AfterScroll = QrGraCmpParAfterScroll
    SQL.Strings = (
      'SELECT gpc.Nome NO_Parte, gcp.*'
      'FROM gracmppar gcp'
      'LEFT JOIN GraParCad gpc ON gpc.Codigo=gcp.Parte'
      'WHERE gcp.Codigo=:P0'
      'ORDER BY gcp.Controle')
    Left = 176
    Top = 233
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraCmpParNO_Parte: TWideStringField
      FieldName = 'NO_Parte'
      Size = 60
    end
    object QrGraCmpParCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGraCmpParControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGraCmpParParte: TIntegerField
      FieldName = 'Parte'
      Required = True
    end
    object QrGraCmpParLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrGraCmpParDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraCmpParDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraCmpParUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrGraCmpParUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrGraCmpParAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrGraCmpParAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrGraCmpParAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrGraCmpParAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrGraCmpParPercTot: TFloatField
      FieldName = 'PercTot'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrGraCmpParPercImp: TFloatField
      FieldName = 'PercImp'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrGraCmpParPercNoI: TFloatField
      FieldName = 'PercNoI'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrGraCmpParQtdeTot: TIntegerField
      FieldName = 'QtdeTot'
      DisplayFormat = '0;-0; '
    end
    object QrGraCmpParQtdeImp: TIntegerField
      FieldName = 'QtdeImp'
      DisplayFormat = '0;-0; '
    end
    object QrGraCmpParQtdeNoI: TIntegerField
      FieldName = 'QtdeNoI'
      DisplayFormat = '0;-0; '
    end
  end
  object DsGraCmpPar: TDataSource
    DataSet = QrGraCmpPar
    Left = 176
    Top = 277
  end
  object PMPar: TPopupMenu
    OnPopup = PMParPopup
    Left = 668
    Top = 364
    object ParInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ParInclui1Click
    end
    object ParAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ParAltera1Click
    end
    object ParExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ParExclui1Click
    end
  end
  object PMCad: TPopupMenu
    OnPopup = PMCadPopup
    Left = 532
    Top = 364
    object CadInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CadInclui1Click
    end
    object CadAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CadAltera1Click
    end
    object CadExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CadExclui1Click
    end
  end
  object DBTeste: TMySQLDatabase
    DatabaseName = 'planning'
    DesignOptions = []
    UserName = 'root'
    Host = 'localhost'
    ConnectOptions = [coCompress]
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'UID=root'
      'DatabaseName=planning'
      'Host=localhost')
    DatasetOptions = []
    Left = 156
    Top = 168
  end
  object QrGraCmpFib: TMySQLQuery
    Database = DBTeste
    SQL.Strings = (
      'SELECT gfc.Nome NO_Fibra, gcf.*'
      'FROM gracmpfib gcf'
      'LEFT JOIN GraFIbCad gfc ON gfc.Codigo=gcf.Fibra'
      'WHERE gcf.Codigo=:P0'
      'ORDER BY gcf.Controle')
    Left = 260
    Top = 233
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraCmpFibNO_Fibra: TWideStringField
      FieldName = 'NO_Fibra'
      Size = 30
    end
    object QrGraCmpFibCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGraCmpFibControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGraCmpFibConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrGraCmpFibFibra: TIntegerField
      FieldName = 'Fibra'
      Required = True
    end
    object QrGraCmpFibPercComp: TFloatField
      FieldName = 'PercComp'
      Required = True
      DisplayFormat = '0.00;-0.00; '
    end
    object QrGraCmpFibImprime: TSmallintField
      FieldName = 'Imprime'
      Required = True
    end
    object QrGraCmpFibLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrGraCmpFibDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraCmpFibDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraCmpFibUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrGraCmpFibUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrGraCmpFibAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrGraCmpFibAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrGraCmpFibAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrGraCmpFibAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrGraCmpFibNO_Imprime: TWideStringField
      FieldName = 'NO_Imprime'
      Size = 3
    end
  end
  object DsGraCmpFib: TDataSource
    DataSet = QrGraCmpFib
    Left = 260
    Top = 277
  end
  object PMFib: TPopupMenu
    OnPopup = PMFibPopup
    Left = 776
    Top = 360
    object FibInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = FibInclui1Click
    end
    object FIbAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = FIbAltera1Click
    end
    object FibExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = FibExclui1Click
    end
  end
end
