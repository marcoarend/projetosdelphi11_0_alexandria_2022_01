unit FiConsBas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmFiConsBas = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBGGXSorc: TdmkDBLookupComboBox;
    EdGGXSorc: TdmkEditCB;
    Label1: TLabel;
    SbGGXSrc: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGGXSorc: TMySQLQuery;
    QrGGXSorcGraGru1: TIntegerField;
    QrGGXSorcControle: TIntegerField;
    QrGGXSorcNO_PRD_TAM_COR: TWideStringField;
    QrGGXSorcSIGLAUNIDMED: TWideStringField;
    QrGGXSorcCODUSUUNIDMED: TIntegerField;
    QrGGXSorcNOMEUNIDMED: TWideStringField;
    DsGGXSorc: TDataSource;
    Label2: TLabel;
    EdGGXSubs: TdmkEditCB;
    CBGGXSubs: TdmkDBLookupComboBox;
    SbGGXSorc: TSpeedButton;
    EdGG1Dest: TdmkEdit;
    EdNomeGG1Dest: TdmkEdit;
    QrGGXSubs: TMySQLQuery;
    QrGGXSubsGraGru1: TIntegerField;
    QrGGXSubsControle: TIntegerField;
    QrGGXSubsNO_PRD_TAM_COR: TWideStringField;
    QrGGXSubsSIGLAUNIDMED: TWideStringField;
    QrGGXSubsCODUSUUNIDMED: TIntegerField;
    QrGGXSubsNOMEUNIDMED: TWideStringField;
    DsGGXSubs: TDataSource;
    QrGraParCad: TMySQLQuery;
    QrGraParCadCodigo: TIntegerField;
    DsGraParCad: TDataSource;
    Label4: TLabel;
    EdParte: TdmkEditCB;
    CBParte: TdmkDBLookupComboBox;
    SbParte: TSpeedButton;
    EdQtdUso: TdmkEdit;
    Label8: TLabel;
    CkObrigatorio: TCheckBox;
    QrGraParCadNome: TWideStringField;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    Label9: TLabel;
    DBEdit2: TDBEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbGGXSrcClick(Sender: TObject);
    procedure SbGGXSorcClick(Sender: TObject);
    procedure SbParteClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFiConsBas: TFmFiConsBas;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnGrade_Jan, UnTX_PF, UnGraTX_Jan;

{$R *.DFM}

procedure TFmFiConsBas.BtOKClick(Sender: TObject);
var
  GG1Dest, Controle, GGXSorc, GGXSubs, Parte, Obrigatorio: Integer;
  QtdUso: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  GG1Dest        := EdGG1Dest.ValueVariant;
  Controle       := EdControle.ValueVariant;
  GGXSorc        := EdGGXSorc.ValueVariant;
  GGXSubs        := EdGGXSubs.ValueVariant;
  Parte          := EdParte.ValueVariant;
  QtdUso         := EdQtdUso.ValueVariant;
  Obrigatorio    := Geral.BoolToInt(CkObrigatorio.Checked);
  //
  if MyObjects.FIC(GGXSorc = 0, EdGGXSorc, 'Informe o reduzido preferencial!') then
    Exit;
  if MyObjects.FIC(QtdUso < 0.001, EdGGXSorc, 'Informe a quantidade!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('ficonsbas', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ficonsbas', False, [
  'GG1Dest', 'GGXSorc', 'GGXSubs',
  'Parte', 'QtdUso', 'Obrigatorio'], [
  'Controle'], [
  GG1Dest, GGXSorc, GGXSubs,
  Parte, QtdUso, Obrigatorio], [
  Controle], True) then
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdGGXSorc.ValueVariant   := 0;
      CBGGXSubs.KeyValue       := Null;
      EdGGXSubs.ValueVariant   := 0;
      CBGGXSubs.KeyValue       := Null;
      EdParte.ValueVariant     := 0;
      CBParte.KeyValue         := Null;
      EdQtdUso.ValueVariant    := 0;
      //
      EdGGXSorc.SetFocus;
    end else Close;
  end;
end;

procedure TFmFiConsBas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFiConsBas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFiConsBas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TX_PF.ReopenGraGruX(QrGGXSorc, '');
  TX_PF.ReopenGraGruX(QrGGXSubs, '');
  UnDmkDAC_PF.AbreQuery(QrGraParCad, Dmod.MyDB);
end;

procedure TFmFiConsBas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFiConsBas.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmFiConsBas.SbGGXSorcClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Grade_Jan.MostraFormGraGruN(0(*Nivel1: Integer; TabePr: Integer = 0;
  TipoLista: TTipoListaPreco = tpGraCusPrc; MostraSubForm: TSubForm = tsfNenhum*));
  UMyMod.SetaCodigoPesquisado(EdGGXSubs, CBGGXSubs, QrGGXSubs, VAR_CADASTRO);
end;

procedure TFmFiConsBas.SbGGXSrcClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  Grade_Jan.MostraFormGraGruN(0(*Nivel1: Integer; TabePr: Integer = 0;
  TipoLista: TTipoListaPreco = tpGraCusPrc; MostraSubForm: TSubForm = tsfNenhum*));
  UMyMod.SetaCodigoPesquisado(EdGGXSorc, CBGGXSorc, QrGGXSorc, VAR_CADASTRO);
end;

procedure TFmFiConsBas.SbParteClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  GraTX_Jan.MostraFormGraParCad();
  UMyMod.SetaCodigoPesquisado(EdParte, CBParte, QrGraParCad, VAR_CADASTRO);
end;

end.
