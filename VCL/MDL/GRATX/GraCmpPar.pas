unit GraCmpPar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmGraCmpPar = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBParte: TdmkDBLookupComboBox;
    EdParte: TdmkEditCB;
    Label1: TLabel;
    SbParte: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraParCad: TMySQLQuery;
    QrGraParCadCodigo: TIntegerField;
    QrGraParCadNome: TWideStringField;
    DsGraParCad: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbParteClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmGraCmpPar: TFmGraCmpPar;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnGraTX_Jan;

{$R *.DFM}

procedure TFmGraCmpPar.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Parte: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  Parte          := EdParte.ValueVariant;
  //
  if MyObjects.FIC(Parte = 0, EdParte,
    'Informe uma parte v�lida (diferente de zero)!') then
      Exit;
  //
  //Evitar duplica��o
  if UMyMod.VerificaDuplicadoNovo(SQLType, Dmod.QrAux, Dmod.MyDB, 'gracmppar',
  'Codigo', 'Controle', Codigo, Controle, 'Parte', Parte, (*ShowMsgDupl*)True)
  then Exit;
  //
  Controle := UMyMod.BPGS1I32('gracmppar', 'Controle', '', '', tsPos, stIns, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gracmppar', False, [
  'Codigo', 'Parte'], [
  'Controle'], [
  Codigo, Parte], [
  Controle], True) then
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdParte.ValueVariant     := 0;
      CBParte.KeyValue         := Null;
      //
      EdParte.SetFocus;
    end else Close;
  end;
end;

procedure TFmGraCmpPar.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraCmpPar.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmGraCmpPar.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrGraParCad, Dmod.MyDB);
end;

procedure TFmGraCmpPar.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGraCmpPar.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmGraCmpPar.SbParteClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  GraTX_Jan.MostraFormGraParCad();
  UMyMod.SetaCodigoPesquisado(EdParte, CBParte, QrGraParCad, VAR_CADASTRO);
end;

end.
