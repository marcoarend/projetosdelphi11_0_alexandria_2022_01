unit FiConsSpc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.Grids, Vcl.ComCtrls, Vcl.DBGrids,
  dmkDBGridZTO, mySQLDirectQuery;

type
  //THackDBGrid = class(TDBGrid);
  //THackCustomGrid = class(TCustomGrid);
  TFmFiConsSpc = class(TForm)
    PnBase: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel1: TPanel;
    Label5: TLabel;
    EdGG1Dest: TdmkEdit;
    Label3: TLabel;
    EdNomeGG1Dest: TdmkEdit;
    PnLeft: TPanel;
    PCOrigens: TPageControl;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel3: TPanel;
    PnVV: TPanel;
    Panel5: TPanel;
    QrGraGru1: TMySQLQuery;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1CodUsu: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    DsGraGru1: TDataSource;
    Label1: TLabel;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    QrGraGru1GraTamCad: TIntegerField;
    QrFiConsOri: TMySQLQuery;
    DBGFiConsOri: TdmkDBGridZTO;
    DsFiConsOri: TDataSource;
    Sb_NtoN: TSpeedButton;
    Sb_DelN: TSpeedButton;
    Sb_1toN: TSpeedButton;
    Sb_Del1: TSpeedButton;
    Sb_1to1: TSpeedButton;
    DqAux: TMySQLDirectQuery;
    QrGraGru1GG1Subst: TIntegerField;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DBGSrcArt: TdmkDBGridZTO;
    Splitter1: TSplitter;
    Panel8: TPanel;
    QrSrcArt: TMySQLQuery;
    DsSrcArt: TDataSource;
    QrSrcTeC: TMySQLQuery;
    DsSrcTeC: TDataSource;
    QrSrcArtNivel1: TIntegerField;
    QrSrcArtNO_GG1_Sorc: TWideStringField;
    QrSrcArtITENS: TLargeintField;
    QrFiConsOriNO_TAM: TWideStringField;
    QrFiConsOriNO_COR: TWideStringField;
    QrFiConsOriGraCorCad: TIntegerField;
    QrFiConsOriGraGruC: TIntegerField;
    QrFiConsOriGraGru1: TIntegerField;
    QrFiConsOriGraTamI: TIntegerField;
    QrFiConsOriGraTamCad: TIntegerField;
    QrFiConsOriGGXSubs: TIntegerField;
    QrFiConsOriParte: TIntegerField;
    QrFiConsOriObrigatorio: TSmallintField;
    QrFiConsOriNO_Obrigatorio: TWideStringField;
    QrFiConsOriNO_PARTE: TWideStringField;
    QrFiConsOriNO_PRD_TAM_COR_SUBS: TWideStringField;
    QrFiConsOriGraGruX: TIntegerField;
    QrFiConsOriFiConsOri: TIntegerField;
    DBGSrcTeC: TdmkDBGridZTO;
    QrSrcTeCGraTamI: TIntegerField;
    QrSrcTeCNO_TAM: TWideStringField;
    QrSrcTeCGraGruC: TIntegerField;
    QrSrcTeCGraCorCad: TIntegerField;
    QrSrcTeCNO_COR: TWideStringField;
    QrSrcTeCITENS: TLargeintField;
    PnDest: TPanel;
    PCGrades: TPageControl;
    TabSheet1: TTabSheet;
    GradeK: TStringGrid;
    TabSheet2: TTabSheet;
    GradeA: TStringGrid;
    TabSheet3: TTabSheet;
    GradeC: TStringGrid;
    TabSheet4: TTabSheet;
    GradeX: TStringGrid;
    QrSrcTeCGraGruX: TIntegerField;
    QrFiConsOriSIGLAUNIDMED: TStringField;
    QrFiConsOriCODUSUUNIDMED: TIntegerField;
    QrFiConsOriNOMEUNIDMED: TStringField;
    QrFiConsOriSIGLAUNIDMED_s: TStringField;
    QrFiConsOriCODUSUUNIDMED_s: TIntegerField;
    QrFiConsOriNOMEUNIDMED_s: TStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    //procedure GradeKDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      //State: TGridDrawState);
    procedure EdGraGru1Redefinido(Sender: TObject);
    procedure Sb_1to1Click(Sender: TObject);
    procedure DBGFiConsOriCellClick(Column: TColumn);
    procedure QrFiConsOriAfterScroll(DataSet: TDataSet);
    procedure QrSrcArtAfterScroll(DataSet: TDataSet);
    procedure QrSrcArtBeforeClose(DataSet: TDataSet);
    procedure DBGSrcArtDblClick(Sender: TObject);
    procedure DBGSrcTamDblClick(Sender: TObject);
    procedure DBGSrcCorDblClick(Sender: TObject);
    procedure GradeKFixedCellClick(Sender: TObject; ACol, ARow: Integer);
    procedure Sb_1toNClick(Sender: TObject);
    procedure GradeKDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure Sb_NtoNClick(Sender: TObject);
    procedure DBGSrcTeCCellClick(Column: TColumn);
  private
    { Private declarations }
    procedure ReopenFiConsOri(GraGru1, GGXToLoc: Integer);
    procedure ReopenSrcTeC();
    procedure InsereItem(GG1Dest, GGXDest, GGXSorc, GG1Subst: Integer; QtdUso:
              Double);
    function  ObtemQuantidade(var q: Double): Boolean;
    function  MostraFormFiConsOri(var QtdUso: Double): Boolean;
  public
    { Public declarations }
    FGG1Dest, FGraTamCadDest,
    FOriGraGru1, FOriGraTamI: Integer;
    //FDBGClicked: Boolean;
    //
    procedure PreparaGrades();
    procedure ReopenSrcArt();

  end;

  var
  FmFiConsSpc: TFmFiConsSpc;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnGrade_Jan, UnTX_PF, UnGraTX_Jan, ModProd, ModuleGeral, UMySQLDB,
  GetValor, FiConsOri;

{$R *.DFM}

const
  iColsPerItem = 1; //3;
  iFixedCols   = 1;
  iFixedRows   = 2;

procedure TFmFiConsSpc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFiConsSpc.DBGFiConsOriCellClick(Column: TColumn);
begin
  //FDBGClicked := True;
end;

procedure TFmFiConsSpc.DBGSrcArtDblClick(Sender: TObject);
begin
  EdGraGru1.ValueVariant := QrSrcArtNivel1.Value;
  CBGraGru1.KeyValue     := QrSrcArtNivel1.Value;
  //
  PCOrigens.ActivePageIndex := 1;
end;

procedure TFmFiConsSpc.DBGSrcCorDblClick(Sender: TObject);
begin
(*&�%
  EdGraGru1.ValueVariant := QrSrcArtNivel1.Value;
  CBGraGru1.KeyValue     := QrSrcArtNivel1.Value;
  //
  EdGraTamIts.ValueVariant := QrSrcTamControle.Value;
  CBGraTamIts.KeyValue     := QrSrcTamControle.Value;
  //
  QrCores.Locate('Codigo', QrSrcCorCodigo.Value, []);
  //
  PCOrigens.ActivePageIndex := 1;
*)
end;

procedure TFmFiConsSpc.DBGSrcTamDblClick(Sender: TObject);
begin
(*&�%
  EdGraGru1.ValueVariant := QrSrcArtNivel1.Value;
  CBGraGru1.KeyValue     := QrSrcArtNivel1.Value;
  //
  EdGraTamIts.ValueVariant := QrSrcTamControle.Value;
  CBGraTamIts.KeyValue     := QrSrcTamControle.Value;
  //
  PCOrigens.ActivePageIndex := 1;
*)
end;

procedure TFmFiConsSpc.DBGSrcTeCCellClick(Column: TColumn);
begin
  EdGraGru1.ValueVariant := QrSrcArtNivel1.Value;
  CBGraGru1.KeyValue     := QrSrcArtNivel1.Value;
  //
  QrFiConsOri.Locate('GraGruX', QrSrcTeCGraGruX.Value, []);
  //
  PCOrigens.ActivePageIndex := 1;
end;

procedure TFmFiConsSpc.EdGraGru1Redefinido(Sender: TObject);
var
  CodUsu, GraTamCad: Integer;
begin
  CodUsu := EdGraGru1.ValueVariant;
  if CodUsu <> 0 then
  begin
    FOriGraGru1 := QrGraGru1Nivel1.Value;
    ReopenFiConsOri(FOriGraGru1, 0);
  end else
  begin
    FOriGraGru1 := 0;
    GraTamCad   := 0;
    QrFiConsOri.Close;
  end;
end;

procedure TFmFiConsSpc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFiConsSpc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  //FDBGClicked            := False;
  FOriGraGru1            := 0;
  FOriGraTamI            := 0;
  PCGrades.ActivePageIndex := 0;
  //
  GradeA.ColWidths[0] := 192;
  GradeC.ColWidths[0] := 192;
  GradeX.ColWidths[0] := 192;
  GradeK.ColWidths[0] := 192;
  //
  //
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
  //
end;

procedure TFmFiConsSpc.FormResize(Sender: TObject);
begin
  GradeK.Invalidate;
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

{
procedure TFmFiConsSpc.GradeKDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
  Valor: Double;
  Texto: String;
begin
  if (ACol = 0) or (ARow = 0) then
    Exit;
  if GradeC.Cells[ACol, ARow] <> '' then
    Cor := clWindow
  else
    Cor := clMenu;
  //
  Valor := Geral.DMV(GradeK.Cells[Acol, ARow]);
  if Valor <> 0 then
    Texto := Geral.FFT(Valor, 3, siNegativo)
  else
    Texto := '';
  //
  (*
  if (ACol = 0) or (ARow = 0) then
    MyObjects.DesenhaTextoEmStringGrid(GradeK, Rect, clBlack,
      Cor, taLeftJustify,
      GradeK.Cells[Acol, ARow], 0, 0, False)
  else
  *)
    MyObjects.DesenhaTextoEmStringGrid(GradeK, Rect, clBlack,
      Cor, taRightJustify, Texto, 0, 0, False);
end;
}

procedure TFmFiConsSpc.GradeKDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Old_DefaultDrawing:Boolean;
begin
  //if GradeK.DefaultDrawing then
  begin
(*
    case CellsAlignment[ACol,ARow] of
      taLeftJustify: begin
        Canvas.TextRect(Rect,Rect.Left+2,Rect.Top+2,Cells[ACol,ARow]);
      end;
      taRightJustify: begin
        Canvas.TextRect(Rect,Rect.Right -2 -Canvas.TextWidth(Cells[ACol,ARow]), Rect.Top+2,Cells[ACol,ARow]);
      end;
      taCenter: begin
        Canvas.TextRect(Rect,(Rect.Left+Rect.Right-Canvas.TextWidth(Cells[ACol,ARow]))div 2,Rect.Top+2,Cells[ACol,ARow]);
      end;
    end;
*)
    if ACol = 0 then
    //taLeftJustify:
    begin
       // Duplica escrita
       //GradeK.Canvas.TextRect(Rect,Rect.Left+2,Rect.Top+2,GradeK.Cells[ACol,ARow]);
    end else
    if ARow = 0 then
    //taCenter:
    begin
       // Duplica escrita
       //GradeK.Canvas.TextRect(Rect,(Rect.Left+Rect.Right-GradeK.Canvas.TextWidth(GradeK.Cells[ACol,ARow]))div 2,Rect.Top+2,GradeK.Cells[ACol,ARow]);
    end else
    //taRightJustify:
    begin
       if not (gdSelected in State) then // Duplica escrita
         GradeK.Canvas.TextRect(Rect,Rect.Right -2 -GradeK.Canvas.TextWidth(GradeK.Cells[ACol,ARow]), Rect.Top+2,GradeK.Cells[ACol,ARow]);
    end;
  end;
  {
  Old_DefaultDrawing:= GradeK.DefaultDrawing;
  GradeK.DefaultDrawing:=False;
  //inherited DrawCell(ACol,ARow,Rect,AState);
  GradeK.DefaultDrawing:= Old_DefaultDrawing;
  }
end;

procedure TFmFiConsSpc.GradeKFixedCellClick(Sender: TObject; ACol,
  ARow: Integer);
var
  GR: TGridRect;
begin
  if (ACol = 0) and (ARow = 0) then
  begin
    GR.Left   := 0;
    GR.Top    := 0;
    GR.Right  := 0;
    GR.Bottom := 0;
    //
    GradeK.Selection := GR;
  end else
  if (ACol = 0) and (ARow > 1) then
  begin
    GR.Left   := 1;
    GR.Top    := ARow;
    GR.Right  := GradeK.ColCount - 1;
    GR.Bottom := ARow;
    //
    GradeK.Selection := GR;
  end;
end;

procedure TFmFiConsSpc.InsereItem(GG1Dest, GGXDest, GGXSorc, GG1Subst: Integer;
  QtdUso: Double);
var
  //GG1Dest, GGXDest, GGXSorc,
  Controle, GGXSubs, Parte, Obrigatorio: Integer;
  //QtdUso: Double;
  SQLType: TSQLType;
  IsOK: Boolean;
begin
  //SQLType        :=
  //GG1Dest        := ;
  //GGXDest        := ;
  //GGXSorc        := ;
  // Ver se j� existe
  UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, DModG.MyCompressDB, [
  'SELECT Controle',
  'FROM ficonsspc',
  'WHERE GGXSorc=' + Geral.FF0(GGXSorc),
  'AND GGXDest=' + Geral.FF0(GGXDest),
  EmptyStr]);
  Controle       := USQLDB.Int(DqAux, 'Controle');
  //
  if Controle <> 0 then
  begin
    SQLType := stUpd;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ficonsspc', False, [
    'GGXSorc', 'QtdUso'], ['Controle'], [GGXSorc, QtdUso], [Controle], True);
  end else
  begin
    SQLType := stIns;
    //
    GGXSubs := 0;
    if GG1Subst <> 0 then
    begin
      GGXSubs := Dmod.MyDB.SelectInteger(
      ' SELECT ggx.Controle ' +
      ' FROM gragrux ggx ' +
      ' LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC' +
      ' WHERE ggx.GraGru1=' + Geral.FF0(GG1Subst) +
      ' AND ggx.GraTamI=' + Geral.FF0(QrFiConsOriGraTamI.Value) +
      ' AND ggc.GraCorCad=' + Geral.FF0(QrFiConsOriGraCorCad.Value) +
      EmptyStr, IsOK, 0);
      if (IsOK <> True) then
        GGXSubs := 0;
    end;
    Parte          := 0;
    //QtdUso         := 0.000;
    Obrigatorio    := 1;

    //
    Controle := UMyMod.BPGS1I32('ficonsspc', 'Controle', '', '', tsPos, SQLType, Controle);
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ficonsspc', False, [
    'GG1Dest', 'GGXDest', 'GGXSorc',
    'GGXSubs', 'Parte', 'QtdUso',
    'Obrigatorio'], [
    'Controle'], [
    GG1Dest, GGXDest, GGXSorc,
    GGXSubs, Parte, QtdUso,
    Obrigatorio], [
    Controle], True);
  end;
end;

function TFmFiConsSpc.MostraFormFiConsOri(var QtdUso: Double): Boolean;
var
  SQLType: TSQLType;
begin
  Result := False;
  if QrFiConsOriFiConsOri.Value = 0 then
    SQLType := stIns
  else
    SQLType := stUpd;
  if DBCheck.CriaFm(TFmFiConsOri, FmFiConsOri, afmoNegarComAviso) then
  begin
    FmFiConsOri.ImgTipo.SQLType := SQLType;
    //
    FmFiConsOri.EdGG1Dest.ValueVariant     := EdGG1Dest.ValueVariant;
    FmFiConsOri.EdGG1NomeDest.ValueVariant := EdNomeGG1Dest.ValueVariant;
    FmFiConsOri.EdGGXSorc.ValueVariant     := QrFiConsOriGraGruX.Value;
    FmFiConsOri.EdGGXPRD_TAM_CORSorc.Text  := CBGraGru1.Text;
    //
    if SQLType = stIns then
      //
    else
    begin
      FmFiConsOri.EdControle.ValueVariant    := QrFiConsOriFiConsOri.Value;
      //
      FmFiConsOri.EdQtdUso.ValueVariant      := QtdUso;
      FmFiConsOri.EdGGXSubs.ValueVariant     := QrFiConsOriGGXSubs.Value;
      FmFiConsOri.CBGGXSubs.KeyValue         := QrFiConsOriGGXSubs.Value;
      FmFiConsOri.EdParte.ValueVariant       := QrFiConsOriParte.Value;
      FmFiConsOri.CBParte.KeyValue           := QrFiConsOriParte.Value;
      FmFiConsOri.CkObrigatorio.Checked      := Geral.IntToBool(QrFiConsOriObrigatorio.Value);
    end;
    FmFiConsOri.ShowModal;
    if FmFiConsOri.FAlterou then
    begin
      Result := True;
      QtdUso := FmFiConsOri.FQtdUso;
    end;
    FmFiConsOri.Destroy;
  end;
end;

function TFmFiConsSpc.ObtemQuantidade(var q: Double): Boolean;
const
  FormCaption  = 'XXX-XXXXX-001 :: Quantidade';
  ValCaption   = 'Informe a quantidade:';
  WidthCaption = Length(ValCaption) * 7;
var
  ValVar: Variant;
begin
  Result := MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  q, 3, 0, '0,000', '', True, FormCaption, ValCaption, WidthCaption,
  ValVar);
  //
  if Result then
    q := Geral.DMV(ValVar);
end;

procedure TFmFiConsSpc.PreparaGrades();
begin
  DmProd.ConfigGradesFiConsSpc(FGG1Dest, FGraTamCadDest, FOriGraGru1, GradeA,
    GradeC, GradeX, GradeK);
end;

procedure TFmFiConsSpc.QrFiConsOriAfterScroll(DataSet: TDataSet);
begin
  DmProd.AtualizaGradesFiConsSpc(FGG1Dest, FOriGraGru1, QrFiConsOriGraCorCad.Value,
    QrFiConsOriGraTamI.Value, GradeK);
end;

procedure TFmFiConsSpc.QrSrcArtAfterScroll(DataSet: TDataSet);
begin
  ReopenSrcTeC();
end;

procedure TFmFiConsSpc.QrSrcArtBeforeClose(DataSet: TDataSet);
begin
  QrSrcTeC.Close;
end;

procedure TFmFiConsSpc.ReopenFiConsOri(GraGru1, GGXToLoc: Integer);
begin
  //Mudar para Cor X Tamanho  + parte + Obrigatorio + GGXSubs
  UnDmkDAC_PF.AbreMySQLQuery0(QrFiConsOri, Dmod.MyDB, [
  'SELECT ggx.Controle GraGruX, gti.Nome NO_TAM, ',
  'gcc.Nome NO_COR, unm.Sigla SIGLAUNIDMED, ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED, ',
  'ggc.GraCorCad, ggx.GraGruC, ',
  'ggx.GraGru1, ggx.GraTamI , gti.Codigo GraTamCad,',
  'fco.Controle FiConsOri, fco.GGXSubs, fco.Parte, fco.Obrigatorio,',
  'IF(fco.Obrigatorio IS NULL, "", ',
  '  IF(fco.Obrigatorio=1, "Sim", "N�o")) NO_Obrigatorio,',
  'gpc.Nome NO_PARTE, ',
  'CONCAT(gg1s.Nome,  ',
  '  IF(gtis.PrintTam=0 OR gtis.Codigo IS NULL, "", CONCAT(" ", gtis.Nome)),  ',
  '  IF(gccs.PrintCor=0 OR gccs.Codigo IS NULL, "", CONCAT(" ", gccs.Nome)))  ',
  '  NO_PRD_TAM_COR_SUBS, unms.Sigla SIGLAUNIDMED_s, ',
  'unms.CodUsu CODUSUUNIDMED_s, unms.Nome NOMEUNIDMED_s ',
  'FROM gragrux ggx  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  //
  'LEFT JOIN ficonsori  fco ON fco.GGXSorc=ggx.Controle ',
  '                        AND fco.GG1Dest=' + Geral.FF0(FGG1Dest),
  'LEFT JOIN graparcad  gpc ON gpc.Codigo=fco.Parte',
  'LEFT JOIN gragrux    ggxs ON ggxs.Controle=fco.GGXSubs',
  'LEFT JOIN gragruc    ggcs ON ggcs.Controle=ggxs.GraGruC ',
  'LEFT JOIN gracorcad  gccs ON gccs.Codigo=ggcs.GraCorCad ',
  'LEFT JOIN gratamits  gtis ON gtis.Controle=ggxs.GraTamI ',
  'LEFT JOIN gragru1    gg1s ON gg1s.Nivel1=ggxs.GraGru1 ',
  'LEFT JOIN unidmed    unms ON unms.Codigo=gg1s.UnidMed ',
  '',
  'WHERE ggx.GraGru1=' + Geral.FF0(GraGru1),
  'ORDER BY gcc.Nome, gti.Nome ',
  EmptyStr]);
  //Geral.MB_SQL(Self, QrFiConsOri);
  //
  if GGXToLoc <> 0 then
    QrFiConsOri.Locate('GraGruX', GGXToLoc, []);
end;

procedure TFmFiConsSpc.ReopenSrcArt();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrcArt, Dmod.MyDB, [
  'SELECT gg11.Nivel1, gg11.Nome NO_GG1_Sorc, ',
  'COUNT(fcs.Controle) ITENS  ',
  'FROM ficonsspc fcs ',
  'LEFT JOIN gragrux    ggx1 ON ggx1.Controle=fcs.GGXSorc ',
  'LEFT JOIN gragru1    gg11 ON gg11.Nivel1=ggx1.GraGru1  ',
  'WHERE fcs.GG1Dest=' + Geral.FF0(FGG1Dest),
  'GROUP BY gg11.Nivel1',
  'ORDER BY gg11.Nome ',
  EmptyStr]);
end;

procedure TFmFiConsSpc.ReopenSrcTeC();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSrcTeC, Dmod.MyDB, [
  'SELECT ggxa.GraTamI, gtia.Nome NO_TAM, ggxa.GraGruC,   ',
  'ggca.GraCorCad, gcca.Nome NO_COR, ggxa.Controle GraGruX, ',
  'COUNT(fcb.Controle) ITENS  ',
  'FROM ficonsspc fcb  ',
  '  ',
  'LEFT JOIN gragrux    ggxa ON ggxa.Controle=fcb.GGXSorc  ',
  'LEFT JOIN gragruc    ggca ON ggca.Controle=ggxa.GraGruC   ',
  'LEFT JOIN gracorcad  gcca ON gcca.Codigo=ggca.GraCorCad   ',
  'LEFT JOIN gratamits  gtia ON gtia.Controle=ggxa.GraTamI   ',
  'LEFT JOIN gragru1    gg1a ON gg1a.Nivel1=ggxa.GraGru1  ',
  '  ',
  'LEFT JOIN gragrux    ggxb ON ggxb.Controle=fcb.GGXSubs  ',
  'LEFT JOIN gragruc    ggcb ON ggcb.Controle=ggxb.GraGruC   ',
  'LEFT JOIN gracorcad  gccb ON gccb.Codigo=ggcb.GraCorCad   ',
  'LEFT JOIN gratamits  gtib ON gtib.Controle=ggxb.GraTamI   ',
  'LEFT JOIN gragru1    gg1b ON gg1b.Nivel1=ggxb.GraGru1  ',
  '  ',
  'LEFT JOIN graparcad  gpc  ON gpc.Codigo=fcb.Parte   ',
  'WHERE fcb.GG1Dest=' + Geral.FF0(FGG1Dest),
  'AND ggxa.GraGru1=' + Geral.FF0(QrSrcArtNivel1.Value),
  'GROUP BY ggxa.GraTamI, ggxa.GraGruC, ggca.GraCorCad  ',  EmptyStr]);
end;

procedure TFmFiConsSpc.Sb_1to1Click(Sender: TObject);
var
  CellSel: Boolean;
  Col, Row, Ativo, GGXDest, GGXSorc, DBGc, DBGl, ColVal: Integer;
  IsOK: Boolean;
  QtdUso: Double;
begin
  if MyObjects.FIC(CBGraGru1.KeyValue = Null, EdGraGru1,
  'Selecione o produto de origem!') then Exit;
  //
  if MyObjects.FIC(QrFiConsOri.RecordCount = 0, nil,
  'Selecione uma cor de origem v�lida!') then Exit;
  //
  CellSel := (GradeK.Col > 0) and (GradeK.Row > 1);
  if MyObjects.FIC(CellSel = False, GradeK,
  'Selecione o reduzido de desino!') then Exit;
  //
  Col := ((GradeK.Col - iFixedCols) div iColsPerItem) + 1;
  Row := GradeK.Row; // - iFixedRows + 1;
  //
  Ativo   := Geral.IMV(GradeA.Cells[Col, Row]);
  GGXDest := Geral.IMV(GradeC.Cells[Col, Row]);
  //
  if (Ativo = 1) and (GGXDest <> 0) then
  begin
    //
    GGXSorc := Dmod.MyDB.SelectInteger(
    ' SELECT ggx.Controle ' +
    ' FROM gragrux ggx ' +
    ' WHERE ggx.GraGru1=' + Geral.FF0(FOriGraGru1) +
    ' AND ggx.GraTamI=' + Geral.FF0(QrFiConsOriGraTamI.Value) +
    ' AND ggx.GraGruC=' + Geral.FF0(QrFiConsOriGraGruC.Value) +
    EmptyStr, IsOK, 0);
    if (IsOK = False) or (GGXSorc = 0) then
      Geral.MB_Aviso('Reduzido de origem n�o encontrado!')
    else
    begin
      ColVal := ((GradeK.Col  + (iColsPerItem -1)) div iColsPerItem) * iColsPerItem;
      QtdUso := Geral.DMV(GradeK.Cells[ColVal, GradeK.Row]);
      //if ObtemQuantidade(QtdUso) then
      if MostraFormFiConsOri(QtdUso) then
      begin
        InsereItem(FGG1Dest, GGXDest, GGXSorc, QrGraGru1GG1Subst.Value, QtdUso);
        //
        ReopenFiConsOri(FOriGraGru1, QrFiConsOriGraGruX.Value);
        DmProd.AtualizaGradesFiConsSpc(FGG1Dest, FOriGraGru1, QrFiConsOriGraCorCad.Value,
          QrFiConsOriGraTamI.Value, GradeK);
      end;
    end;
  end else
  begin
    if GGXDest = 0 then
      Geral.MB_Aviso('Reduzido de destino n�o existe!')
    else
    if Ativo <> 1 then
      Geral.MB_Aviso('Reduzido de destino n�o est� ativo!')
    else
  end;
end;

procedure TFmFiConsSpc.Sb_1toNClick(Sender: TObject);
var
  CellSel: Boolean;
  Col, Row, Ativo, GGXDest, GGXSorc, DBGc, DBGl, ColVal: Integer;
  IsOK: Boolean;
  QtdUso: Double;
  //
  GR: TGridRect;
begin
  if MyObjects.FIC(CBGraGru1.KeyValue = Null, EdGraGru1,
  'Selecione o produto de origem!') then Exit;
  //
  if MyObjects.FIC(QrFiConsOri.RecordCount = 0, nil,
  'Selecione uma cor de origem v�lida!') then Exit;
  //
  CellSel := (GradeK.Col > 0) and (GradeK.Row > 1);
  if MyObjects.FIC(CellSel = False, GradeK,
  'Selecione o reduzido de desino!') then Exit;
  //
  GR := GradeK.Selection;
  if MyObjects.FIC((GR.Top = GR.Bottom) and (GR.Left = GR.Right), nil,
  'Selecione mais de uma c�lula para multi inclus�es!') then Exit;
  //
  Col := GR.Left;
  Row := GR.Top;
  ColVal := ((GradeK.Col  + (iColsPerItem -1)) div iColsPerItem) * iColsPerItem;
  QtdUso := Geral.DMV(GradeK.Cells[ColVal, Row]);
  //
  if MostraFormFiConsOri(QtdUso) then
  begin
    try
      for Col := GR.Left to GR.Right do
      begin
        for Row := GR.Top to GR.Bottom do
        begin
          Ativo   := Geral.IMV(GradeA.Cells[Col, Row]);
          GGXDest := Geral.IMV(GradeC.Cells[Col, Row]);
          //
          if (Ativo = 1) and (GGXDest <> 0) then
          begin
            GGXSorc := Dmod.MyDB.SelectInteger(
            ' SELECT ggx.Controle ' +
            ' FROM gragrux ggx ' +
            ' WHERE ggx.GraGru1=' + Geral.FF0(FOriGraGru1) +
            ' AND ggx.GraTamI=' + Geral.FF0(QrFiConsOriGraTamI.Value) +
            ' AND ggx.GraGruC=' + Geral.FF0(QrFiConsOriGraGruC.Value) +
            EmptyStr, IsOK, 0);
            if (IsOK = False) or (GGXSorc = 0) then
              Geral.MB_Aviso('Reduzido de origem n�o encontrado!')
            else
            begin
              (*
              ColVal := ((GradeK.Col  + (iColsPerItem -1)) div iColsPerItem) * iColsPerItem;
              QtdUso := Geral.DMV(GradeK.Cells[ColVal, GradeK.Row]);
              if ObtemQuantidade(QtdUso) then
              if MostraFormFiConsOri(QtdUso) then*)
              begin
                InsereItem(FGG1Dest, GGXDest, GGXSorc, QrGraGru1GG1Subst.Value, QtdUso);
                //
              end;
            end;
          end else
          begin
          (*
            if GGXDest = 0 then
              Geral.MB_Aviso('Reduzido de destino n�o existe!')
            else
            if Ativo <> 1 then
              Geral.MB_Aviso('Reduzido de destino n�o est� ativo!')
            else
            *)
          end;
        end;
      end;
    finally
      ReopenFiConsOri(FOriGraGru1, QrFiConsOriGraGruX.Value);
      DmProd.AtualizaGradesFiConsSpc(FGG1Dest, FOriGraGru1, QrFiConsOriGraCorCad.Value,
        QrFiConsOriGraTamI.Value, GradeK);
    end;
  end;
end;

procedure TFmFiConsSpc.Sb_NtoNClick(Sender: TObject);
var
  GraCorCad, Col, Row, GGXSorc, GGXDest, Ativo: Integer;
  QUDefined: Boolean;
  QtdUso: Double;
begin
  QUDefined := False;
  QrFiConsOri.First;
  while not QrFiConsOri.Eof do
  begin
    for Row := 1 to GradeX.RowCount - 1 do
    begin
      GraCorCad := Geral.IMV(GradeX.Cells[1, Row]);
      if GraCorCad = QrFiConsOriGraCorCad.Value then
      begin
        GGXSorc := QrFiConsOriGraGruX.Value;
        for Col := 1 to GradeX.ColCount - 1 do
        begin
          if not QUDefined then
          begin
            if not MostraFormFiConsOri(QtdUso) then
              Exit
            else
              QUDefined := True;
          end;
          GGXDest := Geral.IMV(GradeC.Cells[Col, Row]);
          Ativo   := Geral.IMV(GradeA.Cells[Col, Row]);
          if (Ativo = 1) and (GGXDest <> 0) then
            InsereItem(FGG1Dest, GGXDest, GGXSorc, QrGraGru1GG1Subst.Value, QtdUso);
          //
        end;
      end;
    end;
    //
    QrFiConsOri.Next;
  end;
  if not QUDefined then
  begin
    ReopenFiConsOri(FOriGraGru1, QrFiConsOriGraGruX.Value);
    DmProd.AtualizaGradesFiConsSpc(FGG1Dest, FOriGraGru1, QrFiConsOriGraCorCad.Value,
      QrFiConsOriGraTamI.Value, GradeK);
  end;
end;

end.
