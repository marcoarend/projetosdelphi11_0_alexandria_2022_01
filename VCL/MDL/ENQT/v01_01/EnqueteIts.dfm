object FmEnqueteIts: TFmEnqueteIts
  Left = 339
  Top = 185
  Caption = 'CAD-ENQUE-002 :: Respostas da Enquete'
  ClientHeight = 662
  ClientWidth = 740
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 740
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 692
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 644
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 277
        Height = 32
        Caption = 'Respostas da Enquete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 277
        Height = 32
        Caption = 'Respostas da Enquete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 277
        Height = 32
        Caption = 'Respostas da Enquete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 740
    Height = 500
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 740
      Height = 500
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 740
        Height = 500
        Align = alClient
        TabOrder = 0
        object Splitter2: TSplitter
          Left = 2
          Top = 148
          Width = 736
          Height = 10
          Cursor = crVSplit
          Align = alBottom
          ExplicitLeft = -14
          ExplicitTop = 234
          ExplicitWidth = 740
        end
        object PNRespDes: TPanel
          Left = 2
          Top = 158
          Width = 736
          Height = 40
          Align = alBottom
          Constraints.MinHeight = 40
          TabOrder = 2
          object Label1: TLabel
            Left = 1
            Top = 1
            Width = 48
            Height = 13
            Align = alTop
            Caption = 'Resposta:'
            Color = clBtnFace
            ParentColor = False
          end
          object EdResposta: TdmkEdit
            Left = 1
            Top = 14
            Width = 734
            Height = 21
            Align = alTop
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object PNRespVar: TPanel
          Left = 2
          Top = 298
          Width = 736
          Height = 200
          Align = alBottom
          TabOrder = 0
          object DBGResposta: TDBGrid
            Left = 1
            Top = 1
            Width = 734
            Height = 192
            Align = alTop
            DataSource = DsRespMul
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDrawColumnCell = DBGRespostaDrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'Ativo'
                Width = 23
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RESPOSTA'
                ReadOnly = True
                Title.Caption = 'Resposta'
                Width = 330
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 330
                Visible = True
              end>
          end
        end
        object PNRespUni: TPanel
          Left = 2
          Top = 198
          Width = 736
          Height = 100
          Align = alBottom
          Constraints.MinHeight = 100
          TabOrder = 1
          object RGResposta: TRadioGroup
            Left = 1
            Top = 1
            Width = 734
            Height = 77
            Align = alClient
            Caption = 'Respostas'
            Columns = 2
            Items.Strings = (
              '1'
              '2'
              '3')
            TabOrder = 0
            OnClick = RGRespostaClick
          end
          object EdDescricao: TdmkEdit
            Left = 1
            Top = 78
            Width = 734
            Height = 21
            Align = alBottom
            TabOrder = 1
            Visible = False
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object PnEntidades: TPanel
          Left = 2
          Top = 15
          Width = 736
          Height = 133
          Align = alClient
          TabOrder = 3
          object DBGEntidades: TdmkDBGrid
            Left = 1
            Top = 51
            Width = 734
            Height = 81
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Width = 75
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ENTNOME'
                Title.Caption = 'Nome / Razao social'
                Width = 450
                Visible = True
              end>
            Color = clWindow
            DataSource = DsEntidades
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Width = 75
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ENTNOME'
                Title.Caption = 'Nome / Razao social'
                Width = 450
                Visible = True
              end>
          end
          object Panel5: TPanel
            Left = 1
            Top = 1
            Width = 734
            Height = 50
            Align = alTop
            TabOrder = 0
            object Label9: TLabel
              Left = 6
              Top = 5
              Width = 45
              Height = 13
              Caption = 'Entidade:'
              Color = clBtnFace
              ParentColor = False
            end
            object EdNome: TdmkEdit
              Left = 6
              Top = 21
              Width = 400
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'Nome'
              UpdCampo = 'Nome'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdNomeChange
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 548
    Width = 740
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 736
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 607
        Height = 16
        Caption = 
          'Para adicionar mais de uma entidade precione a tecla Ctrl e cliq' +
          'ue nas linhas que voc'#234' deseja selecionar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 607
        Height = 16
        Caption = 
          'Para adicionar mais de uma entidade precione a tecla Ctrl e cliq' +
          'ue nas linhas que voc'#234' deseja selecionar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 592
    Width = 740
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 594
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 592
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF (Tipo=0, RazaoSocial, Nome) ENTNOME'
      'FROM entidades'
      'WHERE IF (Tipo=0, RazaoSocial, Nome) LIKE "%:P0%"'
      'ORDER BY ENTNOME')
    Left = 144
    Top = 160
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesENTNOME: TWideStringField
      FieldName = 'ENTNOME'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 172
    Top = 160
  end
  object QrRespostas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM enqueteits'
      'WHERE Entidade=0'
      'AND Codigo=:P0'
      'ORDER BY Controle')
    Left = 200
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRespostasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRespostasControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrRespostasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrRespostasDescri: TWideStringField
      FieldName = 'Descri'
      Size = 100
    end
    object QrRespostasEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrRespostasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrRespostasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrRespostasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrRespostasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrRespostasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrRespostasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrRespostasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrRespostasEditDescri: TSmallintField
      FieldName = 'EditDescri'
    end
  end
  object DsRespostas: TDataSource
    DataSet = QrRespostas
    Left = 228
    Top = 160
  end
  object DsRespMul: TDataSource
    DataSet = TbRespMul
    Left = 284
    Top = 160
  end
  object QrRes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM enqueteits'
      'WHERE Entidade=0'
      'AND Codigo=:P0'
      'ORDER BY Nome')
    Left = 312
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrResCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrResControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrResNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrResDescri: TWideStringField
      FieldName = 'Descri'
      Size = 100
    end
    object QrResEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrResLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrResDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrResDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrResUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrResUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrResAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrResAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrResEditDescri: TSmallintField
      FieldName = 'EditDescri'
    end
  end
  object TbRespMul: TMySQLTable
    Database = DModG.MyPID_DB
    Filtered = True
    BeforeInsert = TbRespMulBeforeInsert
    BeforeDelete = TbRespMulBeforeDelete
    FieldDefs = <
      item
        Name = 'Codigo'
        DataType = ftInteger
      end
      item
        Name = 'Controle'
        DataType = ftInteger
      end
      item
        Name = 'Nome'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Descri'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Entidade'
        DataType = ftInteger
      end
      item
        Name = 'Lk'
        DataType = ftInteger
      end
      item
        Name = 'DataCad'
        DataType = ftDate
      end
      item
        Name = 'DataAlt'
        DataType = ftDate
      end
      item
        Name = 'UserCad'
        DataType = ftInteger
      end
      item
        Name = 'UserAlt'
        DataType = ftInteger
      end
      item
        Name = 'AlterWeb'
        DataType = ftSmallint
      end
      item
        Name = 'Ativo'
        DataType = ftSmallint
      end>
    IndexDefs = <
      item
        Name = 'Ordem'
        Fields = 'Ordem'
      end
      item
        Name = 'TbListaPreItsIndex2'
        Fields = 'Conta'
        Options = [ixPrimary, ixUnique]
      end
      item
        Name = 'idxParc'
        Fields = 'Parc'
      end
      item
        Name = 'idxValor'
        Fields = 'Valor'
      end>
    StoreDefs = True
    TableName = '_cod_nom_'
    Left = 256
    Top = 160
    object TbRespMulCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbRespMulNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object TbRespMulAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object TbRespMulRESPOSTA: TWideStringField
      FieldKind = fkLookup
      FieldName = 'RESPOSTA'
      LookupDataSet = QrRes
      LookupKeyFields = 'Controle'
      LookupResultField = 'Nome'
      KeyFields = 'Codigo'
      Size = 255
      Lookup = True
    end
    object TbRespMulTEMDESCRI: TSmallintField
      FieldKind = fkLookup
      FieldName = 'TEMDESCRI'
      LookupDataSet = QrRes
      LookupKeyFields = 'Controle'
      LookupResultField = 'EditDescri'
      KeyFields = 'Codigo'
      Lookup = True
    end
  end
end
