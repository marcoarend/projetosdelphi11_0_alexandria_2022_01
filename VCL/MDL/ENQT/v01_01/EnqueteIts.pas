unit EnqueteIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGrid, DBCGrids, Mask, UnDmkEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmEnqueteIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PNRespVar: TPanel;
    PNRespUni: TPanel;
    PNRespDes: TPanel;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesENTNOME: TWideStringField;
    QrRespostas: TmySQLQuery;
    QrRespostasCodigo: TIntegerField;
    QrRespostasControle: TIntegerField;
    QrRespostasNome: TWideStringField;
    QrRespostasDescri: TWideStringField;
    QrRespostasEntidade: TIntegerField;
    QrRespostasLk: TIntegerField;
    QrRespostasDataCad: TDateField;
    QrRespostasDataAlt: TDateField;
    QrRespostasUserCad: TIntegerField;
    QrRespostasUserAlt: TIntegerField;
    QrRespostasAlterWeb: TSmallintField;
    QrRespostasAtivo: TSmallintField;
    QrRespostasEditDescri: TSmallintField;
    RGResposta: TRadioGroup;
    EdDescricao: TdmkEdit;
    Splitter2: TSplitter;
    PnEntidades: TPanel;
    DBGEntidades: TdmkDBGrid;
    Panel5: TPanel;
    Label9: TLabel;
    EdNome: TdmkEdit;
    EdResposta: TdmkEdit;
    Label1: TLabel;
    DBGResposta: TDBGrid;
    DsRespostas: TDataSource;
    DsRespMul: TDataSource;
    QrRes: TmySQLQuery;
    QrResCodigo: TIntegerField;
    QrResControle: TIntegerField;
    QrResNome: TWideStringField;
    QrResDescri: TWideStringField;
    QrResEntidade: TIntegerField;
    QrResLk: TIntegerField;
    QrResDataCad: TDateField;
    QrResDataAlt: TDateField;
    QrResUserCad: TIntegerField;
    QrResUserAlt: TIntegerField;
    QrResAlterWeb: TSmallintField;
    QrResAtivo: TSmallintField;
    QrResEditDescri: TSmallintField;
    TbRespMul: TmySQLTable;
    TbRespMulCodigo: TIntegerField;
    TbRespMulNome: TWideStringField;
    TbRespMulAtivo: TSmallintField;
    TbRespMulRESPOSTA: TWideStringField;
    TbRespMulTEMDESCRI: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdNomeChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RGRespostaClick(Sender: TObject);
    procedure DBGRespostaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TbRespMulBeforeInsert(DataSet: TDataSet);
    procedure TbRespMulBeforeDelete(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenEntidades(Nome: String);
    procedure ReopenRespostas(Codigo: Integer);
  public
    { Public declarations }
    FCodigo, FTipResp: Integer;
    FRespostas: String;
  end;

  var
  FmEnqueteIts: TFmEnqueteIts;

implementation

uses Module, UnMyObjects, UCreate, ModuleGeral, MyVCLSkin, UMySQLModule,
  Enquete;

{$R *.DFM}

procedure TFmEnqueteIts.BtOKClick(Sender: TObject);
  procedure InsereRespDescri(Entidade: Integer);
  var
    Controle: Integer;
  begin
    if Entidade <> 0 then
    begin
      Controle := UMyMod.BuscaEmLivreY_Def('enqueteits', 'Controle', stIns, 0);
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO enqueteits SET Codigo=:P0, ');
      Dmod.QrUpd.SQL.Add('Controle=:P1, Nome=:P2, Descri=:P3, Entidade=:P4, ');
      Dmod.QrUpd.SQL.Add('EditDescri=:P5, DataCad=:P6, UserCad=:P7');
      Dmod.QrUpd.Params[0].AsInteger := FCodigo;
      Dmod.QrUpd.Params[1].AsInteger := Controle;
      Dmod.QrUpd.Params[2].AsString  := EdResposta.ValueVariant;
      Dmod.QrUpd.Params[3].AsString  := '';
      Dmod.QrUpd.Params[4].AsInteger := Entidade;
      Dmod.QrUpd.Params[5].AsInteger := 0;
      Dmod.QrUpd.Params[6].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpd.Params[7].AsInteger := VAR_USUARIO;
      Dmod.QrUpd.ExecSQL;
    end;
  end;
  procedure InsereRespSel(Entidade: Integer; Descri: String);
  var
    Controle: Integer;
  begin
    QrRespostas.First;
    while not QrRespostas.Eof do
    begin
      if RGResposta.ItemIndex = (QrRespostas.RecNo - 1) then
      begin
        if Entidade <> 0 then
        begin
          Controle := UMyMod.BuscaEmLivreY_Def('enqueteits', 'Controle', stIns, 0);
          //
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO enqueteits SET Codigo=:P0, ');
          Dmod.QrUpd.SQL.Add('Controle=:P1, Nome=:P2, Descri=:P3, Entidade=:P4, ');
          Dmod.QrUpd.SQL.Add('EditDescri=:P5, DataCad=:P6, UserCad=:P7');
          Dmod.QrUpd.Params[0].AsInteger := FCodigo;
          Dmod.QrUpd.Params[1].AsInteger := Controle;
          Dmod.QrUpd.Params[2].AsString  := QrRespostasNome.Value;
          Dmod.QrUpd.Params[3].AsString  := Descri;
          Dmod.QrUpd.Params[4].AsInteger := Entidade;
          Dmod.QrUpd.Params[5].AsInteger := 0;
          Dmod.QrUpd.Params[6].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
          Dmod.QrUpd.Params[7].AsInteger := VAR_USUARIO;
          Dmod.QrUpd.ExecSQL;
        end;
        Exit;
      end;
      QrRespostas.Next;
    end;
  end;
  procedure InsereRespSelMul(Entidade: Integer);
  var
    Controle: Integer;
  begin
    TbRespMul.First;
    while not TbRespMul.Eof do
    begin
      if TbRespMulAtivo.Value = 1 then
      begin
        if Entidade <> 0 then
        begin
          Controle := UMyMod.BuscaEmLivreY_Def('enqueteits', 'Controle', stIns, 0);
          //
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO enqueteits SET Codigo=:P0, ');
          Dmod.QrUpd.SQL.Add('Controle=:P1, Nome=:P2, Descri=:P3, Entidade=:P4, ');
          Dmod.QrUpd.SQL.Add('EditDescri=:P5, DataCad=:P6, UserCad=:P7');
          Dmod.QrUpd.Params[0].AsInteger := FCodigo;
          Dmod.QrUpd.Params[1].AsInteger := Controle;
          Dmod.QrUpd.Params[2].AsString  := TbRespMulRESPOSTA.Value;
          Dmod.QrUpd.Params[3].AsString  := TbRespMulNome.Value;
          Dmod.QrUpd.Params[4].AsInteger := Entidade;
          Dmod.QrUpd.Params[5].AsInteger := 0;
          Dmod.QrUpd.Params[6].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
          Dmod.QrUpd.Params[7].AsInteger := VAR_USUARIO;
          Dmod.QrUpd.ExecSQL;
        end;
      end;
      //
      TbRespMul.Next;
    end;
  end;
var
  Entidade, i: Integer;
begin
  if DBGEntidades.SelectedRows.Count > 1 then
  begin
    with DBGEntidades.DataSource.DataSet do
    for i:= 0 to DBGEntidades.SelectedRows.Count - 1 do
    begin
      GotoBookmark(DBGEntidades.SelectedRows.Items[i]);
      //
      Entidade := QrEntidadesCodigo.Value;
      //
      if PNRespDes.Visible = True then
      begin
        if MyObjects.FIC(Length(EdResposta.ValueVariant) = 0, EdResposta,
          'Defina uma resposta!') then Exit;
        InsereRespDescri(Entidade);
      end else
      if PNRespUni.Visible = True then
      begin
        if MyObjects.FIC(RGResposta.ItemIndex = -1, RGResposta,
          'Defina uma resposta!') then Exit;
        if EdDescricao.Visible = True then
        begin
          if MyObjects.FIC(Length(EdDescricao.ValueVariant) = 0, EdDescricao,
            'Defina uma descri��o!') then Exit;
        end;
        InsereRespSel(Entidade, EdDescricao.ValueVariant);
      end else
      if PNRespVar.Visible = True then
        InsereRespSelMul(Entidade);
    end;
  end else
  begin
    Entidade := QrEntidadesCodigo.Value;
    //
    if PNRespDes.Visible = True then
    begin
      if MyObjects.FIC(Length(EdResposta.ValueVariant) = 0, EdResposta,
        'Defina uma resposta!') then Exit;
      InsereRespDescri(Entidade);
    end else
    if PNRespUni.Visible = True then
    begin
      if MyObjects.FIC(RGResposta.ItemIndex = -1, RGResposta,
        'Defina uma resposta!') then Exit;
      if EdDescricao.Visible = True then
      begin
        if MyObjects.FIC(Length(EdDescricao.ValueVariant) = 0, EdDescricao,
          'Defina uma descri��o!') then Exit;
      end;
      InsereRespSel(Entidade, EdDescricao.ValueVariant);
    end else
    if PNRespVar.Visible = True then
      InsereRespSelMul(Entidade);
  end;
  if PNRespDes.Visible = True then
  begin
    Geral.MensagemBox('Dados salvos com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
    //
    EdResposta.ValueVariant := '';
    EdResposta.SetFocus;
  end else
  if PNRespUni.Visible = True then
    Geral.MensagemBox('Dados salvos com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING)
  else if PNRespVar.Visible = True then
  begin
    Geral.MensagemBox('Dados salvos com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
    //
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE ' + FRespostas + ' SET Nome="", Ativo=0');
    DModG.QrUpdPID1.ExecSQL;
    //
    TbRespMul.Close;
    TbRespMul.Open;
  end;
  FmEnquete.ReopenEnqueteIts(FCodigo);
end;

procedure TFmEnqueteIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEnqueteIts.DBGRespostaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if Column.FieldName = 'Ativo' then
    MeuVCLSkin.DrawGrid(DBGResposta, Rect, 1, TbRespMulAtivo.Value);
  if (Column.FieldName = 'Nome')then
  begin
    if TbRespMulTEMDESCRI.Value = 1 then
    begin
      Cor := clWindow;
    end else
    begin
      Cor := cl3DLight;
    end;
    with DBGResposta.Canvas do
    begin
      Brush.Color := Cor;
      Font.Color  := clBlack;
      //
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmEnqueteIts.EdNomeChange(Sender: TObject);
begin
  ReopenEntidades(EdNome.ValueVariant);
end;

procedure TFmEnqueteIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ImgTipo.SQLType := stPsq;
end;

procedure TFmEnqueteIts.FormCreate(Sender: TObject);
begin
  QrEntidades.Close;
end;

procedure TFmEnqueteIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEnqueteIts.FormShow(Sender: TObject);
begin
  case FTipResp of
    0://�nica alternativa
    begin
      PNRespUni.Visible := True;
      PNRespVar.Visible := False;
      PNRespDes.Visible := False;
      //
      ReopenRespostas(FCodigo);
      //
      RGResposta.Items.Clear;
      if QrRespostas.RecordCount > 0 then
      begin
        while not QrRespostas.Eof do
        begin
          RGResposta.Items.Add(QrRespostasNome.Value);
          //
          QrRespostas.Next;
        end;
      end;
      EdDescricao.ValueVariant := '';
    end;
    1://V�rias alternativas
    begin
      PNRespVar.Visible := True;
      PNRespUni.Visible := False;
      PNRespDes.Visible := False;
      //
      ReopenRespostas(FCodigo);
      if QrRespostas.RecordCount > 0 then
      begin
        FRespostas := UCriar.RecriaTempTableNovo(ntrtt_CodNom, DModG.QrUpdPID1, False);
        DModG.QrUpdPID1.SQL.Clear;
        DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + FRespostas + ' SET ');
        DModG.QrUpdPID1.SQL.Add('Codigo=:P0, Nome=:P1, Ativo=:P2');
        //
        QrRespostas.First;
        while not QrRespostas.Eof do
        begin
          DModG.QrUpdPID1.Params[0].AsInteger := QrRespostasControle.Value;
          DModG.QrUpdPID1.Params[1].AsString  := '';
          DModG.QrUpdPID1.Params[2].AsInteger := 0;
          DModG.QrUpdPID1.ExecSQL;
          //
          QrRespostas.Next;
        end;
        TbRespMul.Close;
        TbRespMul.TableName := FRespostas;
        TbRespMul.Open;
      end;
    end
    else //Resposta descritiva
    begin
      PNRespDes.Visible := True;
      PNRespUni.Visible := False;
      PNRespVar.Visible := False;
      //
      EdResposta.ValueVariant := '';
    end;
  end;
end;

procedure TFmEnqueteIts.ReopenEntidades(Nome: String);
begin
  QrEntidades.Close;
  QrEntidades.SQL.Clear;
  QrEntidades.SQL.Add('SELECT Codigo, IF (Tipo=0, RazaoSocial, Nome) ENTNOME');
  QrEntidades.SQL.Add('FROM entidades');
  QrEntidades.SQL.Add('WHERE IF (Tipo=0, RazaoSocial, Nome) LIKE "%'+ Nome +'%"');
  QrEntidades.SQL.Add('ORDER BY ENTNOME');
  QrEntidades.Open;
end;

procedure TFmEnqueteIts.ReopenRespostas(Codigo: Integer);
begin
  QrRespostas.Close;
  QrRespostas.Params[0].AsInteger := Codigo;
  QrRespostas.Open;
  //
  QrRes.Close;
  QrRes.Params[0].AsInteger := Codigo;
  QrRes.Open;
end;

procedure TFmEnqueteIts.RGRespostaClick(Sender: TObject);
begin
  QrRespostas.First;
  //
  EdDescricao.Visible := False;
  //
  while not QrRespostas.Eof do
  begin
    if RGResposta.ItemIndex = (QrRespostas.RecNo - 1) then
    begin
      if QrRespostasEditDescri.Value = 1 then
      begin
        EdDescricao.Visible := True;
        Exit;
      end;
    end;
    QrRespostas.Next;
  end;
end;

procedure TFmEnqueteIts.TbRespMulBeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TFmEnqueteIts.TbRespMulBeforeInsert(DataSet: TDataSet);
begin
  Abort;
end;

end.
