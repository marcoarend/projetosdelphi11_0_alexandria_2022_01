unit Enquete;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  Menus, Grids, DBGrids, dmkDBGrid, UnDmkProcFunc, frxClass, frxDBSet,
  UnDmkEnums;

type
  TFmEnquete = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtResp: TBitBtn;
    BtEnquete: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrEnquete: TmySQLQuery;
    DsEnquete: TDataSource;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    RGTipResp: TdmkRadioGroup;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Label8: TLabel;
    EdCodUsu: TdmkEdit;
    Label3: TLabel;
    EdDescri: TdmkEdit;
    dmkDBEdit1: TdmkDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    QrEnqueteCodigo: TIntegerField;
    QrEnqueteCodUsu: TIntegerField;
    QrEnqueteNome: TWideStringField;
    QrEnqueteDescri: TWideStringField;
    QrEnqueteLk: TIntegerField;
    QrEnqueteDataCad: TDateField;
    QrEnqueteDataAlt: TDateField;
    QrEnqueteUserCad: TIntegerField;
    QrEnqueteUserAlt: TIntegerField;
    QrEnqueteAlterWeb: TSmallintField;
    QrEnqueteAtivo: TSmallintField;
    QrEnqueteTipResp: TSmallintField;
    PMEnquete: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    dmkPermissoes1: TdmkPermissoes;
    TbEnqIts: TmySQLTable;
    DsEnqIts: TDataSource;
    TbEnqItsCodigo: TIntegerField;
    TbEnqItsControle: TIntegerField;
    TbEnqItsNome: TWideStringField;
    TbEnqItsDescri: TWideStringField;
    TbEnqItsEntidade: TIntegerField;
    TbEnqItsLk: TIntegerField;
    TbEnqItsDataCad: TDateField;
    TbEnqItsDataAlt: TDateField;
    TbEnqItsUserCad: TIntegerField;
    TbEnqItsUserAlt: TIntegerField;
    TbEnqItsAlterWeb: TSmallintField;
    TbEnqItsAtivo: TSmallintField;
    TbEnqItsEditDescri: TSmallintField;
    DBGRespPad: TDBGrid;
    GBRespostas: TGroupBox;
    QrEnqueteIts: TmySQLQuery;
    DsEnqueteIts: TDataSource;
    QrEnqueteItsNome: TWideStringField;
    QrEnqueteItsTotRep: TLargeintField;
    QrEnqueteEnt: TmySQLQuery;
    DsEnqueteEnt: TDataSource;
    QrEnqueteEntCodigo: TIntegerField;
    QrEnqueteEntENTNOME: TWideStringField;
    QrEnqueteEntControle: TIntegerField;
    PMResposta: TPopupMenu;
    Inclui2: TMenuItem;
    Exclui2: TMenuItem;
    DBGEntidade: TdmkDBGrid;
    DBGResposta: TdmkDBGrid;
    Splitter1: TSplitter;
    QrLoc: TmySQLQuery;
    PMImprime: TPopupMenu;
    Listadeparticipantes1: TMenuItem;
    Resultadodaenquete1: TMenuItem;
    N1: TMenuItem;
    frxCAD_ENQUE_001_001: TfrxReport;
    frxDsEnquete: TfrxDBDataset;
    QrParticipantes: TmySQLQuery;
    QrParticipantesENTNOME: TWideStringField;
    frxDsParticipantes: TfrxDBDataset;
    frxCAD_ENQUE_001_002: TfrxReport;
    frxDsEnqueteIts: TfrxDBDataset;
    QrTotParticip: TmySQLQuery;
    QrTotParticipTOTENT: TLargeintField;
    frxDsTotParticip: TfrxDBDataset;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEnqueteBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure BtEnqueteClick(Sender: TObject);
    procedure PMEnquetePopup(Sender: TObject);
    procedure TbEnqItsBeforeOpen(DataSet: TDataSet);
    procedure TbEnqItsBeforePost(DataSet: TDataSet);
    procedure DBGRespPadDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtRespClick(Sender: TObject);
    procedure QrEnqueteAfterScroll(DataSet: TDataSet);
    procedure QrEnqueteItsAfterOpen(DataSet: TDataSet);
    procedure QrEnqueteBeforeClose(DataSet: TDataSet);
    procedure QrEnqueteItsAfterScroll(DataSet: TDataSet);
    procedure QrEnqueteItsBeforeClose(DataSet: TDataSet);
    procedure Exclui2Click(Sender: TObject);
    procedure PMRespostaPopup(Sender: TObject);
    procedure Inclui2Click(Sender: TObject);
    procedure RGTipRespClick(Sender: TObject);
    procedure TbEnqItsBeforeDelete(DataSet: TDataSet);
    procedure Exclui1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Listadeparticipantes1Click(Sender: TObject);
    procedure Resultadodaenquete1Click(Sender: TObject);
  private
    //FPerguntas: String;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ReopenEnqueteEnt(Nome: String);
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
  public
    { Public declarations }
    procedure ReopenEnqueteIts(Codigo: Integer);
  end;

var
  FmEnquete: TFmEnquete;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects, MyVCLSkin, EnqueteIts, MyDBCheck;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEnquete.Listadeparticipantes1Click(Sender: TObject);
begin
  QrParticipantes.Close;
  QrParticipantes.Params[0].AsInteger := QrEnqueteCodigo.Value;
  QrParticipantes.Open;
  //
  MyObjects.frxMostra(frxCAD_ENQUE_001_001, 'Lista de participantes da enquete')
end;

procedure TFmEnquete.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEnquete.MostraEdicao(Mostra: Integer; SQLType: TSQLType;
  Codigo: Integer);
begin
  case Mostra of
   0:
    begin
      PnDados.Visible    := True;
      PnEdita.Visible    := False;
      DBGRespPad.Visible := False;
    end;
    1:
    begin
      PnDados.Visible    := False;
      PnEdita.Visible    := True;
      DBGRespPad.Visible := False;
      TbEnqIts.Open;
      //
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant  := FormatFloat(FFormatFloat, Codigo);
        EdCodUsu.ValueVariant  := 0;
        EdNome.ValueVariant    := '';
        EdDescri.ValueVariant  := '';
        RGTipResp.ItemIndex    := -1;
        DBGRespPad.Visible     := False;
      end else begin
        EdCodigo.ValueVariant := QrEnqueteCodigo.Value;
        EdCodUsu.ValueVariant := QrEnqueteCodUsu.Value;
        EdNome.ValueVariant   := QrEnqueteNome.Value;
        EdDescri.ValueVariant := QrEnqueteDescri.Value;
        RGTipResp.ItemIndex   := QrEnqueteTipResp.Value;
        //
        if QrEnqueteTipResp.Value in [0,1] then
          DBGRespPad.Visible := True;
        //
        QrLoc.Close;
        QrLoc.SQL.Clear;
        QrLoc.SQL.Add('SELECT Controle');
        QrLoc.SQL.Add('FROM enqueteits');
        QrLoc.SQL.Add('WHERE Codigo=:P0');
        QrLoc.SQL.Add('AND Entidade<>0');
        QrLoc.Params[0].AsInteger := QrEnqueteCodigo.Value;
        QrLoc.Open;
        if QrLoc.RecordCount > 0 then
          RGTipResp.Enabled := False
        else
          RGTipResp.Enabled := True;
      end;
      EdCodUsu.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmEnquete.Exclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  if(QrEnquete.State <> dsInactive) and (QrEnquete.RecordCount > 0) then
  begin
    Codigo := QrEnqueteCodigo.Value;
    //
    if (QrEnqueteIts.RecordCount > 0) then
    begin
      Geral.MensagemBox('Exclus�o cancelada!' + #13#10 +
        'Motivo esta enquete possui respostas cadastradas nela!', 'Aviso',
        MB_OK+MB_ICONWARNING);
      Exit;
    end;    
    //
    if Geral.MensagemBox('Confirma a exclus�o da enquete ID n�mero ' +
      FormatFloat('0', Codigo) + '?', 'Exclus�o de registro',
      MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM enquete WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
    end;
  end;
end;

procedure TFmEnquete.Exclui2Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEnqueteEnt, TDBGrid(DBGEntidade),
  'enqueteits', ['Controle'], ['Controle'], istPergunta, '');
  //
  ReopenEnqueteIts(QrEnqueteCodigo.Value);
end;

procedure TFmEnquete.PMEnquetePopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrEnquete.State <> dsInactive) and (QrEnquete.RecordCount > 0);
  //
  Altera1.Enabled := Enab;
  Exclui1.Enabled := Enab;
end;

procedure TFmEnquete.PMRespostaPopup(Sender: TObject);
var
  Enab1, Enab2: Boolean;
begin
  Enab1 := (QrEnquete.State <> dsInactive) and (QrEnquete.RecordCount > 0);
  Enab2 := (QrEnqueteIts.State <> dsInactive) and (QrEnqueteIts.RecordCount > 0);
  //
  Inclui2.Enabled := Enab1;
  Exclui2.Enabled := Enab1 and Enab2;
end;

procedure TFmEnquete.BtEnqueteClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PmEnquete, BtEnquete);
end;

procedure TFmEnquete.BtRespClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMResposta, BtResp);
end;

procedure TFmEnquete.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEnqueteCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEnquete.DefParams;
begin
  VAR_GOTOTABELA := 'enquete';
  VAR_GOTOMYSQLTABLE := QrEnquete;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM enquete');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmEnquete.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'enquete', 'CodUsu', [], [], stIns, 0, siPositivo, nil);
end;

procedure TFmEnquete.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEnquete.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEnquete.ReopenEnqueteEnt(Nome: String);
begin
  QrEnqueteEnt.Close;
  QrEnqueteEnt.Params[0].AsString := Nome;
  QrEnqueteEnt.Open;
end;

procedure TFmEnquete.ReopenEnqueteIts(Codigo: Integer);
begin
  QrEnqueteIts.Close;
  QrEnqueteIts.Params[0].AsInteger := Codigo;
  QrEnqueteIts.Open;
end;

procedure TFmEnquete.Resultadodaenquete1Click(Sender: TObject);
begin
  QrTotParticip.Close;
  QrTotParticip.Params[0].AsInteger := QrEnqueteCodigo.Value;
  QrTotParticip.Open;
  //
  MyObjects.frxMostra(frxCAD_ENQUE_001_002, 'Enquete')
end;

procedure TFmEnquete.RGTipRespClick(Sender: TObject);
begin
  if ImgTipo.SQLType = stUpd then
    DBGRespPad.Visible := RGTipResp.ItemIndex in [0,1];
end;

procedure TFmEnquete.DBGRespPadDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'EditDescri' then
    MeuVCLSkin.DrawGrid(DBGRespPad, Rect, 1, TbEnqItsEditDescri.Value);
end;

procedure TFmEnquete.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEnquete.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEnquete.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEnquete.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEnquete.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEnquete.TbEnqItsBeforeDelete(DataSet: TDataSet);
begin
  QrLoc.Close;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT Controle');
  QrLoc.SQL.Add('FROM enqueteits');
  QrLoc.SQL.Add('WHERE Nome=:P0');
  QrLoc.SQL.Add('AND Entidade<>0');
  QrLoc.Params[0].AsString := TbEnqItsNome.Value;
  QrLoc.Open;
  if QrLoc.RecordCount > 0 then
  begin
    Geral.MensagemBox('Este item n�o pode ser exclu�do!' + #13#10 +
      'Motivo: Ele foi utilizado em uma resposta.', 'Aviso', MB_OK+MB_ICONWARNING);
    Abort;
  end;
end;

procedure TFmEnquete.TbEnqItsBeforeOpen(DataSet: TDataSet);
begin
  TbEnqIts.Filter := 'Codigo=' + IntToStr(QrEnqueteCodigo.Value) + 'AND Entidade=0';
end;

procedure TFmEnquete.TbEnqItsBeforePost(DataSet: TDataSet);
begin
  if (TbEnqIts.State = dsInsert) then
  begin
    TbEnqItsDataCad.Value := Date;
    TbEnqItsUserCad.Value := VAR_USUARIO;
    TbEnqItsCodigo.Value  := QrEnqueteCodigo.Value;
    //
    if TbEnqItsControle.Value = 0 then
    begin
      TbEnqItsControle.ReadOnly := False;
      TbEnqItsControle.Value    := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
        'Controle', 'enqueteits', 'enqueteits', 'Controle');
      TbEnqItsControle.ReadOnly := True;
    end;
  end else begin
    TbEnqItsDataAlt.Value := Date;
    TbEnqItsUserAlt.Value := VAR_USUARIO;
  end;
end;

procedure TFmEnquete.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEnquete.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEnqueteCodigo.Value;
  Close;
end;

procedure TFmEnquete.Altera1Click(Sender: TObject);
begin
  TbEnqIts.Open;
  //
  {
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrEnquete, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'enquete');
  }
  MostraEdicao(1, stUpd, 0);
end;

procedure TFmEnquete.BtConfirmaClick(Sender: TObject);
var
  Codigo, TipResp: Integer;
  Nome, Descri: String;
begin
  Nome    := EdNome.ValueVariant;
  Descri  := EdDescri.ValueVariant;
  TipResp := RGTipResp.ItemIndex;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma pergunta!') then Exit;
  if MyObjects.FIC(TipResp = -1, RGTipResp, 'Defina o tipo de resposta!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('enquete', 'Codigo', ImgTipo.SQLType,
    QrEnqueteCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmEnquete, GBEdita, 'enquete',
    Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    if RGTipResp.ItemIndex = 2 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM enqueteits');
      Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0 AND Entidade=0');
      Dmod.QrUpd.Params[0].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
    end;
    TbEnqIts.Close;
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmEnquete.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  TbEnqIts.Close;
  //
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'enquete', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'enquete', 'Codigo');
end;

procedure TFmEnquete.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType   := stLok;
  DBGRespPad.Align  := alClient;
  GBRespostas.Align := alClient;
  //
  CriaOForm;
end;

procedure TFmEnquete.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEnqueteCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEnquete.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmEnquete.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEnquete.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrEnqueteCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEnquete.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEnquete.QrEnqueteAfterScroll(DataSet: TDataSet);
begin
  ReopenEnqueteIts(QrEnqueteCodigo.Value);
end;

procedure TFmEnquete.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmEnquete.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEnqueteCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'enquete', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEnquete.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEnquete.Inclui1Click(Sender: TObject);
begin
  TbEnqIts.Close;
  TbEnqIts.Open;
  //
  {
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrEnquete, [PnDados],
  [PnEdita], EdCodUsu, ImgTipo, 'enquete');
  }
  MostraEdicao(1, stIns, 0);
end;

procedure TFmEnquete.Inclui2Click(Sender: TObject);
begin
  if QrEnqueteTipResp.Value in [0,1] then
  begin
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT Controle FROM enqueteits');
    QrLoc.SQL.Add('WHERE Codigo=:P0 AND Entidade=0');
    QrLoc.Params[0].AsInteger := QrEnqueteCodigo.Value;
    QrLoc.Open;
    if QrLoc.RecordCount = 0 then
    begin
      if Geral.MensagemBox('Para respostas do tipo M�ltipla escolha voc� deve cadastrar respostas padr�o!' +
        #13#10 + 'Deseja cadastr�-las agora?', 'Pergunta',
        MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        MostraEdicao(1, stUpd, 0);
        Exit;
      end;
    end;
  end;
  if DBCheck.CriaFm(TFmEnqueteIts, FmEnqueteIts, afmoNegarComAviso) then
  begin
    FmEnqueteIts.FCodigo  := QrEnqueteCodigo.Value;
    FmEnqueteIts.FTipResp := QrEnqueteTipResp.Value;
    FmEnqueteIts.ShowModal;
    FmEnqueteIts.Destroy;
  end;
end;

procedure TFmEnquete.QrEnqueteBeforeClose(DataSet: TDataSet);
begin
  QrEnqueteIts.Close;
end;

procedure TFmEnquete.QrEnqueteBeforeOpen(DataSet: TDataSet);
begin
  QrEnqueteCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmEnquete.QrEnqueteItsAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEnquete.QrEnqueteItsAfterScroll(DataSet: TDataSet);
begin
  ReopenEnqueteEnt(QrEnqueteItsNome.Value);
end;

procedure TFmEnquete.QrEnqueteItsBeforeClose(DataSet: TDataSet);
begin
  QrEnqueteEnt.Close;
end;

end.

