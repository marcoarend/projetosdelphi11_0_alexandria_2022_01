object FmEnquete: TFmEnquete
  Left = 368
  Top = 194
  Caption = 'CAD-ENQUE-001 :: Cadastro de Enquetes'
  ClientHeight = 603
  ClientWidth = 924
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 924
    Height = 507
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 924
      Height = 169
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 137
        Top = 16
        Width = 46
        Height = 13
        Caption = 'Pergunta:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 137
        Top = 32
        Width = 572
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object RGTipResp: TdmkRadioGroup
        Left = 15
        Top = 100
        Width = 694
        Height = 60
        Caption = 'Tipo de resposta'
        Color = clBtnFace
        Columns = 2
        Items.Strings = (
          'M'#250'ltipla escolha (apenas uma resposta por pergunta)'
          'M'#250'ltipla escolha (uma ou mais respostas por pergunta)'
          'Texto livre (resposta descritiva)')
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        OnClick = RGTipRespClick
        QryCampo = 'TipResp'
        UpdCampo = 'TipResp'
        UpdType = utYes
        OldValor = 0
      end
      object EdCodUsu: TdmkEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdDescri: TdmkEdit
        Left = 16
        Top = 72
        Width = 693
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Descri'
        UpdCampo = 'Descri'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 444
      Width = 924
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 814
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object DBGRespPad: TDBGrid
      Left = 0
      Top = 169
      Width = 924
      Height = 192
      Align = alTop
      DataSource = DsEnqIts
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit, dgMultiSelect]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Visible = False
      OnDrawColumnCell = DBGRespPadDrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Pergunta'
          Width = 400
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EditDescri'
          Title.Caption = 'Permite adicionar descri'#231#227'o?'
          Width = 150
          Visible = True
        end>
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 924
    Height = 507
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 924
      Height = 185
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 136
        Top = 16
        Width = 46
        Height = 13
        Caption = 'Pergunta:'
        FocusControl = DBEdNome
      end
      object Label4: TLabel
        Left = 76
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = dmkDBEdit1
      end
      object Label5: TLabel
        Left = 16
        Top = 59
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = dmkDBEdit2
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsEnquete
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 136
        Top = 32
        Width = 573
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsEnquete
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 16
        Top = 105
        Width = 693
        Height = 60
        Caption = 'Dep'#243'sito'
        Columns = 2
        DataField = 'TipResp'
        DataSource = DsEnquete
        Items.Strings = (
          'M'#250'ltipla escolha (apenas uma resposta por pergunta)'
          'M'#250'ltipla escolha (uma ou mais respostas por pergunta)'
          'Texto livre (resposta descritiva)')
        ParentBackground = True
        TabOrder = 2
        Values.Strings = (
          '0'
          '1'
          '2')
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'CodUsu'
        DataSource = DsEnquete
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 3
        UpdType = utYes
        Alignment = taRightJustify
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 16
        Top = 75
        Width = 693
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Descri'
        DataSource = DsEnquete
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 443
      Width = 924
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 401
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtResp: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Respostas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtRespClick
        end
        object BtEnquete: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Enquete'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtEnqueteClick
        end
        object Panel2: TPanel
          Left = 412
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object GBRespostas: TGroupBox
      Left = 0
      Top = 185
      Width = 924
      Height = 232
      Align = alTop
      Caption = 'Respostas'
      TabOrder = 2
      object Splitter1: TSplitter
        Left = 452
        Top = 15
        Width = 10
        Height = 215
        ExplicitLeft = 334
        ExplicitHeight = 165
      end
      object DBGEntidade: TdmkDBGrid
        Left = 462
        Top = 15
        Width = 460
        Height = 215
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'ENTNOME'
            Title.Caption = 'Entidade'
            Width = 350
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'ID'
            Width = 45
            Visible = True
          end>
        Color = clWindow
        DataSource = DsEnqueteEnt
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'ENTNOME'
            Title.Caption = 'Entidade'
            Width = 350
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'ID'
            Width = 45
            Visible = True
          end>
      end
      object DBGResposta: TdmkDBGrid
        Left = 2
        Top = 15
        Width = 450
        Height = 215
        Align = alLeft
        Columns = <
          item
            Expanded = False
            FieldName = 'TotRep'
            Title.Caption = 'Total'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Resposta'
            Width = 350
            Visible = True
          end>
        Color = clWindow
        DataSource = DsEnqueteIts
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'TotRep'
            Title.Caption = 'Total'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Resposta'
            Width = 350
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 924
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 876
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 660
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 272
        Height = 32
        Caption = 'Cadastro de Enquetes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 272
        Height = 32
        Caption = 'Cadastro de Enquetes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 272
        Height = 32
        Caption = 'Cadastro de Enquetes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 924
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 920
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrEnquete: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrEnqueteBeforeOpen
    BeforeClose = QrEnqueteBeforeClose
    AfterScroll = QrEnqueteAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM enquete')
    Left = 64
    Top = 64
    object QrEnqueteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEnqueteCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEnqueteNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrEnqueteDescri: TWideStringField
      FieldName = 'Descri'
      Size = 100
    end
    object QrEnqueteLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEnqueteDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEnqueteDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEnqueteUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEnqueteUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEnqueteAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEnqueteAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEnqueteTipResp: TSmallintField
      FieldName = 'TipResp'
    end
  end
  object DsEnquete: TDataSource
    DataSet = QrEnquete
    Left = 92
    Top = 64
  end
  object PMEnquete: TPopupMenu
    OnPopup = PMEnquetePopup
    Left = 260
    Top = 64
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Inclui1
    CanUpd01 = Altera1
    CanDel01 = Exclui1
    Left = 232
    Top = 64
  end
  object TbEnqIts: TmySQLTable
    Database = Dmod.MyDB
    Filtered = True
    BeforeOpen = TbEnqItsBeforeOpen
    BeforePost = TbEnqItsBeforePost
    BeforeDelete = TbEnqItsBeforeDelete
    FieldDefs = <
      item
        Name = 'Codigo'
        DataType = ftInteger
      end
      item
        Name = 'Controle'
        DataType = ftInteger
      end
      item
        Name = 'Nome'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Descri'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Entidade'
        DataType = ftInteger
      end
      item
        Name = 'Lk'
        DataType = ftInteger
      end
      item
        Name = 'DataCad'
        DataType = ftDate
      end
      item
        Name = 'DataAlt'
        DataType = ftDate
      end
      item
        Name = 'UserCad'
        DataType = ftInteger
      end
      item
        Name = 'UserAlt'
        DataType = ftInteger
      end
      item
        Name = 'AlterWeb'
        DataType = ftSmallint
      end
      item
        Name = 'Ativo'
        DataType = ftSmallint
      end>
    IndexDefs = <
      item
        Name = 'Ordem'
        Fields = 'Ordem'
      end
      item
        Name = 'TbListaPreItsIndex2'
        Fields = 'Conta'
        Options = [ixPrimary, ixUnique]
      end
      item
        Name = 'idxParc'
        Fields = 'Parc'
      end
      item
        Name = 'idxValor'
        Fields = 'Valor'
      end>
    StoreDefs = True
    TableName = 'enqueteits'
    Left = 548
    Top = 16
    object TbEnqItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbEnqItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object TbEnqItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object TbEnqItsDescri: TWideStringField
      FieldName = 'Descri'
      Size = 100
    end
    object TbEnqItsEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object TbEnqItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbEnqItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbEnqItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbEnqItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbEnqItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbEnqItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object TbEnqItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object TbEnqItsEditDescri: TSmallintField
      FieldName = 'EditDescri'
      MaxValue = 1
    end
  end
  object DsEnqIts: TDataSource
    DataSet = TbEnqIts
    Left = 576
    Top = 16
  end
  object QrEnqueteIts: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEnqueteItsBeforeClose
    AfterScroll = QrEnqueteItsAfterScroll
    SQL.Strings = (
      'SELECT COUNT(Nome) TotRep, Nome'
      'FROM enqueteits'
      'WHERE Entidade <> 0'
      'AND Codigo=:P0'
      'GROUP BY Nome'
      'ORDER BY TotRep DESC')
    Left = 120
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEnqueteItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrEnqueteItsTotRep: TLargeintField
      FieldName = 'TotRep'
      Required = True
    end
  end
  object DsEnqueteIts: TDataSource
    DataSet = QrEnqueteIts
    Left = 148
    Top = 64
  end
  object QrEnqueteEnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT its.Controle, ent.Codigo, IF (ent.Tipo=0, ent.RazaoSocial' +
        ', ent.Nome) ENTNOME'
      'FROM enqueteits its'
      'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade'
      'WHERE its.Entidade <> 0'
      'AND its.Nome=:P0'
      'ORDER BY ENTNOME')
    Left = 176
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEnqueteEntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEnqueteEntENTNOME: TWideStringField
      FieldName = 'ENTNOME'
      Size = 100
    end
    object QrEnqueteEntControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsEnqueteEnt: TDataSource
    DataSet = QrEnqueteEnt
    Left = 204
    Top = 64
  end
  object PMResposta: TPopupMenu
    OnPopup = PMRespostaPopup
    Left = 288
    Top = 64
    object Inclui2: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui2Click
    end
    object Exclui2: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui2Click
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 604
    Top = 16
  end
  object PMImprime: TPopupMenu
    Left = 316
    Top = 64
    object Listadeparticipantes1: TMenuItem
      Caption = '&Lista de participantes'
      OnClick = Listadeparticipantes1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Resultadodaenquete1: TMenuItem
      Caption = 'Resultado da enquete'
      OnClick = Resultadodaenquete1Click
    end
  end
  object frxCAD_ENQUE_001_001: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.627769097200000000
    ReportOptions.LastChange = 41018.507888483800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 752
    Top = 144
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEnquete
        DataSetName = 'frxDsEnquete'
      end
      item
        DataSet = frxDsParticipantes
        DataSetName = 'frxDsParticipantes'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 13.228346460000000000
        Top = 211.653680000000000000
        Width = 680.315400000000000000
        Columns = 3
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsParticipantes
        DataSetName = 'frxDsParticipantes'
        RowCount = 0
        object Memo13: TfrxMemoView
          Width = 200.315068030000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsParticipantes
          DataSetName = 'frxDsParticipantes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsParticipantes."ENTNOME"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 22.677180000000000000
        Top = 325.039580000000000000
        Width = 680.315400000000000000
        object Memo4: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 94.488250000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'LISTA DE PARTICIPANTES DA ENQUETE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEnquete."Nome"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEnquete."Descri"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Height = 13.228346460000000000
        Top = 173.858380000000000000
        Width = 680.315400000000000000
        object Memo17: TfrxMemoView
          Width = 680.315314570000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NOME DO PARTICIPANTE')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 13.228346460000000000
        Top = 249.448980000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          Width = 680.315314570000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
      end
    end
  end
  object frxDsEnquete: TfrxDBDataset
    UserName = 'frxDsEnquete'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'CodUsu=CodUsu'
      'Nome=Nome'
      'Descri=Descri'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'TipResp=TipResp')
    DataSet = QrEnquete
    BCDToCurrency = False
    Left = 776
    Top = 216
  end
  object QrParticipantes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT DISTINCT  IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) ENTN' +
        'OME'
      'FROM enqueteits its'
      'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade'
      'WHERE its.Codigo=:P0'
      'AND its.Entidade <> 0'
      'ORDER BY ENTNOME')
    Left = 632
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParticipantesENTNOME: TWideStringField
      FieldName = 'ENTNOME'
      Size = 100
    end
  end
  object frxDsParticipantes: TfrxDBDataset
    UserName = 'frxDsParticipantes'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ENTNOME=ENTNOME')
    DataSet = QrParticipantes
    BCDToCurrency = False
    Left = 748
    Top = 216
  end
  object frxCAD_ENQUE_001_002: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.627769097200000000
    ReportOptions.LastChange = 41018.479676111100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 780
    Top = 144
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEnquete
        DataSetName = 'frxDsEnquete'
      end
      item
        DataSet = frxDsEnqueteIts
        DataSetName = 'frxDsEnqueteIts'
      end
      item
        DataSet = frxDsParticipantes
        DataSetName = 'frxDsParticipantes'
      end
      item
        DataSet = frxDsTotParticip
        DataSetName = 'frxDsTotParticip'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        Height = 71.811060240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEnquete."Nome"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEnquete."Descri"]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        Height = 302.362204720000000000
        Top = 151.181200000000000000
        Width = 680.315400000000000000
        RowCount = 1
        object Chart3: TfrxChartView
          Width = 699.213050000000000000
          Height = 302.362204720000000000
          ShowHint = False
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E4272757368
            2E436F6C6F720707636C5768697465144261636B57616C6C2E50656E2E566973
            69626C6508104C6567656E642E416C69676E6D656E7407086C61426F74746F6D
            124C6567656E642E466F6E742E48656967687402F8105469746C652E466F6E74
            2E436F6C6F720707636C426C61636B115469746C652E466F6E742E4865696768
            7402F0105469746C652E466F6E742E5374796C650B066673426F6C640011426F
            74746F6D417869732E4C6162656C730817426F74746F6D417869732E4C616265
            6C734F6E41786973080D4672616D652E56697369626C6508175669657733444F
            7074696F6E732E456C65766174696F6E033B01195669657733444F7074696F6E
            732E50657273706563746976650200165669657733444F7074696F6E732E526F
            746174696F6E0368010A426576656C4F75746572070662764E6F6E6505436F6C
            6F720707636C576869746511436F6C6F7250616C65747465496E646578020D00
            0A54426172536572696573000E436F6C6F7245616368506F696E7409134D6172
            6B732E4172726F772E56697369626C6509194D61726B732E43616C6C6F75742E
            42727573682E436F6C6F720707636C426C61636B1B4D61726B732E43616C6C6F
            75742E4172726F772E56697369626C6509144D61726B732E43616C6C6F75742E
            4C656E67746802280A4D61726B732E436C6970090D4D61726B732E5669736962
            6C65080C5856616C7565732E4E616D650601580D5856616C7565732E4F726465
            72070B6C6F417363656E64696E670C5956616C7565732E4E616D650603426172
            0D5956616C7565732E4F7264657207066C6F4E6F6E65000000}
          ChartElevation = 315
          SeriesData = <
            item
              DataType = dtDBData
              DataSet = frxDsEnqueteIts
              DataSetName = 'frxDsEnqueteIts'
              SortOrder = soNone
              TopN = 0
              XType = xtText
              Source1 = 'frxDsEnqueteIts."Nome"'
              Source2 = 'frxDsEnqueteIts."TotRep"'
              XSource = 'frxDsEnqueteIts."Nome"'
              YSource = 'frxDsEnqueteIts."TotRep"'
            end>
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        Height = 13.228346460000000000
        Top = 56.692950000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEnqueteIts
        DataSetName = 'frxDsEnqueteIts'
        RowCount = 0
        object Memo13: TfrxMemoView
          Width = 604.724778030000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Nome'
          DataSet = frxDsEnqueteIts
          DataSetName = 'frxDsEnqueteIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsEnqueteIts."Nome"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsEnqueteIts
          DataSetName = 'frxDsEnqueteIts'
          DisplayFormat.FormatStr = '%g'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEnqueteIts."TotRep"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Height = 13.228346460000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo3: TfrxMemoView
          Width = 604.724714570000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'RESPOSTAS')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590514570000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'VOTOS')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 17.007874020000000000
        Top = 94.488250000000000000
        Width = 718.110700000000000000
        object Memo2: TfrxMemoView
          Left = 30.236240000000000000
          Top = 3.779527560000000000
          Width = 574.488474570000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTotParticip."TOTENT"] participantes')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 604.724800000000000000
          Top = 3.779527560000000000
          Width = 75.590514570000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%g'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEnqueteIts."TotRep">)]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Top = 3.779530000000000000
          Width = 30.236154570000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAIS')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 22.677180000000000000
        Top = 173.858380000000000000
        Width = 718.110700000000000000
        object Memo4: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxDsEnqueteIts: TfrxDBDataset
    UserName = 'frxDsEnqueteIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Nome=Nome'
      'TotRep=TotRep')
    DataSet = QrEnqueteIts
    BCDToCurrency = False
    Left = 804
    Top = 216
  end
  object QrTotParticip: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(DISTINCT Entidade) TOTENT'
      'FROM enqueteits'
      'WHERE Codigo=:P0'
      'AND Entidade <> 0')
    Left = 660
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotParticipTOTENT: TLargeintField
      FieldName = 'TOTENT'
      Required = True
    end
  end
  object frxDsTotParticip: TfrxDBDataset
    UserName = 'frxDsTotParticip'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TOTENT=TOTENT')
    DataSet = QrTotParticip
    BCDToCurrency = False
    Left = 832
    Top = 216
  end
end
