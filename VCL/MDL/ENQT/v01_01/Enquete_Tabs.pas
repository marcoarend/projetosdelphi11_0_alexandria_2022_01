unit Enquete_Tabs;

{ Colocar no MyListas:

Uses Enquete_Tabs;


//
function TMyListas.CriaListaTabelas:

      Enquete_Tb.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
      Enquete_Tb.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:

    Enquete_Tb.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      Enquete_Tb.CarregaListaFRIndices(Tabela, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      Enquete_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos);
    Enquete_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  Enquete_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections, Windows, Messages, SysUtils, Classes, Graphics,
  Controls, Dialogs, DB, (*DBTables,*) UnMyLinguas, Forms, UnInternalConsts,
  dmkGeral, UnDmkEnums;

type
  TEnquete_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  Enquete_Tb: TEnquete_Tabs;

implementation

uses UMySQLModule;

function TEnquete_Tabs.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  try
    MyLinguas.AdTbLst(Lista, False, LowerCase('enquete'), '');
    MyLinguas.AdTbLst(Lista, False, LowerCase('enqueteits'), '');
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TEnquete_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if UpperCase(Tabela) = UpperCase('enquete') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"(Sem enquetes)"');
  end;
end;

function TEnquete_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('') then
  begin
  end else if Uppercase(Tabela) = Uppercase('enquete') then
  begin
    //FListaSQL.Add('"Entidades"|"Cadastro de pessoas f�sicas e jur�dicas (clientes| fornecedores| etc.)"');
    //FListaSQL.Add('""|""');
  end;
end;


function TEnquete_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('enquete') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'CodUsu';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('enqueteits') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end;
end;

function TEnquete_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('enquete') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);                       //0 = M�ltipla escolha (apenas uma resposta por pergunta)
      FRCampos.Field      := 'TipResp';    //1 = M�ltipla escolha (uma ou mais respostas por pergunta)
      FRCampos.Tipo       := 'tinyint(1)'; //2 = Texto livre (resposta descritiva)
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('enqueteits') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);                       //Campo utilizado para editar informar
      FRCampos.Field      := 'EditDescri'; //se o usu�rio poder� editar uma descri��o
      FRCampos.Tipo       := 'tinyint(1)'; //para a resposta selecionada
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TEnquete_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'enquete';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'enqueteits';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TEnquete_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  // CAD-ENQUE-001 :: Cadastro de Enquetes
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-ENQUE-001';
  FRJanelas.Nome      := 'FmEnquete';
  FRJanelas.Descricao := 'Cadastro de Enquetes';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-ENQUE-002 :: Respostas da Enquete
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-ENQUE-002';
  FRJanelas.Nome      := 'FmEnqueteIts';
  FRJanelas.Descricao := 'Respostas da Enquete';
  FLJanelas.Add(FRJanelas);
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
