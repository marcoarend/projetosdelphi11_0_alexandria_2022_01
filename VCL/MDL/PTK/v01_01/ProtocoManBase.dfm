object FmProtocoManBase: TFmProtocoManBase
  Left = 339
  Top = 185
  Caption = 'GER-PROTO-008 :: Itens de Protocolos (Base)'
  ClientHeight = 567
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 405
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitHeight = 398
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 782
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Grupo base:'
      end
      object EdProtBaseC: TdmkEditCB
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdProtBaseCChange
        OnEnter = EdProtBaseCEnter
        OnExit = EdProtBaseCExit
        DBLookupComboBox = CBProtBaseC
        IgnoraDBLookupComboBox = False
      end
      object CBProtBaseC: TdmkDBLookupComboBox
        Left = 68
        Top = 20
        Width = 701
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsProtBaseC
        TabOrder = 1
        dmkEditCB = EdProtBaseC
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 48
      Width = 784
      Height = 357
      Align = alClient
      DataSource = DsProtPakI
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      OnColEnter = DBGrid1ColEnter
      OnColExit = DBGrid1ColExit
      OnDrawColumnCell = DBGrid1DrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'DataE'
          Title.Caption = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_MANUAL'
          Title.Caption = 'Ocorr'#234'ncia'
          Width = 128
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          Title.Caption = 'Observa'#231#227'o'
          Width = 108
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MesAno'
          Title.Caption = 'Compet.'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SerieCH'
          Title.Caption = 'S'#233'rie'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Docum'
          Title.Caption = 'Docum.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Retorna'
          ReadOnly = True
          Title.Caption = 'R?'
          Width = 20
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LimiteSai'
          Title.Caption = 'Lim.Sai'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LimiteRem'
          Title.Caption = 'Lim. Rem.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LimiteRet'
          Title.Caption = 'Lim. Ret.'
          Width = 56
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 322
        Height = 32
        Caption = 'Itens de Protocolos (Base)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 322
        Height = 32
        Caption = 'Itens de Protocolos (Base)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 322
        Height = 32
        Caption = 'Itens de Protocolos (Base)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 453
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 378
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 497
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 422
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrProtBaseC: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrProtBaseCAfterOpen
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM protbasec'
      'ORDER BY Nome')
    Left = 188
    Top = 56
    object QrProtBaseCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProtBaseCCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrProtBaseCNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsProtBaseC: TDataSource
    DataSet = QrProtBaseC
    Left = 216
    Top = 56
  end
  object QrProtBaseI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Ocorrencia, incPerio, Retorna,'
      'Texto, LimiteSai, LimiteRem, LimiteRet,'
      'IncVencto, Valor'
      'FROM protbasei'
      'WHERE Codigo=:P0')
    Left = 248
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProtBaseIOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
    end
    object QrProtBaseIincPerio: TSmallintField
      FieldName = 'incPerio'
    end
    object QrProtBaseIRetorna: TSmallintField
      FieldName = 'Retorna'
    end
    object QrProtBaseITexto: TWideStringField
      FieldName = 'Texto'
      Size = 30
    end
    object QrProtBaseILimiteSai: TIntegerField
      FieldName = 'LimiteSai'
    end
    object QrProtBaseILimiteRem: TIntegerField
      FieldName = 'LimiteRem'
    end
    object QrProtBaseILimiteRet: TIntegerField
      FieldName = 'LimiteRet'
    end
    object QrProtBaseIIncVencto: TIntegerField
      FieldName = 'IncVencto'
    end
    object QrProtBaseIValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object TbProtPakI: TmySQLTable
    Database = DModG.MyPID_DB
    BeforePost = TbProtPakIBeforePost
    TableName = 'ProtPakI'
    Left = 284
    Top = 56
    object TbProtPakIManual: TIntegerField
      FieldName = 'Manual'
    end
    object TbProtPakIDataE: TDateField
      FieldName = 'DataE'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbProtPakIMesAno: TDateField
      FieldName = 'MesAno'
      DisplayFormat = 'mm/yy'
      EditMask = '\0\1/90/0000;1;_'
    end
    object TbProtPakIDepto: TIntegerField
      FieldName = 'Depto'
    end
    object TbProtPakISerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object TbProtPakIDocum: TFloatField
      FieldName = 'Docum'
      DisplayFormat = '000000;-000000; '
    end
    object TbProtPakIValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object TbProtPakIVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbProtPakIRetorna: TSmallintField
      FieldName = 'Retorna'
    end
    object TbProtPakITexto: TWideStringField
      FieldName = 'Texto'
      Size = 30
    end
    object TbProtPakILimiteSai: TDateField
      FieldName = 'LimiteSai'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbProtPakILimiteRem: TDateField
      FieldName = 'LimiteRem'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbProtPakILimiteRet: TDateField
      FieldName = 'LimiteRet'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbProtPakINO_MANUAL: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NO_MANUAL'
      LookupDataSet = QrProtocoOco
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Manual'
      Size = 50
      Lookup = True
    end
  end
  object DsProtPakI: TDataSource
    DataSet = TbProtPakI
    Left = 312
    Top = 56
  end
  object QrProtocoOco: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM protocooco')
    Left = 348
    Top = 56
    object QrProtocoOcoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProtocoOcoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
end
