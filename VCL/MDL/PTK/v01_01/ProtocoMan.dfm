object FmProtocoMan: TFmProtocoMan
  Left = 339
  Top = 185
  Caption = 'GER-PROTO-005 :: Item de Protocolo Manual'
  ClientHeight = 516
  ClientWidth = 493
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 137
    Width = 493
    Height = 265
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 64
      Top = 4
      Width = 55
      Height = 13
      Caption = 'Ocorr'#234'ncia:'
    end
    object Label2: TLabel
      Left = 8
      Top = 48
      Width = 26
      Height = 13
      Caption = 'Data:'
    end
    object Label3: TLabel
      Left = 116
      Top = 48
      Width = 41
      Height = 13
      Caption = 'Per'#237'odo:'
    end
    object Label4: TLabel
      Left = 8
      Top = 132
      Width = 61
      Height = 13
      Caption = 'Observa'#231#227'o:'
    end
    object LaDepto: TLabel
      Left = 328
      Top = 48
      Width = 100
      Height = 13
      Caption = 'Unidade habitacional'
    end
    object LaDoc: TLabel
      Left = 6
      Top = 88
      Width = 104
      Height = 13
      Caption = 'S'#233'rie  e docum. (CH) :'
    end
    object Label9: TLabel
      Left = 8
      Top = 4
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object SpeedButton5: TSpeedButton
      Left = 460
      Top = 20
      Width = 21
      Height = 21
      Hint = 'Inclui item de carteira'
      Caption = '...'
      OnClick = SpeedButton5Click
    end
    object Label5: TLabel
      Left = 158
      Top = 88
      Width = 27
      Height = 13
      Caption = 'Valor:'
    end
    object Label6: TLabel
      Left = 240
      Top = 88
      Width = 59
      Height = 13
      Caption = 'Vencimento:'
    end
    object EdPeriodo_txt: TdmkEdit
      Left = 168
      Top = 64
      Width = 157
      Height = 21
      TabStop = False
      ReadOnly = True
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdManual: TdmkEditCB
      Left = 64
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Manual'
      UpdCampo = 'Manual'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBManual
      IgnoraDBLookupComboBox = False
    end
    object CBManual: TdmkDBLookupComboBox
      Left = 124
      Top = 20
      Width = 333
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsProtocoOco
      TabOrder = 2
      dmkEditCB = EdManual
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object TPDataE: TdmkEditDateTimePicker
      Left = 8
      Top = 64
      Width = 105
      Height = 21
      Date = 39786.892084409720000000
      Time = 39786.892084409720000000
      TabOrder = 3
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'DataE'
      UpdCampo = 'DataE'
      UpdType = utYes
    end
    object EdPeriodo: TdmkEdit
      Left = 116
      Top = 64
      Width = 48
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Periodo'
      UpdCampo = 'Periodo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdPeriodoChange
    end
    object EdDepto: TdmkEditCB
      Left = 328
      Top = 64
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Depto'
      UpdCampo = 'Depto'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBDepto
      IgnoraDBLookupComboBox = False
    end
    object CBDepto: TdmkDBLookupComboBox
      Left = 384
      Top = 64
      Width = 97
      Height = 21
      KeyField = 'Conta'
      ListField = 'Unidade'
      ListSource = DsCondImov
      TabOrder = 7
      dmkEditCB = EdDepto
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdSerieCH: TdmkEdit
      Left = 8
      Top = 104
      Width = 80
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 8
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'SerieCH'
      UpdCampo = 'SerieCH'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdDoc: TdmkEdit
      Left = 88
      Top = 104
      Width = 62
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 6
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000000'
      QryCampo = 'Docum'
      UpdCampo = 'Docum'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdConta: TdmkEdit
      Left = 8
      Top = 20
      Width = 53
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Conta'
      UpdCampo = 'Conta'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object CkContinuar: TCheckBox
      Left = 8
      Top = 240
      Width = 121
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 15
    end
    object dmkEdit1: TdmkEdit
      Left = 8
      Top = 148
      Width = 472
      Height = 21
      TabOrder = 13
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Texto'
      UpdCampo = 'Texto'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object dmkRadioGroup1: TdmkRadioGroup
      Left = 352
      Top = 88
      Width = 129
      Height = 41
      Caption = ' Retorna: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'N'#227'o'
        'Sim')
      TabOrder = 12
      OnClick = dmkRadioGroup1Click
      QryCampo = 'Retorna'
      UpdCampo = 'Retorna'
      UpdType = utYes
      OldValor = 0
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 172
      Width = 473
      Height = 65
      Caption = ' Datas limites do item de protocolo: '
      TabOrder = 14
      object LaLimiteSai: TLabel
        Left = 12
        Top = 16
        Width = 32
        Height = 13
        Caption = 'Sa'#237'da:'
      end
      object LaLimiteRem: TLabel
        Left = 136
        Top = 16
        Width = 98
        Height = 13
        Caption = 'Chegada no destino:'
      end
      object LaLimiteRet: TLabel
        Left = 260
        Top = 16
        Width = 41
        Height = 13
        Caption = 'Retorno:'
        Visible = False
      end
      object TPLimiteSai: TdmkEditDateTimePicker
        Left = 12
        Top = 32
        Width = 109
        Height = 21
        Date = 40238.476802430550000000
        Time = 40238.476802430550000000
        TabOrder = 0
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'LimiteSai'
        UpdCampo = 'LimiteSai'
        UpdType = utYes
      end
      object TPLimiteRem: TdmkEditDateTimePicker
        Left = 136
        Top = 32
        Width = 109
        Height = 21
        Date = 40238.476802430550000000
        Time = 40238.476802430550000000
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'LimiteRem'
        UpdCampo = 'LimiteRem'
        UpdType = utYes
      end
      object TPLimiteRet: TdmkEditDateTimePicker
        Left = 260
        Top = 32
        Width = 109
        Height = 21
        Date = 40238.476802430550000000
        Time = 40238.476802430550000000
        TabOrder = 2
        Visible = False
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'LimiteRet'
        UpdCampo = 'LimiteRet'
        UpdType = utYes
      end
    end
    object EdValor: TdmkEdit
      Left = 156
      Top = 104
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Valor'
      UpdCampo = 'Valor'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object TPVencto: TdmkEditDateTimePicker
      Left = 240
      Top = 104
      Width = 109
      Height = 21
      Date = 40238.476802430550000000
      Time = 40238.476802430550000000
      TabOrder = 11
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'Vencto'
      UpdCampo = 'Vencto'
      UpdType = utYes
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 493
    Height = 89
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label7: TLabel
      Left = 8
      Top = 8
      Width = 34
      Height = 13
      Caption = 'Tarefa:'
    end
    object Label8: TLabel
      Left = 408
      Top = 8
      Width = 24
      Height = 13
      Caption = 'Lote:'
    end
    object Label10: TLabel
      Left = 8
      Top = 48
      Width = 70
      Height = 13
      Caption = 'Cliente interno:'
    end
    object dmkDBEdit1: TdmkDBEdit
      Left = 8
      Top = 24
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      DataSource = FmProtocolos.DsProtocolos
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object dmkDBEdit2: TdmkDBEdit
      Left = 408
      Top = 24
      Width = 72
      Height = 21
      TabStop = False
      DataField = 'Controle'
      DataSource = FmProtocolos.DsProtocoPak
      TabOrder = 2
      UpdCampo = 'Controle'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object DBEdit1: TDBEdit
      Left = 64
      Top = 24
      Width = 341
      Height = 21
      TabStop = False
      DataField = 'Nome'
      DataSource = FmProtocolos.DsProtocolos
      TabOrder = 1
    end
    object DBEdit2: TDBEdit
      Left = 64
      Top = 64
      Width = 417
      Height = 21
      DataField = 'NOME_CLIENT'
      DataSource = FmProtocolos.DsProtocolos
      TabOrder = 4
    end
    object EdCliInt: TdmkEdit
      Left = 8
      Top = 64
      Width = 53
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'CliInt'
      UpdCampo = 'CliInt'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 493
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 445
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 397
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 310
        Height = 32
        Caption = 'Item de Protocolo Manual'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 310
        Height = 32
        Caption = 'Item de Protocolo Manual'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 310
        Height = 32
        Caption = 'Item de Protocolo Manual'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 402
    Width = 493
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 489
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 446
    Width = 493
    Height = 70
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 347
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 13
        Left = 8
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 345
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrProtocoOco: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM protocooco'
      'ORDER BY Nome')
    Left = 180
    Top = 56
    object QrProtocoOcoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProtocoOcoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsProtocoOco: TDataSource
    DataSet = QrProtocoOco
    Left = 208
    Top = 56
  end
  object QrCondImov: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cim.Conta, cib.Descri BLOCO, cim.Unidade'
      'FROM condimov cim'
      'LEFT JOIN condbloco cib ON cib.Controle=cim.Controle'
      'WHERE cim.Codigo=:P0'
      'ORDER BY cib.Descri, cim.Unidade')
    Left = 252
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondImovConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrCondImovBLOCO: TWideStringField
      FieldName = 'BLOCO'
      Size = 100
    end
    object QrCondImovUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
  end
  object DsCondImov: TDataSource
    DataSet = QrCondImov
    Left = 280
    Top = 56
  end
end
