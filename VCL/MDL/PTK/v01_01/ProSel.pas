unit ProSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, DB, mySQLDbTables,
  dmkEdit, dmkEditCB, DBCtrls, dmkDBLookupComboBox, ComCtrls,
  dmkEditDateTimePicker, dmkGeral, dmkImage, DmkDAC_PF, dmkPermissoes, UnDmkEnums;

type
  TFmProSel = class(TForm)
    Panel1: TPanel;
    PainelDados: TPanel;
    QrProtocolos: TmySQLQuery;
    QrProtocolosCodigo: TIntegerField;
    QrProtocolosNome: TWideStringField;
    QrProtocolosDef_Retorn: TIntegerField;
    DsProtocolos: TDataSource;
    QrProtocoPak: TmySQLQuery;
    QrProtocoPakCodigo: TIntegerField;
    QrProtocoPakControle: TIntegerField;
    QrProtocoPakDataI: TDateField;
    QrProtocoPakDataL: TDateField;
    QrProtocoPakDataF: TDateField;
    QrProtocoPakLk: TIntegerField;
    QrProtocoPakDataCad: TDateField;
    QrProtocoPakDataAlt: TDateField;
    QrProtocoPakUserCad: TIntegerField;
    QrProtocoPakUserAlt: TIntegerField;
    QrProtocoPakAlterWeb: TSmallintField;
    QrProtocoPakAtivo: TSmallintField;
    QrProtocoPakMez: TIntegerField;
    QrProtocoPakMES: TWideStringField;
    QrProtocoPakDATAF_TXT: TWideStringField;
    DsProtocoPak: TDataSource;
    Label3: TLabel;
    CBTarefa: TdmkDBLookupComboBox;
    EdTarefa: TdmkEditCB;
    SpeedButton1: TSpeedButton;
    CkRetorna: TCheckBox;
    GroupBox1: TGroupBox;
    LaLimiteSai: TLabel;
    LaLimiteRem: TLabel;
    LaLimiteRet: TLabel;
    TPLimiteSai: TdmkEditDateTimePicker;
    TPLimiteRem: TdmkEditDateTimePicker;
    TPLimiteRet: TdmkEditDateTimePicker;
    Panel4: TPanel;
    Panel6: TPanel;
    CkAbertos: TCheckBox;
    StaticText1: TStaticText;
    DBGrid1: TDBGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel3: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BitBtn1: TBitBtn;
    QrProtocolosTipo: TIntegerField;
    QrProtocolosDef_Client: TIntegerField;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdTarefaChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CkAbertosClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure CkRetornaClick(Sender: TObject);
  private
    { Private declarations }
    FLimiteSai, FLimiteRem, FLimiteRet: String;
    procedure ReopenProtocoPak(Controle: Integer);
  public
    { Public declarations }
    FProtocolo, FEntidade, FPeriodo, FPrevCod, FRecipItem, FLote: Integer;
    FBloqueto, FValor: Double;
    FVencto: TDate;
    FQuais: TSelType;
    FNomeQrSrc, FRecipEmeio: String;
    FQrSource, FQrLci: TmySQLQuery;
    FDBGrid: TDBGrid;
    FAskTodos, FInseriu, FDesistiu: Boolean;
    procedure ReopenProtocolos(TipoProtocolo, Tarefa, Def_Client: Integer);
  end;

  var
  FmProSel: TFmProSel;

implementation

uses ProtocoPak, Module, MyDBCheck, UnInternalConsts, UMySQLModule, Protocolo,
  Protocolos, UnFinanceiro, UnMyObjects, ModuleGeral, UnBloqGerl;

{$R *.DFM}

procedure TFmProSel.BitBtn1Click(Sender: TObject);
var
  Codigo: Integer;
  Hoje: TDateTime;
begin
  Codigo := EdTarefa.ValueVariant;
  Hoje   := DModG.ObtemAgora();
  //
  if MyObjects.FIC(Codigo = 0, EdTarefa, 'Informe a terefa!') then Exit;
  //
  if DBCheck.CriaFm(TFmProtocoPak, FmProtocoPak, afmoNegarComAviso) then
  begin
    FmProtocoPak.dmkEdDataI.ValueVariant := Hoje;
    FmProtocoPak.dmkEdDataL.ValueVariant := Hoje;
    FmProtocoPak.EdMes.Text              := Geral.FDT(Hoje, 14);
    FmProtocoPak.ImgTipo.SQLType         := stIns;
    FmProtocoPak.FCodigo                 := Codigo;
    FmProtocoPak.FControle               := 0;
    FmProtocoPak.FTipo                   := QrProtocolosTipo.Value;
    FmProtocoPak.FDef_Client             := QrProtocolosDef_Client.Value;
    //
    FmProtocoPak.ShowModal;
    if FmProtocoPak.FExecuted then
    begin
      ReopenProtocoPak(FmProtocoPak.FControle);
    end;
    FmProtocoPak.Destroy;
  end;
end;

procedure TFmProSel.BtConfirmaClick(Sender: TObject);

  procedure GeraAtual(var SemEmeio, SemQuery: Integer);

    procedure GeraProtocolo_DocumLanctos(var SemQuery: Integer);
    var
      Protocolo, Controle, Sub, Conta, CliInt, Cliente, Fornece, ITENS, Lancto,
      Tipo, Carteira: Integer;
      Credito, Debito, Documento: Double;
      SerieCH: String;
      Data, Vencto: TDateTime;
    begin
      ITENS     := 0; 
      Protocolo := -1;
      Controle  := 0;
      CliInt    := 0;
      Cliente   := 0;
      Fornece   := 0;
      Credito   := 0;
      Debito    := 0;
      Documento := 0;
      Vencto    := 0;
      Sub       := 0;
      Tipo      := 0;
      Carteira  := 0;
      Data      := 0;
      if FQrSource <> nil then
      begin
        Protocolo := 0;
        Tipo      := FQrSource.FieldByName('Tipo'      ).AsInteger;
        Carteira  := FQrSource.FieldByName('Carteira'  ).AsInteger;
        Controle  := FQrSource.FieldByName('Controle'  ).AsInteger;
        Sub       := FQrSource.FieldByName('Sub'       ).AsInteger;
        CliInt    := FQrSource.FieldByName('CliInt'    ).AsInteger;
        Cliente   := FQrSource.FieldByName('Cliente'   ).AsInteger;
        Fornece   := FQrSource.FieldByName('Fornecedor').AsInteger;
        //
        Documento := FQrSource.FieldByName('Documento' ).AsFloat;
        Credito   := FQrSource.FieldByName('Credito'   ).AsFloat;
        Debito    := FQrSource.FieldByName('Debito'    ).AsFloat;
        //
        SerieCH   := FQrSource.FieldByName('SerieCH'   ).AsString;
        //
        Data      := FQrSource.FieldByName('Data'      ).AsDateTime;
        Vencto    := FQrSource.FieldByName('Vencimento').AsDateTime;
        //
        ITENS     := FQrSource.FieldByName('ITENS').AsInteger;
      end else begin
        Inc(SemQuery, 1);
      end;
      if Protocolo = 0 then
      begin
        Conta := UMyMod.BuscaEmLivreY_Def('protpakits', 'conta', stIns, 0);
        //
        // Incluir antes o itens (quando mais de um!
        if ITENS > 1 then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(FQrLcI, FQrLcI.Database, [
          'SELECT Controle Lancto ',
          'FROM ' + VAR_ModBloq_TabLctA,
          'WHERE Data="' + Geral.FDT(Data, 1) + '"',
          'AND Tipo=' + Geral.FF0(Tipo),
          'AND Carteira=' + Geral.FF0(Carteira),
          'AND CliInt=' + Geral.FF0(CliInt),
          'AND SerieCH="' + SerieCH + '"',
          'AND Documento=' + Geral.FFI(Documento),
          ' ']);
          FQrLcI.First;
          while not FQrLcI.Eof do
          begin
            Lancto := FQrLcI.FieldByName('Lancto').AsInteger;
            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'protpakmul', False, [
            'Codigo', 'Controle'], [
            'Conta', 'Lancto'], [
            QrProtocoPakCodigo.Value,
            QrProtocoPakControle.Value],
            [Conta, Lancto], True) then ;
            // J� antecipar aqui se tiver mais de um lancto com o mesmo protocolo!
            UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
              'Protocolo'], ['Controle', 'Sub'], [
               Conta], [Lancto, Sub], True, '', VAR_ModBloq_TabLctA);
            //
            FQrLcI.Next;
          end;
        end;

        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'protpakits', False,
        [
          'Codigo', 'Controle', 'Link_ID', 'CliInt',
          'Cliente', 'Fornece', 'Periodo', 'Lancto',
          'Docum', 'Depto',
          'ID_Cod1', 'ID_Cod2', 'ID_Cod3', 'ID_Cod4',
          'Retorna', 'Vencto',
          'Valor', 'MoraDiaVal', 'MultaVal', 'Cedente',
          'SerieCH', 'LimiteSai', 'LimiteRem', 'LimiteRet'
        ], ['Conta'], [
          QrProtocoPakCodigo.Value, QrProtocoPakControle.Value,
          VAR_TIPO_LINK_ID_01_GENERICO, CliInt,
          Cliente, Fornece, 0(*Periodo*), Controle,
          Documento, 0,
          0(*FID_Cod1*), 0(*FID_Cod2*), 0(*FID_Cod3*), 0(*FID_Cod4*),
          Geral.BoolToInt(CkRetorna.Checked), Geral.FDT(Vencto, 1),
          Credito-Debito, 0(*MoraDiaVal*), 0(*MultaVal*), 0(*Cedente*),
          SerieCH, FLimiteSai, FLimiteRem, FLimiteRet
        ], [Conta], True) then
        // erro no controle de lancamentos?
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Protocolo'], ['Controle', 'Sub'], [
           Conta], [Controle, Sub], True, '', VAR_ModBloq_TabLctA);
      end;
    end;

    procedure GeraProtocolo_Lancamentos(var SemQuery: Integer);
    var
      Protocolo, Controle, Sub, Conta, CliInt, Cliente, Fornece: Integer;
      Credito, Debito, Documento: Double;
      SerieCH: String;
      Vencto: TDateTime;
    begin
      Protocolo := -1;
      Controle  := 0;
      CliInt    := 0;
      Cliente   := 0;
      Fornece   := 0;
      Credito   := 0;
      Debito    := 0;
      Documento := 0;
      Vencto    := 0;
      Sub       := 0;
      if FQrSource <> nil then
      begin
        Protocolo := 0;
        Controle  := FQrSource.FieldByName('Controle'  ).AsInteger;
        Sub       := FQrSource.FieldByName('Sub'       ).AsInteger;
        CliInt    := FQrSource.FieldByName('CliInt'    ).AsInteger;
        Cliente   := FQrSource.FieldByName('Cliente'   ).AsInteger;
        Fornece   := FQrSource.FieldByName('Fornecedor').AsInteger;
        //
        Documento := FQrSource.FieldByName('Documento' ).AsFloat;
        Credito   := FQrSource.FieldByName('Credito'   ).AsFloat;
        Debito    := FQrSource.FieldByName('Debito'    ).AsFloat;
        //
        SerieCH   := FQrSource.FieldByName('SerieCH'   ).AsString;
        //
        Vencto    := FQrSource.FieldByName('Vencimento').AsDateTime;
        //
      end else begin
        Inc(SemQuery, 1);
      end;
      if Protocolo = 0 then
      begin
        Conta := UMyMod.BuscaEmLivreY_Def('protpakits', 'conta', stIns, 0);
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'protpakits', False,
        [
          'Codigo', 'Controle', 'Link_ID', 'CliInt',
          'Cliente', 'Fornece', 'Periodo', 'Lancto',
          'Docum', 'Depto',
          'ID_Cod1', 'ID_Cod2', 'ID_Cod3', 'ID_Cod4',
          'Retorna', 'Vencto',
          'Valor', 'MoraDiaVal', 'MultaVal', 'Cedente',
          'SerieCH', 'LimiteSai', 'LimiteRem', 'LimiteRet'
        ], ['Conta'], [
          QrProtocoPakCodigo.Value, QrProtocoPakControle.Value,
          VAR_TIPO_LINK_ID_01_GENERICO, CliInt,
          Cliente, Fornece, 0(*Periodo*), Controle,
          Documento, 0,
          0(*FID_Cod1*), 0(*FID_Cod2*), 0(*FID_Cod3*), 0(*FID_Cod4*),
          Geral.BoolToInt(CkRetorna.Checked), Geral.FDT(Vencto, 1),
          Credito-Debito, 0(*MoraDiaVal*), 0(*MultaVal*), 0(*Cedente*),
          SerieCH, FLimiteSai, FLimiteRem, FLimiteRet
        ], [Conta], True) then
        {$IFDEF DEFINE_VARLCT}
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
            'Protocolo'], ['Controle', 'Sub'], [
             Conta], [Controle, Sub], True, '', VAR_ModBloq_TabLctA);
        {$ELSE}
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
            'Protocolo'], ['Controle', 'Sub'], [
             Conta], [Controle, Sub], True, '');
        {$ENDIF}
      end;
    end;

    procedure GeraProtocolo_BloqProto(var SemEmeio, SemQuery: Integer);
    var
      Protocolo, Entidade, Conta, Apto, Emeio_ID, Periodo, PrevCod: Integer;
      Vencto: TDateTime;
      Bloqueto, Valor, MoraDiaVal, MultaVal, MoraPer, MultaPer: Double;
      Qry: TMySQLQuery;
    begin
      Qry      := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
      Entidade := 0;
      Bloqueto := 0;
      Apto     := 0;
      EMeio_ID := 0;
      Vencto   := 0;
      Valor    := 0;
      Periodo  := 0;
      PrevCod  := 0;
      try
        MoraDiaVal := Round(Valor * (1 / 30)) / 100;
        MultaVal   := Round(Valor * 2) / 100;
        //Cedente    := 0;
        if FNomeQrSrc = 'QrCD1' then
        begin
          Protocolo := FQrSource.FieldByName('PROTOCOLO').AsInteger;
          Entidade  := FQrSource.FieldByName('Entidade').AsInteger;
          Bloqueto  := FQrSource.FieldByName('Boleto').AsFloat;
          Vencto    := FQrSource.FieldByName('Vencto').AsDateTime;
          Valor     := FQrSource.FieldByName('Valor').AsFloat;
          Periodo   := FQrSource.FieldByName('Periodo').AsInteger;
          PrevCod   := FQrSource.FieldByName('PREVCOD').AsInteger;
        end else
        if FNomeQrSrc = 'QrCR1' then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
            'SELECT cfg.MultaPerc, cfg.JurosPerc',
            'FROM protocopak pak',
            'LEFT JOIN cnab_cfg cfg ON cfg.Codigo=pak.CNAB_Cfg',
            'WHERE pak.Controle=:P0',
            '']);
          MoraPer  := Qry.FieldByName('JurosPerc').AsFloat;
          MultaPer := Qry.FieldByName('MultaPerc').AsFloat;
          //
          Protocolo  := FQrSource.FieldByName('PROTOCOLO').AsInteger;
          Entidade   := FQrSource.FieldByName('Entidade').AsInteger;
          Bloqueto   := FQrSource.FieldByName('Boleto').AsFloat;
          Vencto     := FQrSource.FieldByName('Vencto').AsDateTime;
          Valor      := FQrSource.FieldByName('Valor').AsFloat;
          MoraDiaVal := Round(Valor * (MoraPer / 30)) / 100;
          MultaVal   := Round(Valor * MultaPer) / 100;
          Periodo    := FQrSource.FieldByName('Periodo').AsInteger;
          PrevCod    := FQrSource.FieldByName('PREVCOD').AsInteger;
        end else
        if FNomeQrSrc = 'QrProtoMail' then
        begin
          if FRecipEmeio <> '' then
          begin
            Protocolo := FQrSource.FieldByName('Protocolo').AsInteger;
            Entidade  := FQrSource.FieldByName('Entid_Cod').AsInteger;
            Bloqueto  := FQrSource.FieldByName('Bloqueto').AsFloat;
            EMeio_ID  := FQrSource.FieldByName('RecipItem').AsInteger;
            Vencto    := FQrSource.FieldByName('Vencimento').AsDateTime;
            Valor     := FQrSource.FieldByName('Valor').AsFloat;
            Periodo   := FQrSource.FieldByName('Periodo').AsInteger;
            PrevCod   := FQrSource.FieldByName('PrevCod').AsInteger;
          end else begin
            Inc(SemEmeio, 1);
            Protocolo := -1;
          end;
        end else begin
          Inc(SemQuery, 1);
          Protocolo := -1;
        end;
        //
        if Protocolo = 0 then
        begin
          Conta := UMyMod.BuscaEmLivreY_Def('protpakits', 'conta', stIns, 0);
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'protpakits', False,
            [
              'Codigo', 'Controle', 'Link_ID', 'CliInt',
              'Cliente', 'Fornece', 'Periodo',
              'Lancto', 'Docum',
              'Depto',
              'ID_Cod1', 'ID_Cod2', 'ID_Cod3', 'ID_Cod4',
              'Retorna', 'Vencto',
              'Valor', 'MoraDiaVal', 'MultaVal', 'Cedente',
              'LimiteSai', 'LimiteRem', 'LimiteRet'
            ], ['Conta'], [
              QrProtocoPakCodigo.Value, QrProtocoPakControle.Value,
              VAR_TIPO_LINK_ID_01_GENERICO, VAR_ModBloq_EntCliInt,
              Entidade, 0, Periodo, 0, Bloqueto, Apto,
              PrevCod, VAR_ModBloq_EntCliInt, EMeio_ID, 0,
              Geral.BoolToInt(CkRetorna.Checked), Geral.FDT(Vencto, 1),
              Valor, MoraDiaVal, MultaVal, 0,
              FLimiteSai, FLimiteRem, FLimiteRet
            ], [Conta], True) then
          begin
            if FNomeQrSrc = 'QrCR1' then
            begin
              UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
                'UPDATE arreits SET ProtocoloCR="' + Geral.FF0(QrProtocoPakCodigo.Value) + '", ',
                'ProtocoPakCR="' + Geral.FF0(QrProtocoPakControle.Value) + '", ',
                'WHERE Boleto="' + Geral.FFI(Bloqueto) + '" ',
                'AND Codigo="' + Geral.FF0(PrevCod) + '" ',
                '']);
            end else
            begin
              UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
                'UPDATE arreits SET Protocolo="' + Geral.FF0(QrProtocoPakCodigo.Value) + '", ',
                'ProtocoPak="' + Geral.FF0(QrProtocoPakControle.Value) + '", ',
                'WHERE Boleto="' + Geral.FFI(Bloqueto) + '" ',
                'AND Codigo="' + Geral.FF0(PrevCod) + '" ',
                '']);
            end;
          end;
        end;
      finally
        Qry.Free;
      end;
    end;

    procedure GeraProtocolo_OS1(var SemQuery: Integer);
    var
      Codigo, Controle, Link_ID, CliInt, Cliente, Fornece, Periodo, Lancto,
      Depto, ID_Cod1, ID_Cod2, ID_Cod3, ID_Cod4, Retorna, MoraDiaVal, MultaVal,
      Cedente, EntContrat, EntPagante: Integer;
      Valor, Docum: Double;
      LimiteSai, LimiteRem, LimiteRet, SerieCH, Vencto: String;
      Conta: Integer;
      //
      Vencimento: TDateTime;
      Protocolo: Integer;
      //
      Qry: TmySQLQuery;
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * FROM oscab ',
        'WHERE Codigo=' + Geral.FF0(FQrSource.FieldByName('ID_Cod1').AsInteger),
        '']);
        Protocolo  := -1;
        CliInt     := 0;
        Cliente    := 0;
        Fornece    := 0;
        EntContrat := 0;
        EntPagante := 0;
        Valor      := 0;
        Vencimento := 0;
        SerieCH    := '';
        ID_Cod1    := 0;
        ID_Cod2    := 0;
        if Qry <> nil then
        begin
          Protocolo  := 0;
          CliInt     := DModG.ObtemEntidadeDeFilial(Qry.FieldByName('Empresa').AsInteger);
          Cliente    := Qry.FieldByName('Entidade'  ).AsInteger;
          Fornece    := 0;
          EntContrat := Qry.FieldByName('EntContrat').AsInteger;
          EntPagante := Qry.FieldByName('EntContrat').AsInteger;
          Valor      := Qry.FieldByName('ValorTotal').AsFloat;
          Vencimento := Qry.FieldByName('DtaExePrv' ).AsDateTime;
          ID_Cod1    := FQrSource.FieldByName('ID_Cod1').AsInteger;
          ID_Cod2    := FQrSource.FieldByName('ID_Cod2').AsInteger;
        end else begin
          Inc(SemQuery, 1);
        end;
        if Protocolo = 0 then
        begin
          Conta := UMyMod.BuscaEmLivreY_Def('protpakits', 'conta', stIns, 0);
          //
          Codigo     := QrProtocoPakCodigo.Value;
          Controle   := QrProtocoPakControle.Value;
          Link_ID    := VAR_TIPO_LINK_ID_03_ORDEM_DE_SERVCO1;
          //CliInt     := CliInt;
          //Cliente    := Cliente;
          //Fornece    := Fornece;
          Periodo    := 0(*Periodo*);
          Lancto     := 0;
          Docum      := 0;
          Depto      := 0;
          //ID_Cod1    := ;
          //ID_Cod2    := ; // Vistoria ou execu��o?; ou escalonamento? ou Status da OS?
          ID_Cod3    := 0(*FID_Cod3*);
          ID_Cod4    := 0(*FID_Cod4*);
          Retorna    := Geral.BoolToInt(CkRetorna.Checked);
          Vencto     := Geral.FDT(Vencimento, 1);
          //Valor      := Valor;
          MoraDiaVal := 0(*MoraDiaVal*);
          MultaVal   := 0(*MultaVal*);
          Cedente    := 0(*Cedente*);
          SerieCH    := SerieCH;
          LimiteSai  := FLimiteSai;
          LimiteRem  := FLimiteRem;
          LimiteRet  := FLimiteRet;
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'protpakits', False,
          [
            'Codigo', 'Controle', 'Link_ID', 'CliInt',
            'Cliente', 'Fornece', 'Periodo', 'Lancto',
            'Docum', 'Depto',
            'ID_Cod1', 'ID_Cod2', 'ID_Cod3', 'ID_Cod4',
            'Retorna', 'Vencto',
            'Valor', 'MoraDiaVal', 'MultaVal', 'Cedente',
            'SerieCH', 'LimiteSai', 'LimiteRem', 'LimiteRet',
            'EntContrat', 'EntPagante'
          ], ['Conta'], [
            Codigo, Controle, Link_ID, CliInt,
            Cliente, Fornece, Periodo, Lancto,
            Docum, Depto,
            ID_Cod1, ID_Cod2, ID_Cod3, ID_Cod4,
            Retorna, Vencto,
            Valor, MoraDiaVal, MultaVal, Cedente,
            SerieCH, LimiteSai, LimiteRem, LimiteRet,
            EntContrat, EntPagante
          ], [Conta], True) then
          begin
            // Coloca numero do protocolo na OSCab!
            (*
            SQLInsUpd na OSCab(Dmod.QrUpd, stUpd, False, [
              'Protocolo'], ['Controle', 'Sub'], [
            *)
          end;
        end;
      finally
        Qry.Free;
      end;
    end;

  begin
    // 2012-05-21
    //if FNomeQrSrc = 'DmLct0.QrLcP' then
    if Copy(FNomeQrSrc, 8) = 'QrLcP' then
      GeraProtocolo_DocumLanctos(SemQuery)
    else
    // fim 2012-05-21
    if FNomeQrSrc = 'FmPrincipal.QrLct' then
      GeraProtocolo_Lancamentos(SemQuery)
    else
    //if FNomeQrSrc = 'DmLct0.QrLct' then
    if Copy(FNomeQrSrc, 8) = 'QrLct' then
      GeraProtocolo_Lancamentos(SemQuery)
    else
    // 2013-09-05
    //if FNomeQrSrc = 'DmLct0.QrLct' then
    if FNomeQrSrc = 'QrOSCab' then
      GeraProtocolo_OS1(SemQuery)
    else
    // FIM 2013-09-05
      GeraProtocolo_BloqProto(SemEmeio, SemQuery);
  end;
var
  //Codigo, Conta,
  i, se, sq, Continua: Integer;
begin
  FLimiteSai := Geral.FDT(TPLimiteSai.Date, 1);
  FLimiteRem := Geral.FDT(TPLimiteRem.Date, 1);
  FLimiteRet := Geral.FDT(TPLimiteRet.Date, 1);
  se := 0;
  sq := 0;

  if QrProtocoPakControle.Value = 0 then
  begin
    Geral.MensagemBox('N�o h� lote selecionado!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  try
    Screen.Cursor := crHourGlass;
    if FQuais = istTodos then
    begin
      if not FAskTodos then
        Continua := ID_YES
      else
      Continua := Geral.MensagemBox(
      'Gera protocolo para todos itens?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION);
      if Continua = ID_YES then
      begin
        //
        FQrSource.First;
        while not FQrSource.Eof do
        begin
          GeraAtual(se, sq);
          FQrSource.Next;
        end;
        //
      end;
    end else if FQuais = istSelecionados then
    begin
      if FDBGrid.SelectedRows.Count > 1 then
      begin
        with FDBGrid.DataSource.DataSet do
        for i := 0 to FDBGrid.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(FDBGrid.SelectedRows.Items[i]));
          GeraAtual(se, sq);
        end;
      end else GeraAtual(se, sq);
    end else if FQuais = istAtual then
      GeraAtual(se, sq);
  finally
    FLote         := QrProtocoPakControle.Value;
    Screen.Cursor := crDefault;
  end;
  if (FNomeQrSrc = 'QrProtoMail') or (FNomeQrSrc <> '') then
    FInseriu := True
  else
    FInseriu := False;
  if se > 0 then
    Geral.MB_Aviso(Geral.FF0(se) + ' protocolos n�o ' +
      'foram gerados porque o boleto n�o tem e-mail recipiente!');
  if sq > 0 then
    Geral.MB_Erro(Geral.FF0(sq) + ' protocolos n�o ' +
      'foram gerados porque o tipo de protocolo n�o foi impementado pela ' +
      ' DERMATEK!');
  Close;
end;

procedure TFmProSel.BtSaidaClick(Sender: TObject);
begin
  FDesistiu := True;
  Close;
end;

procedure TFmProSel.CkAbertosClick(Sender: TObject);
begin
  ReopenProtocoPak(QrProtocoPakControle.Value);
end;

procedure TFmProSel.CkRetornaClick(Sender: TObject);
begin
  LaLimiteRet.Visible := CkRetorna.Checked;
  TPLimiteRet.Visible := CkRetorna.Checked;
end;

procedure TFmProSel.EdTarefaChange(Sender: TObject);
begin
  ReopenProtocoPak(QrProtocoPakControle.Value);
end;

procedure TFmProSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmProSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FInseriu         := False;
  FDesistiu        := False;
  TPLimiteSai.Date := Date + 1;
  TPLimiteRem.Date := Date + 1;
  TPLimiteRet.Date := Date + 4;
end;

procedure TFmProSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProSel.ReopenProtocolos(TipoProtocolo, Tarefa, Def_Client: Integer);
var
  SQLParam1, SQLParam2, SQLParam3: String;
begin
  if TipoProtocolo <> 0 then
    SQLParam1 := 'AND ptc.Tipo =' + Geral.FF0(TipoProtocolo)
  else
    SQLParam1 := '';
  if Tarefa <> 0 then
    SQLParam2 := 'AND ptc.Codigo =' + Geral.FF0(Tarefa)
  else
    SQLParam2 := '';
  if Def_Client <> 0 then
    SQLParam3 := 'AND ptc.Def_Client =' + Geral.FF0(Def_Client)
  else
    SQLParam3 := '';  
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrProtocolos, Dmod.MyDB, [
  'SELECT ptc.Codigo, ptc.Nome, ptc.Def_Retorn, ',
  'ptc.Tipo, ptc.Def_Client ',
  'FROM protocolos ptc ',
  'WHERE ptc.Codigo > 0 ',
  SQLParam1,
  SQLParam2,
  SQLParam3,
  'ORDER BY Nome ',
  '']);
end;

procedure TFmProSel.ReopenProtocoPak(Controle: Integer);
var
  Proto: Integer;
begin
  CkRetorna.Checked := Geral.IntToBool(QrProtocolosDef_Retorn.Value);
  QrProtocoPak.Close;
  Proto := Geral.IMV(EdTarefa.Text);
  if Proto > 0 then
  begin
    QrProtocoPak.SQL.Clear;
    QrProtocoPak.SQL.Add('SELECT ptp.*,');
    QrProtocoPak.SQL.Add('CONCAT(RIGHT(Mez, 2), "/",  LEFT(Mez + 200000, 4)) MES,');
    QrProtocoPak.SQL.Add('IF(ptp.DataF=0,"", DATE_FORMAT(ptp.DataF, "%d/%m/%y" )) DATAF_TXT');
    QrProtocoPak.SQL.Add('FROM protocopak ptp');
    QrProtocoPak.SQL.Add('');
    QrProtocoPak.SQL.Add('WHERE ptp.Codigo=' + FormatFloat('0', Proto));
    //
    if CkAbertos.Checked then
      QrProtocoPak.SQL.Add('AND ptp.DataF=0');
    QrProtocoPak.SQL.Add('ORDER BY Controle DESC');
    QrProtocoPak.Open;
    //
    if Controle > 0 then QrProtocoPak.Locate('Controle', Controle, []);
  end;
end;

procedure TFmProSel.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmProtocolos, FmProtocolos, afmoNegarComAviso) then
  begin
    FmProtocolos.ShowModal;
    FmProtocolos.Destroy;
    //
    QrProtocolos.Close;
    QrProtocolos.Open;
    //
    EdTarefa.ValueVariant := VAR_CADASTRO;
    CBTarefa.KeyValue     := VAR_CADASTRO;
  end;
end;

end.
