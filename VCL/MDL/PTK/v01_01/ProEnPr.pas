unit ProEnPr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkEditCB,
  dmkDBLookupComboBox, dmkCheckBox, dmkValUsu, Variants, Menus, Grids, DBGrids,
  dmkDBGrid, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmProEnPr = class(TForm)
    PainelDados: TPanel;
    DsProEnPr: TDataSource;
    QrProEnPr: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    PainelData: TPanel;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    CBProtocolo: TdmkDBLookupComboBox;
    EdProtocolo: TdmkEditCB;
    CkAtivo: TdmkCheckBox;
    CkContinuar: TCheckBox;
    QrProtocolos: TmySQLQuery;
    DsProtocolos: TDataSource;
    QrLoc: TmySQLQuery;
    PMProtocolo: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    PMEntidade: TPopupMenu;
    Inclui2: TMenuItem;
    Exclui2: TMenuItem;
    Panel4: TPanel;
    QrProEnPrIt: TmySQLQuery;
    DsProEnPrIt: TDataSource;
    QrProtocolosCodigo: TAutoIncField;
    dmkPermissoes1: TdmkPermissoes;
    QrProEnPrCodigo: TIntegerField;
    QrProEnPrProtocolo: TIntegerField;
    QrProEnPrLk: TIntegerField;
    QrProEnPrDataCad: TDateField;
    QrProEnPrDataAlt: TDateField;
    QrProEnPrUserCad: TIntegerField;
    QrProEnPrUserAlt: TIntegerField;
    QrProEnPrAlterWeb: TSmallintField;
    QrProEnPrAtivo: TSmallintField;
    QrProEnPrNOMEPROT: TWideStringField;
    dmkDBGrid1: TdmkDBGrid;
    QrProEnPrItCodigo: TIntegerField;
    QrProEnPrItControle: TIntegerField;
    QrProEnPrItEntidade: TIntegerField;
    QrProEnPrItLk: TIntegerField;
    QrProEnPrItDataCad: TDateField;
    QrProEnPrItDataAlt: TDateField;
    QrProEnPrItUserCad: TIntegerField;
    QrProEnPrItUserAlt: TIntegerField;
    QrProEnPrItAlterWeb: TSmallintField;
    QrProEnPrItAtivo: TSmallintField;
    QrProEnPrItNOMEENT: TWideStringField;
    QrProtocolosNome: TWideStringField;
    PnEnti: TPanel;
    Label1: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    CkContinuarEnt: TCheckBox;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    VUEntidade: TdmkValUsu;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesCodUsu: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    QrProtocolosTipo: TIntegerField;
    SpeedButton5: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtProtocolo: TBitBtn;
    BtEntidade: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox1: TGroupBox;
    Panel7: TPanel;
    Panel8: TPanel;
    BitBtn5: TBitBtn;
    BitBtn4: TBitBtn;
    RGFinalidade: TRadioGroup;
    QrProEnPrFinalidade: TIntegerField;
    DBRGFinalidade: TDBRadioGroup;
    QrProEnPrDef_Client: TIntegerField;
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrProEnPrAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrProEnPrBeforeOpen(DataSet: TDataSet);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure BtProtocoloClick(Sender: TObject);
    procedure PMProtocoloPopup(Sender: TObject);
    procedure BtEntidadeClick(Sender: TObject);
    procedure Inclui2Click(Sender: TObject);
    procedure PMEntidadePopup(Sender: TObject);
    procedure QrProEnPrAfterScroll(DataSet: TDataSet);
    procedure QrProEnPrBeforeClose(DataSet: TDataSet);
    procedure Exclui2Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenProEnPrIt(Codigo, Entidade: Integer);
    function VerificaSeProtocoloExiste(Protocolo: Integer): Integer;
    function VerificaSeEntidadeExiste(Entidade, Protocolo: Integer): Integer;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmProEnPr: TFmProEnPr;
const
  FFormatFloat = '00000';

implementation

uses Module, MyDBCheck, UnMyObjects, UnProtocoUnit, Protocolo;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmProEnPr.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmProEnPr.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrProEnPrCodigo.Value, LaRegistro.Caption[2]);
end;

function TFmProEnPr.VerificaSeEntidadeExiste(Entidade,
  Protocolo: Integer): Integer;
begin
  QrLoc.Close;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT its.*');
  QrLoc.SQL.Add('FROM proenprit its');
  QrLoc.SQL.Add('LEFT JOIN proenpr pro ON pro.Codigo = its.Codigo');
  QrLoc.SQL.Add('WHERE pro.Protocolo=:P0');
  QrLoc.SQL.Add('AND its.Entidade=:P1');
  QrLoc.Params[0].AsInteger := Protocolo;
  QrLoc.Params[1].AsInteger := Entidade;
  QrLoc.Open;
  if QrLoc.RecordCount > 0 then
    Result := QrLoc.FieldByName('Controle').AsInteger
  else
    Result := 0;
end;

function TFmProEnPr.VerificaSeProtocoloExiste(Protocolo: Integer): Integer;
begin
  QrLoc.Close;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT * FROM proenpr WHERE Protocolo=:P0');
  QrLoc.Params[0].AsInteger := Protocolo;
  QrLoc.Open;
  if QrLoc.RecordCount > 0 then
    Result := QrLoc.FieldByName('Codigo').AsInteger
  else
    Result := 0;
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmProEnPr.DefParams;
begin
  VAR_GOTOTABELA := 'proenpr';
  VAR_GOTOMYSQLTABLE := QrProEnPr;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT blo.*, pro.Nome NOMEPROT, pro.Def_Client ');
  VAR_SQLx.Add('FROM proenpr blo');
  VAR_SQLx.Add('LEFT JOIN protocolos pro ON pro.Codigo = blo.Protocolo');
  VAR_SQLx.Add('WHERE blo.Codigo > 0');
  //
  VAR_SQL1.Add('AND blo.Codigo=:P0');
  //
  //VAR_SQLa.Add('AND blo.Nome Like :P0');
  //
end;

procedure TFmProEnPr.Exclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrProEnPrCodigo.Value;
  //
  if QrProEnPrIt.State <> dsInactive then
  begin
    if QrProEnPrIt.RecordCount > 0 then
    begin
      Geral.MensagemBox('Exclus�o abortada!' + #13#10 +
      'Este protocolo possui entidades cadastradas nele!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  if Geral.MensagemBox('Confirma a exclus�o do protocolo ' +
    QrProEnPrNOMEPROT.Value + '?', 'Exclus�o de registro',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM proenpr WHERE Codigo=' + FormatFloat('0', Codigo));
    Dmod.QrUpd.ExecSQL;
    //
    Va(vpLast);
  end;
end;

procedure TFmProEnPr.Exclui2Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrProEnPrIt, TDBGrid(dmkDBGrid1),
    'proenprit', ['Controle'], ['Controle'], istPergunta, '');
  //
  ReopenProEnPrIt(QrProEnPrCodigo.Value, 0);
end;

procedure TFmProEnPr.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  RGFinalidade.Items.Clear;
  RGFinalidade.Items.AddStrings(UnProtocolo.ObtemListaProEnPrFinalidade());
  //
  DBRGFinalidade.Items.Clear;
  DBRGFinalidade.Items.AddStrings(UnProtocolo.ObtemListaProEnPrFinalidade());
  //
  case Mostra of
    0:
    begin
      GBCntrl.Visible        := True;
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
      PnEnti.Visible         := False;
    end;
    1:
    begin
      PainelEdita.Visible    := True;
      PainelDados.Visible    := False;
      GBCntrl.Visible        := False;
      PnEnti.Visible         := False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant    := FormatFloat(FFormatFloat, Codigo);
        EdProtocolo.ValueVariant := 0;
        CBProtocolo.KeyValue     := Null;
        RGFinalidade.ItemIndex   := -1;
        CkAtivo.Checked          := True;
        CkContinuar.Checked      := False;
        CkContinuar.Visible      := True;
      end else
      begin
        EdCodigo.ValueVariant    := QrProEnPrCodigo.Value;
        EdProtocolo.ValueVariant := QrProEnPrProtocolo.Value;
        CBProtocolo.KeyValue     := QrProEnPrProtocolo.Value;
        RGFinalidade.ItemIndex   := QrProEnPrFinalidade.Value;
        CkAtivo.Checked          := MLAGeral.ITB(QrProEnPrAtivo.Value);
        CkContinuar.Checked      := False;
        CkContinuar.Visible      := False;
      end;
      EdProtocolo.SetFocus;
    end;
    2:
    begin
      GBCntrl.Visible        := False;
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
      PnEnti.Visible         := True;
      if SQLType = stIns then
      begin
        EdEntidade.ValueVariant := 0;
        CBEntidade.KeyValue     := Null;
        CkContinuarEnt.Checked  := True;
        CkContinuarEnt.Visible  := True;
      end else
      begin
        EdEntidade.ValueVariant := QrProEnPrItEntidade.Value;
        CBEntidade.KeyValue     := QrProEnPrItEntidade.Value;
        CkContinuarEnt.Checked  := False;
        CkContinuarEnt.Visible  := False;
      end;
      EdEntidade.SetFocus;
    end
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmProEnPr.PMProtocoloPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrProEnPr.State <> dsInactive) and (QrProEnPr.RecordCount > 0);
  //
  Altera1.Enabled := Enab;
  Exclui1.Enabled := Enab;
end;

procedure TFmProEnPr.PMEntidadePopup(Sender: TObject);
var
  Enab1, Enab2: Boolean;
begin
  Enab1 := (QrProEnPr.State <> dsInactive) and (QrProEnPr.RecordCount > 0) and (QrProEnPrFinalidade.Value = 0);
  Enab2 := (QrProEnPrIt.State <> dsInactive) and (QrProEnPrIt.RecordCount > 0);
  //
  Inclui2.Enabled := Enab1;
  Exclui2.Enabled := Enab1 and Enab2;
end;

procedure TFmProEnPr.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmProEnPr.QueryPrincipalAfterOpen;
begin
end;

procedure TFmProEnPr.ReopenProEnPrIt(Codigo, Entidade: Integer);
begin
  QrProEnPrIt.Close;
  QrProEnPrIt.Params[0].AsInteger := Codigo;
  QrProEnPrIt.Open;
  if Entidade > 0 then
    QrProEnPrIt.Locate('Entidade', Entidade, []);
end;

procedure TFmProEnPr.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmProEnPr.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmProEnPr.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmProEnPr.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmProEnPr.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmProEnPr.SpeedButton5Click(Sender: TObject);
var
  Protocolo: Integer;
begin
  VAR_CADASTRO := 0;
  Protocolo    := EdProtocolo.ValueVariant;
  //
  ProtocoUnit.MostraFormProtocolos(0, Protocolo);
  //
  if VAR_CADASTRO > 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdProtocolo, CBProtocolo, QrProtocolos, VAR_CADASTRO);
    CBProtocolo.SetFocus;
  end;
end;

procedure TFmProEnPr.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrProEnPrCodigo.Value;
  Close;
end;

procedure TFmProEnPr.BtEntidadeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEntidade, BtEntidade);
end;

procedure TFmProEnPr.Altera1Click(Sender: TObject);
begin
  MostraEdicao(1, stUpd, QrProEnPrCodigo.Value);
end;

procedure TFmProEnPr.BitBtn4Click(Sender: TObject);
var
  Codigo, Controle, ControleCad, Entidade: Integer;
begin
  Codigo   := QrProEnPrCodigo.Value;
  Entidade := EdEntidade.ValueVariant;
  Controle := UMyMod.BuscaEmLivreY_Def('ProEnPrit', 'Controle', ImgTipo.SQLType,
              QrProEnPrItControle.Value);
  //
  if MyObjects.FIC(EdEntidade.ValueVariant = 0, EdEntidade, 'Defina a entidade!') then Exit;
  //
  ControleCad := VerificaSeEntidadeExiste(Entidade, QrProEnPrProtocolo.Value);
  //
  if (ControleCad <> 0) and (ControleCad <> Controle) then
  begin
    Geral.MB_Aviso('Esta entidade j� foi cadastrada!');
    Exit;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ProEnPrit', False,
    ['Entidade', 'Codigo'], ['Controle'],
    [VUEntidade.ValueVariant, Codigo], [Controle], True) then
  begin
    if QrProEnPrFinalidade.Value = 0 then //Boleto
      UnProtocolo.RemoveProEnPrItsDuplicados(Dmod.MyDB, '0,1', ptkBoleto,
        QrProEnPrDef_Client.Value, VUEntidade.ValueVariant, Codigo);
    //
    ReopenProEnPrIt(Codigo, Entidade);
    if CkContinuarEnt.Checked then
    begin
      Geral.MensagemBox('Dados salvos com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
      MostraEdicao(2, stIns, 0)
    end else
      MostraEdicao(0, stLok, 0);
  end;
end;

procedure TFmProEnPr.BitBtn5Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmProEnPr.BtConfirmaClick(Sender: TObject);
var
  Codigo, CodigoCad, Protocolo, TipoProt, Finalidade, Ativo: Integer;
begin
  Protocolo  := EdProtocolo.ValueVariant;
  Finalidade := RGFinalidade.ItemIndex;
  Ativo      := MLAGeral.BTI(CkAtivo.Checked);
  Codigo     := UMyMod.BuscaEmLivreY_Def('ProEnPr', 'Codigo', ImgTipo.SQLType,
                 QrProEnPrCodigo.Value);
  //
  if MyObjects.FIC(Protocolo = 0, EdProtocolo, 'Defina o protocolo!') then Exit;
  if MyObjects.FIC(Finalidade < 0, RGFinalidade, 'Defina a finalidade!') then Exit;
  //
  CodigoCad := VerificaSeProtocoloExiste(Protocolo);
  //
  if (CodigoCad <> 0) and (CodigoCad <> Codigo) then
  begin
    Geral.MB_Aviso('Este protocolo j� foi cadastrado no ID n�mero ' +
      Geral.FF0(CodigoCad) + '!');
    Exit;
  end;
  //
  if Finalidade = 1 then
  begin
    TipoProt := QrProtocolosTipo.Value;
    //
    if MyObjects.FIC(TipoProt <> 2, EdProtocolo,
      'Defina um protocolo do tipo: CE - Circula��o eletr�nica (E-mail)!') then Exit;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ProEnPr', False,
    ['Protocolo', 'Finalidade', 'Ativo'], ['Codigo'],
    [Protocolo, Finalidade, Ativo], [Codigo], True) then
  begin
    LocCod(Codigo, Codigo);
    if CkContinuar.Checked then
    begin
      Geral.MB_Aviso('Dados salvos com sucesso!');
      MostraEdicao(1, stIns, 0)
    end else
      MostraEdicao(0, stLok, 0);
  end;
end;

procedure TFmProEnPr.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmProEnPr.BtProtocoloClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProtocolo, BtProtocolo);
end;

procedure TFmProEnPr.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType  := stLok;
  PainelEdit.Align := alClient;
  Panel4.Align     := alClient;
  CriaOForm;
  //
  MostraEdicao(0, stLok, 0);
  //
  UMyMod.AbreQuery(QrProtocolos, Dmod.MyDB);
  UMyMod.AbreQuery(QrEntidades, Dmod.MyDB);
end;

procedure TFmProEnPr.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrProEnPrCodigo.Value, LaRegistro.Caption);
end;

procedure TFmProEnPr.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmProEnPr.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmProEnPr.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmProEnPr.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmProEnPr.QrProEnPrAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmProEnPr.QrProEnPrAfterScroll(DataSet: TDataSet);
begin
  ReopenProEnPrIt(QrProEnPrCodigo.Value, 0);
end;

procedure TFmProEnPr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmProEnPr.SbQueryClick(Sender: TObject);
begin
  LocCod(QrProEnPrCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ProEnPr', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmProEnPr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProEnPr.Inclui1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmProEnPr.Inclui2Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmProEnPr.QrProEnPrBeforeClose(DataSet: TDataSet);
begin
  QrProEnPrIt.Close;
end;

procedure TFmProEnPr.QrProEnPrBeforeOpen(DataSet: TDataSet);
begin
  QrProEnPrCodigo.DisplayFormat := FFormatFloat;
end;

end.

