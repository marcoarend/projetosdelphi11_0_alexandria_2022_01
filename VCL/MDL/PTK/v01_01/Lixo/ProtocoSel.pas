u n i t   P r o t o c o S e l ;  
  
 i n t e r f a c e  
  
 u s e s  
     W i n d o w s ,   M e s s a g e s ,   S y s U t i l s ,   C l a s s e s ,   G r a p h i c s ,   C o n t r o l s ,   F o r m s ,   D i a l o g s ,  
     E x t C t r l s ,   S t d C t r l s ,   U n I n t e r n a l C o n s t s ,   B u t t o n s ,   D B C t r l s ,   D b ,   D B T a b l e s ,  
     U n   U n G O T O y ,   M a s k ,   U M y S Q L M o d u l e ,   m y S Q L D b T a b l e s ,   G r i d s ,   D B G r i d s ,  
     d m k G e r a l ,   C o m C t r l s ,   d m k E d i t D a t e T i m e P i c k e r ,   d m k E d i t ,   d m k E d i t C B ,  
     d m k D B L o o k u p C o m b o B o x ,   d m k I m a g e ,   U n D m k E n u m s ;  
  
 t y p e  
     T F m P r o t o c o S e l   =   c l a s s ( T F o r m )  
         P a i n e l D a d o s :   T P a n e l ;  
         L a b e l 1 :   T L a b e l ;  
         C B T a r e f a :   T d m k D B L o o k u p C o m b o B o x ;  
         D s P r o t o c o l o s :   T D a t a S o u r c e ;  
         E d T a r e f a :   T d m k E d i t C B ;  
         Q r P r o t o c o l o s :   T m y S Q L Q u e r y ;  
         P a n e l 4 :   T P a n e l ;  
         P a n e l 6 :   T P a n e l ;  
         C k A b e r t o s :   T C h e c k B o x ;  
         D B G r i d 1 :   T D B G r i d ;  
         Q r P r o t o c o P a k :   T m y S Q L Q u e r y ;  
         D s P r o t o c o P a k :   T D a t a S o u r c e ;  
         Q r P r o t o c o l o s C o d i g o :   T I n t e g e r F i e l d ;  
         Q r P r o t o c o l o s N o m e :   T S t r i n g F i e l d ;  
         Q r P r o t o c o P a k C o d i g o :   T I n t e g e r F i e l d ;  
         Q r P r o t o c o P a k C o n t r o l e :   T I n t e g e r F i e l d ;  
         Q r P r o t o c o P a k D a t a I :   T D a t e F i e l d ;  
         Q r P r o t o c o P a k D a t a L :   T D a t e F i e l d ;  
         Q r P r o t o c o P a k D a t a F :   T D a t e F i e l d ;  
         Q r P r o t o c o P a k L k :   T I n t e g e r F i e l d ;  
         Q r P r o t o c o P a k D a t a C a d :   T D a t e F i e l d ;  
         Q r P r o t o c o P a k D a t a A l t :   T D a t e F i e l d ;  
         Q r P r o t o c o P a k U s e r C a d :   T I n t e g e r F i e l d ;  
         Q r P r o t o c o P a k U s e r A l t :   T I n t e g e r F i e l d ;  
         Q r P r o t o c o P a k A l t e r W e b :   T S m a l l i n t F i e l d ;  
         Q r P r o t o c o P a k A t i v o :   T S m a l l i n t F i e l d ;  
         Q r P r o t o c o P a k M e z :   T I n t e g e r F i e l d ;  
         Q r P r o t o c o P a k M E S :   T S t r i n g F i e l d ;  
         Q r P r o t o c o P a k D A T A F _ T X T :   T S t r i n g F i e l d ;  
         C k R e t o r n a :   T C h e c k B o x ;  
         Q r P r o t o c o l o s D e f _ R e t o r n :   T I n t e g e r F i e l d ;  
         G r o u p B o x 1 :   T G r o u p B o x ;  
         T P L i m i t e S a i :   T d m k E d i t D a t e T i m e P i c k e r ;  
         L a L i m i t e S a i :   T L a b e l ;  
         T P L i m i t e R e m :   T d m k E d i t D a t e T i m e P i c k e r ;  
         L a L i m i t e R e m :   T L a b e l ;  
         T P L i m i t e R e t :   T d m k E d i t D a t e T i m e P i c k e r ;  
         L a L i m i t e R e t :   T L a b e l ;  
         G B A v i s o s 1 :   T G r o u p B o x ;  
         P a n e l 1 :   T P a n e l ;  
         L a A v i s o 1 :   T L a b e l ;  
         L a A v i s o 2 :   T L a b e l ;  
         G B R o d a P e :   T G r o u p B o x ;  
         P n S a i D e s i s :   T P a n e l ;  
         B t D e s i s t e :   T B i t B t n ;  
         P a n e l 2 :   T P a n e l ;  
         B t C o n f i r m a :   T B i t B t n ;  
         B i t B t n 1 :   T B i t B t n ;  
         P n C a b e c a :   T P a n e l ;  
         G B _ R :   T G r o u p B o x ;  
         I m g T i p o :   T d m k I m a g e ;  
         G B _ L :   T G r o u p B o x ;  
         G B _ M :   T G r o u p B o x ;  
         L a T i t u l o 1 A :   T L a b e l ;  
         L a T i t u l o 1 B :   T L a b e l ;  
         L a T i t u l o 1 C :   T L a b e l ;  
         p r o c e d u r e   B t D e s i s t e C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   B t C o n f i r m a C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   E d T a r e f a C h a n g e ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   F o r m C r e a t e ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   F o r m R e s i z e ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   F o r m A c t i v a t e ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   C k A b e r t o s C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   B i t B t n 1 C l i c k ( S e n d e r :   T O b j e c t ) ;  
         p r o c e d u r e   C k R e t o r n a C l i c k ( S e n d e r :   T O b j e c t ) ;  
     p r i v a t e  
         {   P r i v a t e   d e c l a r a t i o n s   }  
         F L i m i t e S a i ,   F L i m i t e R e m ,   F L i m i t e R e t :   S t r i n g ;  
         p r o c e d u r e   R e o p e n P r o t o c o P a k ( C o n t r o l e :   I n t e g e r ) ;  
         / / p r o c e d u r e   G e r a P r o t o c l o _ C o n d G e r P r o t o ( ) ;  
         / / p r o c e d u r e   G e r a P r o t o c l o _ L a n c a m e n t o s ( ) ;  
     p u b l i c  
         F Q u a i s                 :   T S e l T y p e ;  
         F N o m e Q r S r c         :   S t r i n g ;  
         F Q r L c I                 :   T m y S Q L Q u e r y ;  
         F Q r S o u r c e           :   T m y S Q L Q u e r y ;  
         F D B G r i d               :   T D B G r i d ;  
         F T i p o P r o t o c o l o :   I n t e g e r ;  
         F T a b L c t A             :   S t r i n g ;  
         F L c t P r o t o           :   S t r i n g ;  
         F A g r u p a r D o c       :   B o o l e a n ;  
         F A s k T o d o s           :   B o o l e a n ;  
         {   P u b l i c   d e c l a r a t i o n s   }  
     e n d ;  
  
 v a r  
     F m P r o t o c o S e l :   T F m P r o t o c o S e l ;  
  
 i m p l e m e n t a t i o n  
  
 u s e s   U n F i n a n c e i r o ,   M o d u l e ,   E n t i d a d e s ,   C o n d G e r P r o t o ,   P r o t o c o P a k ,   M y D B C h e c k ,  
 U n M y O b j e c t s ,   D m k D A C _ P F ;  
  
 { $ R   * . D F M }  
  
 p r o c e d u r e   T F m P r o t o c o S e l . B t D e s i s t e C l i c k ( S e n d e r :   T O b j e c t ) ;  
 b e g i n  
     C l o s e ;  
 e n d ;  
  
 p r o c e d u r e   T F m P r o t o c o S e l . B t C o n f i r m a C l i c k ( S e n d e r :   T O b j e c t ) ;  
     p r o c e d u r e   G e r a A t u a l ( v a r   S e m E m e i o ,   S e m Q u e r y :   I n t e g e r ) ;  
         p r o c e d u r e   G e r a P r o t o c o l o _ D o c u m L a n c t o s ( v a r   S e m Q u e r y :   I n t e g e r ) ;  
         v a r  
             P r o t o c o l o ,   C o n t r o l e ,   S u b ,   C o n t a ,   C l i I n t ,   C l i e n t e ,   F o r n e c e ,   I T E N S ,   L a n c t o ,  
             T i p o ,   C a r t e i r a :   I n t e g e r ;  
             C r e d i t o ,   D e b i t o ,   D o c u m e n t o :   D o u b l e ;  
             S e r i e C H :   S t r i n g ;  
             D a t a ,   V e n c t o :   T D a t e T i m e ;  
         b e g i n  
             P r o t o c o l o   : =   - 1 ;  
             C o n t r o l e     : =   0 ;  
             C l i I n t         : =   0 ;  
             C l i e n t e       : =   0 ;  
             F o r n e c e       : =   0 ;  
             C r e d i t o       : =   0 ;  
             D e b i t o         : =   0 ;  
             D o c u m e n t o   : =   0 ;  
             V e n c t o         : =   0 ;  
             S u b               : =   0 ;  
             i f   F Q r S o u r c e   < >   n i l   t h e n  
             b e g i n  
                 P r o t o c o l o   : =   0 ;  
                 T i p o             : =   F Q r S o u r c e . F i e l d B y N a m e ( ' T i p o '             ) . A s I n t e g e r ;  
                 C a r t e i r a     : =   F Q r S o u r c e . F i e l d B y N a m e ( ' C a r t e i r a '     ) . A s I n t e g e r ;  
                 C o n t r o l e     : =   F Q r S o u r c e . F i e l d B y N a m e ( ' C o n t r o l e '     ) . A s I n t e g e r ;  
                 S u b               : =   F Q r S o u r c e . F i e l d B y N a m e ( ' S u b '               ) . A s I n t e g e r ;  
                 C l i I n t         : =   F Q r S o u r c e . F i e l d B y N a m e ( ' C l i I n t '         ) . A s I n t e g e r ;  
                 C l i e n t e       : =   F Q r S o u r c e . F i e l d B y N a m e ( ' C l i e n t e '       ) . A s I n t e g e r ;  
                 F o r n e c e       : =   F Q r S o u r c e . F i e l d B y N a m e ( ' F o r n e c e d o r ' ) . A s I n t e g e r ;  
                 / /  
                 D o c u m e n t o   : =   F Q r S o u r c e . F i e l d B y N a m e ( ' D o c u m e n t o '   ) . A s F l o a t ;  
                 C r e d i t o       : =   F Q r S o u r c e . F i e l d B y N a m e ( ' C r e d i t o '       ) . A s F l o a t ;  
                 D e b i t o         : =   F Q r S o u r c e . F i e l d B y N a m e ( ' D e b i t o '         ) . A s F l o a t ;  
                 / /  
                 S e r i e C H       : =   F Q r S o u r c e . F i e l d B y N a m e ( ' S e r i e C H '       ) . A s S t r i n g ;  
                 / /  
                 D a t a             : =   F Q r S o u r c e . F i e l d B y N a m e ( ' D a t a '             ) . A s D a t e T i m e ;  
                 V e n c t o         : =   F Q r S o u r c e . F i e l d B y N a m e ( ' V e n c i m e n t o ' ) . A s D a t e T i m e ;  
                 / /  
                 I T E N S           : =   F Q r S o u r c e . F i e l d B y N a m e ( ' I T E N S ' ) . A s I n t e g e r ;  
             e n d   e l s e   b e g i n  
                 I n c ( S e m Q u e r y ,   1 ) ;  
             e n d ;  
             i f   P r o t o c o l o   =   0   t h e n  
             b e g i n  
                 C o n t a   : =   U M y M o d . B u s c a E m L i v r e Y _ D e f ( ' p r o t p a k i t s ' ,   ' c o n t a ' ,   s t I n s ,   0 ) ;  
                 / /  
                 / /   I n c l u i r   a n t e s   o   i t e n s   ( q u a n d o   m a i s   d e   u m !  
                 i f   I T E N S   >   1   t h e n  
                 b e g i n  
                     U n D m k D A C _ P F . A b r e M y S Q L Q u e r y 0 ( F Q r L c I ,   F Q r L c I . D a t a b a s e ,   [  
                     ' S E L E C T   C o n t r o l e   L a n c t o   ' ,  
                     ' F R O M   '   +   F L c t P r o t o ,  
                     ' W H E R E   D a t a = " '   +   G e r a l . F D T ( D a t a ,   1 )   +   ' " ' ,  
                     ' A N D   T i p o = '   +   G e r a l . F F 0 ( T i p o ) ,  
                     ' A N D   C a r t e i r a = '   +   G e r a l . F F 0 ( C a r t e i r a ) ,  
                     ' A N D   C l i I n t = '   +   G e r a l . F F 0 ( C l i I n t ) ,  
                     ' A N D   S e r i e C H = " '   +   S e r i e C H   +   ' " ' ,  
                     ' A N D   D o c u m e n t o = '   +   G e r a l . F F I ( D o c u m e n t o ) ,  
                     '   ' ] ) ;  
                     F Q r L c I . F i r s t ;  
                     w h i l e   n o t   F Q r L c I . E o f   d o  
                     b e g i n  
                         L a n c t o   : =   F Q r L c I . F i e l d B y N a m e ( ' L a n c t o ' ) . A s I n t e g e r ;  
                         / /  
                         i f   U M y M o d . S Q L I n s U p d ( D m o d . Q r U p d ,   s t I n s ,   ' p r o t p a k m u l ' ,   F a l s e ,   [  
                         ' C o d i g o ' ,   ' C o n t r o l e ' ] ,   [  
                         ' C o n t a ' ,   ' L a n c t o ' ] ,   [  
                         Q r P r o t o c o P a k C o d i g o . V a l u e ,  
                         Q r P r o t o c o P a k C o n t r o l e . V a l u e ] ,  
                         [ C o n t a ,   L a n c t o ] ,   T r u e )   t h e n   ;  
                         / /   J �   a n t e c i p a r   a q u i   s e   t i v e r   m a i s   d e   u m   l a n c t o   c o m   o   m e s m o   p r o t o c o l o !  
                         U F i n a n c e i r o . S Q L I n s U p d _ L c t ( D m o d . Q r U p d ,   s t U p d ,   F a l s e ,   [  
                             ' P r o t o c o l o ' ] ,   [ ' C o n t r o l e ' ,   ' S u b ' ] ,   [  
                               C o n t a ] ,   [ L a n c t o ,   S u b ] ,   T r u e ,   ' ' ,   F T a b L c t A ) ;  
                         / /  
                         F Q r L c I . N e x t ;  
                     e n d ;  
                 e n d ;  
  
                 i f   U M y M o d . S Q L I n s U p d ( D m o d . Q r U p d ,   s t I n s ,   ' p r o t p a k i t s ' ,   F a l s e ,  
                 [  
                     ' C o d i g o ' ,   ' C o n t r o l e ' ,   ' L i n k _ I D ' ,   ' C l i I n t ' ,  
                     ' C l i e n t e ' ,   ' F o r n e c e ' ,   ' P e r i o d o ' ,   ' L a n c t o ' ,  
                     ' D o c u m ' ,   ' D e p t o ' ,  
                     ' I D _ C o d 1 ' ,   ' I D _ C o d 2 ' ,   ' I D _ C o d 3 ' ,   ' I D _ C o d 4 ' ,  
                     ' R e t o r n a ' ,   ' V e n c t o ' ,  
                     ' V a l o r ' ,   ' M o r a D i a V a l ' ,   ' M u l t a V a l ' ,   ' C e d e n t e ' ,  
                     ' S e r i e C H ' ,   ' L i m i t e S a i ' ,   ' L i m i t e R e m ' ,   ' L i m i t e R e t '  
                 ] ,   [ ' C o n t a ' ] ,   [  
                     Q r P r o t o c o P a k C o d i g o . V a l u e ,   Q r P r o t o c o P a k C o n t r o l e . V a l u e ,  
                     V A R _ T I P O _ L I N K _ I D _ 0 1 _ G E N E R I C O ,   C l i I n t ,  
                     C l i e n t e ,   F o r n e c e ,   0 ( * P e r i o d o * ) ,   C o n t r o l e ,  
                     D o c u m e n t o ,   0 ,  
                     0 ( * F I D _ C o d 1 * ) ,   0 ( * F I D _ C o d 2 * ) ,   0 ( * F I D _ C o d 3 * ) ,   0 ( * F I D _ C o d 4 * ) ,  
                     M L A G e r a l . B o o l T o I n t ( C k R e t o r n a . C h e c k e d ) ,   G e r a l . F D T ( V e n c t o ,   1 ) ,  
                     C r e d i t o - D e b i t o ,   0 ( * M o r a D i a V a l * ) ,   0 ( * M u l t a V a l * ) ,   0 ( * C e d e n t e * ) ,  
                     S e r i e C H ,   F L i m i t e S a i ,   F L i m i t e R e m ,   F L i m i t e R e t  
                 ] ,   [ C o n t a ] ,   T r u e )   t h e n  
                 / /   e r r o   n o   c o n t r o l e   d e   l a n c a m e n t o s ?  
                 U F i n a n c e i r o . S Q L I n s U p d _ L c t ( D m o d . Q r U p d ,   s t U p d ,   F a l s e ,   [  
                     ' P r o t o c o l o ' ] ,   [ ' C o n t r o l e ' ,   ' S u b ' ] ,   [  
                       C o n t a ] ,   [ C o n t r o l e ,   S u b ] ,   T r u e ,   ' ' ,   F T a b L c t A ) ;  
                 / /  
             e n d ;  
             / /  
         e n d ;  
         p r o c e d u r e   G e r a P r o t o c o l o _ L a n c a m e n t o s ( v a r   S e m Q u e r y :   I n t e g e r ) ;  
         v a r  
             P r o t o c o l o ,   C o n t r o l e ,   S u b ,   C o n t a ,   C l i I n t ,   C l i e n t e ,   F o r n e c e :   I n t e g e r ;  
             C r e d i t o ,   D e b i t o ,   D o c u m e n t o :   D o u b l e ;  
             S e r i e C H :   S t r i n g ;  
             V e n c t o :   T D a t e T i m e ;  
         b e g i n  
             P r o t o c o l o   : =   - 1 ;  
             C o n t r o l e     : =   0 ;  
             C l i I n t         : =   0 ;  
             C l i e n t e       : =   0 ;  
             F o r n e c e       : =   0 ;  
             C r e d i t o       : =   0 ;  
             D e b i t o         : =   0 ;  
             D o c u m e n t o   : =   0 ;  
             V e n c t o         : =   0 ;  
             S u b               : =   0 ;  
             i f   F Q r S o u r c e   < >   n i l   t h e n  
             b e g i n  
                 P r o t o c o l o   : =   0 ;  
                 C o n t r o l e     : =   F Q r S o u r c e . F i e l d B y N a m e ( ' C o n t r o l e '     ) . A s I n t e g e r ;  
                 S u b               : =   F Q r S o u r c e . F i e l d B y N a m e ( ' S u b '               ) . A s I n t e g e r ;  
                 C l i I n t         : =   F Q r S o u r c e . F i e l d B y N a m e ( ' C l i I n t '         ) . A s I n t e g e r ;  
                 C l i e n t e       : =   F Q r S o u r c e . F i e l d B y N a m e ( ' C l i e n t e '       ) . A s I n t e g e r ;  
                 F o r n e c e       : =   F Q r S o u r c e . F i e l d B y N a m e ( ' F o r n e c e d o r ' ) . A s I n t e g e r ;  
                 / /  
                 D o c u m e n t o   : =   F Q r S o u r c e . F i e l d B y N a m e ( ' D o c u m e n t o '   ) . A s F l o a t ;  
                 C r e d i t o       : =   F Q r S o u r c e . F i e l d B y N a m e ( ' C r e d i t o '       ) . A s F l o a t ;  
                 D e b i t o         : =   F Q r S o u r c e . F i e l d B y N a m e ( ' D e b i t o '         ) . A s F l o a t ;  
                 / /  
                 S e r i e C H       : =   F Q r S o u r c e . F i e l d B y N a m e ( ' S e r i e C H '       ) . A s S t r i n g ;  
                 / /  
                 V e n c t o         : =   F Q r S o u r c e . F i e l d B y N a m e ( ' V e n c i m e n t o ' ) . A s D a t e T i m e ;  
                 / /  
             e n d   e l s e   b e g i n  
                 I n c ( S e m Q u e r y ,   1 ) ;  
             e n d ;  
             i f   P r o t o c o l o   =   0   t h e n  
             b e g i n  
                 C o n t a   : =   U M y M o d . B u s c a E m L i v r e Y _ D e f ( ' p r o t p a k i t s ' ,   ' c o n t a ' ,   s t I n s ,   0 ) ;  
                 / /  
                 i f   U M y M o d . S Q L I n s U p d ( D m o d . Q r U p d ,   s t I n s ,   ' p r o t p a k i t s ' ,   F a l s e ,  
                 [  
                     ' C o d i g o ' ,   ' C o n t r o l e ' ,   ' L i n k _ I D ' ,   ' C l i I n t ' ,  
                     ' C l i e n t e ' ,   ' F o r n e c e ' ,   ' P e r i o d o ' ,   ' L a n c t o ' ,  
                     ' D o c u m ' ,   ' D e p t o ' ,  
                     ' I D _ C o d 1 ' ,   ' I D _ C o d 2 ' ,   ' I D _ C o d 3 ' ,   ' I D _ C o d 4 ' ,  
                     ' R e t o r n a ' ,   ' V e n c t o ' ,  
                     ' V a l o r ' ,   ' M o r a D i a V a l ' ,   ' M u l t a V a l ' ,   ' C e d e n t e ' ,  
                     ' S e r i e C H ' ,   ' L i m i t e S a i ' ,   ' L i m i t e R e m ' ,   ' L i m i t e R e t '  
                 ] ,   [ ' C o n t a ' ] ,   [  
                     Q r P r o t o c o P a k C o d i g o . V a l u e ,   Q r P r o t o c o P a k C o n t r o l e . V a l u e ,  
                     V A R _ T I P O _ L I N K _ I D _ 0 1 _ G E N E R I C O ,   C l i I n t ,  
                     C l i e n t e ,   F o r n e c e ,   0 ( * P e r i o d o * ) ,   C o n t r o l e ,  
                     D o c u m e n t o ,   0 ,  
                     0 ( * F I D _ C o d 1 * ) ,   0 ( * F I D _ C o d 2 * ) ,   0 ( * F I D _ C o d 3 * ) ,   0 ( * F I D _ C o d 4 * ) ,  
                     M L A G e r a l . B o o l T o I n t ( C k R e t o r n a . C h e c k e d ) ,   G e r a l . F D T ( V e n c t o ,   1 ) ,  
                     C r e d i t o - D e b i t o ,   0 ( * M o r a D i a V a l * ) ,   0 ( * M u l t a V a l * ) ,   0 ( * C e d e n t e * ) ,  
                     S e r i e C H ,   F L i m i t e S a i ,   F L i m i t e R e m ,   F L i m i t e R e t  
                 ] ,   [ C o n t a ] ,   T r u e )   t h e n  
                 / /   e r r o   n o   c o n t r o l e   d e   l a n c a m e n t o s ?  
                 U F i n a n c e i r o . S Q L I n s U p d _ L c t ( D m o d . Q r U p d ,   s t U p d ,   F a l s e ,   [  
                     ' P r 