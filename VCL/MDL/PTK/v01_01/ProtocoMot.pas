unit ProtocoMot;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, DmkDAC_PF,
  dmkDBGridDAC, Db, mySQLDbTables, dmkDBGrid, Menus, frxClass, frxDBSet,
  dmkImage, UnDmkEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmProtocoMot = class(TForm)
    Panel1: TPanel;
    DsProtocoMot: TDataSource;
    QrProtocoMot: TmySQLQuery;
    dmkDBGridDAC1: TdmkDBGridDAC;
    PMAcao: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui1: TMenuItem;
    QrProtocoMotCodigo: TIntegerField;
    QrProtocoMotNome: TWideStringField;
    QrProtocoMotLk: TIntegerField;
    QrProtocoMotDataCad: TDateField;
    QrProtocoMotDataAlt: TDateField;
    QrProtocoMotUserCad: TIntegerField;
    QrProtocoMotUserAlt: TIntegerField;
    QrProtocoMotAlterWeb: TSmallintField;
    QrProtocoMotAtivo: TSmallintField;
    N1: TMenuItem;
    Imprimegrade1: TMenuItem;
    frxDsCadProtoMot: TfrxDBDataset;
    frxProtoMot: TfrxReport;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    BtAcao: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure dmkDBGridDAC1CellClick(Column: TColumn);
    procedure Imprimegrade1Click(Sender: TObject);
    procedure frxProtoMotGetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
    procedure ReopenQrProtocoMot(Codigo: Double);
  public
    { Public declarations }
  end;

  var
  FmProtocoMot: TFmProtocoMot;

implementation

uses UnMyObjects, Module, UMySQLModule, ModuleGeral;

{$R *.DFM}

procedure TFmProtocoMot.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProtocoMot.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProtocoMot.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProtocoMot.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  ReopenQrProtocoMot(0);
end;

procedure TFmProtocoMot.Inclui1Click(Sender: TObject);
var
  Codigo: String;
  CodVal: Integer;
begin
  //CodVal := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    //'ProtocoMot', 'ProtocoMot', 'Codigo');
  CodVal := UMyMod.BuscaPrimeiroCodigoLivre('protocomot', 'Codigo');
  Codigo := Geral.FFT(CodVal, 0, siPositivo);
  if InputQuery('Novo motivo', 'Informe o c�digo do motivo:',
  Codigo) then
  begin
    CodVal := Geral.IMV(Codigo);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO protocomot SET Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := CodVal;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrProtocoMot(CodVal);
  end;
end;

procedure TFmProtocoMot.ReopenQrProtocoMot(Codigo: Double);
begin
  QrProtocoMot.Close;
  UnDmkDAC_PF.AbreQuery(QrProtocoMot, Dmod.MyDB);
  QrProtocoMot.Locate('Codigo', Codigo, []);
end;

procedure TFmProtocoMot.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmProtocoMot.Exclui1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do registro selecionado?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM protocomot WHERE Codigo=:P0');
    Dmod.QrUpd.Params[00].AsInteger := QrProtocoMotCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrProtocoMot(Int(Date));
  end;
end;

procedure TFmProtocoMot.dmkDBGridDAC1CellClick(Column: TColumn);
var
  Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if QrProtocoMotAtivo.Value = 1 then Ativo := 0 else Ativo := 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE protocomot SET Ativo=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
    //
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsFloat   := QrProtocoMotCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrProtocoMot(QrProtocoMotCodigo.Value);
  end;
end;

procedure TFmProtocoMot.Imprimegrade1Click(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxProtoMot, [
    DModG.frxDsMaster,
    frxDsCadProtoMot
    ]);
  MyObjects.frxMostra(frxProtoMot, Caption);
end;

procedure TFmProtocoMot.frxProtoMotGetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_TITULO') = 0 then
    Value := Caption;
end;

end.

