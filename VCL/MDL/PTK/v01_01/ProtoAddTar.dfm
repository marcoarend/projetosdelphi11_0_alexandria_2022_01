object FmProtoAddTar: TFmProtoAddTar
  Left = 395
  Top = 185
  Caption = 'GER-PROTO-012 :: Sel. Protocolo Padr'#227'o'
  ClientHeight = 242
  ClientWidth = 479
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 479
    Height = 86
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label3: TLabel
      Left = 20
      Top = 9
      Width = 34
      Height = 13
      Caption = 'Tarefa:'
    end
    object SpeedButton1: TSpeedButton
      Left = 435
      Top = 26
      Width = 24
      Height = 21
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object CBTarefa: TdmkDBLookupComboBox
      Left = 76
      Top = 26
      Width = 357
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsProtocolos
      TabOrder = 1
      dmkEditCB = EdTarefa
      QryCampo = 'Protocolo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdTarefa: TdmkEditCB
      Left = 20
      Top = 26
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Protocolo'
      UpdCampo = 'Protocolo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBTarefa
      IgnoraDBLookupComboBox = False
    end
    object CkPadrao: TCheckBox
      Left = 20
      Top = 53
      Width = 280
      Height = 17
      Caption = 'Definir como padr'#227'o para as entidades selecionadas'
      TabOrder = 2
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 479
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 431
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 383
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 114
        Height = 32
        Caption = 'Protocolo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 114
        Height = 32
        Caption = 'Protocolo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 114
        Height = 32
        Caption = 'Protocolo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 134
    Width = 479
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 475
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 178
    Width = 479
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 475
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 331
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 6
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 18
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object DsProtocolos: TDataSource
    DataSet = QrProtocolos
    Left = 256
    Top = 130
  end
  object QrProtocolos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ptc.Codigo, ptc.Nome, ptc.Tipo'
      'FROM protocolos ptc'
      'WHERE ptc.Codigo > 0'
      'AND ptc.Tipo in (1,2)'
      'ORDER BY Nome')
    Left = 228
    Top = 130
    object QrProtocolosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProtocolosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrProtocolosTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 304
    Top = 11
  end
end
