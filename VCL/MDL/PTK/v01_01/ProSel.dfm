object FmProSel: TFmProSel
  Left = 339
  Top = 185
  Caption = 'GER-PROTO-013 :: Protocolo Lotes'
  ClientHeight = 519
  ClientWidth = 479
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 479
    Height = 363
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PainelDados: TPanel
      Left = 0
      Top = 0
      Width = 479
      Height = 140
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label3: TLabel
        Left = 6
        Top = 4
        Width = 66
        Height = 13
        Caption = 'Configura'#231#227'o:'
      end
      object SpeedButton1: TSpeedButton
        Left = 445
        Top = 20
        Width = 24
        Height = 21
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object CBTarefa: TdmkDBLookupComboBox
        Left = 62
        Top = 20
        Width = 380
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsProtocolos
        TabOrder = 0
        dmkEditCB = EdTarefa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdTarefa: TdmkEditCB
        Left = 6
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTarefaChange
        DBLookupComboBox = CBTarefa
        IgnoraDBLookupComboBox = False
      end
      object CkRetorna: TCheckBox
        Left = 6
        Top = 44
        Width = 453
        Height = 17
        Caption = 'Devolve documento / aviso que recebeu.'
        TabOrder = 2
        OnClick = CkRetornaClick
      end
      object GroupBox1: TGroupBox
        Left = 12
        Top = 68
        Width = 453
        Height = 65
        Caption = ' Datas limites do item de protocolo: '
        TabOrder = 3
        object LaLimiteSai: TLabel
          Left = 12
          Top = 16
          Width = 32
          Height = 13
          Caption = 'Sa'#237'da:'
        end
        object LaLimiteRem: TLabel
          Left = 136
          Top = 16
          Width = 98
          Height = 13
          Caption = 'Chegada no destino:'
        end
        object LaLimiteRet: TLabel
          Left = 260
          Top = 16
          Width = 41
          Height = 13
          Caption = 'Retorno:'
          Visible = False
        end
        object TPLimiteSai: TdmkEditDateTimePicker
          Left = 12
          Top = 32
          Width = 109
          Height = 21
          Date = 40238.476802430550000000
          Time = 40238.476802430550000000
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPLimiteRem: TdmkEditDateTimePicker
          Left = 136
          Top = 32
          Width = 109
          Height = 21
          Date = 40238.476802430550000000
          Time = 40238.476802430550000000
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPLimiteRet: TdmkEditDateTimePicker
          Left = 260
          Top = 32
          Width = 109
          Height = 21
          Date = 40238.476802430550000000
          Time = 40238.476802430550000000
          TabOrder = 2
          Visible = False
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 140
      Width = 479
      Height = 223
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 479
        Height = 44
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object CkAbertos: TCheckBox
          Left = 12
          Top = 4
          Width = 181
          Height = 17
          Caption = 'Pesquisar somente lotes abertos.'
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = CkAbertosClick
        end
        object StaticText1: TStaticText
          Left = 0
          Top = 24
          Width = 479
          Height = 20
          Align = alBottom
          Alignment = taCenter
          AutoSize = False
          BorderStyle = sbsSunken
          Caption = 'Selecione o lote no qual ser'#225' adicionado os protocolos:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 44
        Width = 479
        Height = 179
        Align = alClient
        DataSource = DsProtocoPak
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'Lote'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataL'
            Title.Caption = 'Data limite'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataI'
            Title.Caption = 'Criado em'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATAF_TXT'
            Title.Caption = 'Encerrado em'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MES'
            Title.Caption = 'Per'#237'odo'
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 479
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 431
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 383
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 189
        Height = 32
        Caption = 'Protocolo Lotes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 189
        Height = 32
        Caption = 'Protocolo Lotes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 189
        Height = 32
        Caption = 'Protocolo Lotes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 411
    Width = 479
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 475
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 455
    Width = 479
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 475
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 331
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object BtSaida: TBitBtn
          Tag = 13
          Left = 10
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 10
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 174
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Novo Lote'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BitBtn1Click
      end
    end
  end
  object QrProtocolos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ptc.Codigo, ptc.Nome, ptc.Def_Retorn,'
      'ptc.Tipo, ptc.Def_Client'
      'FROM protocolos ptc'
      'WHERE ptc.Codigo > 0'
      'AND ptc.Tipo=:P0'
      'AND ptc.Codigo=:P1'
      'ORDER BY Nome')
    Left = 16
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrProtocolosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrProtocolosNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrProtocolosDef_Retorn: TIntegerField
      FieldName = 'Def_Retorn'
      Required = True
    end
    object QrProtocolosTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrProtocolosDef_Client: TIntegerField
      FieldName = 'Def_Client'
    end
  end
  object DsProtocolos: TDataSource
    DataSet = QrProtocolos
    Left = 44
    Top = 10
  end
  object QrProtocoPak: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ptp.*, '
      'CONCAT(RIGHT(Mez, 2), '#39'/'#39',  LEFT(Mez + 200000, 4)) MES,'
      
        'IF(ptp.DataF=0,"", DATE_FORMAT(ptp.DataF, "%d/%m/%y" )) DATAF_TX' +
        'T'
      'FROM protocopak ptp'
      'WHERE ptp.Codigo=:P0'
      'AND ptp.DataF>:P1'
      'ORDER BY Controle DESC')
    Left = 72
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrProtocoPakCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrProtocoPakControle: TIntegerField
      FieldName = 'Controle'
      Required = True
      DisplayFormat = '000000'
    end
    object QrProtocoPakDataI: TDateField
      FieldName = 'DataI'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrProtocoPakDataL: TDateField
      FieldName = 'DataL'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrProtocoPakDataF: TDateField
      FieldName = 'DataF'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrProtocoPakLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrProtocoPakDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProtocoPakDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProtocoPakUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrProtocoPakUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrProtocoPakAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrProtocoPakAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrProtocoPakMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrProtocoPakMES: TWideStringField
      FieldName = 'MES'
      Required = True
      Size = 7
    end
    object QrProtocoPakDATAF_TXT: TWideStringField
      FieldName = 'DATAF_TXT'
      Size = 8
    end
  end
  object DsProtocoPak: TDataSource
    DataSet = QrProtocoPak
    Left = 100
    Top = 10
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 304
    Top = 11
  end
end
