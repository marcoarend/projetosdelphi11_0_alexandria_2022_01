unit ProtoAddTar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, DB,
  mySQLDbTables, dmkEdit, dmkEditCB, DBCtrls, dmkDBLookupComboBox, dmkGeral,
  dmkImage, dmkDBGrid, dmkPermissoes, UnDmkEnums, dmkDBGridZTO, DmkDAC_PF,
  Protocolo;

type
  TFmProtoAddTar = class(TForm)
    Panel1: TPanel;
    DsProtocolos: TDataSource;
    QrProtocolos: TmySQLQuery;
    CBTarefa: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdTarefa: TdmkEditCB;
    SpeedButton1: TSpeedButton;
    QrProtocolosCodigo: TIntegerField;
    QrProtocolosNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    CkPadrao: TCheckBox;
    QrProtocolosTipo: TIntegerField;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenProtocolos();
    procedure VerificaSeProtocoloPadraoExiste(const Protocolo,
                Entidade: Integer; var Codigo, Controle: Integer);
  public
    { Public declarations }
    FTiposProt: String; //Separar por v�rgula. Ex.: 1,2
    FProtNum, FCliInt: Integer;
    FFinalidade: TProtFinalidade;
    FQuery: TmySQLQuery;
    FGrade: TdmkDBGridZTO;
  end;

  var
  FmProtoAddTar: TFmProtoAddTar;

implementation

uses Protocolos, UnInternalConsts, MyDBCheck, UMySQLModule, Module, UnMyObjects,
{$IfDef IMP_BLOQLCT} UnBloquetos, {$EndIf}
UnProtocoUnit;

{$R *.DFM}

procedure TFmProtoAddTar.BtOKClick(Sender: TObject);

  procedure MostraMensagemEncerrado(Bloqueto: Double);
  begin
    Geral.MB_Aviso('N�o foi poss�vel editar o protocolo do boleto n�mero'
      + Geral.FFI(Bloqueto) + sLineBreak + 'Motivo: Per�odo encerrado!');
  end;

  {$IfDef IMP_BLOQLCT}
  function AtualizaProtocoloBoleto(Protocolo, Codigo, Entidade: Integer;
    Boleto: Double): Boolean;
  var
    CodigoPro, ControlePro: Integer;
    CampoProtocolo: String;
  begin
    Result := False;
    //
    if FFinalidade = ptkBoleto then
    begin
      case FProtNum of
          1: CampoProtocolo := 'Protocolo';
          2: CampoProtocolo := 'Protocolo2';
          3: CampoProtocolo := 'Protocolo3';
        else
        begin
          Geral.MB_Aviso('Falha ao definir protocolo!');
          Exit;
        end;
      end;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'arreits', False, [CampoProtocolo],
        ['Codigo', 'Entidade', 'Boleto'], [Protocolo],
        [Codigo, Entidade, Geral.FFI(Boleto)], True) then
      begin
        if (CkPadrao.Checked) and (Protocolo <> 0) then
        begin
          VerificaSeProtocoloPadraoExiste(Protocolo, Entidade,
            CodigoPro, ControlePro);
          if CodigoPro = 0 then
          begin
            CodigoPro := UMyMod.BuscaEmLivreY_Def('proenpr', 'Codigo', stIns, 0);
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'proenpr', False,
              ['Protocolo', 'Finalidade'], ['Codigo'], [Protocolo, 0], [CodigoPro], True);
          end;
          if (CodigoPro <> 0) and (ControlePro = 0) then
          begin
            ControlePro := UMyMod.BuscaEmLivreY_Def('proenprit', 'Controle', stIns, 0);
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'proenprit', False,
              ['Entidade', 'Codigo'], ['Controle'],
              [Entidade, CodigoPro], [ControlePro], True);
          end;
          Result := UnProtocolo.RemoveProEnPrItsDuplicados(Dmod.MyDB, FTiposProt,
            FFinalidade, FCliInt, Entidade, CodigoPro);
        end else
          Result := True;
      end;
    end else
      Geral.MB_Aviso('Finalidade n�o implementada!');
  end;
  {$EndIf}

  {$IfNDef sNFSe}
  function AtualizaProtocoloNFSe: Boolean;
  var
    Padrao, Tarefa, Finalidade, CodigoPro, ControlePro: Integer;
  begin
    Result := False;
    Tarefa := EdTarefa.ValueVariant;
    Padrao := Geral.BoolToInt(CkPadrao.Checked);
    //
    if FFinalidade = ptkNFSeAut then
      Finalidade := 1
    else
      Finalidade := 2;
    //
    if MyObjects.FIC(Tarefa = 0, EdTarefa, 'O campo Tarefa deve ser preenchido!') then Exit;
    if MyObjects.FIC(Padrao = 0, CkPadrao, 'O campo Definir como padr�o para as entidades selecionadas deve ser marcado!') then Exit;
    //
    VerificaSeProtocoloPadraoExiste(Tarefa, 0, CodigoPro, ControlePro);

    if CodigoPro = 0 then
    begin
      CodigoPro := UMyMod.BuscaEmLivreY_Def('proenpr', 'Codigo', stIns, 0);
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'proenpr', False,
                ['Protocolo', 'Finalidade'], ['Codigo'],
                [Tarefa, Finalidade], [CodigoPro], True);
    end;
  end;
  {$EndIf}
var
  Fechar: Boolean;
  i, Tarefa: Integer;
begin
  Screen.Cursor := crHourGlass;
  Tarefa        := EdTarefa.ValueVariant;
  Fechar        := False;
  //
  try
    if FFinalidade = ptkBoleto then
    begin
      {$IfDef IMP_BLOQLCT}
      if FGrade.SelectedRows.Count > 1 then
      begin
        with FGrade.DataSource.DataSet do
        for i := 0 to FGrade.SelectedRows.Count - 1 do
        begin
          GotoBookmark(pointer(FGrade.SelectedRows.Items[i]));
          //
          if not UBloquetos.VerificaSePeriodoEstaEncerrado(
            FQuery.FieldByName('Codigo').AsInteger) then
          begin
            Fechar := AtualizaProtocoloBoleto(Tarefa,
                        FQuery.FieldByName('Codigo').AsInteger,
                        FQuery.FieldByName('Entidade').AsInteger,
                        FQuery.FieldByName('Boleto').AsFloat);
          end else
            MostraMensagemEncerrado(FQuery.FieldByName('Boleto').AsFloat);
        end;
      end else
      begin
        if not UBloquetos.VerificaSePeriodoEstaEncerrado(
          FQuery.FieldByName('Codigo').AsInteger) then
        begin
          Fechar := AtualizaProtocoloBoleto(Tarefa,
                      FQuery.FieldByName('Codigo').AsInteger,
                      FQuery.FieldByName('Entidade').AsInteger,
                      FQuery.FieldByName('Boleto').AsFloat);
        end else
          MostraMensagemEncerrado(FQuery.FieldByName('Boleto').AsFloat);
      end;
      {$EndIf}
    end else
    if (FFinalidade = ptkNFSeAut) or (FFinalidade = ptkNFSeCan) then
    begin
      {$IfNDef sNFSe}
        Fechar := AtualizaProtocoloNFSe;
      {$EndIf}
    end;
  finally
    Screen.Cursor := crDefault;
    if Fechar then
      Close;
  end;
end;

procedure TFmProtoAddTar.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProtoAddTar.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmProtoAddTar.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmProtoAddTar.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProtoAddTar.FormShow(Sender: TObject);
begin
  //N�o mostrar o 4 Com Registro pois deve ser para todos
  ReopenProtocolos;
end;

procedure TFmProtoAddTar.ReopenProtocolos;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrProtocolos, Dmod.MyDB, [
    'SELECT ptc.Codigo, ptc.Nome, ptc.Tipo ',
    'FROM protocolos ptc ',
    'WHERE ptc.Codigo <> 0 ',
    'AND ptc.Tipo in (' + FTiposProt + ') ',
    'AND ptc.Def_Client=' + Geral.FF0(FCliInt),
    'ORDER BY Nome ',
    '']);
end;

procedure TFmProtoAddTar.SpeedButton1Click(Sender: TObject);
var
  Tarefa: Integer;
begin
  VAR_CADASTRO := 0;
  Tarefa       := EdTarefa.ValueVariant;

  ProtocoUnit.MostraFormProtocolos(0, Tarefa);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdTarefa, CBTarefa, QrProtocolos, VAR_CADASTRO);
    CBTarefa.SetFocus;
  end;
end;

procedure TFmProtoAddTar.VerificaSeProtocoloPadraoExiste(const Protocolo,
  Entidade: Integer; var Codigo, Controle: Integer);
var
  Query: TMySQLQuery;
begin
  Codigo   := 0;
  Controle := 0;

  Query := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    //Verifica protocolo padr�o
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM proenpr ',
      'WHERE Protocolo=' + Geral.FF0(Protocolo),
      '']);
    if Query.RecordCount > 0 then
      Codigo := Query.FieldByName('Codigo').AsInteger
    else
      Exit;
    if FFinalidade = ptkBoleto then
    begin
      //Verifica itens do protocolo padr�o
      UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
        'SELECT Controle ',
        'FROM proenprit ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        'AND Entidade=' + Geral.FF0(Entidade),
        '']);
      if Query.RecordCount > 0 then
        Controle := Query.FieldByName('Controle').AsInteger;
    end;
  finally
    Query.Free;
  end;
end;

end.
