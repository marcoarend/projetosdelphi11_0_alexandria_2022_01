unit Protocolo;

interface

uses
  Forms, StdCtrls, ComCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes,
  Graphics, Controls, Dialogs, Variants, MaskUtils, UnMsgInt, mySQLDbTables,
  DB,(* DbTables,*) DBGrids, mySQLExceptions, dmkGeral, UnInternalConsts,
  UnDmkEnums;

type
  TComoConf = (ptkTodos=-2, ptkOutros=-1, ptkManual=1, ptkEmail=2, ptkJaPagou=3, ptkWebLink=4, ptkWebAuto=5);
  TProtFinalidade = (ptkBoleto, ptkNFSeAut, ptkNFSeCan);
  TUnProtocolo = class(TObject)
  private
    {private declaration}
    function  LocalizaLote(ProtocoPak: Integer; QueryLoc: TmySQLQuery;
              Database: TmySQLDatabase): Integer;
    procedure GeraProtocolo_NFSeProto(RecipEmeio: String;
              QuerySource: TmySQLQuery; Database: TmySQLDatabase;
              ProtocoPak, EntCliInt, TipoProt: Integer);
    procedure GeraProtocolo_BloqProto(RecipEmeio: String; QuerySource, QueryLoc,
              QueryUpd: TmySQLQuery; Database: TmySQLDatabase; ProtocoPak,
              EntCliInt, TipoProt: Integer);
  public
    {public declaration}
    function  IntOfComoConf(ComoConf: TComoConf): Integer;
    function  NomeDoTipoDeProtocolo(Tipo: Integer): String;
    function  LocalizaProtocolo(Database: TmySQLDatabase; EntiDepto: Integer;
              Excecao: Integer = 0): Integer;
    function  LocalizaProtocoloCR(Database: TmySQLDatabase; CNAB_Cfg: Integer): Integer;
    //
    procedure ProtocolosCD_Lct(Quais: TSelType; DBGLct: TDBGrid;
              ModuleLctX: TDataModule; CliInt: Integer; FormOrigem: String);
    procedure ProtocolosCD_Doc(Quais: TSelType; DBGLct: TDBGrid;
              ModuleLctX: TDataModule; CliInt: Integer; FormOrigem: String);
    procedure ExcluiLoteProtocoloBoleto(QueryLoc, QueryUpd: TmySQLQuery;
              Database: TmySQLDatabase; Arreits: Integer; Tabela: String);
    procedure ExcluiLoteProtocoloNFSe(QueryLoc, QueryUpd: TmySQLQuery;
              Database: TmySQLDatabase; CliInt, Ambiente, EntiMail, ProtocoPak,
              RpsIDNumero: Integer; RpsIDSerie: String);
    function  GeraLoteProtocolo(Database: TmySQLDatabase; Protocolo,
              CNAB_Cfg, TipoProt: Integer): Integer;
    function  ProtocolosCD_OS1(Quais: TSelType; DBGOScab: TDBGrid;
              QrOSCab: TmySQLQuery; CliInt: Integer): Integer;
    function  GeraProtocoloNFSe(Quais: TSelType; QueryIts: TmySQLQuery;
              Database: TmySQLDatabase; Grade: TDBGrid;
              Progress: TProgressBar; EntCliInt: Integer): Boolean;
    function  GeraProtocoloBoleto(Quais: TSelType; QueryIts, QueryUpd,
              QueryLoc: TmySQLQuery; Database: TmySQLDatabase; Grade: TDBGrid;
              Progress: TProgressBar; EntCliInt: Integer): Boolean;
    function  GetTipoProtocolo(Aba: Integer): Integer;
    //
    procedure Proto_Email_GeraLinksConfirmacaoReceb(const DataBase: TmySQLDatabase;
              const Web_MyURL, Email: String; const Cliente, Protocolo,
              DMKID_APP: Integer; var ParamLink, ParamTopo: String);
    function  Proto_Email_CriaProtocoloEnvio(DataBase: TmySQLDatabase; Cliente,
              Protocolo: Integer; Email: String): Boolean;
    function  Proto_Email_AtualizDataE(DataBase: TmySQLDatabase;
              Protocolo: Integer; Agora: TDateTime): Boolean;
    function  ObtemListaProEnPrFinalidade(): TStringList;
    function  RemoveProEnPrItsDuplicados(DataBase: TmySQLDatabase;
              TiposProt: String; Finalidade: TProtFinalidade; CliInt, Entidade,
              ProEnPr: Integer): Boolean;
  end;

var
  UnProtocolo: TUnProtocolo;

const
  VAR_TIPO_PROTOCOLO_ND_00 = 0; // ND - N�o definido
  VAR_TIPO_PROTOCOLO_EB_01 = 1; // EB - Entrega de Bloquetos
  VAR_TIPO_PROTOCOLO_CE_02 = 2; // CE - Circula��o eletr�nica (emeio)
  VAR_TIPO_PROTOCOLO_EM_03 = 3; // EM - Entrega de material de consumo
  VAR_TIPO_PROTOCOLO_CR_04 = 4; // CR - Cobran�a registrada (banc�ria)
  VAR_TIPO_PROTOCOLO_CD_05 = 5; // CD - Circula��o de documentos

  VAR_TIPO_LINK_ID_01_GENERICO = 1; // ????
  VAR_TIPO_LINK_ID_02_FATURA_PROD_SERV = 2; // Fatura de produtos / servi�os (Parcelamento de fatura)
  VAR_TIPO_LINK_ID_03_ORDEM_DE_SERVCO1 = 3; // Ordens de servi�o (mais algum al�m do bgstrl?)

implementation

uses UnCreateProtoco,
{$IFDEF DEFINE_VARLCT}ModuleLct2, {$ENDIF}
{$IFNDEF NO_FINANCEIRO} UCreateFin, {$EndIf}
ProSel, MyDBCheck, ModuleGeral, UnMyObjects, UMySQLModule, DmkDAC_PF;

{ TUnProtocolo }

function TUnProtocolo.LocalizaLote(ProtocoPak: Integer; QueryLoc: TmySQLQuery;
  Database: TmySQLDatabase): Integer;
begin
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
      'SELECT Controle, SeqArq ',
      'FROM protocopak ',
      'WHERE Controle=' + Geral.FF0(ProtocoPak),
      '']);
    if (QueryLoc.RecordCount > 0) and (QueryLoc.FieldByName('SeqArq').AsInteger = 0) then
      Result := QueryLoc.FieldByName('Controle').AsInteger
    else
      Result := 0;
  except
    Result := -1;
  end;
end;

function TUnProtocolo.LocalizaProtocolo(Database: TmySQLDatabase;
  EntiDepto: Integer; Excecao: Integer = 0): Integer;
var
  Qry: TmySQLQuery;
  Campo, SQLCompl: String;
begin
  Result := 0;
  Qry    := TmySQLQuery.Create(TDataModule(Database.Owner));
  try
    if EntiDepto <> 0 then
    begin
      {$IfDef TEM_UH}
        Campo := 'Depto';
      {$Else}
        Campo := 'Entidade';
      {$EndIf}
      if Excecao <> 0 then
        SQLCompl := 'AND pro.Protocolo <> ' + Geral.FF0(Excecao)
      else
        SQLCompl := '';
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
        'SELECT pro.Protocolo ',
        'FROM proenprit its ',
        'LEFT JOIN proenpr pro ON pro.Codigo = its.Codigo ',
        'WHERE its.' + Campo + '=' + Geral.FF0(EntiDepto),
        'AND pro.Finalidade = 0 ', //Boletos
        SQLCompl,
        '']);
      if Qry.RecordCount > 0 then
        Result := Qry.FieldByName('Protocolo').AsInteger;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnProtocolo.LocalizaProtocoloCR(Database: TmySQLDatabase; CNAB_Cfg: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(TDataModule(Database.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
      'SELECT ProtocolCR ',
      'FROM cnab_cfg ',
      'WHERE Codigo = ' + Geral.FF0(CNAB_Cfg),
      '']);
    if Qry.RecordCount > 0 then
      Result := Qry.FieldByName('ProtocolCR').AsInteger;
  finally
    Qry.Free;
  end;
end;

procedure TUnProtocolo.GeraProtocolo_NFSeProto(RecipEmeio: String;
  QuerySource: TmySQLQuery; Database: TmySQLDatabase; ProtocoPak,
  EntCliInt, TipoProt: Integer);
var
  Protocolo, Entidade, Conta, Emeio_ID, Retorna, ProtPakIts, Ambiente,
  RpsIDNumero: Integer;
  Vencto: TDateTime;
  NfsNumero: Largeint;
  Valor: Double;
  LimiteSai, LimiteRem, LimiteRet, RpsIDSerie: String;
  QueryLoc, QueryUpd: TmySQLQuery;
begin
  Entidade  := 0;
  NfsNumero := 0;
  Vencto    := 0;
  Valor     := 0;
  //
  QueryLoc := TmySQLQuery.Create(TDataModule(Database.Owner));
  QueryUpd := TmySQLQuery.Create(TDataModule(Database.Owner));
  //
  QueryLoc.DataBase := Database;
  QueryUpd.DataBase := Database;
  //
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
      'SELECT cfg.MultaPerc, cfg.JurosPerc, pak.DataI, ',
      'pak.DataL, pak.DataF, pro.Def_Retorn ',
      'FROM protocopak pak ',
      'LEFT JOIN protocolos pro ON pro.Codigo = pak.Codigo ',
      'LEFT JOIN cnab_cfg cfg ON cfg.Codigo=pak.CNAB_Cfg ',
      'WHERE pak.Controle=' + Geral.FF0(ProtocoPak),
      '']);
    //
    Retorna     := QueryLoc.FieldByName('Def_Retorn').AsInteger;
    LimiteSai   := Geral.FDT(QueryLoc.FieldByName('DataI').AsDateTime, 1);
    LimiteRem   := Geral.FDT(QueryLoc.FieldByName('DataL').AsDateTime, 1);
    LimiteRet   := Geral.FDT(QueryLoc.FieldByName('DataF').AsDateTime, 1);
    Protocolo   := QuerySource.FieldByName('PROTOCOD').AsInteger;
    Entidade    := QuerySource.FieldByName('Entidade').AsInteger;
    NfsNumero   := QuerySource.FieldByName('NfsNumero').AsLargeInt;
    Vencto      := QuerySource.FieldByName('RpsDataEmissao').AsDateTime;
    Valor       := QuerySource.FieldByName('ValorServicos').AsFloat;
    ProtPakIts  := QuerySource.FieldByName('PROTOCOLO').AsInteger;
    EMeio_ID    := Trunc(QuerySource.FieldByName('EMeio_ID').AsFloat);
    Ambiente    := QuerySource.FieldByName('Ambiente').AsInteger;
    RpsIDSerie  := QuerySource.FieldByName('RpsIDSerie').AsString;
    RpsIDNumero := QuerySource.FieldByName('RpsIDNumero').AsInteger;
    //
    if Protocolo = 0 then
      Exit;
    //
    if ProtPakIts = 0 then
    begin
      Conta := UMyMod.BuscaEmLivreY_Def('protpakits', 'conta', stIns, 0);
      //
      UMyMod.SQLInsUpd(QueryUpd, stIns, 'protpakits', False,
        [
          'Codigo', 'Controle', 'Link_ID', 'CliInt',
          'Cliente', 'Fornece', 'Periodo', 'Lancto', 'Docum',
          'Depto', 'ID_Cod1', 'ID_Cod2', 'ID_Cod3', 'ID_Cod4',
          'Retorna', 'Vencto',
          'Valor', 'MoraDiaVal', 'MultaVal', 'Cedente',
          'LimiteSai', 'LimiteRem', 'LimiteRet'
        ], ['Conta'], [
          Protocolo, ProtocoPak, VAR_TIPO_LINK_ID_01_GENERICO, EntCliInt,
          Entidade, 0, 0, 0, NfsNumero,
          0, Ambiente, RpsIDSerie, RpsIDNumero, EMeio_ID,
          Retorna, Geral.FDT(Vencto, 1),
          Valor, 0, 0, 0,
          LimiteSai, LimiteRem, LimiteRet
        ], [Conta], True);
    end;
  finally
    QueryLoc.Free;
    QueryUpd.Free;
  end;
end;

procedure TUnProtocolo.GeraProtocolo_BloqProto(RecipEmeio: String; QuerySource,
  QueryLoc, QueryUpd: TmySQLQuery; Database: TmySQLDatabase; ProtocoPak,
  EntCliInt, TipoProt: Integer);
var
  Protocolo, Entidade, Conta, Apto, Emeio_ID, Periodo, PrevCod, Retorna,
  CNAB_Cfg, ProtPakIts: Integer;
  Vencto: TDateTime;
  Bloqueto, Valor, MoraDiaVal, MultaVal, MoraPer, MultaPer: Double;
  LimiteSai, LimiteRem, LimiteRet: String;
begin
  Entidade := 0;
  Bloqueto := 0;
  Apto     := 0;
  Vencto   := 0;
  Valor    := 0;
  Periodo  := 0;
  PrevCod  := 0;
  //
  MoraDiaVal := Round(Valor * (1 / 30)) / 100;
  MultaVal   := Round(Valor * 2) / 100;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
    'SELECT cfg.MultaPerc, cfg.JurosPerc, pak.DataI, ',
    'pak.DataL, pak.DataF, pro.Def_Retorn ',
    'FROM protocopak pak ',
    'LEFT JOIN protocolos pro ON pro.Codigo = pak.Codigo ',
    'LEFT JOIN cnab_cfg cfg ON cfg.Codigo=pak.CNAB_Cfg ',
    'WHERE pak.Controle=' + Geral.FF0(ProtocoPak),
    '']);
  //
  Retorna    := QueryLoc.FieldByName('Def_Retorn').AsInteger;
  LimiteSai  := Geral.FDT(QueryLoc.FieldByName('DataI').AsDateTime, 1);
  LimiteRem  := Geral.FDT(QueryLoc.FieldByName('DataL').AsDateTime, 1);
  LimiteRet  := Geral.FDT(QueryLoc.FieldByName('DataF').AsDateTime, 1);
  MoraPer    := QueryLoc.FieldByName('JurosPerc').AsFloat;
  MultaPer   := QueryLoc.FieldByName('MultaPerc').AsFloat;
  Protocolo  := QuerySource.FieldByName('PROTOCOD').AsInteger;
  Entidade   := QuerySource.FieldByName('Entidade').AsInteger;
  Bloqueto   := QuerySource.FieldByName('Boleto').AsFloat;
  Vencto     := QuerySource.FieldByName('Vencto').AsDateTime;
  Valor      := QuerySource.FieldByName('Valor').AsFloat;
  MoraDiaVal := Round(Valor * (MoraPer / 30)) / 100;
  MultaVal   := Round(Valor * MultaPer) / 100;
  Periodo    := QuerySource.FieldByName('Periodo').AsInteger;
  PrevCod    := QuerySource.FieldByName('PREVCOD').AsInteger;
  CNAB_Cfg   := QuerySource.FieldByName('CNAB_Cfg').AsInteger;
  ProtPakIts := QuerySource.FieldByName('PROTOCOLO').AsInteger;
  EMeio_ID   := Trunc(QuerySource.FieldByName('EMeio_ID').AsFloat);
  //
  if Protocolo = 0 then
    Exit;
  //
  if ProtPakIts = 0 then
  begin
    Conta := UMyMod.BuscaEmLivreY_Def('protpakits', 'conta', stIns, 0);
    //
    if UMyMod.SQLInsUpd(QueryUpd, stIns, 'protpakits', False,
      [
        'Codigo', 'Controle', 'Link_ID', 'CliInt',
        'Cliente', 'Fornece', 'Periodo', 'Lancto', 'Docum',
        'Depto', 'ID_Cod1', 'ID_Cod2', 'ID_Cod3', 'ID_Cod4',
        'Retorna', 'Vencto',
        'Valor', 'MoraDiaVal', 'MultaVal', 'Cedente',
        'LimiteSai', 'LimiteRem', 'LimiteRet'
      ], ['Conta'], [
        Protocolo, ProtocoPak, VAR_TIPO_LINK_ID_01_GENERICO, EntCliInt,
        Entidade, 0, Periodo, 0, Bloqueto,
        Apto, PrevCod, EntCliInt, EMeio_ID, 0,
        Retorna, Geral.FDT(Vencto, 1),
        Valor, MoraDiaVal, MultaVal, 0,
        LimiteSai, LimiteRem, LimiteRet
      ], [Conta], True) then
    begin
      if TipoProt = 4 then //CR
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, Database, [
          'UPDATE arreits SET ProtocoloCR="' + Geral.FF0(Protocolo) + '", ',
          'ProtocoPakCR="' + Geral.FF0(ProtocoPak) + '" ',
          'WHERE Boleto="' + Geral.FFI(Bloqueto) + '" ',
          'AND Codigo="' + Geral.FF0(PrevCod) + '" ',
          'AND CNAB_Cfg="' + Geral.FF0(CNAB_Cfg) + '" ',
          '']);
      end else
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, Database, [
          'UPDATE arreits SET Protocolo="' + Geral.FF0(Protocolo) + '", ',
          'ProtocoPak="' + Geral.FF0(ProtocoPak) + '" ',
          'WHERE Boleto="' + Geral.FFI(Bloqueto) + '" ',
          'AND Codigo="' + Geral.FF0(PrevCod) + '" ',
          'AND CNAB_Cfg="' + Geral.FF0(CNAB_Cfg) + '" ',
          '']);
      end;
    end;
  end;
end;

function TUnProtocolo.GetTipoProtocolo(Aba: Integer): Integer;

  procedure AvisaErroGet;
  begin
    Geral.MB_Aviso('Aba do controlador de p�ginas sem implementa��o!' +
      'AVISE A DERMATEK!');
  end;

begin
  case Aba of
      0: Result := 1;
      1: Result := 2;
      2: Result := 4;
    else Result := -1;
  end;
  if Result = -1 then
    AvisaErroGet;
end;

function TUnProtocolo.IntOfComoConf(ComoConf: TComoConf): Integer;
begin
  case ComoConf of
    ptkTodos: //Todos
      Result := -2;
    ptkOutros: //Outros
      Result := -1;
    ptkManual: //Manual(For�ado)
      Result := 1;
    ptkEmail: //E-mail
      Result := 2;
    ptkJaPagou: //J� pagou
      Result := 3;
    ptkWebLink: //Web Link
      Result := 4;
    ptkWebAuto: //Web Auto (Cabe�alho do e-mail)
      Result := 5;
  end;
end;

function TUnProtocolo.GeraProtocoloBoleto(Quais: TSelType; QueryIts, QueryUpd,
  QueryLoc: TmySQLQuery; Database: TmySQLDatabase; Grade: TDBGrid;
  Progress: TProgressBar; EntCliInt: Integer): Boolean;

  function ObtemLoteProtocolo(Protocolo, ProtocoPak, CNAN_Cfg,
    TipoProt: Integer): Integer;
  var
    Agora: TDateTime;
    Lote: Integer;
    Mes: String;
  begin
    Lote := LocalizaLote(ProtocoPak, QueryLoc, Database);
    //
    if Lote = 0 then
    begin
      Agora := DModG.ObtemAgora();
      Mes   := Geral.DataToMez(Agora);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
        'SELECT Controle ',
        'FROM protocopak ',
        'WHERE Codigo=' + Geral.FF0(Protocolo),
        'AND Mez=' + Mes,
        'AND SeqArq = 0 ',
        '']);
      if QueryLoc.RecordCount > 0 then
        Lote := QueryLoc.FieldByName('Controle').AsInteger
      else
        Lote := GeraLoteProtocolo(DataBase, Protocolo, CNAN_Cfg, TipoProt);
    end;
    //
    Result := Lote;
  end;

var
  i, Lote, ProtocoPak, CNAB_Cfg, TipoProt, Protocol: Integer;
  ProtocoloCR: Boolean;
  RecipEmeio: String;
begin
  Result := False;
  //
  if (QueryIts.State <> dsInactive) and (QueryIts.RecordCount > 0) then
  begin
    Progress.Position := 0;
    //
    case Quais of
      istSelecionados:
      begin
        if Grade.SelectedRows.Count > 1 then
        begin
          try
            QueryIts.DisableControls;
            Grade.Enabled := False;
            Progress.Max  := Grade.SelectedRows.Count;
            //
            with Grade.DataSource.DataSet do
            begin
              for i := 0 to Grade.SelectedRows.Count-1 do
              begin
                GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
                //
                Progress.Position := Progress.Position + 1;
                Progress.Update;
                Application.ProcessMessages;
                //
                Protocol   := QueryIts.FieldByName('PROTOCOD').AsInteger;
                ProtocoPak := QueryIts.FieldByName('ProtocoPak').AsInteger;
                CNAB_Cfg   := QueryIts.FieldByName('CNAB_Cfg').AsInteger;
                TipoProt   := QueryIts.FieldByName('TipoProt').AsInteger;
                Lote       := ObtemLoteProtocolo(Protocol, ProtocoPak, CNAB_Cfg, TipoProt);
                RecipEmeio := '';
                //
                GeraProtocolo_BloqProto(RecipEmeio, QueryIts, QueryLoc,
                  QueryUpd, Database, Lote, EntCliInt, TipoProt);
              end;
            end;
          finally
            Progress.Position := 0;
            Grade.Enabled     := True;
            QueryIts.EnableControls;
            //
            Result := True;
          end;
        end else
        begin
          Protocol   := QueryIts.FieldByName('PROTOCOD').AsInteger;
          ProtocoPak := QueryIts.FieldByName('ProtocoPak').AsInteger;
          CNAB_Cfg   := QueryIts.FieldByName('CNAB_Cfg').AsInteger;
          TipoProt   := QueryIts.FieldByName('TipoProt').AsInteger;
          Lote       := ObtemLoteProtocolo(Protocol, ProtocoPak, CNAB_Cfg, TipoProt);
          RecipEmeio := '';
          //
          GeraProtocolo_BloqProto(RecipEmeio, QueryIts, QueryLoc,
            QueryUpd, Database, Lote, EntCliInt, TipoProt);
          //
          Result := True;
        end;
      end;
      istTodos:
      begin
        try
          QueryIts.DisableControls;
          QueryIts.First;
          Grade.Enabled := False;
          Progress.Max  := QueryIts.RecordCount;
          //
          while not QueryIts.Eof do
          begin
            Progress.Position := Progress.Position + 1;
            Progress.Update;
            Application.ProcessMessages;
            //
            Protocol   := QueryIts.FieldByName('PROTOCOD').AsInteger;
            ProtocoPak := QueryIts.FieldByName('ProtocoPak').AsInteger;
            CNAB_Cfg   := QueryIts.FieldByName('CNAB_Cfg').AsInteger;
            TipoProt   := QueryIts.FieldByName('TipoProt').AsInteger;
            Lote       := ObtemLoteProtocolo(Protocol, ProtocoPak, CNAB_Cfg, TipoProt);
            RecipEmeio := '';
            //
            GeraProtocolo_BloqProto(RecipEmeio, QueryIts, QueryLoc,
              QueryUpd, Database, Lote, EntCliInt, TipoProt);
            //
            QueryIts.Next;
          end;
        finally
          Progress.Position := 0;
          Grade.Enabled     := True;
          QueryIts.EnableControls;
          //
          Result := True;
        end;
      end;
      else
      begin
        Geral.MB_Aviso('Modo se sele��o n�o implementado!');
        Result := False;
        Exit;
      end;
    end;
  end else
    Result := True;
end;

function TUnProtocolo.GeraProtocoloNFSe(Quais: TSelType; QueryIts: TmySQLQuery;
  Database: TmySQLDatabase; Grade: TDBGrid; Progress: TProgressBar;
  EntCliInt: Integer): Boolean;

  function ObtemLoteProtocolo(Protocolo, ProtocoPak, CNAN_Cfg,
    TipoProt: Integer; QueryLoc, QueryUpd: TmySQLQuery): Integer;
  var
    Agora: TDateTime;
    Lote: Integer;
    Mes: String;
  begin
    Lote := LocalizaLote(ProtocoPak, QueryLoc, Database);
    //
    if Lote = 0 then
    begin
      Agora := DModG.ObtemAgora();
      Mes   := Geral.DataToMez(Agora);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
        'SELECT Controle ',
        'FROM protocopak ',
        'WHERE Codigo=' + Geral.FF0(Protocolo),
        'AND Mez=' + Mes,
        'AND SeqArq = 0 ',
        '']);
      if QueryLoc.RecordCount > 0 then
        Lote := QueryLoc.FieldByName('Controle').AsInteger
      else
        Lote := GeraLoteProtocolo(Database, Protocolo, CNAN_Cfg, TipoProt);
    end;
    //
    Result := Lote;
  end;

var
  i, Lote, ProtocoPak, TipoProt, Protocol: Integer;
  ProtocoloCR: Boolean;
  RecipEmeio: String;
  QueryLoc, QueryUpd: TmySQLQuery;
begin
  Result := False;
  //
  if (QueryIts.State <> dsInactive) and (QueryIts.RecordCount > 0) then
  begin
    Progress.Position := 0;
    QueryLoc          := TmySQLQuery.Create(TDataModule(Database.Owner));
    QueryUpd          := TmySQLQuery.Create(TDataModule(Database.Owner));
    //
    try
      case Quais of
        istSelecionados:
        begin
          if Grade.SelectedRows.Count > 1 then
          begin
            try
              QueryIts.DisableControls;
              Grade.Enabled := False;
              Progress.Max  := Grade.SelectedRows.Count;
              //
              with Grade.DataSource.DataSet do
              begin
                for i := 0 to Grade.SelectedRows.Count-1 do
                begin
                  GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
                  //
                  Progress.Position := Progress.Position + 1;
                  Progress.Update;
                  Application.ProcessMessages;
                  //
                  Protocol   := QueryIts.FieldByName('PROTOCOD').AsInteger;
                  ProtocoPak := QueryIts.FieldByName('ProtocoPak').AsInteger;
                  TipoProt   := QueryIts.FieldByName('TipoProt').AsInteger;
                  RecipEmeio := '';
                  Lote       := ObtemLoteProtocolo(Protocol, ProtocoPak, 0,
                                  TipoProt, QueryLoc, QueryUpd);
                  //
                  GeraProtocolo_NFSeProto(RecipEmeio, QueryIts, Database, Lote,
                    EntCliInt, TipoProt);
                end;
              end;
            finally
              Progress.Position := 0;
              Grade.Enabled     := True;
              QueryIts.EnableControls;
              //
              Result := True;
            end;
          end else
          begin
            Protocol   := QueryIts.FieldByName('PROTOCOD').AsInteger;
            ProtocoPak := QueryIts.FieldByName('ProtocoPak').AsInteger;
            TipoProt   := QueryIts.FieldByName('TipoProt').AsInteger;
            RecipEmeio := '';
            Lote       := ObtemLoteProtocolo(Protocol, ProtocoPak, 0, TipoProt,
                            QueryLoc, QueryUpd);
            //
            GeraProtocolo_NFSeProto(RecipEmeio, QueryIts, Database, Lote,
              EntCliInt, TipoProt);
            //
            Result := True;
          end;
        end;
        istTodos:
        begin
          try
            QueryIts.DisableControls;
            Grade.Enabled := False;
            Progress.Max  := QueryIts.RecordCount;
            //
            QueryIts.First;
            //
            while not QueryIts.Eof do
            begin
              Progress.Position := Progress.Position + 1;
              Progress.Update;
              Application.ProcessMessages;
              //
              Protocol   := QueryIts.FieldByName('PROTOCOD').AsInteger;
              ProtocoPak := QueryIts.FieldByName('ProtocoPak').AsInteger;
              TipoProt   := QueryIts.FieldByName('TipoProt').AsInteger;
              RecipEmeio := '';
              Lote       := ObtemLoteProtocolo(Protocol, ProtocoPak, 0,
                              TipoProt, QueryLoc, QueryUpd);
              //
              GeraProtocolo_NFSeProto(RecipEmeio, QueryIts, Database, Lote,
                EntCliInt, TipoProt);
              //
              QueryIts.Next;
            end;
          finally
            Progress.Position := 0;
            Grade.Enabled     := True;
            QueryIts.EnableControls;
            //
            Result := True;
          end;
        end;
        else
        begin
          Geral.MB_Aviso('Modo se sele��o n�o implementado!');
          Result := False;
          Exit;
        end;
      end;
    finally
      QueryLoc.Free;
      QueryUpd.Free;
    end;
  end else
    Result := True;
end;

function TUnProtocolo.NomeDoTipoDeProtocolo(Tipo: Integer): String;
begin
  case Tipo of
    VAR_TIPO_PROTOCOLO_ND_00: Result := 'ND - N�o definido';
    VAR_TIPO_PROTOCOLO_EB_01: Result := 'EB - Entrega de Bloquetos';
    VAR_TIPO_PROTOCOLO_CE_02: Result := 'CE - Circula��o eletr�nica (emeio)';
    VAR_TIPO_PROTOCOLO_EM_03: Result := 'EM - Entrega de material de consumo';
    VAR_TIPO_PROTOCOLO_CR_04: Result := 'CR - Cobran�a registrada (banc�ria)';
    VAR_TIPO_PROTOCOLO_CD_05: Result := 'CD - Circula��o de documentos';
    else Result := '? ? ? ? ?';
  end;
end;

function TUnProtocolo.ObtemListaProEnPrFinalidade: TStringList;
var
  Finalidade: TStringList;
begin
  Finalidade := TStringList.Create;
  try
    Finalidade.Add('Envio de boletos');
    Finalidade.Add('Envio de NFS-es autorizadas');
    Finalidade.Add('Envio de NFS-es canceladas');
  finally
    Result := Finalidade;
  end;
end;

procedure TUnProtocolo.ExcluiLoteProtocoloBoleto(QueryLoc, QueryUpd: TmySQLQuery;
  Database: TmySQLDatabase; Arreits: Integer; Tabela: String);

  procedure ExcluiLoteSeVazio(ProtocoPak: Integer);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
      'SELECT * ',
      'FROM protpakits ',
      'WHERE Controle=' + Geral.FF0(ProtocoPak),
      '']);
    if QueryLoc.RecordCount = 0 then
      UMyMod.ExcluiRegistroInt1('', 'protocopak', 'Controle', ProtocoPak, Database);
  end;

var
  ProtocoPakCR, ProtocoPak: Integer;
  Boleto: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
    'SELECT ProtocoPakCR, ProtocoPak, Boleto ',
    'FROM ' + Tabela,
    'WHERE Controle=' + Geral.FF0(Arreits),
    '']);
  ProtocoPakCR := QueryLoc.FieldByName('ProtocoPakCR').AsInteger;
  ProtocoPak   := QueryLoc.FieldByName('ProtocoPak').AsInteger;
  Boleto       := QueryLoc.FieldByName('Boleto').AsFloat;
  //
  if ProtocoPakCR <> 0 then
  begin
    ExcluiLoteSeVazio(ProtocoPakCR);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, Database, [
      'DELETE FROM protpakits ',
      'WHERE Controle=' + Geral.FF0(ProtocoPakCR),
      'AND Docum=' + Geral.FFI(Boleto),
      '']);
  end;
  if ProtocoPak <> 0 then
  begin
    ExcluiLoteSeVazio(ProtocoPak);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, Database, [
      'DELETE FROM protpakits ',
      'WHERE Controle=' + Geral.FF0(ProtocoPak),
      'AND Docum=' + Geral.FFI(Boleto),
      '']);
  end;
end;

procedure TUnProtocolo.ExcluiLoteProtocoloNFSe(QueryLoc, QueryUpd: TmySQLQuery;
  Database: TmySQLDatabase; CliInt, Ambiente, EntiMail, ProtocoPak,
  RpsIDNumero: Integer; RpsIDSerie: String);

  procedure ExcluiLoteSeVazio(ProtocoPak: Integer);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
      'SELECT * ',
      'FROM protpakits ',
      'WHERE Controle=' + Geral.FF0(ProtocoPak),
      '']);
    if QueryLoc.RecordCount = 0 then
      UMyMod.ExcluiRegistroInt1('', 'protocopak', 'Controle', ProtocoPak, Database);
  end;

begin
  if ProtocoPak <> 0 then
  begin
    ExcluiLoteSeVazio(ProtocoPak);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, Database, [
      'DELETE FROM protpakits ',
      'WHERE Controle=' + Geral.FF0(ProtocoPak),
      'AND ID_Cod1=' + Geral.FFI(Ambiente),
      'AND ID_Cod2="' + RpsIDSerie + '"',
      'AND ID_Cod3=' + Geral.FFI(RpsIDNumero),
      'AND ID_Cod4=' + Geral.FFI(EntiMail),
      'AND CliInt=' + Geral.FFI(CliInt),
      '']);
  end;
end;

function TUnProtocolo.GeraLoteProtocolo(Database: TmySQLDatabase; Protocolo,
  CNAB_Cfg, TipoProt: Integer): Integer;
var
  Controle: Integer;
  DataI, DataL, Mes: String;
  Agora: TDateTime;
  QueryUpd: TmySQLQuery;
begin
  QueryUpd          := TmySQLQuery.Create(TDataModule(Database.Owner));
  QueryUpd.DataBase := Database;
  Result            := 0;
  //
  if (TipoProt = 4) and (CNAB_Cfg = 0) then //CR
    Exit;
  //
  try
    Agora    := DModG.ObtemAgora();
    Controle := UMyMod.BuscaEmLivreY_Def('protocopak', 'Controle', stIns, 0);
    DataI    := Geral.FDT(Agora, 01);
    DataL    := Geral.FDT(Agora, 01);
    Mes      := Geral.DataToMez(Agora);
    //
    if (Controle > 0) and (Protocolo > 0 ) then
    begin
      if UMyMod.SQLInsUpd(QueryUpd, stIns, 'protocopak', False,
        ['DataI', 'DataL', 'Mez', 'Codigo', 'CNAB_Cfg'], ['Controle'],
        [DataI, DataL, Mes, Protocolo, CNAB_Cfg], [Controle], True)
      then
        Result := Controle;
    end;
  finally
    QueryUpd.Free;
  end;
end;

procedure TUnProtocolo.ProtocolosCD_Doc(Quais: TSelType; DBGLct: TDBGrid;
ModuleLctX: TDataModule; CliInt: Integer; FormOrigem: String);
var
  QrSource: TmySQLQuery;
  I: Integer;
  LctProto: String;
  procedure IncluiAtual();
  var
    Data, SerieCH, Vencimento: String;
    Tipo, Carteira, Sub, CliInt, Cliente, Fornece: Integer;
    Controle, Documento, Credito, Debito: Double;
  begin
    Data           := Geral.FDT(QrSource.FieldByName('Data').AsDateTime, 1);
    Tipo           := QrSource.FieldByName('Tipo'      ).AsInteger;
    Carteira       := QrSource.FieldByName('Carteira'  ).AsInteger;
    Controle       := QrSource.FieldByName('Controle'  ).AsInteger;
    Sub            := QrSource.FieldByName('Sub'       ).AsInteger;
    CliInt         := QrSource.FieldByName('CliInt'    ).AsInteger;
    Cliente        := QrSource.FieldByName('Cliente'   ).AsInteger;
    Fornece        := QrSource.FieldByName('Fornecedor').AsInteger;
    Documento      := QrSource.FieldByName('Documento' ).AsFloat;
    Credito        := QrSource.FieldByName('Credito'   ).AsFloat;
    Debito         := QrSource.FieldByName('Debito'    ).AsFloat;
    SerieCH        := QrSource.FieldByName('SerieCH'   ).AsString;
    Vencimento     := Geral.FDT(QrSource.FieldByName('Vencimento').AsDateTime, 1);
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, LctProto, False, [
    'CliInt', 'Cliente', 'Fornecedor',
    'Documento', 'Credito', 'Debito',
    'SerieCH', 'Vencimento'], [
    'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
    CliInt, Cliente, Fornece,
    Documento, Credito, Debito,
    SerieCH, Vencimento], [
    Data, Tipo, Carteira, Controle, Sub], False);
    //
  end;
var
  Carteira, Controle: Integer;
begin
  LctProto := CriarProtoco.RecriaTempTableNovo(UnCreateProtoco.ntrtt_LctProto, DmodG.QrUpdPID1, False);
  if MyObjects.FIC(DBGLct = nil, nil, 'DBGLct indefinido em UnProtocolo!') then
    Exit;
  QrSource := TmySQLQuery(DBGLct.DataSource.DataSet);
  if MyObjects.FIC(QrSource = nil, nil, 'DataSet indefinido em UnProtocolo!') then
    Exit;
  //
  if Quais = istTodos then
  begin
    if Geral.MB_Pergunta('Gera protocolo para todos itens?') = ID_YES then
    begin
      //
      QrSource.First;
      while not QrSource.Eof do
      begin
        IncluiAtual();
        QrSource.Next;
      end;
      //
    end;
  end else if Quais = istSelecionados then
  begin
    if DBGLct.SelectedRows.Count > 1 then
    begin
      with DBGLct.DataSource.DataSet do
      for I := 0 to DBGLct.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGLct.SelectedRows.Items[I]));
        IncluiAtual();
      end;
    end else IncluiAtual();
  end else if Quais = istAtual then IncluiAtual()
  else
  begin
    Geral.MB_Erro('SelType indefinido em UnProtocolo!');
    //
    Exit;
  end;
  //
  {$IFDEF DEFINE_VARLCT}
  TDmLct2(ModuleLctX).QrLcP.Close;
  TDmLct2(ModuleLctX).QrLcP.Database := DModG.MyPID_DB;
  UnDmkDAC_PF.AbreMySQLQuery0(TDmLct2(ModuleLctX).QrLcP, DModG.MyPID_DB, [
  'SELECT Data, Tipo, Carteira, Controle, ',
  'Sub, CliInt, Cliente, Fornecedor, ',
  'Documento, SUM(Credito) Credito, SUM(Debito) ',
  'Debito, SerieCH, Vencimento, COUNT(Ativo) ITENS ',
  'FROM ' + LctProto,
  'GROUP BY Data, Tipo, Carteira, CliInt, ',
  'SerieCH, Documento ',
  '']);
  //
  if TDmLct2(ModuleLctX).QrCrt.State = dsInactive then Carteira := 0
  else Carteira := TDmLct2(ModuleLctX).QrCrtCodigo.Value;
  //
  if DBCheck.CriaFm(TFmProSel, FmProSel, afmoNegarComAviso) then
  begin
    {
    FmProSel.QrProtocolos.Close;
    FmProSel.QrProtocolos.Params[0].AsInteger := VAR_TIPO_PROTOCOLO_CD_05; // Circula��o de documentos
    FmProSel.QrProtocolos. O p e n ;
    }
    FmProSel.ReopenProtocolos(VAR_TIPO_PROTOCOLO_CD_05, 0, CliInt);
    //
    //FmProSel.FQuais := Quais;
    // Muda para todos pois foi criada uma tabela para o(s) selecionado(s)
    FmProSel.FQuais      := istTodos;
    FmProSel.FNomeQrSrc  := 'TDmLct2(ModuleLctX).QrLcP';
    FmProSel.FQrSource   := TDmLct2(ModuleLctX).QrLcP;
    FmProSel.FQrLci      := TDmLct2(ModuleLctX).QrLcI;
    FmProSel.FDBGrid     := DBGLct;
    //FmProSel.FTabLctA    := TDmLct2(ModuleLctX).FTabLctA;
    //FmProSel.FLctProto   := LctProto;
    FmProSel.FAskTodos   := False;
    //
    FmProSel.ShowModal;
    FmProSel.Destroy;
    //
    Controle := TDmLct2(ModuleLctX).QrLctControle.Value;
    //
    TDmLct2(ModuleLctX).ReabreCarteiras(Carteira, TDmLct2(ModuleLctX).QrCrt, TDmLct2(ModuleLctX).QrCrtSum,
      'TFmCondGer.ProtocolosCD_Doc()');
    TDmLct2(ModuleLctX).QrLct.Locate('Controle', Controle, []);
  end;
  {$ENDIF}
end;

procedure TUnProtocolo.ProtocolosCD_Lct(Quais: TSelType; DBGLct: TDBGrid;
  ModuleLctX: TDataModule; CliInt: Integer; FormOrigem: String);
var
  Controle: Integer;
  Carteira: Integer;
begin
  {$IFDEF DEFINE_VARLCT}
  if TDmLct2(ModuleLctX).QrCrt.State = dsInactive then Carteira := 0
  else Carteira := TDmLct2(ModuleLctX).QrCrtCodigo.Value;
  //
  if DBCheck.CriaFm(TFmProSel, FmProSel, afmoNegarComAviso) then
  begin
    {
    FmProSel.QrProtocolos.Close;
    FmProSel.QrProtocolos.Params[0].AsInteger := VAR_TIPO_PROTOCOLO_CD_05; // Circula��o de documentos
    FmProSel.QrProtocolos . O p e n ;
    }
    FmProSel.ReopenProtocolos(VAR_TIPO_PROTOCOLO_CD_05, 0, CliInt);
    //
    FmProSel.FQuais      := Quais;
    FmProSel.FNomeQrSrc  := 'TDmLct2(ModuleLctX).QrLct';
    FmProSel.FQrSource   := TDmLct2(ModuleLctX).QrLct;
    //FmProSel.FDBGrid     := TDBGrid(DBGLct);
    //FmProSel.FTabLctA    := TDmLct2(ModuleLctX).FTabLctA;
    //
    FmProSel.ShowModal;
    FmProSel.Destroy;
    //
    Controle := TDmLct2(ModuleLctX).QrLctControle.Value;
    //
    TDmLct2(ModuleLctX).ReabreCarteiras(Carteira, TDmLct2(ModuleLctX).QrCrt, TDmLct2(ModuleLctX).QrCrtSum,
      'TFmCondGer.ProtocolosCD_Lct()');
    TDmLct2(ModuleLctX).QrLct.Locate('Controle', Controle, []);
  end;
  {$ENDIF}
end;

function TUnProtocolo.ProtocolosCD_OS1(Quais: TSelType; DBGOScab: TDBGrid;
  QrOSCab: TmySQLQuery; CliInt: Integer): Integer;
(*
var
  Controle: Integer;
  Carteira: Integer;
*)
begin
{$IfNDef S_BLQ}
(*
  if TDmLct2(ModuleLctX).QrCrt.State = dsInactive then Carteira := 0
  else Carteira := TDmLct2(ModuleLctX).QrCrtCodigo.Value;
  //
*)
  Result := 0;
  //
  if DBCheck.CriaFm(TFmProSel, FmProSel, afmoNegarComAviso) then
  begin
    {
    FmProSel.QrProtocolos.Close;
    FmProSel.QrProtocolos.Params[0].AsInteger := VAR_TIPO_PROTOCOLO_CD_05; // Circula��o de documentos
    FmProSel.QrProtocolos . O p e n ;
    }
    FmProSel.ReopenProtocolos(VAR_TIPO_PROTOCOLO_CD_05, 0, CliInt);
    //
    FmProSel.FQuais      := Quais;
    FmProSel.FNomeQrSrc  := 'QrOSCab';
    FmProSel.FQrSource   := QrOSCab;
    FmProSel.FDBGrid     := TDBGrid(DBGOSCab);
    //FmProSel.FTabLctA    := '';
    //
    FmProSel.ShowModal;
    Result := FmProSel.FLote;
    FmProSel.Destroy;
    //
(*
    Controle := TDmLct2(ModuleLctX).QrLctControle.Value;
    //
    TDmLct2(ModuleLctX).ReabreCarteiras(Carteira, TDmLct2(ModuleLctX).QrCrt, TDmLct2(ModuleLctX).QrCrtSum,
      'TFmCondGer.ProtocolosCD_Lct()');
    TDmLct2(ModuleLctX).QrLct.Locate('Controle', Controle, []);
*)
  end;
{$EndIf}
end;

function TUnProtocolo.Proto_Email_AtualizDataE(DataBase: TmySQLDatabase;
  Protocolo: Integer; Agora: TDateTime): Boolean;
{$IfDef TEM_DBWEB}
var
  Query: TmySQLQuery;
begin
  Result := False;
  Query  := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    if UnDmkDAC_PF.ExecutaMySQLQuery0(Query, DataBase, [
      'UPDATE protpakits SET AlterWeb=1, ',
      'DataE="' + Geral.FDT(Agora, 1) + '"',
      'WHERE Conta=' + Geral.FF0(Protocolo),
      ''])
    then
      Result := True;
  finally

  end;
{$Else}
begin
  Result := True; //Sem DBWeb n�o faz nada
{$EndIf}
end;

function TUnProtocolo.Proto_Email_CriaProtocoloEnvio(DataBase: TmySQLDatabase;
  Cliente, Protocolo: Integer; Email: String): Boolean;
{$IfDef TEM_DBWEB}
var
  Query: TmySQLQuery;
begin
    Result := False;
    Query  := TmySQLQuery.Create(TDataModule(DataBase.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
        'SELECT * ',
        'FROM wpropaits ',
        'WHERE Cliente=' + Geral.FF0(Cliente),
        'AND Protocolo=' + Geral.FF0(Protocolo),
        'AND Email="' + Email + '"',
        '']);
      if Query.RecordCount = 0 then
      begin
        //Registra no servidor que vai mandar o e-mail
        UnDmkDAC_PF.ExecutaMySQLQuery0(Query, DataBase, [
          'INSERT INTO wpropaits SET ',
          'Cliente=' + Geral.FF0(Cliente) + ', ',
          'Protocolo=' + Geral.FF0(Protocolo) + ', ',
          'Email="' + Email + '"',
          '']);
      end;
      Result := True;
    finally
      Query.Free;
    end;
{$Else}
begin
    Result := True; //Sem DBWeb n�o faz nada
{$EndIf}
end;

procedure TUnProtocolo.Proto_Email_GeraLinksConfirmacaoReceb(
  const DataBase: TmySQLDatabase; const Web_MyURL, Email: String;
  const Cliente, Protocolo, DMKID_APP: Integer; var ParamLink, ParamTopo: String);
{$IfDef TEM_DBWEB}
const
  URL_Site = 'http://www.dermatek.net.br';
var
  Query: TmySQLQuery;
  ComoConfL, ComoConfT, Mail, Cli, Prot, IDWeb: String;
begin
    ComoConfL := Geral.FF0(UnProtocolo.IntOfComoConf(ptkWebLink));
    ComoConfT := Geral.FF0(UnProtocolo.IntOfComoConf(ptkWebAuto));

    if DMKID_APP = 17 then //DControl
      IDWeb := '&idweb=dmk'
    else if DMKID_APP = 4 then //Syndi2
      IDWeb := '&idweb=syndinet'
    else
      IDWeb := '';

    Query := TmySQLQuery.Create(TDataModule(DataBase.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
        'SELECT ',
        'MD5("'+ Email +'") Email, ',
        'MD5("'+ Geral.FF0(Cliente) +'") Cliente, ',
        'MD5("'+ Geral.FF0(Protocolo) +'") Protocolo, ',
        'MD5("'+ ComoConfL +'") ComoConfL, ',
        'MD5("'+ ComoConfT +'") ComoConfT ',
        '']);

      ComoConfL := Query.FieldByName('ComoConfL').AsString;
      ComoConfT := Query.FieldByName('ComoConfT').AsString;
      Mail      := Query.FieldByName('Email').AsString;
      Cli       := Query.FieldByName('Cliente').AsString;
      Prot      := Query.FieldByName('Protocolo').AsString;

      ParamLink := Web_MyURL + '?page=recebibloq&comoconf=' + ComoConfL +
                     '&email=' + Mail + '&cliente=' + Cli + '&protocolo=' +
                     Prot;

      ParamTopo := '<img src=''' + URL_Site +
                     '/complementos/com_emailconfrecebi.php?comoconf=' +
                     ComoConfT + '&email=' + Mail + '&cliente=' + Cli +
                     '&protocolo=' + Prot + IDWeb + ''' width=''1'' height=''1''/>';
    finally
      Query.Free;
    end;
{$Else}
begin
    ParamLink := 'N�o esque�a de enviar a confirma��o de recebimento!';
    ParamTopo := '';
{$EndIf}
end;

function TUnProtocolo.RemoveProEnPrItsDuplicados(DataBase: TmySQLDatabase;
  TiposProt: String; Finalidade: TProtFinalidade; CliInt, Entidade,
  ProEnPr: Integer): Boolean;
var
  Query: TMySQLQuery;
begin
  Result := False;
  //
  if Finalidade = ptkBoleto then
  begin
    Query := TmySQLQuery.Create(TDataModule(DataBase.Owner));
    try
      Result := UnDmkDAC_PF.ExecutaMySQLQuery0(Query, DataBase, [
        'DELETE proenprit ',
        'FROM proenprit ',
        'LEFT JOIN proenpr ON proenpr.Codigo = proenprit.Codigo ',
        'LEFT JOIN protocolos ON protocolos.Codigo = proenpr .Protocolo ',
        'WHERE protocolos.Def_Client=' + Geral.FF0(CliInt),
        'AND proenprit.Entidade=' + Geral.FF0(Entidade),
        'AND protocolos.Tipo IN (' + TiposProt + ') ',
        'AND proenpr.Finalidade = 0 ', //Boletos
        'AND proenprit.Codigo <>' + Geral.FF0(ProEnPr),
        '']);
    finally
      Query.Free;
    end;
  end;
end;

end.
