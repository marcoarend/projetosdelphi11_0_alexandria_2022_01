unit ProtocoMan;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, DmkDAC_PF,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, ComCtrls,
  dmkEditDateTimePicker, Mask, dmkDBEdit, dmkGeral, dmkLabel, dmkRadioGroup,
  dmkImage, UnDmkEnums;

type
  TFmProtocoMan = class(TForm)
    Panel1: TPanel;
    EdManual: TdmkEditCB;
    CBManual: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrProtocoOco: TmySQLQuery;
    DsProtocoOco: TDataSource;
    QrProtocoOcoCodigo: TIntegerField;
    QrProtocoOcoNome: TWideStringField;
    TPDataE: TdmkEditDateTimePicker;
    Label2: TLabel;
    Label3: TLabel;
    EdPeriodo: TdmkEdit;
    Label4: TLabel;
    EdDepto: TdmkEditCB;
    LaDepto: TLabel;
    CBDepto: TdmkDBLookupComboBox;
    EdSerieCH: TdmkEdit;
    EdDoc: TdmkEdit;
    LaDoc: TLabel;
    Panel3: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    DBEdit1: TDBEdit;
    EdConta: TdmkEdit;
    Label9: TLabel;
    EdPeriodo_txt: TdmkEdit;
    QrCondImov: TmySQLQuery;
    DsCondImov: TDataSource;
    QrCondImovConta: TIntegerField;
    QrCondImovBLOCO: TWideStringField;
    QrCondImovUnidade: TWideStringField;
    CkContinuar: TCheckBox;
    dmkEdit1: TdmkEdit;
    dmkRadioGroup1: TdmkRadioGroup;
    DBEdit2: TDBEdit;
    Label10: TLabel;
    EdCliInt: TdmkEdit;
    GroupBox1: TGroupBox;
    LaLimiteSai: TLabel;
    LaLimiteRem: TLabel;
    LaLimiteRet: TLabel;
    TPLimiteSai: TdmkEditDateTimePicker;
    TPLimiteRem: TdmkEditDateTimePicker;
    TPLimiteRet: TdmkEditDateTimePicker;
    SpeedButton5: TSpeedButton;
    EdValor: TdmkEdit;
    Label5: TLabel;
    TPVencto: TdmkEditDateTimePicker;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel5: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPeriodoChange(Sender: TObject);
    procedure dmkRadioGroup1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmProtocoMan: TFmProtocoMan;

implementation

uses UnMyObjects, Module, Protocolos, UMySQLModule, UnInternalConsts, MyDBCheck,
  ProtocoOco;

{$R *.DFM}

procedure TFmProtocoMan.BtOKClick(Sender: TObject);
var
  Conta: Integer;
begin
  if Geral.IMV(EdManual.Text) = 0 then
  begin
    Geral.MB_Aviso('A ocorrência manual deve ser diferente de zero!');
    EdManual.SetFocus;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    Conta := UMyMod.BuscaEmLivreY_Def('protpakits', 'Conta', ImgTipo.SQLType,
      EdConta.ValueVariant);
    EdConta.ValueVariant := Conta;
    if UMyMod.ExecSQLInsUpdFm(FmProtocoMan, ImgTipo.SQLType, 'protpakits',
    Conta, Dmod.QrUpd) then
    begin
      FmProtocolos.ReopenProtPakIts(Conta);
      if CkContinuar.Checked then
      begin
        EdConta.ValueVariant := 0;
        ImgTipo.SQLType := stIns;
        EdManual.SetFocus;
      end else
        Close;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmProtocoMan.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProtocoMan.dmkRadioGroup1Click(Sender: TObject);
begin
  LaLimiteRet.Visible := dmkRadioGroup1.ItemIndex = 1;
  TPLimiteRet.Visible := dmkRadioGroup1.ItemIndex = 1;
end;

procedure TFmProtocoMan.EdPeriodoChange(Sender: TObject);
begin
  EdPeriodo_txt.Text := Uppercase(Geral.FDT(Geral.PeriodoToDate(
    EdPeriodo.ValueVariant, 1, False), 7));
end;

procedure TFmProtocoMan.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProtocoMan.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  UnDmkDAC_PF.AbreQuery(QrProtocoOco, Dmod.MyDB);
  if VAR_KIND_DEPTO = kdUH then
  //if UpperCase(Application.Title) = 'SYNDIC' then
  begin
    QrCondImov.Close;
    QrCondImov.Params[0].AsInteger := FmProtocolos.QrProtocolosDef_Client.Value;
    UnDmkDAC_PF.AbreQuery(QrCondImov, Dmod.MyDB);
    //
    LaDepto.Visible    := True;
    EdDepto.Visible    := True;
    CBDepto.Visible    := True;
    CBDepto.ListSource := DsCondImov;
  end else
  begin
    LaDepto.Visible    := False;
    EdDepto.Visible    := False;
    CBDepto.Visible    := False;
    CBDepto.ListSource := nil;
  end;
  EdCliInt.ValueVariant  := FmProtocolos.QrProtocolosDef_Client.Value;
  EdPeriodo.ValueVariant := Geral.Periodo2000(Date);
  TPDataE.Date := Date;
  TPLimiteSai.Date := Date + 1;
  TPLimiteRem.Date := Date + 1;
  TPLimiteRet.Date := Date + 4;
  TPVencto.Date    := Date;
end;

procedure TFmProtocoMan.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProtocoMan.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmProtocoOco, FmProtocoOco, afmoNegarComAviso) then
  begin
    FmProtocoOco.ShowModal;
    FmProtocoOco.Destroy;
    //
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodigoPesquisado(EdManual, CBManual, QrProtocoOco, VAR_CADASTRO);
  end;
end;

end.
