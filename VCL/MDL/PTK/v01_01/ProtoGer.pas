unit ProtoGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGrid, Mask, dmkValUsu, dmkPermissoes, UnDmkEnums,
  dmkCompoStore, DmkDAC_PF, UnDmkProcFunc, VCLTee.TeEngine, VCLTee.TeeProcs,
  VCLTee.Chart, VCLTee.DBChart, VCLTee.Series, Protocolo;

type
  TMosta = (gerSta=0, gerGer=1);
  TFmProtoGer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    QrEmailProt: TmySQLQuery;
    QrEmailProtCodigo: TIntegerField;
    QrEmailProtNome: TWideStringField;
    QrEmailProtITENS: TLargeintField;
    DsEmailProt: TDataSource;
    QrEmailProtPak: TmySQLQuery;
    QrEmailProtPakControle: TIntegerField;
    QrEmailProtPakMez: TIntegerField;
    QrEmailProtPakNOMEMEZ: TWideStringField;
    QrEmailProtPakITENS: TLargeintField;
    DsEmailProtPak: TDataSource;
    QrEmailProtPakIts: TmySQLQuery;
    QrEmailProtPakItsConta: TIntegerField;
    QrEmailProtPakItsDataE: TDateField;
    QrEmailProtPakItsCliente: TIntegerField;
    QrEmailProtPakItsDepto: TIntegerField;
    QrEmailProtPakItsDocum: TFloatField;
    QrEmailProtPakItsNOMECLI: TWideStringField;
    QrEmailProtPakItsDATAE_TXT: TWideStringField;
    QrEmailProtPakItsID_Cod1: TIntegerField;
    QrEmailProtPakItsID_Cod2: TIntegerField;
    QrEmailProtPakItsID_Cod3: TIntegerField;
    QrEmailProtPakItsID_Cod4: TIntegerField;
    QrEmailProtPakItsVencto: TDateField;
    QrEmailProtPakItsLancto: TIntegerField;
    DsEmailProtPakIts: TDataSource;
    QrProtPg: TmySQLQuery;
    QrProtPgRegistros: TLargeintField;
    QrRecebiBloq: TmySQLQuery;
    QrReenv: TmySQLQuery;
    QrReenvNOMECLI: TWideStringField;
    QrReenvDATAE_TXT: TWideStringField;
    QrReenvCodigo: TIntegerField;
    QrReenvControle: TIntegerField;
    QrReenvConta: TIntegerField;
    QrReenvDataE: TDateField;
    QrReenvDataD: TDateTimeField;
    QrReenvRetorna: TSmallintField;
    QrReenvCancelado: TIntegerField;
    QrReenvMotivo: TIntegerField;
    QrReenvLink_ID: TIntegerField;
    QrReenvCliInt: TIntegerField;
    QrReenvCliente: TIntegerField;
    QrReenvFornece: TIntegerField;
    QrReenvPeriodo: TIntegerField;
    QrReenvLancto: TIntegerField;
    QrReenvDocum: TFloatField;
    QrReenvDepto: TIntegerField;
    QrReenvID_Cod1: TIntegerField;
    QrReenvID_Cod2: TIntegerField;
    QrReenvID_Cod3: TIntegerField;
    QrReenvID_Cod4: TIntegerField;
    QrReenvCedente: TIntegerField;
    QrReenvVencto: TDateField;
    QrReenvValor: TFloatField;
    QrReenvMoraDiaVal: TFloatField;
    QrReenvMultaVal: TFloatField;
    QrReenvComoConf: TSmallintField;
    QrReenvSerieCH: TWideStringField;
    QrReenvManual: TIntegerField;
    QrReenvTexto: TWideStringField;
    QrEntiMail: TmySQLQuery;
    QrPPIMail: TmySQLQuery;
    QrPPIMailPROTOCOLO: TIntegerField;
    QrPPIMailLOTE: TIntegerField;
    QrPPIMailDataE: TDateField;
    QrPPIMailDATAE_TXT: TWideStringField;
    QrPPIMailDATAD_TXT: TWideStringField;
    QrPPIMailCancelado: TIntegerField;
    QrPPIMailMotivo: TIntegerField;
    QrPPIMailID_Cod1: TIntegerField;
    QrPPIMailID_Cod2: TIntegerField;
    QrPPIMailID_Cod3: TIntegerField;
    QrPPIMailID_Cod4: TIntegerField;
    QrPPIMailCliInt: TIntegerField;
    QrPPIMailCliente: TIntegerField;
    QrPPIMailPeriodo: TIntegerField;
    QrPPIMailDef_Sender: TIntegerField;
    QrPPIMailDELIVER: TWideStringField;
    QrPPIMailDocum: TFloatField;
    QrPPIMailPreEmeio: TIntegerField;
    QrPPIMailDataD: TDateTimeField;
    QrPPIMailTAREFA_COD: TIntegerField;
    QrPPIMailTAREFA_NOM: TWideStringField;
    QrBloOpcoes: TmySQLQuery;
    QrEntiMailConta: TIntegerField;
    QrEntiMailEMail: TWideStringField;
    QrEntiMailNOMEENT: TWideStringField;
    dmkPermissoes1: TdmkPermissoes;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    PageControl2: TPageControl;
    TabSheet10: TTabSheet;
    Panel11: TPanel;
    Panel10: TPanel;
    BtAtzListaMailProt: TBitBtn;
    BtReenvia: TBitBtn;
    TabSheet12: TTabSheet;
    Panel14: TPanel;
    GradePTK: TdmkDBGrid;
    Panel15: TPanel;
    Panel16: TPanel;
    BitBtn17: TBitBtn;
    BitBtn18: TBitBtn;
    BitBtn19: TBitBtn;
    GradePPI: TDBGrid;
    CSTabSheetChamou: TdmkCompoStore;
    Panel5: TPanel;
    LaEmailProtTotal: TLabel;
    DBGEmailProt: TdmkDBGrid;
    Panel6: TPanel;
    DBGEmailProtPak: TdmkDBGrid;
    Splitter2: TSplitter;
    LaGEmailProtPakTotal: TLabel;
    Splitter1: TSplitter;
    Panel7: TPanel;
    DBGEmailProtPakIts: TdmkDBGrid;
    LaGEmailProtPakItsTotal: TLabel;
    BtProtocolos: TBitBtn;
    Panel17: TPanel;
    LaDef_Sender: TLabel;
    EdDef_Sender: TdmkEditCB;
    CBDef_Sender: TdmkDBLookupComboBox;
    RGProtoFiltro: TRadioGroup;
    QrPTK: TmySQLQuery;
    QrPTKNome: TWideStringField;
    QrPTKLote: TIntegerField;
    QrPTKMez: TIntegerField;
    DsPTK: TDataSource;
    QrPPI: TmySQLQuery;
    QrPPIDataE: TDateField;
    QrPPINO_RETORNA: TWideStringField;
    QrPPINO_Motivo: TWideStringField;
    QrPPILimiteSai: TDateField;
    QrPPISaiu: TIntegerField;
    QrPPIDataSai: TDateTimeField;
    QrPPILimiteRem: TDateField;
    QrPPIRecebeu: TIntegerField;
    QrPPIDataRec: TDateTimeField;
    QrPPILimiteRet: TDateField;
    QrPPIRetornou: TIntegerField;
    QrPPIDataRet: TDateTimeField;
    QrPPIConta: TIntegerField;
    QrPPIRetorna: TSmallintField;
    QrPPITexto: TWideStringField;
    QrPPILancto: TIntegerField;
    QrPPIDocum: TFloatField;
    DsPPI: TDataSource;
    QrSender: TmySQLQuery;
    QrSenderCodigo: TIntegerField;
    QrSenderNOMEENT: TWideStringField;
    DsSender: TDataSource;
    QrEmpresas: TmySQLQuery;
    QrEmpresasCodigo: TIntegerField;
    QrEmpresasFilial: TIntegerField;
    QrEmpresasNOMEFILIAL: TWideStringField;
    QrEmpresasCNPJ_CPF: TWideStringField;
    QrEmpresasIE: TWideStringField;
    QrEmpresasNIRE: TWideStringField;
    DsEmpresas: TDataSource;
    Splitter3: TSplitter;
    QrPTKMez_TXT: TWideStringField;
    QrPTKDef_Client: TIntegerField;
    QrPTKDef_Sender: TIntegerField;
    QrPPIDataSai_TXT: TWideStringField;
    QrPPIDataRec_TXT: TWideStringField;
    QrPPIDataRet_TXT: TWideStringField;
    LaGradePTK: TLabel;
    LaGradePPI: TLabel;
    Label11: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrPTKDef_Client_TXT: TWideStringField;
    TabSheet1: TTabSheet;
    QrGraficoStatus: TmySQLQuery;
    PnPesq: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdDef_Sender2: TdmkEditCB;
    CBDef_Sender2: TdmkDBLookupComboBox;
    EdEmpresa2: TdmkEditCB;
    CBEmpresa2: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdProtocolo: TdmkEditCB;
    CBProtocolo: TdmkDBLookupComboBox;
    CBLote: TdmkDBLookupComboBox;
    EdLote: TdmkEditCB;
    Label4: TLabel;
    BtPesquisa: TBitBtn;
    BitBtn1: TBitBtn;
    GridPanel1: TGridPanel;
    Chart1: TChart;
    Series1: TPieSeries;
    Chart2: TChart;
    PieSeries1: TPieSeries;
    PB1: TProgressBar;
    QrSender2: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsSender2: TDataSource;
    QrEmpresas2: TmySQLQuery;
    DsEmpresas2: TDataSource;
    DsProtocolos: TDataSource;
    QrProtocolos: TmySQLQuery;
    QrProtocoPak: TmySQLQuery;
    DsProtocoPak: TDataSource;
    QrProtocoPakControle: TIntegerField;
    Label5: TLabel;
    EdEmpresa3: TdmkEditCB;
    CBEmpresa3: TdmkDBLookupComboBox;
    QrEmpresas3: TmySQLQuery;
    DsEmpresas3: TDataSource;
    QrEmpresas3Codigo: TIntegerField;
    QrEmpresas3Filial: TIntegerField;
    QrEmpresas3NOMEFILIAL: TWideStringField;
    QrEmpresas3CNPJ_CPF: TWideStringField;
    QrEmpresas3IE: TWideStringField;
    QrEmpresas3NIRE: TWideStringField;
    QrEmpresas2Codigo: TIntegerField;
    QrEmpresas2Filial: TIntegerField;
    QrEmpresas2NOMEFILIAL: TWideStringField;
    QrEmpresas2CNPJ_CPF: TWideStringField;
    QrEmpresas2IE: TWideStringField;
    QrEmpresas2NIRE: TWideStringField;
    QrProtocolosCodigo: TIntegerField;
    QrProtocolosNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAtzListaMailProtClick(Sender: TObject);
    procedure BtReenviaClick(Sender: TObject);
    procedure BitBtn17Click(Sender: TObject);
    procedure BitBtn18Click(Sender: TObject);
    procedure BitBtn19Click(Sender: TObject);
    procedure QrEmailProtAfterScroll(DataSet: TDataSet);
    procedure QrEmailProtBeforeClose(DataSet: TDataSet);
    procedure QrEmailProtPakAfterScroll(DataSet: TDataSet);
    procedure QrEmailProtPakBeforeClose(DataSet: TDataSet);
    procedure GradePPICellClick(Column: TColumn);
    procedure GradePPIDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure QrEmailProtPakItsBeforeClose(DataSet: TDataSet);
    procedure QrEmailProtPakItsAfterScroll(DataSet: TDataSet);
    procedure BtProtocolosClick(Sender: TObject);
    procedure QrPTKAfterScroll(DataSet: TDataSet);
    procedure QrPTKBeforeClose(DataSet: TDataSet);
    procedure QrPPIAfterScroll(DataSet: TDataSet);
    procedure QrPPIBeforeClose(DataSet: TDataSet);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrProtocolosAfterScroll(DataSet: TDataSet);
    procedure QrProtocolosBeforeClose(DataSet: TDataSet);
    procedure PageControl2Change(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenEmailProt(Empresa: Integer);
    procedure ReopenEmailProtPak();
    procedure ReopenEmailProtPakIts();
    procedure ReopenPTK(Def_Client, Sender, FiltroPPI, Tarefa, Lote: Integer);
    function  TotalPTKFinaliz(Def_Client, Sender, Tarefa, Lote: Integer; ComoConf: TComoConf): Integer;
    procedure ReopenPPI();
    procedure ReopenSender(Query: TmySQLQuery);
    procedure ReopenProtocolos(Query: TmySQLQuery);
    procedure ReopenProtocolosLotes(Query: TmySQLQuery; Protocolo: Integer);
    {$IFDEF TEM_DBWEB}
      procedure AtzListaMailProt_Web();
      procedure AtzListaMailProt_Pgt();
    {$ENDIF}
    procedure VerificaReenvioEmailAvisoRecebBloqueto(PreEmeio: Integer;
              PB1: TProgressBar);
    function  InsereItem_ProtoMail(RecipEMeio, RecipNome: String;
              RecipItem: Integer; Boleto: Double; Entidade: Integer;
              NOMEENT: String; PROTOCOD: Integer; TAREFA: String;
              DEF_SENDER: Integer; DELIVER: String; DATAE: TDateTime;
              DATAE_TXT: String; DATAD: TDateTime; DATAD_TXT: String; LOTE,
              PROTOCOLO, Nivel, Item: Integer; Vencimento: TDateTime;
              Valor: Double; PreEmeio, IDEmeio: Integer): Boolean;
    procedure EmailBloqueto(PB: TProgressBar(*; PermiteAnexarBloqueto: Boolean*));
    procedure ConfiguraGrafico();
    procedure PesquisaProtocolos();
    procedure ConfiguraPageControlChange();
  public
    { Public declarations }
    procedure MostraEdicao(Mostra: TMosta; Aba, Tarefa, Lote, Empresa, Def_Sender: Integer);
  end;

  var
  FmProtoGer: TFmProtoGer;

implementation

uses Module, UnMyObjects, ModuleGeral, UMySQLModule, UCreate,
  MyVCLSkin, UnDmkWeb, MyGlyfs, Principal, UnProtocoUnit;

{$R *.DFM}

{$IFDEF TEM_DBWEB}
procedure TFmProtoGer.AtzListaMailProt_Web;
const
  Txt1 = 'Verificando e-mails confirmados...';
var
  DataD: String;
  Conta: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Txt1);
  //
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrOpcoesGerl, 1) then
  begin
    Screen.Cursor := crHourGlass;
    try
      UMyMod.AbreQuery(QrRecebiBloq, DMod.MyDBn);
      //
      if QrRecebiBloq.RecordCount > 0 then
      begin
        PB1.Position := 0;
        PB1.Max      := QrRecebiBloq.RecordCount;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE protpakits SET AlterWeb=1, ');
        Dmod.QrUpd.SQL.Add('ComoConf=:P0, DataD=:P1 ');
        Dmod.QrUpd.SQL.Add('WHERE DataD + 0 < 2');
        Dmod.QrUpd.SQL.Add('AND Conta=:P2');
        //
        QrRecebiBloq.First;
        while not QrRecebiBloq.Eof do
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, False, Txt1 +
            ' Novos itens confirmados por e-mail:' + Geral.FF0(QrRecebiBloq.RecordCount));
          //
          DataD := Geral.FDT(QrRecebiBloq.FieldByName('DataHora').AsDateTime, 105);
          Conta := QrRecebiBloq.FieldByName('Protocolo').AsInteger;
          //
          Dmod.QrUpd.Params[00].AsInteger := QrRecebiBloq.FieldByName('ComoConf').AsInteger;
          Dmod.QrUpd.Params[01].AsString  := DataD;
          Dmod.QrUpd.Params[02].AsInteger := Conta;
          Dmod.QrUpd.ExecSQL;
          //
          PB1.Position := PB1.Position + 1;
          PB1.Update;
          Application.ProcessMessages;
          //
          QrRecebiBloq.Next;
        end;
      end;
      ReopenEmailProt(EdEmpresa3.ValueVariant);
    finally
      PB1.Position  := 0;
      Screen.Cursor := crDefault;
    end;
  end;
end;
{$ENDIF}

{$IFDEF TEM_DBWEB}
procedure TFmProtoGer.AtzListaMailProt_Pgt();
const
  Txt1 = 'Verificando pagos ou caducados ...';
var
  Enti, CliIn, Pagos, Velhos, A, B, DdAutConfMail,
  PgRegistros: Integer;
  TbLctA, TbLctB, TbLctD: String;
  DtEncer, DtMorto: TDateTime;
begin
  if MyObjects.FIC(EdEmpresa3.ValueVariant = 0, EdEmpresa3, 'Defina o cliente interno!') then Exit;
  //
  UMyMod.AbreQuery(QrBloOpcoes, DMod.MyDBn);
  //
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Defina a empresa!') then Exit;
  //
  DdAutConfMail := QrBloOpcoes.FieldByName('DdAutConfMail').AsInteger;
  //
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, False, Txt1);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE protpakits SET AlterWeb=1, ');
    Dmod.QrUpd.SQL.Add('ComoConf=:P0, DataD=:P1 ');
    Dmod.QrUpd.SQL.Add('WHERE Conta=:P0');
    //
    Dmod.QrUpdN.SQL.Clear;
    Dmod.QrUpdN.SQL.Add('UPDATE protpakits SET AlterWeb=1, ');
    Dmod.QrUpdN.SQL.Add('ComoConf=:P0, DataD=:P1 ');
    Dmod.QrUpdN.SQL.Add('WHERE Conta=:P2');
    //
    Pagos  := 0;
    Velhos := 0;
    //
    {$IFDEF DEFINE_VARLCT}
      Enti  := QrEmpresas3Codigo.Value;
      CliIn := EdEmpresa3.ValueVariant;
      //
      DModG.Def_EM_ABD(TMeuDB, Enti, CliIn, DtEncer, DtMorto, TbLctA, TbLctB, TbLctD);
    {$ELSE}
      TbLctA := VAR_LCT;
    {$ENDIF}
    //
    QrEmailProt.First;
    while not QrEmailProt.Eof do
    begin
      QrEmailProtPak.First;
      while not QrEmailProtPak.Eof do
      begin
        QrEmailProtPakIts.First;
        while not QrEmailProtPakIts.Eof do
        begin
          {$IFDEF DEFINE_VARLCT}
            PgRegistros := 0;
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrProtPg, Dmod.MyDB, [
              'SELECT COUNT(*) Registros ',
              'FROM ' + TbLctA,
              'WHERE Tipo=2 ',
              'AND FatID >=600 ',
              'AND Sit =0 ',
              'AND Compensado <= 1 ',
              'AND Mez=' + Geral.FF0(QrEmailProtPakMez.Value),
              'AND Cliente=' + Geral.FF0(QrEmailProtPakItsCliente.Value),
              'AND Depto=' + Geral.FF0(QrEmailProtPakItsDepto.Value),
              'AND Documento=' + FloatToStr(QrEmailProtPakItsDocum.Value),
              '']);
            PgRegistros := PgRegistros + QrProtPgRegistros.Value;
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrProtPg, Dmod.MyDB, [
              'SELECT COUNT(*) Registros ',
              'FROM ' + TbLctB,
              'WHERE Tipo=2 ',
              'AND FatID >=600 ',
              'AND Sit =0 ',
              'AND Compensado <= 1 ',
              'AND Mez=' + Geral.FF0(QrEmailProtPakMez.Value),
              'AND Cliente=' + Geral.FF0(QrEmailProtPakItsCliente.Value),
              'AND Depto=' + Geral.FF0(QrEmailProtPakItsDepto.Value),
              'AND Documento=' + FloatToStr(QrEmailProtPakItsDocum.Value),
              '']);
            PgRegistros := PgRegistros + QrProtPgRegistros.Value;
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrProtPg, Dmod.MyDB, [
              'SELECT COUNT(*) Registros ',
              'FROM ' + TbLctD,
              'WHERE Tipo=2 ',
              'AND FatID >=600 ',
              'AND Sit =0 ',
              'AND Compensado <= 1 ',
              'AND Mez=' + Geral.FF0(QrEmailProtPakMez.Value),
              'AND Cliente=' + Geral.FF0(QrEmailProtPakItsCliente.Value),
              'AND Depto=' + Geral.FF0(QrEmailProtPakItsDepto.Value),
              'AND Documento=' + FloatToStr(QrEmailProtPakItsDocum.Value),
              '']);
            PgRegistros := PgRegistros + QrProtPgRegistros.Value;
          {$ELSE}
            UnDmkDAC_PF.AbreMySQLQuery0(QrProtPg, Dmod.MyDB, [
              'SELECT COUNT(*) Registros ',
              'FROM ' + TbLctA,
              'WHERE Tipo=2 ',
              'AND FatID >=600 ',
              'AND Sit =0 ',
              'AND Compensado <= 1 ',
              'AND Mez=' + Geral.FF0(QrEmailProtPakMez.Value),
              'AND Cliente=' + Geral.FF0(QrEmailProtPakItsCliente.Value),
              'AND Depto=' + Geral.FF0(QrEmailProtPakItsDepto.Value),
              'AND Documento=' + FloatToStr(QrEmailProtPakItsDocum.Value),
              '']);
            PgRegistros := QrProtPgRegistros.Value;
          {$ENDIF}
          //
          if PgRegistros = 0 then
          begin
            Pagos := Pagos + 1;
            MyObjects.Informa2(LaAviso1, LaAviso2, False, Txt1 + 
              ' Novos itens pagos: ' + FormatFloat('0', Pagos) +
              ' - Novos itens caducados: ' + FormatFloat('0', Velhos));
            //
            Dmod.QrUpd.Params[00].AsInteger := UnProtocolo.IntOfComoConf(ptkJaPagou);
            Dmod.QrUpd.Params[01].AsString  := Geral.FDT(Date, 1);
            Dmod.QrUpd.Params[02].AsInteger := QrEmailProtPakItsConta.Value;
            Dmod.QrUpd.ExecSQL;
          end else begin
            // eliminar antigos - que j� faz tempo que foram enviados
            A := Trunc(QrEmailProtPakItsDataE.Value);
            B := Trunc(Date - DdAutConfMail);
            if A < B then
            begin
              Velhos := Velhos + 1;
              //
              MyObjects.Informa2(LaAviso1, LaAviso2, False, Txt1 +
                ' Novos itens pagos: ' + FormatFloat('0', Pagos) +
                ' - Novos itens caducados: ' + FormatFloat('0', Velhos));
              //
              Dmod.QrUpdN.Params[00].AsInteger := UnProtocolo.IntOfComoConf(ptkOutros);
              Dmod.QrUpdN.Params[01].AsString  := Geral.FDT(Date, 1);
              Dmod.QrUpdN.Params[02].AsInteger := QrEmailProtPakItsConta.Value;
              Dmod.QrUpdN.ExecSQL;
            end;
          end;
          QrEmailProtPakIts.Next;
        end;
        QrEmailProtPak.Next;
      end;
      QrEmailProt.Next;
    end;
    //
    if Pagos > 0 then
      Geral.MB_Aviso(Geral.FF0(Pagos) + ' boletos foram pagos sem ' +
        'sua confirma��o de recebimento por e-mail!');
    //
    if Velhos > 0 then
      Geral.MB_Aviso(Geral.FF0(Velhos) + ' boletos foram considerados ' +
        'como recebidos pois j� venceram a mais de ' + Geral.FF0(DdAutConfMail) +
        ' dias!');
    //
  finally
    ReopenEmailProt(EdEmpresa3.ValueVariant);
    Screen.Cursor := crDefault;
  end;
end;
{$ENDIF}

procedure TFmProtoGer.BitBtn17Click(Sender: TObject);
var
  DataHora, DataSai: TDateTime;
  Saiu, Conta: Integer;
begin
  if (QrPPI.State <> dsInactive) and (QrPPI.RecordCount > 0) then
  begin
    DataHora := DModG.ObtemAgora;
    //
    if Geral.MB_Pergunta('Confirma a sa�da de todos protocolos do lote ' +
      Geral.FF0(QrPTKLote.Value) + ' em ' + Geral.FDT(DataHora, 8)
      + sLineBreak + ' no login de "' + VAR_LOGIN + '"?') = ID_YES then
    begin
      Saiu    := VAR_USUARIO;
      DataSai := DataHora;
      //
      QrPPI.First;
      while not QrPPI.Eof do
      begin
        if QrPPISaiu.Value = 0 then
        begin
          Conta := QrPPIConta.Value;
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
          'Saiu', 'DataSai'], ['Conta'], [Saiu, DataSai], [Conta], True);
        end;
        QrPPI.Next;
        //
      end;
      PesquisaProtocolos()
    end;
  end;
end;

procedure TFmProtoGer.BitBtn18Click(Sender: TObject);
var
  DataHora,DataRec: TDateTime;
  Recebeu, Conta: Integer;
begin
  if (QrPPI.State <> dsInactive) and (QrPPI.RecordCount > 0) then
  begin
    DataHora := DmoDG.ObtemAgora;
    //
    if Geral.MB_Pergunta('Confirma o recebimento de todos protocolos do lote ' +
      Geral.FF0(QrPTKLote.Value) + ' em ' + Geral.FDT(DataHora, 8)
      + sLineBreak + ' no login de "' + VAR_LOGIN + '"?') = ID_YES then
    begin
      Recebeu := VAR_USUARIO;
      DataRec := DataHora;
      //
      QrPPI.First;
      while not QrPPI.Eof do
      begin
        if (QrPPISaiu.Value <> 0) and (QrPPIRecebeu.Value = 0) then
        begin
          Conta := QrPPIConta.Value;
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
            'Recebeu', 'DataRec'], ['Conta'], [Recebeu, DataRec], [Conta], True);
        end;
        QrPPI.Next;
        //
      end;
      PesquisaProtocolos;
    end;
  end;
end;

procedure TFmProtoGer.BitBtn19Click(Sender: TObject);
var
  DataHora,DataRet: TDateTime;
  Retornou, Conta: Integer;
begin
  if (QrPPI.State <> dsInactive) and (QrPPI.RecordCount > 0) then
  begin
    DataHora := DModG.ObtemAgora;
    //
    if Geral.MB_Pergunta('Confirma o recebimento de todos protocolos do lote ' +
      Geral.FF0(QrPTKLote.Value) + ' em ' + Geral.FDT(DataHora, 8)
      + sLineBreak + ' no login de "' + VAR_LOGIN + '"?') = ID_YES then
    begin
      Retornou := VAR_USUARIO;
      DataRet  := DataHora;
      //
      QrPPI.First;
      while not QrPPI.Eof do
      begin
        if (QrPPIRecebeu.Value <> 0) and (QrPPIRetorna.Value = 1)
          and (QrPPIRetornou.Value = 0) then
        begin
          Conta := QrPPIConta.Value;
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
          'Retornou', 'DataRet'], ['Conta'], [Retornou, DataRet], [Conta], True);
        end;
        QrPPI.Next;
        //
      end;
      PesquisaProtocolos;
    end;
  end;
end;

procedure TFmProtoGer.BitBtn1Click(Sender: TObject);
begin
  ConfiguraGrafico();
end;

procedure TFmProtoGer.BtAtzListaMailProtClick(Sender: TObject);
begin
  if MyObjects.FIC(EdEmpresa3.ValueVariant = 0, EdEmpresa3, 'Defina o cliente interno!') then Exit;
  //
  {$IFDEF TEM_DBWEB}
    try
      AtzListaMailProt_Web();
      AtzListaMailProt_Pgt();
    finally
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Atualizado em ' +
        Geral.FDT(DmodG.ObtemAgora, 0));
    end;
  {$ENDIF}
end;

procedure TFmProtoGer.BtPesquisaClick(Sender: TObject);
begin
  PesquisaProtocolos();
end;

procedure TFmProtoGer.BtProtocolosClick(Sender: TObject);
var
  Tarefa, Lote: Integer;
begin
  Tarefa := 0;
  Lote   := 0;
  //
  if PageControl1.ActivePageIndex = 0 then
  begin
    if EdLote.ValueVariant <> 0 then
      Lote := EdLote.ValueVariant
    else if EdProtocolo.ValueVariant <> 0 then
      Tarefa := EdProtocolo.ValueVariant;
  end else
  begin
    if PageControl2.ActivePageIndex = 0 then
    begin
      if (QrPTK.State <> dsInactive) and (QrPTK.RecordCount > 0) then
        Lote := QrPTKLote.Value;
    end else
    begin
      if (QrEmailProt.State <> dsInactive) and (QrEmailProt.RecordCount > 0) then
        Tarefa := QrEmailProtCodigo.Value;
    end;
  end;
  //
  ProtocoUnit.MostraFormProtocolos(Lote, Tarefa);
end;

procedure TFmProtoGer.BtReenviaClick(Sender: TObject);
(*
var
  PreEmeio, PreMailReenv: Integer;
*)
begin

(*
  Screen.Cursor := crHourGlass;
  try
    PreMailReenv := QrBloOpcoes.FieldByName('PreMailReenv').AsInteger;
    PreEmeio     := PreMailReenv;
    if PreEmeio > 0 then
    begin
      VerificaReenvioEmailAvisoRecebBloqueto(PreEmeio, PB1);
      EmailBloqueto(PB1);
      //
    end else Geral.MensagemBox('Pr�-e-mail n�o definido nas op��es ' +
    'espec�ficas do aplicativo! Envio abortado!', 'Aviso', MB_OK+MB_ICONWARNING);
  finally
    Screen.Cursor := crDefault;
  end;
*)
  Geral.MB_Aviso('Em desenvolvimento!');
end;

procedure TFmProtoGer.BtSaidaClick(Sender: TObject);
begin
  if TFmProtoGer(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmProtoGer.ConfiguraGrafico;
var
  Empresa, Def_Client, Sender, FiltroPPI, Abertos, Cancelados, Finalizados,
  Outros, Manual, Email, JaPagou, WebLink, WebAuto, Todos, Tarefa, Lote: Integer;
begin
  Empresa := EdEmpresa2.ValueVariant;
  //
  if Empresa <> 0 then
  begin
    Def_Client := QrEmpresas2Codigo.Value;
    Sender     := EdDef_Sender2.ValueVariant;
    Tarefa     := EdProtocolo.ValueVariant;
    Lote       := EdLote.ValueVariant;
    //
    with Series1 do
    begin
      Series1.Clear;

      ReopenPTK(Def_Client, Sender, 1, Tarefa, Lote);

      Abertos := QrPTK.RecordCount;

      Add(Abertos, 'Abertos');

      ReopenPTK(Def_Client, Sender, 3, Tarefa, Lote);

      Cancelados := QrPTK.RecordCount;

      Add(Cancelados, 'Cancelados');

      Finalizados := TotalPTKFinaliz(Def_Client, Sender, Tarefa, Lote, ptkTodos);

      Add(Finalizados, 'Finalizados');
    end;

    with PieSeries1 do
    begin
      PieSeries1.Clear;

      Manual  := TotalPTKFinaliz(Def_Client, Sender, Tarefa, Lote, ptkManual);
      Email   := TotalPTKFinaliz(Def_Client, Sender, Tarefa, Lote, ptkEmail);
      JaPagou := TotalPTKFinaliz(Def_Client, Sender, Tarefa, Lote, ptkJaPagou);
      WebLink := TotalPTKFinaliz(Def_Client, Sender, Tarefa, Lote, ptkWebLink);
      WebAuto := TotalPTKFinaliz(Def_Client, Sender, Tarefa, Lote, ptkWebAuto);
      Todos   := TotalPTKFinaliz(Def_Client, Sender, Tarefa, Lote, ptkTodos);
      Outros  := TotalPTKFinaliz(Def_Client, Sender, Tarefa, Lote, ptkOutros);
      //
      Add(WebAuto, 'Confirma��o autom�tica ao abrir e-mail');
      Add(WebLink, 'Link de confirma��o no e-mail');
      Add(JaPagou, 'Baixado ap�s quitar');
      Add(Email,   'E-mail');
      Add(Manual,  'Baixado manualmente');
      Add(Outros,  'Outros');
    end;
  end else
    Geral.MB_Aviso('Defina o cliente interno!');
end;

procedure TFmProtoGer.ConfiguraPageControlChange;
var
  Aba, Tarefa, Lote, Empresa, Def_Sender: Integer;
begin
  if PageControl1.ActivePageIndex = 1 then
  begin
    if PageControl2.ActivePageIndex = 1 then //E-mail
    begin
      Aba        := 1;
      Tarefa     := 0;
      Lote       := 0;
      Empresa    := EdEmpresa3.ValueVariant;
      Def_Sender := 0;
    end else
    begin
      Aba        := 0;
      Tarefa     := 0;
      Lote       := 0;
      Empresa    := EdEmpresa.ValueVariant;
      Def_Sender := EdDef_Sender.ValueVariant;
    end;
  end else
  begin
    Aba        := -1;
    Tarefa     := EdProtocolo.ValueVariant;
    Lote       := EdLote.ValueVariant;
    Empresa    := EdEmpresa2.ValueVariant;
    Def_Sender := EdDef_Sender2.ValueVariant;
  end;
  MostraEdicao(TMosta(PageControl1.ActivePageIndex), Aba, Tarefa, Lote, Empresa, Def_Sender);
end;

procedure TFmProtoGer.EmailBloqueto(PB: TProgressBar);
{
var
  Ordem: Integer;
}
begin
{
  Screen.Cursor := crHourGlass;
  try
    //FSdoOldNew :=
    UCriar.RecriaTempTable('UnidCond', DmodG.QrUpdPID1, False);
    //
    DModG.QrProtoMail.Close;
    DModG.QrProtoMail.Database := DModG.MyPID_DB;
    DModG.QrProtoMail.Open;
    if PB <> nil then
    begin
      PB.Position := 0;
      PB.Max := DModG.QrProtoMail.RecordCount;
    end;
    DModG.QrProtoMail.First;
    while not DModG.QrProtoMail.Eof do
    begin
      if PB <> nil then
      begin
        PB.Position := PB.Position + 1;
        PB.Update;
      end;
      Application.ProcessMessages;
      //
      Ordem := DModG.QrProtoMail.RecNo;
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'unidcond', False, [
        'Apto', 'Unidade',
        'Proprie', 'Selecio',
        'Entidad', 'Protoco',
        'Bloquet',
        'Data1',
        'Data2',
        'RecipItem', 'RecipNome',
        'RecipEMeio', 'RecipProno',
        'Vencimento', 'Valor',
        'Condominio', 'PreEmeio',
        'IDEmeio'
      ], ['Ordem'], [
        DModG.QrProtoMailEntid_Cod.Value, DModG.QrProtoMailDepto_Txt.Value,
        DModG.QrProtoMailEntid_Txt.Value, MLAGeral.BoolToInt(DModG.QrProtoMailDATAE.Value = 0),
        DModG.QrProtoMailEntid_Cod.Value, DModG.QrProtoMailProtocolo.Value,
        DModG.QrProtoMailBloqueto.Value,
        Geral.FDT(DModG.QrProtoMailDataE.Value, 1),
        Geral.FDT(DModG.QrProtoMailDataD.Value, 1),
        DModG.QrProtoMailRecipItem.Value, DModG.QrProtoMailRecipNome.Value,
        DModG.QrProtoMailRecipEmeio.Value, DModG.QrProtoMailRecipProno.Value,
        Geral.FDT(DModG.QrProtoMailVencimento.Value, 1), DModG.QrProtoMailValor.Value,
        DModG.QrProtoMailCondominio.Value, DModG.QrProtoMailPreEmeio.Value,
        DModG.QrProtoMailIDEmeio.Value
      ], [Ordem], False);
      DModG.QrProtoMail.Next;
    end;
    Application.CreateForm(TFmBloGerenEmail, FmBloGerenEmail);
  finally
    Screen.Cursor := crDefault;
  end;
  FmBloGerenEmail.ShowModal;
  FmBloGerenEmail.Destroy;
}
end;

procedure TFmProtoGer.FormActivate(Sender: TObject);
begin
  if TFmProtoGer(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
  ImgTipo.SQLType := stPsq;
end;

procedure TFmProtoGer.FormCreate(Sender: TObject);
begin
  {$IFDEF TEM_DBWEB}
    TabSheet10.TabVisible := True;
  {$ELSE}
    TabSheet10.TabVisible := False;
  {$ENDIF}
  //
  DBGEmailProt.DataSource       := DsEmailProt;
  DBGEmailProtPak.DataSource    := DsEmailProtPak;
  DBGEmailProtPakIts.DataSource := DsEmailProtPakIts;
  GradePTK.DataSource           := DsPTK;
  GradePPI.DataSource           := DsPPI;
  //
  QrPTK.DataBase := Dmod.MyDB;
  QrPPI.DataBase := Dmod.MyDB;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdEmpresa2, CBEmpresa2, '', False, QrEmpresas2);
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdEmpresa3, CBEmpresa3, '', False, QrEmpresas3);
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdEmpresa, CBEmpresa, '', False, QrEmpresas);
  //
  ReopenSender(QrSender);
  ReopenSender(QrSender2);
  //
  ReopenProtocolos(QrProtocolos);
  ReopenEmailProt(EdEmpresa3.ValueVariant);
end;

procedure TFmProtoGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProtoGer.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
end;

procedure TFmProtoGer.GradePPICellClick(Column: TColumn);
var
  DataHora, DataSai, DataRec, DataRet: TDateTime;
  Saiu, Recebeu, Retornou, Conta: Integer;
begin
  if (Column.FieldName = 'Saiu') and (QrPPISaiu.Value = 0) then
  begin
    DataHora := DmodG.ObtemAgora;
    //
    if Geral.MB_Pergunta('Confirma a sa�da do protocolo ' +
      Geral.FF0(QrPPIConta.Value) + ' em ' + Geral.FDT(DataHora, 8) + sLineBreak
      + ' no login de "' + VAR_LOGIN + '"?') = ID_YES then
    begin
      Saiu    := VAR_USUARIO;
      DataSai := DataHora;
      Conta   := QrPPIConta.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False,
        ['Saiu', 'DataSai'], ['Conta'], [Saiu, DataSai], [Conta], True) then
      begin
        PesquisaProtocolos;
      end;
    end;
  end;
  //
  if (Column.FieldName = 'Recebeu') and (QrPPIRecebeu.Value = 0) then
  begin
    if QrPPISaiu.Value = 0 then
    begin
      Geral.MB_Aviso('� necess�rio informar a sa�da antes do recebimento!');
    end else
    begin
      DataHora := DmodG.ObtemAgora;
      //
      if Geral.MB_Pergunta('Confirma o recebimento do protocolo ' +
        Geral.FF0(QrPPIConta.Value) + ' em ' + Geral.FDT(DataHora, 8) +
        sLineBreak + ' no login de "' + VAR_LOGIN + '"?') = ID_YES then
      begin
        Recebeu := VAR_USUARIO;
        DataRec := DataHora;
        Conta   := QrPPIConta.Value;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
          'Recebeu', 'DataRec'], ['Conta'], [Recebeu, DataRec], [Conta], True) then
        begin
          PesquisaProtocolos;
        end;
      end;
    end;
  end;
  //
  if (Column.FieldName = 'Retornou') and (QrPPIRetornou.Value = 0) then
  begin
    if QrPPIRecebeu.Value = 0 then
    begin
      Geral.MB_Aviso('� necess�rio informar o recebimento antes do retorno!');
    end else
    begin
      DataHora := DmodG.ObtemAgora;
      //
      if Geral.MB_Pergunta('Confirma o retorno do protocolo ' +
        Geral.FF0(QrPPIConta.Value) + ' em ' + Geral.FDT(DataHora, 8) +
        sLineBreak + ' no login de "' + VAR_LOGIN + '"?') = ID_YES then
      begin
        Retornou := VAR_USUARIO;
        DataRet  := DataHora;
        Conta    := QrPPIConta.Value;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'protpakits', False, [
        'Retornou', 'DataRet'], ['Conta'], [Retornou, DataRet], [Conta], True) then
        PesquisaProtocolos;
      end;
    end;
  end;
end;

procedure TFmProtoGer.GradePPIDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Txt, Bak: TColor;
begin
  if (Column.FieldName = 'NO_RETORNA') then
  begin
    if QrPPIRetorna.Value = 1 then
    begin
      Txt := clWindow;
      Bak := clRed;
    end else begin
      Txt := clWindow;
      Bak := clBlue;
    end;
    MyObjects.DesenhaTextoEmDBGrid(GradePPI, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end;
  if Column.FieldName = 'Saiu' then
    MeuVCLSkin.DrawGrid(TDBGrid(GradePPI), Rect, 1, QrPPISaiu.Value);
  if Column.FieldName = 'Recebeu' then
    MeuVCLSkin.DrawGrid(TDBGrid(GradePPI), Rect, 1, QrPPIRecebeu.Value);
  if Column.FieldName = 'Retornou' then
    MeuVCLSkin.DrawGrid(TDBGrid(GradePPI), Rect, 1, QrPPIRetornou.Value);
end;

function TFmProtoGer.InsereItem_ProtoMail(RecipEMeio, RecipNome: String;
  RecipItem: Integer; Boleto: Double;
  Entidade: Integer; NOMEENT: String; PROTOCOD: Integer;
  TAREFA: String; DEF_SENDER: Integer; DELIVER: String; DATAE: TDateTime;
  DATAE_TXT: String; DATAD: TDateTime; DATAD_TXT: String; LOTE, PROTOCOLO,
  Nivel, Item: Integer; Vencimento: TDateTime; Valor: Double;
  PreEmeio, IDEmeio: Integer): Boolean;
const
  GetNivel: array [0..4] of String = ('Sem emeio', 'Sem protocolo',
  'N�o enviado', 'Aguardando retorno', 'Retornado');
begin
  try
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'protomail', False, [
      'RecipEMeio', 'RecipNome', 'RecipItem', 'Bloqueto', 'Entid_Cod',
      'Entid_Txt', 'Taref_Cod', 'Taref_Txt', 'Deliv_Cod', 'Deliv_Txt',
      'DataE', 'DataE_Txt', 'DataD', 'DataD_Txt', 'ProtoLote', 'Protocolo',
      'NivelEmail', 'NivelDescr', 'Vencimento', 'Valor', 'PreEmeio',
      'IDEmeio'
    ], ['Item'], [
      RecipEMeio, RecipNome, RecipItem, Boleto, Entidade,
      NOMEENT, PROTOCOD, TAREFA, DEF_SENDER, DELIVER,
      DATAE, DATAE_TXT, DATAD, DATAD_TXT, LOTE, PROTOCOLO,
      Nivel, GetNivel[Nivel], Geral.FDT(Vencimento, 1), Valor, PreEmeio,
      IDEmeio
    ], [Item], False);
    Result := True;
  except
    //Result := False;
    raise;
  end;
end;

procedure TFmProtoGer.MostraEdicao(Mostra: TMosta; Aba, Tarefa, Lote, Empresa,
  Def_Sender: Integer);
begin
    case Mostra of
    gerGer:
    begin
      PageControl1.ActivePageIndex := 1;
      PageControl2.ActivePageIndex := Aba;
      //
      if Aba = 1 then
      begin
        EdEmpresa3.ValueVariant := Empresa;
        CBEmpresa3.KeyValue     := Empresa;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
        //
        ReopenEmailProt(Empresa);
      end else
      begin
        EdEmpresa.ValueVariant    := Empresa;
        CBEmpresa.KeyValue        := Empresa;
        EdDef_Sender.ValueVariant := Def_Sender;
        CBDef_Sender.KeyValue     := Def_Sender;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, False,
          'R? => S = Informar o recebimento / N = N�o � necess�rio informar o recebimento');
        //
        PesquisaProtocolos;
      end;
    end;
    gerSta:
    begin
      PageControl1.ActivePageIndex := 0;
      PageControl2.ActivePageIndex := 0;
      //
      EdEmpresa2.ValueVariant    := Empresa;
      CBEmpresa2.KeyValue        := Empresa;
      EdDef_Sender2.ValueVariant := Def_Sender;
      CBDef_Sender2.KeyValue     := Def_Sender;
      EdProtocolo.ValueVariant   := Tarefa;
      CBProtocolo.KeyValue       := Tarefa;
      EdLote.ValueVariant        := Lote;
      CBLote.KeyValue            := Lote;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      ConfiguraGrafico();
    end;
  end;
end;

procedure TFmProtoGer.PageControl1Change(Sender: TObject);
begin
  ConfiguraPageControlChange();
end;

procedure TFmProtoGer.PageControl2Change(Sender: TObject);
begin
  ConfiguraPageControlChange();
end;

procedure TFmProtoGer.PesquisaProtocolos;
var
  Empresa, Def_Client, FiltroPPI, Sender: Integer;
begin
  Empresa := EdEmpresa.ValueVariant;
  //
  if Empresa <> 0 then
  begin
    Def_Client := QrEmpresasCodigo.Value;
    Sender     := EdDef_Sender.ValueVariant;
    FiltroPPI  := RGProtoFiltro.ItemIndex;
    //
    ReopenPTK(Def_Client, Sender, FiltroPPI, 0, 0);
  end else
    Geral.MB_Aviso('Defina o cliente interno!');
end;

procedure TFmProtoGer.QrEmailProtAfterScroll(DataSet: TDataSet);
begin
  ReopenEmailProtPak;
  //
  LaEmailProtTotal.Caption := 'Total de itens: ' + Geral.FF0(QrEmailProt.RecordCount);
end;

procedure TFmProtoGer.QrEmailProtBeforeClose(DataSet: TDataSet);
begin
  QrEmailProtPak.Close;
  //
  LaEmailProtTotal.Caption := '';
end;

procedure TFmProtoGer.QrEmailProtPakAfterScroll(DataSet: TDataSet);
begin
  ReopenEmailProtPakIts;
  //
  LaGEmailProtPakTotal.Caption := 'Total de itens: ' + Geral.FF0(QrEmailProtPak.RecordCount);
end;

procedure TFmProtoGer.QrEmailProtPakBeforeClose(DataSet: TDataSet);
begin
  QrEmailProtPakIts.Close;
  //
  LaGEmailProtPakTotal.Caption := '';
end;

procedure TFmProtoGer.QrEmailProtPakItsAfterScroll(DataSet: TDataSet);
begin
  LaGEmailProtPakItsTotal.Caption := 'Total de itens: ' + Geral.FF0(QrEmailProtPakIts.RecordCount);
end;

procedure TFmProtoGer.QrEmailProtPakItsBeforeClose(DataSet: TDataSet);
begin
  LaGEmailProtPakItsTotal.Caption := '';
end;

procedure TFmProtoGer.QrPPIAfterScroll(DataSet: TDataSet);
begin
  LaGradePPI.Caption := 'Total de itens: ' + Geral.FF0(QrPPI.RecordCount);
end;

procedure TFmProtoGer.QrPPIBeforeClose(DataSet: TDataSet);
begin
  LaGradePTK.Caption := '';
end;

procedure TFmProtoGer.QrProtocolosAfterScroll(DataSet: TDataSet);
begin
  ReopenProtocolosLotes(QrProtocoPak, QrProtocolosCodigo.Value);
end;

procedure TFmProtoGer.QrProtocolosBeforeClose(DataSet: TDataSet);
begin
  QrProtocoPak.Close;
end;

procedure TFmProtoGer.QrPTKAfterScroll(DataSet: TDataSet);
begin
  ReopenPPI();
  //
  LaGradePTK.Caption := 'Total de itens: ' + Geral.FF0(QrPTK.RecordCount);
end;

procedure TFmProtoGer.QrPTKBeforeClose(DataSet: TDataSet);
begin
  QrPPI.Close;
  //
  LaGradePTK.Caption := '';
end;

procedure TFmProtoGer.ReopenEmailProt(Empresa: Integer);
begin
  if Empresa <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEmailProt, Dmod.MyDB, [
      'SELECT ptc.Codigo, ptc.Nome, COUNT(ppi.Conta) ITENS, ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Def_Client_TXT ',
      'FROM protpakits ppi ',
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo ',
      'LEFT JOIN entidades ent ON ent.Codigo=ptc.Def_Client',
      'WHERE ppi.DataD <= "1899-12-30" ',
      'AND ptc.Tipo=2 ',
      'AND ppi.DataE > 1 ',
      'AND ptc.Def_Client=' + Geral.FF0(QrEmpresas3Codigo.Value),
      'GROUP BY ptc.Codigo ',
      'ORDER BY ptc.Nome ',
      '']);
  end;
end;

procedure TFmProtoGer.ReopenEmailProtPak;
var
  Codigo: Integer;
begin
  Codigo := QrEmailProtCodigo.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmailProtPak, Dmod.MyDB, [
    'SELECT ptp.Controle, ptp.Mez, COUNT(ppi.Conta) ITENS, ',
    'CONCAT(RIGHT(Mez, 2), "/",  LEFT(Mez + 200000, 4)) NOMEMEZ ',
    'FROM protpakits ppi ',
    'LEFT JOIN protocopak ptp ON ptp.Controle=ppi.Controle ',
    'WHERE ppi.DataD <= "1899-12-30" ',
    'AND ppi.DataE > 1 ',
    'AND ptp.Codigo=' + Geral.FF0(Codigo),
    'GROUP BY ptp.Controle ',
    '']);
end;

procedure TFmProtoGer.ReopenEmailProtPakIts;
var
  Controle: Integer;
begin
  Controle := QrEmailProtPakControle.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmailProtPakIts, Dmod.MyDB, [
    'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI, ',
    'IF(ppi.DataE=0, "", ',
    'DATE_FORMAT(ppi.DataE, "%d/%m/%y")) DATAE_TXT, ',
    'ppi.Conta, ppi.DataE, ppi.Cliente, ppi.Depto, ppi.Docum, ',
    'ppi.ID_Cod1, ppi.ID_Cod2, ppi.ID_Cod3, ppi.ID_Cod4, ',
    'ppi.Vencto, ppi.Lancto ',
    'FROM protpakits ppi ',
    'LEFT JOIN entidades cli ON cli.Codigo=ppi.Cliente ',
    'WHERE ppi.DataD = 0 ',
    'AND ppi.DataE > 1 ',
    'AND ppi.Controle=' + Geral.FF0(Controle),
    '']);
end;

procedure TFmProtoGer.ReopenPTK(Def_Client, Sender, FiltroPPI, Tarefa, Lote: Integer);
begin
  if Def_Client <> 0 then
  begin
    QrPTK.Close;
    //
    if Def_Client <> 0 then
    begin
      QrPTK.SQL.Clear;
      QrPTK.SQL.Add('SELECT DISTINCT ptc.Nome, ptk.Controle Lote, ptk.Mez,');
      QrPTK.SQL.Add('ptc.Def_Sender, ptc.Def_Client, ');
      QrPTK.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Def_Client_TXT, ');
      QrPTK.SQL.Add('CONCAT(SUBSTRING(ptk.Mez, 3, 2), "/", SUBSTRING(ptk.Mez, 1, 2)) Mez_TXT');
      QrPTK.SQL.Add('FROM protpakits ppi');
      QrPTK.SQL.Add('LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo');
      QrPTK.SQL.Add('LEFT JOIN protocopak ptk ON ptk.Controle=ppi.Controle');
      QrPTK.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ptc.Def_Client');
      QrPTK.SQL.Add('WHERE (');
      case FiltroPPI of
        0: // Antigo false
        begin
          QrPTK.SQL.Add('  (Saiu=0 AND LimiteSai <= SYSDATE())');
          QrPTK.SQL.Add('   OR');
          QrPTK.SQL.Add('  (Recebeu=0 AND LimiteRem <= SYSDATE())');
          QrPTK.SQL.Add('   OR');
          QrPTK.SQL.Add('  (Retorna=1 AND Retornou=0 AND LimiteRet <= SYSDATE())');
        end;
        1: // antigo true
        begin
          QrPTK.SQL.Add('  (Saiu=0)');
          QrPTK.SQL.Add('   OR');
          QrPTK.SQL.Add('  (Recebeu=0)');
          QrPTK.SQL.Add('   OR');
          QrPTK.SQL.Add('  (Retorna=1 AND Retornou=0)');
        end;
        2,3: // Tudo
        begin
          QrPTK.SQL.Add('Conta <> 0');
        end;
      end;
      QrPTK.SQL.Add(')');
      if FiltroPPI = 3 then
        QrPTK.SQL.Add('AND ppi.Cancelado=1')
      else
        QrPTK.SQL.Add('AND ppi.Cancelado=0');
      if Sender <> 0 then
        QrPTK.SQL.Add('AND ptc.Def_Sender=' + Geral.FF0(Sender));
      if Tarefa <> 0 then
        QrPTK.SQL.Add('AND ptc.Codigo=' + Geral.FF0(Tarefa));
      if Lote <> 0 then
        QrPTK.SQL.Add('AND ptk.Controle=' + Geral.FF0(Lote));
      QrPTK.SQL.Add('AND ptc.Def_Client=' + Geral.FF0(Def_Client));
      QrPTK.SQL.Add('AND ptc.Tipo <> 2');
      QrPTK.SQL.Add('ORDER BY ptc.Nome, ptk.Mez, ptk.Controle');
      UMyMod.AbreQuery(QrPTK, Dmod.MyDB);
    end;
  end else
    QrPTK.Close;
end;

function TFmProtoGer.TotalPTKFinaliz(Def_Client, Sender, Tarefa, Lote: Integer;
  ComoConf: TComoConf): Integer;
var
  SQL_Def_Client, SQL_Sender, SQL_ComoConf, SQL_Tarefa, SQL_Lote: String;
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry    := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
  try
    if Def_Client <> 0 then
      SQL_Def_Client := 'AND pro.Def_Client=' + Geral.FF0(Def_Client)
    else
      SQL_Def_Client := '';
    if Sender <> 0 then
      SQL_Sender := 'AND pro.Def_Sender=' + Geral.FF0(Sender)
    else
      SQL_Sender := '';
    if Tarefa <> 0 then
      SQL_Tarefa := 'AND pro.Codigo=' + Geral.FF0(Tarefa)
    else
      SQL_Tarefa := '';
    if Lote <> 0 then
      SQL_Lote := 'AND its.Controle=' + Geral.FF0(Lote)
    else
      SQL_Lote := '';
    //
    if ComoConf = ptkTodos then
      SQL_ComoConf := ''
    else if ComoConf = ptkOutros then
      SQL_ComoConf := 'AND its.ComoConf IN(0, ' + Geral.FF0(UnProtocolo.IntOfComoConf(ComoConf)) + ') '
    else
      SQL_ComoConf := 'AND its.ComoConf=' + Geral.FF0(UnProtocolo.IntOfComoConf(ComoConf));
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT its.* ',
      'FROM protpakits its ',
      'LEFT JOIN protocolos pro ON pro.Codigo = its.Codigo ',
      'WHERE ',
      '((its.Retorna = 1 ',
      'AND its.Retornou <> 0 ',
      'AND its.DataRet > "1900-01-01") ',
      'OR ',
      '(its.Retorna = 0 ',
      'AND its.Recebeu <> 0 ',
      'AND its.DataRec > "1900-01-01") ',
      'OR ',
      '(pro.Tipo=2 ',
      'AND its.DataE > "1900-01-01" ',
      'AND its.DataD > "1900-01-01")) ',
      'AND its.Cancelado = 0 ',
      SQL_Def_Client,
      SQL_Sender,
      SQL_ComoConf,
      SQL_Tarefa,
      SQL_Lote,
      '']);
    Result := Qry.RecordCount;
  finally
    Qry.Free;
  end;
end;

procedure TFmProtoGer.ReopenProtocolos(Query: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM protocolos ',
    'WHERE Ativo = 1 ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmProtoGer.ReopenProtocolosLotes(Query: TmySQLQuery; Protocolo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM protocopak ',
    'WHERE Codigo=' + Geral.FF0(Protocolo),
    'ORDER BY Controle DESC ',
    '']);
end;

procedure TFmProtoGer.ReopenSender(Query: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT ',
    'FROM entidades ',
    'ORDER BY NOMEENT ',
    '']);
end;

procedure TFmProtoGer.ReopenPPI();
var
  Def_Client, Def_Sender, Filtro: Integer;
begin
  Def_Client := QrPTKDef_Client.Value;
  Def_Sender := QrPTKDef_Sender.Value;
  Filtro     := RGProtoFiltro.ItemIndex;
  //
  QrPPI.Close;
  QrPPI.SQL.Clear;
  QrPPI.SQL.Add('SELECT ppi.Texto, ppi.DataE, ppi.Conta, ppi.Retorna,');
  QrPPI.SQL.Add('IF(ppi.Retorna=0,"N","S") NO_RETORNA,');
  QrPPI.SQL.Add('IF(ppi.Manual=0,"",pto.Nome) NO_Motivo,');
  QrPPI.SQL.Add('ppi.LimiteSai, ppi.Saiu, ppi.DataSai,');
  QrPPI.SQL.Add('ppi.LimiteRem, ppi.Recebeu, ppi.DataRec,');
  QrPPI.SQL.Add('ppi.LimiteRet, ppi.Retornou, ppi.DataRet,');
  QrPPI.SQL.Add('ppi.Texto, ppi.Lancto, ppi.Docum,');
  QrPPI.SQL.Add('IF(ppi.DataSai <= "1899-12-30", "", DATE_FORMAT(ppi.DataSai, "%d/%m/%y")) DataSai_Txt,');
  QrPPI.SQL.Add('IF(ppi.DataRec <= "1899-12-30", "", DATE_FORMAT(ppi.DataRec, "%d/%m/%y")) DataRec_Txt,');
  QrPPI.SQL.Add('IF(ppi.DataRet <= "1899-12-30", "", DATE_FORMAT(ppi.DataRet, "%d/%m/%y")) DataRet_Txt ');
  QrPPI.SQL.Add('FROM protpakits ppi');
  QrPPI.SQL.Add('LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo');
  QrPPI.SQL.Add('LEFT JOIN protocopak ptk ON ptk.Controle=ppi.Controle');
  QrPPI.SQL.Add('LEFT JOIN protocooco pto ON pto.Codigo=ppi.Manual');
  QrPPI.SQL.Add('WHERE (');
  case Filtro of
    0: // Antigo false
    begin
      QrPPI.SQL.Add('  (Saiu=0 AND LimiteSai <= SYSDATE())');
      QrPPI.SQL.Add('   OR');
      QrPPI.SQL.Add('  (Recebeu=0 AND LimiteRem <= SYSDATE())');
      QrPPI.SQL.Add('   OR');
      QrPPI.SQL.Add('  (Retorna=1 AND Retornou=0 AND LimiteRet <= SYSDATE())');
    end;
    1: // antigo true
    begin
      QrPPI.SQL.Add('  (Saiu=0)');
      QrPPI.SQL.Add('   OR');
      QrPPI.SQL.Add('  (Recebeu=0)');
      QrPPI.SQL.Add('   OR');
      QrPPI.SQL.Add('  (Retorna=1 AND Retornou=0)');
    end;
    2,3: // Tudo
    begin
      QrPPI.SQL.Add('Conta <> 0');
    end;
  end;
  QrPPI.SQL.Add(')');
  if Filtro = 3 then
    QrPPI.SQL.Add('AND ppi.Cancelado=1')
  else
    QrPPI.SQL.Add('AND ppi.Cancelado=0');
  if Def_Sender <> 0 then
    QrPPI.SQL.Add('AND ptc.Def_Sender=' + Geral.FF0(Def_Sender));
  //
  QrPPI.SQL.Add('AND ptc.Def_Client=' + Geral.FF0(Def_Client));
  //
  QrPPI.SQL.Add('AND ptk.Controle=' + Geral.FF0(QrPTKLote.Value));
  QrPPI.SQL.Add('ORDER BY ppi.DataD, ppi.Conta');
  UnDmkDAC_PF.AbreQueryApenas(QrPPI);
end;

procedure TFmProtoGer.VerificaReenvioEmailAvisoRecebBloqueto(
  PreEmeio: Integer; PB1: TProgressBar);
var
  Nivel, Item, PE: Integer;
begin
  Item := 0;
  Screen.Cursor := crHourGlass;
  QrReenv.Close;
  QrReenv.Open;
  //
  if QrReenv.RecordCount > 0 then
  begin
    if PB1 <> nil then
    begin
      PB1.Position := 0;
      PB1.Max := QrReenv.RecordCount;
    end;
    Application.ProcessMessages;
    //
    UCriar.RecriaTempTable('ProtoMail', DmodG.QrUpdPid1, False);
    QrReenv.First;
    while not QrReenv.Eof do
    begin
      if PB1 <> nil then
      begin
        PB1.Position := PB1.Position + 1;
        PB1.Update;
      end;
      Application.ProcessMessages;
      QrEntiMail.Close;
      QrEntiMail.Params[0].AsInteger := QrBloOpcoes.FieldByName('EntiTipCto').AsInteger;
      QrEntiMail.Params[1].AsInteger := QrReenvCliente.Value;
      QrEntiMail.Open;
      if QrEntiMail.RecordCount > 0 then
      begin
        QrEntiMail.First;
        while not QrEntiMail.Eof do
        begin
          QrPPIMail.Close;
          QrPPIMail.Params[0].AsInteger := QrReenvID_Cod1.Value;  //ID_Cod1; // Prev
          QrPPIMail.Params[1].AsInteger := QrReenvID_Cod2.Value;  //ID_Cod2; // Entidade
          QrPPIMail.Params[2].AsInteger := QrEntiMailConta.Value;
          QrPPIMail.Open;
          //
          if PreEmeio > 0 then
            PE := PreEmeio
          else
            PE := QrPPIMailPreEmeio.Value;
          //
          Nivel := MLAGeral.BoolToIntDeVarios(
          [
            QrPPIMailDataD.Value > 0,
            QrPPIMailDataE.Value > 0,
            QrPPIMailPROTOCOLO.Value > 0
          ], [4,3,2], 1);
          //
          Inc(Item, 1);
          InsereItem_ProtoMail(
            // EMeio
            QrEntiMailEMail.Value,
            QrEntiMailNOMEENT.Value,
            // ID do cadastro do emeio
            QrEntiMailConta.Value,
            //Bloqueto
            QrReenvDocum.Value,
            //Entidade
            QrReenvCliente.Value, QrReenvNOMECLI.Value,
            // Tarefa
            QrPPIMailTAREFA_COD.Value,
            QrPPIMailTAREFA_NOM.Value,
            // Deliver
            QrPPIMailDef_Sender.Value,
            QrPPIMailDELIVER.Value,
            // Data entrega
            QrPPIMailDataE.Value, QrPPIMailDATAE_TXT.Value,
            // Data devolu��o
            QrPPIMailDataD.Value, QrPPIMailDATAD_TXT.Value,
            // Lote e protocolo
            QrPPIMailLOTE.Value, QrPPIMailPROTOCOLO.Value,
            // N�vel, ID
            Nivel, Item,
            // Vencto, valor
            QrReenvVencto.Value, QrReenvValor.Value(*zerado*),
            // Sauda��o e pr�-emeio
            PE,
            // ID do emeio (c�digo do cadastro > campo Item da tabela CondEmeios)
            QrEntiMailConta.Value);
          QrEntiMail.Next;
        end;
      end else begin
        Inc(Item, 1);
        InsereItem_ProtoMail(
            // EMeio
            '', '', 0,
            //Bloqueto
            QrReenvDocum.Value,
            //Entidade
            QrReenvCliente.Value, QrReenvNOMECLI.Value,
            // Tarefa
            QrPPIMailTAREFA_COD.Value,
            QrPPIMailTAREFA_NOM.Value,
            // Deliver
            QrPPIMailDef_Sender.Value,
            QrPPIMailDELIVER.Value,
            // Data entrega
            QrPPIMailDATAE.Value, QrPPIMailDATAE_TXT.Value,
            // Data devolu��o
            QrPPIMailDATAD.Value, QrPPIMailDATAD_TXT.Value,
            // Lote e protocolo
            QrPPIMailLOTE.Value, QrPPIMailPROTOCOLO.Value,
            // N�vel, ID
            0, Item,
            // Vencto, valor
            QrReenvVencto.Value, QrReenvValor.Value,
            // Sauda��o e pr�-emeio
            PreEmeio, 0);
      end;
      QrReenv.Next;
    end;
    //LaProtoMail.Caption := '';
  end else
  begin
    UCriar.RecriaTempTable('ProtoMail', DmodG.QrUpdPid1, False);
    //LaProtoMail.Caption := 'Nunhuma UH est� cadastrada para entrega por emeio!';
    Application.MessageBox('Nunhuma UH foi localizada!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  //PnProtoMail.Visible := True;
  if PB1 <> nil then
  begin
    PB1.Max             := 0;
    PB1.Position        := 0;
  end;
  //
  //ReopenProtoMails;
  Screen.Cursor := crDefault;
end;

end.                                                                                                                                   
