unit Protocolos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, ComCtrls, Menus, dmkEdit, frxClass, frxDBSet, dmkDBGrid, dmkGeral,
  dmkDBLookupComboBox, dmkEditCB, Variants, UnDmkProcFunc, dmkImage,
  UnDmkEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmProtocolos = class(TForm)
    DsProtocolos: TDataSource;
    QrProtocolos: TmySQLQuery;
    QrProtocolosCodigo: TIntegerField;
    QrProtocolosNome: TWideStringField;
    QrProtocolosTipo: TIntegerField;
    QrProtocolosDef_Client: TIntegerField;
    QrProtocolosDef_Sender: TIntegerField;
    QrProtocolosLk: TIntegerField;
    QrProtocolosDataCad: TDateField;
    QrProtocolosDataAlt: TDateField;
    QrProtocolosUserCad: TIntegerField;
    QrProtocolosUserAlt: TIntegerField;
    QrProtocolosAlterWeb: TSmallintField;
    QrProtocolosAtivo: TSmallintField;
    QrClient: TmySQLQuery;
    DsClient: TDataSource;
    QrSender: TmySQLQuery;
    DsSender: TDataSource;
    QrClientCodigo: TIntegerField;
    QrClientNOMEENT: TWideStringField;
    QrSenderCodigo: TIntegerField;
    QrSenderNOMEENT: TWideStringField;
    QrProtocolosNOMETIPO: TWideStringField;
    QrProtocolosNOME_CLIENT: TWideStringField;
    QrProtocolosNOME_SENDER: TWideStringField;
    QrProtocoPak: TmySQLQuery;
    DsProtocoPak: TDataSource;
    QrProtocoPakCodigo: TIntegerField;
    QrProtocoPakControle: TIntegerField;
    QrProtocoPakDataI: TDateField;
    QrProtocoPakDataL: TDateField;
    QrProtocoPakDataF: TDateField;
    QrProtocoPakLk: TIntegerField;
    QrProtocoPakDataCad: TDateField;
    QrProtocoPakDataAlt: TDateField;
    QrProtocoPakUserCad: TIntegerField;
    QrProtocoPakUserAlt: TIntegerField;
    QrProtocoPakAlterWeb: TSmallintField;
    QrProtocoPakAtivo: TSmallintField;
    PMTarefa: TPopupMenu;
    Crianovatarefa1: TMenuItem;
    Alteratarefaatual1: TMenuItem;
    Excluitarefaatual1: TMenuItem;
    PMLote: TPopupMenu;
    Crianovolote1: TMenuItem;
    Alteraloteatual1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    PMProtocolo: TPopupMenu;
    QrProtocoPakMez: TIntegerField;
    QrProtPakIts_: TmySQLQuery;
    DsProtPakIts: TDataSource;
    QrProtPakIts_Codigo: TIntegerField;
    QrProtPakIts_Controle: TIntegerField;
    QrProtPakIts_Conta: TIntegerField;
    QrProtPakIts_DataE: TDateField;
    QrProtPakIts_DataD: TDateField;
    QrProtPakIts_Cancelado: TIntegerField;
    QrProtPakIts_Motivo: TIntegerField;
    QrProtPakIts_Lk: TIntegerField;
    QrProtPakIts_DataCad: TDateField;
    QrProtPakIts_DataAlt: TDateField;
    QrProtPakIts_UserCad: TIntegerField;
    QrProtPakIts_UserAlt: TIntegerField;
    QrProtPakIts_AlterWeb: TSmallintField;
    QrProtPakIts_Ativo: TSmallintField;
    QrProtPakIts_ID_Cod1: TIntegerField;
    QrProtPakIts_ID_Cod2: TIntegerField;
    QrProtPakIts_ID_Cod3: TIntegerField;
    QrProtPakIts_ID_Cod4: TIntegerField;
    QrProtocoPakMES: TWideStringField;
    QrProtocoPakDATAF_TXT: TWideStringField;
    QrProtPakIts_Link_ID: TIntegerField;
    QrProtPakIts_DATAE_TXT: TWideStringField;
    QrProtPakIts_DATAD_TXT: TWideStringField;
    QrProtPakIts_NOMEMOTIVO: TWideStringField;
    QrProtPakIts_CANCELADO_TXT: TWideStringField;
    QrProtPakIts_Unidade: TWideStringField;
    QrProtPakIts_NOMECLI_INT: TWideStringField;
    QrProtPakIts_NOMECLIENTE: TWideStringField;
    QrProtPakIts_CliInt: TIntegerField;
    QrProtPakIts_Cliente: TIntegerField;
    QrProtPakIts_Fornece: TIntegerField;
    QrProtPakIts_Periodo: TIntegerField;
    QrProtPakIts_Lancto: TIntegerField;
    QrProtPakIts_Depto: TIntegerField;
    QrProtPakIts_PERIODO_TXT: TWideStringField;
    N1: TMenuItem;
    Imprimir1: TMenuItem;
    frxLote_Unidade_: TfrxReport;
    frxDsProtPakIts: TfrxDBDataset;
    QrPropriet: TmySQLQuery;
    QrProprietCodigo: TIntegerField;
    QrProprietCadastro: TDateField;
    QrProprietENatal: TDateField;
    QrProprietPNatal: TDateField;
    QrProprietTipo: TSmallintField;
    QrProprietRespons1: TWideStringField;
    QrProprietNOMEDONO: TWideStringField;
    QrProprietCNPJ_CPF: TWideStringField;
    QrProprietIE_RG: TWideStringField;
    QrProprietNIRE_: TWideStringField;
    QrProprietRUA: TWideStringField;
    QrProprietCOMPL: TWideStringField;
    QrProprietBAIRRO: TWideStringField;
    QrProprietCIDADE: TWideStringField;
    QrProprietNOMELOGRAD: TWideStringField;
    QrProprietNOMEUF: TWideStringField;
    QrProprietPais: TWideStringField;
    QrProprietTE1: TWideStringField;
    QrProprietFAX: TWideStringField;
    QrProprietNUMERO_TXT: TWideStringField;
    QrProprietLNR: TWideStringField;
    QrProprietLN2: TWideStringField;
    QrProtPakIts_LNR: TWideStringField;
    QrProtPakIts_LN2: TWideStringField;
    frxDsProtocoPak: TfrxDBDataset;
    frxDsProtocolos: TfrxDBDataset;
    QrProtocoPakTITULO: TWideStringField;
    QrProtPakIts_Retorna: TSmallintField;
    QrProtocolosDef_Retorn: TIntegerField;
    DefinedatadaEntrega1: TMenuItem;
    DefinedatadoRetorno1: TMenuItem;
    N2: TMenuItem;
    Cancelaentrega1: TMenuItem;
    Excluiprotocolo1: TMenuItem;
    QrAbertos: TmySQLQuery;
    DsAbertos: TDataSource;
    QrAbertosNOMEPROTOCOLO: TWideStringField;
    QrAbertosUnidade: TWideStringField;
    QrAbertosPERIODO_TXT: TWideStringField;
    QrAbertosNOMECLI_INT: TWideStringField;
    QrAbertosNOMECLIENTE: TWideStringField;
    QrAbertosNOMEMOTIVO: TWideStringField;
    QrAbertosCodigo: TIntegerField;
    QrAbertosControle: TIntegerField;
    QrAbertosConta: TIntegerField;
    QrAbertosDataE: TDateField;
    QrAbertosDataD: TDateField;
    QrAbertosRetorna: TSmallintField;
    QrAbertosCancelado: TIntegerField;
    QrAbertosMotivo: TIntegerField;
    QrAbertosLk: TIntegerField;
    QrAbertosDataCad: TDateField;
    QrAbertosDataAlt: TDateField;
    QrAbertosUserCad: TIntegerField;
    QrAbertosUserAlt: TIntegerField;
    QrAbertosAlterWeb: TSmallintField;
    QrAbertosAtivo: TSmallintField;
    QrAbertosID_Cod1: TIntegerField;
    QrAbertosID_Cod2: TIntegerField;
    QrAbertosID_Cod3: TIntegerField;
    QrAbertosID_Cod4: TIntegerField;
    QrAbertosLink_ID: TIntegerField;
    QrAbertosCliInt: TIntegerField;
    QrAbertosCliente: TIntegerField;
    QrAbertosFornece: TIntegerField;
    QrAbertosPeriodo: TIntegerField;
    QrAbertosLancto: TIntegerField;
    QrAbertosDepto: TIntegerField;
    QrAbertosCANCELADO_TXT: TWideStringField;
    QrAbertosDATAE_TXT: TWideStringField;
    QrAbertosDATAD_TXT: TWideStringField;
    PMProtocolo2: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    QrLocPak: TmySQLQuery;
    QrLocPakCodigo: TIntegerField;
    QrProtocoPakCNAB_Cfg: TIntegerField;
    GeraarquivoremessaCNAB1: TMenuItem;
    QrProtPakIts_Valor: TFloatField;
    QrProtPakIts_Vencto: TDateField;
    QrProtPakIts_VENCTO_TXT: TWideStringField;
    QrProtPakIts_MoraDiaVal: TFloatField;
    QrProtPakIts_MultaVal: TFloatField;
    QrProtPakIts_Cedente: TIntegerField;
    QrSeqArq: TmySQLQuery;
    QrProtocoPakSeqArq: TIntegerField;
    Zerarnmero1: TMenuItem;
    N3: TMenuItem;
    QrAbertosDocum: TFloatField;
    QrProtPakIts_Docum: TFloatField;
    QrProtocolosPreEmeio: TIntegerField;
    QrProtocolosNOMEPREEMEIO: TWideStringField;
    QrPreEmail: TmySQLQuery;
    DsPreEmail: TDataSource;
    QrPreEmailCodigo: TIntegerField;
    QrPreEmailNome: TWideStringField;
    Circulaodedocumentos1: TMenuItem;
    frxLote_Circula1L1: TfrxReport;
    QrProtPakIts_ComoConf: TSmallintField;
    QrProtPakIts_SerieCH: TWideStringField;
    Incluiprotocolomanual1: TMenuItem;
    N4: TMenuItem;
    Alteraprotocolomanual1: TMenuItem;
    QrProtPakIts_Manual: TIntegerField;
    QrProtPakIts_Texto: TWideStringField;
    QrProtPakIts_NOMEMANUAL: TWideStringField;
    QrCircula: TmySQLQuery;
    frxDsCircula: TfrxDBDataset;
    QrCirculaManual: TIntegerField;
    QrCirculaSerieCH: TWideStringField;
    QrCirculaDocum: TFloatField;
    QrCirculaNOMEOCORMAN: TWideStringField;
    QrCirculaIts: TmySQLQuery;
    frxDsCirculaIts: TfrxDBDataset;
    QrCirculaItsConta: TIntegerField;
    QrCirculaItsRETORNA_TXT: TWideStringField;
    QrCirculaItsLancto: TIntegerField;
    QrCirculaItsValor: TFloatField;
    QrCirculaItsTEXTO: TWideStringField;
    QrCirculaItsVENCTO_TXT: TWideStringField;
    QrCirculaItsMesAno: TWideStringField;
    QrCirculaItsUNIDADE: TWideStringField;
    Ordenarporunidade1: TMenuItem;
    OrdenarpeloNomedoProprietrio1: TMenuItem;
    QrProtPakIts_Retornou: TIntegerField;
    QrProtPakIts_Saiu: TIntegerField;
    QrProtPakIts_Recebeu: TIntegerField;
    QrProtPakIts_DataSai: TDateTimeField;
    QrProtPakIts_DataRec: TDateTimeField;
    QrProtPakIts_DataRet: TDateTimeField;
    QrProtPakIts_LimiteSai: TDateField;
    QrProtPakIts_LimiteRem: TDateField;
    QrProtPakIts_LimiteRet: TDateField;
    QrProtPakIts_ROTACLI: TWideStringField;
    Ordenerpelarotadeentrega1: TMenuItem;
    PMImprime: TPopupMenu;
    EntregadeBloquetos2: TMenuItem;
    OrdenerpelaRotadeentrega2: TMenuItem;
    OrdenarpelonomedoProprietrio2: TMenuItem;
    OrdenarpelaUnidade1: TMenuItem;
    Circulaodedocumentos2: TMenuItem;
    QrCliInt: TmySQLQuery;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMECLI: TWideStringField;
    QrCliIntCliInt: TIntegerField;
    DsCliInt: TDataSource;
    QrCliProt: TmySQLQuery;
    DsCliProt: TDataSource;
    QrCliProtNOMETIPO: TWideStringField;
    QrCliProtNOMEPREEMEIO: TWideStringField;
    QrCliProtCodigo: TIntegerField;
    QrCliProtNome: TWideStringField;
    QrCliProtTipo: TIntegerField;
    QrCliProtDef_Client: TIntegerField;
    QrCliProtDef_Sender: TIntegerField;
    QrCliProtLk: TIntegerField;
    QrCliProtDataCad: TDateField;
    QrCliProtDataAlt: TDateField;
    QrCliProtUserCad: TIntegerField;
    QrCliProtUserAlt: TIntegerField;
    QrCliProtAlterWeb: TSmallintField;
    QrCliProtAtivo: TSmallintField;
    QrCliProtDef_Retorn: TIntegerField;
    QrCliProtPreEmeio: TIntegerField;
    QrCliProtNOME_SENDER: TWideStringField;
    PnGeral: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    dmkDBGrid3: TdmkDBGrid;
    Panel8: TPanel;
    BtProtocolo2: TBitBtn;
    Panel9: TPanel;
    BitBtn4: TBitBtn;
    TabSheet2: TTabSheet;
    PainelDados: TPanel;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    RGTipo: TRadioGroup;
    GroupBox1: TGroupBox;
    LaCliente: TLabel;
    LaDef_Sender: TLabel;
    LaPreEmeio: TLabel;
    CkDef_Retorn: TCheckBox;
    EdDef_Client: TdmkEditCB;
    CBDef_Client: TdmkDBLookupComboBox;
    EdDef_Sender: TdmkEditCB;
    CBDef_Sender: TdmkDBLookupComboBox;
    EdPreEmeio: TdmkEditCB;
    CBPreEmeio: TdmkDBLookupComboBox;
    QrCircul2: TmySQLQuery;
    frxDsCircul2: TfrxDBDataset;
    frxLote_CirculaG: TfrxReport;
    frxLote_Entidade_: TfrxReport;
    QrProtPakIts: TmySQLQuery;
    QrProtPakItsCodigo: TIntegerField;
    QrProtPakItsControle: TIntegerField;
    QrProtPakItsConta: TIntegerField;
    QrProtPakItsDataE: TDateField;
    QrProtPakItsDataD: TDateField;
    QrProtPakItsCancelado: TIntegerField;
    QrProtPakItsMotivo: TIntegerField;
    QrProtPakItsLk: TIntegerField;
    QrProtPakItsDataCad: TDateField;
    QrProtPakItsDataAlt: TDateField;
    QrProtPakItsUserCad: TIntegerField;
    QrProtPakItsUserAlt: TIntegerField;
    QrProtPakItsAlterWeb: TSmallintField;
    QrProtPakItsAtivo: TSmallintField;
    QrProtPakItsLink_ID: TIntegerField;
    QrProtPakItsID_Cod1: TIntegerField;
    QrProtPakItsID_Cod2: TIntegerField;
    QrProtPakItsID_Cod3: TIntegerField;
    QrProtPakItsID_Cod4: TIntegerField;
    QrProtPakItsDATAE_TXT: TWideStringField;
    QrProtPakItsDATAD_TXT: TWideStringField;
    QrProtPakItsNOMEMOTIVO: TWideStringField;
    QrProtPakItsCANCELADO_TXT: TWideStringField;
    QrProtPakItsUnidade: TWideStringField;
    QrProtPakItsNOMECLI_INT: TWideStringField;
    QrProtPakItsNOMECLIENTE: TWideStringField;
    QrProtPakItsCliInt: TIntegerField;
    QrProtPakItsCliente: TIntegerField;
    QrProtPakItsFornece: TIntegerField;
    QrProtPakItsPeriodo: TIntegerField;
    QrProtPakItsLancto: TIntegerField;
    QrProtPakItsDepto: TIntegerField;
    QrProtPakItsPERIODO_TXT: TWideStringField;
    QrProtPakItsRetorna: TSmallintField;
    QrProtPakItsVencto: TDateField;
    QrProtPakItsVENCTO_TXT: TWideStringField;
    QrProtPakItsMoraDiaVal: TFloatField;
    QrProtPakItsMultaVal: TFloatField;
    QrProtPakItsCedente: TIntegerField;
    QrProtPakItsDocum: TFloatField;
    QrProtPakItsComoConf: TSmallintField;
    QrProtPakItsSerieCH: TWideStringField;
    QrProtPakItsManual: TIntegerField;
    QrProtPakItsTexto: TWideStringField;
    QrProtPakItsNOMEMANUAL: TWideStringField;
    QrProtPakItsRetornou: TIntegerField;
    QrProtPakItsSaiu: TIntegerField;
    QrProtPakItsRecebeu: TIntegerField;
    QrProtPakItsDataSai: TDateTimeField;
    QrProtPakItsDataRec: TDateTimeField;
    QrProtPakItsDataRet: TDateTimeField;
    QrProtPakItsLimiteSai: TDateField;
    QrProtPakItsLimiteRem: TDateField;
    QrProtPakItsLimiteRet: TDateField;
    QrProtPakItsROTACLI: TWideStringField;
    QrProtPakItsSERIE_DOCUM: TWideStringField;
    Panel15: TPanel;
    Panel4: TPanel;
    Panel6: TPanel;
    CkAbertos: TCheckBox;
    DBGrid1: TDBGrid;
    Panel16: TPanel;
    PainelData: TPanel;
    Label2: TLabel;
    Label5: TLabel;
    Label1: TLabel;
    Label6: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Panel10: TPanel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label12: TLabel;
    dmkEdHoraI: TdmkEdit;
    dmkEdHoraF: TdmkEdit;
    dmkEdTempo: TdmkEdit;
    DBEdit4: TDBEdit;
    IncluiProtocoloBases1: TMenuItem;
    PMNumero: TPopupMenu;
    arefa1: TMenuItem;
    Lote1: TMenuItem;
    Protocolo1: TMenuItem;
    QrLoc: TmySQLQuery;
    QrLocCodigo: TIntegerField;
    QrLocControle: TIntegerField;
    frxLote_Unidade_1: TfrxReport;
    frxLote_Entidade_1: TfrxReport;
    frxLote_Unidade_2: TfrxReport;
    frxLote_Entidade_2: TfrxReport;
    ImprimirEntregadeboletosModelo21: TMenuItem;
    OrdenarpelaUnidade2: TMenuItem;
    OrdenarpelonomedoProprietrio3: TMenuItem;
    OrdenerpelaRotadeentrega3: TMenuItem;
    Label13: TLabel;
    EdDescricao: TdmkEdit;
    QrProtocolosDescricao: TWideStringField;
    QrProtocolosPRINT_DESCRI: TWideStringField;
    QrLocPakCliInt: TIntegerField;
    EdSigla: TdmkEdit;
    Label14: TLabel;
    QrProtocolosSigla: TWideStringField;
    Label16: TLabel;
    DBEdit5: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel17: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    frxLote_Circula1I1: TfrxReport;
    QrCirculaItsEAN128: TWideStringField;
    QrProtocolosCliInt: TIntegerField;
    QrProtocoPakEAN128: TWideStringField;
    QrProprietNUMERO: TFloatField;
    QrProprietLograd: TFloatField;
    QrProprietCEP: TFloatField;
    IncluiboletoscomregistroCR1: TMenuItem;
    frxLote_Unidade_3: TfrxReport;
    ImprimirEntregadeboletosModelo31: TMenuItem;
    OrdenarpelaUnidade3: TMenuItem;
    OrdenarpelonomedoProprietrio4: TMenuItem;
    OrdenerpelaRotadeentrega4: TMenuItem;
    frxLote_Entidade_3: TfrxReport;
    GBRodaPe: TGroupBox;
    Panel14: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtTarefa: TBitBtn;
    BtLote: TBitBtn;
    BtProtocolo: TBitBtn;
    dmkEdX: TdmkEdit;
    Label3: TLabel;
    TabSheet3: TTabSheet;
    PnFast: TPanel;
    Panel11: TPanel;
    SpeedButton5: TSpeedButton;
    EdCliInt: TEdit;
    dmkDBGrid1: TdmkDBGrid;
    Splitter1: TSplitter;
    Panel1: TPanel;
    dmkDBGrid2: TdmkDBGrid;
    Panel13: TPanel;
    EdCliProt: TEdit;
    CkSohAtivos: TCheckBox;
    DBGrid2: TDBGrid;
    QrProtPakItsLN2: TWideStringField;
    QrProtPakItsLNR: TWideStringField;
    QrProtPakItsValor: TFloatField;
    QrCircula3: TmySQLQuery;
    QrCircula3ID_Cod2: TIntegerField;
    QrCircula3NO_ID_Cod2: TWideStringField;
    frxDsCircula3: TfrxDBDataset;
    frxLote_Circula3L1: TfrxReport;
    QrCirculaIts3: TmySQLQuery;
    QrCirculaIts3Conta: TIntegerField;
    QrCirculaIts3RETORNA_TXT: TWideStringField;
    QrCirculaIts3OSCab: TIntegerField;
    QrCirculaIts3Valor: TFloatField;
    QrCirculaIts3TEXTO: TWideStringField;
    QrCirculaIts3VENCTO_TXT: TWideStringField;
    QrCirculaIts3NO_CLI: TWideStringField;
    frxDsCirculaIts3: TfrxDBDataset;
    QrProtocoPakTITULO3: TWideStringField;
    QrTpTaref: TmySQLQuery;
    QrTpTarefIDLink: TFloatField;
    QrTpTarefITENS: TFloatField;
    QrCirculaIts3Cliente: TIntegerField;
    frxLote_Circula3I1: TfrxReport;
    QrCirculaIts3EAN128: TWideStringField;
    QrProtocoPakEAN128_3: TWideStringField;
    ImprimirEntregadeboletosModelo41: TMenuItem;
    OrdenarpelaUnidade4: TMenuItem;
    OrdenarpelonomedoProprietrio5: TMenuItem;
    OrdenerpelaRotadeentrega5: TMenuItem;
    frxLote_Entidade_4: TfrxReport;
    frxLote_Unidade_4: TfrxReport;
    QrProtPakItsDataSai_TXT: TWideStringField;
    QrProtPakItsDataRec_TXT: TWideStringField;
    QrProtPakItsDataRet_TXT: TWideStringField;
    OrdenarpeloVencimento1: TMenuItem;
    OrdenarpeloVencimento2: TMenuItem;
    OrdenarpeloVencimento3: TMenuItem;
    OrdenarpeloVencimento4: TMenuItem;
    OrdenarpeloProtocolo1: TMenuItem;
    OrdenarpeloProtocolo2: TMenuItem;
    OrdenarpeloProtocolo3: TMenuItem;
    OrdenarpeloProtocolo4: TMenuItem;
    OrdernarpeloLote1: TMenuItem;
    SBDef_Sender: TSpeedButton;
    LaTotal: TLabel;
    QrProtocoPakDATAL_TXT: TWideStringField;
    BtProtoGer: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrProtocolosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrProtocolosBeforeOpen(DataSet: TDataSet);
    procedure QrProtocolosBeforeClose(DataSet: TDataSet);
    procedure QrProtocolosAfterScroll(DataSet: TDataSet);
    procedure Crianovatarefa1Click(Sender: TObject);
    procedure BtTarefaClick(Sender: TObject);
    procedure Alteratarefaatual1Click(Sender: TObject);
    procedure BtLoteClick(Sender: TObject);
    procedure QrProtocoPakBeforeClose(DataSet: TDataSet);
    procedure QrProtocoPakAfterOpen(DataSet: TDataSet);
    procedure Crianovolote1Click(Sender: TObject);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure QrProtocoPakAfterScroll(DataSet: TDataSet);
    procedure QrProprietCalcFields(DataSet: TDataSet);
    procedure QrProtocoPakCalcFields(DataSet: TDataSet);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Excluiprotocolo1Click(Sender: TObject);
    procedure DefinedatadaEntrega1Click(Sender: TObject);
    procedure DefinedatadoRetorno1Click(Sender: TObject);
    procedure Cancelaentrega1Click(Sender: TObject);
    procedure DBGrid2ColEnter(Sender: TObject);
    procedure DBGrid2ColExit(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure QrAbertosBeforeClose(DataSet: TDataSet);
    procedure QrAbertosAfterOpen(DataSet: TDataSet);
    procedure BtProtocoloClick(Sender: TObject);
    procedure BtProtocolo2Click(Sender: TObject);
    procedure CkAbertosClick(Sender: TObject);
    procedure PMLotePopup(Sender: TObject);
    procedure Excluiloteatual1Click(Sender: TObject);
    procedure GeraarquivoremessaCNAB1Click(Sender: TObject);
    procedure Zerarnmero1Click(Sender: TObject);
    procedure Incluiprotocolomanual1Click(Sender: TObject);
    procedure Alteraprotocolomanual1Click(Sender: TObject);
    procedure PMProtocoloPopup(Sender: TObject);
    procedure Circulaodedocumentos1Click(Sender: TObject);
    procedure QrCirculaAfterScroll(DataSet: TDataSet);
    procedure Ordenarporunidade1Click(Sender: TObject);
    procedure OrdenarpeloNomedoProprietrio1Click(Sender: TObject);
    procedure Ordenerpelarotadeentrega1Click(Sender: TObject);
    procedure OrdenarpelaUnidade1Click(Sender: TObject);
    procedure OrdenarpelonomedoProprietrio2Click(Sender: TObject);
    procedure OrdenerpelaRotadeentrega2Click(Sender: TObject);
    procedure Circulaodedocumentos2Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure QrCliIntAfterScroll(DataSet: TDataSet);
    procedure EdCliProtChange(Sender: TObject);
    procedure QrCliProtAfterScroll(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure QrProtPakItsCalcFields(DataSet: TDataSet);
    procedure IncluiProtocoloBases1Click(Sender: TObject);
    procedure arefa1Click(Sender: TObject);
    procedure Lote1Click(Sender: TObject);
    procedure Protocolo1Click(Sender: TObject);
    procedure OrdenarpelaUnidade2Click(Sender: TObject);
    procedure OrdenarpelonomedoProprietrio3Click(Sender: TObject);
    procedure OrdenerpelaRotadeentrega3Click(Sender: TObject);
    procedure QrProtocolosCalcFields(DataSet: TDataSet);
    procedure QrCirculaItsCalcFields(DataSet: TDataSet);
    procedure IncluiboletoscomregistroCR1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure OrdenarpelaUnidade3Click(Sender: TObject);
    procedure OrdenarpelonomedoProprietrio4Click(Sender: TObject);
    procedure OrdenerpelaRotadeentrega4Click(Sender: TObject);
    procedure dmkDBGrid2DblClick(Sender: TObject);
    procedure PMTarefaPopup(Sender: TObject);
    procedure frxLote_Entidade_1GetValue(const VarName: string;
      var Value: Variant);
    procedure QrCircula3BeforeClose(DataSet: TDataSet);
    procedure QrCircula3AfterScroll(DataSet: TDataSet);
    procedure QrCirculaIts3CalcFields(DataSet: TDataSet);
    procedure OrdenarpelaUnidade4Click(Sender: TObject);
    procedure OrdenarpelonomedoProprietrio5Click(Sender: TObject);
    procedure OrdenerpelaRotadeentrega5Click(Sender: TObject);
    procedure OrdenarpeloVencimento1Click(Sender: TObject);
    procedure OrdenarpeloVencimento2Click(Sender: TObject);
    procedure OrdenarpeloVencimento3Click(Sender: TObject);
    procedure OrdenarpeloVencimento4Click(Sender: TObject);
    procedure OrdenarpeloProtocolo1Click(Sender: TObject);
    procedure OrdenarpeloProtocolo2Click(Sender: TObject);
    procedure OrdenarpeloProtocolo3Click(Sender: TObject);
    procedure OrdenarpeloProtocolo4Click(Sender: TObject);
    procedure OrdernarpeloLote1Click(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure SBDef_SenderClick(Sender: TObject);
    procedure QrProtPakItsAfterScroll(DataSet: TDataSet);
    procedure QrProtPakItsBeforeClose(DataSet: TDataSet);
    procedure BtProtoGerClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenAbertos();
    procedure ReopenCliInt();
    procedure ReopenCliProt(Protocolo: Integer);
    procedure LocalizaLote(Lote: Integer);
    procedure ConfiguraCampos(TipoProt: Integer);
    // C�lculos Gerais
    function DataDesconto(Item: Integer): TDateTime;
    function ValrDesconto(Item: Integer): Double;
    function DataMulta(Vencto: TDateTime; Dias: Integer): TDateTime;
    // Espec�ficos de cada layout de cada banco
    // 399
    function  GeraCampo551_399_MODULO_I_2009_10(Grade: TStringGrid;
              Linha: Integer; MultaPerc, MultaValr, DiasMult, DataMult,
              JurosPerc, JurosValr, DiasMora, DataMora: Double): String;
    function  GeraCampo720_399_MODULO_I_2009_10(Grade: TStringGrid;
              Linha: Integer): String;
    function  GeraCampo647_399_MODULO_I_2009_10(Grade: TStringGrid;
              Linha: Integer): String;
  public
    { Public declarations }
    FLocLote: Integer;

    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenProtocoPak(Controle: Integer);
    procedure ReopenProtPakIts(Conta: Integer);
  end;

var
  FmProtocolos: TFmProtocolos;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ProtocoPak, MyDBCheck, MyVCLSkin, GetData, SelCod,
  {$IfNDef NO_CNAB} UnBco_Rem, CNAB_Rem, ModuleBco, UnBancos, {$EndIf}
  {$IfDef IMP_BLOQLCT} UnBloquetos, {$EndIf}
  // 2014-08-22 Blue Derm ???
  {$IfNDef S_BLQ} BloSemLot, {$EndIf}
  {$IfNDef SemProtocolo} Protocolo, {$EndIf}
  {$IfNDef NO_FINANCEIRO} UnFinanceiro, {$EndIf}
  UCreate, ProtocoMan, ModuleGeral, ProtocoManBase, CustomFR3Imp,
  DmkDAC_PF, MyListas, UnProtocoUnit;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmProtocolos.LocalizaLote(Lote: Integer);
var
  Achou: Boolean;
begin
  QrLocPak.Close;
  QrLocPak.Params[0].AsInteger := Lote;
  UMyMod.AbreQuery(QrLocPak, Dmod.MyDB);
  if QrLocPakCodigo.Value <> 0 then
  begin
    LocCod(QrLocPakCodigo.Value, QrLocPakCodigo.Value);
    Achou := QrProtocoPak.Locate('Controle', Lote, []);
    if not Achou then
    begin
      CkAbertos.Checked := False;
      Achou := QrProtocoPak.Locate('Controle', Lote, []);
    end;
    // else Achou := True;
    if not Achou then
    Geral.MB_Aviso('A tarefa do lote ' + IntToStr(Lote) +
    ' foi localizada mas o lote n�o!') else
      PageControl1.ActivePageIndex := 1;
  end else
    Geral.MB_Aviso('N�o foi localizada a tarefa do lote ' +
    IntToStr(Lote) + '!');
end;

procedure TFmProtocolos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmProtocolos.Lote1Click(Sender: TObject);
var
  LoteTxt: String;
  LoteInt: Integer;
begin
  LoteTxt := '0';
  if InputQuery('Lacasliza��o de Lote', 'Informe o n�mero do lote' , LoteTxt) then
  begin
    LoteInt := Geral.IMV(LoteTxt);
    if LoteInt <> 0 then
      LocalizaLote(LoteInt);
  end;
end;

procedure TFmProtocolos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrProtocolosCodigo.Value, LaRegistro.Caption[2]);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmProtocolos.DefParams;
begin
  VAR_GOTOTABELA := 'protocolos';
  VAR_GOTOMYSQLTABLE := QrProtocolos;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT ELT(ptc.Tipo+1,"ND - N�o definido",');
  VAR_SQLx.Add('  "EB - Entrega de bloquetos",');
  VAR_SQLx.Add('  "CE - Circula��o eletr�nica (e-mail)",');
  VAR_SQLx.Add('  "EM - Entrega de material de consumo",');
  VAR_SQLx.Add('  "CR - Cobran�a registrada (banc�ria)",');
  VAR_SQLx.Add('  "CD - Circula��o de documentos"');
  VAR_SQLx.Add('  ) NOMETIPO,');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('IF(d_c.Tipo=0, d_c.RazaoSocial,d_c.Nome) NOME_CLIENT,');
  VAR_SQLx.Add('IF(d_s.Tipo=0, d_s.RazaoSocial,d_s.Nome) NOME_SENDER,');
{$IfNDef NO_USE_EMAILDMK}
  VAR_SQLx.Add('pre.Nome NOMEPREEMEIO, ');
{$Else}
  VAR_SQLx.Add('"" NOMEPREEMEIO, ');
{$EndIf}
  VAR_SQLx.Add('d_c.CliInt, ptc.*');
  VAR_SQLx.Add('FROM protocolos ptc');
  VAR_SQLx.Add('LEFT JOIN entidades d_c ON d_c.Codigo=ptc.Def_Client');
  VAR_SQLx.Add('LEFT JOIN entidades d_s ON d_s.Codigo=ptc.Def_Sender');
{$IfNDef NO_USE_EMAILDMK}
  VAR_SQLx.Add('LEFT JOIN preemail  pre ON pre.Codigo=ptc.PreEmeio');
{$EndIf}
  VAR_SQLx.Add('WHERE ptc.Codigo > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND ptc.Codigo=:P0');
  //
  VAR_SQLa.Add('AND ptc.Nome Like :P0');
  //
end;

procedure TFmProtocolos.dmkDBGrid2DblClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
end;

procedure TFmProtocolos.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible  := True;
      PainelEdita.Visible  := False;
      TabSheet1.TabVisible := True;
      TabSheet3.TabVisible := True;
    end;
    1:
    begin
      PainelEdita.Visible        := True;
      PainelDados.Visible        := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text            := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text              := '';
        EdSigla.Text             := '';
        EdDescricao.Text         := '';
        RGTipo.ItemIndex         := 0;
        EdDef_Client.Text        := '';
        CBDef_Client.KeyValue    := 0;
        EdDef_Sender.Text        := '';
        CBDef_Sender.KeyValue    := 0;
        CkDef_Retorn.Checked     := False;
        EdPreEmeio.Text          := '';
        CBPreEmeio.KeyValue      := Null;
      end else begin
        EdCodigo.Text            := DBEdCodigo.Text;
        EdNome.Text              := DBEdNome.Text;
        EdSigla.Text             := QrProtocolosSigla.Value;
        EdDescricao.Text         := QrProtocolosDescricao.Value;
        RGTipo.ItemIndex         := QrProtocolosTipo.Value;
        EdDef_Client.Text        := IntToStr(QrProtocolosDef_Client.Value);
        CBDef_Client.KeyValue    := QrProtocolosDef_Client.Value;
        EdDef_Sender.Text        := IntToStr(QrProtocolosDef_Sender.Value);
        CBDef_Sender.KeyValue    := QrProtocolosDef_Sender.Value;
        CkDef_Retorn.Checked     := Geral.IntToBool_0(QrProtocolosDef_Retorn.Value);
        EdPreEmeio.ValueVariant  := QrProtocolosPreEmeio.Value;
        CBPreEmeio.KeyValue      := QrProtocolosPreEmeio.Value;
      end;
      ConfiguraCampos(RGTipo.ItemIndex);
      EdNome.SetFocus;
      TabSheet1.TabVisible := False;
      TabSheet3.TabVisible := False;
    end;
    2:
    begin
      if DBCheck.CriaFm(TFmProtocoPak, FmProtocoPak, afmoNegarComAviso) then
      begin
        if SQLType = stIns then
        begin
          FmProtocoPak.dmkEdDataI.ValueVariant := Date;
          FmProtocoPak.dmkEdDataL.ValueVariant := Date;
          FmProtocoPak.EdMes.Text := Geral.FDT(Date, 14);
          FmProtocoPak.dmkEdCNAB_Cfg.ValueVariant := NULL;
          FmProtocoPak.dmkCBCNAB_Cfg.KeyValue := NULL;
        end else begin
          FmProtocoPak.dmkEdDataI.ValueVariant    := QrProtocoPakDataI.Value;
          FmProtocoPak.dmkEdDataL.ValueVariant    := QrProtocoPakDataL.Value;
          FmProtocoPak.EdMes.Text                 := dmkPF.MezToMesEAno(QrProtocoPakMez.Value);
          FmProtocoPak.dmkEdCNAB_Cfg.ValueVariant := QrProtocoPakCNAB_Cfg.Value;
          FmProtocoPak.dmkCBCNAB_Cfg.KeyValue     := QrProtocoPakCNAB_Cfg.Value;
        end;
        FmProtocoPak.ImgTipo.SQLType := SQLType;
        FmProtocoPak.FCodigo         := QrProtocolosCodigo.Value;
        FmProtocoPak.FControle       := QrProtocoPakControle.Value;
        FmProtocoPak.FTipo           := QrProtocolosTipo.Value;
        FmProtocoPak.FDef_Client     := QrProtocolosDef_Client.Value;
        //
        FmProtocoPak.ShowModal;
        if FmProtocoPak.FExecuted then
        begin
          LocCod(FmProtocoPak.FCodigo, FmProtocoPak.FCodigo);
          ReopenProtocoPak(FmProtocoPak.FControle);
        end;
        FmProtocoPak.Destroy;
      end;
    end;
    3:
    begin
      //
    end;
    else Geral.MB_Erro('A��o de Inclus�o/altera��o n�o definida!');
  end;
  if Mostra < 2 then
  begin
    ImgTipo.SQLType := SQLType;
    GOTOy.BotoesSb(ImgTipo.SQLType);
  end;
end;

procedure TFmProtocolos.OrdenarpelaUnidade1Click(Sender: TObject);
begin
  Ordenarporunidade1Click(Self);
end;

procedure TFmProtocolos.OrdenarpelaUnidade2Click(Sender: TObject);
var
  SortField: String;
begin
  SortField := QrProtPakits.SortFieldNames;
  QrProtPakits.SortFieldNames := 'Unidade';
  //if UpperCase(Application.Title) = 'SYNDIC' then
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_2, 'Lote de protocolos')
  else
    //Nada - Unidades somente para condom�nios
  //MyObjects.frxMostra(frxLote_Unidade_2, 'Lote de protocolos');
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenarpelaUnidade3Click(Sender: TObject);
var
  SortField: String;
begin
  SortField := QrProtPakits.SortFieldNames;
  QrProtPakits.SortFieldNames := 'Unidade';
  //if UpperCase(Application.Title) = 'SYNDIC' then
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_3, 'Lote de protocolos')
  else
    //Nada - Unidades somente para condom�nios
  //MyObjects.frxMostra(frxLote_Unidade_2, 'Lote de protocolos');
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenarpelaUnidade4Click(Sender: TObject);
var
  SortField: String;
  GH: TfrxGroupHeader;
begin
  SortField := QrProtPakits.SortFieldNames + ', Conta';
  QrProtPakits.SortFieldNames := 'Unidade, Conta';
  //if UpperCase(Application.Title) = 'SYNDIC' then
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_4, 'Lote de protocolos')
  else
  begin
    GH := frxLote_Entidade_4.FindObject('GH1') as TfrxGroupHeader;
    GH.Condition := 'frxDsProtPakIts."Unidade"';
    //
    MyObjects.frxMostra(frxLote_Entidade_4, 'Lote de protocolos');
  end;
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenarpeloNomedoProprietrio1Click(Sender: TObject);
var
  SortField: String;
begin
  SortField := QrProtPakits.SortFieldNames;
  QrProtPakits.SortFieldNames := 'NOMECLIENTE';
  //if UpperCase(Application.Title) = 'SYNDIC' then
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_1, 'Lote de protocolos')
  else
    MyObjects.frxMostra(frxLote_Entidade_1, 'Lote de protocolos');
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenarpelonomedoProprietrio2Click(Sender: TObject);
begin
  OrdenarpeloNomedoProprietrio1Click(Self);
end;

procedure TFmProtocolos.OrdenarpelonomedoProprietrio3Click(Sender: TObject);
var
  SortField: String;
begin
  SortField := QrProtPakits.SortFieldNames;
  QrProtPakits.SortFieldNames := 'NOMECLIENTE';
  //if UpperCase(Application.Title) = 'SYNDIC' then
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_2, 'Lote de protocolos')
  else
    MyObjects.frxMostra(frxLote_Entidade_2, 'Lote de protocolos');
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenarpelonomedoProprietrio4Click(Sender: TObject);
var
  SortField: String;
begin
  SortField := QrProtPakits.SortFieldNames;
  QrProtPakits.SortFieldNames := 'NOMECLIENTE';
  //if UpperCase(Application.Title) = 'SYNDIC' then
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_3, 'Lote de protocolos')
  else
    MyObjects.frxMostra(frxLote_Entidade_3, 'Lote de protocolos');
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenarpelonomedoProprietrio5Click(Sender: TObject);
var
  SortField: String;
  GH: TfrxGroupHeader;
begin
  SortField := QrProtPakits.SortFieldNames + ', Conta';
  QrProtPakits.SortFieldNames := 'NOMECLIENTE, Conta';
  if VAR_KIND_DEPTO = kdUH then
    //MyObjects.frxMostra(frxLote_Unidade_4, 'Lote de protocolos')
  else
  begin
    GH := frxLote_Entidade_4.FindObject('GH1') as TfrxGroupHeader;
    GH.Condition := 'frxDsProtPakIts."NOMECLIENTE"';
    //
    MyObjects.frxMostra(frxLote_Entidade_4, 'Lote de protocolos');
  end;
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenarpeloProtocolo1Click(Sender: TObject);
var
  SortField: String;
begin
  SortField := QrProtPakits.SortFieldNames;
  QrProtPakits.SortFieldNames := 'Conta';
  //
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_1, 'Lote de protocolos')
  else
    MyObjects.frxMostra(frxLote_Entidade_1, 'Lote de protocolos');
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenarpeloProtocolo2Click(Sender: TObject);
var
  SortField: String;
begin
  SortField := QrProtPakits.SortFieldNames;
  QrProtPakits.SortFieldNames := 'Conta';
  //
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_2, 'Lote de protocolos')
  else
    MyObjects.frxMostra(frxLote_Entidade_2, 'Lote de protocolos');
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenarpeloProtocolo3Click(Sender: TObject);
var
  SortField: String;
begin
  SortField := QrProtPakits.SortFieldNames;
  QrProtPakits.SortFieldNames := 'Conta';
  //
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_3, 'Lote de protocolos')
  else
    MyObjects.frxMostra(frxLote_Entidade_3, 'Lote de protocolos');
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenarpeloProtocolo4Click(Sender: TObject);
var
  SortField: String;
  GH: TfrxGroupHeader;
begin
  SortField := QrProtPakits.SortFieldNames;
  QrProtPakits.SortFieldNames := 'Conta';
  //
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_4, 'Lote de protocolos')
  else
  begin
    GH := frxLote_Entidade_4.FindObject('GH1') as TfrxGroupHeader;
    GH.Condition := 'frxDsProtPakIts."Conta"';
    //
    MyObjects.frxMostra(frxLote_Entidade_4, 'Lote de protocolos');
  end;
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenarpeloVencimento1Click(Sender: TObject);
var
  SortField: String;
begin
  SortField := QrProtPakits.SortFieldNames;
  QrProtPakits.SortFieldNames := 'Vencto';
  //
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_1, 'Lote de protocolos')
  else
    MyObjects.frxMostra(frxLote_Entidade_1, 'Lote de protocolos');
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenarpeloVencimento2Click(Sender: TObject);
var
  SortField: String;
begin
  SortField := QrProtPakits.SortFieldNames;
  QrProtPakits.SortFieldNames := 'Vencto';
  //
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_2, 'Lote de protocolos')
  else
    MyObjects.frxMostra(frxLote_Entidade_2, 'Lote de protocolos');
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenarpeloVencimento3Click(Sender: TObject);
var
  SortField: String;
begin
  SortField := QrProtPakits.SortFieldNames;
  QrProtPakits.SortFieldNames := 'Vencto';
  //
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_3, 'Lote de protocolos')
  else
    MyObjects.frxMostra(frxLote_Entidade_3, 'Lote de protocolos');
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenarpeloVencimento4Click(Sender: TObject);
var
  SortField: String;
  GH: TfrxGroupHeader;
begin
  SortField := QrProtPakits.SortFieldNames + ', Conta';
  QrProtPakits.SortFieldNames := 'Vencto, Conta';
  //
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_4, 'Lote de protocolos')
  else
  begin
    GH := frxLote_Entidade_4.FindObject('GH1') as TfrxGroupHeader;
    GH.Condition := 'frxDsProtPakIts."Vencto"';
    //
    MyObjects.frxMostra(frxLote_Entidade_4, 'Lote de protocolos');
  end;
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.Ordenarporunidade1Click(Sender: TObject);
var
  SortField: String;
begin
  SortField := QrProtPakits.SortFieldNames;
  QrProtPakits.SortFieldNames := 'Unidade';
  //
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_1, 'Lote de protocolos')
  else
    //Nada - Unidades somente para condom�nios
  //MyObjects.frxMostra(frxLote_Unidade_1, 'Lote de protocolos');
    Geral.MB_Aviso('Aplicativo sem uso de unidade!');
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.Ordenerpelarotadeentrega1Click(Sender: TObject);
var
  SortField: String;
begin
  SortField := QrProtPakits.SortFieldNames;
  QrProtPakits.SortFieldNames := 'ROTACLI';
  //
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_1, 'Lote de protocolos')
  else
    MyObjects.frxMostra(frxLote_Entidade_1, 'Lote de protocolos');
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenerpelaRotadeentrega2Click(Sender: TObject);
begin
  Ordenerpelarotadeentrega1Click(Self);
end;

procedure TFmProtocolos.OrdenerpelaRotadeentrega3Click(Sender: TObject);
var
  SortField: String;
begin
  SortField := QrProtPakits.SortFieldNames;
  QrProtPakits.SortFieldNames := 'ROTACLI';
  //
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_2, 'Lote de protocolos')
  else
    MyObjects.frxMostra(frxLote_Entidade_2, 'Lote de protocolos');
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenerpelaRotadeentrega4Click(Sender: TObject);
var
  SortField: String;
begin
  SortField := QrProtPakits.SortFieldNames;
  QrProtPakits.SortFieldNames := 'ROTACLI';
  //
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_3, 'Lote de protocolos')
  else
    MyObjects.frxMostra(frxLote_Entidade_3, 'Lote de protocolos');
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdenerpelaRotadeentrega5Click(Sender: TObject);
var
  SortField: String;
  GH: TfrxGroupHeader;
begin
  SortField := QrProtPakits.SortFieldNames + ', Conta';
  QrProtPakits.SortFieldNames := 'ROTACLI, Conta';
  //
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_4, 'Lote de protocolos')
  else
  begin
    GH := frxLote_Entidade_4.FindObject('GH1') as TfrxGroupHeader;
    GH.Condition := 'frxDsProtPakIts."ROTACLI"';
    //
    MyObjects.frxMostra(frxLote_Entidade_4, 'Lote de protocolos');
  end;
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.OrdernarpeloLote1Click(Sender: TObject);
var
  SortField: String;
  GH: TfrxGroupHeader;
begin
  SortField := QrProtPakits.SortFieldNames + ', Conta';
  QrProtPakits.SortFieldNames := 'Controle, Conta';
  //
  if VAR_KIND_DEPTO = kdUH then
    MyObjects.frxMostra(frxLote_Unidade_4, 'Lote de protocolos')
  else
  begin
    GH := frxLote_Entidade_4.FindObject('GH1') as TfrxGroupHeader;
    GH.Condition := 'frxDsProtPakIts."Controle"';
    //
    MyObjects.frxMostra(frxLote_Entidade_4, 'Lote de protocolos');
  end;
  QrProtPakits.SortFieldNames := SortField;
end;

procedure TFmProtocolos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmProtocolos.Alteraprotocolomanual1Click(Sender: TObject);
begin
  UMyMod.FormInsUpd_Show(TFmProtocoMan, FmProtocoMan, afmoLiberado,
    QrProtPakIts, stUpd);
end;

procedure TFmProtocolos.AlteraRegistro;
var
  Protocolos : Integer;
begin
  Protocolos := QrProtocolosCodigo.Value;
  if not UMyMod.SelLockY(Protocolos, Dmod.MyDB, 'Protocolos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Protocolos, Dmod.MyDB, 'Protocolos', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmProtocolos.IncluiboletoscomregistroCR1Click(Sender: TObject);
begin
{$IfDef IMP_BLOQLCT}
  if DBCheck.CriaFm(TFmBloSemLot, FmBloSemLot, afmoNegarComAviso) then
  begin
    FmBloSemLot.GBConfigPesq.Enabled    := False;
    FmBloSemLot.FProtocoPakCodigo       := QrProtocoPakCodigo.Value;
    FmBloSemLot.FProtocoPakControle     := QrProtocoPakControle.Value;
    FmBloSemLot.FQrIts                  := QrProtPakIts;
    FmBloSemLot.EdCNAB_Cfg.ValueVariant := QrProtocoPakCNAB_Cfg.Value;
    FmBloSemLot.CBCNAB_Cfg.KeyValue     := QrProtocoPakCNAB_Cfg.Value;
    FmBloSemLot.ReopenArreIts();
    FmBloSemLot.ReopenCNAB_Cfg(QrProtocolosCodigo.Value);
    //
    FmBloSemLot.ShowModal;
    FmBloSemLot.Destroy;
  end;
{$EndIf}
end;

procedure TFmProtocolos.Incluiprotocolomanual1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmProtocoMan, FmProtocoMan, afmoNegarComAviso) then
  begin
    FmProtocoMan.ShowModal;
    FmProtocoMan.Destroy;
  end;
end;

procedure TFmProtocolos.IncluiProtocoloBases1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmProtocoManBase, FmProtocoManBase, afmoNegarComAviso) then
  begin
    FmProtocoManBase.ShowModal;
    FmProtocoManBase.Destroy;
  end;
end;

procedure TFmProtocolos.IncluiRegistro;
var
  Cursor : TCursor;
  Protocolos : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Protocolos := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Protocolos', 'Protocolos', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Protocolos))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro(
      'Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, Protocolos);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmProtocolos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmProtocolos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmProtocolos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmProtocolos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmProtocolos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmProtocolos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmProtocolos.SpeedButton5Click(Sender: TObject);
begin
  ReopenCliInt();
end;

procedure TFmProtocolos.SBDef_SenderClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(EdDef_Sender.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO > 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrSender, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdDef_Sender, CBDef_Sender, QrSender, VAR_CADASTRO);
    EdDef_Sender.SetFocus;
  end;
end;

procedure TFmProtocolos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrProtocolosCodigo.Value;
  Close;
end;

procedure TFmProtocolos.BtConfirmaClick(Sender: TObject);
var
  Codigo, Tipo, Def_Client, Def_Sender, Def_Retorn, PreEmeio: Integer;
  Nome, Descricao, Sigla: String;
begin
  Nome := EdNome.Text;
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o.') then Exit;
  //
  Def_Client := EdDef_Client.ValueVariant;
  Def_Sender := EdDef_Sender.ValueVariant;
  Def_Retorn := Geral.BoolToInt(CkDef_Retorn.Checked);
  PreEmeio   := 0;
  Descricao  := EdDescricao.ValueVariant;
  Tipo       := RGTipo.ItemIndex;
  Sigla      := EdSigla.ValueVariant;
  //
  if MyObjects.FIC(Tipo = 0, RGTipo, 'Defina um tipo de Protocolo!') then Exit;
  if MyObjects.FIC(Def_Client = 0, EdDef_Client, 'Defina o cliente interno!') then Exit;
  //
  if Tipo = 2 then //E-mail
  begin
    PreEmeio := EdPreEmeio.ValueVariant;
    //
    if MyObjects.FIC(PreEmeio = 0, EdPreEmeio, 'Defina o pr�-email!') then Exit;
    //
    Def_Retorn := 0;
    Def_Sender := 0;
  end else
  if Tipo = 4 then //CR
  begin
    if MyObjects.FIC(Def_Sender = 0, EdDef_Sender, 'Defina a entidade banc�ria!') then Exit;
  end;
  {
  Codigo := UMyMod.BuscaEmLivreY_Def('MPP', 'Codigo', ImgTipo.SQLType, QrMPPCodigo.Value);
  }
  Codigo := Geral.IMV(EdCodigo.Text);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'Protocolos', False,
  [
    'Nome', 'Tipo', 'Def_Client', 'Def_Sender', 'Def_Retorn', 'PreEmeio',
    'Descricao', 'Sigla'
  ], ['Codigo'], [
    Nome, Tipo, Def_Client, Def_Sender, Def_Retorn, PreEmeio,
    Descricao, Sigla
  ], [Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Protocolos', 'Codigo');
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmProtocolos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Protocolos', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Protocolos', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Protocolos', 'Codigo');
end;

procedure TFmProtocolos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  try
    PageControl1.ActivePageIndex := 1;
    DBGrid2.Align                := alClient;
    Screen.Cursor                := crHourGlass;
    CriaOForm;
    UMyMod.AbreQuery(QrClient, Dmod.MyDB);
    UMyMod.AbreQuery(QrSender, Dmod.MyDB);
    //UMyMod.AbreQuery(QrPropriet, Dmod.MyDB);
{$IfNDef NO_USE_EMAILDMK}
    UMyMod.AbreQuery(QrPreEmail, Dmod.MyDB);
{$EndIf}
    //ReopenAbertos; Desbilitado para acelerar a abertura do form
    ReopenCliInt();
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmProtocolos.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNumero, SbNumero);
end;

procedure TFmProtocolos.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SBImprime);
end;

procedure TFmProtocolos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmProtocolos.SbNovoClick(Sender: TObject);
begin
  Circulaodedocumentos1Click(Self);
end;

procedure TFmProtocolos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmProtocolos.QrCircula3AfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCirculaIts3, Dmod.MyDB, [
  'SELECT ppi.Conta, IF(ppi.Retorna=0, "N�o", "Sim") RETORNA_TXT, ',
  'ppi.ID_Cod1 OSCab, ppi.Valor, ppi.Texto TEXTO, ',
  'IF(ppi.Vencto=0, "", DATE_FORMAT(ppi.Vencto, "%d/%m/%Y")) VENCTO_TXT, ',
  'ppi.Cliente, IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLI ',
  'FROM protpakits ppi ',
  'LEFT JOIN entidades ent ON ent.Codigo=ppi.Cliente',
  'WHERE ppi.Controle=' + Geral.FF0(QrProtocoPakControle.Value),
  'AND ppi.ID_Cod2=' + Geral.FF0(QrCircula3ID_Cod2.Value),
  '']);
end;

procedure TFmProtocolos.QrCircula3BeforeClose(DataSet: TDataSet);
begin
  QrCirculaIts3.Close;
end;

procedure TFmProtocolos.QrProtocolosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtLote.Enabled := QrProtocolos.RecordCount > 0;
end;

procedure TFmProtocolos.FormActivate(Sender: TObject);
var
  Achou: Boolean;
begin
  MyObjects.CorIniComponente();
  if FLocLote > 0 then
  begin
    QrLocPak.Close;
    QrLocPak.Params[0].AsInteger := FLocLote;
    UMyMod.AbreQuery(QrLocPak, Dmod.MyDB);
    if not QrCliInt.Locate('Codigo', QrLocPakCliInt.Value, []) then
    begin
      Geral.MB_Aviso('A entidade ' + FormatFloat('0', QrLocPakCliInt.Value) +
      ' do lote ' + FormatFloat('0', FLocLote) + ' n�o foi localizada!');
      Exit;
    end;
    if QrLocPakCodigo.Value <> 0 then
    begin
      LocCod(QrLocPakCodigo.Value, QrLocPakCodigo.Value);
      Achou := QrProtocoPak.Locate('Controle', FLocLote, []);
      if not Achou then
      begin
        CkAbertos.Checked := False;
        Achou := QrProtocoPak.Locate('Controle', FLocLote, []);
      end;
      // else Achou := True;
      if not Achou then Geral.MB_Aviso('A tarefa do lote ' +
      IntToStr(FLocLote) + ' foi localizada mas o lote n�o!') else
        PageControl1.ActivePageIndex := 1;
      FLocLote := 0;
    end else Geral.MB_Aviso('N�o foi localizada a tarefa do lote ' +
    IntToStr(FLocLote) + '!');
  end;
end;

procedure TFmProtocolos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrProtocolosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Protocolos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmProtocolos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProtocolos.frxLote_Entidade_1GetValue(const VarName: string;
  var Value: Variant);
var
  Docum: Double;
begin
  {$IfDef IMP_BLOQLCT}
  Docum := UBloquetos.ObtemDocumProtPakIts(QrProtPakItsConta.Value);
  {$Else}
  Docum := QrProtPakItsDocum.Value;
  {$EndIf}
  //
  if VarName = 'VARF_DOCUM' then
    Value := Docum;
end;

procedure TFmProtocolos.QrProtocolosBeforeOpen(DataSet: TDataSet);
begin
  QrProtocolosCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmProtocolos.QrProtocolosCalcFields(DataSet: TDataSet);
begin
  if Trim(QrProtocolosDescricao.Value) <> '' then
    QrProtocolosPRINT_DESCRI.Value := QrProtocolosDescricao.Value
  else
    QrProtocolosPRINT_DESCRI.Value := QrProtocolosNome.Value;
end;

procedure TFmProtocolos.EdCliProtChange(Sender: TObject);
begin
  ReopenCliProt(0);
end;

procedure TFmProtocolos.Circulaodedocumentos1Click(Sender: TObject);
  {$IfNDef NO_FINANCEIRO}
  procedure GeraParteSQL(TabLct, Ctrl: String);
  begin
    QrCircul2.SQL.Add('SELECT ppi.Manual, ppi.SerieCH, ppi.Docum, pto.Nome NOMEOCORMAN,');
    QrCircul2.SQL.Add('ppi.Conta, IF(ppi.Retorna=0, "N�o", "Sim") RETORNA_TXT,');
    QrCircul2.SQL.Add('ppi.Lancto, Valor, IF(ppi.Manual=0, lan.Descricao, ppi.Texto) TEXTO,');
    QrCircul2.SQL.Add('IF(ppi.Vencto=0, "", DATE_FORMAT(ppi.Vencto, "%d/%m/%Y")) VENCTO_TXT,');
    QrCircul2.SQL.Add('IF(ppi.Manual=0,');
    QrCircul2.SQL.Add('  IF(Mez=0,"",CONCAT(RIGHT(Mez, 2), "/",  LEFT(Mez + 200000, 4))),');
    QrCircul2.SQL.Add('  IF(Periodo=0,"",CONCAT(RIGHT(PERIOD_ADD(200000, ppi.Periodo), 2), "/",');
    QrCircul2.SQL.Add('  LEFT(PERIOD_ADD(200000, ppi.Periodo), 4)))) MesAno,');
    QrCircul2.SQL.Add('"" UNIDADE');
    QrCircul2.SQL.Add('FROM protpakits ppi');
    QrCircul2.SQL.Add('LEFT JOIN protocooco pto ON pto.Codigo=ppi.Manual');
    QrCircul2.SQL.Add('LEFT JOIN ' + TabLct + ' lan ON lan.Controle=ppi.Lancto');
    QrCircul2.SQL.Add('WHERE ppi.Controle=' + Ctrl);
    //QrCircul2.SQL.Add('ORDER BY ppi.Manual, ppi.SerieCH, ppi.Docum');
    QrCircul2.SQL.Add('');
  end;
var
  Impressao, ID_Link: Integer;
  DtEncer, DtMorto: TDateTime;
  Ctrl, TabLctA, TabLctB, TabLctD: String;
begin
  Screen.Cursor := crHourGlass;
  try
    DModG.Def_EM_ABD(TMeuDB, QrProtocolosDef_Client.Value,
    QrProtocolosCliInt.Value, DtEncer, DtMorto, TabLctA, TabLctB, TabLctD);
    //
    if VAR_KIND_DEPTO IN ([kdUH, kdOS1]) then
    begin
      Impressao := MyObjects.SelRadioGroup('Forma de Impress�o',
      'Mostrar o c�digo de barras', ['Apenas no LOTE', 'No LOTE e nos ITENS'], 1);
      // Ver o tipo de tarefa!
      UnDmkDAC_PF.AbreMySQLQuery0(QrTpTaref, Dmod.MyDB, [
      'SELECT IF(Link_ID = 0, 1, Link_ID) + 0.000 IDLink, ',
      'COUNT(Controle) + 0.000 ITENS ',
      'FROM protpakits ppi  ',
      'WHERE ppi.Controle=' + Geral.FF0(QrProtocoPakControle.Value),
      'GROUP BY Link_ID ',
      'ORDER BY IDLink DESC ',
      '']);
      ID_Link := Trunc(QrTpTarefIDLink.Value);
      //
      if (VAR_KIND_DEPTO = kdOS1) and (ID_Link = VAR_TIPO_LINK_ID_03_ORDEM_DE_SERVCO1) then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrCircula3, Dmod.MyDB, [
        'SELECT ppi.ID_Cod2, sta.Nome NO_ID_Cod2',
        'FROM protpakits ppi ',
        'LEFT JOIN estatusoss sta ON sta.Codigo=ppi.ID_Cod2 ',
        'WHERE ppi.Controle=' + Geral.FF0(QrProtocoPakControle.Value),
        'GROUP BY ppi.ID_Cod2 ',
        'ORDER BY ppi.ID_Cod2 ',
        '']);
        case Impressao of
          0:
          begin
            MyObjects.frxDefineDataSets(frxLote_Circula3L1, [
              frxDsCircula3,
              frxDsCirculaIts3,
              DModG.frxDsDono,
              frxDsProtocolos,
              frxDsProtocoPak
            ]);
            MyObjects.frxMostra(frxLote_Circula3L1, 'Lote de protocolos');
          end;
          1:
          begin
            MyObjects.frxDefineDataSets(frxLote_Circula3I1, [
              frxDsCircula3,
              frxDsCirculaIts3,
              DModG.frxDsDono,
              frxDsProtocolos,
              frxDsProtocoPak
            ]);
            MyObjects.frxMostra(frxLote_Circula3I1, 'Lote de protocolos');
          end;
          else ; // Nada! nem mensagem
        end;
      end else
        begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrCircula, Dmod.MyDB, [
        'SELECT ppi.Manual, ppi.SerieCH, ppi.Docum, ',
        'pto.Nome NOMEOCORMAN ',
        'FROM protpakits ppi ',
        'LEFT JOIN protocooco pto ON pto.Codigo=ppi.Manual ',
        'WHERE ppi.Controle=' + Geral.FF0(QrProtocoPakControle.Value),
        'GROUP BY ppi.Manual, ppi.SerieCH, ppi.Docum ',
        'ORDER BY ppi.Manual, ppi.SerieCH, ppi.Docum ',
        '']);
        case Impressao of
          0: MyObjects.frxMostra(frxLote_Circula1L1, 'Lote de protocolos');
          1: MyObjects.frxMostra(frxLote_Circula1I1, 'Lote de protocolos');
          else ; // Nada! nem mensagem
        end;
      end;
    end else begin
      Ctrl  := FormatFloat('0', QrProtocoPakControle.Value);
      //
      QrCircul2.Close;
      QrCircul2.SQL.Clear;

      //QrCircul2.SQL.Add('DROP TABLE IF EXISTS _GER_PROTO_001_CIRCUL2;');
      //QrCircul2.SQL.Add('CREATE TABLE _GER_PROTO_001_CIRCUL2');
      QrCircul2.SQL.Add('');
      GeraParteSQL(TabLctA, Ctrl);
      QrCircul2.SQL.Add('UNION');
      GeraParteSQL(TabLctB, Ctrl);
      QrCircul2.SQL.Add('UNION');
      GeraParteSQL(TabLctD, Ctrl);
      QrCircul2.SQL.Add('');
      QrCircul2.SQL.Add('ORDER BY Manual, SerieCH, Docum');
      QrCircul2.SQL.Add(';');
      QrCircul2.SQL.Add('');
      //QrCircul2.SQL.Add('SELECT * FROM _GER_PROTO_001_CIRCUL2;');
      QrCircul2.SQL.Add('');
      //QrCircul2.SQL.Add('DROP TABLE IF EXISTS _GER_PROTO_001_CIRCUL2;');
      QrCircul2.SQL.Add('');
      UMyMod.AbreQuery(QrCircul2, Dmod.MyDB);
      //
      MyObjects.frxMostra(frxLote_CirculaG, 'Lote de protocolos');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappFinanceiro);
{$EndIf}
end;

procedure TFmProtocolos.Circulaodedocumentos2Click(Sender: TObject);
begin
  Circulaodedocumentos1Click(Self);
end;

procedure TFmProtocolos.QrProtocolosBeforeClose(DataSet: TDataSet);
begin
  QrProtocoPak.Close;
  BtLote.Enabled := False;
end;

procedure TFmProtocolos.QrProtocolosAfterScroll(DataSet: TDataSet);
begin
  ReopenProtocoPak(0);
end;

procedure TFmProtocolos.ReopenProtocoPak(Controle: Integer);
var
  Proto: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    QrProtocoPak.Close;
    Proto := QrProtocolosCodigo.Value;
    if Proto > 0 then
    begin
      QrProtocoPak.SQL.Clear;
      QrProtocoPak.SQL.Add('SELECT ptp.*,');
      QrProtocoPak.SQL.Add('CONCAT(RIGHT(Mez, 2), "/",  LEFT(Mez + 200000, 4)) MES,');
      QrProtocoPak.SQL.Add('IF(ptp.DataF<="1899-12-30","", DATE_FORMAT(ptp.DataF, "%d/%m/%y" )) DATAF_TXT,');
      QrProtocoPak.SQL.Add('IF(ptp.DataL<="1899-12-30","", DATE_FORMAT(ptp.DataL, "%d/%m/%y" )) DATAL_TXT');
      QrProtocoPak.SQL.Add('FROM protocopak ptp');
      QrProtocoPak.SQL.Add('WHERE ptp.Codigo=' + FormatFloat('0', Proto));
      //
      if CkAbertos.Checked then
        QrProtocoPak.SQL.Add('AND ptp.DataF=0');
      QrProtocoPak.SQL.Add('ORDER BY DataL DESC, ptp.Controle DESC');
      UMyMod.AbreQuery(QrProtocoPak, Dmod.MyDB);
      //
      if Controle > 0 then QrProtocoPak.Locate('Controle', Controle, []);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmProtocolos.BtTarefaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTarefa, BtTarefa);
end;

procedure TFmProtocolos.Crianovatarefa1Click(Sender: TObject);
begin
  //MostraEdicao(1, stIns, 0);
  IncluiRegistro;
end;

procedure TFmProtocolos.Alteratarefaatual1Click(Sender: TObject);
begin
  //MostraEdicao(1, stUpd, 0);
  AlteraRegistro;
end;

procedure TFmProtocolos.arefa1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrProtocolosCodigo.Value,LaRegistro.Caption);
end;

procedure TFmProtocolos.BtLoteClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLote, BtLote);
end;

procedure TFmProtocolos.QrProtocoPakBeforeClose(DataSet: TDataSet);
begin
  BtProtocolo.Enabled := False;
  QrProtPakIts.Close;
end;

procedure TFmProtocolos.QrProtocoPakAfterOpen(DataSet: TDataSet);
begin
  BtProtocolo.Enabled := QrProtocoPak.RecordCount > 0;
end;

procedure TFmProtocolos.Crianovolote1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmProtocolos.Alteraloteatual1Click(Sender: TObject);
begin
  MostraEdicao(2, stUpd, 0);
end;

procedure TFmProtocolos.QrProtocoPakAfterScroll(DataSet: TDataSet);
begin
  ReopenProtPakIts(0);
end;

procedure TFmProtocolos.ReopenProtPakIts(Conta: Integer);
begin
  QrProtPakIts.Close;
  QrProtPakIts.SQL.Clear;
  QrProtPakIts.SQL.Add('SELECT ');
  if VAR_KIND_DEPTO = kdUH then
  //if UpperCase(Application.Title) = 'SYNDIC' then
    QrProtPakIts.SQL.Add('cimv.Unidade,')
  else
    QrProtPakIts.SQL.Add('"" Unidade,');
  QrProtPakIts.SQL.Add('IF(ppi.Periodo < 1, "", ');
  QrProtPakIts.SQL.Add('CONCAT(RIGHT(PERIOD_ADD(200000, ppi.Periodo), 2), "/",');
  QrProtPakIts.SQL.Add('  LEFT(PERIOD_ADD(200000, ppi.Periodo), 4))) PERIODO_TXT,');
  QrProtPakIts.SQL.Add('IF(clii.Tipo=0, clii.RazaoSocial, clii.Nome) NOMECLI_INT,');
  QrProtPakIts.SQL.Add('IF(clie.Tipo=0, clie.RazaoSocial, clie.Nome) NOMECLIENTE,');
  QrProtPakIts.SQL.Add('IF(clie.Tipo=0, clie.ERua, clie.PRua) ROTACLI,');
  QrProtPakIts.SQL.Add('ptm.Nome NOMEMOTIVO, pto.Nome NOMEMANUAL, ppi.*,');
  QrProtPakIts.SQL.Add('IF(ppi.Cancelado=0,"N�O","SIM") CANCELADO_TXT,');
  QrProtPakIts.SQL.Add('IF(ppi.DataE<="1899-12-30", "", DATE_FORMAT(ppi.DataE, "%d/%m/%y")) DATAE_TXT,');
  QrProtPakIts.SQL.Add('IF(ppi.DataD<="1899-12-30", "", DATE_FORMAT(ppi.DataD, "%d/%m/%y %H:%i:%S")) DATAD_TXT,');
  QrProtPakIts.SQL.Add('IF(ppi.Vencto<="1899-12-30", "", DATE_FORMAT(ppi.Vencto, "%d/%m/%y")) VENCTO_TXT,');
  QrProtPakIts.SQL.Add('IF(ppi.DataSai<="1899-12-30", "", DATE_FORMAT(ppi.DataSai, "%d/%m/%y %H:%i:%S")) DataSai_TXT,');
  //QrProtPakIts.SQL.Add('IF(ppi.Retorna<="1899-12-30", "", DATE_FORMAT(ppi.Retorna, "%d/%m/%y")) Retorna_TXT,');
  QrProtPakIts.SQL.Add('IF(ppi.DataRec<="1899-12-30", "", DATE_FORMAT(ppi.DataRec, "%d/%m/%y %H:%i:%S")) DataRec_TXT,');
  QrProtPakIts.SQL.Add('IF(ppi.DataRet<="1899-12-30", "", DATE_FORMAT(ppi.DataRet, "%d/%m/%y %H:%i:%S")) DataRet_TXT,');
  QrProtPakIts.SQL.Add('IF(ppi.DataRet<="1899-12-30", "", DATE_FORMAT(ppi.DataRet, "%d/%m/%y %H:%i:%S")) DataRet_TXT');
  QrProtPakIts.SQL.Add('FROM protpakits ppi');
  QrProtPakIts.SQL.Add('LEFT JOIN protocomot ptm ON ptm.Codigo=ppi.Motivo');
  QrProtPakIts.SQL.Add('LEFT JOIN protocooco pto ON pto.Codigo=ppi.Manual');
  QrProtPakIts.SQL.Add('LEFT JOIN entidades clii ON clii.Codigo=ppi.CliInt');
  QrProtPakIts.SQL.Add('LEFT JOIN entidades clie ON clie.Codigo=ppi.Cliente');
  if VAR_KIND_DEPTO = kdUH then
  //if UpperCase(Application.Title) = 'SYNDIC' then
    QrProtPakIts.SQL.Add('LEFT JOIN condimov  cimv ON cimv.Conta=ppi.Depto');
  QrProtPakIts.SQL.Add('WHERE ppi.Controle=:P0');
  QrProtPakIts.Params[00].AsInteger := QrProtocoPakControle.Value;
  UMyMod.AbreQuery(QrProtPakIts, Dmod.MyDB);
  //
  if Conta > 0 then QrProtPakIts.Locate('Conta', Conta, []);
end;

procedure TFmProtocolos.RGTipoClick(Sender: TObject);
begin
  ConfiguraCampos(RGTipo.ItemIndex);
end;

procedure TFmProtocolos.QrProprietCalcFields(DataSet: TDataSet);
begin
  QrProprietNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrProprietRua.Value, Trunc(QrProprietNumero.Value), False);
  QrProprietLNR.Value := QrProprietNOMELOGRAD.Value;
  if Trim(QrProprietLNR.Value) <> '' then QrProprietLNR.Value :=
    QrProprietLNR.Value + ' ';
  QrProprietLNR.Value := QrProprietLNR.Value + QrProprietRua.Value;
  if Trim(QrProprietRua.Value) <> '' then QrProprietLNR.Value :=
    QrProprietLNR.Value + ', ' + QrProprietNUMERO_TXT.Value;
  if Trim(QrProprietCompl.Value) <>  '' then QrProprietLNR.Value :=
    QrProprietLNR.Value + ' ' + QrProprietCompl.Value;
  if Trim(QrProprietBairro.Value) <>  '' then QrProprietLNR.Value :=
    QrProprietLNR.Value + ' - ' + QrProprietBairro.Value;
  //
  QrProprietLN2.Value := '';
  if Trim(QrProprietCidade.Value) <>  '' then QrProprietLN2.Value :=
    QrProprietLN2.Value + QrProprietCIDADE.Value;
  QrProprietLN2.Value := QrProprietLN2.Value + ' - '+QrProprietNOMEUF.Value;
  if QrProprietCEP.Value > 0 then QrProprietLN2.Value :=
    QrProprietLN2.Value + '     CEP ' +Geral.FormataCEP_NT(QrProprietCEP.Value);
  //
  {QrProprietCUC.Value := QrProprietLNR.Value+ '   ' +QrProprietLN2.Value;
  //
  QrProprietCEP_TXT.Value :=Geral.FormataCEP_NT(QrProprietCEP.Value);}
  //
end;

procedure TFmProtocolos.QrProtocoPakCalcFields(DataSet: TDataSet);
begin
  QrProtocoPakTITULO.Value := 'LOTE N� ' + FormatFloat('000000',
    QrProtocoPakControle.Value);
  if QrProtocoPakMez.Value > 0 then
    QrProtocoPakTITULO.Value := QrProtocoPakTITULO.Value + ' - ' +
    QrProtocoPakMES.Value;
  //
  QrProtocoPakTITULO3.Value := 'LOTE N� ' + FormatFloat('000000',
    QrProtocoPakControle.Value);
  //
  QrProtocoPakEAN128.Value := '>L<' + Geral.FFN(QrProtocoPakControle.Value, 11);
  QrProtocoPakEAN128_3.Value := 'OS1L' + Geral.FFN(QrProtocoPakControle.Value, 10);
end;

procedure TFmProtocolos.QrProtPakItsAfterScroll(DataSet: TDataSet);
begin
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrProtPakIts.RecordCount);
end;

procedure TFmProtocolos.QrProtPakItsBeforeClose(DataSet: TDataSet);
begin
  LaTotal.Caption := '';
end;

procedure TFmProtocolos.QrProtPakItsCalcFields(DataSet: TDataSet);
begin
  QrProtPakItsSERIE_DOCUM.Value := '';
  if QrProtPakItsSerieCH.Value <> '' then
    QrProtPakItsSERIE_DOCUM.Value := QrProtPakItsSerieCH.Value + ' ';
  if QrProtPakItsDocum.Value <> 0 then
    QrProtPakItsSERIE_DOCUM.Value :=
    QrProtPakItsSERIE_DOCUM.Value + FormatFloat('000000', QrProtPakItsDocum.Value);
  //
  QrPropriet.Close;
  QrPropriet.Params[0].AsInteger  := QrProtPakItsCliente.Value;
  UMyMod.AbreQuery(QrPropriet, DMod.MyDB);
  //
  QrProtPakItsLNR.Value := QrProprietLNR.Value;
  QrProtPakItsLN2.Value := QrProprietLN2.Value;
end;

procedure TFmProtocolos.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Retorna' then
    MeuVCLSkin.DrawGrid(DBGrid2, Rect, 1, QrProtPakItsRetorna.Value);
end;

procedure TFmProtocolos.Excluiprotocolo1Click(Sender: TObject);
var
  ProtocoPak: Integer;
begin
  if QrProtocolosTipo.Value = VAR_TIPO_PROTOCOLO_CR_04 then
  begin
    if Geral.MB_Pergunta('O Tipo de protocolo "' +
    UnProtocolo.NomeDoTipoDeProtocolo(VAR_TIPO_PROTOCOLO_CR_04) +
    '" tem boletos que devem ser desvinculados antes desta exclus�o!' +
    sLineBreak + 'Confirma este desvinculamento?') = ID_YES then
    begin
      ProtocoPak := QrProtocoPakControle.Value;
      //
      if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'arreits', False, [
        'Protocolo', 'ProtocoloCR'], ['ProtocoloCR'],
        [0, 0], [ProtocoPak], True) then
        Exit;
    end;
  end;
  //
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrProtPakIts, DBGrid2, 'protpakits',
    ['Conta'], ['Conta'], istPergunta, '');
  ReopenAbertos();
end;

procedure TFmProtocolos.DefinedatadaEntrega1Click(Sender: TObject);
var
  Data: TDateTime;
begin
  if not DBCheck.ObtemData(Date, Data, 0) then Exit;
  DBCheck.QuaisItens_Altera(Dmod.QrUpd, QrProtPakIts, DBGrid2, 'protpakits',
  ['DataE'], ['Conta'], ['Conta'], [Data], istPergunta, True, True);
  ReopenAbertos();
end;

procedure TFmProtocolos.DefinedatadoRetorno1Click(Sender: TObject);
var
  Data: TDateTime;
begin
  if not DBCheck.ObtemData(Date, Data, 0) then Exit;
  DBCheck.QuaisItens_Altera(Dmod.QrUpd, QrProtPakIts, DBGrid2, 'protpakits',
  ['DataD'], ['Conta'], ['Conta'], [Data], istPergunta, True, True);
  ReopenAbertos();
end;

procedure TFmProtocolos.Cancelaentrega1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Motivo de Cancelamento';
  Prompt = 'Motivo de cancelamento:';
  Campo  = 'Descricao';
var
  Codigo: Variant;
begin
{
  Application.CreateForm(TFm SelCod, Fm SelCod);
  Fm SelCod.Caption := 'Motivo de cancelamento';
  Fm SelCod.LaPrompt.Caption := 'Motivo de cancelamento:';
  Fm SelCod.QrSel.Close;
  Fm SelCod.QrSel.SQL.Clear;
  Fm SelCod.QrSel.SQL.Add('SELECT Nome Descricao, Codigo');
  Fm SelCod.QrSel.SQL.Add('FROM protocomot');
  Fm SelCod.QrSel.SQL.Add('ORDER BY Descricao');
  UMyMod.AbreQuery(Fm SelCod.QrSel, Dmod.MyDB);
  //
  Fm SelCod.ShowModal;
  Fm SelCod.Destroy;
  if VAR_SELCOD > 0 then
}
  Codigo := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Nome Descricao, Codigo ',
    'FROM protocomot ',
    'ORDER BY Descricao ',
    ''], Dmod.MyDB, False);
  //
  if Codigo <> Null then
    DBCheck.QuaisItens_Altera(Dmod.QrUpd, QrProtPakIts, DBGrid2, 'protpakits',
      ['Cancelado', 'Motivo'], ['Conta'], ['Conta'], [1, Codigo], istPergunta,
      True, True);
  ReopenAbertos();
end;

function TFmProtocolos.DataDesconto(Item: Integer): TDateTime;
var
  Banco, Dias: Integer;
begin
{$IfNDef NO_CNAB}
  Dias := 0;
  Banco := DmBco.QrCNAB_CfgCedBanco.Value;
  //
  case Item of
    1: Dias := DmBco.QrCNAB_CfgDesco1Dds.Value;
    2: Dias := DmBco.QrCNAB_CfgDesco2Dds.Value;
    3: Dias := DmBco.QrCNAB_CfgDesco3Dds.Value;
  end;
  //
  case Banco of
    008, 033, 353, 756:
    begin
      if Dias = 0 then
        Result := 0
      else
        Result := QrProtPakItsVencto.Value - Dias;
    end
    else
      Result := QrProtPakItsVencto.Value - Dias;
  end;
{$EndIf}
end;

function TFmProtocolos.DataMulta(Vencto: TDateTime; Dias: Integer): TDateTime;
begin
  if Dias > 0 then
    Result := Vencto + Dias
  else
    Result := Vencto + 1;
end;

function TFmProtocolos.ValrDesconto(Item: Integer): Double;
var
  Perc: Double;
begin
{$IfNDef NO_CNAB}
  Perc := 0;
  case Item of
    1: Perc := DmBco.QrCNAB_CfgDesco1Fat.Value;
    2: Perc := DmBco.QrCNAB_CfgDesco2Fat.Value;
    3: Perc := DmBco.QrCNAB_CfgDesco3Fat.Value;
  end;
  //
  Result := (QrProtPakItsValor.Value * Perc) / 100;
{$EndIf}
end;

procedure TFmProtocolos.DBGrid2ColEnter(Sender: TObject);
begin
  if (TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName = 'Retorna') then
    TDBGrid(Sender).Options := TDBGrid(Sender).Options - [dgEditing] else
    TDBGrid(Sender).Options := TDBGrid(Sender).Options + [dgEditing];
end;

procedure TFmProtocolos.DBGrid2ColExit(Sender: TObject);
begin
  THackDBGrid(Sender).Options := THackDBGrid(Sender).Options - [dgEditing];
end;

procedure TFmProtocolos.ReopenAbertos();
begin
  QrAbertos.Close;
  QrAbertos.SQL.Clear;
  QrAbertos.SQL.Add('SELECT ptc.Nome NOMEPROTOCOLO,');
  if VAR_KIND_DEPTO = kdUH then
  //if UpperCase(Application.Title) = 'SYNDIC' then
   QrAbertos.SQL.Add('cimv.Unidade,')
  else
    QrAbertos.SQL.Add('"" Unidade,');
  QrAbertos.SQL.Add('CONCAT(RIGHT(PERIOD_ADD(200000, ppi.Periodo), 2), "/",');
  QrAbertos.SQL.Add('  LEFT(PERIOD_ADD(200000, ppi.Periodo), 4)) PERIODO_TXT,');
  QrAbertos.SQL.Add('IF(clii.Tipo=0, clii.RazaoSocial, clii.Nome) NOMECLI_INT,');
  QrAbertos.SQL.Add('IF(clie.Tipo=0, clie.RazaoSocial, clie.Nome) NOMECLIENTE,');
  QrAbertos.SQL.Add('ptm.Nome NOMEMOTIVO, ppi.*,');
  QrAbertos.SQL.Add('IF(ppi.Cancelado=0,"N�O","SIM") CANCELADO_TXT,');
  QrAbertos.SQL.Add('IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DATAE_TXT,');
  QrAbertos.SQL.Add('IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DATAD_TXT');
  QrAbertos.SQL.Add('FROM protpakits ppi');
  QrAbertos.SQL.Add('LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo');
  QrAbertos.SQL.Add('/*LEFT JOIN protocopak ptp ON ptp.Controle=ppi.Controle*/');
  QrAbertos.SQL.Add('LEFT JOIN protocomot ptm ON ptm.Codigo=ppi.Motivo');
  QrAbertos.SQL.Add('LEFT JOIN entidades clii ON clii.Codigo=ppi.CliInt');
  QrAbertos.SQL.Add('LEFT JOIN entidades clie ON clie.Codigo=ppi.Cliente');
  //if UpperCase(Application.Title) = 'SYNDIC' then
  if VAR_KIND_DEPTO = kdUH then
    QrAbertos.SQL.Add('LEFT JOIN condimov  cimv ON cimv.Conta=ppi.Depto');
  QrAbertos.SQL.Add('WHERE (DataE=0)');
  QrAbertos.SQL.Add('OR ((DataD=0) AND (Retorna>0))');
  UMyMod.AbreQuery(QrAbertos, Dmod.MyDB);
end;

procedure TFmProtocolos.ReopenCliProt(Protocolo: Integer);
begin
  QrCliProt.Close;
  QrCliProt.SQL.Clear;
  QrCliProt.SQL.Add('SELECT IF(ptc.Tipo=0,"ND",');
  QrCliProt.SQL.Add('  IF(ptc.Tipo=1,"Circula��o de documentos",');
  QrCliProt.SQL.Add('  IF(ptc.Tipo=2,"Circula��o eletr�nica",');
  QrCliProt.SQL.Add('                "Entrega de material de consumo"))');
  QrCliProt.SQL.Add('  ) NOMETIPO,');
{$IfNDef NO_USE_EMAILDMK}
  QrCliProt.SQL.Add('pre.Nome NOMEPREEMEIO, ');
{$Else}
  QrCliProt.SQL.Add('"" NOMEPREEMEIO, ');
{$EndIf}
  QrCliProt.SQL.Add('ptc.*,');
  QrCliProt.SQL.Add('IF(ptc.Def_Sender=0,"",IF(d_s.Tipo=0, d_s.RazaoSocial,d_s.Nome)) NOME_SENDER');
  QrCliProt.SQL.Add('FROM protocolos ptc');
  QrCliProt.SQL.Add('LEFT JOIN entidades d_s ON d_s.Codigo=ptc.Def_Sender');
{$IfNDef NO_USE_EMAILDMK}
  QrCliProt.SQL.Add('LEFT JOIN preemail  pre ON pre.Codigo=ptc.PreEmeio');
{$EndIf}
  QrCliProt.SQL.Add('WHERE ptc.Def_Client=' +
                     dmkPF.FFP(QrCliIntCodigo.Value, 0));
  QrCliProt.SQL.Add('');
  if Trim(EdCliProt.Text) <> '' then
    QrCliProt.SQL.Add('AND ptc.Nome LIKE "%' + EdCliProt.Text + '%"');
  if CkSohAtivos.Checked then
    QrCliProt.SQL.Add('AND ptc.Ativo=1');
  UMyMod.AbreQuery(QrCliProt, Dmod.MyDB, 'TFmProtocolos.ReopenCliProt()');
  //
  QrCliProt.Locate('Codigo', Protocolo, []);
end;

procedure TFmProtocolos.ReopenCliInt();
begin
  QrCliInt.Close;
  QrCliInt.SQL.Clear;
  QrCliInt.SQL.Add('SELECT DISTINCT ent.Codigo, ent.CliInt,');
  QrCliInt.SQL.Add('IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMECLI');
  QrCliInt.SQL.Add('FROM entidades ent');
  QrCliInt.SQL.Add('LEFT JOIN protocolos pro ON pro.Def_Client=ent.Codigo');
  QrCliInt.SQL.Add('WHERE (ent.CliInt<>0');
  QrCliInt.SQL.Add('OR pro.Def_Client <> 0)');
  QrCliInt.SQL.Add('AND ent.Codigo IN (' + VAR_LIB_EMPRESAS + ')');
  if Trim(EdCliInt.Text) <> '' then
    QrCliInt.SQL.Add('AND (IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome)) LIKE "%' +
    EdCliInt.Text + '%"');
   UMyMod.AbreQuery(QrCliInt, Dmod.MyDB, 'TFmProtocolos.ReopenCliInt()');
end;

procedure TFmProtocolos.MenuItem1Click(Sender: TObject);
var
  Data: TDateTime;
begin
  if not DBCheck.ObtemData(Date, Data, 0) then Exit;
  DBCheck.QuaisItens_Altera(Dmod.QrUpd, QrAbertos, TDBGrid(dmkDBGrid3), 'protpakits',
  ['DataE'], ['Conta'], ['Conta'], [Data], istPergunta, True, True);
  ReopenAbertos();
end;

procedure TFmProtocolos.MenuItem2Click(Sender: TObject);
var
  Data: TDateTime;
begin
  if not DBCheck.ObtemData(Date, Data, 0) then Exit;
  DBCheck.QuaisItens_Altera(Dmod.QrUpd, QrAbertos, TDBGrid(dmkDBGrid3), 'protpakits',
  ['DataD', 'ComoConf'], ['Conta'], ['Conta'],
  [Data, UnProtocolo.IntOfComoConf(ptkManual)], istPergunta, True, True);
  ReopenAbertos();
end;

procedure TFmProtocolos.MenuItem4Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Motivo de Cancelamento';
  Prompt = 'Motivo de cancelamento:';
  Campo  = 'Descricao';
var
  Codigo: Integer;
begin
{
  Application.CreateForm(TFm SelCod, Fm SelCod);
  Fm SelCod.Caption := 'Motivo de cancelamento';
  Fm SelCod.LaPrompt.Caption := 'Motivo de cancelamento:';
  Fm SelCod.QrSel.Close;
  Fm SelCod.QrSel.SQL.Clear;
  Fm SelCod.QrSel.SQL.Add('SELECT Nome Descricao, Codigo');
  Fm SelCod.QrSel.SQL.Add('FROM protocomot');
  Fm SelCod.QrSel.SQL.Add('ORDER BY Descricao');
  UMyMod.AbreQuery(Fm SelCod.QrSel, Dmod.MyDB);
  //
  Fm SelCod.ShowModal;
  Fm SelCod.Destroy;
  if VAR_SELCOD > 0 then
}
  Codigo := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
  'SELECT Nome Descricao, Codigo ',
  'FROM protocomot ',
  'ORDER BY Descricao ',
  ''], Dmod.MyDB, False);
  if Codigo <> 0 then
  DBCheck.QuaisItens_Altera(Dmod.QrUpd, QrAbertos, TDBGrid(dmkDBGrid3), 'protpakits',
    ['Cancelado', 'Motivo'], ['Conta'], ['Conta'], [1, Codigo], istPergunta,
    True, True);
  ReopenAbertos();
end;

procedure TFmProtocolos.MenuItem5Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrAbertos, TDBGrid(dmkDBGrid3), 'protpakits',
    ['Conta'], ['Conta'], istPergunta, '');
  ReopenAbertos();
end;

procedure TFmProtocolos.QrAbertosBeforeClose(DataSet: TDataSet);
begin
  BtProtocolo2.Enabled := False;
end;

procedure TFmProtocolos.QrCirculaAfterScroll(DataSet: TDataSet);
  procedure GeraParteSQL(TabLct, Ctrl, Serie, Docum: String);
  begin
    QrCirculaIts.SQL.Add('SELECT ppi.Conta, IF(ppi.Retorna=0, "N�o", "Sim") RETORNA_TXT,');
    QrCirculaIts.SQL.Add('ppi.Lancto, Valor, IF(ppi.Manual=0, lan.Descricao, ppi.Texto) TEXTO,');
    QrCirculaIts.SQL.Add('IF(ppi.Vencto=0, "", DATE_FORMAT(ppi.Vencto, "%d/%m/%Y")) VENCTO_TXT,');
    QrCirculaIts.SQL.Add('IF(ppi.Manual=0,');
    QrCirculaIts.SQL.Add('  IF(Mez=0,"",CONCAT(RIGHT(Mez, 2), "/",  LEFT(Mez + 200000, 4))),');
    QrCirculaIts.SQL.Add('  IF(Periodo=0,"",CONCAT(RIGHT(PERIOD_ADD(200000, ppi.Periodo), 2), "/",');
    QrCirculaIts.SQL.Add('  LEFT(PERIOD_ADD(200000, ppi.Periodo), 4)))) MesAno,');
    if VAR_KIND_DEPTO = kdUH then
      QrCirculaIts.SQL.Add('IF(ppi.Depto =0, "", cim.Unidade) UNIDADE')
    else
      QrCirculaIts.SQL.Add('"" UNIDADE');
    QrCirculaIts.SQL.Add('FROM protpakits ppi');
    QrCirculaIts.SQL.Add('LEFT JOIN ' + TabLct + ' lan ON lan.Controle=ppi.Lancto');
    if VAR_KIND_DEPTO = kdUH then
      QrCirculaIts.SQL.Add('LEFT JOIN condimov cim ON cim.Conta=ppi.Depto')
    else
      ; // Nada!
    // 2011-08-01 -> n�o duplicar ou triplicar!
    QrCirculaIts.SQL.Add('WHERE lan.Controle IS NOT NULL');
    QrCirculaIts.SQL.Add('AND ppi.Controle=' + Ctrl);
    QrCirculaIts.SQL.Add('AND ppi.SerieCH="' + Serie + '"');
    QrCirculaIts.SQL.Add('AND ppi.Docum=' + Docum);
    // 2011-08-17 -> N�o duplicar!
    QrCirculaIts.SQL.Add('AND ppi.Manual=' + FormatFloat('0', QrCirculaManual.Value));
  end;
var
  Ctrl, Serie, Docum: String;
  DtEncer, DtMorto: TDateTime;
  TabLctA, TabLctB, TabLctD: String;
begin
  DModG.Def_EM_ABD(TMeuDB, QrProtocolosDef_Client.Value,
  QrProtocolosCliInt.Value, DtEncer, DtMorto, TabLctA, TabLctB, TabLctD);
  //
  Ctrl  := FormatFloat('0', QrProtocoPakControle.Value);
  Serie := QrCirculaSerieCH.Value;
  Docum := FormatFloat('0', QrCirculaDocum.Value);
  //
  QrCirculaIts.Close;
  QrCirculaIts.SQL.Clear;
{
  QrCirculaIts.Params[00].AsInteger := QrProtocoPakControle.Value;
  QrCirculaIts.Params[01].AsString  := QrCirculaSerieCH.Value;
  QrCirculaIts.Params[02].AsFloat   := QrCirculaDocum.Value;
}

  //
  QrCirculaIts.Close;
  QrCirculaIts.SQL.Clear;
  //QrCirculaIts.SQL.Add('DROP TABLE IF EXISTS _GER_PROTO_001_CIRCULAITS;');
  //QrCirculaIts.SQL.Add('CREATE TABLE _GER_PROTO_001_CIRCULAITS');
  QrCirculaIts.SQL.Add('');
  GeraParteSQL(TabLctA, Ctrl, Serie, Docum);
  QrCirculaIts.SQL.Add('UNION');
  GeraParteSQL(TabLctB, Ctrl, Serie, Docum);
  QrCirculaIts.SQL.Add('UNION');
  GeraParteSQL(TabLctD, Ctrl, Serie, Docum);
  QrCirculaIts.SQL.Add(';');
  QrCirculaIts.SQL.Add('');
  //QrCirculaIts.SQL.Add('SELECT * FROM _GER_PROTO_001_CIRCULAITS;');
  QrCirculaIts.SQL.Add('');
  //QrCirculaIts.SQL.Add('DROP TABLE IF EXISTS _GER_PROTO_001_CIRCULAITS;');
  QrCirculaIts.SQL.Add('');
  UMyMod.AbreQuery(QrCirculaIts, Dmod.MyDB);
end;

procedure TFmProtocolos.QrCirculaIts3CalcFields(DataSet: TDataSet);
begin
  QrCirculaIts3EAN128.Value := 'OS1I' + Geral.FFN(QrCirculaIts3Conta.Value, 10);
end;

procedure TFmProtocolos.QrCirculaItsCalcFields(DataSet: TDataSet);
begin
  QrCirculaItsEAN128.Value := '>I<' + Geral.FFN(QrCirculaItsConta.Value, 11);
end;

procedure TFmProtocolos.QrCliIntAfterScroll(DataSet: TDataSet);
begin
  ReopenCliProt(0);
end;

procedure TFmProtocolos.QrCliProtAfterScroll(DataSet: TDataSet);
begin
  LocCod(QrCliProtCodigo.Value, QrCliProtCodigo.Value);
end;

procedure TFmProtocolos.QrAbertosAfterOpen(DataSet: TDataSet);
begin
  BtProtocolo2.Enabled := QrAbertos.RecordCount > 0;
end;

procedure TFmProtocolos.BtProtocoloClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProtocolo, BtProtocolo);
end;

procedure TFmProtocolos.BtProtoGerClick(Sender: TObject);
var
  Tarefa, Lote: Integer;
begin
  if (QrProtocolos.State <> dsInactive) and (QrProtocolos.RecordCount > 0) then
  begin
    Tarefa := QrProtocolosCodigo.Value;
    Lote   := QrProtocoPakControle.Value;
  end else
  begin
    Tarefa := 0;
    Lote   := 0;
  end;
  ProtocoUnit.MostraProtoGer(False, nil, nil, Tarefa, Lote);
end;

procedure TFmProtocolos.BtProtocolo2Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProtocolo2, BtProtocolo2);
end;

procedure TFmProtocolos.CkAbertosClick(Sender: TObject);
begin
  ReopenProtocoPak(QrProtocoPakControle.Value);
end;

procedure TFmProtocolos.ConfiguraCampos(TipoProt: Integer);
var
  TipEmail: Boolean;
begin
  TipEmail := (TipoProt = 2);
  //
  LaPreEmeio.Visible   := TipEmail;
  EdPreEmeio.Visible   := TipEmail;
  CBPreEmeio.Visible   := TipEmail;
  CkDef_Retorn.Visible := not TipEmail;
  //
  LaDef_Sender.Visible := not TipEmail;
  EdDef_Sender.Visible := not TipEmail;
  CBDef_Sender.Visible := not TipEmail;
  SBDef_Sender.Visible := not TipEmail;
end;

procedure TFmProtocolos.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 0 then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      if (QrAbertos.State = dsInactive) or (QrAbertos.RecordCount = 0) then
        ReopenAbertos();
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmProtocolos.PMLotePopup(Sender: TObject);
var
  Nome: String;
  Enab, EnabDepto, CDEnab: Boolean;
begin
  EnabDepto := VAR_KIND_DEPTO = kdUH;
  //EnabDepto := VAR_KIND_DEPTO IN ([kdUH, kdOS1]);
  //
  if EnabDepto then
    Nome := 'Propriet�rio'
  else
    Nome := 'Entidade';
  //
  if (QrProtocoPak.State <> dsInactive) and (QrProtocoPak.RecordCount > 0) then
  begin
    Alteraloteatual1.Enabled := True;
    Excluiloteatual1.Enabled := QrProtPakIts.RecordCount = 0;
  end else begin
    Excluiloteatual1.Enabled := False;
  end;
  GeraarquivoremessaCNAB1.Enabled := (QrProtocoPakCNAB_Cfg.Value <> 0) and (QrProtocolosTipo.Value = VAR_TIPO_PROTOCOLO_CR_04);
  Zerarnmero1.Enabled             := (QrProtocoPakCNAB_Cfg.Value <> 0) and (QrProtocolosTipo.Value = VAR_TIPO_PROTOCOLO_CR_04);
  //
  Enab := (QrProtPakIts.State <> dsInactive) and (QrProtPakIts.RecordCount > 0);
  //
  Imprimir1.Enabled                        := Enab;
  ImprimirEntregadeboletosModelo21.Enabled := Enab;
  ImprimirEntregadeboletosModelo31.Enabled := Enab;
  ImprimirEntregadeboletosModelo41.Enabled := Enab;
  Circulaodedocumentos1.Enabled            := Enab;
  //
  Ordenarporunidade1.Visible  := EnabDepto;
  OrdenarpelaUnidade2.Visible := EnabDepto;
  OrdenarpelaUnidade3.Visible := EnabDepto;
  OrdenarpelaUnidade4.Visible := EnabDepto;
  //
  OrdenarpeloNomedoProprietrio1.Caption := 'Ordenar pelo nome do &' + Nome;
  OrdenarpelonomedoProprietrio3.Caption := 'Ordenar pelo nome do &' + Nome;
  OrdenarpelonomedoProprietrio4.Caption := 'Ordenar pelo nome do &' + Nome;
  OrdenarpelonomedoProprietrio5.Caption := 'Ordenar pelo nome do &' + Nome;
  //
  if CO_DMKID_APP <> 4 then //Syndi2 => A Euro usa um protocolo para v�rios tipos de tarefas
  begin
    if Enab then
      CDEnab := QrProtocolosTipo.Value  = 5 //Circula��o de documentos
    else
      CDEnab := False;
  end else
    CDEnab := True;
  //
  Circulaodedocumentos1.Enabled := Enab and CDEnab;
end;

procedure TFmProtocolos.PMProtocoloPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
{
  0: "ND - N�o definido"
  1: "EB - Entrega de bloquetos"
  2: "CE - Circula��o eletr�nica (e-mail)"
  3: "EM - Entrega de material de consumo"
  4: "CR - Cobran�a registrada (banc�ria)"
  5: "CD - Circula��o de documentos"
}
  {$IfDef IMP_BLOQLCT}
  Habilita := (QrProtocolosTipo.Value = VAR_TIPO_PROTOCOLO_CR_04)
               and (QrProtocoPakDataF.Value < 2);
  {$Else}
  Habilita := False;
  {$EndIf}
  IncluiboletoscomregistroCR1.Enabled := Habilita;
  //
  Habilita := not Habilita;
  //
  IncluiProtocoloBases1.Enabled  := Habilita;
  Incluiprotocolomanual1.Enabled := Habilita;
  //
  Habilita := (QrProtPakIts.State <> dsInactive) and
    (QrProtPakIts.RecordCount > 0) and (QrProtPakItsManual.Value > 0);
  Alteraprotocolomanual1.Enabled := Habilita;
end;

procedure TFmProtocolos.PMTarefaPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrProtocolos.State <> dsInactive) and (QrProtocolos.RecordCount > 0);
  //
  Alteratarefaatual1.Enabled := Enab;
  Excluitarefaatual1.Enabled := False; //N�o implementado
end;

procedure TFmProtocolos.Protocolo1Click(Sender: TObject);
var
  PPItsTxt: String;
  PPItsInt: Integer;
begin
  PPItsTxt := '0';
  if InputQuery('Localiza��o de item de protocolo', 'Informe o n�mero do protocolo' , PPItsTxt) then
  begin
    PPItsInt := Geral.IMV(PPItsTxt);
    if PPItsInt <> 0 then
    begin
      QrLoc.Close;
      QrLoc.Params[0].AsInteger := PPItsInt;
      UMyMod.AbreQuery(QrLoc, Dmod.MyDB);
      if QrLoc.RecordCount > 0 then
      begin
        LocalizaLote(QrLocControle.Value);
        QrProtPakIts.Locate('Conta', PPItsInt, []);
      end else
        Geral.MB_Aviso('Protocolo n�o localizado: ' + FormatFloat('0', PPItsInt));
      //
    end;
  end;
end;

procedure TFmProtocolos.Excluiloteatual1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do lote selecionado?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM protocopak WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrProtocoPakControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    LocCod(QrProtocolosCodigo.Value, QrProtocolosCodigo.Value);
  end;
end;

procedure TFmProtocolos.GeraarquivoremessaCNAB1Click(Sender: TObject);
{$IfNDef NO_CNAB}
  function Endereco(): String;
  begin
    Result := DmBco.QrAddr3NOMELOGRAD.Value;
    if Result <> '' then Result := Result + ' ';

    Result := Result + DmBco.QrAddr3RUA.Value;
    if DmBco.QrAddr3RUA.Value <> '' then Result := Result + ', ';

    Result := Result + DmBco.QrAddr3NUMERO_TXT.Value;
    if DmBco.QrAddr3NUMERO_TXT.Value <> '' then Result := Result + ' ';

    if DmBco.QrAddr3COMPL.Value <> '' then Result := Result + '- ';
    Result := Result + DmBco.QrAddr3COMPL.Value;

  end;
var
  Vezes, CedenteTipo, SacadorTipo, Item, i, n, np, nq, DiasMora, DiasMult: Integer;
  SQL, CedenteNome, SacadorNome, CedenteCNPJ, SacadorCNPJ,
  NomeCedente, NomeSacador, CEPSacado_, CEPSacador: String;
  HoraI, HoraF, DataMora, DataMult: TDateTime;
  g, gp, gq: TStringGrid;
  JurosPerc, JurosValr, MultaPerc, MultaValr, ValTitulos, Documento: Double;
  Addr3_Tipo: Integer;
  Addr3_CNPJ, Addr3_NOMEENT, Addr3_NOMELOGRAD, Addr3_RUA, Addr3_NUMERO_TXT,
  Addr3_COMPL, Addr3_BAIRRO, Addr3_CIDADE, Addr3_NOMEUF,
  Sequencial_TXT, DAC_NossoNum, NossoNum_REM, NossoNum_TXT,
  Texto01, Texto02, Texto03, Texto04, Msg: String;
begin
  ValTitulos := 0;
  Documento  := 0;
  //
  if QrProtocoPakCNAB_Cfg.Value = 0 then Exit;
  //
  HoraI := Now();
  //
  dmkEdHoraI.ValueVariant := HoraI;
  Screen.Cursor           := crHourGlass;
  try
    SQL := 'SELECT COUNT(ppi.Conta) Itens' + sLineBreak+
           'FROM protpakits ppi' + sLineBreak+
           'WHERE ppi.Controle=' +
            FormatFloat('0', QrProtocoPakControle.Value) +sLineBreak+
           'AND ppi.CliInt <>' +
            FormatFloat('0', QrProtocolosDef_Client.Value);

    if not DmBco.ReopenCNAB_Cfg(QrProtocoPakCNAB_Cfg.Value,
      QrProtocolosDef_Client.Value, SQL,
      CedenteTipo, CedenteNome, CedenteCNPJ,
      SacadorTipo, SacadorNome, SacadorCNPJ) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    //
    (* O campo CedNome � somente para impress�o
    if DmBco.QrCNAB_CfgCedNome.Value <> '' then
      NomeCedente := DmBco.QrCNAB_CfgCedNome.Value
    else
      NomeCedente := CedenteNome;
    *)
    NomeCedente := CedenteNome;
    //
    (*
    if DmBco.QrCNAB_CfgSacAvaNome.Value <> '' then
      NomeSacador := DmBco.QrCNAB_CfgSacAvaNome.Value
    else
      NomeSacador := SacadorNome;
    *)
    //Usar a Raz�o Social por causa da imporss�o do boleto que vai o CNPJ junto no campo nome
    NomeSacador := SacadorNome;
    //
    if QrProtocoPakSeqArq.Value > 0 then
    begin
      if Geral.MB_Pergunta('Este lote j� possui um n�mero sequencial ' +
        'de remessa de arquivo! Ser� utilizado o mesmo n�mero.' + sLineBreak +
        'Alguns bancos podem n�o aceitar o mesmo n�mero sequencial mais de uma vez!.' + sLineBreak +
        '- Caso voc� ainda n�o enviou este arquivo, desconsidere este aviso.' + sLineBreak +
        '- Caso queira usar um n�mero espec�fico informe o n�mero anterior a ele na configura��o de remessa correspondente.'+ sLineBreak+
        '- Caso queira zerar o n�mero cancele e use o menu do bot�o de lotes deste formul�rio'+ sLineBreak +
        '- Caso queira continuar assim mesmo confirme esta a��o') <> ID_YES
      then
        Exit;
    end;
    UCriar.RecriaTempTable('CNAB_Rem', DModG.QrUpdPID1, False);
    //
    //
    Item := 0;
    //inc(Item, 1);
    if DBCheck.CriaFm(TFmCNAB_Rem, FmCNAB_Rem, afmoNegarComAviso) then
    begin
      with FmCNAB_Rem do
      begin
        // Dados para gera��o do arquivo
        // Banco
        FBanco := DmBco.QrCNAB_CfgCedBanco.Value;
        // Layout
        FNomeLayout := DmBco.QrCNAB_CfgLayoutRem.Value;
        // Lote
        FLote := QrProtocoPakControle.Value;
        // N�mero de remessa do arquivo (Bradesco)
        FSeqArqRem := QrProtocoPakSeqArq.Value;
        ////////////////// -0- R E G I S T R O    H E A D E R ////////////////////
        //dmkEd_0_010.ValueVariant := 0;
        dmkEd_0_005.ValueVariant := 1;
        dmkEd_0_006.ValueVariant := 'REMESSA';
        dmkEd_0_003.ValueVariant := 1;
        dmkEd_0_004.ValueVariant := 'COBRANCA';
        dmkEd_0_020.ValueVariant := DmBco.QrCNAB_CfgCedAgencia.Value;
        dmkEd_0_021.ValueVariant := DmBco.QrCNAB_CfgCedConta.Value;
        dmkEd_0_022.ValueVariant := DmBco.QrCNAB_CfgCedDAC_A.Value;
        dmkEd_0_023.ValueVariant := DmBco.QrCNAB_CfgCedDAC_C.Value;
        dmkEd_0_024.ValueVariant := DmBco.QrCNAB_CfgCedDAC_AC.Value;
        dmkEd_0_037.ValueVariant := DmBco.QrCNAB_CfgConcatCod.Value;
        //dmkEd_0_038.ValueVariant := DmBco.QrCNAB_Cfg.Value;
        //
        if (FBanco = 756) and (FNomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
          dmkEd_0_410.ValueVariant := DmBco.QrCNAB_CfgCodLidrBco.Value
        else
          dmkEd_0_410.ValueVariant := DmBco.QrCNAB_CfgCodEmprBco.Value;
        //
        if (FBanco = 756) and (FNomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
          dmkEd_0_402.ValueVariant := NomeSacador
        else
          dmkEd_0_402.ValueVariant := NomeCedente;
        //
        dmkEd_0_001.ValueVariant := FBanco;
        dmkEd_0_891.ValueVariant := DmBco.QrCNAB_CfgNumVersaoI3.Value;
        dmkEd_0_990.ValueVariant := QrProtocoPakDataI.Value;
        dmkEd_0_699.ValueVariant := DmBco.QrCNAB_CfgCodOculto.Value;

        //
        ///////////////// -1- R E G I S T R O    D E T A L H E ///////////////////

        JurosPerc := DmBco.QrCNAB_CfgJurosPerc.Value;
        MultaPerc := DmBco.QrCNAB_CfgMultaPerc.Value;
        Vezes := Geral.IMV(dmkEdX.ValueVariant);
        if Vezes = 0 then Vezes := 1;
        for i := 1 to Vezes do
        begin
          QrProtPakIts.First;
          while not QrProtPakIts.Eof do
          begin
            if i = 1 then
              ValTitulos := ValTitulos + QrProtPakItsValor.Value;
            //
            inc(Item, 1);
            g := Grade_1;
            n := AddLinha(FLinG1, Grade_1);
            //
            gp := Grade_240_3_P;
            np := AddLinha(FLinG3P, Grade_240_3_P);
            //
            gq := Grade_240_3_Q;
            nq := AddLinha(FLinG3Q, Grade_240_3_Q);
            //
            JurosValr := Round(QrProtPakItsValor.Value * JurosPerc / 30) / 100;
            if QrProtPakItsMoraDiaVal.Value > JurosValr then
            JurosValr := QrProtPakItsMoraDiaVal.Value;
            //
            MultaValr := Round(QrProtPakItsValor.Value * MultaPerc) / 100;
            //
            DiasMora  := DmBco.QrCNAB_CfgJurosDias.Value;
            DataMora  := QrProtPakItsVencto.Value + DiasMora;
            //
            DiasMult  := DmBco.QrCNAB_CfgMultaDias.Value;
            DataMult  := QrProtPakItsVencto.Value + DiasMult;
            //
            UBancos.GeraNossoNumero(
              DmBco.QrCNAB_CfgModalCobr.Value,
              DmBco.QrCNAB_CfgCedBanco.Value (*Banco*),
              DmBco.QrCNAB_CfgCedAgencia.Value (*Agencia*),
              DmBco.QrCNAB_CfgCedPosto.Value (*Posto*),
              QrProtPakItsDocum.Value (*BLOQUETO*),
              DmBco.QrCNAB_CfgCedConta.Value (*Conta*),
              DmBco.QrCNAB_CfgCartNum.Value (*Carteira*),
              DmBco.QrCNAB_CfgIDCobranca.Value,
              Geral.SoNumero_TT(DmBco.QrCNAB_CfgCodEmprBco.Value) (*CodCedente*),
              QrProtPakItsVencto.Value (*Vencto do BLOQUETO!*),
              DmBco.QrCNAB_CfgTipoCobranca.Value,
              DmBco.QrCNAB_CfgEspecieDoc.Value,
              DmBco.QrCNAB_CfgCNAB.Value,
              DmBco.QrCNAB_CfgCtaCooper.Value, DmBco.QrCNAB_CfgLayoutRem.Value,
              NossoNum_TXT, NossoNum_REM);
            NossoNum_TXT := Geral.SoNumero_TT(NossoNum_TXT);
            NossoNum_REM := Geral.SoNumero_TT(NossoNum_REM);
            Sequencial_TXT := Geral.FFI(QrProtPakItsDocum.Value);

            {$IfDef IMP_BLOQLCT}
              Documento := UBloquetos.ObtemDocumProtPakIts(QrProtPakItsConta.Value);
            {$Else}
              Documento := QrProtPakItsDocum.Value;
            {$EndIf}

            //Calcula DAC Nosso N�mero
            DAC_NossoNumero(QrProtPakItsDocum.Value, DAC_NossoNum);

            //Detalhe 3 - CNAB 400 - In�cio
            AddCampo(g, n, F1_000, '0000'    , Item);
            AddCampo(g, n, F1_011, '0'       , 1);
            AddCampo(g, n, F1_400, '0'       , CedenteTipo);
            AddCampo(g, n, F1_401, 'CNPJ'    , CedenteCNPJ);
            AddCampo(g, n, F1_020, '0000'    , DmBco.QrCNAB_CfgCedAgencia.Value);
            AddCampo(g, n, F1_021, ''        , DmBco.QrCNAB_CfgCedConta.Value);
            AddCampo(g, n, F1_022, ''        , DmBco.QrCNAB_CfgCedDAC_A.Value);
            AddCampo(g, n, F1_023, ''        , DmBco.QrCNAB_CfgCedDAC_C.Value);
            AddCampo(g, n, F1_024, ''        , DmBco.QrCNAB_CfgCedDAC_AC.Value);
            AddCampo(g, n, F1_037, ''        , DmBco.QrCNAB_CfgConcatCod.Value);
            AddCampo(g, n, F1_038, ''        , DmBco.QrCNAB_CfgComplmCod.Value);
            AddCampo(g, n, F1_506, '0'       , Documento);
            // 2012-05-01 Est� errado! Como foi aceito at� agora?  Ver CONTIMAR !!!
            // AddCampo(g, n, F1_501, '0'      , QrProtPakItsDocum.Value);
            AddCampo(g, n, F1_501, '0'       , NossoNum_REM);
            AddCampo(g, n, F1_511, '0'       , DAC_NossoNum);
            AddCampo(g, n, F1_512, '0'       , Sequencial_TXT);
            // Fim 2012-05-01
            AddCampo(g, n, F1_509, ''        , DmBco.QrCNAB_CfgCartNum.Value);
            AddCampo(g, n, F1_508, ''        , DmBco.QrCNAB_CfgCartCod.Value);
            AddCampo(g, n, F1_504, ''        , '001'); // Minha ocorr�ncia para Remessa
            AddCampo(g, n, F1_502, ''        , QrProtPakItsDocum.Value);
            AddCampo(g, n, F1_580, 'dd/mm/yy', QrProtPakItsVencto.Value);
            AddCampo(g, n, F1_550, '0.00'    , QrProtPakItsValor.Value);
            AddCampo(g, n, F1_001, '000'     , DmBco.QrCNAB_CfgCedBanco.Value);
            AddCampo(g, n, F1_507, ''        , DmBco.QrCNAB_CfgEspecieTit.Value);
            AddCampo(g, n, F1_520, '0'       , DmBco.QrCNAB_CfgAceiteTit.Value);
            AddCampo(g, n, F1_549, ''        , DmBco.QrCNAB_CfgEspecieVal.Value);
            AddCampo(g, n, F1_583, 'dd/mm/yy', QrProtocoPakDataI.Value);
            AddCampo(g, n, F1_701, ''        , DmBco.QrCNAB_CfgInstrCobr1.Value);
            AddCampo(g, n, F1_702, ''        , DmBco.QrCNAB_CfgInstrCobr2.Value);
            AddCampo(g, n, F1_720, ''        , DmBco.QrCNAB_CfgNaoRecebDd.Value);
            AddCampo(g, n, F1_950, '00'      , DmBco.QrCNAB_CfgInstrDias.Value);
            AddCampo(g, n, F1_572, '0.00'    , JurosValr);
            AddCampo(g, n, F1_573, '0.00'    , MultaValr);
            AddCampo(g, n, F1_574, '0.00'    , JurosPerc);
            AddCampo(g, n, F1_575, '0.00'    , MultaPerc);
            AddCampo(g, n, F1_576, '0'       , DmBco.QrCNAB_CfgJurosTipo.Value);
            AddCampo(g, n, F1_577, '0'       , DmBco.QrCNAB_CfgMultaTipo.Value);
            AddCampo(g, n, F1_587, '0'       , DataMulta(QrProtPakItsVencto.Value, DmBco.QrCNAB_CfgMultaDias.Value));
            {// Descontos 1 a 3 N�o implementados!}
            AddCampo(g, n, F1_591, '0'       , DmBco.QrCNAB_CfgDesco1Cod.Value);
            if DataDesconto(1) = 0 then
              AddCampo(g, n, F1_592, ''      , '00/00/00')
            else
              AddCampo(g, n, F1_592, 'dd/mm/yy', DataDesconto(1));
            AddCampo(g, n, F1_593, '0.00'    , ValrDesconto(1));
            //
            AddCampo(g, n, F1_594, '0'       , DmBco.QrCNAB_CfgDesco2Cod.Value);
            if DataDesconto(2) = 0 then
              AddCampo(g, n, F1_595, ''      , '00/00/00')
            else
              AddCampo(g, n, F1_595, 'dd/mm/yy', DataDesconto(2));
            AddCampo(g, n, F1_596, '0.00'     , ValrDesconto(2));
            //
            AddCampo(g, n, F1_597, '0'        , DmBco.QrCNAB_CfgDesco3Cod.Value);
            if DataDesconto(3) = 0 then
              AddCampo(g, n, F1_598, ''        , '00/00/00')
            else
              AddCampo(g, n, F1_598, 'dd/mm/yy', DataDesconto(3));
            AddCampo(g, n, F1_599, '0.00'     , ValrDesconto(3));
            //Detalhe 3 - CNAB 400 - Fim

            //Detalhe 3 - CNAB 240 - In�cio
            AddCampo(gp, np, F3P_000, '0000'    , Item);
            AddCampo(gp, np, F3P_011, '0'       , 1);
            AddCampo(gp, np, F3P_501, '0'       , NossoNum_REM);
            AddCampo(gp, np, F3P_502, ''        , QrProtPakItsDocum.Value);
            AddCampo(gp, np, F3P_506, '0'       , Documento);
            AddCampo(gp, np, F3P_507, ''        , DmBco.QrCNAB_CfgEspecieTit.Value);
            AddCampo(gp, np, F3P_509, ''        , DmBco.QrCNAB_CfgCartNum.Value);
            AddCampo(gp, np, F3P_580, 'dd/mm/yy', QrProtPakItsVencto.Value);
            AddCampo(gp, np, F3P_550, '0.00'    , QrProtPakItsValor.Value);
            AddCampo(gp, np, F3P_520, '0'       , DmBco.QrCNAB_CfgAceiteTit.Value);
            AddCampo(gp, np, F3P_549, ''        , DmBco.QrCNAB_CfgEspecieVal.Value);
            AddCampo(gp, np, F3P_583, 'dd/mm/yy', QrProtocoPakDataI.Value);
            AddCampo(gp, np, F3P_574, '0.00'    , JurosPerc);
            AddCampo(gp, np, F3P_576, '0'       , DmBco.QrCNAB_CfgJurosTipo.Value);
            {// Descontos 1 a 3 N�o implementados!}
            if DataDesconto(1) = 0 then
              AddCampo(gp, np, F3P_592, ''      , '00/00/00')
            else
              AddCampo(gp, np, F3P_592, 'dd/mm/yy', DataDesconto(1));
            AddCampo(gp, np, F3P_593, '0.00'    , ValrDesconto(1));
            //Detalhe 5 - CNAB 240 - Fim
            //}
            (*
            DmBco.QrAddr3.Close;
            DmBco.QrAddr3.Params[0].AsInteger := QrProtPakItsCliente.Value;
            UMyMod.AbreQuery(DmBco.QrAddr3, Dmod.MyDB);
            *)
            if not DmBco.ReopenAddr3(QrProtPakItsCliente.Value, DmBco.QrCNAB_CfgModalCobr.Value, Msg) then
            begin
              Geral.MB_Aviso('A��o abortada!' + sLineBreak + 'Motivo: ' + Msg);
              //
              if Geral.MB_Pergunta('Deseja corrigir o cadastro agora?') = ID_YES then
                DModG.CadastroDeEntidade(QrProtPakItsCliente.Value, fmcadEntidade2, fmcadEntidade2);
              //
              Exit;
            end;
            //
            CEPSacado_ := Geral.SoNumero_TT(DmBco.QrAddr3ECEP_TXT.Value);
            //
            if DmBco.QrCNAB_CfgSacadAvali.Value <> 0 then
            begin
              if (FBanco <> 756) and (FNomeLayout <> CO_756_CORRESPONDENTE_BRADESCO_2015) then
                SacadorTipo := -1;
            end;
            //
            //Detalhe 3 - CNAB 400 - In�cio
            AddCampo(g, n, F1_801, '0'       , DmBco.QrAddr3Tipo.Value);
            AddCampo(g, n, F1_802, 'CNPJ'    , DmBco.QrAddr3CNPJ_CPF.Value);
            AddCampo(g, n, F1_803, ''        , DmBco.QrAddr3NOMEENT.Value);
            AddCampo(g, n, F1_811, ''        , DmBco.QrAddr3NOMELOGRAD.Value);
            AddCampo(g, n, F1_812, ''        , DmBco.QrAddr3RUA.Value);
            AddCampo(g, n, F1_813, ''        , DmBco.QrAddr3NUMERO_TXT.Value);
            AddCampo(g, n, F1_814, ''        , DmBco.QrAddr3COMPL.Value);
            AddCampo(g, n, F1_805, ''        , DmBco.QrAddr3BAIRRO.Value);
            AddCampo(g, n, F1_806, ''        , CEPSacado_);
            AddCampo(g, n, F1_807, ''        , DmBco.QrAddr3CIDADE.Value);
            AddCampo(g, n, F1_808, ''        , DmBco.QrAddr3NOMEUF.Value);
            AddCampo(g, n, F1_851, ''        , SacadorTipo);
            AddCampo(g, n, F1_852, ''        , SacadorCNPJ);
            AddCampo(g, n, F1_853, ''        , NomeSacador);
            AddCampo(g, n, F1_584, 'dd/mm/yy', DataMora);
            AddCampo(g, n, F1_621, '0'       , DmBco.QrCNAB_CfgQuemPrint.Value);
            AddCampo(g, n, F1_627, '0'       , DmBco.QrCNAB_CfgQuemDistrb.Value);
            AddCampo(g, n, F1_639, '0'       , DmBco.QrCNAB_Cfg_237Mens1.Value);
            AddCampo(g, n, F1_640, '0'       , DmBco.QrCNAB_Cfg_237Mens2.Value);
            AddCampo(g, n, F1_665, ''        , DmBco.QrCNAB_CfgTipBloqUso.Value);
            AddCampo(g, n, F1_647, '0'       , DmBco.QrCNAB_CfgProtesDds.Value);
            // 2012-11-01
            AddCampo(g, n, F1_643, '0'       , DmBco.QrCNAB_CfgIndicatBB.Value);
            AddCampo(g, n, F1_034, '0'       , DmBco.QrCNAB_CfgTipoCart.Value);
            AddCampo(g, n, F1_410, '0'       , DmBco.QrCNAB_CfgCodEmprBco.Value);
            // Fim 2012-11-01

            //  I N F O R M A T I V O S
            // Endere�o completo do logradouro
            AddCampo(g, n, F1_804, ''        , Endereco());

            //  N � O   I M P L E M E N T A D O S
            // Data limite para desconto (n�o tem)
            AddCampo(g, n, F1_586, ''        , '00/00/00');
            // Valor do desconto
            AddCampo(g, n, F1_552, ''        , 0);
            // Valor do IOF
            AddCampo(g, n, F1_569, ''        , 0);
            // Valor do deconto bonifica��o
            AddCampo(g, n, F1_558, ''        , 0);
            // Valor do t�tulo em outra unidade (008, 033, 353 - Santander banespa)
            AddCampo(g, n, F1_667, '0'       , 0);
            // 2012-11-01
            // N�mero da parcela da fatura (Banco 756)
            AddCampo(g, n, F1_513, '0'       , 1);
            // N�mero do Contrato de Garantia (Banco 756)
            AddCampo(g, n, F1_514, '0'       , 0);
            // DV do N�mero do Contrato de Garantia (Banco 756)
            AddCampo(g, n, F1_515, '0'       , 0);
            // N�mero do Border�
            AddCampo(g, n, F1_516, '0'       , 0);
            // FIM 2012-11-01

            // I M P L E M E N T A D O   A P E N A S   P A R A :  CO_399_MODULO_I_2009_10
            // Valor do abatimento
            AddCampo(g, n, F1_551, ''        , 0); // Pr� define em zero para n�o ficar vazio
            //Detalhe 3 - CNAB 400 - Fim

            //Detalhe 3 - CNAB 240 - In�cio
            AddCampo(gq, nq, F3Q_801, '0'       , DmBco.QrAddr3Tipo.Value);
            AddCampo(gq, nq, F3Q_802, 'CNPJ'    , DmBco.QrAddr3CNPJ_CPF.Value);
            AddCampo(gq, nq, F3Q_803, ''        , DmBco.QrAddr3NOMEENT.Value);
            AddCampo(gq, nq, F3Q_805, ''        , DmBco.QrAddr3BAIRRO.Value);
            AddCampo(gq, nq, F3Q_806, ''        , CEPSacado_);
            AddCampo(gq, nq, F3Q_807, ''        , DmBco.QrAddr3CIDADE.Value);
            AddCampo(gq, nq, F3Q_808, ''        , DmBco.QrAddr3NOMEUF.Value);
            AddCampo(gq, nq, F3Q_851, ''        , SacadorTipo);
            AddCampo(gq, nq, F3Q_852, ''        , SacadorCNPJ);
            AddCampo(gq, nq, F3Q_853, ''        , NomeSacador);
            AddCampo(gp, np, F3P_621, '0'       , DmBco.QrCNAB_CfgQuemPrint.Value);
            AddCampo(gp, np, F3P_647, '0'       , DmBco.QrCNAB_CfgProtesDds.Value);
            AddCampo(gp, np, F3P_651, '0'       , DmBco.QrCNAB_CfgProtesCod.Value);
            //  I N F O R M A T I V O S
            // Endere�o completo do logradouro
            AddCampo(gq, nq, F3Q_804, ''        , Endereco());
            //Detalhe 3 - CNAB 240 - Fim

            if FBanco = 399 then
            begin
              if FNomeLayout = CO_399_MODULO_I_2009_10 then
              begin
                //Ocorrencia :=
                GeraCampo551_399_MODULO_I_2009_10(g, n,
                  MultaPerc, MultaValr, DiasMult, DataMult,
                  JurosPerc, JurosValr, DiasMora, DataMora);
                GeraCampo720_399_MODULO_I_2009_10(g, n);
                GeraCampo647_399_MODULO_I_2009_10(g, n);
              end;
            end;

        //
        ///////////////// -2- R E G I S T R O    M E N S A G E M ///////////////////
        g := Grade_2;
        n := AddLinha(FLinG2, Grade_2);

        Texto01 := DmBco.TraduzInstrucaoBloqueto(DmBco.QrCNAB_CfgTexto01.Value, QrProtPakItsValor.Value, MultaPerc, JurosPerc, 0, 0);
        Texto02 := DmBco.TraduzInstrucaoBloqueto(DmBco.QrCNAB_CfgTexto02.Value, QrProtPakItsValor.Value, MultaPerc, JurosPerc, 0, 0);
        Texto03 := DmBco.TraduzInstrucaoBloqueto(DmBco.QrCNAB_CfgTexto03.Value, QrProtPakItsValor.Value, MultaPerc, JurosPerc, 0, 0);
        Texto04 := DmBco.TraduzInstrucaoBloqueto(DmBco.QrCNAB_CfgTexto04.Value, QrProtPakItsValor.Value, MultaPerc, JurosPerc, 0, 0);

        AddCampo(g, n, F2_201, ''  , Texto01);
        AddCampo(g, n, F2_202, ''  , Texto02);
        AddCampo(g, n, F2_203, ''  , Texto03);
        AddCampo(g, n, F2_204, ''  , Texto04);
        AddCampo(g, n, F2_508, '0' , DmBco.QrCNAB_CfgCartNum.Value);
        //
        ///////////////// -3- R E G I S T R O    D E T A L H E ///////////////////

        ///////////////// -5- R E G I S T R O    D E T A L H E ///////////////////
            if ((DmBco.QrCNAB_CfgEnvEmeio.Value = 1) and
            (DmBco.QrAddr3EMail.Value <> '')) or
            (DmBco.QrCNAB_CfgSacadAvali.Value <> 0) then
            begin
              // � o mesmo do registro 1
              //inc(Item, 1);
              g := Grade_5;
              n := AddLinha(FLinG5, Grade_5);
              AddCampo(g, n, F5_000, '0000' , Item);
              AddCampo(g, n, F5_011, '0'      , 5);
              //AddCampo(g, n, F5_000, '000000' , Linha);
              //AddCampo(g, n, F5_011, '0'      , 5);
              AddCampo(g, n, F5_815, ''       , DmBco.QrAddr3EMail.Value);
              //
              DmBco.QrAddr3.Close;
              if DmBco.QrCNAB_CfgSacadAvali.Value <> 0 then
              begin
                (*
                DmBco.QrAddr3.Params[0].AsInteger := DmBco.QrCNAB_CfgSacadAvali.Value;
                UMyMod.AbreQuery(DmBco.QrAddr3, Dmod.MyDB);
                *)
                if not DmBco.ReopenAddr3(DmBco.QrCNAB_CfgSacadAvali.Value, DmBco.QrCNAB_CfgModalCobr.Value, Msg) then
                begin
                  Geral.MB_Aviso('A��o abortada!' + sLineBreak + 'Motivo: ' + Msg);
                  //
                  if Geral.MB_Pergunta('Deseja corrigir o cadastro agora?') = ID_YES then
                    DModG.CadastroDeEntidade(QrProtPakItsCliente.Value, fmcadEntidade2, fmcadEntidade2);
                  //
                  Exit;
                end;
                CEPSacador       := Geral.SoNumero_TT(DmBco.QrAddr3ECEP_TXT.Value);
                Addr3_Tipo       := DmBco.QrAddr3Tipo.Value;
                Addr3_CNPJ       := DmBco.QrAddr3CNPJ_CPF.Value;
                Addr3_NOMEENT    := DmBco.QrAddr3NOMEENT.Value;
                Addr3_NOMELOGRAD := DmBco.QrAddr3NOMELOGRAD.Value;
                Addr3_RUA        := DmBco.QrAddr3RUA.Value;
                Addr3_NUMERO_TXT := DmBco.QrAddr3NUMERO_TXT.Value;
                Addr3_COMPL      := DmBco.QrAddr3COMPL.Value;
                Addr3_BAIRRO     := DmBco.QrAddr3BAIRRO.Value;
                Addr3_CIDADE     := DmBco.QrAddr3CIDADE.Value;
                Addr3_NOMEUF     := DmBco.QrAddr3NOMEUF.Value;
              end else begin
                CEPSacador       := '';
                Addr3_Tipo       := -1;
                Addr3_CNPJ       := '';
                Addr3_NOMEENT    := '';
                Addr3_NOMELOGRAD := '';
                Addr3_RUA        := '';
                Addr3_NUMERO_TXT := '';
                Addr3_COMPL      := '';
                Addr3_BAIRRO     := '';
                Addr3_CIDADE     := '';
                Addr3_NOMEUF     := '';
              end;
              //
              AddCampo(g, n, F5_851, '0'      , Addr3_Tipo);
              AddCampo(g, n, F5_852, 'CNPJ'   , Addr3_CNPJ);
              AddCampo(g, n, F5_861, ''       , Addr3_NOMELOGRAD);
              AddCampo(g, n, F5_862, ''       , Addr3_RUA);
              AddCampo(g, n, F5_863, ''       , Addr3_NUMERO_TXT);
              AddCampo(g, n, F5_864, ''       , Addr3_COMPL);
              AddCampo(g, n, F5_855, ''       , Addr3_BAIRRO);
              AddCampo(g, n, F5_856, ''       , CEPSacador);
              AddCampo(g, n, F5_857, ''       , Addr3_CIDADE);
              AddCampo(g, n, F5_858, ''       , Addr3_NOMEUF);
              //
              //  I N F O R M A T I V O S
              // Endere�o completo do logradouro
              AddCampo(g, n, F5_854, ''        , Endereco());
            end;
            QrProtPakIts.Next;
          end;
        end;
        ////////////////// -9- R E G I S T R O    T R A I L E R //////////////////
        dmkEd_9_302.ValueVariant := ValTitulos;

        HoraF := Now();
        dmkEdHoraF.ValueVariant := HoraF;
        dmkEdTempo.ValueVariant := (HoraF - HoraI) * 24 * 60 * 60;
        Screen.Cursor := crDefault;
        ShowModal;
        Destroy;
        // alterado o SeqArq - reabrir
        ReopenProtocoPak(QrProtocoPakControle.Value);
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
{$Else}
begin
  Geral.MB_Aviso('Diretiva CNAB desabilitada!');
{$EndIf}
end;

function TFmProtocolos.GeraCampo551_399_MODULO_I_2009_10(Grade: TStringGrid;
Linha: Integer; MultaPerc, MultaValr, DiasMult, DataMult,
                JurosPerc, JurosValr, DiasMora, DataMora: Double): String;
{$IfNDef NO_CNAB}
var
  Formato, Dias, Data, Mult, Tipo, Bran: String;
  Valor: Variant;
  Ocorrencia, InstrucaoA, Instrucao1, Instrucao2: Integer;
begin
  Valor := 0;
  Ocorrencia := DmBco.QrCNAB_CfgComando.Value;
  //
  case Ocorrencia of
    1:
    begin
      InstrucaoA := -999999999;
      Instrucao1 := Geral.IMV(DmBco.QrCNAB_CfgInstrCobr1.Value);
      Instrucao2 := Geral.IMV(DmBco.QrCNAB_CfgInstrCobr2.Value);

      // Instru��es concorrentes aos campos 193 a 218
      if Instrucao1 in ([15,16,19,22,24,29,73,74]) then
        InstrucaoA := Instrucao1
      else
        if Instrucao2 in ([15,16,19,22,24,29,73,74]) then
          InstrucaoA := Instrucao2;
      case InstrucaoA of
        16: // posi��es 193 a 218 (Campo do IOF e do Abatimento usados para cobrar multa!)
        begin
           //  C�lculo da multa por percentual ao m�s
          { N�o tem como tirar os zeros do campo IOF!
          // Campo originalmente do IOF (193 A 205)
          // Deixar o campo IOF (193 a 205) com brancos!
          FmCNAB_Rem.AddCampo(Grade, Linha, FmCNAB_Rem.F1_569, '', '             ');

          // Campo originalmente do abatimento (206 A 218):
          // Data Inicial da cobran�a da multa (206 a 211)
          Data := FormatDateTime('DDMMYY', DataMult);
          // % da multa (212 a 215)
          Mult := Geral.FTX(MultaPerc, 2, 2, siPositivo);
          // Como cobrar (216 a 216)
          // seria Tipo = 'V' se fosse multa di�ria em R$, mas vou usar %, ent�o:
          Tipo := ' ';  // fica em branco para uso por %
          // N�o utilizado??? (217 a 218)
          Bran := '  ';
          //
          Valor := Data + Mult + Tipo + Bran;
          FmCNAB_Rem.AddCampo(Grade, Linha, FmCNAB_Rem.F1_551, '', Valor);
          } // Termina aqui se a multa � por percentual ao m�s!


          // Campo originalmente do IOF (193 A 205)
          // Colocar aqui o valor da multa diaria! (193 a 205) com brancos!
          Mult := Geral.FTX(MultaValr, 11, 2, siPositivo);
          FmCNAB_Rem.AddCampo(Grade, Linha, FmCNAB_Rem.F1_569, '', Mult);

          // Campo originalmente do abatimento (206 A 218):
          // Data Inicial da cobran�a da multa (206 a 211)
          Data := FormatDateTime('DDMMYY', DataMult);
          // Deixar em branco (212 a 215)
          Mult := '    ';
          // Como cobrar (216 a 216)
          // seria Tipo = 'V' se fosse multa di�ria em R$, mas vou usar %, ent�o:
          Tipo := 'V';  // fica em branco para uso por %
          // N�o utilizado??? (217 a 218)
          Bran := '  ';
          //
          Valor := Data + Mult + Tipo + Bran;
          FmCNAB_Rem.AddCampo(Grade, Linha, FmCNAB_Rem.F1_551, '', Valor);
        end;
        // 15, 19, 22, 24, 73, 74: as posi��es 206 a 218 (Campo do Abatimento usados para cobrar multa!)
        15:
        begin
          // Campo originalmente do abatimento (206 A 218):
          // Data Inicial da cobran�a da multa (206 a 211)
          Data := FormatDateTime('DDMMYY', DataMult);
          // Deixar em branco (212 a 215)
          Mult := Geral.FTX(MultaPerc, 2, 2, siPositivo);
          // Brancos (216 a 218)
          Bran := '   ';
          //
          Valor := Data + Mult + Bran;
          FmCNAB_Rem.AddCampo(Grade, Linha, FmCNAB_Rem.F1_551, '', Valor);
        end;
        19, 22, 24:
        begin
          // Campo originalmente do abatimento (206 A 218):
          // Valor da multa (206 a 215)
          Mult := Geral.FTX(MultaValr, 8, 2, siPositivo);
          // Dias corridos(19) ou �teis (22) ou zero (24) ap�s o vencimento (216 a 218)
          if InstrucaoA = 24 then
            Dias := '000'
          else
            Dias := Geral.FTX(DiasMult, 3, 0, siPositivo);
          //
          Valor := Mult + Dias;
          FmCNAB_Rem.AddCampo(Grade, Linha, FmCNAB_Rem.F1_551, '', Valor);
        end;
        29: // posi��es 206 a 218 (Campo do Abatimento usados para informar uma data inicial para cobran�a de juros, mas quando cobrar, cobrar desde o vencimento!)
        begin
          // Data Inicial da cobran�a dos juros desde o vencimento (206 a 211)
          Data := FormatDateTime('DDMMYY', DataMora);
          // Brancos?? ou zeros??? (212 a 218)
          Bran := '       ';
          //
          Valor := Data + Bran;
          FmCNAB_Rem.AddCampo(Grade, Linha, FmCNAB_Rem.F1_551, '', Valor);
        end;
        73, 74:
        begin
          // Brancos (206 a 211)
          Bran := '      ';
          // % da multa (212 a 215)
          Mult := Geral.FTX(MultaPerc, 2, 2, siPositivo);
          // Dias corridos(73) ou �teis (74) ap�s o vencimento (216 a 218)
          Dias := Geral.FTX(DiasMult, 3, 0, siPositivo);
          //
          Valor := Bran + Mult + Dias;
          FmCNAB_Rem.AddCampo(Grade, Linha, FmCNAB_Rem.F1_551, '', Valor);
        end;
      end;
    end;
    4,5:
    begin
      // N�o vale para remessa! S� para concess�o e cancelamento de abatimento via instru��o p�s envio de lote!
    end;
    else
    begin
      Geral.MB_Aviso(
      'Ocorr�ncia inv�lida para defini��o de Abatimrnto / Multa');
    end;
  end;
  FmCNAB_Rem.AddCampo(Grade, Linha, FmCNAB_Rem.F1_551, Formato, Valor);
{$Else}
begin
  Geral.MB_Aviso('Diretiva CNAB desabilitada!');
{$EndIf}
end;

function TFmProtocolos.GeraCampo647_399_MODULO_I_2009_10(Grade: TStringGrid;
  Linha: Integer): String;
{$IfNDef NO_CNAB}
var
  Valor: Variant;
begin
  if DmBco.QrCNAB_CfgProtesCod.Value = 1 then // C�digo que usei pois n�o existe nenhum!
    Valor := Geral.FFN(DmBco.QrCNAB_CfgProtesDds.Value, 2)
  else
    Valor := '  ';
  //
  FmCNAB_Rem.AddCampo(Grade, Linha, FmCNAB_Rem.F1_647, '', Valor);
{$Else}
begin
  Geral.MB_Aviso('Diretiva CNAB desabilitada!');
{$EndIf}
end;

function TFmProtocolos.GeraCampo720_399_MODULO_I_2009_10(Grade: TStringGrid;
  Linha: Integer): String;
{$IfNDef NO_CNAB}
var
  //Formato, Dias, Data, Mult, Tipo, Bran: String;
  Valor: Variant;
  //NaoRecebDd,
  Ocorrencia, InstrucaoA, Instrucao1, Instrucao2: Integer;
begin
  Valor := '  ';
  Ocorrencia := DmBco.QrCNAB_CfgComando.Value;
  //
  case Ocorrencia of
    1:
    begin
      InstrucaoA := -999999999;
      Instrucao1 := Geral.IMV(DmBco.QrCNAB_CfgInstrCobr1.Value);
      Instrucao2 := Geral.IMV(DmBco.QrCNAB_CfgInstrCobr2.Value);

      // Instru��es concorrentes aos campos 313 a 314
      if Instrucao1 in ([71,72]) then
        InstrucaoA := Instrucao1
      else
        if Instrucao2 in ([71,72]) then
          InstrucaoA := Instrucao2;
      case InstrucaoA of
        71, 72: Valor := Geral.FFN(DmBco.QrCNAB_CfgNaoRecebDd.Value, 2);
        else Valor := '  ';
      end;
    end;
  end;
  FmCNAB_Rem.AddCampo(Grade, Linha, FmCNAB_Rem.F1_720, '', Valor);
{$Else}
begin
  Geral.MB_Aviso('Diretiva CNAB desabilitada!');
{$EndIf}
end;

procedure TFmProtocolos.Zerarnmero1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Confirma o zeramento do n�mero de arquivo de remessa deste lote?') = ID_YES
  then begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE protocopak SET SeqArq=0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
    Dmod.QrUpd.Params[00].AsInteger := QrProtocoPakControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenProtocoPak(QrProtocoPakControle.Value);
  end;
end;

end.

