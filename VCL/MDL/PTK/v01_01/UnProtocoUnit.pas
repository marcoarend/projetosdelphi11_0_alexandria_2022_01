unit UnProtocoUnit;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  ComCtrls, (*DBTables,*) UnMyLinguas, Forms, UnInternalConsts, dmkGeral,
  Variants, dmkDBGrid, mySQLDbTables, DBGrids, dmkDBGridZTO, Protocolo,
  //
  //AdvToolBar,
  //
  //mySQLDbTables,
  //Planner,
  UnDmkEnums, AdvToolBar;

type
  TUnProtocoUnit = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    //
    procedure MostraFormProtocolos(Lote, Tarefa: Integer);
    procedure MostraFormProtocoMot();
    procedure MostraFormProtocoOco();
    (* N�o usa
    procedure MostraProSel(Tarefa, EntCliInt, Protocolo, Entidade,
                Periodo, PrevCod, Aba: Integer; NomeQrSrc, RecipEmeio: String;
                Bloqueto, Valor: Double; Vencto: TDate; Quais: TSelType;
                QrSource: TmySQLQuery; Grade: TdmkDBGrid; var Inseriu,
                Desistiu: Boolean);
    *)
    procedure MostraProtoAddTar(ProtNum, Tarefa: Integer; Finalidade: TProtFinalidade;
                TiposProt: String; DBGradeBol: TdmkDBGridZTO; Query: TmySQLQuery;
                CliInt: Integer = 0); overload;
    procedure MostraProtoAddTar(Finalidade: TProtFinalidade; TiposProt: String;
                DBGradeBol: TdmkDBGridZTO; Query: TmySQLQuery; CliInt: Integer = 0); overload;
    procedure MostraProtoGer(AbrirEmAba: Boolean;
                InOwner: TWincontrol; AdvToolBarPager: TAdvToolBarPager;
                Tarefa, Lote: Integer);
    procedure MostraProEnPr(Codigo: Integer);
  end;

//const

var
  ProtocoUnit: TUnProtocoUnit;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, MyDBCheck,
  ModuleGeral, (*CfgCadLista, ProSel, *) ProtoAddTar, ProtoGer, ProEnPr,
  Protocolos, ProtocoMot, ProtocoOco;

{ TUnProtocoUnit }

{ TUnProtocoUnit }

procedure TUnProtocoUnit.MostraProEnPr(Codigo: Integer);
begin
{$IfNDef SemProtocolo}
  if DBCheck.CriaFm(TFmProEnPr, FmProEnPr, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmProEnPr.LocCod(Codigo, Codigo);
    FmProEnPr.ShowModal;
    FmProEnPr.Destroy;
  end;
{$Else}
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

procedure TUnProtocoUnit.MostraFormProtocolos(Lote, Tarefa: Integer);
begin
{$IfNDef SemProtocolo}
  if DBCheck.CriaFm(TFmProtocolos, FmProtocolos, afmoNegarComAviso) then
  begin
    if (Lote = 0) and (Tarefa <> 0) then
      FmProtocolos.LocCod(Tarefa, Tarefa);
    FmProtocolos.FLocLote := Lote;
    FmProtocolos.ShowModal;
    FmProtocolos.Destroy;
    DModG.ReopenPTK();
  end;
{$Else}
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

procedure TUnProtocoUnit.MostraFormProtocoMot();
begin
{$IfNDef SemProtocolo}
  if DBCheck.CriaFm(TFmProtocoMot, FmProtocoMot, afmoNegarComAviso) then
  begin
    FmProtocoMot.ShowModal;
    FmProtocoMot.Destroy;
  end;
{$Else}
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

procedure TUnProtocoUnit.MostraFormProtocoOco();
begin
{$IfNDef SemProtocolo}
  if DBCheck.CriaFm(TFmProtocoOco, FmProtocoOco, afmoNegarComAviso) then
  begin
    FmProtocoOco.ShowModal;
    FmProtocoOco.Destroy;
  end;
{$Else}
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

(* N�o usa
procedure TUnProtocoUnit.MostraProSel(Tarefa, EntCliInt, Protocolo, Entidade,
  Periodo, PrevCod, Aba: Integer; NomeQrSrc, RecipEmeio: String; Bloqueto,
  Valor: Double; Vencto: TDate; Quais: TSelType; QrSource: TmySQLQuery;
  Grade: TdmkDBGrid; var Inseriu, Desistiu: Boolean);
begin
  if DBCheck.CriaFm(TFmProSel, FmProSel, afmoNegarComAviso) then
  begin
    FmProSel.ReopenProtocolos(UnProtocolo.GetTipoProtocolo(Aba), Tarefa, EntCliInt);
    //
    FmProSel.EdTarefa.ValueVariant := Tarefa;
    FmProSel.CBTarefa.KeyValue     := Tarefa;
    FmProSel.FQuais                := Quais;
    FmProSel.FNomeQrSrc            := NomeQrSrc;
    FmProSel.FQrSource             := QrSource;
    FmProSel.FDBGrid               := TDBGrid(Grade);
    FmProSel.FProtocolo            := Protocolo;
    FmProSel.FEntidade             := Entidade;
    FmProSel.FBloqueto             := Bloqueto;
    FmProSel.FVencto               := Vencto;
    FmProSel.FValor                := Valor;
    FmProSel.FPeriodo              := Periodo;
    FmProSel.FPrevCod              := PrevCod;
    FmProSel.FRecipEmeio           := RecipEmeio;
    //
    FmProSel.ShowModal;
    //
    Inseriu  := FmProSel.FInseriu;
    Desistiu := FmProSel.FDesistiu;
    //
    FmProSel.Destroy;
  end;
end;
*)

procedure TUnProtocoUnit.MostraProtoAddTar(ProtNum, Tarefa: Integer;
  Finalidade: TProtFinalidade; TiposProt: String; DBGradeBol: TdmkDBGridZTO;
  Query: TmySQLQuery; CliInt: Integer = 0);
var
  i, Empresa: Integer;
begin
  Empresa := 0;
  //
  //Apenas para boletos
  if (Query <> nil) and (Query.State <> dsInactive) and (Query.RecordCount > 0)
    and (Finalidade = ptkBoleto) then
  begin
    if DBGradeBol.SelectedRows.Count > 1 then
    begin
      with DBGradeBol.DataSource.DataSet do
      for i := 0 to DBGradeBol.SelectedRows.Count - 1 do
      begin
        GotoBookmark(pointer(DBGradeBol.SelectedRows.Items[i]));

        if Empresa = 0 then
          Empresa := Query.FieldByName('Cedente').AsInteger;

        if Empresa <> Query.FieldByName('Cedente').AsInteger then
        begin
          Geral.MB_Aviso('Todos os boletos selecionados devem ser da mesma empresa!');
          Exit;
        end;
      end;
    end else
      Empresa := Query.FieldByName('Cedente').AsInteger;
  end else
  if (Finalidade = ptkNFSeAut) or (Finalidade = ptkNFSeCan) then
    Empresa := CliInt;
  //
  if DBCheck.CriaFm(TFmProtoAddTar, FmProtoAddTar, afmoNegarComAviso) then
  begin
    FmProtoAddTar.EdTarefa.ValueVariant := Tarefa;
    FmProtoAddTar.CBTarefa.KeyValue     := Tarefa;
    FmProtoAddTar.FProtNum              := ProtNum;
    FmProtoAddTar.FTiposProt            := TiposProt;
    FmProtoAddTar.FQuery                := Query;
    FmProtoAddTar.FCliInt               := Empresa;
    FmProtoAddTar.FFinalidade           := Finalidade;
    FmProtoAddTar.FGrade                := DBGradeBol;
    FmProtoAddTar.ShowModal;
    FmProtoAddTar.Destroy;
  end;
end;

procedure TUnProtocoUnit.MostraProtoAddTar(Finalidade: TProtFinalidade;
  TiposProt: String; DBGradeBol: TdmkDBGridZTO; Query: TmySQLQuery;
  CliInt: Integer);
begin
  //Compatibilidade usada apenas no PTK/v02.01
  ProtocoUnit.MostraProtoAddTar(-1, 0, Finalidade,
    Geral.FF0(VAR_TIPO_PROTOCOLO_CE_02), nil, nil, CliInt);
end;

procedure TUnProtocoUnit.MostraProtoGer(AbrirEmAba: Boolean;
  InOwner: TWincontrol; AdvToolBarPager: TAdvToolBarPager;
  Tarefa, Lote: Integer);
var
  Form: TForm;
  Empresa: Integer;
begin
{$IfNDef SemProtocolo}
  Empresa := DmodG.QrFiliLogFilial.Value;
  //
  if AbrirEmAba then
  begin
    Form:= MyObjects.FormTDICria(TFmProtoGer, InOwner, AdvToolBarPager, False, True);
    //
    TFmProtoGer(Form).MostraEdicao(gerSta, -1, Tarefa, Lote, Empresa, 0);
  end else
  begin
    if DBCheck.CriaFm(TFmProtoGer, FmProtoGer, afmoNegarComAviso) then
    begin
      FmProtoGer.MostraEdicao(gerSta, -1, Tarefa, Lote, Empresa, 0);
      FmProtoGer.ShowModal;
      FmProtoGer.Destroy;
    end;
  end;
{$Else}
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

end.
