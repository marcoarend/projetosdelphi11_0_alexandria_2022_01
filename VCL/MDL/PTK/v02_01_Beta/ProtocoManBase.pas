unit ProtocoManBase;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, DBCtrls, DmkDAC_PF,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkGeral,
  dmkImage, UnDmkEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmProtocoManBase = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    EdProtBaseC: TdmkEditCB;
    CBProtBaseC: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrProtBaseC: TmySQLQuery;
    DsProtBaseC: TDataSource;
    QrProtBaseCCodigo: TIntegerField;
    QrProtBaseCCodUsu: TIntegerField;
    QrProtBaseCNome: TWideStringField;
    QrProtBaseI: TmySQLQuery;
    QrProtBaseIOcorrencia: TIntegerField;
    QrProtBaseIincPerio: TSmallintField;
    QrProtBaseIRetorna: TSmallintField;
    QrProtBaseITexto: TWideStringField;
    QrProtBaseILimiteSai: TIntegerField;
    QrProtBaseILimiteRem: TIntegerField;
    QrProtBaseILimiteRet: TIntegerField;
    QrProtBaseIIncVencto: TIntegerField;
    QrProtBaseIValor: TFloatField;
    TbProtPakI: TmySQLTable;
    DsProtPakI: TDataSource;
    DBGrid1: TDBGrid;
    QrProtocoOco: TmySQLQuery;
    QrProtocoOcoCodigo: TIntegerField;
    QrProtocoOcoNome: TWideStringField;
    TbProtPakIManual: TIntegerField;
    TbProtPakIDataE: TDateField;
    TbProtPakIDepto: TIntegerField;
    TbProtPakISerieCH: TWideStringField;
    TbProtPakIDocum: TFloatField;
    TbProtPakIValor: TFloatField;
    TbProtPakIVencto: TDateField;
    TbProtPakIRetorna: TSmallintField;
    TbProtPakITexto: TWideStringField;
    TbProtPakILimiteSai: TDateField;
    TbProtPakILimiteRem: TDateField;
    TbProtPakILimiteRet: TDateField;
    TbProtPakINO_MANUAL: TWideStringField;
    TbProtPakIMesAno: TDateField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrProtBaseCAfterOpen(DataSet: TDataSet);
    procedure EdProtBaseCChange(Sender: TObject);
    procedure EdProtBaseCExit(Sender: TObject);
    procedure EdProtBaseCEnter(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TbProtPakIBeforePost(DataSet: TDataSet);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1ColEnter(Sender: TObject);
    procedure DBGrid1ColExit(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FCodUsu: Integer;
    FProtoPakI: String;
    procedure RelancaProtBaseI();
  public
    { Public declarations }
  end;

  var
  FmProtocoManBase: TFmProtocoManBase;

implementation

{$R *.DFM}

uses UnMyObjects, Module, ModuleGeral, UCreate, UMySQLModule, MyVCLSkin,
  Protocolos, UnDmkProcFunc;

procedure TFmProtocoManBase.BtOKClick(Sender: TObject);
var
  //MesAno,
  Conta, Manual, Depto, Retorna, Codigo, Controle, CliInt, Periodo: Integer;
  Texto, Vencto, DataE, SerieCH, LimiteSai, LimiteRem, LimiteRet: String;
  Docum, Valor: Double;
begin
  TbProtPakI.DisableControls;
  TbProtPakI.First;
  while not TbProtPakI.Eof do
  begin
    Conta     := UMyMod.BuscaEmLivreY_Def('protpakits', 'Conta', stIns, 0);
    //
    Codigo    := FmProtocolos.QrProtocoPakCodigo.Value;
    Controle  := FmProtocolos.QrProtocoPakControle.Value;
    CliInt    := FmProtocolos.QrProtocolosDef_Client.Value;
    //
    Manual    := TbProtPakIManual.Value;
    DataE     := Geral.FDT(TbProtPakIDataE.Value, 1);
    //MesAno    := TbProtPakIMesAno.Value;
    Periodo   := Geral.Periodo2000(TbProtPakIMesAno.Value);
    Depto     := TbProtPakIDepto.Value;
    SerieCH   := TbProtPakISerieCH.Value;
    Docum     := TbProtPakIDocum.Value;
    Valor     := TbProtPakIValor.Value;
    Vencto    := Geral.FDT(TbProtPakIVencto.Value, 1);
    Retorna   := TbProtPakIRetorna.Value;
    Texto     := TbProtPakITexto.Value;
    LimiteSai := Geral.FDT(TbProtPakILimiteSai.Value, 1);
    LimiteRem := Geral.FDT(TbProtPakILimiteRem.Value, 1);
    LimiteRet := Geral.FDT(TbProtPakILimiteRet.Value, 1);
    //
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'protpakits', False, [
    'Codigo', 'Controle', (*'Link_ID',*)
    'CliInt', (*'Cliente', 'Cedente',
    'Fornece',*) 'Periodo', (*'Lancto',*)
    'Docum', 'Depto', 'DataE',
    (*'DataD',*) 'Retorna', (*'Cancelado',
    'Motivo', 'ID_Cod1', 'ID_Cod2',
    'ID_Cod3', 'ID_Cod4',*) 'Vencto',
    'Valor', (*'MoraDiaVal', 'MultaVal',
    'ComoConf',*) 'SerieCH', 'Manual',
    'Texto', (*'Saiu', 'Recebeu',
    'Retornou',*) 'LimiteSai', 'LimiteRem',
    'LimiteRet'(*, 'DataSai', 'DataRec',
    'DataRet'*)], [
    'Conta'], [
    Codigo, Controle, (*Link_ID,*)
    CliInt, (*Cliente, Cedente,
    Fornece,*) Periodo, (*Lancto,*)
    Docum, Depto, DataE,
    (*DataD,*) Retorna, (*Cancelado,
    Motivo, ID_Cod1, ID_Cod2,
    ID_Cod3, ID_Cod4,*) Vencto,
    Valor, (*MoraDiaVal, MultaVal,
    ComoConf,*) SerieCH, Manual,
    Texto, (*Saiu, Recebeu,
    Retornou,*) LimiteSai, LimiteRem,
    LimiteRet(*, DataSai, DataRec,
    DataRet*)], [
    Conta], True);
    //
    TbProtPakI.Next;
  end;
  //N�o precisa, vai fechar! TbProtPakI.EnableControls;
  FmProtocolos.ReopenProtPakIts(0);
  //
  Close;
end;

procedure TFmProtocoManBase.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProtocoManBase.DBGrid1CellClick(Column: TColumn);
begin
  if Column.FieldName = 'Retorna' then
  begin
    TbProtPakI.Edit;
    if TbProtPakIRetorna.Value = 1 then
      TbProtPakIRetorna.Value := 0
    else
      TbProtPakIRetorna.Value := 1;
    TbProtPakI.Post;
  end;
end;

procedure TFmProtocoManBase.DBGrid1ColEnter(Sender: TObject);
begin
  if DBGrid1.Columns[THackDBGrid(DBGrid1).Col -1].FieldName = 'Retorna' then
    DBGrid1.Options := DBGrid1.Options - [dgEditing] else
    DBGrid1.Options := DBGrid1.Options + [dgEditing];

end;

procedure TFmProtocoManBase.DBGrid1ColExit(Sender: TObject);
begin
  DBGrid1.Options := DBGrid1.Options + [dgEditing];
end;

procedure TFmProtocoManBase.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Retorna' then
    //MeuVCLSkin.DrawGridCheck(DBGrid1, Rect, 1, TbProtPakIRetorna.Value, dmkrc0e1);
end;

procedure TFmProtocoManBase.EdProtBaseCChange(Sender: TObject);
begin
  if not EdProtBaseC.Focused then
    RelancaProtBaseI();
end;

procedure TFmProtocoManBase.EdProtBaseCEnter(Sender: TObject);
begin
  FCodUsu := EdProtBaseC.ValueVariant;
end;

procedure TFmProtocoManBase.EdProtBaseCExit(Sender: TObject);
begin
  if FCodUsu <> EdProtBaseC.ValueVariant then
  begin
    FCodUsu := EdProtBaseC.ValueVariant;
    RelancaProtBaseI();
  end;
end;

procedure TFmProtocoManBase.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProtocoManBase.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrProtocoOco, Dmod.MyDB);
  //
  TbProtPakI.Close;
  TbProtPakI.Database := DModG.MyPID_DB;
  //
  FCodUsu    := 0;
  FProtoPakI := UCriar.RecriaTempTable('ProtPakI', DModG.QrUpdPID1, False);
  //
  QrProtBaseC.Close;
  UnDmkDAC_PF.AbreQuery(QrProtBaseC, Dmod.MyDB);
end;

procedure TFmProtocoManBase.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProtocoManBase.QrProtBaseCAfterOpen(DataSet: TDataSet);
begin
  if QrProtBaseC.RecordCount = 1 then
  begin
    EdProtBaseC.ValueVariant := QrProtBaseCCodUsu.Value;
    CBProtBaseC.KeyValue     := QrProtBaseCCodUsu.Value;
  end;
end;

procedure TFmProtocoManBase.RelancaProtBaseI();
var
  ProtBaseC, Manual, Depto, Retorna: Integer;
  DataE, Vencto, LimiteSai, LimiteRem, LimiteRet: TDateTime;
  MesAno, Texto, SerieCH: String;
  Docum, Valor: Double;
  Data: TDateTime;
begin
  if UMyMod.ObtemCodigoDeCodUsu(EdProtBaseC, ProtBaseC, 'Informe o grupo base!') then
  begin
    QrProtBaseI.Close;
    QrProtBaseI.Params[0].AsInteger := ProtBaseC;
    UnDmkDAC_PF.AbreQuery(QrProtBaseI, Dmod.MyDB);
    //
    QrProtBaseI.First;
    while not QrProtBaseI.Eof do
    begin
      Data      := IncMonth(Date, QrProtBaseIincPerio.Value);
      MesAno    := Geral.FDT(Data, 1);
      Manual    := QrProtBaseIOcorrencia.Value;
      Depto     := 0; // UH
      Retorna   := QrProtBaseIRetorna.Value;
      DataE     := Date;
      Vencto    := Date + QrProtBaseIIncVencto.Value;
      LimiteSai := Date + QrProtBaseILimiteSai.Value;
      LimiteRem := Date + QrProtBaseILimiteRem.Value;
      LimiteRet := Date + QrProtBaseILimiteRet.Value;
      Texto     := QrProtBaseITexto.Value;
      SerieCH   := '';
      Docum     := 0;
      Valor     := QrProtBaseIValor.Value;
      //? := UMyMod.BuscaEmLivreY_Def('protpaki', ', ImgTipo.SQLType, CodAtual);
      //if
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'protpaki', True, [
      'Manual', 'DataE', 'MesAno',
      'Depto', 'SerieCH', 'Docum',
      'Valor', 'Vencto', 'Retorna',
      'Texto', 'LimiteSai', 'LimiteRem',
      'LimiteRet'], [
      ], [
      Manual, DataE, MesAno,
      Depto, SerieCH, Docum,
      Valor, Vencto, Retorna,
      Texto, LimiteSai, LimiteRem,
      LimiteRet], [
      ], False);
      //
      QrProtBaseI.Next;
    end;
    //
    TbProtPakI.Close;
    TbProtPakI. Open;
  end;
end;

procedure TFmProtocoManBase.TbProtPakIBeforePost(DataSet: TDataSet);
{
var
  A, M, D: Word;
}
begin
{
  Fazer no validate?
  DecodeDate(TbProtPakIMesAno.Value, A, M, D);
  TbProtPakIMesAno.Value := EncodeDate(M, D, 1);
}
end;

end.
