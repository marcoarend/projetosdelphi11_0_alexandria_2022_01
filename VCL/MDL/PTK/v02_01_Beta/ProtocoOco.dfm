object FmProtocoOco: TFmProtocoOco
  Left = 510
  Top = 211
  Caption = 'GER-PROTO-002 :: Ocorr'#234'ncias de Protocolo'
  ClientHeight = 450
  ClientWidth = 568
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 568
    Height = 288
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object dmkDBGridDAC1: TdmkDBGridDAC
      Left = 0
      Top = 0
      Width = 568
      Height = 288
      SQLFieldsToChange.Strings = (
        'Nome')
      SQLIndexesOnUpdate.Strings = (
        'Codigo')
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Width = 468
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ativo'
          Width = 17
          Visible = True
        end>
      Color = clWindow
      DataSource = DsProtocoOco
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dmkDBGridDAC1CellClick
      SQLTable = 'ProtocoOco'
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o'
          Width = 468
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ativo'
          Width = 17
          Visible = True
        end>
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 336
    Width = 568
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 564
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 380
    Width = 568
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 422
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 420
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtAcao: TBitBtn
        Tag = 294
        Left = 10
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&A'#231#227'o'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtAcaoClick
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 568
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 520
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 472
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 307
        Height = 32
        Caption = 'Ocorr'#234'ncias de Protocolo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 307
        Height = 32
        Caption = 'Ocorr'#234'ncias de Protocolo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 307
        Height = 32
        Caption = 'Ocorr'#234'ncias de Protocolo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object DsProtocoOco: TDataSource
    DataSet = QrProtocoOco
    Left = 168
    Top = 176
  end
  object QrProtocoOco: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM protocooco')
    Left = 140
    Top = 176
    object QrProtocoOcoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ProtocoOco.Codigo'
    end
    object QrProtocoOcoNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'ProtocoOco.Nome'
      Size = 50
    end
    object QrProtocoOcoLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'ProtocoOco.Lk'
    end
    object QrProtocoOcoDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'ProtocoOco.DataCad'
    end
    object QrProtocoOcoDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'ProtocoOco.DataAlt'
    end
    object QrProtocoOcoUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'ProtocoOco.UserCad'
    end
    object QrProtocoOcoUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'ProtocoOco.UserAlt'
    end
    object QrProtocoOcoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'ProtocoOco.AlterWeb'
    end
    object QrProtocoOcoAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'ProtocoOco.Ativo'
      MaxValue = 1
    end
  end
  object PMAcao: TPopupMenu
    Left = 104
    Top = 249
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Imprimegrade1: TMenuItem
      Caption = '&Imprime lista'
      OnClick = Imprimegrade1Click
    end
  end
  object frxDsCadProtoMot: TfrxDBDataset
    UserName = 'frxDsCadProtoMot'
    CloseDataSource = False
    DataSet = QrProtocoOco
    BCDToCurrency = False
    Left = 348
    Top = 120
  end
  object frxProtoMot: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38011.488035601900000000
    ReportOptions.LastChange = 39495.767711585600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLogoC' +
        'aminho>);'
      'end.')
    OnGetValue = frxProtoMotGetValue
    Left = 376
    Top = 121
    Datasets = <
      item
        DataSet = frxDsCadProtoMot
        DataSetName = 'frxDsCadProtoMot'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -7
      Font.Name = 'Arial'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      Columns = 1
      ColumnWidth = 287.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 113.385900000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 553.432900000000000000
          Top = 8.661410000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 190.661410000000000000
          Top = 49.763760000000010000
          Width = 521.858070000000000000
          Height = 42.220470000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = -0.236240000000000000
          Top = 25.763760000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          Left = 190.661410000000000000
          Top = 25.763760000000000000
          Width = 525.543290000000000000
          Height = 22.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 94.488250000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsCadProtoMot
          DataSetName = 'frxDsCadProtoMot'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 75.590600000000000000
          Top = 94.488250000000000000
          Width = 623.622450000000000000
          Height = 18.897650000000000000
          DataSet = frxDsCadProtoMot
          DataSetName = 'frxDsCadProtoMot'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 699.213050000000000000
          Top = 94.488250000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          DataSet = frxDsCadProtoMot
          DataSetName = 'frxDsCadProtoMot'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'A')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 192.756030000000000000
        Width = 718.110700000000000000
        DataSet = frxDsCadProtoMot
        DataSetName = 'frxDsCadProtoMot'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'Codigo'
          DataSet = frxDsCadProtoMot
          DataSetName = 'frxDsCadProtoMot'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCadProtoMot."Codigo"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 75.590600000000000000
          Width = 623.622450000000000000
          Height = 18.897650000000000000
          DataField = 'Nome'
          DataSet = frxDsCadProtoMot
          DataSetName = 'frxDsCadProtoMot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCadProtoMot."Nome"]')
          ParentFont = False
        end
        object CheckBox1: TfrxCheckBoxView
          Left = 699.213050000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'Ativo'
          DataSet = frxDsCadProtoMot
          DataSetName = 'frxDsCadProtoMot'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 60.472480000000000000
        Top = 234.330860000000000000
        Width = 718.110700000000000000
        object Memo6: TfrxMemoView
          Left = 551.811380000000000000
          Top = 7.559059999999988000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
    end
  end
end
