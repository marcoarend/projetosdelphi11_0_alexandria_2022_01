unit UnProtocol_Tabs;

{ Colocar no MyListas:

Uses Protocol_Tabs;


//
function TMyListas.CriaListaTabelas:
    Protocol_Tb.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
    Protocol_Tb.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:
    Protocol_Tb.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      Protocol_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      Protocol_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    Protocol_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  Protocol_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections, mysqlDBTables,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms, UnGrl_Vars, dmkGeral, UnDmkEnums;

type
  TUnProtocol_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Database: TmySQLDatabase; Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos: TCampos;
             FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  Protocol_Tabs: TUnProtocol_Tabs;

const
  CO_TAREFA_ENVIO_PROTOCOLOS_ND_NAO_DEFINIDO                 = 0;
  CO_TAREFA_ENVIO_PROTOCOLOS_EB_ENTREGA_BOLETOS_POSTAL       = 1;
  CO_TAREFA_ENVIO_PROTOCOLOS_CE_CIRCUL_ELETRON_EMAIL         = 2;
  CO_TAREFA_ENVIO_PROTOCOLOS_EM_ENTREGA_MATERIAL_CONSUMO     = 3;
  CO_TAREFA_ENVIO_PROTOCOLOS_CR_COBRANCA_REGISTRADA_BANCARIA = 4;
  CO_TAREFA_ENVIO_PROTOCOLOS_CD_CIRCULACAO_DOCUMENTOS_POSTAL = 5;

implementation

uses UMySQLModule;

function TUnProtocol_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('Protocolos') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"A Definir"');
  end;
  if Uppercase(Tabela) = Uppercase('ProtocoOco') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"Lan�amentos"');
  end;
end;

function TUnProtocol_Tabs.CarregaListaTabelas(Database: TmySQLDatabase;
  Lista: TList<TTabelas>): Boolean;
begin
  try
    if Database.DatabaseName = VAR_DBWEB then
    begin
      MyLinguas.AdTbLst(Lista, False, LowerCase('wprobolimp'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('wpropaits'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('Protocolos'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProtocoPak'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProtPakIts'), '');
      //
      MyLinguas.AdTbLst(Lista, False, LowerCase('WProtocoPak'), 'ProtocoPak');
      MyLinguas.AdTbLst(Lista, False, LowerCase('WProtPakIts'), 'ProtPakIts');
      //
      MyLinguas.AdTbLst(Lista, False, LowerCase('recebibloq'), '');  //Para o Syndi2 => Deprecado no Syndi3
    end else
    begin
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProtBaseC'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProtBaseI'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('Protocolos'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProtocoOco'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProtocoMot'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProtocoPak'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProtPakIts'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProtPakMul'), '');
      //
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProEnPr'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ProEnPrIt'), '');
    end;
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnProtocol_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('') then
    begin
    {
    end else
    if Uppercase(Tabela) = Uppercase('ProtocoOco') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"Lan�amentos"');
    }
    end else
    ;
  except
    raise;
    Result := False;
  end;
end;


function TUnProtocol_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('ProtBaseC') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'CodUsu';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ProtBaseI') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('Protocolos') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ProtocoOco') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ProtocoMot') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ProtocoPak') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ProtPakIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ProtPakMul') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Lancto';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('wprobolimp') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('proenpr') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('proenprit') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('recebibloq') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Apto';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Protoco';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Bloqueto';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'Emeio';
    FLIndices.Add(FRIndices);
    //
  end;
end;

function TUnProtocol_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos: TCampos;
  FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
   if Uppercase(Tabela) = Uppercase('ProtBaseC') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
   end else
   if Uppercase(Tabela) = Uppercase('ProtBaseI') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ocorrencia';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IncPerio';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Retorna';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LimiteSai';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LimiteRem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LimiteRet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IncVencto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
   end else
   if Uppercase(Tabela) = Uppercase('Protocolos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Def_Client';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Def_Sender';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Def_Retorn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PreEmeio';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '???';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProtocoOco') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProtocoMot') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProtocoPak') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';  // Protocolo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataI';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataL';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataF';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Mez';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNAB_Cfg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SeqArq'; // Seq��ncia do arquivo de remessa CNAB
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProtPakIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Link_ID'; // protocolo.pas > VAR_TIPO_LINK_ID_...
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliInt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cedente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fornece';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Lancto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Docum';
      FRCampos.Tipo       := 'double(20,0)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Depto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataE';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataD';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Retorna';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cancelado';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motivo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_Cod1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_Cod2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_Cod3';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ID_Cod4';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Vencto'; // Vencimento do do documento
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor'; // Valor do documento
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MoraDiaVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MultaVal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComoConf'; // 1=Manual(For�ado), 2=Email, 3=J� pagou, 4=Web Link, 5=Web Auto (Cabe�alho do e-mail)
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieCH';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Manual';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Saiu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Recebeu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Retornou';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LimiteSai';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LimiteRem';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LimiteRet';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataSai';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataRec';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataRet';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntContrat';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntPagante';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IPCad';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '127.0.0.1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IPAlt';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '127.0.0.1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SecureStr';
      FRCampos.Tipo       := 'varchar(40)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ProtPakMul') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Lancto';
      FRCampos.Tipo       := 'bigint(20)'; // prever bigint para Lanto.Controle
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('wprobolimp') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SecureStr';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora'; //TimeZone
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Documento';
      FRCampos.Tipo       := 'double(20,0)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sacado';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Configuracao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UserTip';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UserCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Vencto';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CedBanco';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CedAgencia';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CedDAC_A'; // D�gito verificador Ag�ncia
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CedConta';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CedDAC_C'; // D�gito verificador Conta corrente
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinhaDigitavel';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('wpropaits') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Protocolo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';  //CondImov Conta  => No Syndi2
      FRCampos.Tipo       := 'int(11)';  //Entidade Codigo => Nos Demais
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Email';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Telefone';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComoConf'; // 1=Manual(For�ado), 2=Email, 3=J� pagou, 4=Web Link, 5=Web Beacon (Cabe�alho do e-mail)
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComoConfRec'; // 1=Manual(For�ado), 2=Email, 3=J� pagou, 4=Web Link, 5=Web Beacon (Cabe�alho do e-mail)
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHoraRec'; //Para Web Beacon
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IP';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IPRec';  //Para Web Beacon
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Params';  //Inoforma��es de quem confirmou
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ParamsRec';  //Inoforma��es de quem confirmou
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sincro';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('proenpr') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Protocolo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Finalidade'; // 0=Envio de boletos, 1=Envio de NFSe
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('proenprit') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Depto';   //Para os aplicativos que possuem UHs
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('recebibloq') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Apto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Protoco';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Bloqueto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Emeio';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Envios';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHoraRecebeu';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IP';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IPRecebeu';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComoConf'; // 1=Manual(For�ado), 2=Email, 3=J� pagou, 4=Web Link, 5=Web Auto (Cabe�alho do e-mail)
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sincro';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnProtocol_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      New(FRCampos);
      FRCampos.Field      := 'ProtBaseC';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProtBaseI';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Protocolos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProtocoOco';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProtocoMot';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProtocoPak';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProtPakIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProEnPr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProEnPrIt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnProtocol_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  //GER-PROTO-001 :: Cadastro e Gerenciamento de Protocolos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-001';
  FRJanelas.Nome      := 'FmProtocolos';
  FRJanelas.Descricao := 'Cadastro e Gerenciamento de Protocolos';
  FRJanelas.Modulo    := 'PTK';
  FLJanelas.Add(FRJanelas);
  //
  //GER-PROTO-002 :: Ocorr�ncias de Protocolo
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-002';
  FRJanelas.Nome      := 'FmProtocoOco';
  FRJanelas.Descricao := 'Ocorr�ncia de Protocolo';
  FRJanelas.Modulo    := 'PTK';
  FLJanelas.Add(FRJanelas);
  //
  //GER-PROTO-003 :: Motivos de n�o Entrega de Protocolo
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-003';
  FRJanelas.Nome      := 'FmProtocoMot';
  FRJanelas.Descricao := 'Motivos de n�o Entrega de Protocolo';
  FRJanelas.Modulo    := 'PTK';
  FLJanelas.Add(FRJanelas);
  //
  //GER-PROTO-004 :: Lote de Protocolo
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-004';
  FRJanelas.Nome      := 'FmProtocoPak';
  FRJanelas.Descricao := 'Novo Lote de Protocolos';
  FRJanelas.Modulo    := 'PTK';
  FLJanelas.Add(FRJanelas);
  //
  //GER-PROTO-005 :: Item de Protocolo Manual
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-005';
  FRJanelas.Nome      := 'FmProtocoMan';
  FRJanelas.Descricao := 'Item de Protocolo Manual';
  FRJanelas.Modulo    := 'PTK';
  FLJanelas.Add(FRJanelas);
  //
  //GER-PROTO-006 :: Grupos Bases de Protocolos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-006';
  FRJanelas.Nome      := 'FmProtBaseC';
  FRJanelas.Descricao := 'Grupos Bases de Protocolos';
  FRJanelas.Modulo    := 'PTK';
  FLJanelas.Add(FRJanelas);
  //
  //GER-PROTO-007 :: Itens Bases de Protocolos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-007';
  FRJanelas.Nome      := 'FmProtBaseI';
  FRJanelas.Descricao := 'Itens Bases de Protocolos';
  FRJanelas.Modulo    := 'PTK';
  FLJanelas.Add(FRJanelas);
  //
  //GER-PROTO-008 :: Itens de Protocolos (Base)
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-008';
  FRJanelas.Nome      := 'FmProtocoManBase';
  FRJanelas.Descricao := 'Itens de Protocolos (Base)';
  FRJanelas.Modulo    := 'PTK';
  FLJanelas.Add(FRJanelas);
  //
  //GER-PROTO-009 :: Sele��o de protocolo
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-009';
  FRJanelas.Nome      := 'FmCondGerProtoSel';
  FRJanelas.Descricao := 'Sele��o de protocolo';
  FRJanelas.Modulo    := 'PTK';
  FLJanelas.Add(FRJanelas);
  //
  // GER-PROTO-010 :: Protocolos Padr�o
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-010';
  FRJanelas.Nome      := 'FmProEnPr';
  FRJanelas.Descricao := 'Protocolos Padr�o';
  FRJanelas.Modulo    := 'PTK';
  FLJanelas.Add(FRJanelas);
  //
  //GER-PROTO-011 :: Gerenciamento de Protocolos
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-011';
  FRJanelas.Nome      := 'FmProtoGer';
  FRJanelas.Descricao := 'Gerenciamento de Protocolos';
  FRJanelas.Modulo    := 'PTK';
  FLJanelas.Add(FRJanelas);
  //
  //GER-PROTO-012 :: Sel. Protocolo Padr�o
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-012';
  FRJanelas.Nome      := 'FmProtoAddTar';
  FRJanelas.Descricao := 'Sel. Protocolo Padr�o';
  FRJanelas.Modulo    := 'PTK';
  FLJanelas.Add(FRJanelas);
  //
  //GER-PROTO-013 :: Protocolo Lotes
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-013';
  FRJanelas.Nome      := 'FmProSel';
  FRJanelas.Descricao := 'Protocolo Lotes';
  FRJanelas.Modulo    := 'PTK';
  FLJanelas.Add(FRJanelas);
  //
  //GER-PROTO-014 :: Prot. Padr�o - Gerenciamento
  New(FRJanelas);
  FRJanelas.ID        := 'GER-PROTO-014';
  FRJanelas.Nome      := 'FmProEnPrGer';
  FRJanelas.Descricao := 'Prot. Padr�o - Gerenciamento';
  FRJanelas.Modulo    := 'PTK';
  FLJanelas.Add(FRJanelas);
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
