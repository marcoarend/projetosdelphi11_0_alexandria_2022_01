unit ProEnPrGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables, dmkDBGridZTO, Protocolo, UnDmkProcFunc;

type
  TFmProEnPrGer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    DsEntidades: TDataSource;
    QrProEnPrIt: TmySQLQuery;
    DsProEnPrIt: TDataSource;
    Panel5: TPanel;
    BtPesquisa: TBitBtn;
    RGFinalidade: TRadioGroup;
    CBEntDepto: TdmkDBLookupComboBox;
    EdEntDepto: TdmkEditCB;
    LaEntDepto: TLabel;
    DBGradeBlo: TdmkDBGridZTO;
    QrProEnPrItCodigo: TIntegerField;
    QrProEnPrItControle: TIntegerField;
    QrProEnPrItTarefa_TXT: TWideStringField;
    QrProEnPrItFinalidade_Txt: TWideStringField;
    QrProEnPrItFinalidade: TIntegerField;
    BtExclui: TBitBtn;
    QrEntidadesNome: TWideStringField;
    BtProtocolo: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure QrProEnPrItCalcFields(DataSet: TDataSet);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtProtocoloClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenProEnprIt(EntDepto, Finalidade: Integer);
    procedure ReopenEntDepto();
  public
    { Public declarations }
    FEmpresa, FEntDepto: Integer;
    FFinalidade: TProtFinalidade;
  end;

  var
  FmProEnPrGer: TFmProEnPrGer;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, MyDBCheck, ModuleGeral,
  UnProtocoUnit;

{$R *.DFM}

procedure TFmProEnPrGer.BtExcluiClick(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrProEnPrIt, TDBGrid(DBGradeBlo),
    'proenprit', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmProEnPrGer.BtOKClick(Sender: TObject);

  function InsereItemAtual(EntDepto, Finalidade: Integer): Boolean;
  var
    Tarefa: Integer;
  begin
    Tarefa := DModG.QrSelCodsNivel1.Value;
    //
    if Tarefa <> 0 then
    begin
      {$IfNDef S_BLQ}
      Result := UnProtocolo.AtualizaProtocoloBoleto(Tarefa, EntDepto,
                  TProtFinalidade(Finalidade), Dmod.QrUpd, Dmod.MyDB);
      {$Else}
      Result := False;
      {$EndIf}
    end;
  end;

var
  EntDepto, Finalidade: Integer;
  SQL: String;
begin
  EntDepto   := EdEntDepto.ValueVariant;
  Finalidade := RGFinalidade.ItemIndex;
  //
  if MyObjects.FIC(FEmpresa = 0, nil, 'Empresa n�o definida!') then Exit;
  if MyObjects.FIC(Finalidade < 0, nil, 'Defina a finalidade') then Exit;
  //
  if VAR_KIND_DEPTO = kdUH then
  begin
    if MyObjects.FIC(EntDepto = 0, nil, 'Defina a unidade habitacional!') then Exit;
    //
    SQL := 'WHERE its.Depto=' + Geral.FF0(EntDepto);
  end else
  begin
    if MyObjects.FIC(EntDepto = 0, nil, 'Defina a entidade!') then Exit;
    //
    SQL := 'WHERE its.Entidade=' + Geral.FF0(EntDepto);
  end;
  if DBCheck.EscolheCodigosMultiplos_0('...', 'XXX-XXXXX-000 :: Protocolos',
    'Seleciones as tarefas de protocolos desejados:', nil, 'Ativo', 'Nivel1', 'Nome', [
    'DELETE FROM _selcods_; ',
    'INSERT INTO _selcods_ ',
    'SELECT pro.Codigo Nivel1, 0 Nivel2, ',
    '0 Nivel3, 0 Nivel4, 0 Nivel5, pro.Nome, 0 Ativo ',
    'FROM ' + TMeuDB + '.protocolos pro ',
    'WHERE pro.Tipo <> 4 ', //O CR � obrigat�rio n�o mostrar aqui
    'AND pro.Def_Client=' + Geral.FF0(FEmpresa),
    'AND pro.Codigo > 0 ',
    'AND pro.Codigo NOT IN ',
    '( ',
    'SELECT pep.Protocolo ',
    'FROM ' + TMeuDB + '.proenprit its ',
    'LEFT JOIN ' + TMeuDB + '.proenpr pep ON pep.Codigo = its.Codigo ',
    SQL,
    ') ',
    ''],[
    'SELECT * FROM _selcods_; ',
    ''], Dmod.QrUpd) then
  begin
    DModG.QrSelCods.First;
    while not DModG.QrSelCods.Eof do
    begin
      if DModG.QrSelCodsAtivo.Value = 1 then
      begin
        if not InsereItemAtual(EntDepto, Finalidade) then
        begin
          Geral.MB_Erro('Falha ao incluir registro!');
          Exit;
        end;
      end;
      DModG.QrSelCods.Next;
    end;
    ReopenProEnprIt(EntDepto, Finalidade);
  end;
end;

procedure TFmProEnPrGer.BtPesquisaClick(Sender: TObject);
var
  Entidade, Finalidade: Integer;
begin
  Entidade   := EdEntDepto.ValueVariant;
  Finalidade := RGFinalidade.ItemIndex;
  //
  ReopenProEnprIt(Entidade, Finalidade);
end;

procedure TFmProEnPrGer.BtProtocoloClick(Sender: TObject);
begin
  ProtocoUnit.MostraFormProtocolos(0, 0);
end;

procedure TFmProEnPrGer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProEnPrGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProEnPrGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  if VAR_KIND_DEPTO = kdUH then
    LaEntDepto.Caption := 'Unidade habitacional:'
  else
    LaEntDepto.Caption := 'Entidade:';
  //
  RGFinalidade.Items.Clear;
  RGFinalidade.Items.AddStrings(UnProtocolo.ObtemListaProEnPrFinalidade());
end;

procedure TFmProEnPrGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProEnPrGer.FormShow(Sender: TObject);
begin
  ReopenEntDepto();
  //
  EdEntDepto.ValueVariant := FEntDepto;
  CBEntDepto.KeyValue     := FEntDepto;
  RGFinalidade.ItemIndex  := Integer(FFinalidade);
  //
  ReopenProEnprIt(FEntDepto, Integer(FFinalidade));
end;

procedure TFmProEnPrGer.QrProEnPrItCalcFields(DataSet: TDataSet);
var
  Finalidade: Integer;
begin
  Finalidade := QrProEnPrItFinalidade.Value;
  //
  QrProEnPrItFinalidade_TXT.Value := UnProtocolo.ObtemDescriItemDeListaProEnPrFinalidade(Finalidade);
end;

procedure TFmProEnPrGer.ReopenEntDepto;
begin
  if VAR_KIND_DEPTO = kdUH then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEntidades, Dmod.MyDB, [
      'SELECT imv.Conta Codigo, imv.Unidade Nome ',
      'FROM condimov imv ',
      'LEFT JOIN cond con ON con.Codigo=imv.Codigo ',
      'WHERE con.Cliente=' + Geral.FF0(FEmpresa),
      'ORDER BY Nome ',
      '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEntidades, Dmod.MyDB, [
      'SELECT Codigo, IF (Tipo=0, RazaoSocial, Nome) Nome ',
      'FROM entidades ',
      'ORDER BY Nome ',
      '']);
  end;
end;

procedure TFmProEnPrGer.ReopenProEnprIt(EntDepto, Finalidade: Integer);
var
  SQL: String;
begin
  if VAR_KIND_DEPTO = kdUH then
  begin
    if MyObjects.FIC(EntDepto = 0, nil, 'Unidade habitacional n�o definida!') then Exit;
    //
    SQL := 'AND its.Depto=' + Geral.FF0(EntDepto);
  end else
  begin
    if MyObjects.FIC(EntDepto = 0, nil, 'Entidade n�o definida!') then Exit;
    //
    SQL := 'AND its.Entidade=' + Geral.FF0(EntDepto);
  end;
  if MyObjects.FIC(Finalidade < 0, nil, 'Finalidade n�o definida!') then Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrProEnPrIt, Dmod.MyDB, [
    'SELECT ppr.Codigo, its.Controle, pro.Nome Tarefa_TXT, ppr.Finalidade ',
    'FROM proenprit its ',
    'LEFT JOIN proenpr ppr ON ppr.Codigo = its.Codigo ',
    'LEFT JOIN protocolos pro ON pro.Codigo = ppr.Protocolo ',
    'WHERE ppr.Finalidade=' + Geral.FF0(Finalidade),
    SQL,
    '']);
end;

end.
