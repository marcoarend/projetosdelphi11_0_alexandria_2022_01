unit ProtocoOco;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, DmkDAC_PF,
  dmkDBGridDAC, Db, mySQLDbTables, dmkDBGrid, Menus, frxClass, frxDBSet,
  dmkImage, UnDmkEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmProtocoOco = class(TForm)
    Panel1: TPanel;
    DsProtocoOco: TDataSource;
    QrProtocoOco: TmySQLQuery;
    dmkDBGridDAC1: TdmkDBGridDAC;
    PMAcao: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui1: TMenuItem;
    QrProtocoOcoCodigo: TIntegerField;
    QrProtocoOcoNome: TWideStringField;
    QrProtocoOcoLk: TIntegerField;
    QrProtocoOcoDataCad: TDateField;
    QrProtocoOcoDataAlt: TDateField;
    QrProtocoOcoUserCad: TIntegerField;
    QrProtocoOcoUserAlt: TIntegerField;
    QrProtocoOcoAlterWeb: TSmallintField;
    QrProtocoOcoAtivo: TSmallintField;
    N1: TMenuItem;
    Imprimegrade1: TMenuItem;
    frxDsCadProtoMot: TfrxDBDataset;
    frxProtoMot: TfrxReport;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    BtAcao: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure dmkDBGridDAC1CellClick(Column: TColumn);
    procedure Imprimegrade1Click(Sender: TObject);
    procedure frxProtoMotGetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
    procedure ReopenQrProtocoOco(Codigo: Double);
  public
    { Public declarations }
  end;

  var
  FmProtocoOco: TFmProtocoOco;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, ModuleGeral;

{$R *.DFM}

procedure TFmProtocoOco.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrProtocoOcoCodigo.Value;
  Close;
end;

procedure TFmProtocoOco.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProtocoOco.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProtocoOco.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  ReopenQrProtocoOco(0);
end;

procedure TFmProtocoOco.Inclui1Click(Sender: TObject);
var
  Codigo: String;
  CodVal: Integer;
begin
  //CodVal := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    //'ProtocoOco', 'ProtocoOco', 'Codigo');
  CodVal := UMyMod.BuscaPrimeiroCodigoLivre('ProtocoOco', 'Codigo');
  Codigo := Geral.FFT(CodVal, 0, siPositivo);
  if InputQuery('Novo motivo', 'Informe o c�digo do motivo:',
  Codigo) then
  begin
    CodVal := Geral.IMV(Codigo);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO protocooco SET Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := CodVal;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrProtocoOco(CodVal);
  end;
end;

procedure TFmProtocoOco.ReopenQrProtocoOco(Codigo: Double);
begin
  QrProtocoOco.Close;
  UnDmkDAC_PF.AbreQuery(QrProtocoOco, Dmod.MyDB);
  QrProtocoOco.Locate('Codigo', Codigo, []);
end;

procedure TFmProtocoOco.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmProtocoOco.Exclui1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do registro selecionado?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM protocooco WHERE Codigo=:P0');
    Dmod.QrUpd.Params[00].AsInteger := QrProtocoOcoCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrProtocoOco(Int(Date));
  end;
end;

procedure TFmProtocoOco.dmkDBGridDAC1CellClick(Column: TColumn);
var
  Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if QrProtocoOcoAtivo.Value = 1 then Ativo := 0 else Ativo := 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE protocooco SET Ativo=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
    //
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsFloat   := QrProtocoOcoCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenQrProtocoOco(QrProtocoOcoCodigo.Value);
  end;
end;

procedure TFmProtocoOco.Imprimegrade1Click(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxProtoMot, [
    DModG.frxDsMaster,
    frxDsCadProtoMot
    ]);
  MyObjects.frxMostra(frxProtoMot, Caption);
end;

procedure TFmProtocoOco.frxProtoMotGetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_TITULO') = 0 then
    Value := Caption;
end;

end.

