unit Protocolo;

interface

uses
  Forms, StdCtrls, ComCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes,
  Graphics, Controls, Dialogs, Variants, MaskUtils, UnMsgInt, mySQLDbTables,
  DB,(* DbTables,*) DBGrids, mySQLExceptions, dmkGeral, UnInternalConsts,
  UnDmkEnums, UnDmkProcFunc;

type
  TComoConf = (ptkTodos=-2, ptkOutros=-1, ptkManual=1, ptkEmail=2, ptkJaPagou=3, ptkWebLink=4, ptkWebAuto=5, ptkWhatsApp=6);
  TProtFinalidade = (ptkBoleto=0, ptkNFSeAut=1, ptkNFSeCan=2);
  TUnProtocolo = class(TObject)
  private
    {private declaration}
    function  LocalizaLote(ProtocoPak: Integer; QueryLoc: TmySQLQuery;
              Database: TmySQLDatabase): Integer;
    procedure GeraProtocolo_NFSeProto(RecipEmeio: String;
              QuerySource: TmySQLQuery; Database: TmySQLDatabase;
              ProtocoPak, EntCliInt, TipoProt: Integer);
    procedure GeraProtocolo_BloqProto(RecipEmeio: String; QuerySource, QueryLoc,
              QueryUpd: TmySQLQuery; Database: TmySQLDatabase; ProtocoPak,
              EntCliInt, TipoProt: Integer);
    procedure VerificaSeProtocoloPadraoExiste(const Database: TmySQLDatabase;
              const Protocolo, EntDepto: Integer; const Finalidade: TProtFinalidade;
              var Codigo, Controle: Integer);
  public
    {public declaration}
    function  IntOfComoConf(ComoConf: TComoConf): Integer;
    function  NomeDoTipoDeProtocolo(Tipo: Integer): String;
    function  LocalizaProtocolo(Database: TmySQLDatabase; EntiDepto: Integer;
              Excecao: Integer = 0): Integer;
    function  LocalizaProtocoloCR(Database: TmySQLDatabase; CNAB_Cfg: Integer): Integer;
    //
    procedure ProtocolosCD_Lct(Quais: TSelType; DBGLct: TDBGrid;
              ModuleLctX: TDataModule; CliInt: Integer; FormOrigem, TabLctA: String);
    procedure ProtocolosCD_Doc(Quais: TSelType; DBGLct: TDBGrid;
              ModuleLctX: TDataModule; CliInt: Integer; FormOrigem, TabLctA: String);
    procedure ExcluiLoteProtocoloBoleto(Database: TmySQLDatabase; Tarefa, PrevCod,
              EntCliInt, Arreits: Integer; TabelaAri: String);
    procedure ExcluiLoteProtocoloNFSe(QueryLoc, QueryUpd: TmySQLQuery;
              Database: TmySQLDatabase; CliInt, Ambiente, EntiMail, ProtocoPak,
              RpsIDNumero: Integer; RpsIDSerie: String);
    function  GeraLoteProtocolo(Database: TmySQLDatabase; Protocolo,
              CNAB_Cfg, TipoProt: Integer): Integer;
    function  ProtocolosCD_OS1(Quais: TSelType; DBGOScab: TDBGrid;
              QrOSCab: TmySQLQuery; CliInt: Integer): Integer;
    function  GeraProtocoloNFSe(Quais: TSelType; QueryIts: TmySQLQuery;
              Database: TmySQLDatabase; Grade: TDBGrid;
              Progress: TProgressBar; EntCliInt: Integer): Boolean;
    function  GeraProtocoloFatura(Quais: TSelType; QueryIts, QueryUpd,
              QueryLoc: TmySQLQuery; Database: TmySQLDatabase; Grade: TDBGrid;
              Progress: TProgressBar; EntCliInt: Integer): Boolean;
    function  GetTipoProtocolo(Aba: Integer): Integer;
    function  VerificaSeLoteCRFoiGerado(Database: TmySQLDatabase; PrevCod,
              EntCliInt, CNAB_Cfg: Integer; EntDepto: Integer; Boleto: Double): Boolean;
    function  VerificaSeProtocoloRetornou(Database: TmySQLDatabase; PrevCod,
              EntCliInt, CNAB_Cfg: Integer; EntDepto: Integer; Boleto: Double): Boolean;
    //
    procedure Proto_Email_GeraLinksConfirmacaoReceb(const DataBase: TmySQLDatabase;
              const Web_MyURL, WebId, Email: String; const Cliente, Protocolo,
              DMKID_APP: Integer; var ParamLink, ParamTopo: String); deprecated; // use Proto_Email_GeraLinksConfirmacaoReceb2
    procedure Proto_Email_GeraLinksConfirmacaoReceb2(const DataBase: TmySQLDatabase;
              const Web_Id, Email, Telefone: String; const Cliente, Protocolo,
              DMKID_APP: Integer; Finalidade: TProtFinalidade;
              var ProtocolImp: String; var Protocol: String; WhatsApp: Boolean = False);
    procedure AtualizaProtocolo(CampoUpd, ValUpd: String; PrevCod, EntCliInt,
              CNAB_Cfg, Entidade: Integer; Boleto: Double; Database: TmySQLDatabase);
    function  Proto_Email_CriaProtocoloEnvio(DataBase: TmySQLDatabase; Cliente,
              Protocolo: Integer; Email: String): Boolean;
    function  Proto_Email_AtualizDataE(DataBase: TmySQLDatabase;
              Protocolo: Integer; Agora: TDateTime): Boolean;
    function  ObtemListaProEnPrFinalidade(): TStringList;
    function  ObtemDescriItemDeListaProEnPrFinalidade(Id: Integer): String;
    {$IfNDef S_BLQ}
    function  LiberaAtualizacaoDeProtocoloBoleto(Database: TmySQLDatabase;
              Docum: Double; Cliente, ID_Cod1, ID_Cod2, ID_Cod4: Integer): Boolean;
    function  AtualizaProtocoloBoleto(Protocolo, EntDepto: Integer;
              Finalidade: TProtFinalidade; QueryUpd: TmySQLQuery;
              Database: TmySQLDatabase): Boolean;
    {$EndIf}
    {$IfNDef sNFSe}
    function  AtualizaProtocoloNFSe(Tarefa: Integer; Finalidade: TProtFinalidade;
              QueryUpd: TmySQLQuery; Database: TmySQLDatabase): Boolean;
    {$EndIf}
  end;

var
  UnProtocolo: TUnProtocolo;

const
  VAR_TIPO_PROTOCOLO_ND_00 = 0; // ND - N�o definido
  VAR_TIPO_PROTOCOLO_EB_01 = 1; // EB - Entrega de Bloquetos
  VAR_TIPO_PROTOCOLO_CE_02 = 2; // CE - Circula��o eletr�nica (emeio)
  VAR_TIPO_PROTOCOLO_EM_03 = 3; // EM - Entrega de material de consumo
  VAR_TIPO_PROTOCOLO_CR_04 = 4; // CR - Cobran�a registrada (banc�ria)
  VAR_TIPO_PROTOCOLO_CD_05 = 5; // CD - Circula��o de documentos

  VAR_TIPO_LINK_ID_01_GENERICO = 1; // ????
  VAR_TIPO_LINK_ID_02_FATURA_PROD_SERV = 2; // Fatura de produtos / servi�os (Parcelamento de fatura)
  VAR_TIPO_LINK_ID_03_ORDEM_DE_SERVCO1 = 3; // Ordens de servi�o (mais algum al�m do bgstrl?)
  VAR_TIPO_LINK_ID_04_CADAS_DE_WEBUSR = 4; // Cadastro de usu�rio na WEB

  CO_PTK_Finalidades: array[0..2] of string = ('Envio de faturas',
                                               'Envio de NFS-es autorizadas',
                                               'Envio de NFS-es canceladas');

implementation

uses UnCreateProtoco, MyListas,
{$IFDEF DEFINE_VARLCT}ModuleLct2, {$ENDIF}
{$IFNDEF NO_FINANCEIRO} UCreateFin, {$EndIf}
ProSel, MyDBCheck, ModuleGeral, UnMyObjects, UMySQLModule, DmkDAC_PF;

{ TUnProtocolo }

{$IfNDef S_BLQ}
function TUnProtocolo.LiberaAtualizacaoDeProtocoloBoleto(Database: TmySQLDatabase;
  Docum: Double; Cliente, ID_Cod1, ID_Cod2, ID_Cod4: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry    := TmySQLQuery.Create(TDataModule(Database.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
      'SELECT pak.SeqArq ',
      'FROM protpakits ppi ',
      'LEFT JOIN protocopak pak ON pak.Controle=ppi.Controle ',
      'WHERE ppi.Docum=' + Geral.FFI(Docum),
      'AND ppi.Cliente=' + Geral.FF0(Cliente),
      'AND ppi.ID_Cod1=' + Geral.FF0(ID_Cod1),
      'AND ppi.ID_Cod2=' + Geral.FF0(ID_Cod2),
      'AND ppi.ID_Cod4=' + Geral.FF0(ID_Cod4),
      '']);
    if Qry.RecordCount > 0 then
    begin
      if Qry.FieldByName('SeqArq').AsInteger = 0 then
        Result := True;
    end;
  finally
    Qry.Free;
  end;
end;
{$EndIf}

function TUnProtocolo.LocalizaLote(ProtocoPak: Integer; QueryLoc: TmySQLQuery;
  Database: TmySQLDatabase): Integer;
begin
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
      'SELECT Controle, SeqArq ',
      'FROM protocopak ',
      'WHERE Controle=' + Geral.FF0(ProtocoPak),
      '']);
    if (QueryLoc.RecordCount > 0) and (QueryLoc.FieldByName('SeqArq').AsInteger = 0) then
      Result := QueryLoc.FieldByName('Controle').AsInteger
    else
      Result := 0;
  except
    Result := -1;
  end;
end;

{$IfNDef sNFSe}
function TUnProtocolo.AtualizaProtocoloNFSe(Tarefa: Integer; Finalidade: TProtFinalidade;
  QueryUpd: TmySQLQuery; Database: TmySQLDatabase): Boolean;
var
  Finalid, CodigoPro, ControlePro: Integer;
begin
  Result := False;
  //
  if Finalidade = ptkNFSeAut then
    Finalid := 1
  else
    Finalid := 2;
  //
  if MyObjects.FIC(Tarefa = 0, nil, 'O campo Tarefa deve ser preenchido!') then Exit;
  //
  VerificaSeProtocoloPadraoExiste(Database, Tarefa, 0, Finalidade, CodigoPro, ControlePro);

  if CodigoPro = 0 then
  begin
    CodigoPro := UMyMod.BuscaEmLivreY_Def('proenpr', 'Codigo', stIns, 0);
    //
    Result := UMyMod.SQLInsUpd(QueryUpd, stIns, 'proenpr', False,
              ['Protocolo', 'Finalidade'], ['Codigo'],
              [Tarefa, Finalid], [CodigoPro], True);
  end;
end;
{$EndIf}

{$IfNDef S_BLQ}
function TUnProtocolo.AtualizaProtocoloBoleto(Protocolo, EntDepto: Integer;
  Finalidade: TProtFinalidade; QueryUpd: TmySQLQuery; Database: TmySQLDatabase): Boolean;
var
  CodigoPro, ControlePro: Integer;
  CampoProtocolo: String;
begin
  Result := False;
  //
  if Finalidade = ptkBoleto then
  begin
    if (Protocolo <> 0) then
    begin
      VerificaSeProtocoloPadraoExiste(Database, Protocolo, EntDepto, Finalidade,
        CodigoPro, ControlePro);
      //
      if CodigoPro = 0 then
      begin
        CodigoPro := UMyMod.BuscaEmLivreY_Def('proenpr', 'Codigo', stIns, 0);
        //
        UMyMod.SQLInsUpd(QueryUpd, stIns, 'proenpr', False,
          ['Protocolo', 'Finalidade'], ['Codigo'], [Protocolo, 0], [CodigoPro], True);
      end;
      if (CodigoPro <> 0) and (ControlePro = 0) then
      begin
        ControlePro := UMyMod.BuscaEmLivreY_Def('proenprit', 'Controle', stIns, 0);
        //
        if VAR_KIND_DEPTO = kdUH then
        begin
          Result := UMyMod.SQLInsUpd(QueryUpd, stIns, 'proenprit', False,
                      ['Depto', 'Codigo'], ['Controle'],
                      [EntDepto, CodigoPro], [ControlePro], True);
        end else
        begin
          Result := UMyMod.SQLInsUpd(QueryUpd, stIns, 'proenprit', False,
                      ['Entidade', 'Codigo'], ['Controle'],
                      [EntDepto, CodigoPro], [ControlePro], True);
        end;
      end else
        Result := True;
    end else
      Result := False;
  end else
    Geral.MB_Aviso('Finalidade n�o implementada!');
end;
{$EndIf}

procedure TUnProtocolo.AtualizaProtocolo(CampoUpd, ValUpd: String; PrevCod,
  EntCliInt, CNAB_Cfg, Entidade: Integer; Boleto: Double; Database: TmySQLDatabase);
var
  Qry: TmySQLQuery;
  QryUpd: TmySQLQuery;
  Conta: Integer;
begin
  Qry    := TmySQLQuery.Create(TDataModule(Database.Owner));
  QryUpd := TmySQLQuery.Create(TDataModule(Database.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
      'SELECT Conta ',
      'FROM protpakits ',
      'WHERE ID_Cod1=' + Geral.FF0(PrevCod),
      'AND ID_Cod2=' + Geral.FF0(EntCliInt),
      'AND ID_Cod4=' + Geral.FF0(CNAB_Cfg),
      'AND Cliente=' + Geral.FF0(Entidade),
      'AND Docum=' + Geral.FFI(Boleto),
      '']);
    if Qry.RecordCount > 0 then
    begin
      while not Qry.EOF do
      begin
        Conta := Qry.FieldByName('Conta').AsInteger;
        //
        UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
          'UPDATE protpakits SET ' + CampoUpd + '="' + ValUpd + '"',
          'WHERE Conta=' + Geral.FF0(Conta),
          '']);
        //
        Qry.Next;
      end;
    end;
  finally
    Qry.Free;
    QryUpd.Free;
  end;
end;

function TUnProtocolo.LocalizaProtocolo(Database: TmySQLDatabase;
  EntiDepto: Integer; Excecao: Integer = 0): Integer;
var
  Qry: TmySQLQuery;
  Campo, SQLCompl: String;
begin
  Result := 0;
  Qry    := TmySQLQuery.Create(TDataModule(Database.Owner));
  try
    if EntiDepto <> 0 then
    begin
      if VAR_KIND_DEPTO = kdUH then
        Campo := 'Depto'
      else
        Campo := 'Entidade';
      //
      if Excecao <> 0 then
        SQLCompl := 'AND pro.Protocolo <> ' + Geral.FF0(Excecao)
      else
        SQLCompl := '';
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
        'SELECT pro.Protocolo ',
        'FROM proenprit its ',
        'LEFT JOIN proenpr pro ON pro.Codigo = its.Codigo ',
        'WHERE its.' + Campo + '=' + Geral.FF0(EntiDepto),
        'AND pro.Finalidade = 0 ', //Boletos
        SQLCompl,
        '']);
      if Qry.RecordCount > 0 then
        Result := Qry.FieldByName('Protocolo').AsInteger;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnProtocolo.LocalizaProtocoloCR(Database: TmySQLDatabase; CNAB_Cfg: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(TDataModule(Database.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
      'SELECT ProtocolCR ',
      'FROM cnab_cfg ',
      'WHERE Codigo = ' + Geral.FF0(CNAB_Cfg),
      '']);
    if Qry.RecordCount > 0 then
      Result := Qry.FieldByName('ProtocolCR').AsInteger;
  finally
    Qry.Free;
  end;
end;

procedure TUnProtocolo.GeraProtocolo_NFSeProto(RecipEmeio: String;
  QuerySource: TmySQLQuery; Database: TmySQLDatabase; ProtocoPak,
  EntCliInt, TipoProt: Integer);
var
  Protocolo, Entidade, Conta, Emeio_ID, Retorna, ProtPakIts, Ambiente,
  RpsIDNumero: Integer;
  Vencto: TDateTime;
  NfsNumero: Largeint;
  Valor: Double;
  LimiteSai, LimiteRem, LimiteRet, RpsIDSerie: String;
  QueryLoc, QueryUpd: TmySQLQuery;
begin
  Entidade  := 0;
  NfsNumero := 0;
  Vencto    := 0;
  Valor     := 0;
  //
  QueryLoc := TmySQLQuery.Create(TDataModule(Database.Owner));
  QueryUpd := TmySQLQuery.Create(TDataModule(Database.Owner));
  //
  QueryLoc.DataBase := Database;
  QueryUpd.DataBase := Database;
  //
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
      'SELECT cfg.MultaPerc, cfg.JurosPerc, pak.DataI, ',
      'pak.DataL, pak.DataF, pro.Def_Retorn ',
      'FROM protocopak pak ',
      'LEFT JOIN protocolos pro ON pro.Codigo = pak.Codigo ',
      'LEFT JOIN cnab_cfg cfg ON cfg.Codigo=pak.CNAB_Cfg ',
      'WHERE pak.Controle=' + Geral.FF0(ProtocoPak),
      '']);
    //
    Retorna     := QueryLoc.FieldByName('Def_Retorn').AsInteger;
    LimiteSai   := Geral.FDT(QueryLoc.FieldByName('DataI').AsDateTime, 1);
    LimiteRem   := Geral.FDT(QueryLoc.FieldByName('DataL').AsDateTime, 1);
    LimiteRet   := Geral.FDT(QueryLoc.FieldByName('DataF').AsDateTime, 1);
    Protocolo   := QuerySource.FieldByName('PROTOCOD').AsInteger;
    Entidade    := QuerySource.FieldByName('Entidade').AsInteger;
    NfsNumero   := QuerySource.FieldByName('NfsNumero').AsLargeInt;
    Vencto      := QuerySource.FieldByName('RpsDataEmissao').AsDateTime;
    Valor       := QuerySource.FieldByName('ValorServicos').AsFloat;
    ProtPakIts  := QuerySource.FieldByName('PROTOCOLO').AsInteger;
    EMeio_ID    := Trunc(QuerySource.FieldByName('EMeio_ID').AsFloat);
    Ambiente    := QuerySource.FieldByName('Ambiente').AsInteger;
    RpsIDSerie  := QuerySource.FieldByName('RpsIDSerie').AsString;
    RpsIDNumero := QuerySource.FieldByName('RpsIDNumero').AsInteger;
    //
    if Protocolo = 0 then
      Exit;
    //
    if ProtPakIts = 0 then
    begin
      Conta := UMyMod.BuscaEmLivreY_Def('protpakits', 'conta', stIns, 0);
      //
      UMyMod.SQLInsUpd(QueryUpd, stIns, 'protpakits', False,
        [
          'Codigo', 'Controle', 'Link_ID', 'CliInt',
          'Cliente', 'Fornece', 'Periodo', 'Lancto', 'Docum',
          'Depto', 'ID_Cod1', 'ID_Cod2', 'ID_Cod3', 'ID_Cod4',
          'Retorna', 'Vencto',
          'Valor', 'MoraDiaVal', 'MultaVal', 'Cedente',
          'LimiteSai', 'LimiteRem', 'LimiteRet'
        ], ['Conta'], [
          Protocolo, ProtocoPak, VAR_TIPO_LINK_ID_01_GENERICO, EntCliInt,
          Entidade, 0, 0, 0, NfsNumero,
          0, Ambiente, RpsIDSerie, RpsIDNumero, EMeio_ID,
          Retorna, Geral.FDT(Vencto, 1),
          Valor, 0, 0, 0,
          LimiteSai, LimiteRem, LimiteRet
        ], [Conta], True);
    end;
  finally
    QueryLoc.Free;
    QueryUpd.Free;
  end;
end;

procedure TUnProtocolo.GeraProtocolo_BloqProto(RecipEmeio: String; QuerySource,
  QueryLoc, QueryUpd: TmySQLQuery; Database: TmySQLDatabase; ProtocoPak,
  EntCliInt, TipoProt: Integer);
var
  Protocolo, Entidade, Conta, Apto, Emeio_ID, Periodo, PrevCod, Retorna,
  CNAB_Cfg, Fatur_Cfg, Config, ProtPakIts: Integer;
  Vencto: TDateTime;
  Bloqueto, Valor, MoraDiaVal, MultaVal, MoraPer, MultaPer: Double;
  LimiteSai, LimiteRem, LimiteRet: String;
begin
  Entidade := 0;
  Bloqueto := 0;
  Apto     := 0;
  Vencto   := 0;
  Valor    := 0;
  Periodo  := 0;
  PrevCod  := 0;
  //
  MoraDiaVal := Round(Valor * (1 / 30)) / 100;
  MultaVal   := Round(Valor * 2) / 100;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
    'SELECT pak.DataI, pak.DataL, pak.DataF, pro.Def_Retorn ',
    'FROM protocopak pak ',
    'LEFT JOIN protocolos pro ON pro.Codigo = pak.Codigo ',
    'WHERE pak.Controle=' + Geral.FF0(ProtocoPak),
    '']);
  //
  Retorna    := QueryLoc.FieldByName('Def_Retorn').AsInteger;
  LimiteSai  := Geral.FDT(QueryLoc.FieldByName('DataI').AsDateTime, 1);
  LimiteRem  := Geral.FDT(QueryLoc.FieldByName('DataL').AsDateTime, 1);
  LimiteRet  := Geral.FDT(QueryLoc.FieldByName('DataF').AsDateTime, 1);
  MoraPer    := QuerySource.FieldByName('JurosPerc').AsFloat;
  MultaPer   := QuerySource.FieldByName('MultaPerc').AsFloat;
  Protocolo  := QuerySource.FieldByName('PROTOCOD').AsInteger;
  Bloqueto   := QuerySource.FieldByName('Boleto').AsFloat;
  Vencto     := QuerySource.FieldByName('Vencto').AsDateTime;
  Valor      := QuerySource.FieldByName('Valor').AsFloat;
  MoraDiaVal := Round(Valor * (MoraPer / 30)) / 100;
  MultaVal   := Round(Valor * MultaPer) / 100;
  Periodo    := QuerySource.FieldByName('Periodo').AsInteger;
  PrevCod    := QuerySource.FieldByName('PREVCOD').AsInteger;
  CNAB_Cfg   := QuerySource.FieldByName('CNAB_Cfg').AsInteger;
  Fatur_Cfg  := QuerySource.FieldByName('Fatur_Cfg').AsInteger;
  ProtPakIts := QuerySource.FieldByName('PROTOCOLO').AsInteger;
  EMeio_ID   := Trunc(QuerySource.FieldByName('EMeio_ID').AsFloat);
  Entidade   := QuerySource.FieldByName('Entidade').AsInteger;
  //
  if CNAB_Cfg <> 0 then
    Config := CNAB_Cfg
  else
    Config := Fatur_Cfg;
  //
  if VAR_KIND_DEPTO = kdUH then
    Apto := QuerySource.FieldByName('Apto').AsInteger;
  //
  if Protocolo = 0 then
    Exit;
  //
  if ProtPakIts = 0 then
  begin
    Conta := UMyMod.BuscaEmLivreY_Def('protpakits', 'conta', stIns, 0);
    //
    UMyMod.SQLInsUpd(QueryUpd, stIns, 'protpakits', False,
      [
      'Codigo', 'Controle', 'Link_ID', 'CliInt',
      'Cliente', 'Fornece', 'Periodo', 'Lancto', 'Docum',
      'Depto', 'ID_Cod1', 'ID_Cod2', 'ID_Cod3', 'ID_Cod4',
      'Retorna', 'Vencto',
      'Valor', 'MoraDiaVal', 'MultaVal', 'Cedente',
      'LimiteSai', 'LimiteRem', 'LimiteRet'
      ], ['Conta'], [
      Protocolo, ProtocoPak, VAR_TIPO_LINK_ID_01_GENERICO, EntCliInt,
      Entidade, 0, Periodo, 0, Bloqueto,
      Apto, PrevCod, EntCliInt, EMeio_ID, Config,
      Retorna, Geral.FDT(Vencto, 1),
      Valor, MoraDiaVal, MultaVal, 0,
      LimiteSai, LimiteRem, LimiteRet
      ], [Conta], True);
  end;
end;

function TUnProtocolo.GetTipoProtocolo(Aba: Integer): Integer;

  procedure AvisaErroGet;
  begin
    Geral.MB_Aviso('Aba do controlador de p�ginas sem implementa��o!' +
      'AVISE A DERMATEK!');
  end;

begin
  case Aba of
      0: Result := 1;
      1: Result := 2;
      2: Result := 4;
    else Result := -1;
  end;
  if Result = -1 then
    AvisaErroGet;
end;

function TUnProtocolo.IntOfComoConf(ComoConf: TComoConf): Integer;
begin
  case ComoConf of
    ptkTodos: //Todos
      Result := -2;
    ptkOutros: //Outros
      Result := -1;
    ptkManual: //Manual(For�ado)
      Result := 1;
    ptkEmail: //E-mail
      Result := 2;
    ptkJaPagou: //J� pagou
      Result := 3;
    ptkWebLink: //Web Link
      Result := 4;
    ptkWebAuto: //Web Auto (Cabe�alho do e-mail)
      Result := 5;
    ptkWhatsApp: //WhatsApp
      Result := 6;
  end;
end;

function TUnProtocolo.GeraProtocoloFatura(Quais: TSelType; QueryIts, QueryUpd,
  QueryLoc: TmySQLQuery; Database: TmySQLDatabase; Grade: TDBGrid;
  Progress: TProgressBar; EntCliInt: Integer): Boolean;

  function ObtemLoteProtocolo(Protocolo, ProtocoPak, CNAB_Cfg,
    TipoProt: Integer): Integer;
  var
    Agora: TDateTime;
    Lote: Integer;
    Mes: String;
  begin
    Lote := LocalizaLote(ProtocoPak, QueryLoc, Database);
    //
    if Lote = 0 then
    begin
      Agora := DModG.ObtemAgora();
      Mes   := Geral.DataToMez(Agora);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
        'SELECT Controle ',
        'FROM protocopak ',
        'WHERE Codigo=' + Geral.FF0(Protocolo),
        'AND Mez=' + Mes,
        'AND SeqArq = 0 ',
        '']);
      if QueryLoc.RecordCount > 0 then
        Lote := QueryLoc.FieldByName('Controle').AsInteger
      else
        Lote := GeraLoteProtocolo(DataBase, Protocolo, CNAB_Cfg, TipoProt);
    end;
    //
    Result := Lote;
  end;

var
  i, Lote, ProtocoPak, CNAB_Cfg, TipoProt, Protocol: Integer;
  RecipEmeio: String;
begin
  Result := False;
  //
  if (QueryIts.State <> dsInactive) and (QueryIts.RecordCount > 0) then
  begin
    Progress.Position := 0;
    //
    case Quais of
      istSelecionados:
      begin
        if Grade.SelectedRows.Count > 1 then
        begin
          try
            QueryIts.DisableControls;
            Grade.Enabled := False;
            Progress.Max  := Grade.SelectedRows.Count;
            //
            with Grade.DataSource.DataSet do
            begin
              for i := 0 to Grade.SelectedRows.Count-1 do
              begin
                GotoBookmark(Grade.SelectedRows.Items[i]);
                //
                Progress.Position := Progress.Position + 1;
                Progress.Update;
                Application.ProcessMessages;
                //
                Protocol   := QueryIts.FieldByName('PROTOCOD').AsInteger;
                ProtocoPak := QueryIts.FieldByName('ProtocoPak').AsInteger;
                CNAB_Cfg   := QueryIts.FieldByName('CNAB_Cfg').AsInteger;
                TipoProt   := QueryIts.FieldByName('TipoProt').AsInteger;
                Lote       := ObtemLoteProtocolo(Protocol, ProtocoPak, CNAB_Cfg, TipoProt);
                RecipEmeio := '';
                //
                GeraProtocolo_BloqProto(RecipEmeio, QueryIts, QueryLoc,
                  QueryUpd, Database, Lote, EntCliInt, TipoProt);
              end;
            end;
          finally
            Progress.Position := 0;
            Grade.Enabled     := True;
            QueryIts.EnableControls;
            //
            Result := True;
          end;
        end else
        begin
          Protocol   := QueryIts.FieldByName('PROTOCOD').AsInteger;
          ProtocoPak := QueryIts.FieldByName('ProtocoPak').AsInteger;
          CNAB_Cfg   := QueryIts.FieldByName('CNAB_Cfg').AsInteger;
          TipoProt   := QueryIts.FieldByName('TipoProt').AsInteger;
          Lote       := ObtemLoteProtocolo(Protocol, ProtocoPak, CNAB_Cfg, TipoProt);
          RecipEmeio := '';
          //
          GeraProtocolo_BloqProto(RecipEmeio, QueryIts, QueryLoc,
            QueryUpd, Database, Lote, EntCliInt, TipoProt);
          //
          Result := True;
        end;
      end;
      istTodos:
      begin
        try
          QueryIts.DisableControls;
          QueryIts.First;
          Grade.Enabled := False;
          Progress.Max  := QueryIts.RecordCount;
          //
          while not QueryIts.Eof do
          begin
            Progress.Position := Progress.Position + 1;
            Progress.Update;
            Application.ProcessMessages;
            //
            Protocol   := QueryIts.FieldByName('PROTOCOD').AsInteger;
            ProtocoPak := QueryIts.FieldByName('ProtocoPak').AsInteger;
            CNAB_Cfg   := QueryIts.FieldByName('CNAB_Cfg').AsInteger;
            TipoProt   := QueryIts.FieldByName('TipoProt').AsInteger;
            Lote       := ObtemLoteProtocolo(Protocol, ProtocoPak, CNAB_Cfg, TipoProt);
            RecipEmeio := '';
            //
            GeraProtocolo_BloqProto(RecipEmeio, QueryIts, QueryLoc,
              QueryUpd, Database, Lote, EntCliInt, TipoProt);
            //
            QueryIts.Next;
          end;
        finally
          Progress.Position := 0;
          Grade.Enabled     := True;
          QueryIts.EnableControls;
          //
          Result := True;
        end;
      end;
      else
      begin
        Geral.MB_Aviso('Modo se sele��o n�o implementado!');
        Result := False;
        Exit;
      end;
    end;
  end else
    Result := True;
end;

function TUnProtocolo.GeraProtocoloNFSe(Quais: TSelType; QueryIts: TmySQLQuery;
  Database: TmySQLDatabase; Grade: TDBGrid; Progress: TProgressBar;
  EntCliInt: Integer): Boolean;

  function ObtemLoteProtocolo(Protocolo, ProtocoPak, CNAN_Cfg,
    TipoProt: Integer; QueryLoc, QueryUpd: TmySQLQuery): Integer;
  var
    Agora: TDateTime;
    Lote: Integer;
    Mes: String;
  begin
    Lote := LocalizaLote(ProtocoPak, QueryLoc, Database);
    //
    if Lote = 0 then
    begin
      Agora := DModG.ObtemAgora();
      Mes   := Geral.DataToMez(Agora);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
        'SELECT Controle ',
        'FROM protocopak ',
        'WHERE Codigo=' + Geral.FF0(Protocolo),
        'AND Mez=' + Mes,
        'AND SeqArq = 0 ',
        '']);
      if QueryLoc.RecordCount > 0 then
        Lote := QueryLoc.FieldByName('Controle').AsInteger
      else
        Lote := GeraLoteProtocolo(Database, Protocolo, CNAN_Cfg, TipoProt);
    end;
    //
    Result := Lote;
  end;

var
  i, Lote, ProtocoPak, TipoProt, Protocol: Integer;
  RecipEmeio: String;
  QueryLoc, QueryUpd: TmySQLQuery;
begin
  Result := False;
  //
  if (QueryIts.State <> dsInactive) and (QueryIts.RecordCount > 0) then
  begin
    Progress.Position := 0;
    QueryLoc          := TmySQLQuery.Create(TDataModule(Database.Owner));
    QueryUpd          := TmySQLQuery.Create(TDataModule(Database.Owner));
    //
    try
      case Quais of
        istSelecionados:
        begin
          if Grade.SelectedRows.Count > 1 then
          begin
            try
              QueryIts.DisableControls;
              Grade.Enabled := False;
              Progress.Max  := Grade.SelectedRows.Count;
              //
              with Grade.DataSource.DataSet do
              begin
                for i := 0 to Grade.SelectedRows.Count-1 do
                begin
                  GotoBookmark(Grade.SelectedRows.Items[i]);
                  //
                  Progress.Position := Progress.Position + 1;
                  Progress.Update;
                  Application.ProcessMessages;
                  //
                  Protocol   := QueryIts.FieldByName('PROTOCOD').AsInteger;
                  ProtocoPak := QueryIts.FieldByName('ProtocoPak').AsInteger;
                  TipoProt   := QueryIts.FieldByName('TipoProt').AsInteger;
                  RecipEmeio := '';
                  Lote       := ObtemLoteProtocolo(Protocol, ProtocoPak, 0,
                                  TipoProt, QueryLoc, QueryUpd);
                  //
                  GeraProtocolo_NFSeProto(RecipEmeio, QueryIts, Database, Lote,
                    EntCliInt, TipoProt);
                end;
              end;
            finally
              Progress.Position := 0;
              Grade.Enabled     := True;
              QueryIts.EnableControls;
              //
              Result := True;
            end;
          end else
          begin
            Protocol   := QueryIts.FieldByName('PROTOCOD').AsInteger;
            ProtocoPak := QueryIts.FieldByName('ProtocoPak').AsInteger;
            TipoProt   := QueryIts.FieldByName('TipoProt').AsInteger;
            RecipEmeio := '';
            Lote       := ObtemLoteProtocolo(Protocol, ProtocoPak, 0, TipoProt,
                            QueryLoc, QueryUpd);
            //
            GeraProtocolo_NFSeProto(RecipEmeio, QueryIts, Database, Lote,
              EntCliInt, TipoProt);
            //
            Result := True;
          end;
        end;
        istTodos:
        begin
          try
            QueryIts.DisableControls;
            Grade.Enabled := False;
            Progress.Max  := QueryIts.RecordCount;
            //
            QueryIts.First;
            //
            while not QueryIts.Eof do
            begin
              Progress.Position := Progress.Position + 1;
              Progress.Update;
              Application.ProcessMessages;
              //
              Protocol   := QueryIts.FieldByName('PROTOCOD').AsInteger;
              ProtocoPak := QueryIts.FieldByName('ProtocoPak').AsInteger;
              TipoProt   := QueryIts.FieldByName('TipoProt').AsInteger;
              RecipEmeio := '';
              Lote       := ObtemLoteProtocolo(Protocol, ProtocoPak, 0,
                              TipoProt, QueryLoc, QueryUpd);
              //
              GeraProtocolo_NFSeProto(RecipEmeio, QueryIts, Database, Lote,
                EntCliInt, TipoProt);
              //
              QueryIts.Next;
            end;
          finally
            Progress.Position := 0;
            Grade.Enabled     := True;
            QueryIts.EnableControls;
            //
            Result := True;
          end;
        end;
        else
        begin
          Geral.MB_Aviso('Modo se sele��o n�o implementado!');
          Result := False;
          Exit;
        end;
      end;
    finally
      QueryLoc.Free;
      QueryUpd.Free;
    end;
  end else
    Result := True;
end;

function TUnProtocolo.NomeDoTipoDeProtocolo(Tipo: Integer): String;
begin
  case Tipo of
    VAR_TIPO_PROTOCOLO_ND_00: Result := 'ND - N�o definido';
    VAR_TIPO_PROTOCOLO_EB_01: Result := 'EB - Entrega de Bloquetos';
    VAR_TIPO_PROTOCOLO_CE_02: Result := 'CE - Circula��o eletr�nica (emeio)';
    VAR_TIPO_PROTOCOLO_EM_03: Result := 'EM - Entrega de material de consumo';
    VAR_TIPO_PROTOCOLO_CR_04: Result := 'CR - Cobran�a registrada (banc�ria)';
    VAR_TIPO_PROTOCOLO_CD_05: Result := 'CD - Circula��o de documentos';
    else Result := '? ? ? ? ?';
  end;
end;

function TUnProtocolo.ObtemListaProEnPrFinalidade: TStringList;
var
  I: Integer;
  Lista: TStringList;
begin
  Lista := TStringList.Create;
  try
    for I := 0 to Length(CO_PTK_Finalidades) - 1 do
      Lista.Add(CO_PTK_Finalidades[I]);
  finally
    Result := Lista;
  end;
end;

function TUnProtocolo.ObtemDescriItemDeListaProEnPrFinalidade(Id: Integer): String;
begin
  Result := CO_PTK_Finalidades[Id];
end;

procedure TUnProtocolo.ExcluiLoteProtocoloBoleto(Database: TmySQLDatabase;
  Tarefa, PrevCod, EntCliInt, Arreits: Integer; TabelaAri: String);

  procedure ExcluiLotesVazios(QryLoc: TmySQLQuery);
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(QryLoc, Database, [
      'DELETE ',
      'FROM protocopak ',
      'WHERE Controle NOT IN ',
      '( ',
      'SELECT Controle ',
      'FROM protpakits ',
      ') ',
      '']);
  end;

var
  QryLoc, QryUpd: TmySQLQuery;
  Conta, CNAB_Cfg, Apto, Entidade: Integer;
  Boleto: Double;
  SQL: String;
begin
  QryLoc := TmySQLQuery.Create(TDataModule(Database.Owner));
  QryUpd := TmySQLQuery.Create(TDataModule(Database.Owner));
  //
  if Tarefa <> 0 then
    SQL := 'AND Codigo=' + Geral.FF0(Tarefa)
  else
    SQL := '';
  //
  try
    if VAR_KIND_DEPTO = kdUH then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QryLoc, Database, [
        'SELECT ari.CNAB_Cfg, ari.Apto, ari.Boleto ',
        'FROM ' + TabelaAri + ' ari ',
        'LEFT JOIN cnab_cfg cfg ON cfg.Codigo = ari.CNAB_Cfg',
        'WHERE ari.Controle=' + Geral.FF0(Arreits),
        '']);
      CNAB_Cfg := QryLoc.FieldByName('CNAB_Cfg').AsInteger;
      Apto     := QryLoc.FieldByName('Apto').AsInteger;
      Boleto   := QryLoc.FieldByName('Boleto').AsFloat;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QryLoc, Database, [
        'SELECT * ',
        'FROM protpakits ',
        'WHERE ID_Cod1=' + Geral.FF0(PrevCod),
        'AND ID_Cod2=' + Geral.FF0(EntCliInt),
        'AND ID_Cod4=' + Geral.FF0(CNAB_Cfg),
        'AND Depto=' + Geral.FF0(Apto),
        'AND Docum=' + Geral.FFI(Boleto),
        SQL,
        '']);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QryLoc, Database, [
        'SELECT ari.CNAB_Cfg, ari.Entidade, ari.Boleto ',
        'FROM ' + TabelaAri + ' ari ',
        'LEFT JOIN cnab_cfg cfg ON cfg.Codigo = ari.CNAB_Cfg',
        'WHERE ari.Controle=' + Geral.FF0(Arreits),
        '']);
      CNAB_Cfg := QryLoc.FieldByName('CNAB_Cfg').AsInteger;
      Entidade := QryLoc.FieldByName('Entidade').AsInteger;
      Boleto   := QryLoc.FieldByName('Boleto').AsFloat;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QryLoc, Database, [
        'SELECT * ',
        'FROM protpakits ',
        'WHERE ID_Cod1=' + Geral.FF0(PrevCod),
        'AND ID_Cod2=' + Geral.FF0(EntCliInt),
        'AND ID_Cod4=' + Geral.FF0(CNAB_Cfg),
        'AND Cliente=' + Geral.FF0(Entidade),
        'AND Docum=' + Geral.FFI(Boleto),
        SQL,
        '']);
    end;
    if QryLoc.RecordCount > 0 then
    begin
      while not QryLoc.Eof do
      begin
        Conta := QryLoc.FieldByName('Conta').AsInteger;
        //
        UnDmkDAC_PF.ExecutaMySQLQuery0(QryUpd, Database, [
          'DELETE FROM protpakits ',
          'WHERE Conta=' + Geral.FF0(Conta),
          '']);
        //
        QryLoc.Next;
      end;
      ExcluiLotesVazios(QryUpd);
    end;
  finally
    QryLoc.Free;
    QryUpd.Free;
  end;
end;

procedure TUnProtocolo.ExcluiLoteProtocoloNFSe(QueryLoc, QueryUpd: TmySQLQuery;
  Database: TmySQLDatabase; CliInt, Ambiente, EntiMail, ProtocoPak,
  RpsIDNumero: Integer; RpsIDSerie: String);

  procedure ExcluiLoteSeVazio(ProtocoPak: Integer);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
      'SELECT * ',
      'FROM protpakits ',
      'WHERE Controle=' + Geral.FF0(ProtocoPak),
      '']);
    if QueryLoc.RecordCount = 0 then
      UMyMod.ExcluiRegistroInt1('', 'protocopak', 'Controle', ProtocoPak, Database);
  end;

begin
  if ProtocoPak <> 0 then
  begin
    ExcluiLoteSeVazio(ProtocoPak);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, Database, [
      'DELETE FROM protpakits ',
      'WHERE Controle=' + Geral.FF0(ProtocoPak),
      'AND ID_Cod1=' + Geral.FFI(Ambiente),
      'AND ID_Cod2="' + RpsIDSerie + '"',
      'AND ID_Cod3=' + Geral.FFI(RpsIDNumero),
      'AND ID_Cod4=' + Geral.FFI(EntiMail),
      'AND CliInt=' + Geral.FFI(CliInt),
      '']);
  end;
end;

function TUnProtocolo.GeraLoteProtocolo(Database: TmySQLDatabase; Protocolo,
  CNAB_Cfg, TipoProt: Integer): Integer;
var
  Controle: Integer;
  DataI, DataL, Mes: String;
  Agora: TDateTime;
  QueryAux, QueryUpd: TmySQLQuery;
begin
  Result := 0;
  //
  if (TipoProt = 4) and (CNAB_Cfg = 0) then //CR
    Exit;
  if Protocolo = 0 then
    Exit;
  //
  QueryUpd          := TmySQLQuery.Create(TDataModule(Database.Owner));
  QueryAux          := TmySQLQuery.Create(TDataModule(Database.Owner));
  QueryUpd.DataBase := Database;
  QueryAux.DataBase := Database;
  try
    Agora := DModG.ObtemAgora();
    DataI := Geral.FDT(Agora, 01);
    DataL := Geral.FDT(Agora, 01);
    Mes   := Geral.DataToMez(Agora);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QueryAux, Database, [
      'SELECT MAX(Controle) Controle ',
      'FROM protocopak ',
      'WHERE SeqArq = 0 ',
      'AND DataI="' + DataI + '"',
      'AND DataL="' + DataL + '"',
      'AND Mez="' + Mes + '"',
      'AND Codigo=' + Geral.FF0(Protocolo),
      'AND CNAB_Cfg=' + Geral.FF0(CNAB_Cfg),
      '']);
    if (QueryAux.RecordCount = 0) or (QueryAux.FieldByName('Controle').AsInteger = 0) then
    begin
      Controle := UMyMod.BuscaEmLivreY_Def('protocopak', 'Controle', stIns, 0);
      //
      if (Controle > 0) and (Protocolo > 0 ) then
      begin
        if UMyMod.SQLInsUpd(QueryUpd, stIns, 'protocopak', False,
          ['DataI', 'DataL', 'Mez', 'Codigo', 'CNAB_Cfg'], ['Controle'],
          [DataI, DataL, Mes, Protocolo, CNAB_Cfg], [Controle], True)
        then
          Result := Controle;
      end;
    end else
      Result := QueryAux.FieldByName('Controle').AsInteger;
  finally
    QueryUpd.Free;
    QueryAux.Free;
  end;
end;

procedure TUnProtocolo.ProtocolosCD_Doc(Quais: TSelType; DBGLct: TDBGrid;
  ModuleLctX: TDataModule; CliInt: Integer; FormOrigem, TabLctA: String);
var
  QrSource: TmySQLQuery;
  I: Integer;
  LctProto: String;
  procedure IncluiAtual();
  var
    Data, SerieCH, Vencimento: String;
    Tipo, Carteira, Sub, CliInt, Cliente, Fornece: Integer;
    Controle, Documento, Credito, Debito: Double;
  begin
    Data           := Geral.FDT(QrSource.FieldByName('Data').AsDateTime, 1);
    Tipo           := QrSource.FieldByName('Tipo'      ).AsInteger;
    Carteira       := QrSource.FieldByName('Carteira'  ).AsInteger;
    Controle       := QrSource.FieldByName('Controle'  ).AsInteger;
    Sub            := QrSource.FieldByName('Sub'       ).AsInteger;
    CliInt         := QrSource.FieldByName('CliInt'    ).AsInteger;
    Cliente        := QrSource.FieldByName('Cliente'   ).AsInteger;
    Fornece        := QrSource.FieldByName('Fornecedor').AsInteger;
    Documento      := QrSource.FieldByName('Documento' ).AsFloat;
    Credito        := QrSource.FieldByName('Credito'   ).AsFloat;
    Debito         := QrSource.FieldByName('Debito'    ).AsFloat;
    SerieCH        := QrSource.FieldByName('SerieCH'   ).AsString;
    Vencimento     := Geral.FDT(QrSource.FieldByName('Vencimento').AsDateTime, 1);
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, LctProto, False, [
    'CliInt', 'Cliente', 'Fornecedor',
    'Documento', 'Credito', 'Debito',
    'SerieCH', 'Vencimento'], [
    'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
    CliInt, Cliente, Fornece,
    Documento, Credito, Debito,
    SerieCH, Vencimento], [
    Data, Tipo, Carteira, Controle, Sub], False);
    //
  end;
var
  Carteira, Controle: Integer;
begin
  LctProto := CriarProtoco.RecriaTempTableNovo(UnCreateProtoco.ntrtt_LctProto, DmodG.QrUpdPID1, False);
  if MyObjects.FIC(DBGLct = nil, nil, 'DBGLct indefinido em UnProtocolo!') then
    Exit;
  QrSource := TmySQLQuery(DBGLct.DataSource.DataSet);
  if MyObjects.FIC(QrSource = nil, nil, 'DataSet indefinido em UnProtocolo!') then
    Exit;
  //
  if Quais = istTodos then
  begin
    if Geral.MB_Pergunta('Gera protocolo para todos itens?') = ID_YES then
    begin
      //
      QrSource.First;
      while not QrSource.Eof do
      begin
        IncluiAtual();
        QrSource.Next;
      end;
      //
    end;
  end else if Quais = istSelecionados then
  begin
    if DBGLct.SelectedRows.Count > 1 then
    begin
      with DBGLct.DataSource.DataSet do
      for I := 0 to DBGLct.SelectedRows.Count-1 do
      begin
        GotoBookmark(DBGLct.SelectedRows.Items[I]);
        IncluiAtual();
      end;
    end else IncluiAtual();
  end else if Quais = istAtual then IncluiAtual()
  else
  begin
    Geral.MB_Erro('SelType indefinido em UnProtocolo!');
    //
    Exit;
  end;
  //
  {$IFDEF DEFINE_VARLCT}
  TDmLct2(ModuleLctX).QrLcP.Close;
  TDmLct2(ModuleLctX).QrLcP.Database := DModG.MyPID_DB;
  UnDmkDAC_PF.AbreMySQLQuery0(TDmLct2(ModuleLctX).QrLcP, DModG.MyPID_DB, [
  'SELECT Data, Tipo, Carteira, Controle, ',
  'Sub, CliInt, Cliente, Fornecedor, ',
  'Documento, SUM(Credito) Credito, SUM(Debito) ',
  'Debito, SerieCH, Vencimento, COUNT(Ativo) ITENS ',
  'FROM ' + LctProto,
  'GROUP BY Data, Tipo, Carteira, CliInt, ',
  'SerieCH, Documento ',
  '']);
  //
  if TDmLct2(ModuleLctX).QrCrt.State = dsInactive then Carteira := 0
  else Carteira := TDmLct2(ModuleLctX).QrCrtCodigo.Value;
  //
  if DBCheck.CriaFm(TFmProSel, FmProSel, afmoNegarComAviso) then
  begin
    {
    FmProSel.QrProtocolos.Close;
    FmProSel.QrProtocolos.Params[0].AsInteger := VAR_TIPO_PROTOCOLO_CD_05; // Circula��o de documentos
    FmProSel.QrProtocolos. O p e n ;
    }
    FmProSel.FEntCliInt := CliInt;
    FmProSel.FTabLctA   := TabLctA;
    //
    FmProSel.ReopenProtocolos(VAR_TIPO_PROTOCOLO_CD_05, 0, CliInt);
    //
    //FmProSel.FQuais := Quais;
    // Muda para todos pois foi criada uma tabela para o(s) selecionado(s)
    FmProSel.FQuais      := istTodos;
    FmProSel.FNomeQrSrc  := 'TDmLct2(ModuleLctX).QrLcP';
    FmProSel.FQrSource   := TDmLct2(ModuleLctX).QrLcP;
    FmProSel.FQrLci      := TDmLct2(ModuleLctX).QrLcI;
    FmProSel.FDBGrid     := DBGLct;
    //FmProSel.FTabLctA    := TDmLct2(ModuleLctX).FTabLctA;
    //FmProSel.FLctProto   := LctProto;
    FmProSel.FAskTodos   := False;
    //
    FmProSel.ShowModal;
    FmProSel.Destroy;
    //
    Controle := TDmLct2(ModuleLctX).QrLctControle.Value;
    //
    TDmLct2(ModuleLctX).ReabreCarteiras(Carteira, TDmLct2(ModuleLctX).QrCrt, TDmLct2(ModuleLctX).QrCrtSum,
      'TFmCondGer.ProtocolosCD_Doc()');
    TDmLct2(ModuleLctX).QrLct.Locate('Controle', Controle, []);
  end;
  {$ENDIF}
end;

procedure TUnProtocolo.ProtocolosCD_Lct(Quais: TSelType; DBGLct: TDBGrid;
  ModuleLctX: TDataModule; CliInt: Integer; FormOrigem, TabLctA: String);
var
  Controle: Integer;
  Carteira: Integer;
begin
  {$IFDEF DEFINE_VARLCT}
  if TDmLct2(ModuleLctX).QrCrt.State = dsInactive then Carteira := 0
  else Carteira := TDmLct2(ModuleLctX).QrCrtCodigo.Value;
  //
  if DBCheck.CriaFm(TFmProSel, FmProSel, afmoNegarComAviso) then
  begin
    {
    FmProSel.QrProtocolos.Close;
    FmProSel.QrProtocolos.Params[0].AsInteger := VAR_TIPO_PROTOCOLO_CD_05; // Circula��o de documentos
    FmProSel.QrProtocolos . O p e n ;
    }
    FmProSel.ReopenProtocolos(VAR_TIPO_PROTOCOLO_CD_05, 0, CliInt);
    //
    FmProSel.FEntCliInt := CliInt;
    FmProSel.FTabLctA   := TabLctA;
    //
    FmProSel.FQuais      := Quais;
    FmProSel.FNomeQrSrc  := 'TDmLct2(ModuleLctX).QrLct';
    FmProSel.FQrSource   := TDmLct2(ModuleLctX).QrLct;
    //FmProSel.FDBGrid     := TDBGrid(DBGLct);
    //FmProSel.FTabLctA    := TDmLct2(ModuleLctX).FTabLctA;
    //
    FmProSel.ShowModal;
    FmProSel.Destroy;
    //
    Controle := TDmLct2(ModuleLctX).QrLctControle.Value;
    //
    TDmLct2(ModuleLctX).ReabreCarteiras(Carteira, TDmLct2(ModuleLctX).QrCrt, TDmLct2(ModuleLctX).QrCrtSum,
      'TFmCondGer.ProtocolosCD_Lct()');
    TDmLct2(ModuleLctX).QrLct.Locate('Controle', Controle, []);
  end;
  {$ENDIF}
end;

function TUnProtocolo.ProtocolosCD_OS1(Quais: TSelType; DBGOScab: TDBGrid;
  QrOSCab: TmySQLQuery; CliInt: Integer): Integer;
(*
var
  Controle: Integer;
  Carteira: Integer;
*)
begin
(*
  if TDmLct2(ModuleLctX).QrCrt.State = dsInactive then Carteira := 0
  else Carteira := TDmLct2(ModuleLctX).QrCrtCodigo.Value;
  //
*)
  Result := 0;
  //
  if DBCheck.CriaFm(TFmProSel, FmProSel, afmoNegarComAviso) then
  begin
    {
    FmProSel.QrProtocolos.Close;
    FmProSel.QrProtocolos.Params[0].AsInteger := VAR_TIPO_PROTOCOLO_CD_05; // Circula��o de documentos
    FmProSel.QrProtocolos . O p e n ;
    }
    FmProSel.ReopenProtocolos(VAR_TIPO_PROTOCOLO_CD_05, 0, CliInt);
    //
    FmProSel.FQuais      := Quais;
    FmProSel.FNomeQrSrc  := 'QrOSCab';
    FmProSel.FQrSource   := QrOSCab;
    FmProSel.FDBGrid     := TDBGrid(DBGOSCab);
    //FmProSel.FTabLctA    := '';
    //
    FmProSel.ShowModal;
    Result := FmProSel.FLote;
    FmProSel.Destroy;
    //
(*
    Controle := TDmLct2(ModuleLctX).QrLctControle.Value;
    //
    TDmLct2(ModuleLctX).ReabreCarteiras(Carteira, TDmLct2(ModuleLctX).QrCrt, TDmLct2(ModuleLctX).QrCrtSum,
      'TFmCondGer.ProtocolosCD_Lct()');
    TDmLct2(ModuleLctX).QrLct.Locate('Controle', Controle, []);
*)
  end;
end;

function TUnProtocolo.Proto_Email_AtualizDataE(DataBase: TmySQLDatabase;
  Protocolo: Integer; Agora: TDateTime): Boolean;
{$IfDef TEM_DBWEB}
var
  Query: TmySQLQuery;
begin
  Result := False;
  Query  := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    if UnDmkDAC_PF.ExecutaMySQLQuery0(Query, DataBase, [
      'UPDATE protpakits SET AlterWeb=1, ',
      'DataE="' + Geral.FDT(Agora, 1) + '"',
      'WHERE Conta=' + Geral.FF0(Protocolo),
      ''])
    then
      Result := True;
  finally

  end;
{$Else}
begin
  Result := True; //Sem DBWeb n�o faz nada
{$EndIf}
end;

function TUnProtocolo.Proto_Email_CriaProtocoloEnvio(DataBase: TmySQLDatabase;
  Cliente, Protocolo: Integer; Email: String): Boolean;
{$IfDef TEM_DBWEB}
var
  Query: TmySQLQuery;
begin
    Result := False;
    Query  := TmySQLQuery.Create(TDataModule(DataBase.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
        'SELECT * ',
        'FROM wpropaits ',
        'WHERE Cliente=' + Geral.FF0(Cliente),
        'AND Protocolo=' + Geral.FF0(Protocolo),
        'AND Email="' + Email + '"',
        '']);
      if Query.RecordCount = 0 then
      begin
        //Registra no servidor que vai mandar o e-mail
        UnDmkDAC_PF.ExecutaMySQLQuery0(Query, DataBase, [
          'INSERT INTO wpropaits SET ',
          'Cliente=' + Geral.FF0(Cliente) + ', ',
          'Protocolo=' + Geral.FF0(Protocolo) + ', ',
          'Email="' + Email + '"']);
      end;
      Result := True;
    finally
      Query.Free;
    end;
{$Else}
begin
    Result := True; //Sem DBWeb n�o faz nada
{$EndIf}
end;

procedure TUnProtocolo.Proto_Email_GeraLinksConfirmacaoReceb2(
  const DataBase: TmySQLDatabase; const Web_Id, Email, Telefone: String; const Cliente,
  Protocolo, DMKID_APP: Integer; Finalidade: TProtFinalidade;
  var ProtocolImp: String; var Protocol: String; WhatsApp: Boolean = False);
{$IfDef TEM_DBWEB}
const
  URL_Site  = 'http://www.dermatek.com.br/links/protocolo/';
  Separador = '__';
var
  Query: TmySQLQuery;
  ComoConfL, Mail, Cli, Prot, Imp, NImp, Web_App_Nome, LinkProt, LinkProtImp,
  Finalid, Tel: String;
  Web_App_Id: Integer;
begin
  ProtocolImp := '';
  Protocol    := '';
  //
  if WhatsApp = True then
  begin
    Tel       := Telefone;
    ComoConfL := Geral.FF0(IntOfComoConf(ptkWhatsApp))
  end else
  begin
    Tel       := '';
    ComoConfL := Geral.FF0(IntOfComoConf(ptkEmail));
  end;
  //
  if DMKID_APP = 17 then //DControl
  begin
    Web_App_Nome := 'dcontrol';
    Web_App_Id   := 6;
  end else
  begin
    Web_App_Id   := 0;
    Web_App_Nome := '';
    //
    Geral.MB_Erro('Web App n�o definido na fun��o:' + sLineBreak +
      '"TUnProtocolo.Proto_Email_GeraLinksConfirmacaoReceb2"');
  end;

  if Web_Id = '' then
  begin
    Geral.MB_Erro('Vari�vel Web_Id n�o definida na fun��o:' + sLineBreak +
      '"TUnProtocolo.Proto_Email_GeraLinksConfirmacaoReceb2"');
  end;

  Query := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    case Finalidade of
      ptkBoleto:
        Finalid := 'boleto';
      ptkNFSeAut:
        Finalid := 'nfse';
      ptkNFSeCan:
        Finalid := 'nfse';
    end;

    UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
      'SELECT ',
      'MD5("'+ Email +'") Email, ',
      'MD5("'+ Geral.FF0(Cliente) +'") Cliente, ',
      'MD5("'+ Geral.FF0(Protocolo) +'") Protocolo, ',
      'MD5("'+ ComoConfL +'") ComoConfL, ',
      'MD5(1) Imprime, ',
      'MD5(0) NImprime ',
      '']);

    ComoConfL   := Query.FieldByName('ComoConfL').AsString;
    Mail        := Query.FieldByName('Email').AsString;
    Cli         := Query.FieldByName('Cliente').AsString;
    Prot        := Query.FieldByName('Protocolo').AsString;
    Imp         := Query.FieldByName('Imprime').AsString;
    NImp        := Query.FieldByName('NImprime').AsString;
    LinkProt    := Prot + Separador + Cli + Separador + Mail + Separador + ComoConfL + Separador + NImp;
    LinkProtImp := Prot + Separador + Cli + Separador + Mail + Separador + ComoConfL + Separador + Imp;
    Protocol    := URL_Site + Geral.FF0(Web_App_Id) + '/' + Web_App_Nome + '/' + Web_Id + '/' + LinkProt + '/' + Finalid + '/' + Tel;
    ProtocolImp := URL_Site + Geral.FF0(Web_App_Id) + '/' + Web_App_Nome + '/' + Web_Id + '/' + LinkProtImp + '/' + Finalid + '/' + Tel;
  finally
    Query.Free;
  end;
{$Else}
begin
    ProtocolImp := 'N�o esque�a de enviar a confirma��o de recebimento!';
    Protocol    := '';
{$EndIf}
end;

procedure TUnProtocolo.Proto_Email_GeraLinksConfirmacaoReceb(
  const DataBase: TmySQLDatabase; const Web_MyURL, WebId, Email: String;
  const Cliente, Protocolo, DMKID_APP: Integer; var ParamLink, ParamTopo: String); deprecated; // use Proto_Email_GeraLinksConfirmacaoReceb2
{$IfDef TEM_DBWEB}
const
  URL_Site = 'http://www.dermatek.net.br';
var
  Query: TmySQLQuery;
  ComoConfL, ComoConfT, Mail, Cli, Prot, IDWeb: String;
begin
    ComoConfL := Geral.FF0(IntOfComoConf(ptkWebLink));
    ComoConfT := Geral.FF0(IntOfComoConf(ptkWebAuto));

    if DMKID_APP = 17 then //DControl
      IDWeb := '&idweb=dmk'
    else if DMKID_APP = 4 then //Syndi2
      IDWeb := '&idweb=syndinet'
    else if DMKID_APP = 43 then //Syndi3
      IDWeb := '&idweb=syndinet'
    else if DMKID_APP = 24 then //Bugstrol
      IDWeb := '&idweb=bugstrolweb'
    else
      IDWeb := '';

    Query := TmySQLQuery.Create(TDataModule(DataBase.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
        'SELECT ',
        'MD5("'+ Email +'") Email, ',
        'MD5("'+ Geral.FF0(Cliente) +'") Cliente, ',
        'MD5("'+ Geral.FF0(Protocolo) +'") Protocolo, ',
        'MD5("'+ ComoConfL +'") ComoConfL, ',
        'MD5("'+ ComoConfT +'") ComoConfT ',
        '']);

      ComoConfL := Query.FieldByName('ComoConfL').AsString;
      ComoConfT := Query.FieldByName('ComoConfT').AsString;
      Mail      := Query.FieldByName('Email').AsString;
      Cli       := Query.FieldByName('Cliente').AsString;
      Prot      := Query.FieldByName('Protocolo').AsString;

      ParamLink := Web_MyURL + '?page=recebibloq&comoconf=' + ComoConfL +
                     '&email=' + Mail + '&cliente=' + Cli + '&protocolo=' +
                     Prot;

      ParamTopo := '<img src="' + URL_Site +
                     '/complementos/com_emailconfrecebi.php?comoconf=' +
                     ComoConfT + '&email=' + Mail + '&cliente=' + Cli +
                     '&nomeweb=' + WebId + '&protocolo=' + Prot + IDWeb +
                     '" width="1" height="1"/>';
    finally
      Query.Free;
    end;
{$Else}
begin
    ParamLink := 'N�o esque�a de enviar a confirma��o de recebimento!';
    ParamTopo := '';
{$EndIf}
end;

function TUnProtocolo.VerificaSeLoteCRFoiGerado(Database: TmySQLDatabase;
  PrevCod, EntCliInt, CNAB_Cfg: Integer; EntDepto: Integer; Boleto: Double): Boolean;
var
  Qry: TmySQLQuery;
  ProtocolCR: Integer;
begin
  Result := False;
  Qry    := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
      'SELECT ProtocolCR ',
      'FROM cnab_cfg ',
      'WHERE Codigo=' + Geral.FF0(CNAB_Cfg),
      '']);
    ProtocolCR := Qry.FieldByName('ProtocolCR').AsInteger;
    //
    if VAR_KIND_DEPTO = kdUH then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
        'SELECT its.* ',
        'FROM protpakits its ',
        'LEFT JOIN protocopak pak ON pak.Controle = its.Controle ',
        'WHERE its.ID_Cod1=' + Geral.FF0(PrevCod),
        'AND its.ID_Cod2=' + Geral.FF0(EntCliInt),
        'AND its.ID_Cod4=' + Geral.FF0(CNAB_Cfg),
        'AND its.Depto=' + Geral.FF0(EntDepto),
        'AND its.Docum=' + Geral.FFI(Boleto),
        'AND its.Codigo=' + Geral.FF0(ProtocolCR),
        'AND pak.SeqArq <> 0 ',
        '']);
      if Qry.RecordCount > 0 then
        Result := True;
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
        'SELECT its.* ',
        'FROM protpakits its ',
        'LEFT JOIN protocopak pak ON pak.Controle = its.Controle ',
        'WHERE its.ID_Cod1=' + Geral.FF0(PrevCod),
        'AND its.ID_Cod2=' + Geral.FF0(EntCliInt),
        'AND its.ID_Cod4=' + Geral.FF0(CNAB_Cfg),
        'AND its.Cliente=' + Geral.FF0(EntDepto),
        'AND its.Docum=' + Geral.FFI(Boleto),
        'AND its.Codigo=' + Geral.FF0(ProtocolCR),
        'AND pak.SeqArq <> 0 ',
        '']);
      if Qry.RecordCount > 0 then
        Result := True;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnProtocolo.VerificaSeProtocoloRetornou(Database: TmySQLDatabase;
  PrevCod, EntCliInt, CNAB_Cfg: Integer; EntDepto: Integer; Boleto: Double): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry    := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  try
    if VAR_KIND_DEPTO = kdUH then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
        'SELECT its.* ',
        'FROM protpakits its ',
        'LEFT JOIN protocopak pak ON pak.Controle = its.Controle ',
        'WHERE its.ID_Cod1=' + Geral.FF0(PrevCod),
        'AND its.ID_Cod2=' + Geral.FF0(EntCliInt),
        'AND its.ID_Cod4=' + Geral.FF0(CNAB_Cfg),
        'AND its.Depto=' + Geral.FF0(EntDepto),
        'AND its.Docum=' + Geral.FFI(Boleto),
        'AND its.DataD >= "1900-01-01" ',
        '']);
      if Qry.RecordCount > 0 then
        Result := True;
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
        'SELECT its.* ',
        'FROM protpakits its ',
        'LEFT JOIN protocopak pak ON pak.Controle = its.Controle ',
        'WHERE its.ID_Cod1=' + Geral.FF0(PrevCod),
        'AND its.ID_Cod2=' + Geral.FF0(EntCliInt),
        'AND its.ID_Cod4=' + Geral.FF0(CNAB_Cfg),
        'AND its.Cliente=' + Geral.FF0(EntDepto),
        'AND its.Docum=' + Geral.FFI(Boleto),
        'AND its.DataD >= "1900-01-01" ',
        '']);
      if Qry.RecordCount > 0 then
        Result := True;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnProtocolo.VerificaSeProtocoloPadraoExiste(const Database: TmySQLDatabase;
  const Protocolo, EntDepto: Integer; const Finalidade: TProtFinalidade;
  var Codigo, Controle: Integer);
var
  Query: TMySQLQuery;
begin
  Codigo   := 0;
  Controle := 0;

  Query := TmySQLQuery.Create(TDataModule(Database.Owner));
  try
    //Verifica protocolo padr�o
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
      'SELECT Codigo ',
      'FROM proenpr ',
      'WHERE Protocolo=' + Geral.FF0(Protocolo),
      '']);
    if Query.RecordCount > 0 then
      Codigo := Query.FieldByName('Codigo').AsInteger
    else
      Exit;
    if Finalidade = ptkBoleto then
    begin
      //Verifica itens do protocolo padr�o
      if VAR_KIND_DEPTO = kdUH then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
          'SELECT Controle ',
          'FROM proenprit ',
          'WHERE Codigo=' + Geral.FF0(Codigo),
          'AND Depto=' + Geral.FF0(EntDepto),
          '']);
        if Query.RecordCount > 0 then
          Controle := Query.FieldByName('Controle').AsInteger;
      end else
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
          'SELECT Controle ',
          'FROM proenprit ',
          'WHERE Codigo=' + Geral.FF0(Codigo),
          'AND Entidade=' + Geral.FF0(EntDepto),
          '']);
        if Query.RecordCount > 0 then
          Controle := Query.FieldByName('Controle').AsInteger;
      end;
    end;
  finally
    Query.Free;
  end;
end;

end.
