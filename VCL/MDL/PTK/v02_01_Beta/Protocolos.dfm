object FmProtocolos: TFmProtocolos
  Left = 379
  Top = 177
  Caption = 'GER-PROTO-001 :: Ocorr'#234'ncias de Protocolos'
  ClientHeight = 718
  ClientWidth = 993
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnGeral: TPanel
    Left = 0
    Top = 94
    Width = 993
    Height = 624
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 993
      Height = 624
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'Gerencia'
        object dmkDBGrid3: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 986
          Height = 552
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Tarefa'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPROTOCOLO'
              Title.Caption = 'Descri'#231#227'o tarefa'
              Width = 160
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lote'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conta'
              Title.Caption = 'Protocolo'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Docum'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PERIODO_TXT'
              Title.Caption = 'Per'#237'odo'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATAE_TXT'
              Title.Caption = 'Entrega'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Retorna'
              ReadOnly = True
              Title.Caption = 'R?  Retorna?'
              Width = 14
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATAD_TXT'
              Title.Caption = 'Retorno'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLI_INT'
              Title.Caption = 'Cliente interno'
              Width = 96
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLIENTE'
              Title.Caption = 'Cliente'
              Width = 96
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CANCELADO_TXT'
              Title.Caption = 'Cancelado'
              Width = 27
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEMOTIVO'
              Title.Caption = 'Motivo do cancelamento'
              Width = 240
              Visible = True
            end>
          Color = clWindow
          DataSource = DsAbertos
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnColEnter = DBGrid2ColEnter
          OnColExit = DBGrid2ColExit
          OnDrawColumnCell = DBGrid2DrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Tarefa'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPROTOCOLO'
              Title.Caption = 'Descri'#231#227'o tarefa'
              Width = 160
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lote'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conta'
              Title.Caption = 'Protocolo'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Docum'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PERIODO_TXT'
              Title.Caption = 'Per'#237'odo'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATAE_TXT'
              Title.Caption = 'Entrega'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Retorna'
              ReadOnly = True
              Title.Caption = 'R?  Retorna?'
              Width = 14
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATAD_TXT'
              Title.Caption = 'Retorno'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLI_INT'
              Title.Caption = 'Cliente interno'
              Width = 96
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLIENTE'
              Title.Caption = 'Cliente'
              Width = 96
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CANCELADO_TXT'
              Title.Caption = 'Cancelado'
              Width = 27
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEMOTIVO'
              Title.Caption = 'Motivo do cancelamento'
              Width = 240
              Visible = True
            end>
        end
        object Panel8: TPanel
          Left = 0
          Top = 552
          Width = 986
          Height = 47
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object BtProtocolo2: TBitBtn
            Left = 4
            Top = 4
            Width = 89
            Height = 39
            Cursor = crHandPoint
            Caption = '&Protocolo'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtProtocolo2Click
          end
          object Panel9: TPanel
            Left = 879
            Top = 0
            Width = 107
            Height = 47
            Align = alRight
            Alignment = taRightJustify
            BevelOuter = bvNone
            TabOrder = 1
            object BitBtn4: TBitBtn
              Tag = 13
              Left = 4
              Top = 4
              Width = 89
              Height = 39
              Cursor = crHandPoint
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Cadastro'
        ImageIndex = 1
        object PainelEdita: TPanel
          Left = 0
          Top = 0
          Width = 985
          Height = 596
          Align = alClient
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBackground = False
          ParentFont = False
          TabOrder = 1
          Visible = False
          ExplicitWidth = 986
          ExplicitHeight = 599
          object PainelEdit: TPanel
            Left = 0
            Top = 0
            Width = 985
            Height = 307
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitWidth = 986
            object Label9: TLabel
              Left = 16
              Top = 8
              Width = 36
              Height = 13
              Caption = 'C'#243'digo:'
            end
            object Label10: TLabel
              Left = 67
              Top = 8
              Width = 51
              Height = 13
              Caption = 'Descri'#231#227'o:'
            end
            object Label13: TLabel
              Left = 16
              Top = 264
              Width = 371
              Height = 13
              Caption = 
                'Referente a: (descri'#231#227'o do assunto no canhoto a ser assinado pel' +
                'o recebedor)'
            end
            object Label14: TLabel
              Left = 705
              Top = 8
              Width = 26
              Height = 13
              Caption = 'Sigla:'
            end
            object EdCodigo: TdmkEdit
              Left = 16
              Top = 24
              Width = 47
              Height = 20
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 8281908
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdNome: TdmkEdit
              Left = 67
              Top = 24
              Width = 635
              Height = 20
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object RGTipo: TRadioGroup
              Left = 16
              Top = 47
              Width = 738
              Height = 64
              Caption = ' Tipo de protocolo: '
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                'ND - N'#227'o definido'
                'EB - Entrega de Bloquetos (postal)'
                'CE - Circula'#231#227'o eletr'#244'nica (E-mail)'
                'EM - Entrega de material de consumo (postal)'
                'CR - Cobran'#231'a registrada (banc'#225'ria)'
                'CD - Circula'#231#227'o de documentos (postal)')
              TabOrder = 3
              OnClick = RGTipoClick
            end
            object GroupBox1: TGroupBox
              Left = 16
              Top = 118
              Width = 738
              Height = 143
              Caption = ' Padr'#245'es: '
              TabOrder = 4
              object LaCliente: TLabel
                Left = 8
                Top = 16
                Width = 149
                Height = 13
                Caption = 'Cliente interno (obrigat'#243'rio): [F7]'
              end
              object LaDef_Sender: TLabel
                Left = 370
                Top = 16
                Width = 263
                Height = 13
                Caption = 'Entregador / coletor / (entidade banc'#225'ria para CR): [F7]'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object LaPreEmeio: TLabel
                Left = 8
                Top = 78
                Width = 93
                Height = 13
                Caption = 'Pr'#233'-email (para CE):'
              end
              object SBDef_Sender: TSpeedButton
                Left = 710
                Top = 31
                Width = 20
                Height = 21
                Caption = '...'
                OnClick = SBDef_SenderClick
              end
              object SBPreEmeio: TSpeedButton
                Left = 710
                Top = 94
                Width = 20
                Height = 21
                Caption = '...'
                OnClick = SBPreEmeioClick
              end
              object CkDef_Retorn: TCheckBox
                Left = 8
                Top = 59
                Width = 230
                Height = 17
                Caption = 'Devolve documento / aviso que recebeu.'
                TabOrder = 4
              end
              object EdDef_Client: TdmkEditCB
                Left = 8
                Top = 31
                Width = 55
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBDef_Client
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBDef_Client: TdmkDBLookupComboBox
                Left = 63
                Top = 31
                Width = 303
                Height = 21
                Color = clWhite
                KeyField = 'Codigo'
                ListField = 'NOMEENT'
                ListSource = DsClient
                TabOrder = 1
                dmkEditCB = EdDef_Client
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdDef_Sender: TdmkEditCB
                Left = 370
                Top = 31
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBDef_Sender
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBDef_Sender: TdmkDBLookupComboBox
                Left = 426
                Top = 31
                Width = 280
                Height = 21
                Color = clWhite
                KeyField = 'Codigo'
                ListField = 'NOMEENT'
                ListSource = DsSender
                TabOrder = 3
                dmkEditCB = EdDef_Sender
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdPreEmeio: TdmkEditCB
                Left = 8
                Top = 94
                Width = 55
                Height = 21
                Alignment = taRightJustify
                TabOrder = 5
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBPreEmeio
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBPreEmeio: TdmkDBLookupComboBox
                Left = 63
                Top = 94
                Width = 643
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsPreEmail
                TabOrder = 6
                dmkEditCB = EdPreEmeio
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
            end
            object EdDescricao: TdmkEdit
              Left = 16
              Top = 280
              Width = 738
              Height = 20
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdSigla: TdmkEdit
              Left = 705
              Top = 24
              Width = 47
              Height = 20
              MaxLength = 3
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object GBRodaPe: TGroupBox
            Left = 0
            Top = 533
            Width = 985
            Height = 63
            Align = alBottom
            TabOrder = 1
            ExplicitTop = 536
            ExplicitWidth = 986
            object Panel14: TPanel
              Left = 2
              Top = 14
              Width = 983
              Height = 48
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object PnSaiDesis: TPanel
                Left = 842
                Top = 0
                Width = 141
                Height = 47
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 1
                object BtDesiste: TBitBtn
                  Tag = 15
                  Left = 7
                  Top = 2
                  Width = 118
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Desiste'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  NumGlyphs = 2
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtDesisteClick
                end
              end
              object BtConfirma: TBitBtn
                Tag = 14
                Left = 8
                Top = 2
                Width = 118
                Height = 40
                Cursor = crHandPoint
                Caption = '&Confirma'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtConfirmaClick
              end
            end
          end
        end
        object PainelDados: TPanel
          Left = 0
          Top = 0
          Width = 985
          Height = 596
          Align = alClient
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
          ExplicitWidth = 986
          ExplicitHeight = 599
          object LaTotal: TLabel
            Left = 0
            Top = 254
            Width = 3
            Height = 13
            Align = alBottom
          end
          object Panel15: TPanel
            Left = 0
            Top = 0
            Width = 986
            Height = 174
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Panel4: TPanel
              Left = 532
              Top = 0
              Width = 454
              Height = 174
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel6: TPanel
                Left = 0
                Top = 0
                Width = 454
                Height = 24
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object CkAbertos: TCheckBox
                  Left = 4
                  Top = 4
                  Width = 127
                  Height = 17
                  Caption = 'Somente lotes abertos.'
                  Checked = True
                  State = cbChecked
                  TabOrder = 0
                  OnClick = CkAbertosClick
                end
              end
              object DBGrid1: TDBGrid
                Left = 0
                Top = 24
                Width = 454
                Height = 150
                Align = alClient
                DataSource = DsProtocoPak
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'Lote'
                    Width = 45
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DATAL_TXT'
                    Title.Caption = 'Data limite'
                    Width = 45
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CNAB_Cfg'
                    Title.Caption = 'CNAB - Configura'#231#227'o'
                    Width = 94
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'SeqArq'
                    Title.Caption = 'Seq.Arq.'
                    Width = 38
                    Visible = True
                  end>
              end
            end
            object Panel16: TPanel
              Left = 0
              Top = 0
              Width = 532
              Height = 174
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object PainelData: TPanel
                Left = 0
                Top = 0
                Width = 532
                Height = 127
                Align = alTop
                BevelOuter = bvNone
                Enabled = False
                TabOrder = 0
                object Label2: TLabel
                  Left = 8
                  Top = 8
                  Width = 34
                  Height = 13
                  Caption = 'Tarefa:'
                  FocusControl = DBEdNome
                end
                object Label5: TLabel
                  Left = 8
                  Top = 86
                  Width = 72
                  Height = 13
                  Caption = 'Tipo de tarefa: '
                  FocusControl = DBEdit3
                end
                object Label1: TLabel
                  Left = 8
                  Top = 47
                  Width = 44
                  Height = 13
                  Caption = 'Empresa:'
                end
                object Label6: TLabel
                  Left = 130
                  Top = 86
                  Width = 242
                  Height = 13
                  Caption = 'Entregador / coletor / (entidade banc'#225'ria para CR):'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                end
                object Label16: TLabel
                  Left = 449
                  Top = 8
                  Width = 26
                  Height = 13
                  Caption = 'Sigla:'
                  FocusControl = DBEdit5
                end
                object DBEdCodigo: TDBEdit
                  Left = 8
                  Top = 24
                  Width = 47
                  Height = 21
                  Hint = 'N'#186' do banco'
                  TabStop = False
                  DataField = 'Codigo'
                  DataSource = DsProtocolos
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 8281908
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ParentShowHint = False
                  ReadOnly = True
                  ShowHint = True
                  TabOrder = 0
                end
                object DBEdNome: TDBEdit
                  Left = 59
                  Top = 24
                  Width = 385
                  Height = 21
                  Hint = 'Nome do banco'
                  Color = clWhite
                  DataField = 'Nome'
                  DataSource = DsProtocolos
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                end
                object DBEdit1: TDBEdit
                  Left = 8
                  Top = 63
                  Width = 496
                  Height = 21
                  DataField = 'NOME_CLIENT'
                  DataSource = DsProtocolos
                  TabOrder = 2
                end
                object DBEdit2: TDBEdit
                  Left = 130
                  Top = 102
                  Width = 374
                  Height = 21
                  DataField = 'NOME_SENDER'
                  DataSource = DsProtocolos
                  TabOrder = 3
                end
                object DBEdit3: TDBEdit
                  Left = 8
                  Top = 102
                  Width = 118
                  Height = 21
                  DataField = 'NOMETIPO'
                  DataSource = DsProtocolos
                  TabOrder = 4
                end
                object DBEdit5: TDBEdit
                  Left = 449
                  Top = 24
                  Width = 55
                  Height = 21
                  DataField = 'Sigla'
                  DataSource = DsProtocolos
                  TabOrder = 5
                end
              end
              object Panel10: TPanel
                Left = 0
                Top = 127
                Width = 532
                Height = 47
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                object Label4: TLabel
                  Left = 8
                  Top = 4
                  Width = 55
                  Height = 13
                  Caption = 'Hora inicial:'
                end
                object Label7: TLabel
                  Left = 67
                  Top = 4
                  Width = 48
                  Height = 13
                  Caption = 'Hora final:'
                end
                object Label8: TLabel
                  Left = 126
                  Top = 4
                  Width = 59
                  Height = 13
                  Caption = 'Tempo total:'
                end
                object Label12: TLabel
                  Left = 189
                  Top = 4
                  Width = 96
                  Height = 13
                  Caption = 'Pr'#233' e-mail (para CE):'
                  FocusControl = DBEdit4
                end
                object dmkEdHoraI: TdmkEdit
                  Left = 8
                  Top = 20
                  Width = 55
                  Height = 20
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfLong
                  HoraFormat = dmkhfLong
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEdHoraF: TdmkEdit
                  Left = 67
                  Top = 20
                  Width = 55
                  Height = 20
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfLong
                  HoraFormat = dmkhfLong
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEdTempo: TdmkEdit
                  Left = 126
                  Top = 20
                  Width = 55
                  Height = 20
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfLong
                  HoraFormat = dmkhfLong
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object DBEdit4: TDBEdit
                  Left = 189
                  Top = 20
                  Width = 316
                  Height = 21
                  DataField = 'NOMEPREEMEIO'
                  DataSource = DsProtocolos
                  TabOrder = 3
                end
              end
            end
          end
          object GBCntrl: TGroupBox
            Left = 0
            Top = 536
            Width = 986
            Height = 63
            Align = alBottom
            TabOrder = 1
            object Panel5: TPanel
              Left = 2
              Top = 14
              Width = 169
              Height = 48
              Align = alLeft
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 1
              object SpeedButton4: TBitBtn
                Tag = 4
                Left = 126
                Top = 4
                Width = 40
                Height = 39
                Cursor = crHandPoint
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = SpeedButton4Click
              end
              object SpeedButton3: TBitBtn
                Tag = 3
                Left = 86
                Top = 4
                Width = 40
                Height = 39
                Cursor = crHandPoint
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = SpeedButton3Click
              end
              object SpeedButton2: TBitBtn
                Tag = 2
                Left = 47
                Top = 4
                Width = 39
                Height = 39
                Cursor = crHandPoint
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = SpeedButton2Click
              end
              object SpeedButton1: TBitBtn
                Tag = 1
                Left = 8
                Top = 4
                Width = 39
                Height = 39
                Cursor = crHandPoint
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 3
                OnClick = SpeedButton1Click
              end
            end
            object LaRegistro: TStaticText
              Left = 171
              Top = 14
              Width = 30
              Height = 17
              Align = alClient
              BevelInner = bvLowered
              BevelKind = bkFlat
              Caption = '[N]: 0'
              TabOrder = 2
            end
            object Panel3: TPanel
              Left = 452
              Top = 14
              Width = 533
              Height = 48
              Align = alRight
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object Label3: TLabel
                Left = 362
                Top = 4
                Width = 32
                Height = 13
                Caption = 'Vezes:'
              end
              object Panel2: TPanel
                Left = 402
                Top = 0
                Width = 131
                Height = 47
                Align = alRight
                Alignment = taRightJustify
                BevelOuter = bvNone
                TabOrder = 0
                object BtSaida: TBitBtn
                  Tag = 13
                  Left = 4
                  Top = 4
                  Width = 118
                  Height = 39
                  Cursor = crHandPoint
                  Caption = '&Sa'#237'da'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtSaidaClick
                end
              end
              object BtTarefa: TBitBtn
                Left = 4
                Top = 4
                Width = 118
                Height = 39
                Cursor = crHandPoint
                Caption = '&Tarefa'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtTarefaClick
              end
              object BtLote: TBitBtn
                Left = 122
                Top = 4
                Width = 118
                Height = 39
                Cursor = crHandPoint
                Caption = '&Lote'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BtLoteClick
              end
              object BtProtocolo: TBitBtn
                Left = 240
                Top = 4
                Width = 118
                Height = 39
                Cursor = crHandPoint
                Caption = '&Protocolo'
                Enabled = False
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 3
                OnClick = BtProtocoloClick
              end
              object dmkEdX: TdmkEdit
                Left = 362
                Top = 20
                Width = 36
                Height = 20
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '1'
                ValMax = '100'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object DBGrid2: TDBGrid
            Left = 0
            Top = 266
            Width = 986
            Height = 270
            Align = alBottom
            DataSource = DsProtPakIts
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnColEnter = DBGrid2ColEnter
            OnColExit = DBGrid2ColExit
            OnDrawColumnCell = DBGrid2DrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'Conta'
                Title.Caption = 'Protocolo'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SERIE_DOCUM'
                Title.Caption = 'S'#233'rie / Documento'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Unidade'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VENCTO_TXT'
                Title.Caption = 'Vencto'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PERIODO_TXT'
                Title.Caption = 'Per'#237'odo'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DATAE_TXT'
                Title.Caption = 'Entrega'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataSai_TXT'
                Title.Caption = 'Saiu'
                Width = 110
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Retorna'
                ReadOnly = True
                Title.Caption = 'R?  Retorna?'
                Width = 14
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataRec_TXT'
                Title.Caption = 'Recebeu'
                Width = 110
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DATAD_TXT'
                Title.Caption = 'Retorno'
                Width = 94
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataRet_TXT'
                Title.Caption = 'Retornou'
                Width = 110
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEMANUAL'
                Title.Caption = 'Ocorr'#234'ncia Manual'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Texto'
                Title.Caption = 'Observa'#231#227'o'
                Width = 144
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECLI_INT'
                Title.Caption = 'Cliente interno'
                Width = 144
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECLIENTE'
                Title.Caption = 'Cliente'
                Width = 96
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CANCELADO_TXT'
                Title.Caption = 'Cancelado'
                Width = 27
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEMOTIVO'
                Title.Caption = 'Motivo do cancelamento'
                Width = 240
                Visible = True
              end>
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Pesquisa'
        ImageIndex = 2
        object Splitter1: TSplitter
          Left = 221
          Top = 0
          Width = 9
          Height = 599
        end
        object PnFast: TPanel
          Left = 0
          Top = 0
          Width = 221
          Height = 599
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 221
            Height = 27
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object SpeedButton5: TSpeedButton
              Left = 193
              Top = 4
              Width = 21
              Height = 21
              Caption = 'ok'
              OnClick = SpeedButton5Click
            end
            object EdCliInt: TEdit
              Left = 4
              Top = 4
              Width = 186
              Height = 21
              TabOrder = 0
            end
          end
          object dmkDBGrid1: TdmkDBGrid
            Left = 0
            Top = 27
            Width = 221
            Height = 572
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'CliInt'
                Title.Caption = 'Cli.int.'
                Width = 35
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'Entid.'
                Width = 35
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECLI'
                Title.Caption = 'Raz'#227'o Social / Nome'
                Width = 179
                Visible = True
              end>
            Color = clWindow
            DataSource = DsCliInt
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'CliInt'
                Title.Caption = 'Cli.int.'
                Width = 35
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'Entid.'
                Width = 35
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECLI'
                Title.Caption = 'Raz'#227'o Social / Nome'
                Width = 179
                Visible = True
              end>
          end
        end
        object Panel1: TPanel
          Left = 230
          Top = 0
          Width = 756
          Height = 599
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object dmkDBGrid2: TdmkDBGrid
            Left = 0
            Top = 27
            Width = 756
            Height = 572
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 278
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETIPO'
                Title.Caption = 'Tipo'
                Width = 171
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOME_SENDER'
                Title.Caption = 'Entregador'
                Width = 156
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEPREEMEIO'
                Title.Caption = 'Pr'#233'-email'
                Width = 218
                Visible = True
              end>
            Color = clWindow
            DataSource = DsCliProt
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = dmkDBGrid2DblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 278
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETIPO'
                Title.Caption = 'Tipo'
                Width = 171
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOME_SENDER'
                Title.Caption = 'Entregador'
                Width = 156
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEPREEMEIO'
                Title.Caption = 'Pr'#233'-email'
                Width = 218
                Visible = True
              end>
          end
          object Panel13: TPanel
            Left = 0
            Top = 0
            Width = 756
            Height = 27
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object EdCliProt: TEdit
              Left = 4
              Top = 4
              Width = 111
              Height = 21
              TabOrder = 0
              OnChange = EdCliProtChange
            end
            object CkSohAtivos: TCheckBox
              Left = 118
              Top = 8
              Width = 96
              Height = 17
              Caption = 'Somente ativos.'
              Checked = True
              State = cbChecked
              TabOrder = 1
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 993
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 946
      Top = 0
      Width = 47
      Height = 51
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 254
      Height = 51
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 86
        Top = 8
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 128
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 170
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object BtProtoGer: TBitBtn
        Tag = 10202
        Left = 210
        Top = 8
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtProtoGerClick
      end
    end
    object GB_M: TGroupBox
      Left = 254
      Top = 0
      Width = 692
      Height = 51
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 121
        Height = 31
        Caption = 'Protocolos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 121
        Height = 31
        Caption = 'Protocolos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 121
        Height = 31
        Caption = 'Protocolos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 51
    Width = 993
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel17: TPanel
      Left = 2
      Top = 14
      Width = 989
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 652
        Height = 16
        Caption = 
          'Na aba Pesquisa d'#234' um duplo clique na grade onde mostram as tare' +
          'fas dos protocolos para localiz'#225'-la.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 652
        Height = 16
        Caption = 
          'Na aba Pesquisa d'#234' um duplo clique na grade onde mostram as tare' +
          'fas dos protocolos para localiz'#225'-la.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsProtocolos: TDataSource
    DataSet = QrProtocolos
    Left = 448
    Top = 8
  end
  object QrProtocolos: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrProtocolosBeforeOpen
    AfterOpen = QrProtocolosAfterOpen
    BeforeClose = QrProtocolosBeforeClose
    AfterScroll = QrProtocolosAfterScroll
    OnCalcFields = QrProtocolosCalcFields
    SQL.Strings = (
      'SELECT IF(ptc.Tipo=0,"ND",'
      '  IF(ptc.Tipo=1,"Circula'#231#227'o de documentos",'
      '  IF(ptc.Tipo=2,"Circula'#231#227'o eletr'#244'nica",'
      '                "Entrega de material de consumo"))'
      '  ) NOMETIPO,'
      'IF(d_c.Tipo=0, d_c.RazaoSocial,d_c.Nome) NOME_CLIENT,'
      'IF(d_s.Tipo=0, d_s.RazaoSocial,d_s.Nome) NOME_SENDER,'
      'pre.Nome NOMEPREEMEIO, d_c.CliInt, ptc.* '
      'FROM protocolos ptc'
      'LEFT JOIN entidades d_c ON d_c.Codigo=ptc.Def_Client'
      'LEFT JOIN entidades d_s ON d_s.Codigo=ptc.Def_Sender'
      'LEFT JOIN preemail  pre ON pre.Codigo=ptc.PreEmeio'
      'WHERE ptc.Codigo > 0')
    Left = 420
    Top = 8
    object QrProtocolosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProtocolosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrProtocolosTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrProtocolosDef_Client: TIntegerField
      FieldName = 'Def_Client'
    end
    object QrProtocolosDef_Sender: TIntegerField
      FieldName = 'Def_Sender'
    end
    object QrProtocolosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrProtocolosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProtocolosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProtocolosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrProtocolosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrProtocolosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrProtocolosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrProtocolosNOMETIPO: TWideStringField
      FieldName = 'NOMETIPO'
      Required = True
      Size = 7
    end
    object QrProtocolosNOME_CLIENT: TWideStringField
      FieldName = 'NOME_CLIENT'
      Size = 100
    end
    object QrProtocolosNOME_SENDER: TWideStringField
      FieldName = 'NOME_SENDER'
      Size = 100
    end
    object QrProtocolosDef_Retorn: TIntegerField
      FieldName = 'Def_Retorn'
      Required = True
    end
    object QrProtocolosPreEmeio: TIntegerField
      FieldName = 'PreEmeio'
      Required = True
    end
    object QrProtocolosNOMEPREEMEIO: TWideStringField
      FieldName = 'NOMEPREEMEIO'
      Size = 100
    end
    object QrProtocolosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrProtocolosPRINT_DESCRI: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PRINT_DESCRI'
      Size = 100
      Calculated = True
    end
    object QrProtocolosSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrProtocolosCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object QrClient: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT ent.Codigo, IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOM' +
        'EENT'
      'FROM enticliint cli'
      'LEFT JOIN entidades ent ON ent.Codigo = cli.CodEnti '
      'ORDER BY NOMEENT')
    Left = 420
    Top = 40
    object QrClientCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsClient: TDataSource
    DataSet = QrClient
    Left = 448
    Top = 40
  end
  object QrSender: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 508
    Top = 40
    object QrSenderCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSenderNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsSender: TDataSource
    DataSet = QrSender
    Left = 536
    Top = 40
  end
  object QrProtocoPak: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrProtocoPakAfterOpen
    BeforeClose = QrProtocoPakBeforeClose
    AfterScroll = QrProtocoPakAfterScroll
    OnCalcFields = QrProtocoPakCalcFields
    SQL.Strings = (
      'SELECT ptp.*, '
      'CONCAT(RIGHT(Mez, 2), '#39'/'#39',  LEFT(Mez + 200000, 4)) MES,'
      
        'IF(ptp.DataF=0,"", DATE_FORMAT(ptp.DataF, "%d/%m/%y" )) DATAF_TX' +
        'T'
      'FROM protocopak ptp'
      'WHERE ptp.Codigo=:P0'
      'AND ptp.DataF>:P1'
      'ORDER BY DataL DESC, ptp.Controle DESC')
    Left = 508
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrProtocoPakCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProtocoPakControle: TIntegerField
      FieldName = 'Controle'
      DisplayFormat = '000000'
    end
    object QrProtocoPakDataI: TDateField
      FieldName = 'DataI'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrProtocoPakDataL: TDateField
      FieldName = 'DataL'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrProtocoPakDataF: TDateField
      FieldName = 'DataF'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrProtocoPakLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrProtocoPakDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProtocoPakDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProtocoPakUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrProtocoPakUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrProtocoPakAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrProtocoPakAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrProtocoPakMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrProtocoPakMES: TWideStringField
      FieldName = 'MES'
      Required = True
      Size = 7
    end
    object QrProtocoPakDATAF_TXT: TWideStringField
      FieldName = 'DATAF_TXT'
      Size = 8
    end
    object QrProtocoPakTITULO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TITULO'
      Size = 255
      Calculated = True
    end
    object QrProtocoPakCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
      Required = True
    end
    object QrProtocoPakSeqArq: TIntegerField
      FieldName = 'SeqArq'
      Required = True
      DisplayFormat = '0000000;-0000000; '
    end
    object QrProtocoPakEAN128: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EAN128'
      Calculated = True
    end
    object QrProtocoPakTITULO3: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TITULO3'
      Size = 255
      Calculated = True
    end
    object QrProtocoPakEAN128_3: TWideStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'EAN128_3'
      Calculated = True
    end
    object QrProtocoPakDATAL_TXT: TWideStringField
      FieldName = 'DATAL_TXT'
      Size = 8
    end
  end
  object DsProtocoPak: TDataSource
    DataSet = QrProtocoPak
    Left = 536
    Top = 8
  end
  object PMTarefa: TPopupMenu
    OnPopup = PMTarefaPopup
    Left = 557
    Top = 600
    object Crianovatarefa1: TMenuItem
      Caption = '&Cria nova tarefa'
      OnClick = Crianovatarefa1Click
    end
    object Alteratarefaatual1: TMenuItem
      Caption = '&Altera tarefa atual'
      OnClick = Alteratarefaatual1Click
    end
    object Excluitarefaatual1: TMenuItem
      Caption = '&Exclui tarefa atual'
      Enabled = False
    end
  end
  object PMLote: TPopupMenu
    OnPopup = PMLotePopup
    Left = 645
    Top = 601
    object Crianovolote1: TMenuItem
      Caption = '&Cria novo lote'
      OnClick = Crianovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&Altera lote atual'
      Enabled = False
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      Enabled = False
      OnClick = Excluiloteatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Imprimir1: TMenuItem
      Caption = '&Imprimir - Entrega de boletos - Modelo 1'
      object Ordenarporunidade1: TMenuItem
        Caption = 'Ordenar pela &Unidade'
        OnClick = Ordenarporunidade1Click
      end
      object OrdenarpeloNomedoProprietrio1: TMenuItem
        Caption = 'Ordenar pelo nome do &Propriet'#225'rio'
        OnClick = OrdenarpeloNomedoProprietrio1Click
      end
      object Ordenerpelarotadeentrega1: TMenuItem
        Caption = 'Ordenar pela &Rota de entrega'
        OnClick = Ordenerpelarotadeentrega1Click
      end
      object OrdenarpeloVencimento1: TMenuItem
        Caption = 'Ordenar pelo &Vencimento'
        OnClick = OrdenarpeloVencimento1Click
      end
      object OrdenarpeloProtocolo1: TMenuItem
        Caption = 'Ordenar pelo Pr&otocolo'
        OnClick = OrdenarpeloProtocolo1Click
      end
    end
    object ImprimirEntregadeboletosModelo21: TMenuItem
      Caption = '&Imprimir - Entrega de boletos - Modelo 2'
      object OrdenarpelaUnidade2: TMenuItem
        Caption = 'Ordenar pela &Unidade'
        OnClick = OrdenarpelaUnidade2Click
      end
      object OrdenarpelonomedoProprietrio3: TMenuItem
        Caption = 'Ordenar pelo nome do &Propriet'#225'rio'
        OnClick = OrdenarpelonomedoProprietrio3Click
      end
      object OrdenerpelaRotadeentrega3: TMenuItem
        Caption = 'Ordenar pela &Rota de entrega'
        OnClick = OrdenerpelaRotadeentrega3Click
      end
      object OrdenarpeloVencimento2: TMenuItem
        Caption = 'Ordenar pelo &Vencimento'
        OnClick = OrdenarpeloVencimento2Click
      end
      object OrdenarpeloProtocolo2: TMenuItem
        Caption = 'Ordenar pelo Pr&otocolo'
        OnClick = OrdenarpeloProtocolo2Click
      end
    end
    object ImprimirEntregadeboletosModelo31: TMenuItem
      Caption = '&Imprimir - Entrega de boletos - Modelo 3'
      object OrdenarpelaUnidade3: TMenuItem
        Caption = 'Ordenar pela &Unidade'
        OnClick = OrdenarpelaUnidade3Click
      end
      object OrdenarpelonomedoProprietrio4: TMenuItem
        Caption = 'Ordenar pelo nome do &Propriet'#225'rio'
        OnClick = OrdenarpelonomedoProprietrio4Click
      end
      object OrdenerpelaRotadeentrega4: TMenuItem
        Caption = 'Ordenar pela &Rota de entrega'
        OnClick = OrdenerpelaRotadeentrega4Click
      end
      object OrdenarpeloVencimento3: TMenuItem
        Caption = 'Ordenar pelo &Vencimento'
        OnClick = OrdenarpeloVencimento3Click
      end
      object OrdenarpeloProtocolo3: TMenuItem
        Caption = 'Ordenar pelo Pr&otocolo'
        OnClick = OrdenarpeloProtocolo3Click
      end
    end
    object ImprimirEntregadeboletosModelo41: TMenuItem
      Caption = '&Imprimir - Entrega de boletos - Modelo 4'
      object OrdernarpeloLote1: TMenuItem
        Caption = 'Ordernar pelo &Lote'
        OnClick = OrdernarpeloLote1Click
      end
      object OrdenarpelaUnidade4: TMenuItem
        Caption = 'Ordenar pela &Unidade'
        OnClick = OrdenarpelaUnidade4Click
      end
      object OrdenarpelonomedoProprietrio5: TMenuItem
        Caption = 'Ordenar pelo nome do &Propriet'#225'rio'
        OnClick = OrdenarpelonomedoProprietrio5Click
      end
      object OrdenerpelaRotadeentrega5: TMenuItem
        Caption = 'Ordenar pela &Rota de entrega'
        OnClick = OrdenerpelaRotadeentrega5Click
      end
      object OrdenarpeloVencimento4: TMenuItem
        Caption = 'Ordenar pelo &Vencimento'
        OnClick = OrdenarpeloVencimento4Click
      end
      object OrdenarpeloProtocolo4: TMenuItem
        Caption = 'Ordenar pelo Pr&otocolo'
        OnClick = OrdenarpeloProtocolo4Click
      end
    end
    object Circulaodedocumentos1: TMenuItem
      Caption = 'Imprimir - &Circula'#231#227'o de documentos'
      OnClick = Circulaodedocumentos1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object GeraarquivoremessaCNAB1: TMenuItem
      Caption = '&Gera arquivo remessa CNAB'
      OnClick = GeraarquivoremessaCNAB1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Zerarnmero1: TMenuItem
      Caption = 'Zerar o n'#250'mero sequencial de arquivo de remessa'
      OnClick = Zerarnmero1Click
    end
  end
  object PMProtocolo: TPopupMenu
    OnPopup = PMProtocoloPopup
    Left = 737
    Top = 605
    object DefinedatadaEntrega1: TMenuItem
      Caption = 'Define data da &Entrega'
      OnClick = DefinedatadaEntrega1Click
    end
    object DefinedatadoRetorno1: TMenuItem
      Caption = '&Define data do &Retorno'
      OnClick = DefinedatadoRetorno1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Cancelaentrega1: TMenuItem
      Caption = '&Cancela entrega'
      OnClick = Cancelaentrega1Click
    end
    object Excluiprotocolo1: TMenuItem
      Caption = 'E&xclui protocolo'
      OnClick = Excluiprotocolo1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Incluiprotocolomanual1: TMenuItem
      Caption = '&Inclui protocolo manual'
      OnClick = Incluiprotocolomanual1Click
    end
    object Alteraprotocolomanual1: TMenuItem
      Caption = '&Altera protocolo manual'
      Enabled = False
      OnClick = Alteraprotocolomanual1Click
    end
    object IncluiboletoscomregistroCR1: TMenuItem
      Caption = '&Inclui boletos com registro (CR)'
      Enabled = False
      OnClick = IncluiboletoscomregistroCR1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object IncluiProtocoloBases1: TMenuItem
      Caption = 'Inclui protocolos &Bases'
      OnClick = IncluiProtocoloBases1Click
    end
  end
  object QrProtPakIts_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cimv.Unidade, '
      'CONCAT(RIGHT(PERIOD_ADD(200000, ppi.Periodo), 2), "/", '
      '  LEFT(PERIOD_ADD(200000, ppi.Periodo), 4)) PERIODO_TXT,'
      'IF(clii.Tipo=0, clii.RazaoSocial, clii.Nome) NOMECLI_INT,'
      'IF(clie.Tipo=0, clie.RazaoSocial, clie.Nome) NOMECLIENTE,'
      'IF(clie.Tipo=0, clie.ERua, clie.PRua) ROTACLI,'
      'ptm.Nome NOMEMOTIVO, pto.Nome NOMEMANUAL, ppi.*,'
      'IF(ppi.Cancelado=0,"N'#195'O","SIM") CANCELADO_TXT, '
      
        'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%y")) DATAE_TX' +
        'T,'
      
        'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%y")) DATAD_TX' +
        'T,'
      
        'IF(ppi.Vencto=0, "", DATE_FORMAT(ppi.Vencto, "%d/%m/%y")) VENCTO' +
        '_TXT'
      'FROM protpakits ppi'
      'LEFT JOIN protocomot ptm ON ptm.Codigo=ppi.Motivo'
      'LEFT JOIN protocooco pto ON pto.Codigo=ppi.Manual'
      'LEFT JOIN entidades clii ON clii.Codigo=ppi.CliInt'
      'LEFT JOIN entidades clie ON clie.Codigo=ppi.Cliente'
      'LEFT JOIN condimov  cimv ON cimv.Conta=ppi.Depto'
      'WHERE ppi.Controle=:P0'
      'ORDER BY Conta DESC'
      ''
      '')
    Left = 596
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProtPakIts_Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'protpakits.Codigo'
    end
    object QrProtPakIts_Controle: TIntegerField
      FieldName = 'Controle'
      Origin = 'protpakits.Controle'
    end
    object QrProtPakIts_Conta: TIntegerField
      FieldName = 'Conta'
      Origin = 'protpakits.Conta'
    end
    object QrProtPakIts_DataE: TDateField
      FieldName = 'DataE'
      Origin = 'protpakits.DataE'
    end
    object QrProtPakIts_DataD: TDateField
      FieldName = 'DataD'
      Origin = 'protpakits.DataD'
    end
    object QrProtPakIts_Cancelado: TIntegerField
      FieldName = 'Cancelado'
      Origin = 'protpakits.Cancelado'
    end
    object QrProtPakIts_Motivo: TIntegerField
      FieldName = 'Motivo'
      Origin = 'protpakits.Motivo'
    end
    object QrProtPakIts_Lk: TIntegerField
      FieldName = 'Lk'
      Origin = 'protpakits.Lk'
    end
    object QrProtPakIts_DataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'protpakits.DataCad'
    end
    object QrProtPakIts_DataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'protpakits.DataAlt'
    end
    object QrProtPakIts_UserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'protpakits.UserCad'
    end
    object QrProtPakIts_UserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'protpakits.UserAlt'
    end
    object QrProtPakIts_AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'protpakits.AlterWeb'
    end
    object QrProtPakIts_Ativo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'protpakits.Ativo'
    end
    object QrProtPakIts_Link_ID: TIntegerField
      FieldName = 'Link_ID'
      Origin = 'protpakits.Link_ID'
      Required = True
    end
    object QrProtPakIts_ID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
      Origin = 'protpakits.ID_Cod1'
    end
    object QrProtPakIts_ID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
      Origin = 'protpakits.ID_Cod2'
    end
    object QrProtPakIts_ID_Cod3: TIntegerField
      FieldName = 'ID_Cod3'
      Origin = 'protpakits.ID_Cod3'
    end
    object QrProtPakIts_ID_Cod4: TIntegerField
      FieldName = 'ID_Cod4'
      Origin = 'protpakits.ID_Cod4'
    end
    object QrProtPakIts_DATAE_TXT: TWideStringField
      FieldName = 'DATAE_TXT'
      Size = 10
    end
    object QrProtPakIts_DATAD_TXT: TWideStringField
      FieldName = 'DATAD_TXT'
      Size = 10
    end
    object QrProtPakIts_NOMEMOTIVO: TWideStringField
      FieldName = 'NOMEMOTIVO'
      Origin = 'protocomot.Nome'
      Size = 50
    end
    object QrProtPakIts_CANCELADO_TXT: TWideStringField
      FieldName = 'CANCELADO_TXT'
      Required = True
      Size = 3
    end
    object QrProtPakIts_Unidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'condimov.Unidade'
      Size = 10
    end
    object QrProtPakIts_NOMECLI_INT: TWideStringField
      FieldName = 'NOMECLI_INT'
      Size = 100
    end
    object QrProtPakIts_NOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrProtPakIts_CliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'protpakits.CliInt'
      Required = True
    end
    object QrProtPakIts_Cliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'protpakits.Cliente'
      Required = True
    end
    object QrProtPakIts_Fornece: TIntegerField
      FieldName = 'Fornece'
      Origin = 'protpakits.Fornece'
      Required = True
    end
    object QrProtPakIts_Periodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'protpakits.Periodo'
      Required = True
    end
    object QrProtPakIts_Lancto: TIntegerField
      FieldName = 'Lancto'
      Origin = 'protpakits.Lancto'
      Required = True
    end
    object QrProtPakIts_Depto: TIntegerField
      FieldName = 'Depto'
      Origin = 'protpakits.Depto'
      Required = True
    end
    object QrProtPakIts_PERIODO_TXT: TWideStringField
      FieldName = 'PERIODO_TXT'
      Required = True
      Size = 7
    end
    object QrProtPakIts_LN2: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LN2'
      LookupDataSet = QrPropriet
      LookupKeyFields = 'Codigo'
      LookupResultField = 'LN2'
      KeyFields = 'Cliente'
      Size = 255
      Lookup = True
    end
    object QrProtPakIts_LNR: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LNR'
      LookupDataSet = QrPropriet
      LookupKeyFields = 'Codigo'
      LookupResultField = 'LNR'
      KeyFields = 'Cliente'
      Size = 255
      Lookup = True
    end
    object QrProtPakIts_Retorna: TSmallintField
      FieldName = 'Retorna'
      Origin = 'protpakits.Retorna'
      Required = True
    end
    object QrProtPakIts_Valor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrProtPakIts_Vencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrProtPakIts_VENCTO_TXT: TWideStringField
      FieldName = 'VENCTO_TXT'
      Size = 10
    end
    object QrProtPakIts_MoraDiaVal: TFloatField
      FieldName = 'MoraDiaVal'
      Required = True
    end
    object QrProtPakIts_MultaVal: TFloatField
      FieldName = 'MultaVal'
      Required = True
    end
    object QrProtPakIts_Cedente: TIntegerField
      FieldName = 'Cedente'
      Required = True
    end
    object QrProtPakIts_Docum: TFloatField
      FieldName = 'Docum'
      Required = True
    end
    object QrProtPakIts_ComoConf: TSmallintField
      FieldName = 'ComoConf'
      Required = True
    end
    object QrProtPakIts_SerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrProtPakIts_Manual: TIntegerField
      FieldName = 'Manual'
    end
    object QrProtPakIts_Texto: TWideStringField
      FieldName = 'Texto'
      Size = 30
    end
    object QrProtPakIts_NOMEMANUAL: TWideStringField
      FieldName = 'NOMEMANUAL'
      Size = 50
    end
    object QrProtPakIts_Retornou: TIntegerField
      FieldName = 'Retornou'
    end
    object QrProtPakIts_Saiu: TIntegerField
      FieldName = 'Saiu'
    end
    object QrProtPakIts_Recebeu: TIntegerField
      FieldName = 'Recebeu'
    end
    object QrProtPakIts_DataSai: TDateTimeField
      FieldName = 'DataSai'
    end
    object QrProtPakIts_DataRec: TDateTimeField
      FieldName = 'DataRec'
    end
    object QrProtPakIts_DataRet: TDateTimeField
      FieldName = 'DataRet'
    end
    object QrProtPakIts_LimiteSai: TDateField
      FieldName = 'LimiteSai'
    end
    object QrProtPakIts_LimiteRem: TDateField
      FieldName = 'LimiteRem'
    end
    object QrProtPakIts_LimiteRet: TDateField
      FieldName = 'LimiteRet'
    end
    object QrProtPakIts_ROTACLI: TWideStringField
      FieldName = 'ROTACLI'
      Size = 30
    end
  end
  object DsProtPakIts: TDataSource
    DataSet = QrProtPakIts
    Left = 624
    Top = 8
  end
  object frxLote_Unidade_: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 680
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProtocolos
        DataSetName = 'frxDsProtocolos'
      end
      item
        DataSet = frxDsProtocoPak
        DataSetName = 'frxDsProtocoPak'
      end
      item
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 90.708720000000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
        RowCount = 0
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 7.559059999999988000
          Width = 680.315400000000000000
          Height = 83.149660000000000000
          Frame.Typ = []
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 7.559059999999988000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."Unidade"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 7.559059999999988000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtPakIts."NOMECLIENTE"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 26.456709999999990000
          Width = 396.850650000000000000
          Height = 37.795275590000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtPakIts."LNR"] - [frxDsProtPakIts."LN2"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 7.559059999999988000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 7.559059999999988000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 7.559059999999988000
          Width = 105.826840000000000000
          Height = 56.692950000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 26.456709999999990000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 26.456709999999990000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 45.354359999999990000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 45.354359999999990000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 64.252009999999990000
          Width = 396.850650000000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 64.252009999999990000
          Width = 283.464750000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ass.:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 7.559059999999988000
          Width = 26.456710000000000000
          Height = 83.149660000000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."Conta"]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 226.771787800000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 37.795300000000000000
          Width = 706.772110000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Top = 37.795300000000000000
          Width = 695.433520000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 56.692949999999990000
          Width = 411.968770000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LOTE DE PROTOCOLOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Top = 56.692949999999990000
          Width = 139.842610000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 83.149660000000000000
          Width = 706.772110000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."NOME_SENDER"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 109.606370000000000000
          Width = 706.772110000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."Nome"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 132.283550000000000000
          Width = 706.772110000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocoPak."TITULO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 170.078850000000000000
          Width = 94.488250000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 188.976500000000000000
          Width = 396.850650000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Endere'#231'o de entrega')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 170.078850000000000000
          Width = 302.362400000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do propriet'#225'rio')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 170.078850000000000000
          Width = 177.637848980000000000
          Height = 37.795275590551200000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '3 Tentativas:'
            'data e hora')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 170.078850000000000000
          Width = 105.826840000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Motivo de n'#227'o'
            'entrega / rubrica')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 207.874150000000000000
          Width = 396.850650000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome leg'#237'vel de quem recebeu')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 207.874150000000000000
          Width = 283.464750000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Assinatura de quem recebeu')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 170.078850000000000000
          Width = 26.456710000000000000
          Height = 56.692950000000010000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 457.323130000000000000
        Width = 718.110700000000000000
      end
    end
  end
  object frxDsProtPakIts: TfrxDBDataset
    UserName = 'frxDsProtPakIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Conta=Conta'
      'DataE=DataE'
      'DataD=DataD'
      'Cancelado=Cancelado'
      'Motivo=Motivo'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'Link_ID=Link_ID'
      'ID_Cod1=ID_Cod1'
      'ID_Cod2=ID_Cod2'
      'ID_Cod3=ID_Cod3'
      'ID_Cod4=ID_Cod4'
      'DATAE_TXT=DATAE_TXT'
      'DATAD_TXT=DATAD_TXT'
      'NOMEMOTIVO=NOMEMOTIVO'
      'CANCELADO_TXT=CANCELADO_TXT'
      'Unidade=Unidade'
      'NOMECLI_INT=NOMECLI_INT'
      'NOMECLIENTE=NOMECLIENTE'
      'CliInt=CliInt'
      'Cliente=Cliente'
      'Fornece=Fornece'
      'Periodo=Periodo'
      'Lancto=Lancto'
      'Depto=Depto'
      'PERIODO_TXT=PERIODO_TXT'
      'Retorna=Retorna'
      'Valor=Valor'
      'Vencto=Vencto'
      'VENCTO_TXT=VENCTO_TXT'
      'MoraDiaVal=MoraDiaVal'
      'MultaVal=MultaVal'
      'Cedente=Cedente'
      'Docum=Docum'
      'ComoConf=ComoConf'
      'SerieCH=SerieCH'
      'Manual=Manual'
      'Texto=Texto'
      'NOMEMANUAL=NOMEMANUAL'
      'Retornou=Retornou'
      'Saiu=Saiu'
      'Recebeu=Recebeu'
      'DataSai=DataSai'
      'DataRec=DataRec'
      'DataRet=DataRet'
      'LimiteSai=LimiteSai'
      'LimiteRem=LimiteRem'
      'LimiteRet=LimiteRet'
      'ROTACLI=ROTACLI'
      'SERIE_DOCUM=SERIE_DOCUM'
      'LN2=LN2'
      'LNR=LNR')
    DataSet = QrProtPakIts
    BCDToCurrency = False
    
    Left = 652
    Top = 8
  end
  object QrPropriet: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrProprietCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, '
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEDONO, '
      'IF(en.Tipo=0, en.CNPJ   , en.CPF     ) CNPJ_CPF, '
      'IF(en.Tipo=0, en.IE     , en.RG      ) IE_RG, '
      'IF(en.Tipo=0, en.NIRE   , ""         ) NIRE_, '
      'IF(en.Tipo=0, en.ERua   , en.PRua    ) RUA, '
      'IF(en.Tipo=0, en.ENumero, en.PNumero) + 0.000 NUMERO, '
      'IF(en.Tipo=0, en.ECompl , en.PCompl  ) COMPL, '
      'IF(en.Tipo=0, en.EBairro, en.PBairro ) BAIRRO, '
      'IF(en.Tipo=0, en.ECidade, en.PCidade ) CIDADE, '
      'IF(en.Tipo=0, lle.Nome  , llp.Nome   ) NOMELOGRAD, '
      'IF(en.Tipo=0, ufe.Nome  , ufp.Nome   ) NOMEUF, '
      'IF(en.Tipo=0, en.EPais  , en.PPais   ) Pais,'
      'IF(en.Tipo=0, en.ELograd, en.PLograd ) + 0.000 Lograd,'
      'IF(en.Tipo=0, en.ECEP   , en.PCEP    ) + 0.000 CEP,'
      'IF(en.Tipo=0, en.ETe1   , en.PTe1    ) TE1, '
      'IF(en.Tipo=0, en.EFax   , en.PFax    ) FAX '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Cliente2="V"'
      'AND en.Codigo=:P0')
    Left = 764
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProprietCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrProprietCadastro: TDateField
      FieldName = 'Cadastro'
      Origin = 'entidades.Cadastro'
    end
    object QrProprietENatal: TDateField
      FieldName = 'ENatal'
      Origin = 'entidades.ENatal'
    end
    object QrProprietPNatal: TDateField
      FieldName = 'PNatal'
      Origin = 'entidades.PNatal'
    end
    object QrProprietTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrProprietRespons1: TWideStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Required = True
      Size = 60
    end
    object QrProprietNOMEDONO: TWideStringField
      FieldName = 'NOMEDONO'
      Size = 100
    end
    object QrProprietCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrProprietIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrProprietNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrProprietRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrProprietCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrProprietBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrProprietCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrProprietNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrProprietNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrProprietPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrProprietTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrProprietFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrProprietNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 50
      Calculated = True
    end
    object QrProprietLNR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LNR'
      Size = 255
      Calculated = True
    end
    object QrProprietLN2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LN2'
      Size = 255
      Calculated = True
    end
    object QrProprietNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrProprietLograd: TFloatField
      FieldName = 'Lograd'
      Required = True
    end
    object QrProprietCEP: TFloatField
      FieldName = 'CEP'
    end
  end
  object frxDsProtocoPak: TfrxDBDataset
    UserName = 'frxDsProtocoPak'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'DataI=DataI'
      'DataL=DataL'
      'DataF=DataF'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'Mez=Mez'
      'MES=MES'
      'DATAF_TXT=DATAF_TXT'
      'TITULO=TITULO'
      'CNAB_Cfg=CNAB_Cfg'
      'SeqArq=SeqArq'
      'EAN128=EAN128'
      'TITULO3=TITULO3'
      'EAN128_3=EAN128_3')
    DataSet = QrProtocoPak
    BCDToCurrency = False
    
    Left = 564
    Top = 8
  end
  object frxDsProtocolos: TfrxDBDataset
    UserName = 'frxDsProtocolos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome'
      'Tipo=Tipo'
      'Def_Client=Def_Client'
      'Def_Sender=Def_Sender'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NOMETIPO=NOMETIPO'
      'NOME_CLIENT=NOME_CLIENT'
      'NOME_SENDER=NOME_SENDER'
      'Def_Retorn=Def_Retorn'
      'PreEmeio=PreEmeio'
      'NOMEPREEMEIO=NOMEPREEMEIO'
      'Descricao=Descricao'
      'PRINT_DESCRI=PRINT_DESCRI')
    DataSet = QrProtocolos
    BCDToCurrency = False
    
    Left = 476
    Top = 8
  end
  object QrAbertos: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrAbertosAfterOpen
    BeforeClose = QrAbertosBeforeClose
    SQL.Strings = (
      'SELECT ptc.Nome NOMEPROTOCOLO, cimv.Unidade, '
      'CONCAT(RIGHT(PERIOD_ADD(200000, ppi.Periodo), 2), "/", '
      '  LEFT(PERIOD_ADD(200000, ppi.Periodo), 4)) PERIODO_TXT,'
      'IF(clii.Tipo=0, clii.RazaoSocial, clii.Nome) NOMECLI_INT,'
      'IF(clie.Tipo=0, clie.RazaoSocial, clie.Nome) NOMECLIENTE,'
      'ptm.Nome NOMEMOTIVO, ppi.*,'
      'IF(ppi.Cancelado=0,"N'#195'O","SIM") CANCELADO_TXT, '
      
        'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DATAE_TX' +
        'T,'
      
        'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DATAD_TX' +
        'T'
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      '/*LEFT JOIN protocopak ptp ON ptp.Controle=ppi.Controle*/'
      'LEFT JOIN protocomot ptm ON ptm.Codigo=ppi.Motivo'
      'LEFT JOIN entidades clii ON clii.Codigo=ppi.CliInt'
      'LEFT JOIN entidades clie ON clie.Codigo=ppi.Cliente'
      'LEFT JOIN condimov  cimv ON cimv.Conta=ppi.Depto'
      'WHERE (DataE=0) '
      'OR ((DataD=0) AND (Retorna>0)) ')
    Left = 792
    Top = 8
    object QrAbertosNOMEPROTOCOLO: TWideStringField
      FieldName = 'NOMEPROTOCOLO'
      Size = 100
    end
    object QrAbertosUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrAbertosPERIODO_TXT: TWideStringField
      FieldName = 'PERIODO_TXT'
      Required = True
      Size = 7
    end
    object QrAbertosNOMECLI_INT: TWideStringField
      FieldName = 'NOMECLI_INT'
      Size = 100
    end
    object QrAbertosNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrAbertosNOMEMOTIVO: TWideStringField
      FieldName = 'NOMEMOTIVO'
      Size = 50
    end
    object QrAbertosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
      DisplayFormat = '000'
    end
    object QrAbertosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
      DisplayFormat = '000000'
    end
    object QrAbertosConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrAbertosDataE: TDateField
      FieldName = 'DataE'
      Required = True
    end
    object QrAbertosDataD: TDateField
      FieldName = 'DataD'
      Required = True
    end
    object QrAbertosRetorna: TSmallintField
      FieldName = 'Retorna'
      Required = True
      MaxValue = 1
    end
    object QrAbertosCancelado: TIntegerField
      FieldName = 'Cancelado'
      Required = True
    end
    object QrAbertosMotivo: TIntegerField
      FieldName = 'Motivo'
      Required = True
    end
    object QrAbertosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrAbertosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrAbertosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAbertosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAbertosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAbertosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrAbertosAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrAbertosID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
      Required = True
    end
    object QrAbertosID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
      Required = True
    end
    object QrAbertosID_Cod3: TIntegerField
      FieldName = 'ID_Cod3'
      Required = True
    end
    object QrAbertosID_Cod4: TIntegerField
      FieldName = 'ID_Cod4'
      Required = True
    end
    object QrAbertosLink_ID: TIntegerField
      FieldName = 'Link_ID'
      Required = True
    end
    object QrAbertosCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrAbertosCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrAbertosFornece: TIntegerField
      FieldName = 'Fornece'
      Required = True
    end
    object QrAbertosPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrAbertosLancto: TIntegerField
      FieldName = 'Lancto'
      Required = True
    end
    object QrAbertosDepto: TIntegerField
      FieldName = 'Depto'
      Required = True
    end
    object QrAbertosCANCELADO_TXT: TWideStringField
      FieldName = 'CANCELADO_TXT'
      Required = True
      Size = 3
    end
    object QrAbertosDATAE_TXT: TWideStringField
      FieldName = 'DATAE_TXT'
      Size = 10
    end
    object QrAbertosDATAD_TXT: TWideStringField
      FieldName = 'DATAD_TXT'
      Size = 10
    end
    object QrAbertosDocum: TFloatField
      FieldName = 'Docum'
      Required = True
    end
  end
  object DsAbertos: TDataSource
    DataSet = QrAbertos
    Left = 820
    Top = 8
  end
  object PMProtocolo2: TPopupMenu
    Left = 49
    Top = 425
    object MenuItem1: TMenuItem
      Caption = 'Define data da &Entrega'
      OnClick = MenuItem1Click
    end
    object MenuItem2: TMenuItem
      Caption = '&Define data do &Retorno'
      OnClick = MenuItem2Click
    end
    object MenuItem3: TMenuItem
      Caption = '-'
    end
    object MenuItem4: TMenuItem
      Caption = '&Cancela entrega'
      OnClick = MenuItem4Click
    end
    object MenuItem5: TMenuItem
      Caption = 'E&xclui protocolo'
      OnClick = MenuItem5Click
    end
  end
  object QrLocPak: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ppi.Codigo, ppi.CliInt'
      'FROM protpakits ppi'
      'WHERE ppi.Controle=:P0'
      '')
    Left = 372
    Top = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPakCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocPakCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object QrSeqArq: TMySQLQuery
    Database = Dmod.MyDB
    Left = 400
    Top = 228
  end
  object QrPreEmail: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM preemail'
      'ORDER BY Nome')
    Left = 476
    Top = 240
    object QrPreEmailCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreEmailNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPreEmail: TDataSource
    DataSet = QrPreEmail
    Left = 504
    Top = 240
  end
  object frxLote_Circula1L1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 852
    Top = 324
    Datasets = <
      item
        DataSet = frxDsCircula
        DataSetName = 'frxDsCircula'
      end
      item
        DataSet = frxDsCirculaIts
        DataSetName = 'frxDsCirculaIts'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProtocolos
        DataSetName = 'frxDsProtocolos'
      end
      item
        DataSet = frxDsProtocoPak
        DataSetName = 'frxDsProtocoPak'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015760240000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCircula
        DataSetName = 'frxDsCircula'
        RowCount = 0
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DataSet = frxDsCircula
          DataSetName = 'frxDsCircula'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              '[frxDsCircula."SerieCH"]  [frxDsCircula."Docum"]  [frxDsCircula.' +
              '"NOMEOCORMAN"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Protocolo')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 18.897650000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Retorna?')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Lan'#231'amento')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 18.897650000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Top = 18.897650000000000000
          Width = 219.212740000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 18.897650000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCircula
          DataSetName = 'frxDsCircula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '>')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Top = 18.897650000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '<')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921460000000000000
          Top = 18.897650000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Top = 18.897650000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 158.740247800000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LOTE DE PROTOCOLOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."NOME_SENDER"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 71.811070000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."Nome"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 132.283550000000000000
          Width = 207.874150000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie / Documento')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Top = 132.283550000000000000
          Width = 472.441250000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ocorr'#234'ncia')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 94.488250000000000000
          Width = 453.543600000000000000
          Height = 37.795300000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProtocoPak."TITULO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode2: TfrxBarCodeView
          AllowVectorExport = True
          Top = 102.047310000000000000
          Width = 178.000000000000000000
          Height = 22.677180000000000000
          BarType = bcCodeEAN128B
          DataField = 'EAN128'
          DataSet = frxDsProtocoPak
          DataSetName = 'frxDsProtocoPak'
          Frame.Typ = []
          Rotation = 0
          ShowText = False
          TestLine = False
          Text = 'L00012345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          ColorBar = clBlack
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 102.047310000000000000
        Top = 370.393940000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 83.149660000000000000
          Frame.Typ = []
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 396.850650000000000000
          Height = 56.692925590000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 18.897650000000000000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 18.897650000000000000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 37.795300000000000000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 37.795300000000000000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 56.692949999999990000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 56.692949999999990000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590600000000000000
          Width = 396.850650000000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 75.590600000000000000
          Width = 283.464750000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ass.:')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCirculaIts
        DataSetName = 'frxDsCirculaIts'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Conta'
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCirculaIts."Conta"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataField = 'RETORNA_TXT'
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCirculaIts."RETORNA_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Lancto'
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCirculaIts."Lancto"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCirculaIts."Valor"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCirculaIts."VENCTO_TXT"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Width = 219.212740000000000000
          Height = 15.118110240000000000
          DataField = 'TEXTO'
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCirculaIts."TEXTO"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921460000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DataField = 'UNIDADE'
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCirculaIts."UNIDADE"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataField = 'MesAno'
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCirculaIts."MesAno"]')
          ParentFont = False
        end
      end
    end
  end
  object QrCircula: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCirculaAfterScroll
    SQL.Strings = (
      'SELECT ppi.Manual, ppi.SerieCH, ppi.Docum, '
      'pto.Nome NOMEOCORMAN'
      'FROM protpakits ppi'
      'LEFT JOIN protocooco pto ON pto.Codigo=ppi.Manual'
      'WHERE ppi.Controle=:P0'
      'GROUP BY ppi.Manual, ppi.SerieCH, ppi.Docum'
      'ORDER BY ppi.Manual, ppi.SerieCH, ppi.Docum')
    Left = 852
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCirculaManual: TIntegerField
      FieldName = 'Manual'
    end
    object QrCirculaSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrCirculaDocum: TFloatField
      FieldName = 'Docum'
      Required = True
    end
    object QrCirculaNOMEOCORMAN: TWideStringField
      FieldName = 'NOMEOCORMAN'
      Size = 50
    end
  end
  object frxDsCircula: TfrxDBDataset
    UserName = 'frxDsCircula'
    CloseDataSource = False
    DataSet = QrCircula
    BCDToCurrency = False
    
    Left = 852
    Top = 192
  end
  object QrCirculaIts: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCirculaItsCalcFields
    Left = 852
    Top = 236
    object QrCirculaItsConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrCirculaItsRETORNA_TXT: TWideStringField
      FieldName = 'RETORNA_TXT'
      Required = True
      Size = 3
    end
    object QrCirculaItsLancto: TIntegerField
      FieldName = 'Lancto'
      Required = True
    end
    object QrCirculaItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrCirculaItsTEXTO: TWideStringField
      FieldName = 'TEXTO'
      Size = 100
    end
    object QrCirculaItsVENCTO_TXT: TWideStringField
      FieldName = 'VENCTO_TXT'
      Size = 10
    end
    object QrCirculaItsMesAno: TWideStringField
      FieldName = 'MesAno'
      Size = 7
    end
    object QrCirculaItsUNIDADE: TWideStringField
      FieldName = 'UNIDADE'
      Size = 10
    end
    object QrCirculaItsEAN128: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EAN128'
      Calculated = True
    end
  end
  object frxDsCirculaIts: TfrxDBDataset
    UserName = 'frxDsCirculaIts'
    CloseDataSource = False
    DataSet = QrCirculaIts
    BCDToCurrency = False
    
    Left = 852
    Top = 280
  end
  object PMImprime: TPopupMenu
    Left = 28
    Top = 52
    object EntregadeBloquetos2: TMenuItem
      Caption = 'Entrega de &Bloquetos'
      object OrdenarpelaUnidade1: TMenuItem
        Caption = 'Ordenar pela &Unidade'
        OnClick = OrdenarpelaUnidade1Click
      end
      object OrdenarpelonomedoProprietrio2: TMenuItem
        Caption = 'Ordenar pelo nome do &Propriet'#225'rio'
        OnClick = OrdenarpelonomedoProprietrio2Click
      end
      object OrdenerpelaRotadeentrega2: TMenuItem
        Caption = 'Ordener pela &Rota de entrega'
        OnClick = OrdenerpelaRotadeentrega2Click
      end
    end
    object Circulaodedocumentos2: TMenuItem
      Caption = '&Circula'#231#227'o de documentos'
      OnClick = Circulaodedocumentos2Click
    end
  end
  object QrCliInt: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCliIntAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo, ent.CliInt,'
      'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMECLI'
      'FROM entidades ent'
      'LEFT JOIN protocolos pro ON pro.Def_Client=ent.Codigo'
      'WHERE ent.CliInt<>0'
      'OR pro.Def_Client <> 0')
    Left = 88
    Top = 144
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
    object QrCliIntCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 116
    Top = 144
  end
  object QrCliProt: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCliProtAfterScroll
    SQL.Strings = (
      'SELECT IF(ptc.Tipo=0,"ND",'
      '  IF(ptc.Tipo=1,"Circula'#231#227'o de documentos",'
      '  IF(ptc.Tipo=2,"Circula'#231#227'o eletr'#244'nica",'
      '                "Entrega de material de consumo"))'
      '  ) NOMETIPO,'
      'pre.Nome NOMEPREEMEIO, ptc.*, '
      
        'IF(ptc.Def_Sender=0,"",IF(d_s.Tipo=0, d_s.RazaoSocial,d_s.Nome))' +
        ' NOME_SENDER'
      'FROM protocolos ptc'
      'LEFT JOIN entidades d_s ON d_s.Codigo=ptc.Def_Sender'
      'LEFT JOIN preemail  pre ON pre.Codigo=ptc.PreEmeio'
      'WHERE ptc.Def_Client=:P0')
    Left = 88
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliProtNOMETIPO: TWideStringField
      FieldName = 'NOMETIPO'
      Required = True
      Size = 30
    end
    object QrCliProtNOMEPREEMEIO: TWideStringField
      FieldName = 'NOMEPREEMEIO'
      Size = 100
    end
    object QrCliProtCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliProtNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCliProtTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCliProtDef_Client: TIntegerField
      FieldName = 'Def_Client'
    end
    object QrCliProtDef_Sender: TIntegerField
      FieldName = 'Def_Sender'
    end
    object QrCliProtLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCliProtDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCliProtDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCliProtUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCliProtUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCliProtAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCliProtAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCliProtDef_Retorn: TIntegerField
      FieldName = 'Def_Retorn'
    end
    object QrCliProtPreEmeio: TIntegerField
      FieldName = 'PreEmeio'
    end
    object QrCliProtNOME_SENDER: TWideStringField
      FieldName = 'NOME_SENDER'
      Size = 100
    end
  end
  object DsCliProt: TDataSource
    DataSet = QrCliProt
    Left = 116
    Top = 172
  end
  object QrCircul2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 548
    Top = 108
  end
  object frxDsCircul2: TfrxDBDataset
    UserName = 'frxDsCircul2'
    CloseDataSource = False
    DataSet = QrCircul2
    BCDToCurrency = False
    
    Left = 576
    Top = 108
  end
  object frxLote_CirculaG: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 40348.463415219900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 604
    Top = 108
    Datasets = <
      item
        DataSet = frxDsCircul2
        DataSetName = 'frxDsCircul2'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProtocolos
        DataSetName = 'frxDsProtocolos'
      end
      item
        DataSet = frxDsProtocoPak
        DataSetName = 'frxDsProtocoPak'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCircul2
        DataSetName = 'frxDsCircul2'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsCircul2
          DataSetName = 'frxDsCircul2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000;???'#39', <frxDsCircul2."Conta">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataField = 'RETORNA_TXT'
          DataSet = frxDsCircul2
          DataSetName = 'frxDsCircul2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCircul2."RETORNA_TXT"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsCircul2
          DataSetName = 'frxDsCircul2'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCircul2."Valor"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700990000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DataField = 'VENCTO_TXT'
          DataSet = frxDsCircul2
          DataSetName = 'frxDsCircul2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCircul2."VENCTO_TXT"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Width = 215.433210000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCircul2
          DataSetName = 'frxDsCircul2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCircul2."TEXTO"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataField = 'MesAno'
          DataSet = frxDsCircul2
          DataSetName = 'frxDsCircul2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCircul2."MesAno"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Width = 136.063040940000000000
          Height = 15.118110240000000000
          DataField = 'NOMEOCORMAN'
          DataSet = frxDsCircul2
          DataSetName = 'frxDsCircul2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCircul2."NOMEOCORMAN"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 147.401660240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LOTE DE PROTOCOLOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."NOME_SENDER"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 71.811070000000000000
          Width = 680.315400000000000000
          Height = 20.787401574803150000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."Nome"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 113.385900000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocoPak."TITULO"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Top = 132.283550000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Top = 132.283550000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Retorna?')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 132.283550000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 313.700990000000000000
          Top = 132.283550000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 132.283550000000000000
          Width = 215.433210000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 132.283550000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '>')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Top = 132.283550000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '<')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 132.283550000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 132.283550000000000000
          Width = 136.063040940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ocorr'#234'ncia')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Top = 92.598425196850380000
          Width = 680.315400000000000000
          Height = 20.787401570000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."NOME_CLIENT"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456692910000000000
        Top = 427.086890000000000000
        Width = 680.315400000000000000
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 102.047310000000000000
        Top = 302.362400000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 83.149660000000000000
          Frame.Typ = []
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 396.850650000000000000
          Height = 56.692925590000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 18.897650000000000000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 18.897650000000000000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 105.826840000000000000
          Height = 56.692950000000010000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 37.795300000000000000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 37.795300000000000000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 56.692949999999990000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 56.692949999999990000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590600000000000000
          Width = 396.850650000000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 75.590600000000000000
          Width = 283.464750000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ass.:')
          ParentFont = False
        end
      end
    end
  end
  object frxLote_Entidade_: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 40347.632889143490000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 708
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProtocolos
        DataSetName = 'frxDsProtocolos'
      end
      item
        DataSet = frxDsProtocoPak
        DataSetName = 'frxDsProtocoPak'
      end
      item
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 90.708720000000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
        RowCount = 0
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 7.559059999999988000
          Width = 680.315400000000000000
          Height = 83.149660000000000000
          Frame.Typ = []
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 7.559059999999988000
          Width = 396.850650000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtPakIts."NOMECLIENTE"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 26.456709999999990000
          Width = 396.850650000000000000
          Height = 37.795275590000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtPakIts."LNR"] - [frxDsProtPakIts."LN2"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 7.559059999999988000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 7.559059999999988000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 7.559059999999988000
          Width = 105.826840000000000000
          Height = 56.692950000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 26.456709999999990000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 26.456709999999990000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 45.354359999999990000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 45.354359999999990000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 64.252009999999990000
          Width = 396.850650000000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 64.252009999999990000
          Width = 283.464750000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ass.:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 7.559059999999988000
          Width = 26.456710000000000000
          Height = 83.149660000000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."Conta"]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 226.771787800000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 37.795300000000000000
          Width = 706.772110000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Top = 37.795300000000000000
          Width = 695.433520000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 15.118110240000000000
          Top = 56.692949999999990000
          Width = 695.433520000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 56.692949999999990000
          Width = 411.968770000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LOTE DE PROTOCOLOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Top = 56.692949999999990000
          Width = 139.842610000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 83.149660000000000000
          Width = 706.772110000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."NOME_SENDER"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 109.606370000000000000
          Width = 706.772110000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."Nome"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 132.283550000000000000
          Width = 706.772110000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocoPak."TITULO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 188.976500000000000000
          Width = 396.850650000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Endere'#231'o de entrega')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 170.078850000000000000
          Width = 396.850650000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do cliente')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 170.078850000000000000
          Width = 177.637848980000000000
          Height = 37.795275590551200000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '3 Tentativas:'
            'data e hora')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 170.078850000000000000
          Width = 105.826840000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Motivo de n'#227'o'
            'entrega / rubrica')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 207.874150000000000000
          Width = 396.850650000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome leg'#237'vel de quem recebeu')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 207.874150000000000000
          Width = 283.464750000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Assinatura de quem recebeu')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 170.078850000000000000
          Width = 26.456710000000000000
          Height = 56.692950000000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 457.323130000000000000
        Width = 718.110700000000000000
      end
    end
  end
  object QrProtPakIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrProtPakItsBeforeClose
    AfterScroll = QrProtPakItsAfterScroll
    OnCalcFields = QrProtPakItsCalcFields
    SQL.Strings = (
      'SELECT cimv.Unidade, '
      'CONCAT(RIGHT(PERIOD_ADD(200000, ppi.Periodo), 2), "/", '
      '  LEFT(PERIOD_ADD(200000, ppi.Periodo), 4)) PERIODO_TXT,'
      'IF(clii.Tipo=0, clii.RazaoSocial, clii.Nome) NOMECLI_INT,'
      'IF(clie.Tipo=0, clie.RazaoSocial, clie.Nome) NOMECLIENTE,'
      'IF(clie.Tipo=0, clie.ERua, clie.PRua) ROTACLI,'
      'ptm.Nome NOMEMOTIVO, pto.Nome NOMEMANUAL, ppi.*,'
      'IF(ppi.Cancelado=0,"N'#195'O","SIM") CANCELADO_TXT, '
      
        'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%y")) DATAE_TX' +
        'T,'
      
        'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%y")) DATAD_TX' +
        'T,'
      
        'IF(ppi.Vencto=0, "", DATE_FORMAT(ppi.Vencto, "%d/%m/%y")) VENCTO' +
        '_TXT'
      'FROM protpakits ppi'
      'LEFT JOIN protocomot ptm ON ptm.Codigo=ppi.Motivo'
      'LEFT JOIN protocooco pto ON pto.Codigo=ppi.Manual'
      'LEFT JOIN entidades clii ON clii.Codigo=ppi.CliInt'
      'LEFT JOIN entidades clie ON clie.Codigo=ppi.Cliente'
      'LEFT JOIN condimov  cimv ON cimv.Conta=ppi.Depto'
      'WHERE ppi.Controle=:P0')
    Left = 596
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProtPakItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'protpakits.Codigo'
    end
    object QrProtPakItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'protpakits.Controle'
    end
    object QrProtPakItsConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'protpakits.Conta'
    end
    object QrProtPakItsDataE: TDateField
      FieldName = 'DataE'
      Origin = 'protpakits.DataE'
    end
    object QrProtPakItsDataD: TDateField
      FieldName = 'DataD'
      Origin = 'protpakits.DataD'
    end
    object QrProtPakItsCancelado: TIntegerField
      FieldName = 'Cancelado'
      Origin = 'protpakits.Cancelado'
    end
    object QrProtPakItsMotivo: TIntegerField
      FieldName = 'Motivo'
      Origin = 'protpakits.Motivo'
    end
    object QrProtPakItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'protpakits.Lk'
    end
    object QrProtPakItsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'protpakits.DataCad'
    end
    object QrProtPakItsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'protpakits.DataAlt'
    end
    object QrProtPakItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'protpakits.UserCad'
    end
    object QrProtPakItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'protpakits.UserAlt'
    end
    object QrProtPakItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'protpakits.AlterWeb'
    end
    object QrProtPakItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'protpakits.Ativo'
    end
    object QrProtPakItsLink_ID: TIntegerField
      FieldName = 'Link_ID'
      Origin = 'protpakits.Link_ID'
      Required = True
    end
    object QrProtPakItsID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
      Origin = 'protpakits.ID_Cod1'
    end
    object QrProtPakItsID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
      Origin = 'protpakits.ID_Cod2'
    end
    object QrProtPakItsID_Cod3: TIntegerField
      FieldName = 'ID_Cod3'
      Origin = 'protpakits.ID_Cod3'
    end
    object QrProtPakItsID_Cod4: TIntegerField
      FieldName = 'ID_Cod4'
      Origin = 'protpakits.ID_Cod4'
    end
    object QrProtPakItsDATAE_TXT: TWideStringField
      FieldName = 'DATAE_TXT'
      Size = 10
    end
    object QrProtPakItsDATAD_TXT: TWideStringField
      FieldName = 'DATAD_TXT'
      Size = 10
    end
    object QrProtPakItsNOMEMOTIVO: TWideStringField
      FieldName = 'NOMEMOTIVO'
      Origin = 'protocomot.Nome'
      Size = 50
    end
    object QrProtPakItsCANCELADO_TXT: TWideStringField
      FieldName = 'CANCELADO_TXT'
      Required = True
      Size = 3
    end
    object QrProtPakItsUnidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'condimov.Unidade'
      Size = 10
    end
    object QrProtPakItsNOMECLI_INT: TWideStringField
      FieldName = 'NOMECLI_INT'
      Size = 100
    end
    object QrProtPakItsNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrProtPakItsCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'protpakits.CliInt'
      Required = True
    end
    object QrProtPakItsCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'protpakits.Cliente'
      Required = True
    end
    object QrProtPakItsFornece: TIntegerField
      FieldName = 'Fornece'
      Origin = 'protpakits.Fornece'
      Required = True
    end
    object QrProtPakItsPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'protpakits.Periodo'
      Required = True
    end
    object QrProtPakItsLancto: TIntegerField
      FieldName = 'Lancto'
      Origin = 'protpakits.Lancto'
      Required = True
    end
    object QrProtPakItsDepto: TIntegerField
      FieldName = 'Depto'
      Origin = 'protpakits.Depto'
      Required = True
    end
    object QrProtPakItsPERIODO_TXT: TWideStringField
      FieldName = 'PERIODO_TXT'
      Required = True
      Size = 7
    end
    object QrProtPakItsRetorna: TSmallintField
      FieldName = 'Retorna'
      Origin = 'protpakits.Retorna'
      Required = True
    end
    object QrProtPakItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrProtPakItsVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrProtPakItsVENCTO_TXT: TWideStringField
      FieldName = 'VENCTO_TXT'
      Size = 10
    end
    object QrProtPakItsMoraDiaVal: TFloatField
      FieldName = 'MoraDiaVal'
      Required = True
    end
    object QrProtPakItsMultaVal: TFloatField
      FieldName = 'MultaVal'
      Required = True
    end
    object QrProtPakItsCedente: TIntegerField
      FieldName = 'Cedente'
      Required = True
    end
    object QrProtPakItsDocum: TFloatField
      FieldName = 'Docum'
      Required = True
    end
    object QrProtPakItsComoConf: TSmallintField
      FieldName = 'ComoConf'
      Required = True
    end
    object QrProtPakItsSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrProtPakItsManual: TIntegerField
      FieldName = 'Manual'
    end
    object QrProtPakItsTexto: TWideStringField
      FieldName = 'Texto'
      Size = 30
    end
    object QrProtPakItsNOMEMANUAL: TWideStringField
      FieldName = 'NOMEMANUAL'
      Size = 50
    end
    object QrProtPakItsRetornou: TIntegerField
      FieldName = 'Retornou'
    end
    object QrProtPakItsSaiu: TIntegerField
      FieldName = 'Saiu'
    end
    object QrProtPakItsRecebeu: TIntegerField
      FieldName = 'Recebeu'
    end
    object QrProtPakItsDataSai: TDateTimeField
      FieldName = 'DataSai'
    end
    object QrProtPakItsDataRec: TDateTimeField
      FieldName = 'DataRec'
    end
    object QrProtPakItsDataRet: TDateTimeField
      FieldName = 'DataRet'
    end
    object QrProtPakItsLimiteSai: TDateField
      FieldName = 'LimiteSai'
    end
    object QrProtPakItsLimiteRem: TDateField
      FieldName = 'LimiteRem'
    end
    object QrProtPakItsLimiteRet: TDateField
      FieldName = 'LimiteRet'
    end
    object QrProtPakItsROTACLI: TWideStringField
      FieldName = 'ROTACLI'
      Size = 30
    end
    object QrProtPakItsSERIE_DOCUM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_DOCUM'
      Size = 25
      Calculated = True
    end
    object QrProtPakItsLN2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LN2'
      Size = 255
      Calculated = True
    end
    object QrProtPakItsLNR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LNR'
      Size = 255
      Calculated = True
    end
    object QrProtPakItsDataSai_TXT: TWideStringField
      FieldName = 'DataSai_TXT'
      Size = 19
    end
    object QrProtPakItsDataRec_TXT: TWideStringField
      FieldName = 'DataRec_TXT'
      Size = 19
    end
    object QrProtPakItsDataRet_TXT: TWideStringField
      FieldName = 'DataRet_TXT'
      Size = 19
    end
  end
  object PMNumero: TPopupMenu
    Left = 96
    Top = 48
    object arefa1: TMenuItem
      Caption = '&Tarefa'
      OnClick = arefa1Click
    end
    object Lote1: TMenuItem
      Caption = '&Lote'
      OnClick = Lote1Click
    end
    object Protocolo1: TMenuItem
      Caption = '&Protocolo'
      OnClick = Protocolo1Click
    end
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Controle'
      'FROM protpakits'
      'WHERE Conta=:P0'
      '')
    Left = 276
    Top = 20
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object frxLote_Unidade_1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 228
    Top = 456
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProtocolos
        DataSetName = 'frxDsProtocolos'
      end
      item
        DataSet = frxDsProtocoPak
        DataSetName = 'frxDsProtocoPak'
      end
      item
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 90.708720000000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
        RowCount = 0
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 7.559059999999988000
          Width = 680.315400000000000000
          Height = 83.149660000000000000
          Frame.Typ = []
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 7.559059999999988000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."Unidade"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 7.559059999999988000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtPakIts."NOMECLIENTE"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 26.456709999999990000
          Width = 396.850650000000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtPakIts."LNR"] - [frxDsProtPakIts."LN2"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 7.559059999999988000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 7.559059999999988000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 7.559059999999988000
          Width = 105.826840000000000000
          Height = 56.692950000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 26.456709999999990000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 26.456709999999990000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 45.354359999999990000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 45.354359999999990000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 64.252009999999990000
          Width = 396.850650000000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 64.252009999999990000
          Width = 283.464750000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ass.:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 7.559059999999988000
          Width = 26.456710000000000000
          Height = 83.149660000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."Conta"]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 226.771787800000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 37.795300000000000000
          Width = 706.772110000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Top = 37.795300000000000000
          Width = 695.433520000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 56.692949999999990000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 56.692949999999990000
          Width = 411.968770000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LOTE DE PROTOCOLOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Top = 56.692949999999990000
          Width = 139.842610000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 83.149660000000000000
          Width = 706.772110000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."NOME_SENDER"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 109.606370000000000000
          Width = 706.772110000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."Nome"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 132.283550000000000000
          Width = 706.772110000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocoPak."TITULO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 170.078850000000000000
          Width = 94.488250000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 188.976500000000000000
          Width = 396.850650000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Endere'#231'o de entrega')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 170.078850000000000000
          Width = 302.362400000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do propriet'#225'rio')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 170.078850000000000000
          Width = 177.637848980000000000
          Height = 37.795275590551200000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '3 Tentativas:'
            'data e hora')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 170.078850000000000000
          Width = 105.826840000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Motivo de n'#227'o'
            'entrega / rubrica')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 207.874150000000000000
          Width = 396.850650000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome leg'#237'vel de quem recebeu')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 207.874150000000000000
          Width = 283.464750000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Assinatura de quem recebeu')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 170.078850000000000000
          Width = 26.456710000000000000
          Height = 56.692950000000010000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 457.323130000000000000
        Width = 718.110700000000000000
      end
    end
  end
  object frxLote_Entidade_1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 40347.632889143490000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxLote_Entidade_1GetValue
    Left = 372
    Top = 452
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProtocolos
        DataSetName = 'frxDsProtocolos'
      end
      item
        DataSet = frxDsProtocoPak
        DataSetName = 'frxDsProtocoPak'
      end
      item
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 90.708720000000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
        RowCount = 0
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 7.559059999999988000
          Width = 680.315400000000000000
          Height = 83.149660000000000000
          Frame.Typ = []
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 7.559059999999988000
          Width = 238.110390000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtPakIts."NOMECLIENTE"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 26.456709999999990000
          Width = 396.850650000000000000
          Height = 37.795275590000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtPakIts."LNR"] - [frxDsProtPakIts."LN2"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 7.559059999999988000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 7.559059999999988000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 7.559059999999988000
          Width = 105.826840000000000000
          Height = 56.692950000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 26.456709999999990000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 26.456709999999990000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 45.354359999999990000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 45.354359999999990000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 64.252009999999990000
          Width = 396.850650000000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 64.252009999999990000
          Width = 283.464750000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ass.:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 7.559059999999988000
          Width = 26.456710000000000000
          Height = 83.149660000000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."Conta"]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Top = 7.559059999999988000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DOCUM]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 7.559059999999988000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProtPakIts."Valor"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Top = 7.559059999999988000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DataField = 'VENCTO_TXT'
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."VENCTO_TXT"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 226.771787800000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 37.795300000000000000
          Width = 706.772110000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Top = 37.795300000000000000
          Width = 695.433520000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 15.118110240000000000
          Top = 56.692949999999990000
          Width = 695.433520000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 56.692949999999990000
          Width = 411.968770000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LOTE DE PROTOCOLOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Top = 56.692949999999990000
          Width = 139.842610000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 56.692949999999990000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 83.149660000000000000
          Width = 706.772110000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."NOME_SENDER"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 109.606370000000000000
          Width = 706.772110000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."Nome"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 132.283550000000000000
          Width = 706.772110000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocoPak."TITULO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 188.976500000000000000
          Width = 396.850650000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Endere'#231'o de entrega')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 170.078850000000000000
          Width = 238.110390000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do cliente')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 170.078850000000000000
          Width = 177.637848980000000000
          Height = 37.795275590551200000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '3 Tentativas:'
            'data e hora')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 170.078850000000000000
          Width = 105.826840000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Motivo de n'#227'o'
            'entrega / rubrica')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 207.874150000000000000
          Width = 396.850650000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome leg'#237'vel de quem recebeu')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 207.874150000000000000
          Width = 283.464750000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Assinatura de quem recebeu')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 170.078850000000000000
          Width = 26.456710000000000000
          Height = 56.692950000000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Top = 170.078850000000000000
          Width = 52.913420000000000000
          Height = 18.897637800000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 170.078850000000000000
          Width = 52.913420000000000000
          Height = 18.897637800000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Top = 170.078850000000000000
          Width = 52.913420000000000000
          Height = 18.897637800000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Docum.')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 457.323130000000000000
        Width = 718.110700000000000000
      end
    end
  end
  object frxLote_Unidade_2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 40674.886104027780000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 228
    Top = 504
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProtocolos
        DataSetName = 'frxDsProtocolos'
      end
      item
        DataSet = frxDsProtocoPak
        DataSetName = 'frxDsProtocoPak'
      end
      item
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 83.149606300000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
        RowCount = 0
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 79.370130000000000000
          Frame.Typ = []
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Width = 366.614410000000000000
          Height = 12.094485750000000000
          DataField = 'NOMECLI_INT'
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."NOMECLI_INT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 12.094488190000000000
          Width = 366.614410000000000000
          Height = 12.094485750000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Referente a: [frxDsProtocolos."PRINT_DESCRI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 36.661417320000000000
          Width = 366.614410000000000000
          Height = 23.811023620000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtPakIts."LNR"] - [frxDsProtPakIts."LN2"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Width = 90.708658980000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Width = 71.811021180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Width = 124.724490000000000000
          Height = 56.692950000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 18.897637800000000000
          Width = 90.708658980000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 18.897637800000000000
          Width = 71.811021180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 37.795275590000000000
          Width = 90.708658980000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 37.795275590000000000
          Width = 71.811021180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 60.472480000000000000
          Width = 366.614410000000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 56.692950000000000000
          Width = 287.244280000000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ass.:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Width = 26.456710000000000000
          Height = 79.370130000000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."Conta"]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 24.188976380000000000
          Width = 94.488250000000000000
          Height = 12.472438500000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."Unidade"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944960000000000000
          Top = 24.188976380000000000
          Width = 272.126160000000000000
          Height = 12.472438500000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtPakIts."NOMECLIENTE"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 147.401670000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 18.897650000000000000
          Width = 517.795610000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."NOME_SENDER"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 18.897650000000000000
          Width = 79.370078740157480000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em '
            '[Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 600.945270000000000000
          Top = 18.897650000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 566.929500000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtocolos."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 37.795300000000000000
          Width = 453.543600000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProtocoPak."TITULO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 79.370130000000000000
          Frame.Typ = []
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 64.252010000000000000
          Width = 366.614410000000000000
          Height = 12.094485750000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Nome da Empresa')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 76.346456690000000000
          Width = 366.614410000000000000
          Height = 12.094485750000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Referente a')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 100.913385826771700000
          Width = 366.614410000000000000
          Height = 23.811023622047240000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Endere'#231'o de Entrega')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 64.252010000000000000
          Width = 162.519728980000000000
          Height = 56.692937800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '3 Tentativas:'
            'data e hora')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Top = 64.252010000000000000
          Width = 124.724490000000000000
          Height = 56.692950000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Motivo de n'#227'o'
            'entrega / rubrica')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 124.724490000000000000
          Width = 366.614410000000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome leg'#237'vel de quem recebeu')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 120.944960000000000000
          Width = 287.244280000000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Assinatura de quem recebeu')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 26.456710000000000000
          Height = 79.370130000000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 88.440944881889750000
          Width = 94.488250000000000000
          Height = 12.472438500000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944960000000000000
          Top = 88.440944880000000000
          Width = 272.126160000000000000
          Height = 12.472438500000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Propriet'#225'rio')
          ParentFont = False
        end
      end
    end
  end
  object frxLote_Entidade_2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 40674.886104027780000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxLote_Entidade_1GetValue
    Left = 372
    Top = 500
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProtocolos
        DataSetName = 'frxDsProtocolos'
      end
      item
        DataSet = frxDsProtocoPak
        DataSetName = 'frxDsProtocoPak'
      end
      item
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 83.149606300000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
        RowCount = 0
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 79.370130000000000000
          Frame.Typ = []
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Width = 366.614410000000000000
          Height = 12.094485750000000000
          DataField = 'NOMECLI_INT'
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."NOMECLI_INT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 12.094488189999990000
          Width = 207.874150000000000000
          Height = 12.094485750000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtocolos."PRINT_DESCRI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 36.661417320000030000
          Width = 366.614410000000000000
          Height = 23.811023620000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtPakIts."LNR"] - [frxDsProtPakIts."LN2"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Width = 90.708658980000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Width = 71.811021180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Width = 124.724490000000000000
          Height = 56.692950000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 18.897637800000010000
          Width = 90.708658980000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 18.897637800000010000
          Width = 71.811021180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 37.795275589999990000
          Width = 90.708658980000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 37.795275589999990000
          Width = 71.811021180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 60.472479999999990000
          Width = 366.614410000000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 56.692950000000030000
          Width = 287.244280000000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ass.:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Width = 26.456710000000000000
          Height = 79.370130000000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."Conta"]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 24.188976380000010000
          Width = 366.614410000000000000
          Height = 12.472438500000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtPakIts."NOMECLIENTE"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 12.094488189999990000
          Width = 52.913420000000000000
          Height = 12.094485750000000000
          DataField = 'VENCTO_TXT'
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."VENCTO_TXT"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Top = 12.094488189999990000
          Width = 52.913420000000000000
          Height = 12.094485750000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DOCUM]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 287.244280000000000000
          Top = 12.094488189999990000
          Width = 52.913420000000000000
          Height = 12.094485750000000000
          DataField = 'Valor'
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProtPakIts."Valor"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 147.401670000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 18.897650000000000000
          Width = 517.795610000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."NOME_SENDER"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 18.897650000000000000
          Width = 79.370078740157480000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em '
            '[Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 600.945270000000000000
          Top = 18.897650000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 566.929500000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtocolos."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 37.795300000000000000
          Width = 453.543600000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProtocoPak."TITULO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 79.370130000000000000
          Frame.Typ = []
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 64.252010000000000000
          Width = 366.614410000000000000
          Height = 12.094485750000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Nome da Empresa')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 76.346456690000000000
          Width = 207.874150000000000000
          Height = 12.094485750000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Referente a')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 100.913385826771700000
          Width = 366.614410000000000000
          Height = 23.811023622047240000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Endere'#231'o de Entrega')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 64.252010000000000000
          Width = 162.519728980000000000
          Height = 56.692937800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '3 Tentativas:'
            'data e hora')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 555.590910000000000000
          Top = 64.252010000000000000
          Width = 124.724490000000000000
          Height = 56.692950000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Motivo de n'#227'o'
            'entrega / rubrica')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 124.724490000000000000
          Width = 366.614410000000000000
          Height = 18.897635350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome leg'#237'vel de quem recebeu')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 120.944960000000000000
          Width = 287.244280000000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Assinatura de quem recebeu')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 26.456710000000000000
          Height = 79.370130000000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 88.440944880000000000
          Width = 366.614410000000000000
          Height = 12.472438500000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Cliente')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 76.346456690000000000
          Width = 52.913420000000000000
          Height = 12.094485750000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 287.244280000000000000
          Top = 76.346456690000000000
          Width = 52.913420000000000000
          Height = 12.094485750000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Top = 76.346456690000000000
          Width = 52.913420000000000000
          Height = 12.094485750000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
        end
      end
    end
  end
  object frxLote_Circula1I1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 41047.714570682870000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 856
    Top = 368
    Datasets = <
      item
        DataSet = frxDsCircula
        DataSetName = 'frxDsCircula'
      end
      item
        DataSet = frxDsCirculaIts
        DataSetName = 'frxDsCirculaIts'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProtocolos
        DataSetName = 'frxDsProtocolos'
      end
      item
        DataSet = frxDsProtocoPak
        DataSetName = 'frxDsProtocoPak'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015760240000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCircula
        DataSetName = 'frxDsCircula'
        RowCount = 0
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DataSet = frxDsCircula
          DataSetName = 'frxDsCircula'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              '[frxDsCircula."SerieCH"]  [frxDsCircula."Docum"]  [frxDsCircula.' +
              '"NOMEOCORMAN"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Top = 18.897650000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Protocolo')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 279.685220000000000000
          Top = 18.897650000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Retorna?')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Lan'#231'amento')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Top = 18.897650000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 18.897650000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCircula
          DataSetName = 'frxDsCircula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '>')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Top = 18.897650000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '<')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 593.386210000000000000
          Top = 18.897650000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 219.212700940000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo de barras EAN 128-B')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 158.740247800000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LOTE DE PROTOCOLOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."NOME_SENDER"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 71.811070000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."Nome"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 94.488250000000000000
          Width = 453.543600000000000000
          Height = 37.795300000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProtocoPak."TITULO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 132.283550000000000000
          Width = 207.874150000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie / Documento')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 207.874150000000000000
          Top = 132.283550000000000000
          Width = 472.441250000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ocorr'#234'ncia')
          ParentFont = False
        end
        object BarCode2: TfrxBarCodeView
          AllowVectorExport = True
          Top = 102.047310000000000000
          Width = 178.000000000000000000
          Height = 22.677180000000000000
          BarType = bcCodeEAN128B
          DataField = 'EAN128'
          DataSet = frxDsProtocoPak
          DataSetName = 'frxDsProtocoPak'
          Frame.Typ = []
          Rotation = 0
          ShowText = False
          TestLine = False
          Text = 'L00012345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          ColorBar = clBlack
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 102.047310000000000000
        Top = 385.512060000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 83.149660000000000000
          Frame.Typ = []
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 396.850650000000000000
          Height = 56.692925590000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 18.897650000000000000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 18.897650000000000000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 37.795300000000000000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 37.795300000000000000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 56.692949999999990000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 56.692949999999990000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590600000000000000
          Width = 396.850650000000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 75.590600000000000000
          Width = 283.464750000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ass.:')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 294.803340000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCirculaIts
        DataSetName = 'frxDsCirculaIts'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCirculaIts."Conta"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 279.685220000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataField = 'RETORNA_TXT'
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCirculaIts."RETORNA_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Lancto'
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCirculaIts."Lancto"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCirculaIts."Valor"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCirculaIts."VENCTO_TXT"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Top = 15.118119999999980000
          Width = 423.307360000000000000
          Height = 15.118110240000000000
          DataField = 'TEXTO'
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCirculaIts."TEXTO"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DataField = 'UNIDADE'
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCirculaIts."UNIDADE"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 593.386210000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataField = 'MesAno'
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCirculaIts."MesAno"]')
          ParentFont = False
        end
        object BarCode1: TfrxBarCodeView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 211.000000000000000000
          Height = 22.677180000000000000
          BarType = bcCodeEAN128B
          DataField = 'EAN128'
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Frame.Typ = []
          Rotation = 0
          ShowText = False
          TestLine = False
          Text = 'OS1L00012345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          ColorBar = clBlack
        end
      end
    end
  end
  object frxLote_Unidade_3: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 41256.703550162030000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 228
    Top = 552
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProtocolos
        DataSetName = 'frxDsProtocolos'
      end
      item
        DataSet = frxDsProtocoPak
        DataSetName = 'frxDsProtocoPak'
      end
      item
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.944881890000000000
        Top = 154.960730000000000000
        Width = 680.315400000000000000
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Width = 60.472418980000000000
          Height = 24.944881890000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '     /    /')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Width = 34.015721180000000000
          Height = 24.944881890000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '    :     ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Width = 245.669450000000000000
          Height = 24.944881890000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Width = 56.692913390000000000
          Height = 24.944881890000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."Conta"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Width = 56.692950000000000000
          Height = 24.944881890000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."Unidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Width = 170.078740160000000000
          Height = 24.944881890000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtPakIts."NOMECLIENTE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Width = 56.692888980000000000
          Height = 24.944881890000000000
          DataField = 'VENCTO_TXT'
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."VENCTO_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 76.724450940000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 18.897650000000000000
          Width = 517.795610000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."NOME_SENDER"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 18.897650000000000000
          Width = 79.370078740157500000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em '
            '[Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 600.945270000000000000
          Top = 18.897650000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 566.929500000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtocolos."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 37.795300000000000000
          Width = 453.543600000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProtocoPak."TITULO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 12.472440944881890000
          Frame.Typ = []
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 64.252010000000000000
          Width = 94.488188980000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data e hora')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 64.252010000000000000
          Width = 245.669450000000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Assinatura de quem recebeu')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 64.251968500000000000
          Width = 56.692913385826770000
          Height = 12.472438500000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 64.251968500000000000
          Width = 170.078727950000000000
          Height = 12.472438500000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Propriet'#225'rio')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 12.472438500000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 64.252010000000000000
          Width = 56.692888980000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxLote_Entidade_3: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 41256.703550162030000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxLote_Entidade_1GetValue
    Left = 372
    Top = 548
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProtocolos
        DataSetName = 'frxDsProtocolos'
      end
      item
        DataSet = frxDsProtocoPak
        DataSetName = 'frxDsProtocoPak'
      end
      item
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.944881890000000000
        Top = 154.960730000000000000
        Width = 680.315400000000000000
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Width = 60.472418980000000000
          Height = 24.944881890000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '     /    /')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 457.323130000000000000
          Width = 34.015721180000000000
          Height = 24.944881890000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '    :     ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Width = 188.976500000000000000
          Height = 24.944881890000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Width = 56.692913390000000000
          Height = 24.944881890000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."Conta"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Width = 170.078740160000000000
          Height = 24.944881890000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtPakIts."NOMECLIENTE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 56.692888980000000000
          Height = 24.944881890000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DOCUM]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Width = 56.692888980000000000
          Height = 24.944881890000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProtPakIts."Valor"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Width = 56.692888980000000000
          Height = 24.944881890000000000
          DataField = 'VENCTO_TXT'
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."VENCTO_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 76.724450940000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 18.897650000000000000
          Width = 517.795610000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."NOME_SENDER"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 18.897650000000000000
          Width = 79.370078740157500000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em '
            '[Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 600.945270000000000000
          Top = 18.897650000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 566.929500000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtocolos."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 37.795300000000000000
          Width = 453.543600000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProtocoPak."TITULO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 12.472440944881890000
          Frame.Typ = []
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 64.252010000000000000
          Width = 94.488188980000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data e hora')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 491.338900000000000000
          Top = 64.252010000000000000
          Width = 188.976500000000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Assinatura de quem recebeu')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 64.251968500000000000
          Width = 226.771677950000000000
          Height = 12.472438500000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Cliente')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 12.472438500000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 64.252010000000000000
          Width = 56.692888980000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 64.252010000000000000
          Width = 56.692888980000000000
          Height = 12.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 64.252010000000000000
          Width = 56.692888980000000000
          Height = 12.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrCircula3: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCircula3BeforeClose
    AfterScroll = QrCircula3AfterScroll
    SQL.Strings = (
      'SELECT ppi.ID_Cod2, sta.Nome NO_ID_Cod2'
      'FROM protpakits ppi'
      'LEFT JOIN estatusoss sta ON sta.Codigo=ppi.ID_Cod2'
      'WHERE ppi.Controle=:P0'
      'GROUP BY ppi.ID_Cod2'
      'ORDER BY ppi.ID_Cod2')
    Left = 940
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCircula3ID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
    end
    object QrCircula3NO_ID_Cod2: TWideStringField
      FieldName = 'NO_ID_Cod2'
      Size = 60
    end
  end
  object frxDsCircula3: TfrxDBDataset
    UserName = 'frxDsCircula3'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID_Cod2=ID_Cod2'
      'NO_ID_Cod2=NO_ID_Cod2')
    DataSet = QrCircula3
    BCDToCurrency = False
    
    Left = 940
    Top = 192
  end
  object frxLote_Circula3L1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 940
    Top = 324
    Datasets = <
      item
        DataSet = frxDsCircula3
        DataSetName = 'frxDsCircula3'
      end
      item
        DataSet = frxDsCirculaIts3
        DataSetName = 'frxDsCirculaIts3'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProtocolos
        DataSetName = 'frxDsProtocolos'
      end
      item
        DataSet = frxDsProtocoPak
        DataSetName = 'frxDsProtocoPak'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015760240000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCircula3
        DataSetName = 'frxDsCircula3'
        RowCount = 0
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DataSet = frxDsCircula3
          DataSetName = 'frxDsCircula3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCircula3."ID_Cod2"] - [frxDsCircula3."NO_ID_Cod2"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Protocolo')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 18.897650000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Retorna?')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Localizador')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 18.897650000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921460000000000000
          Top = 18.897650000000000000
          Width = 332.598640000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 18.897650000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCircula
          DataSetName = 'frxDsCircula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '>')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Top = 18.897650000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '<')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 151.181187800000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LOTE DE PROTOCOLOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."NOME_SENDER"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 71.811070000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."Nome"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 132.283550000000000000
          Width = 680.315400000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Status')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 94.488250000000000000
          Width = 453.543600000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProtocoPak."TITULO3"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode2: TfrxBarCodeView
          AllowVectorExport = True
          Top = 102.047310000000000000
          Width = 178.000000000000000000
          Height = 22.677180000000000000
          BarType = bcCodeEAN128B
          DataField = 'EAN128_3'
          DataSet = frxDsProtocoPak
          DataSetName = 'frxDsProtocoPak'
          Frame.Typ = []
          Rotation = 0
          ShowText = False
          TestLine = False
          Text = 'L00012345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          ColorBar = clBlack
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 102.047310000000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 83.149660000000000000
          Frame.Typ = []
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 396.850650000000000000
          Height = 56.692925590000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 18.897650000000000000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 18.897650000000000000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 37.795300000000000000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 37.795300000000000000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 56.692949999999990000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 56.692949999999990000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590600000000000000
          Width = 396.850650000000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 75.590600000000000000
          Width = 283.464750000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ass.:')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 287.244280000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCirculaIts3
        DataSetName = 'frxDsCirculaIts3'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Conta'
          DataSet = frxDsCirculaIts3
          DataSetName = 'frxDsCirculaIts3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCirculaIts3."Conta"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataField = 'RETORNA_TXT'
          DataSet = frxDsCirculaIts3
          DataSetName = 'frxDsCirculaIts3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCirculaIts3."RETORNA_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'OSCab'
          DataSet = frxDsCirculaIts3
          DataSetName = 'frxDsCirculaIts3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCirculaIts3."OSCab"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsCirculaIts3
          DataSetName = 'frxDsCirculaIts3'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCirculaIts3."Valor"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DataField = 'VENCTO_TXT'
          DataSet = frxDsCirculaIts3
          DataSetName = 'frxDsCirculaIts3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCirculaIts3."VENCTO_TXT"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Width = 287.244280000000000000
          Height = 15.118110240000000000
          DataField = 'NO_CLI'
          DataSet = frxDsCirculaIts3
          DataSetName = 'frxDsCirculaIts3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCirculaIts3."NO_CLI"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 309.921460000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'OSCab'
          DataSet = frxDsCirculaIts3
          DataSetName = 'frxDsCirculaIts3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCirculaIts3."OSCab"]')
          ParentFont = False
        end
      end
    end
  end
  object QrCirculaIts3: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCirculaIts3CalcFields
    SQL.Strings = (
      'SELECT ppi.Conta, IF(ppi.Retorna=0, "N'#227'o", "Sim") RETORNA_TXT, '
      'ppi.ID_Cod1 OSCab, ppi.Valor, ppi.Texto TEXTO, '
      
        'IF(ppi.Vencto=0, "", DATE_FORMAT(ppi.Vencto, "%d/%m/%Y")) VENCTO' +
        '_TXT, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLI '
      'FROM protpakits ppi '
      'LEFT JOIN entidades ent ON ent.Codigo=ppi.Cliente')
    Left = 940
    Top = 236
    object QrCirculaIts3Conta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCirculaIts3RETORNA_TXT: TWideStringField
      FieldName = 'RETORNA_TXT'
      Required = True
      Size = 3
    end
    object QrCirculaIts3OSCab: TIntegerField
      FieldName = 'OSCab'
    end
    object QrCirculaIts3Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrCirculaIts3TEXTO: TWideStringField
      FieldName = 'TEXTO'
      Size = 30
    end
    object QrCirculaIts3VENCTO_TXT: TWideStringField
      FieldName = 'VENCTO_TXT'
      Size = 10
    end
    object QrCirculaIts3NO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrCirculaIts3Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrCirculaIts3EAN128: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EAN128'
      Calculated = True
    end
  end
  object frxDsCirculaIts3: TfrxDBDataset
    UserName = 'frxDsCirculaIts3'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Conta=Conta'
      'RETORNA_TXT=RETORNA_TXT'
      'OSCab=OSCab'
      'Valor=Valor'
      'TEXTO=TEXTO'
      'VENCTO_TXT=VENCTO_TXT'
      'NO_CLI=NO_CLI'
      'Cliente=Cliente'
      'EAN128=EAN128')
    DataSet = QrCirculaIts3
    BCDToCurrency = False
    
    Left = 940
    Top = 280
  end
  object QrTpTaref: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(Link_ID = 0, 1, Link_ID) + 0.000 IDLink,'
      'COUNT(Controle) + 0.000 ITENS'
      'FROM protpakits ppi'
      'WHERE ppi.Controle=63'
      'GROUP BY IDLink'
      '')
    Left = 172
    Top = 308
    object QrTpTarefIDLink: TFloatField
      FieldName = 'IDLink'
      Required = True
    end
    object QrTpTarefITENS: TFloatField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object frxLote_Circula3I1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 41047.714570682870000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 940
    Top = 368
    Datasets = <
      item
        DataSet = frxDsCircula3
        DataSetName = 'frxDsCircula3'
      end
      item
        DataSet = frxDsCirculaIts3
        DataSetName = 'frxDsCirculaIts3'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProtocolos
        DataSetName = 'frxDsProtocolos'
      end
      item
        DataSet = frxDsProtocoPak
        DataSetName = 'frxDsProtocoPak'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015760240000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCircula3
        DataSetName = 'frxDsCircula3'
        RowCount = 0
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DataSet = frxDsCircula
          DataSetName = 'frxDsCircula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCircula3."ID_Cod2"] - [frxDsCircula3."NO_ID_Cod2"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Top = 18.897650000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Protocolo')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 279.685220000000000000
          Top = 18.897650000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Retorna?')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Localizador')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 18.897650000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Top = 18.897650000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 18.897650000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCircula
          DataSetName = 'frxDsCircula'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '>')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Top = 18.897650000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '<')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Top = 18.897650000000000000
          Width = 113.385900000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Rubrica')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 219.212700940000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo de barras EAN 128-B')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 151.181187800000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LOTE DE PROTOCOLOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."NOME_SENDER"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 71.811070000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."Nome"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 94.488250000000000000
          Width = 453.543600000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProtocoPak."TITULO3"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 132.283550000000000000
          Width = 680.315400000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Status')
          ParentFont = False
        end
        object BarCode2: TfrxBarCodeView
          AllowVectorExport = True
          Top = 102.047310000000000000
          Width = 211.000000000000000000
          Height = 22.677180000000000000
          BarType = bcCodeEAN128B
          DataField = 'EAN128_3'
          DataSet = frxDsProtocoPak
          DataSetName = 'frxDsProtocoPak'
          Frame.Typ = []
          Rotation = 0
          ShowText = False
          TestLine = False
          Text = 'OS1L00012345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          ColorBar = clBlack
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 102.047310000000000000
        Top = 377.953000000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 83.149660000000000000
          Frame.Typ = []
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 396.850650000000000000
          Height = 56.692925590000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 18.897650000000000000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 18.897650000000000000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 37.795300000000000000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 37.795300000000000000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 56.692949999999990000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 56.692949999999990000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Top = 75.590600000000000000
          Width = 396.850650000000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 75.590600000000000000
          Width = 283.464750000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ass.:')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 287.244280000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCirculaIts3
        DataSetName = 'frxDsCirculaIts3'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Conta'
          DataSet = frxDsCirculaIts3
          DataSetName = 'frxDsCirculaIts3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCirculaIts3."Conta"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 279.685220000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          DataField = 'RETORNA_TXT'
          DataSet = frxDsCirculaIts3
          DataSetName = 'frxDsCirculaIts3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCirculaIts3."RETORNA_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 328.819110000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'OSCab'
          DataSet = frxDsCirculaIts3
          DataSetName = 'frxDsCirculaIts3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCirculaIts3."OSCab"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsCirculaIts3
          DataSetName = 'frxDsCirculaIts3'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCirculaIts3."Valor"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Width = 64.252010000000000000
          Height = 15.118110240000000000
          DataField = 'VENCTO_TXT'
          DataSet = frxDsCirculaIts3
          DataSetName = 'frxDsCirculaIts3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCirculaIts3."VENCTO_TXT"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Top = 15.118119999999980000
          Width = 309.921460000000000000
          Height = 15.118110240000000000
          DataSet = frxDsCirculaIts3
          DataSetName = 'frxDsCirculaIts3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCirculaIts3."Cliente"] - [frxDsCirculaIts3."NO_CLI"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Width = 18.897650000000000000
          Height = 30.236230240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Width = 113.385900000000000000
          Height = 30.236230240000000000
          DataSet = frxDsCirculaIts
          DataSetName = 'frxDsCirculaIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object BarCode1: TfrxBarCodeView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 211.000000000000000000
          Height = 22.677180000000000000
          BarType = bcCodeEAN128B
          DataField = 'EAN128'
          DataSet = frxDsCirculaIts3
          DataSetName = 'frxDsCirculaIts3'
          Frame.Typ = []
          Rotation = 0
          ShowText = False
          TestLine = False
          Text = 'OS1L00012345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          ColorBar = clBlack
        end
      end
    end
  end
  object frxLote_Entidade_4: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 41256.703550162030000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxLote_Entidade_1GetValue
    Left = 372
    Top = 596
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProtocolos
        DataSetName = 'frxDsProtocolos'
      end
      item
        DataSet = frxDsProtocoPak
        DataSetName = 'frxDsProtocoPak'
      end
      item
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 177.637910000000000000
        Width = 680.315400000000000000
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
        KeepTogether = True
        RowCount = 0
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Width = 113.385863390000000000
          Height = 13.228346456692920000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."Conta"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Width = 340.157590160000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtPakIts."NOMECLIENTE"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 13.228346456692920000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DOCUM]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Width = 75.590551180000000000
          Height = 13.228346456692920000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProtPakIts."Valor"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590538980000000000
          Height = 13.228346456692920000
          DataField = 'VENCTO_TXT'
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."VENCTO_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 76.724450940000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 18.897650000000000000
          Width = 517.795610000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."NOME_SENDER"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 18.897650000000000000
          Width = 79.370078740157500000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em '
            '[Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 600.945270000000000000
          Top = 18.897650000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 566.929500000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtocolos."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 37.795300000000000000
          Width = 453.543600000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProtocoPak."TITULO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 12.472440944881890000
          Frame.Typ = []
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 64.251968500000000000
          Width = 340.157577950000000000
          Height = 12.472438500000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Cliente')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 113.385863390000000000
          Height = 12.472438500000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 64.252010000000000000
          Width = 75.590538980000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 12.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 12.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 154.960730000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsProtPakIts."NOMECLIENTE"'
        KeepTogether = True
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 94.488184090000000000
        Top = 215.433210000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 83.149660000000000000
          Frame.Typ = []
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Width = 396.850650000000000000
          Height = 56.692925590000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 105.826840000000000000
          Height = 56.692950000000010000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 18.897650000000000000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 18.897650000000000000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 37.795300000000000000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 37.795300000000000000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692949999999800000
          Width = 396.850650000000000000
          Height = 26.456692910000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 56.692949999999800000
          Width = 283.464750000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ass.:')
          ParentFont = False
        end
      end
    end
  end
  object frxLote_Unidade_4: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 41256.703550162030000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 228
    Top = 600
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsProtocolos
        DataSetName = 'frxDsProtocolos'
      end
      item
        DataSet = frxDsProtocoPak
        DataSetName = 'frxDsProtocoPak'
      end
      item
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 177.637910000000000000
        Width = 680.315400000000000000
        DataSet = frxDsProtPakIts
        DataSetName = 'frxDsProtPakIts'
        RowCount = 0
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Width = 94.488201180000000000
          Height = 13.228346456692920000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."Conta"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 75.590551180000000000
          Height = 13.228346456692920000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."Unidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 434.645840160000000000
          Height = 13.228346456692920000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtPakIts."NOMECLIENTE"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 13.228346456692920000
          DataField = 'VENCTO_TXT'
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtPakIts."VENCTO_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 76.724450940000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 18.897650000000000000
          Width = 517.795610000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsProtocolos."NOME_SENDER"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 18.897650000000000000
          Width = 79.370078740157500000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em '
            '[Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 600.945270000000000000
          Top = 18.897650000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 566.929500000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsProtocolos."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 37.795300000000000000
          Width = 453.543600000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsProtocoPak."TITULO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 64.251968500000000000
          Width = 75.590551180000000000
          Height = 12.472438500000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 64.251968500000000000
          Width = 434.645827950000000000
          Height = 12.472438500000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome do Propriet'#225'rio')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 94.488201180000000000
          Height = 12.472438500000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Protocolo')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 64.252010000000000000
          Width = 75.590551180000000000
          Height = 12.472440940000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 154.960730000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsProtPakIts."NOMECLIENTE"'
        KeepTogether = True
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 94.488184090000000000
        Top = 215.433210000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 83.149660000000000000
          Frame.Typ = []
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Width = 396.850650000000000000
          Height = 56.692925590000000000
          DataSet = frxDsProtPakIts
          DataSetName = 'frxDsProtPakIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 105.826840000000000000
          Height = 56.692950000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 18.897650000000000000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 18.897650000000000000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 37.795300000000000000
          Width = 105.826778980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '        /        /')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 37.795300000000000000
          Width = 71.811021180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '         :     ')
          ParentFont = False
        end
      end
    end
  end
end
