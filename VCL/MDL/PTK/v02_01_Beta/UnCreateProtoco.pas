unit UnCreateProtoco;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables,(* DbTables,*) dmkGeral;

type
  TNomeTabRecriaTempTablePtk = (ntrtt_LctProto);
  TAcaoCreateFin = (acDrop, acCreate, acFind);
  TUnCreateProtoco = class(TObject)

  private
    { Private declarations }
    procedure Cria_ntrtt_Lct_Proto(Qry: TmySQLQuery);

  public
    { Public declarations }
    function RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTablePtk;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
  end;

var
  CriarProtoco: TUnCreateProtoco;

implementation

uses UnMyObjects, Module, ModuleGeral;

procedure TUnCreateProtoco.Cria_ntrtt_Lct_Proto(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Data                 date         NOT NULL                      ,');
  Qry.SQL.Add('  Tipo                 tinyint(3)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Carteira             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             bigint(20)   NOT NULL DEFAULT "0"          ,'); // j� testar como ser� quando precisar elevar de int para bigint no lct!
  Qry.SQL.Add('  Sub                  tinyint(1)   NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  CliInt               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Cliente              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Fornecedor           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Documento            double(20,0) NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Credito              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Debito               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  SerieCH              varchar(10)  NOT NULL                      ,');
  Qry.SQL.Add('  Vencimento           date         NOT NULL                      ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PRIMARY KEY (Data,Tipo,Carteira,Controle,Sub)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

function TUnCreateProtoco.RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTablePtk; Qry: TmySQLQuery;
UniqueTableName: Boolean; Repeticoes: Integer = 1; NomeTab: String = ''): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      ntrtt_LctProto:       Nome := Lowercase('_Lct_Proto_');
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MB_Erro(
    'Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)');
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    ntrtt_LctProto:     Cria_ntrtt_Lct_Proto(Qry);
    //
    else Geral.MB_Erro('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o!');
  end;
  Result := TabNo;
end;

end.

