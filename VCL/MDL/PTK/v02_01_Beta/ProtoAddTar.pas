unit ProtoAddTar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, DB,
  mySQLDbTables, dmkEdit, dmkEditCB, DBCtrls, dmkDBLookupComboBox, dmkGeral,
  dmkImage, dmkDBGrid, dmkPermissoes, UnDmkEnums, dmkDBGridZTO, DmkDAC_PF,
  Protocolo;

type
  TFmProtoAddTar = class(TForm)
    Panel1: TPanel;
    DsProtocolos: TDataSource;
    QrProtocolos: TmySQLQuery;
    CBTarefa: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdTarefa: TdmkEditCB;
    SpeedButton1: TSpeedButton;
    QrProtocolosCodigo: TIntegerField;
    QrProtocolosNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    QrProtocolosTipo: TIntegerField;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenProtocolos();
  public
    { Public declarations }
    FTiposProt: String; //Separar por v�rgula. Ex.: 1,2
    FCliInt: Integer;
    FFinalidade: TProtFinalidade;
    FQuery: TmySQLQuery;
    FGrade: TdmkDBGridZTO;
  end;

  var
  FmProtoAddTar: TFmProtoAddTar;

implementation

uses Protocolos, UnInternalConsts, MyDBCheck, UMySQLModule, Module, UnMyObjects,
UnProtocoUnit;

{$R *.DFM}

procedure TFmProtoAddTar.BtOKClick(Sender: TObject);
var
  Fechar: Boolean;
  i, Tarefa, EntDepto: Integer;
begin
  Screen.Cursor := crHourGlass;
  Tarefa        := EdTarefa.ValueVariant;
  Fechar        := False;
  //
  try
    if FFinalidade = ptkBoleto then
    begin
      {$IfNDef S_BLQ}
      if FGrade.SelectedRows.Count > 1 then
      begin
        with FGrade.DataSource.DataSet do
        for i := 0 to FGrade.SelectedRows.Count - 1 do
        begin
          GotoBookmark(FGrade.SelectedRows.Items[i]);
          //
          if VAR_KIND_DEPTO = kdUH then
            EntDepto := FQuery.FieldByName('Apto').AsInteger
          else
            EntDepto := FQuery.FieldByName('Entidade').AsInteger;
          //
          Fechar := UnProtocolo.AtualizaProtocoloBoleto(Tarefa, EntDepto,
                      FFinalidade, Dmod.QrUpd, Dmod.MyDB);
        end;
      end else
      begin
        if VAR_KIND_DEPTO = kdUH then
          EntDepto := FQuery.FieldByName('Apto').AsInteger
        else
          EntDepto := FQuery.FieldByName('Entidade').AsInteger;
        //
        Fechar := UnProtocolo.AtualizaProtocoloBoleto(Tarefa, EntDepto,
                    FFinalidade, Dmod.QrUpd, Dmod.MyDB);
      end;
      {$EndIf}
    end else
    if (FFinalidade = ptkNFSeAut) or (FFinalidade = ptkNFSeCan) then
    begin
      {$IfNDef sNFSe}
        Fechar := UnProtocolo.AtualizaProtocoloNFSe(Tarefa, FFinalidade,
                    Dmod.QrUpd, Dmod.MyDB);
      {$EndIf}
    end;
  finally
    Screen.Cursor := crDefault;
    if Fechar then
      Close;
  end;
end;

procedure TFmProtoAddTar.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProtoAddTar.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmProtoAddTar.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmProtoAddTar.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProtoAddTar.FormShow(Sender: TObject);
begin
  //N�o mostrar o 4 Com Registro pois deve ser para todos
  ReopenProtocolos;
end;

procedure TFmProtoAddTar.ReopenProtocolos;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrProtocolos, Dmod.MyDB, [
    'SELECT ptc.Codigo, ptc.Nome, ptc.Tipo ',
    'FROM protocolos ptc ',
    'LEFT JOIN enticliint eci ON eci.CodEnti = ptc.Def_Client ',
    'WHERE ptc.Codigo <> 0 ',
    'AND ptc.Tipo in (' + FTiposProt + ') ',
    'AND ptc.Def_Client=' + Geral.FF0(FCliInt),
    'ORDER BY Nome ',
    '']);
end;

procedure TFmProtoAddTar.SpeedButton1Click(Sender: TObject);
var
  Tarefa: Integer;
begin
  VAR_CADASTRO := 0;
  Tarefa       := EdTarefa.ValueVariant;

  ProtocoUnit.MostraFormProtocolos(0, Tarefa);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdTarefa, CBTarefa, QrProtocolos, VAR_CADASTRO);
    CBTarefa.SetFocus;
  end;
end;

end.
