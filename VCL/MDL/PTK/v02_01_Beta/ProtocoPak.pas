unit ProtocoPak;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, DBCtrls, dmkGeral,
  dmkDBLookupComboBox, dmkEditCB, Db, mySQLDbTables, UnDmkProcFunc, dmkImage,
  DmkDAC_PF, UnDmkEnums;

type
  TFmProtocoPak = class(TForm)
    Panel9: TPanel;
    Label1: TLabel;
    Label6: TLabel;
    LaMes: TLabel;
    Label7: TLabel;
    dmkEdDataI: TdmkEdit;
    dmkEdDataL: TdmkEdit;
    EdMes: TdmkEdit;
    dmkEdCNAB_Cfg: TdmkEditCB;
    dmkCBCNAB_Cfg: TdmkDBLookupComboBox;
    LaCBCNAB_Cfg: TLabel;
    QrCNAB_Cfg: TmySQLQuery;
    DsCNAB_Cfg: TDataSource;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdMesExit(Sender: TObject);
    procedure EdMesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCNAB_Cfg(Cedente: Integer);
  public
    { Public declarations }
    FCodigo, FControle, FTipo, FDef_Client, FCNAB_Cfg: Integer;
    FExecuted: Boolean;
  end;

  var
  FmProtocoPak: TFmProtocoPak;

implementation

uses UnMyObjects, UnInternalConsts, UMySQLModule, Module;

{$R *.DFM}

procedure TFmProtocoPak.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmProtocoPak.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmProtocoPak.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmProtocoPak.FormShow(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := FTipo = 4; //Cobran�a com registro
  //
  LaCBCNAB_Cfg.Visible  := Enab;
  dmkEdCNAB_Cfg.Visible := Enab;
  dmkCBCNAB_Cfg.Visible := Enab;
  //
  ReopenCNAB_Cfg(FDef_Client);
end;

procedure TFmProtocoPak.ReopenCNAB_Cfg(Cedente: Integer);
begin
{$IfNDef NO_CNAB}
  UnDmkDAC_PF.AbreMySQLQuery0(QrCNAB_Cfg, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM cnab_cfg ',
    'WHERE (Cedente=' + Geral.FF0(Cedente) + ' OR SacadAvali=' + Geral.FF0(Cedente) + ') ',
    'ORDER BY Nome ',
    '']);
{$EndIf}
end;

procedure TFmProtocoPak.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FExecuted       := False;
end;

procedure TFmProtocoPak.BtOKClick(Sender: TObject);
var
  CNAB_Cfg: Integer;
  DataI, DataL, Mes: String;
begin
  if EdMes.Text = CO_VAZIO then
    Mes := CO_VAZIO
  else
  if (EdMes.Enabled = False) then
    Mes := CO_VAZIO
  else
  begin
    //Mes := (*EdMes.Text[4]+EdMes.Text[5]+*)EdMes.Text[6]+EdMes.Text[7]+(*'/'+*)
    //EdMes.Text[1]+EdMes.Text[2](*+'/01'*);
    Mes := Geral.MesBarraAnoToMez(EdMes.Text);
    //
    if Mes = CO_VAZIO then
    begin
      Geral.MB_Aviso('AVISO! Defina o m�s de compet�ncia.');
      if EdMes.Enabled then
        EdMes.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  if (Mes = CO_VAZIO) and (EdMes.Enabled = True) then
  begin
    Geral.MB_Aviso('AVISO! Defina o m�s de compet�ncia.');
    if EdMes.Enabled then
      EdMes.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if dmkEdCNAB_Cfg.Visible then
  begin
    if dmkEdCNAB_Cfg.ValueVariant = 0 then
    begin
      Geral.MB_Aviso('AVISO! Defina a configura��o para gera��o do arquivo remessa CNAB.');
      if dmkEdCNAB_Cfg.Enabled then
        dmkEdCNAB_Cfg.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end else
      CNAB_Cfg := dmkEdCNAB_Cfg.ValueVariant;
  end else
    CNAB_Cfg := 0;
  //
  if MyObjects.FIC(dmkEdDataI.Text = '', dmkEdDataI, 'Defina a data da cria��o!') then Exit;
  //
  DataI := dmkPF.DDS(dmkEdDataI.Text);
  //
  if dmkEdDataL.Text <> '' then
    DataL := dmkPF.DDS(dmkEdDataL.Text)
  else
    DataL := '0000-00-00';
  //
  FControle :=
    UMyMod.BuscaEmLivreY_Def('protocopak', 'Controle', ImgTipo.SQLType, FControle);
  if (FControle > 0) and (FCodigo > 0 ) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'protocopak', False,
    [
      'DataI', 'DataL', 'Mez', 'Codigo', 'CNAB_Cfg'
    ], ['Controle'], [
      DataI, DataL, Mes, FCodigo, CNAB_Cfg
    ], [FControle], True) then
    begin
      //FLocCod(Codigo,Codigo);
      //FmProtocolos.ReopenProtocoPak(Controle);
      FExecuted := True;
      Close;
    end;
  end else
  if FCodigo = 0 then
    Geral.MB_Aviso('Inclus�o cancelada! ' + sLineBreak + 'Protocolo n�o definido!')
  else if FControle = 0 then
    Geral.MB_Aviso('Inclus�o cancelada! ' + sLineBreak + 'Lote n�o definido!');
end;

procedure TFmProtocoPak.EdMesExit(Sender: TObject);
begin
  (*
  EdMes.Text := Geral.TST(EdMes.Text, True);
  if EdMes.Text = CO_VAZIO then
    EdMes.SetFocus;
  *)
end;

procedure TFmProtocoPak.EdMesKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
(*
var
  Periodo: Integer;
*)
begin
  (*
  if key in ([VK_DOWN, VK_UP, VK_F5, VK_F6]) then
  begin
    if (EdMes.Text = '') or (key=VK_F5) then
      Periodo := Geral.Periodo2000(Date)
    else begin
      Periodo := dmkPF.MensalToPeriodo(EdMes.Text);
      if key=VK_DOWN then Periodo := Periodo -1;
      if key=VK_UP   then Periodo := Periodo +1;
      if key=VK_F4   then Periodo := Periodo -1;
      if key=VK_F5   then Periodo := Periodo   ;
    end;
    EdMes.Text := dmkPF.PeriodoToMensal(Periodo);
  end;
  *)
end;

end.
