object FmProtoGer: TFmProtoGer
  Left = 339
  Top = 185
  Caption = 'GER-PROTO-011 :: Gerenciamento de Protocolos'
  ClientHeight = 692
  ClientWidth = 1199
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1199
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1140
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object BtProtocolos: TBitBtn
        Tag = 10051
        Left = 5
        Top = 5
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtProtocolosClick
      end
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1081
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 422
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Protocolos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 422
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Protocolos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 422
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Protocolos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 1199
    Height = 492
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1199
      Height = 492
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1199
        Height = 492
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 551
    Width = 1199
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1194
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 601
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'R? => S = Informar o recebimento / N = N'#227'o '#233' necess'#225'rio informar' +
          ' o recebimento'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 601
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'R? => S = Informar o recebimento / N = N'#227'o '#233' necess'#225'rio informar' +
          ' o recebimento'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 606
    Width = 1199
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 1019
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 1017
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 59
    Width = 1199
    Height = 492
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ActivePage = TabSheet2
    Align = alClient
    TabHeight = 25
    TabOrder = 4
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Estat'#237'sticas'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PnPesq: TPanel
        Left = 0
        Top = 0
        Width = 1191
        Height = 123
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitWidth = 1189
        object Label1: TLabel
          Left = 457
          Top = 5
          Width = 301
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Entregador / coletor / (entidade banc'#225'ria para CR):'
        end
        object Label2: TLabel
          Left = 10
          Top = 5
          Width = 87
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cliente Interno:'
        end
        object Label3: TLabel
          Left = 10
          Top = 64
          Width = 61
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Protocolo:'
        end
        object Label4: TLabel
          Left = 457
          Top = 64
          Width = 108
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Lote de protocolo:'
        end
        object EdDef_Sender2: TdmkEditCB
          Left = 457
          Top = 25
          Width = 59
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBDef_Sender2
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBDef_Sender2: TdmkDBLookupComboBox
          Left = 518
          Top = 25
          Width = 378
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'NOMEENT'
          ListSource = DsSender2
          TabOrder = 1
          dmkEditCB = EdDef_Sender2
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdEmpresa2: TdmkEditCB
          Left = 10
          Top = 25
          Width = 59
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEmpresa2
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmpresa2: TdmkDBLookupComboBox
          Left = 71
          Top = 25
          Width = 378
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DsEmpresas2
          TabOrder = 3
          dmkEditCB = EdEmpresa2
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdProtocolo: TdmkEditCB
          Left = 10
          Top = 84
          Width = 59
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBProtocolo
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBProtocolo: TdmkDBLookupComboBox
          Left = 71
          Top = 84
          Width = 378
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsProtocolos
          TabOrder = 5
          dmkEditCB = EdProtocolo
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CBLote: TdmkDBLookupComboBox
          Left = 518
          Top = 84
          Width = 378
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Controle'
          ListField = 'Controle'
          ListSource = DsProtocoPak
          TabOrder = 6
          dmkEditCB = EdLote
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdLote: TdmkEditCB
          Left = 457
          Top = 84
          Width = 59
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBLote
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object BitBtn1: TBitBtn
          Tag = 22
          Left = 913
          Top = 44
          Width = 148
          Height = 50
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Pesquisa'
          NumGlyphs = 2
          TabOrder = 8
          OnClick = BitBtn1Click
        end
      end
      object GridPanel1: TGridPanel
        Left = 0
        Top = 123
        Width = 1191
        Height = 334
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = 'GridPanel1'
        ColumnCollection = <
          item
            Value = 33.333333333333330000
          end
          item
            Value = 33.333333333333330000
          end
          item
            Value = 33.333333333333330000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = Chart1
            Row = 0
          end
          item
            Column = 1
            Control = Chart2
            Row = 0
          end>
        RowCollection = <
          item
            Value = 100.000000000000000000
          end>
        TabOrder = 1
        ExplicitWidth = 1189
        ExplicitHeight = 326
        object Chart1: TChart
          Left = 1
          Top = 1
          Width = 395
          Height = 324
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Title.Font.Height = -19
          Title.Text.Strings = (
            'Status de protocolos')
          View3DOptions.Elevation = 315
          View3DOptions.Orthogonal = False
          View3DOptions.Perspective = 0
          View3DOptions.Rotation = 360
          Align = alClient
          TabOrder = 0
          DefaultCanvas = 'TGDIPlusCanvas'
          PrintMargins = (
            15
            14
            15
            14)
          ColorPaletteIndex = 6
          object Series1: TPieSeries
            Marks.Emboss.Color = 8487297
            Marks.Shadow.Color = 8487297
            Marks.Visible = False
            XValues.Order = loAscending
            YValues.Name = 'Pie'
            YValues.Order = loNone
            Frame.InnerBrush.BackColor = clRed
            Frame.InnerBrush.Gradient.EndColor = clGray
            Frame.InnerBrush.Gradient.MidColor = clWhite
            Frame.InnerBrush.Gradient.StartColor = 4210752
            Frame.InnerBrush.Gradient.Visible = True
            Frame.MiddleBrush.BackColor = clYellow
            Frame.MiddleBrush.Gradient.EndColor = 8553090
            Frame.MiddleBrush.Gradient.MidColor = clWhite
            Frame.MiddleBrush.Gradient.StartColor = clGray
            Frame.MiddleBrush.Gradient.Visible = True
            Frame.OuterBrush.BackColor = clGreen
            Frame.OuterBrush.Gradient.EndColor = 4210752
            Frame.OuterBrush.Gradient.MidColor = clWhite
            Frame.OuterBrush.Gradient.StartColor = clSilver
            Frame.OuterBrush.Gradient.Visible = True
            Frame.Width = 4
            OtherSlice.Legend.Visible = False
          end
        end
        object Chart2: TChart
          Left = 396
          Top = 1
          Width = 395
          Height = 324
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Title.Font.Height = -19
          Title.Text.Strings = (
            'Protocolos finalizados')
          View3DOptions.Elevation = 315
          View3DOptions.Orthogonal = False
          View3DOptions.Perspective = 0
          View3DOptions.Rotation = 360
          Align = alClient
          TabOrder = 1
          DefaultCanvas = 'TGDIPlusCanvas'
          PrintMargins = (
            15
            12
            15
            12)
          ColorPaletteIndex = 6
          object PieSeries1: TPieSeries
            Marks.Emboss.Color = 8487297
            Marks.Shadow.Color = 8487297
            Marks.Visible = False
            Title = 'Series2'
            XValues.Order = loAscending
            YValues.Name = 'Pie'
            YValues.Order = loNone
            Frame.InnerBrush.BackColor = clRed
            Frame.InnerBrush.Gradient.EndColor = clGray
            Frame.InnerBrush.Gradient.MidColor = clWhite
            Frame.InnerBrush.Gradient.StartColor = 4210752
            Frame.InnerBrush.Gradient.Visible = True
            Frame.MiddleBrush.BackColor = clYellow
            Frame.MiddleBrush.Gradient.EndColor = 8553090
            Frame.MiddleBrush.Gradient.MidColor = clWhite
            Frame.MiddleBrush.Gradient.StartColor = clGray
            Frame.MiddleBrush.Gradient.Visible = True
            Frame.OuterBrush.BackColor = clGreen
            Frame.OuterBrush.Gradient.EndColor = 4210752
            Frame.OuterBrush.Gradient.MidColor = clWhite
            Frame.OuterBrush.Gradient.StartColor = clSilver
            Frame.OuterBrush.Gradient.Visible = True
            Frame.Width = 4
            OtherSlice.Legend.Visible = False
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Gerenciamento'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PageControl2: TPageControl
        Left = 0
        Top = 0
        Width = 1191
        Height = 436
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ActivePage = TabSheet12
        Align = alClient
        TabHeight = 25
        TabOrder = 0
        OnChange = PageControl2Change
        ExplicitWidth = 1189
        ExplicitHeight = 428
        object TabSheet12: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Outros protocolos n'#227'o confirmados '
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Splitter3: TSplitter
            Left = 459
            Top = 0
            Width = 12
            Height = 401
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ExplicitHeight = 385
          end
          object Panel14: TPanel
            Left = 0
            Top = 0
            Width = 459
            Height = 401
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            ParentBackground = False
            TabOrder = 0
            ExplicitHeight = 385
            object LaGradePTK: TLabel
              Left = 1
              Top = 384
              Width = 457
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alBottom
              ExplicitWidth = 3
            end
            object GradePTK: TdmkDBGrid
              Left = 1
              Top = 235
              Width = 457
              Height = 149
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Lote'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Mez_TXT'
                  Title.Caption = 'M'#234's'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Def_Client_TXT'
                  Title.Caption = 'Cliente interno'
                  Width = 250
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsPTK
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Lote'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Mez_TXT'
                  Title.Caption = 'M'#234's'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Def_Client_TXT'
                  Title.Caption = 'Cliente interno'
                  Width = 250
                  Visible = True
                end>
            end
            object Panel17: TPanel
              Left = 1
              Top = 1
              Width = 457
              Height = 234
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object LaDef_Sender: TLabel
                Left = 10
                Top = 54
                Width = 301
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Entregador / coletor / (entidade banc'#225'ria para CR):'
              end
              object Label11: TLabel
                Left = 10
                Top = 5
                Width = 87
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Cliente Interno:'
              end
              object EdDef_Sender: TdmkEditCB
                Left = 10
                Top = 74
                Width = 59
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBDef_Sender
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBDef_Sender: TdmkDBLookupComboBox
                Left = 71
                Top = 74
                Width = 378
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'NOMEENT'
                ListSource = DsSender
                TabOrder = 1
                dmkEditCB = EdDef_Sender
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object RGProtoFiltro: TRadioGroup
                Left = 10
                Top = 105
                Width = 439
                Height = 65
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Filtro de protocolos: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'V'#225'lidos vencidos'
                  'V'#225'lidos abertos'
                  'Todos v'#225'lidos'
                  'Cancelados')
                TabOrder = 2
              end
              object EdEmpresa: TdmkEditCB
                Left = 10
                Top = 25
                Width = 59
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBEmpresa
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBEmpresa: TdmkDBLookupComboBox
                Left = 71
                Top = 25
                Width = 378
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Filial'
                ListField = 'NOMEFILIAL'
                ListSource = DsEmpresas
                TabOrder = 4
                dmkEditCB = EdEmpresa
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object BtPesquisa: TBitBtn
                Tag = 22
                Left = 10
                Top = 177
                Width = 148
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Pesquisa'
                NumGlyphs = 2
                TabOrder = 5
                OnClick = BtPesquisaClick
              end
            end
          end
          object Panel15: TPanel
            Left = 471
            Top = 0
            Width = 712
            Height = 401
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            ExplicitWidth = 708
            ExplicitHeight = 385
            object LaGradePPI: TLabel
              Left = 0
              Top = 369
              Width = 3
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alBottom
            end
            object Panel16: TPanel
              Left = 0
              Top = 0
              Width = 708
              Height = 59
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object BitBtn17: TBitBtn
                Tag = 294
                Left = 7
                Top = 5
                Width = 234
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Confirma sa'#237'da de todo lote'
                TabOrder = 0
                OnClick = BitBtn17Click
              end
              object BitBtn18: TBitBtn
                Tag = 294
                Left = 244
                Top = 5
                Width = 234
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Confirma recebimento de todo lote'
                TabOrder = 1
                OnClick = BitBtn18Click
              end
              object BitBtn19: TBitBtn
                Tag = 294
                Left = 480
                Top = 5
                Width = 234
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Confirma retorno de todo lote'
                TabOrder = 2
                OnClick = BitBtn19Click
              end
            end
            object GradePPI: TDBGrid
              Left = 0
              Top = 59
              Width = 708
              Height = 310
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsPPI
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnCellClick = GradePPICellClick
              OnDrawColumnCell = GradePPIDrawColumnCell
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Title.Caption = 'Protocolo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Lancto'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Docum'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Texto'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_RETORNA'
                  Title.Caption = 'R?'
                  Width = 17
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'LimiteSai'
                  Title.Caption = 'Lim.Sa'#237'da'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Saiu'
                  Title.Caption = 'S?'
                  Width = 18
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataSai_TXT'
                  Title.Caption = 'Dt.Sa'#237'da'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'LimiteRem'
                  Title.Caption = 'Lim.Recebeu'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Recebeu'
                  Title.Caption = 'R?'
                  Width = 18
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataRec_TXT'
                  Title.Caption = 'Dt.Recebeu'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'LimiteRet'
                  Title.Caption = 'Lim.Retorno'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Retornou'
                  Title.Caption = 'R?'
                  Width = 18
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataRet_TXT'
                  Title.Caption = 'Dt.Retorno'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_Motivo'
                  Title.Caption = 'Motivo'
                  Visible = True
                end>
            end
          end
        end
        object TabSheet10: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Protocolos de e-mail n'#227'o confirmados'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 1183
            Height = 401
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            TabOrder = 0
            ExplicitWidth = 1179
            ExplicitHeight = 385
            object Splitter2: TSplitter
              Left = 709
              Top = 119
              Width = 12
              Height = 281
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              ExplicitHeight = 265
            end
            object Splitter1: TSplitter
              Left = 469
              Top = 119
              Width = 12
              Height = 281
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              ExplicitHeight = 265
            end
            object Panel10: TPanel
              Left = 1
              Top = 1
              Width = 1181
              Height = 118
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 1177
              object Label5: TLabel
                Left = 10
                Top = 5
                Width = 87
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Cliente Interno:'
              end
              object BtAtzListaMailProt: TBitBtn
                Tag = 18
                Left = 6
                Top = 62
                Width = 148
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Atualiza lista'
                TabOrder = 0
                OnClick = BtAtzListaMailProtClick
              end
              object BtReenvia: TBitBtn
                Tag = 244
                Left = 161
                Top = 62
                Width = 148
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Reenvia e-mail'
                TabOrder = 1
                Visible = False
                OnClick = BtReenviaClick
              end
              object EdEmpresa3: TdmkEditCB
                Left = 10
                Top = 25
                Width = 59
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBEmpresa3
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBEmpresa3: TdmkDBLookupComboBox
                Left = 71
                Top = 25
                Width = 378
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Filial'
                ListField = 'NOMEFILIAL'
                ListSource = DsEmpresas3
                TabOrder = 3
                dmkEditCB = EdEmpresa3
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
            end
            object Panel5: TPanel
              Left = 1
              Top = 119
              Width = 468
              Height = 281
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              ExplicitHeight = 265
              object LaEmailProtTotal: TLabel
                Left = 0
                Top = 249
                Width = 3
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alBottom
              end
              object DBGEmailProt: TdmkDBGrid
                Left = 0
                Top = 0
                Width = 468
                Height = 249
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'ITENS'
                    Title.Caption = 'Itens'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Title.Caption = 'Tarefa'
                    Width = 225
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID'
                    Width = 65
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Def_Client_TXT'
                    Title.Caption = 'Cliente interno'
                    Width = 250
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsEmailProt
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'ITENS'
                    Title.Caption = 'Itens'
                    Width = 32
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Title.Caption = 'Tarefa'
                    Width = 225
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID'
                    Width = 65
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Def_Client_TXT'
                    Title.Caption = 'Cliente interno'
                    Width = 250
                    Visible = True
                  end>
              end
            end
            object Panel6: TPanel
              Left = 481
              Top = 119
              Width = 228
              Height = 281
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 2
              ExplicitHeight = 265
              object LaGEmailProtPakTotal: TLabel
                Left = 0
                Top = 249
                Width = 3
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alBottom
              end
              object DBGEmailProtPak: TdmkDBGrid
                Left = 0
                Top = 0
                Width = 228
                Height = 249
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'Lote'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEMEZ'
                    Title.Caption = 'Per'#237'odo'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ITENS'
                    Title.Caption = 'Itens'
                    Width = 32
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsEmailProtPak
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'Lote'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEMEZ'
                    Title.Caption = 'Per'#237'odo'
                    Width = 44
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ITENS'
                    Title.Caption = 'Itens'
                    Width = 32
                    Visible = True
                  end>
              end
            end
            object Panel7: TPanel
              Left = 721
              Top = 119
              Width = 461
              Height = 281
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 3
              ExplicitWidth = 457
              ExplicitHeight = 265
              object LaGEmailProtPakItsTotal: TLabel
                Left = 0
                Top = 249
                Width = 3
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alBottom
              end
              object DBGEmailProtPakIts: TdmkDBGrid
                Left = 0
                Top = 0
                Width = 457
                Height = 249
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Conta'
                    Title.Caption = 'Protocolo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DATAE_TXT'
                    Title.Caption = 'Envio'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Docum'
                    Title.Caption = 'Documento'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMECLI'
                    Title.Caption = 'Entidade'
                    Width = 250
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsEmailProtPakIts
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Conta'
                    Title.Caption = 'Protocolo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DATAE_TXT'
                    Title.Caption = 'Envio'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Docum'
                    Title.Caption = 'Documento'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMECLI'
                    Title.Caption = 'Entidade'
                    Width = 250
                    Visible = True
                  end>
              end
            end
          end
        end
      end
      object PB1: TProgressBar
        Left = 0
        Top = 436
        Width = 1191
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 1
        ExplicitTop = 428
        ExplicitWidth = 1189
      end
    end
  end
  object QrEmailProt: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEmailProtBeforeClose
    AfterScroll = QrEmailProtAfterScroll
    SQL.Strings = (
      'SELECT ptc.Codigo, ptc.Nome, COUNT(ppi.Conta) ITENS'
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'WHERE ppi.DataD = 0'
      'AND ptc.Tipo=2'
      'GROUP BY ptc.Codigo')
    Left = 572
    Top = 12
    object QrEmailProtCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmailProtNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrEmailProtITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsEmailProt: TDataSource
    DataSet = QrEmailProt
    Left = 600
    Top = 12
  end
  object QrEmailProtPak: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEmailProtPakBeforeClose
    AfterScroll = QrEmailProtPakAfterScroll
    SQL.Strings = (
      'SELECT ptp.Controle, ptp.Mez, COUNT(ppi.Conta) ITENS,'
      'CONCAT(RIGHT(Mez, 2), '#39'/'#39',  LEFT(Mez + 200000, 4)) NOMEMEZ'
      'FROM protpakits ppi'
      'LEFT JOIN protocopak ptp ON ptp.Controle=ppi.Controle'
      'WHERE ppi.DataD = 0'
      'AND ppi.DataE > 1'
      'AND ptp.Codigo=:P0'
      'GROUP BY ptp.Controle')
    Left = 628
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmailProtPakControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEmailProtPakMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrEmailProtPakNOMEMEZ: TWideStringField
      FieldName = 'NOMEMEZ'
      Size = 7
    end
    object QrEmailProtPakITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsEmailProtPak: TDataSource
    DataSet = QrEmailProtPak
    Left = 656
    Top = 12
  end
  object QrEmailProtPakIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEmailProtPakItsBeforeClose
    AfterScroll = QrEmailProtPakItsAfterScroll
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI, '
      'IF(ppi.DataE=0, "", '
      'DATE_FORMAT(ppi.DataE, "%d/%m/%y")) DATAE_TXT,'
      'ppi.Conta, ppi.DataE, ppi.Cliente, ppi.Depto, ppi.Docum,'
      'ppi.ID_Cod1, ppi.ID_Cod2, ppi.ID_Cod3, ppi.ID_Cod4, '
      'ppi.Vencto, ppi.Lancto'
      'FROM protpakits ppi'
      'LEFT JOIN entidades cli ON cli.Codigo=ppi.Cliente'
      'WHERE ppi.DataD = 0'
      'AND ppi.DataE > 1'
      'AND ppi.Controle=:P0')
    Left = 684
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmailProtPakItsConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEmailProtPakItsDataE: TDateField
      FieldName = 'DataE'
      Required = True
    end
    object QrEmailProtPakItsCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrEmailProtPakItsDepto: TIntegerField
      FieldName = 'Depto'
      Required = True
    end
    object QrEmailProtPakItsDocum: TFloatField
      FieldName = 'Docum'
      Required = True
    end
    object QrEmailProtPakItsNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrEmailProtPakItsDATAE_TXT: TWideStringField
      FieldName = 'DATAE_TXT'
      Size = 8
    end
    object QrEmailProtPakItsID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
      Required = True
    end
    object QrEmailProtPakItsID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
      Required = True
    end
    object QrEmailProtPakItsID_Cod3: TIntegerField
      FieldName = 'ID_Cod3'
      Required = True
    end
    object QrEmailProtPakItsID_Cod4: TIntegerField
      FieldName = 'ID_Cod4'
      Required = True
    end
    object QrEmailProtPakItsVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmailProtPakItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
  end
  object DsEmailProtPakIts: TDataSource
    DataSet = QrEmailProtPakIts
    Left = 712
    Top = 12
  end
  object QrProtPg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Registros'
      'FROM lanctos'
      'WHERE Tipo=2'
      'AND FatID >=600'
      'AND Sit =0  '
      'AND Compensado <= 1'
      'AND Mez=:P0'
      'AND Cliente=:P1'
      'AND Depto=:P2'
      'AND Documento=:P3')
    Left = 396
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrProtPgRegistros: TLargeintField
      FieldName = 'Registros'
      Required = True
    end
  end
  object QrRecebiBloq: TMySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT * '
      'FROM wpropaits'
      'WHERE Sincro=0')
    Left = 832
    Top = 201
  end
  object QrReenv: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI, '
      'IF(ppi.DataE=0, "", '
      'DATE_FORMAT(ppi.DataE, "%d/%m/%y")) DATAE_TXT,'
      'ppi.*'
      'FROM protpakits ppi'
      'LEFT JOIN entidades cli ON cli.Codigo=ppi.Cliente'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'WHERE ppi.DataD = 0'
      'AND ptc.Tipo=2'
      'AND ppi.DataE > 1')
    Left = 424
    Top = 360
    object QrReenvNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrReenvDATAE_TXT: TWideStringField
      FieldName = 'DATAE_TXT'
      Size = 8
    end
    object QrReenvCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'protpakits.Codigo'
    end
    object QrReenvControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'protpakits.Controle'
    end
    object QrReenvConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'protpakits.Conta'
    end
    object QrReenvDataE: TDateField
      FieldName = 'DataE'
      Origin = 'protpakits.DataE'
    end
    object QrReenvDataD: TDateTimeField
      FieldName = 'DataD'
      Origin = 'protpakits.DataD'
    end
    object QrReenvRetorna: TSmallintField
      FieldName = 'Retorna'
      Origin = 'protpakits.Retorna'
    end
    object QrReenvCancelado: TIntegerField
      FieldName = 'Cancelado'
      Origin = 'protpakits.Cancelado'
    end
    object QrReenvMotivo: TIntegerField
      FieldName = 'Motivo'
      Origin = 'protpakits.Motivo'
    end
    object QrReenvLink_ID: TIntegerField
      FieldName = 'Link_ID'
      Origin = 'protpakits.Link_ID'
    end
    object QrReenvCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'protpakits.CliInt'
    end
    object QrReenvCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'protpakits.Cliente'
    end
    object QrReenvFornece: TIntegerField
      FieldName = 'Fornece'
      Origin = 'protpakits.Fornece'
    end
    object QrReenvPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'protpakits.Periodo'
    end
    object QrReenvLancto: TIntegerField
      FieldName = 'Lancto'
      Origin = 'protpakits.Lancto'
    end
    object QrReenvDocum: TFloatField
      FieldName = 'Docum'
      Origin = 'protpakits.Docum'
    end
    object QrReenvDepto: TIntegerField
      FieldName = 'Depto'
      Origin = 'protpakits.Depto'
    end
    object QrReenvID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
      Origin = 'protpakits.ID_Cod1'
    end
    object QrReenvID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
      Origin = 'protpakits.ID_Cod2'
    end
    object QrReenvID_Cod3: TIntegerField
      FieldName = 'ID_Cod3'
      Origin = 'protpakits.ID_Cod3'
    end
    object QrReenvID_Cod4: TIntegerField
      FieldName = 'ID_Cod4'
      Origin = 'protpakits.ID_Cod4'
    end
    object QrReenvCedente: TIntegerField
      FieldName = 'Cedente'
      Origin = 'protpakits.Cedente'
    end
    object QrReenvVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'protpakits.Vencto'
    end
    object QrReenvValor: TFloatField
      FieldName = 'Valor'
      Origin = 'protpakits.Valor'
    end
    object QrReenvMoraDiaVal: TFloatField
      FieldName = 'MoraDiaVal'
      Origin = 'protpakits.MoraDiaVal'
    end
    object QrReenvMultaVal: TFloatField
      FieldName = 'MultaVal'
      Origin = 'protpakits.MultaVal'
    end
    object QrReenvComoConf: TSmallintField
      FieldName = 'ComoConf'
      Origin = 'protpakits.ComoConf'
    end
    object QrReenvSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Origin = 'protpakits.SerieCH'
      Size = 10
    end
    object QrReenvManual: TIntegerField
      FieldName = 'Manual'
      Origin = 'protpakits.Manual'
    end
    object QrReenvTexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'protpakits.Texto'
      Size = 30
    end
  end
  object QrEntiMail: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mai.Conta, mai.EMail,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT'
      'FROM entimail mai'
      'LEFT JOIN entidades ent ON ent.Codigo = mai.Codigo'
      'WHERE mai.EntiTipCto=:P0'
      'AND ent.Codigo=:P1')
    Left = 452
    Top = 359
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEntiMailConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrEntiMailEMail: TWideStringField
      FieldName = 'EMail'
      Size = 255
    end
    object QrEntiMailNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object QrPPIMail: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppi.Conta PROTOCOLO, ppi.Controle LOTE, ppi.DataE, '
      
        'IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DATAE_TX' +
        'T,'
      
        'IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DATAD_TX' +
        'T,'
      'ppi.DataD, ppi.Cancelado, ppi.Motivo, ppi.ID_Cod1, ppi.ID_Cod2, '
      'ppi.ID_Cod3, ppi.ID_Cod4, ppi.CliInt, ppi.Cliente, ppi.Periodo,'
      
        'ppi.Docum, ptc.Codigo TAREFA_COD, ptc.Nome TAREFA_NOM, ptc.Def_S' +
        'ender,'
      'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, ptc.PreEmeio'
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'LEFT JOIN entidades  snd ON snd.Codigo=ptc.Def_Sender'
      'WHERE ppi.Link_ID=1'
      'AND ppi.ID_Cod1=:P0'
      'AND ppi.ID_Cod2=:P1'
      'AND ppi.ID_Cod3=:P2')
    Left = 488
    Top = 359
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPPIMailPROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      Required = True
    end
    object QrPPIMailLOTE: TIntegerField
      FieldName = 'LOTE'
      Required = True
    end
    object QrPPIMailDataE: TDateField
      FieldName = 'DataE'
      Required = True
    end
    object QrPPIMailDATAE_TXT: TWideStringField
      FieldName = 'DATAE_TXT'
      Size = 10
    end
    object QrPPIMailDATAD_TXT: TWideStringField
      FieldName = 'DATAD_TXT'
      Size = 10
    end
    object QrPPIMailCancelado: TIntegerField
      FieldName = 'Cancelado'
      Required = True
    end
    object QrPPIMailMotivo: TIntegerField
      FieldName = 'Motivo'
      Required = True
    end
    object QrPPIMailID_Cod1: TIntegerField
      FieldName = 'ID_Cod1'
      Required = True
    end
    object QrPPIMailID_Cod2: TIntegerField
      FieldName = 'ID_Cod2'
      Required = True
    end
    object QrPPIMailID_Cod3: TIntegerField
      FieldName = 'ID_Cod3'
      Required = True
    end
    object QrPPIMailID_Cod4: TIntegerField
      FieldName = 'ID_Cod4'
      Required = True
    end
    object QrPPIMailCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrPPIMailCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrPPIMailPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrPPIMailDef_Sender: TIntegerField
      FieldName = 'Def_Sender'
    end
    object QrPPIMailDELIVER: TWideStringField
      FieldName = 'DELIVER'
      Size = 100
    end
    object QrPPIMailDocum: TFloatField
      FieldName = 'Docum'
      Required = True
    end
    object QrPPIMailPreEmeio: TIntegerField
      FieldName = 'PreEmeio'
    end
    object QrPPIMailDataD: TDateTimeField
      FieldName = 'DataD'
      Required = True
    end
    object QrPPIMailTAREFA_COD: TIntegerField
      FieldName = 'TAREFA_COD'
      Required = True
    end
    object QrPPIMailTAREFA_NOM: TWideStringField
      FieldName = 'TAREFA_NOM'
      Size = 100
    end
  end
  object QrBloOpcoes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM bloopcoes')
    Left = 501
    Top = 263
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 832
    Top = 11
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 804
    Top = 11
  end
  object QrPTK: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPTKBeforeClose
    AfterScroll = QrPTKAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT ptc.Nome, ptk.Controle Lote, ptk.Mez'
      'FROM protpakits ppi'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo'
      'LEFT JOIN protocopak ptk ON ptk.Controle=ppi.Controle'
      'WHERE ('
      '  (Saiu=0 AND LimiteSai <= SYSDATE())'
      '   OR '
      '  (Recebeu=0 AND LimiteRem <= SYSDATE())'
      '   OR '
      '  (Retorna=1 AND Retornou=0 AND LimiteRet <= SYSDATE()))'
      'AND ppi.Cancelado=0'
      'ORDER BY ptk.Mez, ptk.Controle')
    Left = 424
    Top = 12
    object QrPTKNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrPTKLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrPTKMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPTKMez_TXT: TWideStringField
      FieldName = 'Mez_TXT'
      Size = 5
    end
    object QrPTKDef_Client: TIntegerField
      FieldName = 'Def_Client'
    end
    object QrPTKDef_Sender: TIntegerField
      FieldName = 'Def_Sender'
    end
    object QrPTKDef_Client_TXT: TWideStringField
      FieldName = 'Def_Client_TXT'
      Size = 100
    end
  end
  object DsPTK: TDataSource
    DataSet = QrPTK
    Left = 452
    Top = 12
  end
  object QrPPI: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPPIBeforeClose
    AfterScroll = QrPPIAfterScroll
    SQL.Strings = (
      'SELECT ppi.DataE, ppi.Conta, ppi.Retorna,'
      'IF(ppi.Retorna=0,"N","S") NO_RETORNA,'
      'IF(ppi.Manual=0,"",pto.Nome) NO_Motivo,'
      'ppi.LimiteSai, ppi.Saiu, ppi.DataSai,'
      'ppi.LimiteRem, ppi.Recebeu, ppi.DataRec,'
      'ppi.LimiteRet, ppi.Retornou, ppi.DataRet,'
      'ppi.Texto, ppi.Lancto, ppi.Docum'
      'FROM protpakits ppi'
      'LEFT JOIN protocopak ptk ON ptk.Controle=ppi.Controle'
      'LEFT JOIN protocooco pto ON pto.Codigo=ppi.Manual'
      'WHERE ('
      '  (Saiu=0 AND LimiteSai <= SYSDATE())'
      '   OR'
      '  (Recebeu=0 AND LimiteRem <= SYSDATE())'
      '   OR'
      '  (Retorna=1 AND Retornou=0 AND LimiteRet <= SYSDATE()))'
      'AND ppi.Cancelado=0'
      'AND ptk.Controle=:P0'
      'ORDER BY DataD, Conta'
      '')
    Left = 480
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPPIDataE: TDateField
      FieldName = 'DataE'
    end
    object QrPPINO_RETORNA: TWideStringField
      FieldName = 'NO_RETORNA'
      Required = True
      Size = 1
    end
    object QrPPINO_Motivo: TWideStringField
      FieldName = 'NO_Motivo'
      Size = 50
    end
    object QrPPILimiteSai: TDateField
      FieldName = 'LimiteSai'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPISaiu: TIntegerField
      FieldName = 'Saiu'
      MaxValue = 1
    end
    object QrPPIDataSai: TDateTimeField
      FieldName = 'DataSai'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPILimiteRem: TDateField
      FieldName = 'LimiteRem'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPIRecebeu: TIntegerField
      FieldName = 'Recebeu'
      MaxValue = 1
    end
    object QrPPIDataRec: TDateTimeField
      FieldName = 'DataRec'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPILimiteRet: TDateField
      FieldName = 'LimiteRet'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPIRetornou: TIntegerField
      FieldName = 'Retornou'
      MaxValue = 1
    end
    object QrPPIDataRet: TDateTimeField
      FieldName = 'DataRet'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPPIConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPPIRetorna: TSmallintField
      FieldName = 'Retorna'
    end
    object QrPPITexto: TWideStringField
      FieldName = 'Texto'
      Size = 30
    end
    object QrPPILancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrPPIDocum: TFloatField
      FieldName = 'Docum'
    end
    object QrPPIDataSai_TXT: TWideStringField
      FieldName = 'DataSai_TXT'
      Size = 8
    end
    object QrPPIDataRec_TXT: TWideStringField
      FieldName = 'DataRec_TXT'
      Size = 8
    end
    object QrPPIDataRet_TXT: TWideStringField
      FieldName = 'DataRet_TXT'
      Size = 8
    end
  end
  object DsPPI: TDataSource
    DataSet = QrPPI
    Left = 508
    Top = 12
  end
  object QrSender: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 668
    Top = 320
    object QrSenderCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSenderNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsSender: TDataSource
    DataSet = QrSender
    Left = 696
    Top = 320
  end
  object QrEmpresas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Filial, ent.CliInt, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, IE, NIRE'
      'FROM senhasits sei'
      'LEFT JOIN entidades ent ON ent.Codigo=sei.Empresa'
      'WHERE sei.Numero=:P0'
      'AND ent.Codigo < -10')
    Left = 724
    Top = 320
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmpresasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmpresasFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrEmpresasNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Size = 100
    end
    object QrEmpresasCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEmpresasIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEmpresasNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
  end
  object DsEmpresas: TDataSource
    DataSet = QrEmpresas
    Left = 753
    Top = 320
  end
  object QrGraficoStatus: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM bloopcoes')
    Left = 717
    Top = 207
  end
  object QrSender2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 668
    Top = 376
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsSender2: TDataSource
    DataSet = QrSender2
    Left = 696
    Top = 376
  end
  object QrEmpresas2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Filial, ent.CliInt, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, IE, NIRE'
      'FROM senhasits sei'
      'LEFT JOIN entidades ent ON ent.Codigo=sei.Empresa'
      'WHERE sei.Numero=:P0'
      'AND ent.Codigo < -10')
    Left = 724
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmpresas2Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmpresas2Filial: TIntegerField
      FieldName = 'Filial'
    end
    object QrEmpresas2NOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Size = 100
    end
    object QrEmpresas2CNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEmpresas2IE: TWideStringField
      FieldName = 'IE'
    end
    object QrEmpresas2NIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
  end
  object DsEmpresas2: TDataSource
    DataSet = QrEmpresas2
    Left = 753
    Top = 376
  end
  object DsProtocolos: TDataSource
    DataSet = QrProtocolos
    Left = 810
    Top = 375
  end
  object QrProtocolos: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrProtocolosBeforeClose
    AfterScroll = QrProtocolosAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM protocolos'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 781
    Top = 375
    object QrProtocolosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrProtocolosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrProtocoPak: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'#11
      'FROM protocopak'
      'WHERE Codigo=:P0'
      'ORDER BY Controle ASC')
    Left = 838
    Top = 375
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProtocoPakControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsProtocoPak: TDataSource
    DataSet = QrProtocoPak
    Left = 867
    Top = 375
  end
  object QrEmpresas3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Filial, ent.CliInt, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, IE, NIRE'
      'FROM senhasits sei'
      'LEFT JOIN entidades ent ON ent.Codigo=sei.Empresa'
      'WHERE sei.Numero=:P0'
      'AND ent.Codigo < -10')
    Left = 782
    Top = 320
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmpresas3Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmpresas3Filial: TIntegerField
      FieldName = 'Filial'
    end
    object QrEmpresas3NOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Size = 100
    end
    object QrEmpresas3CNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEmpresas3IE: TWideStringField
      FieldName = 'IE'
    end
    object QrEmpresas3NIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
  end
  object DsEmpresas3: TDataSource
    DataSet = QrEmpresas3
    Left = 811
    Top = 320
  end
end
