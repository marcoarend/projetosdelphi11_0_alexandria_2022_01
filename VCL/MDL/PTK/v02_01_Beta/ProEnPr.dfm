object FmProEnPr: TFmProEnPr
  Left = 368
  Top = 194
  Caption = 'GER-PROTO-010 :: Protocolos Padr'#227'o'
  ClientHeight = 509
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 413
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 130
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label8: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 92
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Protocolo:'
        FocusControl = DBEdNome
      end
      object SpeedButton5: TSpeedButton
        Left = 755
        Top = 20
        Width = 21
        Height = 21
        Hint = 'Inclui item de carteira'
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CBProtocolo: TdmkDBLookupComboBox
        Left = 149
        Top = 20
        Width = 605
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsProtocolos
        TabOrder = 2
        dmkEditCB = EdProtocolo
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdProtocolo: TdmkEditCB
        Left = 92
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBProtocolo
        IgnoraDBLookupComboBox = False
      end
      object CkAtivo: TdmkCheckBox
        Left = 8
        Top = 103
        Width = 45
        Height = 17
        Caption = 'Ativo'
        TabOrder = 3
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object CkContinuar: TCheckBox
        Left = 71
        Top = 103
        Width = 121
        Height = 17
        Caption = 'Continuar inserindo.'
        TabOrder = 4
      end
      object RGFinalidade: TRadioGroup
        Left = 8
        Top = 47
        Width = 746
        Height = 45
        Caption = ' Finalidade '
        Columns = 3
        Items.Strings = (
          'Item1'
          'Item2'
          'Item3')
        TabOrder = 5
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 349
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 788
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 644
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 413
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 100
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label2: TLabel
        Left = 92
        Top = 4
        Width = 45
        Height = 13
        Caption = 'Protocolo'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object DBEdNome: TdmkDBEdit
        Left = 92
        Top = 20
        Width = 688
        Height = 21
        Color = clWhite
        DataField = 'NOMEPROT'
        DataSource = DsProEnPr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 8
        Top = 20
        Width = 80
        Height = 21
        DataField = 'Codigo'
        DataSource = DsProEnPr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object DBRGFinalidade: TDBRadioGroup
        Left = 8
        Top = 47
        Width = 772
        Height = 45
        Caption = ' Finalidade '
        Columns = 3
        DataField = 'Finalidade'
        DataSource = DsProEnPr
        Items.Strings = (
          'Item1'
          'Item2'
          'Item3')
        ParentBackground = True
        TabOrder = 2
        Values.Strings = (
          '0'
          '1'
          '2')
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 100
      Width = 792
      Height = 236
      Align = alTop
      TabOrder = 1
      object dmkDBGrid1: TdmkDBGrid
        Left = 1
        Top = 1
        Width = 790
        Height = 119
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEENT'
            Title.Caption = 'Entidade'
            Width = 653
            Visible = True
          end>
        Color = clWindow
        DataSource = DsProEnPrIt
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEENT'
            Title.Caption = 'Entidade'
            Width = 653
            Visible = True
          end>
      end
      object PnEnti: TPanel
        Left = 1
        Top = 120
        Width = 790
        Height = 115
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object Label1: TLabel
          Left = 9
          Top = 6
          Width = 45
          Height = 13
          Caption = 'Entidade:'
        end
        object EdEntidade: TdmkEditCB
          Left = 9
          Top = 22
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEntidade
          IgnoraDBLookupComboBox = False
        end
        object CBEntidade: TdmkDBLookupComboBox
          Left = 65
          Top = 22
          Width = 585
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'NOMEENT'
          ListSource = DsEntidades
          TabOrder = 1
          dmkEditCB = EdEntidade
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CkContinuarEnt: TCheckBox
          Left = 654
          Top = 23
          Width = 121
          Height = 17
          Caption = 'Continuar inserindo.'
          TabOrder = 2
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 51
          Width = 790
          Height = 64
          Align = alBottom
          TabOrder = 3
          object Panel7: TPanel
            Left = 2
            Top = 15
            Width = 786
            Height = 47
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel8: TPanel
              Left = 642
              Top = 0
              Width = 144
              Height = 47
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object BitBtn5: TBitBtn
                Tag = 15
                Left = 16
                Top = 2
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Caption = '&Desiste'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BitBtn5Click
              end
            end
            object BitBtn4: TBitBtn
              Tag = 14
              Left = 7
              Top = 2
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Caption = '&Confirma'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BitBtn4Click
            end
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 349
      Width = 792
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 95
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 269
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtProtocolo: TBitBtn
          Tag = 10051
          Left = 6
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Protocolo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtProtocoloClick
        end
        object BtEntidade: TBitBtn
          Tag = 132
          Left = 129
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Entidades'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtEntidadeClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 528
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 222
        Height = 32
        Caption = 'Protocolos Padr'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 222
        Height = 32
        Caption = 'Protocolos Padr'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 222
        Height = 32
        Caption = 'Protocolos Padr'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 792
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsProEnPr: TDataSource
    DataSet = QrProEnPr
    Left = 414
    Top = 249
  end
  object QrProEnPr: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrProEnPrBeforeOpen
    AfterOpen = QrProEnPrAfterOpen
    BeforeClose = QrProEnPrBeforeClose
    AfterScroll = QrProEnPrAfterScroll
    SQL.Strings = (
      'SELECT blo.*, pro.Nome NOMEPROT, pro.Def_Client'
      'FROM proenpr blo'
      'LEFT JOIN protocolos pro ON pro.Codigo = blo.Protocolo')
    Left = 386
    Top = 249
    object QrProEnPrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProEnPrProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrProEnPrLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrProEnPrDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProEnPrDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProEnPrUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrProEnPrUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrProEnPrAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrProEnPrAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrProEnPrNOMEPROT: TWideStringField
      FieldName = 'NOMEPROT'
      Size = 100
    end
    object QrProEnPrFinalidade: TIntegerField
      FieldName = 'Finalidade'
    end
    object QrProEnPrDef_Client: TIntegerField
      FieldName = 'Def_Client'
    end
  end
  object QrProtocolos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo'
      'FROM Protocolos'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 218
    Top = 225
    object QrProtocolosCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrProtocolosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrProtocolosTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsProtocolos: TDataSource
    DataSet = QrProtocolos
    Left = 246
    Top = 225
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 274
    Top = 225
  end
  object PMProtocolo: TPopupMenu
    OnPopup = PMProtocoloPopup
    Left = 360
    Top = 358
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
  end
  object PMEntidade: TPopupMenu
    OnPopup = PMEntidadePopup
    Left = 448
    Top = 358
    object Inclui2: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui2Click
    end
    object Exclui2: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui2Click
    end
  end
  object QrProEnPrIt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT its.*,  IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEEN' +
        'T'
      'FROM proenprit its'
      'LEFT JOIN entidades ent ON ent.Codigo = its.Entidade'
      'WHERE its.Codigo=:P0'
      'ORDER BY NOMEENT')
    Left = 442
    Top = 249
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProEnPrItCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProEnPrItControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrProEnPrItEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrProEnPrItLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrProEnPrItDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProEnPrItDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProEnPrItUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrProEnPrItUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrProEnPrItAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrProEnPrItAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrProEnPrItNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object DsProEnPrIt: TDataSource
    DataSet = QrProEnPrIt
    Left = 470
    Top = 249
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Inclui1
    CanIns02 = Inclui2
    CanUpd01 = Altera1
    CanDel01 = Exclui1
    CanDel02 = Exclui2
    Left = 358
    Top = 249
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, IF (Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 218
    Top = 253
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntidadesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 246
    Top = 253
  end
  object VUEntidade: TdmkValUsu
    dmkEditCB = EdEntidade
    Panel = PnEnti
    QryCampo = 'Entidade'
    UpdCampo = 'Entidade'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 498
    Top = 249
  end
end
