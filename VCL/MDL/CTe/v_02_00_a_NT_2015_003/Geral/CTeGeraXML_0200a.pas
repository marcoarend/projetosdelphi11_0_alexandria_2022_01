unit CTeGeraXML_0200a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, Variants, Math,
  IBCustomDataSet, IBQuery, Clipbrd, DB, DBClient, dmkGeral, UrlMon,
  InvokeRegistry, SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, JwaWinCrypt,
  WinInet, XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB, dmkImage, dmkEdit,
  // Consulta status servi�o
  consStatServCTe_v200,
  // Modal Rodoviario
  cteModalRodoviario_v200,
  // Consulta situa��o CT-e
  consSitCTe_v200,
  // Pede inutiliza��o CT-e
  inutCTe_v200,
  // Gera CTe
  cte_v200,
  // Envia lote de CT-es
  // ???
  // Consulta Lote de CT-es
  consReciCTe_v200,
  // Consulta cadastro contribuinte
  //consCad_v200,
  //
  XSBuiltIns, StrUtils, ComObj, SoapConst, mySQLDbTables,
  UnDmkEnums, UnCTeListas;

const
  NO_SEM_VALOR = 'CT-E EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';
  FVersaoModal_Rodoviario = 2.00;
{
  //FVersaoWS           = '3.10'; // 0310
  N F e_AllModelos      = '55'; // SQL 55,?,?
  N F e_CodAutorizaTxt  = '100';
  N F e_CodCanceladTxt  = '101';
  N F e_CodInutilizTxt  = '102';
  N F e_CodDenegadoTxt  = '110,301,302';
  // XML
}
  sXML_Version        = '1.0';
  sXML_Encoding       = 'UTF-8';
  ENCODING_UTF8 = '?xml version="1.0" encoding="UTF-8"?';
  ENCODING_UTF8_STD = '?xml version="1.0" encoding="UTF-8" standalone="no"?';
{
  NAME_SPACE = 'xmlns="http://www.portalfiscal.inf.br/cte"';
  NAME_SPACE_CTE = 'xmlns="http://www.portalfiscal.inf.br/cte"';
  sCampoNulo = '#NULL#';
}

type
  TFmCTeGeraXML_0200a = class(TForm)
    Panel1: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
  private
    { Private declarations }
    //
    procedure ConfiguraReqResp(ReqResp: THTTPReqResp);
    function  CriarDocumentoCTe(FatID, FatNum, Empresa: Integer;
              var XMLGerado_Arq, XMLGerado_Dir: String; LaAviso1, LaAviso2: TLabel;
              GravaCampos: Integer): Boolean;
    function  MontaID_Inutilizacao(const cUF, Ano, emitCNPJ, Modelo, Serie, nCTIni,
              nCTFim: String; var Id: String): Boolean;
    procedure OnBeforePost(const HTTPReqResp: THTTPReqResp;
              Data: Pointer);
    function  SeparaDados(Texto: String; Chave: String; MantemChave:
              Boolean = False): String;
    function  TipoXML(NoStandAlone: Boolean): String;
    // Montagem do CTe
    function  GerarXMLdoCTe(FatID, FatNum, Empresa: Integer; const cteID: String;
              GravaCampos: Integer): Boolean;
    function  GerarXMLmodalRodo(FatID, FatNum, Empresa: Integer;
              GravaCampos: Integer): Boolean;
    function  Def(const Grupo, Codigo: Integer; const Source: Variant;
              var Dest: String): Boolean;
    function  Def_UTC(const Grupo, Codigo: Integer; const Source1, Source2,
              Source3: Variant; var Dest: String): Boolean;
  public
    { Public declarations }
    function  GerarArquivoXMLdoCTe(FatID, FatNum, Empresa: Integer;
              LaAviso1, LaAviso2: TLabel; GravaCampos: Integer): Boolean;
    function  GerarLoteCTeNovo(Lote, Empresa: Integer; out PathLote: String;
              out XML_Lote: String; LaAviso1, LaAviso2: TLabel; Sincronia:
              TXXeIndSinc): Boolean;
    function  NomeAcao(Acao: TTipoConsumoWS_CTe): String;
    function  ObtemWebServer2(UFServico: String; Ambiente, CodigoUF: Integer;
              Acao: TTipoConsumoWS_CTe; sAviso: String): Boolean;
    function  VersaoWS(Acao: TTipoConsumoWS_CTe; Formata: Boolean = True): String;
    function  WS_CTeConsultaCT(UFServico: String; Ambiente, CodigoUF: Byte;
              ChaveCTe: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio:
              TMemo; EdWS: TEdit): String;
    function  WS_CTeInutilizacaoCTe(UFServico: String; Ambiente, CodigoUF, Ano: Byte;
              Id, CNPJ, Mod_, Serie, NCTIni, NCTFin: String;
              XJust, NumeroSerial: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio:
              TMemo; EdWS: TEdit; var ID_Res: String): String;
    function  WS_EventoCancelamentoCTe(UFServico: String; Ambiente, CodigoUF:
              Byte; XML_Evento: String; TipoConsumo: TTipoConsumoWS_CTe;
              RETxtEnvio: TMemo; EdWS: TEdit): String;
    function  WS_EventoCartaDeCorrecao(UFServico: String; Ambiente, CodigoUF:
              Byte; XML_Evento: String; TipoConsumo: TTipoConsumoWS_CTe;
              RETxtEnvio: TMemo; EdWS: TEdit): String;
    function  WS_EventoEPEC(UFServico: String; Ambiente, CodigoUF:
              Byte; XML_Evento: String; TipoConsumo: TTipoConsumoWS_CTe;
              RETxtEnvio: TMemo; EdWS: TEdit): String;
    function  WS_CTeRecepcaoLote(UFServico: String; Ambiente, CodigoUF: Byte;
              NumeroSerial: String; Lote: Integer; LaAviso1, LaAviso2: TLabel;
              RETxtEnvio: TMemo; EdWS: TEdit; Sincronia: TXXeIndSinc): String;
    function  WS_CTeRetRecepcao(UFServico: String; Ambiente, CodigoUF: Byte;
              Recibo: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
              EdWS: TEdit): String;
    function  WS_CTeStatusServico(UFServico: String; Ambiente,
              CodigoUF: Byte; Certificado: String; LaAviso1, LaAviso2: TLabel;
              RETxtEnvio: TMemo; EdWS: TEdit): String;
    function  XML_CTeInutCTe(var Id: String; TpAmb, CUF: Byte; Ano: Integer;
              CNPJ, Mod_, Serie, NCTIni, NCTFin, XJust: String): String;
    function  XML_ConsReciCTe(TpAmb: Integer; NRec: String): String;
    function  XML_ConsSitCTe(TpAmb: Integer; ChCTe, VersaoAcao: String): String;
    function  XML_ConsStatServ(TpAmb, CUF: Integer): String;

  end;
  var
  FmCTeGeraXML_0200a: TFmCTeGeraXML_0200a;

implementation

uses UnMyObjects, Module, UnInternalConsts, UMySQLModule, ModuleGeral,
  ModuleCTe_0000, UnXXe_PF, CTeSteps_0200a,
  //N F e XMLGerencia,
  UnCTe_PF,
  //Recibo,
  DmkDAC_PF, UnDmkProcFunc, Recibo;

var
  FGravaCampo: Integer;
  FLaAviso1, FLaAviso2: TLabel;
  FdocXML: TXMLDocument;
{
  FCabecTxt,}
  FAssinTxt: String;
  //FWSDL,
  FDadosTxt, FURL: String;
{
  //  ACBr
  CertStore     : IStore3;
  CertStoreMem  : IStore3;
  PrivateKey    : IPrivateKey;
  Certs         : ICertificates2;}
  Cert          : ICertificate2;
{
  NumCertCarregado : String;
  //
  NomeArquivo: String;
  CaminhoArquivo: String;
  strTpAmb: string;
}
  SerieCT: String;
  NumeroCT: String;
  CDV: String;
  modalXML: String;
  cXML: IXMLTCTe;
  cRodo: IXMLRodo;
  cRodo_occ: IXMLRodo_occ;
  cRodo_valePed: IXMLRodo_valePed;
  cRodo_veic: IXMLRodo_veic;
  cRodo_veic_prop: IXMLRodo_veic_prop;
  cRodo_lacRodo: IXMLRodo_lacRodo;
  cRodo_moto: IXMLRodo_moto;
  arqXML: TXMLDocument;
  (* Objetos do Documento XML... *)
  cObsCont: IXMLObsCont;
  cObsFisco: IXMLObsFisco;
  cComp: IXMLComp;
  cInfQ: IXMLInfQ;
  cInfNF: IXMLInfNF;
  cInfNFe: IXMLInfNFe;
  cInfOutros: IXMLInfOutros;
  cPeri: IXMLPeri;
  cSeg: IXMLSeg;
  cDup: IXMLDup;
  cAutXml: IXMLAutXml;
  //
  rodoXML: TXMLDocument;
  //
  FFatID,
  FFatNum,
  FEmpresa,
  FOrdem: Integer;



{$R *.DFM}

{ TFmCTeGeraXML_0200a }

procedure TFmCTeGeraXML_0200a.ConfiguraReqResp(ReqResp: THTTPReqResp);
begin
  ReqResp.OnBeforePost := OnBeforePost;
end;

function TFmCTeGeraXML_0200a.CriarDocumentoCTe(FatID, FatNum, Empresa: Integer;
  var XMLGerado_Arq, XMLGerado_Dir: String; LaAviso1, LaAviso2: TLabel;
  GravaCampos: Integer): Boolean;
var
  strChaveAcesso: String;
  TextoXML: String;
begin
  Result := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM ctexmli WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
  Dmod.QrUpd.Params[00].AsInteger := FatID;
  Dmod.QrUpd.Params[01].AsInteger := FatNum;
  Dmod.QrUpd.Params[02].AsInteger := Empresa;
  Dmod.QrUpd.ExecSQL;
  //
  FFatID   := FatID;
  FFatNum  := FatNum;
  FEmpresa := Empresa;
  FOrdem   := 0;
  //
  DmCTe_0000.ReopenCTeCabA(FatID, FatNum, Empresa);
  //
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  cXML := GetCTe(arqXML);
  arqXML.Version := '1.0';
  arqXML.Encoding := 'UTF-8';
  {
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/cte/enviCTe_v1.12.xsd';
  }

  SerieCT := FormatFloat('000', DmCTe_0000.QrCTeCabAide_Serie.Value);
  NumeroCT := FormatFloat('00000000', DmCTe_0000.QrCTeCabAide_cCT.Value);

  (* Montar a Chave de Acesso da CTe de acordo com as informa��es do Registro...*)
  (* cUF=??,dEmi=...*)
  strChaveAcesso := DmCTe_0000.QrCTeCabAId.Value;

  CDV := Copy(strChaveAcesso, Length(strChaveAcesso), Length(strChaveAcesso));
  if GerarXMLdoCTe(FatID, FatNum, Empresa, 'CTe' + strChaveAcesso, GravaCampos) then
  begin
    DmCTe_0000.ReopenEmpresa(Empresa);
    //
    //modalXML := rodoXml.XML.Text;
    modalXML := Geral.Substitui(modalXML, ' xmlns="http://www.portalfiscal.inf.br/cte"', '');
    modalXml := Geral.Substitui(modalXML, slineBreak, '');
    //
    TextoXML := arqXML.XML.Text;
    //Geral.MB_Info(TextoXML);
    TextoXML := Geral.Substitui(TextoXML, '<_></_>', modalXML);
    DmCTe_0000.SalvaXML(CTE_EXT_CTE_XML, strChaveAcesso, TextoXML, nil, False);
    //
    Result := True;
  end;
  arqXML := nil;
end;

function TFmCTeGeraXML_0200a.Def(const Grupo, Codigo: Integer;
  const Source: Variant; var Dest: String): Boolean;
const
  sFmtDtHr = 'YYYY-MM-DDTHH:NN:SS';
var
  Continua, Avisa: Boolean;
  Tipo, FormatStr: String;
  TipoVar: TVarType;
begin
  //versao := ERf(0, 2, QrOpcoesCTeCTeversao.Value, 2);
  Result := False;
  Dest   := '';
  if DmCTe_0000.QrCTeLayI.Locate('Grupo;Codigo', VarArrayOf([Grupo, Codigo]), []) then
  begin
    if DmCTe_0000.QrCTeLayIOcorMin.Value > 0 then
    begin
(*    Quntidade na N F e
      if (Codigo = '255') and (ID = 'O11') and (Source = 0) then
        Continua := False else
      if (Codigo = '256') and (ID = 'O12') and (Source = 0) then
        Continua := False else
*)
        Continua := True;
    end else
      Continua := XXe_PF.VarTypeValido(Source, Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo));
    if Continua then
    begin
      Result := True;
      //
      TipoVar   := VarType(Source);
      Tipo      := Uppercase(DmCTe_0000.QrCTeLayITipo.Value);
      FormatStr := Uppercase(DmCTe_0000.QrCTeLayIFormatStr.Value);
      if (Tipo = 'C') and (TipoVar = varDate) then
        Tipo := 'D';
      //
      // n�mero
      if Tipo = 'N' then
      begin
        // Texto
        if (TipoVar = varString) or (TipoVar = varUString) then
          Dest := Source
        //Double
        else if DmCTe_0000.QrCTeLayIDeciCasas.Value > 0 then
          Dest := XXe_PF.DecimalPonto(FloatToStrF(Source, ffFixed, 15,
            DmCTe_0000.QrCTeLayIDeciCasas.Value))
        else begin
        //integer
          (*if DmCTe_0000.QrCTeLayILeftZeros.Value = 1 then
            Dest := XXe_PF.DecimalPonto(Geral.TFD(FloatToStr(Source),
            DmCTe_0000.QrCTeLayITamMax.Value, siPositivo))
          else*)
            Dest := XXe_PF.DecimalPonto(FloatToStr(Source));
            while Length(Dest) < DmCTe_0000.QrCTeLayITamMin.Value do
              Dest := '0' + Dest;
        end;
      end else
      // data
      if Tipo = 'D' then
      begin
        if FormatStr = sFmtDtHr then
        begin
          Dest := FormatDateTime('yyyy-mm-dd"T"hh:nn:ss', Source);
        end else
        begin
          if DmCTe_0000.QrCTeLayIFormatStr.Value <> 'YYYY-MM-DD' then
            Geral.MB_Aviso('Este � apenas um aviso: "' +
            DmCTe_0000.QrCTeLayIFormatStr.Value + '". Informe a DERMATEK!' +
            sLineBreak + 'Grupo: ' + Geral.FF0(Grupo) +
            sLineBreak + 'C�digo: ' + Geral.FF0(Codigo) + sLineBreak);
          Dest := FormatDateTime('yyyy-mm-dd', Source);
        end;
      end else
      // hora
      if Uppercase(DmCTe_0000.QrCTeLayITipo.Value) = 'H' then
        Dest := FormatDateTime('hh:nn:ss', Source)
      else
      // texto
      if Uppercase(DmCTe_0000.QrCTeLayITipo.Value) = 'C' then
        Dest := Source
      else
      // desconhecido
      begin
        Dest := Source;
        Geral.MB_Aviso(DmCTe_0000.MensagemDeID_CTe(Grupo, Codigo, Dest,
        'Tipo de formata��o desconhecida:', 1));
      end;
      //
      Dest := Trim(XXe_PF.ValidaTexto_XML(Dest, Geral.FF0(Grupo), Geral.FF0(Codigo)));
      if (Dest = '') and (DmCTe_0000.QrCTeLayIOcorMin.Value > 0) then
      begin
        // verificar se � vazio obrigat�rio
        if DmCTe_0000.QrCTeLayIInfoVazio.Value = 0 then
        begin
          Avisa := True;
(*
          if (Codigo = '78') and (ID = 'E17') and
          ((DmCTe_0000.QrCTeCabAdest_CNPJ.Value = '') or
          (Geral.SoNumero1a9_TT(DmCTe_0000.QrCTeCabAdest_CNPJ.Value) = '')) then
            Avisa := False;
*)
          if Avisa then
            Geral.MB_Aviso(DmCTe_0000.MensagemDeID_CTe(Grupo, Codigo, Dest,
            'Valor n�o definido:', 2));
        end;
      end;
      if FGravaCampo = ID_YES then
      begin
        FOrdem := FOrdem + 1;
        //
        MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Inserindo valor de XML no banco de dados.'
        + IntToStr(FOrdem));
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ctexmli', False, [
        'Codigo', 'ID', 'Valor'], [
        'FatID', 'FatNum', 'Empresa', 'Ordem'], [
        Grupo, Codigo, Dest], [
        FFatID, FFatNum, FEmpresa, FOrdem], False);
      end;
    end;
  end else Geral.MB_Erro(DmCTe_0000.MensagemDeID_CTe(Grupo, Codigo, Dest,
  'N�o foi poss�vel definir o valor', 3));
end;

function TFmCTeGeraXML_0200a.Def_UTC(const Grupo, Codigo: Integer;
  const Source1, Source2, Source3: Variant; var Dest: String): Boolean;
var
  Continua, Avisa: Boolean;
begin
  //versao := ERf(0, 2, QrOpcoesCTeCTeversao.Value, 2);
  Result := False;
  Dest   := '';
  if DmCTe_0000.QrCTeLayI.Locate('Grupo;Codigo', VarArrayOf([Grupo, Codigo]), []) then
  begin
    if DmCTe_0000.QrCTeLayIOcorMin.Value > 0 then
    begin
(*    Quntidade na N F e
      if (Codigo = '255') and (ID = 'O11') and (Source = 0) then
        Continua := False else
      if (Codigo = '256') and (ID = 'O12') and (Source = 0) then
        Continua := False else
*)
        Continua := True;
    end else
      Continua := XXe_PF.VarTypeValido(Source1, Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo));
    if Continua then
    begin
      Result := True;
      // n�mero
      if Uppercase(DmCTe_0000.QrCTeLayITipo.Value) = 'N' then
      begin
        // Texto
        (*if (Codigo = '17') and (ID = 'B13') then
          Dest := Source
         Double
        else*) if DmCTe_0000.QrCTeLayIDeciCasas.Value > 0 then
          Dest := XXe_PF.DecimalPonto(FloatToStrF(Source1, ffFixed, 15,
            DmCTe_0000.QrCTeLayIDeciCasas.Value))
        else begin
        //integer
          (*if DmCTe_0000.QrCTeLayILeftZeros.Value = 1 then
            Dest := XXe_PF.DecimalPonto(Geral.TFD(FloatToStr(Source),
            DmCTe_0000.QrCTeLayITamMax.Value, siPositivo))
          else*)
            Dest := XXe_PF.DecimalPonto(FloatToStr(Source1));
        end;
      end else
      // data
      if Uppercase(DmCTe_0000.QrCTeLayITipo.Value) = 'D' then
      begin
        if DmCTe_0000.QrCTeLayIFormatStr.Value = 'YYYY-MM-DD''T''HH:NN:SS' then
        begin
          Dest := FormatDateTime('yyyy-mm-dd"T"hh:nn:ss', Source1 + Source2);
        end else
        if DmCTe_0000.QrCTeLayIFormatStr.Value = 'YYYY-MM-DD''T''HH:NN:SSTZD' then
        begin
          Dest := FormatDateTime('yyyy-mm-dd"T"hh:nn:ss', Source1 + Source2) +
                  dmkPF.TZD_UTC_FloatToSignedStr(Source3);
        end else
        begin
          if DmCTe_0000.QrCTeLayIFormatStr.Value <> 'YYYY-MM-DD' then
            Geral.MB_Aviso('Este � apenas um aviso: "' +
            DmCTe_0000.QrCTeLayIFormatStr.Value + '". Informe a DERMATEK!');
          Dest := FormatDateTime('yyyy-mm-dd', Source1);
        end;
      end else
      // hora
      if Uppercase(DmCTe_0000.QrCTeLayITipo.Value) = 'H' then
        Dest := FormatDateTime('hh:nn:ss', Source1)
      else
      // texto
      if Uppercase(DmCTe_0000.QrCTeLayITipo.Value) = 'C' then
        Dest := Source1
      else
      // desconhecido
      begin
        Dest := Source1;
        Geral.MB_Aviso(DmCTe_0000.MensagemDeID_CTe(Grupo, Codigo, Dest,
        'Tipo de formata��o desconhecida:', 1));
      end;
      //
      Dest := Trim(XXe_PF.ValidaTexto_XML(Dest, Geral.FF0(Grupo), Geral.FF0(Codigo)));
      if (Dest = '') and (DmCTe_0000.QrCTeLayIOcorMin.Value > 0) then
      begin
        // verificar se � vazio obrigat�rio
        if DmCTe_0000.QrCTeLayIInfoVazio.Value = 0 then
        begin
          Avisa := True;
(*
          if (Codigo = '78') and (ID = 'E17') and
          ((DmCTe_0000.QrCTeCabAdest_CNPJ.Value = '') or
          (Geral.SoNumero1a9_TT(DmCTe_0000.QrCTeCabAdest_CNPJ.Value) = '')) then
            Avisa := False;
*)
          if Avisa then
            Geral.MB_Aviso(DmCTe_0000.MensagemDeID_CTe(Grupo, Codigo, Dest,
            'Valor n�o definido:', 2));
        end;
      end;
      if FGravaCampo = ID_YES then
      begin
        FOrdem := FOrdem + 1;
        //
        MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Inserindo valor de XML no banco de dados.'
        + IntToStr(FOrdem));
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ctexmli', False, [
        'Codigo', 'ID', 'Valor'], [
        'FatID', 'FatNum', 'Empresa', 'Ordem'], [
        Grupo, Codigo, Dest], [
        FFatID, FFatNum, FEmpresa, FOrdem], False);
      end;
    end;
  end else Geral.MB_Erro(DmCTe_0000.MensagemDeID_CTe(Grupo, Codigo, Dest,
  'N�o foi poss�vel definir o valor', 3));
end;

function TFmCTeGeraXML_0200a.GerarArquivoXMLdoCTe(FatID, FatNum,
  Empresa: Integer; LaAviso1, LaAviso2: TLabel; GravaCampos: Integer): Boolean;
var
  Continua: Boolean;
  XMLGerado_Arq, XMLGerado_Dir, XMLAssinado_Dir: String;
  Status: Integer;
begin
  Status := DmCTe_0000.ReopenCTeCabA(FatID, FatNum, Empresa);
  //Status := DmCTe_0000.QrCTeCabAStatus.Value;
  //
  Continua := True;
  if (Continua) or (Status = Integer(ctemystatusCTeDados)) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Montando arquivo XML');
    Continua := CriarDocumentoCTe(FatID, FatNum, Empresa,
      XMLGerado_Arq, XMLGerado_Dir, LaAviso1, LaAviso2, GravaCampos);
    if Continua then
      Continua := DmCTe_0000.StepCTeCab(FatID, FatNum, Empresa,
        ctemystatusCTeGerada, LaAviso1, LaAviso2);
  end;
  if Continua or (Status = Integer(ctemystatusCTeGerada)) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando dados para assinatura do arquivo XML');
    // Assinar CT-e
    //Continua := False;
    if XMLGerado_Arq = '' then
    begin
      Status := DmCTe_0000.ReopenCTeCabA(FatID, FatNum, Empresa);
      if Status = Integer(ctemystatusCTeGerada) then
      begin
        DmCTe_0000.ReopenEmpresa(Empresa);
        //
        XMLGerado_Arq := DmCTe_0000.QrCTeCabAId.Value + CTE_EXT_CTE_XML;
        XMLGerado_Dir := DmCTe_0000.QrFilialDirCTeGer.Value;
      end;
    end;
    if not Geral.VerificaDir(XMLGerado_Dir, '\', 'XML gerado', True) then Exit;
    if XMLGerado_Arq = '' then
      Geral.MB_Aviso('Nome do arquivo da CT-e gerada indefinido!')
    else
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Assinando arquivo XML');
      Continua := DmCTe_0000.AssinarArquivoXML(XMLGerado_Arq, XMLGerado_Dir,
        Empresa, 0, DmCTe_0000.QrCTeCabAIDCtrl.Value, XMLAssinado_Dir);
    if Continua then
    begin
      //Continua :=
      DmCTe_0000.StepCTeCab(FatID, FatNum, Empresa,
        ctemystatusCTeAssinada, LaAviso1, LaAviso2);
      Result := True;
    end;
  end;
end;

function TFmCTeGeraXML_0200a.GerarLoteCTeNovo(Lote, Empresa: Integer;
  out PathLote, XML_Lote: String; LaAviso1, LaAviso2: TLabel;
  Sincronia: TXXeIndSinc): Boolean;
var
  fArquivoTexto: TextFile;
  MeuXMLAssinado: PChar;
  buf, pathSaida : String;
  mTexto, mTexto2: TStringList;
  XMLAssinado_Dir, XMLAssinado_Arq, XMLArq, LoteStr: String;
  I, IndSinc: Integer;
  XML_STR: String;
  VersaoDados: String;
begin
  Result := False;
  mTexto2 := TStringList.Create;
  mTexto2.Clear;

  DmCTe_0000.QrCTeLEnC.Close;
  DmCTe_0000.QrCTeLEnC.Params[00].AsInteger := Lote;
  UMyMod.AbreQuery(DmCTe_0000.QrCTeLEnC, Dmod.MyDB, 'TFmCTeGeraXML_0200a.GerarLoteCTeNovo()');
  //
  if DmCTe_0000.QrCTeLEnC.RecordCount = 0 then
  begin
    Result := False;
    Application.MessageBox('N�o existem CT-e para serem enviadas!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  LoteStr := XXe_PF.StrZero(Lote, 9, 0);
  //
  DmCTe_0000.ReopenEmpresa(Empresa);
  XMLAssinado_Dir := DmCTe_0000.QrFilialDirCTeAss.Value;
  if not Geral.VerificaDir(XMLAssinado_Dir, '\', 'XML assinado', True) then Exit;
  //
  DmCTe_0000.ObtemDirXML(CTE_EXT_ENV_LOT_XML, pathSaida, False);
  if not Geral.VerificaDir(pathSaida, '\', 'Lotes de envio de XML', True) then Exit;
  PathLote := pathSaida + LoteStr + CTE_EXT_ENV_LOT_XML;
  AssignFile(fArquivoTexto, (PathLote));
  Rewrite(fArquivoTexto);
  case Sincronia of
    nisAssincrono: indSInc := 0;
    nisSincrono:   indSinc := 1;
    else Geral.MB_Erro('Sincronia n�o definida em "TFmCTeGeraXML_0200a.GerarLoteCTeNovo()"');
  end;
    begin

  end;

  VersaoDados := Geral.FFT_Dot(DmCTe_0000.QropcoesCTeCTeVerEnvLot.Value, 2, siNegativo);

  XML_Lote := '<?xml version="1.0" encoding="UTF-8"?>' +
  '<enviCTe xmlns="http://www.portalfiscal.inf.br/cte" versao="'+
  VersaoDados + '">' +
  '<idLote>' + LoteStr + '</idLote>' +
  //'<indSinc>' + Geral.FF0(indSinc) + '</indSinc>' +
  '';
//repetir essa parte do codigo quando quiser anexar varios arquivos...
  DmCTe_0000.QrCTeLEnC.First;
  while not DmCTe_0000.QrCTeLEnC.Eof do
  begin
    XMLAssinado_Arq := DmCTe_0000.QrCTeLEnCId.Value;
    XMLArq := XMLAssinado_Dir + XMLAssinado_Arq + CTE_EXT_CTE_XML;
    if FileExists(XMLArq) then
    begin
      buf := '';
      mTexto := TStringList.Create;
      mTexto.Clear;
      mTexto.LoadFromFile(XMLArq);
      MeuXMLAssinado := PChar(mTexto.Text);
      buf := Copy(MeuXMLAssinado, Pos('<CTe', MeuXMLAssinado), Length(MeuXMLAssinado));
      XML_Lote := XML_Lote + buf;
      mTexto.Free;
    end;
    DmCTe_0000.QrCTeLEnC.Next;
  end;
  XML_Lote := XML_Lote + '</enviCTe>';
  //
  XML_STR := '';
  for i := 1 to Length(XML_Lote) do
  begin
    if not (XML_Lote[I] in ([#10,#13])) then
      XML_STR := XML_STR + XML_Lote[I];
  end;
  Write(fArquivoTexto, XML_STR);
  CloseFile(fArquivoTexto);
  //
  Result := True;
end;

function TFmCTeGeraXML_0200a.GerarXMLdoCTe(FatID, FatNum, Empresa: Integer;
  const cteID: String; GravaCampos: Integer): Boolean;
var
  K, h, j, nItem, Controle: Integer;
  Valor, IM, xNome, infAdProd: String;
  //GeraNT2015_03: Boolean;
  DataHora: TDateTime;
  AnyNode: IXMLNode;
begin
  modalXML := '';
  //GeraNT2015_03 := Un N F e _PF.GeraGrupoNA_0310(DmCTe_0000.QrCTeCabAide_dEmi.Value);
  //
  Result := False;
  FGravaCampo := GravaCampos;
  DmCTe_0000.ReopenCTeLayI();
(*1*) // Informa��es da TAG InfCTe...
  // Vers�o do layout
  if Def(0, 2, DmCTe_0000.QrCTeCabAversao.Value, Valor) then
    cXML.InfCTe.Versao := Valor;
  //if Def(0, 3, cteID, Valor) then
  cXML.InfCTe.Id := cteID;
  (*4*) // Informa��es da TAG InfCTe.Ide...
  // C�digo da UF do emitente do Documento Fiscal. Utilizar a Tabela do IBGE
  if Def(0, 5, DmCTe_0000.QrCTeCabAide_cUF.Value, Valor) then
    cXML.InfCTe.Ide.CUF := Valor;
  //C�digo num�rico que comp�e a Chave de Acesso. N�mero aleat�rio gerado pelo emitente para cada CT-e
  if Def(0, 6, DmCTe_0000.QrCTeCabAide_cCT.Value, Valor) then
    cXML.InfCTe.Ide.CCT := Valor;
  // CFOP
  if Def(0, 7, DmCTe_0000.QrCTeCabAide_CFOP.Value, Valor) then
    cXML.InfCTe.Ide.CFOP := Valor;
  //Descri��o da Natureza da Opera��o
  if Def(0, 8, DmCTe_0000.QrCTeCabAide_natOp.Value, Valor) then
    cXML.InfCTe.Ide.NatOp := Valor;
  //Indicador da forma de pagamento:
    // 0 � pago;
    // 1 � A pagar;
    // 2 � Outros.
  if Def(0, 9, DmCTe_0000.QrCTeCabAide_forPag.Value, Valor) then
    cXML.InfCTe.Ide.ForPag := Valor;
  //C�digo do modelo do Documento Fiscal. Utilizar 55 para identifica��o da CT-e, emitida em substitui��o ao modelo 1 e 1A.
  if Def(0, 10, DmCTe_0000.QrCTeCabAide_mod.Value, Valor) then
    cXML.InfCTe.Ide.Mod_ := Valor;//'55';
  //S�rie do Documento Fiscal
  if Def(0, 11, DmCTe_0000.QrCTeCabAide_serie.Value, Valor) then
    cXML.InfCTe.Ide.Serie := Valor;
  //N�mero do Documento Fiscal
  if Def(0, 12, DmCTe_0000.QrCTeCabAide_nCT.Value, Valor) then
    cXML.InfCTe.Ide.NCT := Valor;

  //Data e hora no formato : aaaa-mm-ddThh:mm:ss
  //if Def_UTC(0, 13,
  DataHora := DmCTe_0000.QrCTeCabAide_dEmi.Value +
              DmCTe_0000.QrCTeCabAide_hEmi.Value;
  if Def(0, 13, DataHora, Valor) then
    cXML.InfCTe.Ide.DhEmi := Valor;
  //Tipo do Documento Fiscal (0 - entrada; 1 - sa�da)
  if Def(0, 14, DmCTe_0000.QrCTeCabAide_tpImp.Value, Valor) then
    cXML.InfCTe.Ide.TpImp := Valor;
  if Def(0, 15, DmCTe_0000.QrCTeCabAide_tpEmis.Value, Valor) then
    cXML.InfCTe.Ide.TpEmis := Valor;
  if Def(0, 16, DmCTe_0000.QrCTeCabAide_cDV.Value, Valor) then
    cXML.InfCTe.Ide.CDV := Valor;
  if Def(0, 17, DmCTe_0000.QrCTeCabAide_tpAmb.Value, Valor) then
    cXML.InfCTe.Ide.TpAmb := Valor;
  if Def(0, 18, DmCTe_0000.QrCTeCabAide_tpCTe.Value, Valor) then
    cXML.InfCTe.Ide.TpCTe := Valor;
  if Def(0, 19, DmCTe_0000.QrCTeCabAide_procEmi.Value, Valor) then
    cXML.InfCTe.Ide.ProcEmi := Valor;
  if Def(0, 20, DmCTe_0000.QrCTeCabAide_verProc.Value, Valor) then
    cXML.InfCTe.Ide.VerProc := Valor;
  if Def(0, 21, DmCTe_0000.QrCTeCabAide_refCTe.Value, Valor) then
    cXML.InfCTe.Ide.refCTe := Valor;

  if Def(0, 22, DmCTe_0000.QrCTeCabAide_cMunEnv.Value, Valor) then
    cXML.InfCTe.Ide.CMunEnv := Valor;
  if Def(0, 23, DmCTe_0000.QrCTeCabAide_xMunEnv.Value, Valor) then
    cXML.InfCTe.Ide.XMunEnv := Valor;
  if Def(0, 24, DmCTe_0000.QrCTeCabAide_UFEnv.Value, Valor) then
    cXML.InfCTe.Ide.UFEnv := Valor;
  if Def(0, 25, DmCTe_0000.QrCTeCabAide_Modal.Value, Valor) then
    cXML.InfCTe.Ide.Modal := Valor;
  if Def(0, 26, DmCTe_0000.QrCTeCabAide_tpServ.Value, Valor) then
    cXML.InfCTe.Ide.TpServ := Valor;
  if Def(0, 27, DmCTe_0000.QrCTeCabAide_cMunIni.Value, Valor) then
    cXML.InfCTe.Ide.CMunIni := Valor;
  if Def(0, 28, DmCTe_0000.QrCTeCabAide_xMunIni.Value, Valor) then
    cXML.InfCTe.Ide.XMunIni := Valor;
  if Def(0, 29, DmCTe_0000.QrCTeCabAide_UFIni.Value, Valor) then
    cXML.InfCTe.Ide.UFIni := Valor;
  if Def(0, 30, DmCTe_0000.QrCTeCabAide_cMunFim.Value, Valor) then
    cXML.InfCTe.Ide.CMunFim := Valor;
  if Def(0, 31, DmCTe_0000.QrCTeCabAide_xMunFim.Value, Valor) then
    cXML.InfCTe.Ide.XMunFim := Valor;
  if Def(0, 32, DmCTe_0000.QrCTeCabAide_UFFim.Value, Valor) then
    cXML.InfCTe.Ide.UFFim := Valor;
  if Def(0, 33, DmCTe_0000.QrCTeCabAide_Retira.Value, Valor) then
    cXML.InfCTe.Ide.Retira := Valor;
  if Def(0, 34, DmCTe_0000.QrCTeCabAide_xDetRetira.Value, Valor) then
    cXML.InfCTe.Ide.XDetRetira := Valor;
  if TCTeToma(DmCTe_0000.QrCTeCabAtoma99_toma.Value) <> ctetomaOutros then
  begin
    (*35*) //toma03
    if Def(0, 36, DmCTe_0000.QrCTeCabAtoma99_toma.Value, Valor) then
      cXML.InfCTe.Ide.Toma03.Toma := Valor;
  end else
  begin
    (*37*) //toma04
    if Def(0, 38, DmCTe_0000.QrCTeCabAtoma99_toma.Value, Valor) then
      cXML.InfCTe.Ide.Toma4.Toma := Valor;
    if Trim(DmCTe_0000.QrCTeCabAtoma04_CNPJ.Value) <> '' then
    begin
      if Def(0, 39, Geral.SoNumero_TT(DmCTe_0000.QrCTeCabAtoma04_CNPJ.Value), Valor) then
        cXML.InfCTe.Ide.Toma4.CNPJ := Valor;
    end else
    begin
      if Def(0, 40, Geral.SoNumero_TT(DmCTe_0000.QrCTeCabAtoma04_CPF.Value), Valor) then
        cXML.InfCTe.Ide.Toma4.CPF := Valor;
    end;
    if Def(0, 41, DmCTe_0000.QrCTeCabAtoma04_IE.Value, Valor) then
      cXML.InfCTe.Ide.Toma4.IE := Valor;
    if Def(0, 42, DmCTe_0000.QrCTeCabAtoma04_xNome.Value, Valor) then
      cXML.InfCTe.Ide.toma4.XNome := Valor;
    if Def(0, 43, DmCTe_0000.QrCTeCabAtoma04_xFant.Value, Valor) then
      cXML.InfCTe.Ide.toma4.XFant := Valor;
    if Def(0, 44, DmCTe_0000.QrCTeCabAtoma04_fone.Value, Valor) then
      cXML.InfCTe.Ide.toma4.Fone := Valor;
    // (*45*) // enderToma
    DmCTe_0000.ReopenCTECab2Toma04(FatID, FatNum, Empresa);
    if Def(0, 46, DmCTe_0000.QrCTECab2Toma04enderToma_xLgr.Value, Valor) then
      cXML.InfCTe.Ide.toma4.EnderToma.XLgr := Valor;
    if Def(0, 47, DmCTe_0000.QrCTECab2Toma04enderToma_nro.Value, Valor) then
      cXML.InfCTe.Ide.toma4.EnderToma.Nro := Valor;
    if Def(0, 48, DmCTe_0000.QrCTECab2Toma04enderToma_xCpl.Value, Valor) then
      cXML.InfCTe.Ide.toma4.EnderToma.XCpl := Valor;
    if Def(0, 49, DmCTe_0000.QrCTECab2Toma04enderToma_xBairro.Value, Valor) then
      cXML.InfCTe.Ide.toma4.EnderToma.XBairro := Valor;
    if Def(0, 50, DmCTe_0000.QrCTECab2Toma04enderToma_cMun.Value, Valor) then
      cXML.InfCTe.Ide.toma4.EnderToma.CMun := Valor;
    if Def(0, 51, DmCTe_0000.QrCTECab2Toma04enderToma_xMun.Value, Valor) then
      cXML.InfCTe.Ide.toma4.EnderToma.XMun := Valor;
    if Def(0, 52, DmCTe_0000.QrCTECab2Toma04enderToma_CEP.Value, Valor) then
      cXML.InfCTe.Ide.toma4.EnderToma.CEP := Valor;
    if Def(0, 53, DmCTe_0000.QrCTECab2Toma04enderToma_UF.Value, Valor) then
      cXML.InfCTe.Ide.toma4.EnderToma.UF := Valor;
    if Def(0, 54, DmCTe_0000.QrCTECab2Toma04enderToma_cPais.Value, Valor) then
      cXML.InfCTe.Ide.toma4.EnderToma.CPais := Valor;
    if Def(0, 55, DmCTe_0000.QrCTECab2Toma04enderToma_xPais.Value, Valor) then
      cXML.InfCTe.Ide.toma4.EnderToma.XPais := Valor;
    if Def(0, 56, DmCTe_0000.QrCTECab2Toma04enderToma_email.Value, Valor) then
      cXML.InfCTe.Ide.toma4.Email := Valor;
  end;
  if DmCTe_0000.QrCTeCabAide_dhCont.Value > 2 then
  begin
    if Def_UTC(0, 57, DmCTe_0000.QrCTeCabAide_dhCont.Value,
    0,
    DmCTe_0000.QrCTeCabAide_dhContTZD.Value, Valor) then
      cXML.InfCTe.Ide.DhCont := Valor;
    if Def(0, 58, DmCTe_0000.QrCTeCabAide_xJust.Value, Valor) then
      cXML.InfCTe.Ide.XJust := Valor;
  end;
  (*59*)  // compl - Dados complementares do CT-e para fins operacionais ou comerciais
  if Def(0, 60, DmCTe_0000.QrCTeCabAcompl_xCaracAd.Value, Valor) then
    cXML.InfCTe.Compl.XCaracAd := Valor;
  if Def(0, 61, DmCTe_0000.QrCTeCabAcompl_xCaracSer.Value, Valor) then
    cXML.InfCTe.Compl.XCaracSer := Valor;
  if Def(0, 62, DmCTe_0000.QrCTeCabAcompl_xEmi.Value, Valor) then
    cXML.InfCTe.Compl.XEmi := Valor;
  //
{ //////////////////////////////////////////////////////////////////////////////
Obrigat�rio para modal a�reo !!!
////////////////////////////////////////////////////////////////////////////////
  //
  (*63*) // fluxo - Previs�o do fluxo da carga
  if Def(0, 64, DmCTe_0000.Qr???.Value, Valor) then
    cXML.InfCTe.Compl.Fluxo.xOrig := Valor;
  (*65*) // pass
  if Def(0, 66, DmCTe_0000.Qr???.Value, Valor) then
    cXML.InfCTe.Compl.Fluxo.Pass := Valor;
  if Def(0, 67, DmCTe_0000.QrCTeCabA.Value, Valor) then
    cXML.InfCTe.Compl.Fluxo.XDest := Valor;
  if Def(0, 68, DmCTe_0000.Qr???.Value, Valor) then
    cXML.InfCTe.Compl.Fluxo.XRota := Valor;
}
  ///
  ///
  ///

{ //////////////////////////////////////////////////////////////////////////////
N�o Obrigat�rio !!!
////////////////////////////////////////////////////////////////////////////////
  //
  (*69*) // Entrega - Informa��es ref. a previs�o de entrega
  case DmCTe_0000.QrCTeCabAentrega_tpPer.Value of
    0: (*70*) // semData - Entrega sem data definida
    begin
      if Def(0, 71, DmCTe_0000.QrCTeCabAentrega_tpPer.Value, Valor) then
        cXML.InfCTe.Compl.Entrega.SemData.TpPer := Valor;
    end;
    1, 2, 3: (*72*) // Entrega com data definida
    begin
      if Def(0, 73, DmCTe_0000.QrCTeCabAentrega_tpPer.Value, Valor) then
        cXML.InfCTe.Compl.Entrega.ComData.TpPer := Valor;
      if Def(0, 74, DmCTe_0000.QrCTeCabAentrega_dProg.Value, Valor) then
        cXML.InfCTe.Compl.Entrega.ComData.DProg := Valor;
    end;
    4: (*75*) // Data programada
    begin
      if Def(0, 76, DmCTe_0000.QrCTeCabAentrega_tpPer.Value, Valor) then
        cXML.InfCTe.Compl.Entrega.NoPeriodo.TpPer := Valor;
      if Def(0, 77, DmCTe_0000.QrCTeCabAentrega_dIni.Value, Valor) then
        cXML.InfCTe.Compl.Entrega.NoPeriodo.DIni := Valor;
      if Def(0, 78, DmCTe_0000.QrCTeCabAentrega_dFim.Value, Valor) then
        cXML.InfCTe.Compl.Entrega.NoPeriodo.DFim := Valor;
    end;
  end;
  case DmCTe_0000.QrCTeCabAentrega_tpHor.Value of
    0: (*79*) // semHora - Entrega sem hora definida
    begin
      if Def(0, 80, DmCTe_0000.QrCTeCabAentrega_tpPer.Value, Valor) then
        cXML.InfCTe.Compl.Entrega.SemData.TpPer := Valor;
    end;
    1, 2, 3: (*81*) // Entrega com hora definida
    begin
      if Def(0, 82, DmCTe_0000.QrCTeCabAentrega_tpHor.Value, Valor) then
        cXML.InfCTe.Compl.Entrega.ComHora.TpHor := Valor;
      if Def(0, 83, DmCTe_0000.QrCTeCabAentrega_hProg.Value, Valor) then
        cXML.InfCTe.Compl.Entrega.ComHora.HProg := Valor;
    end;
    4: (*84*) // No intervalo
    begin
      if Def(0, 85, DmCTe_0000.QrCTeCabAentrega_tpHor.Value, Valor) then
        cXML.InfCTe.Compl.Entrega.NoInter.TpHor := Valor;
      if Def(0, 86, DmCTe_0000.QrCTeCabAentrega_hIni.Value, Valor) then
        cXML.InfCTe.Compl.Entrega.NoInter.HIni := Valor;
      if Def(0, 87, DmCTe_0000.QrCTeCabAentrega_hFim.Value, Valor) then
        cXML.InfCTe.Compl.Entrega.NoInter.HFim := Valor;
    end;
  end;
}
  ///
  ///
  ///

{ //////////////////////////////////////////////////////////////////////////////
N�o Obrigat�rio !!!
////////////////////////////////////////////////////////////////////////////////
}
  if Def(0, 88, DmCTe_0000.QrCTeCabAide_origCalc.Value, Valor) then
    cXML.InfCTe.Compl.OrigCalc := Valor;
  if Def(0, 89, DmCTe_0000.QrCTeCabAide_destCalc.Value, Valor) then
    cXML.InfCTe.Compl.DestCalc := Valor;
  if Def(0, 90, DmCTe_0000.QrCTeCabAide_xObs.Value, Valor) then
    cXML.InfCTe.Compl.XObs := Valor;
  //
  (*91*) // ObsCont - Campo de uso livre do contribuinte
  DmCTe_0000.ReopenCteIt2ObsCont(FatID, FatNum, Empresa);
  DmCTe_0000.QrCteIt2ObsCont.First;
  while not DmCTe_0000.QrCteIt2ObsCont.Eof do
  begin
    cObsCont := cXML.InfCTe.Compl.ObsCont.Add;
    //
    if Def(0, 92, DmCTe_0000.QrCTeIt2ObsContxCampo.Value, Valor) then
      cObsCont.XCampo := Valor;
    if Def(0, 93, DmCTe_0000.QrCTeIt2ObsContxTexto.Value, Valor) then
      cObsCont.XTexto := Valor;
    //
    DmCTe_0000.QrCteIt2ObsCont.Next;
  end;
  //
  (*94*) // ObsFisco - Campo de uso livre do contribuinte
  DmCTe_0000.ReopenCteIt2ObsFisco(FatID, FatNum, Empresa);
  DmCTe_0000.QrCteIt2ObsFisco.First;
  while not DmCTe_0000.QrCteIt2ObsFisco.Eof do
  begin
    cObsFisco := cXML.InfCTe.Compl.ObsFisco.Add;
    //
    if Def(0, 95, DmCTe_0000.QrCTeIt2ObsFiscoxCampo.Value, Valor) then
      cObsFisco.XCampo := Valor;
    if Def(0, 96, DmCTe_0000.QrCTeIt2ObsFiscoxTexto.Value, Valor) then
      cObsFisco.XTexto := Valor;
    //
    DmCTe_0000.QrCteIt2ObsFisco.Next;
  end;
  //
  (*97*) // Identifica��o do Emitente do CT-e
  if Def(0, 98, Geral.SoNumero_TT(DmCTe_0000.QrCTeCabAemit_CNPJ.Value), Valor) then
    cXML.InfCTe.Emit.CNPJ := Valor;
  if Def(0, 99, DmCTe_0000.QrCTeCabAemit_IE.Value, Valor) then
    cXML.InfCTe.Emit.IE := Valor;
////////////////////////////////////////////////////////////////////////////////
  if DmCTe_0000.QrCTeCabAide_tpAmb.Value = 2 then
    xNome := NO_SEM_VALOR
  else
    xNome := DmCTe_0000.QrCTeCabAemit_xNome.Value;
  if Def(0, 100, xNome, Valor) then
    cXML.InfCTe.Emit.XNome := Valor;
////////////////////////////////////////////////////////////////////////////////
  if Def(0, 101, DmCTe_0000.QrCTeCabAemit_xFant.Value, Valor) then
    cXML.InfCTe.Emit.xFant := Valor;
  (*102*) // Endere�o do emitente
  if Def(0, 103, DmCTe_0000.QrCTeCabAemit_xLgr.Value, Valor) then
    cXML.InfCTe.Emit.EnderEmit.XLgr := Valor;
  if Def(0, 104, DmCTe_0000.QrCTeCabAemit_nro.Value, Valor) then
    cXML.InfCTe.Emit.EnderEmit.Nro := Valor;
  if Def(0, 105, DmCTe_0000.QrCTeCabAemit_xCpl.Value, Valor) then
    cXML.InfCTe.Emit.EnderEmit.XCpl := Valor;
  if Def(0, 106, DmCTe_0000.QrCTeCabAemit_xBairro.Value, Valor) then
    cXML.InfCTe.Emit.EnderEmit.XBairro := Valor;
  if Def(0, 107, DmCTe_0000.QrCTeCabAemit_cMun.Value, Valor) then
    cXML.InfCTe.Emit.EnderEmit.CMun := Valor;
  if Def(0, 108, DmCTe_0000.QrCTeCabAemit_xMun.Value, Valor) then
    cXML.InfCTe.Emit.EnderEmit.XMun := Valor;
  if Def(0, 109, DmCTe_0000.QrCTeCabAemit_CEP.Value, Valor) then
    cXML.InfCTe.Emit.EnderEmit.CEP := Valor;
  if Def(0, 110, DmCTe_0000.QrCTeCabAemit_UF.Value, Valor) then
    cXML.InfCTe.Emit.EnderEmit.UF := Valor;
  if Def(0, 111, DmCTe_0000.QrCTeCabAemit_fone.Value, Valor) then
    cXML.InfCTe.Emit.EnderEmit.Fone := Valor;
  //
  (*112*) // rem - Informa��es do Remidor da Carga
  if Trim(DmCTe_0000.QrCTeCabArem_CNPJ.Value) <> '' then
  begin
    if Def(0, 113, Geral.SoNumero_TT(DmCTe_0000.QrCTeCabArem_CNPJ.Value), Valor) then
      cXML.InfCTe.Rem.CNPJ := Valor;
  end else
  begin
    if Def(0, 114, Geral.SoNumero_TT(DmCTe_0000.QrCTeCabArem_CPF.Value), Valor) then
      cXML.InfCTe.Rem.CPF := Valor;
  end;
  if Def(0, 115, DmCTe_0000.QrCTeCabArem_IE.Value, Valor) then
    cXML.InfCTe.Rem.IE := Valor;
////////////////////////////////////////////////////////////////////////////////
    if DmCTe_0000.QrCTeCabAide_tpAmb.Value = 2 then
      xNome := NO_SEM_VALOR
    else
      xNome := DmCTe_0000.QrCTeCabArem_xNome.Value;
  if Def(0, 116, xNome, Valor) then
    cXML.InfCTe.Rem.XNome := Valor;
////////////////////////////////////////////////////////////////////////////////
  if Def(0, 117, DmCTe_0000.QrCTeCabArem_xFant.Value, Valor) then
    cXML.InfCTe.Rem.XFant := Valor;
  if Def(0, 118, DmCTe_0000.QrCTeCabArem_fone.Value, Valor) then
    cXML.InfCTe.Rem.Fone := Valor;
  (*119*) // enderRem - Dados do endere�o
  if Def(0, 120, DmCTe_0000.QrCTeCabArem_xLgr.Value, Valor) then
    cXML.InfCTe.Rem.EnderReme.XLgr := Valor;
  if Def(0, 121, DmCTe_0000.QrCTeCabArem_nro.Value, Valor) then
    cXML.InfCTe.Rem.EnderReme.nro := Valor;
  if Def(0, 122, DmCTe_0000.QrCTeCabArem_xCpl.Value, Valor) then
    cXML.InfCTe.Rem.EnderReme.XCpl := Valor;
  if Def(0, 123, DmCTe_0000.QrCTeCabArem_xBairro.Value, Valor) then
    cXML.InfCTe.Rem.EnderReme.XBairro := Valor;
  if Def(0, 124, DmCTe_0000.QrCTeCabArem_cMun.Value, Valor) then
    cXML.InfCTe.Rem.EnderReme.CMun := Valor;
  if Def(0, 125, DmCTe_0000.QrCTeCabArem_xMun.Value, Valor) then
    cXML.InfCTe.Rem.EnderReme.XMun := Valor;
  if Def(0, 126, DmCTe_0000.QrCTeCabArem_CEP.Value, Valor) then
    cXML.InfCTe.Rem.EnderReme.CEP := Valor;
  if Def(0, 127, DmCTe_0000.QrCTeCabArem_UF.Value, Valor) then
    cXML.InfCTe.Rem.EnderReme.UF := Valor;
  if Def(0, 128, DmCTe_0000.QrCTeCabArem_cPais.Value, Valor) then
    cXML.InfCTe.Rem.EnderReme.CPais := Valor;
  if Def(0, 129, DmCTe_0000.QrCTeCabArem_xPais.Value, Valor) then
    cXML.InfCTe.Rem.EnderReme.XPais := Valor;
  if Def(0, 130, DmCTe_0000.QrCTeCabArem_email.Value, Valor) then
    cXML.InfCTe.Rem.Email := Valor;
  //
  //
{  Nao consta no XSD!!!!
  (*131*) // locColeta - Local da Coleta
  DmCTe_0000.ReopenCteCab2LocColeta(FatID, FatNum, Empresa);
  if Trim(DmCTe_0000.QrCTeCab2LocColetaCNPJ.Value) <> '' then
  begin
    if Def(0, 132, Geral.SoNumero_TT(DmCTe_0000.QrCTeCab2LocColetaCNPJ.Value), Valor) then
      cXML.InfCTe.Rem.LocColeta. := Valor;
  end else
  begin
    if Def(0, 133, Geral.SoNumero_TT(DmCTe_0000.QrCTeCab2LocColetaCPF.Value, Valor)) then
      cXML.InfCTe.Rem.LocColeta. := Valor;
  end;
  if Def(0, 134, DmCTe_0000.QrCTeCab2LocColetaxNome.Value, Valor) then
    cXML.InfCTe.Rem.LocColeta. := Valor;
  if Def(0, 135, DmCTe_0000.QrCTeCab2LocColetaxLgr.Value, Valor) then
    cXML.InfCTe.Rem.LocColeta. := Valor;
  if Def(0, 136, DmCTe_0000.QrCTeCab2LocColetanro.Value, Valor) then
    cXML.InfCTe.Rem.LocColeta. := Valor;
  if Def(0, 137, DmCTe_0000.QrCTeCab2LocColetaxCpl.Value, Valor) then
    cXML.InfCTe.Rem.LocColeta. := Valor;
  if Def(0, 138, DmCTe_0000.QrCTeCab2LocColetaxBairro.Value, Valor) then
    cXML.InfCTe.Rem.LocColeta. := Valor;
  if Def(0, 139, DmCTe_0000.QrCTeCab2LocColetacMun.Value, Valor) then
    cXML.InfCTe.Rem.LocColeta. := Valor;
  if Def(0, 140, DmCTe_0000.QrCTeCab2LocColetaxMun.Value, Valor) then
    cXML.InfCTe.Rem.LocColeta. := Valor;
  if Def(0, 141, DmCTe_0000.QrCTeCab2LocColetaUF.Value, Valor) then
    cXML.InfCTe.Rem.LocColeta. := Valor;
}
  (*142*) // exped - Informa��es do Expedidor da Carga
  DmCTe_0000.ReopenCteCab1Exped(FatID, FatNum, Empresa);
  if DmCTe_0000.QrCteCab1Exped.RecordCount > 0 then
  begin
    if Trim(DmCTe_0000.QrCTeCab1ExpedCNPJ.Value) <> '' then
    begin
      if Def(0, 143, Geral.SoNumero_TT(DmCTe_0000.QrCTeCab1ExpedCNPJ.Value), Valor) then
        cXML.InfCTe.Exped.CNPJ := Valor;
    end else
    begin
      if Def(0, 144, Geral.SoNumero_TT(DmCTe_0000.QrCTeCab1ExpedCPF.Value), Valor) then
        cXML.InfCTe.Exped.CPF := Valor;
    end;
    if Trim(DmCTe_0000.QrCTeCab1ExpedCNPJ.Value) <> '' then
    begin
      if Def(0, 145, DmCTe_0000.QrCTeCab1ExpedIE.Value, Valor) then
        cXML.InfCTe.Exped.IE := Valor;
    end else
      cXML.InfCTe.Exped.IE := '';
////////////////////////////////////////////////////////////////////////////////
    if DmCTe_0000.QrCTeCabAide_tpAmb.Value = 2 then
      xNome := NO_SEM_VALOR
    else
      xNome := DmCTe_0000.QrCTeCab1ExpedxNome.Value;
    if Def(0, 146, xNome, Valor) then
      cXML.InfCTe.Exped.XNome := Valor;
////////////////////////////////////////////////////////////////////////////////
    if Def(0, 147, DmCTe_0000.QrCTeCab1Expedfone.Value, Valor) then
      cXML.InfCTe.Exped.Fone := Valor;
    (*148*) // enderExped - Dados do endere�o
    if Def(0, 149, DmCTe_0000.QrCTeCab1ExpedxLgr.Value, Valor) then
      cXML.InfCTe.Exped.EnderExped.XLgr := Valor;
    if Def(0, 150, DmCTe_0000.QrCTeCab1Expednro.Value, Valor) then
      cXML.InfCTe.Exped.EnderExped.nro := Valor;
    if Def(0, 151, DmCTe_0000.QrCTeCab1ExpedxCpl.Value, Valor) then
      cXML.InfCTe.Exped.EnderExped.XCpl := Valor;
    if Def(0, 152, DmCTe_0000.QrCTeCab1ExpedxBairro.Value, Valor) then
      cXML.InfCTe.Exped.EnderExped.XBairro := Valor;
    if Def(0, 153, DmCTe_0000.QrCTeCab1ExpedcMun.Value, Valor) then
      cXML.InfCTe.Exped.EnderExped.CMun := Valor;
    if Def(0, 154, DmCTe_0000.QrCTeCab1ExpedxMun.Value, Valor) then
      cXML.InfCTe.Exped.EnderExped.XMun := Valor;
    if Def(0, 155, DmCTe_0000.QrCTeCab1ExpedCEP.Value, Valor) then
      cXML.InfCTe.Exped.EnderExped.CEP := Valor;
    if Def(0, 156, DmCTe_0000.QrCTeCab1ExpedUF.Value, Valor) then
      cXML.InfCTe.Exped.EnderExped.UF := Valor;
    if Def(0, 157, DmCTe_0000.QrCTeCab1ExpedcPais.Value, Valor) then
      cXML.InfCTe.Exped.EnderExped.CPais := Valor;
    if Def(0, 158, DmCTe_0000.QrCTeCab1ExpedxPais.Value, Valor) then
      cXML.InfCTe.Exped.EnderExped.XPais := Valor;
    if Def(0, 159, DmCTe_0000.QrCTeCab1Expedemail.Value, Valor) then
      cXML.InfCTe.Exped.Email := Valor;
  end;
  //
  (*160*) // receb - Informa��es do Recebedor da Carga
  DmCTe_0000.ReopenCteCab1Receb(FatID, FatNum, Empresa);
  if DmCTe_0000.QrCteCab1Receb.RecordCount > 0 then
    begin
    if Trim(DmCTe_0000.QrCTeCab1RecebCNPJ.Value) <> '' then
    begin
      if Def(0, 161, Geral.SoNumero_TT(DmCTe_0000.QrCTeCab1RecebCNPJ.Value), Valor) then
        cXML.InfCTe.Receb.CNPJ := Valor;
    end else
    begin
      if Def(0, 162, Geral.SoNumero_TT(DmCTe_0000.QrCTeCab1RecebCPF.Value), Valor) then
        cXML.InfCTe.Receb.CPF := Valor;
    end;
    if Trim(DmCTe_0000.QrCTeCab1RecebCNPJ.Value) <> '' then
    begin
      if Def(0, 163, DmCTe_0000.QrCTeCab1RecebIE.Value, Valor) then
        cXML.InfCTe.Receb.IE := Valor;
    end else
      // no manual diz para nao informar, mas o web server exige!!!
      cXML.InfCTe.Receb.IE := '';
    if DmCTe_0000.QrCTeCabAide_tpAmb.Value = 2 then
      xNome := NO_SEM_VALOR
    else
      xNome := DmCTe_0000.QrCTeCab1RecebxNome.Value;
    if Def(0, 164, xNome, Valor) then
      cXML.InfCTe.Receb.XNome := Valor;
////////////////////////////////////////////////////////////////////////////////
    if Def(0, 165, DmCTe_0000.QrCTeCab1Recebfone.Value, Valor) then
      cXML.InfCTe.Receb.Fone := Valor;
    (*166*) // enderReceb - Dados do endere�o
    if Def(0, 167, DmCTe_0000.QrCTeCab1RecebxLgr.Value, Valor) then
      cXML.InfCTe.Receb.EnderReceb.XLgr := Valor;
    if Def(0, 168, DmCTe_0000.QrCTeCab1Recebnro.Value, Valor) then
      cXML.InfCTe.Receb.EnderReceb.nro := Valor;
    if Def(0, 169, DmCTe_0000.QrCTeCab1RecebxCpl.Value, Valor) then
      cXML.InfCTe.Receb.EnderReceb.XCpl := Valor;
    if Def(0, 170, DmCTe_0000.QrCTeCab1RecebxBairro.Value, Valor) then
      cXML.InfCTe.Receb.EnderReceb.XBairro := Valor;
    if Def(0, 171, DmCTe_0000.QrCTeCab1RecebcMun.Value, Valor) then
      cXML.InfCTe.Receb.EnderReceb.CMun := Valor;
    if Def(0, 172, DmCTe_0000.QrCTeCab1RecebxMun.Value, Valor) then
      cXML.InfCTe.Receb.EnderReceb.XMun := Valor;
    if Def(0, 173, DmCTe_0000.QrCTeCab1RecebCEP.Value, Valor) then
      cXML.InfCTe.Receb.EnderReceb.CEP := Valor;
    if Def(0, 174, DmCTe_0000.QrCTeCab1RecebUF.Value, Valor) then
      cXML.InfCTe.Receb.EnderReceb.UF := Valor;
    if Def(0, 175, DmCTe_0000.QrCTeCab1RecebcPais.Value, Valor) then
      cXML.InfCTe.Receb.EnderReceb.CPais := Valor;
    if Def(0, 176, DmCTe_0000.QrCTeCab1RecebxPais.Value, Valor) then
      cXML.InfCTe.Receb.EnderReceb.XPais := Valor;
    if Def(0, 177, DmCTe_0000.QrCTeCab1Recebemail.Value, Valor) then
      cXML.InfCTe.Receb.Email := Valor;
    //
    //
  end;
  (*178*) // dest - Informa��es do Destinat�rio do CT-e
  if Trim(DmCTe_0000.QrCTeCabAdest_CNPJ.Value) <> '' then
  begin
    if Def(0, 179, Geral.SoNumero_TT(DmCTe_0000.QrCTeCabAdest_CNPJ.Value), Valor) then
      cXML.InfCTe.Dest.CNPJ := Valor;
  end else
  begin
    if Def(0, 180, Geral.SoNumero_TT(DmCTe_0000.QrCTeCabAdest_CPF.Value), Valor) then
      cXML.InfCTe.Dest.CPF := Valor;
  end;
  if Def(0, 181, DmCTe_0000.QrCTeCabAdest_IE.Value, Valor) then
    cXML.InfCTe.Dest.IE := Valor;
////////////////////////////////////////////////////////////////////////////////
    if DmCTe_0000.QrCTeCabAide_tpAmb.Value = 2 then
      xNome := NO_SEM_VALOR
    else
      xNome := DmCTe_0000.QrCTeCabAdest_xNome.Value;
  if Def(0, 182, xNome, Valor) then
    cXML.InfCTe.Dest.XNome := Valor;
////////////////////////////////////////////////////////////////////////////////
  if Def(0, 183, DmCTe_0000.QrCTeCabAdest_fone.Value, Valor) then
    cXML.InfCTe.Dest.Fone := Valor;
  if Def(0, 184, DmCTe_0000.QrCTeCabAdest_ISUF.Value, Valor) then
    cXML.InfCTe.Dest.ISUF := Valor;
  (*185*) // enderDest - Dados do endere�o
  if Def(0, 186, DmCTe_0000.QrCTeCabAdest_xLgr.Value, Valor) then
    cXML.InfCTe.Dest.EnderDest.XLgr := Valor;
  if Def(0, 187, DmCTe_0000.QrCTeCabAdest_nro.Value, Valor) then
    cXML.InfCTe.Dest.EnderDest.nro := Valor;
  if Def(0, 188, DmCTe_0000.QrCTeCabAdest_xCpl.Value, Valor) then
    cXML.InfCTe.Dest.EnderDest.XCpl := Valor;
  if Def(0, 189, DmCTe_0000.QrCTeCabAdest_xBairro.Value, Valor) then
    cXML.InfCTe.Dest.EnderDest.XBairro := Valor;
  if Def(0, 190, DmCTe_0000.QrCTeCabAdest_cMun.Value, Valor) then
    cXML.InfCTe.Dest.EnderDest.CMun := Valor;
  if Def(0, 191, DmCTe_0000.QrCTeCabAdest_xMun.Value, Valor) then
    cXML.InfCTe.Dest.EnderDest.XMun := Valor;
  if Def(0, 192, DmCTe_0000.QrCTeCabAdest_CEP.Value, Valor) then
    cXML.InfCTe.Dest.EnderDest.CEP := Valor;
  if Def(0, 193, DmCTe_0000.QrCTeCabAdest_UF.Value, Valor) then
    cXML.InfCTe.Dest.EnderDest.UF := Valor;
  if Def(0, 194, DmCTe_0000.QrCTeCabAdest_cPais.Value, Valor) then
    cXML.InfCTe.Dest.EnderDest.CPais := Valor;
  if Def(0, 195, DmCTe_0000.QrCTeCabAdest_xPais.Value, Valor) then
    cXML.InfCTe.Dest.EnderDest.XPais := Valor;
  if Def(0, 196, DmCTe_0000.QrCTeCabAdest_email.Value, Valor) then
    cXML.InfCTe.Dest.Email := Valor;
  //
  //
{  Nao consta no XSD!!!!
  (*197*) // locEnt - Local da Coleta
  DmCTe_0000.ReopenCteCab2LocEnt(FatID, FatNum, Empresa);
  if Trim(DmCTe_0000.QrCTeCab2LocEntCNPJ.Value) <> '' then
  begin
    if Def(0, 198, Geral.SoNumero_TT(DmCTe_0000.QrCTeCab2LocEntCNPJ.Value), Valor) then
      cXML.InfCTe.Dest.LocEnt := Valor;
  end else
  begin
    if Def(0, 199, Geral.SoNumero_TT(DmCTe_0000.QrCTeCab2LocEntCPF.Value, Valor)) then
      cXML.InfCTe.Dest.LocEnt. := Valor;
  end;
  if Def(0, 200, DmCTe_0000.QrCTeCab2LocEntxNome.Value, Valor) then
    cXML.InfCTe.Dest.LocEnt. := Valor;
  if Def(0, 201, DmCTe_0000.QrCTeCab2LocEntxLgr.Value, Valor) then
    cXML.InfCTe.Dest.LocEnt. := Valor;
  if Def(0, 202, DmCTe_0000.QrCTeCab2LocEntnro.Value, Valor) then
    cXML.InfCTe.Dest.LocEnt. := Valor;
  if Def(0, 203, DmCTe_0000.QrCTeCab2LocEntxCpl.Value, Valor) then
    cXML.InfCTe.Dest.LocEnt. := Valor;
  if Def(0, 204, DmCTe_0000.QrCTeCab2LocEntxBairro.Value, Valor) then
    cXML.InfCTe.Dest.LocEnt. := Valor;
  if Def(0, 205, DmCTe_0000.QrCTeCab2LocEntcMun.Value, Valor) then
    cXML.InfCTe.Dest.LocEnt. := Valor;
  if Def(0, 206, DmCTe_0000.QrCTeCab2LocEntxMun.Value, Valor) then
    cXML.InfCTe.Dest.LocEnt. := Valor;
  if Def(0, 207, DmCTe_0000.QrCTeCab2LocEntUF.Value, Valor) then
    cXML.InfCTe.Dest.LocEnt. := Valor;
}
  //
  (*208*) // vPrest - Valores da Presta��o de Servi�o
  if Def(0, 209, DmCTe_0000.QrCTeCabAvPrest_vTPrest.Value, Valor) then
    cXML.InfCTe.VPrest.VTPrest := Valor;
  if Def(0, 210, DmCTe_0000.QrCTeCabAvPrest_vRec.Value, Valor) then
    cXML.InfCTe.VPrest.VRec := Valor;
  //
  (*211*) // Comp - Componentes do Valor da Presta��o
  DmCTe_0000.ReopenCteIt2Comp(FatID, FatNum, Empresa);
  while not DmCTe_0000.QrCteIt2Comp.Eof do
  begin
    cComp := cXML.InfCTe.VPrest.Comp.Add;
    //
    if Def(0, 212, DmCTe_0000.QrCTeIt2CompxNome.Value, Valor) then
      cComp.XNome := Valor;
    if Def(0, 213, DmCTe_0000.QrCTeIt2CompvComp.Value, Valor) then
      cComp.VComp := Valor;
    //
    DmCTe_0000.QrCteIt2Comp.Next;
  end;
  (*214*) // imp - Informa��es relativas aos Impostos
  (*215*) // ICMS - Informa��es relativas ao ICMS
  case TCTeMyCST(DmCTe_0000.QrCTeCabACTeMyCST.Value) of
    (*216*) // ICMS00 - Presta��o sujeito � tributa��o normal do ICMS
    ctemycstCST00Normal:
    begin
      if Def(0, 217, DmCTe_0000.QrCTeCabAICMS_CST.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS00.CST := Valor;
      if Def(0, 218, DmCTe_0000.QrCTeCabAICMS_vBC.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS00.VBC := Valor;
      if Def(0, 219, DmCTe_0000.QrCTeCabAICMS_pICMS.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS00.PICMS := Valor;
      if Def(0, 220, DmCTe_0000.QrCTeCabAICMS_vICMS.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS00.VICMS := Valor;
    end;
    ctemycstCST20BCRed: (*221*) // Presta��o sujeito � tributa��o com redu��o de BC do ICMS
    begin
      if Def(0, 222, DmCTe_0000.QrCTeCabAICMS_CST.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS20.CST := Valor;
      if Def(0, 223, DmCTe_0000.QrCTeCabAICMS_pRedBC.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS20.PRedBC := Valor;
      if Def(0, 224, DmCTe_0000.QrCTeCabAICMS_vBC.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS20.VBC := Valor;
      if Def(0, 225, DmCTe_0000.QrCTeCabAICMS_pICMS.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS20.PICMS := Valor;
      if Def(0, 226, DmCTe_0000.QrCTeCabAICMS_vICMS.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS20.VICMS := Valor;
    end;
    (*227*) // ICMS45 - ICMS Isento, n�o Tributado ou diferido
    ctemycstCST40Isensao,
    ctemycstCST41NaoTrib,
    ctemycstCST51Diferido:
    begin
      if Def(0, 228, DmCTe_0000.QrCTeCabAICMS_CST.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS45.CST := Valor;
    end;
    ctemycstCST60ST: (*229*) // Tributa��o pelo ICMS60 - ICMS cobrado por substitui��o tribut�ria.Responsabilidade do recolhimento do ICMS atribu�do ao tomador ou 3� por ST
    begin
      if Def(0, 230, DmCTe_0000.QrCTeCabAICMS_CST.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS60.CST := Valor;
      if Def(0, 231, DmCTe_0000.QrCTeCabAICMS_vBCSTRet.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS60.VBCSTRet := Valor;
      if Def(0, 232, DmCTe_0000.QrCTeCabAICMS_pICMSSTRet.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS60.PICMSSTRet := Valor;
      if Def(0, 233, DmCTe_0000.QrCTeCabAICMS_vICMSSTRet.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS60.VICMSSTRet := Valor;
      if Def(0, 234, DmCTe_0000.QrCTeCabAICMS_vCred.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS60.VCred := Valor;
    end;
    ctemycstCST90Outros: (*235*) // ICMS Outros
    begin
      if Def(0, 236, DmCTe_0000.QrCTeCabAICMS_CST.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS90.CST := Valor;
      if Def(0, 237, DmCTe_0000.QrCTeCabAICMS_pRedBC.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS90.PRedBC := Valor;
      if Def(0, 238, DmCTe_0000.QrCTeCabAICMS_vBC.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS90.VBC := Valor;
      if Def(0, 239, DmCTe_0000.QrCTeCabAICMS_pICMS.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS90.PICMS := Valor;
      if Def(0, 240, DmCTe_0000.QrCTeCabAICMS_vICMS.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS90.VICMS := Valor;
      if Def(0, 241, DmCTe_0000.QrCTeCabAICMS_vCred.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMS90.VCred := Valor;
    end;
    ctemycstCST90OutraUF: (*242*) // ICMS devido � UF de origem da presta��o, quando diferente da UF do emitente
    begin
      if Def(0, 243, DmCTe_0000.QrCTeCabAICMS_CST.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMSOutraUF.CST := Valor;
      if Def(0, 244, DmCTe_0000.QrCTeCabAICMS_pRedBCOutraUF.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMSOutraUF.PRedBCOutraUF := Valor;
      if Def(0, 245, DmCTe_0000.QrCTeCabAICMS_vBCOutraUF.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMSOutraUF.VBCOutraUF := Valor;
      if Def(0, 246, DmCTe_0000.QrCTeCabAICMS_pICMSOutraUF.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMSOutraUF.PICMSOutraUF := Valor;
      if Def(0, 247, DmCTe_0000.QrCTeCabAICMS_vICMSOutraUF.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMSOutraUF.VICMSOutraUF := Valor;
    end;
    ctemycstCSTSimplesNacional: (*248*) // ICMSSN
    begin
      if Def(0, 249, DmCTe_0000.QrCTeCabAICMSSN_indSN.Value, Valor) then
        cXML.InfCTe.Imp.ICMS.ICMSSN.IndSN := Valor;
    end;
  end;
  if Def(0, 250, DmCTe_0000.QrCTeCabAimp_vTotTrib.Value, Valor) then
    cXML.InfCTe.Imp.VTotTrib := Valor;
  if Def(0, 251, DmCTe_0000.QrCTeCabAimp_InfAdFisco.Value, Valor) then
    cXML.InfCTe.Imp.InfAdFisco := Valor;
  //
////////////////////////////////////////////////////////////////////////////////
///  Tipo de CTe
///
  case TCTeTpCTe(DmCTe_0000.QrCTeCabAide_tpCTe.Value) of
    (*0*)ctetpcteNormal, (*252*)
    (*3*)ctetpcteSubstituto:
    begin
      (*253*) // infCarga Informa��es da Carga do CT-e
      if Def(0, 254, DmCTe_0000.QrFrtFatCabvCarga.Value, Valor) then
        cXML.InfCTe.InfCTeNorm.InfCarga.VCarga := Valor;
      if Def(0, 255, DmCTe_0000.QrCTeCabAinfCarga_proPred.Value, Valor) then
        cXML.InfCTe.InfCTeNorm.InfCarga.ProPred := Valor;
      if Def(0, 256, DmCTe_0000.QrCTeCabAinfCarga_xOutCat.Value, Valor) then
        cXML.InfCTe.InfCTeNorm.InfCarga.XOutCat := Valor;
      //
      (*257*)
      DmCTe_0000.ReopenCteIt3InfQ(FatID, FatNum, Empresa);
      while not DmCTe_0000.QrCteIt3InfQ.Eof do
      begin
        cInfQ := cXML.InfCTe.InfCTeNorm.InfCarga.InfQ.Add;
        //
        if Def(0, 258, DmCTe_0000.QrCTeIt3InfQcUnid.Value, Valor) then
          cInfQ.CUnid := Valor;
        if Def(0, 259, DmCTe_0000.QrCTeIt3InfQtpMed.Value, Valor) then
          cInfQ.TpMed := Valor;
        if Def(0, 260, DmCTe_0000.QrCTeIt3InfQqCarga.Value, Valor) then
          cInfQ.QCarga := Valor;
        //
        DmCTe_0000.QrCteIt3InfQ.Next;
      end;
      //
      (*261*) // infDoc - Informa��es dos documentos transportados pelo CT-e Opcional para Redespacho Intermediario e Servi�o vinculado a multimodal.
      //
      (*262*) // infNF - Informa��es das NF - (01 - NF Modelo 01/1A e Avulsa; 04 - NF de Produtor)
      DmCTe_0000.ReopenCteIt2InfDoc(FatID, FatNum, Empresa, ctemtdiNFsOld);
      while not DmCTe_0000.QrCteIt2InfDoc.Eof do
      begin
        cInfNF := cXML.InfCTe.InfCTeNorm.InfDoc.InfNF.Add;
        //
        if Def(0, 263, DmCTe_0000.QrCTeIt2InfDocInfNF_nRoma.Value, Valor) then
          cInfNF.NRoma := Valor;
        if Def(0, 264, DmCTe_0000.QrCTeIt2InfDocInfNF_nPed.Value, Valor) then
          cInfNF.NPed := Valor;
        if Def(0, 265, DmCTe_0000.QrCTeIt2InfDocInfNF_mod.Value, Valor) then
          cInfNF.Mod_ := Valor;
        if Def(0, 266, DmCTe_0000.QrCTeIt2InfDocInfNF_serie.Value, Valor) then
          cInfNF.Serie := Valor;
        if Def(0, 267, DmCTe_0000.QrCTeIt2InfDocInfNF_nDoc.Value, Valor) then
          cInfNF.NDoc := Valor;
        if Def(0, 268, DmCTe_0000.QrCTeIt2InfDocInfNF_dEmi.Value, Valor) then
          cInfNF.DEmi := Valor;
        if Def(0, 269, DmCTe_0000.QrCTeIt2InfDocInfNF_vBC.Value, Valor) then
          cInfNF.VBC := Valor;
        if Def(0, 270, DmCTe_0000.QrCTeIt2InfDocInfNF_vICMS.Value, Valor) then
          cInfNF.vICMS := Valor;
        if Def(0, 271, DmCTe_0000.QrCTeIt2InfDocInfNF_vBCST.Value, Valor) then
          cInfNF.vBCST := Valor;
        if Def(0, 272, DmCTe_0000.QrCTeIt2InfDocInfNF_vST.Value, Valor) then
          cInfNF.vST := Valor;
        if Def(0, 273, DmCTe_0000.QrCTeIt2InfDocInfNF_vProd.Value, Valor) then
          cInfNF.VProd := Valor;
        if Def(0, 274, DmCTe_0000.QrCTeIt2InfDocInfNF_vNF.Value, Valor) then
          cInfNF.VNF := Valor;
        if Def(0, 275, DmCTe_0000.QrCTeIt2InfDocInfNF_nCFOP.Value, Valor) then
          cInfNF.NCFOP := Valor;
        if Def(0, 276, DmCTe_0000.QrCTeIt2InfDocInfNF_nPeso.Value, Valor) then
          cInfNF.NPeso := Valor;
        if Def(0, 277, DmCTe_0000.QrCTeIt2InfDocInfNF_PIN.Value, Valor) then
          cInfNF.PIN := Valor;
        if Def(0, 278, DmCTe_0000.QrCTeIt2InfDocInfNF_dPrev.Value, Valor) then
          cInfNF.DPrev := Valor;
        //
        (*279*) // infUnidTransp - Informa��es das Unidades de Transporte (Carreta/Reboque/Vag�o)
        (*280*) //   tpUnidTransp
        (*281*) //   idUnidTransp
        (*282*) //   lacUnidTransp - Lacres das Unidades de Transporte
        (*283*) //     nLacre
        (*284*) //   infUnidCarga - Informa��es das Unidades de Carga (Containeres/ULD/Outros)
        (*285*) //     tpUnidCarga
        (*286*) //     idUnidCarga
        (*287*) //     lacUnidCarga - Lacres das Unidades de Carga
        (*288*) //       nLacre
        (*289*) //     qtdRat
        (*290*) //   qtdRat
        (*291*) // infUnidCarga - Informa��es das Unidades de Carga (Containeres/ULD/Outros)
        (*292*) //   tpUnidCarga
        (*293*) //   idUnidCarga
        (*294*) //   lacUnidCarga - Lacres das Unidades de Carga
        (*295*) //     nLacre
        (*296*) //   qtdRat
        //
        DmCTe_0000.QrCteIt2InfDoc.Next;
      end;
      (*297*) // infNFe - Informa��es das CT-e
      DmCTe_0000.ReopenCteIt2InfDoc(FatID, FatNum, Empresa, ctemtdiNFe);
      while not DmCTe_0000.QrCteIt2InfDoc.Eof do
      begin
        cInfNFe := cXML.InfCTe.InfCTeNorm.InfDoc.InfNFe.Add;
        //
        if Def(0, 298, DmCTe_0000.QrCTeIt2InfDocInfNFe_chave.Value, Valor) then
          cInfNFe.Chave := Valor;
        if Def(0, 299, DmCTe_0000.QrCTeIt2InfDocInfNFe_PIN.Value, Valor) then
          cInfNFe.PIN := Valor;
        if Def(0, 300, DmCTe_0000.QrCTeIt2InfDocInfNFe_dPrev.Value, Valor) then
          cInfNFe.DPrev := Valor;
        //
        (*301*) // infUnidTransp - Informa��es das Unidades de Transporte (Carreta/Reboque/Vag�o)
        (*302*) //   tpUnidTransp
        (*303*) //   idUnidTransp
        (*304*) //   lacUnidTransp - Lacres das Unidades de Transporte
        (*305*) //     nLacre
        (*306*) //   infUnidCarga - Informa��es das Unidades de Carga (Containeres/ULD/Outros)
        (*307*) //     tpUnidCarga
        (*308*) //     idUnidCarga
        (*309*) //     lacUnidCarga - Lacres das Unidades de Carga
        (*310*) //       nLacre
        (*311*) //     qtdRat
        (*312*) //   qtdRat
        (*313*) // infUnidCarga - Informa��es das Unidades de Carga (Containeres/ULD/Outros)
        (*314*) //   tpUnidCarga
        (*315*) //   idUnidCarga
        (*316*) //   lacUnidCarga - Lacres das Unidades de Carga
        (*317*) //     nLacre
        (*318*) //   qtdRat
        //
        DmCTe_0000.QrCteIt2InfDoc.Next;
      end;
      //
      (*319*) // infOutros - Informa��es dos demais documentos
      DmCTe_0000.ReopenCteIt2InfDoc(FatID, FatNum, Empresa, ctemtdiOutrosDoc);
      while not DmCTe_0000.QrCteIt2InfDoc.Eof do
      begin
        cInfOutros := cXML.InfCTe.InfCTeNorm.InfDoc.InfOutros.Add;
        //
        if Def(0, 320, DmCTe_0000.QrCTeIt2InfDocInfOutros_tpDoc.Value, Valor) then
          cInfOutros.TpDoc := Valor;
        if Def(0, 321, DmCTe_0000.QrCTeIt2InfDocInfOutros_descOutros.Value, Valor) then
          cInfOutros.DescOutros := Valor;
        if Def(0, 322, DmCTe_0000.QrCTeIt2InfDocInfOutros_nDoc.Value, Valor) then
          cInfOutros.NDoc := Valor;
        if Def(0, 323, DmCTe_0000.QrCTeIt2InfDocInfOutros_dEmi.Value, Valor) then
          cInfOutros.DEmi := Valor;
        if Def(0, 324, DmCTe_0000.QrCTeIt2InfDocInfOutros_vDocFisc.Value, Valor) then
          cInfOutros.VDocFisc := Valor;
        if Def(0, 325, DmCTe_0000.QrCTeIt2InfDocInfOutros_dPrev.Value, Valor) then
          cInfOutros.DPrev := Valor;
        //
        (*326*) // infUnidTransp - Informa��es das Unidades de Transporte (Carreta/Reboque/Vag�o)
        (*327*) //   tpUnidTransp
        (*328*) //   idUnidTransp
        (*329*) //   lacUnidTransp - Lacres das Unidades de Transporte
        (*330*) //     nLacre
        (*331*) //   infUnidCarga - Informa��es das Unidades de Carga (Containeres/ULD/Outros)
        (*332*) //     tpUnidCarga
        (*333*) //     idUnidCarga
        (*334*) //     lacUnidCarga - Lacres das Unidades de Carga
        (*335*) //       nLacre
        (*336*) //     qtdRat
        (*337*) //   qtdRat
        (*338*) // infUnidCarga - Informa��es das Unidades de Carga (Containeres/ULD/Outros)
        (*339*) //   tpUnidCarga
        (*340*) //   idUnidCarga
        (*341*) //   lacUnidCarga - Lacres das Unidades de Carga
        (*342*) //     nLacre
        (*343*) //   qtdRat
        //
        DmCTe_0000.QrCteIt2InfDoc.Next;
      end;
      (*344*) // docAnt
      (*345*) //   emiDocAnt
      (*346*) //     CNPJ
      (*347*) //     CPF
      (*348*) //     IE
      (*349*) //     UF
      (*350*) //     xNome
      (*351*) //     idDocAnt
      (*352*) //       idDocAntPap
      (*353*) //         tpDoc
      (*354*) //         serie
      (*355*) //         subser
      (*356*) //         nDoc
      (*357*) //         dEmi
      (*358*) //       idDocAntEle
      (*359*) //         chave
      //
      (*360*) // seg - Informa��es do seguro da carga
      DmCTe_0000.ReopenCteIt2Seg(FatID, FatNum, Empresa);
      while not DmCTe_0000.QrCteIt2Seg.Eof do
      begin
        cSeg := cXML.InfCTe.InfCTeNorm.Seg.Add;
        //
        if Def(0, 361, DmCTe_0000.QrCteIt2SegrespSeg.Value, Valor) then
          cSeg.RespSeg := Valor;
        if Def(0, 362, DmCTe_0000.QrCteIt2SegxSeg.Value, Valor) then
          cSeg.XSeg := Valor;
        if Def(0, 363, DmCTe_0000.QrCteIt2SegnApol.Value, Valor) then
          cSeg.NApol := Valor;
        if Def(0, 364, DmCTe_0000.QrCteIt2SegnAver.Value, Valor) then
          cSeg.NAver := Valor;
        if Def(0, 365, DmCTe_0000.QrCteIt2SegvCarga.Value, Valor) then
          cSeg.VCarga := Valor;
        //
        DmCTe_0000.QrCteIt2Seg.Next;
      end;
      (*366*) // infModal
      if TCTeModal(DmCTe_0000.QrCTeCabAide_Modal.Value) = ctemodalRodoviario then
      begin
        if Def(0, 367, FVersaoModal_Rodoviario, Valor) then
          cXML.InfCTe.InfCTeNorm.InfModal.VersaoModal := Valor;
        //
        AnyNode := cXML.InfCTe.InfCTeNorm.InfModal.AddChild('_');
        AnyNode.Text := '';
        //
////////////////////////////////////////////////////////////////////////////////
/// Layout Modal Rodovi�rio ////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
        (*368*)GerarXMLmodalRodo(FatID, FatNum, Empresa, GravaCampos);
///
////////////////////////////////////////////////////////////////////////////////
      end else
      begin
        Geral.MB_Aviso('Modal selecionado n�o implementado! Solicite � DERMATEK');
      (*
      (ctemodalIndefinido=0, =1, ctemodalAereo=2,
      ctemodalAquaviario=3, ctemodalFerroviario=4,
      ctemodalDutoviario=5, ctemodalMultimodal=6);
      *)
      end;
       //
      (*369*) // peri - Preenchido quando for transporte de produtos classificados pela ONU como perigosos.
      DmCTe_0000.ReopenCteIt2Peri(FatID, FatNum, Empresa);
      while not DmCTe_0000.QrCteIt2Peri.Eof do
      begin
        cPeri := cXML.InfCTe.InfCTeNorm.Peri.Add;
        //
        if Def(0, 370, DmCTe_0000.QrCteIt2PerinONU.Value, Valor) then
          cPeri.NONU := Valor;
        if Def(0, 371, DmCTe_0000.QrCteIt2PerixNomeAE.Value, Valor) then
          cPeri.XNomeAE := Valor;
        if Def(0, 372, DmCTe_0000.QrCteIt2PerixClaRisco.Value, Valor) then
          cPeri.XClaRisco := Valor;
        if Def(0, 373, DmCTe_0000.QrCteIt2PerigrEmb.Value, Valor) then
          cPeri.GrEmb := Valor;
        if Def(0, 374, DmCTe_0000.QrCteIt2PeriqTotProd.Value, Valor) then
          cPeri.QTotProd := Valor;
        if Def(0, 375, DmCTe_0000.QrCteIt2PeriqVolTipo.Value, Valor) then
          cPeri.QVolTipo := Valor;
        if Def(0, 376, DmCTe_0000.QrCteIt2PeripontoFulgor.Value, Valor) then
          cPeri.PontoFulgor := Valor;
        //
        DmCTe_0000.QrCteIt2Peri.Next;
      end;
      //
      (*377*) // veicNovos - Informa��es dos ve�culos transportados
        (*378*) // chassi
        (*379*) // nCor
        (*380*) // xCor
        (*381*) // cMod
        (*382*) // vUnit
        (*383*) // vFrete
      //
      (*384*) // cobr - Dados da cobran�a do CT-e
        (*385*) // fat - Dados da fatura
      if Def(0, 386, DmCTe_0000.QrCTeCabAcobr_Fat_nFat.Value, Valor) then
        cXML.InfCTe.InfCTeNorm.Cobr.Fat.NFat := Valor;
      if Def(0, 387, DmCTe_0000.QrCTeCabAcobr_Fat_vOrig.Value, Valor) then
        cXML.InfCTe.InfCTeNorm.Cobr.Fat.VOrig := Valor;
      if Def(0, 388, DmCTe_0000.QrCTeCabAcobr_Fat_vDesc.Value, Valor) then
        cXML.InfCTe.InfCTeNorm.Cobr.Fat.VDesc := Valor;
      if Def(0, 389, DmCTe_0000.QrCTeCabAcobr_Fat_vLiq.Value, Valor) then
        cXML.InfCTe.InfCTeNorm.Cobr.Fat.VLiq := Valor;
        (*390*) // dup - Dados das duplicatas
      DmCTe_0000.ReopenCteIt2Peri(FatID, FatNum, Empresa);
      while not DmCTe_0000.QrCteIt3Dup.Eof do
      begin
        cDup := cXML.InfCTe.InfCTeNorm.Cobr.Dup.Add;
        //
        if Def(0, 391, DmCTe_0000.QrCTeIt3DupnDup.Value, Valor) then
          cDup.NDup := Valor;
        //DataHora :=
        if Def(0, 392, DmCTe_0000.QrCTeIt3DupdVenc.Value, Valor) then
          cDup.DVenc := Valor;
        if Def(0, 393, DmCTe_0000.QrCTeIt3DupvDup.Value, Valor) then
          cDup.VDup := Valor;
        //
        DmCTe_0000.QrCteIt3Dup.Next;
      end;
      //
      if TCTeTpCTe(DmCTe_0000.QrCTeCabAide_tpCTe.Value) = ctetpcteSubstituto then
      begin
        (*394*) // infCteSub - Informa��es do CT-e de substitui��o
        if Def(0, 395, DmCTe_0000.QrCTeCabAinfCteSub_chCte.Value, Valor) then
          cXML.InfCTe.InfCTeNorm.InfCteSub.ChCte := Valor;
        //
        (*396*) // tomaICMS
        if DmCTe_0000.QrCTeCabAinfCteSub_ContribICMS.Value = 1 then
        begin
          case DmCTe_0000.QrCTeCabAinfCteSub_TipoDoc.Value of
            1: // N F e
            begin
              if Def(0, 397, DmCTe_0000.QrCTeCabAinfCteSub_refNFe.Value, Valor) then
                cXML.InfCTe.InfCTeNorm.InfCteSub.TomaICMS.RefNFe := Valor;
            end;
            2: // NF 1/1A CT
            begin
              (*398*) // refNF
              //Geral.MB_Aviso('"tomaICMS.refNF" n�o implementado: ');
              if (Trim(DmCTe_0000.QrCTeCabAinfCteSub_CNPJ.Value) <> '') then
              begin
                if Def(0, 399, DmCTe_0000.QrCTeCabAinfCteSub_CNPJ.Value, Valor) then
                  cXML.InfCTe.InfCTeNorm.InfCteSub.TomaICMS.RefNF.CNPJ := Valor;
              end else
              begin
                if Def(0, 400, DmCTe_0000.QrCTeCabAinfCteSub_CPF.Value, Valor) then
                  cXML.InfCTe.InfCTeNorm.InfCteSub.TomaICMS.RefNF.CPF := Valor;
              end;
              if Def(0, 401, DmCTe_0000.QrCTeCabAinfCteSub_mod.Value, Valor) then
                cXML.InfCTe.InfCTeNorm.InfCteSub.TomaICMS.RefNF.Mod_ := Valor;
              if Def(0, 402, DmCTe_0000.QrCTeCabAinfCteSub_serie.Value, Valor) then
                cXML.InfCTe.InfCTeNorm.InfCteSub.TomaICMS.RefNF.Serie := Valor;
              if Def(0, 403, DmCTe_0000.QrCTeCabAinfCteSub_subserie.Value, Valor) then
                cXML.InfCTe.InfCTeNorm.InfCteSub.TomaICMS.RefNF.Subserie := Valor;
              if Def(0, 404, DmCTe_0000.QrCTeCabAinfCteSub_nro.Value, Valor) then
                cXML.InfCTe.InfCTeNorm.InfCteSub.TomaICMS.RefNF.Nro := Valor;
              if Def(0, 405, DmCTe_0000.QrCTeCabAinfCteSub_valor.Value, Valor) then
                cXML.InfCTe.InfCTeNorm.InfCteSub.TomaICMS.RefNF.Valor := Valor;
              if Def(0, 406, DmCTe_0000.QrCTeCabAinfCteSub_dEmi.Value, Valor) then
                cXML.InfCTe.InfCTeNorm.InfCteSub.TomaICMS.RefNF.DEmi := Valor;
            end;
            3: // CTe
            begin
              if Def(0, 407, DmCTe_0000.QrCTeCabAinfCteSub_refCTe.Value, Valor) then
                cXML.InfCTe.InfCTeNorm.InfCteSub.TomaICMS.RefCte := Valor;
            end;
            else
            begin
              Geral.MB_Aviso('"tomaICMS.refNF" n�o implementado: ' +
              Geral.FF0(DmCTe_0000.QrCTeCabAinfCteSub_TipoDoc.Value));
            end;
          end;
        end else
        (*408*) // tomaNaoICMS
        begin
          if Def(0, 409, DmCTe_0000.QrCTeCabAinfCteSub_refCTeAnu.Value, Valor) then
            cXML.InfCTe.InfCTeNorm.InfCteSub.TomaNaoICMS.RefCteAnu := Valor;
        end;
      end;
    end;
    (*1*)ctetpcteComplValrs: (*410*) // infCteComp
    begin
      if Def(0, 411, DmCTe_0000.QrCTeCabAinfCTeComp_chave.Value, Valor) then
        cXML.InfCTe.InfCteComp.Chave := Valor;
    end;
    (*2*)ctetpcteAnulacao: (*412*) // infCteAnu
    begin
      if Def(0, 413, DmCTe_0000.QrCTeCabAinfCTeAnu_chCte.Value, Valor) then
        cXML.InfCTe.InfCteAnu.ChCte := Valor;
      if Def(0, 414, DmCTe_0000.QrCTeCabAinfCTeAnu_dEmi.Value, Valor) then
        cXML.InfCTe.InfCteAnu.DEmi := Valor;
    end;
  end;
  //
  (*415*) // Autorizados para download do XML do DF-e
  DmCTe_0000.ReopenCteIt1AutXml(FatID, FatNum, Empresa);
  while not DmCTe_0000.QrCteIt1AutXml.Eof do
  begin
    cAutXml := cXML.InfCTe.AutXML.Add;
    //
    if Trim(DmCTe_0000.QrCTeIt1AutXmlCNPJ.Value) <> '' then
    begin
      if Def(0, 416, DmCTe_0000.QrCTeIt1AutXmlCNPJ.Value, Valor) then
        cAutXml.CNPJ := Valor;
    end else
    if Trim(DmCTe_0000.QrCTeIt1AutXmlCPF.Value) <> '' then
    begin
      if Def(0, 417, DmCTe_0000.QrCTeIt1AutXmlCPF.Value, Valor) then
        cAutXml.CPF := Valor;
    end;
    (*418*) // ds:Signature
    //
    DmCTe_0000.QrCteIt1AutXml.Next;
  end;
  //
  Result := True;
end;

function TFmCTeGeraXML_0200a.GerarXMLmodalRodo(FatID, FatNum, Empresa,
  GravaCampos: Integer): Boolean;
var
  Valor: String;
  DataHora: TDateTime;
begin
  rodoXML := TXMLDocument.Create(nil);
  rodoXML.Active := False;
  rodoXML.FileName := '';
  //rodoXML.DocumentElement.Attributes['xmlns'] := '';
  cRodo := GetRodo(rodoXML);
  //rodoXML.Version := '1.0';
  //rodoXML.Encoding := 'UTF-8';
  DmCTe_0000.ReopenCTeMoRodoCab(FatID, FatNum, Empresa);
  DmCTe_0000.QrCTeMoRodoCab.First;
  while not DmCTe_0000.QrCTeMoRodoCab.Eof do
  begin
    if Def(1, 2, DmCTe_0000.QrCTeMoRodoCabRNTRC.Value, Valor) then
      cRodo.RNTRC := Valor;
    DataHora := DmCTe_0000.QrCTeMoRodoCabdPrev.Value;
    if Def(1, 3, DataHora, Valor) then
      cRodo.DPrev := Valor;
    if Def(1, 4, DmCTe_0000.QrCTeMoRodoCablota.Value, Valor) then
      cRodo.Lota := Valor;
    if Def(1, 5, DmCTe_0000.QrCTeMoRodoCabCIOT.Value, Valor) then
      cRodo.CIOT := Valor;
    //
    DmCTe_0000.QrCTeMoRodoCab.Next;
  end;
   //
  (*6*) // occ - Ordens de coleta associadas
  DmCTe_0000.ReopenCTeMoRodoOcc(FatID, FatNum, Empresa);
  DmCTe_0000.QrCTeMoRodoOcc.First;
  while not DmCTe_0000.QrCTeMoRodoOcc.Eof do
  begin
    if Def(1, 7, DmCTe_0000.QrCTeMoRodoOccserie.Value, Valor) then
      cRodo_occ.Serie := Valor;
    if Def(1, 8, DmCTe_0000.QrCTeMoRodoOccnOcc.Value, Valor) then
      cRodo_occ.NOcc := Valor;
    DataHora := DmCTe_0000.QrCTeMoRodoOccdEmi.Value;
    if Def(1, 9, DataHora, Valor) then
      cRodo_occ.DEmi := Valor;
    (*10*) // emiOcc
      if Def(1, 11, Geral.SoNumero_TT(DmCTe_0000.QrCTeMoRodoOccemiOcc_CNPJ.Value), Valor) then
        cRodo_occ.EmiOcc.CNPJ := Valor;
      if Def(1, 12, DmCTe_0000.QrCTeMoRodoOccemiOcc_cInt.Value, Valor) then
        cRodo_occ.EmiOcc.CInt := Valor;
      if Def(1, 13, DmCTe_0000.QrCTeMoRodoOccemiOcc_IE.Value, Valor) then
        cRodo_occ.EmiOcc.IE := Valor;
      if Def(1, 14, DmCTe_0000.QrCTeMoRodoOccemiOcc_UF.Value, Valor) then
        cRodo_occ.EmiOcc.UF := Valor;
      if Def(1, 15, DmCTe_0000.QrCTeMoRodoOccemiOcc_fone.Value, Valor) then
        cRodo_occ.EmiOcc.Fone := Valor;
    //
    DmCTe_0000.QrCTeMoRodoOcc.Next;
  end;
  //
  (*16*) // valePed - Informa��es de vale ped�gio
  DmCTe_0000.ReopenCTeMoRodoVPed(FatID, FatNum, Empresa);
  DmCTe_0000.QrCTeMoRodoVPed.First;
  while not DmCTe_0000.QrCTeMoRodoVPed.Eof do
  begin
    cRodo_valePed := cRodo.ValePed.Add;
    //
    if Def(1, 17, DmCTe_0000.QrCTeMoRodoVPedCNPJForn.Value, Valor) then
      cRodo_valePed.CNPJForn := Valor;
    if Def(1, 18, DmCTe_0000.QrCTeMoRodoVPednCompra.Value, Valor) then
      cRodo_valePed.NCompra := Valor;
    if Def(1, 19, DmCTe_0000.QrCTeMoRodoVPedCNPJPg.Value, Valor) then
      cRodo_valePed.CNPJPg := Valor;
    if Def(1, 20, DmCTe_0000.QrCTeMoRodoVPedvValePed.Value, Valor) then
      cRodo_valePed.VValePed := Valor;
    //
    DmCTe_0000.QrCTeMoRodoVPed.Next;
  end;
  //
  (*21*) // veic - Dados dos Veiculos
  DmCTe_0000.ReopenCTeMoRodoVeic(FatID, FatNum, Empresa);
  DmCTe_0000.QrCTeMoRodoVeic.First;
  while not DmCTe_0000.QrCTeMoRodoVeic.Eof do
  begin
    cRodo_Veic := cRodo.Veic.Add;
    //
    if Def(1, 22, DmCTe_0000.QrCTeMoRodoVeiccInt.Value, Valor) then
      cRodo_veic.CInt := Valor;
    if Def(1, 23, DmCTe_0000.QrCTeMoRodoVeicRENAVAM.Value, Valor) then
      cRodo_veic.RENAVAM := Valor;
    if Def(1, 24, DmCTe_0000.QrCTeMoRodoVeicplaca.Value, Valor) then
      cRodo_veic.Placa := Valor;
    if Def(1, 25, DmCTe_0000.QrCTeMoRodoVeictara.Value, Valor) then
      cRodo_veic.Tara := Valor;
    if Def(1, 26, DmCTe_0000.QrCTeMoRodoVeiccapKG.Value, Valor) then
      cRodo_veic.CapKG := Valor;
    if Def(1, 27, DmCTe_0000.QrCTeMoRodoVeiccapM3.Value, Valor) then
      cRodo_veic.CapM3 := Valor;
    if Def(1, 28, DmCTe_0000.QrCTeMoRodoVeictpProp.Value, Valor) then
      cRodo_veic.TpProp := Valor;
    if Def(1, 29, DmCTe_0000.QrCTeMoRodoVeictpVeic.Value, Valor) then
      cRodo_veic.TpVeic := Valor;
    if Def(1, 30, DmCTe_0000.QrCTeMoRodoVeictpRod.Value, Valor) then
      cRodo_veic.TpRod := Valor;
    if Def(1, 31, DmCTe_0000.QrCTeMoRodoVeictpCar.Value, Valor) then
      cRodo_veic.TpCar := Valor;
    if Def(1, 32, DmCTe_0000.QrCTeMoRodoVeicUF.Value, Valor) then
      cRodo_veic.UF := Valor;
    (*33*) // prop - Propriet�rios do Ve�culo.
           //        S� preenchido quando o ve�culo n�o
           //        pertencer � empresa emitente do CT-e
    DmCTe_0000.ReopenCTeMoRodoProp(FatID, FatNum, Empresa, DmCTe_0000.QrCTeMoRodoVeicConta.Value);
    DmCTe_0000.QrCTeMoRodoProp.First;
    while not DmCTe_0000.QrCTeMoRodoProp.Eof do
    begin
      if Trim(Geral.SoNumero_TT(DmCTe_0000.QrCTeMoRodoPropCPF.Value)) <> '' then
      begin
        if Def(1, 34, Geral.SoNumero_TT(DmCTe_0000.QrCTeMoRodoPropCPF.Value), Valor) then
          cRodo_veic.Prop.CPF := Valor;
      end else
      begin
        if Def(1, 35, Geral.SoNumero_TT(DmCTe_0000.QrCTeMoRodoPropCNPJ.Value), Valor) then
          cRodo_veic.Prop.CNPJ := Valor;
      end;
      if Def(1, 36, DmCTe_0000.QrCTeMoRodoPropRNTRC.Value, Valor) then
        cRodo_veic.Prop.RNTRC := Valor;
      if Def(1, 37, DmCTe_0000.QrCTeMoRodoPropxNome.Value, Valor) then
        cRodo_veic.Prop.XNome := Valor;
      if Trim(Geral.SoNumero_TT(DmCTe_0000.QrCTeMoRodoPropCNPJ.Value)) <> '' then
      begin
        if Def(1, 38, DmCTe_0000.QrCTeMoRodoPropIE.Value, Valor) then
          cRodo_veic.Prop.IE := Valor;
      end;
      if Def(1, 39, DmCTe_0000.QrCTeMoRodoPropUF.Value, Valor) then
        cRodo_veic.Prop.UF := Valor;
      if Def(1, 40, DmCTe_0000.QrCTeMoRodoProptpProp.Value, Valor) then
        cRodo_veic.Prop.TpProp := Valor;
      //
      DmCTe_0000.QrCTeMoRodoProp.Next;
    end;
    //
    DmCTe_0000.QrCTeMoRodoVeic.Next;
  end;
  //
  (*41*) // lacRodo - Lacres
  DmCTe_0000.ReopenCTeMoRodoLacr(FatID, FatNum, Empresa);
  DmCTe_0000.QrCTeMoRodoLacr.First;
  while not DmCTe_0000.QrCTeMoRodoLacr.Eof do
  begin
    cRodo_lacRodo := cRodo.LacRodo.Add;
    //
    if Def(1, 42, DmCTe_0000.QrCTeMoRodoLacrnLacre.Value, Valor) then
      cRodo_lacRodo.NLacre := Valor;
    //
    DmCTe_0000.QrCTeMoRodoLacr.Next;
  end;
  //
  (*43*) // moto - Informa��es do(s) Motorista(s)
  DmCTe_0000.ReopenCTeMoRodoMoto(FatID, FatNum, Empresa);
  DmCTe_0000.QrCTeMoRodoMoto.First;
  while not DmCTe_0000.QrCTeMoRodoMoto.Eof do
  begin
    cRodo_moto := cRodo.Moto.Add;
    //
    if Def(1, 44, DmCTe_0000.QrCTeMoRodoMotoxNome.Value, Valor) then
      cRodo_moto.XNome := Valor;
    if Def(1, 45, Geral.SoNumero_TT(DmCTe_0000.QrCTeMoRodoMotoCPF.Value), Valor) then
      cRodo_moto.CPF := Valor;
    //
    DmCTe_0000.QrCTeMoRodoMoto.Next;
  end;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  modalXML := rodoXML.XML.Text;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

function TFmCTeGeraXML_0200a.MontaID_Inutilizacao(const cUF, Ano, emitCNPJ,
  Modelo, Serie, nCTIni, nCTFim: String; var Id: String): Boolean;
var
  K: Integer;
begin
  Id := 'ID' + cUF + (*Ano +*) emitCNPJ + Modelo +
    XXe_PF.StrZero(StrToInt(Serie),3,0) +
    XXe_PF.StrZero(StrToInt(nCTIni),9,0) +
    XXe_PF.StrZero(StrToInt(nCTFim),9,0);
  K := Length(Id);
  if K = 41 then
    Result := True
  else begin
    Result := False;
    Geral.MB_Erro('ID de inutiliza��o com tamanho inv�lido: "' +
    Id + '". Deveria ter 41 carateres e tem ' + IntToStr(K) + '.');
  end;
end;

function TFmCTeGeraXML_0200a.NomeAcao(Acao: TTipoConsumoWS_CTe): String;
begin
  Result := sTipoConsumoWS_CTe[Integer(Acao)];
end;

function TFmCTeGeraXML_0200a.ObtemWebServer2(UFServico: String; Ambiente,
  CodigoUF: Integer; Acao: TTipoConsumoWS_CTe; sAviso: String): Boolean;

  function TextoAmbiente(Ambiente: Byte): String;
  begin
    case Ambiente of
        1: Result := 'Produ��o';
        2: Result := 'Homologa��o';
      else Result := '[Desconhecido]';
    end;
  end;

  function TextoAcao(Acao: TTipoConsumoWS_CTe): String;
  begin
    case Acao of
      tcwscteStatusServico:    Result := 'Status do servi�o';
       else Result := '[Desconhecido]';
    end;
  end;

  function TipoConsumoWs2Acao(TipoConsumoWS: TTipoConsumoWS_CTe): Byte;
  begin
    Result := Integer(TipoConsumoWS);
  end;

var
  cUF, A: Integer;
  Versao, NomeA, x: String;
begin
  Versao := VersaoWS(Acao, True);
  NomeA  := NomeAcao(Acao);
  FURL   := CTe_PF.ObtemURLWebServerCTe(UFServico, NomeA, Versao, Ambiente);
  A      := TipoConsumoWs2Acao(Acao);
  cUF    := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(UFServico);
  x      := Uppercase(UFServico + ' ' + NomeA + ' ' + Versao);
  //
  if FURL = '' then
  begin
    sAviso := 'N�o foi poss�vel definir o endere�o do Web Service! ' + sLineBreak  +
    x + sLineBreak +
    'UF SEFAZ: ' + Geral.FF0(CodigoUF) + ' = ' + Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(CodigoUF)
    + sLineBreak  + 'UF Servi�o: ' + Geral.FF0(cUF) + ' = ' + UFServico
    + sLineBreak  + 'Ambiente: ' + Geral.FF0(Ambiente)
    + ' = ' + TextoAmbiente(Ambiente)
    + sLineBreak  + 'A��o: ' + Geral.FF0(A) + ' = ' + TextoAcao(Acao)
    + sLineBreak + 'Vers�o: ' + Versao
    + sLineBreak + 'x = "' + x + '"' + sLineBreak +
    sLineBreak  + 'AVISE A DERMATEK' + sLineBreak;
    Result := False;
    Geral.MB_Aviso(sAviso);
    Exit;
  end else begin
    Result := True;
    sAviso := FURL;
  end;
end;

procedure TFmCTeGeraXML_0200a.OnBeforePost(const HTTPReqResp: THTTPReqResp;
  Data: Pointer);
var
  Store        : IStore;
  Certs        : ICertificates;
  Cert         : ICertificate2;
  CertContext  : ICertContext;
  PCertContext : PCCERT_CONTEXT;
  i : Integer;
  ContentHeader: string;
begin
  Store := CoStore.Create;
  //Reposit�rios de Certifcados da M�quina

  Store.Open( CAPICOM_CURRENT_USER_STORE, 'MY',    CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED );
  //Abre a lista de certificados

  Certs := Store.Certificates ;
  //Aloca todos os certificados instalados na m�quia

  i := 0;
  //loop de procura ao certificado requerido pelo n�mero serial
  while i < Certs.Count do
   begin
     Cert := IInterface( Certs.Item[ i+1 ] ) as ICertificate2;
     //Cria objeto para acesso a leitura do certificado
     if UpperCase(Cert.SerialNumber ) = UpperCase(CO_CTE_CERTIFICADO_DIGITAL_SERIAL_NUMBER) then
     //se o n�mero do serial for igual ao que queremos utilizar
      begin
        //carrega informa��es do certificado
        CertContext := Cert as ICertContext;
        CertContext.Get_CertContext( Integer( PCertContext ) );

        //if not (InternetSetOption(Data, 84, PCertContext, Sizeof(CERT_CONTEXT))) then
        if not InternetSetOption(Data, INTERNET_OPTION_CLIENT_CERT_CONTEXT, PCertContext, sizeof(CertContext)*5) then
        begin
          Geral.MB_Aviso('Falha ao selecionar o certificado.');
        end;

        ContentHeader := Format(ContentTypeTemplate, ['application/soap+xml; charset=utf-8']);
        HttpAddRequestHeaders(Data, PChar(ContentHeader), Length(ContentHeader), HTTP_ADDREQ_FLAG_REPLACE);

        i := Certs.Count;
        //encerra o loop
      end;
     i := i + 1;
  end;
end;

function TFmCTeGeraXML_0200a.SeparaDados(Texto: String; Chave: String;
  MantemChave: Boolean): String;
var
  PosIni, PosFim : Integer;
begin
  if MantemChave then
   begin
     PosIni := Pos(Chave,Texto)-1;
     PosFim := Pos('/'+Chave,Texto)+length(Chave)+3;

     if (PosIni = 0) or (PosFim = 0) then
      begin
        PosIni := Pos('ns2:'+Chave,Texto)-1;
        PosFim := Pos('/ns2:'+Chave,Texto)+length(Chave)+3;
      end;
   end
  else
   begin
     PosIni := Pos(Chave,Texto)+Pos('>',copy(Texto,Pos(Chave,Texto),length(Texto)));
     PosFim := Pos('/'+Chave,Texto);

     if (PosIni = 0) or (PosFim = 0) then
      begin
        PosIni := Pos('ns2:'+Chave,Texto)+Pos('>',copy(Texto,Pos('ns2:'+Chave,Texto),length(Texto)));
        PosFim := Pos('/ns2:'+Chave,Texto);
      end;
   end;
  Result := copy(Texto,PosIni,PosFim-(PosIni+1));
  if Result = '' then
    Geral.MB_Erro(Texto);
end;

function TFmCTeGeraXML_0200a.TipoXML(NoStandAlone: Boolean): String;
begin
  Result :=
    '<?xml version="' + sXML_Version + '" encoding="' + sXML_Encoding + '"';
  if NoStandAlone then Result := Result + ' standalone="no"';
  Result := Result +  '?>';
end;

function TFmCTeGeraXML_0200a.VersaoWS(Acao: TTipoConsumoWS_CTe;
  Formata: Boolean): String;
begin
  Result := Trim(sVersaoConsumoWS_CTe[Integer(Acao)]);
  if Result = '' then
  begin
    Geral.MB_Aviso('Vers�o n�o definida para "' + NomeAcao(Acao) + '"!');
    Result := '0.00';
  end;
end;

function TFmCTeGeraXML_0200a.WS_CTeConsultaCT(UFServico: String; Ambiente,
  CodigoUF: Byte; ChaveCTe: String; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit): String;
const
  TipoConsumo = tcwscteConsultaCTe;
var
  sAviso, Texto, ProcedimentoEnv, ProcedimentoRet: String;
  Acao: TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  VersaoDados: String;
begin
  Screen.Cursor := crHourGlass;

  VersaoDados := Geral.FFT_Dot(DmCTe_0000.QropcoesCTeCTeVerConCTe.Value, 2, siNegativo);

  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_ConsSitCTe(Ambiente, ChaveCTe, VersaoDados);
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;


  ProcedimentoEnv := 'CteConsultaCT';
  ProcedimentoRet := 'cteConsultaCTResult';
  //ProcedimentoEnv := 'CTeConsulta';
  //ProcedimentoRet := 'cteConsultaResult';

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<cteCabecMsg xmlns="http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</cteCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<cteDadosMsg xmlns="http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</cteDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

     ReqResp := THTTPReqResp.Create(nil);
     ConfiguraReqResp( ReqResp );
     ReqResp.URL := FURL;
     ReqResp.UseUTF8InHeader := True;
     ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv;
  try
       RETxtEnvio.Text :=  Acao.Text;
       EdWS.Text := FURL;
       ReqResp.Execute(Acao.Text, Stream);
       StrStream := TStringStream.Create('');
       StrStream.CopyFrom(Stream, 0);
       Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
       //
       StrStream.Free;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmCTeGeraXML_0200a.WS_CTeInutilizacaoCTe(UFServico: String; Ambiente,
  CodigoUF, Ano: Byte; Id, CNPJ, Mod_, Serie, NCTIni, NCTFin, XJust,
  NumeroSerial: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
  EdWS: TEdit; var ID_Res: String): String;
const
  TipoConsumo = tcwsctePediInutilizacao;
var
  sAviso: String;
var
  Texto, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  Acao: TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
begin
  Screen.Cursor := crHourGlass;
  //
  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_CTeInutCTe(Id, Ambiente, CodigoUF, Ano, CNPJ, Mod_, Serie, NCTIni, NCTFin, XJust);
  ID_Res := Id;
  //DmCTe_0000.ReopenEmpresa(Empresa);
  DmCTe_0000.SalvaXML(CTE_EXT_PED_INU_XML, ID_Res, FDadosTxt, RETxtEnvio, False);

  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;
  if XXe_PF.ObtemCertificado(NumeroSerial, Cert) then
  begin
    if CTe_PF.AssinarMSXML(FDadosTxt, Cert, FAssinTxt) then
    begin
      Acao := TStringList.Create;
      Stream := TMemoryStream.Create;

      VersaoDados := Geral.FFT_Dot(DmCTe_0000.QropcoesCTeCTeVerInuNum.Value, 2, siNegativo);

      ProcedimentoEnv := 'cteInutilizacaoCT';
      ProcedimentoRet := 'cteInutilizacaoCTResult';

      Texto := '<?xml version="1.0" encoding="utf-8"?>';
      Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
      Texto := Texto +   '<soap12:Header>';
      Texto := Texto +     '<cteCabecMsg xmlns="http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv + '">';
      Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
      Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
      Texto := Texto +     '</cteCabecMsg>';
      Texto := Texto +   '</soap12:Header>';
      Texto := Texto +   '<soap12:Body>';
      Texto := Texto +     '<cteDadosMsg xmlns="http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv + '">';
      Texto := Texto + FAssinTxt;
      Texto := Texto +     '</cteDadosMsg>';
      Texto := Texto +   '</soap12:Body>';
      Texto := Texto +'</soap12:Envelope>';

      Acao.Text := Texto;
      Acao.SaveToStream(Stream);

      ReqResp := THTTPReqResp.Create(nil);
      ConfiguraReqResp( ReqResp );
      ReqResp.URL := FURL;
      ReqResp.UseUTF8InHeader := True;
      ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv;
      try
        RETxtEnvio.Text :=  Acao.Text;
        EdWS.Text := FURL;
        ReqResp.Execute(Acao.Text, Stream);
        StrStream := TStringStream.Create('');
        StrStream.CopyFrom(Stream, 0);
        Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
       //
       StrStream.Free;
      finally
        Acao.Free;
        Stream.Free;
      end;
    end;
  end;
end;

function TFmCTeGeraXML_0200a.WS_CTeRecepcaoLote(UFServico: String; Ambiente,
  CodigoUF: Byte; NumeroSerial: String; Lote: Integer; LaAviso1,
  LaAviso2: TLabel; RETxtEnvio: TMemo; EdWS: TEdit;
  Sincronia: TXXeIndSinc): String;
var
  sAviso: String;
  //
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  I: Integer;
  XML_STR: String;
  //
  VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  TipoConsumo_Cte: TTipoConsumoWS_CTe;
begin
  Screen.Cursor := crHourGlass;
  //
  TipoConsumo_Cte := tcwscteRecepcao;
  //VersaoDados :=  verEnviCTeNovo_Versao;
  //
  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo_Cte, sAviso) then Exit;
  //
  FDadosTxt := FmCTeSteps_0200a.FXML_LoteCTe;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  VersaoDados := Geral.FFT_Dot(DmCTe_0000.QropcoesCTeCTeVerEnvLot.Value, 2, siNegativo);
  //ProcedimentoEnv := 'CteAutorizacao';
  //ProcedimentoRet := 'cteAutorizacaoLoteResult';
  ProcedimentoEnv := 'CteRecepcao';
  ProcedimentoRet := 'cteRecepcaoLoteResult';
  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<cteCabecMsg xmlns="http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</cteCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<cteDadosMsg xmlns="http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</cteDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';


  XML_STR := '';
  for i := 1 to Length(Texto) do
  begin
    if Ord(Texto[I]) > 31 then
      XML_STR := XML_STR + Texto[I]
  end;
  Texto := XML_STR;
  //
  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;
  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv;
  try
    RETxtEnvio.Text :=  Acao.Text;
    EdWS.Text := FURL;
    ReqResp.Execute(Acao.Text, Stream);

    StrStream := TStringStream.Create('');
    StrStream.CopyFrom(Stream, 0);

    Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
    //
    StrStream.Free;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmCTeGeraXML_0200a.WS_CTeRetRecepcao(UFServico: String; Ambiente,
  CodigoUF: Byte; Recibo: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
  EdWS: TEdit): String;
var
  sAviso, VersaoDados, Texto, ProcedimentoEnv, ProcedimentoRet: String;
  Acao: TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  TipoConsumo_Cte: TTipoConsumoWS_CTe;
begin
  TipoConsumo_Cte := tcwscteRetRecepcao;
  //
  Screen.Cursor := crHourGlass;
  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo_Cte, sAviso) then Exit;
  //
  FDadosTxt := XML_ConsReciCTe(Ambiente, Recibo);
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  VersaoDados := Geral.FFT_Dot(DmCTe_0000.QropcoesCTeCTeVerConLot.Value, 2, siNegativo);
  //
  ProcedimentoEnv := 'CteRetRecepcao';
  //ProcedimentoRet abaixo!

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<cteCabecMsg xmlns="http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</cteCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<cteDadosMsg xmlns="http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</cteDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;
  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv;
  try
    RETxtEnvio.Text :=  Acao.Text;
    EdWS.Text := FURL;
    ReqResp.Execute(Acao.Text, Stream);
    StrStream := TStringStream.Create('');
    StrStream.CopyFrom(Stream, 0);
    if pos('cteRetRecepcaoResult', StrStream.DataString) > 0 then
      ProcedimentoRet := 'cteRetRecepcaoResult'
    else
      ProcedimentoRet := 'cteRetRecepcaoLoteResult';
    //
    Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
    //
    StrStream.Free;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmCTeGeraXML_0200a.WS_CTeStatusServico(UFServico: String; Ambiente,
  CodigoUF: Byte; Certificado: String; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit): String;
const
  TipoConsumo = tcwscteStatusServico;
var
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
begin
  Screen.Cursor := crHourGlass;
  //
  //EdSerialNumber.Text := Certificado;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_ConsStatServ(Ambiente, CodigoUF);
  FDadosTxt := StringReplace( FDadosTxt, '<' + ENCODING_UTF8_STD + '>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<' + ENCODING_UTF8 + '>', '', [rfReplaceAll] ) ;
  VersaoDados := Geral.FFT_Dot(DmCTe_0000.QropcoesCTeCTeVerStaSer.Value, 2, siNegativo);

(*
  if pos(' N F ESTATUSSERVICO2', UPPERCASE(FURL)) > 0 then
  begin
    ProcedimentoEnv := 'N f e StatusServico2';
    ProcedimentoRet := ' n f e StatusServicoNF2Result';
  end else
  begin
    ProcedimentoEnv := ' N f e StatusServico3';
    ProcedimentoRet := ' n f e StatusServicoNFResult';
  end;
*)
  //
  ProcedimentoEnv := 'CteStatusServico';
  ProcedimentoRet := 'cteStatusServicoCTResult';
  //
  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;
  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<cteCabecMsg xmlns="http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  //Texto := Texto +       '<versaoDados>' + verConsStatServ_Versao + '</versaoDados>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</cteCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<cteDadosMsg xmlns="http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</cteDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

     ReqResp := THTTPReqResp.Create(nil);
     ConfiguraReqResp( ReqResp );
     ReqResp.URL := FURL;
     ReqResp.UseUTF8InHeader := True;

     ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv;
  try
    try
         RETxtEnvio.Text :=  Acao.Text;
         EdWS.Text := FURL;
         ReqResp.Execute(Acao.Text, Stream);
         StrStream := TStringStream.Create('');
         StrStream.CopyFrom(Stream, 0);
         //Result := ??? SeparaDados(StrStream.DataString,'cteStatusServicoNF2Result');
         Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
         //
         StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Consulta Status servi�o:' + sLineBreak +
                              '- Inativo ou Inoperante tente novamente.' + sLineBreak +
                              '- '+E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmCTeGeraXML_0200a.WS_EventoCancelamentoCTe(UFServico: String;
  Ambiente, CodigoUF: Byte; (*NumeroSerial: String; Lote: Integer; LaAviso1,
  LaAviso2: TLabel; RETxtEnvio: TMemo; EdWS: TEdit*)XML_Evento: String;
  TipoConsumo: TTipoConsumoWS_CTe;  RETxtEnvio: TMemo; EdWS: TEdit): String;
var
  Texto : String;
  Acao: TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso, VersaoDados: String;
  //I: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_Evento;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, #13, '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, #10, '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  VersaoDados := Geral.FFT_Dot(DmCTe_0000.QropcoesCTeCTeVerLotEve.Value, 2, siNegativo);

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +    '<soap12:Header>';
  Texto := Texto +      '<cteCabecMsg xmlns="http://www.portalfiscal.inf.br/cte/wsdl/RecepcaoEvento">';
  Texto := Texto +        '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +        '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +      '</cteCabecMsg>';
  Texto := Texto +    '</soap12:Header>';
  Texto := Texto +    '<soap12:Body>';
  Texto := Texto +      '<cteDadosMsg xmlns="http://www.portalfiscal.inf.br/cte/wsdl/RecepcaoEvento">' + FDadosTxt + '</cteDadosMsg>';
  Texto := Texto +    '</soap12:Body>';
  Texto := Texto +  '</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;

  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/cte/wsdl/cteRecepcaoEvento';
  try
    try
      RETxtEnvio.Text :=  Acao.Text;
      EdWS.Text := FURL;
      ReqResp.Execute(Acao.Text, Stream);
      StrStream := TStringStream.Create('');
      StrStream.CopyFrom(Stream, 0);
      Result := SeparaDados(StrStream.DataString,'cteRecepcaoEventoResult');
      //
      StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Recep��o Evento:' + sLineBreak  +
       '- Inativo ou Inoperante tente novamente.' + sLineBreak  + '- ' + E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmCTeGeraXML_0200a.WS_EventoCartaDeCorrecao(UFServico: String;
  Ambiente, CodigoUF: Byte; XML_Evento: String; TipoConsumo: TTipoConsumoWS_CTe;
  RETxtEnvio: TMemo; EdWS: TEdit): String;
var
  Texto : String;
  Acao: TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso, VersaoDados: String;
  //I: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_Evento;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, #13, '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, #10, '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  VersaoDados := Geral.FFT_Dot(DmCTe_0000.QropcoesCTeCTeVerLotEve.Value, 2, siNegativo);

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +    '<soap12:Header>';
  Texto := Texto +      '<cteCabecMsg xmlns="http://www.portalfiscal.inf.br/cte/wsdl/RecepcaoEvento">';
  Texto := Texto +        '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +        '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +      '</cteCabecMsg>';
  Texto := Texto +    '</soap12:Header>';
  Texto := Texto +    '<soap12:Body>';
  Texto := Texto +      '<cteDadosMsg xmlns="http://www.portalfiscal.inf.br/cte/wsdl/RecepcaoEvento">' + FDadosTxt + '</cteDadosMsg>';
  Texto := Texto +    '</soap12:Body>';
  Texto := Texto +  '</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;

  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/cte/wsdl/cteRecepcaoEvento';
  try
    try
      RETxtEnvio.Text :=  Acao.Text;
      EdWS.Text := FURL;
      ReqResp.Execute(Acao.Text, Stream);
      StrStream := TStringStream.Create('');
      StrStream.CopyFrom(Stream, 0);
      Result := SeparaDados(StrStream.DataString,'cteRecepcaoEventoResult');
      //
      StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Recep��o Evento:' + sLineBreak  +
       '- Inativo ou Inoperante tente novamente.' + sLineBreak  + '- ' + E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmCTeGeraXML_0200a.WS_EventoEPEC(UFServico: String; Ambiente,
  CodigoUF: Byte; XML_Evento: String; TipoConsumo: TTipoConsumoWS_CTe;
  RETxtEnvio: TMemo; EdWS: TEdit): String;
var
  Texto : String;
  Acao: TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  //I: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_Evento;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, #13, '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, #10, '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  ProcedimentoEnv := 'CteRecepcaoEvento';
  ProcedimentoRet := 'cteRecepcaoEventoResult';

  VersaoDados := Geral.FFT_Dot(DmCTe_0000.QropcoesCTeCTeVerLotEve.Value, 2, siNegativo);

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +    '<soap12:Header>';
  Texto := Texto +      '<cteCabecMsg xmlns="http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +        '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +        '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +      '</cteCabecMsg>';
  Texto := Texto +    '</soap12:Header>';
  Texto := Texto +    '<soap12:Body>';
  Texto := Texto +      '<cteDadosMsg xmlns="http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv + '">' + FDadosTxt + '</cteDadosMsg>';
  Texto := Texto +    '</soap12:Body>';
  Texto := Texto +  '</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;

  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/cte/wsdl/' + ProcedimentoEnv;
  try
    try
      RETxtEnvio.Text :=  Acao.Text;
      EdWS.Text := FURL;
      ReqResp.Execute(Acao.Text, Stream);
      StrStream := TStringStream.Create('');
      StrStream.CopyFrom(Stream, 0);
      Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
      //
      StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Recep��o Evento:' + sLineBreak  +
       '- Inativo ou Inoperante tente novamente.' + sLineBreak  + '- ' + E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmCTeGeraXML_0200a.XML_ConsReciCTe(TpAmb: Integer;
  NRec: String): String;
var
  XML_ConsReciCTe_Str: IXMLTConsReciCTe;
  VersaoDados: String;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_ConsReciCTe_Str := GetConsReciCTe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;
    //
    VersaoDados := Geral.FFT_Dot(DmCTe_0000.QropcoesCTeCTeVerConLot.Value, 2, siNegativo);

    //XML_ConsReciCTe_Str.Versao      := verConsReciCTe_Versao;
    XML_ConsReciCTe_Str.Versao      := VersaoDados;
    //XML_ConsReciCTe_Str.Versao      := Versao;
    XML_ConsReciCTe_Str.TpAmb       := FormatFloat('0', TpAmb);
    XML_ConsReciCTe_Str.NRec        := NRec;
    //
    Result := TipoXML(False) + XML_ConsReciCTe_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmCTeGeraXML_0200a.XML_ConsSitCTe(TpAmb: Integer; ChCTe,
  VersaoAcao: String): String;
var
  XML_ConsSitCTe_Str: IXMLTConsSitCTe;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_ConsSitCTe_Str := GetConsSitCTe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;
    {
    FdocXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
    FdocXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
    FdocXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/cte/enviCTe_v1.12.xsd';
    }
    XML_ConsSitCTe_Str.Versao      := VersaoAcao;
    XML_ConsSitCTe_Str.TpAmb       := FormatFloat('0', TpAmb);
    XML_ConsSitCTe_Str.XServ       := 'CONSULTAR';
    XML_ConsSitCTe_Str.ChCTe       := ChCTe;
    //
    Result := TipoXML(False) + XML_ConsSitCTe_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmCTeGeraXML_0200a.XML_ConsStatServ(TpAmb, CUF: Integer): String;
var
  XML_ConsStatServ_Str: IXMLTConsStatServ;
  Versao: String;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active    := False;
    FdocXML.FileName  := '';
    XML_ConsStatServ_Str := GetConsStatServ(FdocXML);
    FdocXML.Version   := sXML_Version;
    FdocXML.Encoding  := sXML_Encoding;

    Versao := Geral.FFT_Dot(DmCTe_0000.QropcoesCTeCTeVerStaSer.Value, 2, siNegativo);

    //XML_ConsStatServ_Str.Versao      := verConsStatServ_Versao;
    XML_ConsStatServ_Str.Versao      := Versao;
    XML_ConsStatServ_Str.TpAmb       := FormatFloat('0', TpAmb);
    // Nao tem UF no CTe !!!
    //XML_ConsStatServ_Str.        := FormatFloat('00', CUF);
    XML_ConsStatServ_Str.XServ       := 'STATUS';
    //
    Result := TipoXML(False) + XML_ConsStatServ_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmCTeGeraXML_0200a.XML_CTeInutCTe(var Id: String; TpAmb, CUF: Byte;
  Ano: Integer; CNPJ, Mod_, Serie, NCTIni, NCTFin, XJust: String): String;
var
  XML_CTeInutCTe_Str: IXMLTInutCTe;
  xUF, xAno, Versao: String;
begin
  xUF  := FormatFloat('00', CUF);
  xAno := FormatFloat('00', Ano);
  MontaID_Inutilizacao(xUF, xAno, CNPJ, Mod_, Serie, nCTIni, nCTFin, Id);
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_CTeInutCTe_Str := GetInutCTe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;

    Versao := Geral.FFT_Dot(DmCTe_0000.QropcoesCTeCTeVerInuNum.Value, 2, siNegativo);

    //XML_CTeInutCTe_Str.Versao         := verCTeInutCTe_Versao;
    XML_CTeInutCTe_Str.Versao         := Versao;
    XML_CTeInutCTe_Str.InfInut.Id     := Id;
    XML_CTeInutCTe_Str.InfInut.TpAmb  := FormatFloat('0', TpAmb);
    XML_CTeInutCTe_Str.InfInut.XServ  := 'INUTILIZAR';
    XML_CTeInutCTe_Str.InfInut.CUF    := FormatFloat('00', CUF);
    XML_CTeInutCTe_Str.InfInut.Ano    := Ano;
    XML_CTeInutCTe_Str.InfInut.CNPJ   := CNPJ;
    XML_CTeInutCTe_Str.InfInut.Mod_   := Mod_;
    XML_CTeInutCTe_Str.InfInut.Serie  := Serie;
    XML_CTeInutCTe_Str.InfInut.NCTIni := NCTIni;
    XML_CTeInutCTe_Str.InfInut.NCTFin := NCTFin;
    XML_CTeInutCTe_Str.InfInut.XJust  := XJust;
    Result := TipoXML(False) + XML_CTeInutCTe_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

end.
