unit CTeSteps_0200a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, Vcl.OleCtrls,
  OmniXML, OmniXMLUtils, Variants,
  UnDmkProcFunc, UnCTe_PF,
  SHDocVw, dmkPageControl, dmkRadioGroup, mySQLDbTables, dmkDBLookupComboBox,
  dmkEditCB;

type
  TTipoTagXML = (
     ttx_Id                ,
     ttx_idLote            ,
     ttx_versao            ,
     ttx_tpAmb             ,
     ttx_verAplic          ,
     ttx_cOrgao            ,
     ttx_cStat             ,
     ttx_xMotivo           ,
     ttx_cUF               ,
     ttx_dhRecbto          ,
     ttx_chCTe             ,
     ttx_nProt             ,
     ttx_digVal            ,
     ttx_ano               ,
     ttx_CNPJ              ,
     ttx_mod               ,
     ttx_serie             ,
     ttx_nCTIni            ,
     ttx_nCTFin            ,
     ttx_nRec              ,
     ttx_tMed              ,
     ttx_tpEvento          ,
     ttx_xEvento           ,
     ttx_CNPJDest          ,
     ttx_CPFDest           ,
     ttx_emailDest         ,
     ttx_nSeqEvento        ,
     ttx_dhRegEvento       ,
     ttx_dhResp            ,
     ttx_indCont           ,
     ttx_ultNSU            ,
     ttx_maxNSU            ,
     ttx_NSU               ,
     ttx_CPF               ,
     ttx_xNome             ,
     ttx_IE                ,
     ttx_dEmi              ,
     ttx_tpCT              ,
     ttx_vCT               ,
     ttx_cSitCTe           ,
     ttx_cSitConf          ,
     ttx_dhEvento          ,
     ttx_descEvento        ,
     //ttx_x Correcao         ,
     ttx_cJust             ,
     ttx_xJust             ,
     ttx_verEvento         ,
     ttx_schema            ,
     ttx_docZip            ,
     ttx_dhEmi             ,
     //
     ttx_grupoAlterado     ,
     ttx_campoAlterado     ,
     ttx_valorAlterado     ,
     ttx_nroItemAlterado   ,
     ttx_versaoEvento      ,
     //
     ttx_
     );

  TFmCTeSteps_0200a = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    PnConfirma: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PnConfig1: TPanel;
    Panel6: TPanel;
    Label5: TLabel;
    Label2: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    EdSerialNumber: TdmkEdit;
    EdUF_Servico: TdmkEdit;
    CBUF: TComboBox;
    EdEmpresa: TdmkEdit;
    Panel7: TPanel;
    RGAmbiente: TRadioGroup;
    Panel11: TPanel;
    Label19: TLabel;
    EdWebService: TEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    RETxtEnvio: TMemo;
    TabSheet5: TTabSheet;
    WBEnvio: TWebBrowser;
    TabSheet2: TTabSheet;
    RETxtRetorno: TMemo;
    TabSheet3: TTabSheet;
    WBResposta: TWebBrowser;
    TabSheet4: TTabSheet;
    MeInfo: TMemo;
    TabSheet6: TTabSheet;
    MeChaves: TMemo;
    Panel1: TPanel;
    EdEmitCNPJ: TdmkEdit;
    Label14: TLabel;
    Label20: TLabel;
    EdVersaoAcao: TdmkEdit;
    RGAcao: TRadioGroup;
    LaExpiraCertDigital: TLabel;
    CkSoLer: TCheckBox;
    RGIndSinc: TdmkRadioGroup;
    REWarning: TRichEdit;
    Timer1: TTimer;
    QrCTeCabA1: TmySQLQuery;
    QrCTeCabA1FatID: TIntegerField;
    QrCTeCabA1FatNum: TIntegerField;
    QrCTeCabA1Empresa: TIntegerField;
    QrCTeCabA1IDCtrl: TIntegerField;
    QrCTeJustInu: TmySQLQuery;
    QrCTeJustInuCodigo: TIntegerField;
    QrCTeJustInuNome: TWideStringField;
    QrCTeJustInuCodUsu: TIntegerField;
    QrCTeJustInuAplicacao: TIntegerField;
    DsCTeJustInu: TDataSource;
    QrCTeJustCan: TmySQLQuery;
    QrCTeJustCanCodigo: TIntegerField;
    QrCTeJustCanNome: TWideStringField;
    QrCTeJustCanCodUsu: TIntegerField;
    QrCTeJustCanAplicacao: TIntegerField;
    DsCTeJustCan: TDataSource;
    QrCabA: TmySQLQuery;
    QrCabAIDCtrl: TIntegerField;
    QrCabAinfProt_ID: TWideStringField;
    QrCabAinfProt_nProt: TWideStringField;
    QrCTeCabA2: TmySQLQuery;
    QrCTeCabA2FatID: TIntegerField;
    QrCTeCabA2FatNum: TIntegerField;
    QrCTeCabA2Empresa: TIntegerField;
    QrCTeCabA2Id: TWideStringField;
    Panel5: TPanel;
    PnAbrirXML: TPanel;
    BtAbrir: TButton;
    Button1: TButton;
    PCAcao: TdmkPageControl;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    Panel8: TPanel;
    Label4: TLabel;
    Label6: TLabel;
    EdLote: TdmkEdit;
    EdRecibo: TdmkEdit;
    TabSheet11: TTabSheet;
    TabSheet9: TTabSheet;
    Panel9: TPanel;
    Label3: TLabel;
    SpeedButton1: TSpeedButton;
    EdCTeJustCan: TdmkEditCB;
    CBCTeJustCan: TdmkDBLookupComboBox;
    Panel10: TPanel;
    Panel12: TPanel;
    Label22: TLabel;
    Label23: TLabel;
    Label8: TLabel;
    Label1: TLabel;
    Label9: TLabel;
    Label16: TLabel;
    EdModeloCan: TdmkEdit;
    EdSerieCan: TdmkEdit;
    EdnCTCan: TdmkEdit;
    EdnProtCan: TdmkEdit;
    EdchCTeCan: TdmkEdit;
    EdnSeqEventoCan: TdmkEdit;
    TabSheet10: TTabSheet;
    PnInutiliza: TPanel;
    Label15: TLabel;
    GroupBox2: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    EdnCTIni: TdmkEdit;
    EdnCTFim: TdmkEdit;
    Panel13: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    EdModeloInu: TdmkEdit;
    EdSerieInu: TdmkEdit;
    EdAno: TdmkEdit;
    PnJustificativa: TPanel;
    Label7: TLabel;
    SpeedButton2: TSpeedButton;
    EdCTeJustInu: TdmkEditCB;
    CBCTeJustInu: TdmkDBLookupComboBox;
    TabSheet12: TTabSheet;
    Panel14: TPanel;
    Label33: TLabel;
    Label29: TLabel;
    EdChaveCTeCTe: TdmkEdit;
    EdIDCtrlCTe: TdmkEdit;
    TabSheet13: TTabSheet;
    Panel15: TPanel;
    Label21: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    EdModeloCCe: TdmkEdit;
    EdSerieCCe: TdmkEdit;
    EdnCTCCe: TdmkEdit;
    EdnProtCCe: TdmkEdit;
    EdchCTeCCe: TdmkEdit;
    EdnSeqEventoCCe: TdmkEdit;
    QrCTeEveRCab: TmySQLQuery;
    QrCTeEveRCabFatID: TIntegerField;
    QrCTeEveRCabFatNum: TIntegerField;
    QrCTeEveRCabEmpresa: TIntegerField;
    QrCTeEveRCabControle: TIntegerField;
    QrCTeEveRCCeIts: TmySQLQuery;
    QrCTeEveRCCeItsFatID: TIntegerField;
    QrCTeEveRCCeItsFatNum: TIntegerField;
    QrCTeEveRCCeItsEmpresa: TIntegerField;
    QrCTeEveRCCeItsControle: TIntegerField;
    QrCTeEveRCCeItsConta: TIntegerField;
    QrCTeEveRCCeItsIDItem: TIntegerField;
    TabSheet14: TTabSheet;
    TabSheet15: TTabSheet;
    Panel16: TPanel;
    Panel17: TPanel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    dmkEdit1: TdmkEdit;
    dmkEdit2: TdmkEdit;
    dmkEdit3: TdmkEdit;
    dmkEdit4: TdmkEdit;
    EdchCTeEPEC: TdmkEdit;
    dmkEdit6: TdmkEdit;
    Panel18: TPanel;
    Label37: TLabel;
    SpeedButton3: TSpeedButton;
    dmkEditCB1: TdmkEditCB;
    dmkDBLookupComboBox1: TdmkDBLookupComboBox;
    QrCTeJustEPEC: TmySQLQuery;
    DsCTeJustEPEC: TDataSource;
    QrCTeJustEPECCodigo: TIntegerField;
    QrCTeJustEPECNome: TWideStringField;
    QrCTeJustEPECCodUsu: TIntegerField;
    QrCTeJustEPECAplicacao: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure RETxtEnvioChange(Sender: TObject);
    procedure RETxtRetornoChange(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BtAbrirClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure RGAcaoClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoUF_Int, FAmbiente_Int: Integer;
    FAmbiente_Txt, FCodigoUF_Txt, FWSDL, FURL: String;
    xmlDoc: IXMLDocument;
    xmlNode, xmlSub, xmlChild1, xmlChild2, xmlChild3: IXMLNode;
    FPathLoteCTe: String;
    FSegundos, FSecWait: Integer;
    FNaoExecutaLeitura: Boolean;
    FSiglas_WS: MyArrayLista;
    xmlList(*, xmlSubs*): IXMLNodeList;
    //
    function  AbreArquivoXML(Arq, Ext: String; Assinado: Boolean): Boolean;
    function  DefinechCTe(const EdChaveCte: TdmkEdit; var chCTe: String): Boolean;
    function  DefinenCTIni(var nCTIni: String): Boolean;
    function  DefinenCTFin(var nCTFim: String): Boolean;
    function  DefineIDCtrl(const EdIDCtrl: TdmkEdit; var IDCtrl: Integer): Boolean;
    function  DefineEmitCNPJ(var EmitCNPJ: String): Boolean;
    function  DefineEmpresa(var Empresa: Integer): Boolean;
    function  DefineLote(var Lote: Integer): Boolean;
    function  DefineModelo(const ModInt: Integer; var ModTxt: String): Boolean;
    function  DefineSerie(const SerVar: Variant; var SerTxt: String): Boolean;
    function  DefineXMLDoc(): Boolean;
    //
    procedure ExecutaConsultaCTe();
    procedure ExecutaConsultaLoteCTe();
    procedure ExecutaEvento_Cancelamanto();
    procedure ExecutaEvento_CartaDeCorrecao();
    procedure ExecutaEvento_EPEC();
    procedure ExecutaEnvioDeLoteCTe(Sincronia: TXXeIndSinc);
    procedure ExecutaInutilizaNumerosCTe();
    //
    procedure HabilitaBotoes(Visivel: Boolean = True);
    function  LeNoXML(No: IXMLNode; Tipo: TTipoNoXML; Tag: TTipoTagXML; AvisaVersao: Boolean = True): String;
    //
    procedure LerTextoConsultaCTe();
    procedure LerTextoConsultaLoteCTe();
    function  LerTextoEnvioLoteCTe(): Boolean;
    procedure LerTextoInutilizaNumerosCTe();
    procedure LerTextoStatusServico();
    function  LerTextoEvento(): Boolean;
    procedure LerTextoEvento_Cancelamento();
    procedure LerTextoEvento_CartaDeCorrecao();
    procedure LerTextoEvento_EPEC();
    procedure LerXML_procEventoCTe(Lista: IXMLNodeList; digVal, cStat, dhRecbto:
              String);
    //
    procedure MostraTextoRetorno(Texto: String);
    function  ObtemNomeDaTag(Tag: TTipoTagXML): String;
    function  ObtemDescricaoDaTag(Tag: TTipoTagXML): String;
    procedure ReopenCTeJust(QrCTeJust: TmySQLQuery; Aplicacao: Byte);
    function  TextoArqDefinido(Texto: String): Boolean;
    procedure VerificaCertificadoDigital(Empresa: Integer);
    procedure VerificaStatusServico();

  public
    { Public declarations }
    FXML_Evento, FTextoArq: String;
    FXML_LoteCTe, FXML_LoteEvento: String;
    FQry: TmySQLQuery;
    //
    procedure PreparaConsultaCTe(Empresa, IDCtrl: Integer; ChaveCTe: String);
    procedure PreparaConsultaLote(Lote, Empresa: Integer; Recibo: String);
    function  PreparaEnvioDeLoteCTe(Lote, Empresa: Integer; Sincronia: TXXeIndSinc): Boolean;
    procedure PreparaInutilizaNumerosCTe(Empresa, Lote, Ano, Modelo, Serie,
              nCTIni, nCTFim, Justif: Integer);
    procedure PreparaCancelamentoDeCTe(Empresa, Serie, nCT, nSeqEvento,
              Justif: Integer; Modelo, nProtCan, chCTe, XML_Evento: String);
    procedure PreparaCartaDeCorrecao(Empresa, Serie, nCT, nSeqEvento: Integer;
              Modelo, nProt, ChCte, XML_Evento: String);
    procedure PreparaEPEC(Empresa, Serie, nCT, nSeqEvento,
              Justif: Integer; Modelo, chCTe, XML_Evento: String);
    procedure PreparaStepGenerico(Empresa: Integer);
    procedure PreparaVerificacaoStatusServico(Empresa: Integer);
  end;

  var
  FmCTeSteps_0200a: TFmCTeSteps_0200a;

var
  FverXML_versao: String;

implementation

uses UnMyObjects, Module, CTeGeraXML_0200a, UnXXe_PF, ModuleCTe_0000,
  ModuleGeral, CTeLEnC_0200a, CTeLEnU_0200a, UMySQLModule, CTEInut_0000,
  CTeEveRCab, dmkDAC_PF;

{$R *.DFM}

const
  FXML_Load_Failure = 'Falha ao carregar o XML!';
  CO_Texto_Opt_Sel = 'Clique em "OK" para ler arquivo selecionado, ou em "Abrir" para selecionar um arquivo!';
  CO_Texto_Clk_Sel = 'Configure a forma de consulta e clique em "OK"!';
  CO_Texto_Env_Sel = 'Configure a forma de envio e clique em "OK" para enviar o lote ao fisco!';

function TFmCTeSteps_0200a.AbreArquivoXML(Arq, Ext: String;
  Assinado: Boolean): Boolean;
var
  Dir, Arquivo: String;
begin
  Result := False;
  FTextoArq := '';
  if not DmCTe_0000.ObtemDirXML(Ext, Dir, Assinado) then
    Exit;
  Arquivo := Dir + Arq + Ext;
  if FileExists(Arquivo) then
  begin
    if dmkPF.CarregaArquivo(Arquivo, FTextoArq) then
    begin
      MostraTextoRetorno(FTextoArq);
      Result := FTextoArq <> '';
      if Result then HabilitaBotoes();
    end;
  end else Geral.MB_Erro('Arquivo n�o localizado "' + Arquivo + '"!');
end;

procedure TFmCTeSteps_0200a.BtAbrirClick(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir('C:\Dermatek\CTe\');
  Arquivo := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo XML', '', [], Arquivo) then
  begin
    //AbreArquivoSelecionado(Arquivo);
    //Result := False;
    if dmkPF.CarregaArquivo(Arquivo, FTextoArq) then
    begin
      MostraTextoRetorno(FTextoArq);
      if FTextoArq <> '' then
        HabilitaBotoes();
      //Result := true;
    end;
  end;
end;

procedure TFmCTeSteps_0200a.BtOKClick(Sender: TObject);
begin
  CO_CTE_CERTIFICADO_DIGITAL_SERIAL_NUMBER := EdSerialNumber.Text;
  REWarning.Text     := '';
  RETxtEnvio.Text    := '';
  MeInfo.Text        := '';
  MostraTextoRetorno('');
  PageControl1.ActivePageIndex := 0;
  Update;
  Application.ProcessMessages;
  //
  FCodigoUF_Int  := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(CBUF.Text);
  FAmbiente_Int  := RGAmbiente.ItemIndex;
  if FAmbiente_Int = 0 then
  begin
    Geral.MB_Aviso('Defina o ambiente!');
    Exit;
  end;
  FAmbiente_Txt  := IntToStr(FAmbiente_Int);
  FCodigoUF_Txt  := IntToStr(FCodigoUF_Int);
  FWSDL          := '';
  FURL           := '';
  if FCodigoUF_Int = 0 then
  begin
    Geral.MB_Aviso('Defina a UF!');
    Exit;
  end;
  //
  BtOK.Enabled := False;
  case RGAcao.ItemIndex of
    0: VerificaStatusServico();
    1:
    begin
      if CkSoLer.Checked then
        LerTextoEnvioLoteCTe((*nisSincrono*))
      else
        ExecutaEnvioDeLoteCTe(TXXeIndSinc(RGIndSinc.ItemIndex));
    end;
    2:
    begin
      if CkSoLer.Checked then
        LerTextoConsultaLoteCTe()
      else
        ExecutaConsultaLoteCTe();
    end;
    3:
    begin
      if CkSoLer.Checked then
        LerTextoEvento_Cancelamento()
      else
        ExecutaEvento_Cancelamanto();
    end;
    4:
    begin
      if CkSoLer.Checked then
        LerTextoInutilizaNumerosCTe()
      else
        ExecutaInutilizaNumerosCTe();
    end;
    5:
    begin
      if CkSoLer.Checked then
        LerTextoConsultaCTe()
      else
        ExecutaConsultaCTe();
    end;
    6:
    begin
      if CkSoLer.Checked then
        LerTextoEvento_CartaDeCorrecao()
      else
        ExecutaEvento_CartaDeCorrecao()
    end;
(*
    7: ConsultaCadastroContribuinte();
*)
    8:
    begin
      if CkSoLer.Checked then
        LerTextoEvento_EPEC()
      else
        ExecutaEvento_EPEC()
    end;
(*
    9:
    begin
      if CkSoLer.Checked then
        LerTextoConsultaCTeDest(EdultNSU.Text)
      else
        ExecutaConsultaCTDest();
    end;
    10:
    begin
      if CkSoLer.Checked then
        LerTextoDownloadCTeConfirmadas()
      else
        ExecutaDownloadCTeConfirmadas();
    end;
    11:
    begin
      if CkSoLer.Checked then
        LerTextoConsultaDistribuicaoDFeInteresse()
      else
        ExecutaConsultaDistribuicaoDFeInteresse();
    end;
*)
    else Geral.MB_Aviso('A a��o "' + RGAcao.Items[RGAcao.ItemIndex] +
    '" n�o est� implementada! AVISE A DERMATEK!');
  end;
  if Trim(REWarning.Text) <> '' then
    dmkPF.LeTexto_Permanente(REWarning.Text, 'Aviso do Web Service');
end;

procedure TFmCTeSteps_0200a.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCTeSteps_0200a.Button1Click(Sender: TObject);
begin
  dmkPF.LeTexto_Permanente(FTextoArq, FXML_Load_Failure);
end;

function TFmCTeSteps_0200a.DefinechCTe(const EdChaveCte: TdmkEdit; var chCTe:
  String): Boolean;
var
  K: Integer;
begin
  Result := False;
  chCTe  := EdChaveCTe.Text;
  K := Length(chCTe);
  if K <> 44 then
    Geral.MB_Erro('Tamanho da chave difere de 44: tamanho = ' + IntToStr(K))
  else if Geral.SoNumero1a9_TT(chCTe) = '' then
    Geral.MB_Erro('Chave n�o definida!')
  else
    Result := True;
end;

function TFmCTeSteps_0200a.DefineEmitCNPJ(var EmitCNPJ: String): Boolean;
var
  K: Integer;
begin
  EmitCNPJ := Geral.SoNumero_TT(EdEmitCNPJ.Text);
  k := Length(EmitCNPJ);
  if K = 14 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro('CNPJ da empresa emitente com tamanho incorreto!');
  end;
end;

function TFmCTeSteps_0200a.DefineEmpresa(var Empresa: Integer): Boolean;
begin
  Empresa := EdEmpresa.ValueVariant;
  if Empresa <> 0 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro('Empresa n�o definida!');
  end;
end;

function TFmCTeSteps_0200a.DefineIDCtrl(const EdIDCtrl: TdmkEdit; var IDCtrl:
  Integer): Boolean;
begin
  IDCtrl := EdIDCtrl.ValueVariant;
  if IDCtrl <> 0 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro(
    'IDCtrl n�o definido! A��o/consulta n�o ser� inclu�da no hist�rico da CTe!');
  end;
end;

function TFmCTeSteps_0200a.DefineLote(var Lote: Integer): Boolean;
begin
  Lote := EdLote.ValueVariant;
  if Lote <> 0 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro('Lote n�o definido!');
  end;
end;

function TFmCTeSteps_0200a.DefineModelo(const ModInt: Integer; var ModTxt: String): Boolean;
begin
  ModTxt := FormatFloat('00', ModInt);
  if ModInt <> 57 then
  begin
    Result := False;
    Geral.MB_Erro('Modelo de CT-e n�o implementado: ' + ModTxt);
  end else Result := True;
end;

function TFmCTeSteps_0200a.DefinenCTFin(var nCTFim: String): Boolean;
begin
  if (EdnCTFim.ValueVariant = null) or (EdnCTFim.ValueVariant < 0) then
  begin
    Result := False;
    Geral.MB_Erro('Numera��o final de CT-e inv�lida!');
  end else begin
    nCTFim := FormatFloat('0', EdnCTFim.ValueVariant);
    Result := True;
  end;
end;

function TFmCTeSteps_0200a.DefinenCTIni(var nCTIni: String): Boolean;
begin
  if (EdnCTIni.ValueVariant = null) or (EdnCTIni.ValueVariant < 0) then
  begin
    Result := False;
    Geral.MB_Erro('Numera��o inicial de CT-e inv�lida!');
  end else begin
    nCTIni := FormatFloat('0', EdnCTIni.ValueVariant);
    Result := True;
  end;
end;

function TFmCTeSteps_0200a.DefineSerie(const SerVar: Variant; var SerTxt: String): Boolean;
begin
  if (SerVar = null) or
  (SerVar < 0) or
  (SerVar > 899) then
  begin
    Result := False;
    Geral.MB_Aviso('N�mero de s�rie inv�lido!');
  end else
  begin
    SerTxt  := FormatFloat('0', SerVar);
    Result := True;
  end;
end;

function TFmCTeSteps_0200a.DefineXMLDoc(): Boolean;
begin
  xmlDoc := TXMLDocument.Create;
  if not XMLLoadFromAnsiString(xmlDoc, FTextoArq) then
  begin
    Result := False;
    dmkPF.LeTexto_Permanente(FTextoArq, FXML_Load_Failure);
  end else Result := True;
end;

procedure TFmCTeSteps_0200a.EdEmpresaChange(Sender: TObject);
begin
  DmCTe_0000.ReopenOpcoesCTe(DmCTe_0000.QrOpcoesCTe, EdEmpresa.ValueVariant, True);
  RGAmbiente.ItemIndex := DmCTe_0000.QrOpcoesCTeCTeide_tpAmb.Value;
end;

procedure TFmCTeSteps_0200a.ExecutaConsultaCTe();
var
  Empresa, IDCtrl: Integer;
  chCTe: String;
  Id, Dir, Aviso: String;
begin
  if not DefineEmpresa(Empresa) then Exit;
  if not DefinechCTe(EdChaveCTeCte, chCTe) then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    FTextoArq := FmCTeGeraXML_0200a.WS_CTeConsultaCT(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, chCTe, LaAviso1, LaAviso2,
      RETxtEnvio, EdWebService);
    if FTextoArq = '' then
      Exit;
    MostraTextoRetorno(FTextoArq);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Resposta recebida com Sucesso!');
    //
    DmCTe_0000.SalvaXML(CTE_EXT_SIT_XML, chCTe, FTextoArq, RETxtRetorno, False);
    //
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros!');
    end;
    if not FNaoExecutaLeitura then
    begin
      LerTextoConsultaCTe();
      //
      QrCabA.Close;
      QrCabA.Params[00].AsInteger := EdEmpresa.ValueVariant;
      QrCabA.Params[01].AsString  := chCTe;
      QrCabA.Open;
      IDCtrl := QrCabAIDCtrl.Value;
      if IDCtrl > 0 then
      begin
        Id    := QrCabAinfProt_ID.Value;
        if Trim(Id) = '' then
          Id := QrCabAinfProt_nProt.Value;
        Dir   := DmCTe_0000.QrOpcoesCTeDirCTeSit.Value;
        Aviso := '';
        DmCTe_0000.AtualizaXML_No_BD_ConsultaCTe(chCTe, Id, IDCtrl, Dir, Aviso);
        if Aviso <> '' then Geral.MB_Aviso(
        'Os arquivos abaixo n�o foram localizados:' + sLineBreak + Aviso);
      end;
    end else
    begin
      LerTextoConsultaCTe();
      Close;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCTeSteps_0200a.ExecutaConsultaLoteCTe();
var
  Recibo, LoteStr: String;
  Empresa, Lote: Integer;
begin
  Recibo := EdRecibo.Text;
  if (Recibo <> '')  then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando consulta');
    if not DefineEmpresa(Empresa) then Exit;
    if not DefineLote(Lote) then Exit;
    DmCTe_0000.ReopenEmpresa(Empresa);
    FTextoArq :='';
    Screen.Cursor := CrHourGlass;
    try
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Consultando o servidor do fisco');
      FTextoArq := FmCTeGeraXML_0200a.WS_CTeRetRecepcao(EdUF_Servico.Text, FAmbiente_Int,
        FCodigoUF_Int, EdRecibo.Text, LaAviso1, LaAviso2, RETxtEnvio, EdWebService);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Sucesso!');
      MostraTextoRetorno(FTextoArq);
      if Pos('Erros:', FTextoArq) > 0 then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2,False, 'O fisco acusou erros na resposta!');
        Geral.MB_Erro('Resposta recebida com Erros!');
      end else
      begin
        DmCTe_0000.ReopenEmpresa(Empresa);
        //
        LoteStr := DmCTe_0000.FormataLoteCTe(Lote);
        DmCTe_0000.SalvaXML(CTE_EXT_PRO_REC_XML, LoteStr, FTextoArq, RETxtRetorno, False);
        //
        LerTextoConsultaLoteCTe();
        try
          if FQry <> nil then
            UnDmkDAC_PF.AbreQuery(FQry, FQry.Database)
          else
            Geral.MB_Aviso(
            'Tabela n�o definida para localizar o lote de CT-e(s) n�mero ' +
            EdLote.Text + '!');
        except
          Geral.MB_Aviso('N�o foi poss�vel localizar o lote de CT-e(s) n�mero ' +
          EdLote.Text + '!');
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end
  else
    Geral.MB_Aviso('Recibo n�o informado para consulta...');
end;

procedure TFmCTeSteps_0200a.ExecutaEnvioDeLoteCTe(Sincronia: TXXeIndSinc);
var
  retWS, rtfDadosMsg, LoteStr: String;
  Lote, Empresa: Integer;
begin
  FTextoArq := '';
  if not DefineLote(Lote) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  //
  if not CkSoLer.Checked then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando lote ao fisco');
    if not FileExists(FPathLoteCTe) then
    begin
      //Continua := False;
      Geral.MB_Erro('O lote "' + FPathLoteCTe + '" n�o foi localizado!');
      Exit;
    end;
    if dmkPF.CarregaArquivo(FPathLoteCTe, rtfDadosMsg) then
    begin
      if rtfDadosMsg = '' then
      begin
        Geral.MB_Erro('O lote de CTe "' + FPathLoteCTe +
        '" foi carregado mas est� vazio!');
        Exit;
      end;
      retWS :='';
      FTextoArq :='';
      Screen.Cursor := crHourGlass;
      try
        FTextoArq := FmCTeGeraXML_0200a.WS_CTeRecepcaoLote(EdUF_Servico.Text,
        FAmbiente_Int, FCodigoUF_Int, EdSerialNumber.Text, Lote, LaAviso1, LaAviso2, RETxtEnvio,
        EdWebService, Sincronia);
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Sucesso!');
        MostraTextoRetorno(FTextoArq);
        //
        DmCTe_0000.ReopenEmpresa(Empresa);
        LoteStr := FormatFloat('000000000', Lote);
        DmCTe_0000.SalvaXML(CTE_EXT_REC_XML, LoteStr, FTextoArq, RETxtRetorno, False);
        //
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
  if FTextoArq <> '' then
    if LerTextoEnvioLoteCTe((*Sincronia*)) then
      Timer1.Enabled := Sincronia = TXXeIndSinc.nisAssincrono;
end;

procedure TFmCTeSteps_0200a.ExecutaEvento_Cancelamanto();
const
  TipoConsumo = tcwscteEveCancelamento;
var
  ID_Res, Id, xJust, Modelo, Serie, nCTIni, nCTFin, EmitCNPJ, LoteStr: String;
  Empresa, Lote, K: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando dados a serem enviados ao servidor do fisco');
  Screen.Cursor := CrHourGlass;
  try
    FTextoArq := FmCTeGeraXML_0200a.WS_EventoCancelamentoCTe(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int,  (*NumeroSerial: String; Lote: Integer; LaAviso1,
      LaAviso2: TLabel; RETxtEnvio: TMemo; EdWS: TEdit*)FXML_Evento, TipoConsumo,
      RETxtEnvio, EdWebService);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebido com Sucesso!');
    //
    // Salva arquivo
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Salvando resposta');
    //
    ID := EdchCTeCan.Text;
    //
    DmCTe_0000.SalvaXML(CTE_EXT_EVE_RET_CAN_XML, EdchCTeCan.Text, FTextoArq, RETxtRetorno, False);
    //
    LerTextoEvento_Cancelamento();
    //
    if FQry <> nil then
      UnDmkDAC_PF.AbreQuery(FQry, FQry.Database);
    //
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCTeSteps_0200a.ExecutaEvento_CartaDeCorrecao();
const
  TipoConsumo = tcwscteEveCartaDeCorrecao;
var
  ID_Res, Id, xJust, Modelo, Serie, nCTIni, nCTFin, EmitCNPJ, LoteStr: String;
  Empresa, Lote, K: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando dados a serem enviados ao servidor do fisco');
  Screen.Cursor := CrHourGlass;
  try
    FTextoArq := FmCTeGeraXML_0200a.WS_EventoCartaDeCorrecao(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int,  (*NumeroSerial: String; Lote: Integer; LaAviso1,
      LaAviso2: TLabel; RETxtEnvio: TMemo; EdWS: TEdit*)FXML_Evento, TipoConsumo,
      RETxtEnvio, EdWebService);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebido com Sucesso!');
    //
    // Salva arquivo
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Salvando resposta');
    //
    ID := EdchCTeCCe.Text;
    //
    DmCTe_0000.SalvaXML(CTE_EXT_EVE_RET_CCE_XML, EdchCTeCCe.Text, FTextoArq, RETxtRetorno, False);
    //
    LerTextoEvento_CartaDeCorrecao();
    //
    if FQry <> nil then
      UnDmkDAC_PF.AbreQuery(FQry, FQry.Database);
    //
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCTeSteps_0200a.ExecutaEvento_EPEC();
const
  TipoConsumo = tcwscteEveEPEC;
var
  ID_Res, Id, xJust, Modelo, Serie, nCTIni, nCTFin, EmitCNPJ, LoteStr: String;
  Empresa, Lote, K: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando dados a serem enviados ao servidor do fisco');
  Screen.Cursor := CrHourGlass;
  try
    FTextoArq := FmCTeGeraXML_0200a.WS_EventoEPEC(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int,  (*NumeroSerial: String; Lote: Integer; LaAviso1,
      LaAviso2: TLabel; RETxtEnvio: TMemo; EdWS: TEdit*)FXML_Evento, TipoConsumo,
      RETxtEnvio, EdWebService);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebido com Sucesso!');
    //
    // Salva arquivo
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Salvando resposta');
    //
    ID := EdchCTeEPEC.Text;
    //
    DmCTe_0000.SalvaXML(CTE_EXT_EVE_RET_EPEC_XML, EdchCTeEPEC.Text, FTextoArq, RETxtRetorno, False);
    //
    LerTextoEvento_EPEC();
    //
    if FQry <> nil then
      UnDmkDAC_PF.AbreQuery(FQry, FQry.Database);
    //
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCTeSteps_0200a.ExecutaInutilizaNumerosCTe();
var
  ID_Res, Id, xJust, Modelo, Serie, nCTIni, nCTFin, EmitCNPJ, LoteStr: String;
  Empresa, Lote, K: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando dados a serem enviados ao servidor do fisco');
  if not DefineEmpresa(Empresa) then Exit;
  if not DefineLote(Lote) then Exit;
  if not DefineModelo(EdModeloInu.ValueVariant, Modelo) then Exit;
  if not DefineSerie(EdSerieInu.ValueVariant, Serie) then Exit;
  if not DefinenCTIni(nCTIni) then Exit;
  if not DefinenCTFin(nCTFin) then Exit;
  if not DefineEmitCNPJ(EmitCNPJ) then Exit;
  //
  xJust := Trim(XXe_PF.ValidaTexto_XML(CBCTeJustInu.Text, 'xJust', 'xJust'));
  K := Length(xJust);
  if K < 15 then
  begin
    Geral.MB_Aviso('A justificativa deve ter pelo menos 15 caracteres!' +
    sLineBreak + 'O texto "' + xJust + '" tem apenas ' + IntToStr(K)+'.');
    Exit;
  end;
  xJust := Geral.TFD(FloatToStr(QrCTeJustInuCodigo.Value), 10, siPositivo) + ' - ' + xJust;
  Screen.Cursor := CrHourGlass;
  try
    FTextoArq := FmCTeGeraXML_0200a.WS_CTeInutilizacaoCTe(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, EdAno.ValueVariant, Id, emitCNPJ, Modelo,
      Serie, nCTIni, nCTFin, XJust, EdSerialNumber.Text, LaAviso1, LaAviso2, RETxtEnvio,
      EdWebService, ID_Res);
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebido com Sucesso!');
    //
    // Salva arquivo
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Salvando resposta');
    //
    //cUF := IntToStr(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(FSiglaUF));
    DmCTe_0000.MontaID_Inutilizacao(FCodigoUF_Txt, EdAno.Text, emitCNPJ, Modelo, Serie,
      nCTIni, nCTFin, Id);
    //
    LoteStr := Id;// + '_' + FormatFloat('000000000', Lote);
    DmCTe_0000.SalvaXML(CTE_EXT_INU_XML, LoteStr, FTextoArq, RETxtRetorno, False);
    //
    LerTextoInutilizaNumerosCTe();
    //
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCTeSteps_0200a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCTeSteps_0200a.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
(*
  FultNSU  := 0;
  FNSU     := 0;
  //
  //Self.Height := 730;
*)
  //
  FTextoArq :='';
  FNaoExecutaLeitura := False;
  FSecWait := 15;
  Timer1.Enabled := False;
  FSegundos := 0;
  //
  PageControl1.ActivePageIndex := 0;
  FSiglas_WS  := Geral.SiglasWebService();
  //FFormChamou := '';
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Configurando conforme solicita��o');
  if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER)) then
  begin
    EdLote.Enabled     := True;
    EdEmpresa.Enabled  := True;
    EdRecibo.Enabled   := True;
    //
    BtAbrir.Enabled    := True;
  end;
  //
end;

procedure TFmCTeSteps_0200a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCTeSteps_0200a.HabilitaBotoes(Visivel: Boolean);
begin
  PnConfirma.Visible := Visivel;
end;

function TFmCTeSteps_0200a.LeNoXML(No: IXMLNode; Tipo: TTipoNoXML;
  Tag: TTipoTagXML; AvisaVersao: Boolean): String;
var
  Texto: String;
begin
  Result := '';
  case Tipo of
    tnxTextStr: Result := GetNodeTextStr(No, ObtemNomeDaTag(Tag), '');
    tnxAttrStr: Result := GetNodeAttrStr(No, ObtemNomeDaTag(Tag), '');
    else Result := '???' + ObtemNomeDaTag(Tag) + '???';
  end;
  //
  if (Tag = ttx_Versao) and (Result <> FverXML_versao) then
    if AvisaVersao then
      Geral.MB_Aviso('Vers�o do XML difere do esperado!' +
      sLineBreak + 'Vers�o informada: ' + Result + sLineBreak +
      'Verifique a vers�o do servi�o no cadastro das op��es da filial para a vers�o: '
      + Result);
  if (Tag = ttx_dhRecbto) then XXe_PF.Ajusta_dh_XXe(Result);
  if Result <> '' then
  begin
    case Tag of
      ttx_tpAmb: Texto := Result + ' - ' + XXe_PF.ObtemNomeAmbiente(Result);
      ttx_tMed : Texto := Result + ' segundos';
      ttx_cUF  : Texto := Result + ' - ' +
                 Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(StrToInt(Result));
      else Texto := Result;
    end;
  end;
  //
  MeInfo.Lines.Add(ObtemDescricaoDaTag(Tag) + ' = ' + Texto);
end;

procedure TFmCTeSteps_0200a.LerTextoConsultaCTe();
var
  IDCtrl, Controle, FatID, FatNum, Empresa: Integer;
  tpAmb, verAplic, cStat, xMotivo, cUF, dhRecbto, Id, nProt,
  chCTe, digVal, cJust, xJust, _Stat, _Motivo, tpEvento, dhEvento: String;
  //
  Status, Evento, nCondUso, infCCe_CCeConta: Integer;
  infProt_dhRecbtoTZD, dhRecbtoTZD, infCanc_dhRecbtoTZD, dhEventoTZD: Double;
  //
  cOrgao, dhRegEvento, xEvento, nSeqEvento, verEvento, CNPJ, CPF: String;
  //
  infCCe_verAplic, infCCe_dhRegEvento, infCCe_nProt,
  infCCe_CNPJ, infCCe_CPF, infCCe_chCTe, infCCe_dhEvento: String;
  infCCe_cOrgao, infCCe_tpAmb, infCCe_tpEvento, infCCe_nSeqEvento, infCCe_cStat,
  infCCe_nCondUso: Integer;
  infCCe_verEvento, infCCe_dhRegEventoTZD, infCCe_dhEventoTZD: Double;
  //
  _CNPJ, _CPF: String;
  _dhEvento: TDateTime;
  _dhEventoTZD, _verEvento: Double;
  CTeEveRCCeConta: Integer;
  //
  versao: String;
begin
  FverXML_versao := Geral.FFT_Dot(DmCTe_0000.QrOpcoesCTeCTeVerConCTe.Value, 2, siNegativo);
  if not DefineEmpresa(Empresa) then Exit;
  if not DefinechCTe(EdChaveCteCte, chCTe) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    if DefineIDCtrl(EdIDCtrlCTe, IDCtrl) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrCTeCabA2, Dmod.MyDB, [
      'SELECT * ',
      'FROM ctecaba ',
      'WHERE IDCtrl=' + Geral.FF0(IDCtrl),
      '']);
      if QrCTeCabA2.RecordCount > 0 then
      begin
        // Verifica se � recibo de consulta de CTe
        xmlNode := xmlDoc.SelectSingleNode('/retConsSitCTe');
        if not assigned(xmlNode) then
          Geral.MB_Aviso(
          'Arquivo XML n�o possui informa��es de Autoriza��o ou Cancelamento de CT-e!')
        else
        begin
          tpAmb    := '';
          verAplic := '';
          cStat    := '';
          xMotivo  := '';
          cUF      := '';
          chCTe    := '';
          dhRecbto := '';
          nProt    := '';
          digVal   := '';
          cJust    := '';
          xJust    := '';
          //
          PageControl1.ActivePageIndex := 4;
          //Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
          tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
          verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
          cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
          xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
          cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
          if (Geral.IMV(cStat) in ([100, 101, 110])) then
          begin
            xmlNode := xmlDoc.SelectSingleNode('/retConsSitCTe/protCTe');
            if assigned(xmlNode) then
            begin
              versao := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
              xmlNode := xmlDoc.SelectSingleNode('/retConsSitCTe/protCTe/infProt');
              if assigned(xmlNode) then
              begin
                //colocar aqui info de 100
                Pagecontrol1.ActivePageIndex := 4;
                Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
                tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
                verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
                chCTe    := LeNoXML(xmlNode, tnxTextStr, ttx_chCTe);
                dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
                nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
                digVal   := LeNoXML(xmlNode, tnxTextStr, ttx_digVal);
                _Stat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
                _Motivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
                //
                if (Geral.IMV(cStat) in ([100, 101, 110])) then
                begin
                  if (chCTe <> QrCTeCabA2Id.Value) then
                  begin
                    Geral.MB_Erro('Chave da CT-e n�o coctere: ' + sLineBreak +
                    'No XML: ' + chCTe + sLineBreak +
                    'No BD: ' + QrCTeCabA2Id.Value);
                    //
                    Exit;
                  end else
                  begin
                    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctecaba', False, [
                      'Status'], ['ID'], [cStat], [chCTe], True);
                  end;
                  if Geral.IMV(_Stat) = 100 then
                  begin
                    //
                    XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, infProt_dhRecbtoTZD);
                    //
                    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctecaba', False, [
                      'infProt_Id', 'infProt_tpAmb',
                      'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
                      'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
                      'infProt_dhRecbtoTZD'
                    ], ['ID'], [
                      Id, tpAmb,
                      verAplic, dhRecbto, nProt,
                      digVal, _Stat, _Motivo,
                      infProt_dhRecbtoTZD
                    ], [chCTe], True);
                  end;
                  //
                end;
              end;
            end;
{
            // Cancelamento Nao Tem!!!
            xmlNode := xmlDoc.SelectSingleNode('/retConsSitCTe/retCancCTe/infCanc');
            if assigned(xmlNode) then
            begin
              //colocar aqui info de 101
              Pagecontrol1.ActivePageIndex := 4;
              Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
              tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
              verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
              _Stat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
              _Motivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
              cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
              //
              if Geral.IMV(_Stat) = 101 then
              begin
                //tMed     := LeNoXML(xmlNode, tnxTextStr, ttx_tMed);
                chCTe    := LeNoXML(xmlNode, tnxTextStr, ttx_chCTe);
                dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
                nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
                digVal   := LeNoXML(xmlNode, tnxTextStr, ttx_digVal);
                cJust    := LeNoXML(xmlNode, tnxTextStr, ttx_cJust);
                xJust    := LeNoXML(xmlNode, tnxTextStr, ttx_xJust);
                //
                XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, infProt_dhRecbtoTZD);
                //
                UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctecaba', False, [
                  'nfCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
                  'nfCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
                  'nfCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
                  'nfCanc_xJust', 'infCanc_dhRecbtoTZD'
                ], ['ID'], [
                  Id, tpAmb, verAplic,
                  dhRecbto, nProt, digVal,
                  _Stat, _Motivo, cJust,
                  xJust, infProt_dhRecbtoTZD
                ], [chCTe], True);
              end;
            end;
}

////////////////////////////////////////////////////////////////////////////////
            // E V E N T O S
            xmlList := xmlDoc.SelectNodes('/retConsSitCTe/procEventoCTe');
            LerXML_procEventoCTe(xmlList, digVal, cStat, dhRecbto);
          end else
            Geral.MB_Erro('C�digo de retorno ' + cStat + ' - ' +
            CTe_PF.Texto_StatusCTe(Geral.IMV(cStat), 0) + sLineBreak +
            'N�o esperado na consulta!');
        end;
      end else Geral.MB_Aviso('A Nota Fiscal de chave "' + chCTe +
      '" n�o foi localizada e ficar� sem defini��o de Autoriza��o ou Cancelamento DESTA CONSULTA!');
      if Geral.IMV(cStat) > 0 then
      begin
        if DefineIDCtrl(EdIDCtrlCTe, IDCtrl) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrCTeCabA2, Dmod.MyDB, [
          'SELECT * ',
          'FROM ctecaba ',
          'WHERE IDCtrl=' + Geral.FF0(IDCtrl),
          '']);
            //
          if QrCTeCabA2.RecordCount > 0 then
          begin
            // hist�rico da NF
            FatID   := QrCTeCabA2FatID.Value;
            FatNum  := QrCTeCabA2FatNum.Value;
            Empresa := QrCTeCabA2Empresa.Value;
            dhRecbto    := '0000-00-00 00:00:00';
            dhRecbtoTZD := infProt_dhRecbtoTZD;
            //
            Controle := DModG.BuscaProximoCodigoInt(
              'ctectrl', 'ctecabamsg', '', 0);
            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ctecabamsg', False, [
            'FatID', 'FatNum', 'Empresa', 'Solicit',
            'Id', 'tpAmb', 'verAplic',
            'dhRecbto', 'nProt', 'digVal',
            'cStat', 'xMotivo', '_Ativo_',
            'dhRecbtoTZD'], [
            'Controle'], [
            FatID, FatNum, Empresa, 100(*homologa��o*),
            Id, tpAmb, verAplic,
            dhRecbto, nProt, digVal,
            cStat, xMotivo, 1,
            dhRecbtoTZD], [
            Controle], True) then
            begin
              // Mostrar
              //Para evitar erros quando aberto em aba FmCTe_Pesq_0000.PageControl1.ActivePageIndex := 1;
            end;
          end;
          //end;
        end else Geral.MB_Aviso(
        'O Status retornou zerado e a Nota Fiscal de chave "' + chCTe +
        '" ficar� sem o hist�rico desta consulta!');
        //
      end else Geral.MB_Aviso(
      'Arquivo XML n�o possui informa��es de Autoriza��o ou Cancelamento de CT-e!');
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [7]');
  end;
  try
    (*
    Para evitar erros quando aberto em aba
    if IDCtrl <> 0 then
      FmCTe_Pesq_0000.ReopenCTeCabA(IDCtrl, False);
    *)
  except
    //
  end;
  //Close;
end;

procedure TFmCTeSteps_0200a.LerTextoConsultaLoteCTe();
var
  Codigo, Controle, FatID, FatNum, Empresa, Status: Integer;
  versao, tpAmb, verAplic, cStat, xMotivo, cUF, nRec, dhRecbto, tMed,
  infProt_Id, infProt_chCTe, infProt_nProt, infProt_digVal, infProt_tpAmb,
  infProt_verAplic, infProt_dhRecbto, infProt_cStat, infProt_xMotivo: String;
  dhRecbtoTZD, infProt_dhRecbtoTZD: Double;
begin
  FverXML_versao := Geral.FFT_Dot(DmCTe_0000.QrOpcoesCTeCTeVerConLot.Value, 2, siNegativo);
  dhRecbto       := '0000-00-00';
  dhRecbtoTZD    := 0;
  tMed           := '0';
  //
  if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � recibo de lote de envio
    xmlNode := xmlDoc.SelectSingleNode('/retConsReciCTe');
    if assigned(xmlNode) then
    begin
      Pagecontrol1.ActivePageIndex := 4;
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      nRec     := LeNoXML(xmlNode, tnxTextStr, ttx_nRec);
      cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      //
      Status := Integer(ctemystatusLoteEnvConsulta);
      Controle := DModG.BuscaProximoCodigoInt('ctectrl', 'ctelenm', '', 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ctelenm', False, [
        'Codigo', 'versao', 'tpAmb',
        'verAplic', 'cStat', 'xMotivo',
        'cUF', 'nRec', 'dhRecbto', 'tMed',
        'Status', 'dhRecbtoTZD'
      ], ['Controle'], [
        Codigo, versao, tpAmb,
        verAplic, cStat, xMotivo,
        cUF, nRec, dhRecbto, tMed,
        Status, dhRecbtoTZD
      ], [Controle], True) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctelenc', False, [
          'versao', 'tpAmb', 'verAplic',
          'cStat', 'xMotivo', 'cUF',
          'nRec', 'dhRecbto', 'tMed',
          'dhRecbtoTZD'
        ], ['Codigo'], [
          versao, tpAmb, verAplic,
          cStat, xMotivo, cUF,
          nRec, dhRecbto, tMed,
          dhRecbtoTZD
        ], [Codigo], True) then
        begin
          xmlList := xmlDoc.SelectNodes('/retConsReciCTe/protCTe/infProt');
          if xmlList.Length > 0 then
          begin
            while xmlList.Length > 0 do
            begin
              //
              infProt_Id       := LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_Id);
              infProt_tpAmb    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_tpAmb);
              infProt_verAplic := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_verAplic);
              infProt_chCTe    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_chCTe);
              infProt_dhRecbto := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_dhRecbto);
              infProt_nProt    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_nProt);
              infProt_digVal   := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_digVal);
              infProt_cStat    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_cStat);
              infProt_xMotivo  := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_xMotivo);
              //
              XXe_PF.Ajusta_dh_XXe_UTC(infProt_dhRecbto, infProt_dhRecbtoTZD);
              //
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctecaba', False, [
                'Status', 'infProt_Id', 'infProt_tpAmb',
                'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
                'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
                'protCTe_versao', 'infProt_dhRecbtoTZD'
              ], ['ID', 'LoteEnv'], [
                infProt_cStat, infProt_Id, infProt_tpAmb,
                infProt_verAplic, infProt_dhRecbto, infProt_nProt,
                infProt_digVal, infProt_cStat, infProt_xMotivo,
                versao, infProt_dhRecbtoTZD
              ], [infProt_chCTe, Codigo], True) then
              begin
                // hist�rico da NF
                QrCTeCabA1.Close;
                QrCTeCabA1.Params[00].AsString  := infProt_chCTe;
                QrCTeCabA1.Params[01].AsInteger := Codigo;
                QrCTeCabA1.Open;
                if QrCTeCabA1.RecordCount > 0 then
                begin
                  FatID       := QrCTeCabA1FatID.Value;
                  FatNum      := QrCTeCabA1FatNum.Value;
                  Empresa     := QrCTeCabA1Empresa.Value;
                  //
                  Controle := DModG.BuscaProximoCodigoInt(
                    'ctectrl', 'ctecabamsg', '', 0);
                  //
                  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ctecabamsg', False, [
                  'FatID', 'FatNum', 'Empresa', 'Solicit',
                  'Id', 'tpAmb', 'verAplic',
                  'dhRecbto', 'nProt', 'digVal',
                  'cStat', 'xMotivo', '_Ativo_',
                  'dhRecbtoTZD'], [
                  'Controle'], [
                  FatID, FatNum, Empresa, 100(*autoriza��o*),
                  infProt_Id, infProt_tpAmb, infProt_verAplic,
                  infProt_dhRecbto, infProt_nProt, infProt_digVal,
                  infProt_cStat, infProt_xMotivo, 1,
                  infProt_dhRecbtoTZD], [
                  Controle], True);
                end else Geral.MB_Aviso('O Conhecimento de Frete de chave "' +
                infProt_chCTe +
                '" n�o foi localizada e ficara sem o hist�rico desta consulta!');
              end;
              //
              xmlList.Remove(xmlList.Item[0]);
              //
            end;
          end
        end;
      end;
      //
      DmCTe_0000.AtualizaXML_No_BD_Tudo(False);
      //
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [6]');
  end;
end;

function TFmCTeSteps_0200a.LerTextoEnvioLoteCTe: Boolean;
  function CorrigeDataStringVazio(Data: String): String;
  begin
    if Data = '' then
      Result := '0000-00-00'
    else
      Result := Data;
  end;
var
  Status, Codigo, Controle, Empresa: Integer;
  versao, tpAmb, verAplic, cStat, xMotivo, cUF, nRec, dhRecbto, tMed: String;
  //
  FatID, FatNum: Integer;
  infProt_Id, infProt_chCTe, infProt_nProt, infProt_digVal, infProt_tpAmb,
  infProt_verAplic, infProt_dhRecbto, infProt_cStat, infProt_xMotivo: String;
  infProt_dhRecbtoTZD, dhRecbtoTZD: Double;
  Sincronia: TXXeIndSinc;
  Dir, Aviso: String;
  IDCtrl: Integer;
begin
  FverXML_versao := Geral.FFT_Dot(DmCTe_0000.QrOpcoesCTeCTeVerEnvLot.Value, 2, siNegativo);
  Result := False;
  if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � envio de lote
    xmlNode := xmlDoc.SelectSingleNode('/retEnviCte');
    if assigned(xmlNode) then
    begin
      Pagecontrol1.ActivePageIndex := 4;
(*
Arquivo XML n�o conhecido ou n�o implementado:
<retEnviCte versao='2.00' xmlns='http://www.portalfiscal.inf.br/cte'>
  <tpAmb>2</tpAmb>
  <cUF>41</cUF>
  <verAplic>PR-v2_1_21</verAplic>
  <cStat>103</cStat>
  <xMotivo>Lote recebido com sucesso</xMotivo>
  <infRec>
    <nRec>411000000672763</nRec>
    <dhRecbto>2015-12-19T20:15:34</dhRecbto>
    <tMed>1</tMed>
  </infRec>
</retEnviCte>*)
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      //
      xmlNode := xmlDoc.SelectSingleNode('/retEnviCte/protCTe');
      if assigned(xmlNode) then
      begin
        Sincronia := nisSincrono;
        Geral.MB_Aviso('Envio de lote s�ncrono n�ao implementado!');
{
(*==============================================================================
      CTe Sincrona
================================================================================
- <protCTe versao="3.10">
- <infProt Id="ID141140001620054">
  <tpAmb>2</tpAmb>
  <verAplic>PR-v3_2_1</verAplic>
  <chCTe>41141002717861000110550010000045811360413650</chCTe>
  <dhRecbto>2014-10-22T00:53:41-02:00</dhRecbto>
  <nProt>141140001620054</nProt>
  <digVal>gzIavxRcIKOO6WbOu2M+HwEH0Cg=</digVal>
  <cStat>100</cStat>
  <xMotivo>Autorizado o uso da CT-e</xMotivo>
  </infProt>
  </protCTe>
  </retEnviCTe>
*)
        xmlNode := xmlDoc.SelectSingleNode('/retEnviCTe/protCTe/infProt');
        if assigned(xmlNode) then
        begin
          infProt_Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
          infProt_tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
          infProt_verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
          infProt_chCTe    := LeNoXML(xmlNode, tnxTextStr, ttx_chCTe);
          infProt_dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
          infProt_nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
          infProt_digVal   := LeNoXML(xmlNode, tnxTextStr, ttx_digVal);
          infProt_cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
          infProt_xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
          //
          XXe_PF.Ajusta_dh_XXe_UTC(infProt_dhRecbto, infProt_dhRecbtoTZD);
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctecaba', False, [
            'Status', 'infProt_Id', 'infProt_tpAmb',
            'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
            'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
            'protCTe_versao', 'infProt_dhRecbtoTZD'
          ], ['ID', 'LoteEnv'], [
            infProt_cStat, infProt_Id, infProt_tpAmb,
            infProt_verAplic, infProt_dhRecbto, infProt_nProt,
            infProt_digVal, infProt_cStat, infProt_xMotivo,
            versao, infProt_dhRecbtoTZD
          ], [infProt_chCTe, Codigo], True) then
          begin
            Result := True;
            // hist�rico da NF
            QrCTeCabA1.Close;
            QrCTeCabA1.Params[00].AsString  := infProt_chCTe;
            QrCTeCabA1.Params[01].AsInteger := Codigo;
            QrCTeCabA1.Open;
            if QrCTeCabA1.RecordCount > 0 then
            begin
              FatID   := QrCTeCabA1FatID.Value;
              FatNum  := QrCTeCabA1FatNum.Value;
              Empresa := QrCTeCabA1Empresa.Value;
              //
              Controle := DModG.BuscaProximoCodigoInt(
                'ctectrl', 'ctecabamsg', '', 0);
              //
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ctecabamsg', False, [
              'FatID', 'FatNum', 'Empresa', 'Solicit',
              'Id', 'tpAmb', 'verAplic',
              'dhRecbto', 'nProt', 'digVal',
              'cStat', 'xMotivo', 'dhRecbtoTZD',
              '_Ativo_'], [
              'Controle'], [
              FatID, FatNum, Empresa, 100(*autoriza��o*),
              infProt_Id, infProt_tpAmb, infProt_verAplic,
              infProt_dhRecbto, infProt_nProt, infProt_digVal,
              infProt_cStat, infProt_xMotivo, infProt_dhRecbtoTZD,
              1], [
              Controle], True);
            end else Geral.MB_Aviso('O Conhecimento de Frete de chave "' +
            infProt_chCTe +
            '" n�o foi localizada e ficara sem o hist�rico desta consulta!');
            //
            Dir := DModG.QrParamsEmpDirRec.Value;
            Aviso := '';
            IDCtrl := QrCTeCabA1IDCtrl.Value;
            //DmCTe_0000.AtualizaXML_No_BD_Aut_Sinc(IDCtrl, Codigo, infProt_Id, Dir, Aviso);
            DmCTe_0000.AtualizaXML_No_BD_Aut_Sinc(IDCtrl, Codigo, infProt_chCTe, Dir, Aviso);
            //
            //DmCTe_0000.AtualizaXML_No_BD_CTe(QrCTeCabA1IDCtrl.Value, Dir, Aviso);
            //DmCTe_0000.AtualizaXML_No_BD_Tudo(False);
            //
           if Aviso <> '' then Geral.MB_Aviso(
            'Os arquivos abaixo n�o foram localizados:' + sLineBreak + Aviso);
          end;
          // CTeLEnM
          // Conforme Manual CTe 3.10:
          // C. Processamento S�ncrono
          //No caso de processamento s�ncrono do Lote de CT-e, as valida��es da
          //CT-e ser�o feitas na sequ�ncia, sem a gera��o de um N�mero de Recibo.
          nRec      := 'CTe s�ncrona';
          dhRecbto := infProt_dhRecbto;
          tMed      := '0';
          //
        end else Geral.MB_Erro('Protocolo de CTe sem inform��es!');
(*==============================================================================
      FIM  CTe 3.10  - CTe Sincrona
==============================================================================*)
}
      end else
      begin
        Sincronia := nisAssincrono;
        xmlNode := xmlDoc.SelectSingleNode('/retEnviCte/infRec');
        if assigned(xmlNode) then
        begin
          nRec      := LeNoXML(xmlNode, tnxTextStr, ttx_nRec);
          dhRecbto  := CorrigeDataStringVazio(LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto));
          tMed      := LeNoXML(xmlNode, tnxTextStr, ttx_tMed);
        end else begin
          nRec      := '';
          dhRecbto  := CorrigeDataStringVazio('');
          tMed      := '0';
        end;
      end;
      //
      XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, dhRecbtoTZD);
      //
      Controle := DModG.BuscaProximoCodigoInt('ctectrl', 'ctelenm', '', 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ctelenm', False, [
        'Codigo', 'versao', 'tpAmb',
        'verAplic', 'cStat', 'xMotivo',
        'cUF', 'nRec', 'dhRecbto', 'tMed',
        'Status', 'dhRecbtoTZD'
      ], ['Controle'], [
        Codigo, versao, tpAmb,
        verAplic, cStat, xMotivo,
        cUF, nRec, dhRecbto, tMed,
        Integer(ctemystatusLoteEnvEnviado), dhRecbtoTZD
      ], [Controle], True) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctelenc', False, [
          'versao', 'tpAmb', 'verAplic',
          'cStat', 'xMotivo', 'cUF',
          'nRec', 'dhRecbto', 'tMed'
        ], ['Codigo'], [
          versao, tpAmb, verAplic,
          cStat, xMotivo, cUF,
          nRec, dhRecbto, tMed
        ], [Codigo], True) then
        begin
          // CUIDADO!!!!  Somente se for lote Assincrono!!
          // Se fizer no sincrono a CTe mesmo autorizada fimca Status = 40 !!!
          if Sincronia = nisAssincrono then
          begin
            Status := Geral.IMV(cStat);
            //
            if Status <> 103 then
              Status := Integer(ctemystatusLoteRejeitado);
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctecaba', False, [
              'Status'], ['LoteEnv', 'Empresa'], [
              Status], [Codigo, Empresa], True);
          end;
          Result := True;
        end;
      end;
    end else
      Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado:' +
      sLineBreak + FTextoArq);
  end;
  //
  if FQry <> nil then
    UnDmkDAC_PF.AbreQuery(FQry, FQry.Database)
  else
    Geral.MB_Aviso(
    'Tabela n�o definida para localizar o lote de CT-e(s) n�mero ' +
    EdLote.Text + '!');
end;

function TFmCTeSteps_0200a.LerTextoEvento(): Boolean;
var
  Status, Codigo, Controle, Empresa, cJust: Integer;
  //tMed, nRec
  versaoRet, idLote, tpAmb, verAplic, cOrgao, cStat, xMotivo:
  String;
  infEvento_Id, infEvento_tpAmb, infEvento_verAplic, infEvento_cOrgao,
  infEvento_cStat, infEvento_xMotivo, infEvento_chCTe, infEvento_tpEvento,
  infEvento_xEvento, infEvento_CNPJDest, infEvento_CPFDest, infEvento_emailDest,
  infEvento_nSeqEvento, infEvento_dhRegEvento, infEvento_nProt, XML_retEve,
  infEvento_versao: String;
  //
  infCCe_verAplic, infCCe_dhRegEvento, infCCe_nProt,
  infCCe_CNPJ, infCCe_chCTe, infCCe_dhEvento: String;
  infCCe_cOrgao, infCCe_tpAmb, infCCe_tpEvento, infCCe_nSeqEvento, infCCe_cStat,
  infCCe_CCeConta: Integer;
  infCCe_verEvento, infCCe_dhRegEventoTZD, infCCe_dhEventoTZD: Double;
  //
  eveEPEC_Id, eveEPEC_verAplic, eveEPEC_xMotivo, eveEPEC_chCTe, eveEPEC_xEvento,
  eveEPEC_dhRegEvento, eveEPEC_nProt, eveEPEC_CNPJ, eveEPEC_dhEvento,
  eveEPEC_xJust, eveEPEC_Toma04_UF, eveEPEC_Toma04_CNPJ, eveEPEC_Toma04_CPF,
  eveEPEC_Toma04_IE, eveEPEC_UFIni, eveEPEC_UFFim: String;
  eveEPEC_tpAmb, eveEPEC_cOrgao, eveEPEC_cStat, eveEPEC_tpEvento,
  eveEPEC_nSeqEvento, eveEPEC_cJust, eveEPEC_CTeEveREPECConta,
  eveEPEC_CodInfoTomad, eveEPEC_Toma04_toma, eveEPEC_Modal: Integer;
  eveEPEC_versaoEnv, eveEPEC_versaoEvento, eveEPEC_vICMS, eveEPEC_vTPrest,
  eveEPEC_vCarga, eveEPEC_versaoRet: Double;
  //
  infCanc_Id, infCanc_tpAmb, infCanc_verAplic, infCanc_dhRecbto,
  infCanc_nProt, infCanc_digVal, infCanc_xMotivo,
  infCanc_xJust, retCancCTe_versao: String;
  ret_TZD_UTC, eveEPEC_TZD_UTC: Double;
  infCanc_cJust, tpEvento, nSeqEvento, SubCtrl, infCanc_cStat: Integer;
  //cSitConf: Integer;
  //cSitCTe: Integer;
  Id, xJust: String;
  SQLType: TSQLType;
  infCanc_dhRecbtoTZD: Double;
  //
  CNPJ, CPF, Toma04_UF, Toma04_CNPJ, Toma04_CPF, Toma04_IE, UFIni, UFFim: String;
  dhEvento: TDateTime;
  dhEventoTZD, verEvento, VersaoEnv, vICMS, vTPrest, vCarga: Double;
  nCondUso, CTeEveRCCeConta, CTeEveREPECConta, CodInfoTomad, Toma04_toma,
  Modal: Integer;
begin
  //FverXML_versao := verEnviEvento_Versao;
  FverXML_versao := Geral.FFT_Dot(DmCTe_0000.QrOpcoesCTeCTeVerLotEve.Value, 2, siNegativo);
  Result := False;
  //if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � envio de lote
    //xmlNode := xmlDoc.SelectSingleNode('/retEnvEvento');
    // Verifica se � evento de CTe
    xmlNode := xmlDoc.SelectSingleNode('/retEventoCTe');
    if assigned(xmlNode) then
    begin
(*
<?xml version="1.0"?>
-<retEventoCTe xmlns="http://www.portalfiscal.inf.br/cte" versao="2.00">
  -<infEvento>
    <tpAmb>2</tpAmb>
    <verAplic>PR-v2_1_21</verAplic>
    <cOrgao>41</cOrgao>
    <cStat>135</cStat>
    <xMotivo>Evento registrado e vinculado a CT-e</xMotivo>
    <chCTe>41151202717861000110570000000000161704437136</chCTe>
    <tpEvento>110111</tpEvento>
    <xEvento>Cancelamento</xEvento>
    <nSeqEvento>1</nSeqEvento>
    <dhRegEvento>2015-12-26T15:09:50</dhRegEvento>
    <nProt>141150000218474</nProt>
  </infEvento>
</retEventoCTe>
*)
      //Pagecontrol1.ActivePageIndex := 4;
      versaoRet   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao, False);
      //
(*
      idLote   := LeNoXML(xmlNode, tnxTextStr, ttx_idLote);
      tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      cOrgao   := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
      cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      //
      ///xmlNode := xmlDoc.SelectSingleNode('/retEnvEvento/retEvento');
      Controle := DModG.BuscaProximoCodigoInt('ctectrl', 'cteeverlor', '', 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'cteeverlor', False, [
        'Codigo', 'versao', 'tpAmb',
        'verAplic', 'cOrgao',
        'cStat', 'xMotivo'
      ], ['Controle'], [
        Codigo, versao, tpAmb,
        verAplic, cOrgao,
        cStat, xMotivo
      ], [Controle], True) then
*)
      begin
(*        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cteeverloe', False, [
          'versao', 'tpAmb', 'verAplic',
          'cOrgao', 'cStat', 'xMotivo'
        ], ['Codigo'], [
          versao, tpAmb, verAplic,
          cOrgao, cStat, xMotivo
        ], [Codigo], True) then
*)
        begin
(*
          xmlList := xmlDoc.SelectNodes('/retEnvEvento/retEvento');
          //xmlList := xmlDoc.SelectNodes('/retEnvEvento/retEvento/infEvento');
          if xmlList.Length > 0 then
*)
          begin
(*
            //I := -1;
            while xmlList.Length > 0 do
*)
            //begin
(*
              //I := I + 1;
              infEvento_versao := LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_versao, False);
              XML_retEve            := xmlList.Item[0].XML;
              //
*)
              //xmlNode := xmlList.Item[0].SelectSingleNode('/retEnvEvento/retEvento/infEvento');
              //xmlNode := xmlList.Item[0].FirstChild;
              //xmlList.Item[0].SelectSingleNode('/retEnvEvento/retEvento/infEvento', xmlNode);
              xmlNode := xmlDoc.SelectSingleNode('/retEventoCTe/infEvento');
              if assigned(xmlNode) then
              begin
(*
<?xml version="1.0"?>
-<retEventoCTe xmlns="http://www.portalfiscal.inf.br/cte" versao="2.00">
  -<infEvento>
    <tpAmb>2</tpAmb>
    <verAplic>PR-v2_1_21</verAplic>
    <cOrgao>41</cOrgao>
    <cStat>135</cStat>
    <xMotivo>Evento registrado e vinculado a CT-e</xMotivo>
    <chCTe>41151202717861000110570000000000161704437136</chCTe>
    <tpEvento>110111</tpEvento>
    <xEvento>Cancelamento</xEvento>
    <nSeqEvento>1</nSeqEvento>
    <dhRegEvento>2015-12-26T15:09:50</dhRegEvento>
    <nProt>141150000218474</nProt>
  </infEvento>
</retEventoCTe>
*)
                //infEvento_Id          := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
                //
                infEvento_tpAmb       := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
                infEvento_verAplic    := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
                infEvento_cOrgao      := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
                infEvento_cStat       := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
                infEvento_xMotivo     := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
                infEvento_chCTe       := LeNoXML(xmlNode, tnxTextStr, ttx_chCTe);
                infEvento_tpEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
                infEvento_xEvento     := LeNoXML(xmlNode, tnxTextStr, ttx_xEvento);
                //infEvento_CNPJDest    := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJDest);
                //infEvento_CPFDest     := LeNoXML(xmlNode, tnxTextStr, ttx_CPFDest);
                //infEvento_emailDest   := LeNoXML(xmlNode, tnxTextStr, ttx_emailDest);
                infEvento_nSeqEvento  := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
                infEvento_dhRegEvento := LeNoXML(xmlNode, tnxTextStr, ttx_dhRegEvento);
                infEvento_nProt       := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
                //
              //
              XXe_PF.Ajusta_dh_XXe_UTC(infEvento_dhRegEvento, ret_TZD_UTC);
              //
              Status := Geral.IMV(infEvento_cStat);

              tpEvento := Geral.IMV(infEvento_tpEvento);
              nSeqEvento := Geral.IMV(infEvento_nSeqEvento);
              //N�O LOCALIZA DIREITO
              Controle := DmCTe_0000.EventoObtemCtrl(Codigo, tpEvento,
                nSeqEvento, infEvento_chCTe);
              //
              if Controle <> 0 then
              begin
                infEvento_versao := versaoRet;
                XML_retEve       := FTextoArq;
                //
                if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cteevercab', False, [
                'XML_retEve',
                'Status', 'ret_versao', 'ret_Id',
                'ret_tpAmb', 'ret_verAplic', 'ret_cOrgao',
                'ret_cStat', 'ret_xMotivo', 'ret_chCTe',
                'ret_tpEvento', 'ret_xEvento', 'ret_nSeqEvento',
                'ret_CNPJDest', 'ret_CPFDest', 'ret_emailDest',
                'ret_dhRegEvento', 'ret_TZD_UTC', 'ret_nProt'], [
                (*'FatID', 'FatNum', 'Empresa',*) 'Controle'], [
                Geral.WideStringToSQLString(XML_retEve), Status,
                (*ret_versao*)infEvento_versao, (*ret_Id*)infEvento_Id,
                (*ret_tpAmb*)infEvento_tpAmb, (*ret_verAplic*)infEvento_verAplic,
                (*ret_cOrgao*)infEvento_cOrgao, (*ret_cStat*)infEvento_cStat,
                (*ret_xMotivo*)infEvento_xMotivo, (*ret_chCTe*)infEvento_chCTe,
                (*ret_tpEvento*)infEvento_tpEvento, (*ret_xEvento*)infEvento_xEvento,
                (*ret_nSeqEvento*)infEvento_nSeqEvento, (*ret_CNPJDest*)infEvento_CNPJDest,
                (*ret_CPFDest*)infEvento_CPFDest, (*ret_emailDest*)infEvento_emailDest,
                (*ret_dhRegEvento*)infEvento_dhRegEvento, ret_TZD_UTC,
                (*ret_nProt*)infEvento_nProt], [
                (*FatID, FatNum, Empresa,*) Controle], True) then
                begin
                  // hist�rico
                  //SubCtrl := DModG.BuscaProximoCodigoInt('ctectrl', 'cteeverret', '', 0);
                  DmCTe_0000.EventoObtemSub(Controle, infEvento_Id, SubCtrl, SQLType);

                  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cteeverret', False, [
                  'Controle', 'ret_versao', 'ret_Id', 'ret_tpAmb',
                  'ret_verAplic', 'ret_cOrgao', 'ret_cStat',
                  'ret_xMotivo', 'ret_chCTe', 'ret_tpEvento',
                  'ret_xEvento', 'ret_nSeqEvento', 'ret_CNPJDest',
                  'ret_CPFDest', 'ret_emailDest', 'ret_dhRegEvento',
                  'ret_TZD_UTC', 'ret_nProt'], [
                  'SubCtrl'], [
                  Controle, (*ret_versao*)infEvento_versao, (*ret_Id*)infEvento_Id,
                  (*ret_tpAmb*)infEvento_tpAmb, (*ret_verAplic*)infEvento_verAplic,
                  (*ret_cOrgao*)infEvento_cOrgao, (*ret_cStat*)infEvento_cStat,
                  (*ret_xMotivo*)infEvento_xMotivo, (*ret_chCTe*)infEvento_chCTe,
                  (*ret_tpEvento*)infEvento_tpEvento, (*ret_xEvento*)infEvento_xEvento,
                  (*ret_nSeqEvento*)infEvento_nSeqEvento, (*ret_CNPJDest*)infEvento_CNPJDest,
                  (*ret_CPFDest*)infEvento_CPFDest, (*ret_emailDest*)infEvento_emailDest,
                  (*ret_dhRegEvento*)infEvento_dhRegEvento, ret_TZD_UTC,
                  (*ret_nProt*)infEvento_nProt], [
                  SubCtrl], True) then
                  begin
                    case Geral.IMV(infEvento_tpEvento) of
                      CO_COD_evexxe110110CCe: // Carta de corre��o.
                      //A princ�pio n�o faz nada!
                      // Implementado nos aplicativos Dermatek s� na CTe 3.10
                      begin
                        //
                        if infEvento_cStat = '135' then
                        begin
                          if not DmCTe_0000.LocalizaCTeInfoEveCCe(
                            infEvento_chCTe, nSeqEvento, CNPJ, CPF, dhEvento,
                              dhEventoTZD, verEvento, nCondUso, CTeEveRCCeConta)
                          then
                            Geral.MB_Erro('Falha ao localizar dados do cancelamento');
                          //
                          infCCe_verAplic       := verAplic;
                          infCCe_cOrgao         := Geral.IMV(cOrgao);
                          infCCe_tpAmb          := Geral.IMV(tpAmb);
                          infCCe_CNPJ           := CNPJ;
                          infCCe_chCTe          := infEvento_chCTe;
                          infCCe_dhEvento       := Geral.FDT(dhEvento, 1);
                          infCCe_dhEventoTZD    := dhEventoTZD;
                          infCCe_tpEvento       := tpEvento;
                          infCCe_nSeqEvento     := nSeqEvento;
                          infCCe_verEvento      := verEvento;
                          infCCe_CCeConta       := CTeEveRCCeConta;
                          infCCe_cStat          := Geral.IMV(cStat);
                          infCCe_dhRegEvento    := infEvento_dhRegEvento;
                          infCCe_dhRegEventoTZD := ret_TZD_UTC;
                          infCCe_nProt          := infEvento_nProt;
                          //
                          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctecaba', False, [
                          'infCCe_verAplic', 'infCCe_cOrgao',
                          'infCCe_tpAmb', 'infCCe_CNPJ', 'infCCe_chCTe',
                          'infCCe_dhEvento', 'infCCe_dhEventoTZD', 'infCCe_tpEvento',
                          'infCCe_nSeqEvento', 'infCCe_verEvento', 'infCCe_CCeConta',
                          'infCCe_cStat', 'infCCe_dhRegEvento', 'infCCe_dhRegEventoTZD',
                          'infCCe_nProt'], [
                          'ID'], [
                          infCCe_verAplic, infCCe_cOrgao,
                          infCCe_tpAmb, infCCe_CNPJ, infCCe_chCTe,
                          infCCe_dhEvento, infCCe_dhEventoTZD, infCCe_tpEvento,
                          infCCe_nSeqEvento, infCCe_verEvento, infCCe_CCeConta,
                          infCCe_cStat, infCCe_dhRegEvento, infCCe_dhRegEventoTZD,
                          infCCe_nProt], [
                          infEvento_chCTe], True);
                        end;
                      end;
                      CO_COD_evexxe110111Can: // Cancelamento
                      begin
                        //
                        if infEvento_cStat = '135' then
                        begin
                          if not DmCTe_0000.LocalizaCTeInfoEveCan(
                            infEvento_chCTe, Id, xJust, cJust)
                          then
                            Geral.MB_Erro('Falha ao localizar dados do cancelamento');
                          //
                          Status              := 101;
                          infCanc_Id          := Id;
                          infCanc_tpAmb       := infEvento_tpAmb;
                          infCanc_verAplic    := infEvento_verAplic;
                          infCanc_dhRecbto    := infEvento_dhRegEvento;
                          infCanc_dhRecbtoTZD := ret_TZD_UTC;
                          infCanc_nProt       := infEvento_nProt;
                          infCanc_digVal      := '';//;N�o vem
                          infCanc_cStat       := 101;
                          infCanc_xMotivo     := 'Cancelamento de CT-e homologado';
                          infCanc_cJust       := cJust;
                          infCanc_xJust       := xJust;
                          retCancCTe_versao   := infEvento_versao;
                          //
                          Id                  := infEvento_chCTe;
                          //
                          if not DmCTe_0000.CancelaFaturamento(Id) then
                            Geral.MB_Aviso('Falha ao atualizar o faturamento!');
                          //
                          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctecaba', False, [
                            'Status', 'infCanc_Id', 'infCanc_tpAmb',
                            'infCanc_verAplic', 'infCanc_dhRecbto', 'infCanc_nProt',
                            'infCanc_digVal', 'infCanc_cStat', 'infCanc_xMotivo',
                            'infCanc_cJust', 'infCanc_xJust', 'retCancCTe_versao',
                            'infCanc_dhRecbtoTZD'
                          ], ['Id'], [
                            Status, infCanc_Id, infCanc_tpAmb,
                            infCanc_verAplic, infCanc_dhRecbto, infCanc_nProt,
                            infCanc_digVal, infCanc_cStat, infCanc_xMotivo,
                            infCanc_cJust, infCanc_xJust, retCancCTe_versao,
                            infCanc_dhRecbtoTZD
                          ], [infEvento_chCTe], True);

                        end;
                      end;
                      CO_COD_evexxe110113EPEC: // EPEC
                      begin
                        if infEvento_cStat = '136' then
                        begin
                          if not DmCTe_0000.LocalizaCTeInfoEveEPEC(
                          infEvento_chCTe, CNPJ, Id, xJust, cJust, CTeEveREPECConta,
                          vICMS, vTPrest, vCarga, CodInfoTomad, Toma04_toma,
                          Toma04_UF, Toma04_CNPJ, Toma04_CPF, Toma04_IE,
                          Modal, UFIni, UFFim, verEvento, versaoEnv, dhEvento) then
                            Geral.MB_Erro('Falha ao localizar dados do EPEC');
                          //
                          Id                  := infEvento_chCTe;
                          //
                          eveEPEC_versaoEnv        := versaoEnv;
                          eveEPEC_CNPJ             := CNPJ;
                          eveEPEC_dhEvento         := Geral.FDT(dhEvento, 109);
                          eveEPEC_versaoEvento     := verEvento;
                          eveEPEC_cJust            := cJust;
                          eveEPEC_xJust            := xJust;
                          eveEPEC_vICMS            := vICMS;
                          eveEPEC_vTPrest          := vTPrest;
                          eveEPEC_vCarga           := vCarga;
                          eveEPEC_versaoRet        := Geral.DMV_Dot(versaoRet);
                          eveEPEC_CTeEveREPECConta := CTeEveREPECConta;
                          eveEPEC_CodInfoTomad     := CodInfoTomad;
                          eveEPEC_Toma04_toma      := Toma04_toma;
                          eveEPEC_Toma04_UF        := Toma04_UF;
                          eveEPEC_Toma04_CNPJ      := Toma04_CNPJ;
                          eveEPEC_Toma04_CPF       := Toma04_CPF;
                          eveEPEC_Toma04_IE        := Toma04_IE;
                          eveEPEC_Modal            := Modal;
                          eveEPEC_UFIni            := UFIni;
                          eveEPEC_UFFim            := UFFim;
                          //
                          eveEPEC_Id          := infEvento_Id;
                          eveEPEC_tpAmb       := Geral.IMV(infEvento_tpAmb);
                          eveEPEC_verAplic    := infEvento_verAplic;
                          eveEPEC_cOrgao      := Geral.IMV(infEvento_cOrgao);
                          eveEPEC_cStat       := Geral.IMV(infEvento_cStat);
                          eveEPEC_xMotivo     := infEvento_xMotivo;
                          eveEPEC_chCTe       := infEvento_chCTe;
                          eveEPEC_tpEvento    := Geral.IMV(infEvento_tpEvento);
                          eveEPEC_xEvento     := infEvento_xEvento;
                          eveEPEC_nSeqEvento  := Geral.IMV(infEvento_nSeqEvento);
                          eveEPEC_dhRegEvento := infEvento_dhRegEvento;
                          eveEPEC_nProt       := infEvento_nProt;
                          //
                          Status := XXe_PF.XXeObtemStatusXXedeChave(tipoxxeCTe, Id);
                          if (Status <> 100) and (Status <> 101) then
                            Status := Integer(ctemystatusEveEPEC_OK);
                          //
                          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctecaba', False, [
                          'Status',
                          'eveEPEC_Id', 'eveEPEC_tpAmb', 'eveEPEC_verAplic',
                          'eveEPEC_cOrgao', 'eveEPEC_cStat', 'eveEPEC_xMotivo',
                          'eveEPEC_chCTe', 'eveEPEC_tpEvento', 'eveEPEC_xEvento',
                          'eveEPEC_nSeqEvento', 'eveEPEC_dhRegEvento', 'eveEPEC_nProt',
                          'eveEPEC_versaoEnv', 'eveEPEC_CNPJ', 'eveEPEC_dhEvento',
                          'eveEPEC_versaoEvento', 'eveEPEC_cJust', 'eveEPEC_xJust',
                          'eveEPEC_vICMS', 'eveEPEC_vTPrest', 'eveEPEC_vCarga',
                          'eveEPEC_versaoRet', 'eveEPEC_CTeEveREPECConta', 'eveEPEC_CodInfoTomad',
                          'eveEPEC_Toma04_toma', 'eveEPEC_Toma04_UF', 'eveEPEC_Toma04_CNPJ',
                          'eveEPEC_Toma04_CPF', 'eveEPEC_Toma04_IE', 'eveEPEC_Modal',
                          'eveEPEC_UFIni', 'eveEPEC_UFFim'
                          ], ['Id'], [
                          Status,
                          eveEPEC_Id, eveEPEC_tpAmb, eveEPEC_verAplic,
                          eveEPEC_cOrgao, eveEPEC_cStat, eveEPEC_xMotivo,
                          eveEPEC_chCTe, eveEPEC_tpEvento, eveEPEC_xEvento,
                          eveEPEC_nSeqEvento, eveEPEC_dhRegEvento, eveEPEC_nProt,
                          eveEPEC_versaoEnv, eveEPEC_CNPJ, eveEPEC_dhEvento,
                          eveEPEC_versaoEvento, eveEPEC_cJust, eveEPEC_xJust,
                          eveEPEC_vICMS, eveEPEC_vTPrest, eveEPEC_vCarga,
                          eveEPEC_versaoRet, eveEPEC_CTeEveREPECConta, eveEPEC_CodInfoTomad,
                          eveEPEC_Toma04_toma, eveEPEC_Toma04_UF, eveEPEC_Toma04_CNPJ,
                          eveEPEC_Toma04_CPF, eveEPEC_Toma04_IE, eveEPEC_Modal,
                          eveEPEC_UFIni, eveEPEC_UFFim
                          ], [Id], True) then
                          begin
                            ///
                          end;
                        end;
                      end;
                      else
                      begin
                        Geral.MB_Aviso('Tipo de evento n�o implementado na fun��o:' +
                        sLineBreak + 'TFmCTeSteps_0200a.LerTextoEnvioLoteEvento()');
                        Result := False;
                        Exit;
                      end;
                    end;
                  end;
                end;
              end;
              //
              //xmlList.Remove(xmlList.Item[0]);
              //
            end;
          end
        end;

      end;
      // N�o precisa! J� Faz direto nas SQL acima!
      DmCTe_0000.AtualizaXML_No_BD_Tudo(False);
      //
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [2]');
  end;
end;

procedure TFmCTeSteps_0200a.LerTextoEvento_Cancelamento();
begin
  LerTextoEvento();
end;

procedure TFmCTeSteps_0200a.LerTextoEvento_CartaDeCorrecao();
begin
  LerTextoEvento();
end;

procedure TFmCTeSteps_0200a.LerTextoEvento_EPEC();
begin
  LerTextoEvento();
end;

procedure TFmCTeSteps_0200a.LerTextoInutilizaNumerosCTe();
var
  Controle, Empresa: Integer;
  versao, tpAmb, verAplic, cStat, xMotivo, cUF, dhRecbto, Id, nProt, xJust, Ano,
  CNPJ, Modelo, Serie, nCTIni, nCTFim: String;
  //
  Lote: Integer;
  dhRecbtoTZD: Double;
begin
  //FverXML_versao := verCTeInutCTe_Versao;
  FverXML_versao := Geral.FFT_Dot(DmCTe_0000.QrOpcoesCTeCTeVerInuNum.Value, 2, siNegativo);
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � recibo de inutiliza��o de n�meros de NF
    xmlNode := xmlDoc.SelectSingleNode('/retInutCTe');
    if assigned(xmlNode) then
    begin
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      tpAmb    := '';
      verAplic := '';
      cStat    := '';
      xMotivo  := '';
      cUF      := '';
      xmlNode := xmlDoc.SelectSingleNode('/retInutCTe/infInut');
      if assigned(xmlNode) then
      begin
        Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
        tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
        verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
        cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
        xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
        cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
        if not DefineLote(Lote) then
        begin
          CTe_PF.DesmontaID_Inutilizacao(
            Id, cUF, Ano, CNPJ, Modelo, Serie, nCTIni, nCTFim);
          DmCTe_0000.QrCTeInut.Close;
          DmCTe_0000.QrCTeInut.Params[00].AsInteger := Empresa;
          DmCTe_0000.QrCTeInut.Params[01].AsString := cUF;
          DmCTe_0000.QrCTeInut.Params[02].AsString := ano;
          DmCTe_0000.QrCTeInut.Params[03].AsString := CNPJ;
          DmCTe_0000.QrCTeInut.Params[04].AsString := modelo;
          DmCTe_0000.QrCTeInut.Params[05].AsString := Serie;
          DmCTe_0000.QrCTeInut.Params[06].AsString := nCTIni;
          DmCTe_0000.QrCTeInut.Params[07].AsString := nCTFim;
          DmCTe_0000.QrCTeInut.Open;
          Lote := DmCTe_0000.QrCTeInutCodigo.Value;
          if Lote = 0 then
          begin
            Geral.MB_Erro('N�o foi poss�vel descobrir o Lote pelo ID!');
            //
            Exit;
          end else
            Geral.MB_Aviso('O Lote foi encontrado pelo ID!');
        end;
        if cStat = '102' then
        begin
          ano      := LeNoXML(xmlNode, tnxTextStr, ttx_ano);
          CNPJ     := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
          modelo   := LeNoXML(xmlNode, tnxTextStr, ttx_mod);
          serie    := LeNoXML(xmlNode, tnxTextStr, ttx_serie);
          nCTIni   := LeNoXML(xmlNode, tnxTextStr, ttx_nCTIni);
          nCTFim   := LeNoXML(xmlNode, tnxTextStr, ttx_nCTFin);
          dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
          nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
          //
          XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, dhRecbtoTZD);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cteinut', False, [
          'versao', 'Id', 'tpAmb',
          'cUF', 'ano', 'CNPJ',
          //ver aqui o que fazer
          'modelo', 'Serie',
          'nCTIni', 'nCTFim', 'xJust',
          'cStat', 'xMotivo', 'dhRecbto',
          'nProt', 'dhRecbtoTZD'], ['Codigo'], [
          versao, Id, tpAmb,
          cUF, ano, CNPJ,
          modelo, Serie,
          nCTIni, nCTFim, xJust,
          cStat, xMotivo, dhRecbto,
          nProt, dhRecbtoTZD], [Lote], True);
        end;
        Controle := DModG.BuscaProximoCodigoInt('ctectrl', 'cteinutmsg', '', 0);
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'cteinutmsg', False, [
        'Codigo', 'versao', 'Id',
        'tpAmb', 'verAplic', 'cStat',
        'xMotivo', 'cUF', '_Ativo_'], [
        'Controle'], [
        Lote, versao, Id,
        tpAmb, verAplic, cStat,
        xMotivo, cUF, 1], [
        Controle], True);
      end else Geral.MB_Aviso(
      'Arquivo XML n�o possui informa��es de Inutiliza��o de numera��o de CT-e!');
    end else Geral.MB_Aviso(
    'Arquivo XML n�o conhecido ou n�o implementado! [3]' +
    sLineBreak + FTextoArq);
    //
    DmCTe_0000.AtualizaXML_No_BD_Tudo(False);
    //
  end;
  FmCTeInut_0000.LocCod(Lote, Lote);
  //Close;
end;

procedure TFmCTeSteps_0200a.LerTextoStatusServico();
(*
<retConsStatServCte versao='2.00' xmlns='http://www.portalfiscal.inf.br/cte'>
  <tpAmb>2</tpAmb>
  <verAplic>PR-v2_1_21</verAplic>
  <cStat>107</cStat>
  <xMotivo>Servico em Operacao</xMotivo>
  <cUF>41</cUF>
  <dhRecbto>2015-12-19T11:49:44</dhRecbto>
  <tMed>0</tMed>
</retConsStatServCte>*)
begin
  FverXML_versao := Geral.FFT_Dot(DmCTe_0000.QrOpcoesCTeCTeVerStaSer.Value, 2, siNegativo);
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � status de servi�o
    xmlNode := xmlDoc.SelectSingleNode('/retConsStatServCte');
    if assigned(xmlNode) then
    begin
      LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
      LeNoXML(xmlNode, tnxTextStr, ttx_tMed);
      //
      Pagecontrol1.ActivePageIndex := 4;
    end else Geral.MB_Aviso(
    'Arquivo XML n�o conhecido ou n�o implementado! [4]' + sLineBreak +
    'Texto XML:' + sLineBreak +
    FTextoArq);
  end;
end;

procedure TFmCTeSteps_0200a.LerXML_procEventoCTe(Lista: IXMLNodeList;
digVal, cStat, dhRecbto: String);
var
  IDCtrl, Controle, Conta, FatID, FatNum, Empresa: Integer;
  // Parte generica
  infEvento_Id,
  infEvento_tpAmb,
  infEvento_verAplic,
  infEvento_cOrgao,
  infEvento_cStat,
  infEvento_xMotivo,
  infEvento_chCTe,
  infEvento_tpEvento,
  infEvento_xEvento,
  infEvento_nSeqEvento,
  infEvento_dhRegEvento,
  infEvento_dhEvento,
  infEvento_nProt,
  // Especifica de Cancelamento
  infEvento_CNPJ,
  infCanc_descEvento,
  infCanc_nProt,
  infCanc_xJust,
  infCanc_dhEvento,
  infCanc_dhRegEvento,



  cUF, //Id,
  cJust, _Stat, _Motivo: String;
  //
  Status, Evento, nCondUso, infCCe_CCeConta: Integer;
  infProt_dhRecbtoTZD, dhRecbtoTZD, infCanc_dhRecbtoTZD, dhEventoTZD: Double;
  //




  // Copiado da N.F.e mas nao existe:
  infEvento_CPF,

  eventoCTe_evento, infEvento_versaoEvento: String;
  //
  infCCe_verAplic, infCCe_dhRegEvento, infCCe_nProt,
  infCCe_CNPJ, infCCe_CPF, infCCe_chCTe, infCCe_dhEvento: String;
  infCCe_cOrgao, infCCe_tpAmb, infCCe_tpEvento, infCCe_nSeqEvento, infCCe_cStat,
  infCCe_nCondUso: Integer;
  infCCe_verEvento, infCCe_dhRegEventoTZD, infCCe_dhEventoTZD: Double;
  //
  _CNPJ, _CPF: String;
  _dhEvento: TDateTime;
  _dhEventoTZD, _verEvento: Double;
  CTeEveRCCeConta: Integer;
  versao, descEvento, grupoAlterado, CampoAlterado, ValorAlterado,
  nroItemAlterado: String;
  //
begin
  if Lista.Length > 0 then
  begin
    while Lista.Length > 0 do
    begin
      eventoCTe_evento   := '';
      infEvento_Id          := '';
      infEvento_tpAmb       := '';
      infEvento_verAplic    := '';
      infEvento_cOrgao      := '';
      infEvento_cStat       := '';
      infEvento_xMotivo     := '';
      infEvento_chCTe       := '';
      infEvento_tpEvento    := '';
      infEvento_xEvento     := '';
      infEvento_nSeqEvento  := '';
      infEvento_dhRegEvento := '';
      infEvento_dhEvento    := '';
      // Cancelamento
      infEvento_CNPJ        := '';
      infCanc_nProt         := '';
      infCanc_xJust         := '';
      infCanc_descEvento    := '';
      infCanc_dhEvento      := '';
      infCanc_dhRegEvento   := '';

      //
      CTeEveRCCeConta  := 0;  // Carta de correcao
      //
      //xmlSub  := Lista.Item[0].FirstChild;
      xmlSub  := nil;
      xmlSub  := Lista.Item[0];
      // Dados do envio ao Fisco...
      // ... dados gerais
      // Erro Aqui! Pega sempre o primeiro!
      //xmlNode := xmlSub.SelectSingleNode('/retConsSitCTe/procEventoCTe/evento/infEvento');
      // Nao pega nada!
      //xmlNode := xmlSub.SelectSingleNode('procEventoCTe/infEvento');
      xmlNode := xmlSub.SelectSingleNode('eventoCTe');
      if assigned(xmlNode) then
      begin
        eventoCTe_evento  := LeNoXML(xmlNode, tnxTextStr, ttx_verEvento);
        //
        xmlNode := xmlSub.SelectSingleNode('eventoCTe/infEvento');
        if assigned(xmlNode) then
        begin
          infEvento_Id         := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
          infEvento_cOrgao     := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
          infEvento_tpAmb      := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
          infEvento_CNPJ       := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
          infEvento_CPF        := LeNoXML(xmlNode, tnxTextStr, ttx_CPF);
          infEvento_chCTe      := LeNoXML(xmlNode, tnxTextStr, ttx_chCTe);
          infEvento_dhEvento   := LeNoXML(xmlNode, tnxTextStr, ttx_dhEvento);
          infEvento_tpEvento   := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
          infEvento_nSeqEvento := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
          //
          XXe_PF.Ajusta_dh_XXe_UTC(infEvento_dhEvento, dhEventoTZD);
          //
          // ... dados especificos de cada evento
          //xmlNode := xmlSub.SelectSingleNode('/retConsSitCTe/procEventoCTe/eventoCTe/infEvento/detEvento');
          xmlChild1 := xmlNode.SelectSingleNode('detEvento');
          if assigned(xmlChild1) then
          begin
            infEvento_versaoEvento := LeNoXML(xmlChild1, tnxAttrStr, ttx_versaoEvento);
            // carta de correcao
            xmlChild2 := xmlNode.SelectSingleNode('detEvento/evCCeCTe');
            if assigned(xmlChild2) then
            begin
              descEvento := LeNoXML(xmlChild2, tnxTextStr, ttx_descEvento);
              xmlChild3 := xmlChild2.SelectSingleNode('infCorrecao');
              if assigned(xmlChild3) then
              begin
                UnDmkDAC_PF.AbreMySQLQuery0(QrCTeEveRCab, Dmod.MyDB, [
                'SELECT * ',
                'FROM cteevercab ',
                'WHERE chCTe="' + infEvento_chCTe + '"',
                'AND tpEvento=' + infEvento_tpEvento, //110110
                'AND nSeqEvento=' + infEvento_nSeqEvento, //2
                '']);
                FatID     := QrCTeEveRCabFatID.Value;
                FatNum    := QrCTeEveRCabFatNum.Value;
                Empresa   := QrCTeEveRCabEmpresa.Value;
                Controle  := QrCTeEveRCabControle.Value;
                //Conta     := QrCTeEveRCabConta.Value;
                while assigned(xmlChild3) do
                begin
                  grupoAlterado := LeNoXML(xmlChild3, tnxTextStr, ttx_grupoAlterado);
                  CampoAlterado := LeNoXML(xmlChild3, tnxTextStr, ttx_campoAlterado);
                  valorAlterado := LeNoXML(xmlChild3, tnxTextStr, ttx_valorAlterado);
                  nroItemAlterado := LeNoXML(xmlChild3, tnxTextStr, ttx_nroItemAlterado);
                  if Trim(nroItemAlterado) = '' then
                    nroItemAlterado := '0';
                  //
                  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeEveRCCeIts, Dmod.MyDB, [
                  'SELECT * ',
                  'FROM cteevercceits ',
                  'WHERE FatID=' + Geral.FF0(FatID),
                  'AND FatNum=' + Geral.FF0(FatNum),
                  'AND Empresa=' + Geral.FF0(Empresa),
                  'AND Controle=' + Geral.FF0(Controle),
                  //'AND Conta=' + Geral.FF0(Conta),
                  'AND GrupoAlterado="' + GrupoAlterado + '"',
                  'AND CampoAlterado="' + CampoAlterado + '"',
                  'AND nroItemAlterado=' + nroItemAlterado,
                  '']);
                  if QrCTeEveRCCeIts.RecordCount = 0 then
                  begin
                    Geral.MB_Erro('Item de carta de corre��o n�o incontrado!' +
                    sLineBreak + '*********AVISE A DERMATEK**********' +
                    sLineBreak + QrCTeEveRCCeIts.SQL.Text);
                  end;
                  //
                  xmlChild2.RemoveChild(xmlChild3);
                  //
                  xmlChild3 := xmlChild2.SelectSingleNode('infCorrecao');
                end;
              end;
            end;
////////////////////////////////////////////////////////////////////////////////
            // Cancelamento
(*
<detEvento versaoEvento="2.00">
  <evCancCTe>
    <descEvento>Cancelamento</descEvento>
    <nProt>141150000218490</nProt>
    <xJust>TESTE DE CANCELAMENTO</xJust>
  </evCancCTe>
</detEvento>
*)
            xmlChild2 := xmlNode.SelectSingleNode('detEvento/evCancCTe');
            if assigned(xmlChild2) then
            begin
              infCanc_descEvento := LeNoXML(xmlChild2, tnxTextStr, ttx_descEvento);
              infCanc_nProt      := LeNoXML(xmlChild2, tnxTextStr, ttx_nProt);
              infCanc_xJust      := LeNoXML(xmlChild2, tnxTextStr, ttx_xJust);
            end;
          end;
// Fim cancelamento
        //
        //
        end;
        // Dados do retorno da consulta no Fisco
        // erro aqui
        //xmlNode := xmlSub.SelectSingleNode('/retConsSitCTe/procEventoCTe/retEvento/infEvento');
        xmlNode := xmlSub.SelectSingleNode('retEventoCTe/infEvento');
        if assigned(xmlNode) then
        begin
          infEvento_tpEvento := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
          _Stat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
          Evento := Geral.IMV(infEvento_tpEvento);
          if (Geral.IMV(_Stat) in ([135, 136])) then
          begin
            infEvento_tpAmb       := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
            infEvento_verAplic    := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
            infEvento_cOrgao      := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
            infEvento_xMotivo     := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
            infEvento_chCTe       := LeNoXML(xmlNode, tnxTextStr, ttx_chCTe);
            infEvento_tpEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
            infEvento_xEvento     := LeNoXML(xmlNode, tnxTextStr, ttx_xEvento);
            infEvento_nSeqEvento  := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
            infEvento_dhRegEvento := LeNoXML(xmlNode, tnxTextStr, ttx_dhRegEvento);
            infEvento_nProt       := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
            //
            //
            case Evento of
              CO_COD_evexxe110110CCe: // = 110110;
              begin
                infCCe_verAplic       := infEvento_verAplic;
                infCCe_cOrgao         := Geral.IMV(infEvento_cOrgao);
                infCCe_tpAmb          := Geral.IMV(infEvento_tpAmb);
                infCCe_CNPJ           := infEvento_CNPJ;
                infCCe_CPF            := infEvento_CPF;
                infCCe_chCTe          := infEvento_chCTe;
                infCCe_dhEvento       := infEvento_dhEvento;
                infCCe_dhEventoTZD    := dhEventoTZD;
                infCCe_tpEvento       := Geral.IMV(infEvento_tpEvento);
                infCCe_nSeqEvento     := Geral.IMV(infEvento_nSeqEvento);
                infCCe_verEvento      := Geral.DMV_Dot(eventoCTe_evento);
                infCCe_CCeConta       := CTeEveRCCeConta;
                infCCe_cStat          := Geral.IMV(_Stat);
                infCCe_dhRegEvento    := infEvento_dhRegEvento;
                //infCCe_dhRegEventoTZD := dhRegEventoTZD;
                infCCe_nProt          := infEvento_nProt;

                if DmCTe_0000.LocalizaCTeInfoEveCCe(infEvento_ChCTe, infCCe_nSeqEvento,
                  _CNPJ, _CPF, _dhEvento, _dhEventoTZD, _verEvento,
                  nCondUso, CTeEveRCCeConta) then
                begin
                  infCCe_CCeConta       := CTeEveRCCeConta;
                  infCCe_nCondUso       := nCondUso;
                  //
                  //XXe_PF.Ajusta_dh_XXe_UTC(infCCe_dhEvento, infCCe_dhEventoTZD);
                  XXe_PF.Ajusta_dh_XXe_UTC(infCCe_dhRegEvento, infCCe_dhRegEventoTZD);
                  //
                  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctecaba', False, [
                  'infCCe_verAplic', 'infCCe_cOrgao',
                  'infCCe_tpAmb', 'infCCe_CNPJ', 'infCCe_CPF', 'infCCe_chCTe',
                  'infCCe_dhEvento', 'infCCe_dhEventoTZD', 'infCCe_tpEvento',
                  'infCCe_nSeqEvento', 'infCCe_verEvento', 'infCCe_CCeConta',
                  'infCCe_cStat', 'infCCe_dhRegEvento', 'infCCe_dhRegEventoTZD',
                  'infCCe_nProt', 'infCCe_nCondUso'], [
                  'ID'], [
                  infCCe_verAplic, infCCe_cOrgao,
                  infCCe_tpAmb, infCCe_CNPJ, infCCe_CPF, infCCe_chCTe,
                  infCCe_dhEvento, infCCe_dhEventoTZD, infCCe_tpEvento,
                  infCCe_nSeqEvento, infCCe_verEvento, infCCe_CCeConta,
                  infCCe_cStat, infCCe_dhRegEvento, infCCe_dhRegEventoTZD,
                  infCCe_nProt, infCCe_nCondUso], [
                  infEvento_chCTe], True);
                end;
              end;
              CO_COD_evexxe110111Can: // = 110111;
              begin
                infEvento_cStat := cStat;
                //
                XXe_PF.Ajusta_dh_XXe_UTC(infEvento_dhRegEvento, infCanc_dhRecbtoTZD);
                //
                // cJust -> Nao informa no retorno!!!
                // xMotivo vem como carta > "Evento registrado e vinculado a CT-e"
                infEvento_xMotivo  := 'Cancelamento de CT-e homologado';
                infCanc_dhEvento    := infEvento_dhEvento;
                infCanc_dhRegEvento := infEvento_dhRegEvento;
                //
(*
<detEvento versaoEvento="2.00">
  <evCancCTe>
    <descEvento>Cancelamento</descEvento>
    <nProt>141150000218490</nProt>
    <xJust>TESTE DE CANCELAMENTO</xJust>
  </evCancCTe>
</detEvento>
*)
  //             acertar valores das variaveis
                UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctecaba', False, [
                  'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
                  'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
                  'infCanc_cStat', 'infCanc_xMotivo', (*'infCanc_cJust',*)
                  'infCanc_xJust', 'infCanc_dhRecbtoTZD',
                  'infCanc_dhEvento', 'infCanc_dhRegEvento'
                ], ['ID'], [
                  infEvento_Id, infEvento_tpAmb, infEvento_verAplic,
                  dhRecbto, infCanc_nProt, digVal,
                  infEvento_cStat, infEvento_xMotivo, (*cJust,*)
                  infCanc_xJust, infProt_dhRecbtoTZD,
                  infCanc_dhEvento, infCanc_dhRegEvento
                ], [infEvento_chCTe], True);
              end;
              else Geral.MB_Erro('Evento: ' + infEvento_tpEvento +
              ' n�o implementado em "TFmCTeSteps_0310.LerTextoConsultaCTe()"');
            end;
          end else
            Geral.MB_Erro('C�digo de retorno de evento ' + _Stat + ' - ' +
            CTe_PF.Texto_StatusCTe(Geral.IMV(_Stat), 0) + sLineBreak +
            'N�o esperado para c�digo de retorno de consulta ' + infEvento_cStat +
            CTe_PF.Texto_StatusCTe(Geral.IMV(infEvento_cStat), 0));
        end;
      end;
      Lista.Remove(Lista.Item[0]);
    end;
  end;
end;

procedure TFmCTeSteps_0200a.MostraTextoRetorno(Texto: String);
begin
  RETxtRetorno.Text := Texto;
end;

function TFmCTeSteps_0200a.ObtemDescricaoDaTag(Tag: TTipoTagXML): String;
begin
  case Tag of
    ttx_Id          : Result := 'ID'                    ;
    ttx_idLote      : Result := 'ID do lote'            ;
    ttx_versao      : Result := 'Vers�o XML'            ;
    ttx_tpAmb       : Result := 'Ambiente'              ;
    ttx_verAplic    : Result := 'Vers�o do aplicativo'  ;
    ttx_cOrgao      : Result := 'Org�o'                 ;
    ttx_cStat       : Result := 'Status'                ;
    ttx_xMotivo     : Result := 'Motivo'                ;
    ttx_cUF         : Result := 'UF'                    ;
    ttx_dhRecbto    : Result := 'Data/hora recibo'      ;
    ttx_chCTe       : Result := 'Chave CT-e'            ;
    ttx_nProt       : Result := 'Protocolo'             ;
    ttx_digVal      : Result := 'Valor "digest"'        ;
    ttx_ano         : Result := 'Ano'                   ;
    ttx_CNPJ        : Result := 'CNPJ'                  ;
    ttx_mod         : Result := 'Modelo CT'             ;
    ttx_serie       : Result := 'S�rie CT'              ;
    ttx_nCTIni      : Result := 'N�mero inicial CT'     ;
    ttx_nCTFin      : Result := 'N�mero final CT'       ;
    ttx_nRec        : Result := 'N�mero do Recibo'      ;
    ttx_tMed        : Result := 'Tempo m�dio'           ;
    ttx_tpEvento    : Result := 'Tipo de evento'        ;
    ttx_xEvento     : Result := 'Descri��o do evento'   ;
    ttx_CNPJDest    : Result := 'CNPJ do destinat�rio'  ;
    ttx_CPFDest     : Result := 'CPF do destinat�rio'   ;
    ttx_emailDest   : Result := 'E-mail do destinat�rio';
    ttx_nSeqEvento  : Result := 'Sequencial do evento'  ;
    ttx_dhRegEvento : Result := 'Data/hora registro'    ;
    ttx_dhResp      : Result := 'Data/hora msg resposta';
    ttx_indCont     : Result := 'Indicador continua��o' ;
    ttx_ultNSU      : Result := '�ltima NSU pesquisada' ;
    ttx_maxNSU      : Result := 'NSU M�xima encontrada' ;
    ttx_NSU         : Result := 'NSU do docum. fiscal'  ;
    ttx_CPF         : Result := 'CPF'                   ;
    ttx_xNome       : Result := 'Raz�o Social ou Nome'  ;
    ttx_IE          : Result := 'Inscri��o Estadual'    ;
    ttx_dEmi        : Result := 'Data da emiss�o'       ;
    ttx_tpCT        : Result := 'Tipo de opera��o CT-e' ;
    ttx_vCT         : Result := 'Valor total da CT-e'   ;
    ttx_cSitCTe     : Result := 'Situa��o da CT-e'      ;
    ttx_cSitConf    : Result := 'Sit. Manifes. Destinat';
    ttx_dhEvento    : Result := 'Data/hora do evento'   ;
    ttx_descEvento  : Result := 'Evento'                ;
    //ttx_x Correcao   : Result := 'x Correcao'             ;
    ttx_cJust       : Result := 'C�digo da justifica��o';
    ttx_xJust       : Result := 'Texto da justifica��o' ;
    ttx_verEvento   : Result := 'Vers�o do evento'      ;
    ttx_schema      : Result := 'Schema XML'            ;
    ttx_docZip      : Result := 'Resumo do documento'   ;
    ttx_dhEmi       : Result := 'Data / hora de emiss�o';
    //
    ttx_grupoAlterado   : Result := 'Grupo alterado';
    ttx_campoAlterado   : Result := 'Campo alterado';
    ttx_valorAlterado   : Result := 'Valor alterado';
    ttx_nroItemAlterado : Result := 'N� do item alterado';
    ttx_versaoEvento    : Result := 'Vers�o do evento';
    //
    else              Result := '? ? ? ? ?';
  end;
end;

function TFmCTeSteps_0200a.ObtemNomeDaTag(Tag: TTipoTagXML): String;
begin
  case Tag of
    ttx_Id          : Result := 'Id'                ;
    ttx_idLote      : Result := 'idLote'            ;
    ttx_versao      : Result := 'versao'            ;
    ttx_tpAmb       : Result := 'tpAmb'             ;
    ttx_verAplic    : Result := 'verAplic'          ;
    ttx_cOrgao      : Result := 'cOrgao'            ;
    ttx_cStat       : Result := 'cStat'             ;
    ttx_xMotivo     : Result := 'xMotivo'           ;
    ttx_cUF         : Result := 'cUF'               ;
    ttx_dhRecbto    : Result := 'dhRecbto'          ;
    ttx_chCTe       : Result := 'chCTe'             ;
    ttx_nProt       : Result := 'nProt'             ;
    ttx_digVal      : Result := 'digVal'            ;
    ttx_ano         : Result := 'ano'               ;
    ttx_CNPJ        : Result := 'CNPJ'              ;
    ttx_mod         : Result := 'mod'               ;
    ttx_serie       : Result := 'serie'             ;
    ttx_nCTIni      : Result := 'nCTIni'            ;
    ttx_nCTFin      : Result := 'nCTFin'            ;
    ttx_nRec        : Result := 'nRec'              ;
    ttx_tMed        : Result := 'tMed'              ;
    ttx_tpEvento    : Result := 'tpEvento'          ;
    ttx_xEvento     : Result := 'xEvento'           ;
    ttx_CNPJDest    : Result := 'CNPJDest'          ;
    ttx_CPFDest     : Result := 'CPFDest'           ;
    ttx_emailDest   : Result := 'emailDest'         ;
    ttx_nSeqEvento  : Result := 'nSeqEvento'        ;
    ttx_dhRegEvento : Result := 'dhRegEvento'       ;
    ttx_dhResp      : Result := 'dhResp'            ;
    ttx_indCont     : Result := 'indCont'           ;
    ttx_ultNSU      : Result := 'ultNSU'            ;
    ttx_maxNSU      : Result := 'maxNSU'            ;
    ttx_NSU         : Result := 'NSU'               ;
    ttx_CPF         : Result := 'CPF'               ;
    ttx_xNome       : Result := 'xNome'             ;
    ttx_IE          : Result := 'IE'                ;
    ttx_dEmi        : Result := 'dEmi'              ;
    ttx_tpCT        : Result := 'tpCT'              ;
    ttx_vCT         : Result := 'vCT'               ;
    ttx_cSitCTe     : Result := 'cSitCTe'           ;
    ttx_cSitConf    : Result := 'cSitConf'          ;
    ttx_dhEvento    : Result := 'dhEvento'          ;
    ttx_descEvento  : Result := 'descEvento'        ;
    //ttx_x Correcao   : Result := 'x Correcao'         ;
    ttx_cJust       : Result := 'cJust'             ;
    ttx_xJust       : Result := 'xJust'             ;
    ttx_verEvento   : Result := 'verEvento'         ;
    ttx_schema      : Result := 'schema'            ;
    ttx_docZip      : Result := 'docZip'            ;
    ttx_dhEmi       : Result := 'dhEmi'             ;
    //
    ttx_grupoAlterado   : Result := 'grupoAlterado';
    ttx_campoAlterado   : Result := 'campoAlterado';
    ttx_valorAlterado   : Result := 'valorAlterado';
    ttx_nroItemAlterado : Result := 'nroItemAlterado';
    ttx_versaoEvento    : Result := 'versaoEvento';
    //
    else           Result :=      '???';
  end;
end;

procedure TFmCTeSteps_0200a.PreparaCancelamentoDeCTe(Empresa, Serie,
  nCT, nSeqEvento, Justif: Integer; Modelo, nProtCan, chCTe, XML_Evento: String);
var
  LoteStr, cUF, Id: String;
begin
  ReopenCTeJust(QrCTeJustCan, 1);
  EdEmpresa.ValueVariant       := Empresa;
  EdModeloCan.ValueVariant     := Modelo;
  EdSerieCan.ValueVariant      := Serie;
  EdnCTCan.ValueVariant        := nCT;
  EdnProtCan.ValueVariant      := nProtCan;
  EdchCTeCan.ValueVariant      := chCTe;
  EdnSeqEventoCan.ValueVariant := nSeqEvento;
  EdCTeJustCan.ValueVariant    := Justif;
  CBCTeJustCan.KeyValue        := Justif;
  FXML_Evento                  := XML_Evento;
  //
  VerificaCertificadoDigital(Empresa);
  //
  HabilitaBotoes();
end;

procedure TFmCTeSteps_0200a.PreparaCartaDeCorrecao(Empresa, Serie, nCT,
  nSeqEvento: Integer; Modelo, nProt, ChCte, XML_Evento: String);
begin
  EdEmpresa.ValueVariant       := Empresa;
  EdModeloCCe.ValueVariant     := Modelo;
  EdSerieCCe.ValueVariant      := Serie;
  EdnCTCCe.ValueVariant        := nCT;
  EdnProtCCe.ValueVariant      := nProt;
  EdchCTeCCe.ValueVariant      := chCTe;
  EdnSeqEventoCCe.ValueVariant := nSeqEvento;
  FXML_Evento                  := XML_Evento;
  //
  VerificaCertificadoDigital(Empresa);
  //
  HabilitaBotoes();
end;

procedure TFmCTeSteps_0200a.PreparaConsultaCTe(Empresa, IDCtrl: Integer;
  ChaveCTe: String);
begin
  EdEmpresa.ValueVariant   := Empresa;
  EdChaveCTeCTe.Text       := ChaveCTe;
  EdIDCtrlCTe.ValueVariant := IDCtrl;
  //
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmCTeSteps_0200a.PreparaConsultaLote(Lote, Empresa: Integer;
  Recibo: String);
var
  LoteStr: String;
begin
  EdLote.ValueVariant      := Lote;
  EdEmpresa.ValueVariant   := Empresa;
  EdRecibo.ValueVariant    := Recibo;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  if CkSoLer.Checked then
  begin
    LoteStr := DmCTe_0000.FormataLoteCTe(Lote);
    if AbreArquivoXML(LoteStr, CTE_EXT_PRO_REC_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Opt_Sel);
  end else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

function TFmCTeSteps_0200a.PreparaEnvioDeLoteCTe(Lote, Empresa: Integer;
  Sincronia: TXXeIndSinc): Boolean;
var
  LoteStr: String;
  Continua: Boolean;
begin
  FPathLoteCTe := '';
  EdLote.ValueVariant    := Lote;
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  if not CkSoLer.Checked then
  begin
    Continua := FmCTeGeraXML_0200a.GerarLoteCTeNovo(Lote, Empresa, FPathLoteCTe,
      FXML_LoteCTe, LaAviso1, LaAviso2, Sincronia);
    if Continua then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      CO_Texto_Env_Sel);
    end else MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Envio cancelado!');
  end else
  begin
    LoteStr := FormatFloat('000000000', Lote);
    if AbreArquivoXML(LoteStr, CTE_EXT_REC_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      CO_Texto_Opt_Sel);
  end;
  HabilitaBotoes();
  Result := True;
end;

procedure TFmCTeSteps_0200a.PreparaEPEC(Empresa, Serie, nCT, nSeqEvento,
  Justif: Integer; Modelo, chCTe, XML_Evento: String);
var
  LoteStr, cUF, Id: String;
begin
  ReopenCTeJust(QrCTeJustEPEC, 4);
  EdEmpresa.ValueVariant       := Empresa;
(*
  EdModeloEPEC.ValueVariant     := Modelo;
  EdSerieEPEC.ValueVariant      := Serie;
  EdnCTEPEC.ValueVariant        := nCT;
*)
  EdchCTeEPEC.ValueVariant      := chCTe;
(*
  EdnSeqEventoEPEC.ValueVariant := nSeqEvento;
  EdCTeJustEPEC.ValueVariant    := Justif;
  CBCTeJustEPEC.KeyValue        := Justif;
*)
  FXML_Evento                  := XML_Evento;
  //
  VerificaCertificadoDigital(Empresa);
  //
  HabilitaBotoes();
end;

procedure TFmCTeSteps_0200a.PreparaInutilizaNumerosCTe(Empresa, Lote, Ano,
  Modelo, Serie, nCTIni, nCTFim, Justif: Integer);
var
  LoteStr, cUF, Id: String;
begin
  ReopenCTeJust(QrCTeJustInu, 2);
  EdEmpresa.ValueVariant      := Empresa;
  EdLote.ValueVariant         := Lote;
  EdAno.ValueVariant          := Ano;
  EdModeloInu.ValueVariant    := Modelo;
  EdSerieInu.ValueVariant     := Serie;
  EdnCTIni.ValueVariant       := nCTIni;
  EdnCTFim.ValueVariant       := nCTFim;
  EdCTeJustInu.ValueVariant   := Justif;
  CBCTeJustInu.KeyValue       := Justif;
  //
  PnJustificativa.Enabled  := False;
  //
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  if CkSoLer.Checked then
  begin
    cUF := IntToStr(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(DmCTe_0000.QrEmpresaNO_UF.Value));
    DmCTe_0000.MontaID_Inutilizacao(cUF, EdAno.Text, EdEmitCNPJ.Text,
      FormatFloat('00', EdModeloInu.ValueVariant),
      FormatFloat('00', EdSerieInu.ValueVariant),
      FormatFloat('000000000', nCTIni),
      FormatFloat('000000000', nCTFim), Id);
    LoteStr := Id + '_' + FormatFloat('000000000', Lote);
    //
    if AbreArquivoXML(LoteStr, CTE_EXT_INU_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Opt_Sel);
  end else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Configure o modo de solicita��o e clique em "OK"!');
  HabilitaBotoes();
end;

procedure TFmCTeSteps_0200a.PreparaStepGenerico(Empresa: Integer);
begin
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmCTeSteps_0200a.PreparaVerificacaoStatusServico(
  Empresa: Integer);
begin
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmCTeSteps_0200a.ReopenCTeJust(QrCTeJust: TmySQLQuery; Aplicacao: Byte);
begin
  QrCTeJust.Close;
  QrCTeJust.Params[0].AsInteger := Aplicacao;
  QrCTeJust.Open;
  //
end;

procedure TFmCTeSteps_0200a.RETxtEnvioChange(Sender: TObject);
begin
  MyObjects.LoadXML(RETxtEnvio, WBEnvio, PageControl1, 1);
end;

procedure TFmCTeSteps_0200a.RETxtRetornoChange(Sender: TObject);
begin
  MyObjects.LoadXML(RETxtRetorno, WBResposta, PageControl1, 3);
end;

procedure TFmCTeSteps_0200a.RGAcaoClick(Sender: TObject);
begin
  PCAcao.ActivePageIndex := RGAcao.ItemIndex;
end;

function TFmCTeSteps_0200a.TextoArqDefinido(Texto: String): Boolean;
begin
  Result := Texto <> '';
  if not Result then
    Geral.MB_Erro('Texto XML n�o definido!');
end;

procedure TFmCTeSteps_0200a.Timer1Timer(Sender: TObject);
begin
  FSegundos := FSegundos + 1;
  //LaWait.Visible := True;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Aguarde o tempo m�nimo de resposta ' +
    FormatFloat('0', FSegundos) + ' de ' + FormatFloat('0', FSecWait));
  //
  if LaAviso1.Font.Color = clGreen then
  begin
    LaAviso1.Font.Color := clBlue;
    LaAviso2.Font.Color := clBlue;
  end else
  begin
    LaAviso1.Font.Color := clGreen;
    LaAviso2.Font.Color := clGreen;
  end;
  //
  if FSegundos = FSecWait then
    Close;
end;

procedure TFmCTeSteps_0200a.VerificaCertificadoDigital(Empresa: Integer);
var
  US: String;
begin
  DmCTe_0000.ReopenEmpresa(Empresa);
  EdEmitCNPJ.Text          := DmCTe_0000.QrEmpresaCNPJ.Value;
  CBUF.Text                := DmCTe_0000.QrFilialCTeUF_WebServ.Value;
////////////////////////////////////////////////////////////////////////////////
  Geral.MB_Info('Ver o que fazer: **TpEmis**! [2]');
  case TCTeTpEmis(DmCTe_0000.QrFilialCTetpEmis.Value) of
    (*1*)ctetpemisNormal    :       US := DmCTe_0000.QrFilialCTeUF_Servico.Value;
    //ctetpemis2=2, ctetpemis3=3,
    (*4*)ctetpemisEPEC_SVC:         US := DmCTe_0000.QrFilialCTeUF_EPEC.Value;
    (*5*)ctetpemisContingenciaFSDA: US := DmCTe_0000.QrFilialCTeUF_Servico.Value;
    //ctetpemis6=6,
    ctetpemisSVC_RS:                US := DmCTe_0000.QrFilialCTeUF_Servico.Value;
    ctetpemisSVC_SP:                US := DmCTe_0000.QrFilialCTeUF_Servico.Value;
    else
    begin
      Geral.MB_Erro('Tipo de emiss�o n�o implementada para UF servi�o!');
      US := DmCTe_0000.QrFilialCTeUF_Servico.Value;
    end;
  end;
  EdUF_Servico.Text := US;
////////////////////////////////////////////////////////////////////////////////
  EdSerialNumber.Text := DmCTe_0000.QrFilialCTeSerNum.Value;
  LaExpiraCertDigital.Caption := '';
  if DmCTe_0000.QrFilialCTeSerVal.Value < 2 then
    LaExpiraCertDigital.Caption :=
    'N�o h� data de validade cadastrada para seu certificado digital!'
  else
  if DmCTe_0000.QrFilialCTeSerVal.Value < Int(Date) then
    LaExpiraCertDigital.Caption :=
    'Seu certificado digital expirou!'
  else
  if DmCTe_0000.QrFilialCTeSerVal.Value <= (Int(Date) +
    DmCTe_0000.QrFilialCTeSerAvi.Value) then
    LaExpiraCertDigital.Caption :=
    'Seu certificado digital expira em ' + FormatFloat('0.000',
    DmCTe_0000.QrFilialCTeSerVal.Value - Now + 1) + ' dias!';
  LaExpiraCertDigital.Visible := LaExpiraCertDigital.Caption <> '';
end;

procedure TFmCTeSteps_0200a.VerificaStatusServico();
var
  CodigoUF_Int: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    CodigoUF_Int  := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(CBUF.Text);
    //
    FTextoArq := FmCTeGeraXML_0200a.WS_CTeStatusServico(EdUF_Servico.Text,
      RGAmbiente.ItemIndex, CodigoUF_Int, EdSerialNumber.Text,
      LaAviso1, LaAviso2, RETxtEnvio, EdWebService);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com sucesso!');
    MostraTextoRetorno(FTextoArq);
    LerTextoStatusServico();
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
