unit CTeConsStatServ_0200a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, Vcl.OleCtrls,
  OmniXML, OmniXMLUtils,
  UnDmkProcFunc,
  SHDocVw;

type
  TTipoTagXML = (
     ttx_Id                ,
     ttx_idLote            ,
     ttx_versao            ,
     ttx_tpAmb             ,
     ttx_verAplic          ,
     ttx_cOrgao            ,
     ttx_cStat             ,
     ttx_xMotivo           ,
     ttx_cUF               ,
     ttx_dhRecbto          ,
     ttx_chNFe             ,
     ttx_nProt             ,
     ttx_digVal            ,
     ttx_ano               ,
     ttx_CNPJ              ,
     ttx_mod               ,
     ttx_serie             ,
     ttx_nNFIni            ,
     ttx_nNFFin            ,
     ttx_nRec              ,
     ttx_tMed              ,
     ttx_tpEvento          ,
     ttx_xEvento           ,
     ttx_CNPJDest          ,
     ttx_CPFDest           ,
     ttx_emailDest         ,
     ttx_nSeqEvento        ,
     ttx_dhRegEvento       ,
     ttx_dhResp            ,
     ttx_indCont           ,
     ttx_ultNSU            ,
     ttx_maxNSU            ,
     ttx_NSU               ,
     ttx_CPF               ,
     ttx_xNome             ,
     ttx_IE                ,
     ttx_dEmi              ,
     ttx_tpNF              ,
     ttx_vNF               ,
     ttx_cSitNFe           ,
     ttx_cSitConf          ,
     ttx_dhEvento          ,
     ttx_descEvento        ,
     ttx_xCorrecao         ,
     ttx_cJust             ,
     ttx_xJust             ,
     ttx_verEvento         ,
     ttx_schema            ,
     ttx_docZip            ,
     ttx_dhEmi             ,
     //
     ttx_
     );

  TFmCTeConsStatServ_0200a = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    PnConfirma: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PnConfig1: TPanel;
    Panel6: TPanel;
    Label5: TLabel;
    Label2: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    EdSerialNumber: TdmkEdit;
    EdUF_Servico: TdmkEdit;
    CBUF: TComboBox;
    EdEmpresa: TdmkEdit;
    Panel7: TPanel;
    RGAmbiente: TRadioGroup;
    Panel11: TPanel;
    Label19: TLabel;
    EdWebService: TEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    RETxtEnvio: TMemo;
    TabSheet5: TTabSheet;
    WBEnvio: TWebBrowser;
    TabSheet2: TTabSheet;
    RETxtRetorno: TMemo;
    TabSheet3: TTabSheet;
    WBResposta: TWebBrowser;
    TabSheet4: TTabSheet;
    MeInfo: TMemo;
    TabSheet6: TTabSheet;
    MeChaves: TMemo;
    Panel1: TPanel;
    EdEmitCNPJ: TdmkEdit;
    Label14: TLabel;
    Label20: TLabel;
    EdVersaoAcao: TdmkEdit;
    RGAcao: TRadioGroup;
    LaExpiraCertDigital: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure RETxtEnvioChange(Sender: TObject);
    procedure RETxtRetornoChange(Sender: TObject);
  private
    { Private declarations }
    xmlDoc: IXMLDocument;
    xmlNode(*, xmlSub, xmlChild1*): IXMLNode;
    //
    function  DefineXMLDoc(): Boolean;
    procedure HabilitaBotoes(Visivel: Boolean = True);
    function  LeNoXML(No: IXMLNode; Tipo: TTipoNoXML; Tag: TTipoTagXML; AvisaVersao: Boolean = True): String;
    procedure MostraTextoRetorno(Texto: String);
    function  ObtemNomeDaTag(Tag: TTipoTagXML): String;
    function  ObtemDescricaoDaTag(Tag: TTipoTagXML): String;
    procedure LerTextoStatusServico();
    function  TextoArqDefinido(Texto: String): Boolean;
    procedure VerificaCertificadoDigital(Empresa: Integer);

  public
    { Public declarations }
    FTextoArq: String;
    //
    procedure PreparaVerificacaoStatusServico(Empresa: Integer);
  end;

  var
  FmCTeConsStatServ_0200a: TFmCTeConsStatServ_0200a;

var
  FverXML_versao: String;

implementation

uses UnMyObjects, Module, CTeGeraXML_0200a, UnXXe_PF, ModuleCTe_0000;

{$R *.DFM}

const
  FXML_Load_Failure = 'Falha ao carregar o XML!';
  CO_Texto_Opt_Sel = 'Clique em "OK" para ler arquivo selecionado, ou em "Abrir" para selecionar um arquivo!';
  CO_Texto_Clk_Sel = 'Configure a forma de consulta e clique em "OK"!';
  CO_Texto_Env_Sel = 'Configure a forma de envio e clique em "OK" para enviar o lote ao fisco!';

procedure TFmCTeConsStatServ_0200a.BtOKClick(Sender: TObject);
var
  CodigoUF_Int: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    CodigoUF_Int  := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(CBUF.Text);
    //
    FTextoArq := FmCTeGeraXML_0200a.WS_CTeStatusServico(EdUF_Servico.Text,
      RGAmbiente.ItemIndex, CodigoUF_Int, EdSerialNumber.Text,
      LaAviso1, LaAviso2, RETxtEnvio, EdWebService);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com sucesso!');
    MostraTextoRetorno(FTextoArq);
    LerTextoStatusServico();
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCTeConsStatServ_0200a.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmCTeConsStatServ_0200a.DefineXMLDoc(): Boolean;
begin
  xmlDoc := TXMLDocument.Create;
  if not XMLLoadFromAnsiString(xmlDoc, FTextoArq) then
  begin
    Result := False;
    dmkPF.LeTexto_Permanente(FTextoArq, FXML_Load_Failure);
  end else Result := True;
end;

procedure TFmCTeConsStatServ_0200a.EdEmpresaChange(Sender: TObject);
begin
  DmCTe_0000.ReopenOpcoesCTe(EdEmpresa.ValueVariant, True);
(*Parei Aqui!!!
  RGAmbiente.ItemIndex := DmCTe_0000.QrOpcoesCTeide_tpAmb.Value;
*)
  Geral.MB_Info('Ambiente forcado em Homologacao!');
  RGAmbiente.ItemIndex := 2;
end;

procedure TFmCTeConsStatServ_0200a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCTeConsStatServ_0200a.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCTeConsStatServ_0200a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCTeConsStatServ_0200a.HabilitaBotoes(Visivel: Boolean);
begin
  PnConfirma.Visible := Visivel;
end;

function TFmCTeConsStatServ_0200a.LeNoXML(No: IXMLNode; Tipo: TTipoNoXML;
  Tag: TTipoTagXML; AvisaVersao: Boolean): String;
var
  Texto: String;
begin
  Result := '';
  case Tipo of
    tnxTextStr: Result := GetNodeTextStr(No, ObtemNomeDaTag(Tag), '');
    tnxAttrStr: Result := GetNodeAttrStr(No, ObtemNomeDaTag(Tag), '');
    else Result := '???' + ObtemNomeDaTag(Tag) + '???';
  end;
  //
  if (Tag = ttx_Versao) and (Result <> FverXML_versao) then
    if AvisaVersao then
      Geral.MB_Aviso('Vers�o do XML difere do esperado!' +
      sLineBreak + 'Vers�o informada: ' + Result + sLineBreak +
      'Verifique a vers�o do servi�o no cadastro das op��es da filial para a vers�o: '
      + Result);
  if (Tag = ttx_dhRecbto) then XXe_PF.Ajusta_dh_NFe(Result);
  if Result <> '' then
  begin
    case Tag of
      ttx_tpAmb: Texto := Result + ' - ' + XXe_PF.ObtemNomeAmbiente(Result);
      ttx_tMed : Texto := Result + ' segundos';
      ttx_cUF  : Texto := Result + ' - ' +
                 Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(StrToInt(Result));
      else Texto := Result;
    end;
  end;
  //
  MeInfo.Lines.Add(ObtemDescricaoDaTag(Tag) + ' = ' + Texto);
end;

procedure TFmCTeConsStatServ_0200a.LerTextoStatusServico();
begin
{ Parei Aqui! Fazer!!!
  FverXML_versao := Geral.FFT_Dot(DmodG.QrParamsEmpNFeVerStaSer.Value, 2, siNegativo);
}
  Geral.MB_Info('Versao Status Servico forcado em 2.00!');
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � status de servi�o
    xmlNode := xmlDoc.SelectSingleNode('/retConsStatServ');
    if assigned(xmlNode) then
    begin
      LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
      LeNoXML(xmlNode, tnxTextStr, ttx_tMed);
      //
      Pagecontrol1.ActivePageIndex := 4;
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [4]');
  end;
end;

procedure TFmCTeConsStatServ_0200a.MostraTextoRetorno(Texto: String);
begin
  RETxtRetorno.Text := Texto;
end;

function TFmCTeConsStatServ_0200a.ObtemDescricaoDaTag(Tag: TTipoTagXML): String;
begin
  case Tag of
    ttx_Id          : Result := 'ID'                    ;
    ttx_idLote      : Result := 'ID do lote'            ;
    ttx_versao      : Result := 'Vers�o XML'            ;
    ttx_tpAmb       : Result := 'Ambiente'              ;
    ttx_verAplic    : Result := 'Vers�o do aplicativo'  ;
    ttx_cOrgao      : Result := 'Org�o'                 ;
    ttx_cStat       : Result := 'Status'                ;
    ttx_xMotivo     : Result := 'Motivo'                ;
    ttx_cUF         : Result := 'UF'                    ;
    ttx_dhRecbto    : Result := 'Data/hora recibo'      ;
    ttx_chNFe       : Result := 'Chave NF-e'            ;
    ttx_nProt       : Result := 'Protocolo'             ;
    ttx_digVal      : Result := 'Valor "digest"'        ;
    ttx_ano         : Result := 'Ano'                   ;
    ttx_CNPJ        : Result := 'CNPJ'                  ;
    ttx_mod         : Result := 'Modelo NF'             ;
    ttx_serie       : Result := 'S�rie NF'              ;
    ttx_nNFIni      : Result := 'N�mero inicial NF'     ;
    ttx_nNFFin      : Result := 'N�mero final NF'       ;
    ttx_nRec        : Result := 'N�mero do Recibo'      ;
    ttx_tMed        : Result := 'Tempo m�dio'           ;
    ttx_tpEvento    : Result := 'Tipo de evento'        ;
    ttx_xEvento     : Result := 'Descri��o do evento'   ;
    ttx_CNPJDest    : Result := 'CNPJ do destinat�rio'  ;
    ttx_CPFDest     : Result := 'CPF do destinat�rio'   ;
    ttx_emailDest   : Result := 'E-mail do destinat�rio';
    ttx_nSeqEvento  : Result := 'Sequencial do evento'  ;
    ttx_dhRegEvento : Result := 'Data/hora registro'    ;
    ttx_dhResp      : Result := 'Data/hora msg resposta';
    ttx_indCont     : Result := 'Indicador continua��o' ;
    ttx_ultNSU      : Result := '�ltima NSU pesquisada' ;
    ttx_maxNSU      : Result := 'NSU M�xima encontrada' ;
    ttx_NSU         : Result := 'NSU do docum. fiscal'  ;
    ttx_CPF         : Result := 'CPF'                   ;
    ttx_xNome       : Result := 'Raz�o Social ou Nome'  ;
    ttx_IE          : Result := 'Inscri��o Estadual'    ;
    ttx_dEmi        : Result := 'Data da emiss�o'       ;
    ttx_tpNF        : Result := 'Tipo de opera��o NF-e' ;
    ttx_vNF         : Result := 'Valor total da NF-e'   ;
    ttx_cSitNFe     : Result := 'Situa��o da NF-e'      ;
    ttx_cSitConf    : Result := 'Sit. Manifes. Destinat';
    ttx_dhEvento    : Result := 'Data/hora do evento'   ;
    ttx_descEvento  : Result := 'Evento'                ;
    ttx_xCorrecao   : Result := 'xCorrecao'             ;
    ttx_cJust       : Result := 'C�digo da justifica��o';
    ttx_xJust       : Result := 'Texto da justifica��o' ;
    ttx_verEvento   : Result := 'Vers�o do evento'      ;
    ttx_schema      : Result := 'Schema XML'            ;
    ttx_docZip      : Result := 'Resumo do documento'   ;
    ttx_dhEmi       : Result := 'Data / hora de emiss�o';

    //
    else              Result := '? ? ? ? ?';
  end;
end;

function TFmCTeConsStatServ_0200a.ObtemNomeDaTag(Tag: TTipoTagXML): String;
begin
  case Tag of
    ttx_Id          : Result := 'Id'                ;
    ttx_idLote      : Result := 'idLote'            ;
    ttx_versao      : Result := 'versao'            ;
    ttx_tpAmb       : Result := 'tpAmb'             ;
    ttx_verAplic    : Result := 'verAplic'          ;
    ttx_cOrgao      : Result := 'cOrgao'            ;
    ttx_cStat       : Result := 'cStat'             ;
    ttx_xMotivo     : Result := 'xMotivo'           ;
    ttx_cUF         : Result := 'cUF'               ;
    ttx_dhRecbto    : Result := 'dhRecbto'          ;
    ttx_chNFe       : Result := 'chNFe'             ;
    ttx_nProt       : Result := 'nProt'             ;
    ttx_digVal      : Result := 'digVal'            ;
    ttx_ano         : Result := 'ano'               ;
    ttx_CNPJ        : Result := 'CNPJ'              ;
    ttx_mod         : Result := 'mod'               ;
    ttx_serie       : Result := 'serie'             ;
    ttx_nNFIni      : Result := 'nNFIni'            ;
    ttx_nNFFin      : Result := 'nNFFin'            ;
    ttx_nRec        : Result := 'nRec'              ;
    ttx_tMed        : Result := 'tMed'              ;
    ttx_tpEvento    : Result := 'tpEvento'          ;
    ttx_xEvento     : Result := 'xEvento'           ;
    ttx_CNPJDest    : Result := 'CNPJDest'          ;
    ttx_CPFDest     : Result := 'CPFDest'           ;
    ttx_emailDest   : Result := 'emailDest'         ;
    ttx_nSeqEvento  : Result := 'nSeqEvento'        ;
    ttx_dhRegEvento : Result := 'dhRegEvento'       ;
    ttx_dhResp      : Result := 'dhResp'            ;
    ttx_indCont     : Result := 'indCont'           ;
    ttx_ultNSU      : Result := 'ultNSU'            ;
    ttx_maxNSU      : Result := 'maxNSU'            ;
    ttx_NSU         : Result := 'NSU'               ;
    ttx_CPF         : Result := 'CPF'               ;
    ttx_xNome       : Result := 'xNome'             ;
    ttx_IE          : Result := 'IE'                ;
    ttx_dEmi        : Result := 'dEmi'              ;
    ttx_tpNF        : Result := 'tpNF'              ;
    ttx_vNF         : Result := 'vNF'               ;
    ttx_cSitNFe     : Result := 'cSitNFe'           ;
    ttx_cSitConf    : Result := 'cSitConf'          ;
    ttx_dhEvento    : Result := 'dhEvento'          ;
    ttx_descEvento  : Result := 'descEvento'        ;
    ttx_xCorrecao   : Result := 'xCorrecao'         ;
    ttx_cJust       : Result := 'cJust'             ;
    ttx_xJust       : Result := 'xJust'             ;
    ttx_verEvento   : Result := 'verEvento'         ;
    ttx_schema      : Result := 'schema'            ;
    ttx_docZip      : Result := 'docZip'            ;
    ttx_dhEmi       : Result := 'dhEmi'             ;
    //
    else           Result :=      '???';
  end;
end;

procedure TFmCTeConsStatServ_0200a.PreparaVerificacaoStatusServico(
  Empresa: Integer);
begin
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmCTeConsStatServ_0200a.RETxtEnvioChange(Sender: TObject);
begin
  DmCTe_0000.LoadXML(RETxtEnvio, WBEnvio, PageControl1, 1);
end;

procedure TFmCTeConsStatServ_0200a.RETxtRetornoChange(Sender: TObject);
begin
  DmCTe_0000.LoadXML(RETxtRetorno, WBResposta, PageControl1, 3);
end;

function TFmCTeConsStatServ_0200a.TextoArqDefinido(Texto: String): Boolean;
begin
  Result := Texto <> '';
  if not Result then
    Geral.MB_Erro('Texto XML n�o definido!');
end;

procedure TFmCTeConsStatServ_0200a.VerificaCertificadoDigital(Empresa: Integer);
begin
  Geral.MB_Info('Certificado digital forcado em NFe!');
  DmCTe_0000.ReopenEmpresa(Empresa);
  EdEmitCNPJ.Text          := DmCTe_0000.QrEmpresaCNPJ.Value;
  CBUF.Text                := DmCTe_0000.QrFilialCTeUF_WebServ.Value;
  //2011-08-25
  //EdUF_Servico.Text        := DmCTe_0000.QrFilialUF_Servico.Value;
  Geral.MB_Info('Ver o que fazer: **TpEmis**! [1]');
  case DmCTe_0000.QrFilialCTetpEmis.Value of
    3(*SCAN*): EdUF_Servico.Text := 'SCAN';
    else EdUF_Servico.Text := DmCTe_0000.QrFilialCTeUF_Servico.Value;
  end;
  // Fim 2011-08-25
  EdSerialNumber.Text := DmCTe_0000.QrFilialCTeSerNum.Value;
  LaExpiraCertDigital.Caption := '';
  if DmCTe_0000.QrFilialCTeSerVal.Value < 2 then
    LaExpiraCertDigital.Caption :=
    'N�o h� data de validade cadastrada para seu certificado digital!'
  else
  if DmCTe_0000.QrFilialCTeSerVal.Value < Int(Date) then
    LaExpiraCertDigital.Caption :=
    'Seu certificado digital expirou!'
  else
  if DmCTe_0000.QrFilialCTeSerVal.Value <= (Int(Date) +
    DmCTe_0000.QrFilialCTeSerAvi.Value) then
    LaExpiraCertDigital.Caption :=
    'Seu certificado digital expira em ' + FormatFloat('0.000',
    DmCTe_0000.QrFilialCTeSerVal.Value - Now + 1) + ' dias!';
  LaExpiraCertDigital.Visible := LaExpiraCertDigital.Caption <> '';
end;

end.
