unit CTeCTaEdit_0200a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditDateTimePicker,
  dmkEdit, mySQLDBTables;

type
  TFmCTeCTaEdit_0200a = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Label23: TLabel;
    TPDataFat: TdmkEditDateTimePicker;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    EdCodigo: TdmkEdit;
    EdSerieCTe: TdmkEdit;
    Label1: TLabel;
    EdNumeroCTe: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdvRec: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label4: TLabel;
    EdHoraFat: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FQrFrtFatCab: TmySQLQuery;
  end;

  var
  FmCTeCTaEdit_0200a: TFmCTeCTaEdit_0200a;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, DmkDAC_PF, ModuleCTe_0000,
  UnCTe_PF, ModuleFin, CTeGeraXML_0200a;

{$R *.DFM}

procedure TFmCTeCTaEdit_0200a.BtOKClick(Sender: TObject);
const
  Financeiro   = 1; // Credito (2=Debito)
  Represen     = 0;
  JurosMes     = 0.00;
  GravaCampos  = ID_NO;
var
  Encerrou, DataFat, FaturaSep, TpDuplicata, TxtFaturas: String;
  FatID, FatNum, Codigo, Status, Empresa, CartEmiss, CondicaoPG, CtaFretPrest,
  FaturaNum, FaturaSeq, SerieCTe, NumeroCTe, TipoCart, Cliente,
  IDDuplicata: Integer;
  SQLType: TSQLType;
  vRec: Double;
  Data: TDateTime;
  Hora: TTime;
begin
  FatID   := VAR_FATID_5001;
  FatNum  := EdCodigo.ValueVariant;
  Empresa := EdEmpresa.ValueVariant;
  //
  CtaFretPrest := DmCTe_0000.QrOpcoesCTeCtaFretPrest.Value;
  FaturaNum    := DModG.QrParamsEmpFaturaNum.Value;
  FaturaSeq    := DModG.QrParamsEmpFaturaSeq.Value;
  FaturaSep    := DModG.QrParamsEmpFaturaSep.Value;
  TpDuplicata  := DmCTe_0000.QrOpcoesCTeDupFretPrest.Value;
  TxtFaturas   := DmCTe_0000.QrOpcoesCTeTxtFretPrest.Value;
  //
  CartEmiss    := FQrFrtFatCab.FieldByName('CartEmiss').AsInteger;
  CondicaoPG   := FQrFrtFatCab.FieldByName('CondicaoPG').AsInteger;
  Cliente      := FQrFrtFatCab.FieldByName('Tomador').AsInteger;
  //
  IDDuplicata  := EdCodigo.ValueVariant;
  //
  NumeroCTe    := EdNumeroCTe.ValueVariant;
  SerieCTe     := EdSerieCTe.ValueVariant;
  vRec         := EdvRec.ValueVariant;
  //
  TipoCart     := DModFin.ObtemTipoDeCarteira(CartEmiss);
  //
  if not CTe_PF.InsUpdFaturasCTe((*Filial,*) Empresa, FatID, FatNum,
  CtaFretPrest, (*Associada, ASS_CtaFaturas, ASS_Filial,*) Financeiro, CartEmiss,
  CondicaoPG, (*AFP_Sit,*) FaturaNum, IDDuplicata, NumeroCTe, FaturaSeq, TipoCart,
  Represen, (*ASS_IDDuplicata, ASS_FaturaSeq,*) Cliente, (*AFP_Per,*) (*FreteVal,
  Seguro, Desconto, Outros*)vRec, JurosMes, TpDuplicata, FaturaSep, TxtFaturas(*,
  ASS_FaturaSep, ASS_TpDuplicata, ASS_TxtFaturas*), TPDataFat.Date,
  (*QueryPrzT, QuerySumT, QueryPrzX, QuerySumX, QueryNF_X: TmySQLQuery;*)
  SerieCTe, NumeroCTe) then
    Exit;
  //
  Data := Trunc(TPDataFat.Date);
  Hora := EdHoraFat.ValueVariant;
  if DmCTe_0000.InsereDadosDeFaturaNasTabelasCTe(FatID, FatNum, Empresa, SerieCTe,
  NumeroCTe, Data, Hora, LaAviso1, LaAviso2) then
  begin
    DataFat        := Geral.FDT(Data + Hora, 109);
    SQLType        := ImgTipo.SQLType;
    Codigo         := EdCodigo.ValueVariant;
    Encerrou       := Geral.FDT(DModG.ObtemAgora(), 109);
    Status         := Integer(ctemystatusCTeDados);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtfatcab', False, [
    'Encerrou', 'DataFat', 'Status'], [
    'Codigo'], [
    Encerrou, DataFat, Status], [
    Codigo], True) then
    begin
      if FmCTeGeraXML_0200a.GerarArquivoXMLdoCTe(FatID, FatNum, Empresa, LaAviso1,
      LaAviso2, GravaCampos) then
      begin
        DmCTE_0000.StepCTeCab(FatID, FatNum, Empresa, ctemystatusCTeAssinada, LaAviso1, LaAviso2);
        UnDmkDAC_PF.AbreQuery(FQrFrtFatCab, FQrFrtFatCab.DataBase);
        Close;
      end;
    end;
  end;
end;

procedure TFmCTeCTaEdit_0200a.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCTeCTaEdit_0200a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCTeCTaEdit_0200a.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stUpd;
  //
  Agora := DModG.ObtemAgora();
  TPDataFat.Date := Trunc(Agora);
  EdHoraFat.ValueVariant := Agora;
end;

procedure TFmCTeCTaEdit_0200a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
