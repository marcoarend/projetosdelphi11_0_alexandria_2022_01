object FmCTeConsStatServ_0200a: TFmCTeConsStatServ_0200a
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: ??? ??? ???'
  ClientHeight = 621
  ClientWidth = 850
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 850
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 802
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 754
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 151
        Height = 32
        Caption = '??? ??? ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 151
        Height = 32
        Caption = '??? ??? ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 151
        Height = 32
        Caption = '??? ??? ???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 850
    Height = 459
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 241
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 850
      Height = 459
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 241
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 850
        Height = 459
        Align = alClient
        TabOrder = 0
        ExplicitHeight = 241
        object LaExpiraCertDigital: TLabel
          Left = 2
          Top = 167
          Width = 846
          Height = 25
          Align = alTop
          Alignment = taCenter
          Caption = 'Expira'#231#227'o do Certificado Digital'
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -21
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
          ExplicitWidth = 331
        end
        object PnConfig1: TPanel
          Left = 2
          Top = 364
          Width = 846
          Height = 64
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitTop = 15
          object Panel6: TPanel
            Left = 113
            Top = 0
            Width = 733
            Height = 64
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label5: TLabel
              Left = 4
              Top = 12
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Label2: TLabel
              Left = 124
              Top = 13
              Width = 53
              Height = 13
              Caption = 'UF (WS):'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label17: TLabel
              Left = 4
              Top = 40
              Width = 127
              Height = 13
              Caption = 'Serial do Certificado digital:'
            end
            object Label18: TLabel
              Left = 256
              Top = 12
              Width = 74
              Height = 13
              Caption = 'Servi'#231'o [F4]:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object EdSerialNumber: TdmkEdit
              Left = 136
              Top = 36
              Width = 260
              Height = 21
              Enabled = False
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdUF_Servico: TdmkEdit
              Left = 332
              Top = 8
              Width = 64
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object CBUF: TComboBox
              Left = 180
              Top = 8
              Width = 69
              Height = 21
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              Items.Strings = (
                'AC'
                'AL'
                'AM'
                'AP'
                'BA'
                'CE'
                'DF'
                'ES'
                'GO'
                'MA'
                'MG'
                'MS'
                'MT'
                'PA'
                'PB'
                'PE'
                'PI'
                'PR'
                'RJ'
                'RN'
                'RO'
                'RR'
                'RS'
                'SC'
                'SE'
                'SP'
                'TO')
            end
            object EdEmpresa: TdmkEdit
              Left = 52
              Top = 8
              Width = 61
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdEmpresaChange
            end
          end
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 113
            Height = 64
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object RGAmbiente: TRadioGroup
              Left = 0
              Top = 0
              Width = 113
              Height = 64
              Align = alClient
              Caption = ' Ambiente: '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ItemIndex = 0
              Items.Strings = (
                'Nenhum'
                'Produ'#231#227'o'
                'Homologa'#231#227'o')
              ParentFont = False
              TabOrder = 0
            end
          end
        end
        object Panel11: TPanel
          Left = 2
          Top = 428
          Width = 846
          Height = 29
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitTop = 210
          object Label19: TLabel
            Left = 8
            Top = 8
            Width = 68
            Height = 13
            Caption = 'Web Service: '
          end
          object Label20: TLabel
            Left = 732
            Top = 8
            Width = 36
            Height = 13
            Caption = 'Vers'#227'o:'
          end
          object EdWebService: TEdit
            Left = 76
            Top = 4
            Width = 653
            Height = 21
            ReadOnly = True
            TabOrder = 0
          end
          object EdVersaoAcao: TdmkEdit
            Left = 772
            Top = 4
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '2,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 2.000000000000000000
            ValWarn = False
          end
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 236
          Width = 846
          Height = 128
          ActivePage = TabSheet2
          Align = alBottom
          TabOrder = 2
          object TabSheet1: TTabSheet
            Caption = ' XML de envio '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 103
            object RETxtEnvio: TMemo
              Left = 0
              Top = 0
              Width = 838
              Height = 100
              Align = alClient
              TabOrder = 0
              WordWrap = False
              OnChange = RETxtEnvioChange
              ExplicitHeight = 103
            end
          end
          object TabSheet5: TTabSheet
            Caption = 'XML de envio (Formatado)'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object WBEnvio: TWebBrowser
              Left = 0
              Top = 0
              Width = 838
              Height = 100
              Align = alClient
              TabOrder = 0
              ExplicitWidth = 776
              ExplicitHeight = 67
              ControlData = {
                4C0000009C560000560A00000000000000000000000000000000000000000000
                000000004C000000000000000000000001000000E0D057007335CF11AE690800
                2B2E126208000000000000004C0000000114020000000000C000000000000046
                8000000000000000000000000000000000000000000000000000000000000000
                00000000000000000100000000000000000000000000000000000000}
            end
          end
          object TabSheet2: TTabSheet
            Caption = ' XML Retornado (Texto) '
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object RETxtRetorno: TMemo
              Left = 0
              Top = 0
              Width = 838
              Height = 100
              Align = alClient
              TabOrder = 0
              WantReturns = False
              OnChange = RETxtRetornoChange
            end
          end
          object TabSheet3: TTabSheet
            Caption = ' XML Retornado (Formatado) '
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object WBResposta: TWebBrowser
              Left = 0
              Top = 0
              Width = 776
              Height = 67
              Align = alClient
              TabOrder = 0
              ExplicitHeight = 21
              ControlData = {
                4C00000034500000ED0600000000000000000000000000000000000000000000
                000000004C000000000000000000000001000000E0D057007335CF11AE690800
                2B2E126208000000000000004C0000000114020000000000C000000000000046
                8000000000000000000000000000000000000000000000000000000000000000
                00000000000000000100000000000000000000000000000000000000}
            end
          end
          object TabSheet4: TTabSheet
            Caption = ' Informa'#231#245'es do XML de retorno '
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object MeInfo: TMemo
              Left = 0
              Top = 0
              Width = 776
              Height = 67
              Align = alClient
              ScrollBars = ssVertical
              TabOrder = 0
              WordWrap = False
            end
          end
          object TabSheet6: TTabSheet
            Caption = 'Chaves'
            ImageIndex = 5
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object MeChaves: TMemo
              Left = 0
              Top = 0
              Width = 776
              Height = 67
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
            end
          end
        end
        object Panel1: TPanel
          Left = 2
          Top = 121
          Width = 846
          Height = 46
          Align = alTop
          TabOrder = 3
          ExplicitLeft = 46
          ExplicitTop = 251
          object Label14: TLabel
            Left = 8
            Top = 4
            Width = 73
            Height = 13
            Caption = 'CNPJ empresa:'
          end
          object EdEmitCNPJ: TdmkEdit
            Left = 8
            Top = 20
            Width = 112
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 1
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '899'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object RGAcao: TRadioGroup
          Left = 2
          Top = 15
          Width = 846
          Height = 106
          Align = alTop
          Caption = ' A'#231#227'o a realizar: '
          Columns = 3
          Enabled = False
          ItemIndex = 0
          Items.Strings = (
            'Status do servi'#231'o'
            'Envio de lote de NF-e ao fisco'
            'Consultar lote enviado'
            'Pedir cancelamento de NF-e'
            'Pedir inutiliza'#231#227'o de n'#250'mero(s) de NF-e'
            'Consultar NF-e'
            'Enviar lote de eventos da NFe'
            'Consulta Cadastro Entidade'
            'Consulta situa'#231#227'o da NFE (inoperante)'
            'Consulta NF-es Destinadas'
            'Download de NF-e(s)'
            'Consulta Distribui'#231#227'o de DFe de Interesse')
          TabOrder = 4
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 507
    Width = 850
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 289
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 846
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 551
    Width = 850
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 333
    object PnSaiDesis: TPanel
      Left = 704
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object PnConfirma: TPanel
      Left = 2
      Top = 15
      Width = 702
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
end
