unit CTeLEnC_0200a;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkValUsu,
  dmkDBLookupComboBox, dmkEditCB, Menus, Grids, DBGrids, UrlMon, ComCtrls,
  OleCtrls, SHDocVw, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmCTeLEnC_0200a = class(TForm)
    PnDados: TPanel;
    DsCTeLEnC: TDataSource;
    QrCTeLEnC: TmySQLQuery;
    PnEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    VuEmpresa: TdmkValUsu;
    QrCTeLEnCDataHora_TXT: TWideStringField;
    PMLote: TPopupMenu;
    Incluinovolote1: TMenuItem;
    Alteraloteatual1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    PMCTe: TPopupMenu;
    IncluiCTeaoloteatual1: TMenuItem;
    DsCTeLEnI: TDataSource;
    QrCTeLEnI: TmySQLQuery;
    QrCTeCabA: TmySQLQuery;
    QrCTeCabAEmpresa: TIntegerField;
    QrCTeCabAFatID: TIntegerField;
    QrCTeCabAFatNum: TIntegerField;
    QrCTeCabAide_nCT: TIntegerField;
    QrCTeCabAide_dEmi: TDateField;
    QrCTeCabAid: TWideStringField;
    QrCTeCabAide_cCT: TIntegerField;
    QrCTeCabAide_serie: TIntegerField;
    QrCTeCabAStatus: TSmallintField;
    DsCTeCabA: TDataSource;
    N1: TMenuItem;
    Envialoteaofisco1: TMenuItem;
    QrCTeLEnCCodigo: TIntegerField;
    QrCTeLEnCCodUsu: TIntegerField;
    QrCTeLEnCNome: TWideStringField;
    QrCTeLEnCEmpresa: TIntegerField;
    QrCTeLEnCversao: TFloatField;
    QrCTeLEnCtpAmb: TSmallintField;
    QrCTeLEnCverAplic: TWideStringField;
    QrCTeLEnCcStat: TIntegerField;
    QrCTeLEnCxMotivo: TWideStringField;
    QrCTeLEnCcUF: TSmallintField;
    QrCTeLEnCnRec: TWideStringField;
    QrCTeLEnCdhRecbto: TDateTimeField;
    QrCTeLEnCtMed: TIntegerField;
    QrCTeLEnCFilial: TIntegerField;
    QrCTeLEnCNO_Empresa: TWideStringField;
    QrCTeLEnCNO_Ambiente: TWideStringField;
    Verificalotenofisco1: TMenuItem;
    QrCTeCabAinfProt_cStat: TIntegerField;
    QrCTeCabAinfProt_xMotivo: TWideStringField;
    N2: TMenuItem;
    Lerarquivo1: TMenuItem;
    Enviodelote1: TMenuItem;
    Consultadelote1: TMenuItem;
    PainelData: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    GroupBox1: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    GroupBox2: TGroupBox;
    Label16: TLabel;
    Label10: TLabel;
    Label17: TLabel;
    DBEdit13: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdCodigo: TDBEdit;
    QrCTeLEnM: TmySQLQuery;
    DsCTeLEnM: TDataSource;
    DBGrid2: TDBGrid;
    QrCTeLEnMControle: TIntegerField;
    QrCTeLEnMversao: TFloatField;
    QrCTeLEnMtpAmb: TSmallintField;
    QrCTeLEnMverAplic: TWideStringField;
    QrCTeLEnMcStat: TIntegerField;
    QrCTeLEnMxMotivo: TWideStringField;
    QrCTeLEnMcUF: TSmallintField;
    QrCTeLEnMnRec: TWideStringField;
    QrCTeLEnMdhRecbto: TDateTimeField;
    QrCTeLEnMtMed: TIntegerField;
    RemoveCTesselecionadas1: TMenuItem;
    N3: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    WB_XML: TWebBrowser;
    PMXML: TPopupMenu;
    AbrirXMLnonavegadordestajanela1: TMenuItem;
    SelecionaroXML1: TMenuItem;
    Lote1: TMenuItem;
    Assinada1: TMenuItem;
    Gerada2: TMenuItem;
    AbrirXMLdaNCTenonavegadorpadro1: TMenuItem;
    Assina1: TMenuItem;
    Gerada1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtLote: TBitBtn;
    BtCTe: TBitBtn;
    BtXML: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    RetornodoLotedeenvio1: TMenuItem;
    RGIndSinc: TdmkRadioGroup;
    DBRadioGroup1: TDBRadioGroup;
    QrCTeLEnCIndSinc: TSmallintField;
    N4: TMenuItem;
    ValidarXMLdaCTE1: TMenuItem;
    Assinada2: TMenuItem;
    Lotedeenvio1: TMenuItem;
    QrCTeCabAvPrest_vTPrest: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCTeLEnCAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCTeLEnCBeforeOpen(DataSet: TDataSet);
    procedure BtLoteClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrCTeLEnCCalcFields(DataSet: TDataSet);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure Incluinovolote1Click(Sender: TObject);
    procedure PMLotePopup(Sender: TObject);
    procedure PMCTePopup(Sender: TObject);
    procedure IncluiCTeaoloteatual1Click(Sender: TObject);
    procedure BtCTeClick(Sender: TObject);
    procedure QrCTeLEnCAfterScroll(DataSet: TDataSet);
    procedure Envialoteaofisco1Click(Sender: TObject);
    procedure Verificalotenofisco1Click(Sender: TObject);
    procedure Enviodelote1Click(Sender: TObject);
    procedure Consultadelote1Click(Sender: TObject);
    procedure RemoveCTesselecionadas1Click(Sender: TObject);
    procedure Gerada1Click(Sender: TObject);
    procedure Assina1Click(Sender: TObject);
    procedure Gerada2Click(Sender: TObject);
    procedure Assinada1Click(Sender: TObject);
    procedure Lote1Click(Sender: TObject);
    procedure SelecionaroXML1Click(Sender: TObject);
    procedure BtXMLClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure RetornodoLotedeenvio1Click(Sender: TObject);
    procedure PMXMLPopup(Sender: TObject);
    procedure Assinada2Click(Sender: TObject);
    procedure Lotedeenvio1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure EnviaLoteAoFisco(SohLer: Boolean);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenCTeLEnI();
    procedure ReopenCTeLEnM();
  end;

var
  FmCTeLEnC_0200a: TFmCTeLEnC_0200a;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, CTeLEnI_0200a, ModuleCTe_0000,
  CTeSteps_0200a, UnCTe_PF, UnXXe_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCTeLEnC_0200a.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCTeLEnC_0200a.Lote1Click(Sender: TObject);
var
  LoteStr: String;
begin
  LoteStr := FormatFloat('000000000', QrCTeLEnCCodigo.Value);
  DmCTe_0000.AbreXML_NoTWebBrowser(QrCTeLEnCEmpresa.Value, CTE_EXT_ENV_LOT_XML,
    LoteStr, True, WB_XML);
end;

procedure TFmCTeLEnC_0200a.Lotedeenvio1Click(Sender: TObject);
var
  LoteStr, Arquivo: String;
begin
  LoteStr := FormatFloat('000000000', QrCTeLEnCCodigo.Value);
  Arquivo := DmCTe_0000.Obtem_Arquivo_XML_(QrCTeLEnCEmpresa.Value,
               CTE_EXT_ENV_LOT_XML, LoteStr, True);
  //
  XXe_PF.ValidaXML_XXe(Arquivo);
end;

procedure TFmCTeLEnC_0200a.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCTeLEncCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmCTeLEnC_0200a.Verificalotenofisco1Click(Sender: TObject);
begin
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  if DBCheck.CriaFm(TFmCTeSteps_0200a, FmCTeSteps_0200a, afmoNegarComAviso) then
  begin
    //FmCTeSteps_0200a.PnLoteEnv.Visible := True;
    FmCTeSteps_0200a.EdVersaoAcao.ValueVariant := DmCTe_0000.QrOpcoesCTeCTeVerConLot.Value;
    FmCTeSteps_0200a.Show;
    //
    FmCTeSteps_0200a.RGAcao.ItemIndex := Integer(TCTeServicoStep.ctesrvConsultarLoteCteEnviado);  // 2 - Verifica lote no fisco
    FmCTeSteps_0200a.PreparaConsultaLote(QrCTeLEnCCodigo.Value, QrCTeLEnCEmpresa.Value,
      QrCTeLEnCnRec.Value);
    FmCTeSteps_0200a.FQry      := QrCTeLEnC;
    //
    //FmCTeSteps_0200a.Destroy;
    //LocCod(QrCTeLEnCCodigo.Value, QrCTeLEnCCodigo.Value);
  end;
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCTeLEnC_0200a.DefParams;
begin
  VAR_GOTOTABELA := 'CTeLEnc';
  VAR_GOTOMYSQLTABLE := QrCTeLEnc;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT lot.*, ent.Filial, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Empresa');
  VAR_SQLx.Add('FROM ctelenc lot');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE lot.Codigo>-1000');
  VAR_SQLx.Add('AND lot.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND lot.Codigo=:P0');
  //
  VAR_SQL2.Add('AND lot.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND lot.Nome Like :P0');
  //
end;

procedure TFmCTeLEnC_0200a.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'ctelenc', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmCTeLEnC_0200a.EnviaLoteAoFisco(SohLer: Boolean);
var
  IndSinc: TXXeIndSinc;
  Lote, Empresa: Integer;
begin
  IndSinc := TXXeIndSinc(QrCTeLEnCindSinc.Value);
  Lote    := QrCTeLEnCCodigo.Value;
  Empresa := QrCTeLEnCEmpresa.Value;
  CTe_PF.EnviaLoteAoFisco(IndSinc, Lote, Empresa, SohLer, QrCTeLEnC);
  //LocCod(Lote, Lote);
end;

procedure TFmCTeLEnC_0200a.Envialoteaofisco1Click(Sender: TObject);
begin
  EnviaLoteAoFisco(False);
end;

procedure TFmCTeLEnC_0200a.Enviodelote1Click(Sender: TObject);
begin
  EnviaLoteAoFisco(True);
end;

procedure TFmCTeLEnC_0200a.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PnDados.Visible := True;
      PnEdita.Visible := False;
    end;
    1:
    begin
      PnEdita.Visible := True;
      PnDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCTeLEnC_0200a.PMLotePopup(Sender: TObject);
var
  HabilitaA, HabilitaB, HabilitaC: Boolean;
begin
  HabilitaA := (QrCTeLEnc.State = dsBrowse) and (QrCTeLEnc.RecordCount > 0);
  HabilitaC := (QrCTeCabA.State = dsBrowse) and (QrCTeCabA.RecordCount > 0);
  Alteraloteatual1.Enabled := HabilitaA;
  // Leituras de arquivos
  LerArquivo1.Enabled      := HabilitaA;
  Enviodelote1.Enabled     := HabilitaA;
  Consultadelote1.Enabled  := HabilitaA;//? and (QrCTeLEnc.);
  //
  HabilitaB := HabilitaA and (not (QrCTeLEnCcStat.Value in ([103,104,105]))) and HabilitaC;
  Envialoteaofisco1.Enabled := HabilitaB;
  HabilitaB := HabilitaA and (QrCTeLEnCcStat.Value > 102);
  Verificalotenofisco1.Enabled := HabilitaB;

  (*
  Envialoteaofisco1.Enabled    := True;
  Verificalotenofisco1.Enabled := True;
  (*desmarcar em testes*)
end;

procedure TFmCTeLEnC_0200a.PMCTePopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrCTeLEnc.State <> dsInactive) and (QrCTeLEnc.RecordCount > 0) and (QrCTeLEnCcStat.Value <> 103);
  //
  Alteraloteatual1.Enabled        := Habilita;
  RemoveCTesselecionadas1.Enabled := Habilita;
end;

procedure TFmCTeLEnC_0200a.PMXMLPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrCTeLEnC.State <> dsInactive) and (QrCTeLEnC.RecordCount > 0);
  Enab2 := (QrCTeCabA.State <> dsInactive) and (QrCTeCabA.RecordCount > 0);
  //
  AbrirXMLnonavegadordestajanela1.Enabled := Enab and Enab2;
  AbrirXMLdaNCTenonavegadorpadro1.Enabled := Enab and Enab2;
  ValidarXMLdaCTe1.Enabled                := Enab and Enab2;
end;

procedure TFmCTeLEnC_0200a.Consultadelote1Click(Sender: TObject);
begin
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  if DBCheck.CriaFm(TFmCTeSteps_0200a, FmCTeSteps_0200a, afmoNegarComAviso) then
  begin
    //FmCTeSteps_0200a.PnLoteEnv.Visible := True;
    FmCTeSteps_0200a.EdVersaoAcao.ValueVariant := DmCTe_0000.QrOpcoesCTeCTeVerConLot.Value;
    FmCTeSteps_0200a.Show;
    //
    FmCTeSteps_0200a.RGAcao.ItemIndex := Integer(TCTeServicoStep.ctesrvConsultarLoteCTeEnviado);  // 2 - verifica lote no fisco
    FmCTeSteps_0200a.CkSoLer.Checked  := True;
    FmCTeSteps_0200a.PreparaConsultaLote(QrCTeLEnCCodigo.Value,
      QrCTeLEnCEmpresa.Value, QrCTeLEnCnRec.Value);
    //FmCTeSteps_0200a.FFormChamou      := 'FmCTeLEnc_0200a';
    FmCTeSteps_0200a.FQry      := QrCTeLEnC;
    //
    //
    //FmCTeSteps_0200a.Destroy;
    //
    //LocCod(QrCTeLEnCCodigo.Value, QrCTeLEnCCodigo.Value);
  end;
end;

procedure TFmCTeLEnC_0200a.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCTeLEnC_0200a.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCTeLEnC_0200a.RemoveCTesselecionadas1Click(Sender: TObject);
var
  Status: Integer;
begin
  QrCTeLEnM.First;
  if (QrCTeCabAStatus.Value < 100) or (not (QrCTeLEnCcStat.Value in ([103,104,105])))
  or ((QrCTeLEnMcStat.Value in ([103,104,105])) and (QrCTeCabAStatus.Value > 200)) then
  begin
    if Geral.MB_Pergunta('Deseja realmente tirar o CT-e deste lote?') = mrYes then
    begin
      if (QrCTeCabAStatus.Value = 50)
      or ((QrCTeCabAStatus.Value = 103) and (QrCTeLEnMcStat.Value > 200)) then
        Status := 30
      else
        Status := QrCTeCabAStatus.Value;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE ctecaba SET LoteEnv = 0, Status=:P0 ');
      Dmod.QrUpd.SQL.Add('WHERE FatID=:P1 AND FatNum=:P2 AND Empresa=:P3');
      Dmod.QrUpd.Params[00].AsInteger := Status;
      Dmod.QrUpd.Params[01].AsInteger := QrCTeCabAFatID.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrCTeCabAFatNum.Value;
      Dmod.QrUpd.Params[03].AsInteger := QrCTeCabAEmpresa.Value;
      Dmod.QrUpd.ExecSQL;
      ReopenCTeLEnI();
    end;
  end else
    Geral.MB_Aviso('A��o n�o permitida!');

end;

procedure TFmCTeLEnC_0200a.ReopenCTeLEnI;
begin
  QrCTeCabA.Close;
  QrCTeCabA.Params[0].AsInteger := QrCTeLEnCCodigo.Value;
  QrCTeCabA.Open;
end;

procedure TFmCTeLEnC_0200a.ReopenCTeLEnM;
begin
  QrCTeLEnM.Close;
  QrCTeLEnM.Params[0].AsInteger := QrCTeLEnCCodigo.Value;
  QrCTeLEnM.Open;
end;

procedure TFmCTeLEnC_0200a.RetornodoLotedeenvio1Click(Sender: TObject);
var
  LoteStr: String;
begin
  LoteStr := FormatFloat('000000000', QrCTeLEnCCodigo.Value);
  DmCTe_0000.AbreXML_NoTWebBrowser(QrCTeLEnCEmpresa.Value, CTE_EXT_REC_XML,
    LoteStr, True, WB_XML);
end;

procedure TFmCTeLEnC_0200a.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCTeLEnC_0200a.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCTeLEnC_0200a.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCTeLEnC_0200a.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCTeLEnC_0200a.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCTeLEnC_0200a.Alteraloteatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCTeLEnc, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ctelenc');
end;

procedure TFmCTeLEnC_0200a.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCTeLEncCodigo.Value;
  Close;
end;

procedure TFmCTeLEnC_0200a.BtXMLClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMXML, BtXML);
end;

procedure TFmCTeLEnC_0200a.BtCTeClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMCTe, BtCTe);
end;

procedure TFmCTeLEnC_0200a.Assina1Click(Sender: TObject);
begin
  DmCTe_0000.AbreXML_NoNavegadorPadrao(QrCTeLEnCEmpresa.Value, CTE_EXT_CTE_XML,
    QrCTeCabAid.Value, True);
end;

procedure TFmCTeLEnC_0200a.Assinada1Click(Sender: TObject);
begin
  DmCTe_0000.AbreXML_NoTWebBrowser(QrCTeLEnCEmpresa.Value, CTE_EXT_CTE_XML,
    QrCTeCabAid.Value, True, WB_XML);
end;

procedure TFmCTeLEnC_0200a.Assinada2Click(Sender: TObject);
var
  Arquivo: String;
begin
  Arquivo := DmCTe_0000.Obtem_Arquivo_XML_(QrCTeLEnCEmpresa.Value,
                CTE_EXT_CTE_XML, QrCTeCabAid.Value, True);
  //
  XXe_PF.ValidaXML_XXe(Arquivo);
end;

procedure TFmCTeLEnC_0200a.BtConfirmaClick(Sender: TObject);
var
  Nome(*, verAplic, xMotivo, nRec, dhRecbto, cMsg, xMsg*): String;
  Codigo, CodUsu, Empresa, (*tpAmb, cStat, cUF, tMed,*) IndSinc: Integer;
  //versao, dhRecbtoTZD: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  (*Empresa*)    DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
(*  versao         := ;
  tpAmb          := ;
  verAplic       := ;
  cStat          := ;
  xMotivo        := ;
  cUF            := ;
  nRec           := ;
  dhRecbto       := ;
  tMed           := ;
  cMsg           := ;
  xMsg           := ;
  dhRecbtoTZD    := ;*)
  IndSinc        := RGIndSinc.ItemIndex;

  //
  if ImgTipo.SQLType = stUpd then
    Codigo := EdCodigo.ValueVariant
  else
    Codigo := DModG.BuscaProximoCodigoInt('ctectrl', 'ctelenc', '', 0, 999999999, '');
  CodUsu := Codigo;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ctelenc', False, [
  'CodUsu', 'Nome', 'Empresa',
  (*'versao', 'tpAmb', 'verAplic',
  'cStat', 'xMotivo', 'cUF',
  'nRec', 'dhRecbto', 'tMed',
  'cMsg', 'xMsg', 'dhRecbtoTZD',*)
  'IndSinc'], [
  'Codigo'], [
  CodUsu, Nome, Empresa,
  (*versao, tpAmb, verAplic,
  cStat, xMotivo, cUF,
  nRec, dhRecbto, tMed,
  cMsg, xMsg, dhRecbtoTZD,*)
  IndSinc], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCTeLEnC_0200a.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CTeLEnc', 'Codigo');
  MostraEdicao(0, stLok, 0);
end;

procedure TFmCTeLEnC_0200a.BtLoteClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMLote, BtLote);
end;

procedure TFmCTeLEnC_0200a.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  PainelEdit.Align   := alClient;
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmCTeLEnC_0200a.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCTeLEncCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCTeLEnC_0200a.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmCTeLEnC_0200a.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCTeLEnC_0200a.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCTeLEncCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmCTeLEnC_0200a.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCTeLEnC_0200a.QrCTeLEnCAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCTeLEnC_0200a.QrCTeLEnCAfterScroll(DataSet: TDataSet);
begin
  ReopenCTeLEnI();
  ReopenCTeLEnM();
end;

procedure TFmCTeLEnC_0200a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCTeLEnC_0200a.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCTeLEncCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'CTeLEnc', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCTeLEnC_0200a.SelecionaroXML1Click(Sender: TObject);
var
  Arquivo: String;
begin
  if MyObjects.FileOpenDialog(Self, 'C:\Dermatek\CTE', '', 'Informe o aqruivo XML',
  '', [], Arquivo) then
    WB_XML.Navigate(Arquivo);
end;

procedure TFmCTeLEnC_0200a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCTeLEnC_0200a.Gerada1Click(Sender: TObject);
begin
  DmCTe_0000.AbreXML_NoNavegadorPadrao(QrCTeLEnCEmpresa.Value, CTE_EXT_CTE_XML,
    QrCTeCabAid.Value, False);
end;

procedure TFmCTeLEnC_0200a.Gerada2Click(Sender: TObject);
begin
  DmCTe_0000.AbreXML_NoTWebBrowser(QrCTeLEnCEmpresa.Value, CTE_EXT_CTE_XML,
    QrCTeCabAid.Value, False, WB_XML);
end;

procedure TFmCTeLEnC_0200a.IncluiCTeaoloteatual1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCTeLEnI_0200a, FmCTeLEnI_0200a, afmoNegarComAviso) then
  begin
    FmCTeLEnI_0200a.QrCTeCabA.Close;
    FmCTeLEnI_0200a.QrCTeCabA.Params[00].AsInteger := QrCTeLEncEmpresa.Value;
    FmCTeLEnI_0200a.QrCTeCabA.Params[01].AsInteger := Integer(ctemystatusCTeAssinada);
    FmCTeLEnI_0200a.QrCTeCabA.Open;
    //
    FmCTeLEnI_0200a.ShowModal;
    FmCTeLEnI_0200a.Destroy;
    //
    //LocCod(QrCTeLEnCCodigo.Value, QrCTeLEnCCodigo.Value);
  end;
end;

procedure TFmCTeLEnC_0200a.Incluinovolote1Click(Sender: TObject);
var
  Filial: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrCTeLEnc, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ctelenc');
  UMyMod.ObtemValorDoCampoXDeIndex_Int(VAR_LIB_EMPRESA_SEL, 'Filial', 'Codigo',
    DmodG.QrEmpresas, EdEmpresa, CBEmpresa, Filial);
end;

procedure TFmCTeLEnC_0200a.QrCTeLEnCBeforeOpen(DataSet: TDataSet);
begin
  QrCTeLEncCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCTeLEnC_0200a.QrCTeLEnCCalcFields(DataSet: TDataSet);
begin
  QrCTeLEncDataHora_TXT.Value := dmkPF.FDT_NULO(QrCTeLEnCdhRecbto.Value, 0);
  case QrCTeLEnCtpAmb.Value of
    1: QrCTeLEnCNO_Ambiente.Value := 'Produ��o';
    2: QrCTeLEnCNO_Ambiente.Value := 'Homologa��o';
    else QrCTeLEnCNO_Ambiente.Value := '';
  end;
end;

end.

