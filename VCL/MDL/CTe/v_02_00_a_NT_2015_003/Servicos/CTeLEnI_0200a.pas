unit CTeLEnI_0200a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, Grids, DBGrids,
  dmkImage, UnDmkEnums;

type
  TFmCTeLEnI_0200a = class(TForm)
    Panel1: TPanel;
    QrCTeCabA: TmySQLQuery;
    QrCTeCabAFatID: TIntegerField;
    QrCTeCabAFatNum: TIntegerField;
    QrCTeCabAide_nCT: TIntegerField;
    QrCTeCabAide_dEmi: TDateField;
    QrCTeCabAid: TWideStringField;
    QrCTeCabAide_cCT: TIntegerField;
    QrCTeCabAide_serie: TIntegerField;
    QrCTeCabAStatus: TSmallintField;
    DBGrid1: TDBGrid;
    DsCTeCabA: TDataSource;
    QrCTeCabAEmpresa: TIntegerField;
    QrCTeCabAinfProt_cStat: TIntegerField;
    QrCTeCabAinfProt_xMotivo: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCTeCabAvPrest_vTPrest: TFloatField;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCTeLEnI_0200a: TFmCTeLEnI_0200a;

implementation

uses UnMyObjects, CTeLEnC_0200a, Module, UMySQLModule, dmkGeral, ModuleCTe_0000;

{$R *.DFM}

procedure TFmCTeLEnI_0200a.BtConfirmaClick(Sender: TObject);
  procedure AdicionaCTeAoLote();
  var
    LoteEnv, FatID, FatNum, Empresa, Status: Integer;
    infProt_cStat, infProt_xMotivo: String;
  begin
    LoteEnv := FmCTeLEnC_0200a.QrCTeLEnCCodigo.Value;
    FatID   := QrCTeCabAFatID.Value;
    FatNum  := QrCTeCabAFatNum.Value;
    Empresa := QrCTeCabAEmpresa.Value;
    Status  := Integer(ctemystatusCTeAddedLote);
    infProt_cStat   := '0'; // Limpar variaveis (ver historico)
    infProt_xMotivo := '';
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctecaba', False, [
    'LoteEnv', 'Status', 'infProt_cStat', 'infProt_xMotivo'], ['FatID', 'FatNum', 'Empresa'], [
    LoteEnv, Status, infProt_cStat, infProt_xMotivo], [FatID, FatNum, Empresa], True);
  end;
var
  I, N: Integer;
begin
  N := DBGrid1.SelectedRows.Count;
  if N + FmCTeLEnC_0200a.QrCTeCabA.RecordCount > 50 then
  begin
    Geral.MensagemBox('Quantidade extrapola o m�ximo de 50 NFs permitidas por lote!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if N > 1 then
  begin
    if Application.MessageBox(PChar('Confirma a adi��o dos ' + IntToStr(N) +
    ' CT-e selecionados?'), 'Pergunta', MB_ICONQUESTION+MB_YESNOCANCEL) =
    ID_YES then
    begin
      with DBGrid1.DataSource.DataSet do
      for I := 0 to N - 1 do
      begin
        GotoBookmark(pointer(DBGrid1.SelectedRows.Items[I]));
        AdicionaCTeAoLote();
      end;
    end;
  end else AdicionaCTeAoLote();
  FmCTeLEnC_0200a.ReopenCTeLEnI();
  Close;
end;

procedure TFmCTeLEnI_0200a.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCTeLEnI_0200a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCTeLEnI_0200a.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FmCTeLEnC_0200a.LocCod(FmCTeLEnC_0200a.QrCTeLEnCCodigo.Value, FmCTeLEnC_0200a.QrCTeLEnCCodigo.Value);
end;

procedure TFmCTeLEnI_0200a.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCTeLEnI_0200a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
