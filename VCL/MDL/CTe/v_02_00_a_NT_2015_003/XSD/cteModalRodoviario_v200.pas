
{********************************************************************************************************************************************}
{                                                                                                                                            }
{                                                              XML Data Binding                                                              }
{                                                                                                                                            }
{         Generated on: 16/12/2015 08:57:03                                                                                                  }
{       Generated from: C:\_Compilers\DELPHI_XE2\VCL\MDL\CTe\v_02_00_a_NT_2015_003\XSD\PL_CTe_200a_NT2015.003\cteModalRodoviario_v2.00.xsd   }
{   Settings stored in: C:\_Compilers\DELPHI_XE2\VCL\MDL\CTe\v_02_00_a_NT_2015_003\XSD\PL_CTe_200a_NT2015.003\cteModalRodoviario_v2.00.xdb   }
{                                                                                                                                            }
{********************************************************************************************************************************************}

unit cteModalRodoviario_v200;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLRodo = interface;
  IXMLRodo_occ = interface;
  IXMLRodo_occList = interface;
  IXMLRodo_occ_emiOcc = interface;
  IXMLRodo_valePed = interface;
  IXMLRodo_valePedList = interface;
  IXMLRodo_veic = interface;
  IXMLRodo_veicList = interface;
  IXMLRodo_veic_prop = interface;
  IXMLRodo_lacRodo = interface;
  IXMLRodo_lacRodoList = interface;
  IXMLRodo_moto = interface;
  IXMLRodo_motoList = interface;
  IXMLTCTe = interface;
  IXMLTCTeList = interface;
  IXMLInfCte = interface;
  IXMLIde = interface;
  IXMLToma03 = interface;
  IXMLToma4 = interface;
  IXMLTEndereco = interface;
  IXMLCompl = interface;
  IXMLFluxo = interface;
  IXMLPass = interface;
  IXMLPassList = interface;
  IXMLEntrega = interface;
  IXMLSemData = interface;
  IXMLComData = interface;
  IXMLNoPeriodo = interface;
  IXMLSemHora = interface;
  IXMLComHora = interface;
  IXMLNoInter = interface;
  IXMLObsCont = interface;
  IXMLObsContList = interface;
  IXMLObsFisco = interface;
  IXMLObsFiscoList = interface;
  IXMLEmit = interface;
  IXMLTEndeEmi = interface;
  IXMLRem = interface;
  IXMLExped = interface;
  IXMLReceb = interface;
  IXMLDest = interface;
  IXMLVPrest = interface;
  IXMLComp = interface;
  IXMLCompList = interface;
  IXMLImp = interface;
  IXMLTImp = interface;
  IXMLICMS00 = interface;
  IXMLICMS20 = interface;
  IXMLICMS45 = interface;
  IXMLICMS60 = interface;
  IXMLICMS90 = interface;
  IXMLICMSOutraUF = interface;
  IXMLICMSSN = interface;
  IXMLICMSUFFim = interface;
  IXMLInfCTeNorm = interface;
  IXMLInfCarga = interface;
  IXMLInfQ = interface;
  IXMLInfQList = interface;
  IXMLInfDoc = interface;
  IXMLInfNF = interface;
  IXMLInfNFList = interface;
  IXMLTUnidadeTransp = interface;
  IXMLTUnidadeTranspList = interface;
  IXMLLacUnidTransp = interface;
  IXMLLacUnidTranspList = interface;
  IXMLTUnidCarga = interface;
  IXMLTUnidCargaList = interface;
  IXMLLacUnidCarga = interface;
  IXMLLacUnidCargaList = interface;
  IXMLInfNFe = interface;
  IXMLInfNFeList = interface;
  IXMLInfOutros = interface;
  IXMLInfOutrosList = interface;
  IXMLDocAnt = interface;
  IXMLEmiDocAnt = interface;
  IXMLIdDocAnt = interface;
  IXMLIdDocAntList = interface;
  IXMLIdDocAntPap = interface;
  IXMLIdDocAntPapList = interface;
  IXMLIdDocAntEle = interface;
  IXMLIdDocAntEleList = interface;
  IXMLSeg = interface;
  IXMLSegList = interface;
  IXMLInfModal = interface;
  IXMLPeri = interface;
  IXMLPeriList = interface;
  IXMLVeicNovos = interface;
  IXMLVeicNovosList = interface;
  IXMLCobr = interface;
  IXMLFat = interface;
  IXMLDup = interface;
  IXMLDupList = interface;
  IXMLInfCteSub = interface;
  IXMLTomaICMS = interface;
  IXMLRefNF = interface;
  IXMLTomaNaoICMS = interface;
  IXMLInfCteComp = interface;
  IXMLInfCteAnu = interface;
  IXMLAutXML = interface;
  IXMLAutXMLList = interface;
  IXMLSignatureType_ds = interface;
  IXMLSignedInfoType_ds = interface;
  IXMLCanonicalizationMethod_ds = interface;
  IXMLSignatureMethod_ds = interface;
  IXMLReferenceType_ds = interface;
  IXMLTransformsType_ds = interface;
  IXMLTransformType_ds = interface;
  IXMLDigestMethod_ds = interface;
  IXMLSignatureValueType_ds = interface;
  IXMLKeyInfoType_ds = interface;
  IXMLX509DataType_ds = interface;
  IXMLTEnviCTe = interface;
  IXMLTRetEnviCTe = interface;
  IXMLInfRec = interface;
  IXMLTEndernac = interface;
  IXMLTEndOrg = interface;
  IXMLTLocal = interface;
  IXMLTEndReEnt = interface;

{ IXMLRodo }

  IXMLRodo = interface(IXMLNode)
    ['{346534DB-0C94-474F-837B-FE0AAC30D585}']
    { Property Accessors }
    function Get_RNTRC: UnicodeString;
    function Get_DPrev: UnicodeString;
    function Get_Lota: UnicodeString;
    function Get_CIOT: UnicodeString;
    function Get_Occ: IXMLRodo_occList;
    function Get_ValePed: IXMLRodo_valePedList;
    function Get_Veic: IXMLRodo_veicList;
    function Get_LacRodo: IXMLRodo_lacRodoList;
    function Get_Moto: IXMLRodo_motoList;
    procedure Set_RNTRC(Value: UnicodeString);
    procedure Set_DPrev(Value: UnicodeString);
    procedure Set_Lota(Value: UnicodeString);
    procedure Set_CIOT(Value: UnicodeString);
    { Methods & Properties }
    property RNTRC: UnicodeString read Get_RNTRC write Set_RNTRC;
    property DPrev: UnicodeString read Get_DPrev write Set_DPrev;
    property Lota: UnicodeString read Get_Lota write Set_Lota;
    property CIOT: UnicodeString read Get_CIOT write Set_CIOT;
    property Occ: IXMLRodo_occList read Get_Occ;
    property ValePed: IXMLRodo_valePedList read Get_ValePed;
    property Veic: IXMLRodo_veicList read Get_Veic;
    property LacRodo: IXMLRodo_lacRodoList read Get_LacRodo;
    property Moto: IXMLRodo_motoList read Get_Moto;
  end;

{ IXMLRodo_occ }

  IXMLRodo_occ = interface(IXMLNode)
    ['{8B73DD8B-3172-4C34-8D2D-F35E00A802A6}']
    { Property Accessors }
    function Get_Serie: UnicodeString;
    function Get_NOcc: UnicodeString;
    function Get_DEmi: UnicodeString;
    function Get_EmiOcc: IXMLRodo_occ_emiOcc;
    procedure Set_Serie(Value: UnicodeString);
    procedure Set_NOcc(Value: UnicodeString);
    procedure Set_DEmi(Value: UnicodeString);
    { Methods & Properties }
    property Serie: UnicodeString read Get_Serie write Set_Serie;
    property NOcc: UnicodeString read Get_NOcc write Set_NOcc;
    property DEmi: UnicodeString read Get_DEmi write Set_DEmi;
    property EmiOcc: IXMLRodo_occ_emiOcc read Get_EmiOcc;
  end;

{ IXMLRodo_occList }

  IXMLRodo_occList = interface(IXMLNodeCollection)
    ['{91EE8565-A205-43D3-8B33-61F2C0868AAE}']
    { Methods & Properties }
    function Add: IXMLRodo_occ;
    function Insert(const Index: Integer): IXMLRodo_occ;

    function Get_Item(Index: Integer): IXMLRodo_occ;
    property Items[Index: Integer]: IXMLRodo_occ read Get_Item; default;
  end;

{ IXMLRodo_occ_emiOcc }

  IXMLRodo_occ_emiOcc = interface(IXMLNode)
    ['{6661745E-7692-472F-9DFB-73EF61DFAAB6}']
    { Property Accessors }
    function Get_CNPJ: UnicodeString;
    function Get_CInt: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_Fone: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CInt(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    { Methods & Properties }
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CInt: UnicodeString read Get_CInt write Set_CInt;
    property IE: UnicodeString read Get_IE write Set_IE;
    property UF: UnicodeString read Get_UF write Set_UF;
    property Fone: UnicodeString read Get_Fone write Set_Fone;
  end;

{ IXMLRodo_valePed }

  IXMLRodo_valePed = interface(IXMLNode)
    ['{AB64D5BF-10CA-4574-B85A-B8FA4EDF222C}']
    { Property Accessors }
    function Get_CNPJForn: UnicodeString;
    function Get_NCompra: UnicodeString;
    function Get_CNPJPg: UnicodeString;
    function Get_VValePed: UnicodeString;
    procedure Set_CNPJForn(Value: UnicodeString);
    procedure Set_NCompra(Value: UnicodeString);
    procedure Set_CNPJPg(Value: UnicodeString);
    procedure Set_VValePed(Value: UnicodeString);
    { Methods & Properties }
    property CNPJForn: UnicodeString read Get_CNPJForn write Set_CNPJForn;
    property NCompra: UnicodeString read Get_NCompra write Set_NCompra;
    property CNPJPg: UnicodeString read Get_CNPJPg write Set_CNPJPg;
    property VValePed: UnicodeString read Get_VValePed write Set_VValePed;
  end;

{ IXMLRodo_valePedList }

  IXMLRodo_valePedList = interface(IXMLNodeCollection)
    ['{FE3D5926-E6E9-431C-917E-26BFD147F036}']
    { Methods & Properties }
    function Add: IXMLRodo_valePed;
    function Insert(const Index: Integer): IXMLRodo_valePed;

    function Get_Item(Index: Integer): IXMLRodo_valePed;
    property Items[Index: Integer]: IXMLRodo_valePed read Get_Item; default;
  end;

{ IXMLRodo_veic }

  IXMLRodo_veic = interface(IXMLNode)
    ['{FA316F01-E464-4AB8-B0CD-3A34BFCBB708}']
    { Property Accessors }
    function Get_CInt: UnicodeString;
    function Get_RENAVAM: UnicodeString;
    function Get_Placa: UnicodeString;
    function Get_Tara: UnicodeString;
    function Get_CapKG: UnicodeString;
    function Get_CapM3: UnicodeString;
    function Get_TpProp: UnicodeString;
    function Get_TpVeic: UnicodeString;
    function Get_TpRod: UnicodeString;
    function Get_TpCar: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_Prop: IXMLRodo_veic_prop;
    procedure Set_CInt(Value: UnicodeString);
    procedure Set_RENAVAM(Value: UnicodeString);
    procedure Set_Placa(Value: UnicodeString);
    procedure Set_Tara(Value: UnicodeString);
    procedure Set_CapKG(Value: UnicodeString);
    procedure Set_CapM3(Value: UnicodeString);
    procedure Set_TpProp(Value: UnicodeString);
    procedure Set_TpVeic(Value: UnicodeString);
    procedure Set_TpRod(Value: UnicodeString);
    procedure Set_TpCar(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    { Methods & Properties }
    property CInt: UnicodeString read Get_CInt write Set_CInt;
    property RENAVAM: UnicodeString read Get_RENAVAM write Set_RENAVAM;
    property Placa: UnicodeString read Get_Placa write Set_Placa;
    property Tara: UnicodeString read Get_Tara write Set_Tara;
    property CapKG: UnicodeString read Get_CapKG write Set_CapKG;
    property CapM3: UnicodeString read Get_CapM3 write Set_CapM3;
    property TpProp: UnicodeString read Get_TpProp write Set_TpProp;
    property TpVeic: UnicodeString read Get_TpVeic write Set_TpVeic;
    property TpRod: UnicodeString read Get_TpRod write Set_TpRod;
    property TpCar: UnicodeString read Get_TpCar write Set_TpCar;
    property UF: UnicodeString read Get_UF write Set_UF;
    property Prop: IXMLRodo_veic_prop read Get_Prop;
  end;

{ IXMLRodo_veicList }

  IXMLRodo_veicList = interface(IXMLNodeCollection)
    ['{E620E3F3-ED2E-4621-8715-6B5614C66378}']
    { Methods & Properties }
    function Add: IXMLRodo_veic;
    function Insert(const Index: Integer): IXMLRodo_veic;

    function Get_Item(Index: Integer): IXMLRodo_veic;
    property Items[Index: Integer]: IXMLRodo_veic read Get_Item; default;
  end;

{ IXMLRodo_veic_prop }

  IXMLRodo_veic_prop = interface(IXMLNode)
    ['{77674B92-2FF7-4C65-8F55-3B60B6ACD6F7}']
    { Property Accessors }
    function Get_CPF: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_RNTRC: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_TpProp: UnicodeString;
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_RNTRC(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_TpProp(Value: UnicodeString);
    { Methods & Properties }
    property CPF: UnicodeString read Get_CPF write Set_CPF;
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property RNTRC: UnicodeString read Get_RNTRC write Set_RNTRC;
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property IE: UnicodeString read Get_IE write Set_IE;
    property UF: UnicodeString read Get_UF write Set_UF;
    property TpProp: UnicodeString read Get_TpProp write Set_TpProp;
  end;

{ IXMLRodo_lacRodo }

  IXMLRodo_lacRodo = interface(IXMLNode)
    ['{481DFD58-66F6-4280-B329-D15E33878FF6}']
    { Property Accessors }
    function Get_NLacre: UnicodeString;
    procedure Set_NLacre(Value: UnicodeString);
    { Methods & Properties }
    property NLacre: UnicodeString read Get_NLacre write Set_NLacre;
  end;

{ IXMLRodo_lacRodoList }

  IXMLRodo_lacRodoList = interface(IXMLNodeCollection)
    ['{B89D15A8-A737-44CD-9617-B8C6D7AC199D}']
    { Methods & Properties }
    function Add: IXMLRodo_lacRodo;
    function Insert(const Index: Integer): IXMLRodo_lacRodo;

    function Get_Item(Index: Integer): IXMLRodo_lacRodo;
    property Items[Index: Integer]: IXMLRodo_lacRodo read Get_Item; default;
  end;

{ IXMLRodo_moto }

  IXMLRodo_moto = interface(IXMLNode)
    ['{951E2FE0-DE61-47B1-8FDA-92FDC49A0347}']
    { Property Accessors }
    function Get_XNome: UnicodeString;
    function Get_CPF: UnicodeString;
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    { Methods & Properties }
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
  end;

{ IXMLRodo_motoList }

  IXMLRodo_motoList = interface(IXMLNodeCollection)
    ['{211A2B87-FEB4-4E67-83FB-BCBE994CF9F9}']
    { Methods & Properties }
    function Add: IXMLRodo_moto;
    function Insert(const Index: Integer): IXMLRodo_moto;

    function Get_Item(Index: Integer): IXMLRodo_moto;
    property Items[Index: Integer]: IXMLRodo_moto read Get_Item; default;
  end;

{ IXMLTCTe }

  IXMLTCTe = interface(IXMLNode)
    ['{7129238F-D6EC-46EC-AB35-C3243D9B4D12}']
    { Property Accessors }
    function Get_InfCte: IXMLInfCte;
    function Get_Signature: IXMLSignatureType_ds;
    { Methods & Properties }
    property InfCte: IXMLInfCte read Get_InfCte;
    property Signature: IXMLSignatureType_ds read Get_Signature;
  end;

{ IXMLTCTeList }

  IXMLTCTeList = interface(IXMLNodeCollection)
    ['{DB0536CD-03EF-48B4-8355-A3D7ED36DBB3}']
    { Methods & Properties }
    function Add: IXMLTCTe;
    function Insert(const Index: Integer): IXMLTCTe;

    function Get_Item(Index: Integer): IXMLTCTe;
    property Items[Index: Integer]: IXMLTCTe read Get_Item; default;
  end;

{ IXMLInfCte }

  IXMLInfCte = interface(IXMLNode)
    ['{9FF89626-EFA9-4D46-8105-FC93047A5A1E}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Ide: IXMLIde;
    function Get_Compl: IXMLCompl;
    function Get_Emit: IXMLEmit;
    function Get_Rem: IXMLRem;
    function Get_Exped: IXMLExped;
    function Get_Receb: IXMLReceb;
    function Get_Dest: IXMLDest;
    function Get_VPrest: IXMLVPrest;
    function Get_Imp: IXMLImp;
    function Get_InfCTeNorm: IXMLInfCTeNorm;
    function Get_InfCteComp: IXMLInfCteComp;
    function Get_InfCteAnu: IXMLInfCteAnu;
    function Get_AutXML: IXMLAutXMLList;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property Id: UnicodeString read Get_Id write Set_Id;
    property Ide: IXMLIde read Get_Ide;
    property Compl: IXMLCompl read Get_Compl;
    property Emit: IXMLEmit read Get_Emit;
    property Rem: IXMLRem read Get_Rem;
    property Exped: IXMLExped read Get_Exped;
    property Receb: IXMLReceb read Get_Receb;
    property Dest: IXMLDest read Get_Dest;
    property VPrest: IXMLVPrest read Get_VPrest;
    property Imp: IXMLImp read Get_Imp;
    property InfCTeNorm: IXMLInfCTeNorm read Get_InfCTeNorm;
    property InfCteComp: IXMLInfCteComp read Get_InfCteComp;
    property InfCteAnu: IXMLInfCteAnu read Get_InfCteAnu;
    property AutXML: IXMLAutXMLList read Get_AutXML;
  end;

{ IXMLIde }

  IXMLIde = interface(IXMLNode)
    ['{D7D8F4A3-53AA-405B-9C4D-E02E905DDCAB}']
    { Property Accessors }
    function Get_CUF: UnicodeString;
    function Get_CCT: UnicodeString;
    function Get_CFOP: UnicodeString;
    function Get_NatOp: UnicodeString;
    function Get_ForPag: UnicodeString;
    function Get_Mod_: UnicodeString;
    function Get_Serie: UnicodeString;
    function Get_NCT: UnicodeString;
    function Get_DhEmi: UnicodeString;
    function Get_TpImp: UnicodeString;
    function Get_TpEmis: UnicodeString;
    function Get_CDV: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_TpCTe: UnicodeString;
    function Get_ProcEmi: UnicodeString;
    function Get_VerProc: UnicodeString;
    function Get_RefCTE: UnicodeString;
    function Get_CMunEnv: UnicodeString;
    function Get_XMunEnv: UnicodeString;
    function Get_UFEnv: UnicodeString;
    function Get_Modal: UnicodeString;
    function Get_TpServ: UnicodeString;
    function Get_CMunIni: UnicodeString;
    function Get_XMunIni: UnicodeString;
    function Get_UFIni: UnicodeString;
    function Get_CMunFim: UnicodeString;
    function Get_XMunFim: UnicodeString;
    function Get_UFFim: UnicodeString;
    function Get_Retira: UnicodeString;
    function Get_XDetRetira: UnicodeString;
    function Get_Toma03: IXMLToma03;
    function Get_Toma4: IXMLToma4;
    function Get_DhCont: UnicodeString;
    function Get_XJust: UnicodeString;
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_CCT(Value: UnicodeString);
    procedure Set_CFOP(Value: UnicodeString);
    procedure Set_NatOp(Value: UnicodeString);
    procedure Set_ForPag(Value: UnicodeString);
    procedure Set_Mod_(Value: UnicodeString);
    procedure Set_Serie(Value: UnicodeString);
    procedure Set_NCT(Value: UnicodeString);
    procedure Set_DhEmi(Value: UnicodeString);
    procedure Set_TpImp(Value: UnicodeString);
    procedure Set_TpEmis(Value: UnicodeString);
    procedure Set_CDV(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_TpCTe(Value: UnicodeString);
    procedure Set_ProcEmi(Value: UnicodeString);
    procedure Set_VerProc(Value: UnicodeString);
    procedure Set_RefCTE(Value: UnicodeString);
    procedure Set_CMunEnv(Value: UnicodeString);
    procedure Set_XMunEnv(Value: UnicodeString);
    procedure Set_UFEnv(Value: UnicodeString);
    procedure Set_Modal(Value: UnicodeString);
    procedure Set_TpServ(Value: UnicodeString);
    procedure Set_CMunIni(Value: UnicodeString);
    procedure Set_XMunIni(Value: UnicodeString);
    procedure Set_UFIni(Value: UnicodeString);
    procedure Set_CMunFim(Value: UnicodeString);
    procedure Set_XMunFim(Value: UnicodeString);
    procedure Set_UFFim(Value: UnicodeString);
    procedure Set_Retira(Value: UnicodeString);
    procedure Set_XDetRetira(Value: UnicodeString);
    procedure Set_DhCont(Value: UnicodeString);
    procedure Set_XJust(Value: UnicodeString);
    { Methods & Properties }
    property CUF: UnicodeString read Get_CUF write Set_CUF;
    property CCT: UnicodeString read Get_CCT write Set_CCT;
    property CFOP: UnicodeString read Get_CFOP write Set_CFOP;
    property NatOp: UnicodeString read Get_NatOp write Set_NatOp;
    property ForPag: UnicodeString read Get_ForPag write Set_ForPag;
    property Mod_: UnicodeString read Get_Mod_ write Set_Mod_;
    property Serie: UnicodeString read Get_Serie write Set_Serie;
    property NCT: UnicodeString read Get_NCT write Set_NCT;
    property DhEmi: UnicodeString read Get_DhEmi write Set_DhEmi;
    property TpImp: UnicodeString read Get_TpImp write Set_TpImp;
    property TpEmis: UnicodeString read Get_TpEmis write Set_TpEmis;
    property CDV: UnicodeString read Get_CDV write Set_CDV;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property TpCTe: UnicodeString read Get_TpCTe write Set_TpCTe;
    property ProcEmi: UnicodeString read Get_ProcEmi write Set_ProcEmi;
    property VerProc: UnicodeString read Get_VerProc write Set_VerProc;
    property RefCTE: UnicodeString read Get_RefCTE write Set_RefCTE;
    property CMunEnv: UnicodeString read Get_CMunEnv write Set_CMunEnv;
    property XMunEnv: UnicodeString read Get_XMunEnv write Set_XMunEnv;
    property UFEnv: UnicodeString read Get_UFEnv write Set_UFEnv;
    property Modal: UnicodeString read Get_Modal write Set_Modal;
    property TpServ: UnicodeString read Get_TpServ write Set_TpServ;
    property CMunIni: UnicodeString read Get_CMunIni write Set_CMunIni;
    property XMunIni: UnicodeString read Get_XMunIni write Set_XMunIni;
    property UFIni: UnicodeString read Get_UFIni write Set_UFIni;
    property CMunFim: UnicodeString read Get_CMunFim write Set_CMunFim;
    property XMunFim: UnicodeString read Get_XMunFim write Set_XMunFim;
    property UFFim: UnicodeString read Get_UFFim write Set_UFFim;
    property Retira: UnicodeString read Get_Retira write Set_Retira;
    property XDetRetira: UnicodeString read Get_XDetRetira write Set_XDetRetira;
    property Toma03: IXMLToma03 read Get_Toma03;
    property Toma4: IXMLToma4 read Get_Toma4;
    property DhCont: UnicodeString read Get_DhCont write Set_DhCont;
    property XJust: UnicodeString read Get_XJust write Set_XJust;
  end;

{ IXMLToma03 }

  IXMLToma03 = interface(IXMLNode)
    ['{684E3E86-3334-4D4B-8D38-D29065F90703}']
    { Property Accessors }
    function Get_Toma: UnicodeString;
    procedure Set_Toma(Value: UnicodeString);
    { Methods & Properties }
    property Toma: UnicodeString read Get_Toma write Set_Toma;
  end;

{ IXMLToma4 }

  IXMLToma4 = interface(IXMLNode)
    ['{1337B817-4435-439E-86EF-FEB6DAB58301}']
    { Property Accessors }
    function Get_Toma: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_XFant: UnicodeString;
    function Get_Fone: UnicodeString;
    function Get_EnderToma: IXMLTEndereco;
    function Get_Email: UnicodeString;
    procedure Set_Toma(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_XFant(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    procedure Set_Email(Value: UnicodeString);
    { Methods & Properties }
    property Toma: UnicodeString read Get_Toma write Set_Toma;
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
    property IE: UnicodeString read Get_IE write Set_IE;
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property XFant: UnicodeString read Get_XFant write Set_XFant;
    property Fone: UnicodeString read Get_Fone write Set_Fone;
    property EnderToma: IXMLTEndereco read Get_EnderToma;
    property Email: UnicodeString read Get_Email write Set_Email;
  end;

{ IXMLTEndereco }

  IXMLTEndereco = interface(IXMLNode)
    ['{65195460-5F8E-4CC7-93ED-34A083F854C6}']
    { Property Accessors }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_CPais: UnicodeString;
    function Get_XPais: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_CPais(Value: UnicodeString);
    procedure Set_XPais(Value: UnicodeString);
    { Methods & Properties }
    property XLgr: UnicodeString read Get_XLgr write Set_XLgr;
    property Nro: UnicodeString read Get_Nro write Set_Nro;
    property XCpl: UnicodeString read Get_XCpl write Set_XCpl;
    property XBairro: UnicodeString read Get_XBairro write Set_XBairro;
    property CMun: UnicodeString read Get_CMun write Set_CMun;
    property XMun: UnicodeString read Get_XMun write Set_XMun;
    property CEP: UnicodeString read Get_CEP write Set_CEP;
    property UF: UnicodeString read Get_UF write Set_UF;
    property CPais: UnicodeString read Get_CPais write Set_CPais;
    property XPais: UnicodeString read Get_XPais write Set_XPais;
  end;

{ IXMLCompl }

  IXMLCompl = interface(IXMLNode)
    ['{02DB057B-6E5B-452A-991B-AE1DCDF63363}']
    { Property Accessors }
    function Get_XCaracAd: UnicodeString;
    function Get_XCaracSer: UnicodeString;
    function Get_XEmi: UnicodeString;
    function Get_Fluxo: IXMLFluxo;
    function Get_Entrega: IXMLEntrega;
    function Get_OrigCalc: UnicodeString;
    function Get_DestCalc: UnicodeString;
    function Get_XObs: UnicodeString;
    function Get_ObsCont: IXMLObsContList;
    function Get_ObsFisco: IXMLObsFiscoList;
    procedure Set_XCaracAd(Value: UnicodeString);
    procedure Set_XCaracSer(Value: UnicodeString);
    procedure Set_XEmi(Value: UnicodeString);
    procedure Set_OrigCalc(Value: UnicodeString);
    procedure Set_DestCalc(Value: UnicodeString);
    procedure Set_XObs(Value: UnicodeString);
    { Methods & Properties }
    property XCaracAd: UnicodeString read Get_XCaracAd write Set_XCaracAd;
    property XCaracSer: UnicodeString read Get_XCaracSer write Set_XCaracSer;
    property XEmi: UnicodeString read Get_XEmi write Set_XEmi;
    property Fluxo: IXMLFluxo read Get_Fluxo;
    property Entrega: IXMLEntrega read Get_Entrega;
    property OrigCalc: UnicodeString read Get_OrigCalc write Set_OrigCalc;
    property DestCalc: UnicodeString read Get_DestCalc write Set_DestCalc;
    property XObs: UnicodeString read Get_XObs write Set_XObs;
    property ObsCont: IXMLObsContList read Get_ObsCont;
    property ObsFisco: IXMLObsFiscoList read Get_ObsFisco;
  end;

{ IXMLFluxo }

  IXMLFluxo = interface(IXMLNode)
    ['{6EB72747-43DE-4B02-9F32-93C0F009733B}']
    { Property Accessors }
    function Get_XOrig: UnicodeString;
    function Get_Pass: IXMLPassList;
    function Get_XDest: UnicodeString;
    function Get_XRota: UnicodeString;
    procedure Set_XOrig(Value: UnicodeString);
    procedure Set_XDest(Value: UnicodeString);
    procedure Set_XRota(Value: UnicodeString);
    { Methods & Properties }
    property XOrig: UnicodeString read Get_XOrig write Set_XOrig;
    property Pass: IXMLPassList read Get_Pass;
    property XDest: UnicodeString read Get_XDest write Set_XDest;
    property XRota: UnicodeString read Get_XRota write Set_XRota;
  end;

{ IXMLPass }

  IXMLPass = interface(IXMLNode)
    ['{5929CC1C-7236-45E7-A4AE-F259625547E0}']
    { Property Accessors }
    function Get_XPass: UnicodeString;
    procedure Set_XPass(Value: UnicodeString);
    { Methods & Properties }
    property XPass: UnicodeString read Get_XPass write Set_XPass;
  end;

{ IXMLPassList }

  IXMLPassList = interface(IXMLNodeCollection)
    ['{6A27FEC8-3064-44B3-B837-59ABFB951757}']
    { Methods & Properties }
    function Add: IXMLPass;
    function Insert(const Index: Integer): IXMLPass;

    function Get_Item(Index: Integer): IXMLPass;
    property Items[Index: Integer]: IXMLPass read Get_Item; default;
  end;

{ IXMLEntrega }

  IXMLEntrega = interface(IXMLNode)
    ['{2A8478AB-ACC4-4E47-93AF-AE07E95695D5}']
    { Property Accessors }
    function Get_SemData: IXMLSemData;
    function Get_ComData: IXMLComData;
    function Get_NoPeriodo: IXMLNoPeriodo;
    function Get_SemHora: IXMLSemHora;
    function Get_ComHora: IXMLComHora;
    function Get_NoInter: IXMLNoInter;
    { Methods & Properties }
    property SemData: IXMLSemData read Get_SemData;
    property ComData: IXMLComData read Get_ComData;
    property NoPeriodo: IXMLNoPeriodo read Get_NoPeriodo;
    property SemHora: IXMLSemHora read Get_SemHora;
    property ComHora: IXMLComHora read Get_ComHora;
    property NoInter: IXMLNoInter read Get_NoInter;
  end;

{ IXMLSemData }

  IXMLSemData = interface(IXMLNode)
    ['{DBF6AFF6-39EF-4A1F-AF07-8896A41308A3}']
    { Property Accessors }
    function Get_TpPer: UnicodeString;
    procedure Set_TpPer(Value: UnicodeString);
    { Methods & Properties }
    property TpPer: UnicodeString read Get_TpPer write Set_TpPer;
  end;

{ IXMLComData }

  IXMLComData = interface(IXMLNode)
    ['{7A159DA4-416A-42C0-8173-784B2D72402E}']
    { Property Accessors }
    function Get_TpPer: UnicodeString;
    function Get_DProg: UnicodeString;
    procedure Set_TpPer(Value: UnicodeString);
    procedure Set_DProg(Value: UnicodeString);
    { Methods & Properties }
    property TpPer: UnicodeString read Get_TpPer write Set_TpPer;
    property DProg: UnicodeString read Get_DProg write Set_DProg;
  end;

{ IXMLNoPeriodo }

  IXMLNoPeriodo = interface(IXMLNode)
    ['{BC709F28-0ADF-4537-8D39-5FE9693E10BC}']
    { Property Accessors }
    function Get_TpPer: UnicodeString;
    function Get_DIni: UnicodeString;
    function Get_DFim: UnicodeString;
    procedure Set_TpPer(Value: UnicodeString);
    procedure Set_DIni(Value: UnicodeString);
    procedure Set_DFim(Value: UnicodeString);
    { Methods & Properties }
    property TpPer: UnicodeString read Get_TpPer write Set_TpPer;
    property DIni: UnicodeString read Get_DIni write Set_DIni;
    property DFim: UnicodeString read Get_DFim write Set_DFim;
  end;

{ IXMLSemHora }

  IXMLSemHora = interface(IXMLNode)
    ['{63444C4F-3190-4276-B159-EA5C589C210A}']
    { Property Accessors }
    function Get_TpHor: UnicodeString;
    procedure Set_TpHor(Value: UnicodeString);
    { Methods & Properties }
    property TpHor: UnicodeString read Get_TpHor write Set_TpHor;
  end;

{ IXMLComHora }

  IXMLComHora = interface(IXMLNode)
    ['{423C0299-43E4-44AC-BF34-939C5F519248}']
    { Property Accessors }
    function Get_TpHor: UnicodeString;
    function Get_HProg: UnicodeString;
    procedure Set_TpHor(Value: UnicodeString);
    procedure Set_HProg(Value: UnicodeString);
    { Methods & Properties }
    property TpHor: UnicodeString read Get_TpHor write Set_TpHor;
    property HProg: UnicodeString read Get_HProg write Set_HProg;
  end;

{ IXMLNoInter }

  IXMLNoInter = interface(IXMLNode)
    ['{A4E56D17-E665-44D9-AED4-28C2BDE45BAA}']
    { Property Accessors }
    function Get_TpHor: UnicodeString;
    function Get_HIni: UnicodeString;
    function Get_HFim: UnicodeString;
    procedure Set_TpHor(Value: UnicodeString);
    procedure Set_HIni(Value: UnicodeString);
    procedure Set_HFim(Value: UnicodeString);
    { Methods & Properties }
    property TpHor: UnicodeString read Get_TpHor write Set_TpHor;
    property HIni: UnicodeString read Get_HIni write Set_HIni;
    property HFim: UnicodeString read Get_HFim write Set_HFim;
  end;

{ IXMLObsCont }

  IXMLObsCont = interface(IXMLNode)
    ['{A857887F-78D5-4577-8BD5-2F21016B02AB}']
    { Property Accessors }
    function Get_XCampo: UnicodeString;
    function Get_XTexto: UnicodeString;
    procedure Set_XCampo(Value: UnicodeString);
    procedure Set_XTexto(Value: UnicodeString);
    { Methods & Properties }
    property XCampo: UnicodeString read Get_XCampo write Set_XCampo;
    property XTexto: UnicodeString read Get_XTexto write Set_XTexto;
  end;

{ IXMLObsContList }

  IXMLObsContList = interface(IXMLNodeCollection)
    ['{793B4B50-505E-4830-82A6-EFA25B35121E}']
    { Methods & Properties }
    function Add: IXMLObsCont;
    function Insert(const Index: Integer): IXMLObsCont;

    function Get_Item(Index: Integer): IXMLObsCont;
    property Items[Index: Integer]: IXMLObsCont read Get_Item; default;
  end;

{ IXMLObsFisco }

  IXMLObsFisco = interface(IXMLNode)
    ['{16B90683-E96A-42B6-9336-2ABE62EDC5F0}']
    { Property Accessors }
    function Get_XCampo: UnicodeString;
    function Get_XTexto: UnicodeString;
    procedure Set_XCampo(Value: UnicodeString);
    procedure Set_XTexto(Value: UnicodeString);
    { Methods & Properties }
    property XCampo: UnicodeString read Get_XCampo write Set_XCampo;
    property XTexto: UnicodeString read Get_XTexto write Set_XTexto;
  end;

{ IXMLObsFiscoList }

  IXMLObsFiscoList = interface(IXMLNodeCollection)
    ['{EC08BBA8-9BDF-4E69-9C65-EDC97C2FCAA5}']
    { Methods & Properties }
    function Add: IXMLObsFisco;
    function Insert(const Index: Integer): IXMLObsFisco;

    function Get_Item(Index: Integer): IXMLObsFisco;
    property Items[Index: Integer]: IXMLObsFisco read Get_Item; default;
  end;

{ IXMLEmit }

  IXMLEmit = interface(IXMLNode)
    ['{C071E8BE-5C71-46D1-B55C-C2775A4A4DD6}']
    { Property Accessors }
    function Get_CNPJ: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_XFant: UnicodeString;
    function Get_EnderEmit: IXMLTEndeEmi;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_XFant(Value: UnicodeString);
    { Methods & Properties }
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property IE: UnicodeString read Get_IE write Set_IE;
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property XFant: UnicodeString read Get_XFant write Set_XFant;
    property EnderEmit: IXMLTEndeEmi read Get_EnderEmit;
  end;

{ IXMLTEndeEmi }

  IXMLTEndeEmi = interface(IXMLNode)
    ['{0D4BF164-C2C7-48C2-9F50-5C441DB37EBC}']
    { Property Accessors }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_Fone: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    { Methods & Properties }
    property XLgr: UnicodeString read Get_XLgr write Set_XLgr;
    property Nro: UnicodeString read Get_Nro write Set_Nro;
    property XCpl: UnicodeString read Get_XCpl write Set_XCpl;
    property XBairro: UnicodeString read Get_XBairro write Set_XBairro;
    property CMun: UnicodeString read Get_CMun write Set_CMun;
    property XMun: UnicodeString read Get_XMun write Set_XMun;
    property CEP: UnicodeString read Get_CEP write Set_CEP;
    property UF: UnicodeString read Get_UF write Set_UF;
    property Fone: UnicodeString read Get_Fone write Set_Fone;
  end;

{ IXMLRem }

  IXMLRem = interface(IXMLNode)
    ['{5959D6E5-0AEB-4F63-A2F2-54739629E90F}']
    { Property Accessors }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_XFant: UnicodeString;
    function Get_Fone: UnicodeString;
    function Get_EnderReme: IXMLTEndereco;
    function Get_Email: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_XFant(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    procedure Set_Email(Value: UnicodeString);
    { Methods & Properties }
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
    property IE: UnicodeString read Get_IE write Set_IE;
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property XFant: UnicodeString read Get_XFant write Set_XFant;
    property Fone: UnicodeString read Get_Fone write Set_Fone;
    property EnderReme: IXMLTEndereco read Get_EnderReme;
    property Email: UnicodeString read Get_Email write Set_Email;
  end;

{ IXMLExped }

  IXMLExped = interface(IXMLNode)
    ['{46D26432-9080-428C-9EDD-B9B4C8A6FFD9}']
    { Property Accessors }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_Fone: UnicodeString;
    function Get_EnderExped: IXMLTEndereco;
    function Get_Email: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    procedure Set_Email(Value: UnicodeString);
    { Methods & Properties }
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
    property IE: UnicodeString read Get_IE write Set_IE;
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property Fone: UnicodeString read Get_Fone write Set_Fone;
    property EnderExped: IXMLTEndereco read Get_EnderExped;
    property Email: UnicodeString read Get_Email write Set_Email;
  end;

{ IXMLReceb }

  IXMLReceb = interface(IXMLNode)
    ['{F31B2AC8-4EFD-4611-ABD8-28574FF0C8A5}']
    { Property Accessors }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_Fone: UnicodeString;
    function Get_EnderReceb: IXMLTEndereco;
    function Get_Email: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    procedure Set_Email(Value: UnicodeString);
    { Methods & Properties }
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
    property IE: UnicodeString read Get_IE write Set_IE;
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property Fone: UnicodeString read Get_Fone write Set_Fone;
    property EnderReceb: IXMLTEndereco read Get_EnderReceb;
    property Email: UnicodeString read Get_Email write Set_Email;
  end;

{ IXMLDest }

  IXMLDest = interface(IXMLNode)
    ['{E7EDF51C-FC13-4B7E-9EE8-DFFEB038F896}']
    { Property Accessors }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_Fone: UnicodeString;
    function Get_ISUF: UnicodeString;
    function Get_EnderDest: IXMLTEndereco;
    function Get_Email: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    procedure Set_ISUF(Value: UnicodeString);
    procedure Set_Email(Value: UnicodeString);
    { Methods & Properties }
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
    property IE: UnicodeString read Get_IE write Set_IE;
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property Fone: UnicodeString read Get_Fone write Set_Fone;
    property ISUF: UnicodeString read Get_ISUF write Set_ISUF;
    property EnderDest: IXMLTEndereco read Get_EnderDest;
    property Email: UnicodeString read Get_Email write Set_Email;
  end;

{ IXMLVPrest }

  IXMLVPrest = interface(IXMLNode)
    ['{2DC461F3-93A2-4E3F-B856-C47AD20896BD}']
    { Property Accessors }
    function Get_VTPrest: UnicodeString;
    function Get_VRec: UnicodeString;
    function Get_Comp: IXMLCompList;
    procedure Set_VTPrest(Value: UnicodeString);
    procedure Set_VRec(Value: UnicodeString);
    { Methods & Properties }
    property VTPrest: UnicodeString read Get_VTPrest write Set_VTPrest;
    property VRec: UnicodeString read Get_VRec write Set_VRec;
    property Comp: IXMLCompList read Get_Comp;
  end;

{ IXMLComp }

  IXMLComp = interface(IXMLNode)
    ['{F88A950C-EA8A-482D-8D2A-2946A574D87F}']
    { Property Accessors }
    function Get_XNome: UnicodeString;
    function Get_VComp: UnicodeString;
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_VComp(Value: UnicodeString);
    { Methods & Properties }
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property VComp: UnicodeString read Get_VComp write Set_VComp;
  end;

{ IXMLCompList }

  IXMLCompList = interface(IXMLNodeCollection)
    ['{B36E99C0-0C3E-4681-9133-D33FE093E42E}']
    { Methods & Properties }
    function Add: IXMLComp;
    function Insert(const Index: Integer): IXMLComp;

    function Get_Item(Index: Integer): IXMLComp;
    property Items[Index: Integer]: IXMLComp read Get_Item; default;
  end;

{ IXMLImp }

  IXMLImp = interface(IXMLNode)
    ['{5499FE74-4C17-4E60-AB42-879EB10316EA}']
    { Property Accessors }
    function Get_ICMS: IXMLTImp;
    function Get_VTotTrib: UnicodeString;
    function Get_InfAdFisco: UnicodeString;
    function Get_ICMSUFFim: IXMLICMSUFFim;
    procedure Set_VTotTrib(Value: UnicodeString);
    procedure Set_InfAdFisco(Value: UnicodeString);
    { Methods & Properties }
    property ICMS: IXMLTImp read Get_ICMS;
    property VTotTrib: UnicodeString read Get_VTotTrib write Set_VTotTrib;
    property InfAdFisco: UnicodeString read Get_InfAdFisco write Set_InfAdFisco;
    property ICMSUFFim: IXMLICMSUFFim read Get_ICMSUFFim;
  end;

{ IXMLTImp }

  IXMLTImp = interface(IXMLNode)
    ['{9DFAF6CC-B069-45E5-8F2C-9AFE7E647156}']
    { Property Accessors }
    function Get_ICMS00: IXMLICMS00;
    function Get_ICMS20: IXMLICMS20;
    function Get_ICMS45: IXMLICMS45;
    function Get_ICMS60: IXMLICMS60;
    function Get_ICMS90: IXMLICMS90;
    function Get_ICMSOutraUF: IXMLICMSOutraUF;
    function Get_ICMSSN: IXMLICMSSN;
    { Methods & Properties }
    property ICMS00: IXMLICMS00 read Get_ICMS00;
    property ICMS20: IXMLICMS20 read Get_ICMS20;
    property ICMS45: IXMLICMS45 read Get_ICMS45;
    property ICMS60: IXMLICMS60 read Get_ICMS60;
    property ICMS90: IXMLICMS90 read Get_ICMS90;
    property ICMSOutraUF: IXMLICMSOutraUF read Get_ICMSOutraUF;
    property ICMSSN: IXMLICMSSN read Get_ICMSSN;
  end;

{ IXMLICMS00 }

  IXMLICMS00 = interface(IXMLNode)
    ['{1FB91A6A-C325-4396-95C6-7A11E2720D23}']
    { Property Accessors }
    function Get_CST: UnicodeString;
    function Get_VBC: UnicodeString;
    function Get_PICMS: UnicodeString;
    function Get_VICMS: UnicodeString;
    procedure Set_CST(Value: UnicodeString);
    procedure Set_VBC(Value: UnicodeString);
    procedure Set_PICMS(Value: UnicodeString);
    procedure Set_VICMS(Value: UnicodeString);
    { Methods & Properties }
    property CST: UnicodeString read Get_CST write Set_CST;
    property VBC: UnicodeString read Get_VBC write Set_VBC;
    property PICMS: UnicodeString read Get_PICMS write Set_PICMS;
    property VICMS: UnicodeString read Get_VICMS write Set_VICMS;
  end;

{ IXMLICMS20 }

  IXMLICMS20 = interface(IXMLNode)
    ['{9FFB3458-945A-4A46-962B-EE682A29AA96}']
    { Property Accessors }
    function Get_CST: UnicodeString;
    function Get_PRedBC: UnicodeString;
    function Get_VBC: UnicodeString;
    function Get_PICMS: UnicodeString;
    function Get_VICMS: UnicodeString;
    procedure Set_CST(Value: UnicodeString);
    procedure Set_PRedBC(Value: UnicodeString);
    procedure Set_VBC(Value: UnicodeString);
    procedure Set_PICMS(Value: UnicodeString);
    procedure Set_VICMS(Value: UnicodeString);
    { Methods & Properties }
    property CST: UnicodeString read Get_CST write Set_CST;
    property PRedBC: UnicodeString read Get_PRedBC write Set_PRedBC;
    property VBC: UnicodeString read Get_VBC write Set_VBC;
    property PICMS: UnicodeString read Get_PICMS write Set_PICMS;
    property VICMS: UnicodeString read Get_VICMS write Set_VICMS;
  end;

{ IXMLICMS45 }

  IXMLICMS45 = interface(IXMLNode)
    ['{D0FF19EF-C6AE-489D-8E94-15E6DA73505C}']
    { Property Accessors }
    function Get_CST: UnicodeString;
    procedure Set_CST(Value: UnicodeString);
    { Methods & Properties }
    property CST: UnicodeString read Get_CST write Set_CST;
  end;

{ IXMLICMS60 }

  IXMLICMS60 = interface(IXMLNode)
    ['{321338EA-D8E8-480D-87C4-311A8F48EEA1}']
    { Property Accessors }
    function Get_CST: UnicodeString;
    function Get_VBCSTRet: UnicodeString;
    function Get_VICMSSTRet: UnicodeString;
    function Get_PICMSSTRet: UnicodeString;
    function Get_VCred: UnicodeString;
    procedure Set_CST(Value: UnicodeString);
    procedure Set_VBCSTRet(Value: UnicodeString);
    procedure Set_VICMSSTRet(Value: UnicodeString);
    procedure Set_PICMSSTRet(Value: UnicodeString);
    procedure Set_VCred(Value: UnicodeString);
    { Methods & Properties }
    property CST: UnicodeString read Get_CST write Set_CST;
    property VBCSTRet: UnicodeString read Get_VBCSTRet write Set_VBCSTRet;
    property VICMSSTRet: UnicodeString read Get_VICMSSTRet write Set_VICMSSTRet;
    property PICMSSTRet: UnicodeString read Get_PICMSSTRet write Set_PICMSSTRet;
    property VCred: UnicodeString read Get_VCred write Set_VCred;
  end;

{ IXMLICMS90 }

  IXMLICMS90 = interface(IXMLNode)
    ['{4BF45034-0D37-4415-B7CD-1ECC28BCBB85}']
    { Property Accessors }
    function Get_CST: UnicodeString;
    function Get_PRedBC: UnicodeString;
    function Get_VBC: UnicodeString;
    function Get_PICMS: UnicodeString;
    function Get_VICMS: UnicodeString;
    function Get_VCred: UnicodeString;
    procedure Set_CST(Value: UnicodeString);
    procedure Set_PRedBC(Value: UnicodeString);
    procedure Set_VBC(Value: UnicodeString);
    procedure Set_PICMS(Value: UnicodeString);
    procedure Set_VICMS(Value: UnicodeString);
    procedure Set_VCred(Value: UnicodeString);
    { Methods & Properties }
    property CST: UnicodeString read Get_CST write Set_CST;
    property PRedBC: UnicodeString read Get_PRedBC write Set_PRedBC;
    property VBC: UnicodeString read Get_VBC write Set_VBC;
    property PICMS: UnicodeString read Get_PICMS write Set_PICMS;
    property VICMS: UnicodeString read Get_VICMS write Set_VICMS;
    property VCred: UnicodeString read Get_VCred write Set_VCred;
  end;

{ IXMLICMSOutraUF }

  IXMLICMSOutraUF = interface(IXMLNode)
    ['{992D2128-954D-4012-A43E-79F8DCFEB48D}']
    { Property Accessors }
    function Get_CST: UnicodeString;
    function Get_PRedBCOutraUF: UnicodeString;
    function Get_VBCOutraUF: UnicodeString;
    function Get_PICMSOutraUF: UnicodeString;
    function Get_VICMSOutraUF: UnicodeString;
    procedure Set_CST(Value: UnicodeString);
    procedure Set_PRedBCOutraUF(Value: UnicodeString);
    procedure Set_VBCOutraUF(Value: UnicodeString);
    procedure Set_PICMSOutraUF(Value: UnicodeString);
    procedure Set_VICMSOutraUF(Value: UnicodeString);
    { Methods & Properties }
    property CST: UnicodeString read Get_CST write Set_CST;
    property PRedBCOutraUF: UnicodeString read Get_PRedBCOutraUF write Set_PRedBCOutraUF;
    property VBCOutraUF: UnicodeString read Get_VBCOutraUF write Set_VBCOutraUF;
    property PICMSOutraUF: UnicodeString read Get_PICMSOutraUF write Set_PICMSOutraUF;
    property VICMSOutraUF: UnicodeString read Get_VICMSOutraUF write Set_VICMSOutraUF;
  end;

{ IXMLICMSSN }

  IXMLICMSSN = interface(IXMLNode)
    ['{4EFBDFE4-5EB7-40C7-9EC5-F4C3936DB12A}']
    { Property Accessors }
    function Get_IndSN: UnicodeString;
    procedure Set_IndSN(Value: UnicodeString);
    { Methods & Properties }
    property IndSN: UnicodeString read Get_IndSN write Set_IndSN;
  end;

{ IXMLICMSUFFim }

  IXMLICMSUFFim = interface(IXMLNode)
    ['{3634AD14-2CC1-4EC4-A5BC-0C8F8D042041}']
    { Property Accessors }
    function Get_VBCUFFim: UnicodeString;
    function Get_PICMSUFFim: UnicodeString;
    function Get_PICMSInter: UnicodeString;
    function Get_PICMSInterPart: UnicodeString;
    function Get_VICMSUFFim: UnicodeString;
    function Get_VICMSUFIni: UnicodeString;
    procedure Set_VBCUFFim(Value: UnicodeString);
    procedure Set_PICMSUFFim(Value: UnicodeString);
    procedure Set_PICMSInter(Value: UnicodeString);
    procedure Set_PICMSInterPart(Value: UnicodeString);
    procedure Set_VICMSUFFim(Value: UnicodeString);
    procedure Set_VICMSUFIni(Value: UnicodeString);
    { Methods & Properties }
    property VBCUFFim: UnicodeString read Get_VBCUFFim write Set_VBCUFFim;
    property PICMSUFFim: UnicodeString read Get_PICMSUFFim write Set_PICMSUFFim;
    property PICMSInter: UnicodeString read Get_PICMSInter write Set_PICMSInter;
    property PICMSInterPart: UnicodeString read Get_PICMSInterPart write Set_PICMSInterPart;
    property VICMSUFFim: UnicodeString read Get_VICMSUFFim write Set_VICMSUFFim;
    property VICMSUFIni: UnicodeString read Get_VICMSUFIni write Set_VICMSUFIni;
  end;

{ IXMLInfCTeNorm }

  IXMLInfCTeNorm = interface(IXMLNode)
    ['{216EA5C0-5572-465F-8CC4-70A7D8C2A394}']
    { Property Accessors }
    function Get_InfCarga: IXMLInfCarga;
    function Get_InfDoc: IXMLInfDoc;
    function Get_DocAnt: IXMLDocAnt;
    function Get_Seg: IXMLSegList;
    function Get_InfModal: IXMLInfModal;
    function Get_Peri: IXMLPeriList;
    function Get_VeicNovos: IXMLVeicNovosList;
    function Get_Cobr: IXMLCobr;
    function Get_InfCteSub: IXMLInfCteSub;
    { Methods & Properties }
    property InfCarga: IXMLInfCarga read Get_InfCarga;
    property InfDoc: IXMLInfDoc read Get_InfDoc;
    property DocAnt: IXMLDocAnt read Get_DocAnt;
    property Seg: IXMLSegList read Get_Seg;
    property InfModal: IXMLInfModal read Get_InfModal;
    property Peri: IXMLPeriList read Get_Peri;
    property VeicNovos: IXMLVeicNovosList read Get_VeicNovos;
    property Cobr: IXMLCobr read Get_Cobr;
    property InfCteSub: IXMLInfCteSub read Get_InfCteSub;
  end;

{ IXMLInfCarga }

  IXMLInfCarga = interface(IXMLNode)
    ['{3252436D-8765-4C73-BBE0-FD7DE7736A74}']
    { Property Accessors }
    function Get_VCarga: UnicodeString;
    function Get_ProPred: UnicodeString;
    function Get_XOutCat: UnicodeString;
    function Get_InfQ: IXMLInfQList;
    procedure Set_VCarga(Value: UnicodeString);
    procedure Set_ProPred(Value: UnicodeString);
    procedure Set_XOutCat(Value: UnicodeString);
    { Methods & Properties }
    property VCarga: UnicodeString read Get_VCarga write Set_VCarga;
    property ProPred: UnicodeString read Get_ProPred write Set_ProPred;
    property XOutCat: UnicodeString read Get_XOutCat write Set_XOutCat;
    property InfQ: IXMLInfQList read Get_InfQ;
  end;

{ IXMLInfQ }

  IXMLInfQ = interface(IXMLNode)
    ['{6C15C52A-A1BB-4530-B7BF-1B70FFFCAA71}']
    { Property Accessors }
    function Get_CUnid: UnicodeString;
    function Get_TpMed: UnicodeString;
    function Get_QCarga: UnicodeString;
    procedure Set_CUnid(Value: UnicodeString);
    procedure Set_TpMed(Value: UnicodeString);
    procedure Set_QCarga(Value: UnicodeString);
    { Methods & Properties }
    property CUnid: UnicodeString read Get_CUnid write Set_CUnid;
    property TpMed: UnicodeString read Get_TpMed write Set_TpMed;
    property QCarga: UnicodeString read Get_QCarga write Set_QCarga;
  end;

{ IXMLInfQList }

  IXMLInfQList = interface(IXMLNodeCollection)
    ['{7C29E726-B232-4AF5-BA51-2430793A2828}']
    { Methods & Properties }
    function Add: IXMLInfQ;
    function Insert(const Index: Integer): IXMLInfQ;

    function Get_Item(Index: Integer): IXMLInfQ;
    property Items[Index: Integer]: IXMLInfQ read Get_Item; default;
  end;

{ IXMLInfDoc }

  IXMLInfDoc = interface(IXMLNode)
    ['{8AF39388-6549-4243-AD49-5283434F31C5}']
    { Property Accessors }
    function Get_InfNF: IXMLInfNFList;
    function Get_InfNFe: IXMLInfNFeList;
    function Get_InfOutros: IXMLInfOutrosList;
    { Methods & Properties }
    property InfNF: IXMLInfNFList read Get_InfNF;
    property InfNFe: IXMLInfNFeList read Get_InfNFe;
    property InfOutros: IXMLInfOutrosList read Get_InfOutros;
  end;

{ IXMLInfNF }

  IXMLInfNF = interface(IXMLNode)
    ['{756CABA8-9591-4104-81C0-E710CF94F6C7}']
    { Property Accessors }
    function Get_NRoma: UnicodeString;
    function Get_NPed: UnicodeString;
    function Get_Mod_: UnicodeString;
    function Get_Serie: UnicodeString;
    function Get_NDoc: UnicodeString;
    function Get_DEmi: UnicodeString;
    function Get_VBC: UnicodeString;
    function Get_VICMS: UnicodeString;
    function Get_VBCST: UnicodeString;
    function Get_VST: UnicodeString;
    function Get_VProd: UnicodeString;
    function Get_VNF: UnicodeString;
    function Get_NCFOP: UnicodeString;
    function Get_NPeso: UnicodeString;
    function Get_PIN: UnicodeString;
    function Get_DPrev: UnicodeString;
    function Get_InfUnidTransp: IXMLTUnidadeTranspList;
    function Get_InfUnidCarga: IXMLTUnidCargaList;
    procedure Set_NRoma(Value: UnicodeString);
    procedure Set_NPed(Value: UnicodeString);
    procedure Set_Mod_(Value: UnicodeString);
    procedure Set_Serie(Value: UnicodeString);
    procedure Set_NDoc(Value: UnicodeString);
    procedure Set_DEmi(Value: UnicodeString);
    procedure Set_VBC(Value: UnicodeString);
    procedure Set_VICMS(Value: UnicodeString);
    procedure Set_VBCST(Value: UnicodeString);
    procedure Set_VST(Value: UnicodeString);
    procedure Set_VProd(Value: UnicodeString);
    procedure Set_VNF(Value: UnicodeString);
    procedure Set_NCFOP(Value: UnicodeString);
    procedure Set_NPeso(Value: UnicodeString);
    procedure Set_PIN(Value: UnicodeString);
    procedure Set_DPrev(Value: UnicodeString);
    { Methods & Properties }
    property NRoma: UnicodeString read Get_NRoma write Set_NRoma;
    property NPed: UnicodeString read Get_NPed write Set_NPed;
    property Mod_: UnicodeString read Get_Mod_ write Set_Mod_;
    property Serie: UnicodeString read Get_Serie write Set_Serie;
    property NDoc: UnicodeString read Get_NDoc write Set_NDoc;
    property DEmi: UnicodeString read Get_DEmi write Set_DEmi;
    property VBC: UnicodeString read Get_VBC write Set_VBC;
    property VICMS: UnicodeString read Get_VICMS write Set_VICMS;
    property VBCST: UnicodeString read Get_VBCST write Set_VBCST;
    property VST: UnicodeString read Get_VST write Set_VST;
    property VProd: UnicodeString read Get_VProd write Set_VProd;
    property VNF: UnicodeString read Get_VNF write Set_VNF;
    property NCFOP: UnicodeString read Get_NCFOP write Set_NCFOP;
    property NPeso: UnicodeString read Get_NPeso write Set_NPeso;
    property PIN: UnicodeString read Get_PIN write Set_PIN;
    property DPrev: UnicodeString read Get_DPrev write Set_DPrev;
    property InfUnidTransp: IXMLTUnidadeTranspList read Get_InfUnidTransp;
    property InfUnidCarga: IXMLTUnidCargaList read Get_InfUnidCarga;
  end;

{ IXMLInfNFList }

  IXMLInfNFList = interface(IXMLNodeCollection)
    ['{DAB643FC-403B-44FA-A77D-E20485D4396F}']
    { Methods & Properties }
    function Add: IXMLInfNF;
    function Insert(const Index: Integer): IXMLInfNF;

    function Get_Item(Index: Integer): IXMLInfNF;
    property Items[Index: Integer]: IXMLInfNF read Get_Item; default;
  end;

{ IXMLTUnidadeTransp }

  IXMLTUnidadeTransp = interface(IXMLNode)
    ['{48676D7C-9FA9-4377-913E-ACA6DEBE1BB1}']
    { Property Accessors }
    function Get_TpUnidTransp: UnicodeString;
    function Get_IdUnidTransp: UnicodeString;
    function Get_LacUnidTransp: IXMLLacUnidTranspList;
    function Get_InfUnidCarga: IXMLTUnidCargaList;
    function Get_QtdRat: UnicodeString;
    procedure Set_TpUnidTransp(Value: UnicodeString);
    procedure Set_IdUnidTransp(Value: UnicodeString);
    procedure Set_QtdRat(Value: UnicodeString);
    { Methods & Properties }
    property TpUnidTransp: UnicodeString read Get_TpUnidTransp write Set_TpUnidTransp;
    property IdUnidTransp: UnicodeString read Get_IdUnidTransp write Set_IdUnidTransp;
    property LacUnidTransp: IXMLLacUnidTranspList read Get_LacUnidTransp;
    property InfUnidCarga: IXMLTUnidCargaList read Get_InfUnidCarga;
    property QtdRat: UnicodeString read Get_QtdRat write Set_QtdRat;
  end;

{ IXMLTUnidadeTranspList }

  IXMLTUnidadeTranspList = interface(IXMLNodeCollection)
    ['{2FDA67AF-B612-4732-8230-ADCE4ABBA914}']
    { Methods & Properties }
    function Add: IXMLTUnidadeTransp;
    function Insert(const Index: Integer): IXMLTUnidadeTransp;

    function Get_Item(Index: Integer): IXMLTUnidadeTransp;
    property Items[Index: Integer]: IXMLTUnidadeTransp read Get_Item; default;
  end;

{ IXMLLacUnidTransp }

  IXMLLacUnidTransp = interface(IXMLNode)
    ['{6F32A973-CB26-49CF-AE21-21388AD9FD49}']
    { Property Accessors }
    function Get_NLacre: UnicodeString;
    procedure Set_NLacre(Value: UnicodeString);
    { Methods & Properties }
    property NLacre: UnicodeString read Get_NLacre write Set_NLacre;
  end;

{ IXMLLacUnidTranspList }

  IXMLLacUnidTranspList = interface(IXMLNodeCollection)
    ['{80AAC9DC-60B9-477D-B7B2-850276984105}']
    { Methods & Properties }
    function Add: IXMLLacUnidTransp;
    function Insert(const Index: Integer): IXMLLacUnidTransp;

    function Get_Item(Index: Integer): IXMLLacUnidTransp;
    property Items[Index: Integer]: IXMLLacUnidTransp read Get_Item; default;
  end;

{ IXMLTUnidCarga }

  IXMLTUnidCarga = interface(IXMLNode)
    ['{39891AC3-9651-4D0C-B7BC-F2D91C523708}']
    { Property Accessors }
    function Get_TpUnidCarga: UnicodeString;
    function Get_IdUnidCarga: UnicodeString;
    function Get_LacUnidCarga: IXMLLacUnidCargaList;
    function Get_QtdRat: UnicodeString;
    procedure Set_TpUnidCarga(Value: UnicodeString);
    procedure Set_IdUnidCarga(Value: UnicodeString);
    procedure Set_QtdRat(Value: UnicodeString);
    { Methods & Properties }
    property TpUnidCarga: UnicodeString read Get_TpUnidCarga write Set_TpUnidCarga;
    property IdUnidCarga: UnicodeString read Get_IdUnidCarga write Set_IdUnidCarga;
    property LacUnidCarga: IXMLLacUnidCargaList read Get_LacUnidCarga;
    property QtdRat: UnicodeString read Get_QtdRat write Set_QtdRat;
  end;

{ IXMLTUnidCargaList }

  IXMLTUnidCargaList = interface(IXMLNodeCollection)
    ['{C0823779-0F8F-4D30-AFF2-34779356A6AD}']
    { Methods & Properties }
    function Add: IXMLTUnidCarga;
    function Insert(const Index: Integer): IXMLTUnidCarga;

    function Get_Item(Index: Integer): IXMLTUnidCarga;
    property Items[Index: Integer]: IXMLTUnidCarga read Get_Item; default;
  end;

{ IXMLLacUnidCarga }

  IXMLLacUnidCarga = interface(IXMLNode)
    ['{18A7C622-8D56-4FE7-8AA2-DD3A7C125576}']
    { Property Accessors }
    function Get_NLacre: UnicodeString;
    procedure Set_NLacre(Value: UnicodeString);
    { Methods & Properties }
    property NLacre: UnicodeString read Get_NLacre write Set_NLacre;
  end;

{ IXMLLacUnidCargaList }

  IXMLLacUnidCargaList = interface(IXMLNodeCollection)
    ['{A2385FD6-C92A-4944-B718-786D4490F67F}']
    { Methods & Properties }
    function Add: IXMLLacUnidCarga;
    function Insert(const Index: Integer): IXMLLacUnidCarga;

    function Get_Item(Index: Integer): IXMLLacUnidCarga;
    property Items[Index: Integer]: IXMLLacUnidCarga read Get_Item; default;
  end;

{ IXMLInfNFe }

  IXMLInfNFe = interface(IXMLNode)
    ['{8DAE09BA-4602-4CC7-96F6-AE16BFBC5910}']
    { Property Accessors }
    function Get_Chave: UnicodeString;
    function Get_PIN: UnicodeString;
    function Get_DPrev: UnicodeString;
    function Get_InfUnidTransp: IXMLTUnidadeTranspList;
    function Get_InfUnidCarga: IXMLTUnidCargaList;
    procedure Set_Chave(Value: UnicodeString);
    procedure Set_PIN(Value: UnicodeString);
    procedure Set_DPrev(Value: UnicodeString);
    { Methods & Properties }
    property Chave: UnicodeString read Get_Chave write Set_Chave;
    property PIN: UnicodeString read Get_PIN write Set_PIN;
    property DPrev: UnicodeString read Get_DPrev write Set_DPrev;
    property InfUnidTransp: IXMLTUnidadeTranspList read Get_InfUnidTransp;
    property InfUnidCarga: IXMLTUnidCargaList read Get_InfUnidCarga;
  end;

{ IXMLInfNFeList }

  IXMLInfNFeList = interface(IXMLNodeCollection)
    ['{2E21EACB-61BC-408B-9C54-247787ADB27D}']
    { Methods & Properties }
    function Add: IXMLInfNFe;
    function Insert(const Index: Integer): IXMLInfNFe;

    function Get_Item(Index: Integer): IXMLInfNFe;
    property Items[Index: Integer]: IXMLInfNFe read Get_Item; default;
  end;

{ IXMLInfOutros }

  IXMLInfOutros = interface(IXMLNode)
    ['{196C5D6C-79D7-408D-A9B2-49F1CB4B5DD4}']
    { Property Accessors }
    function Get_TpDoc: UnicodeString;
    function Get_DescOutros: UnicodeString;
    function Get_NDoc: UnicodeString;
    function Get_DEmi: UnicodeString;
    function Get_VDocFisc: UnicodeString;
    function Get_DPrev: UnicodeString;
    function Get_InfUnidTransp: IXMLTUnidadeTranspList;
    function Get_InfUnidCarga: IXMLTUnidCargaList;
    procedure Set_TpDoc(Value: UnicodeString);
    procedure Set_DescOutros(Value: UnicodeString);
    procedure Set_NDoc(Value: UnicodeString);
    procedure Set_DEmi(Value: UnicodeString);
    procedure Set_VDocFisc(Value: UnicodeString);
    procedure Set_DPrev(Value: UnicodeString);
    { Methods & Properties }
    property TpDoc: UnicodeString read Get_TpDoc write Set_TpDoc;
    property DescOutros: UnicodeString read Get_DescOutros write Set_DescOutros;
    property NDoc: UnicodeString read Get_NDoc write Set_NDoc;
    property DEmi: UnicodeString read Get_DEmi write Set_DEmi;
    property VDocFisc: UnicodeString read Get_VDocFisc write Set_VDocFisc;
    property DPrev: UnicodeString read Get_DPrev write Set_DPrev;
    property InfUnidTransp: IXMLTUnidadeTranspList read Get_InfUnidTransp;
    property InfUnidCarga: IXMLTUnidCargaList read Get_InfUnidCarga;
  end;

{ IXMLInfOutrosList }

  IXMLInfOutrosList = interface(IXMLNodeCollection)
    ['{B1970B63-47DA-46DC-B0A4-2B0AA37CBF89}']
    { Methods & Properties }
    function Add: IXMLInfOutros;
    function Insert(const Index: Integer): IXMLInfOutros;

    function Get_Item(Index: Integer): IXMLInfOutros;
    property Items[Index: Integer]: IXMLInfOutros read Get_Item; default;
  end;

{ IXMLDocAnt }

  IXMLDocAnt = interface(IXMLNodeCollection)
    ['{AAFF77CF-97E2-4E23-A2EC-F34B2761BF63}']
    { Property Accessors }
    function Get_EmiDocAnt(Index: Integer): IXMLEmiDocAnt;
    { Methods & Properties }
    function Add: IXMLEmiDocAnt;
    function Insert(const Index: Integer): IXMLEmiDocAnt;
    property EmiDocAnt[Index: Integer]: IXMLEmiDocAnt read Get_EmiDocAnt; default;
  end;

{ IXMLEmiDocAnt }

  IXMLEmiDocAnt = interface(IXMLNode)
    ['{3767B74A-AFDC-4508-9114-D590ACD5D558}']
    { Property Accessors }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_IdDocAnt: IXMLIdDocAntList;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    { Methods & Properties }
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
    property IE: UnicodeString read Get_IE write Set_IE;
    property UF: UnicodeString read Get_UF write Set_UF;
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property IdDocAnt: IXMLIdDocAntList read Get_IdDocAnt;
  end;

{ IXMLIdDocAnt }

  IXMLIdDocAnt = interface(IXMLNode)
    ['{EB6EE581-2B8E-4DEA-A068-000991713392}']
    { Property Accessors }
    function Get_IdDocAntPap: IXMLIdDocAntPapList;
    function Get_IdDocAntEle: IXMLIdDocAntEleList;
    { Methods & Properties }
    property IdDocAntPap: IXMLIdDocAntPapList read Get_IdDocAntPap;
    property IdDocAntEle: IXMLIdDocAntEleList read Get_IdDocAntEle;
  end;

{ IXMLIdDocAntList }

  IXMLIdDocAntList = interface(IXMLNodeCollection)
    ['{9FB2E5C4-FC0C-4CCD-8597-238650EA205A}']
    { Methods & Properties }
    function Add: IXMLIdDocAnt;
    function Insert(const Index: Integer): IXMLIdDocAnt;

    function Get_Item(Index: Integer): IXMLIdDocAnt;
    property Items[Index: Integer]: IXMLIdDocAnt read Get_Item; default;
  end;

{ IXMLIdDocAntPap }

  IXMLIdDocAntPap = interface(IXMLNode)
    ['{388D60B3-73DC-4EE2-BD85-ADAFE8882A5E}']
    { Property Accessors }
    function Get_TpDoc: UnicodeString;
    function Get_Serie: UnicodeString;
    function Get_Subser: UnicodeString;
    function Get_NDoc: UnicodeString;
    function Get_DEmi: UnicodeString;
    procedure Set_TpDoc(Value: UnicodeString);
    procedure Set_Serie(Value: UnicodeString);
    procedure Set_Subser(Value: UnicodeString);
    procedure Set_NDoc(Value: UnicodeString);
    procedure Set_DEmi(Value: UnicodeString);
    { Methods & Properties }
    property TpDoc: UnicodeString read Get_TpDoc write Set_TpDoc;
    property Serie: UnicodeString read Get_Serie write Set_Serie;
    property Subser: UnicodeString read Get_Subser write Set_Subser;
    property NDoc: UnicodeString read Get_NDoc write Set_NDoc;
    property DEmi: UnicodeString read Get_DEmi write Set_DEmi;
  end;

{ IXMLIdDocAntPapList }

  IXMLIdDocAntPapList = interface(IXMLNodeCollection)
    ['{85230DD2-8D9B-4D45-AEFC-DC645F7EAD77}']
    { Methods & Properties }
    function Add: IXMLIdDocAntPap;
    function Insert(const Index: Integer): IXMLIdDocAntPap;

    function Get_Item(Index: Integer): IXMLIdDocAntPap;
    property Items[Index: Integer]: IXMLIdDocAntPap read Get_Item; default;
  end;

{ IXMLIdDocAntEle }

  IXMLIdDocAntEle = interface(IXMLNode)
    ['{804B1900-D35C-4308-A530-BEFE02EF1E35}']
    { Property Accessors }
    function Get_Chave: UnicodeString;
    procedure Set_Chave(Value: UnicodeString);
    { Methods & Properties }
    property Chave: UnicodeString read Get_Chave write Set_Chave;
  end;

{ IXMLIdDocAntEleList }

  IXMLIdDocAntEleList = interface(IXMLNodeCollection)
    ['{BBC1C35B-1EF0-4E5F-A748-CF296BED1A6D}']
    { Methods & Properties }
    function Add: IXMLIdDocAntEle;
    function Insert(const Index: Integer): IXMLIdDocAntEle;

    function Get_Item(Index: Integer): IXMLIdDocAntEle;
    property Items[Index: Integer]: IXMLIdDocAntEle read Get_Item; default;
  end;

{ IXMLSeg }

  IXMLSeg = interface(IXMLNode)
    ['{2DAC8186-36B3-4628-A372-C8545742211E}']
    { Property Accessors }
    function Get_RespSeg: UnicodeString;
    function Get_XSeg: UnicodeString;
    function Get_NApol: UnicodeString;
    function Get_NAver: UnicodeString;
    function Get_VCarga: UnicodeString;
    procedure Set_RespSeg(Value: UnicodeString);
    procedure Set_XSeg(Value: UnicodeString);
    procedure Set_NApol(Value: UnicodeString);
    procedure Set_NAver(Value: UnicodeString);
    procedure Set_VCarga(Value: UnicodeString);
    { Methods & Properties }
    property RespSeg: UnicodeString read Get_RespSeg write Set_RespSeg;
    property XSeg: UnicodeString read Get_XSeg write Set_XSeg;
    property NApol: UnicodeString read Get_NApol write Set_NApol;
    property NAver: UnicodeString read Get_NAver write Set_NAver;
    property VCarga: UnicodeString read Get_VCarga write Set_VCarga;
  end;

{ IXMLSegList }

  IXMLSegList = interface(IXMLNodeCollection)
    ['{E3BF7302-EC49-4FE7-8148-FCBC1C57B143}']
    { Methods & Properties }
    function Add: IXMLSeg;
    function Insert(const Index: Integer): IXMLSeg;

    function Get_Item(Index: Integer): IXMLSeg;
    property Items[Index: Integer]: IXMLSeg read Get_Item; default;
  end;

{ IXMLInfModal }

  IXMLInfModal = interface(IXMLNode)
    ['{22B3913E-F8AF-422D-A5DE-47A76476B4DF}']
    { Property Accessors }
    function Get_VersaoModal: UnicodeString;
    procedure Set_VersaoModal(Value: UnicodeString);
    { Methods & Properties }
    property VersaoModal: UnicodeString read Get_VersaoModal write Set_VersaoModal;
  end;

{ IXMLPeri }

  IXMLPeri = interface(IXMLNode)
    ['{E4C56768-1939-4884-9641-1479F4D55581}']
    { Property Accessors }
    function Get_NONU: UnicodeString;
    function Get_XNomeAE: UnicodeString;
    function Get_XClaRisco: UnicodeString;
    function Get_GrEmb: UnicodeString;
    function Get_QTotProd: UnicodeString;
    function Get_QVolTipo: UnicodeString;
    function Get_PontoFulgor: UnicodeString;
    procedure Set_NONU(Value: UnicodeString);
    procedure Set_XNomeAE(Value: UnicodeString);
    procedure Set_XClaRisco(Value: UnicodeString);
    procedure Set_GrEmb(Value: UnicodeString);
    procedure Set_QTotProd(Value: UnicodeString);
    procedure Set_QVolTipo(Value: UnicodeString);
    procedure Set_PontoFulgor(Value: UnicodeString);
    { Methods & Properties }
    property NONU: UnicodeString read Get_NONU write Set_NONU;
    property XNomeAE: UnicodeString read Get_XNomeAE write Set_XNomeAE;
    property XClaRisco: UnicodeString read Get_XClaRisco write Set_XClaRisco;
    property GrEmb: UnicodeString read Get_GrEmb write Set_GrEmb;
    property QTotProd: UnicodeString read Get_QTotProd write Set_QTotProd;
    property QVolTipo: UnicodeString read Get_QVolTipo write Set_QVolTipo;
    property PontoFulgor: UnicodeString read Get_PontoFulgor write Set_PontoFulgor;
  end;

{ IXMLPeriList }

  IXMLPeriList = interface(IXMLNodeCollection)
    ['{B026D0D6-C19B-4AF2-9244-D7FB4B6F67C3}']
    { Methods & Properties }
    function Add: IXMLPeri;
    function Insert(const Index: Integer): IXMLPeri;

    function Get_Item(Index: Integer): IXMLPeri;
    property Items[Index: Integer]: IXMLPeri read Get_Item; default;
  end;

{ IXMLVeicNovos }

  IXMLVeicNovos = interface(IXMLNode)
    ['{1B7FAABA-BC2F-4C2C-A77D-37D1687CD467}']
    { Property Accessors }
    function Get_Chassi: UnicodeString;
    function Get_CCor: UnicodeString;
    function Get_XCor: UnicodeString;
    function Get_CMod: UnicodeString;
    function Get_VUnit: UnicodeString;
    function Get_VFrete: UnicodeString;
    procedure Set_Chassi(Value: UnicodeString);
    procedure Set_CCor(Value: UnicodeString);
    procedure Set_XCor(Value: UnicodeString);
    procedure Set_CMod(Value: UnicodeString);
    procedure Set_VUnit(Value: UnicodeString);
    procedure Set_VFrete(Value: UnicodeString);
    { Methods & Properties }
    property Chassi: UnicodeString read Get_Chassi write Set_Chassi;
    property CCor: UnicodeString read Get_CCor write Set_CCor;
    property XCor: UnicodeString read Get_XCor write Set_XCor;
    property CMod: UnicodeString read Get_CMod write Set_CMod;
    property VUnit: UnicodeString read Get_VUnit write Set_VUnit;
    property VFrete: UnicodeString read Get_VFrete write Set_VFrete;
  end;

{ IXMLVeicNovosList }

  IXMLVeicNovosList = interface(IXMLNodeCollection)
    ['{F8C740C0-94CF-4253-B3CE-78FF2191BBBE}']
    { Methods & Properties }
    function Add: IXMLVeicNovos;
    function Insert(const Index: Integer): IXMLVeicNovos;

    function Get_Item(Index: Integer): IXMLVeicNovos;
    property Items[Index: Integer]: IXMLVeicNovos read Get_Item; default;
  end;

{ IXMLCobr }

  IXMLCobr = interface(IXMLNode)
    ['{3FEE5AB8-58A8-4CCE-9B1D-3CCCC74A058F}']
    { Property Accessors }
    function Get_Fat: IXMLFat;
    function Get_Dup: IXMLDupList;
    { Methods & Properties }
    property Fat: IXMLFat read Get_Fat;
    property Dup: IXMLDupList read Get_Dup;
  end;

{ IXMLFat }

  IXMLFat = interface(IXMLNode)
    ['{3588B4C8-2082-48AD-965C-7F32E89F34A2}']
    { Property Accessors }
    function Get_NFat: UnicodeString;
    function Get_VOrig: UnicodeString;
    function Get_VDesc: UnicodeString;
    function Get_VLiq: UnicodeString;
    procedure Set_NFat(Value: UnicodeString);
    procedure Set_VOrig(Value: UnicodeString);
    procedure Set_VDesc(Value: UnicodeString);
    procedure Set_VLiq(Value: UnicodeString);
    { Methods & Properties }
    property NFat: UnicodeString read Get_NFat write Set_NFat;
    property VOrig: UnicodeString read Get_VOrig write Set_VOrig;
    property VDesc: UnicodeString read Get_VDesc write Set_VDesc;
    property VLiq: UnicodeString read Get_VLiq write Set_VLiq;
  end;

{ IXMLDup }

  IXMLDup = interface(IXMLNode)
    ['{56FA47FB-5F1F-4DAA-A71C-8315E906B2B5}']
    { Property Accessors }
    function Get_NDup: UnicodeString;
    function Get_DVenc: UnicodeString;
    function Get_VDup: UnicodeString;
    procedure Set_NDup(Value: UnicodeString);
    procedure Set_DVenc(Value: UnicodeString);
    procedure Set_VDup(Value: UnicodeString);
    { Methods & Properties }
    property NDup: UnicodeString read Get_NDup write Set_NDup;
    property DVenc: UnicodeString read Get_DVenc write Set_DVenc;
    property VDup: UnicodeString read Get_VDup write Set_VDup;
  end;

{ IXMLDupList }

  IXMLDupList = interface(IXMLNodeCollection)
    ['{A2EC2236-180F-4BC5-A87B-331E273D516E}']
    { Methods & Properties }
    function Add: IXMLDup;
    function Insert(const Index: Integer): IXMLDup;

    function Get_Item(Index: Integer): IXMLDup;
    property Items[Index: Integer]: IXMLDup read Get_Item; default;
  end;

{ IXMLInfCteSub }

  IXMLInfCteSub = interface(IXMLNode)
    ['{5069A126-6BBB-4148-A6C7-8D4524EA8C43}']
    { Property Accessors }
    function Get_ChCte: UnicodeString;
    function Get_TomaICMS: IXMLTomaICMS;
    function Get_TomaNaoICMS: IXMLTomaNaoICMS;
    procedure Set_ChCte(Value: UnicodeString);
    { Methods & Properties }
    property ChCte: UnicodeString read Get_ChCte write Set_ChCte;
    property TomaICMS: IXMLTomaICMS read Get_TomaICMS;
    property TomaNaoICMS: IXMLTomaNaoICMS read Get_TomaNaoICMS;
  end;

{ IXMLTomaICMS }

  IXMLTomaICMS = interface(IXMLNode)
    ['{06B348E6-B5A6-426E-A01F-D35A1923FA8C}']
    { Property Accessors }
    function Get_RefNFe: UnicodeString;
    function Get_RefNF: IXMLRefNF;
    function Get_RefCte: UnicodeString;
    procedure Set_RefNFe(Value: UnicodeString);
    procedure Set_RefCte(Value: UnicodeString);
    { Methods & Properties }
    property RefNFe: UnicodeString read Get_RefNFe write Set_RefNFe;
    property RefNF: IXMLRefNF read Get_RefNF;
    property RefCte: UnicodeString read Get_RefCte write Set_RefCte;
  end;

{ IXMLRefNF }

  IXMLRefNF = interface(IXMLNode)
    ['{F5ED3FA4-E1A8-46EB-BFE1-D12243259D21}']
    { Property Accessors }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_Mod_: UnicodeString;
    function Get_Serie: UnicodeString;
    function Get_Subserie: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_Valor: UnicodeString;
    function Get_DEmi: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_Mod_(Value: UnicodeString);
    procedure Set_Serie(Value: UnicodeString);
    procedure Set_Subserie(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_Valor(Value: UnicodeString);
    procedure Set_DEmi(Value: UnicodeString);
    { Methods & Properties }
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
    property Mod_: UnicodeString read Get_Mod_ write Set_Mod_;
    property Serie: UnicodeString read Get_Serie write Set_Serie;
    property Subserie: UnicodeString read Get_Subserie write Set_Subserie;
    property Nro: UnicodeString read Get_Nro write Set_Nro;
    property Valor: UnicodeString read Get_Valor write Set_Valor;
    property DEmi: UnicodeString read Get_DEmi write Set_DEmi;
  end;

{ IXMLTomaNaoICMS }

  IXMLTomaNaoICMS = interface(IXMLNode)
    ['{FAD5A684-F70A-4A87-8C0E-57595315284B}']
    { Property Accessors }
    function Get_RefCteAnu: UnicodeString;
    procedure Set_RefCteAnu(Value: UnicodeString);
    { Methods & Properties }
    property RefCteAnu: UnicodeString read Get_RefCteAnu write Set_RefCteAnu;
  end;

{ IXMLInfCteComp }

  IXMLInfCteComp = interface(IXMLNode)
    ['{3894992C-4709-420F-BC7D-7ED74380ED68}']
    { Property Accessors }
    function Get_Chave: UnicodeString;
    procedure Set_Chave(Value: UnicodeString);
    { Methods & Properties }
    property Chave: UnicodeString read Get_Chave write Set_Chave;
  end;

{ IXMLInfCteAnu }

  IXMLInfCteAnu = interface(IXMLNode)
    ['{4980685E-03B9-4E1D-AC48-BAA23C9D47B3}']
    { Property Accessors }
    function Get_ChCte: UnicodeString;
    function Get_DEmi: UnicodeString;
    procedure Set_ChCte(Value: UnicodeString);
    procedure Set_DEmi(Value: UnicodeString);
    { Methods & Properties }
    property ChCte: UnicodeString read Get_ChCte write Set_ChCte;
    property DEmi: UnicodeString read Get_DEmi write Set_DEmi;
  end;

{ IXMLAutXML }

  IXMLAutXML = interface(IXMLNode)
    ['{AA1A587F-DA74-4894-937D-AC56725BA670}']
    { Property Accessors }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    { Methods & Properties }
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
  end;

{ IXMLAutXMLList }

  IXMLAutXMLList = interface(IXMLNodeCollection)
    ['{E5FE5A8E-7AB7-4AB9-A5BD-86C59E4A51AE}']
    { Methods & Properties }
    function Add: IXMLAutXML;
    function Insert(const Index: Integer): IXMLAutXML;

    function Get_Item(Index: Integer): IXMLAutXML;
    property Items[Index: Integer]: IXMLAutXML read Get_Item; default;
  end;

{ IXMLSignatureType_ds }

  IXMLSignatureType_ds = interface(IXMLNode)
    ['{DC22A345-F66A-474A-858C-BE6DC03FD74E}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_SignedInfo: IXMLSignedInfoType_ds;
    function Get_SignatureValue: IXMLSignatureValueType_ds;
    function Get_KeyInfo: IXMLKeyInfoType_ds;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property SignedInfo: IXMLSignedInfoType_ds read Get_SignedInfo;
    property SignatureValue: IXMLSignatureValueType_ds read Get_SignatureValue;
    property KeyInfo: IXMLKeyInfoType_ds read Get_KeyInfo;
  end;

{ IXMLSignedInfoType_ds }

  IXMLSignedInfoType_ds = interface(IXMLNode)
    ['{18E7B97C-1D6D-4F5A-92F0-62F95A3F698B}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_CanonicalizationMethod: IXMLCanonicalizationMethod_ds;
    function Get_SignatureMethod: IXMLSignatureMethod_ds;
    function Get_Reference: IXMLReferenceType_ds;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property CanonicalizationMethod: IXMLCanonicalizationMethod_ds read Get_CanonicalizationMethod;
    property SignatureMethod: IXMLSignatureMethod_ds read Get_SignatureMethod;
    property Reference: IXMLReferenceType_ds read Get_Reference;
  end;

{ IXMLCanonicalizationMethod_ds }

  IXMLCanonicalizationMethod_ds = interface(IXMLNode)
    ['{70539B28-3262-4659-A03D-B393BADDEE70}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLSignatureMethod_ds }

  IXMLSignatureMethod_ds = interface(IXMLNode)
    ['{8D1750DA-D95F-49E3-B123-962BAFDE0E8D}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLReferenceType_ds }

  IXMLReferenceType_ds = interface(IXMLNode)
    ['{4054A5FC-AAA1-4522-989B-BF435969DD63}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_URI: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_Transforms: IXMLTransformsType_ds;
    function Get_DigestMethod: IXMLDigestMethod_ds;
    function Get_DigestValue: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_URI(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_DigestValue(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property URI: UnicodeString read Get_URI write Set_URI;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property Transforms: IXMLTransformsType_ds read Get_Transforms;
    property DigestMethod: IXMLDigestMethod_ds read Get_DigestMethod;
    property DigestValue: UnicodeString read Get_DigestValue write Set_DigestValue;
  end;

{ IXMLTransformsType_ds }

  IXMLTransformsType_ds = interface(IXMLNodeCollection)
    ['{1CB770F6-CC74-4DAA-B69B-8723EF24579A}']
    { Property Accessors }
    function Get_Transform(Index: Integer): IXMLTransformType_ds;
    { Methods & Properties }
    function Add: IXMLTransformType_ds;
    function Insert(const Index: Integer): IXMLTransformType_ds;
    property Transform[Index: Integer]: IXMLTransformType_ds read Get_Transform; default;
  end;

{ IXMLTransformType_ds }

  IXMLTransformType_ds = interface(IXMLNodeCollection)
    ['{A68BA61F-44EB-4754-9451-ACF3DF0975F5}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    function Get_XPath(Index: Integer): UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    function Add(const XPath: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
    property XPath[Index: Integer]: UnicodeString read Get_XPath; default;
  end;

{ IXMLDigestMethod_ds }

  IXMLDigestMethod_ds = interface(IXMLNode)
    ['{955B6F2A-65AD-4717-9A13-DD32AA03158C}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLSignatureValueType_ds }

  IXMLSignatureValueType_ds = interface(IXMLNode)
    ['{6ACBB4EC-C076-4E46-8A86-538EAF28A61D}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
  end;

{ IXMLKeyInfoType_ds }

  IXMLKeyInfoType_ds = interface(IXMLNode)
    ['{715595A4-3000-4C4A-8EEB-1392F7DC69CE}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_X509Data: IXMLX509DataType_ds;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property X509Data: IXMLX509DataType_ds read Get_X509Data;
  end;

{ IXMLX509DataType_ds }

  IXMLX509DataType_ds = interface(IXMLNode)
    ['{69EB9B5B-502F-44CD-8506-8450C5FD2F57}']
    { Property Accessors }
    function Get_X509Certificate: UnicodeString;
    procedure Set_X509Certificate(Value: UnicodeString);
    { Methods & Properties }
    property X509Certificate: UnicodeString read Get_X509Certificate write Set_X509Certificate;
  end;

{ IXMLTEnviCTe }

  IXMLTEnviCTe = interface(IXMLNode)
    ['{F3F3F66C-BCE2-4169-AA92-3BC07808CDD3}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_IdLote: UnicodeString;
    function Get_CTe: IXMLTCTeList;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_IdLote(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property IdLote: UnicodeString read Get_IdLote write Set_IdLote;
    property CTe: IXMLTCTeList read Get_CTe;
  end;

{ IXMLTRetEnviCTe }

  IXMLTRetEnviCTe = interface(IXMLNode)
    ['{248414A3-3CFE-438D-8A5B-481D243EA1E1}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_InfRec: IXMLInfRec;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property CUF: UnicodeString read Get_CUF write Set_CUF;
    property VerAplic: UnicodeString read Get_VerAplic write Set_VerAplic;
    property CStat: UnicodeString read Get_CStat write Set_CStat;
    property XMotivo: UnicodeString read Get_XMotivo write Set_XMotivo;
    property InfRec: IXMLInfRec read Get_InfRec;
  end;

{ IXMLInfRec }

  IXMLInfRec = interface(IXMLNode)
    ['{48A03058-2272-4062-9397-BC54B1D12959}']
    { Property Accessors }
    function Get_NRec: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_TMed: Integer;
    procedure Set_NRec(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_TMed(Value: Integer);
    { Methods & Properties }
    property NRec: UnicodeString read Get_NRec write Set_NRec;
    property DhRecbto: UnicodeString read Get_DhRecbto write Set_DhRecbto;
    property TMed: Integer read Get_TMed write Set_TMed;
  end;

{ IXMLTEndernac }

  IXMLTEndernac = interface(IXMLNode)
    ['{5CA87D0D-4D34-4673-9DB8-2983DA4D5FDA}']
    { Property Accessors }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    { Methods & Properties }
    property XLgr: UnicodeString read Get_XLgr write Set_XLgr;
    property Nro: UnicodeString read Get_Nro write Set_Nro;
    property XCpl: UnicodeString read Get_XCpl write Set_XCpl;
    property XBairro: UnicodeString read Get_XBairro write Set_XBairro;
    property CMun: UnicodeString read Get_CMun write Set_CMun;
    property XMun: UnicodeString read Get_XMun write Set_XMun;
    property CEP: UnicodeString read Get_CEP write Set_CEP;
    property UF: UnicodeString read Get_UF write Set_UF;
  end;

{ IXMLTEndOrg }

  IXMLTEndOrg = interface(IXMLNode)
    ['{32816518-44DC-4461-A602-60DF8F94AF01}']
    { Property Accessors }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_CPais: UnicodeString;
    function Get_XPais: UnicodeString;
    function Get_Fone: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_CPais(Value: UnicodeString);
    procedure Set_XPais(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    { Methods & Properties }
    property XLgr: UnicodeString read Get_XLgr write Set_XLgr;
    property Nro: UnicodeString read Get_Nro write Set_Nro;
    property XCpl: UnicodeString read Get_XCpl write Set_XCpl;
    property XBairro: UnicodeString read Get_XBairro write Set_XBairro;
    property CMun: UnicodeString read Get_CMun write Set_CMun;
    property XMun: UnicodeString read Get_XMun write Set_XMun;
    property CEP: UnicodeString read Get_CEP write Set_CEP;
    property UF: UnicodeString read Get_UF write Set_UF;
    property CPais: UnicodeString read Get_CPais write Set_CPais;
    property XPais: UnicodeString read Get_XPais write Set_XPais;
    property Fone: UnicodeString read Get_Fone write Set_Fone;
  end;

{ IXMLTLocal }

  IXMLTLocal = interface(IXMLNode)
    ['{26C00E71-2744-431E-A3EF-2FC63C7F1770}']
    { Property Accessors }
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    { Methods & Properties }
    property CMun: UnicodeString read Get_CMun write Set_CMun;
    property XMun: UnicodeString read Get_XMun write Set_XMun;
    property UF: UnicodeString read Get_UF write Set_UF;
  end;

{ IXMLTEndReEnt }

  IXMLTEndReEnt = interface(IXMLNode)
    ['{B3461067-A793-49B9-9022-3A7C86288144}']
    { Property Accessors }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    { Methods & Properties }
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property XLgr: UnicodeString read Get_XLgr write Set_XLgr;
    property Nro: UnicodeString read Get_Nro write Set_Nro;
    property XCpl: UnicodeString read Get_XCpl write Set_XCpl;
    property XBairro: UnicodeString read Get_XBairro write Set_XBairro;
    property CMun: UnicodeString read Get_CMun write Set_CMun;
    property XMun: UnicodeString read Get_XMun write Set_XMun;
    property UF: UnicodeString read Get_UF write Set_UF;
  end;

{ Forward Decls }

  TXMLRodo = class;
  TXMLRodo_occ = class;
  TXMLRodo_occList = class;
  TXMLRodo_occ_emiOcc = class;
  TXMLRodo_valePed = class;
  TXMLRodo_valePedList = class;
  TXMLRodo_veic = class;
  TXMLRodo_veicList = class;
  TXMLRodo_veic_prop = class;
  TXMLRodo_lacRodo = class;
  TXMLRodo_lacRodoList = class;
  TXMLRodo_moto = class;
  TXMLRodo_motoList = class;
  TXMLTCTe = class;
  TXMLTCTeList = class;
  TXMLInfCte = class;
  TXMLIde = class;
  TXMLToma03 = class;
  TXMLToma4 = class;
  TXMLTEndereco = class;
  TXMLCompl = class;
  TXMLFluxo = class;
  TXMLPass = class;
  TXMLPassList = class;
  TXMLEntrega = class;
  TXMLSemData = class;
  TXMLComData = class;
  TXMLNoPeriodo = class;
  TXMLSemHora = class;
  TXMLComHora = class;
  TXMLNoInter = class;
  TXMLObsCont = class;
  TXMLObsContList = class;
  TXMLObsFisco = class;
  TXMLObsFiscoList = class;
  TXMLEmit = class;
  TXMLTEndeEmi = class;
  TXMLRem = class;
  TXMLExped = class;
  TXMLReceb = class;
  TXMLDest = class;
  TXMLVPrest = class;
  TXMLComp = class;
  TXMLCompList = class;
  TXMLImp = class;
  TXMLTImp = class;
  TXMLICMS00 = class;
  TXMLICMS20 = class;
  TXMLICMS45 = class;
  TXMLICMS60 = class;
  TXMLICMS90 = class;
  TXMLICMSOutraUF = class;
  TXMLICMSSN = class;
  TXMLICMSUFFim = class;
  TXMLInfCTeNorm = class;
  TXMLInfCarga = class;
  TXMLInfQ = class;
  TXMLInfQList = class;
  TXMLInfDoc = class;
  TXMLInfNF = class;
  TXMLInfNFList = class;
  TXMLTUnidadeTransp = class;
  TXMLTUnidadeTranspList = class;
  TXMLLacUnidTransp = class;
  TXMLLacUnidTranspList = class;
  TXMLTUnidCarga = class;
  TXMLTUnidCargaList = class;
  TXMLLacUnidCarga = class;
  TXMLLacUnidCargaList = class;
  TXMLInfNFe = class;
  TXMLInfNFeList = class;
  TXMLInfOutros = class;
  TXMLInfOutrosList = class;
  TXMLDocAnt = class;
  TXMLEmiDocAnt = class;
  TXMLIdDocAnt = class;
  TXMLIdDocAntList = class;
  TXMLIdDocAntPap = class;
  TXMLIdDocAntPapList = class;
  TXMLIdDocAntEle = class;
  TXMLIdDocAntEleList = class;
  TXMLSeg = class;
  TXMLSegList = class;
  TXMLInfModal = class;
  TXMLPeri = class;
  TXMLPeriList = class;
  TXMLVeicNovos = class;
  TXMLVeicNovosList = class;
  TXMLCobr = class;
  TXMLFat = class;
  TXMLDup = class;
  TXMLDupList = class;
  TXMLInfCteSub = class;
  TXMLTomaICMS = class;
  TXMLRefNF = class;
  TXMLTomaNaoICMS = class;
  TXMLInfCteComp = class;
  TXMLInfCteAnu = class;
  TXMLAutXML = class;
  TXMLAutXMLList = class;
  TXMLSignatureType_ds = class;
  TXMLSignedInfoType_ds = class;
  TXMLCanonicalizationMethod_ds = class;
  TXMLSignatureMethod_ds = class;
  TXMLReferenceType_ds = class;
  TXMLTransformsType_ds = class;
  TXMLTransformType_ds = class;
  TXMLDigestMethod_ds = class;
  TXMLSignatureValueType_ds = class;
  TXMLKeyInfoType_ds = class;
  TXMLX509DataType_ds = class;
  TXMLTEnviCTe = class;
  TXMLTRetEnviCTe = class;
  TXMLInfRec = class;
  TXMLTEndernac = class;
  TXMLTEndOrg = class;
  TXMLTLocal = class;
  TXMLTEndReEnt = class;

{ TXMLRodo }

  TXMLRodo = class(TXMLNode, IXMLRodo)
  private
    FOcc: IXMLRodo_occList;
    FValePed: IXMLRodo_valePedList;
    FVeic: IXMLRodo_veicList;
    FLacRodo: IXMLRodo_lacRodoList;
    FMoto: IXMLRodo_motoList;
  protected
    { IXMLRodo }
    function Get_RNTRC: UnicodeString;
    function Get_DPrev: UnicodeString;
    function Get_Lota: UnicodeString;
    function Get_CIOT: UnicodeString;
    function Get_Occ: IXMLRodo_occList;
    function Get_ValePed: IXMLRodo_valePedList;
    function Get_Veic: IXMLRodo_veicList;
    function Get_LacRodo: IXMLRodo_lacRodoList;
    function Get_Moto: IXMLRodo_motoList;
    procedure Set_RNTRC(Value: UnicodeString);
    procedure Set_DPrev(Value: UnicodeString);
    procedure Set_Lota(Value: UnicodeString);
    procedure Set_CIOT(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRodo_occ }

  TXMLRodo_occ = class(TXMLNode, IXMLRodo_occ)
  protected
    { IXMLRodo_occ }
    function Get_Serie: UnicodeString;
    function Get_NOcc: UnicodeString;
    function Get_DEmi: UnicodeString;
    function Get_EmiOcc: IXMLRodo_occ_emiOcc;
    procedure Set_Serie(Value: UnicodeString);
    procedure Set_NOcc(Value: UnicodeString);
    procedure Set_DEmi(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRodo_occList }

  TXMLRodo_occList = class(TXMLNodeCollection, IXMLRodo_occList)
  protected
    { IXMLRodo_occList }
    function Add: IXMLRodo_occ;
    function Insert(const Index: Integer): IXMLRodo_occ;

    function Get_Item(Index: Integer): IXMLRodo_occ;
  end;

{ TXMLRodo_occ_emiOcc }

  TXMLRodo_occ_emiOcc = class(TXMLNode, IXMLRodo_occ_emiOcc)
  protected
    { IXMLRodo_occ_emiOcc }
    function Get_CNPJ: UnicodeString;
    function Get_CInt: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_Fone: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CInt(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
  end;

{ TXMLRodo_valePed }

  TXMLRodo_valePed = class(TXMLNode, IXMLRodo_valePed)
  protected
    { IXMLRodo_valePed }
    function Get_CNPJForn: UnicodeString;
    function Get_NCompra: UnicodeString;
    function Get_CNPJPg: UnicodeString;
    function Get_VValePed: UnicodeString;
    procedure Set_CNPJForn(Value: UnicodeString);
    procedure Set_NCompra(Value: UnicodeString);
    procedure Set_CNPJPg(Value: UnicodeString);
    procedure Set_VValePed(Value: UnicodeString);
  end;

{ TXMLRodo_valePedList }

  TXMLRodo_valePedList = class(TXMLNodeCollection, IXMLRodo_valePedList)
  protected
    { IXMLRodo_valePedList }
    function Add: IXMLRodo_valePed;
    function Insert(const Index: Integer): IXMLRodo_valePed;

    function Get_Item(Index: Integer): IXMLRodo_valePed;
  end;

{ TXMLRodo_veic }

  TXMLRodo_veic = class(TXMLNode, IXMLRodo_veic)
  protected
    { IXMLRodo_veic }
    function Get_CInt: UnicodeString;
    function Get_RENAVAM: UnicodeString;
    function Get_Placa: UnicodeString;
    function Get_Tara: UnicodeString;
    function Get_CapKG: UnicodeString;
    function Get_CapM3: UnicodeString;
    function Get_TpProp: UnicodeString;
    function Get_TpVeic: UnicodeString;
    function Get_TpRod: UnicodeString;
    function Get_TpCar: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_Prop: IXMLRodo_veic_prop;
    procedure Set_CInt(Value: UnicodeString);
    procedure Set_RENAVAM(Value: UnicodeString);
    procedure Set_Placa(Value: UnicodeString);
    procedure Set_Tara(Value: UnicodeString);
    procedure Set_CapKG(Value: UnicodeString);
    procedure Set_CapM3(Value: UnicodeString);
    procedure Set_TpProp(Value: UnicodeString);
    procedure Set_TpVeic(Value: UnicodeString);
    procedure Set_TpRod(Value: UnicodeString);
    procedure Set_TpCar(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRodo_veicList }

  TXMLRodo_veicList = class(TXMLNodeCollection, IXMLRodo_veicList)
  protected
    { IXMLRodo_veicList }
    function Add: IXMLRodo_veic;
    function Insert(const Index: Integer): IXMLRodo_veic;

    function Get_Item(Index: Integer): IXMLRodo_veic;
  end;

{ TXMLRodo_veic_prop }

  TXMLRodo_veic_prop = class(TXMLNode, IXMLRodo_veic_prop)
  protected
    { IXMLRodo_veic_prop }
    function Get_CPF: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_RNTRC: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_TpProp: UnicodeString;
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_RNTRC(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_TpProp(Value: UnicodeString);
  end;

{ TXMLRodo_lacRodo }

  TXMLRodo_lacRodo = class(TXMLNode, IXMLRodo_lacRodo)
  protected
    { IXMLRodo_lacRodo }
    function Get_NLacre: UnicodeString;
    procedure Set_NLacre(Value: UnicodeString);
  end;

{ TXMLRodo_lacRodoList }

  TXMLRodo_lacRodoList = class(TXMLNodeCollection, IXMLRodo_lacRodoList)
  protected
    { IXMLRodo_lacRodoList }
    function Add: IXMLRodo_lacRodo;
    function Insert(const Index: Integer): IXMLRodo_lacRodo;

    function Get_Item(Index: Integer): IXMLRodo_lacRodo;
  end;

{ TXMLRodo_moto }

  TXMLRodo_moto = class(TXMLNode, IXMLRodo_moto)
  protected
    { IXMLRodo_moto }
    function Get_XNome: UnicodeString;
    function Get_CPF: UnicodeString;
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
  end;

{ TXMLRodo_motoList }

  TXMLRodo_motoList = class(TXMLNodeCollection, IXMLRodo_motoList)
  protected
    { IXMLRodo_motoList }
    function Add: IXMLRodo_moto;
    function Insert(const Index: Integer): IXMLRodo_moto;

    function Get_Item(Index: Integer): IXMLRodo_moto;
  end;

{ TXMLTCTe }

  TXMLTCTe = class(TXMLNode, IXMLTCTe)
  protected
    { IXMLTCTe }
    function Get_InfCte: IXMLInfCte;
    function Get_Signature: IXMLSignatureType_ds;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTCTeList }

  TXMLTCTeList = class(TXMLNodeCollection, IXMLTCTeList)
  protected
    { IXMLTCTeList }
    function Add: IXMLTCTe;
    function Insert(const Index: Integer): IXMLTCTe;

    function Get_Item(Index: Integer): IXMLTCTe;
  end;

{ TXMLInfCte }

  TXMLInfCte = class(TXMLNode, IXMLInfCte)
  private
    FAutXML: IXMLAutXMLList;
  protected
    { IXMLInfCte }
    function Get_Versao: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Ide: IXMLIde;
    function Get_Compl: IXMLCompl;
    function Get_Emit: IXMLEmit;
    function Get_Rem: IXMLRem;
    function Get_Exped: IXMLExped;
    function Get_Receb: IXMLReceb;
    function Get_Dest: IXMLDest;
    function Get_VPrest: IXMLVPrest;
    function Get_Imp: IXMLImp;
    function Get_InfCTeNorm: IXMLInfCTeNorm;
    function Get_InfCteComp: IXMLInfCteComp;
    function Get_InfCteAnu: IXMLInfCteAnu;
    function Get_AutXML: IXMLAutXMLList;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLIde }

  TXMLIde = class(TXMLNode, IXMLIde)
  protected
    { IXMLIde }
    function Get_CUF: UnicodeString;
    function Get_CCT: UnicodeString;
    function Get_CFOP: UnicodeString;
    function Get_NatOp: UnicodeString;
    function Get_ForPag: UnicodeString;
    function Get_Mod_: UnicodeString;
    function Get_Serie: UnicodeString;
    function Get_NCT: UnicodeString;
    function Get_DhEmi: UnicodeString;
    function Get_TpImp: UnicodeString;
    function Get_TpEmis: UnicodeString;
    function Get_CDV: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_TpCTe: UnicodeString;
    function Get_ProcEmi: UnicodeString;
    function Get_VerProc: UnicodeString;
    function Get_RefCTE: UnicodeString;
    function Get_CMunEnv: UnicodeString;
    function Get_XMunEnv: UnicodeString;
    function Get_UFEnv: UnicodeString;
    function Get_Modal: UnicodeString;
    function Get_TpServ: UnicodeString;
    function Get_CMunIni: UnicodeString;
    function Get_XMunIni: UnicodeString;
    function Get_UFIni: UnicodeString;
    function Get_CMunFim: UnicodeString;
    function Get_XMunFim: UnicodeString;
    function Get_UFFim: UnicodeString;
    function Get_Retira: UnicodeString;
    function Get_XDetRetira: UnicodeString;
    function Get_Toma03: IXMLToma03;
    function Get_Toma4: IXMLToma4;
    function Get_DhCont: UnicodeString;
    function Get_XJust: UnicodeString;
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_CCT(Value: UnicodeString);
    procedure Set_CFOP(Value: UnicodeString);
    procedure Set_NatOp(Value: UnicodeString);
    procedure Set_ForPag(Value: UnicodeString);
    procedure Set_Mod_(Value: UnicodeString);
    procedure Set_Serie(Value: UnicodeString);
    procedure Set_NCT(Value: UnicodeString);
    procedure Set_DhEmi(Value: UnicodeString);
    procedure Set_TpImp(Value: UnicodeString);
    procedure Set_TpEmis(Value: UnicodeString);
    procedure Set_CDV(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_TpCTe(Value: UnicodeString);
    procedure Set_ProcEmi(Value: UnicodeString);
    procedure Set_VerProc(Value: UnicodeString);
    procedure Set_RefCTE(Value: UnicodeString);
    procedure Set_CMunEnv(Value: UnicodeString);
    procedure Set_XMunEnv(Value: UnicodeString);
    procedure Set_UFEnv(Value: UnicodeString);
    procedure Set_Modal(Value: UnicodeString);
    procedure Set_TpServ(Value: UnicodeString);
    procedure Set_CMunIni(Value: UnicodeString);
    procedure Set_XMunIni(Value: UnicodeString);
    procedure Set_UFIni(Value: UnicodeString);
    procedure Set_CMunFim(Value: UnicodeString);
    procedure Set_XMunFim(Value: UnicodeString);
    procedure Set_UFFim(Value: UnicodeString);
    procedure Set_Retira(Value: UnicodeString);
    procedure Set_XDetRetira(Value: UnicodeString);
    procedure Set_DhCont(Value: UnicodeString);
    procedure Set_XJust(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLToma03 }

  TXMLToma03 = class(TXMLNode, IXMLToma03)
  protected
    { IXMLToma03 }
    function Get_Toma: UnicodeString;
    procedure Set_Toma(Value: UnicodeString);
  end;

{ TXMLToma4 }

  TXMLToma4 = class(TXMLNode, IXMLToma4)
  protected
    { IXMLToma4 }
    function Get_Toma: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_XFant: UnicodeString;
    function Get_Fone: UnicodeString;
    function Get_EnderToma: IXMLTEndereco;
    function Get_Email: UnicodeString;
    procedure Set_Toma(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_XFant(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    procedure Set_Email(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTEndereco }

  TXMLTEndereco = class(TXMLNode, IXMLTEndereco)
  protected
    { IXMLTEndereco }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_CPais: UnicodeString;
    function Get_XPais: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_CPais(Value: UnicodeString);
    procedure Set_XPais(Value: UnicodeString);
  end;

{ TXMLCompl }

  TXMLCompl = class(TXMLNode, IXMLCompl)
  private
    FObsCont: IXMLObsContList;
    FObsFisco: IXMLObsFiscoList;
  protected
    { IXMLCompl }
    function Get_XCaracAd: UnicodeString;
    function Get_XCaracSer: UnicodeString;
    function Get_XEmi: UnicodeString;
    function Get_Fluxo: IXMLFluxo;
    function Get_Entrega: IXMLEntrega;
    function Get_OrigCalc: UnicodeString;
    function Get_DestCalc: UnicodeString;
    function Get_XObs: UnicodeString;
    function Get_ObsCont: IXMLObsContList;
    function Get_ObsFisco: IXMLObsFiscoList;
    procedure Set_XCaracAd(Value: UnicodeString);
    procedure Set_XCaracSer(Value: UnicodeString);
    procedure Set_XEmi(Value: UnicodeString);
    procedure Set_OrigCalc(Value: UnicodeString);
    procedure Set_DestCalc(Value: UnicodeString);
    procedure Set_XObs(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLFluxo }

  TXMLFluxo = class(TXMLNode, IXMLFluxo)
  private
    FPass: IXMLPassList;
  protected
    { IXMLFluxo }
    function Get_XOrig: UnicodeString;
    function Get_Pass: IXMLPassList;
    function Get_XDest: UnicodeString;
    function Get_XRota: UnicodeString;
    procedure Set_XOrig(Value: UnicodeString);
    procedure Set_XDest(Value: UnicodeString);
    procedure Set_XRota(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPass }

  TXMLPass = class(TXMLNode, IXMLPass)
  protected
    { IXMLPass }
    function Get_XPass: UnicodeString;
    procedure Set_XPass(Value: UnicodeString);
  end;

{ TXMLPassList }

  TXMLPassList = class(TXMLNodeCollection, IXMLPassList)
  protected
    { IXMLPassList }
    function Add: IXMLPass;
    function Insert(const Index: Integer): IXMLPass;

    function Get_Item(Index: Integer): IXMLPass;
  end;

{ TXMLEntrega }

  TXMLEntrega = class(TXMLNode, IXMLEntrega)
  protected
    { IXMLEntrega }
    function Get_SemData: IXMLSemData;
    function Get_ComData: IXMLComData;
    function Get_NoPeriodo: IXMLNoPeriodo;
    function Get_SemHora: IXMLSemHora;
    function Get_ComHora: IXMLComHora;
    function Get_NoInter: IXMLNoInter;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSemData }

  TXMLSemData = class(TXMLNode, IXMLSemData)
  protected
    { IXMLSemData }
    function Get_TpPer: UnicodeString;
    procedure Set_TpPer(Value: UnicodeString);
  end;

{ TXMLComData }

  TXMLComData = class(TXMLNode, IXMLComData)
  protected
    { IXMLComData }
    function Get_TpPer: UnicodeString;
    function Get_DProg: UnicodeString;
    procedure Set_TpPer(Value: UnicodeString);
    procedure Set_DProg(Value: UnicodeString);
  end;

{ TXMLNoPeriodo }

  TXMLNoPeriodo = class(TXMLNode, IXMLNoPeriodo)
  protected
    { IXMLNoPeriodo }
    function Get_TpPer: UnicodeString;
    function Get_DIni: UnicodeString;
    function Get_DFim: UnicodeString;
    procedure Set_TpPer(Value: UnicodeString);
    procedure Set_DIni(Value: UnicodeString);
    procedure Set_DFim(Value: UnicodeString);
  end;

{ TXMLSemHora }

  TXMLSemHora = class(TXMLNode, IXMLSemHora)
  protected
    { IXMLSemHora }
    function Get_TpHor: UnicodeString;
    procedure Set_TpHor(Value: UnicodeString);
  end;

{ TXMLComHora }

  TXMLComHora = class(TXMLNode, IXMLComHora)
  protected
    { IXMLComHora }
    function Get_TpHor: UnicodeString;
    function Get_HProg: UnicodeString;
    procedure Set_TpHor(Value: UnicodeString);
    procedure Set_HProg(Value: UnicodeString);
  end;

{ TXMLNoInter }

  TXMLNoInter = class(TXMLNode, IXMLNoInter)
  protected
    { IXMLNoInter }
    function Get_TpHor: UnicodeString;
    function Get_HIni: UnicodeString;
    function Get_HFim: UnicodeString;
    procedure Set_TpHor(Value: UnicodeString);
    procedure Set_HIni(Value: UnicodeString);
    procedure Set_HFim(Value: UnicodeString);
  end;

{ TXMLObsCont }

  TXMLObsCont = class(TXMLNode, IXMLObsCont)
  protected
    { IXMLObsCont }
    function Get_XCampo: UnicodeString;
    function Get_XTexto: UnicodeString;
    procedure Set_XCampo(Value: UnicodeString);
    procedure Set_XTexto(Value: UnicodeString);
  end;

{ TXMLObsContList }

  TXMLObsContList = class(TXMLNodeCollection, IXMLObsContList)
  protected
    { IXMLObsContList }
    function Add: IXMLObsCont;
    function Insert(const Index: Integer): IXMLObsCont;

    function Get_Item(Index: Integer): IXMLObsCont;
  end;

{ TXMLObsFisco }

  TXMLObsFisco = class(TXMLNode, IXMLObsFisco)
  protected
    { IXMLObsFisco }
    function Get_XCampo: UnicodeString;
    function Get_XTexto: UnicodeString;
    procedure Set_XCampo(Value: UnicodeString);
    procedure Set_XTexto(Value: UnicodeString);
  end;

{ TXMLObsFiscoList }

  TXMLObsFiscoList = class(TXMLNodeCollection, IXMLObsFiscoList)
  protected
    { IXMLObsFiscoList }
    function Add: IXMLObsFisco;
    function Insert(const Index: Integer): IXMLObsFisco;

    function Get_Item(Index: Integer): IXMLObsFisco;
  end;

{ TXMLEmit }

  TXMLEmit = class(TXMLNode, IXMLEmit)
  protected
    { IXMLEmit }
    function Get_CNPJ: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_XFant: UnicodeString;
    function Get_EnderEmit: IXMLTEndeEmi;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_XFant(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTEndeEmi }

  TXMLTEndeEmi = class(TXMLNode, IXMLTEndeEmi)
  protected
    { IXMLTEndeEmi }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_Fone: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
  end;

{ TXMLRem }

  TXMLRem = class(TXMLNode, IXMLRem)
  protected
    { IXMLRem }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_XFant: UnicodeString;
    function Get_Fone: UnicodeString;
    function Get_EnderReme: IXMLTEndereco;
    function Get_Email: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_XFant(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    procedure Set_Email(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLExped }

  TXMLExped = class(TXMLNode, IXMLExped)
  protected
    { IXMLExped }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_Fone: UnicodeString;
    function Get_EnderExped: IXMLTEndereco;
    function Get_Email: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    procedure Set_Email(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLReceb }

  TXMLReceb = class(TXMLNode, IXMLReceb)
  protected
    { IXMLReceb }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_Fone: UnicodeString;
    function Get_EnderReceb: IXMLTEndereco;
    function Get_Email: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    procedure Set_Email(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDest }

  TXMLDest = class(TXMLNode, IXMLDest)
  protected
    { IXMLDest }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_Fone: UnicodeString;
    function Get_ISUF: UnicodeString;
    function Get_EnderDest: IXMLTEndereco;
    function Get_Email: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    procedure Set_ISUF(Value: UnicodeString);
    procedure Set_Email(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLVPrest }

  TXMLVPrest = class(TXMLNode, IXMLVPrest)
  private
    FComp: IXMLCompList;
  protected
    { IXMLVPrest }
    function Get_VTPrest: UnicodeString;
    function Get_VRec: UnicodeString;
    function Get_Comp: IXMLCompList;
    procedure Set_VTPrest(Value: UnicodeString);
    procedure Set_VRec(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLComp }

  TXMLComp = class(TXMLNode, IXMLComp)
  protected
    { IXMLComp }
    function Get_XNome: UnicodeString;
    function Get_VComp: UnicodeString;
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_VComp(Value: UnicodeString);
  end;

{ TXMLCompList }

  TXMLCompList = class(TXMLNodeCollection, IXMLCompList)
  protected
    { IXMLCompList }
    function Add: IXMLComp;
    function Insert(const Index: Integer): IXMLComp;

    function Get_Item(Index: Integer): IXMLComp;
  end;

{ TXMLImp }

  TXMLImp = class(TXMLNode, IXMLImp)
  protected
    { IXMLImp }
    function Get_ICMS: IXMLTImp;
    function Get_VTotTrib: UnicodeString;
    function Get_InfAdFisco: UnicodeString;
    function Get_ICMSUFFim: IXMLICMSUFFim;
    procedure Set_VTotTrib(Value: UnicodeString);
    procedure Set_InfAdFisco(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTImp }

  TXMLTImp = class(TXMLNode, IXMLTImp)
  protected
    { IXMLTImp }
    function Get_ICMS00: IXMLICMS00;
    function Get_ICMS20: IXMLICMS20;
    function Get_ICMS45: IXMLICMS45;
    function Get_ICMS60: IXMLICMS60;
    function Get_ICMS90: IXMLICMS90;
    function Get_ICMSOutraUF: IXMLICMSOutraUF;
    function Get_ICMSSN: IXMLICMSSN;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLICMS00 }

  TXMLICMS00 = class(TXMLNode, IXMLICMS00)
  protected
    { IXMLICMS00 }
    function Get_CST: UnicodeString;
    function Get_VBC: UnicodeString;
    function Get_PICMS: UnicodeString;
    function Get_VICMS: UnicodeString;
    procedure Set_CST(Value: UnicodeString);
    procedure Set_VBC(Value: UnicodeString);
    procedure Set_PICMS(Value: UnicodeString);
    procedure Set_VICMS(Value: UnicodeString);
  end;

{ TXMLICMS20 }

  TXMLICMS20 = class(TXMLNode, IXMLICMS20)
  protected
    { IXMLICMS20 }
    function Get_CST: UnicodeString;
    function Get_PRedBC: UnicodeString;
    function Get_VBC: UnicodeString;
    function Get_PICMS: UnicodeString;
    function Get_VICMS: UnicodeString;
    procedure Set_CST(Value: UnicodeString);
    procedure Set_PRedBC(Value: UnicodeString);
    procedure Set_VBC(Value: UnicodeString);
    procedure Set_PICMS(Value: UnicodeString);
    procedure Set_VICMS(Value: UnicodeString);
  end;

{ TXMLICMS45 }

  TXMLICMS45 = class(TXMLNode, IXMLICMS45)
  protected
    { IXMLICMS45 }
    function Get_CST: UnicodeString;
    procedure Set_CST(Value: UnicodeString);
  end;

{ TXMLICMS60 }

  TXMLICMS60 = class(TXMLNode, IXMLICMS60)
  protected
    { IXMLICMS60 }
    function Get_CST: UnicodeString;
    function Get_VBCSTRet: UnicodeString;
    function Get_VICMSSTRet: UnicodeString;
    function Get_PICMSSTRet: UnicodeString;
    function Get_VCred: UnicodeString;
    procedure Set_CST(Value: UnicodeString);
    procedure Set_VBCSTRet(Value: UnicodeString);
    procedure Set_VICMSSTRet(Value: UnicodeString);
    procedure Set_PICMSSTRet(Value: UnicodeString);
    procedure Set_VCred(Value: UnicodeString);
  end;

{ TXMLICMS90 }

  TXMLICMS90 = class(TXMLNode, IXMLICMS90)
  protected
    { IXMLICMS90 }
    function Get_CST: UnicodeString;
    function Get_PRedBC: UnicodeString;
    function Get_VBC: UnicodeString;
    function Get_PICMS: UnicodeString;
    function Get_VICMS: UnicodeString;
    function Get_VCred: UnicodeString;
    procedure Set_CST(Value: UnicodeString);
    procedure Set_PRedBC(Value: UnicodeString);
    procedure Set_VBC(Value: UnicodeString);
    procedure Set_PICMS(Value: UnicodeString);
    procedure Set_VICMS(Value: UnicodeString);
    procedure Set_VCred(Value: UnicodeString);
  end;

{ TXMLICMSOutraUF }

  TXMLICMSOutraUF = class(TXMLNode, IXMLICMSOutraUF)
  protected
    { IXMLICMSOutraUF }
    function Get_CST: UnicodeString;
    function Get_PRedBCOutraUF: UnicodeString;
    function Get_VBCOutraUF: UnicodeString;
    function Get_PICMSOutraUF: UnicodeString;
    function Get_VICMSOutraUF: UnicodeString;
    procedure Set_CST(Value: UnicodeString);
    procedure Set_PRedBCOutraUF(Value: UnicodeString);
    procedure Set_VBCOutraUF(Value: UnicodeString);
    procedure Set_PICMSOutraUF(Value: UnicodeString);
    procedure Set_VICMSOutraUF(Value: UnicodeString);
  end;

{ TXMLICMSSN }

  TXMLICMSSN = class(TXMLNode, IXMLICMSSN)
  protected
    { IXMLICMSSN }
    function Get_IndSN: UnicodeString;
    procedure Set_IndSN(Value: UnicodeString);
  end;

{ TXMLICMSUFFim }

  TXMLICMSUFFim = class(TXMLNode, IXMLICMSUFFim)
  protected
    { IXMLICMSUFFim }
    function Get_VBCUFFim: UnicodeString;
    function Get_PICMSUFFim: UnicodeString;
    function Get_PICMSInter: UnicodeString;
    function Get_PICMSInterPart: UnicodeString;
    function Get_VICMSUFFim: UnicodeString;
    function Get_VICMSUFIni: UnicodeString;
    procedure Set_VBCUFFim(Value: UnicodeString);
    procedure Set_PICMSUFFim(Value: UnicodeString);
    procedure Set_PICMSInter(Value: UnicodeString);
    procedure Set_PICMSInterPart(Value: UnicodeString);
    procedure Set_VICMSUFFim(Value: UnicodeString);
    procedure Set_VICMSUFIni(Value: UnicodeString);
  end;

{ TXMLInfCTeNorm }

  TXMLInfCTeNorm = class(TXMLNode, IXMLInfCTeNorm)
  private
    FSeg: IXMLSegList;
    FPeri: IXMLPeriList;
    FVeicNovos: IXMLVeicNovosList;
  protected
    { IXMLInfCTeNorm }
    function Get_InfCarga: IXMLInfCarga;
    function Get_InfDoc: IXMLInfDoc;
    function Get_DocAnt: IXMLDocAnt;
    function Get_Seg: IXMLSegList;
    function Get_InfModal: IXMLInfModal;
    function Get_Peri: IXMLPeriList;
    function Get_VeicNovos: IXMLVeicNovosList;
    function Get_Cobr: IXMLCobr;
    function Get_InfCteSub: IXMLInfCteSub;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfCarga }

  TXMLInfCarga = class(TXMLNode, IXMLInfCarga)
  private
    FInfQ: IXMLInfQList;
  protected
    { IXMLInfCarga }
    function Get_VCarga: UnicodeString;
    function Get_ProPred: UnicodeString;
    function Get_XOutCat: UnicodeString;
    function Get_InfQ: IXMLInfQList;
    procedure Set_VCarga(Value: UnicodeString);
    procedure Set_ProPred(Value: UnicodeString);
    procedure Set_XOutCat(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfQ }

  TXMLInfQ = class(TXMLNode, IXMLInfQ)
  protected
    { IXMLInfQ }
    function Get_CUnid: UnicodeString;
    function Get_TpMed: UnicodeString;
    function Get_QCarga: UnicodeString;
    procedure Set_CUnid(Value: UnicodeString);
    procedure Set_TpMed(Value: UnicodeString);
    procedure Set_QCarga(Value: UnicodeString);
  end;

{ TXMLInfQList }

  TXMLInfQList = class(TXMLNodeCollection, IXMLInfQList)
  protected
    { IXMLInfQList }
    function Add: IXMLInfQ;
    function Insert(const Index: Integer): IXMLInfQ;

    function Get_Item(Index: Integer): IXMLInfQ;
  end;

{ TXMLInfDoc }

  TXMLInfDoc = class(TXMLNode, IXMLInfDoc)
  private
    FInfNF: IXMLInfNFList;
    FInfNFe: IXMLInfNFeList;
    FInfOutros: IXMLInfOutrosList;
  protected
    { IXMLInfDoc }
    function Get_InfNF: IXMLInfNFList;
    function Get_InfNFe: IXMLInfNFeList;
    function Get_InfOutros: IXMLInfOutrosList;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfNF }

  TXMLInfNF = class(TXMLNode, IXMLInfNF)
  private
    FInfUnidTransp: IXMLTUnidadeTranspList;
    FInfUnidCarga: IXMLTUnidCargaList;
  protected
    { IXMLInfNF }
    function Get_NRoma: UnicodeString;
    function Get_NPed: UnicodeString;
    function Get_Mod_: UnicodeString;
    function Get_Serie: UnicodeString;
    function Get_NDoc: UnicodeString;
    function Get_DEmi: UnicodeString;
    function Get_VBC: UnicodeString;
    function Get_VICMS: UnicodeString;
    function Get_VBCST: UnicodeString;
    function Get_VST: UnicodeString;
    function Get_VProd: UnicodeString;
    function Get_VNF: UnicodeString;
    function Get_NCFOP: UnicodeString;
    function Get_NPeso: UnicodeString;
    function Get_PIN: UnicodeString;
    function Get_DPrev: UnicodeString;
    function Get_InfUnidTransp: IXMLTUnidadeTranspList;
    function Get_InfUnidCarga: IXMLTUnidCargaList;
    procedure Set_NRoma(Value: UnicodeString);
    procedure Set_NPed(Value: UnicodeString);
    procedure Set_Mod_(Value: UnicodeString);
    procedure Set_Serie(Value: UnicodeString);
    procedure Set_NDoc(Value: UnicodeString);
    procedure Set_DEmi(Value: UnicodeString);
    procedure Set_VBC(Value: UnicodeString);
    procedure Set_VICMS(Value: UnicodeString);
    procedure Set_VBCST(Value: UnicodeString);
    procedure Set_VST(Value: UnicodeString);
    procedure Set_VProd(Value: UnicodeString);
    procedure Set_VNF(Value: UnicodeString);
    procedure Set_NCFOP(Value: UnicodeString);
    procedure Set_NPeso(Value: UnicodeString);
    procedure Set_PIN(Value: UnicodeString);
    procedure Set_DPrev(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfNFList }

  TXMLInfNFList = class(TXMLNodeCollection, IXMLInfNFList)
  protected
    { IXMLInfNFList }
    function Add: IXMLInfNF;
    function Insert(const Index: Integer): IXMLInfNF;

    function Get_Item(Index: Integer): IXMLInfNF;
  end;

{ TXMLTUnidadeTransp }

  TXMLTUnidadeTransp = class(TXMLNode, IXMLTUnidadeTransp)
  private
    FLacUnidTransp: IXMLLacUnidTranspList;
    FInfUnidCarga: IXMLTUnidCargaList;
  protected
    { IXMLTUnidadeTransp }
    function Get_TpUnidTransp: UnicodeString;
    function Get_IdUnidTransp: UnicodeString;
    function Get_LacUnidTransp: IXMLLacUnidTranspList;
    function Get_InfUnidCarga: IXMLTUnidCargaList;
    function Get_QtdRat: UnicodeString;
    procedure Set_TpUnidTransp(Value: UnicodeString);
    procedure Set_IdUnidTransp(Value: UnicodeString);
    procedure Set_QtdRat(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTUnidadeTranspList }

  TXMLTUnidadeTranspList = class(TXMLNodeCollection, IXMLTUnidadeTranspList)
  protected
    { IXMLTUnidadeTranspList }
    function Add: IXMLTUnidadeTransp;
    function Insert(const Index: Integer): IXMLTUnidadeTransp;

    function Get_Item(Index: Integer): IXMLTUnidadeTransp;
  end;

{ TXMLLacUnidTransp }

  TXMLLacUnidTransp = class(TXMLNode, IXMLLacUnidTransp)
  protected
    { IXMLLacUnidTransp }
    function Get_NLacre: UnicodeString;
    procedure Set_NLacre(Value: UnicodeString);
  end;

{ TXMLLacUnidTranspList }

  TXMLLacUnidTranspList = class(TXMLNodeCollection, IXMLLacUnidTranspList)
  protected
    { IXMLLacUnidTranspList }
    function Add: IXMLLacUnidTransp;
    function Insert(const Index: Integer): IXMLLacUnidTransp;

    function Get_Item(Index: Integer): IXMLLacUnidTransp;
  end;

{ TXMLTUnidCarga }

  TXMLTUnidCarga = class(TXMLNode, IXMLTUnidCarga)
  private
    FLacUnidCarga: IXMLLacUnidCargaList;
  protected
    { IXMLTUnidCarga }
    function Get_TpUnidCarga: UnicodeString;
    function Get_IdUnidCarga: UnicodeString;
    function Get_LacUnidCarga: IXMLLacUnidCargaList;
    function Get_QtdRat: UnicodeString;
    procedure Set_TpUnidCarga(Value: UnicodeString);
    procedure Set_IdUnidCarga(Value: UnicodeString);
    procedure Set_QtdRat(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTUnidCargaList }

  TXMLTUnidCargaList = class(TXMLNodeCollection, IXMLTUnidCargaList)
  protected
    { IXMLTUnidCargaList }
    function Add: IXMLTUnidCarga;
    function Insert(const Index: Integer): IXMLTUnidCarga;

    function Get_Item(Index: Integer): IXMLTUnidCarga;
  end;

{ TXMLLacUnidCarga }

  TXMLLacUnidCarga = class(TXMLNode, IXMLLacUnidCarga)
  protected
    { IXMLLacUnidCarga }
    function Get_NLacre: UnicodeString;
    procedure Set_NLacre(Value: UnicodeString);
  end;

{ TXMLLacUnidCargaList }

  TXMLLacUnidCargaList = class(TXMLNodeCollection, IXMLLacUnidCargaList)
  protected
    { IXMLLacUnidCargaList }
    function Add: IXMLLacUnidCarga;
    function Insert(const Index: Integer): IXMLLacUnidCarga;

    function Get_Item(Index: Integer): IXMLLacUnidCarga;
  end;

{ TXMLInfNFe }

  TXMLInfNFe = class(TXMLNode, IXMLInfNFe)
  private
    FInfUnidTransp: IXMLTUnidadeTranspList;
    FInfUnidCarga: IXMLTUnidCargaList;
  protected
    { IXMLInfNFe }
    function Get_Chave: UnicodeString;
    function Get_PIN: UnicodeString;
    function Get_DPrev: UnicodeString;
    function Get_InfUnidTransp: IXMLTUnidadeTranspList;
    function Get_InfUnidCarga: IXMLTUnidCargaList;
    procedure Set_Chave(Value: UnicodeString);
    procedure Set_PIN(Value: UnicodeString);
    procedure Set_DPrev(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfNFeList }

  TXMLInfNFeList = class(TXMLNodeCollection, IXMLInfNFeList)
  protected
    { IXMLInfNFeList }
    function Add: IXMLInfNFe;
    function Insert(const Index: Integer): IXMLInfNFe;

    function Get_Item(Index: Integer): IXMLInfNFe;
  end;

{ TXMLInfOutros }

  TXMLInfOutros = class(TXMLNode, IXMLInfOutros)
  private
    FInfUnidTransp: IXMLTUnidadeTranspList;
    FInfUnidCarga: IXMLTUnidCargaList;
  protected
    { IXMLInfOutros }
    function Get_TpDoc: UnicodeString;
    function Get_DescOutros: UnicodeString;
    function Get_NDoc: UnicodeString;
    function Get_DEmi: UnicodeString;
    function Get_VDocFisc: UnicodeString;
    function Get_DPrev: UnicodeString;
    function Get_InfUnidTransp: IXMLTUnidadeTranspList;
    function Get_InfUnidCarga: IXMLTUnidCargaList;
    procedure Set_TpDoc(Value: UnicodeString);
    procedure Set_DescOutros(Value: UnicodeString);
    procedure Set_NDoc(Value: UnicodeString);
    procedure Set_DEmi(Value: UnicodeString);
    procedure Set_VDocFisc(Value: UnicodeString);
    procedure Set_DPrev(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfOutrosList }

  TXMLInfOutrosList = class(TXMLNodeCollection, IXMLInfOutrosList)
  protected
    { IXMLInfOutrosList }
    function Add: IXMLInfOutros;
    function Insert(const Index: Integer): IXMLInfOutros;

    function Get_Item(Index: Integer): IXMLInfOutros;
  end;

{ TXMLDocAnt }

  TXMLDocAnt = class(TXMLNodeCollection, IXMLDocAnt)
  protected
    { IXMLDocAnt }
    function Get_EmiDocAnt(Index: Integer): IXMLEmiDocAnt;
    function Add: IXMLEmiDocAnt;
    function Insert(const Index: Integer): IXMLEmiDocAnt;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEmiDocAnt }

  TXMLEmiDocAnt = class(TXMLNode, IXMLEmiDocAnt)
  private
    FIdDocAnt: IXMLIdDocAntList;
  protected
    { IXMLEmiDocAnt }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_IdDocAnt: IXMLIdDocAntList;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLIdDocAnt }

  TXMLIdDocAnt = class(TXMLNode, IXMLIdDocAnt)
  private
    FIdDocAntPap: IXMLIdDocAntPapList;
    FIdDocAntEle: IXMLIdDocAntEleList;
  protected
    { IXMLIdDocAnt }
    function Get_IdDocAntPap: IXMLIdDocAntPapList;
    function Get_IdDocAntEle: IXMLIdDocAntEleList;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLIdDocAntList }

  TXMLIdDocAntList = class(TXMLNodeCollection, IXMLIdDocAntList)
  protected
    { IXMLIdDocAntList }
    function Add: IXMLIdDocAnt;
    function Insert(const Index: Integer): IXMLIdDocAnt;

    function Get_Item(Index: Integer): IXMLIdDocAnt;
  end;

{ TXMLIdDocAntPap }

  TXMLIdDocAntPap = class(TXMLNode, IXMLIdDocAntPap)
  protected
    { IXMLIdDocAntPap }
    function Get_TpDoc: UnicodeString;
    function Get_Serie: UnicodeString;
    function Get_Subser: UnicodeString;
    function Get_NDoc: UnicodeString;
    function Get_DEmi: UnicodeString;
    procedure Set_TpDoc(Value: UnicodeString);
    procedure Set_Serie(Value: UnicodeString);
    procedure Set_Subser(Value: UnicodeString);
    procedure Set_NDoc(Value: UnicodeString);
    procedure Set_DEmi(Value: UnicodeString);
  end;

{ TXMLIdDocAntPapList }

  TXMLIdDocAntPapList = class(TXMLNodeCollection, IXMLIdDocAntPapList)
  protected
    { IXMLIdDocAntPapList }
    function Add: IXMLIdDocAntPap;
    function Insert(const Index: Integer): IXMLIdDocAntPap;

    function Get_Item(Index: Integer): IXMLIdDocAntPap;
  end;

{ TXMLIdDocAntEle }

  TXMLIdDocAntEle = class(TXMLNode, IXMLIdDocAntEle)
  protected
    { IXMLIdDocAntEle }
    function Get_Chave: UnicodeString;
    procedure Set_Chave(Value: UnicodeString);
  end;

{ TXMLIdDocAntEleList }

  TXMLIdDocAntEleList = class(TXMLNodeCollection, IXMLIdDocAntEleList)
  protected
    { IXMLIdDocAntEleList }
    function Add: IXMLIdDocAntEle;
    function Insert(const Index: Integer): IXMLIdDocAntEle;

    function Get_Item(Index: Integer): IXMLIdDocAntEle;
  end;

{ TXMLSeg }

  TXMLSeg = class(TXMLNode, IXMLSeg)
  protected
    { IXMLSeg }
    function Get_RespSeg: UnicodeString;
    function Get_XSeg: UnicodeString;
    function Get_NApol: UnicodeString;
    function Get_NAver: UnicodeString;
    function Get_VCarga: UnicodeString;
    procedure Set_RespSeg(Value: UnicodeString);
    procedure Set_XSeg(Value: UnicodeString);
    procedure Set_NApol(Value: UnicodeString);
    procedure Set_NAver(Value: UnicodeString);
    procedure Set_VCarga(Value: UnicodeString);
  end;

{ TXMLSegList }

  TXMLSegList = class(TXMLNodeCollection, IXMLSegList)
  protected
    { IXMLSegList }
    function Add: IXMLSeg;
    function Insert(const Index: Integer): IXMLSeg;

    function Get_Item(Index: Integer): IXMLSeg;
  end;

{ TXMLInfModal }

  TXMLInfModal = class(TXMLNode, IXMLInfModal)
  protected
    { IXMLInfModal }
    function Get_VersaoModal: UnicodeString;
    procedure Set_VersaoModal(Value: UnicodeString);
  end;

{ TXMLPeri }

  TXMLPeri = class(TXMLNode, IXMLPeri)
  protected
    { IXMLPeri }
    function Get_NONU: UnicodeString;
    function Get_XNomeAE: UnicodeString;
    function Get_XClaRisco: UnicodeString;
    function Get_GrEmb: UnicodeString;
    function Get_QTotProd: UnicodeString;
    function Get_QVolTipo: UnicodeString;
    function Get_PontoFulgor: UnicodeString;
    procedure Set_NONU(Value: UnicodeString);
    procedure Set_XNomeAE(Value: UnicodeString);
    procedure Set_XClaRisco(Value: UnicodeString);
    procedure Set_GrEmb(Value: UnicodeString);
    procedure Set_QTotProd(Value: UnicodeString);
    procedure Set_QVolTipo(Value: UnicodeString);
    procedure Set_PontoFulgor(Value: UnicodeString);
  end;

{ TXMLPeriList }

  TXMLPeriList = class(TXMLNodeCollection, IXMLPeriList)
  protected
    { IXMLPeriList }
    function Add: IXMLPeri;
    function Insert(const Index: Integer): IXMLPeri;

    function Get_Item(Index: Integer): IXMLPeri;
  end;

{ TXMLVeicNovos }

  TXMLVeicNovos = class(TXMLNode, IXMLVeicNovos)
  protected
    { IXMLVeicNovos }
    function Get_Chassi: UnicodeString;
    function Get_CCor: UnicodeString;
    function Get_XCor: UnicodeString;
    function Get_CMod: UnicodeString;
    function Get_VUnit: UnicodeString;
    function Get_VFrete: UnicodeString;
    procedure Set_Chassi(Value: UnicodeString);
    procedure Set_CCor(Value: UnicodeString);
    procedure Set_XCor(Value: UnicodeString);
    procedure Set_CMod(Value: UnicodeString);
    procedure Set_VUnit(Value: UnicodeString);
    procedure Set_VFrete(Value: UnicodeString);
  end;

{ TXMLVeicNovosList }

  TXMLVeicNovosList = class(TXMLNodeCollection, IXMLVeicNovosList)
  protected
    { IXMLVeicNovosList }
    function Add: IXMLVeicNovos;
    function Insert(const Index: Integer): IXMLVeicNovos;

    function Get_Item(Index: Integer): IXMLVeicNovos;
  end;

{ TXMLCobr }

  TXMLCobr = class(TXMLNode, IXMLCobr)
  private
    FDup: IXMLDupList;
  protected
    { IXMLCobr }
    function Get_Fat: IXMLFat;
    function Get_Dup: IXMLDupList;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLFat }

  TXMLFat = class(TXMLNode, IXMLFat)
  protected
    { IXMLFat }
    function Get_NFat: UnicodeString;
    function Get_VOrig: UnicodeString;
    function Get_VDesc: UnicodeString;
    function Get_VLiq: UnicodeString;
    procedure Set_NFat(Value: UnicodeString);
    procedure Set_VOrig(Value: UnicodeString);
    procedure Set_VDesc(Value: UnicodeString);
    procedure Set_VLiq(Value: UnicodeString);
  end;

{ TXMLDup }

  TXMLDup = class(TXMLNode, IXMLDup)
  protected
    { IXMLDup }
    function Get_NDup: UnicodeString;
    function Get_DVenc: UnicodeString;
    function Get_VDup: UnicodeString;
    procedure Set_NDup(Value: UnicodeString);
    procedure Set_DVenc(Value: UnicodeString);
    procedure Set_VDup(Value: UnicodeString);
  end;

{ TXMLDupList }

  TXMLDupList = class(TXMLNodeCollection, IXMLDupList)
  protected
    { IXMLDupList }
    function Add: IXMLDup;
    function Insert(const Index: Integer): IXMLDup;

    function Get_Item(Index: Integer): IXMLDup;
  end;

{ TXMLInfCteSub }

  TXMLInfCteSub = class(TXMLNode, IXMLInfCteSub)
  protected
    { IXMLInfCteSub }
    function Get_ChCte: UnicodeString;
    function Get_TomaICMS: IXMLTomaICMS;
    function Get_TomaNaoICMS: IXMLTomaNaoICMS;
    procedure Set_ChCte(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTomaICMS }

  TXMLTomaICMS = class(TXMLNode, IXMLTomaICMS)
  protected
    { IXMLTomaICMS }
    function Get_RefNFe: UnicodeString;
    function Get_RefNF: IXMLRefNF;
    function Get_RefCte: UnicodeString;
    procedure Set_RefNFe(Value: UnicodeString);
    procedure Set_RefCte(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRefNF }

  TXMLRefNF = class(TXMLNode, IXMLRefNF)
  protected
    { IXMLRefNF }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_Mod_: UnicodeString;
    function Get_Serie: UnicodeString;
    function Get_Subserie: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_Valor: UnicodeString;
    function Get_DEmi: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_Mod_(Value: UnicodeString);
    procedure Set_Serie(Value: UnicodeString);
    procedure Set_Subserie(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_Valor(Value: UnicodeString);
    procedure Set_DEmi(Value: UnicodeString);
  end;

{ TXMLTomaNaoICMS }

  TXMLTomaNaoICMS = class(TXMLNode, IXMLTomaNaoICMS)
  protected
    { IXMLTomaNaoICMS }
    function Get_RefCteAnu: UnicodeString;
    procedure Set_RefCteAnu(Value: UnicodeString);
  end;

{ TXMLInfCteComp }

  TXMLInfCteComp = class(TXMLNode, IXMLInfCteComp)
  protected
    { IXMLInfCteComp }
    function Get_Chave: UnicodeString;
    procedure Set_Chave(Value: UnicodeString);
  end;

{ TXMLInfCteAnu }

  TXMLInfCteAnu = class(TXMLNode, IXMLInfCteAnu)
  protected
    { IXMLInfCteAnu }
    function Get_ChCte: UnicodeString;
    function Get_DEmi: UnicodeString;
    procedure Set_ChCte(Value: UnicodeString);
    procedure Set_DEmi(Value: UnicodeString);
  end;

{ TXMLAutXML }

  TXMLAutXML = class(TXMLNode, IXMLAutXML)
  protected
    { IXMLAutXML }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
  end;

{ TXMLAutXMLList }

  TXMLAutXMLList = class(TXMLNodeCollection, IXMLAutXMLList)
  protected
    { IXMLAutXMLList }
    function Add: IXMLAutXML;
    function Insert(const Index: Integer): IXMLAutXML;

    function Get_Item(Index: Integer): IXMLAutXML;
  end;

{ TXMLSignatureType_ds }

  TXMLSignatureType_ds = class(TXMLNode, IXMLSignatureType_ds)
  protected
    { IXMLSignatureType_ds }
    function Get_Id: UnicodeString;
    function Get_SignedInfo: IXMLSignedInfoType_ds;
    function Get_SignatureValue: IXMLSignatureValueType_ds;
    function Get_KeyInfo: IXMLKeyInfoType_ds;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSignedInfoType_ds }

  TXMLSignedInfoType_ds = class(TXMLNode, IXMLSignedInfoType_ds)
  protected
    { IXMLSignedInfoType_ds }
    function Get_Id: UnicodeString;
    function Get_CanonicalizationMethod: IXMLCanonicalizationMethod_ds;
    function Get_SignatureMethod: IXMLSignatureMethod_ds;
    function Get_Reference: IXMLReferenceType_ds;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCanonicalizationMethod_ds }

  TXMLCanonicalizationMethod_ds = class(TXMLNode, IXMLCanonicalizationMethod_ds)
  protected
    { IXMLCanonicalizationMethod_ds }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLSignatureMethod_ds }

  TXMLSignatureMethod_ds = class(TXMLNode, IXMLSignatureMethod_ds)
  protected
    { IXMLSignatureMethod_ds }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLReferenceType_ds }

  TXMLReferenceType_ds = class(TXMLNode, IXMLReferenceType_ds)
  protected
    { IXMLReferenceType_ds }
    function Get_Id: UnicodeString;
    function Get_URI: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_Transforms: IXMLTransformsType_ds;
    function Get_DigestMethod: IXMLDigestMethod_ds;
    function Get_DigestValue: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_URI(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_DigestValue(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransformsType_ds }

  TXMLTransformsType_ds = class(TXMLNodeCollection, IXMLTransformsType_ds)
  protected
    { IXMLTransformsType_ds }
    function Get_Transform(Index: Integer): IXMLTransformType_ds;
    function Add: IXMLTransformType_ds;
    function Insert(const Index: Integer): IXMLTransformType_ds;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransformType_ds }

  TXMLTransformType_ds = class(TXMLNodeCollection, IXMLTransformType_ds)
  protected
    { IXMLTransformType_ds }
    function Get_Algorithm: UnicodeString;
    function Get_XPath(Index: Integer): UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    function Add(const XPath: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDigestMethod_ds }

  TXMLDigestMethod_ds = class(TXMLNode, IXMLDigestMethod_ds)
  protected
    { IXMLDigestMethod_ds }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLSignatureValueType_ds }

  TXMLSignatureValueType_ds = class(TXMLNode, IXMLSignatureValueType_ds)
  protected
    { IXMLSignatureValueType_ds }
    function Get_Id: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
  end;

{ TXMLKeyInfoType_ds }

  TXMLKeyInfoType_ds = class(TXMLNode, IXMLKeyInfoType_ds)
  protected
    { IXMLKeyInfoType_ds }
    function Get_Id: UnicodeString;
    function Get_X509Data: IXMLX509DataType_ds;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLX509DataType_ds }

  TXMLX509DataType_ds = class(TXMLNode, IXMLX509DataType_ds)
  protected
    { IXMLX509DataType_ds }
    function Get_X509Certificate: UnicodeString;
    procedure Set_X509Certificate(Value: UnicodeString);
  end;

{ TXMLTEnviCTe }

  TXMLTEnviCTe = class(TXMLNode, IXMLTEnviCTe)
  private
    FCTe: IXMLTCTeList;
  protected
    { IXMLTEnviCTe }
    function Get_Versao: UnicodeString;
    function Get_IdLote: UnicodeString;
    function Get_CTe: IXMLTCTeList;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_IdLote(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTRetEnviCTe }

  TXMLTRetEnviCTe = class(TXMLNode, IXMLTRetEnviCTe)
  protected
    { IXMLTRetEnviCTe }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_InfRec: IXMLInfRec;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfRec }

  TXMLInfRec = class(TXMLNode, IXMLInfRec)
  protected
    { IXMLInfRec }
    function Get_NRec: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_TMed: Integer;
    procedure Set_NRec(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_TMed(Value: Integer);
  end;

{ TXMLTEndernac }

  TXMLTEndernac = class(TXMLNode, IXMLTEndernac)
  protected
    { IXMLTEndernac }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
  end;

{ TXMLTEndOrg }

  TXMLTEndOrg = class(TXMLNode, IXMLTEndOrg)
  protected
    { IXMLTEndOrg }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_CPais: UnicodeString;
    function Get_XPais: UnicodeString;
    function Get_Fone: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_CPais(Value: UnicodeString);
    procedure Set_XPais(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
  end;

{ TXMLTLocal }

  TXMLTLocal = class(TXMLNode, IXMLTLocal)
  protected
    { IXMLTLocal }
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
  end;

{ TXMLTEndReEnt }

  TXMLTEndReEnt = class(TXMLNode, IXMLTEndReEnt)
  protected
    { IXMLTEndReEnt }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
  end;

{ Global Functions }

function Getrodo(Doc: IXMLDocument): IXMLRodo;
function Loadrodo(const FileName: string): IXMLRodo;
function Newrodo: IXMLRodo;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/cte';

implementation

{ Global Functions }

function Getrodo(Doc: IXMLDocument): IXMLRodo;
begin
  Result := Doc.GetDocBinding('rodo', TXMLRodo, TargetNamespace) as IXMLRodo;
end;

function Loadrodo(const FileName: string): IXMLRodo;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('rodo', TXMLRodo, TargetNamespace) as IXMLRodo;
end;

function Newrodo: IXMLRodo;
begin
  Result := NewXMLDocument.GetDocBinding('rodo', TXMLRodo, TargetNamespace) as IXMLRodo;
end;

{ TXMLRodo }

procedure TXMLRodo.AfterConstruction;
begin
  RegisterChildNode('occ', TXMLRodo_occ);
  RegisterChildNode('valePed', TXMLRodo_valePed);
  RegisterChildNode('veic', TXMLRodo_veic);
  RegisterChildNode('lacRodo', TXMLRodo_lacRodo);
  RegisterChildNode('moto', TXMLRodo_moto);
  FOcc := CreateCollection(TXMLRodo_occList, IXMLRodo_occ, 'occ') as IXMLRodo_occList;
  FValePed := CreateCollection(TXMLRodo_valePedList, IXMLRodo_valePed, 'valePed') as IXMLRodo_valePedList;
  FVeic := CreateCollection(TXMLRodo_veicList, IXMLRodo_veic, 'veic') as IXMLRodo_veicList;
  FLacRodo := CreateCollection(TXMLRodo_lacRodoList, IXMLRodo_lacRodo, 'lacRodo') as IXMLRodo_lacRodoList;
  FMoto := CreateCollection(TXMLRodo_motoList, IXMLRodo_moto, 'moto') as IXMLRodo_motoList;
  inherited;
end;

function TXMLRodo.Get_RNTRC: UnicodeString;
begin
  Result := ChildNodes['RNTRC'].Text;
end;

procedure TXMLRodo.Set_RNTRC(Value: UnicodeString);
begin
  ChildNodes['RNTRC'].NodeValue := Value;
end;

function TXMLRodo.Get_DPrev: UnicodeString;
begin
  Result := ChildNodes['dPrev'].Text;
end;

procedure TXMLRodo.Set_DPrev(Value: UnicodeString);
begin
  ChildNodes['dPrev'].NodeValue := Value;
end;

function TXMLRodo.Get_Lota: UnicodeString;
begin
  Result := ChildNodes['lota'].Text;
end;

procedure TXMLRodo.Set_Lota(Value: UnicodeString);
begin
  ChildNodes['lota'].NodeValue := Value;
end;

function TXMLRodo.Get_CIOT: UnicodeString;
begin
  Result := ChildNodes['CIOT'].Text;
end;

procedure TXMLRodo.Set_CIOT(Value: UnicodeString);
begin
  ChildNodes['CIOT'].NodeValue := Value;
end;

function TXMLRodo.Get_Occ: IXMLRodo_occList;
begin
  Result := FOcc;
end;

function TXMLRodo.Get_ValePed: IXMLRodo_valePedList;
begin
  Result := FValePed;
end;

function TXMLRodo.Get_Veic: IXMLRodo_veicList;
begin
  Result := FVeic;
end;

function TXMLRodo.Get_LacRodo: IXMLRodo_lacRodoList;
begin
  Result := FLacRodo;
end;

function TXMLRodo.Get_Moto: IXMLRodo_motoList;
begin
  Result := FMoto;
end;

{ TXMLRodo_occ }

procedure TXMLRodo_occ.AfterConstruction;
begin
  RegisterChildNode('emiOcc', TXMLRodo_occ_emiOcc);
  inherited;
end;

function TXMLRodo_occ.Get_Serie: UnicodeString;
begin
  Result := ChildNodes['serie'].Text;
end;

procedure TXMLRodo_occ.Set_Serie(Value: UnicodeString);
begin
  ChildNodes['serie'].NodeValue := Value;
end;

function TXMLRodo_occ.Get_NOcc: UnicodeString;
begin
  Result := ChildNodes['nOcc'].Text;
end;

procedure TXMLRodo_occ.Set_NOcc(Value: UnicodeString);
begin
  ChildNodes['nOcc'].NodeValue := Value;
end;

function TXMLRodo_occ.Get_DEmi: UnicodeString;
begin
  Result := ChildNodes['dEmi'].Text;
end;

procedure TXMLRodo_occ.Set_DEmi(Value: UnicodeString);
begin
  ChildNodes['dEmi'].NodeValue := Value;
end;

function TXMLRodo_occ.Get_EmiOcc: IXMLRodo_occ_emiOcc;
begin
  Result := ChildNodes['emiOcc'] as IXMLRodo_occ_emiOcc;
end;

{ TXMLRodo_occList }

function TXMLRodo_occList.Add: IXMLRodo_occ;
begin
  Result := AddItem(-1) as IXMLRodo_occ;
end;

function TXMLRodo_occList.Insert(const Index: Integer): IXMLRodo_occ;
begin
  Result := AddItem(Index) as IXMLRodo_occ;
end;

function TXMLRodo_occList.Get_Item(Index: Integer): IXMLRodo_occ;
begin
  Result := List[Index] as IXMLRodo_occ;
end;

{ TXMLRodo_occ_emiOcc }

function TXMLRodo_occ_emiOcc.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLRodo_occ_emiOcc.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLRodo_occ_emiOcc.Get_CInt: UnicodeString;
begin
  Result := ChildNodes['cInt'].Text;
end;

procedure TXMLRodo_occ_emiOcc.Set_CInt(Value: UnicodeString);
begin
  ChildNodes['cInt'].NodeValue := Value;
end;

function TXMLRodo_occ_emiOcc.Get_IE: UnicodeString;
begin
  Result := ChildNodes['IE'].Text;
end;

procedure TXMLRodo_occ_emiOcc.Set_IE(Value: UnicodeString);
begin
  ChildNodes['IE'].NodeValue := Value;
end;

function TXMLRodo_occ_emiOcc.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLRodo_occ_emiOcc.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

function TXMLRodo_occ_emiOcc.Get_Fone: UnicodeString;
begin
  Result := ChildNodes['fone'].Text;
end;

procedure TXMLRodo_occ_emiOcc.Set_Fone(Value: UnicodeString);
begin
  ChildNodes['fone'].NodeValue := Value;
end;

{ TXMLRodo_valePed }

function TXMLRodo_valePed.Get_CNPJForn: UnicodeString;
begin
  Result := ChildNodes['CNPJForn'].Text;
end;

procedure TXMLRodo_valePed.Set_CNPJForn(Value: UnicodeString);
begin
  ChildNodes['CNPJForn'].NodeValue := Value;
end;

function TXMLRodo_valePed.Get_NCompra: UnicodeString;
begin
  Result := ChildNodes['nCompra'].Text;
end;

procedure TXMLRodo_valePed.Set_NCompra(Value: UnicodeString);
begin
  ChildNodes['nCompra'].NodeValue := Value;
end;

function TXMLRodo_valePed.Get_CNPJPg: UnicodeString;
begin
  Result := ChildNodes['CNPJPg'].Text;
end;

procedure TXMLRodo_valePed.Set_CNPJPg(Value: UnicodeString);
begin
  ChildNodes['CNPJPg'].NodeValue := Value;
end;

function TXMLRodo_valePed.Get_VValePed: UnicodeString;
begin
  Result := ChildNodes['vValePed'].Text;
end;

procedure TXMLRodo_valePed.Set_VValePed(Value: UnicodeString);
begin
  ChildNodes['vValePed'].NodeValue := Value;
end;

{ TXMLRodo_valePedList }

function TXMLRodo_valePedList.Add: IXMLRodo_valePed;
begin
  Result := AddItem(-1) as IXMLRodo_valePed;
end;

function TXMLRodo_valePedList.Insert(const Index: Integer): IXMLRodo_valePed;
begin
  Result := AddItem(Index) as IXMLRodo_valePed;
end;

function TXMLRodo_valePedList.Get_Item(Index: Integer): IXMLRodo_valePed;
begin
  Result := List[Index] as IXMLRodo_valePed;
end;

{ TXMLRodo_veic }

procedure TXMLRodo_veic.AfterConstruction;
begin
  RegisterChildNode('prop', TXMLRodo_veic_prop);
  inherited;
end;

function TXMLRodo_veic.Get_CInt: UnicodeString;
begin
  Result := ChildNodes['cInt'].Text;
end;

procedure TXMLRodo_veic.Set_CInt(Value: UnicodeString);
begin
  ChildNodes['cInt'].NodeValue := Value;
end;

function TXMLRodo_veic.Get_RENAVAM: UnicodeString;
begin
  Result := ChildNodes['RENAVAM'].Text;
end;

procedure TXMLRodo_veic.Set_RENAVAM(Value: UnicodeString);
begin
  ChildNodes['RENAVAM'].NodeValue := Value;
end;

function TXMLRodo_veic.Get_Placa: UnicodeString;
begin
  Result := ChildNodes['placa'].Text;
end;

procedure TXMLRodo_veic.Set_Placa(Value: UnicodeString);
begin
  ChildNodes['placa'].NodeValue := Value;
end;

function TXMLRodo_veic.Get_Tara: UnicodeString;
begin
  Result := ChildNodes['tara'].Text;
end;

procedure TXMLRodo_veic.Set_Tara(Value: UnicodeString);
begin
  ChildNodes['tara'].NodeValue := Value;
end;

function TXMLRodo_veic.Get_CapKG: UnicodeString;
begin
  Result := ChildNodes['capKG'].Text;
end;

procedure TXMLRodo_veic.Set_CapKG(Value: UnicodeString);
begin
  ChildNodes['capKG'].NodeValue := Value;
end;

function TXMLRodo_veic.Get_CapM3: UnicodeString;
begin
  Result := ChildNodes['capM3'].Text;
end;

procedure TXMLRodo_veic.Set_CapM3(Value: UnicodeString);
begin
  ChildNodes['capM3'].NodeValue := Value;
end;

function TXMLRodo_veic.Get_TpProp: UnicodeString;
begin
  Result := ChildNodes['tpProp'].Text;
end;

procedure TXMLRodo_veic.Set_TpProp(Value: UnicodeString);
begin
  ChildNodes['tpProp'].NodeValue := Value;
end;

function TXMLRodo_veic.Get_TpVeic: UnicodeString;
begin
  Result := ChildNodes['tpVeic'].Text;
end;

procedure TXMLRodo_veic.Set_TpVeic(Value: UnicodeString);
begin
  ChildNodes['tpVeic'].NodeValue := Value;
end;

function TXMLRodo_veic.Get_TpRod: UnicodeString;
begin
  Result := ChildNodes['tpRod'].Text;
end;

procedure TXMLRodo_veic.Set_TpRod(Value: UnicodeString);
begin
  ChildNodes['tpRod'].NodeValue := Value;
end;

function TXMLRodo_veic.Get_TpCar: UnicodeString;
begin
  Result := ChildNodes['tpCar'].Text;
end;

procedure TXMLRodo_veic.Set_TpCar(Value: UnicodeString);
begin
  ChildNodes['tpCar'].NodeValue := Value;
end;

function TXMLRodo_veic.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLRodo_veic.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

function TXMLRodo_veic.Get_Prop: IXMLRodo_veic_prop;
begin
  Result := ChildNodes['prop'] as IXMLRodo_veic_prop;
end;

{ TXMLRodo_veicList }

function TXMLRodo_veicList.Add: IXMLRodo_veic;
begin
  Result := AddItem(-1) as IXMLRodo_veic;
end;

function TXMLRodo_veicList.Insert(const Index: Integer): IXMLRodo_veic;
begin
  Result := AddItem(Index) as IXMLRodo_veic;
end;

function TXMLRodo_veicList.Get_Item(Index: Integer): IXMLRodo_veic;
begin
  Result := List[Index] as IXMLRodo_veic;
end;

{ TXMLRodo_veic_prop }

function TXMLRodo_veic_prop.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLRodo_veic_prop.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLRodo_veic_prop.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLRodo_veic_prop.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLRodo_veic_prop.Get_RNTRC: UnicodeString;
begin
  Result := ChildNodes['RNTRC'].Text;
end;

procedure TXMLRodo_veic_prop.Set_RNTRC(Value: UnicodeString);
begin
  ChildNodes['RNTRC'].NodeValue := Value;
end;

function TXMLRodo_veic_prop.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLRodo_veic_prop.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLRodo_veic_prop.Get_IE: UnicodeString;
begin
  Result := ChildNodes['IE'].Text;
end;

procedure TXMLRodo_veic_prop.Set_IE(Value: UnicodeString);
begin
  ChildNodes['IE'].NodeValue := Value;
end;

function TXMLRodo_veic_prop.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLRodo_veic_prop.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

function TXMLRodo_veic_prop.Get_TpProp: UnicodeString;
begin
  Result := ChildNodes['tpProp'].Text;
end;

procedure TXMLRodo_veic_prop.Set_TpProp(Value: UnicodeString);
begin
  ChildNodes['tpProp'].NodeValue := Value;
end;

{ TXMLRodo_lacRodo }

function TXMLRodo_lacRodo.Get_NLacre: UnicodeString;
begin
  Result := ChildNodes['nLacre'].Text;
end;

procedure TXMLRodo_lacRodo.Set_NLacre(Value: UnicodeString);
begin
  ChildNodes['nLacre'].NodeValue := Value;
end;

{ TXMLRodo_lacRodoList }

function TXMLRodo_lacRodoList.Add: IXMLRodo_lacRodo;
begin
  Result := AddItem(-1) as IXMLRodo_lacRodo;
end;

function TXMLRodo_lacRodoList.Insert(const Index: Integer): IXMLRodo_lacRodo;
begin
  Result := AddItem(Index) as IXMLRodo_lacRodo;
end;

function TXMLRodo_lacRodoList.Get_Item(Index: Integer): IXMLRodo_lacRodo;
begin
  Result := List[Index] as IXMLRodo_lacRodo;
end;

{ TXMLRodo_moto }

function TXMLRodo_moto.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLRodo_moto.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLRodo_moto.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLRodo_moto.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

{ TXMLRodo_motoList }

function TXMLRodo_motoList.Add: IXMLRodo_moto;
begin
  Result := AddItem(-1) as IXMLRodo_moto;
end;

function TXMLRodo_motoList.Insert(const Index: Integer): IXMLRodo_moto;
begin
  Result := AddItem(Index) as IXMLRodo_moto;
end;

function TXMLRodo_motoList.Get_Item(Index: Integer): IXMLRodo_moto;
begin
  Result := List[Index] as IXMLRodo_moto;
end;

{ TXMLTCTe }

procedure TXMLTCTe.AfterConstruction;
begin
  RegisterChildNode('infCte', TXMLInfCte);
  RegisterChildNode('Signature', TXMLSignatureType_ds);
  inherited;
end;

function TXMLTCTe.Get_InfCte: IXMLInfCte;
begin
  Result := ChildNodes['infCte'] as IXMLInfCte;
end;

function TXMLTCTe.Get_Signature: IXMLSignatureType_ds;
begin
  Result := ChildNodes['Signature'] as IXMLSignatureType_ds;
end;

{ TXMLTCTeList }

function TXMLTCTeList.Add: IXMLTCTe;
begin
  Result := AddItem(-1) as IXMLTCTe;
end;

function TXMLTCTeList.Insert(const Index: Integer): IXMLTCTe;
begin
  Result := AddItem(Index) as IXMLTCTe;
end;

function TXMLTCTeList.Get_Item(Index: Integer): IXMLTCTe;
begin
  Result := List[Index] as IXMLTCTe;
end;

{ TXMLInfCte }

procedure TXMLInfCte.AfterConstruction;
begin
  RegisterChildNode('ide', TXMLIde);
  RegisterChildNode('compl', TXMLCompl);
  RegisterChildNode('emit', TXMLEmit);
  RegisterChildNode('rem', TXMLRem);
  RegisterChildNode('exped', TXMLExped);
  RegisterChildNode('receb', TXMLReceb);
  RegisterChildNode('dest', TXMLDest);
  RegisterChildNode('vPrest', TXMLVPrest);
  RegisterChildNode('imp', TXMLImp);
  RegisterChildNode('infCTeNorm', TXMLInfCTeNorm);
  RegisterChildNode('infCteComp', TXMLInfCteComp);
  RegisterChildNode('infCteAnu', TXMLInfCteAnu);
  RegisterChildNode('autXML', TXMLAutXML);
  FAutXML := CreateCollection(TXMLAutXMLList, IXMLAutXML, 'autXML') as IXMLAutXMLList;
  inherited;
end;

function TXMLInfCte.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLInfCte.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLInfCte.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLInfCte.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLInfCte.Get_Ide: IXMLIde;
begin
  Result := ChildNodes['ide'] as IXMLIde;
end;

function TXMLInfCte.Get_Compl: IXMLCompl;
begin
  Result := ChildNodes['compl'] as IXMLCompl;
end;

function TXMLInfCte.Get_Emit: IXMLEmit;
begin
  Result := ChildNodes['emit'] as IXMLEmit;
end;

function TXMLInfCte.Get_Rem: IXMLRem;
begin
  Result := ChildNodes['rem'] as IXMLRem;
end;

function TXMLInfCte.Get_Exped: IXMLExped;
begin
  Result := ChildNodes['exped'] as IXMLExped;
end;

function TXMLInfCte.Get_Receb: IXMLReceb;
begin
  Result := ChildNodes['receb'] as IXMLReceb;
end;

function TXMLInfCte.Get_Dest: IXMLDest;
begin
  Result := ChildNodes['dest'] as IXMLDest;
end;

function TXMLInfCte.Get_VPrest: IXMLVPrest;
begin
  Result := ChildNodes['vPrest'] as IXMLVPrest;
end;

function TXMLInfCte.Get_Imp: IXMLImp;
begin
  Result := ChildNodes['imp'] as IXMLImp;
end;

function TXMLInfCte.Get_InfCTeNorm: IXMLInfCTeNorm;
begin
  Result := ChildNodes['infCTeNorm'] as IXMLInfCTeNorm;
end;

function TXMLInfCte.Get_InfCteComp: IXMLInfCteComp;
begin
  Result := ChildNodes['infCteComp'] as IXMLInfCteComp;
end;

function TXMLInfCte.Get_InfCteAnu: IXMLInfCteAnu;
begin
  Result := ChildNodes['infCteAnu'] as IXMLInfCteAnu;
end;

function TXMLInfCte.Get_AutXML: IXMLAutXMLList;
begin
  Result := FAutXML;
end;

{ TXMLIde }

procedure TXMLIde.AfterConstruction;
begin
  RegisterChildNode('toma03', TXMLToma03);
  RegisterChildNode('toma4', TXMLToma4);
  inherited;
end;

function TXMLIde.Get_CUF: UnicodeString;
begin
  Result := ChildNodes['cUF'].Text;
end;

procedure TXMLIde.Set_CUF(Value: UnicodeString);
begin
  ChildNodes['cUF'].NodeValue := Value;
end;

function TXMLIde.Get_CCT: UnicodeString;
begin
  Result := ChildNodes['cCT'].Text;
end;

procedure TXMLIde.Set_CCT(Value: UnicodeString);
begin
  ChildNodes['cCT'].NodeValue := Value;
end;

function TXMLIde.Get_CFOP: UnicodeString;
begin
  Result := ChildNodes['CFOP'].Text;
end;

procedure TXMLIde.Set_CFOP(Value: UnicodeString);
begin
  ChildNodes['CFOP'].NodeValue := Value;
end;

function TXMLIde.Get_NatOp: UnicodeString;
begin
  Result := ChildNodes['natOp'].Text;
end;

procedure TXMLIde.Set_NatOp(Value: UnicodeString);
begin
  ChildNodes['natOp'].NodeValue := Value;
end;

function TXMLIde.Get_ForPag: UnicodeString;
begin
  Result := ChildNodes['forPag'].Text;
end;

procedure TXMLIde.Set_ForPag(Value: UnicodeString);
begin
  ChildNodes['forPag'].NodeValue := Value;
end;

function TXMLIde.Get_Mod_: UnicodeString;
begin
  Result := ChildNodes['mod'].Text;
end;

procedure TXMLIde.Set_Mod_(Value: UnicodeString);
begin
  ChildNodes['mod'].NodeValue := Value;
end;

function TXMLIde.Get_Serie: UnicodeString;
begin
  Result := ChildNodes['serie'].Text;
end;

procedure TXMLIde.Set_Serie(Value: UnicodeString);
begin
  ChildNodes['serie'].NodeValue := Value;
end;

function TXMLIde.Get_NCT: UnicodeString;
begin
  Result := ChildNodes['nCT'].Text;
end;

procedure TXMLIde.Set_NCT(Value: UnicodeString);
begin
  ChildNodes['nCT'].NodeValue := Value;
end;

function TXMLIde.Get_DhEmi: UnicodeString;
begin
  Result := ChildNodes['dhEmi'].Text;
end;

procedure TXMLIde.Set_DhEmi(Value: UnicodeString);
begin
  ChildNodes['dhEmi'].NodeValue := Value;
end;

function TXMLIde.Get_TpImp: UnicodeString;
begin
  Result := ChildNodes['tpImp'].Text;
end;

procedure TXMLIde.Set_TpImp(Value: UnicodeString);
begin
  ChildNodes['tpImp'].NodeValue := Value;
end;

function TXMLIde.Get_TpEmis: UnicodeString;
begin
  Result := ChildNodes['tpEmis'].Text;
end;

procedure TXMLIde.Set_TpEmis(Value: UnicodeString);
begin
  ChildNodes['tpEmis'].NodeValue := Value;
end;

function TXMLIde.Get_CDV: UnicodeString;
begin
  Result := ChildNodes['cDV'].Text;
end;

procedure TXMLIde.Set_CDV(Value: UnicodeString);
begin
  ChildNodes['cDV'].NodeValue := Value;
end;

function TXMLIde.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLIde.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLIde.Get_TpCTe: UnicodeString;
begin
  Result := ChildNodes['tpCTe'].Text;
end;

procedure TXMLIde.Set_TpCTe(Value: UnicodeString);
begin
  ChildNodes['tpCTe'].NodeValue := Value;
end;

function TXMLIde.Get_ProcEmi: UnicodeString;
begin
  Result := ChildNodes['procEmi'].Text;
end;

procedure TXMLIde.Set_ProcEmi(Value: UnicodeString);
begin
  ChildNodes['procEmi'].NodeValue := Value;
end;

function TXMLIde.Get_VerProc: UnicodeString;
begin
  Result := ChildNodes['verProc'].Text;
end;

procedure TXMLIde.Set_VerProc(Value: UnicodeString);
begin
  ChildNodes['verProc'].NodeValue := Value;
end;

function TXMLIde.Get_RefCTE: UnicodeString;
begin
  Result := ChildNodes['refCTE'].Text;
end;

procedure TXMLIde.Set_RefCTE(Value: UnicodeString);
begin
  ChildNodes['refCTE'].NodeValue := Value;
end;

function TXMLIde.Get_CMunEnv: UnicodeString;
begin
  Result := ChildNodes['cMunEnv'].Text;
end;

procedure TXMLIde.Set_CMunEnv(Value: UnicodeString);
begin
  ChildNodes['cMunEnv'].NodeValue := Value;
end;

function TXMLIde.Get_XMunEnv: UnicodeString;
begin
  Result := ChildNodes['xMunEnv'].Text;
end;

procedure TXMLIde.Set_XMunEnv(Value: UnicodeString);
begin
  ChildNodes['xMunEnv'].NodeValue := Value;
end;

function TXMLIde.Get_UFEnv: UnicodeString;
begin
  Result := ChildNodes['UFEnv'].Text;
end;

procedure TXMLIde.Set_UFEnv(Value: UnicodeString);
begin
  ChildNodes['UFEnv'].NodeValue := Value;
end;

function TXMLIde.Get_Modal: UnicodeString;
begin
  Result := ChildNodes['modal'].Text;
end;

procedure TXMLIde.Set_Modal(Value: UnicodeString);
begin
  ChildNodes['modal'].NodeValue := Value;
end;

function TXMLIde.Get_TpServ: UnicodeString;
begin
  Result := ChildNodes['tpServ'].Text;
end;

procedure TXMLIde.Set_TpServ(Value: UnicodeString);
begin
  ChildNodes['tpServ'].NodeValue := Value;
end;

function TXMLIde.Get_CMunIni: UnicodeString;
begin
  Result := ChildNodes['cMunIni'].Text;
end;

procedure TXMLIde.Set_CMunIni(Value: UnicodeString);
begin
  ChildNodes['cMunIni'].NodeValue := Value;
end;

function TXMLIde.Get_XMunIni: UnicodeString;
begin
  Result := ChildNodes['xMunIni'].Text;
end;

procedure TXMLIde.Set_XMunIni(Value: UnicodeString);
begin
  ChildNodes['xMunIni'].NodeValue := Value;
end;

function TXMLIde.Get_UFIni: UnicodeString;
begin
  Result := ChildNodes['UFIni'].Text;
end;

procedure TXMLIde.Set_UFIni(Value: UnicodeString);
begin
  ChildNodes['UFIni'].NodeValue := Value;
end;

function TXMLIde.Get_CMunFim: UnicodeString;
begin
  Result := ChildNodes['cMunFim'].Text;
end;

procedure TXMLIde.Set_CMunFim(Value: UnicodeString);
begin
  ChildNodes['cMunFim'].NodeValue := Value;
end;

function TXMLIde.Get_XMunFim: UnicodeString;
begin
  Result := ChildNodes['xMunFim'].Text;
end;

procedure TXMLIde.Set_XMunFim(Value: UnicodeString);
begin
  ChildNodes['xMunFim'].NodeValue := Value;
end;

function TXMLIde.Get_UFFim: UnicodeString;
begin
  Result := ChildNodes['UFFim'].Text;
end;

procedure TXMLIde.Set_UFFim(Value: UnicodeString);
begin
  ChildNodes['UFFim'].NodeValue := Value;
end;

function TXMLIde.Get_Retira: UnicodeString;
begin
  Result := ChildNodes['retira'].Text;
end;

procedure TXMLIde.Set_Retira(Value: UnicodeString);
begin
  ChildNodes['retira'].NodeValue := Value;
end;

function TXMLIde.Get_XDetRetira: UnicodeString;
begin
  Result := ChildNodes['xDetRetira'].Text;
end;

procedure TXMLIde.Set_XDetRetira(Value: UnicodeString);
begin
  ChildNodes['xDetRetira'].NodeValue := Value;
end;

function TXMLIde.Get_Toma03: IXMLToma03;
begin
  Result := ChildNodes['toma03'] as IXMLToma03;
end;

function TXMLIde.Get_Toma4: IXMLToma4;
begin
  Result := ChildNodes['toma4'] as IXMLToma4;
end;

function TXMLIde.Get_DhCont: UnicodeString;
begin
  Result := ChildNodes['dhCont'].Text;
end;

procedure TXMLIde.Set_DhCont(Value: UnicodeString);
begin
  ChildNodes['dhCont'].NodeValue := Value;
end;

function TXMLIde.Get_XJust: UnicodeString;
begin
  Result := ChildNodes['xJust'].Text;
end;

procedure TXMLIde.Set_XJust(Value: UnicodeString);
begin
  ChildNodes['xJust'].NodeValue := Value;
end;

{ TXMLToma03 }

function TXMLToma03.Get_Toma: UnicodeString;
begin
  Result := ChildNodes['toma'].Text;
end;

procedure TXMLToma03.Set_Toma(Value: UnicodeString);
begin
  ChildNodes['toma'].NodeValue := Value;
end;

{ TXMLToma4 }

procedure TXMLToma4.AfterConstruction;
begin
  RegisterChildNode('enderToma', TXMLTEndereco);
  inherited;
end;

function TXMLToma4.Get_Toma: UnicodeString;
begin
  Result := ChildNodes['toma'].Text;
end;

procedure TXMLToma4.Set_Toma(Value: UnicodeString);
begin
  ChildNodes['toma'].NodeValue := Value;
end;

function TXMLToma4.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLToma4.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLToma4.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLToma4.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLToma4.Get_IE: UnicodeString;
begin
  Result := ChildNodes['IE'].Text;
end;

procedure TXMLToma4.Set_IE(Value: UnicodeString);
begin
  ChildNodes['IE'].NodeValue := Value;
end;

function TXMLToma4.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLToma4.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLToma4.Get_XFant: UnicodeString;
begin
  Result := ChildNodes['xFant'].Text;
end;

procedure TXMLToma4.Set_XFant(Value: UnicodeString);
begin
  ChildNodes['xFant'].NodeValue := Value;
end;

function TXMLToma4.Get_Fone: UnicodeString;
begin
  Result := ChildNodes['fone'].Text;
end;

procedure TXMLToma4.Set_Fone(Value: UnicodeString);
begin
  ChildNodes['fone'].NodeValue := Value;
end;

function TXMLToma4.Get_EnderToma: IXMLTEndereco;
begin
  Result := ChildNodes['enderToma'] as IXMLTEndereco;
end;

function TXMLToma4.Get_Email: UnicodeString;
begin
  Result := ChildNodes['email'].Text;
end;

procedure TXMLToma4.Set_Email(Value: UnicodeString);
begin
  ChildNodes['email'].NodeValue := Value;
end;

{ TXMLTEndereco }

function TXMLTEndereco.Get_XLgr: UnicodeString;
begin
  Result := ChildNodes['xLgr'].Text;
end;

procedure TXMLTEndereco.Set_XLgr(Value: UnicodeString);
begin
  ChildNodes['xLgr'].NodeValue := Value;
end;

function TXMLTEndereco.Get_Nro: UnicodeString;
begin
  Result := ChildNodes['nro'].Text;
end;

procedure TXMLTEndereco.Set_Nro(Value: UnicodeString);
begin
  ChildNodes['nro'].NodeValue := Value;
end;

function TXMLTEndereco.Get_XCpl: UnicodeString;
begin
  Result := ChildNodes['xCpl'].Text;
end;

procedure TXMLTEndereco.Set_XCpl(Value: UnicodeString);
begin
  ChildNodes['xCpl'].NodeValue := Value;
end;

function TXMLTEndereco.Get_XBairro: UnicodeString;
begin
  Result := ChildNodes['xBairro'].Text;
end;

procedure TXMLTEndereco.Set_XBairro(Value: UnicodeString);
begin
  ChildNodes['xBairro'].NodeValue := Value;
end;

function TXMLTEndereco.Get_CMun: UnicodeString;
begin
  Result := ChildNodes['cMun'].Text;
end;

procedure TXMLTEndereco.Set_CMun(Value: UnicodeString);
begin
  ChildNodes['cMun'].NodeValue := Value;
end;

function TXMLTEndereco.Get_XMun: UnicodeString;
begin
  Result := ChildNodes['xMun'].Text;
end;

procedure TXMLTEndereco.Set_XMun(Value: UnicodeString);
begin
  ChildNodes['xMun'].NodeValue := Value;
end;

function TXMLTEndereco.Get_CEP: UnicodeString;
begin
  Result := ChildNodes['CEP'].Text;
end;

procedure TXMLTEndereco.Set_CEP(Value: UnicodeString);
begin
  ChildNodes['CEP'].NodeValue := Value;
end;

function TXMLTEndereco.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLTEndereco.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

function TXMLTEndereco.Get_CPais: UnicodeString;
begin
  Result := ChildNodes['cPais'].Text;
end;

procedure TXMLTEndereco.Set_CPais(Value: UnicodeString);
begin
  ChildNodes['cPais'].NodeValue := Value;
end;

function TXMLTEndereco.Get_XPais: UnicodeString;
begin
  Result := ChildNodes['xPais'].Text;
end;

procedure TXMLTEndereco.Set_XPais(Value: UnicodeString);
begin
  ChildNodes['xPais'].NodeValue := Value;
end;

{ TXMLCompl }

procedure TXMLCompl.AfterConstruction;
begin
  RegisterChildNode('fluxo', TXMLFluxo);
  RegisterChildNode('Entrega', TXMLEntrega);
  RegisterChildNode('ObsCont', TXMLObsCont);
  RegisterChildNode('ObsFisco', TXMLObsFisco);
  FObsCont := CreateCollection(TXMLObsContList, IXMLObsCont, 'ObsCont') as IXMLObsContList;
  FObsFisco := CreateCollection(TXMLObsFiscoList, IXMLObsFisco, 'ObsFisco') as IXMLObsFiscoList;
  inherited;
end;

function TXMLCompl.Get_XCaracAd: UnicodeString;
begin
  Result := ChildNodes['xCaracAd'].Text;
end;

procedure TXMLCompl.Set_XCaracAd(Value: UnicodeString);
begin
  ChildNodes['xCaracAd'].NodeValue := Value;
end;

function TXMLCompl.Get_XCaracSer: UnicodeString;
begin
  Result := ChildNodes['xCaracSer'].Text;
end;

procedure TXMLCompl.Set_XCaracSer(Value: UnicodeString);
begin
  ChildNodes['xCaracSer'].NodeValue := Value;
end;

function TXMLCompl.Get_XEmi: UnicodeString;
begin
  Result := ChildNodes['xEmi'].Text;
end;

procedure TXMLCompl.Set_XEmi(Value: UnicodeString);
begin
  ChildNodes['xEmi'].NodeValue := Value;
end;

function TXMLCompl.Get_Fluxo: IXMLFluxo;
begin
  Result := ChildNodes['fluxo'] as IXMLFluxo;
end;

function TXMLCompl.Get_Entrega: IXMLEntrega;
begin
  Result := ChildNodes['Entrega'] as IXMLEntrega;
end;

function TXMLCompl.Get_OrigCalc: UnicodeString;
begin
  Result := ChildNodes['origCalc'].Text;
end;

procedure TXMLCompl.Set_OrigCalc(Value: UnicodeString);
begin
  ChildNodes['origCalc'].NodeValue := Value;
end;

function TXMLCompl.Get_DestCalc: UnicodeString;
begin
  Result := ChildNodes['destCalc'].Text;
end;

procedure TXMLCompl.Set_DestCalc(Value: UnicodeString);
begin
  ChildNodes['destCalc'].NodeValue := Value;
end;

function TXMLCompl.Get_XObs: UnicodeString;
begin
  Result := ChildNodes['xObs'].Text;
end;

procedure TXMLCompl.Set_XObs(Value: UnicodeString);
begin
  ChildNodes['xObs'].NodeValue := Value;
end;

function TXMLCompl.Get_ObsCont: IXMLObsContList;
begin
  Result := FObsCont;
end;

function TXMLCompl.Get_ObsFisco: IXMLObsFiscoList;
begin
  Result := FObsFisco;
end;

{ TXMLFluxo }

procedure TXMLFluxo.AfterConstruction;
begin
  RegisterChildNode('pass', TXMLPass);
  FPass := CreateCollection(TXMLPassList, IXMLPass, 'pass') as IXMLPassList;
  inherited;
end;

function TXMLFluxo.Get_XOrig: UnicodeString;
begin
  Result := ChildNodes['xOrig'].Text;
end;

procedure TXMLFluxo.Set_XOrig(Value: UnicodeString);
begin
  ChildNodes['xOrig'].NodeValue := Value;
end;

function TXMLFluxo.Get_Pass: IXMLPassList;
begin
  Result := FPass;
end;

function TXMLFluxo.Get_XDest: UnicodeString;
begin
  Result := ChildNodes['xDest'].Text;
end;

procedure TXMLFluxo.Set_XDest(Value: UnicodeString);
begin
  ChildNodes['xDest'].NodeValue := Value;
end;

function TXMLFluxo.Get_XRota: UnicodeString;
begin
  Result := ChildNodes['xRota'].Text;
end;

procedure TXMLFluxo.Set_XRota(Value: UnicodeString);
begin
  ChildNodes['xRota'].NodeValue := Value;
end;

{ TXMLPass }

function TXMLPass.Get_XPass: UnicodeString;
begin
  Result := ChildNodes['xPass'].Text;
end;

procedure TXMLPass.Set_XPass(Value: UnicodeString);
begin
  ChildNodes['xPass'].NodeValue := Value;
end;

{ TXMLPassList }

function TXMLPassList.Add: IXMLPass;
begin
  Result := AddItem(-1) as IXMLPass;
end;

function TXMLPassList.Insert(const Index: Integer): IXMLPass;
begin
  Result := AddItem(Index) as IXMLPass;
end;

function TXMLPassList.Get_Item(Index: Integer): IXMLPass;
begin
  Result := List[Index] as IXMLPass;
end;

{ TXMLEntrega }

procedure TXMLEntrega.AfterConstruction;
begin
  RegisterChildNode('semData', TXMLSemData);
  RegisterChildNode('comData', TXMLComData);
  RegisterChildNode('noPeriodo', TXMLNoPeriodo);
  RegisterChildNode('semHora', TXMLSemHora);
  RegisterChildNode('comHora', TXMLComHora);
  RegisterChildNode('noInter', TXMLNoInter);
  inherited;
end;

function TXMLEntrega.Get_SemData: IXMLSemData;
begin
  Result := ChildNodes['semData'] as IXMLSemData;
end;

function TXMLEntrega.Get_ComData: IXMLComData;
begin
  Result := ChildNodes['comData'] as IXMLComData;
end;

function TXMLEntrega.Get_NoPeriodo: IXMLNoPeriodo;
begin
  Result := ChildNodes['noPeriodo'] as IXMLNoPeriodo;
end;

function TXMLEntrega.Get_SemHora: IXMLSemHora;
begin
  Result := ChildNodes['semHora'] as IXMLSemHora;
end;

function TXMLEntrega.Get_ComHora: IXMLComHora;
begin
  Result := ChildNodes['comHora'] as IXMLComHora;
end;

function TXMLEntrega.Get_NoInter: IXMLNoInter;
begin
  Result := ChildNodes['noInter'] as IXMLNoInter;
end;

{ TXMLSemData }

function TXMLSemData.Get_TpPer: UnicodeString;
begin
  Result := ChildNodes['tpPer'].Text;
end;

procedure TXMLSemData.Set_TpPer(Value: UnicodeString);
begin
  ChildNodes['tpPer'].NodeValue := Value;
end;

{ TXMLComData }

function TXMLComData.Get_TpPer: UnicodeString;
begin
  Result := ChildNodes['tpPer'].Text;
end;

procedure TXMLComData.Set_TpPer(Value: UnicodeString);
begin
  ChildNodes['tpPer'].NodeValue := Value;
end;

function TXMLComData.Get_DProg: UnicodeString;
begin
  Result := ChildNodes['dProg'].Text;
end;

procedure TXMLComData.Set_DProg(Value: UnicodeString);
begin
  ChildNodes['dProg'].NodeValue := Value;
end;

{ TXMLNoPeriodo }

function TXMLNoPeriodo.Get_TpPer: UnicodeString;
begin
  Result := ChildNodes['tpPer'].Text;
end;

procedure TXMLNoPeriodo.Set_TpPer(Value: UnicodeString);
begin
  ChildNodes['tpPer'].NodeValue := Value;
end;

function TXMLNoPeriodo.Get_DIni: UnicodeString;
begin
  Result := ChildNodes['dIni'].Text;
end;

procedure TXMLNoPeriodo.Set_DIni(Value: UnicodeString);
begin
  ChildNodes['dIni'].NodeValue := Value;
end;

function TXMLNoPeriodo.Get_DFim: UnicodeString;
begin
  Result := ChildNodes['dFim'].Text;
end;

procedure TXMLNoPeriodo.Set_DFim(Value: UnicodeString);
begin
  ChildNodes['dFim'].NodeValue := Value;
end;

{ TXMLSemHora }

function TXMLSemHora.Get_TpHor: UnicodeString;
begin
  Result := ChildNodes['tpHor'].Text;
end;

procedure TXMLSemHora.Set_TpHor(Value: UnicodeString);
begin
  ChildNodes['tpHor'].NodeValue := Value;
end;

{ TXMLComHora }

function TXMLComHora.Get_TpHor: UnicodeString;
begin
  Result := ChildNodes['tpHor'].Text;
end;

procedure TXMLComHora.Set_TpHor(Value: UnicodeString);
begin
  ChildNodes['tpHor'].NodeValue := Value;
end;

function TXMLComHora.Get_HProg: UnicodeString;
begin
  Result := ChildNodes['hProg'].Text;
end;

procedure TXMLComHora.Set_HProg(Value: UnicodeString);
begin
  ChildNodes['hProg'].NodeValue := Value;
end;

{ TXMLNoInter }

function TXMLNoInter.Get_TpHor: UnicodeString;
begin
  Result := ChildNodes['tpHor'].Text;
end;

procedure TXMLNoInter.Set_TpHor(Value: UnicodeString);
begin
  ChildNodes['tpHor'].NodeValue := Value;
end;

function TXMLNoInter.Get_HIni: UnicodeString;
begin
  Result := ChildNodes['hIni'].Text;
end;

procedure TXMLNoInter.Set_HIni(Value: UnicodeString);
begin
  ChildNodes['hIni'].NodeValue := Value;
end;

function TXMLNoInter.Get_HFim: UnicodeString;
begin
  Result := ChildNodes['hFim'].Text;
end;

procedure TXMLNoInter.Set_HFim(Value: UnicodeString);
begin
  ChildNodes['hFim'].NodeValue := Value;
end;

{ TXMLObsCont }

function TXMLObsCont.Get_XCampo: UnicodeString;
begin
  Result := AttributeNodes['xCampo'].Text;
end;

procedure TXMLObsCont.Set_XCampo(Value: UnicodeString);
begin
  SetAttribute('xCampo', Value);
end;

function TXMLObsCont.Get_XTexto: UnicodeString;
begin
  Result := ChildNodes['xTexto'].Text;
end;

procedure TXMLObsCont.Set_XTexto(Value: UnicodeString);
begin
  ChildNodes['xTexto'].NodeValue := Value;
end;

{ TXMLObsContList }

function TXMLObsContList.Add: IXMLObsCont;
begin
  Result := AddItem(-1) as IXMLObsCont;
end;

function TXMLObsContList.Insert(const Index: Integer): IXMLObsCont;
begin
  Result := AddItem(Index) as IXMLObsCont;
end;

function TXMLObsContList.Get_Item(Index: Integer): IXMLObsCont;
begin
  Result := List[Index] as IXMLObsCont;
end;

{ TXMLObsFisco }

function TXMLObsFisco.Get_XCampo: UnicodeString;
begin
  Result := AttributeNodes['xCampo'].Text;
end;

procedure TXMLObsFisco.Set_XCampo(Value: UnicodeString);
begin
  SetAttribute('xCampo', Value);
end;

function TXMLObsFisco.Get_XTexto: UnicodeString;
begin
  Result := ChildNodes['xTexto'].Text;
end;

procedure TXMLObsFisco.Set_XTexto(Value: UnicodeString);
begin
  ChildNodes['xTexto'].NodeValue := Value;
end;

{ TXMLObsFiscoList }

function TXMLObsFiscoList.Add: IXMLObsFisco;
begin
  Result := AddItem(-1) as IXMLObsFisco;
end;

function TXMLObsFiscoList.Insert(const Index: Integer): IXMLObsFisco;
begin
  Result := AddItem(Index) as IXMLObsFisco;
end;

function TXMLObsFiscoList.Get_Item(Index: Integer): IXMLObsFisco;
begin
  Result := List[Index] as IXMLObsFisco;
end;

{ TXMLEmit }

procedure TXMLEmit.AfterConstruction;
begin
  RegisterChildNode('enderEmit', TXMLTEndeEmi);
  inherited;
end;

function TXMLEmit.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLEmit.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLEmit.Get_IE: UnicodeString;
begin
  Result := ChildNodes['IE'].Text;
end;

procedure TXMLEmit.Set_IE(Value: UnicodeString);
begin
  ChildNodes['IE'].NodeValue := Value;
end;

function TXMLEmit.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLEmit.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLEmit.Get_XFant: UnicodeString;
begin
  Result := ChildNodes['xFant'].Text;
end;

procedure TXMLEmit.Set_XFant(Value: UnicodeString);
begin
  ChildNodes['xFant'].NodeValue := Value;
end;

function TXMLEmit.Get_EnderEmit: IXMLTEndeEmi;
begin
  Result := ChildNodes['enderEmit'] as IXMLTEndeEmi;
end;

{ TXMLTEndeEmi }

function TXMLTEndeEmi.Get_XLgr: UnicodeString;
begin
  Result := ChildNodes['xLgr'].Text;
end;

procedure TXMLTEndeEmi.Set_XLgr(Value: UnicodeString);
begin
  ChildNodes['xLgr'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_Nro: UnicodeString;
begin
  Result := ChildNodes['nro'].Text;
end;

procedure TXMLTEndeEmi.Set_Nro(Value: UnicodeString);
begin
  ChildNodes['nro'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_XCpl: UnicodeString;
begin
  Result := ChildNodes['xCpl'].Text;
end;

procedure TXMLTEndeEmi.Set_XCpl(Value: UnicodeString);
begin
  ChildNodes['xCpl'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_XBairro: UnicodeString;
begin
  Result := ChildNodes['xBairro'].Text;
end;

procedure TXMLTEndeEmi.Set_XBairro(Value: UnicodeString);
begin
  ChildNodes['xBairro'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_CMun: UnicodeString;
begin
  Result := ChildNodes['cMun'].Text;
end;

procedure TXMLTEndeEmi.Set_CMun(Value: UnicodeString);
begin
  ChildNodes['cMun'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_XMun: UnicodeString;
begin
  Result := ChildNodes['xMun'].Text;
end;

procedure TXMLTEndeEmi.Set_XMun(Value: UnicodeString);
begin
  ChildNodes['xMun'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_CEP: UnicodeString;
begin
  Result := ChildNodes['CEP'].Text;
end;

procedure TXMLTEndeEmi.Set_CEP(Value: UnicodeString);
begin
  ChildNodes['CEP'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLTEndeEmi.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_Fone: UnicodeString;
begin
  Result := ChildNodes['fone'].Text;
end;

procedure TXMLTEndeEmi.Set_Fone(Value: UnicodeString);
begin
  ChildNodes['fone'].NodeValue := Value;
end;

{ TXMLRem }

procedure TXMLRem.AfterConstruction;
begin
  RegisterChildNode('enderReme', TXMLTEndereco);
  inherited;
end;

function TXMLRem.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLRem.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLRem.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLRem.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLRem.Get_IE: UnicodeString;
begin
  Result := ChildNodes['IE'].Text;
end;

procedure TXMLRem.Set_IE(Value: UnicodeString);
begin
  ChildNodes['IE'].NodeValue := Value;
end;

function TXMLRem.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLRem.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLRem.Get_XFant: UnicodeString;
begin
  Result := ChildNodes['xFant'].Text;
end;

procedure TXMLRem.Set_XFant(Value: UnicodeString);
begin
  ChildNodes['xFant'].NodeValue := Value;
end;

function TXMLRem.Get_Fone: UnicodeString;
begin
  Result := ChildNodes['fone'].Text;
end;

procedure TXMLRem.Set_Fone(Value: UnicodeString);
begin
  ChildNodes['fone'].NodeValue := Value;
end;

function TXMLRem.Get_EnderReme: IXMLTEndereco;
begin
  Result := ChildNodes['enderReme'] as IXMLTEndereco;
end;

function TXMLRem.Get_Email: UnicodeString;
begin
  Result := ChildNodes['email'].Text;
end;

procedure TXMLRem.Set_Email(Value: UnicodeString);
begin
  ChildNodes['email'].NodeValue := Value;
end;

{ TXMLExped }

procedure TXMLExped.AfterConstruction;
begin
  RegisterChildNode('enderExped', TXMLTEndereco);
  inherited;
end;

function TXMLExped.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLExped.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLExped.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLExped.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLExped.Get_IE: UnicodeString;
begin
  Result := ChildNodes['IE'].Text;
end;

procedure TXMLExped.Set_IE(Value: UnicodeString);
begin
  ChildNodes['IE'].NodeValue := Value;
end;

function TXMLExped.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLExped.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLExped.Get_Fone: UnicodeString;
begin
  Result := ChildNodes['fone'].Text;
end;

procedure TXMLExped.Set_Fone(Value: UnicodeString);
begin
  ChildNodes['fone'].NodeValue := Value;
end;

function TXMLExped.Get_EnderExped: IXMLTEndereco;
begin
  Result := ChildNodes['enderExped'] as IXMLTEndereco;
end;

function TXMLExped.Get_Email: UnicodeString;
begin
  Result := ChildNodes['email'].Text;
end;

procedure TXMLExped.Set_Email(Value: UnicodeString);
begin
  ChildNodes['email'].NodeValue := Value;
end;

{ TXMLReceb }

procedure TXMLReceb.AfterConstruction;
begin
  RegisterChildNode('enderReceb', TXMLTEndereco);
  inherited;
end;

function TXMLReceb.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLReceb.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLReceb.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLReceb.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLReceb.Get_IE: UnicodeString;
begin
  Result := ChildNodes['IE'].Text;
end;

procedure TXMLReceb.Set_IE(Value: UnicodeString);
begin
  ChildNodes['IE'].NodeValue := Value;
end;

function TXMLReceb.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLReceb.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLReceb.Get_Fone: UnicodeString;
begin
  Result := ChildNodes['fone'].Text;
end;

procedure TXMLReceb.Set_Fone(Value: UnicodeString);
begin
  ChildNodes['fone'].NodeValue := Value;
end;

function TXMLReceb.Get_EnderReceb: IXMLTEndereco;
begin
  Result := ChildNodes['enderReceb'] as IXMLTEndereco;
end;

function TXMLReceb.Get_Email: UnicodeString;
begin
  Result := ChildNodes['email'].Text;
end;

procedure TXMLReceb.Set_Email(Value: UnicodeString);
begin
  ChildNodes['email'].NodeValue := Value;
end;

{ TXMLDest }

procedure TXMLDest.AfterConstruction;
begin
  RegisterChildNode('enderDest', TXMLTEndereco);
  inherited;
end;

function TXMLDest.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLDest.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLDest.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLDest.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLDest.Get_IE: UnicodeString;
begin
  Result := ChildNodes['IE'].Text;
end;

procedure TXMLDest.Set_IE(Value: UnicodeString);
begin
  ChildNodes['IE'].NodeValue := Value;
end;

function TXMLDest.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLDest.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLDest.Get_Fone: UnicodeString;
begin
  Result := ChildNodes['fone'].Text;
end;

procedure TXMLDest.Set_Fone(Value: UnicodeString);
begin
  ChildNodes['fone'].NodeValue := Value;
end;

function TXMLDest.Get_ISUF: UnicodeString;
begin
  Result := ChildNodes['ISUF'].Text;
end;

procedure TXMLDest.Set_ISUF(Value: UnicodeString);
begin
  ChildNodes['ISUF'].NodeValue := Value;
end;

function TXMLDest.Get_EnderDest: IXMLTEndereco;
begin
  Result := ChildNodes['enderDest'] as IXMLTEndereco;
end;

function TXMLDest.Get_Email: UnicodeString;
begin
  Result := ChildNodes['email'].Text;
end;

procedure TXMLDest.Set_Email(Value: UnicodeString);
begin
  ChildNodes['email'].NodeValue := Value;
end;

{ TXMLVPrest }

procedure TXMLVPrest.AfterConstruction;
begin
  RegisterChildNode('Comp', TXMLComp);
  FComp := CreateCollection(TXMLCompList, IXMLComp, 'Comp') as IXMLCompList;
  inherited;
end;

function TXMLVPrest.Get_VTPrest: UnicodeString;
begin
  Result := ChildNodes['vTPrest'].Text;
end;

procedure TXMLVPrest.Set_VTPrest(Value: UnicodeString);
begin
  ChildNodes['vTPrest'].NodeValue := Value;
end;

function TXMLVPrest.Get_VRec: UnicodeString;
begin
  Result := ChildNodes['vRec'].Text;
end;

procedure TXMLVPrest.Set_VRec(Value: UnicodeString);
begin
  ChildNodes['vRec'].NodeValue := Value;
end;

function TXMLVPrest.Get_Comp: IXMLCompList;
begin
  Result := FComp;
end;

{ TXMLComp }

function TXMLComp.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLComp.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLComp.Get_VComp: UnicodeString;
begin
  Result := ChildNodes['vComp'].Text;
end;

procedure TXMLComp.Set_VComp(Value: UnicodeString);
begin
  ChildNodes['vComp'].NodeValue := Value;
end;

{ TXMLCompList }

function TXMLCompList.Add: IXMLComp;
begin
  Result := AddItem(-1) as IXMLComp;
end;

function TXMLCompList.Insert(const Index: Integer): IXMLComp;
begin
  Result := AddItem(Index) as IXMLComp;
end;

function TXMLCompList.Get_Item(Index: Integer): IXMLComp;
begin
  Result := List[Index] as IXMLComp;
end;

{ TXMLImp }

procedure TXMLImp.AfterConstruction;
begin
  RegisterChildNode('ICMS', TXMLTImp);
  RegisterChildNode('ICMSUFFim', TXMLICMSUFFim);
  inherited;
end;

function TXMLImp.Get_ICMS: IXMLTImp;
begin
  Result := ChildNodes['ICMS'] as IXMLTImp;
end;

function TXMLImp.Get_VTotTrib: UnicodeString;
begin
  Result := ChildNodes['vTotTrib'].Text;
end;

procedure TXMLImp.Set_VTotTrib(Value: UnicodeString);
begin
  ChildNodes['vTotTrib'].NodeValue := Value;
end;

function TXMLImp.Get_InfAdFisco: UnicodeString;
begin
  Result := ChildNodes['infAdFisco'].Text;
end;

procedure TXMLImp.Set_InfAdFisco(Value: UnicodeString);
begin
  ChildNodes['infAdFisco'].NodeValue := Value;
end;

function TXMLImp.Get_ICMSUFFim: IXMLICMSUFFim;
begin
  Result := ChildNodes['ICMSUFFim'] as IXMLICMSUFFim;
end;

{ TXMLTImp }

procedure TXMLTImp.AfterConstruction;
begin
  RegisterChildNode('ICMS00', TXMLICMS00);
  RegisterChildNode('ICMS20', TXMLICMS20);
  RegisterChildNode('ICMS45', TXMLICMS45);
  RegisterChildNode('ICMS60', TXMLICMS60);
  RegisterChildNode('ICMS90', TXMLICMS90);
  RegisterChildNode('ICMSOutraUF', TXMLICMSOutraUF);
  RegisterChildNode('ICMSSN', TXMLICMSSN);
  inherited;
end;

function TXMLTImp.Get_ICMS00: IXMLICMS00;
begin
  Result := ChildNodes['ICMS00'] as IXMLICMS00;
end;

function TXMLTImp.Get_ICMS20: IXMLICMS20;
begin
  Result := ChildNodes['ICMS20'] as IXMLICMS20;
end;

function TXMLTImp.Get_ICMS45: IXMLICMS45;
begin
  Result := ChildNodes['ICMS45'] as IXMLICMS45;
end;

function TXMLTImp.Get_ICMS60: IXMLICMS60;
begin
  Result := ChildNodes['ICMS60'] as IXMLICMS60;
end;

function TXMLTImp.Get_ICMS90: IXMLICMS90;
begin
  Result := ChildNodes['ICMS90'] as IXMLICMS90;
end;

function TXMLTImp.Get_ICMSOutraUF: IXMLICMSOutraUF;
begin
  Result := ChildNodes['ICMSOutraUF'] as IXMLICMSOutraUF;
end;

function TXMLTImp.Get_ICMSSN: IXMLICMSSN;
begin
  Result := ChildNodes['ICMSSN'] as IXMLICMSSN;
end;

{ TXMLICMS00 }

function TXMLICMS00.Get_CST: UnicodeString;
begin
  Result := ChildNodes['CST'].Text;
end;

procedure TXMLICMS00.Set_CST(Value: UnicodeString);
begin
  ChildNodes['CST'].NodeValue := Value;
end;

function TXMLICMS00.Get_VBC: UnicodeString;
begin
  Result := ChildNodes['vBC'].Text;
end;

procedure TXMLICMS00.Set_VBC(Value: UnicodeString);
begin
  ChildNodes['vBC'].NodeValue := Value;
end;

function TXMLICMS00.Get_PICMS: UnicodeString;
begin
  Result := ChildNodes['pICMS'].Text;
end;

procedure TXMLICMS00.Set_PICMS(Value: UnicodeString);
begin
  ChildNodes['pICMS'].NodeValue := Value;
end;

function TXMLICMS00.Get_VICMS: UnicodeString;
begin
  Result := ChildNodes['vICMS'].Text;
end;

procedure TXMLICMS00.Set_VICMS(Value: UnicodeString);
begin
  ChildNodes['vICMS'].NodeValue := Value;
end;

{ TXMLICMS20 }

function TXMLICMS20.Get_CST: UnicodeString;
begin
  Result := ChildNodes['CST'].Text;
end;

procedure TXMLICMS20.Set_CST(Value: UnicodeString);
begin
  ChildNodes['CST'].NodeValue := Value;
end;

function TXMLICMS20.Get_PRedBC: UnicodeString;
begin
  Result := ChildNodes['pRedBC'].Text;
end;

procedure TXMLICMS20.Set_PRedBC(Value: UnicodeString);
begin
  ChildNodes['pRedBC'].NodeValue := Value;
end;

function TXMLICMS20.Get_VBC: UnicodeString;
begin
  Result := ChildNodes['vBC'].Text;
end;

procedure TXMLICMS20.Set_VBC(Value: UnicodeString);
begin
  ChildNodes['vBC'].NodeValue := Value;
end;

function TXMLICMS20.Get_PICMS: UnicodeString;
begin
  Result := ChildNodes['pICMS'].Text;
end;

procedure TXMLICMS20.Set_PICMS(Value: UnicodeString);
begin
  ChildNodes['pICMS'].NodeValue := Value;
end;

function TXMLICMS20.Get_VICMS: UnicodeString;
begin
  Result := ChildNodes['vICMS'].Text;
end;

procedure TXMLICMS20.Set_VICMS(Value: UnicodeString);
begin
  ChildNodes['vICMS'].NodeValue := Value;
end;

{ TXMLICMS45 }

function TXMLICMS45.Get_CST: UnicodeString;
begin
  Result := ChildNodes['CST'].Text;
end;

procedure TXMLICMS45.Set_CST(Value: UnicodeString);
begin
  ChildNodes['CST'].NodeValue := Value;
end;

{ TXMLICMS60 }

function TXMLICMS60.Get_CST: UnicodeString;
begin
  Result := ChildNodes['CST'].Text;
end;

procedure TXMLICMS60.Set_CST(Value: UnicodeString);
begin
  ChildNodes['CST'].NodeValue := Value;
end;

function TXMLICMS60.Get_VBCSTRet: UnicodeString;
begin
  Result := ChildNodes['vBCSTRet'].Text;
end;

procedure TXMLICMS60.Set_VBCSTRet(Value: UnicodeString);
begin
  ChildNodes['vBCSTRet'].NodeValue := Value;
end;

function TXMLICMS60.Get_VICMSSTRet: UnicodeString;
begin
  Result := ChildNodes['vICMSSTRet'].Text;
end;

procedure TXMLICMS60.Set_VICMSSTRet(Value: UnicodeString);
begin
  ChildNodes['vICMSSTRet'].NodeValue := Value;
end;

function TXMLICMS60.Get_PICMSSTRet: UnicodeString;
begin
  Result := ChildNodes['pICMSSTRet'].Text;
end;

procedure TXMLICMS60.Set_PICMSSTRet(Value: UnicodeString);
begin
  ChildNodes['pICMSSTRet'].NodeValue := Value;
end;

function TXMLICMS60.Get_VCred: UnicodeString;
begin
  Result := ChildNodes['vCred'].Text;
end;

procedure TXMLICMS60.Set_VCred(Value: UnicodeString);
begin
  ChildNodes['vCred'].NodeValue := Value;
end;

{ TXMLICMS90 }

function TXMLICMS90.Get_CST: UnicodeString;
begin
  Result := ChildNodes['CST'].Text;
end;

procedure TXMLICMS90.Set_CST(Value: UnicodeString);
begin
  ChildNodes['CST'].NodeValue := Value;
end;

function TXMLICMS90.Get_PRedBC: UnicodeString;
begin
  Result := ChildNodes['pRedBC'].Text;
end;

procedure TXMLICMS90.Set_PRedBC(Value: UnicodeString);
begin
  ChildNodes['pRedBC'].NodeValue := Value;
end;

function TXMLICMS90.Get_VBC: UnicodeString;
begin
  Result := ChildNodes['vBC'].Text;
end;

procedure TXMLICMS90.Set_VBC(Value: UnicodeString);
begin
  ChildNodes['vBC'].NodeValue := Value;
end;

function TXMLICMS90.Get_PICMS: UnicodeString;
begin
  Result := ChildNodes['pICMS'].Text;
end;

procedure TXMLICMS90.Set_PICMS(Value: UnicodeString);
begin
  ChildNodes['pICMS'].NodeValue := Value;
end;

function TXMLICMS90.Get_VICMS: UnicodeString;
begin
  Result := ChildNodes['vICMS'].Text;
end;

procedure TXMLICMS90.Set_VICMS(Value: UnicodeString);
begin
  ChildNodes['vICMS'].NodeValue := Value;
end;

function TXMLICMS90.Get_VCred: UnicodeString;
begin
  Result := ChildNodes['vCred'].Text;
end;

procedure TXMLICMS90.Set_VCred(Value: UnicodeString);
begin
  ChildNodes['vCred'].NodeValue := Value;
end;

{ TXMLICMSOutraUF }

function TXMLICMSOutraUF.Get_CST: UnicodeString;
begin
  Result := ChildNodes['CST'].Text;
end;

procedure TXMLICMSOutraUF.Set_CST(Value: UnicodeString);
begin
  ChildNodes['CST'].NodeValue := Value;
end;

function TXMLICMSOutraUF.Get_PRedBCOutraUF: UnicodeString;
begin
  Result := ChildNodes['pRedBCOutraUF'].Text;
end;

procedure TXMLICMSOutraUF.Set_PRedBCOutraUF(Value: UnicodeString);
begin
  ChildNodes['pRedBCOutraUF'].NodeValue := Value;
end;

function TXMLICMSOutraUF.Get_VBCOutraUF: UnicodeString;
begin
  Result := ChildNodes['vBCOutraUF'].Text;
end;

procedure TXMLICMSOutraUF.Set_VBCOutraUF(Value: UnicodeString);
begin
  ChildNodes['vBCOutraUF'].NodeValue := Value;
end;

function TXMLICMSOutraUF.Get_PICMSOutraUF: UnicodeString;
begin
  Result := ChildNodes['pICMSOutraUF'].Text;
end;

procedure TXMLICMSOutraUF.Set_PICMSOutraUF(Value: UnicodeString);
begin
  ChildNodes['pICMSOutraUF'].NodeValue := Value;
end;

function TXMLICMSOutraUF.Get_VICMSOutraUF: UnicodeString;
begin
  Result := ChildNodes['vICMSOutraUF'].Text;
end;

procedure TXMLICMSOutraUF.Set_VICMSOutraUF(Value: UnicodeString);
begin
  ChildNodes['vICMSOutraUF'].NodeValue := Value;
end;

{ TXMLICMSSN }

function TXMLICMSSN.Get_IndSN: UnicodeString;
begin
  Result := ChildNodes['indSN'].Text;
end;

procedure TXMLICMSSN.Set_IndSN(Value: UnicodeString);
begin
  ChildNodes['indSN'].NodeValue := Value;
end;

{ TXMLICMSUFFim }

function TXMLICMSUFFim.Get_VBCUFFim: UnicodeString;
begin
  Result := ChildNodes['vBCUFFim'].Text;
end;

procedure TXMLICMSUFFim.Set_VBCUFFim(Value: UnicodeString);
begin
  ChildNodes['vBCUFFim'].NodeValue := Value;
end;

function TXMLICMSUFFim.Get_PICMSUFFim: UnicodeString;
begin
  Result := ChildNodes['pICMSUFFim'].Text;
end;

procedure TXMLICMSUFFim.Set_PICMSUFFim(Value: UnicodeString);
begin
  ChildNodes['pICMSUFFim'].NodeValue := Value;
end;

function TXMLICMSUFFim.Get_PICMSInter: UnicodeString;
begin
  Result := ChildNodes['pICMSInter'].Text;
end;

procedure TXMLICMSUFFim.Set_PICMSInter(Value: UnicodeString);
begin
  ChildNodes['pICMSInter'].NodeValue := Value;
end;

function TXMLICMSUFFim.Get_PICMSInterPart: UnicodeString;
begin
  Result := ChildNodes['pICMSInterPart'].Text;
end;

procedure TXMLICMSUFFim.Set_PICMSInterPart(Value: UnicodeString);
begin
  ChildNodes['pICMSInterPart'].NodeValue := Value;
end;

function TXMLICMSUFFim.Get_VICMSUFFim: UnicodeString;
begin
  Result := ChildNodes['vICMSUFFim'].Text;
end;

procedure TXMLICMSUFFim.Set_VICMSUFFim(Value: UnicodeString);
begin
  ChildNodes['vICMSUFFim'].NodeValue := Value;
end;

function TXMLICMSUFFim.Get_VICMSUFIni: UnicodeString;
begin
  Result := ChildNodes['vICMSUFIni'].Text;
end;

procedure TXMLICMSUFFim.Set_VICMSUFIni(Value: UnicodeString);
begin
  ChildNodes['vICMSUFIni'].NodeValue := Value;
end;

{ TXMLInfCTeNorm }

procedure TXMLInfCTeNorm.AfterConstruction;
begin
  RegisterChildNode('infCarga', TXMLInfCarga);
  RegisterChildNode('infDoc', TXMLInfDoc);
  RegisterChildNode('docAnt', TXMLDocAnt);
  RegisterChildNode('seg', TXMLSeg);
  RegisterChildNode('infModal', TXMLInfModal);
  RegisterChildNode('peri', TXMLPeri);
  RegisterChildNode('veicNovos', TXMLVeicNovos);
  RegisterChildNode('cobr', TXMLCobr);
  RegisterChildNode('infCteSub', TXMLInfCteSub);
  FSeg := CreateCollection(TXMLSegList, IXMLSeg, 'seg') as IXMLSegList;
  FPeri := CreateCollection(TXMLPeriList, IXMLPeri, 'peri') as IXMLPeriList;
  FVeicNovos := CreateCollection(TXMLVeicNovosList, IXMLVeicNovos, 'veicNovos') as IXMLVeicNovosList;
  inherited;
end;

function TXMLInfCTeNorm.Get_InfCarga: IXMLInfCarga;
begin
  Result := ChildNodes['infCarga'] as IXMLInfCarga;
end;

function TXMLInfCTeNorm.Get_InfDoc: IXMLInfDoc;
begin
  Result := ChildNodes['infDoc'] as IXMLInfDoc;
end;

function TXMLInfCTeNorm.Get_DocAnt: IXMLDocAnt;
begin
  Result := ChildNodes['docAnt'] as IXMLDocAnt;
end;

function TXMLInfCTeNorm.Get_Seg: IXMLSegList;
begin
  Result := FSeg;
end;

function TXMLInfCTeNorm.Get_InfModal: IXMLInfModal;
begin
  Result := ChildNodes['infModal'] as IXMLInfModal;
end;

function TXMLInfCTeNorm.Get_Peri: IXMLPeriList;
begin
  Result := FPeri;
end;

function TXMLInfCTeNorm.Get_VeicNovos: IXMLVeicNovosList;
begin
  Result := FVeicNovos;
end;

function TXMLInfCTeNorm.Get_Cobr: IXMLCobr;
begin
  Result := ChildNodes['cobr'] as IXMLCobr;
end;

function TXMLInfCTeNorm.Get_InfCteSub: IXMLInfCteSub;
begin
  Result := ChildNodes['infCteSub'] as IXMLInfCteSub;
end;

{ TXMLInfCarga }

procedure TXMLInfCarga.AfterConstruction;
begin
  RegisterChildNode('infQ', TXMLInfQ);
  FInfQ := CreateCollection(TXMLInfQList, IXMLInfQ, 'infQ') as IXMLInfQList;
  inherited;
end;

function TXMLInfCarga.Get_VCarga: UnicodeString;
begin
  Result := ChildNodes['vCarga'].Text;
end;

procedure TXMLInfCarga.Set_VCarga(Value: UnicodeString);
begin
  ChildNodes['vCarga'].NodeValue := Value;
end;

function TXMLInfCarga.Get_ProPred: UnicodeString;
begin
  Result := ChildNodes['proPred'].Text;
end;

procedure TXMLInfCarga.Set_ProPred(Value: UnicodeString);
begin
  ChildNodes['proPred'].NodeValue := Value;
end;

function TXMLInfCarga.Get_XOutCat: UnicodeString;
begin
  Result := ChildNodes['xOutCat'].Text;
end;

procedure TXMLInfCarga.Set_XOutCat(Value: UnicodeString);
begin
  ChildNodes['xOutCat'].NodeValue := Value;
end;

function TXMLInfCarga.Get_InfQ: IXMLInfQList;
begin
  Result := FInfQ;
end;

{ TXMLInfQ }

function TXMLInfQ.Get_CUnid: UnicodeString;
begin
  Result := ChildNodes['cUnid'].Text;
end;

procedure TXMLInfQ.Set_CUnid(Value: UnicodeString);
begin
  ChildNodes['cUnid'].NodeValue := Value;
end;

function TXMLInfQ.Get_TpMed: UnicodeString;
begin
  Result := ChildNodes['tpMed'].Text;
end;

procedure TXMLInfQ.Set_TpMed(Value: UnicodeString);
begin
  ChildNodes['tpMed'].NodeValue := Value;
end;

function TXMLInfQ.Get_QCarga: UnicodeString;
begin
  Result := ChildNodes['qCarga'].Text;
end;

procedure TXMLInfQ.Set_QCarga(Value: UnicodeString);
begin
  ChildNodes['qCarga'].NodeValue := Value;
end;

{ TXMLInfQList }

function TXMLInfQList.Add: IXMLInfQ;
begin
  Result := AddItem(-1) as IXMLInfQ;
end;

function TXMLInfQList.Insert(const Index: Integer): IXMLInfQ;
begin
  Result := AddItem(Index) as IXMLInfQ;
end;

function TXMLInfQList.Get_Item(Index: Integer): IXMLInfQ;
begin
  Result := List[Index] as IXMLInfQ;
end;

{ TXMLInfDoc }

procedure TXMLInfDoc.AfterConstruction;
begin
  RegisterChildNode('infNF', TXMLInfNF);
  RegisterChildNode('infNFe', TXMLInfNFe);
  RegisterChildNode('infOutros', TXMLInfOutros);
  FInfNF := CreateCollection(TXMLInfNFList, IXMLInfNF, 'infNF') as IXMLInfNFList;
  FInfNFe := CreateCollection(TXMLInfNFeList, IXMLInfNFe, 'infNFe') as IXMLInfNFeList;
  FInfOutros := CreateCollection(TXMLInfOutrosList, IXMLInfOutros, 'infOutros') as IXMLInfOutrosList;
  inherited;
end;

function TXMLInfDoc.Get_InfNF: IXMLInfNFList;
begin
  Result := FInfNF;
end;

function TXMLInfDoc.Get_InfNFe: IXMLInfNFeList;
begin
  Result := FInfNFe;
end;

function TXMLInfDoc.Get_InfOutros: IXMLInfOutrosList;
begin
  Result := FInfOutros;
end;

{ TXMLInfNF }

procedure TXMLInfNF.AfterConstruction;
begin
  RegisterChildNode('infUnidTransp', TXMLTUnidadeTransp);
  RegisterChildNode('infUnidCarga', TXMLTUnidCarga);
  FInfUnidTransp := CreateCollection(TXMLTUnidadeTranspList, IXMLTUnidadeTransp, 'infUnidTransp') as IXMLTUnidadeTranspList;
  FInfUnidCarga := CreateCollection(TXMLTUnidCargaList, IXMLTUnidCarga, 'infUnidCarga') as IXMLTUnidCargaList;
  inherited;
end;

function TXMLInfNF.Get_NRoma: UnicodeString;
begin
  Result := ChildNodes['nRoma'].Text;
end;

procedure TXMLInfNF.Set_NRoma(Value: UnicodeString);
begin
  ChildNodes['nRoma'].NodeValue := Value;
end;

function TXMLInfNF.Get_NPed: UnicodeString;
begin
  Result := ChildNodes['nPed'].Text;
end;

procedure TXMLInfNF.Set_NPed(Value: UnicodeString);
begin
  ChildNodes['nPed'].NodeValue := Value;
end;

function TXMLInfNF.Get_Mod_: UnicodeString;
begin
  Result := ChildNodes['mod'].Text;
end;

procedure TXMLInfNF.Set_Mod_(Value: UnicodeString);
begin
  ChildNodes['mod'].NodeValue := Value;
end;

function TXMLInfNF.Get_Serie: UnicodeString;
begin
  Result := ChildNodes['serie'].Text;
end;

procedure TXMLInfNF.Set_Serie(Value: UnicodeString);
begin
  ChildNodes['serie'].NodeValue := Value;
end;

function TXMLInfNF.Get_NDoc: UnicodeString;
begin
  Result := ChildNodes['nDoc'].Text;
end;

procedure TXMLInfNF.Set_NDoc(Value: UnicodeString);
begin
  ChildNodes['nDoc'].NodeValue := Value;
end;

function TXMLInfNF.Get_DEmi: UnicodeString;
begin
  Result := ChildNodes['dEmi'].Text;
end;

procedure TXMLInfNF.Set_DEmi(Value: UnicodeString);
begin
  ChildNodes['dEmi'].NodeValue := Value;
end;

function TXMLInfNF.Get_VBC: UnicodeString;
begin
  Result := ChildNodes['vBC'].Text;
end;

procedure TXMLInfNF.Set_VBC(Value: UnicodeString);
begin
  ChildNodes['vBC'].NodeValue := Value;
end;

function TXMLInfNF.Get_VICMS: UnicodeString;
begin
  Result := ChildNodes['vICMS'].Text;
end;

procedure TXMLInfNF.Set_VICMS(Value: UnicodeString);
begin
  ChildNodes['vICMS'].NodeValue := Value;
end;

function TXMLInfNF.Get_VBCST: UnicodeString;
begin
  Result := ChildNodes['vBCST'].Text;
end;

procedure TXMLInfNF.Set_VBCST(Value: UnicodeString);
begin
  ChildNodes['vBCST'].NodeValue := Value;
end;

function TXMLInfNF.Get_VST: UnicodeString;
begin
  Result := ChildNodes['vST'].Text;
end;

procedure TXMLInfNF.Set_VST(Value: UnicodeString);
begin
  ChildNodes['vST'].NodeValue := Value;
end;

function TXMLInfNF.Get_VProd: UnicodeString;
begin
  Result := ChildNodes['vProd'].Text;
end;

procedure TXMLInfNF.Set_VProd(Value: UnicodeString);
begin
  ChildNodes['vProd'].NodeValue := Value;
end;

function TXMLInfNF.Get_VNF: UnicodeString;
begin
  Result := ChildNodes['vNF'].Text;
end;

procedure TXMLInfNF.Set_VNF(Value: UnicodeString);
begin
  ChildNodes['vNF'].NodeValue := Value;
end;

function TXMLInfNF.Get_NCFOP: UnicodeString;
begin
  Result := ChildNodes['nCFOP'].Text;
end;

procedure TXMLInfNF.Set_NCFOP(Value: UnicodeString);
begin
  ChildNodes['nCFOP'].NodeValue := Value;
end;

function TXMLInfNF.Get_NPeso: UnicodeString;
begin
  Result := ChildNodes['nPeso'].Text;
end;

procedure TXMLInfNF.Set_NPeso(Value: UnicodeString);
begin
  ChildNodes['nPeso'].NodeValue := Value;
end;

function TXMLInfNF.Get_PIN: UnicodeString;
begin
  Result := ChildNodes['PIN'].Text;
end;

procedure TXMLInfNF.Set_PIN(Value: UnicodeString);
begin
  ChildNodes['PIN'].NodeValue := Value;
end;

function TXMLInfNF.Get_DPrev: UnicodeString;
begin
  Result := ChildNodes['dPrev'].Text;
end;

procedure TXMLInfNF.Set_DPrev(Value: UnicodeString);
begin
  ChildNodes['dPrev'].NodeValue := Value;
end;

function TXMLInfNF.Get_InfUnidTransp: IXMLTUnidadeTranspList;
begin
  Result := FInfUnidTransp;
end;

function TXMLInfNF.Get_InfUnidCarga: IXMLTUnidCargaList;
begin
  Result := FInfUnidCarga;
end;

{ TXMLInfNFList }

function TXMLInfNFList.Add: IXMLInfNF;
begin
  Result := AddItem(-1) as IXMLInfNF;
end;

function TXMLInfNFList.Insert(const Index: Integer): IXMLInfNF;
begin
  Result := AddItem(Index) as IXMLInfNF;
end;

function TXMLInfNFList.Get_Item(Index: Integer): IXMLInfNF;
begin
  Result := List[Index] as IXMLInfNF;
end;

{ TXMLTUnidadeTransp }

procedure TXMLTUnidadeTransp.AfterConstruction;
begin
  RegisterChildNode('lacUnidTransp', TXMLLacUnidTransp);
  RegisterChildNode('infUnidCarga', TXMLTUnidCarga);
  FLacUnidTransp := CreateCollection(TXMLLacUnidTranspList, IXMLLacUnidTransp, 'lacUnidTransp') as IXMLLacUnidTranspList;
  FInfUnidCarga := CreateCollection(TXMLTUnidCargaList, IXMLTUnidCarga, 'infUnidCarga') as IXMLTUnidCargaList;
  inherited;
end;

function TXMLTUnidadeTransp.Get_TpUnidTransp: UnicodeString;
begin
  Result := ChildNodes['tpUnidTransp'].Text;
end;

procedure TXMLTUnidadeTransp.Set_TpUnidTransp(Value: UnicodeString);
begin
  ChildNodes['tpUnidTransp'].NodeValue := Value;
end;

function TXMLTUnidadeTransp.Get_IdUnidTransp: UnicodeString;
begin
  Result := ChildNodes['idUnidTransp'].Text;
end;

procedure TXMLTUnidadeTransp.Set_IdUnidTransp(Value: UnicodeString);
begin
  ChildNodes['idUnidTransp'].NodeValue := Value;
end;

function TXMLTUnidadeTransp.Get_LacUnidTransp: IXMLLacUnidTranspList;
begin
  Result := FLacUnidTransp;
end;

function TXMLTUnidadeTransp.Get_InfUnidCarga: IXMLTUnidCargaList;
begin
  Result := FInfUnidCarga;
end;

function TXMLTUnidadeTransp.Get_QtdRat: UnicodeString;
begin
  Result := ChildNodes['qtdRat'].Text;
end;

procedure TXMLTUnidadeTransp.Set_QtdRat(Value: UnicodeString);
begin
  ChildNodes['qtdRat'].NodeValue := Value;
end;

{ TXMLTUnidadeTranspList }

function TXMLTUnidadeTranspList.Add: IXMLTUnidadeTransp;
begin
  Result := AddItem(-1) as IXMLTUnidadeTransp;
end;

function TXMLTUnidadeTranspList.Insert(const Index: Integer): IXMLTUnidadeTransp;
begin
  Result := AddItem(Index) as IXMLTUnidadeTransp;
end;

function TXMLTUnidadeTranspList.Get_Item(Index: Integer): IXMLTUnidadeTransp;
begin
  Result := List[Index] as IXMLTUnidadeTransp;
end;

{ TXMLLacUnidTransp }

function TXMLLacUnidTransp.Get_NLacre: UnicodeString;
begin
  Result := ChildNodes['nLacre'].Text;
end;

procedure TXMLLacUnidTransp.Set_NLacre(Value: UnicodeString);
begin
  ChildNodes['nLacre'].NodeValue := Value;
end;

{ TXMLLacUnidTranspList }

function TXMLLacUnidTranspList.Add: IXMLLacUnidTransp;
begin
  Result := AddItem(-1) as IXMLLacUnidTransp;
end;

function TXMLLacUnidTranspList.Insert(const Index: Integer): IXMLLacUnidTransp;
begin
  Result := AddItem(Index) as IXMLLacUnidTransp;
end;

function TXMLLacUnidTranspList.Get_Item(Index: Integer): IXMLLacUnidTransp;
begin
  Result := List[Index] as IXMLLacUnidTransp;
end;

{ TXMLTUnidCarga }

procedure TXMLTUnidCarga.AfterConstruction;
begin
  RegisterChildNode('lacUnidCarga', TXMLLacUnidCarga);
  FLacUnidCarga := CreateCollection(TXMLLacUnidCargaList, IXMLLacUnidCarga, 'lacUnidCarga') as IXMLLacUnidCargaList;
  inherited;
end;

function TXMLTUnidCarga.Get_TpUnidCarga: UnicodeString;
begin
  Result := ChildNodes['tpUnidCarga'].Text;
end;

procedure TXMLTUnidCarga.Set_TpUnidCarga(Value: UnicodeString);
begin
  ChildNodes['tpUnidCarga'].NodeValue := Value;
end;

function TXMLTUnidCarga.Get_IdUnidCarga: UnicodeString;
begin
  Result := ChildNodes['idUnidCarga'].Text;
end;

procedure TXMLTUnidCarga.Set_IdUnidCarga(Value: UnicodeString);
begin
  ChildNodes['idUnidCarga'].NodeValue := Value;
end;

function TXMLTUnidCarga.Get_LacUnidCarga: IXMLLacUnidCargaList;
begin
  Result := FLacUnidCarga;
end;

function TXMLTUnidCarga.Get_QtdRat: UnicodeString;
begin
  Result := ChildNodes['qtdRat'].Text;
end;

procedure TXMLTUnidCarga.Set_QtdRat(Value: UnicodeString);
begin
  ChildNodes['qtdRat'].NodeValue := Value;
end;

{ TXMLTUnidCargaList }

function TXMLTUnidCargaList.Add: IXMLTUnidCarga;
begin
  Result := AddItem(-1) as IXMLTUnidCarga;
end;

function TXMLTUnidCargaList.Insert(const Index: Integer): IXMLTUnidCarga;
begin
  Result := AddItem(Index) as IXMLTUnidCarga;
end;

function TXMLTUnidCargaList.Get_Item(Index: Integer): IXMLTUnidCarga;
begin
  Result := List[Index] as IXMLTUnidCarga;
end;

{ TXMLLacUnidCarga }

function TXMLLacUnidCarga.Get_NLacre: UnicodeString;
begin
  Result := ChildNodes['nLacre'].Text;
end;

procedure TXMLLacUnidCarga.Set_NLacre(Value: UnicodeString);
begin
  ChildNodes['nLacre'].NodeValue := Value;
end;

{ TXMLLacUnidCargaList }

function TXMLLacUnidCargaList.Add: IXMLLacUnidCarga;
begin
  Result := AddItem(-1) as IXMLLacUnidCarga;
end;

function TXMLLacUnidCargaList.Insert(const Index: Integer): IXMLLacUnidCarga;
begin
  Result := AddItem(Index) as IXMLLacUnidCarga;
end;

function TXMLLacUnidCargaList.Get_Item(Index: Integer): IXMLLacUnidCarga;
begin
  Result := List[Index] as IXMLLacUnidCarga;
end;

{ TXMLInfNFe }

procedure TXMLInfNFe.AfterConstruction;
begin
  RegisterChildNode('infUnidTransp', TXMLTUnidadeTransp);
  RegisterChildNode('infUnidCarga', TXMLTUnidCarga);
  FInfUnidTransp := CreateCollection(TXMLTUnidadeTranspList, IXMLTUnidadeTransp, 'infUnidTransp') as IXMLTUnidadeTranspList;
  FInfUnidCarga := CreateCollection(TXMLTUnidCargaList, IXMLTUnidCarga, 'infUnidCarga') as IXMLTUnidCargaList;
  inherited;
end;

function TXMLInfNFe.Get_Chave: UnicodeString;
begin
  Result := ChildNodes['chave'].Text;
end;

procedure TXMLInfNFe.Set_Chave(Value: UnicodeString);
begin
  ChildNodes['chave'].NodeValue := Value;
end;

function TXMLInfNFe.Get_PIN: UnicodeString;
begin
  Result := ChildNodes['PIN'].Text;
end;

procedure TXMLInfNFe.Set_PIN(Value: UnicodeString);
begin
  ChildNodes['PIN'].NodeValue := Value;
end;

function TXMLInfNFe.Get_DPrev: UnicodeString;
begin
  Result := ChildNodes['dPrev'].Text;
end;

procedure TXMLInfNFe.Set_DPrev(Value: UnicodeString);
begin
  ChildNodes['dPrev'].NodeValue := Value;
end;

function TXMLInfNFe.Get_InfUnidTransp: IXMLTUnidadeTranspList;
begin
  Result := FInfUnidTransp;
end;

function TXMLInfNFe.Get_InfUnidCarga: IXMLTUnidCargaList;
begin
  Result := FInfUnidCarga;
end;

{ TXMLInfNFeList }

function TXMLInfNFeList.Add: IXMLInfNFe;
begin
  Result := AddItem(-1) as IXMLInfNFe;
end;

function TXMLInfNFeList.Insert(const Index: Integer): IXMLInfNFe;
begin
  Result := AddItem(Index) as IXMLInfNFe;
end;

function TXMLInfNFeList.Get_Item(Index: Integer): IXMLInfNFe;
begin
  Result := List[Index] as IXMLInfNFe;
end;

{ TXMLInfOutros }

procedure TXMLInfOutros.AfterConstruction;
begin
  RegisterChildNode('infUnidTransp', TXMLTUnidadeTransp);
  RegisterChildNode('infUnidCarga', TXMLTUnidCarga);
  FInfUnidTransp := CreateCollection(TXMLTUnidadeTranspList, IXMLTUnidadeTransp, 'infUnidTransp') as IXMLTUnidadeTranspList;
  FInfUnidCarga := CreateCollection(TXMLTUnidCargaList, IXMLTUnidCarga, 'infUnidCarga') as IXMLTUnidCargaList;
  inherited;
end;

function TXMLInfOutros.Get_TpDoc: UnicodeString;
begin
  Result := ChildNodes['tpDoc'].Text;
end;

procedure TXMLInfOutros.Set_TpDoc(Value: UnicodeString);
begin
  ChildNodes['tpDoc'].NodeValue := Value;
end;

function TXMLInfOutros.Get_DescOutros: UnicodeString;
begin
  Result := ChildNodes['descOutros'].Text;
end;

procedure TXMLInfOutros.Set_DescOutros(Value: UnicodeString);
begin
  ChildNodes['descOutros'].NodeValue := Value;
end;

function TXMLInfOutros.Get_NDoc: UnicodeString;
begin
  Result := ChildNodes['nDoc'].Text;
end;

procedure TXMLInfOutros.Set_NDoc(Value: UnicodeString);
begin
  ChildNodes['nDoc'].NodeValue := Value;
end;

function TXMLInfOutros.Get_DEmi: UnicodeString;
begin
  Result := ChildNodes['dEmi'].Text;
end;

procedure TXMLInfOutros.Set_DEmi(Value: UnicodeString);
begin
  ChildNodes['dEmi'].NodeValue := Value;
end;

function TXMLInfOutros.Get_VDocFisc: UnicodeString;
begin
  Result := ChildNodes['vDocFisc'].Text;
end;

procedure TXMLInfOutros.Set_VDocFisc(Value: UnicodeString);
begin
  ChildNodes['vDocFisc'].NodeValue := Value;
end;

function TXMLInfOutros.Get_DPrev: UnicodeString;
begin
  Result := ChildNodes['dPrev'].Text;
end;

procedure TXMLInfOutros.Set_DPrev(Value: UnicodeString);
begin
  ChildNodes['dPrev'].NodeValue := Value;
end;

function TXMLInfOutros.Get_InfUnidTransp: IXMLTUnidadeTranspList;
begin
  Result := FInfUnidTransp;
end;

function TXMLInfOutros.Get_InfUnidCarga: IXMLTUnidCargaList;
begin
  Result := FInfUnidCarga;
end;

{ TXMLInfOutrosList }

function TXMLInfOutrosList.Add: IXMLInfOutros;
begin
  Result := AddItem(-1) as IXMLInfOutros;
end;

function TXMLInfOutrosList.Insert(const Index: Integer): IXMLInfOutros;
begin
  Result := AddItem(Index) as IXMLInfOutros;
end;

function TXMLInfOutrosList.Get_Item(Index: Integer): IXMLInfOutros;
begin
  Result := List[Index] as IXMLInfOutros;
end;

{ TXMLDocAnt }

procedure TXMLDocAnt.AfterConstruction;
begin
  RegisterChildNode('emiDocAnt', TXMLEmiDocAnt);
  ItemTag := 'emiDocAnt';
  ItemInterface := IXMLEmiDocAnt;
  inherited;
end;

function TXMLDocAnt.Get_EmiDocAnt(Index: Integer): IXMLEmiDocAnt;
begin
  Result := List[Index] as IXMLEmiDocAnt;
end;

function TXMLDocAnt.Add: IXMLEmiDocAnt;
begin
  Result := AddItem(-1) as IXMLEmiDocAnt;
end;

function TXMLDocAnt.Insert(const Index: Integer): IXMLEmiDocAnt;
begin
  Result := AddItem(Index) as IXMLEmiDocAnt;
end;

{ TXMLEmiDocAnt }

procedure TXMLEmiDocAnt.AfterConstruction;
begin
  RegisterChildNode('idDocAnt', TXMLIdDocAnt);
  FIdDocAnt := CreateCollection(TXMLIdDocAntList, IXMLIdDocAnt, 'idDocAnt') as IXMLIdDocAntList;
  inherited;
end;

function TXMLEmiDocAnt.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLEmiDocAnt.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLEmiDocAnt.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLEmiDocAnt.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLEmiDocAnt.Get_IE: UnicodeString;
begin
  Result := ChildNodes['IE'].Text;
end;

procedure TXMLEmiDocAnt.Set_IE(Value: UnicodeString);
begin
  ChildNodes['IE'].NodeValue := Value;
end;

function TXMLEmiDocAnt.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLEmiDocAnt.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

function TXMLEmiDocAnt.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLEmiDocAnt.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLEmiDocAnt.Get_IdDocAnt: IXMLIdDocAntList;
begin
  Result := FIdDocAnt;
end;

{ TXMLIdDocAnt }

procedure TXMLIdDocAnt.AfterConstruction;
begin
  RegisterChildNode('idDocAntPap', TXMLIdDocAntPap);
  RegisterChildNode('idDocAntEle', TXMLIdDocAntEle);
  FIdDocAntPap := CreateCollection(TXMLIdDocAntPapList, IXMLIdDocAntPap, 'idDocAntPap') as IXMLIdDocAntPapList;
  FIdDocAntEle := CreateCollection(TXMLIdDocAntEleList, IXMLIdDocAntEle, 'idDocAntEle') as IXMLIdDocAntEleList;
  inherited;
end;

function TXMLIdDocAnt.Get_IdDocAntPap: IXMLIdDocAntPapList;
begin
  Result := FIdDocAntPap;
end;

function TXMLIdDocAnt.Get_IdDocAntEle: IXMLIdDocAntEleList;
begin
  Result := FIdDocAntEle;
end;

{ TXMLIdDocAntList }

function TXMLIdDocAntList.Add: IXMLIdDocAnt;
begin
  Result := AddItem(-1) as IXMLIdDocAnt;
end;

function TXMLIdDocAntList.Insert(const Index: Integer): IXMLIdDocAnt;
begin
  Result := AddItem(Index) as IXMLIdDocAnt;
end;

function TXMLIdDocAntList.Get_Item(Index: Integer): IXMLIdDocAnt;
begin
  Result := List[Index] as IXMLIdDocAnt;
end;

{ TXMLIdDocAntPap }

function TXMLIdDocAntPap.Get_TpDoc: UnicodeString;
begin
  Result := ChildNodes['tpDoc'].Text;
end;

procedure TXMLIdDocAntPap.Set_TpDoc(Value: UnicodeString);
begin
  ChildNodes['tpDoc'].NodeValue := Value;
end;

function TXMLIdDocAntPap.Get_Serie: UnicodeString;
begin
  Result := ChildNodes['serie'].Text;
end;

procedure TXMLIdDocAntPap.Set_Serie(Value: UnicodeString);
begin
  ChildNodes['serie'].NodeValue := Value;
end;

function TXMLIdDocAntPap.Get_Subser: UnicodeString;
begin
  Result := ChildNodes['subser'].Text;
end;

procedure TXMLIdDocAntPap.Set_Subser(Value: UnicodeString);
begin
  ChildNodes['subser'].NodeValue := Value;
end;

function TXMLIdDocAntPap.Get_NDoc: UnicodeString;
begin
  Result := ChildNodes['nDoc'].Text;
end;

procedure TXMLIdDocAntPap.Set_NDoc(Value: UnicodeString);
begin
  ChildNodes['nDoc'].NodeValue := Value;
end;

function TXMLIdDocAntPap.Get_DEmi: UnicodeString;
begin
  Result := ChildNodes['dEmi'].Text;
end;

procedure TXMLIdDocAntPap.Set_DEmi(Value: UnicodeString);
begin
  ChildNodes['dEmi'].NodeValue := Value;
end;

{ TXMLIdDocAntPapList }

function TXMLIdDocAntPapList.Add: IXMLIdDocAntPap;
begin
  Result := AddItem(-1) as IXMLIdDocAntPap;
end;

function TXMLIdDocAntPapList.Insert(const Index: Integer): IXMLIdDocAntPap;
begin
  Result := AddItem(Index) as IXMLIdDocAntPap;
end;

function TXMLIdDocAntPapList.Get_Item(Index: Integer): IXMLIdDocAntPap;
begin
  Result := List[Index] as IXMLIdDocAntPap;
end;

{ TXMLIdDocAntEle }

function TXMLIdDocAntEle.Get_Chave: UnicodeString;
begin
  Result := ChildNodes['chave'].Text;
end;

procedure TXMLIdDocAntEle.Set_Chave(Value: UnicodeString);
begin
  ChildNodes['chave'].NodeValue := Value;
end;

{ TXMLIdDocAntEleList }

function TXMLIdDocAntEleList.Add: IXMLIdDocAntEle;
begin
  Result := AddItem(-1) as IXMLIdDocAntEle;
end;

function TXMLIdDocAntEleList.Insert(const Index: Integer): IXMLIdDocAntEle;
begin
  Result := AddItem(Index) as IXMLIdDocAntEle;
end;

function TXMLIdDocAntEleList.Get_Item(Index: Integer): IXMLIdDocAntEle;
begin
  Result := List[Index] as IXMLIdDocAntEle;
end;

{ TXMLSeg }

function TXMLSeg.Get_RespSeg: UnicodeString;
begin
  Result := ChildNodes['respSeg'].Text;
end;

procedure TXMLSeg.Set_RespSeg(Value: UnicodeString);
begin
  ChildNodes['respSeg'].NodeValue := Value;
end;

function TXMLSeg.Get_XSeg: UnicodeString;
begin
  Result := ChildNodes['xSeg'].Text;
end;

procedure TXMLSeg.Set_XSeg(Value: UnicodeString);
begin
  ChildNodes['xSeg'].NodeValue := Value;
end;

function TXMLSeg.Get_NApol: UnicodeString;
begin
  Result := ChildNodes['nApol'].Text;
end;

procedure TXMLSeg.Set_NApol(Value: UnicodeString);
begin
  ChildNodes['nApol'].NodeValue := Value;
end;

function TXMLSeg.Get_NAver: UnicodeString;
begin
  Result := ChildNodes['nAver'].Text;
end;

procedure TXMLSeg.Set_NAver(Value: UnicodeString);
begin
  ChildNodes['nAver'].NodeValue := Value;
end;

function TXMLSeg.Get_VCarga: UnicodeString;
begin
  Result := ChildNodes['vCarga'].Text;
end;

procedure TXMLSeg.Set_VCarga(Value: UnicodeString);
begin
  ChildNodes['vCarga'].NodeValue := Value;
end;

{ TXMLSegList }

function TXMLSegList.Add: IXMLSeg;
begin
  Result := AddItem(-1) as IXMLSeg;
end;

function TXMLSegList.Insert(const Index: Integer): IXMLSeg;
begin
  Result := AddItem(Index) as IXMLSeg;
end;

function TXMLSegList.Get_Item(Index: Integer): IXMLSeg;
begin
  Result := List[Index] as IXMLSeg;
end;

{ TXMLInfModal }

function TXMLInfModal.Get_VersaoModal: UnicodeString;
begin
  Result := AttributeNodes['versaoModal'].Text;
end;

procedure TXMLInfModal.Set_VersaoModal(Value: UnicodeString);
begin
  SetAttribute('versaoModal', Value);
end;

{ TXMLPeri }

function TXMLPeri.Get_NONU: UnicodeString;
begin
  Result := ChildNodes['nONU'].Text;
end;

procedure TXMLPeri.Set_NONU(Value: UnicodeString);
begin
  ChildNodes['nONU'].NodeValue := Value;
end;

function TXMLPeri.Get_XNomeAE: UnicodeString;
begin
  Result := ChildNodes['xNomeAE'].Text;
end;

procedure TXMLPeri.Set_XNomeAE(Value: UnicodeString);
begin
  ChildNodes['xNomeAE'].NodeValue := Value;
end;

function TXMLPeri.Get_XClaRisco: UnicodeString;
begin
  Result := ChildNodes['xClaRisco'].Text;
end;

procedure TXMLPeri.Set_XClaRisco(Value: UnicodeString);
begin
  ChildNodes['xClaRisco'].NodeValue := Value;
end;

function TXMLPeri.Get_GrEmb: UnicodeString;
begin
  Result := ChildNodes['grEmb'].Text;
end;

procedure TXMLPeri.Set_GrEmb(Value: UnicodeString);
begin
  ChildNodes['grEmb'].NodeValue := Value;
end;

function TXMLPeri.Get_QTotProd: UnicodeString;
begin
  Result := ChildNodes['qTotProd'].Text;
end;

procedure TXMLPeri.Set_QTotProd(Value: UnicodeString);
begin
  ChildNodes['qTotProd'].NodeValue := Value;
end;

function TXMLPeri.Get_QVolTipo: UnicodeString;
begin
  Result := ChildNodes['qVolTipo'].Text;
end;

procedure TXMLPeri.Set_QVolTipo(Value: UnicodeString);
begin
  ChildNodes['qVolTipo'].NodeValue := Value;
end;

function TXMLPeri.Get_PontoFulgor: UnicodeString;
begin
  Result := ChildNodes['pontoFulgor'].Text;
end;

procedure TXMLPeri.Set_PontoFulgor(Value: UnicodeString);
begin
  ChildNodes['pontoFulgor'].NodeValue := Value;
end;

{ TXMLPeriList }

function TXMLPeriList.Add: IXMLPeri;
begin
  Result := AddItem(-1) as IXMLPeri;
end;

function TXMLPeriList.Insert(const Index: Integer): IXMLPeri;
begin
  Result := AddItem(Index) as IXMLPeri;
end;

function TXMLPeriList.Get_Item(Index: Integer): IXMLPeri;
begin
  Result := List[Index] as IXMLPeri;
end;

{ TXMLVeicNovos }

function TXMLVeicNovos.Get_Chassi: UnicodeString;
begin
  Result := ChildNodes['chassi'].Text;
end;

procedure TXMLVeicNovos.Set_Chassi(Value: UnicodeString);
begin
  ChildNodes['chassi'].NodeValue := Value;
end;

function TXMLVeicNovos.Get_CCor: UnicodeString;
begin
  Result := ChildNodes['cCor'].Text;
end;

procedure TXMLVeicNovos.Set_CCor(Value: UnicodeString);
begin
  ChildNodes['cCor'].NodeValue := Value;
end;

function TXMLVeicNovos.Get_XCor: UnicodeString;
begin
  Result := ChildNodes['xCor'].Text;
end;

procedure TXMLVeicNovos.Set_XCor(Value: UnicodeString);
begin
  ChildNodes['xCor'].NodeValue := Value;
end;

function TXMLVeicNovos.Get_CMod: UnicodeString;
begin
  Result := ChildNodes['cMod'].Text;
end;

procedure TXMLVeicNovos.Set_CMod(Value: UnicodeString);
begin
  ChildNodes['cMod'].NodeValue := Value;
end;

function TXMLVeicNovos.Get_VUnit: UnicodeString;
begin
  Result := ChildNodes['vUnit'].Text;
end;

procedure TXMLVeicNovos.Set_VUnit(Value: UnicodeString);
begin
  ChildNodes['vUnit'].NodeValue := Value;
end;

function TXMLVeicNovos.Get_VFrete: UnicodeString;
begin
  Result := ChildNodes['vFrete'].Text;
end;

procedure TXMLVeicNovos.Set_VFrete(Value: UnicodeString);
begin
  ChildNodes['vFrete'].NodeValue := Value;
end;

{ TXMLVeicNovosList }

function TXMLVeicNovosList.Add: IXMLVeicNovos;
begin
  Result := AddItem(-1) as IXMLVeicNovos;
end;

function TXMLVeicNovosList.Insert(const Index: Integer): IXMLVeicNovos;
begin
  Result := AddItem(Index) as IXMLVeicNovos;
end;

function TXMLVeicNovosList.Get_Item(Index: Integer): IXMLVeicNovos;
begin
  Result := List[Index] as IXMLVeicNovos;
end;

{ TXMLCobr }

procedure TXMLCobr.AfterConstruction;
begin
  RegisterChildNode('fat', TXMLFat);
  RegisterChildNode('dup', TXMLDup);
  FDup := CreateCollection(TXMLDupList, IXMLDup, 'dup') as IXMLDupList;
  inherited;
end;

function TXMLCobr.Get_Fat: IXMLFat;
begin
  Result := ChildNodes['fat'] as IXMLFat;
end;

function TXMLCobr.Get_Dup: IXMLDupList;
begin
  Result := FDup;
end;

{ TXMLFat }

function TXMLFat.Get_NFat: UnicodeString;
begin
  Result := ChildNodes['nFat'].Text;
end;

procedure TXMLFat.Set_NFat(Value: UnicodeString);
begin
  ChildNodes['nFat'].NodeValue := Value;
end;

function TXMLFat.Get_VOrig: UnicodeString;
begin
  Result := ChildNodes['vOrig'].Text;
end;

procedure TXMLFat.Set_VOrig(Value: UnicodeString);
begin
  ChildNodes['vOrig'].NodeValue := Value;
end;

function TXMLFat.Get_VDesc: UnicodeString;
begin
  Result := ChildNodes['vDesc'].Text;
end;

procedure TXMLFat.Set_VDesc(Value: UnicodeString);
begin
  ChildNodes['vDesc'].NodeValue := Value;
end;

function TXMLFat.Get_VLiq: UnicodeString;
begin
  Result := ChildNodes['vLiq'].Text;
end;

procedure TXMLFat.Set_VLiq(Value: UnicodeString);
begin
  ChildNodes['vLiq'].NodeValue := Value;
end;

{ TXMLDup }

function TXMLDup.Get_NDup: UnicodeString;
begin
  Result := ChildNodes['nDup'].Text;
end;

procedure TXMLDup.Set_NDup(Value: UnicodeString);
begin
  ChildNodes['nDup'].NodeValue := Value;
end;

function TXMLDup.Get_DVenc: UnicodeString;
begin
  Result := ChildNodes['dVenc'].Text;
end;

procedure TXMLDup.Set_DVenc(Value: UnicodeString);
begin
  ChildNodes['dVenc'].NodeValue := Value;
end;

function TXMLDup.Get_VDup: UnicodeString;
begin
  Result := ChildNodes['vDup'].Text;
end;

procedure TXMLDup.Set_VDup(Value: UnicodeString);
begin
  ChildNodes['vDup'].NodeValue := Value;
end;

{ TXMLDupList }

function TXMLDupList.Add: IXMLDup;
begin
  Result := AddItem(-1) as IXMLDup;
end;

function TXMLDupList.Insert(const Index: Integer): IXMLDup;
begin
  Result := AddItem(Index) as IXMLDup;
end;

function TXMLDupList.Get_Item(Index: Integer): IXMLDup;
begin
  Result := List[Index] as IXMLDup;
end;

{ TXMLInfCteSub }

procedure TXMLInfCteSub.AfterConstruction;
begin
  RegisterChildNode('tomaICMS', TXMLTomaICMS);
  RegisterChildNode('tomaNaoICMS', TXMLTomaNaoICMS);
  inherited;
end;

function TXMLInfCteSub.Get_ChCte: UnicodeString;
begin
  Result := ChildNodes['chCte'].Text;
end;

procedure TXMLInfCteSub.Set_ChCte(Value: UnicodeString);
begin
  ChildNodes['chCte'].NodeValue := Value;
end;

function TXMLInfCteSub.Get_TomaICMS: IXMLTomaICMS;
begin
  Result := ChildNodes['tomaICMS'] as IXMLTomaICMS;
end;

function TXMLInfCteSub.Get_TomaNaoICMS: IXMLTomaNaoICMS;
begin
  Result := ChildNodes['tomaNaoICMS'] as IXMLTomaNaoICMS;
end;

{ TXMLTomaICMS }

procedure TXMLTomaICMS.AfterConstruction;
begin
  RegisterChildNode('refNF', TXMLRefNF);
  inherited;
end;

function TXMLTomaICMS.Get_RefNFe: UnicodeString;
begin
  Result := ChildNodes['refNFe'].Text;
end;

procedure TXMLTomaICMS.Set_RefNFe(Value: UnicodeString);
begin
  ChildNodes['refNFe'].NodeValue := Value;
end;

function TXMLTomaICMS.Get_RefNF: IXMLRefNF;
begin
  Result := ChildNodes['refNF'] as IXMLRefNF;
end;

function TXMLTomaICMS.Get_RefCte: UnicodeString;
begin
  Result := ChildNodes['refCte'].Text;
end;

procedure TXMLTomaICMS.Set_RefCte(Value: UnicodeString);
begin
  ChildNodes['refCte'].NodeValue := Value;
end;

{ TXMLRefNF }

function TXMLRefNF.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLRefNF.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLRefNF.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLRefNF.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLRefNF.Get_Mod_: UnicodeString;
begin
  Result := ChildNodes['mod'].Text;
end;

procedure TXMLRefNF.Set_Mod_(Value: UnicodeString);
begin
  ChildNodes['mod'].NodeValue := Value;
end;

function TXMLRefNF.Get_Serie: UnicodeString;
begin
  Result := ChildNodes['serie'].Text;
end;

procedure TXMLRefNF.Set_Serie(Value: UnicodeString);
begin
  ChildNodes['serie'].NodeValue := Value;
end;

function TXMLRefNF.Get_Subserie: UnicodeString;
begin
  Result := ChildNodes['subserie'].Text;
end;

procedure TXMLRefNF.Set_Subserie(Value: UnicodeString);
begin
  ChildNodes['subserie'].NodeValue := Value;
end;

function TXMLRefNF.Get_Nro: UnicodeString;
begin
  Result := ChildNodes['nro'].Text;
end;

procedure TXMLRefNF.Set_Nro(Value: UnicodeString);
begin
  ChildNodes['nro'].NodeValue := Value;
end;

function TXMLRefNF.Get_Valor: UnicodeString;
begin
  Result := ChildNodes['valor'].Text;
end;

procedure TXMLRefNF.Set_Valor(Value: UnicodeString);
begin
  ChildNodes['valor'].NodeValue := Value;
end;

function TXMLRefNF.Get_DEmi: UnicodeString;
begin
  Result := ChildNodes['dEmi'].Text;
end;

procedure TXMLRefNF.Set_DEmi(Value: UnicodeString);
begin
  ChildNodes['dEmi'].NodeValue := Value;
end;

{ TXMLTomaNaoICMS }

function TXMLTomaNaoICMS.Get_RefCteAnu: UnicodeString;
begin
  Result := ChildNodes['refCteAnu'].Text;
end;

procedure TXMLTomaNaoICMS.Set_RefCteAnu(Value: UnicodeString);
begin
  ChildNodes['refCteAnu'].NodeValue := Value;
end;

{ TXMLInfCteComp }

function TXMLInfCteComp.Get_Chave: UnicodeString;
begin
  Result := ChildNodes['chave'].Text;
end;

procedure TXMLInfCteComp.Set_Chave(Value: UnicodeString);
begin
  ChildNodes['chave'].NodeValue := Value;
end;

{ TXMLInfCteAnu }

function TXMLInfCteAnu.Get_ChCte: UnicodeString;
begin
  Result := ChildNodes['chCte'].Text;
end;

procedure TXMLInfCteAnu.Set_ChCte(Value: UnicodeString);
begin
  ChildNodes['chCte'].NodeValue := Value;
end;

function TXMLInfCteAnu.Get_DEmi: UnicodeString;
begin
  Result := ChildNodes['dEmi'].Text;
end;

procedure TXMLInfCteAnu.Set_DEmi(Value: UnicodeString);
begin
  ChildNodes['dEmi'].NodeValue := Value;
end;

{ TXMLAutXML }

function TXMLAutXML.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLAutXML.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLAutXML.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLAutXML.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

{ TXMLAutXMLList }

function TXMLAutXMLList.Add: IXMLAutXML;
begin
  Result := AddItem(-1) as IXMLAutXML;
end;

function TXMLAutXMLList.Insert(const Index: Integer): IXMLAutXML;
begin
  Result := AddItem(Index) as IXMLAutXML;
end;

function TXMLAutXMLList.Get_Item(Index: Integer): IXMLAutXML;
begin
  Result := List[Index] as IXMLAutXML;
end;

{ TXMLSignatureType_ds }

procedure TXMLSignatureType_ds.AfterConstruction;
begin
  RegisterChildNode('SignedInfo', TXMLSignedInfoType_ds);
  RegisterChildNode('SignatureValue', TXMLSignatureValueType_ds);
  RegisterChildNode('KeyInfo', TXMLKeyInfoType_ds);
  inherited;
end;

function TXMLSignatureType_ds.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignatureType_ds.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLSignatureType_ds.Get_SignedInfo: IXMLSignedInfoType_ds;
begin
  Result := ChildNodes['SignedInfo'] as IXMLSignedInfoType_ds;
end;

function TXMLSignatureType_ds.Get_SignatureValue: IXMLSignatureValueType_ds;
begin
  Result := ChildNodes['SignatureValue'] as IXMLSignatureValueType_ds;
end;

function TXMLSignatureType_ds.Get_KeyInfo: IXMLKeyInfoType_ds;
begin
  Result := ChildNodes['KeyInfo'] as IXMLKeyInfoType_ds;
end;

{ TXMLSignedInfoType_ds }

procedure TXMLSignedInfoType_ds.AfterConstruction;
begin
  RegisterChildNode('CanonicalizationMethod', TXMLCanonicalizationMethod_ds);
  RegisterChildNode('SignatureMethod', TXMLSignatureMethod_ds);
  RegisterChildNode('Reference', TXMLReferenceType_ds);
  inherited;
end;

function TXMLSignedInfoType_ds.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignedInfoType_ds.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLSignedInfoType_ds.Get_CanonicalizationMethod: IXMLCanonicalizationMethod_ds;
begin
  Result := ChildNodes['CanonicalizationMethod'] as IXMLCanonicalizationMethod_ds;
end;

function TXMLSignedInfoType_ds.Get_SignatureMethod: IXMLSignatureMethod_ds;
begin
  Result := ChildNodes['SignatureMethod'] as IXMLSignatureMethod_ds;
end;

function TXMLSignedInfoType_ds.Get_Reference: IXMLReferenceType_ds;
begin
  Result := ChildNodes['Reference'] as IXMLReferenceType_ds;
end;

{ TXMLCanonicalizationMethod_ds }

function TXMLCanonicalizationMethod_ds.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLCanonicalizationMethod_ds.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLSignatureMethod_ds }

function TXMLSignatureMethod_ds.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLSignatureMethod_ds.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLReferenceType_ds }

procedure TXMLReferenceType_ds.AfterConstruction;
begin
  RegisterChildNode('Transforms', TXMLTransformsType_ds);
  RegisterChildNode('DigestMethod', TXMLDigestMethod_ds);
  inherited;
end;

function TXMLReferenceType_ds.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLReferenceType_ds.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLReferenceType_ds.Get_URI: UnicodeString;
begin
  Result := AttributeNodes['URI'].Text;
end;

procedure TXMLReferenceType_ds.Set_URI(Value: UnicodeString);
begin
  SetAttribute('URI', Value);
end;

function TXMLReferenceType_ds.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLReferenceType_ds.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

function TXMLReferenceType_ds.Get_Transforms: IXMLTransformsType_ds;
begin
  Result := ChildNodes['Transforms'] as IXMLTransformsType_ds;
end;

function TXMLReferenceType_ds.Get_DigestMethod: IXMLDigestMethod_ds;
begin
  Result := ChildNodes['DigestMethod'] as IXMLDigestMethod_ds;
end;

function TXMLReferenceType_ds.Get_DigestValue: UnicodeString;
begin
  Result := ChildNodes['DigestValue'].Text;
end;

procedure TXMLReferenceType_ds.Set_DigestValue(Value: UnicodeString);
begin
  ChildNodes['DigestValue'].NodeValue := Value;
end;

{ TXMLTransformsType_ds }

procedure TXMLTransformsType_ds.AfterConstruction;
begin
  RegisterChildNode('Transform', TXMLTransformType_ds);
  ItemTag := 'Transform';
  ItemInterface := IXMLTransformType_ds;
  inherited;
end;

function TXMLTransformsType_ds.Get_Transform(Index: Integer): IXMLTransformType_ds;
begin
  Result := List[Index] as IXMLTransformType_ds;
end;

function TXMLTransformsType_ds.Add: IXMLTransformType_ds;
begin
  Result := AddItem(-1) as IXMLTransformType_ds;
end;

function TXMLTransformsType_ds.Insert(const Index: Integer): IXMLTransformType_ds;
begin
  Result := AddItem(Index) as IXMLTransformType_ds;
end;

{ TXMLTransformType_ds }

procedure TXMLTransformType_ds.AfterConstruction;
begin
  ItemTag := 'XPath';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLTransformType_ds.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLTransformType_ds.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

function TXMLTransformType_ds.Get_XPath(Index: Integer): UnicodeString;
begin
  Result := List[Index].Text;
end;

function TXMLTransformType_ds.Add(const XPath: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := XPath;
end;

function TXMLTransformType_ds.Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := XPath;
end;

{ TXMLDigestMethod_ds }

function TXMLDigestMethod_ds.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLDigestMethod_ds.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLSignatureValueType_ds }

function TXMLSignatureValueType_ds.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignatureValueType_ds.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

{ TXMLKeyInfoType_ds }

procedure TXMLKeyInfoType_ds.AfterConstruction;
begin
  RegisterChildNode('X509Data', TXMLX509DataType_ds);
  inherited;
end;

function TXMLKeyInfoType_ds.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLKeyInfoType_ds.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLKeyInfoType_ds.Get_X509Data: IXMLX509DataType_ds;
begin
  Result := ChildNodes['X509Data'] as IXMLX509DataType_ds;
end;

{ TXMLX509DataType_ds }

function TXMLX509DataType_ds.Get_X509Certificate: UnicodeString;
begin
  Result := ChildNodes['X509Certificate'].Text;
end;

procedure TXMLX509DataType_ds.Set_X509Certificate(Value: UnicodeString);
begin
  ChildNodes['X509Certificate'].NodeValue := Value;
end;

{ TXMLTEnviCTe }

procedure TXMLTEnviCTe.AfterConstruction;
begin
  RegisterChildNode('CTe', TXMLTCTe);
  FCTe := CreateCollection(TXMLTCTeList, IXMLTCTe, 'CTe') as IXMLTCTeList;
  inherited;
end;

function TXMLTEnviCTe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTEnviCTe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTEnviCTe.Get_IdLote: UnicodeString;
begin
  Result := ChildNodes['idLote'].Text;
end;

procedure TXMLTEnviCTe.Set_IdLote(Value: UnicodeString);
begin
  ChildNodes['idLote'].NodeValue := Value;
end;

function TXMLTEnviCTe.Get_CTe: IXMLTCTeList;
begin
  Result := FCTe;
end;

{ TXMLTRetEnviCTe }

procedure TXMLTRetEnviCTe.AfterConstruction;
begin
  RegisterChildNode('infRec', TXMLInfRec);
  inherited;
end;

function TXMLTRetEnviCTe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetEnviCTe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetEnviCTe.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTRetEnviCTe.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTRetEnviCTe.Get_CUF: UnicodeString;
begin
  Result := ChildNodes['cUF'].Text;
end;

procedure TXMLTRetEnviCTe.Set_CUF(Value: UnicodeString);
begin
  ChildNodes['cUF'].NodeValue := Value;
end;

function TXMLTRetEnviCTe.Get_VerAplic: UnicodeString;
begin
  Result := ChildNodes['verAplic'].Text;
end;

procedure TXMLTRetEnviCTe.Set_VerAplic(Value: UnicodeString);
begin
  ChildNodes['verAplic'].NodeValue := Value;
end;

function TXMLTRetEnviCTe.Get_CStat: UnicodeString;
begin
  Result := ChildNodes['cStat'].Text;
end;

procedure TXMLTRetEnviCTe.Set_CStat(Value: UnicodeString);
begin
  ChildNodes['cStat'].NodeValue := Value;
end;

function TXMLTRetEnviCTe.Get_XMotivo: UnicodeString;
begin
  Result := ChildNodes['xMotivo'].Text;
end;

procedure TXMLTRetEnviCTe.Set_XMotivo(Value: UnicodeString);
begin
  ChildNodes['xMotivo'].NodeValue := Value;
end;

function TXMLTRetEnviCTe.Get_InfRec: IXMLInfRec;
begin
  Result := ChildNodes['infRec'] as IXMLInfRec;
end;

{ TXMLInfRec }

function TXMLInfRec.Get_NRec: UnicodeString;
begin
  Result := ChildNodes['nRec'].Text;
end;

procedure TXMLInfRec.Set_NRec(Value: UnicodeString);
begin
  ChildNodes['nRec'].NodeValue := Value;
end;

function TXMLInfRec.Get_DhRecbto: UnicodeString;
begin
  Result := ChildNodes['dhRecbto'].Text;
end;

procedure TXMLInfRec.Set_DhRecbto(Value: UnicodeString);
begin
  ChildNodes['dhRecbto'].NodeValue := Value;
end;

function TXMLInfRec.Get_TMed: Integer;
begin
  Result := ChildNodes['tMed'].NodeValue;
end;

procedure TXMLInfRec.Set_TMed(Value: Integer);
begin
  ChildNodes['tMed'].NodeValue := Value;
end;

{ TXMLTEndernac }

function TXMLTEndernac.Get_XLgr: UnicodeString;
begin
  Result := ChildNodes['xLgr'].Text;
end;

procedure TXMLTEndernac.Set_XLgr(Value: UnicodeString);
begin
  ChildNodes['xLgr'].NodeValue := Value;
end;

function TXMLTEndernac.Get_Nro: UnicodeString;
begin
  Result := ChildNodes['nro'].Text;
end;

procedure TXMLTEndernac.Set_Nro(Value: UnicodeString);
begin
  ChildNodes['nro'].NodeValue := Value;
end;

function TXMLTEndernac.Get_XCpl: UnicodeString;
begin
  Result := ChildNodes['xCpl'].Text;
end;

procedure TXMLTEndernac.Set_XCpl(Value: UnicodeString);
begin
  ChildNodes['xCpl'].NodeValue := Value;
end;

function TXMLTEndernac.Get_XBairro: UnicodeString;
begin
  Result := ChildNodes['xBairro'].Text;
end;

procedure TXMLTEndernac.Set_XBairro(Value: UnicodeString);
begin
  ChildNodes['xBairro'].NodeValue := Value;
end;

function TXMLTEndernac.Get_CMun: UnicodeString;
begin
  Result := ChildNodes['cMun'].Text;
end;

procedure TXMLTEndernac.Set_CMun(Value: UnicodeString);
begin
  ChildNodes['cMun'].NodeValue := Value;
end;

function TXMLTEndernac.Get_XMun: UnicodeString;
begin
  Result := ChildNodes['xMun'].Text;
end;

procedure TXMLTEndernac.Set_XMun(Value: UnicodeString);
begin
  ChildNodes['xMun'].NodeValue := Value;
end;

function TXMLTEndernac.Get_CEP: UnicodeString;
begin
  Result := ChildNodes['CEP'].Text;
end;

procedure TXMLTEndernac.Set_CEP(Value: UnicodeString);
begin
  ChildNodes['CEP'].NodeValue := Value;
end;

function TXMLTEndernac.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLTEndernac.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

{ TXMLTEndOrg }

function TXMLTEndOrg.Get_XLgr: UnicodeString;
begin
  Result := ChildNodes['xLgr'].Text;
end;

procedure TXMLTEndOrg.Set_XLgr(Value: UnicodeString);
begin
  ChildNodes['xLgr'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_Nro: UnicodeString;
begin
  Result := ChildNodes['nro'].Text;
end;

procedure TXMLTEndOrg.Set_Nro(Value: UnicodeString);
begin
  ChildNodes['nro'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_XCpl: UnicodeString;
begin
  Result := ChildNodes['xCpl'].Text;
end;

procedure TXMLTEndOrg.Set_XCpl(Value: UnicodeString);
begin
  ChildNodes['xCpl'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_XBairro: UnicodeString;
begin
  Result := ChildNodes['xBairro'].Text;
end;

procedure TXMLTEndOrg.Set_XBairro(Value: UnicodeString);
begin
  ChildNodes['xBairro'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_CMun: UnicodeString;
begin
  Result := ChildNodes['cMun'].Text;
end;

procedure TXMLTEndOrg.Set_CMun(Value: UnicodeString);
begin
  ChildNodes['cMun'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_XMun: UnicodeString;
begin
  Result := ChildNodes['xMun'].Text;
end;

procedure TXMLTEndOrg.Set_XMun(Value: UnicodeString);
begin
  ChildNodes['xMun'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_CEP: UnicodeString;
begin
  Result := ChildNodes['CEP'].Text;
end;

procedure TXMLTEndOrg.Set_CEP(Value: UnicodeString);
begin
  ChildNodes['CEP'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLTEndOrg.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_CPais: UnicodeString;
begin
  Result := ChildNodes['cPais'].Text;
end;

procedure TXMLTEndOrg.Set_CPais(Value: UnicodeString);
begin
  ChildNodes['cPais'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_XPais: UnicodeString;
begin
  Result := ChildNodes['xPais'].Text;
end;

procedure TXMLTEndOrg.Set_XPais(Value: UnicodeString);
begin
  ChildNodes['xPais'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_Fone: UnicodeString;
begin
  Result := ChildNodes['fone'].Text;
end;

procedure TXMLTEndOrg.Set_Fone(Value: UnicodeString);
begin
  ChildNodes['fone'].NodeValue := Value;
end;

{ TXMLTLocal }

function TXMLTLocal.Get_CMun: UnicodeString;
begin
  Result := ChildNodes['cMun'].Text;
end;

procedure TXMLTLocal.Set_CMun(Value: UnicodeString);
begin
  ChildNodes['cMun'].NodeValue := Value;
end;

function TXMLTLocal.Get_XMun: UnicodeString;
begin
  Result := ChildNodes['xMun'].Text;
end;

procedure TXMLTLocal.Set_XMun(Value: UnicodeString);
begin
  ChildNodes['xMun'].NodeValue := Value;
end;

function TXMLTLocal.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLTLocal.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

{ TXMLTEndReEnt }

function TXMLTEndReEnt.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLTEndReEnt.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLTEndReEnt.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLTEndReEnt.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_XLgr: UnicodeString;
begin
  Result := ChildNodes['xLgr'].Text;
end;

procedure TXMLTEndReEnt.Set_XLgr(Value: UnicodeString);
begin
  ChildNodes['xLgr'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_Nro: UnicodeString;
begin
  Result := ChildNodes['nro'].Text;
end;

procedure TXMLTEndReEnt.Set_Nro(Value: UnicodeString);
begin
  ChildNodes['nro'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_XCpl: UnicodeString;
begin
  Result := ChildNodes['xCpl'].Text;
end;

procedure TXMLTEndReEnt.Set_XCpl(Value: UnicodeString);
begin
  ChildNodes['xCpl'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_XBairro: UnicodeString;
begin
  Result := ChildNodes['xBairro'].Text;
end;

procedure TXMLTEndReEnt.Set_XBairro(Value: UnicodeString);
begin
  ChildNodes['xBairro'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_CMun: UnicodeString;
begin
  Result := ChildNodes['cMun'].Text;
end;

procedure TXMLTEndReEnt.Set_CMun(Value: UnicodeString);
begin
  ChildNodes['cMun'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_XMun: UnicodeString;
begin
  Result := ChildNodes['xMun'].Text;
end;

procedure TXMLTEndReEnt.Set_XMun(Value: UnicodeString);
begin
  ChildNodes['xMun'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLTEndReEnt.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

end.