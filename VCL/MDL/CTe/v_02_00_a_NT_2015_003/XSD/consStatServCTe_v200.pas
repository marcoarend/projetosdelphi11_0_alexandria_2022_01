
{*****************************************************************************************************************************************}
{                                                                                                                                         }
{                                                            XML Data Binding                                                             }
{                                                                                                                                         }
{         Generated on: 12/11/2015 20:38:17                                                                                               }
{       Generated from: C:\_Compilers\DELPHI_XE2\VCL\MDL\CTe\v_02_00_a_NT_2015_003\XSD\PL_CTe_200a_NT2015.003\consStatServCTe_v2.00.xsd   }
{   Settings stored in: C:\_Compilers\DELPHI_XE2\VCL\MDL\CTe\v_02_00_a_NT_2015_003\XSD\PL_CTe_200a_NT2015.003\consStatServCTe_v2.00.xdb   }
{                                                                                                                                         }
{*****************************************************************************************************************************************}

unit consStatServCTe_v200;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTConsStatServ = interface;
  IXMLTRetConsStatServ = interface;

{ IXMLTConsStatServ }

  IXMLTConsStatServ = interface(IXMLNode)
    ['{F2F1424B-906F-4DBC-A186-9FB97ECBCD33}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_XServ: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_XServ(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property XServ: UnicodeString read Get_XServ write Set_XServ;
  end;

{ IXMLTRetConsStatServ }

  IXMLTRetConsStatServ = interface(IXMLNode)
    ['{3F49C7A2-39FA-481A-BD89-402C156D1817}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_TMed: Integer;
    function Get_DhRetorno: UnicodeString;
    function Get_XObs: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_TMed(Value: Integer);
    procedure Set_DhRetorno(Value: UnicodeString);
    procedure Set_XObs(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property VerAplic: UnicodeString read Get_VerAplic write Set_VerAplic;
    property CStat: UnicodeString read Get_CStat write Set_CStat;
    property XMotivo: UnicodeString read Get_XMotivo write Set_XMotivo;
    property CUF: UnicodeString read Get_CUF write Set_CUF;
    property DhRecbto: UnicodeString read Get_DhRecbto write Set_DhRecbto;
    property TMed: Integer read Get_TMed write Set_TMed;
    property DhRetorno: UnicodeString read Get_DhRetorno write Set_DhRetorno;
    property XObs: UnicodeString read Get_XObs write Set_XObs;
  end;

{ Forward Decls }

  TXMLTConsStatServ = class;
  TXMLTRetConsStatServ = class;

{ TXMLTConsStatServ }

  TXMLTConsStatServ = class(TXMLNode, IXMLTConsStatServ)
  protected
    { IXMLTConsStatServ }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_XServ: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_XServ(Value: UnicodeString);
  end;

{ TXMLTRetConsStatServ }

  TXMLTRetConsStatServ = class(TXMLNode, IXMLTRetConsStatServ)
  protected
    { IXMLTRetConsStatServ }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_TMed: Integer;
    function Get_DhRetorno: UnicodeString;
    function Get_XObs: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_TMed(Value: Integer);
    procedure Set_DhRetorno(Value: UnicodeString);
    procedure Set_XObs(Value: UnicodeString);
  end;

{ Global Functions }

function GetconsStatServ(Doc: IXMLDocument): IXMLTConsStatServ;
function LoadconsStatServ(const FileName: string): IXMLTConsStatServ;
function NewconsStatServ: IXMLTConsStatServ;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/cte';

implementation

{ Global Functions }

function GetconsStatServ(Doc: IXMLDocument): IXMLTConsStatServ;
begin
  Result := Doc.GetDocBinding('consStatServCte', TXMLTConsStatServ, TargetNamespace) as IXMLTConsStatServ;
end;

function LoadconsStatServ(const FileName: string): IXMLTConsStatServ;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('consStatServCte', TXMLTConsStatServ, TargetNamespace) as IXMLTConsStatServ;
end;

function NewconsStatServ: IXMLTConsStatServ;
begin
  Result := NewXMLDocument.GetDocBinding('consStatServCte', TXMLTConsStatServ, TargetNamespace) as IXMLTConsStatServ;
end;

{ TXMLTConsStatServ }

function TXMLTConsStatServ.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTConsStatServ.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTConsStatServ.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTConsStatServ.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTConsStatServ.Get_XServ: UnicodeString;
begin
  Result := ChildNodes['xServ'].Text;
end;

procedure TXMLTConsStatServ.Set_XServ(Value: UnicodeString);
begin
  ChildNodes['xServ'].NodeValue := Value;
end;

{ TXMLTRetConsStatServ }

function TXMLTRetConsStatServ.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetConsStatServ.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetConsStatServ.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTRetConsStatServ.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_VerAplic: UnicodeString;
begin
  Result := ChildNodes['verAplic'].Text;
end;

procedure TXMLTRetConsStatServ.Set_VerAplic(Value: UnicodeString);
begin
  ChildNodes['verAplic'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_CStat: UnicodeString;
begin
  Result := ChildNodes['cStat'].Text;
end;

procedure TXMLTRetConsStatServ.Set_CStat(Value: UnicodeString);
begin
  ChildNodes['cStat'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_XMotivo: UnicodeString;
begin
  Result := ChildNodes['xMotivo'].Text;
end;

procedure TXMLTRetConsStatServ.Set_XMotivo(Value: UnicodeString);
begin
  ChildNodes['xMotivo'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_CUF: UnicodeString;
begin
  Result := ChildNodes['cUF'].Text;
end;

procedure TXMLTRetConsStatServ.Set_CUF(Value: UnicodeString);
begin
  ChildNodes['cUF'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_DhRecbto: UnicodeString;
begin
  Result := ChildNodes['dhRecbto'].Text;
end;

procedure TXMLTRetConsStatServ.Set_DhRecbto(Value: UnicodeString);
begin
  ChildNodes['dhRecbto'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_TMed: Integer;
begin
  Result := ChildNodes['tMed'].NodeValue;
end;

procedure TXMLTRetConsStatServ.Set_TMed(Value: Integer);
begin
  ChildNodes['tMed'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_DhRetorno: UnicodeString;
begin
  Result := ChildNodes['dhRetorno'].Text;
end;

procedure TXMLTRetConsStatServ.Set_DhRetorno(Value: UnicodeString);
begin
  ChildNodes['dhRetorno'].NodeValue := Value;
end;

function TXMLTRetConsStatServ.Get_XObs: UnicodeString;
begin
  Result := ChildNodes['xObs'].Text;
end;

procedure TXMLTRetConsStatServ.Set_XObs(Value: UnicodeString);
begin
  ChildNodes['xObs'].NodeValue := Value;
end;

end.