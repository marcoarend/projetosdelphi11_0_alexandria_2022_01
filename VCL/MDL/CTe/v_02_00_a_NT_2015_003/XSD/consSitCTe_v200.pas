
{************************************************************************************************************************************}
{                                                                                                                                    }
{                                                          XML Data Binding                                                          }
{                                                                                                                                    }
{         Generated on: 09/01/2016 08:27:53                                                                                          }
{       Generated from: C:\_Compilers\DELPHI_XE2\VCL\MDL\CTe\v_02_00_a_NT_2015_003\XSD\PL_CTe_200a_NT2015.003\consSitCTe_v2.00.xsd   }
{   Settings stored in: C:\_Compilers\DELPHI_XE2\VCL\MDL\CTe\v_02_00_a_NT_2015_003\XSD\PL_CTe_200a_NT2015.003\consSitCTe_v2.00.xdb   }
{                                                                                                                                    }
{************************************************************************************************************************************}

unit consSitCTe_v200;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTConsSitCTe = interface;
  IXMLTProtCTe = interface;
  IXMLTProtCTeList = interface;
  IXMLInfProt = interface;
  IXMLSignatureType_ = interface;
  IXMLSignedInfoType_ = interface;
  IXMLCanonicalizationMethod_ = interface;
  IXMLSignatureMethod_ = interface;
  IXMLReferenceType_ = interface;
  IXMLTransformsType_ = interface;
  IXMLTransformType_ = interface;
  IXMLDigestMethod_ = interface;
  IXMLSignatureValueType_ = interface;
  IXMLKeyInfoType_ = interface;
  IXMLX509DataType_ = interface;
  IXMLTConsReciCTe = interface;
  IXMLTRetConsReciCTe = interface;
  IXMLTCancCTe = interface;
  IXMLInfCanc = interface;
  IXMLTRetCancCTe = interface;
  IXMLTProcCancCTe = interface;
  IXMLTEvento = interface;
  IXMLInfEvento = interface;
  IXMLDetEvento = interface;
  IXMLTRetEvento = interface;
  IXMLTProcEvento = interface;
  IXMLTProcEventoList = interface;
  IXMLTRetConsSitCTe = interface;

{ IXMLTConsSitCTe }

  IXMLTConsSitCTe = interface(IXMLNode)
    ['{4195DDC0-881E-4583-B6A6-71A478925867}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_XServ: UnicodeString;
    function Get_ChCTe: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_XServ(Value: UnicodeString);
    procedure Set_ChCTe(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property XServ: UnicodeString read Get_XServ write Set_XServ;
    property ChCTe: UnicodeString read Get_ChCTe write Set_ChCTe;
  end;

{ IXMLTProtCTe }

  IXMLTProtCTe = interface(IXMLNode)
    ['{B4D108CC-8046-4059-BE7D-7F1A1B83261D}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_InfProt: IXMLInfProt;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property InfProt: IXMLInfProt read Get_InfProt;
    property Signature: IXMLSignatureType_ read Get_Signature;
  end;

{ IXMLTProtCTeList }

  IXMLTProtCTeList = interface(IXMLNodeCollection)
    ['{DFB3A434-133E-40E7-9B04-ACE635469766}']
    { Methods & Properties }
    function Add: IXMLTProtCTe;
    function Insert(const Index: Integer): IXMLTProtCTe;

    function Get_Item(Index: Integer): IXMLTProtCTe;
    property Items[Index: Integer]: IXMLTProtCTe read Get_Item; default;
  end;

{ IXMLInfProt }

  IXMLInfProt = interface(IXMLNode)
    ['{A8C78453-7943-44D6-A031-F29281528C36}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_ChCTe: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_NProt: UnicodeString;
    function Get_DigVal: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_ChCTe(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_NProt(Value: UnicodeString);
    procedure Set_DigVal(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property VerAplic: UnicodeString read Get_VerAplic write Set_VerAplic;
    property ChCTe: UnicodeString read Get_ChCTe write Set_ChCTe;
    property DhRecbto: UnicodeString read Get_DhRecbto write Set_DhRecbto;
    property NProt: UnicodeString read Get_NProt write Set_NProt;
    property DigVal: UnicodeString read Get_DigVal write Set_DigVal;
    property CStat: UnicodeString read Get_CStat write Set_CStat;
    property XMotivo: UnicodeString read Get_XMotivo write Set_XMotivo;
  end;

{ IXMLSignatureType_ }

  IXMLSignatureType_ = interface(IXMLNode)
    ['{513FAC30-8D9E-43C6-A299-DFEDF235884F}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_SignedInfo: IXMLSignedInfoType_;
    function Get_SignatureValue: IXMLSignatureValueType_;
    function Get_KeyInfo: IXMLKeyInfoType_;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property SignedInfo: IXMLSignedInfoType_ read Get_SignedInfo;
    property SignatureValue: IXMLSignatureValueType_ read Get_SignatureValue;
    property KeyInfo: IXMLKeyInfoType_ read Get_KeyInfo;
  end;

{ IXMLSignedInfoType_ }

  IXMLSignedInfoType_ = interface(IXMLNode)
    ['{94B043F0-943E-40BD-9593-0ADADEA15644}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_CanonicalizationMethod: IXMLCanonicalizationMethod_;
    function Get_SignatureMethod: IXMLSignatureMethod_;
    function Get_Reference: IXMLReferenceType_;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property CanonicalizationMethod: IXMLCanonicalizationMethod_ read Get_CanonicalizationMethod;
    property SignatureMethod: IXMLSignatureMethod_ read Get_SignatureMethod;
    property Reference: IXMLReferenceType_ read Get_Reference;
  end;

{ IXMLCanonicalizationMethod_ }

  IXMLCanonicalizationMethod_ = interface(IXMLNode)
    ['{C5E7FC2F-B030-45DB-9C2F-48609159D7B1}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLSignatureMethod_ }

  IXMLSignatureMethod_ = interface(IXMLNode)
    ['{FAD4E82A-EE35-4241-897E-275440ACCCE7}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLReferenceType_ }

  IXMLReferenceType_ = interface(IXMLNode)
    ['{527BC29D-5733-4795-9A2F-60C07AFFB093}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_URI: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_Transforms: IXMLTransformsType_;
    function Get_DigestMethod: IXMLDigestMethod_;
    function Get_DigestValue: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_URI(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_DigestValue(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property URI: UnicodeString read Get_URI write Set_URI;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property Transforms: IXMLTransformsType_ read Get_Transforms;
    property DigestMethod: IXMLDigestMethod_ read Get_DigestMethod;
    property DigestValue: UnicodeString read Get_DigestValue write Set_DigestValue;
  end;

{ IXMLTransformsType_ }

  IXMLTransformsType_ = interface(IXMLNodeCollection)
    ['{0C01A95E-7F58-44DE-9F24-1D90D3FAC79F}']
    { Property Accessors }
    function Get_Transform(Index: Integer): IXMLTransformType_;
    { Methods & Properties }
    function Add: IXMLTransformType_;
    function Insert(const Index: Integer): IXMLTransformType_;
    property Transform[Index: Integer]: IXMLTransformType_ read Get_Transform; default;
  end;

{ IXMLTransformType_ }

  IXMLTransformType_ = interface(IXMLNodeCollection)
    ['{64A362AB-F3D9-402E-A7D8-DC7228AC8936}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    function Get_XPath(Index: Integer): UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    function Add(const XPath: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
    property XPath[Index: Integer]: UnicodeString read Get_XPath; default;
  end;

{ IXMLDigestMethod_ }

  IXMLDigestMethod_ = interface(IXMLNode)
    ['{377D7200-222D-43AD-8FCF-64AE2CBF3E98}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLSignatureValueType_ }

  IXMLSignatureValueType_ = interface(IXMLNode)
    ['{890CACD0-79A8-4BF6-89F7-642893849B0F}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
  end;

{ IXMLKeyInfoType_ }

  IXMLKeyInfoType_ = interface(IXMLNode)
    ['{9733207E-AFF5-41D3-B20B-9994A895258D}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_X509Data: IXMLX509DataType_;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property X509Data: IXMLX509DataType_ read Get_X509Data;
  end;

{ IXMLX509DataType_ }

  IXMLX509DataType_ = interface(IXMLNode)
    ['{E17538BD-1D16-4F76-99D2-A4BB687AC51B}']
    { Property Accessors }
    function Get_X509Certificate: UnicodeString;
    procedure Set_X509Certificate(Value: UnicodeString);
    { Methods & Properties }
    property X509Certificate: UnicodeString read Get_X509Certificate write Set_X509Certificate;
  end;

{ IXMLTConsReciCTe }

  IXMLTConsReciCTe = interface(IXMLNode)
    ['{F8C01F90-078C-45A3-909A-7057CEC5131F}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_NRec: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_NRec(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property NRec: UnicodeString read Get_NRec write Set_NRec;
  end;

{ IXMLTRetConsReciCTe }

  IXMLTRetConsReciCTe = interface(IXMLNode)
    ['{32B366D0-332A-42E4-BAC0-8349EDE6ACE0}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_NRec: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_ProtCTe: IXMLTProtCTeList;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_NRec(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property VerAplic: UnicodeString read Get_VerAplic write Set_VerAplic;
    property NRec: UnicodeString read Get_NRec write Set_NRec;
    property CStat: UnicodeString read Get_CStat write Set_CStat;
    property XMotivo: UnicodeString read Get_XMotivo write Set_XMotivo;
    property CUF: UnicodeString read Get_CUF write Set_CUF;
    property ProtCTe: IXMLTProtCTeList read Get_ProtCTe;
  end;

{ IXMLTCancCTe }

  IXMLTCancCTe = interface(IXMLNode)
    ['{54B32721-879A-4243-AC8C-E7982BFEE872}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_InfCanc: IXMLInfCanc;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property InfCanc: IXMLInfCanc read Get_InfCanc;
    property Signature: IXMLSignatureType_ read Get_Signature;
  end;

{ IXMLInfCanc }

  IXMLInfCanc = interface(IXMLNode)
    ['{0C2CD574-BAFF-47E8-AB56-22083E496264}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_XServ: UnicodeString;
    function Get_ChCTe: UnicodeString;
    function Get_NProt: UnicodeString;
    function Get_XJust: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_XServ(Value: UnicodeString);
    procedure Set_ChCTe(Value: UnicodeString);
    procedure Set_NProt(Value: UnicodeString);
    procedure Set_XJust(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property XServ: UnicodeString read Get_XServ write Set_XServ;
    property ChCTe: UnicodeString read Get_ChCTe write Set_ChCTe;
    property NProt: UnicodeString read Get_NProt write Set_NProt;
    property XJust: UnicodeString read Get_XJust write Set_XJust;
  end;

{ IXMLTRetCancCTe }

  IXMLTRetCancCTe = interface(IXMLNode)
    ['{6157E44B-A977-486E-A9FC-57A4AEBE2D1E}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_InfCanc: IXMLInfCanc;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property InfCanc: IXMLInfCanc read Get_InfCanc;
    property Signature: IXMLSignatureType_ read Get_Signature;
  end;

{ IXMLTProcCancCTe }

  IXMLTProcCancCTe = interface(IXMLNode)
    ['{D93B564F-6E5F-4A5F-91B3-3C1102BE230D}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_CancCTe: IXMLTCancCTe;
    function Get_RetCancCTe: IXMLTRetCancCTe;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property CancCTe: IXMLTCancCTe read Get_CancCTe;
    property RetCancCTe: IXMLTRetCancCTe read Get_RetCancCTe;
  end;

{ IXMLTEvento }

  IXMLTEvento = interface(IXMLNode)
    ['{C97B81B9-8FC6-4048-8E2C-167C3F2EAFD7}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_InfEvento: IXMLInfEvento;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property InfEvento: IXMLInfEvento read Get_InfEvento;
    property Signature: IXMLSignatureType_ read Get_Signature;
  end;

{ IXMLInfEvento }

  IXMLInfEvento = interface(IXMLNode)
    ['{A95B3E5D-091C-4E45-A7B0-988D008E3A28}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_COrgao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_ChCTe: UnicodeString;
    function Get_DhEvento: UnicodeString;
    function Get_TpEvento: UnicodeString;
    function Get_NSeqEvento: UnicodeString;
    function Get_DetEvento: IXMLDetEvento;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_COrgao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_ChCTe(Value: UnicodeString);
    procedure Set_DhEvento(Value: UnicodeString);
    procedure Set_TpEvento(Value: UnicodeString);
    procedure Set_NSeqEvento(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property COrgao: UnicodeString read Get_COrgao write Set_COrgao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property ChCTe: UnicodeString read Get_ChCTe write Set_ChCTe;
    property DhEvento: UnicodeString read Get_DhEvento write Set_DhEvento;
    property TpEvento: UnicodeString read Get_TpEvento write Set_TpEvento;
    property NSeqEvento: UnicodeString read Get_NSeqEvento write Set_NSeqEvento;
    property DetEvento: IXMLDetEvento read Get_DetEvento;
  end;

{ IXMLDetEvento }

  IXMLDetEvento = interface(IXMLNode)
    ['{905E17A2-02B1-42FC-B30C-42D43093FA6A}']
    { Property Accessors }
    function Get_VersaoEvento: UnicodeString;
    procedure Set_VersaoEvento(Value: UnicodeString);
    { Methods & Properties }
    property VersaoEvento: UnicodeString read Get_VersaoEvento write Set_VersaoEvento;
  end;

{ IXMLTRetEvento }

  IXMLTRetEvento = interface(IXMLNode)
    ['{A6710B0D-71A6-41F0-BB41-7E2C8B3CE949}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_InfEvento: IXMLInfEvento;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property InfEvento: IXMLInfEvento read Get_InfEvento;
    property Signature: IXMLSignatureType_ read Get_Signature;
  end;

{ IXMLTProcEvento }

  IXMLTProcEvento = interface(IXMLNode)
    ['{40192025-B040-4E84-86CA-7701F6598D83}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_EventoCTe: IXMLTEvento;
    function Get_RetEventoCTe: IXMLTRetEvento;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property EventoCTe: IXMLTEvento read Get_EventoCTe;
    property RetEventoCTe: IXMLTRetEvento read Get_RetEventoCTe;
  end;

{ IXMLTProcEventoList }

  IXMLTProcEventoList = interface(IXMLNodeCollection)
    ['{E340FE75-4305-4C5B-8683-54C92909BC85}']
    { Methods & Properties }
    function Add: IXMLTProcEvento;
    function Insert(const Index: Integer): IXMLTProcEvento;

    function Get_Item(Index: Integer): IXMLTProcEvento;
    property Items[Index: Integer]: IXMLTProcEvento read Get_Item; default;
  end;

{ IXMLTRetConsSitCTe }

  IXMLTRetConsSitCTe = interface(IXMLNode)
    ['{9DC1A87A-C176-4C6A-AE1F-52472D123A17}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_ProtCTe: IXMLTProtCTe;
    function Get_RetCancCTe: IXMLTRetCancCTe;
    function Get_ProcEventoCTe: IXMLTProcEventoList;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property VerAplic: UnicodeString read Get_VerAplic write Set_VerAplic;
    property CStat: UnicodeString read Get_CStat write Set_CStat;
    property XMotivo: UnicodeString read Get_XMotivo write Set_XMotivo;
    property CUF: UnicodeString read Get_CUF write Set_CUF;
    property ProtCTe: IXMLTProtCTe read Get_ProtCTe;
    property RetCancCTe: IXMLTRetCancCTe read Get_RetCancCTe;
    property ProcEventoCTe: IXMLTProcEventoList read Get_ProcEventoCTe;
  end;

{ Forward Decls }

  TXMLTConsSitCTe = class;
  TXMLTProtCTe = class;
  TXMLTProtCTeList = class;
  TXMLInfProt = class;
  TXMLSignatureType_ = class;
  TXMLSignedInfoType_ = class;
  TXMLCanonicalizationMethod_ = class;
  TXMLSignatureMethod_ = class;
  TXMLReferenceType_ = class;
  TXMLTransformsType_ = class;
  TXMLTransformType_ = class;
  TXMLDigestMethod_ = class;
  TXMLSignatureValueType_ = class;
  TXMLKeyInfoType_ = class;
  TXMLX509DataType_ = class;
  TXMLTConsReciCTe = class;
  TXMLTRetConsReciCTe = class;
  TXMLTCancCTe = class;
  TXMLInfCanc = class;
  TXMLTRetCancCTe = class;
  TXMLTProcCancCTe = class;
  TXMLTEvento = class;
  TXMLInfEvento = class;
  TXMLDetEvento = class;
  TXMLTRetEvento = class;
  TXMLTProcEvento = class;
  TXMLTProcEventoList = class;
  TXMLTRetConsSitCTe = class;

{ TXMLTConsSitCTe }

  TXMLTConsSitCTe = class(TXMLNode, IXMLTConsSitCTe)
  protected
    { IXMLTConsSitCTe }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_XServ: UnicodeString;
    function Get_ChCTe: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_XServ(Value: UnicodeString);
    procedure Set_ChCTe(Value: UnicodeString);
  end;

{ TXMLTProtCTe }

  TXMLTProtCTe = class(TXMLNode, IXMLTProtCTe)
  protected
    { IXMLTProtCTe }
    function Get_Versao: UnicodeString;
    function Get_InfProt: IXMLInfProt;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTProtCTeList }

  TXMLTProtCTeList = class(TXMLNodeCollection, IXMLTProtCTeList)
  protected
    { IXMLTProtCTeList }
    function Add: IXMLTProtCTe;
    function Insert(const Index: Integer): IXMLTProtCTe;

    function Get_Item(Index: Integer): IXMLTProtCTe;
  end;

{ TXMLInfProt }

  TXMLInfProt = class(TXMLNode, IXMLInfProt)
  protected
    { IXMLInfProt }
    function Get_Id: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_ChCTe: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_NProt: UnicodeString;
    function Get_DigVal: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_ChCTe(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_NProt(Value: UnicodeString);
    procedure Set_DigVal(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
  end;

{ TXMLSignatureType_ }

  TXMLSignatureType_ = class(TXMLNode, IXMLSignatureType_)
  protected
    { IXMLSignatureType_ }
    function Get_Id: UnicodeString;
    function Get_SignedInfo: IXMLSignedInfoType_;
    function Get_SignatureValue: IXMLSignatureValueType_;
    function Get_KeyInfo: IXMLKeyInfoType_;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSignedInfoType_ }

  TXMLSignedInfoType_ = class(TXMLNode, IXMLSignedInfoType_)
  protected
    { IXMLSignedInfoType_ }
    function Get_Id: UnicodeString;
    function Get_CanonicalizationMethod: IXMLCanonicalizationMethod_;
    function Get_SignatureMethod: IXMLSignatureMethod_;
    function Get_Reference: IXMLReferenceType_;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCanonicalizationMethod_ }

  TXMLCanonicalizationMethod_ = class(TXMLNode, IXMLCanonicalizationMethod_)
  protected
    { IXMLCanonicalizationMethod_ }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLSignatureMethod_ }

  TXMLSignatureMethod_ = class(TXMLNode, IXMLSignatureMethod_)
  protected
    { IXMLSignatureMethod_ }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLReferenceType_ }

  TXMLReferenceType_ = class(TXMLNode, IXMLReferenceType_)
  protected
    { IXMLReferenceType_ }
    function Get_Id: UnicodeString;
    function Get_URI: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_Transforms: IXMLTransformsType_;
    function Get_DigestMethod: IXMLDigestMethod_;
    function Get_DigestValue: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_URI(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_DigestValue(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransformsType_ }

  TXMLTransformsType_ = class(TXMLNodeCollection, IXMLTransformsType_)
  protected
    { IXMLTransformsType_ }
    function Get_Transform(Index: Integer): IXMLTransformType_;
    function Add: IXMLTransformType_;
    function Insert(const Index: Integer): IXMLTransformType_;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransformType_ }

  TXMLTransformType_ = class(TXMLNodeCollection, IXMLTransformType_)
  protected
    { IXMLTransformType_ }
    function Get_Algorithm: UnicodeString;
    function Get_XPath(Index: Integer): UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    function Add(const XPath: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDigestMethod_ }

  TXMLDigestMethod_ = class(TXMLNode, IXMLDigestMethod_)
  protected
    { IXMLDigestMethod_ }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLSignatureValueType_ }

  TXMLSignatureValueType_ = class(TXMLNode, IXMLSignatureValueType_)
  protected
    { IXMLSignatureValueType_ }
    function Get_Id: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
  end;

{ TXMLKeyInfoType_ }

  TXMLKeyInfoType_ = class(TXMLNode, IXMLKeyInfoType_)
  protected
    { IXMLKeyInfoType_ }
    function Get_Id: UnicodeString;
    function Get_X509Data: IXMLX509DataType_;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLX509DataType_ }

  TXMLX509DataType_ = class(TXMLNode, IXMLX509DataType_)
  protected
    { IXMLX509DataType_ }
    function Get_X509Certificate: UnicodeString;
    procedure Set_X509Certificate(Value: UnicodeString);
  end;

{ TXMLTConsReciCTe }

  TXMLTConsReciCTe = class(TXMLNode, IXMLTConsReciCTe)
  protected
    { IXMLTConsReciCTe }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_NRec: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_NRec(Value: UnicodeString);
  end;

{ TXMLTRetConsReciCTe }

  TXMLTRetConsReciCTe = class(TXMLNode, IXMLTRetConsReciCTe)
  private
    FProtCTe: IXMLTProtCTeList;
  protected
    { IXMLTRetConsReciCTe }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_NRec: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_ProtCTe: IXMLTProtCTeList;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_NRec(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTCancCTe }

  TXMLTCancCTe = class(TXMLNode, IXMLTCancCTe)
  protected
    { IXMLTCancCTe }
    function Get_Versao: UnicodeString;
    function Get_InfCanc: IXMLInfCanc;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfCanc }

  TXMLInfCanc = class(TXMLNode, IXMLInfCanc)
  protected
    { IXMLInfCanc }
    function Get_Id: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_XServ: UnicodeString;
    function Get_ChCTe: UnicodeString;
    function Get_NProt: UnicodeString;
    function Get_XJust: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_XServ(Value: UnicodeString);
    procedure Set_ChCTe(Value: UnicodeString);
    procedure Set_NProt(Value: UnicodeString);
    procedure Set_XJust(Value: UnicodeString);
  end;

{ TXMLTRetCancCTe }

  TXMLTRetCancCTe = class(TXMLNode, IXMLTRetCancCTe)
  protected
    { IXMLTRetCancCTe }
    function Get_Versao: UnicodeString;
    function Get_InfCanc: IXMLInfCanc;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTProcCancCTe }

  TXMLTProcCancCTe = class(TXMLNode, IXMLTProcCancCTe)
  protected
    { IXMLTProcCancCTe }
    function Get_Versao: UnicodeString;
    function Get_CancCTe: IXMLTCancCTe;
    function Get_RetCancCTe: IXMLTRetCancCTe;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTEvento }

  TXMLTEvento = class(TXMLNode, IXMLTEvento)
  protected
    { IXMLTEvento }
    function Get_Versao: UnicodeString;
    function Get_InfEvento: IXMLInfEvento;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfEvento }

  TXMLInfEvento = class(TXMLNode, IXMLInfEvento)
  protected
    { IXMLInfEvento }
    function Get_Id: UnicodeString;
    function Get_COrgao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_ChCTe: UnicodeString;
    function Get_DhEvento: UnicodeString;
    function Get_TpEvento: UnicodeString;
    function Get_NSeqEvento: UnicodeString;
    function Get_DetEvento: IXMLDetEvento;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_COrgao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_ChCTe(Value: UnicodeString);
    procedure Set_DhEvento(Value: UnicodeString);
    procedure Set_TpEvento(Value: UnicodeString);
    procedure Set_NSeqEvento(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDetEvento }

  TXMLDetEvento = class(TXMLNode, IXMLDetEvento)
  protected
    { IXMLDetEvento }
    function Get_VersaoEvento: UnicodeString;
    procedure Set_VersaoEvento(Value: UnicodeString);
  end;

{ TXMLTRetEvento }

  TXMLTRetEvento = class(TXMLNode, IXMLTRetEvento)
  protected
    { IXMLTRetEvento }
    function Get_Versao: UnicodeString;
    function Get_InfEvento: IXMLInfEvento;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTProcEvento }

  TXMLTProcEvento = class(TXMLNode, IXMLTProcEvento)
  protected
    { IXMLTProcEvento }
    function Get_Versao: UnicodeString;
    function Get_EventoCTe: IXMLTEvento;
    function Get_RetEventoCTe: IXMLTRetEvento;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTProcEventoList }

  TXMLTProcEventoList = class(TXMLNodeCollection, IXMLTProcEventoList)
  protected
    { IXMLTProcEventoList }
    function Add: IXMLTProcEvento;
    function Insert(const Index: Integer): IXMLTProcEvento;

    function Get_Item(Index: Integer): IXMLTProcEvento;
  end;

{ TXMLTRetConsSitCTe }

  TXMLTRetConsSitCTe = class(TXMLNode, IXMLTRetConsSitCTe)
  private
    FProcEventoCTe: IXMLTProcEventoList;
  protected
    { IXMLTRetConsSitCTe }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_ProtCTe: IXMLTProtCTe;
    function Get_RetCancCTe: IXMLTRetCancCTe;
    function Get_ProcEventoCTe: IXMLTProcEventoList;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ Global Functions }

function GetconsSitCTe(Doc: IXMLDocument): IXMLTConsSitCTe;
function LoadconsSitCTe(const FileName: string): IXMLTConsSitCTe;
function NewconsSitCTe: IXMLTConsSitCTe;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/cte';

implementation

{ Global Functions }

function GetconsSitCTe(Doc: IXMLDocument): IXMLTConsSitCTe;
begin
  Result := Doc.GetDocBinding('consSitCTe', TXMLTConsSitCTe, TargetNamespace) as IXMLTConsSitCTe;
end;

function LoadconsSitCTe(const FileName: string): IXMLTConsSitCTe;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('consSitCTe', TXMLTConsSitCTe, TargetNamespace) as IXMLTConsSitCTe;
end;

function NewconsSitCTe: IXMLTConsSitCTe;
begin
  Result := NewXMLDocument.GetDocBinding('consSitCTe', TXMLTConsSitCTe, TargetNamespace) as IXMLTConsSitCTe;
end;

{ TXMLTConsSitCTe }

function TXMLTConsSitCTe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTConsSitCTe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTConsSitCTe.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTConsSitCTe.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTConsSitCTe.Get_XServ: UnicodeString;
begin
  Result := ChildNodes['xServ'].Text;
end;

procedure TXMLTConsSitCTe.Set_XServ(Value: UnicodeString);
begin
  ChildNodes['xServ'].NodeValue := Value;
end;

function TXMLTConsSitCTe.Get_ChCTe: UnicodeString;
begin
  Result := ChildNodes['chCTe'].Text;
end;

procedure TXMLTConsSitCTe.Set_ChCTe(Value: UnicodeString);
begin
  ChildNodes['chCTe'].NodeValue := Value;
end;

{ TXMLTProtCTe }

procedure TXMLTProtCTe.AfterConstruction;
begin
  RegisterChildNode('infProt', TXMLInfProt);
  RegisterChildNode('Signature', TXMLSignatureType_);
  inherited;
end;

function TXMLTProtCTe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTProtCTe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTProtCTe.Get_InfProt: IXMLInfProt;
begin
  Result := ChildNodes['infProt'] as IXMLInfProt;
end;

function TXMLTProtCTe.Get_Signature: IXMLSignatureType_;
begin
  Result := ChildNodes['Signature'] as IXMLSignatureType_;
end;

{ TXMLTProtCTeList }

function TXMLTProtCTeList.Add: IXMLTProtCTe;
begin
  Result := AddItem(-1) as IXMLTProtCTe;
end;

function TXMLTProtCTeList.Insert(const Index: Integer): IXMLTProtCTe;
begin
  Result := AddItem(Index) as IXMLTProtCTe;
end;

function TXMLTProtCTeList.Get_Item(Index: Integer): IXMLTProtCTe;
begin
  Result := List[Index] as IXMLTProtCTe;
end;

{ TXMLInfProt }

function TXMLInfProt.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLInfProt.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLInfProt.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLInfProt.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLInfProt.Get_VerAplic: UnicodeString;
begin
  Result := ChildNodes['verAplic'].Text;
end;

procedure TXMLInfProt.Set_VerAplic(Value: UnicodeString);
begin
  ChildNodes['verAplic'].NodeValue := Value;
end;

function TXMLInfProt.Get_ChCTe: UnicodeString;
begin
  Result := ChildNodes['chCTe'].Text;
end;

procedure TXMLInfProt.Set_ChCTe(Value: UnicodeString);
begin
  ChildNodes['chCTe'].NodeValue := Value;
end;

function TXMLInfProt.Get_DhRecbto: UnicodeString;
begin
  Result := ChildNodes['dhRecbto'].Text;
end;

procedure TXMLInfProt.Set_DhRecbto(Value: UnicodeString);
begin
  ChildNodes['dhRecbto'].NodeValue := Value;
end;

function TXMLInfProt.Get_NProt: UnicodeString;
begin
  Result := ChildNodes['nProt'].Text;
end;

procedure TXMLInfProt.Set_NProt(Value: UnicodeString);
begin
  ChildNodes['nProt'].NodeValue := Value;
end;

function TXMLInfProt.Get_DigVal: UnicodeString;
begin
  Result := ChildNodes['digVal'].Text;
end;

procedure TXMLInfProt.Set_DigVal(Value: UnicodeString);
begin
  ChildNodes['digVal'].NodeValue := Value;
end;

function TXMLInfProt.Get_CStat: UnicodeString;
begin
  Result := ChildNodes['cStat'].Text;
end;

procedure TXMLInfProt.Set_CStat(Value: UnicodeString);
begin
  ChildNodes['cStat'].NodeValue := Value;
end;

function TXMLInfProt.Get_XMotivo: UnicodeString;
begin
  Result := ChildNodes['xMotivo'].Text;
end;

procedure TXMLInfProt.Set_XMotivo(Value: UnicodeString);
begin
  ChildNodes['xMotivo'].NodeValue := Value;
end;

{ TXMLSignatureType_ }

procedure TXMLSignatureType_.AfterConstruction;
begin
  RegisterChildNode('SignedInfo', TXMLSignedInfoType_);
  RegisterChildNode('SignatureValue', TXMLSignatureValueType_);
  RegisterChildNode('KeyInfo', TXMLKeyInfoType_);
  inherited;
end;

function TXMLSignatureType_.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignatureType_.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLSignatureType_.Get_SignedInfo: IXMLSignedInfoType_;
begin
  Result := ChildNodes['SignedInfo'] as IXMLSignedInfoType_;
end;

function TXMLSignatureType_.Get_SignatureValue: IXMLSignatureValueType_;
begin
  Result := ChildNodes['SignatureValue'] as IXMLSignatureValueType_;
end;

function TXMLSignatureType_.Get_KeyInfo: IXMLKeyInfoType_;
begin
  Result := ChildNodes['KeyInfo'] as IXMLKeyInfoType_;
end;

{ TXMLSignedInfoType_ }

procedure TXMLSignedInfoType_.AfterConstruction;
begin
  RegisterChildNode('CanonicalizationMethod', TXMLCanonicalizationMethod_);
  RegisterChildNode('SignatureMethod', TXMLSignatureMethod_);
  RegisterChildNode('Reference', TXMLReferenceType_);
  inherited;
end;

function TXMLSignedInfoType_.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignedInfoType_.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLSignedInfoType_.Get_CanonicalizationMethod: IXMLCanonicalizationMethod_;
begin
  Result := ChildNodes['CanonicalizationMethod'] as IXMLCanonicalizationMethod_;
end;

function TXMLSignedInfoType_.Get_SignatureMethod: IXMLSignatureMethod_;
begin
  Result := ChildNodes['SignatureMethod'] as IXMLSignatureMethod_;
end;

function TXMLSignedInfoType_.Get_Reference: IXMLReferenceType_;
begin
  Result := ChildNodes['Reference'] as IXMLReferenceType_;
end;

{ TXMLCanonicalizationMethod_ }

function TXMLCanonicalizationMethod_.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLCanonicalizationMethod_.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLSignatureMethod_ }

function TXMLSignatureMethod_.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLSignatureMethod_.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLReferenceType_ }

procedure TXMLReferenceType_.AfterConstruction;
begin
  RegisterChildNode('Transforms', TXMLTransformsType_);
  RegisterChildNode('DigestMethod', TXMLDigestMethod_);
  inherited;
end;

function TXMLReferenceType_.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLReferenceType_.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLReferenceType_.Get_URI: UnicodeString;
begin
  Result := AttributeNodes['URI'].Text;
end;

procedure TXMLReferenceType_.Set_URI(Value: UnicodeString);
begin
  SetAttribute('URI', Value);
end;

function TXMLReferenceType_.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLReferenceType_.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

function TXMLReferenceType_.Get_Transforms: IXMLTransformsType_;
begin
  Result := ChildNodes['Transforms'] as IXMLTransformsType_;
end;

function TXMLReferenceType_.Get_DigestMethod: IXMLDigestMethod_;
begin
  Result := ChildNodes['DigestMethod'] as IXMLDigestMethod_;
end;

function TXMLReferenceType_.Get_DigestValue: UnicodeString;
begin
  Result := ChildNodes['DigestValue'].Text;
end;

procedure TXMLReferenceType_.Set_DigestValue(Value: UnicodeString);
begin
  ChildNodes['DigestValue'].NodeValue := Value;
end;

{ TXMLTransformsType_ }

procedure TXMLTransformsType_.AfterConstruction;
begin
  RegisterChildNode('Transform', TXMLTransformType_);
  ItemTag := 'Transform';
  ItemInterface := IXMLTransformType_;
  inherited;
end;

function TXMLTransformsType_.Get_Transform(Index: Integer): IXMLTransformType_;
begin
  Result := List[Index] as IXMLTransformType_;
end;

function TXMLTransformsType_.Add: IXMLTransformType_;
begin
  Result := AddItem(-1) as IXMLTransformType_;
end;

function TXMLTransformsType_.Insert(const Index: Integer): IXMLTransformType_;
begin
  Result := AddItem(Index) as IXMLTransformType_;
end;

{ TXMLTransformType_ }

procedure TXMLTransformType_.AfterConstruction;
begin
  ItemTag := 'XPath';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLTransformType_.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLTransformType_.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

function TXMLTransformType_.Get_XPath(Index: Integer): UnicodeString;
begin
  Result := List[Index].Text;
end;

function TXMLTransformType_.Add(const XPath: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := XPath;
end;

function TXMLTransformType_.Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := XPath;
end;

{ TXMLDigestMethod_ }

function TXMLDigestMethod_.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLDigestMethod_.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLSignatureValueType_ }

function TXMLSignatureValueType_.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignatureValueType_.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

{ TXMLKeyInfoType_ }

procedure TXMLKeyInfoType_.AfterConstruction;
begin
  RegisterChildNode('X509Data', TXMLX509DataType_);
  inherited;
end;

function TXMLKeyInfoType_.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLKeyInfoType_.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLKeyInfoType_.Get_X509Data: IXMLX509DataType_;
begin
  Result := ChildNodes['X509Data'] as IXMLX509DataType_;
end;

{ TXMLX509DataType_ }

function TXMLX509DataType_.Get_X509Certificate: UnicodeString;
begin
  Result := ChildNodes['X509Certificate'].Text;
end;

procedure TXMLX509DataType_.Set_X509Certificate(Value: UnicodeString);
begin
  ChildNodes['X509Certificate'].NodeValue := Value;
end;

{ TXMLTConsReciCTe }

function TXMLTConsReciCTe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTConsReciCTe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTConsReciCTe.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTConsReciCTe.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTConsReciCTe.Get_NRec: UnicodeString;
begin
  Result := ChildNodes['nRec'].Text;
end;

procedure TXMLTConsReciCTe.Set_NRec(Value: UnicodeString);
begin
  ChildNodes['nRec'].NodeValue := Value;
end;

{ TXMLTRetConsReciCTe }

procedure TXMLTRetConsReciCTe.AfterConstruction;
begin
  RegisterChildNode('protCTe', TXMLTProtCTe);
  FProtCTe := CreateCollection(TXMLTProtCTeList, IXMLTProtCTe, 'protCTe') as IXMLTProtCTeList;
  inherited;
end;

function TXMLTRetConsReciCTe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetConsReciCTe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetConsReciCTe.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTRetConsReciCTe.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTRetConsReciCTe.Get_VerAplic: UnicodeString;
begin
  Result := ChildNodes['verAplic'].Text;
end;

procedure TXMLTRetConsReciCTe.Set_VerAplic(Value: UnicodeString);
begin
  ChildNodes['verAplic'].NodeValue := Value;
end;

function TXMLTRetConsReciCTe.Get_NRec: UnicodeString;
begin
  Result := ChildNodes['nRec'].Text;
end;

procedure TXMLTRetConsReciCTe.Set_NRec(Value: UnicodeString);
begin
  ChildNodes['nRec'].NodeValue := Value;
end;

function TXMLTRetConsReciCTe.Get_CStat: UnicodeString;
begin
  Result := ChildNodes['cStat'].Text;
end;

procedure TXMLTRetConsReciCTe.Set_CStat(Value: UnicodeString);
begin
  ChildNodes['cStat'].NodeValue := Value;
end;

function TXMLTRetConsReciCTe.Get_XMotivo: UnicodeString;
begin
  Result := ChildNodes['xMotivo'].Text;
end;

procedure TXMLTRetConsReciCTe.Set_XMotivo(Value: UnicodeString);
begin
  ChildNodes['xMotivo'].NodeValue := Value;
end;

function TXMLTRetConsReciCTe.Get_CUF: UnicodeString;
begin
  Result := ChildNodes['cUF'].Text;
end;

procedure TXMLTRetConsReciCTe.Set_CUF(Value: UnicodeString);
begin
  ChildNodes['cUF'].NodeValue := Value;
end;

function TXMLTRetConsReciCTe.Get_ProtCTe: IXMLTProtCTeList;
begin
  Result := FProtCTe;
end;

{ TXMLTCancCTe }

procedure TXMLTCancCTe.AfterConstruction;
begin
  RegisterChildNode('infCanc', TXMLInfCanc);
  RegisterChildNode('Signature', TXMLSignatureType_);
  inherited;
end;

function TXMLTCancCTe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTCancCTe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTCancCTe.Get_InfCanc: IXMLInfCanc;
begin
  Result := ChildNodes['infCanc'] as IXMLInfCanc;
end;

function TXMLTCancCTe.Get_Signature: IXMLSignatureType_;
begin
  Result := ChildNodes['Signature'] as IXMLSignatureType_;
end;

{ TXMLInfCanc }

function TXMLInfCanc.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLInfCanc.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLInfCanc.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLInfCanc.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLInfCanc.Get_XServ: UnicodeString;
begin
  Result := ChildNodes['xServ'].Text;
end;

procedure TXMLInfCanc.Set_XServ(Value: UnicodeString);
begin
  ChildNodes['xServ'].NodeValue := Value;
end;

function TXMLInfCanc.Get_ChCTe: UnicodeString;
begin
  Result := ChildNodes['chCTe'].Text;
end;

procedure TXMLInfCanc.Set_ChCTe(Value: UnicodeString);
begin
  ChildNodes['chCTe'].NodeValue := Value;
end;

function TXMLInfCanc.Get_NProt: UnicodeString;
begin
  Result := ChildNodes['nProt'].Text;
end;

procedure TXMLInfCanc.Set_NProt(Value: UnicodeString);
begin
  ChildNodes['nProt'].NodeValue := Value;
end;

function TXMLInfCanc.Get_XJust: UnicodeString;
begin
  Result := ChildNodes['xJust'].Text;
end;

procedure TXMLInfCanc.Set_XJust(Value: UnicodeString);
begin
  ChildNodes['xJust'].NodeValue := Value;
end;

{ TXMLTRetCancCTe }

procedure TXMLTRetCancCTe.AfterConstruction;
begin
  RegisterChildNode('infCanc', TXMLInfCanc);
  RegisterChildNode('Signature', TXMLSignatureType_);
  inherited;
end;

function TXMLTRetCancCTe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetCancCTe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetCancCTe.Get_InfCanc: IXMLInfCanc;
begin
  Result := ChildNodes['infCanc'] as IXMLInfCanc;
end;

function TXMLTRetCancCTe.Get_Signature: IXMLSignatureType_;
begin
  Result := ChildNodes['Signature'] as IXMLSignatureType_;
end;

{ TXMLTProcCancCTe }

procedure TXMLTProcCancCTe.AfterConstruction;
begin
  RegisterChildNode('cancCTe', TXMLTCancCTe);
  RegisterChildNode('retCancCTe', TXMLTRetCancCTe);
  inherited;
end;

function TXMLTProcCancCTe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTProcCancCTe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTProcCancCTe.Get_CancCTe: IXMLTCancCTe;
begin
  Result := ChildNodes['cancCTe'] as IXMLTCancCTe;
end;

function TXMLTProcCancCTe.Get_RetCancCTe: IXMLTRetCancCTe;
begin
  Result := ChildNodes['retCancCTe'] as IXMLTRetCancCTe;
end;

{ TXMLTEvento }

procedure TXMLTEvento.AfterConstruction;
begin
  RegisterChildNode('infEvento', TXMLInfEvento);
  RegisterChildNode('Signature', TXMLSignatureType_);
  inherited;
end;

function TXMLTEvento.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTEvento.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTEvento.Get_InfEvento: IXMLInfEvento;
begin
  Result := ChildNodes['infEvento'] as IXMLInfEvento;
end;

function TXMLTEvento.Get_Signature: IXMLSignatureType_;
begin
  Result := ChildNodes['Signature'] as IXMLSignatureType_;
end;

{ TXMLInfEvento }

procedure TXMLInfEvento.AfterConstruction;
begin
  RegisterChildNode('detEvento', TXMLDetEvento);
  inherited;
end;

function TXMLInfEvento.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLInfEvento.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLInfEvento.Get_COrgao: UnicodeString;
begin
  Result := ChildNodes['cOrgao'].Text;
end;

procedure TXMLInfEvento.Set_COrgao(Value: UnicodeString);
begin
  ChildNodes['cOrgao'].NodeValue := Value;
end;

function TXMLInfEvento.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLInfEvento.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLInfEvento.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLInfEvento.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLInfEvento.Get_ChCTe: UnicodeString;
begin
  Result := ChildNodes['chCTe'].Text;
end;

procedure TXMLInfEvento.Set_ChCTe(Value: UnicodeString);
begin
  ChildNodes['chCTe'].NodeValue := Value;
end;

function TXMLInfEvento.Get_DhEvento: UnicodeString;
begin
  Result := ChildNodes['dhEvento'].Text;
end;

procedure TXMLInfEvento.Set_DhEvento(Value: UnicodeString);
begin
  ChildNodes['dhEvento'].NodeValue := Value;
end;

function TXMLInfEvento.Get_TpEvento: UnicodeString;
begin
  Result := ChildNodes['tpEvento'].Text;
end;

procedure TXMLInfEvento.Set_TpEvento(Value: UnicodeString);
begin
  ChildNodes['tpEvento'].NodeValue := Value;
end;

function TXMLInfEvento.Get_NSeqEvento: UnicodeString;
begin
  Result := ChildNodes['nSeqEvento'].Text;
end;

procedure TXMLInfEvento.Set_NSeqEvento(Value: UnicodeString);
begin
  ChildNodes['nSeqEvento'].NodeValue := Value;
end;

function TXMLInfEvento.Get_DetEvento: IXMLDetEvento;
begin
  Result := ChildNodes['detEvento'] as IXMLDetEvento;
end;

{ TXMLDetEvento }

function TXMLDetEvento.Get_VersaoEvento: UnicodeString;
begin
  Result := AttributeNodes['versaoEvento'].Text;
end;

procedure TXMLDetEvento.Set_VersaoEvento(Value: UnicodeString);
begin
  SetAttribute('versaoEvento', Value);
end;

{ TXMLTRetEvento }

procedure TXMLTRetEvento.AfterConstruction;
begin
  RegisterChildNode('infEvento', TXMLInfEvento);
  RegisterChildNode('Signature', TXMLSignatureType_);
  inherited;
end;

function TXMLTRetEvento.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetEvento.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetEvento.Get_InfEvento: IXMLInfEvento;
begin
  Result := ChildNodes['infEvento'] as IXMLInfEvento;
end;

function TXMLTRetEvento.Get_Signature: IXMLSignatureType_;
begin
  Result := ChildNodes['Signature'] as IXMLSignatureType_;
end;

{ TXMLTProcEvento }

procedure TXMLTProcEvento.AfterConstruction;
begin
  RegisterChildNode('eventoCTe', TXMLTEvento);
  RegisterChildNode('retEventoCTe', TXMLTRetEvento);
  inherited;
end;

function TXMLTProcEvento.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTProcEvento.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTProcEvento.Get_EventoCTe: IXMLTEvento;
begin
  Result := ChildNodes['eventoCTe'] as IXMLTEvento;
end;

function TXMLTProcEvento.Get_RetEventoCTe: IXMLTRetEvento;
begin
  Result := ChildNodes['retEventoCTe'] as IXMLTRetEvento;
end;

{ TXMLTProcEventoList }

function TXMLTProcEventoList.Add: IXMLTProcEvento;
begin
  Result := AddItem(-1) as IXMLTProcEvento;
end;

function TXMLTProcEventoList.Insert(const Index: Integer): IXMLTProcEvento;
begin
  Result := AddItem(Index) as IXMLTProcEvento;
end;

function TXMLTProcEventoList.Get_Item(Index: Integer): IXMLTProcEvento;
begin
  Result := List[Index] as IXMLTProcEvento;
end;

{ TXMLTRetConsSitCTe }

procedure TXMLTRetConsSitCTe.AfterConstruction;
begin
  RegisterChildNode('protCTe', TXMLTProtCTe);
  RegisterChildNode('retCancCTe', TXMLTRetCancCTe);
  RegisterChildNode('procEventoCTe', TXMLTProcEvento);
  FProcEventoCTe := CreateCollection(TXMLTProcEventoList, IXMLTProcEvento, 'procEventoCTe') as IXMLTProcEventoList;
  inherited;
end;

function TXMLTRetConsSitCTe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetConsSitCTe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetConsSitCTe.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTRetConsSitCTe.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTRetConsSitCTe.Get_VerAplic: UnicodeString;
begin
  Result := ChildNodes['verAplic'].Text;
end;

procedure TXMLTRetConsSitCTe.Set_VerAplic(Value: UnicodeString);
begin
  ChildNodes['verAplic'].NodeValue := Value;
end;

function TXMLTRetConsSitCTe.Get_CStat: UnicodeString;
begin
  Result := ChildNodes['cStat'].Text;
end;

procedure TXMLTRetConsSitCTe.Set_CStat(Value: UnicodeString);
begin
  ChildNodes['cStat'].NodeValue := Value;
end;

function TXMLTRetConsSitCTe.Get_XMotivo: UnicodeString;
begin
  Result := ChildNodes['xMotivo'].Text;
end;

procedure TXMLTRetConsSitCTe.Set_XMotivo(Value: UnicodeString);
begin
  ChildNodes['xMotivo'].NodeValue := Value;
end;

function TXMLTRetConsSitCTe.Get_CUF: UnicodeString;
begin
  Result := ChildNodes['cUF'].Text;
end;

procedure TXMLTRetConsSitCTe.Set_CUF(Value: UnicodeString);
begin
  ChildNodes['cUF'].NodeValue := Value;
end;

function TXMLTRetConsSitCTe.Get_ProtCTe: IXMLTProtCTe;
begin
  Result := ChildNodes['protCTe'] as IXMLTProtCTe;
end;

function TXMLTRetConsSitCTe.Get_RetCancCTe: IXMLTRetCancCTe;
begin
  Result := ChildNodes['retCancCTe'] as IXMLTRetCancCTe;
end;

function TXMLTRetConsSitCTe.Get_ProcEventoCTe: IXMLTProcEventoList;
begin
  Result := FProcEventoCTe;
end;

end.