unit CTeEveRCan;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkRadioGroup, dmkMemo, UnCTe_PF, UnDmkEnums, UnXXe_PF;

type
  TFmCTeEveRCan = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Painel1: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrJust: TmySQLQuery;
    QrJustCodigo: TIntegerField;
    QrJustNome: TWideStringField;
    QrJustCodUsu: TIntegerField;
    QrJustAplicacao: TIntegerField;
    DsJust: TDataSource;
    GroupBox2: TGroupBox;
    PnJustificativa: TPanel;
    Label7: TLabel;
    EdJust: TdmkEditCB;
    CBJust: TdmkDBLookupComboBox;
    Panel2: TPanel;
    Label1: TLabel;
    Edide_serie: TdmkEdit;
    Label2: TLabel;
    Edide_nCT: TdmkEdit;
    Label3: TLabel;
    EdnProt: TdmkEdit;
    Memo1: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdJustChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCTeEveRCan: TFmCTeEveRCan;

implementation

uses UnMyObjects, Module, UnCTe_Tabs, CTeEveRCab, ModuleGeral, UMySQLModule,
  CTeEveGeraXMLCan_0200a, ModuleCTe_0000;

{$R *.DFM}

procedure TFmCTeEveRCan.BtOKClick(Sender: TObject);
var
  Destino, Dir, nProt, xJust: String;
  Status, FatID, FatNum, Empresa, Controle, Conta, nJust: Integer;
  // XML
  XML_Eve: String;
begin
  if not DmCTe_0000.ObtemDirXML(CTE_EXT_EVE_ENV_CAN_XML, Dir, True) then
    Exit;
  if ImgTipo.SQLType = stIns then
  begin
    if not FmCTeEveRCab.IncluiCTeEveRCab(0, evexxe110111Can, FatID, FatNum, Empresa,
    Controle) then
    begin
      Geral.MensagemBox('ERRO ao incluir o cabe�alho gen�rico de evento!',
      'ERRO', MB_OK+MB_ICONERROR);
      Exit;
    end;
    Conta := DModG.BuscaProximoCodigoInt('ctectrl', 'cteevercan', '', 0);
  end else begin
    FatID := FmCTeEveRCab.QrCTeEveRCabFatID.Value;
    FatNum := FmCTeEveRCab.QrCTeEveRCabFatNum.Value;
    Empresa := FmCTeEveRCab.QrCTeEveRCabEmpresa.Value;
    Controle := FmCTeEveRCab.QrCTeEveRCabControle.Value;
    Conta := FmCTeEveRCab.QrCTeEveRCanConta.Value;
  end;
  //
  nProt := EdnProt.Text;
  nJust := EdJust.ValueVariant;
  xJust :=  Trim(XXe_PF.ValidaTexto_XML(QrJustNome.Value, 'eve.can.xjust', 'eve.can.xjust'));
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'cteevercan', False, [
  'nProt', 'nJust', 'xJust'], [
  'FatID', 'FatNum', 'Empresa', 'Controle', 'Conta'], [
  nProt, nJust, xJust], [
  FatID, FatNum, Empresa, Controle, Conta], True) then
  begin
    if UnCTeEveGeraXMLCan_0200a.CriarDocumentoEveCan(FatID, FatNum, Empresa,
    FmCTeEveRCab.QrERCId.Value, FmCTeEveRCab.QrERCcOrgao.Value,
    FmCTeEveRCab.QrERCtpAmb.Value, FmCTeEveRCab.QrERCTipoEnt.Value,
    FmCTeEveRCab.QrERCCNPJ.Value, FmCTeEveRCab.QrERCCPF.Value,
    FmCTeEveRCab.QrERCchCTe.Value, FmCTeEveRCab.QrERCdhEvento.Value,
    FmCTeEveRCab.QrERCTZD_UTC.Value, FmCTeEveRCab.QrERCtpEvento.Value,
    FmCTeEveRCab.QrERCnSeqEvento.Value, FmCTeEveRCab.QrERCverEvento.Value,
    FmCTeEveRCab.QrERCversao.Value, FmCTeEveRCab.QrERCdescEvento.Value,
    nProt, xJust,
    XML_Eve, LaAviso1, LaAviso2) then
    begin
      Destino := DmCTe_0000.SalvaXML(CTE_EXT_EVE_ENV_CAN_XML,
      FmCTeEveRCab.QrERCId.Value, XML_Eve, Memo1, True);
      if FileExists(Destino) then
      begin
        Status := Integer(ctemystatusCTeAssinada);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cteevercab', False, [
        'XML_Eve', 'Status'], [
        'FatID', 'FatNum', 'Empresa', 'Controle'], [
        Geral.WideStringToSQLString(XML_Eve), Status], [
        FatID, FatNum, Empresa, Controle], True) then
        begin
          FmCTeEveRCab.ReopenCTeEveRCab(Controle);
          //
          Empresa := FmCTeEveRCab.QrCTeEveRCabEmpresa.Value;
          CTe_PF.MostraFormStepsCTe_Cancelamento(Empresa,
          Edide_Serie.ValueVariant, Edide_nCT.ValueVariant,
          FmCTeEveRCab.QrERCnSeqEvento.Value, EdJust.ValueVariant,
          CTe_AllModelos, EdnProt.ValueVariant, FmCTeEveRCab.QrERCchCTe.Value,
          XML_Eve, FmCTeEveRCab.QrA);
        end;
      end;
    end;
    //
    FmCTeEveRCab.ReopenCTeEveRCan(0);
    Close;
  end;
end;

procedure TFmCTeEveRCan.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCTeEveRCan.EdJustChange(Sender: TObject);
begin
  BtOK.Enabled := EdJust.ValueVariant;
end;

procedure TFmCTeEveRCan.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCTeEveRCan.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  UMyMod.AbreQuery(QrJust, Dmod.MyDB);
end;

procedure TFmCTeEveRCan.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
