object FmCTeEveREPEC: TFmCTeEveREPEC
  Left = 339
  Top = 185
  Caption = 'CTe-EVENT-005 :: Emiss'#227'o em Conting'#234'ncia (EPEC)'
  ClientHeight = 422
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 413
        Height = 32
        Caption = 'Emiss'#227'o em Conting'#234'ncia (EPEC)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 413
        Height = 32
        Caption = 'Emiss'#227'o em Conting'#234'ncia (EPEC)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 413
        Height = 32
        Caption = 'Emiss'#227'o em Conting'#234'ncia (EPEC)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Painel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 260
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 260
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 73
        Align = alTop
        Caption = ' Dados do CT-e: '
        Enabled = False
        TabOrder = 0
        object Panel2: TPanel
          Left = 2
          Top = 15
          Width = 135
          Height = 56
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 8
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
          end
          object Label2: TLabel
            Left = 48
            Top = 8
            Width = 32
            Height = 13
            Caption = 'N'#186' CT:'
          end
          object Edide_serie: TdmkEdit
            Left = 8
            Top = 24
            Width = 37
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object Edide_nCT: TdmkEdit
            Left = 48
            Top = 24
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object Memo1: TMemo
          Left = 137
          Top = 15
          Width = 645
          Height = 56
          Align = alClient
          ReadOnly = True
          TabOrder = 1
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 73
        Width = 784
        Height = 187
        Align = alClient
        Caption = ' Cancelamento: '
        TabOrder = 1
        object PnJustificativa: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 170
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label7: TLabel
            Left = 8
            Top = 4
            Width = 169
            Height = 13
            Caption = 'Justificativa (m'#237'nimo 15 caracteres):'
          end
          object Label13: TLabel
            Left = 8
            Top = 44
            Width = 75
            Height = 13
            Caption = 'Valor tot. carga:'
          end
          object Label22: TLabel
            Left = 92
            Top = 44
            Width = 80
            Height = 13
            Caption = 'V. tot.prest.serv.:'
          end
          object Label23: TLabel
            Left = 176
            Top = 44
            Width = 56
            Height = 13
            Caption = 'Valor ICMS:'
          end
          object Label17: TLabel
            Left = 261
            Top = 44
            Width = 30
            Height = 13
            Caption = 'UF ini:'
          end
          object Label18: TLabel
            Left = 301
            Top = 44
            Width = 33
            Height = 13
            Caption = 'UF fim:'
          end
          object Label10: TLabel
            Left = 340
            Top = 44
            Width = 76
            Height = 13
            Caption = 'Outro (tomador):'
          end
          object SbToma4: TSpeedButton
            Left = 747
            Top = 60
            Width = 21
            Height = 21
            Caption = '...'
          end
          object EdJust: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdJustChange
            DBLookupComboBox = CBJust
            IgnoraDBLookupComboBox = False
          end
          object CBJust: TdmkDBLookupComboBox
            Left = 64
            Top = 20
            Width = 705
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsJust
            TabOrder = 1
            dmkEditCB = EdJust
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdvCarga: TdmkEdit
            Left = 8
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'vCarga'
            UpdCampo = 'vCarga'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdvTPrest: TdmkEdit
            Left = 92
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'vTPrest'
            UpdCampo = 'vTPrest'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdvICMS: TdmkEdit
            Left = 176
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'vICMS'
            UpdCampo = 'vICMS'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdUFIni: TdmkEdit
            Left = 261
            Top = 60
            Width = 36
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtUF
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'UFIni'
            UpdCampo = 'UFIni'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdUFFim: TdmkEdit
            Left = 301
            Top = 60
            Width = 36
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtUF
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'UFFim'
            UpdCampo = 'UFFim'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdToma4: TdmkEditCB
            Left = 340
            Top = 60
            Width = 52
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Toma4'
            UpdCampo = 'Toma4'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBToma4
            IgnoraDBLookupComboBox = False
          end
          object CBToma4: TdmkDBLookupComboBox
            Left = 395
            Top = 60
            Width = 350
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENT'
            ListSource = DsToma4s
            TabOrder = 8
            dmkEditCB = EdToma4
            QryCampo = 'Toma4'
            UpdType = utNil
            LocF7SQLMasc = '$#'
          end
          object RGModal: TdmkRadioGroup
            Left = 8
            Top = 88
            Width = 285
            Height = 73
            Caption = ' Modal: '
            Columns = 3
            ItemIndex = 1
            Items.Strings = (
              '00-Indefinido'
              '01-Rodovi'#225'rio'
              '02-A'#233'reo'
              '03-Aquavi'#225'rio'
              '04-Ferrovi'#225'rio'
              '05-Dutovi'#225'rio'
              '06-Multimodal')
            TabOrder = 9
            QryCampo = 'Modal'
            UpdCampo = 'Modal'
            UpdType = utYes
            OldValor = 0
          end
          object GroupBox3: TGroupBox
            Left = 484
            Top = 88
            Width = 285
            Height = 73
            Caption = ' Dados do tomador selecionado: '
            TabOrder = 11
            object Panel5: TPanel
              Left = 2
              Top = 15
              Width = 281
              Height = 56
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label4: TLabel
                Left = 8
                Top = 8
                Width = 17
                Height = 13
                Caption = 'UF:'
                FocusControl = EdToma04_UF
              end
              object Label5: TLabel
                Left = 44
                Top = 8
                Width = 61
                Height = 13
                Caption = 'CNPJ / CPF:'
                FocusControl = EdToma04_CNPJ_CPF
              end
              object Label6: TLabel
                Left = 160
                Top = 8
                Width = 13
                Height = 13
                Caption = 'IE:'
                FocusControl = EdToma04_IE
              end
              object EdToma04_UF: TDBEdit
                Left = 8
                Top = 24
                Width = 30
                Height = 21
                DataField = 'NOMEUF'
                DataSource = DsToma4s
                TabOrder = 0
              end
              object EdToma04_CNPJ_CPF: TDBEdit
                Left = 44
                Top = 24
                Width = 112
                Height = 21
                DataField = 'CNPJ_CPF'
                DataSource = DsToma4s
                TabOrder = 1
              end
              object EdToma04_IE: TDBEdit
                Left = 160
                Top = 24
                Width = 112
                Height = 21
                DataField = 'IE'
                DataSource = DsToma4s
                TabOrder = 2
              end
            end
          end
          object RGToma04_toma: TdmkRadioGroup
            Left = 295
            Top = 88
            Width = 186
            Height = 73
            Caption = ' Tomador do servi'#231'o: '
            Columns = 2
            Items.Strings = (
              '0-Remetente'
              '1-Expedidor'
              '2-Recebedor'
              '3-Destinat'#225'rio'
              '4-Outros')
            TabOrder = 10
            QryCampo = 'Toma'
            UpdCampo = 'Toma'
            UpdType = utYes
            OldValor = 0
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 308
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 352
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrJust: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctejust'
      'WHERE 4 & Aplicacao > 0'
      'ORDER BY Nome')
    Left = 288
    Top = 68
    object QrJustCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrJustNome: TWideStringField
      FieldName = 'Nome'
      Size = 240
    end
    object QrJustCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrJustAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
  end
  object DsJust: TDataSource
    DataSet = QrJust
    Left = 288
    Top = 116
  end
  object QrToma4s: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu, ent.indIEDest, ent.Tipo,'
      'ent.IE,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      'ufs.Nome NOMEUF'
      'FROM entidades ent'
      'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)'
      'WHERE ent.Cliente1="V"'
      'AND ent.Ativo=1'
      'ORDER BY NOMEENT')
    Left = 344
    Top = 68
    object QrToma4sCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrToma4sNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrToma4sCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrToma4sNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrToma4sCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrToma4sIE: TWideStringField
      FieldName = 'IE'
    end
    object QrToma4sindIEDest: TIntegerField
      FieldName = 'indIEDest'
    end
    object QrToma4sTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrToma4sCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
  end
  object DsToma4s: TDataSource
    DataSet = QrToma4s
    Left = 344
    Top = 116
  end
end
