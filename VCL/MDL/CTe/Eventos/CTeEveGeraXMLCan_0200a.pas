unit CTeEveGeraXMLCan_0200a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, UnMLAGeral, Variants, Math,
  IBCustomDataSet, IBQuery, Clipbrd, DB, DBClient, dmkGeral, UrlMon,
  InvokeRegistry, SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, JwaWinCrypt,
  WinInet, XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB, UnDmkProcFunc,
  UnCTe_PF, UnXXe_PF,
  // Cancelamento
  evCancCTe_v200, eventoCTe_v200;

const
 { TODO : Colocar aqui os stat da Can }
{
  CTe_AllModelos      = '55';
  CTe_CodAutorizaTxt  = '100';
  CTe_CodCanceladTxt  = '101';
  CTe_CodInutilizTxt  = '102';
  CTe_CodDenegadoTxt  = '110,301,302';
}
  // XML
  sXML_Version        = '1.0';
  sXML_Encoding       = 'UTF-8';
  ENCODING_UTF8 = '?xml version="1.0" encoding="UTF-8"?';
  ENCODING_UTF8_STD = '?xml version="1.0" encoding="UTF-8" standalone="no"?';
  NAME_SPACE = 'xmlns="http://www.portalfiscal.inf.br/cte"';
  sCampoNulo = '#NULL#';
  ///
  ///
  ///
  verEventosCan_Layout        = '2.00'; // Evento - Cancelamento da CTe - Vers�o do layout
  verEventosCan_Evento        = '2.00'; // Evento - Cancelamento da CTe - Vers�o do Evento
  verEventosCan_Cancel        = '2.00'; // Evento - Cancelamento da CTe - Vers�o do Cancelamento

type
  TCTeEveGeraXMLCan_0200a = class(TObject)
  private
    {private declaration}
     function  GeraXML_Can(const FatID, FatNum, Empresa: Integer; nProt,
               xJust: String; const LaAviso1, LaAviso2: TLabel;
               var XML: String): Boolean;
  public
    {public declaration}
    function  CriarDocumentoEveCan(const FatID, FatNum, Empresa:
              Integer; const Id: String; const cOrgao, tpAmb, Tipo: Integer;
              const CNPJ, CPF, chCTe: String;
              const dhEvento: TDateTime; const TZD_UTC: Double;
              const tpEvento, nSeqEvento: Integer; verEvento: Double;
              const versao: Double; descEvento, nProt, xJust: String;
              var XMLAssinado: String;
              const LaAviso1, LaAviso2: TLabel): Boolean;
  end;

var
  UnCTeEveGeraXMLCan_0200a: TCTeEveGeraXMLCan_0200a;

implementation

uses ModuleCTe_0000;

var
  //  ACBr
  //CertStore     : IStore3;
  //CertStoreMem  : IStore3;
  //PrivateKey    : IPrivateKey;
  //Certs         : ICertificates2;
  Cert          : ICertificate2;
  //NumCertCarregado : String;
  // XML
  EveXML: IXMLTEvento;
  CanXML: IXMLEvCancCTe;
  arqXML: TXMLDocument;
  //
  FLaAviso1: TLabel;
  FLaAviso2: TLabel;
  FFatID, FFatNum, FEmpresa: Integer;

{ TEveGeraXMLCan_0200a }

function TCTeEveGeraXMLCan_0200a.CriarDocumentoEveCan(const FatID, FatNum,
  Empresa: Integer; const Id: String; const cOrgao, tpAmb, Tipo: Integer;
  const CNPJ, CPF, chCTe: String; const dhEvento: TDateTime;
  const TZD_UTC: Double; const tpEvento, nSeqEvento: Integer; verEvento: Double;
  const versao: Double; descEvento, nProt, xJust: String;
  var XMLAssinado: String; const LaAviso1, LaAviso2: TLabel): Boolean;
var
  //strChaveAcesso,
  NumeroSerial: String;
  //DadosTxt,
  Texto, XMLAny: String;
  AnyNode: IXMLNode;
begin
  Result := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  //
  FFatID   := FatID;
  FFatNum  := FatNum;
  FEmpresa := Empresa;
  //
  if GeraXML_Can(FatID, FatNum, Empresa, nProt, xJust, LaAviso1, LaAviso2,
  XMLAny) then
  begin
    (* Criando o Documento XML e Gravando cabe�alho... *)
    arqXML := TXMLDocument.Create(nil);
    arqXML.Active := False;
    arqXML.FileName := '';
    EveXML := GetEventoCte(arqXML);
    arqXML.Version := sXML_Version;
    arqXML.Encoding := sXML_Encoding;
    (*
    arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
    arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
    arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviCTe_v1.12.xsd';
    *)

    EveXML.versao := verEventosCan_Layout;
    EveXML.InfEvento.Id := Id;
    EveXML.InfEvento.COrgao := Geral.FFN(cOrgao, 2);
    EveXML.InfEvento.TpAmb := Geral.FF0(tpAmb);
    EveXML.InfEvento.CNPJ := Geral.SoNumero_TT(CNPJ);
    EveXML.InfEvento.ChCTe := Geral.SoNumero_TT(chCTe);
    EveXML.InfEvento.DhEvento := XXe_PF.FDT_XXe_UTC(dhEvento, Null(*TZD_UTC*));
    EveXML.InfEvento.TpEvento := Geral.FF0(tpEvento);
    EveXML.InfEvento.NSeqEvento := Geral.FF0(nSeqEvento);
    EveXML.InfEvento.DetEvento.VersaoEvento := verEventosCan_Cancel;
    AnyNode := EveXML.InfEvento.DetEvento.AddChild('_');
    AnyNode.Text := '';
(*
    EveXML.InfEvento.DetEvento.DescEvento := descEvento;
    EveXML.InfEvento.DetEvento.NProt := nProt;
    EveXML.InfEvento.DetEvento.XJust := xJust;
*)
    //

    Texto := EveXML.XML;
    Texto := Geral.Substitui(Texto, '<_></_>', XMLAny);
    Texto := StringReplace(Texto, #10, '', [rfReplaceAll] );
    Texto := StringReplace(Texto, #13, '', [rfReplaceAll] );

    // Assinar!
    NumeroSerial := DmCTe_0000.QrFilialCTeSerNum.Value;
    if XXe_PF.ObtemCertificado(NumeroSerial, Cert) then
      Result := CTe_PF.AssinarMSXML(Texto, Cert, XMLAssinado);
    arqXML := nil;
  end else
    Geral.MB_Erro('N�o foi possivel gerar o XML any!');
end;

function TCTeEveGeraXMLCan_0200a.GeraXML_Can(const FatID, FatNum, Empresa: Integer;
  nProt, xJust: String; const LaAviso1, LaAviso2: TLabel; var XML: String): Boolean;
var
  //strChaveAcesso,
  NumeroSerial: String;
  //DadosTxt,
  Texto: String;
begin
  Result := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  //
  FFatID   := FatID;
  FFatNum  := FatNum;
  FEmpresa := Empresa;
  //
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  CanXML := GetevCancCTe(arqXML);
  arqXML.Version := sXML_Version;
  arqXML.Encoding := sXML_Encoding;
  (*
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/cte/enviCTe_v1.12.xsd';
  *)

  //CanXML.versao := verEventosCan_Layout;
  CanXML.DescEvento := 'Cancelamento';
  CanXML.NProt := nProt;
  CanXML.XJust := xJust;
  //
  Texto := CanXML.XML;
  Texto := StringReplace(Texto, #10, '', [rfReplaceAll] );
  Texto := StringReplace(Texto, #13, '', [rfReplaceAll] );

  //modalXML := rodoXml.XML.Text;
  XML := Geral.Substitui(Texto, ' xmlns="http://www.portalfiscal.inf.br/cte"', '');
  XML := Geral.Substitui(Texto, slineBreak, '');
  //
(*
  // Assinar!
  NumeroSerial := DmCTe_0000.QrFilialCTeSerNum.Value;
  if XXe_PF.ObtemCertificado(NumeroSerial, Cert) then
    Result := CTe_PF.AssinarMSXML(Texto, Cert, XMLAssinado);
  arqXML := nil;
*)
  Result := True;
end;

end.
