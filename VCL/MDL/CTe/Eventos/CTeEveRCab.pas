unit CTeEveRCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGrid, Menus, DmkDAC_PF, (*EveGeraXMLCCe_0200*)evCCeCTe_v200,
  (*EveGeraXMLCan_0200*)evCancCTe_v200, frxClass, frxDBSet, UnDmkProcFunc,
  frxBarcode, UnDmkEnums, UnXXe_PF, UnCTe_PF;

type
  TFmCTeEveRcab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtCCe: TBitBtn;
    LaTitulo1C: TLabel;
    DsA: TDataSource;
    DBGrid1: TDBGrid;
    PMCCe: TPopupMenu;
    Crianovacorreo1: TMenuItem;
    Alteracorreo1: TMenuItem;
    Excluicorreo1: TMenuItem;
    QrCTeEveRCab: TmySQLQuery;
    DsCTeEveRCab: TDataSource;
    QrCTeEveRCabFatID: TIntegerField;
    QrCTeEveRCabFatNum: TIntegerField;
    QrCTeEveRCabEmpresa: TIntegerField;
    QrCTeEveRCabControle: TIntegerField;
    QrCTeEveRCabEventoLote: TIntegerField;
    QrCTeEveRCabId: TWideStringField;
    QrCTeEveRCabcOrgao: TSmallintField;
    QrCTeEveRCabtpAmb: TSmallintField;
    QrCTeEveRCabTipoEnt: TSmallintField;
    QrCTeEveRCabCNPJ: TWideStringField;
    QrCTeEveRCabCPF: TWideStringField;
    QrCTeEveRCabchCTe: TWideStringField;
    QrCTeEveRCabdhEvento: TDateTimeField;
    QrCTeEveRCabverEvento: TFloatField;
    QrCTeEveRCabtpEvento: TIntegerField;
    QrCTeEveRCabnSeqEvento: TSmallintField;
    QrCTeEveRCabversao: TFloatField;
    QrCTeEveRCabdescEvento: TWideStringField;
    QrCTeEveRCabLk: TIntegerField;
    QrCTeEveRCabDataCad: TDateField;
    QrCTeEveRCabDataAlt: TDateField;
    QrCTeEveRCabUserCad: TIntegerField;
    QrCTeEveRCabUserAlt: TIntegerField;
    QrCTeEveRCabAlterWeb: TSmallintField;
    QrCTeEveRCabAtivo: TSmallintField;
    QrCTeEveRCCe: TmySQLQuery;
    DsCTeEveRCCe: TDataSource;
    QrCTeEveRCCeFatID: TIntegerField;
    QrCTeEveRCCeFatNum: TIntegerField;
    QrCTeEveRCCeEmpresa: TIntegerField;
    QrCTeEveRCCeControle: TIntegerField;
    QrCTeEveRCCeConta: TIntegerField;
    QrCTeEveRCCenCondUso: TSmallintField;
    QrCTeEveRCCeLk: TIntegerField;
    QrCTeEveRCCeDataCad: TDateField;
    QrCTeEveRCCeDataAlt: TDateField;
    QrCTeEveRCCeUserCad: TIntegerField;
    QrCTeEveRCCeUserAlt: TIntegerField;
    QrCTeEveRCCeAlterWeb: TSmallintField;
    QrCTeEveRCCeAtivo: TSmallintField;
    DBGrid2: TDBGrid;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid4: TDBGrid;
    QrERC: TmySQLQuery;
    QrERCFatID: TIntegerField;
    QrERCFatNum: TIntegerField;
    QrERCEmpresa: TIntegerField;
    QrERCControle: TIntegerField;
    QrERCEventoLote: TIntegerField;
    QrERCId: TWideStringField;
    QrERCcOrgao: TSmallintField;
    QrERCtpAmb: TSmallintField;
    QrERCTipoEnt: TSmallintField;
    QrERCCNPJ: TWideStringField;
    QrERCCPF: TWideStringField;
    QrERCchCTe: TWideStringField;
    QrERCdhEvento: TDateTimeField;
    QrERCverEvento: TFloatField;
    QrERCtpEvento: TIntegerField;
    QrERCnSeqEvento: TSmallintField;
    QrERCversao: TFloatField;
    QrERCdescEvento: TWideStringField;
    QrCTeEveRCabTZD_UTC: TFloatField;
    QrERCTZD_UTC: TFloatField;
    Splitter1: TSplitter;
    QrCTeEveRCabtpAmb_TXT: TWideStringField;
    QrCTeEveRCabStatus: TIntegerField;
    BtCan: TBitBtn;
    PMCan: TPopupMenu;
    IncluicancelamentodaCTe1: TMenuItem;
    ExcluicancelamentodaCTe1: TMenuItem;
    QrCTeEveRCan: TmySQLQuery;
    DsCTeEveRCan: TDataSource;
    QrCTeEveRCanFatID: TIntegerField;
    QrCTeEveRCanFatNum: TIntegerField;
    QrCTeEveRCanEmpresa: TIntegerField;
    QrCTeEveRCanControle: TIntegerField;
    QrCTeEveRCanConta: TIntegerField;
    QrCTeEveRCannProt: TLargeintField;
    QrCTeEveRCannJust: TIntegerField;
    QrCTeEveRCanxJust: TWideStringField;
    QrCTeEveRCanLk: TIntegerField;
    N1: TMenuItem;
    Imprime1: TMenuItem;
    frxEveCCe: TfrxReport;
    frxDsA: TfrxDBDataset;
    frxDsCTeEveRCCe: TfrxDBDataset;
    frxDsCTeEveRCab: TfrxDBDataset;
    BtEPEC: TBitBtn;
    PMEPEC: TPopupMenu;
    QrCTeEveREPEC: TmySQLQuery;
    DsCTeEveREPEC: TDataSource;
    TabSheet3: TTabSheet;
    QrCTeEveRCabXML_Eve: TWideMemoField;
    QrCTeEveRCabXML_retEve: TWideMemoField;
    QrCTeEveRCabret_versao: TFloatField;
    QrCTeEveRCabret_Id: TWideStringField;
    QrCTeEveRCabret_tpAmb: TSmallintField;
    QrCTeEveRCabret_verAplic: TWideStringField;
    QrCTeEveRCabret_cOrgao: TSmallintField;
    QrCTeEveRCabret_cStat: TIntegerField;
    QrCTeEveRCabret_xMotivo: TWideStringField;
    QrCTeEveRCabret_chCTe: TWideStringField;
    QrCTeEveRCabret_tpEvento: TIntegerField;
    QrCTeEveRCabret_xEvento: TWideStringField;
    QrCTeEveRCabret_nSeqEvento: TIntegerField;
    QrCTeEveRCabret_CNPJDest: TWideStringField;
    QrCTeEveRCabret_CPFDest: TWideStringField;
    QrCTeEveRCabret_emailDest: TWideStringField;
    QrCTeEveRCabret_dhRegEvento: TDateTimeField;
    QrCTeEveRCabret_TZD_UTC: TFloatField;
    QrCTeEveRCabret_nProt: TWideStringField;
    BtLote: TBitBtn;
    QrA: TmySQLQuery;
    QrANO_Cli: TWideStringField;
    QrAFatID: TIntegerField;
    QrAFatNum: TIntegerField;
    QrALoteEnv: TIntegerField;
    QrAId: TWideStringField;
    QrAide_natOp: TWideStringField;
    QrAEmpresa: TIntegerField;
    QrAide_serie: TIntegerField;
    QrAide_nCT: TIntegerField;
    QrAide_dEmi: TDateField;
    QrAide_tpCTe: TSmallintField;
    QrAnProt: TWideStringField;
    QrAxMotivo: TWideStringField;
    QrAdhRecbto: TWideStringField;
    QrAIDCtrl: TIntegerField;
    QrAdest_CNPJ: TWideStringField;
    QrAdest_CPF: TWideStringField;
    QrAdest_xNome: TWideStringField;
    QrAversao: TFloatField;
    QrAide_tpEmis: TSmallintField;
    QrANOME_tpEmis: TWideStringField;
    QrANOME_tpCTe: TWideStringField;
    QrAinfCanc_xJust: TWideStringField;
    QrAStatus: TIntegerField;
    QrAinfCanc_dhRecbto: TDateTimeField;
    QrAinfCanc_nProt: TWideStringField;
    QrAcStat: TFloatField;
    QrAcSitCTe: TSmallintField;
    QrANO_Emi: TWideStringField;
    QrANO_Terceiro: TWideStringField;
    QrAvPrest_vTPrest: TFloatField;
    QrAvPrest_vRec: TFloatField;
    QrAICMS_vICMS: TFloatField;
    QrAide_Modal: TSmallintField;
    QrACodInfoEmite: TIntegerField;
    QrOpcoesCTe: TmySQLQuery;
    QrOpcoesCTeCTeversao: TFloatField;
    QrOpcoesCTeCTeide_mod: TSmallintField;
    QrOpcoesCTeSimplesFed: TSmallintField;
    QrOpcoesCTeDirCTeGer: TWideStringField;
    QrOpcoesCTeDirCTeAss: TWideStringField;
    QrOpcoesCTeDirCTeEnvLot: TWideStringField;
    QrOpcoesCTeDirCTeRec: TWideStringField;
    QrOpcoesCTeDirCTePedRec: TWideStringField;
    QrOpcoesCTeDirCTeProRec: TWideStringField;
    QrOpcoesCTeDirCTeDen: TWideStringField;
    QrOpcoesCTeDirCTePedCan: TWideStringField;
    QrOpcoesCTeDirCTeCan: TWideStringField;
    QrOpcoesCTeDirCTePedInu: TWideStringField;
    QrOpcoesCTeDirCTeInu: TWideStringField;
    QrOpcoesCTeDirCTePedSit: TWideStringField;
    QrOpcoesCTeDirCTeSit: TWideStringField;
    QrOpcoesCTeDirCTePedSta: TWideStringField;
    QrOpcoesCTeDirCTeSta: TWideStringField;
    QrOpcoesCTeCTeUF_WebServ: TWideStringField;
    QrOpcoesCTeCTeUF_Servico: TWideStringField;
    QrOpcoesCTePathLogoCTe: TWideStringField;
    QrOpcoesCTeCtaFretPrest: TIntegerField;
    QrOpcoesCTeTxtFretPrest: TWideStringField;
    QrOpcoesCTeDupFretPrest: TWideStringField;
    QrOpcoesCTeCTeSerNum: TWideStringField;
    QrOpcoesCTeCTeSerVal: TDateField;
    QrOpcoesCTeCTeSerAvi: TSmallintField;
    QrOpcoesCTeDirDACTes: TWideStringField;
    QrOpcoesCTeDirCTeProt: TWideStringField;
    QrOpcoesCTeCTePreMailAut: TIntegerField;
    QrOpcoesCTeCTePreMailCan: TIntegerField;
    QrOpcoesCTeCTePreMailEveCCe: TIntegerField;
    QrOpcoesCTeCTeVerStaSer: TFloatField;
    QrOpcoesCTeCTeVerEnvLot: TFloatField;
    QrOpcoesCTeCTeVerConLot: TFloatField;
    QrOpcoesCTeCTeVerCanCTe: TFloatField;
    QrOpcoesCTeCTeVerInuNum: TFloatField;
    QrOpcoesCTeCTeVerConCTe: TFloatField;
    QrOpcoesCTeCTeVerLotEve: TFloatField;
    QrOpcoesCTeCTeide_tpImp: TSmallintField;
    QrOpcoesCTeCTeide_tpAmb: TSmallintField;
    QrOpcoesCTeCTeAppCode: TSmallintField;
    QrOpcoesCTeCTeAssDigMode: TSmallintField;
    QrOpcoesCTeCTeEntiTipCto: TIntegerField;
    QrOpcoesCTeCTeEntiTipCt1: TIntegerField;
    QrOpcoesCTeMyEmailCTe: TWideStringField;
    QrOpcoesCTeDirCTeSchema: TWideStringField;
    QrOpcoesCTeDirCTeRWeb: TWideStringField;
    QrOpcoesCTeDirCTeEveEnvLot: TWideStringField;
    QrOpcoesCTeDirCTeEveRetLot: TWideStringField;
    QrOpcoesCTeDirCTeEvePedCCe: TWideStringField;
    QrOpcoesCTeDirCTeEveRetCCe: TWideStringField;
    QrOpcoesCTeDirCTeEveProcCCe: TWideStringField;
    QrOpcoesCTeDirCTeEvePedCan: TWideStringField;
    QrOpcoesCTeDirCTeEveRetCan: TWideStringField;
    QrOpcoesCTeDirCTeRetCTeDes: TWideStringField;
    QrOpcoesCTeCTeCTeVerConDes: TFloatField;
    QrOpcoesCTeDirCTeEvePedMDe: TWideStringField;
    QrOpcoesCTeDirCTeEveRetMDe: TWideStringField;
    QrOpcoesCTeDirDowCTeDes: TWideStringField;
    QrOpcoesCTeDirDowCTeCTe: TWideStringField;
    QrOpcoesCTeCTeInfCpl: TIntegerField;
    QrOpcoesCTeCTeVerConsCad: TFloatField;
    QrOpcoesCTeCTeVerDistDFeInt: TFloatField;
    QrOpcoesCTeCTeVerDowCTe: TFloatField;
    QrOpcoesCTeDirDowCTeCnf: TWideStringField;
    QrOpcoesCTeCTeItsLin: TSmallintField;
    QrOpcoesCTeCTeFTRazao: TSmallintField;
    QrOpcoesCTeCTeFTEnder: TSmallintField;
    QrOpcoesCTeCTeFTFones: TSmallintField;
    QrOpcoesCTeCTeMaiusc: TSmallintField;
    QrOpcoesCTeCTeShowURL: TWideStringField;
    QrOpcoesCTeCTetpEmis: TSmallintField;
    QrOpcoesCTeCTeSCAN_Ser: TIntegerField;
    QrOpcoesCTeCTeSCAN_nCT: TIntegerField;
    QrOpcoesCTeCTe_indFinalCpl: TSmallintField;
    N2: TMenuItem;
    Solicitacancelamento1: TMenuItem;
    QrCTeEveRCCeIts: TmySQLQuery;
    QrCTeEveRCCeItsFatID: TIntegerField;
    QrCTeEveRCCeItsFatNum: TIntegerField;
    QrCTeEveRCCeItsEmpresa: TIntegerField;
    QrCTeEveRCCeItsControle: TIntegerField;
    QrCTeEveRCCeItsConta: TIntegerField;
    QrCTeEveRCCeItsIDItem: TIntegerField;
    QrCTeEveRCCeItsLayC_Grupo: TIntegerField;
    QrCTeEveRCCeItsLayI_Versao: TFloatField;
    QrCTeEveRCCeItsLayI_Grupo: TIntegerField;
    QrCTeEveRCCeItsLayI_Codigo: TIntegerField;
    QrCTeEveRCCeItsGrupoAlterado: TWideStringField;
    QrCTeEveRCCeItsCampoAlterado: TWideStringField;
    QrCTeEveRCCeItsValorAlterado: TWideMemoField;
    QrCTeEveRCCeItsnroItemAlterado: TIntegerField;
    QrCTeEveRCCeItsLk: TIntegerField;
    QrCTeEveRCCeItsDataCad: TDateField;
    QrCTeEveRCCeItsDataAlt: TDateField;
    QrCTeEveRCCeItsUserCad: TIntegerField;
    QrCTeEveRCCeItsUserAlt: TIntegerField;
    QrCTeEveRCCeItsAlterWeb: TSmallintField;
    QrCTeEveRCCeItsAtivo: TSmallintField;
    QrCTeEveRCCeItsValor255: TWideStringField;
    DsCTeEveRCCeIts: TDataSource;
    DBGrid3: TDBGrid;
    DBMemo1: TDBMemo;
    IncluiEPEC1: TMenuItem;
    ExcluiEPEC1: TMenuItem;
    QrAinfCarga_vCarga: TFloatField;
    QrACodInfoTomad: TIntegerField;
    QrAide_UFIni: TWideStringField;
    QrAide_UFFim: TWideStringField;
    QrAtoma99_toma: TSmallintField;
    DBGrid5: TDBGrid;
    QrCTeEveREPECFatID: TIntegerField;
    QrCTeEveREPECFatNum: TIntegerField;
    QrCTeEveREPECEmpresa: TIntegerField;
    QrCTeEveREPECControle: TIntegerField;
    QrCTeEveREPECConta: TIntegerField;
    QrCTeEveREPECnJust: TIntegerField;
    QrCTeEveREPECxJust: TWideStringField;
    QrCTeEveREPECvICMS: TFloatField;
    QrCTeEveREPECvTPrest: TFloatField;
    QrCTeEveREPECvCarga: TFloatField;
    QrCTeEveREPECCodInfoTomad: TIntegerField;
    QrCTeEveREPECToma04_toma: TSmallintField;
    QrCTeEveREPECToma04_UF: TWideStringField;
    QrCTeEveREPECToma04_CNPJ: TWideStringField;
    QrCTeEveREPECToma04_CPF: TWideStringField;
    QrCTeEveREPECToma04_IE: TWideStringField;
    QrCTeEveREPECModal: TSmallintField;
    QrCTeEveREPECUFIni: TWideStringField;
    QrCTeEveREPECUFFim: TWideStringField;
    QrCTeEveREPECLk: TIntegerField;
    QrCTeEveREPECDataCad: TDateField;
    QrCTeEveREPECDataAlt: TDateField;
    QrCTeEveREPECUserCad: TIntegerField;
    QrCTeEveREPECUserAlt: TIntegerField;
    QrCTeEveREPECAlterWeb: TSmallintField;
    QrCTeEveREPECAtivo: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtCCeClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Crianovacorreo1Click(Sender: TObject);
    procedure QrAAfterOpen(DataSet: TDataSet);
    procedure QrAAfterScroll(DataSet: TDataSet);
    procedure QrABeforeClose(DataSet: TDataSet);
    procedure QrCTeEveRCabBeforeClose(DataSet: TDataSet);
    procedure QrCTeEveRCabAfterScroll(DataSet: TDataSet);
    procedure QrCTeEveRCabCalcFields(DataSet: TDataSet);
    procedure PMCCePopup(Sender: TObject);
    procedure Alteracorreo1Click(Sender: TObject);
    procedure BtCanClick(Sender: TObject);
    procedure IncluicancelamentodaCTe1Click(Sender: TObject);
    procedure ExcluicancelamentodaCTe1Click(Sender: TObject);
    procedure Excluicorreo1Click(Sender: TObject);
    procedure PMCanPopup(Sender: TObject);
    procedure Imprime1Click(Sender: TObject);
    procedure frxEveCCeGetValue(const VarName: string; var Value: Variant);
    procedure QrACalcFields(DataSet: TDataSet);
    procedure BtEPECClick(Sender: TObject);
    procedure PMEPECPopup(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtLoteClick(Sender: TObject);
    procedure Solicitacancelamento1Click(Sender: TObject);
    procedure ExcluiEPEC1Click(Sender: TObject);
    procedure IncluiEPEC1Click(Sender: TObject);
  private
    { Private declarations }
    procedure MostraFormCartaCorrecao(SQLType: TSQLType);
    procedure MostraFormCancelamentoCTe(SQLType: TSQLType);
    procedure MostraFormEPEC(SQLType: TSQLType);

  public
    { Public declarations }
    procedure ReopenCTeCabA(FatID, FatNum, Empresa: Integer);
    function  IncluiCTeEveRCab(const EvePreDef: Integer; const EventoCTe:
              TEventoXXe; var FatID, FatNum, Empresa, Controle: Integer):
              Boolean;
    procedure AbreDadosParaXML(Controle: Integer);
    procedure ReopenCTeEveRCab(Controle: Integer);
    procedure ReopenCTeEveRCCe(Conta: Integer);
    procedure ReopenCTeEveRCan(Conta: Integer);
    procedure ReopenCTeEveRCCeIts(IDItem: Integer);
    procedure ReopenCTeEveREPEC(Conta: Integer);
  end;

  var
  FmCTeEveRcab: TFmCTeEveRcab;

implementation

uses UnMyObjects, Module, UMySQLModule, MyDBCheck, CTeEveRCCe, ModuleCTe_0000,
  UnCTe_Tabs, ModuleGeral, CTeGeraXML_0200a, CTeEveRCan, CTeEveREPEC;

{$R *.DFM}

procedure TFmCTeEveRcab.AbreDadosParaXML(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrERC, Dmod.MyDB, [
  'SELECT * FROM cteevercab ',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
end;

procedure TFmCTeEveRcab.Alteracorreo1Click(Sender: TObject);
begin
  MostraFormCartaCorrecao(stUpd);
end;

procedure TFmCTeEveRcab.BtCanClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  //
  MyObjects.MostraPopUpDeBotao(PMCan, BtCan);
end;

procedure TFmCTeEveRcab.BtCCeClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  if QrAStatus.Value = 100 then
    MyObjects.MostraPopUpDeBotao(PMCCe, BtCCe)
  else
    Geral.MB_Aviso(
    'O CT-e n�o pode ter carta de corre��o porque seu status n�o � 100 - "Uso autorizado"');
end;

procedure TFmCTeEveRcab.BtLoteClick(Sender: TObject);
var
  LoteEnv, Controle: Integer;
begin
  if (QrCTeEveRCab.State <> dsInactive) and (QrCTeEveRCab.RecordCount > 0) then
  begin
    Controle := QrCTeEveRCabControle.Value;
    LoteEnv  := QrCTeEveRCabEventoLote.Value;
  end else
  begin
    Controle := 0;
    LoteEnv  := 0;
  end;
  //
  CTe_PF.MostraFormCTeEveRLoE(LoteEnv);
  //
  if Controle <> 0 then
    ReopenCTeEveRCab(Controle);
end;

procedure TFmCTeEveRcab.BtEPECClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  //
  MyObjects.MostraPopUpDeBotao(PMEPEC, BtEPEC);
end;

procedure TFmCTeEveRcab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCTeEveRcab.Crianovacorreo1Click(Sender: TObject);
begin
  MostraFormCartaCorrecao(stIns);
end;

procedure TFmCTeEveRcab.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  DmCTe_0000.ColoreStatusCTeEmDBGrid(DBGrid1, Rect, DataCol, Column,
    State, Trunc(QrAcStat.Value));
end;

procedure TFmCTeEveRcab.ExcluicancelamentodaCTe1Click(Sender: TObject);
begin
  if QrAStatus.Value in ([135, 136]) then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o do cancelamento da CTe?') = ID_YES then
    begin
      DBCheck.ExcluiRegistro(Dmod.QrUpd, QrCTeEveRCan, 'cteevercan',
      ['Conta'], ['Conta'], True);
      //
      DBCheck.ExcluiRegistro(Dmod.QrUpd, QrCTeEveRCab, 'cteevercab',
      ['Controle'], ['Controle'], True);
      //
      ReopenCTeEveRCab(0);
    end;
  end else
    Geral.MB_Aviso(
    'O evento n�o pode ser exclu�do porque j� est� registrado!');
end;

procedure TFmCTeEveRcab.Excluicorreo1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o da carta de corre��o selecionada?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then
  begin
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrCTeEveRCCe, 'cteevercce',
    ['Conta'], ['Conta'], True);
    //
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrCTeEveRCab, 'cteevercab',
    ['Controle'], ['Controle'], True);
    //
    ReopenCTeEveRCab(0);
  end;
end;

procedure TFmCTeEveRcab.ExcluiEPEC1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da emiss�o pr�via - EPEC?') =
  ID_YES then
  begin
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrCTeEveREPEC, 'cteeverepec',
    ['Conta'], ['Conta'], True);
    //
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrCTeEveRCab, 'cteevercab',
    ['Controle'], ['Controle'], True);
    //
    ReopenCTeEveRCab(0);
  end;
end;

procedure TFmCTeEveRcab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmCTeEveRcab.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmCTeEveRcab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCTeEveRcab.frxEveCCeGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'LogoCTeExiste' then Value := FileExists(QrOpcoesCTePathLogoCTe.Value) else
  if VarName = 'LogoCTePath' then Value := QrOpcoesCTePathLogoCTe.Value else
{
  if VarName = 'CTeItsLin' then
  begin
    case DmodG.QrParamsEmpCTeItsLin.Value of
      1: Value := 12.47244;    // 1 linha
      2: Value := 21.54331;    // 2 linhas
      else Value := 32.12598;  // 3 linhas
    end;
  end;
}
  if VarName = 'CTeFTRazao' then Value := QrOpcoesCTeCTeFTRazao.Value else
  if VarName = 'CTeFTEnder' then Value := QrOpcoesCTeCTeFTEnder.Value else
  if VarName = 'CTeFTFones' then Value := QrOpcoesCTeCTeFTFones.Value else
  if VarName = 'xCondUso' then Value := CO_CONDI_USO_CCE_COM[QrCTeEveRCCenCondUso.Value];
end;

procedure TFmCTeEveRcab.Imprime1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM cteevercab ',
    'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
    'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
    'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
    'AND tpEvento=' + Geral.FF0(CO_COD_evexxe110110CCe), // 110110
    'ORDER BY nSeqEvento DESC ',
    '']);
    if Qry.FieldByName('Controle').AsInteger <> QrCTeEveRCabControle.Value then
    begin
      //135 - CTeXMLGerencia.Texto_StatusCTe(
      Geral.MB_Aviso('Impress�o de Carta de Corre��o cancelada!' + slineBreak +
      'Sequencia de carta criada inv�lida!' + sLineBreak +
      'Sequencia correta (�ltima): ' +
      Geral.FF0(Qry.FieldByName('Controle').AsInteger) + sLineBreak +
      'Sequencia  desta carta: ' + Geral.FF0(QrCTeEveRCabControle.Value));
      //
      Exit;
    end;
    if QrCTeEveRCabStatus.Value <> 135 then
    begin
      //135 - CTeXMLGerencia.Texto_StatusCTe(
      Geral.MB_Aviso('Impress�o de Carta de Corre��o cancelada!' + slineBreak +
      'Status inv�lido!' + sLineBreak +
      'Status correto: 135 = ' + CTe_PF.Texto_StatusCTe(135, 0) + slineBreak +
      'Status atual: ' + Geral.FF0(QrCTeEveRCabStatus.Value) + ' = ' +
      CTe_PF.Texto_StatusCTe(QrCTeEveRCabStatus.Value, 0));
      //
      Exit;
    end;
    DModG.ReopenParamsEmp(DmodG.QrEmpresasCodigo.Value);
    MyObjects.frxDefineDataSets(
      frxEveCCe, [frxDsA, frxDsCTeEveRCab, frxDsCTeEveRCCe]);
    MyObjects.frxMostra(frxEveCCe, 'Carta de Corre��o Eletr�nica');
  finally
    Qry.Free;
  end;
end;

procedure TFmCTeEveRcab.IncluicancelamentodaCTe1Click(Sender: TObject);
begin
  if XXe_PF.JaExisteEventoUnicoVinculado(tipoxxeCTe, evexxe110111Can,
  QrAFatID.Value, QrAFatNum.Value, QrAEmpresa.Value) then
    Exit;
  if QrAStatus.Value = 100 then
    MostraFormCancelamentoCTe(stIns)
  else
    Geral.MB_Aviso(
    'A CTe n�o pode ser cancelada porque seu status n�o � 100 - "Uso autorizado"');
end;

function TFmCTeEveRcab.IncluiCTeEveRCab(const EvePreDef: Integer;
const EventoCTe: TEventoXXe; var
FatID, FatNum, Empresa, Controle: Integer): Boolean;
var
  Id, CNPJ, CPF, chCTe, dhEvento, descEvento, verEvento, US: String;
  //EventoLote,
  cOrgao, tpAmb, TipoEnt, tpEvento, nSeqEvento: Integer;
  TZD_UTC, versao: Double;
begin
  Result := False;
  FatID          := QrAFatID.Value;
  FatNum         := QrAFatNum.Value;
  Empresa        := QrAEmpresa.Value;

  DmCTe_0000.ReopenEmpresa(Empresa);
  if DmCTe_0000.QrEmpresaCodigo.Value <> Empresa then
    Exit;
  if not DmCTe_0000.ReopenOpcoesCTe(DmCTe_0000.QrOpcoesCTe, Empresa, True) then
    Exit;
  //
(*
  if EventoCTe = evexxMDe then
    cOrgao         := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(
                      DmCTe_0000.QrOpcoesCTeUF_MDeMDe.Value) // 91 = Ambiente Nacional
  else
*)
  // cOrgao         := Geral.IMV(DmCTe_0000.QrEmpresaDTB_UF.Value);
////////////////////////////////////////////////////////////////////////////////
  case TCTeTpEmis(DmCTe_0000.QrFilialCTetpEmis.Value) of
    (*1*)ctetpemisNormal    :       US := DmCTe_0000.QrFilialCTeUF_WebServ.Value;
    //ctetpemis2=2, ctetpemis3=3,
    (*4*)ctetpemisEPEC_SVC:         US := DmCTe_0000.QrFilialCTeUF_Conting.Value;
    (*5*)ctetpemisContingenciaFSDA: US := DmCTe_0000.QrFilialCTeUF_Conting.Value;
    //ctetpemis6=6,
    ctetpemisSVC_RS:                US := DmCTe_0000.QrFilialCTeUF_Conting.Value;
    ctetpemisSVC_SP:                US := DmCTe_0000.QrFilialCTeUF_Conting.Value;
    else
    begin
      Geral.MB_Erro('Tipo de emiss�o n�o implementada para UF servi�o!');
      US := DmCTe_0000.QrFilialCTeUF_Servico.Value;
    end;
  end;
  cOrgao         := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(US);
////////////////////////////////////////////////////////////////////////////////
  tpAmb          := DmCTe_0000.QrOpcoesCTeCTeide_tpAmb.Value;
  TipoEnt        := DmCTe_0000.QrEmpresaTipo.Value;
  if TipoEnt = 0 then
  begin
    CNPJ           := Geral.SoNumero_TT(DmCTe_0000.QrEmpresaCNPJ.Value);
    CPF            := '';
  end else
  begin
    CNPJ           := '';
    CPF            := Geral.SoNumero_TT(DmCTe_0000.QrEmpresaCPF.Value);
  end;
  chCTe          := QrAId.Value;

  dhEvento       := Geral.FDT(DmodG.ObtemAgora(), 109);
  //TZD_UTC        := DModG.UTC_ObtemDiferenca();
  //TZD_UTC        := DmodG.QrParamsEmpTZD_UTC.Value;
  TZD_UTC        := DmodG.ObtemHorarioTZD_JahCorrigido(Empresa);
  verEvento      := Geral.FFT_Dot(DmCTe_0000.QrOpcoesCTeCTeVerLotEve.Value, 2, siPositivo);
  CTe_PF.Evento_Obtem_DadosEspecificos(EvePreDef, EventoCTe, tpEvento, versao,
    descEvento);
  nSeqEvento     := DmCTe_0000.Evento_Obtem_nSeqEvento(FatID, FatNum, Empresa, tpEvento);
  Id             := CTe_PF.Evento_MontaId(tpEvento, QrAId.Value, nSeqEvento);
  //
  Controle := DModG.BuscaProximoCodigoInt('ctectrl', 'cteevercab', '', 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'cteevercab', False, [
  (*'EventoLote',*) 'Id', 'cOrgao',
  'tpAmb', 'TipoEnt', 'CNPJ',
  'CPF', 'chCTe', 'dhEvento',
  'TZD_UTC', 'verEvento', 'tpEvento',
  'nSeqEvento', 'versao', 'descEvento'], [
  'FatID', 'FatNum', 'Empresa', 'Controle'], [
  (*EventoLote,*) Id, cOrgao,
  tpAmb, TipoEnt, CNPJ,
  CPF, chCTe, dhEvento,
  TZD_UTC, verEvento, tpEvento,
  nSeqEvento, versao, descEvento], [
  FatID, FatNum, Empresa, Controle], True) then
  begin
    AbreDadosParaXML(Controle);
    Result := True;
  end;
end;

procedure TFmCTeEveRcab.IncluiEPEC1Click(Sender: TObject);
begin
  MostraFormEPEC(stIns);
end;

procedure TFmCTeEveRcab.MostraFormCancelamentoCTe(SQLType: TSQLType);
begin
  if (not XXe_PF.PermiteCriacaoDeEvento(tipoxxeCTe, evexxe110111Can,
  QrAFatID.Value, QrAFatNum.Value, QrAEmpresa.Value)) and (SQLType = stIns) then
    Exit;
  //
  if DBCheck.CriaFm(TFmCTeEveRCan, FmCTeEveRCan, afmoNegarComAviso) then
  begin
    FmCTeEveRCan.ImgTipo.SQLType := SQLType;
    FmCTeEveRCan.Edide_serie.ValueVariant := QrAide_serie.Value;
    FmCTeEveRCan.Edide_nCT.ValueVariant := QrAide_nCT.Value;
    FmCTeEveRCan.EdnProt.Text := QrAnProt.Value;
    if SQLType = stUpd then
    begin
      // N�o tem upd
    end;
    FmCTeEveRCan.ShowModal;
    FmCTeEveRCan.Destroy;
  end;
end;

procedure TFmCTeEveRcab.MostraFormCartaCorrecao(SQLType: TSQLType);
begin
  if (not XXe_PF.PermiteCriacaoDeEvento(tipoxxeCTe, evexxe110110CCe,
  QrAFatID.Value, QrAFatNum.Value, QrAEmpresa.Value)) and (SQLType = stIns) then
  begin
    Geral.MB_Aviso(
    'Voc� deve primeiro enviar a carta criada anteriormente ao FISCO antes de gerar uma nova!');
    Exit;
  end;
  //
  if DBCheck.CriaFm(TFmCTeEveRCCe, FmCTeEveRCCe, afmoNegarComAviso) then
  begin
    FmCTeEveRCCe.ImgTipo.SQLType := SQLType;
    FmCTeEveRCCe.FFatID     := QrAFatID.Value;
    FmCTeEveRCCe.FFatNum    := QrAFatNum.Value;
    FmCTeEveRCCe.FEmpresa   := QrAEmpresa.Value;
    FmCTeEveRCCe.FControle  := 0;
    FmCTeEveRCCe.FConta     := 0;
    if SQLType = stUpd then
    begin
      AbreDadosParaXML(QrCTeEveRCabControle.Value);
      //
      FmCTeEveRCCe.FControle  := QrCTeEveRCCeControle.Value;
      FmCTeEveRCCe.FConta     := QrCTeEveRCCeConta.Value;
      FmCTeEveRCCe.DesfazMsgMemo();
      FmCTeEveRCCe.RGnCondUso.ItemIndex := QrCTeEveRCCenCondUso.Value;
      FmCTeEveRCCe.ReopenCTeEveRCCeIts(0);
    end;
    FmCTeEveRCCe.ShowModal;
    FmCTeEveRCCe.Destroy;
  end;
end;

procedure TFmCTeEveRcab.MostraFormEPEC(SQLType: TSQLType);
begin
  if (not XXe_PF.PermiteCriacaoDeEvento(tipoxxeCTe, evexxe110113EPEC,
  QrAFatID.Value, QrAFatNum.Value, QrAEmpresa.Value)) and (SQLType = stIns) then
    Exit;
  //
  if DBCheck.CriaFm(TFmCTeEveREPEC, FmCTeEveREPEC, afmoNegarComAviso) then
  begin
    FmCTeEveREPEC.ImgTipo.SQLType := SQLType;
    FmCTeEveREPEC.Edide_serie.ValueVariant := QrAide_serie.Value;
    FmCTeEveREPEC.Edide_nCT.ValueVariant := QrAide_nCT.Value;
    if SQLType = stIns then
    begin
      FmCTeEveREPEC.EdvICMS.ValueVariant    := QrAICMS_vICMS.Value;
      FmCTeEveREPEC.EdvTPrest.ValueVariant  := QrAvPrest_vTPrest.Value;
      FmCTeEveREPEC.EdvCarga.ValueVariant   := QrAinfCarga_vCarga.Value;
      FmCTeEveREPEC.EdToma4.ValueVariant    := QrACodInfoTomad.Value;
      FmCTeEveREPEC.RGToma04_toma.ItemIndex := QrAtoma99_toma.Value;
      FmCTeEveREPEC.RGModal.ItemIndex       := QrAide_Modal.Value;
      FmCTeEveREPEC.EdUFIni.ValueVariant    := QrAide_UFIni.Value;
      FmCTeEveREPEC.EdUFFim.ValueVariant    := QrAide_UFFim.Value;
    end else
    if SQLType = stUpd then
    begin
      // N�o tem upd
    end;
    FmCTeEveREPEC.ShowModal;
    FmCTeEveREPEC.Destroy;
  end;
end;

procedure TFmCTeEveRcab.PMCanPopup(Sender: TObject);
begin
  ExcluicancelamentodaCTe1.Enabled :=
    (QrAStatus.Value = 100) and
    (QrCTeEveRCabtpEvento.Value = CO_COD_evexxe110111Can)
    and (QrCTeEveRCab.RecNo = 1) and
    ((QrCTeEveRCabStatus.Value <= 30) or
    (QrCTeEveRCabStatus.Value > 200));
end;

procedure TFmCTeEveRcab.PMCCePopup(Sender: TObject);
begin
  Excluicorreo1.Enabled :=
      (QrAStatus.Value = 100)
    and
      (QrCTeEveRCabtpEvento.Value = CO_COD_evexxe110110CCe)
    and
      ((QrCTeEveRCabStatus.Value <= 30) or
      (QrCTeEveRCabStatus.Value > 200));
  Alteracorreo1.Enabled :=
      Excluicorreo1.Enabled
    and
      (QrCTeEveRCab.RecNo = 1);
  //
  Imprime1.Enabled := (QrCTeEveRCCe.State <> dsInactive) and (QrCTeEveRCCe.RecordCount > 0);
end;

procedure TFmCTeEveRcab.PMEPECPopup(Sender: TObject);
var
  EhMDe: Boolean;
begin
  IncluiEPEC1.Enabled :=
    (QrCTeEveRCabStatus.Value <= 30) or
    (QrCTeEveRCabStatus.Value > 400)
    or
    (
      (QrCTeEveRCabStatus.Value > 200) and
      (QrCTeEveRCabStatus.Value < 300)
    );
  //
  EhMDe :=
    (QrCTeEveRCabtpEvento.Value = CO_COD_evexxe110113EPEC);
  //
  ExcluiEPEC1.Enabled := EhMde and
    ((QrCTeEveRCabStatus.Value <= 30) or
    (QrCTeEveRCabStatus.Value > 200));
end;

procedure TFmCTeEveRcab.QrACalcFields(DataSet: TDataSet);
begin
{
  QrAId_TXT.Value := XXe_PF.FormataID_XXe(QrAId.Value);
  //
  QrAEMIT_ENDERECO.Value := QrAemit_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAemit_xLgr.Value,
    QrAemit_nro.Value, True) + ' ' + QrAemit_xCpl.Value + #13#10 +
    QrAemit_xBairro.Value + #13#10 + QrAemit_xMun.Value + ' - ' +
    QrAemit_UF.Value + #13#10 +  'CEP: ' + Geral.FormataCEP_TT(IntToStr(
    QrAemit_CEP.Value)) + '  ' + QrAemit_xPais.Value;

  //
  QrAEMIT_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAemit_fone.Value);
  //
  QrAEMIT_IE_TXT.Value := Geral.Formata_IE(QrAemit_IE.Value, QrAemit_UF.Value, '??');
  QrAEMIT_IEST_TXT.Value := Geral.Formata_IE(QrAemit_IEST.Value, QrAemit_UF.Value, '??');
  //
  if QrAemit_CNPJ.Value <> '' then
    QrAEMIT_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAemit_CNPJ.Value)
  else
    QrAEMIT_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAemit_CPF.Value);
  //
  //
  ////// DESTINAT�RIO
  //
  //
  if QrAdest_CNPJ.Value <> '' then
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CNPJ.Value)
  else
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CPF.Value);
  //
  QrADEST_ENDERECO.Value := QrAdest_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAdest_xLgr.Value, QrAdest_nro.Value, True) + ' ' + QrAdest_xCpl.Value;
  //
  // 2011-08-25
  if DModG.QrParamsEmpCTeShowURL.Value <> ''  then
    QrAEMIT_ENDERECO.Value := QrAEMIT_ENDERECO.Value + #13#10 +
      DModG.QrParamsEmpCTeShowURL.Value;
  if DModG.QrParamsEmpCTeMaiusc.Value = 1  then
  begin
    QrADEST_ENDERECO.Value := AnsiUpperCase(QrADEST_ENDERECO.Value);
    QrADEST_XMUN_TXT.Value := AnsiUpperCase(QrAdest_xMun.Value);
  end else
  begin
    QrADEST_XMUN_TXT.Value := QrAdest_xMun.Value;
  end;
  // Fim 2011-08-25
  //
  QrADEST_CEP_TXT.Value := Geral.FormataCEP_TT(QrAdest_CEP.Value);
  QrADEST_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAdest_fone.Value);
  //
  QrADEST_IE_TXT.Value := Geral.Formata_IE(QrAdest_IE.Value, QrAdest_UF.Value, '??');
  //
  //
  //
  ////// TRANSPORTADORA - REBOQUE???
  //
  //
  if Geral.SoNumero1a9_TT(QrAtransporta_CNPJ.Value) <> '' then
    QrATRANSPORTA_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAtransporta_CNPJ.Value)
  else
    QrATRANSPORTA_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAtransporta_CPF.Value);
  //
  QrATRANSPORTA_IE_TXT.Value := Geral.Formata_IE(QrAtransporta_IE.Value, QrAtransporta_UF.Value, '??');
  //
  if QrAide_tpAmb.Value = 2 then
  begin
    QrADOC_SEM_VLR_JUR.Value :=
      'CT-e emitido em ambiente de homologa��o. N�O POSSUI VALIDADE JUR�DICA';
    QrADOC_SEM_VLR_FIS.Value := 'SEM VALOR FISCAL';
  end else begin
    QrADOC_SEM_VLR_JUR.Value := '';
    QrADOC_SEM_VLR_FIS.Value := QrAinfProt_nProt.Value + ' ' + Geral.FDT(QrAinfProt_dhRecbto.Value, 00);
  end;
  QrAide_DSaiEnt_Txt.Value := dmkPF.FDT_NULO(QrAide_DSaiEnt.Value, 2);
  //
  //
(* p�g 12 da Nota T�cnica 2010/004 de junho/2010
0 � Emitente;
1 � Dest/Rem;
2 � Terceiros;
9 � Sem Frete;
*)
  case QrAModFrete.Value of
    0: QrAMODFRETE_TXT.Value := '0 � Emitente';
    1: QrAMODFRETE_TXT.Value := '1 � Dest/Rem';
    2: QrAMODFRETE_TXT.Value := '2 � Terceiros';
    9: QrAMODFRETE_TXT.Value := '9 � Sem Frete';
    else QrAMODFRETE_TXT.Value := FormatFloat('0', QrAModFrete.Value) + ' - ? ? ? ? ';
  end;
}
end;

procedure TFmCTeEveRcab.QrAAfterOpen(DataSet: TDataSet);
begin
  DmCTe_0000.ReopenEmpresa(QrAEmpresa.Value);
end;

procedure TFmCTeEveRcab.QrAAfterScroll(DataSet: TDataSet);
begin
  BtCCe.Enabled  := QrAEmpresa.Value = QrACodInfoEmite.Value;
  BtCan.Enabled  := QrAEmpresa.Value = QrACodInfoEmite.Value;
  BtEPEC.Enabled := QrAEmpresa.Value = QrACodInfoEmite.Value;
  ReopenCTeEveRCab(0);
  DmCTe_0000.ReopenOpcoesCTe(QrOpcoesCTe, QrAEmpresa.Value, True)
end;

procedure TFmCTeEveRcab.QrABeforeClose(DataSet: TDataSet);
begin
  QrCTeEveRCab.Close;
end;

procedure TFmCTeEveRcab.QrCTeEveRCabAfterScroll(DataSet: TDataSet);
begin
  QrCTeEveRCCe.Close;
  QrCTeEveRCan.Close;
  ReopenCTeEveRCCe(0);
  ReopenCTeEveRCCeIts(0);
  ReopenCTeEveRCan(0);
  ReopenCTeEveREPEC(0);
end;

procedure TFmCTeEveRcab.QrCTeEveRCabBeforeClose(DataSet: TDataSet);
begin
  QrCTeEveRCCe.Close;
  QrCTeEveRCCeIts.Close;
  QrCTeEveRCan.Close;
  QrCTeEveREPEC.Close;
end;

procedure TFmCTeEveRcab.QrCTeEveRCabCalcFields(DataSet: TDataSet);
begin
  QrCTeEveRCabtpAmb_TXT.Value := XXe_PF.TextoAmbiente(QrCTeEveRCabtpAmb.Value);
end;

procedure TFmCTeEveRcab.ReopenCTeCabA(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrA, Dmod.MyDB, [
  'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cli, ',
  'IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome) NO_Emi, ',
  'IF(nfa.FatID=53,  ',
  'IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome), ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome)) NO_Terceiro, ',
  'nfa.FatID, nfa.FatNum, nfa.Empresa, nfa.LoteEnv, nfa.Id, ',
  'ide_natOp, ide_serie, ide_nCT, ide_dEmi, ide_tpCTe, ',
  'vPrest_vTPrest, vPrest_vRec, dest_CNPJ, dest_CPF,  ',
  'dest_xNome,  ',
  'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) + 0.000 cStat, ',
  'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt, ',
  'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo, ',
  'IF(infCanc_cStat>0, DATE_FORMAT(infCanc_dhRecbto, "%d/%m/%Y %H:%i:%S"), ',
  'DATE_FORMAT(infProt_dhRecbto, "%d/%m/%Y %H:%i:%S")) dhRecbto, ',
  'IDCtrl, versao,  ',
  'ide_tpEmis, infCanc_xJust, Status,  ',
  'nfa.ICMS_vICMS, nfa.CodInfoEmite, ',
  'cSitCTe, nfa.infCanc_dhRecbto, nfa.infCanc_nProt,  ',
  'nfa.ide_Modal, nfa.infCarga_vCarga, nfa.CodInfoTomad, ',
  'nfa.ide_UFIni, nfa.ide_UFFim, nfa.toma99_toma ',
  'FROM ctecaba nfa ',
  'LEFT JOIN entidades cli ON cli.Codigo=nfa.CodInfoTomad ',
  'LEFT JOIN entidades emi ON emi.Codigo=nfa.CodInfoEmite ',
(*
  'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cli, ',
  'IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome) NO_Emi, ',
  'IF(nfa.FatID=53, ',
  'IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome), ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome)) NO_Terceiro, ',
  'nfa.FatID, nfa.FatNum, nfa.Empresa, nfa.LoteEnv, nfa.Id, ',
  'ide_natOp, ide_serie, ide_nCT, ide_dEmi, ide_tpCTe, ',
  'vPrest_vTPrest, vPrest_vRec, dest_CNPJ, dest_CPF, ',
  'dest_xNome, ',
  'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) + 0.000 cStat, ',
  'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt, ',
  'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo, ',
  'IF(infCanc_cStat>0, DATE_FORMAT(infCanc_dhRecbto, "%d/%m/%Y %H:%i:%S"), ',
  'DATE_FORMAT(infProt_dhRecbto, "%d/%m/%Y %H:%i:%S")) dhRecbto, ',
  'IDCtrl, versao, ',
  'ide_tpEmis, infCanc_xJust, Status, ',
  ' ',
  'nfa.ICMS_vICMS, nfa.CodInfoEmite, ',
  ' ',
  'cSitCTe, cSitConf, ',
  'ELT(cSitConf+2, "N�o consultada", "Sem manifesta��o", "Confirmada", ',
  '"Desconhecida", "N�o realizada", "Ci�ncia", "? ? ?") NO_cSitConf, ',
  'nfa.infCanc_dhRecbto, nfa.infCanc_nProt ',
  'FROM ctecaba nfa ',
  'LEFT JOIN entidades cli ON cli.Codigo=nfa.CodInfoTomad ',
  'LEFT JOIN entidades emi ON emi.Codigo=nfa.CodInfoEmite ',
*)
  ' ',
  'WHERE nfa.FatID=' + Geral.FF0(FatID),
  'AND nfa.FatNum=' + Geral.FF0(FatNum),
  'AND nfa.Empresa=' + Geral.FF0(Empresa),
  'ORDER BY nfa.ide_dEmi DESC, nfa.ide_nCT DESC ',
  '']);
end;

procedure TFmCTeEveRcab.ReopenCTeEveRCab(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeEveRCab, Dmod.MyDB, [
  'SELECT * FROM cteevercab ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  'ORDER BY Controle DESC ',
  '']);
  //
  if (Controle <> 0) then
    QrCTeEveRCab.Locate('Controle', Controle, []);
end;

procedure TFmCTeEveRcab.ReopenCTeEveRCan(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeEveRCan, Dmod.MyDB, [
  'SELECT * FROM cteevercan ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  'AND Controle=' + Geral.FF0(QrCTeEveRCabControle.Value),
  'ORDER BY Conta DESC ',
  '']);
  //
  if (Conta <> 0) then
    QrCTeEveRCan.Locate('Conta', Conta, []);
end;

procedure TFmCTeEveRcab.ReopenCTeEveRCCe(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeEveRCCe, Dmod.MyDB, [
  'SELECT * FROM cteevercce ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  'AND Controle=' + Geral.FF0(QrCTeEveRCabControle.Value),
  'ORDER BY Conta DESC ',
  '']);
  //
  if (Conta <> 0) then
    QrCTeEveRCCe.Locate('Conta', Conta, []);
end;

procedure TFmCTeEveRcab.ReopenCTeEveRCCeIts(IDItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeEveRCCeIts, Dmod.MyDB, [
  'SELECT LEFT(its.ValorAlterado, 255) Valor255, its.*  ',
  'FROM cteevercceits its ',
  'WHERE its.FatID=' + Geral.FF0(QrAFatID.Value),
  'AND its.FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND its.Empresa=' + Geral.FF0(QrAEmpresa.Value),
  'AND its.Controle=' + Geral.FF0(QrCTeEveRCabControle.Value),
  'AND its.Conta=' + Geral.FF0(QrCTeEveRCCeConta.Value),
  '']);
end;

procedure TFmCTeEveRcab.ReopenCTeEveREPEC(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeEveREPEC, Dmod.MyDB, [
  'SELECT * FROM cteeverepec ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  'AND Controle=' + Geral.FF0(QrCTeEveRCabControle.Value),
  'ORDER BY Conta DESC ',
  '']);
  //
  if (Conta <> 0) then
    QrCTeEveREPEC.Locate('Conta', Conta, []);
end;

procedure TFmCTeEveRcab.Solicitacancelamento1Click(Sender: TObject);
var
  Empresa, Modelo, Serie, nCT, nSeqEvento, Justif: Integer;
  nProtCan, chCTe: String;
begin
(*
  Empresa     := QrCTeEveRCanEmpresa.Value;
  Modelo      := QrCTeEveRCan.Value;
  Serie       := QrCTeEveRCan.Value;
  nCT         := QrCTeEveRCan.Value;
  nSeqEvento  := QrCTeEveRCan.Value;
  Justif      := QrCTeEveRCan.Value;
  nProtCan    := QrCTeEveRCan.Value;
  chCTe       := QrCTeEveRCan.Value;
  //
  CTe_PF.MostraFormStepsCTe_Cancelamento(Empresa, Modelo, Serie, nCT,
              nSeqEvento, Justif: Integer; nProtCan, chCTe: String);
*)
end;


// TODO :      ver retirada de evento de lote }
{ TODO :      mostrar status do evento processado }

end.
