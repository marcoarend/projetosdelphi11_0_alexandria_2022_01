object FmCTeEveRCCeIts: TFmCTeEveRCCeIts
  Left = 339
  Top = 185
  Caption = 'CTe-EVENT-004 :: Item de Carta de Corre'#231#227'o'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 322
        Height = 32
        Caption = 'Item de Carta de Corre'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 322
        Height = 32
        Caption = 'Item de Carta de Corre'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 322
        Height = 32
        Caption = 'Item de Carta de Corre'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 467
        Align = alClient
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 808
          Height = 105
          Align = alTop
          Caption = 'GroupBox2'
          TabOrder = 0
          object Panel5: TPanel
            Left = 356
            Top = 32
            Width = 185
            Height = 41
            Caption = 'Panel5'
            TabOrder = 0
          end
        end
        object GroupBox3: TGroupBox
          Left = 2
          Top = 120
          Width = 808
          Height = 345
          Align = alClient
          Caption = 'GroupBox3'
          TabOrder = 1
          object Panel6: TPanel
            Left = 2
            Top = 15
            Width = 804
            Height = 166
            Align = alTop
            TabOrder = 0
            object Label1: TLabel
              Left = 340
              Top = 4
              Width = 32
              Height = 13
              Caption = 'Grupo:'
            end
            object Label2: TLabel
              Left = 12
              Top = 44
              Width = 98
              Height = 13
              Caption = 'Descri'#231#227'o do Grupo:'
              FocusControl = DBEdit1
            end
            object Label3: TLabel
              Left = 12
              Top = 84
              Width = 36
              Height = 13
              Caption = 'Campo:'
            end
            object Label4: TLabel
              Left = 12
              Top = 124
              Width = 101
              Height = 13
              Caption = 'Descri'#231#227'o do campo:'
              FocusControl = DBEdit2
            end
            object LanroItemAlterado: TLabel
              Left = 412
              Top = 84
              Width = 23
              Height = 13
              Caption = 'Item:'
            end
            object Label5: TLabel
              Left = 12
              Top = 4
              Width = 45
              Height = 13
              Caption = 'Estrutura:'
            end
            object EdGrupo: TdmkEditCB
              Left = 340
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdGrupoRedefinido
              DBLookupComboBox = CBGrupo
              IgnoraDBLookupComboBox = False
            end
            object CBGrupo: TdmkDBLookupComboBox
              Left = 396
              Top = 20
              Width = 337
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Campo'
              ListSource = DsGrupos
              TabOrder = 3
              dmkEditCB = EdGrupo
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object DBEdit1: TDBEdit
              Left = 12
              Top = 60
              Width = 720
              Height = 21
              TabStop = False
              DataField = 'Descricao'
              DataSource = DsGrupos
              TabOrder = 4
              Visible = False
            end
            object EdCampo: TdmkEditCB
              Left = 12
              Top = 100
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdCampoRedefinido
              DBLookupComboBox = CBCampo
              IgnoraDBLookupComboBox = False
            end
            object CBCampo: TdmkDBLookupComboBox
              Left = 68
              Top = 100
              Width = 337
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Campo'
              ListSource = DsCampos
              TabOrder = 6
              dmkEditCB = EdCampo
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object DBEdit2: TDBEdit
              Left = 12
              Top = 140
              Width = 720
              Height = 21
              TabStop = False
              DataField = 'Descricao'
              DataSource = DsCampos
              TabOrder = 8
              Visible = False
            end
            object EdnroItemAlterado: TdmkEdit
              Left = 412
              Top = 100
              Width = 45
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object CBLayC: TdmkDBLookupComboBox
              Left = 68
              Top = 20
              Width = 269
              Height = 21
              KeyField = 'Grupo'
              ListField = 'Nome'
              ListSource = DsLayC
              TabOrder = 1
              dmkEditCB = EdLayC
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdLayC: TdmkEditCB
              Left = 12
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdLayCRedefinido
              DBLookupComboBox = CBLayC
              IgnoraDBLookupComboBox = False
            end
          end
          object PCTipoValor: TPageControl
            Left = 2
            Top = 181
            Width = 804
            Height = 162
            ActivePage = TabSheet0
            Align = alClient
            TabOrder = 1
            object TabSheet0: TTabSheet
              Caption = 'Texto'
              object Panel8: TPanel
                Left = 0
                Top = 0
                Width = 796
                Height = 134
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object LaTexto: TLabel
                  Left = 8
                  Top = 8
                  Width = 39
                  Height = 13
                  Caption = 'LaTexto'
                end
                object EdTexto: TdmkEdit
                  Left = 8
                  Top = 24
                  Width = 781
                  Height = 21
                  TabOrder = 0
                  Visible = False
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnExit = EdTextoExit
                end
                object MeTexto: TdmkMemo
                  Left = 8
                  Top = 32
                  Width = 781
                  Height = 101
                  TabOrder = 1
                  Visible = False
                  UpdType = utYes
                end
              end
            end
            object TabSheet1: TTabSheet
              Caption = 'Inteiro'
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel9: TPanel
                Left = 0
                Top = 0
                Width = 796
                Height = 134
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object LaInteiro: TLabel
                  Left = 8
                  Top = 8
                  Width = 44
                  Height = 13
                  Caption = 'LaInteiro:'
                end
                object EdInteiro: TdmkEdit
                  Left = 8
                  Top = 24
                  Width = 132
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
              end
            end
            object TabSheet2: TTabSheet
              Caption = 'Decimal'
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel10: TPanel
                Left = 0
                Top = 0
                Width = 796
                Height = 134
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object LaDecimal: TLabel
                  Left = 8
                  Top = 8
                  Width = 50
                  Height = 13
                  Caption = 'LaDecimal'
                end
                object EdDecimal: TdmkEdit
                  Left = 8
                  Top = 24
                  Width = 100
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
            object TabSheet3: TTabSheet
              Caption = 'Data / hora'
              ImageIndex = 3
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel7: TPanel
                Left = 0
                Top = 0
                Width = 796
                Height = 134
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object LaData: TLabel
                  Left = 8
                  Top = 4
                  Width = 26
                  Height = 13
                  Caption = 'Data:'
                  Visible = False
                end
                object LaHora: TLabel
                  Left = 156
                  Top = 4
                  Width = 26
                  Height = 13
                  Caption = 'Hora:'
                  Visible = False
                end
                object TPData: TdmkEditDateTimePicker
                  Left = 8
                  Top = 20
                  Width = 145
                  Height = 21
                  Date = 40656.000000000000000000
                  Time = 40656.000000000000000000
                  ShowCheckbox = True
                  TabOrder = 0
                  Visible = False
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                end
                object EdHora: TdmkEdit
                  Left = 156
                  Top = 20
                  Width = 80
                  Height = 21
                  TabOrder = 1
                  Visible = False
                  FormatType = dmktfTime
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '00:00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkContinuar: TCheckBox
        Left = 152
        Top = 16
        Width = 117
        Height = 17
        Caption = 'Continuar inserindo.'
        TabOrder = 1
      end
    end
  end
  object QrGrupos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctelayi'
      'WHERE Elemento="G"'
      'OR Elemento="CG"'
      'ORDER BY Campo, Codigo')
    Left = 104
    Top = 92
    object QrGruposVersao: TFloatField
      FieldName = 'Versao'
    end
    object QrGruposGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrGruposCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGruposCampo: TWideStringField
      FieldName = 'Campo'
      Size = 30
    end
    object QrGruposNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrGruposDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object QrGruposElemento: TWideStringField
      FieldName = 'Elemento'
      Size = 10
    end
    object QrGruposTipo: TWideStringField
      FieldName = 'Tipo'
      Size = 1
    end
    object QrGruposOcorMin: TSmallintField
      FieldName = 'OcorMin'
    end
    object QrGruposOcorMax: TIntegerField
      FieldName = 'OcorMax'
    end
    object QrGruposTamMin: TSmallintField
      FieldName = 'TamMin'
    end
    object QrGruposTamMax: TIntegerField
      FieldName = 'TamMax'
    end
    object QrGruposTamVar: TWideStringField
      FieldName = 'TamVar'
      Size = 30
    end
    object QrGruposObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrGruposInfoVazio: TSmallintField
      FieldName = 'InfoVazio'
    end
    object QrGruposDominio: TWideStringField
      FieldName = 'Dominio'
      Size = 10
    end
    object QrGruposExpReg: TWideStringField
      FieldName = 'ExpReg'
      Size = 10
    end
    object QrGruposFormatStr: TWideStringField
      FieldName = 'FormatStr'
      Size = 30
    end
    object QrGruposDeciCasas: TSmallintField
      FieldName = 'DeciCasas'
    end
    object QrGruposCartaCorrige: TSmallintField
      FieldName = 'CartaCorrige'
    end
    object QrGruposAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGruposAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsGrupos: TDataSource
    DataSet = QrGrupos
    Left = 104
    Top = 140
  end
  object QrCampos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctelayi'
      'WHERE Elemento="G"'
      'OR Elemento="CG"'
      'ORDER BY Campo, Codigo')
    Left = 188
    Top = 92
    object QrCamposVersao: TFloatField
      FieldName = 'Versao'
    end
    object QrCamposGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrCamposCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCamposCampo: TWideStringField
      FieldName = 'Campo'
      Size = 30
    end
    object QrCamposNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrCamposDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object QrCamposElemento: TWideStringField
      FieldName = 'Elemento'
      Size = 10
    end
    object QrCamposTipo: TWideStringField
      FieldName = 'Tipo'
      Size = 1
    end
    object QrCamposOcorMin: TSmallintField
      FieldName = 'OcorMin'
    end
    object QrCamposOcorMax: TIntegerField
      FieldName = 'OcorMax'
    end
    object QrCamposTamMin: TSmallintField
      FieldName = 'TamMin'
    end
    object QrCamposTamMax: TIntegerField
      FieldName = 'TamMax'
    end
    object QrCamposTamVar: TWideStringField
      FieldName = 'TamVar'
      Size = 30
    end
    object QrCamposObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCamposInfoVazio: TSmallintField
      FieldName = 'InfoVazio'
    end
    object QrCamposDominio: TWideStringField
      FieldName = 'Dominio'
      Size = 10
    end
    object QrCamposExpReg: TWideStringField
      FieldName = 'ExpReg'
      Size = 10
    end
    object QrCamposFormatStr: TWideStringField
      FieldName = 'FormatStr'
      Size = 30
    end
    object QrCamposDeciCasas: TSmallintField
      FieldName = 'DeciCasas'
    end
    object QrCamposCartaCorrige: TSmallintField
      FieldName = 'CartaCorrige'
    end
    object QrCamposAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCamposAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsCampos: TDataSource
    DataSet = QrCampos
    Left = 188
    Top = 140
  end
  object QrNextGru: TmySQLQuery
    Database = DModG.AllID_DB
    Left = 260
    Top = 88
    object QrNextGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrLayC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctelayc'
      'ORDER BY Nome')
    Left = 32
    Top = 80
    object QrLayCGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrLayCNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsLayC: TDataSource
    DataSet = QrLayC
    Left = 32
    Top = 128
  end
end
