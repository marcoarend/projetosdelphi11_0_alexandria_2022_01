unit CTeEveRLoE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkValUsu,
  dmkDBLookupComboBox, dmkEditCB, Menus, Grids, DBGrids, UrlMon, ComCtrls,
  OleCtrls, SHDocVw, UnDmkProcFunc, dmkImage, DmkDAC_PF,
  UnDmkEnums;

type
  TFmCTeEveRLoE = class(TForm)
    PainelDados: TPanel;
    DsCTeEveRLoE: TDataSource;
    QrCTeEveRLoE: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    VuEmpresa: TdmkValUsu;
    QrCTeEveRLoEDataHora_TXT: TWideStringField;
    PMLote: TPopupMenu;
    Incluinovolote1: TMenuItem;
    Alteraloteatual1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    PMEvento: TPopupMenu;
    DsCTeEveRCab: TDataSource;
    QrCTeEveRCab: TmySQLQuery;
    N1: TMenuItem;
    Envialoteaofisco1: TMenuItem;
    QrCTeEveRLoENO_Ambiente: TWideStringField;
    N2: TMenuItem;
    Lerarquivo1: TMenuItem;
    PainelData: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    WB_XML: TWebBrowser;
    PMXML: TPopupMenu;
    AbrirXMLnonavegadordestajanela1: TMenuItem;
    SelecionaroXML1: TMenuItem;
    Lote1: TMenuItem;
    Assinada1: TMenuItem;
    Gerada2: TMenuItem;
    AbrirXMLdaCTEnonavegadorpadro1: TMenuItem;
    Assina1: TMenuItem;
    Gerada1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBEdita: TGroupBox;
    Panel6: TPanel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label8: TLabel;
    EdCodUsu: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtLote: TBitBtn;
    BtEvento: TBitBtn;
    BtXML: TBitBtn;
    GroupBox1: TGroupBox;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    GroupBox3: TGroupBox;
    Label16: TLabel;
    Label10: TLabel;
    Label17: TLabel;
    DBEdit13: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdCodigo: TDBEdit;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCTeEveRLoECodigo: TIntegerField;
    QrCTeEveRLoECodUsu: TIntegerField;
    QrCTeEveRLoENome: TWideStringField;
    QrCTeEveRLoEEmpresa: TIntegerField;
    QrCTeEveRLoEversao: TFloatField;
    QrCTeEveRLoEtpAmb: TSmallintField;
    QrCTeEveRLoEverAplic: TWideStringField;
    QrCTeEveRLoEcOrgao: TSmallintField;
    QrCTeEveRLoEcStat: TSmallintField;
    QrCTeEveRLoExMotivo: TWideStringField;
    QrCTeEveRLoELk: TIntegerField;
    QrCTeEveRLoEDataCad: TDateField;
    QrCTeEveRLoEDataAlt: TDateField;
    QrCTeEveRLoEUserCad: TIntegerField;
    QrCTeEveRLoEUserAlt: TIntegerField;
    QrCTeEveRLoEAlterWeb: TSmallintField;
    QrCTeEveRLoEAtivo: TSmallintField;
    QrCTeEveRLoEFilial: TIntegerField;
    QrCTeEveRLoENO_Empresa: TWideStringField;
    IncluiEventoaoloteatual1: TMenuItem;
    RemoveEventosselecionados1: TMenuItem;
    DBGrid2: TDBGrid;
    QrCTeEveRCabFatID: TIntegerField;
    QrCTeEveRCabFatNum: TIntegerField;
    QrCTeEveRCabEmpresa: TIntegerField;
    QrCTeEveRCabControle: TIntegerField;
    QrCTeEveRCabtpAmb: TSmallintField;
    QrCTeEveRCabchCTe: TWideStringField;
    QrCTeEveRCabdhEvento: TDateTimeField;
    QrCTeEveRCabtpEvento: TIntegerField;
    QrCTeEveRCabdescEvento: TWideStringField;
    QrCTeEveRCabchCTe_CT_SER: TWideStringField;
    QrCTeEveRCabchCTe_CT_NUM: TWideStringField;
    QrCTeEveRCabret_cStat: TIntegerField;
    QrCTeEveRCabStatus: TIntegerField;
    BtEnvia: TBitBtn;
    QrProt: TmySQLQuery;
    QrCabA: TmySQLQuery;
    QrCTeEveRCabret_nProt: TWideStringField;
    QrCTeEveRCabret_dhRegEvento: TDateTimeField;
    QrProtItens: TLargeintField;
    QrCTeEveRCabXML_Eve: TWideMemoField;
    QrCTeEveRCabXML_retEve: TWideMemoField;
    QrCTeEveRCabSTATUS_TXT: TWideStringField;
    QrCTeEveRCabversao: TFloatField;
    QrCTeEveRCabcOrgao: TSmallintField;
    QrCTeEveRCabUF_Nome: TWideStringField;
    QrCTeEveRCabCNPJ_CPF: TWideStringField;
    QrCTeEveRCabnSeqEvento: TSmallintField;
    QrCTeEveRCabverEvento: TFloatField;
    QrCTeEveRCCe: TmySQLQuery;
    QrCTeEveRCCeConta: TIntegerField;
    QrCTeEveRCCenCondUso: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCTeEveRLoEAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCTeEveRLoEBeforeOpen(DataSet: TDataSet);
    procedure BtLoteClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrCTeEveRLoECalcFields(DataSet: TDataSet);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure Incluinovolote1Click(Sender: TObject);
    procedure PMLotePopup(Sender: TObject);
    procedure PMEventoPopup(Sender: TObject);
    procedure QrCTeEveRLoEAfterScroll(DataSet: TDataSet);
    procedure Envialoteaofisco1Click(Sender: TObject);
    procedure Verificalotenofisco1Click(Sender: TObject);
    procedure Gerada1Click(Sender: TObject);
    procedure Assina1Click(Sender: TObject);
    procedure Gerada2Click(Sender: TObject);
    procedure Assinada1Click(Sender: TObject);
    procedure SelecionaroXML1Click(Sender: TObject);
    procedure BtXMLClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Lerarquivo1Click(Sender: TObject);
    procedure BtEventoClick(Sender: TObject);
    procedure IncluiEventoaoloteatual1Click(Sender: TObject);
    procedure RemoveEventosselecionados1Click(Sender: TObject);
    procedure QrCTeEveRCabCalcFields(DataSet: TDataSet);
    procedure BtEnviaClick(Sender: TObject);
  private
    FDirCTeXML: String;
    FPreEmail: Integer;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    function  ImpedePelaMisturaDeWebServices(var UF_Servico: String): Boolean;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenCTeEveRCab();
  end;

var
  FmCTeEveRLoE: TFmCTeEveRLoE;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, ModuleCTe_0000,
  CTeEveRLoEAdd, UnMailEnv, UnCTe_Tabs, UnCTe_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCTeEveRLoE.Lerarquivo1Click(Sender: TObject);
const
  SohLer = True;
var
  CodsEve: String;
  EventoLote, Empresa: Integer;
begin
(***
  CodsEve    := CTe_CodsEveWbserverUserDef;
  EventoLote := QrCTeEveRLoECodigo.Value;
  Empresa    := QrCTeEveRLoEEmpresa.Value;
  //
  CTe_PF.MostraFormStepsCTe_CartaDeCorrecao(SohLer, CodsEve, EventoLote, Empresa);
  LocCod(EventoLote, EventoLote);
*)
end;

procedure TFmCTeEveRLoE.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCTeEveRLoE.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCTeEveRLoECodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmCTeEveRLoE.Verificalotenofisco1Click(Sender: TObject);
begin
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCTeEveRLoE.DefParams;
begin
  VAR_GOTOTABELA := 'nfeeverloe';
  VAR_GOTOMYSQLTABLE := QrCTeEveRLoE;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT lot.*, ent.Filial, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Empresa');
  VAR_SQLx.Add('FROM nfeeverloe lot');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa');
  VAR_SQLx.Add('WHERE lot.Codigo>-1000');
  VAR_SQLx.Add('AND lot.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND lot.Codigo=:P0');
  //
  VAR_SQL2.Add('AND lot.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND lot.Nome Like :P0');
  //
end;

procedure TFmCTeEveRLoE.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'nfeeverloe', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmCTeEveRLoE.Envialoteaofisco1Click(Sender: TObject);
const
  SohLer = False;
var
  CodsEve: String;
  EventoLote, Empresa: Integer;
begin
(***
  CodsEve    := CTe_CodsEveWbserverUserDef;
  EventoLote := QrCTeEveRLoECodigo.Value;
  Empresa    := QrCTeEveRLoEEmpresa.Value;
  //
  CTe_PF.MostraFormStepsCTe_CartaDeCorrecao(SohLer, CodsEve, EventoLote, Empresa);
  LocCod(EventoLote, EventoLote);
  ReopenCTeEveRCab();
*)
end;

procedure TFmCTeEveRLoE.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCTeEveRLoE.PMLotePopup(Sender: TObject);
var
  HabilitaA, HabilitaB, HabilitaC, HabilitaD, HabilitaE, HabilitaF: Boolean;
begin
  HabilitaA := (QrCTeEveRLoE.State = dsBrowse) and (QrCTeEveRLoE.RecordCount > 0);
  HabilitaC := (QrCTeEveRCab.State = dsBrowse) and (QrCTeEveRCab.RecordCount > 0);
  // Leituras de arquivos
  LerArquivo1.Enabled      := HabilitaA;
  //
  HabilitaB := HabilitaA and (QrCTeEveRLoE.RecordCount = 0);
  Alteraloteatual1.Enabled := HabilitaB;
  //
  HabilitaF := not (QrCTeEveRLoEcStat.Value in [103,104,105]);
  HabilitaE := not (QrCTeEveRCabStatus.Value in [135, 136]);
  HabilitaD := HabilitaF and HabilitaE;
  HabilitaB := HabilitaA and HabilitaC and HabilitaD;

  Envialoteaofisco1.Enabled := HabilitaB;
  (*
  Envialoteaofisco1.Enabled    := True;
  Verificalotenofisco1.Enabled := True;
  (*desmarcar em testes*)
end;

procedure TFmCTeEveRLoE.PMEventoPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrCTeEveRLoE.RecordCount > 0) and (QrCTeEveRLoEcStat.Value <> 103);
  Alteraloteatual1.Enabled := Habilita;
end;

procedure TFmCTeEveRLoE.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCTeEveRLoE.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCTeEveRLoE.RemoveEventosselecionados1Click(Sender: TObject);
var
  Status: Integer;
begin
  if (QrCTeEveRLoEcStat.Value < 100) or (not (QrCTeEveRCabret_cStat.Value in ([128,129])))
  or ((QrCTeEveRLoEcStat.Value in ([130])) and (QrCTeEveRCabret_cStat.Value > 200)) then
  begin
    if Geral.MB_Pergunta('Deseja realmente tirar o evento deste lote?') = ID_YES then
    begin
      if (QrCTeEveRCabStatus.Value = 50)
      or ((QrCTeEveRLoEcStat.Value = 130) and (QrCTeEveRCabret_cStat.Value > 200)) then
        Status := 30
      else
        Status := QrCTeEveRCabStatus.Value;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE nfeevercab SET EventoLote = 0, Status=:P0 ');
      Dmod.QrUpd.SQL.Add('WHERE FatID=:P1 AND FatNum=:P2 ');
      Dmod.QrUpd.SQL.Add('AND Empresa=:P3 AND Controle=:P4');
      Dmod.QrUpd.Params[00].AsInteger := Status;
      Dmod.QrUpd.Params[01].AsInteger := QrCTeEveRCabFatID.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrCTeEveRCabFatNum.Value;
      Dmod.QrUpd.Params[03].AsInteger := QrCTeEveRCabEmpresa.Value;
      Dmod.QrUpd.Params[04].AsInteger := QrCTeEveRCabControle.Value;
      Dmod.QrUpd.ExecSQL;
      ReopenCTeEveRCab();
    end;
  end else
    Geral.MB_Aviso('A��o n�o permitida!');
end;

procedure TFmCTeEveRLoE.ReopenCTeEveRCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeEveRCab, Dmod.MyDB, [
    'SELECT nec.FatID, nec.FatNum, nec.Empresa, nec.Controle, nec.versao,',
    'nec.tpAmb, nec.chCTe, nec.dhEvento, nec.tpEvento, nec.descEvento,',
    'nec.Status, nec.ret_cStat, nec.ret_nProt, nec.ret_dhRegEvento, nec.verEvento,',
    'nec.XML_Eve, nec.XML_retEve, nec.versao, duf.Nome UF_Nome, nec.cOrgao,',
    'IF(nec.CNPJ<>"", nec.CNPJ, nec.CPF) CNPJ_CPF, nec.nSeqEvento',
    'FROM cteevercab nec',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_ufs duf ON duf.Codigo=nec.cOrgao',
    'WHERE nec.EventoLote=' + Geral.FF0(QrCTeEveRLoECodigo.Value),
    '']);
end;

procedure TFmCTeEveRLoE.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCTeEveRLoE.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCTeEveRLoE.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCTeEveRLoE.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCTeEveRLoE.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCTeEveRLoE.Alteraloteatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrCTeEveRLoE, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'nfeeverloe');
end;

procedure TFmCTeEveRLoE.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCTeEveRLoECodigo.Value;
  Close;
end;

procedure TFmCTeEveRLoE.BtXMLClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMXML, BtXML);
end;

procedure TFmCTeEveRLoE.Assina1Click(Sender: TObject);
begin
{
  DmCTe_0000.AbreXML_NoNavegadorPadrao(QrCTeEveRLoEEmpresa.Value, CTE_EXT_CTE_XML,
    QrCTeEveRCabid.Value, True);
}
end;

procedure TFmCTeEveRLoE.Assinada1Click(Sender: TObject);
begin
{
  DmCTe_0000.AbreXML_NoTWebBrowser(QrCTeEveRLoEEmpresa.Value, CTE_EXT_CTE_XML,
    QrCTeEveRCabid.Value, True, WB_XML);
}
end;

procedure TFmCTeEveRLoE.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeeverloe', '', 0, 999999999, '');
  EdCodigo.ValueVariant := Codigo;
  EdCodUsu.ValueVariant := Codigo;
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PainelEdita,
    'nfeeverloe', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCTeEveRLoE.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'nfeeverloe', 'Codigo');
  MostraEdicao(0, stLok, 0);
end;

procedure TFmCTeEveRLoE.BtEnviaClick(Sender: TObject);
  procedure ExportaXMLCCE(var DiretXML: String);
  var
    Continua: Boolean;
    Protocolo, ArqXML, XML_Distribuicao, Versao: String;
  begin
    Continua := False;
    DiretXML := '';
    //
    // XML
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Verificando exist�ncia do arquivo XML da Carta de Corre��o');
    //
    Protocolo := Geral.SoNumero_TT(QrCTeEveRCabret_nProt.Value);
    ArqXML    := DmCTe_0000.QrFilialDirCTeEveProcCCe.Value;
    //
    Geral.VerificaDir(ArqXML, '\', 'Diret�rio tempor�rio do XML de CTe', True);
    //
    ArqXML := ArqXML + Protocolo + CTE_EXT_EVE_PRO_CCE_XML;
    //
    if not FileExists(ArqXML) then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando arquivo XML da Carta de Corre��o');
      //
      XML_Distribuicao := '';
      //
      Versao := Geral.FFT_Dot(DmCTe_0000.QrOpcoesCTeCTeVerLotEve.Value, 2, siPositivo);
      DmCTe_0000.ReopenOpcoesCTe(DmCTe_0000.QrOpcoesCTe, QrCTeEveRCabEmpresa.Value, True);
      if CTe_PF.XML_DistribuiCartaDeCorrecao((*verProcCCeCTe_Versao*) Versao,
        QrCTeEveRCabXML_Eve.Value, QrCTeEveRCabXML_retEve.Value, XML_Distribuicao)
      then
        Continua := CTe_PF.SalvaXML(ArqXML, XML_Distribuicao);
    end else
      Continua := True;
    if Continua then
      DiretXML := ArqXML;
  end;

  procedure ExportaXMLCan(var DiretXML: String);
  var
    Continua: Boolean;
    Protocolo, ArqXML, XML_Distribuicao: String;
  begin
    Continua := False;
    DiretXML := '';
    //
    // XML
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Verificando exist�ncia do arquivo XML do Cancelamento');
    //
    Protocolo := Geral.SoNumero_TT(QrCTeEveRCabret_nProt.Value);
    ArqXML    := DmCTe_0000.QrFilialDirCTeEveRetCan.Value;
    //
    Geral.VerificaDir(ArqXML, '\', 'Diret�rio tempor�rio do XML de CTe', True);
    //
    ArqXML := ArqXML + Protocolo + '-procEventoCTe.xml';
    //
    if not FileExists(ArqXML) then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando arquivo XML do Cancelamento');
      //
      XML_Distribuicao := '';
      //
      if CTe_PF.XML_DistribuiEvento(QrCTeEveRCabchCTe.Value,
        QrCTeEveRCabret_cStat.Value, QrCTeEveRCabXML_Eve.Value,
        QrCTeEveRCabXML_retEve.Value, False, XML_Distribuicao,
        QrCTeEveRCabversao.Value, 'na base de dados')
      then
        Continua := CTe_PF.SalvaXML(ArqXML, XML_Distribuicao);
    end else
      Continua := True;
    if Continua then
      DiretXML := ArqXML;
  end;

var
  Msg, XML, Orgao, Ambiente, CNPJ_CPF, CHAVE_ACESSO: String;
  Entidade: Integer;
begin
{
(***
  PageControl1.ActivePageIndex := 0;
  Msg := '';
  DmCTe_0000.ReopenEmpresa(QrCTeEveRLoEEmpresa.Value);
  DmCTe_0000.ReopenOpcoesCTe(DmCTe_0000.QrOpcoesCTE, QrCTeEveRLoEEmpresa.Value, True);
  ReopenCTeEveRCab();
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrProt, Dmod.MyDB, [
    'SELECT COUNT(*) Itens ',
    'FROM nfeevercab ',
    'WHERE EventoLote=' + Geral.FF0(QrCTeEveRLoECodigo.Value),
    'AND Status in (135,136)',
    '']);
  if QrProt.RecordCount = 0 then
    Msg := 'N�o h� item protocolado para envio!'
  else
  if QrProtItens.Value < QrCTeEveRCab.RecordCount then
  begin
    Msg := 'Somente os itens protocolados ser�o enviados!'
  end;
  if Msg <> '' then
    Geral.MB_Aviso(Msg);
  if QrProt.RecordCount = 0 then
    Exit;
  //
  case TCTeEventos(QrCTeEveRCabtpEvento.Value) of
    nfeeveCCe: //Carta de corre��o
    begin
      FDirCTeXML := DmCTe_0000.QrFilialDirCTeEveProcCCe.Value;
      XML        := '';

      if FDirCTeXML = '' then
      begin
        Geral.MB_Aviso(
        'Diret�rio do processamento de carta de corre��o n�o definido!');
        Exit;
      end;
      Geral.VerificaDir(FDirCTeXML, '\', '', True);
      FPreEmail := DmCTe_0000.QrFilialCTePreMailEveCCe.Value;

      QrCTeEveRCab.DisableControls;
      try
        QrCTeEveRCab.First;
        while not QrCTeEveRCab.Eof do
        begin
          if QrCTeEveRCabStatus.Value in ([135,136]) then
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(QrCabA, Dmod.MyDB, [
              'SELECT Id, ide_serie, ide_nNF, ',
              'versao, IDCtrl, ',
              'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) cStat, ',
              'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt, ',
              'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo, ',
              'IF(infCanc_cStat>0, infCanc_dhRecbto, infProt_dhRecbto) dhRecbto, ',
              'emit_xNome, emit_CNPJ, emit_CPF, emit_IE, emit_UF, ',
              'dest_xNome, dest_CNPJ, dest_CPF, dest_IE, dest_UF, ',
              'Transporta_CNPJ, Transporta_CPF, Transporta_xNome, ',
              'Transporta_IE, Transporta_UF ',
              'FROM nfecaba ',
              'WHERE Id="' + QrCTeEveRCabchCTe.Value + '"',
              '']);
            //
            ExportaXMLCCE(XML);
            //
            if MyObjects.FIC(XML = '', nil, 'Falha ao exportar XML!' +
              sLineBreak + 'O e-mail n�o poder� ser enviado!') then Exit;
            //
            UnDmkDAC_PF.AbreMySQLQuery0(QrCTeEveRCCe, Dmod.MyDB, [
              SELECT Conta, x C o r r e c a o, nCondUso,
              'FROM nfeevercce',
              'WHERE FatID=' + Geral.FF0(QrCTeEveRCabFatID.Value),
              'AND FatNum=' + Geral.FF0(QrCTeEveRCabFatNum.Value),
              'AND Empresa=' + Geral.FF0(QrCTeEveRCabEmpresa.Value),
              'AND Controle=' + Geral.FF0(QrCTeEveRCabControle.Value),
              '']);
            if QrCTeEveRCCe.RecordCount <> 1 then
            begin
              Geral.MB_Erro(
                'Quantidade de registros de carta de corre��o diferente de um.' + sLineBreak +
                'Quantidade encontrada: ' + Geral.FF0(QrCTeEveRCCe.RecordCount));
                Exit;
            end;
            if QrCabA.FieldByName('dest_CNPJ').AsString <> '' then
              CNPJ_CPF := QrCabA.FieldByName('dest_CNPJ').AsString
            else
              CNPJ_CPF := QrCabA.FieldByName('dest_CPF').AsString;
            //
            DModG.ObtemEntidadeDeCNPJCFP(CNPJ_CPF, Entidade);
            //
            Orgao        := Geral.FF0(QrCTeEveRCabcOrgao.Value) + ' - ' + QrCTeEveRCabUF_Nome.Value;
            Ambiente     := Geral.FF0(QrCTeEveRCabtpAmb.Value) + ' - ' + CTe_PF.TextoAmbiente(QrCTeEveRCabtpAmb.Value);
            CNPJ_CPF     := Geral.FormataCNPJ_TT(QrCTeEveRCabCNPJ_CPF.Value);
            CHAVE_ACESSO := Geral.FormataChaveCTe(QrCTeEveRCabchCTe.Value, cfcnFrendly);
            //
            if UMailEnv.Monta_e_Envia_Mail([Entidade,
              DModG.QrEmpresasCodigo.Value, QrCTeEveRCabStatus.Value],
              meCTeEveCCE, [XML], [QrCabA.FieldByName('dest_xNome').AsString,
              QrCabA. FieldByName('Id').AsString,
              Geral.FormataVersaoCTe(QrCTeEveRCabversao.Value), Orgao, Ambiente,
              CNPJ_CPF, CHAVE_ACESSO, Geral.FDT(QrCTeEveRCabdhEvento.Value, 0),
              Geral.FF0(QrCTeEveRCabtpEvento.Value), Geral.FF0(QrCTeEveRCabnSeqEvento.Value),
              Geral.FormataVersaoCTe(QrCTeEveRCabverEvento.Value), QrCTeEveRCabdescEvento.Value,
              QrCTeEveRCCe x C o r r e c a o .Value, CO_CONDI_USO_CCE_COM[QrCTeEveRCCenCondUso.Value]], True)
            then
              Geral.MB_Aviso('E-mail enviado com sucesso!');
          end;
          //
          QrCTeEveRCab.Next;
        end;
      finally
        QrCTeEveRCab.EnableControls;
      end;
    end;
    nfeeveCan: //Cancelamento
    begin
      FDirCTeXML := DmCTe_0000.QrFilialDirCTeEveRetCan.Value;
      XML        := '';

      if FDirCTeXML = '' then
      begin
        Geral.MB_Aviso('Diret�rio das respostas de cancelamento de CT-e como evento da CT-e (-can.xml) n�o definido!');
        Exit;
      end;
      Geral.VerificaDir(FDirCTeXML, '\', '', True);
      FPreEmail := DmCTe_0000.QrFilialCtePreMailEveCCe.Value;

      QrCTeEveRCab.DisableControls;
      try
        QrCTeEveRCab.First;
        while not QrCTeEveRCab.Eof do
        begin
          if QrCTeEveRCabStatus.Value in ([135,136]) then
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(QrCabA, Dmod.MyDB, [
              'SELECT Id, ide_serie, ide_nNF, ',
              'versao, IDCtrl, ',
              'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) cStat, ',
              'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt, ',
              'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo, ',
              'IF(infCanc_cStat>0, infCanc_dhRecbto, infProt_dhRecbto) dhRecbto, ',
              'emit_xNome, emit_CNPJ, emit_CPF, emit_IE, emit_UF, ',
              'dest_xNome, dest_CNPJ, dest_CPF, dest_IE, dest_UF, ',
              'Transporta_CNPJ, Transporta_CPF, Transporta_xNome, ',
              'Transporta_IE, Transporta_UF ',
              'FROM nfecaba ',
              'WHERE Id="' + QrCTeEveRCabchCTe.Value + '"',
              '']);
            //
            ExportaXMLCan(XML);
            //
            if MyObjects.FIC(XML = '', nil, 'Falha ao exportar XML!' +
              sLineBreak + 'O e-mail n�o poder� ser enviado!') then Exit;
            //
            if QrCabA.FieldByName('dest_CNPJ').AsString <> '' then
              CNPJ_CPF := QrCabA.FieldByName('dest_CNPJ').AsString
            else
              CNPJ_CPF := QrCabA.FieldByName('dest_CPF').AsString;
            //
            DModG.ObtemEntidadeDeCNPJCFP(CNPJ_CPF, Entidade);
            //
            Orgao        := Geral.FF0(QrCTeEveRCabcOrgao.Value) + ' - ' + QrCTeEveRCabUF_Nome.Value;
            Ambiente     := Geral.FF0(QrCTeEveRCabtpAmb.Value) + ' - ' + CTeXMLGeren.TextoAmbiente(QrCTeEveRCabtpAmb.Value);
            CNPJ_CPF     := Geral.FormataCNPJ_TT(QrCTeEveRCabCNPJ_CPF.Value);
            CHAVE_ACESSO := Geral.FormataChaveCTe(QrCTeEveRCabchCTe.Value, cfcnFrendly);
            //
            if UMailEnv.Monta_e_Envia_Mail([Entidade,
              DModG.QrEmpresasCodigo.Value, QrCTeEveRCabStatus.Value],
              meCTeEveCan, [XML], [QrCabA.FieldByName('dest_xNome').AsString,
              QrCabA.FieldByName('Id').AsString], True)
            then
              Geral.MB_Aviso('E-mail enviado com sucesso!');
          end;
          //
          QrCTeEveRCab.Next;
        end;
      finally
        QrCTeEveRCab.EnableControls;
      end;
    end;
    else
      Geral.MB_Aviso('Tipo de devento n�o implementado!');
  end;
*)
}
end;

procedure TFmCTeEveRLoE.BtEventoClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMEvento, BtEvento);
end;

procedure TFmCTeEveRLoE.BtLoteClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMLote, BtLote);
end;

procedure TFmCTeEveRLoE.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  PageControl1.ActivePageIndex := 0;
  PageControl1.Align := alClient;
  CriaOForm;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //N�o implementado
  BtXML.Visible:= False;
end;

procedure TFmCTeEveRLoE.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCTeEveRLoECodigo.Value, LaRegistro.Caption);
end;

procedure TFmCTeEveRLoE.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmCTeEveRLoE.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCTeEveRLoE.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCTeEveRLoECodUsu.Value, LaRegistro.Caption);
end;

procedure TFmCTeEveRLoE.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCTeEveRLoE.QrCTeEveRCabCalcFields(DataSet: TDataSet);
begin
  QrCTeEveRCabchCTe_CT_SER.Value := Copy(QrCTeEveRCabchCTe.Value,
    DEMONTA_CHV_INI_SerCTe,  DEMONTA_CHV_TAM_SerCTe);
  QrCTeEveRCabchCTe_CT_NUM.Value := Copy(QrCTeEveRCabchCTe.Value,
    DEMONTA_CHV_INI_NumCTe,  DEMONTA_CHV_TAM_NumCTe);
  QrCTeEveRCabSTATUS_TXT.Value :=
    CTe_PF.Texto_StatusCTe(QrCTeEveRCabStatus.Value, 0);

end;

procedure TFmCTeEveRLoE.QrCTeEveRLoEAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCTeEveRLoE.QrCTeEveRLoEAfterScroll(DataSet: TDataSet);
begin
  ReopenCTeEveRCab();
end;

procedure TFmCTeEveRLoE.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCTeEveRLoE.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCTeEveRLoECodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'nfeeverloe', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCTeEveRLoE.SelecionaroXML1Click(Sender: TObject);
//var
  //Arquivo: String;
begin
{
  if MyObjects.FileOpenDialog(Self, 'C:\Dermatek\CTE', '', 'Informe o aqruivo XML',
  '', [], Arquivo) then
    WB_XML.Navigate(Arquivo);
}
end;

procedure TFmCTeEveRLoE.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCTeEveRLoE.Gerada1Click(Sender: TObject);
begin
{
  DmCTe_0000.AbreXML_NoNavegadorPadrao(QrCTeEveRLoEEmpresa.Value, CTE_EXT_CTE_XML,
    QrCTeEveRCabid.Value, False);
}
end;

procedure TFmCTeEveRLoE.Gerada2Click(Sender: TObject);
begin
{
  DmCTe_0000.AbreXML_NoTWebBrowser(QrCTeEveRLoEEmpresa.Value, CTE_EXT_CTE_XML,
    QrCTeEveRCabid.Value, False, WB_XML);
}
end;

function TFmCTeEveRLoE.ImpedePelaMisturaDeWebServices(var UF_Servico: String): Boolean;
var
  Qry: TmySQLQuery;
begin
  UF_Servico := '??';
  if DModG.QrParamsEmpUF_MDeMDe.Value <> DModG.QrParamsEmpUF_Servico.Value then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ',
    'SUM(IF(tpEvento IN (' + CTe_CodsEveWbserverUserDef + '), 1, 0)) UsuDef, ',
    'SUM(IF(tpEvento IN (' + CTe_CodsEveWbserverUserDef + '), 0, 1)) PreDef ',
    'FROM nfeevercab ',
    'WHERE EventoLote=' + Geral.FF0(QrCTeEveRLoECodigo.Value),
    '']);
    Result := (Qry.FieldByName('UsuDef').AsFloat >= 1) and
              (Qry.FieldByName('PreDef').AsFloat >= 1);
    if Result then Geral.MB_Aviso('Envio cancelado!' + sLineBreak +
    'Existe mistura de web services nos eventos inclu�dos neste lote!')
    else
    if Qry.FieldByName('UsuDef').AsFloat >= 1 then
      UF_Servico := DModG.QrParamsEmpUF_MDeMDe.Value
    else
      UF_Servico := DModG.QrParamsEmpUF_Servico.Value;
  end else
    Result := False;
end;

procedure TFmCTeEveRLoE.IncluiEventoaoloteatual1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCTeEveRLoEAdd, FmCTeEveRLoEAdd, afmoNegarComAviso) then
  begin
    FmCTeEveRLoEAdd.QrCTeEveRCab.Close;
    FmCTeEveRLoEAdd.QrCTeEveRCab.Params[00].AsInteger := QrCTeEveRLoEEmpresa.Value;
    FmCTeEveRLoEAdd.QrCTeEveRCab.Params[01].AsInteger := Integer(ctemystatusCTeAssinada);
    FmCTeEveRLoEAdd.QrCTeEveRCab.Open;
    //
    FmCTeEveRLoEAdd.ShowModal;
    FmCTeEveRLoEAdd.Destroy;
    //
  end;
end;

procedure TFmCTeEveRLoE.Incluinovolote1Click(Sender: TObject);
var
  Filial: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrCTeEveRLoE, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'nfeeverloe');
  UMyMod.ObtemValorDoCampoXDeIndex_Int(VAR_LIB_EMPRESA_SEL, 'Filial', 'Codigo',
    DmodG.QrEmpresas, EdEmpresa, CBEmpresa, Filial);
end;

procedure TFmCTeEveRLoE.QrCTeEveRLoEBeforeOpen(DataSet: TDataSet);
begin
  QrCTeEveRLoECodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCTeEveRLoE.QrCTeEveRLoECalcFields(DataSet: TDataSet);
begin
  //QrCTeEveRLoEDataHora_TXT.Value := dmkPF.FDT_NULO(QrCTeEveRLoEdhRecbto.Value, 0);
  case QrCTeEveRLoEtpAmb.Value of
    1: QrCTeEveRLoENO_Ambiente.Value := 'Produ��o';
    2: QrCTeEveRLoENO_Ambiente.Value := 'Homologa��o';
    else QrCTeEveRLoENO_Ambiente.Value := '';
  end;
end;

end.

