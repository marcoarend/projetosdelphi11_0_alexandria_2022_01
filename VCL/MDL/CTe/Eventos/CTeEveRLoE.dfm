object FmCTeEveRLoE: TFmCTeEveRLoE
  Left = 368
  Top = 194
  Caption = 'CTe-EVENT-101 :: Lotes de Eventos da CT-e'
  ClientHeight = 592
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 496
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 125
      Align = alTop
      TabOrder = 0
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 108
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label7: TLabel
          Left = 8
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label8: TLabel
          Left = 68
          Top = 4
          Width = 57
          Height = 13
          Caption = 'C'#243'digo: [F4]'
        end
        object Label9: TLabel
          Left = 152
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label4: TLabel
          Left = 8
          Top = 48
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object EdCodigo: TdmkEdit
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdCodUsu: TdmkEdit
          Left = 68
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodUsu'
          UpdCampo = 'CodUsu'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnKeyDown = EdCodUsuKeyDown
        end
        object EdNome: TdmkEdit
          Left = 152
          Top = 20
          Width = 633
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEmpresa: TdmkEditCB
          Left = 8
          Top = 64
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 65
          Top = 64
          Width = 720
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 4
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 433
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 898
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 496
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 205
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 793
        Height = 205
        Align = alLeft
        Caption = ' Dados do Lote: '
        TabOrder = 0
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 789
          Height = 188
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object Label2: TLabel
            Left = 152
            Top = 4
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
          end
          object Label3: TLabel
            Left = 68
            Top = 4
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
            FocusControl = DBEdNome
          end
          object Label5: TLabel
            Left = 8
            Top = 44
            Width = 20
            Height = 13
            Caption = 'Filial'
            FocusControl = DBEdit2
          end
          object DBEdNome: TDBEdit
            Left = 68
            Top = 20
            Width = 80
            Height = 21
            DataField = 'CodUsu'
            DataSource = DsCTeEveRLoE
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 8
            Top = 60
            Width = 56
            Height = 21
            DataField = 'Filial'
            DataSource = DsCTeEveRLoE
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 68
            Top = 60
            Width = 717
            Height = 21
            DataField = 'NO_Empresa'
            DataSource = DsCTeEveRLoE
            TabOrder = 2
          end
          object GroupBox2: TGroupBox
            Left = 8
            Top = 84
            Width = 493
            Height = 101
            Caption = ' Resposta do envio do XML: '
            TabOrder = 3
            object Label11: TLabel
              Left = 8
              Top = 20
              Width = 85
              Height = 13
              Caption = 'Vers'#227'o do leiaute:'
              FocusControl = DBEdit6
            end
            object Label12: TLabel
              Left = 108
              Top = 20
              Width = 85
              Height = 13
              Caption = 'Tipo de ambiente:'
              FocusControl = DBEdit7
            end
            object Label13: TLabel
              Left = 276
              Top = 20
              Width = 171
              Height = 13
              Caption = 'Vers'#227'o do aplic. que recebeu o lote:'
              FocusControl = DBEdit8
            end
            object Label14: TLabel
              Left = 8
              Top = 60
              Width = 48
              Height = 13
              Caption = 'Resposta:'
              FocusControl = DBEdit9
            end
            object Label15: TLabel
              Left = 456
              Top = 20
              Width = 17
              Height = 13
              Caption = 'UF:'
              FocusControl = DBEdit12
            end
            object DBEdit6: TDBEdit
              Left = 8
              Top = 36
              Width = 97
              Height = 21
              DataField = 'versao'
              DataSource = DsCTeEveRLoE
              TabOrder = 0
            end
            object DBEdit7: TDBEdit
              Left = 108
              Top = 36
              Width = 21
              Height = 21
              DataField = 'tpAmb'
              DataSource = DsCTeEveRLoE
              TabOrder = 1
            end
            object DBEdit8: TDBEdit
              Left = 276
              Top = 36
              Width = 177
              Height = 21
              DataField = 'verAplic'
              DataSource = DsCTeEveRLoE
              TabOrder = 2
            end
            object DBEdit9: TDBEdit
              Left = 8
              Top = 76
              Width = 33
              Height = 21
              DataField = 'cStat'
              DataSource = DsCTeEveRLoE
              TabOrder = 3
            end
            object DBEdit10: TDBEdit
              Left = 44
              Top = 76
              Width = 441
              Height = 21
              DataField = 'xMotivo'
              DataSource = DsCTeEveRLoE
              TabOrder = 4
            end
            object DBEdit11: TDBEdit
              Left = 128
              Top = 36
              Width = 145
              Height = 21
              DataField = 'NO_Ambiente'
              DataSource = DsCTeEveRLoE
              TabOrder = 5
            end
            object DBEdit12: TDBEdit
              Left = 456
              Top = 36
              Width = 29
              Height = 21
              DataField = 'cOrgao'
              DataSource = DsCTeEveRLoE
              TabOrder = 6
            end
          end
          object GroupBox3: TGroupBox
            Left = 504
            Top = 84
            Width = 281
            Height = 101
            Caption = ' Recibo de aceite do lote: '
            Enabled = False
            TabOrder = 4
            object Label16: TLabel
              Left = 8
              Top = 16
              Width = 87
              Height = 13
              Caption = 'N'#250'mero do recibo:'
              Enabled = False
              FocusControl = DBEdit13
            end
            object Label10: TLabel
              Left = 8
              Top = 56
              Width = 58
              Height = 13
              Caption = 'Data / hora:'
              Enabled = False
              FocusControl = DBEdit5
            end
            object Label17: TLabel
              Left = 184
              Top = 56
              Width = 84
              Height = 13
              Caption = 't m'#233'd. resp. (seg):'
              Enabled = False
              FocusControl = DBEdit14
            end
            object DBEdit13: TDBEdit
              Left = 8
              Top = 32
              Width = 265
              Height = 21
              DataSource = DsCTeEveRLoE
              Enabled = False
              TabOrder = 0
            end
            object DBEdit5: TDBEdit
              Left = 8
              Top = 72
              Width = 173
              Height = 21
              DataSource = DsCTeEveRLoE
              Enabled = False
              TabOrder = 1
            end
            object DBEdit14: TDBEdit
              Left = 184
              Top = 72
              Width = 89
              Height = 21
              DataSource = DsCTeEveRLoE
              Enabled = False
              TabOrder = 2
            end
          end
          object DBEdit4: TDBEdit
            Left = 152
            Top = 20
            Width = 633
            Height = 21
            DataField = 'Nome'
            DataSource = DsCTeEveRLoE
            TabOrder = 5
          end
          object DBEdCodigo: TDBEdit
            Left = 8
            Top = 20
            Width = 57
            Height = 21
            DataField = 'Codigo'
            DataSource = DsCTeEveRLoE
            TabOrder = 6
          end
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 205
      Width = 1008
      Height = 193
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Informa'#231#245'es dos eventos do lote'
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 165
          Align = alClient
          DataSource = DsCTeEveRCab
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'chNFe'
              Title.Caption = 'Chave da NFe'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'chNFe_NF_SER'
              Title.Caption = 'S'#233'rie'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'chNFe_NF_NUM'
              Title.Caption = 'N'#186' NFe'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'tpEvento'
              Title.Caption = 'Evento'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'descEvento'
              Title.Caption = 'Descri'#231#227'o do Evento'
              Width = 252
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'dhEvento'
              Title.Caption = 'Data / hota evento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'tpAmb'
              Title.Caption = 'Amb'
              Width = 27
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Status'
              Width = 35
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STATUS_TXT'
              Title.Caption = 'Descri'#231#227'o do Status'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' XML do arquivo carregado '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object WB_XML: TWebBrowser
          Left = 0
          Top = 0
          Width = 1000
          Height = 165
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 998
          ControlData = {
            4C0000005A6700000E1100000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 432
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 412
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtLote: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lote'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtLoteClick
        end
        object BtEvento: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Evento'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtEventoClick
        end
        object BtXML: TBitBtn
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&XML'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtXMLClick
        end
        object BtEnvia: TBitBtn
          Tag = 244
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Caption = 'E&Mail'
          NumGlyphs = 2
          TabOrder = 4
          OnClick = BtEnviaClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 315
        Height = 32
        Caption = 'Lotes de Eventos da CT-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 315
        Height = 32
        Caption = 'Lotes de Eventos da CT-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 315
        Height = 32
        Caption = 'Lotes de Eventos da CT-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsCTeEveRLoE: TDataSource
    DataSet = QrCTeEveRLoE
    Left = 232
    Top = 92
  end
  object QrCTeEveRLoE: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCTeEveRLoEBeforeOpen
    AfterOpen = QrCTeEveRLoEAfterOpen
    AfterScroll = QrCTeEveRLoEAfterScroll
    OnCalcFields = QrCTeEveRLoECalcFields
    SQL.Strings = (
      'SELECT lot.*, ent.Filial, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Empresa'
      'FROM nfeeverlot lot'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa'
      ''
      'WHERE lot.Codigo>-1000'
      '')
    Left = 232
    Top = 48
    object QrCTeEveRLoEDataHora_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataHora_TXT'
      Calculated = True
    end
    object QrCTeEveRLoENO_Ambiente: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_Ambiente'
      Calculated = True
    end
    object QrCTeEveRLoECodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'nfeeverlot.Codigo'
    end
    object QrCTeEveRLoECodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'nfeeverlot.CodUsu'
    end
    object QrCTeEveRLoENome: TWideStringField
      FieldName = 'Nome'
      Origin = 'nfeeverlot.Nome'
      Size = 50
    end
    object QrCTeEveRLoEEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfeeverlot.Empresa'
    end
    object QrCTeEveRLoEversao: TFloatField
      FieldName = 'versao'
      Origin = 'nfeeverlot.versao'
    end
    object QrCTeEveRLoEtpAmb: TSmallintField
      FieldName = 'tpAmb'
      Origin = 'nfeeverlot.tpAmb'
    end
    object QrCTeEveRLoEverAplic: TWideStringField
      FieldName = 'verAplic'
      Origin = 'nfeeverlot.verAplic'
    end
    object QrCTeEveRLoEcOrgao: TSmallintField
      FieldName = 'cOrgao'
      Origin = 'nfeeverlot.cOrgao'
    end
    object QrCTeEveRLoEcStat: TSmallintField
      FieldName = 'cStat'
      Origin = 'nfeeverlot.cStat'
    end
    object QrCTeEveRLoExMotivo: TWideStringField
      FieldName = 'xMotivo'
      Origin = 'nfeeverlot.xMotivo'
      Size = 255
    end
    object QrCTeEveRLoELk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfeeverlot.Lk'
    end
    object QrCTeEveRLoEDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfeeverlot.DataCad'
    end
    object QrCTeEveRLoEDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfeeverlot.DataAlt'
    end
    object QrCTeEveRLoEUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfeeverlot.UserCad'
    end
    object QrCTeEveRLoEUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfeeverlot.UserAlt'
    end
    object QrCTeEveRLoEAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfeeverlot.AlterWeb'
    end
    object QrCTeEveRLoEAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfeeverlot.Ativo'
    end
    object QrCTeEveRLoEFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
    end
    object QrCTeEveRLoENO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtXML
    CanUpd01 = BtEvento
    Left = 68
    Top = 12
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 100
    Top = 12
  end
  object PMLote: TPopupMenu
    OnPopup = PMLotePopup
    Left = 520
    Top = 504
    object Incluinovolote1: TMenuItem
      Caption = '&Inclui novo lote'
      OnClick = Incluinovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&Altera lote atual'
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      Enabled = False
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Envialoteaofisco1: TMenuItem
      Caption = '&Envia lote ao fisco'
      Enabled = False
      OnClick = Envialoteaofisco1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Lerarquivo1: TMenuItem
      Caption = '&Ler arquivo'
      OnClick = Lerarquivo1Click
    end
  end
  object PMEvento: TPopupMenu
    OnPopup = PMEventoPopup
    Left = 608
    Top = 508
    object IncluiEventoaoloteatual1: TMenuItem
      Caption = '&Inclui Evento ao lote atual'
      OnClick = IncluiEventoaoloteatual1Click
    end
    object RemoveEventosselecionados1: TMenuItem
      Caption = '&Remove Evento(s) selecionado(s)'
      OnClick = RemoveEventosselecionados1Click
    end
  end
  object DsCTeEveRCab: TDataSource
    DataSet = QrCTeEveRCab
    Left = 320
    Top = 96
  end
  object QrCTeEveRCab: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCTeEveRCabCalcFields
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, Controle, '
      'tpAmb, chNFe, dhEvento, tpEvento, descEvento, '
      'Status, ret_cStat, ret_nProt, ret_dhRegEvento, '
      'XML_Eve, XML_retEve, versao, duf.Nome UF_Nome, '
      'cOrgao, IF(nec.CNPJ<>"", nec.CNPJ, nec.CPF) CNPJ_CPF,'
      'nSeqEvento, verEvento'
      'FROM nfeevercab'
      'LEFT JOIN dtb_ufs duf ON duf.Codigo=nec.cOrgao ')
    Left = 316
    Top = 48
    object QrCTeEveRCabnSeqEvento: TSmallintField
      FieldName = 'nSeqEvento'
    end
    object QrCTeEveRCabCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 14
    end
    object QrCTeEveRCabUF_Nome: TWideStringField
      FieldName = 'UF_Nome'
    end
    object QrCTeEveRCabcOrgao: TSmallintField
      FieldName = 'cOrgao'
    end
    object QrCTeEveRCabversao: TFloatField
      FieldName = 'versao'
    end
    object QrCTeEveRCabFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeEveRCabFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeEveRCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeEveRCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeEveRCabtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrCTeEveRCabchCTe: TWideStringField
      FieldName = 'chCTe'
      Size = 44
    end
    object QrCTeEveRCabdhEvento: TDateTimeField
      FieldName = 'dhEvento'
    end
    object QrCTeEveRCabtpEvento: TIntegerField
      FieldName = 'tpEvento'
    end
    object QrCTeEveRCabdescEvento: TWideStringField
      FieldName = 'descEvento'
      Size = 60
    end
    object QrCTeEveRCabchCTe_CT_SER: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'chCTe_CT_SER'
      Size = 3
      Calculated = True
    end
    object QrCTeEveRCabchCTe_CT_NUM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'chCTe_CT_NUM'
      Size = 9
      Calculated = True
    end
    object QrCTeEveRCabret_cStat: TIntegerField
      FieldName = 'ret_cStat'
    end
    object QrCTeEveRCabStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrCTeEveRCabret_nProt: TWideStringField
      FieldName = 'ret_nProt'
      Size = 15
    end
    object QrCTeEveRCabret_dhRegEvento: TDateTimeField
      FieldName = 'ret_dhRegEvento'
    end
    object QrCTeEveRCabXML_Eve: TWideMemoField
      FieldName = 'XML_Eve'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCTeEveRCabXML_retEve: TWideMemoField
      FieldName = 'XML_retEve'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCTeEveRCabverEvento: TFloatField
      FieldName = 'verEvento'
    end
    object QrCTeEveRCabSTATUS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'STATUS_TXT'
      Size = 255
      Calculated = True
    end
  end
  object PMXML: TPopupMenu
    Left = 792
    Top = 512
    object AbrirXMLnonavegadordestajanela1: TMenuItem
      Caption = 'Abrir XML no navegador desta janela'
      object Gerada2: TMenuItem
        Caption = '&Gerada'
        OnClick = Gerada2Click
      end
      object Assinada1: TMenuItem
        Caption = '&Assinada'
        OnClick = Assinada1Click
      end
      object Lote1: TMenuItem
        Caption = '&Lote'
      end
      object SelecionaroXML1: TMenuItem
        Caption = '&Selecionar o XML'
        OnClick = SelecionaroXML1Click
      end
    end
    object AbrirXMLdaCTEnonavegadorpadro1: TMenuItem
      Caption = 'Abrir XML do CTE no navegador padr'#227'o'
      object Gerada1: TMenuItem
        Caption = '&Gerado'
        OnClick = Gerada1Click
      end
      object Assina1: TMenuItem
        Caption = '&Assinado'
        OnClick = Assina1Click
      end
    end
  end
  object QrProt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(*) Itens '
      'FROM nfeevercab '
      'WHERE EventoLote=:P0'
      'AND Status in (135,136)')
    Left = 420
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProtItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrCabA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Id, ide_serie, ide_nNF, '
      'versao, IDCtrl,'
      'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) cStat,'
      'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt,'
      'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo,'
      
        'IF(infCanc_cStat>0, infCanc_dhRecbto, infProt_dhRecbto) dhRecbto' +
        ','
      'emit_xNome, emit_CNPJ, emit_CPF, emit_IE, emit_UF,'
      'dest_xNome, dest_CNPJ, dest_CPF, dest_IE, dest_UF,'
      'Transporta_CNPJ, Transporta_CPF, Transporta_xNome,  '
      'Transporta_IE, Transporta_UF '
      'FROM nfecaba'
      'WHERE Id=:P0')
    Left = 424
    Top = 108
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrCTeEveRCCe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Conta, xCorrecao, nCondUso'
      'FROM nfeevercce'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      '')
    Left = 424
    Top = 156
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrCTeEveRCCeConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCTeEveRCCenCondUso: TSmallintField
      FieldName = 'nCondUso'
    end
  end
end
