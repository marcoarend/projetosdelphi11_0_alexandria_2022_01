unit CTeEveRCCe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkRadioGroup, dmkMemo, UnCTe_PF, UnDmkEnums, UnXXe_PF, Vcl.Menus;

type
  TFmCTeEveRCCe = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Painel1: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtEnvia: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    RGnCondUso: TdmkRadioGroup;
    MeCondicao: TMemo;
    Memo1: TMemo;
    DBGrid1: TDBGrid;
    Panel2: TPanel;
    CkCondicao: TCheckBox;
    BtItens: TBitBtn;
    BtCria: TBitBtn;
    PMItens: TPopupMenu;
    IncluiItem1: TMenuItem;
    AlteraItem1: TMenuItem;
    ExcluiItem1: TMenuItem;
    QrCTeEveRCCeIts: TmySQLQuery;
    DsCTeEveRCCeIts: TDataSource;
    QrCTeEveRCCeItsFatID: TIntegerField;
    QrCTeEveRCCeItsFatNum: TIntegerField;
    QrCTeEveRCCeItsEmpresa: TIntegerField;
    QrCTeEveRCCeItsControle: TIntegerField;
    QrCTeEveRCCeItsConta: TIntegerField;
    QrCTeEveRCCeItsIDItem: TIntegerField;
    QrCTeEveRCCeItsLayI_Versao: TFloatField;
    QrCTeEveRCCeItsLayI_Grupo: TIntegerField;
    QrCTeEveRCCeItsLayI_Codigo: TIntegerField;
    QrCTeEveRCCeItsGrupoAlterado: TWideStringField;
    QrCTeEveRCCeItsCampoAlterado: TWideStringField;
    QrCTeEveRCCeItsValorAlterado: TWideMemoField;
    QrCTeEveRCCeItsnroItemAlterado: TIntegerField;
    QrCTeEveRCCeItsLk: TIntegerField;
    QrCTeEveRCCeItsDataCad: TDateField;
    QrCTeEveRCCeItsDataAlt: TDateField;
    QrCTeEveRCCeItsUserCad: TIntegerField;
    QrCTeEveRCCeItsUserAlt: TIntegerField;
    QrCTeEveRCCeItsAlterWeb: TSmallintField;
    QrCTeEveRCCeItsAtivo: TSmallintField;
    DBMemo1: TDBMemo;
    QrCTeEveRCCeItsLayC_Grupo: TIntegerField;
    QrCTeEveRCCeItsValor255: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGnCondUsoClick(Sender: TObject);
    procedure CkCondicaoClick(Sender: TObject);
    procedure MeCondicaoChange(Sender: TObject);
    procedure BtCriaClick(Sender: TObject);
    procedure IncluiItem1Click(Sender: TObject);
    procedure AlteraItem1Click(Sender: TObject);
    procedure ExcluiItem1Click(Sender: TObject);
    procedure BtItensClick(Sender: TObject);
    procedure PMItensPopup(Sender: TObject);
    procedure QrCTeEveRCCeItsAfterOpen(DataSet: TDataSet);
    procedure QrCTeEveRCCeItsAfterClose(DataSet: TDataSet);
    procedure BtEnviaClick(Sender: TObject);
  private
    { Private declarations }
    procedure HabilitaOK();
    procedure MostraFormCTeEveRCCeIts(SQLType: TSQLType);

  public
    { Public declarations }
    FFatID, FFatNum, FEmpresa, FControle, FConta: Integer;
    MsgMemoDesfeito: Boolean;
    //
    procedure DesfazMsgMemo();
    procedure ReopenCTeEveRCCeIts(IDItem: Integer);
  end;

  var
  FmCTeEveRCCe: TFmCTeEveRCCe;

implementation

uses UnMyObjects, Module, UnCTe_Tabs, CTeEveRCab, ModuleGeral, UMySQLModule,
  evCCeCTe_v200, ModuleCTe_0000, CTeEveGeraXMLCCe_0200a, CTeEveRCCeIts, MyDBCheck,
  DmkDAC_PF;

{$R *.DFM}

procedure TFmCTeEveRCCe.AlteraItem1Click(Sender: TObject);
begin
  MostraFormCTeEveRCCeIts(stUpd);
end;

procedure TFmCTeEveRCCe.BtCriaClick(Sender: TObject);
var
  Status, nCondUso: Integer;
  Dir: String;
begin
  if not DmCTe_0000.ObtemDirXML(CTE_EXT_EVE_ENV_CCE_XML, Dir, True) then
    Exit;
  if ImgTipo.SQLType = stIns then
  begin
    if not FmCTeEveRCab.IncluiCTeEveRCab(0, evexxe110110CCe, FFatID, FFatNum, FEmpresa,
    FControle) then
    begin
      Geral.MB_Erro('ERRO ao incluir o cabe�alho gen�rico de evento!');
      Exit;
    end;
    FConta := DModG.BuscaProximoCodigoInt('ctectrl', 'cteevercce', '', 0);
(*
  end else begin
    FatID := FmCTeEveRCab.QrCTeEveRCabFatID.Value;
    FatNum := FmCTeEveRCab.QrCTeEveRCabFatNum.Value;
    Empresa := FmCTeEveRCab.QrCTeEveRCabEmpresa.Value;
    Controle := FmCTeEveRCab.QrCTeEveRCabControle.Value;
    Conta := FmCTeEveRCab.QrCTeEveRCCeConta.Value;
*)
  end;
  //
  nCondUso       := RGnCondUso.ItemIndex;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'cteevercce', False, [
  'nCondUso'], [
  'FatID', 'FatNum', 'Empresa', 'Controle', 'Conta'], [
  nCondUso], [
  FFatID, FFatNum, FEmpresa, FControle, FConta], True) then
  begin
    FmCTeEveRCab.ReopenCTeEveRCab(FControle);
    if FmCTeEveRCab.QrCTeEveRCabControle.Value = FControle then
    begin
      ImgTipo.SQLType := stUpd;
      HabilitaOK();
      Geral.MB_Info(
      'O registro de uma nova Carta de Corre��o substitui a Carta de Corre��o anterior, assim a nova Carta de Corre��o deve conter todas as corre��es a serem consideradas.');
    end else
    begin
      Close;
    end;
  end;
end;

procedure TFmCTeEveRCCe.BtEnviaClick(Sender: TObject);
var
  Destino, Dir: String;
  XML_Eve: String;
  Status: Integer;
begin
  if not DmCTe_0000.ObtemDirXML(CTE_EXT_EVE_ENV_CCE_XML, Dir, True) then
    Exit;
    if UnCTeEveGeraXMLCCe_0200a.CriarDocumentoEveCCe(FFatID, FFatNum, FEmpresa,
    FmCTeEveRCab.QrERCId.Value, FmCTeEveRCab.QrERCcOrgao.Value,
    FmCTeEveRCab.QrERCtpAmb.Value, (*FmCTeEveRCab.QrERCTipoEnt.Value,*)
    FmCTeEveRCab.QrERCCNPJ.Value, (*FmCTeEveRCab.QrERCCPF.Value,*)
    FmCTeEveRCab.QrERCchCTe.Value, FmCTeEveRCab.QrERCdhEvento.Value,
    (*FmCTeEveRCab.QrERCTZD_UTC.Value,*) FmCTeEveRCab.QrERCtpEvento.Value,
    FmCTeEveRCab.QrERCnSeqEvento.Value, (*FmCTeEveRCab.QrERCverEvento.Value,
    FmCTeEveRCab.QrERCversao.Value, FmCTeEveRCab.QrERCdescEvento.Value,*)
    FControle, FConta, CO_CONDI_USO_CCE_SEM[RGnCondUso.ItemIndex],
    XML_Eve, LaAviso1, LaAviso2) then
    begin
      Destino := DmCTe_0000.SalvaXML(CTE_EXT_EVE_ENV_CCE_XML,
      FmCTeEveRCab.QrERCId.Value, XML_Eve, Memo1, True);
      if FileExists(Destino) then
      begin
        Status := Integer(ctemystatusCTeAssinada);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cteevercab', False, [
        'XML_Eve', 'Status'], [
        'FatID', 'FatNum', 'Empresa', 'Controle'], [
        Geral.WideStringToSQLString(XML_Eve), Status], [
        FFatID, FFatNum, FEmpresa, FControle], True) then
        begin
          FmCTeEveRCab.ReopenCTeEveRCab(FControle);
          //
          CTe_PF.MostraFormStepsCTe_CartaDeCorrecao(FEmpresa,
          FmCTeEveRCab.QrAide_Serie.Value, FmCTeEveRCab.QrAide_nCT.Value,
          FmCTeEveRCab.QrERCnSeqEvento.Value,
          CTe_AllModelos, FmCTeEveRCab.QrAnProt.Value, FmCTeEveRCab.QrERCchCTe.Value,
          XML_Eve, FmCTeEveRCab.QrA);
          //
          FmCTeEveRCab.ReopenCTeEveRCab(FControle);
        end;
      end;
    end;

  //
  FmCTeEveRCab.ReopenCTeEveRCCe(0);
  Close;
end;

procedure TFmCTeEveRCCe.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmCTeEveRCCe.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCTeEveRCCe.CkCondicaoClick(Sender: TObject);
begin
  HabilitaOK();
end;

procedure TFmCTeEveRCCe.DesfazMsgMemo();
begin
  if not MsgMemoDesfeito then
  begin
    MsgMemoDesfeito := True;
  end;
  HabilitaOK();
end;

procedure TFmCTeEveRCCe.ExcluiItem1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o do item selecionado da carta de corre��o?') =
  ID_YES then
  begin
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrCTeEveRCCeIts, 'cteevercceits',
    ['Conta'], ['Conta'], True);
    //
    ReopenCTeEveRCCeIts(0);
  end;
end;

procedure TFmCTeEveRCCe.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  HabilitaOK();
end;

procedure TFmCTeEveRCCe.FormCreate(Sender: TObject);
begin
  MsgMemoDesfeito := False;
  ImgTipo.SQLType := stPsq;
end;

procedure TFmCTeEveRCCe.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCTeEveRCCe.HabilitaOK();
var
  CondUso, EmIns, EmUpd: Boolean;
begin
  CondUso := RGnCondUso.ItemIndex > 0;
  EmIns   := ImgTipo.SQLType = stIns;
  EmUpd   := ImgTipo.SQLType = stUpd;
  BtCria.Enabled  := CondUso and EmIns;
  BtItens.Enabled := CondUso and EmUpd;
  BtEnvia.Enabled := CondUso and CkCondicao.Checked and
    (QrCTeEveRCCeIts.State <> dsInactive) and
    (QrCTeEveRCCeIts.RecordCount <> 0);
{
  BtOK.Enabled := MsgMemoDesfeito and
    CkCondicao.Checked and
    (RGnCondUso.ItemIndex > 0);
}
end;

procedure TFmCTeEveRCCe.IncluiItem1Click(Sender: TObject);
begin
  MostraFormCTeEveRCCeIts(stIns);
end;

procedure TFmCTeEveRCCe.MeCondicaoChange(Sender: TObject);
begin
  HabilitaOK();
end;

procedure TFmCTeEveRCCe.MostraFormCTeEveRCCeIts(SQLType: TSQLType);
var
  ValorAlterado: String;
  Data: TDateTime;
begin
  if DBCheck.CriaFm(TFmCTeEveRCCeIts, FmCTeEveRCCeIts, afmoNegarComAviso) then
  begin
    FmCTeEveRCCeIts.ImgTipo.SQLType := SQLType;
    FmCTeEveRCCeIts.FQry      := QrCTeEveRCCeIts;
    FmCTeEveRCCeIts.FFatID    := FFatID;
    FmCTeEveRCCeIts.FFatNum   := FFatNum;
    FmCTeEveRCCeIts.FEmpresa  := FEmpresa;
    FmCTeEveRCCeIts.FControle := FControle;
    FmCTeEveRCCeIts.FConta    := FConta;
    if SQLType = stUpd then
    begin
      FmCTeEveRCCeIts.FIDItem := QrCTeEveRCCeItsIDItem.Value;
      FmCTeEveRCCeIts.EdLayC.ValueVariant  := QrCTeEveRCCeItsLayC_Grupo.Value;
      FmCTeEveRCCeIts.CBLayC.KeyValue      := QrCTeEveRCCeItsLayC_Grupo.Value;
      FmCTeEveRCCeIts.EdGrupo.ValueVariant := QrCTeEveRCCeItsLayI_Grupo.Value;
      FmCTeEveRCCeIts.CBGrupo.KeyValue     := QrCTeEveRCCeItsLayI_Grupo.Value;
      FmCTeEveRCCeIts.EdCampo.ValueVariant := QrCTeEveRCCeItsLayI_Codigo.Value;
      FmCTeEveRCCeIts.CBCampo.KeyValue     := QrCTeEveRCCeItsLayI_Codigo.Value;
      FmCTeEveRCCeIts.EdnroItemAlterado.ValueVariant := QrCTeEveRCCeItsnroItemAlterado.Value;
      ValorAlterado := QrCTeEveRCCeItsValorAlterado.Value;
      //
      if FmCTeEveRCCeIts.TabSheet0.Visible then
      begin
        if FmCTeEveRCCeIts.EdTexto.Visible then
          FmCTeEveRCCeIts.EdTexto.Text := ValorAlterado
        else
          FmCTeEveRCCeIts.MeTexto.Text := ValorAlterado;
      end else
      if FmCTeEveRCCeIts.TabSheet1.Visible then
      begin
        FmCTeEveRCCeIts.EdInteiro.ValueVariant := Geral.IMV(ValorAlterado)
      end else
      if FmCTeEveRCCeIts.TabSheet2.Visible then
      begin
        FmCTeEveRCCeIts.EdDecimal.ValueVariant := Geral.DMV_Dot(ValorAlterado);
      end else
      if FmCTeEveRCCeIts.TabSheet3.Visible then
      begin
        Data := 0;
        if Length(ValorAlterado) > 10 then
        begin
          Data := Geral.ValidaData_YYYY_MM_DD(Copy(ValorAlterado, 1, 10));
          Data := Data + StrToTime(Copy(ValorAlterado, 12));
        end else
        if Length(ValorAlterado) = 10 then
        begin
          Data := Geral.ValidaData_YYYY_MM_DD(ValorAlterado);
        end else
        if Length(ValorAlterado) = 5 then
        begin
          Data := StrToTime(ValorAlterado);
        end;
        if FmCTeEveRCCeIts.TPData.Visible then
          FmCTeEveRCCeIts.TPData.Date := Trunc(Data);
        if FmCTeEveRCCeIts.EdHora.Visible then
        begin
          FmCTeEveRCCeIts.EdHora.ValueVariant := Data;
        end;
      end;
    end;
    FmCTeEveRCCeIts.ShowModal;
    FmCTeEveRCCeIts.Destroy;
  end;
  ReopenCTeEveRCCeIts(0);
end;

procedure TFmCTeEveRCCe.PMItensPopup(Sender: TObject);
begin
  //MyObjects.HabilitaMenuItemItsIns(IncluiItem1, Qr??);
  MyObjects.HabilitaMenuItemItsUpd(AlteraItem1, QrCTeEveRCCeIts);
  MyObjects.HabilitaMenuItemItsDel(ExcluiItem1, QrCTeEveRCCeIts);
end;

procedure TFmCTeEveRCCe.QrCTeEveRCCeItsAfterClose(DataSet: TDataSet);
begin
  HabilitaOK();
end;

procedure TFmCTeEveRCCe.QrCTeEveRCCeItsAfterOpen(DataSet: TDataSet);
begin
  HabilitaOK();
end;

procedure TFmCTeEveRCCe.ReopenCTeEveRCCeIts(IDItem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeEveRCCeIts, Dmod.MyDB, [
  'SELECT LEFT(its.ValorAlterado, 255) Valor255, its.*  ',
  'FROM cteevercceits its ',
  'WHERE its.FatID=' + Geral.FF0(FFatID),
  'AND its.FatNum=' + Geral.FF0(FFatNum),
  'AND its.Empresa=' + Geral.FF0(FEmpresa),
  'AND its.Controle=' + Geral.FF0(FControle),
  'AND its.Conta=' + Geral.FF0(FConta),
  '']);
end;

procedure TFmCTeEveRCCe.RGnCondUsoClick(Sender: TObject);
begin
  MeCondicao.Text := CO_CONDI_USO_CCE_COM[RGnCondUso.ItemIndex];
  HabilitaOK();
end;

end.
