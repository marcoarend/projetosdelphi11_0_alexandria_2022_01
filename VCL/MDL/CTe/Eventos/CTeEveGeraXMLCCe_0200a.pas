unit CTeEveGeraXMLCCe_0200a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, UnMLAGeral, Variants, Math,
  IBCustomDataSet, IBQuery, Clipbrd, DB, DBClient, dmkGeral, UrlMon,
  InvokeRegistry, SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, JwaWinCrypt,
  WinInet, XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB, UnDmkProcFunc,
  UnCTe_PF, UnXXe_PF, mySQLDBTables,
  // Carta de correcao
  evCCeCTe_v200, eventoCTe_v200;

const
  // XML
  sXML_Version        = '1.0';
  sXML_Encoding       = 'UTF-8';
  ENCODING_UTF8 = '?xml version="1.0" encoding="UTF-8"?';
  ENCODING_UTF8_STD = '?xml version="1.0" encoding="UTF-8" standalone="no"?';
  NAME_SPACE = 'xmlns="http://www.portalfiscal.inf.br/cte"';
  sCampoNulo = '#NULL#';
  ///
  ///
  ///
  verEventosCCe_Layout        = '2.00'; // Evento - Carta de corre��o - Vers�o do layout
  verEventosCCe_Evento        = '2.00'; // Evento - Carta de corre��o - Vers�o do Evento
  verEventosCCe_CCeerr        = '2.00'; // Evento - Carta de corre��o - Vers�o da carta de Correcao

type
  TCTeEveGeraXMLCCe_0200a = class(TObject)
  private
    {private declaration}
  public
    {public declaration}
    function  CriarDocumentoEveCCe(const FatID, FatNum, Empresa: Integer; ID:
              String; cOrgao, tpAmb: Integer; CNPJ, chCTe: String; dhEvento:
              TDateTime; tpEvento, nSeqEvento, CTeEveRCCeItsControle,
              CTeEveRCCeItsConta: Integer; const xCondUso: String; var
              XMLAssinado: String; const LaAviso1, LaAviso2: TLabel): Boolean;
    function  GeraXML_CCe(const CTeEveRCCeItsControle, CTeEveRCCeItsConta:
              Integer; const xCondUso: String; var XML: String; const LaAviso1,
              LaAviso2: TLabel): Boolean;
(*
    function  GeraXML_Correcoes(const CTeEveRCCeItsConta: Integer; var XML: String;
              const LaAviso1, LaAviso2: TLabel): Boolean;
*)
  end;

var
  UnCTeEveGeraXMLCCe_0200a: TCTeEveGeraXMLCCe_0200a;

implementation

uses ModuleCTe_0000, Module, DmkDAC_PF;

var
  //  ACBr
  //CertStore     : IStore3;
  //CertStoreMem  : IStore3;
  //PrivateKey    : IPrivateKey;
  //Certs         : ICertificates2;
  Cert          : ICertificate2;
  //NumCertCarregado : String;
  // XML  IXMLEvCCeCTe
  EveXML: IXMLTEvento;
  CCeXML: IXMLEvCCeCTe;
  EvCCeCTe_infCorrecao: IXMLEvCCeCTe_infCorrecao;
  //EvCCeCTe_infCorrecaoList: IXMLEvCCeCTe_infCorrecaoList;
  arqXML: TXMLDocument;
  //
  FLaAviso1: TLabel;
  FLaAviso2: TLabel;
  FFatID, FFatNum, FEmpresa: Integer;

{ TCTeEveGeraXMLCCe_0200a }

function TCTeEveGeraXMLCCe_0200a.CriarDocumentoEveCCe(const FatID, FatNum,
  Empresa: Integer; ID: String; cOrgao, tpAmb: Integer; CNPJ, chCTe: String;
  dhEvento: TDateTime; tpEvento, nSeqEvento, CTeEveRCCeItsControle,
  CTeEveRCCeItsConta: Integer; const xCondUso: String; var XMLAssinado: String;
  const LaAviso1, LaAviso2: TLabel): Boolean;
var
  //strChaveAcesso,
  NumeroSerial: String;
  //DadosTxt,
  Texto, XMLAny: String;
  AnyNode: IXMLNode;
begin
  Result := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  //
  FFatID   := FatID;
  FFatNum  := FatNum;
  FEmpresa := Empresa;
  //
  if GeraXML_CCe(CTeEveRCCeItsControle, CTeEveRCCeItsConta, xCondUso, XMLAny,
  LaAviso1, LaAviso2) then
  begin
    (* Criando o Documento XML e Gravando cabe�alho... *)
    arqXML := TXMLDocument.Create(nil);
    arqXML.Active := False;
    arqXML.FileName := '';
    EveXML := GetEventoCTe(arqXML);
    arqXML.Version := sXML_Version;
    arqXML.Encoding := sXML_Encoding;
    (*
    arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
    arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
    arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviCTe_v1.12.xsd';
    *)

    EveXML.versao := verEventosCCe_Layout;
    EveXML.InfEvento.Id := Id;
    EveXML.InfEvento.COrgao := Geral.FFN(cOrgao, 2);
    EveXML.InfEvento.TpAmb := Geral.FF0(tpAmb);
    EveXML.InfEvento.CNPJ := Geral.SoNumero_TT(CNPJ);
    EveXML.InfEvento.ChCTe := Geral.SoNumero_TT(chCTe);
    EveXML.InfEvento.DhEvento := XXe_PF.FDT_XXe_UTC(dhEvento, Null(*TZD_UTC*));
    EveXML.InfEvento.TpEvento := Geral.FF0(tpEvento);
    EveXML.InfEvento.NSeqEvento := Geral.FF0(nSeqEvento);
    EveXML.InfEvento.DetEvento.VersaoEvento := verEventosCCe_CCeerr;
    AnyNode := EveXML.InfEvento.DetEvento.AddChild('_');
    AnyNode.Text := '';
    //

    Texto := EveXML.XML;
    Texto := Geral.Substitui(Texto, '<_></_>', XMLAny);
    Texto := StringReplace(Texto, #10, '', [rfReplaceAll] );
    Texto := StringReplace(Texto, #13, '', [rfReplaceAll] );

    // Assinar!
    NumeroSerial := DmCTe_0000.QrFilialCTeSerNum.Value;
    if XXe_PF.ObtemCertificado(NumeroSerial, Cert) then
      Result := CTe_PF.AssinarMSXML(Texto, Cert, XMLAssinado);
    arqXML := nil;
  end else
    Geral.MB_Erro('N�o foi possivel gerar o XML any!');
end;

function TCTeEveGeraXMLCCe_0200a.GeraXML_CCe(const CTeEveRCCeItsControle,
  CTeEveRCCeItsConta: Integer; const xCondUso: String; var XML: String; const
  LaAviso1, LaAviso2: TLabel): Boolean;
var
  //strChaveAcesso,
  NumeroSerial: String;
  //DadosTxt,
  Texto, XMLAny: String;
  AnyNode: IXMLNode;
  nroItemAlterado: Integer;
  Qry: TmySQLQuery;
begin
  Result := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  //
  //if GeraXML_Correcoes(CTeEveRCCeItsConta, XMLAny, LaAviso1, LaAviso2) then
  begin
  (* Criando o Documento XML e Gravando cabe�alho... *)
    arqXML := TXMLDocument.Create(nil);
    arqXML.Active := False;
    arqXML.FileName := '';
    CCeXML := GetevCCeCTe(arqXML);
    arqXML.Version := sXML_Version;
    arqXML.Encoding := sXML_Encoding;
    (*
    arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/1000/09/xmldsig#';
    arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/1001/XMLSchema-instance';
    arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/cte/enviCTe_v1.12.xsd';
    *)

    //CCeXML.versao := verEventosCCe_Layout;
    CCeXML.DescEvento := 'Carta de Correcao';



//    EvCCeCTe_infCorrecaoList := GetEvCCeCTe_infCorrecaoList(arqXML);
    //(IXMLEvCCeCTe_infCorrecao(CCeXML.InfCorrecao.AddChild('infCorrecao'));

    Qry := TmySQLQuery.Create(Dmod.MyDB);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * FROM cteevercceits ',
      'WHERE FatID=' + Geral.FF0(FFatID),
      'AND FatNum=' + Geral.FF0(FFatNum),
      'AND Empresa=' + Geral.FF0(FEmpresa),
      'AND Controle=' + Geral.FF0(CTeEveRCCeItsControle),
      'AND Conta=' + Geral.FF0(CTeEveRCCeItsConta),
      '']);
      //
      Qry.First;
      while not Qry.Eof do
      begin
        EvCCeCTe_infCorrecao := CCeXML.InfCorrecao.Add;
        EvCCeCTe_infCorrecao.GrupoAlterado := Qry.FieldByName('GrupoAlterado').AsString;
        EvCCeCTe_infCorrecao.CampoAlterado := Qry.FieldByName('CampoAlterado').AsString;
        EvCCeCTe_infCorrecao.ValorAlterado := Qry.FieldByName('ValorAlterado').AsString;
        nroItemAlterado := Qry.FieldByName('nroItemAlterado').AsInteger;
        if nroItemAlterado > 0 then
          EvCCeCTe_infCorrecao.NroItemAlterado := Geral.FF0(nroItemAlterado);
        //
        Qry.Next;
      end;
    finally
      Qry.Free;
    end;

    //

    CCeXML.XCondUso := xCondUso;
    Texto := CCeXML.XML;
    Texto := StringReplace(Texto, #10, '', [rfReplaceAll] );
    Texto := StringReplace(Texto, #13, '', [rfReplaceAll] );

    XML := Geral.Substitui(Texto, ' xmlns="http://www.portalfiscal.inf.br/cte"', '');
    XML := Geral.Substitui(Texto, slineBreak, '');
    //
    arqXML := nil;
    Result := XML <> '';
  end;
end;

{
function TCTeEveGeraXMLCCe_0200a.GeraXML_Correcoes(
  const CTeEveRCCeItsConta: Integer; var XML: String; const LaAviso1,
  LaAviso2: TLabel): Boolean;
  var XML: String; const LaAviso1, LaAviso2: TLabel): Boolean;
var
  //strChaveAcesso,
  NumeroSerial: String;
  //DadosTxt,
  Texto, XMLAny: String;
  AnyNode: IXMLNode;
begin
  Result := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  //
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  CCeXML := GetevCCeCTe(arqXML);
  arqXML.Version := sXML_Version;
  arqXML.Encoding := sXML_Encoding;
  (*
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/1000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/1001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/cte/enviCTe_v1.12.xsd';
  *)

  //CCeXML.versao := verEventosCCe_Layout;
  CCeXML.DescEvento := 'Inclusao Condutor';
  EvCCeCTe_infCorrecao := CCeXML.InfCorrecao

  CCeXML.Condutor.XNome      := xNome;
  CCeXML.Condutor.CPF        := Geral.SoNumero_TT(CPF);
  //

  Texto := CCeXML.XML;
  Texto := StringReplace(Texto, #10, '', [rfReplaceAll] );
  Texto := StringReplace(Texto, #13, '', [rfReplaceAll] );

  XML := Geral.Substitui(Texto, ' xmlns="http://www.portalfiscal.inf.br/cte"', '');
  XML := Geral.Substitui(Texto, slineBreak, '');
  //
  arqXML := nil;
  Result := XML <> '';
end;
}

end.
