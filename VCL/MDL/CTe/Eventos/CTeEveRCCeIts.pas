unit CTeEveRCCeIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, Vcl.Mask, dmkEditDateTimePicker,
  dmkMemo;

type
  TFmCTeEveRCCeIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrGrupos: TmySQLQuery;
    DsGrupos: TDataSource;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Panel5: TPanel;
    Panel6: TPanel;
    EdGrupo: TdmkEditCB;
    CBGrupo: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrGruposVersao: TFloatField;
    QrGruposGrupo: TIntegerField;
    QrGruposCodigo: TIntegerField;
    QrGruposCampo: TWideStringField;
    QrGruposNivel: TIntegerField;
    QrGruposDescricao: TWideStringField;
    QrGruposElemento: TWideStringField;
    QrGruposTipo: TWideStringField;
    QrGruposOcorMin: TSmallintField;
    QrGruposOcorMax: TIntegerField;
    QrGruposTamMin: TSmallintField;
    QrGruposTamMax: TIntegerField;
    QrGruposTamVar: TWideStringField;
    QrGruposObservacao: TWideMemoField;
    QrGruposInfoVazio: TSmallintField;
    QrGruposDominio: TWideStringField;
    QrGruposExpReg: TWideStringField;
    QrGruposFormatStr: TWideStringField;
    QrGruposDeciCasas: TSmallintField;
    QrGruposCartaCorrige: TSmallintField;
    QrGruposAlterWeb: TSmallintField;
    QrGruposAtivo: TSmallintField;
    QrCampos: TmySQLQuery;
    DsCampos: TDataSource;
    QrCamposVersao: TFloatField;
    QrCamposGrupo: TIntegerField;
    QrCamposCodigo: TIntegerField;
    QrCamposCampo: TWideStringField;
    QrCamposNivel: TIntegerField;
    QrCamposDescricao: TWideStringField;
    QrCamposElemento: TWideStringField;
    QrCamposTipo: TWideStringField;
    QrCamposOcorMin: TSmallintField;
    QrCamposOcorMax: TIntegerField;
    QrCamposTamMin: TSmallintField;
    QrCamposTamMax: TIntegerField;
    QrCamposTamVar: TWideStringField;
    QrCamposObservacao: TWideMemoField;
    QrCamposInfoVazio: TSmallintField;
    QrCamposDominio: TWideStringField;
    QrCamposExpReg: TWideStringField;
    QrCamposFormatStr: TWideStringField;
    QrCamposDeciCasas: TSmallintField;
    QrCamposCartaCorrige: TSmallintField;
    QrCamposAlterWeb: TSmallintField;
    QrCamposAtivo: TSmallintField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    EdCampo: TdmkEditCB;
    CBCampo: TdmkDBLookupComboBox;
    QrNextGru: TmySQLQuery;
    QrNextGruCodigo: TIntegerField;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    PCTipoValor: TPageControl;
    TabSheet0: TTabSheet;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    LaTexto: TLabel;
    EdTexto: TdmkEdit;
    MeTexto: TdmkMemo;
    LaInteiro: TLabel;
    EdInteiro: TdmkEdit;
    LaDecimal: TLabel;
    EdDecimal: TdmkEdit;
    TPData: TdmkEditDateTimePicker;
    LaData: TLabel;
    LaHora: TLabel;
    EdHora: TdmkEdit;
    LanroItemAlterado: TLabel;
    EdnroItemAlterado: TdmkEdit;
    CkContinuar: TCheckBox;
    CBLayC: TdmkDBLookupComboBox;
    EdLayC: TdmkEditCB;
    Label5: TLabel;
    QrLayC: TmySQLQuery;
    DsLayC: TDataSource;
    QrLayCGrupo: TIntegerField;
    QrLayCNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGrupoRedefinido(Sender: TObject);
    procedure EdCampoRedefinido(Sender: TObject);
    procedure EdTextoExit(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdLayCRedefinido(Sender: TObject);
  private
    { Private declarations }
    FVersao: Double;
    FAllFormat: TAllFormat;
    //
    procedure ReopenGrupo();
  public
    { Public declarations }
    FQry: TmySQLQuery;
    FFatID, FFatNum, FEmpresa, FControle, FConta, FIDItem: Integer;
  end;

  var
  FmCTeEveRCCeIts: TFmCTeEveRCCeIts;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral, ModuleCTe_0000, UMySQLModule;

{$R *.DFM}

procedure TFmCTeEveRCCeIts.BtOKClick(Sender: TObject);
var
  GrupoAlterado, CampoAlterado, ValorAlterado: String;
  FatID, FatNum, Empresa, Controle, Conta, IDItem, LayC_Grupo, LayI_Grupo,
  LayI_Codigo, nroItemAlterado: Integer;
  LayI_Versao: Double;
  DataHora: TDateTime;
  SQLType: TSQLType;
  Erro: Boolean;
begin
  SQLType         := ImgTipo.SQLType;
  FatID           := FFatID;
  FatNum          := FFatNum;
  Empresa         := FEmpresa;
  Controle        := FControle;
  Conta           := FConta;
  IDItem          := FIDItem;
  LayC_Grupo      := EdLayC.ValueVariant;
  LayI_Versao     := QrCamposVersao.Value;
  LayI_Grupo      := EdGrupo.ValueVariant;
  LayI_Codigo     := EdCampo.ValueVariant;
  GrupoAlterado   := CBGrupo.Text;
  CampoAlterado   := CBCampo.Text;
  ValorAlterado   := '';
  nroItemAlterado := EdnroItemAlterado.ValueVariant;
  //
  if TabSheet0.Visible then
  begin
    if EdTexto.Visible then
      ValorAlterado := EdTexto.Text
    else
      ValorAlterado := MeTexto.Text;
  end else
  if TabSheet1.Visible then
  begin
    ValorAlterado := Geral.FF0(EdInteiro.ValueVariant);
  end else
  if TabSheet2.Visible then
  begin
    ValorAlterado :=
      Geral.FFT_Dot(EdDecimal.ValueVariant, EdDecimal.DecimalSize, siNegativo);
  end else
  if TabSheet3.Visible then
  begin
    ValorAlterado := '';
    if TPData.Visible then
      ValorAlterado := Geral.FDT(TPData.Date, 1);
    if EdHora.Visible then
    begin
      if ValorAlterado <> '' then
        ValorAlterado := ValorAlterado + 'T';
      ValorAlterado := ValorAlterado + Geral.FDT(EdHora.ValueVariant, 102)
    end;
  end;
  //
  if MyObjects.FIC(LayI_Grupo = 0, EdGrupo, 'Informe o Grupo!') then Exit;
  if MyObjects.FIC(LayI_Codigo = 0, EdCampo, 'Informe o Campo!') then Exit;
  if MyObjects.FIC(QrCamposCartaCorrige.Value = 0, nil,
  'O campo selecionado consta na rela��o de campos que n�o podem ser corrigidos no Anexo VII do MOC!')
  then exit;
  Erro := (nroItemAlterado = 0) and (QrCamposOcorMax.Value > 1);
  if MyObjects.FIC(Erro, EdnroItemAlterado, 'Informe o item!') then Exit;
  Erro := (ValorAlterado = '') and (QrCamposInfoVazio.Value = 0);
  if MyObjects.FIC(Erro, nil, 'O campo exige preenchimento!') then Exit;
  //
  IDItem := UMyMod.BPGS1I32('cteevercceits', 'IDItem', '', '', tsPos, SQLType, IDItem);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cteevercceits', False, [
  'LayI_Versao', 'LayI_Grupo', 'LayI_Codigo',
  'GrupoAlterado', 'CampoAlterado', 'ValorAlterado',
  'nroItemAlterado'], [
  'FatID', 'FatNum', 'Empresa', 'Controle', 'Conta', 'IDItem'], [
  LayI_Versao, LayI_Grupo, LayI_Codigo,
  GrupoAlterado, CampoAlterado, ValorAlterado,
  nroItemAlterado], [
  FatID, FatNum, Empresa, Controle, Conta, IDItem], True) then
  begin
    UnDmkDAC_PF.AbreQuery(FQry, Dmod.MyDB);
    if CkContinuar.Checked then
    begin
      SQLType := stIns;
    end else
      Close;
  end;
end;

procedure TFmCTeEveRCCeIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCTeEveRCCeIts.EdCampoRedefinido(Sender: TObject);
var
  Item, TamMax: Integer;
  FmtStr, Tipo: String;
  Mul: Boolean;
begin
  FAllFormat := dmktfUnknown;
  //
(*
dmktfNone, dmktfString, dmktfDouble, dmktfInteger, dmktfLongint,
                 dmktfInt64, dmktfDate, dmktfTime, dmktfDateTime, dmktfMesAno,
                 dmktf_AAAAMM,
                 // Deve ser o �ltimo:
                 dmktfUnknown);
*)
  Item := -1;
  PCTipoValor.Visible := True;
  FmtStr  := Uppercase(QrCamposFormatStr.Value);
  TamMax  := QrCamposTamMax.Value;
  Tipo    := Trim(QrCamposTipo.Value);
  if ((Tipo = 'C') and (FmtStr = ''))
  or ((Tipo = 'N') and (TamMax > 18) ) then
  begin
    FAllFormat := dmktfString;
    Item := 0;
    LaTexto.Caption := QrCamposDescricao.Value + ':';
    if QrCamposTamMax.Value <= 100 then
    begin
      EdTexto.MaxLength := TamMax;
      EdTexto.Visible := True;
      MeTexto.Visible := False;
    end else
    begin
      if TamMax < 500 then
        MeTexto.MaxLength := TamMax
      else
        MeTexto.MaxLength := 500;
      //
      MeTexto.Visible := True;
      EdTexto.Visible := False;
    end;
  end else
  if (Tipo = 'D') or (Tipo = 'T')
  or (Pos('YY-MM-DD', FmtStr) > 0)
  or (Pos('HH:NN', FmtStr) > 0) then
  begin
    Item := 3;
    if pos('YY-MM-DD', FmtStr) > 0 then
    begin
      FAllFormat := dmktfDate;
      LaData.Visible := True;
      TPData.Visible := True;
    end else
    begin
      LaData.Visible := False;
      TPData.Visible := False;
    end;
    if (pos('HH:NN', FmtStr) > 0) or (Tipo = 'T') then
    begin
      if FAllFormat = dmktfDate then
        FAllFormat := dmktfDateTime
      else
      FAllFormat := dmktfTime;
      //
      LaHora.Visible := True;
      EdHora.Visible := True;
    end else
    begin
      LaHora.Visible := False;
      EdHora.Visible := False;
    end;
  end else
  if Tipo = 'N' then
  begin
    if QrCamposDeciCasas.Value > 0 then
    begin
      FAllFormat := dmktfDouble;
      Item := 2;
      LaDecimal.Caption := QrCamposDescricao.Value + ':';
      EdDecimal.DecimalSize := QrCamposDeciCasas.Value;
    end else
    begin
      Item := 1;
      LaInteiro.Caption := QrCamposDescricao.Value + ':';
      if TamMax > 9 then
        FAllFormat := dmktfInt64
      else
        FAllFormat := dmktfInteger;
      EdInteiro.FormatType := FAllFormat;
    end;
  end else
    Geral.MB_Aviso('Tipo n�o implementado: ' + QrCamposTipo.Value);
  //
  Mul := QrCamposOcorMax.Value > 1;
  LanroItemAlterado.Enabled := Mul;
  EdnroItemAlterado.Enabled := Mul;
  if not Mul then
    EdnroItemAlterado.ValueVariant := 0;
  //
  PCTipoValor.Visible := False;
  //
  TabSheet0.TabVisible := True;
  TabSheet1.TabVisible := True;
  TabSheet2.TabVisible := True;
  TabSheet3.TabVisible := True;
  //
  PCTipoValor.ActivePageIndex := Item;
  case Item of
    0: PCTipoValor.ActivePage := TabSheet0;
    1: PCTipoValor.ActivePage := TabSheet1;
    2: PCTipoValor.ActivePage := TabSheet2;
    3: PCTipoValor.ActivePage := TabSheet3;
  end;
  //
  TabSheet0.TabVisible := Item = 0;
  TabSheet1.TabVisible := Item = 1;
  TabSheet2.TabVisible := Item = 2;
  TabSheet3.TabVisible := Item = 3;
  //
  PCTipoValor.Visible := True;
  //
  DBEdit2.Visible := EdCampo.ValueVariant <> 0;
end;

procedure TFmCTeEveRCCeIts.EdGrupoRedefinido(Sender: TObject);
var
  Grupo, Codigo, Nivel: Integer;
  Versao: Double;
  SQLGruFim: String;
begin
  Codigo := EdGrupo.ValueVariant;
  if Codigo <> 0 then
  begin
    Codigo := QrGruposCodigo.Value;
    Grupo  := QrGruposGrupo.Value;
    Versao := FVersao;
    //
    Nivel  := QrGruposNivel.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNextGru, Dmod.MyDB, [
    'SELECT * ',
    'FROM ctelayi ',
    'WHERE  Versao=' + Geral.FFT_Dot(Versao, 2, siPositivo),
    'AND Grupo=' + Geral.FF0(Grupo),
    'AND  ',
    '  (Elemento="G"  ',
    '  OR Elemento="CG") ',
    'AND Codigo>' + Geral.FF0(Codigo),
    'AND Nivel=' + Geral.FF0(Nivel),
    'ORDER BY Codigo ',
    'LIMIT 0, 1 ',
    '']);
    if QrNextGruCodigo.Value > 0 then
      SQLGruFim := 'AND Codigo<' + Geral.FF0(QrNextGruCodigo.Value)
    else
      SQLGruFim := '';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCampos, Dmod.MyDB, [
    'SELECT * ',
    'FROM ctelayi ',
    'WHERE  Versao=' + Geral.FFT_Dot(Versao, 2, siPositivo),
    'AND Grupo=' + Geral.FF0(Grupo),
    'AND  ',
    '  (Elemento="A"  ',
    '  OR Elemento="E"  ',
    '  OR Elemento="CE") ',
    'AND Codigo>' + Geral.FF0(Codigo),
    SQLGruFim,
    'AND Nivel=' + Geral.FF0(Nivel + 1),
    'ORDER BY Campo ',
    ' ']);
    //
    EdCampo.ValueVariant := 0;
    CBCampo.KeyValue     := 0;
  end else
    QrCampos.Close;
  //
  DBEdit1.Visible := EdGrupo.ValueVariant <> 0;
end;

procedure TFmCTeEveRCCeIts.EdLayCRedefinido(Sender: TObject);
begin
  ReopenGrupo();
end;

procedure TFmCTeEveRCCeIts.EdTextoExit(Sender: TObject);
begin
  if QrCamposTipo.Value = 'N' then
    EdTexto.Text := Geral.SoNumero_TT(EdTexto.Text);
end;

procedure TFmCTeEveRCCeIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCTeEveRCCeIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAllFormat      := dmktfUnknown;
  FFatID          := 0;
  FFatNum         := 0;
  FEmpresa        := 0;
  FControle       := 0;
  FConta          := 0;
  FIDItem         := 0;
  MeTexto.Top     := EdTexto.Top;
  //
  DmCTe_0000.ReopenCTeLayI();
  FVersao := DmCTe_0000.QrOpcoesCTeCTeVersao.Value;
  //
  UnDmkDAC_PF.AbreQuery(QrLayC, Dmod.MyDB);
  ReopenGrupo();
  TPData.Date := DmodG.ObtemAgora();
end;

procedure TFmCTeEveRCCeIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCTeEveRCCeIts.ReopenGrupo();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGrupos, Dmod.MyDB, [
  'SELECT *  ',
  'FROM ctelayi ',
  'WHERE Grupo=' + Geral.FF0(EdLayC.ValueVariant),
  'AND (Elemento="G" ',
  '  OR Elemento="CG") ',
  'ORDER BY Campo, Codigo ',
  '']);
  EdGrupo.ValueVariant := 0;
  CBGrupo.KeyValue     := 0;
end;

end.
