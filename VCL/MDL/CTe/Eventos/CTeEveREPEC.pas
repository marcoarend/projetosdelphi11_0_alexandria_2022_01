unit CTeEveREPEC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkRadioGroup, dmkMemo, UnCTe_PF, UnDmkEnums, UnXXe_PF, Vcl.Mask;

type
  TFmCTeEveREPEC = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Painel1: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrJust: TmySQLQuery;
    QrJustCodigo: TIntegerField;
    QrJustNome: TWideStringField;
    QrJustCodUsu: TIntegerField;
    QrJustAplicacao: TIntegerField;
    DsJust: TDataSource;
    GroupBox2: TGroupBox;
    PnJustificativa: TPanel;
    Label7: TLabel;
    EdJust: TdmkEditCB;
    CBJust: TdmkDBLookupComboBox;
    Panel2: TPanel;
    Label1: TLabel;
    Edide_serie: TdmkEdit;
    Label2: TLabel;
    Edide_nCT: TdmkEdit;
    Memo1: TMemo;
    Label13: TLabel;
    EdvCarga: TdmkEdit;
    Label22: TLabel;
    EdvTPrest: TdmkEdit;
    Label23: TLabel;
    EdvICMS: TdmkEdit;
    QrToma4s: TmySQLQuery;
    QrToma4sCodigo: TIntegerField;
    QrToma4sNOMEENT: TWideStringField;
    QrToma4sCIDADE: TWideStringField;
    QrToma4sNOMEUF: TWideStringField;
    QrToma4sCodUsu: TIntegerField;
    QrToma4sIE: TWideStringField;
    QrToma4sindIEDest: TIntegerField;
    QrToma4sTipo: TIntegerField;
    DsToma4s: TDataSource;
    Label17: TLabel;
    EdUFIni: TdmkEdit;
    Label18: TLabel;
    EdUFFim: TdmkEdit;
    Label10: TLabel;
    EdToma4: TdmkEditCB;
    CBToma4: TdmkDBLookupComboBox;
    SbToma4: TSpeedButton;
    RGModal: TdmkRadioGroup;
    GroupBox3: TGroupBox;
    Panel5: TPanel;
    Label4: TLabel;
    EdToma04_UF: TDBEdit;
    QrToma4sCNPJ_CPF: TWideStringField;
    Label5: TLabel;
    EdToma04_CNPJ_CPF: TDBEdit;
    Label6: TLabel;
    EdToma04_IE: TDBEdit;
    RGToma04_toma: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdJustChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCTeEveREPEC: TFmCTeEveREPEC;

implementation

uses UnMyObjects, Module, UnCTe_Tabs, CTeEveRCab, ModuleGeral, UMySQLModule,
  CTeEveGeraXMLEPEC_0200a, ModuleCTe_0000;

{$R *.DFM}

procedure TFmCTeEveREPEC.BtOKClick(Sender: TObject);
var
  Destino, Dir: String;
  Status, FatID, FatNum, Empresa, Controle, Conta, CodInfoTomad: Integer;
  // XML
  CNPJ_CPF, XML_Eve: String;
  xJust, Toma04_UF, Toma04_CNPJ, Toma04_CPF, Toma04_IE, UFIni, UFFim: String;
  nJust, Toma04_toma, Modal: Integer;
  vICMS, vTPrest, vCarga: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  if not DmCTe_0000.ObtemDirXML(CTE_EXT_EVE_ENV_EPEC_XML, Dir, True) then
    Exit;
  if ImgTipo.SQLType = stIns then
  begin
    if not FmCTeEveRCab.IncluiCTeEveRCab(0, evexxe110113EPEC, FatID, FatNum, Empresa,
    Controle) then
    begin
      Geral.MensagemBox('ERRO ao incluir o cabe�alho gen�rico de evento!',
      'ERRO', MB_OK+MB_ICONERROR);
      Exit;
    end;
    Conta := DModG.BuscaProximoCodigoInt('ctectrl', 'cteeverepec', '', 0);
  end else begin
    FatID := FmCTeEveRCab.QrCTeEveRCabFatID.Value;
    FatNum := FmCTeEveRCab.QrCTeEveRCabFatNum.Value;
    Empresa := FmCTeEveRCab.QrCTeEveRCabEmpresa.Value;
    Controle := FmCTeEveRCab.QrCTeEveRCabControle.Value;
    Conta := FmCTeEveRCab.QrCTeEveREPECConta.Value;
  end;
  //
  CNPJ_CPF       := Geral.SoNumero_TT(EdToma04_CNPJ_CPF.Text);
  nJust          := EdJust.ValueVariant;
  xJust          := Trim(XXe_PF.ValidaTexto_XML(QrJustNome.Value, 'eve.epec.xjust', 'eve.epec.xjust'));
  vICMS          := EdvICMS.ValueVariant;
  vTPrest        := EdvTPrest.ValueVariant;
  vCarga         := EdvCarga.ValueVariant;
  CodInfoTomad   := EdToma4.ValueVariant;
  Toma04_toma    := RGToma04_toma.ItemIndex;
  Toma04_UF      := EdToma04_UF.Text;
  if Length(CNPJ_CPF) = 14 then
    Toma04_CNPJ  := CNPJ_CPF
  else
    Toma04_CPF   := CNPJ_CPF;
  Toma04_IE      := Geral.SoNumero_TT(EdToma04_IE.Text);
  Modal          := RGModal.ItemIndex;
  UFIni          := EdUFIni.ValueVariant;
  UFFim          := EdUFFim.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cteeverepec', False, [
  'nJust', 'xJust', 'vICMS',
  'vTPrest', 'vCarga', 'CodInfoTomad',
  'Toma04_toma', 'Toma04_UF', 'Toma04_CNPJ',
  'Toma04_CPF', 'Toma04_IE', 'Modal',
  'UFIni', 'UFFim'], [
  'FatID', 'FatNum', 'Empresa', 'Controle', 'Conta'], [
  nJust, xJust, vICMS,
  vTPrest, vCarga, CodInfoTomad,
  Toma04_toma, Toma04_UF, Toma04_CNPJ,
  Toma04_CPF, Toma04_IE, Modal,
  UFIni, UFFim], [
  FatID, FatNum, Empresa, Controle, Conta], True) then
  begin
    if UnCTeEveGeraXMLEPEC_0200a.CriarDocumentoEveEPEC(FatID, FatNum, Empresa,
    FmCTeEveRCab.QrERCId.Value, FmCTeEveRCab.QrERCcOrgao.Value,
    FmCTeEveRCab.QrERCtpAmb.Value, FmCTeEveRCab.QrERCTipoEnt.Value,
    FmCTeEveRCab.QrERCCNPJ.Value, FmCTeEveRCab.QrERCCPF.Value,
    FmCTeEveRCab.QrERCchCTe.Value, FmCTeEveRCab.QrERCdhEvento.Value,
    FmCTeEveRCab.QrERCTZD_UTC.Value, FmCTeEveRCab.QrERCtpEvento.Value,
    FmCTeEveRCab.QrERCnSeqEvento.Value, FmCTeEveRCab.QrERCverEvento.Value,
    FmCTeEveRCab.QrERCversao.Value, FmCTeEveRCab.QrERCdescEvento.Value,
    xJust, vICMS, vTPrest, vCarga, Toma04_toma, Toma04_UF, Toma04_CNPJ,
    Toma04_CPF, Toma04_IE, Modal, UFIni, UFFim,
    XML_Eve, LaAviso1, LaAviso2) then
    begin
      Destino := DmCTe_0000.SalvaXML(CTE_EXT_EVE_ENV_EPEC_XML,
      FmCTeEveRCab.QrERCId.Value, XML_Eve, Memo1, True);
      if FileExists(Destino) then
      begin
        Status := Integer(ctemystatusCTeAssinada);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cteevercab', False, [
        'XML_Eve', 'Status'], [
        'FatID', 'FatNum', 'Empresa', 'Controle'], [
        Geral.WideStringToSQLString(XML_Eve), Status], [
        FatID, FatNum, Empresa, Controle], True) then
        begin
          FmCTeEveRCab.ReopenCTeEveRCab(Controle);
          //
          Empresa := FmCTeEveRCab.QrCTeEveRCabEmpresa.Value;
          CTe_PF.MostraFormStepsCTe_EPEC(Empresa,
          Edide_Serie.ValueVariant, Edide_nCT.ValueVariant,
          FmCTeEveRCab.QrERCnSeqEvento.Value, EdJust.ValueVariant,
          CTe_AllModelos, FmCTeEveRCab.QrERCchCTe.Value,
          XML_Eve, FmCTeEveRCab.QrA);
        end;
      end;
    end;
    //
    FmCTeEveRCab.ReopenCTeEveREPEC(0);
    Close;
  end;
end;

procedure TFmCTeEveREPEC.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCTeEveREPEC.EdJustChange(Sender: TObject);
begin
  BtOK.Enabled := EdJust.ValueVariant;
end;

procedure TFmCTeEveREPEC.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCTeEveREPEC.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  UMyMod.AbreQuery(QrJust, Dmod.MyDB);
  UMyMod.AbreQuery(QrToma4s, Dmod.MyDB);
end;

procedure TFmCTeEveREPEC.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
