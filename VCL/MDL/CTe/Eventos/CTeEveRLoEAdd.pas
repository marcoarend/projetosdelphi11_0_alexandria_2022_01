unit CTeEveRLoEAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, Grids, DBGrids,
  dmkImage, UnCTe_PF, UnDmkEnums;

type
  TFmCTeEveRLoEAdd = class(TForm)
    Panel1: TPanel;
    QrCTeEveRCab: TmySQLQuery;
    DBGrid1: TDBGrid;
    DsCTeEveRCab: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCTeEveRCabControle: TIntegerField;
    QrCTeEveRCabtpAmb: TSmallintField;
    QrCTeEveRCabchCTe: TWideStringField;
    QrCTeEveRCabdhEvento: TDateTimeField;
    QrCTeEveRCabtpEvento: TIntegerField;
    QrCTeEveRCabdescEvento: TWideStringField;
    QrCTeEveRCabchCTe_CT_SER: TWideStringField;
    QrCTeEveRCabchCTe_CT_NUM: TWideStringField;
    QrCTeEveRCabFatID: TIntegerField;
    QrCTeEveRCabEmpresa: TIntegerField;
    QrCTeEveRCabFatNum: TIntegerField;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure QrCTeEveRCabCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCTeEveRLoEAdd: TFmCTeEveRLoEAdd;

implementation

uses UnMyObjects, CTeEveRLoE, Module, UMySQLModule, dmkGeral, ModuleCTe_0000;

{$R *.DFM}

procedure TFmCTeEveRLoEAdd.BtConfirmaClick(Sender: TObject);
  procedure AdicionaEventoAoLote();
  var
    EventoLote, FatID, FatNum, Empresa, Status, Controle, ret_cStat: Integer;
    ret_xMotivo: String;
  begin
    EventoLote := FmCTeEveRLoE.QrCTeEveRLoECodigo.Value;
    FatID    := QrCTeEveRCabFatID.Value;
    FatNum   := QrCTeEveRCabFatNum.Value;
    Empresa  := QrCTeEveRCabEmpresa.Value;
    Controle := QrCTeEveRCabControle.Value;
    Status   := Integer(ctemystatusCTeAddedLote);
    ret_cStat   := 0;
    ret_xMotivo := '';
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeevercab', False, [
    'EventoLote', 'Status', 'ret_cStat', 'ret_xMotivo'], [
    'FatID', 'FatNum', 'Empresa', 'Controle'], [
    EventoLote, Status, ret_cStat, ret_xMotivo], [
    FatID, FatNum, Empresa, Controle], True);
  end;
var
  I, N: Integer;
begin
  N := DBGrid1.SelectedRows.Count;
  if N + FmCTeEveRLoE.QrCTeEveRCab.RecordCount > 20 then
  begin
    Geral.MensagemBox('Quantidade extrapola o m�ximo de 20 eventos permitidos por lote!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if N > 1 then
  begin
    if Application.MessageBox(PChar('Confirma a adi��o das ' + IntToStr(N) +
    ' CT-e selecionadas?'), 'Pergunta', MB_ICONQUESTION+MB_YESNOCANCEL) =
    ID_YES then
    begin
      with DBGrid1.DataSource.DataSet do
      for I := 0 to N - 1 do
      begin
        GotoBookmark(pointer(DBGrid1.SelectedRows.Items[I]));
        AdicionaEventoAoLote();
      end;
    end;
  end else AdicionaEventoAoLote();
  FmCTeEveRLoE.ReopenCTeEveRCab();
  Close;
end;

procedure TFmCTeEveRLoEAdd.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCTeEveRLoEAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCTeEveRLoEAdd.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FmCTeEveRLoE.LocCod(FmCTeEveRLoE.QrCTeEveRLoECodigo.Value,
    FmCTeEveRLoE.QrCTeEveRLoECodigo.Value);
end;

procedure TFmCTeEveRLoEAdd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCTeEveRLoEAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCTeEveRLoEAdd.QrCTeEveRCabCalcFields(DataSet: TDataSet);
begin
  QrCTeEveRCabchCTe_CT_SER.Value := Copy(QrCTeEveRCabchCTe.Value,
    DEMONTA_CHV_INI_SerCTe,  DEMONTA_CHV_TAM_SerCTe);
  QrCTeEveRCabchCTe_CT_NUM.Value := Copy(QrCTeEveRCabchCTe.Value,
    DEMONTA_CHV_INI_NumCTe,  DEMONTA_CHV_TAM_NumCTe);
end;

end.
