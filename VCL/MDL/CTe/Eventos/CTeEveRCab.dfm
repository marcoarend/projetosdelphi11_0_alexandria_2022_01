object FmCTeEveRcab: TFmCTeEveRcab
  Tag = 526
  Left = 339
  Top = 185
  Caption = 'CTe-EVENT-001 :: Eventos da CT-e'
  ClientHeight = 492
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
      object BtLote: TBitBtn
        Tag = 570
        Left = 5
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtLoteClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 202
        Height = 32
        Caption = 'Eventos da CT-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 202
        Height = 32
        Caption = 'Eventos da CT-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 202
        Height = 32
        Caption = 'Eventos da CT-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 330
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Splitter1: TSplitter
        Left = 0
        Top = 180
        Width = 784
        Height = 5
        Cursor = crVSplit
        Align = alTop
        ExplicitTop = 160
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 784
        Height = 60
        Align = alTop
        DataSource = DsA
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = DBGrid1DrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'ide_serie'
            Title.Caption = 'S'#233'rie'
            Width = 29
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_nCT'
            Title.Caption = 'N'#186' CT'
            Width = 54
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'cStat'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Status'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'xMotivo'
            Title.Caption = 'Motivo do status'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dhRecbto'
            Title.Caption = 'Data/hora fisco'
            Width = 106
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Cli'
            Title.Caption = 'Cliente'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'vPrest_vTPrest'
            Title.Caption = 'R$ Prest.'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'vPrest_vRec'
            Title.Caption = 'R$ Receb.'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Id'
            Title.Caption = 'Chave de acesso da NF-e'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_dEmi'
            Title.Caption = 'Emiss'#227'o'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_natOp'
            Title.Caption = 'Natureza da opera'#231#227'o'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nProt'
            Title.Caption = 'N'#186' do protocolo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_tpNF'
            Title.Caption = 'tpCT'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatNum'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LoteEnv'
            Title.Caption = 'Lote CTe'
            Width = 51
            Visible = True
          end>
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 60
        Width = 784
        Height = 120
        Align = alTop
        DataSource = DsCTeEveRCab
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'cOrgao'
            Title.Caption = 'UF'
            Width = 21
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EventoLote'
            Title.Caption = 'Lote'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'descEvento'
            Title.Caption = 'Tipo de evento'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dhEvento'
            Title.Caption = 'Data / hora gera'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TZD_UTC'
            Title.Caption = 'Dif UTC'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nSeqEvento'
            Title.Caption = 'nSeq'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tpAmb'
            Title.Caption = 'Amb'
            Width = 81
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Status'
            Width = 38
            Visible = True
          end>
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 185
        Width = 784
        Height = 145
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 2
        object TabSheet1: TTabSheet
          Caption = 'Carta de Corre'#231#227'o'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid3: TDBGrid
            Left = 0
            Top = 0
            Width = 591
            Height = 117
            Align = alLeft
            DataSource = DsCTeEveRCCeIts
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'IDItem'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GrupoAlterado'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CampoAlterado'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor255'
                Title.Caption = 'Valor alterado (primeiros 255 caracteres)'
                Width = 236
                Visible = True
              end>
          end
          object DBMemo1: TDBMemo
            Left = 591
            Top = 0
            Width = 185
            Height = 117
            Align = alClient
            DataField = 'ValorAlterado'
            DataSource = DsCTeEveRCCeIts
            TabOrder = 1
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Cancelamento'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid4: TDBGrid
            Left = 0
            Top = 0
            Width = 776
            Height = 117
            Align = alClient
            DataSource = DsCTeEveRCan
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Emiss'#227'o em Conting'#234'ncia EPEC'
          ImageIndex = 2
          object DBGrid5: TDBGrid
            Left = 0
            Top = 0
            Width = 776
            Height = 117
            Align = alClient
            DataSource = DsCTeEveREPEC
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 378
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 422
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtCCe: TBitBtn
        Tag = 10091
        Left = 4
        Top = 4
        Width = 120
        Height = 40
        Caption = 'C&arta corr.'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtCCeClick
      end
      object BtCan: TBitBtn
        Tag = 455
        Left = 124
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Cancelar CT&-e.'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtCanClick
      end
      object BtEPEC: TBitBtn
        Tag = 560
        Left = 244
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&EPEC'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtEPECClick
      end
    end
  end
  object DsA: TDataSource
    DataSet = QrA
    Left = 52
    Top = 180
  end
  object PMCCe: TPopupMenu
    OnPopup = PMCCePopup
    Left = 44
    Top = 392
    object Crianovacorreo1: TMenuItem
      Caption = '&Cria nova corre'#231#227'o'
      OnClick = Crianovacorreo1Click
    end
    object Alteracorreo1: TMenuItem
      Caption = '&Altera corre'#231#227'o'
      Enabled = False
      OnClick = Alteracorreo1Click
    end
    object Excluicorreo1: TMenuItem
      Caption = '&Exclui corre'#231#227'o'
      Enabled = False
      OnClick = Excluicorreo1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Imprime1: TMenuItem
      Caption = '&Imprime'
      OnClick = Imprime1Click
    end
  end
  object QrCTeEveRCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCTeEveRCabBeforeClose
    AfterScroll = QrCTeEveRCabAfterScroll
    OnCalcFields = QrCTeEveRCabCalcFields
    SQL.Strings = (
      'SELECT * FROM nfeevercab'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'ORDER BY Controle DESC')
    Left = 224
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeEveRCabFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeEveRCabFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeEveRCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeEveRCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeEveRCabEventoLote: TIntegerField
      FieldName = 'EventoLote'
    end
    object QrCTeEveRCabId: TWideStringField
      FieldName = 'Id'
      Size = 54
    end
    object QrCTeEveRCabcOrgao: TSmallintField
      FieldName = 'cOrgao'
    end
    object QrCTeEveRCabtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrCTeEveRCabTipoEnt: TSmallintField
      FieldName = 'TipoEnt'
    end
    object QrCTeEveRCabCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrCTeEveRCabCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrCTeEveRCabchCTe: TWideStringField
      FieldName = 'chCTe'
      Size = 44
    end
    object QrCTeEveRCabdhEvento: TDateTimeField
      FieldName = 'dhEvento'
    end
    object QrCTeEveRCabverEvento: TFloatField
      FieldName = 'verEvento'
    end
    object QrCTeEveRCabtpEvento: TIntegerField
      FieldName = 'tpEvento'
    end
    object QrCTeEveRCabnSeqEvento: TSmallintField
      FieldName = 'nSeqEvento'
    end
    object QrCTeEveRCabversao: TFloatField
      FieldName = 'versao'
    end
    object QrCTeEveRCabdescEvento: TWideStringField
      FieldName = 'descEvento'
      Size = 60
    end
    object QrCTeEveRCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeEveRCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeEveRCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeEveRCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeEveRCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeEveRCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeEveRCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCTeEveRCabTZD_UTC: TFloatField
      FieldName = 'TZD_UTC'
    end
    object QrCTeEveRCabtpAmb_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'tpAmb_TXT'
      Size = 15
      Calculated = True
    end
    object QrCTeEveRCabStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrCTeEveRCabXML_Eve: TWideMemoField
      FieldName = 'XML_Eve'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCTeEveRCabXML_retEve: TWideMemoField
      FieldName = 'XML_retEve'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCTeEveRCabret_versao: TFloatField
      FieldName = 'ret_versao'
    end
    object QrCTeEveRCabret_Id: TWideStringField
      FieldName = 'ret_Id'
      Size = 67
    end
    object QrCTeEveRCabret_tpAmb: TSmallintField
      FieldName = 'ret_tpAmb'
    end
    object QrCTeEveRCabret_verAplic: TWideStringField
      FieldName = 'ret_verAplic'
    end
    object QrCTeEveRCabret_cOrgao: TSmallintField
      FieldName = 'ret_cOrgao'
    end
    object QrCTeEveRCabret_cStat: TIntegerField
      FieldName = 'ret_cStat'
    end
    object QrCTeEveRCabret_xMotivo: TWideStringField
      FieldName = 'ret_xMotivo'
      Size = 255
    end
    object QrCTeEveRCabret_chCTe: TWideStringField
      FieldName = 'ret_chCTe'
      Size = 44
    end
    object QrCTeEveRCabret_tpEvento: TIntegerField
      FieldName = 'ret_tpEvento'
    end
    object QrCTeEveRCabret_xEvento: TWideStringField
      FieldName = 'ret_xEvento'
      Size = 60
    end
    object QrCTeEveRCabret_nSeqEvento: TIntegerField
      FieldName = 'ret_nSeqEvento'
    end
    object QrCTeEveRCabret_CNPJDest: TWideStringField
      FieldName = 'ret_CNPJDest'
      Size = 18
    end
    object QrCTeEveRCabret_CPFDest: TWideStringField
      FieldName = 'ret_CPFDest'
      Size = 18
    end
    object QrCTeEveRCabret_emailDest: TWideStringField
      FieldName = 'ret_emailDest'
      Size = 60
    end
    object QrCTeEveRCabret_dhRegEvento: TDateTimeField
      FieldName = 'ret_dhRegEvento'
    end
    object QrCTeEveRCabret_TZD_UTC: TFloatField
      FieldName = 'ret_TZD_UTC'
    end
    object QrCTeEveRCabret_nProt: TWideStringField
      FieldName = 'ret_nProt'
      Size = 15
    end
  end
  object DsCTeEveRCab: TDataSource
    DataSet = QrCTeEveRCab
    Left = 224
    Top = 180
  end
  object QrCTeEveRCCe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfeevercce'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      'ORDER BY Conta DESC')
    Left = 308
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrCTeEveRCCeFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeEveRCCeFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeEveRCCeEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeEveRCCeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeEveRCCeConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCTeEveRCCenCondUso: TSmallintField
      FieldName = 'nCondUso'
    end
    object QrCTeEveRCCeLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeEveRCCeDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeEveRCCeDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeEveRCCeUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeEveRCCeUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeEveRCCeAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeEveRCCeAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsCTeEveRCCe: TDataSource
    DataSet = QrCTeEveRCCe
    Left = 308
    Top = 180
  end
  object QrERC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfeevercab '
      'WHERE Controle=:P0')
    Left = 160
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrERCFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrERCFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrERCEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrERCControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrERCEventoLote: TIntegerField
      FieldName = 'EventoLote'
    end
    object QrERCId: TWideStringField
      FieldName = 'Id'
      Size = 54
    end
    object QrERCcOrgao: TSmallintField
      FieldName = 'cOrgao'
    end
    object QrERCtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrERCTipoEnt: TSmallintField
      FieldName = 'TipoEnt'
    end
    object QrERCCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrERCCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrERCchCTe: TWideStringField
      FieldName = 'chCTe'
      Size = 44
    end
    object QrERCdhEvento: TDateTimeField
      FieldName = 'dhEvento'
    end
    object QrERCTZD_UTC: TFloatField
      FieldName = 'TZD_UTC'
    end
    object QrERCverEvento: TFloatField
      FieldName = 'verEvento'
    end
    object QrERCtpEvento: TIntegerField
      FieldName = 'tpEvento'
    end
    object QrERCnSeqEvento: TSmallintField
      FieldName = 'nSeqEvento'
    end
    object QrERCversao: TFloatField
      FieldName = 'versao'
    end
    object QrERCdescEvento: TWideStringField
      FieldName = 'descEvento'
      Size = 60
    end
  end
  object PMCan: TPopupMenu
    OnPopup = PMCanPopup
    Left = 164
    Top = 396
    object IncluicancelamentodaCTe1: TMenuItem
      Caption = '&Inclui cancelamento da CTe'
      OnClick = IncluicancelamentodaCTe1Click
    end
    object ExcluicancelamentodaCTe1: TMenuItem
      Caption = '&Exclui cancelamento da CTe'
      OnClick = ExcluicancelamentodaCTe1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Solicitacancelamento1: TMenuItem
      Caption = '&Solicita cancelamento'
      Enabled = False
      OnClick = Solicitacancelamento1Click
    end
  end
  object QrCTeEveRCan: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfeevercan'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      'ORDER BY Conta DESC')
    Left = 388
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrCTeEveRCanFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeEveRCanFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeEveRCanEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeEveRCanControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeEveRCanConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCTeEveRCannProt: TLargeintField
      FieldName = 'nProt'
    end
    object QrCTeEveRCannJust: TIntegerField
      FieldName = 'nJust'
    end
    object QrCTeEveRCanxJust: TWideStringField
      FieldName = 'xJust'
      Size = 255
    end
    object QrCTeEveRCanLk: TIntegerField
      FieldName = 'Lk'
    end
  end
  object DsCTeEveRCan: TDataSource
    DataSet = QrCTeEveRCan
    Left = 388
    Top = 180
  end
  object frxEveCCe: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40022.702970393500000000
    ReportOptions.LastChange = 40927.443534282400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Altura: Extended;                                      '
      'begin'
      '  if <LogoNFeExiste> = True then'
      '  begin              '
      '    Picture1.LoadFromFile(<LogoNFePath>);'
      '  end;'
      '  //'
      '  MeRazao_1.Font.Size := <NFeFTRazao>;'
      '  MeEnder_1.Font.Size := <NFeFTEnder>;'
      '  MeFones_1.Font.Size := <NFeFTFones>;'
      'end.')
    OnGetValue = frxEveCCeGetValue
    Left = 104
    Top = 180
    Datasets = <
      item
        DataSet = frxDsA
        DataSetName = 'frxDsA'
      end
      item
        DataSet = frxDsCTeEveRCab
        DataSetName = 'frxDsNFeEveRCab'
      end
      item
        DataSet = frxDsCTeEveRCCe
        DataSetName = 'frxDsNFeEveRCCe'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      MirrorMargins = True
      LargeDesignHeight = True
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 37.795275590000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
      end
      object PageHeader1: TfrxPageHeader
        Tag = 1
        FillType = ftBrush
        Height = 173.858267720000000000
        Top = 79.370130000000000000
        Width = 793.701300000000000000
        object Memo7: TfrxMemoView
          Left = 582.047620000000000000
          Width = 177.637817240000000000
          Height = 75.590558500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeCanhotoNFe: TfrxMemoView
          Left = 457.322737010000000000
          Width = 124.724409450000000000
          Height = 75.590558500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 30.236240000000000000
          Top = 37.795275590551200000
          Width = 427.086675200000000000
          Height = 136.062992130000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Tag = 1
          Left = 35.905511810000000000
          Top = 41.574810470000000000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo8: TfrxMemoView
          Left = 457.322834650000000000
          Top = 75.590548739999990000
          Width = 302.362204720000000000
          Height = 60.472443390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeRazao_1: TfrxMemoView
          Tag = 1
          Left = 171.968621100000000000
          Top = 41.574830000000010000
          Width = 287.244280000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
        end
        object MeEnder_1: TfrxMemoView
          Tag = 1
          Left = 171.968621100000000000
          Top = 79.370130000000000000
          Width = 283.464750000000000000
          Height = 77.480314960000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_1: TfrxMemoView
          Tag = 1
          Left = 171.968621100000000000
          Top = 156.850401020000000000
          Width = 283.464750000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Tag = 1
          Left = 457.323130000000000000
          Top = 154.960629920000000000
          Width = 302.362204724409000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Tag = 1
          Left = 457.322805350000000000
          Top = 136.062992130000000000
          Width = 302.362204724409000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Tag = 1
          Left = 536.693260000000000000
          Top = 159.385736460000000000
          Width = 221.480290550000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          Tag = 1
          Left = 480.755881100000000000
          Top = 137.708556460000000000
          Width = 274.393710550000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_CNPJ_CPF_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo197: TfrxMemoView
          Tag = 1
          Left = 582.047375900000000000
          Top = 53.669181499999990000
          Width = 64.251966060000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Folha:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo199: TfrxMemoView
          Tag = 1
          Left = 646.299188190000000000
          Top = 53.669181499999990000
          Width = 113.385851180000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCanhotoRec: TfrxMemoView
          Left = 30.236240000000000000
          Width = 427.086497010000000000
          Height = 37.795280470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CARTA DE CORRE'#199#195'O ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 582.047620000000000000
          Width = 177.637910000000000000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Evento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 582.157861100000000000
          Top = 37.795309759999990000
          Width = 64.251966060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora:')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Left = 582.047620000000000000
          Top = 22.677180000000010000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sequencia:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 457.433371100000000000
          Top = 37.795309759999990000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 495.228671100000000000
          Top = 37.795309759999990000
          Width = 86.929190000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Left = 457.323130000000000000
          Top = 52.913400470000010000
          Width = 52.913420000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Emiss'#227'o:')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Left = 510.236550000000000000
          Top = 52.913400470000010000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_dEmi"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 457.323130000000000000
          Top = 22.677180000000010000
          Width = 22.677180000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186)
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 480.000310000000000000
          Top = 22.677180000000010000
          Width = 102.047310000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2m'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = 14211288
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 457.323130000000000000
          Width = 124.724490000000000000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NF-e')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          Left = 646.299188190000000000
          Top = 22.677180000000010000
          Width = 113.385858500000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2m'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = 14211288
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeEveRCab."nSeqEvento"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 646.299188190000000000
          Top = 37.795300000000000000
          Width = 113.385900000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = 14211288
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeEveRCab."dhEvento"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 457.323130000000000000
          Top = 75.590600000000010000
          Width = 302.362400000000000000
          Height = 22.677170240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Registro no Fisco')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 457.323130000000000000
          Top = 98.267780000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Protocolo:')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 517.795610000000000000
          Top = 98.267780000000000000
          Width = 241.889920000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFeEveRCab."ret_nProt"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 457.323130000000000000
          Top = 113.385900000000000000
          Width = 68.031540000000010000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / hora:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 525.354670000000100000
          Top = 113.385900000000000000
          Width = 234.330860000000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFeEveRCab."ret_dhRegEvento"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 820.157497400000000000
        Top = 313.700990000000000000
        Width = 793.701300000000000000
        OnAfterPrint = 'MasterData1OnAfterPrint'
        RowCount = 1
        object Me_E04_Titu: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 12.094485749999990000
          Width = 529.133929060000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NOME / RAZ'#195'O SOCIAL')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 124.724409450000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESTINAT'#193'RIO / REMETENTE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_E02_Titu: TfrxMemoView
          Tag = 1
          Left = 559.370149530000000000
          Top = 12.094485749999990000
          Width = 204.094488190000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ / CPF')
          ParentFont = False
        end
        object Me_E04_Dado: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 20.165342130000000000
          Width = 525.354399060000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."dest_xNome"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Me_E02_Dado: TfrxMemoView
          Tag = 1
          Left = 559.370149530000000000
          Top = 20.165342130000000000
          Width = 204.094488190000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_CNPJ_CPF_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo27: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 44.220469999999970000
          Width = 461.102432990000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ENDERE'#199'O')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 52.291326379999990000
          Width = 457.322902990000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DEST_ENDERECO"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo29: TfrxMemoView
          Tag = 1
          Left = 491.338653460000000000
          Top = 44.220469999999970000
          Width = 185.196850390000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BAIRRO / DISTRITO')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Tag = 1
          Left = 491.338653460000000000
          Top = 52.291326379999990000
          Width = 185.196850390000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."dest_xBairro"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo31: TfrxMemoView
          Tag = 1
          Left = 676.535503860000000000
          Top = 44.220469999999970000
          Width = 86.929133860000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Tag = 1
          Left = 676.535503860000000000
          Top = 52.291326379999990000
          Width = 86.929133860000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."dest_CEP_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo35: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 76.346454250000030000
          Width = 378.330779450000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNIC'#205'PIO')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 84.417310629999970000
          Width = 374.551249450000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_XMUN_TXT"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo37: TfrxMemoView
          Tag = 1
          Left = 408.567019450000000000
          Top = 76.346454250000030000
          Width = 123.212578900000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FONE / FAX')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Tag = 1
          Left = 408.567019450000000000
          Top = 84.417310629999970000
          Width = 123.212578900000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_FONE_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo39: TfrxMemoView
          Tag = 1
          Left = 531.779598340000000000
          Top = 76.346454250000030000
          Width = 27.590551180000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Tag = 1
          Left = 531.779598340000000000
          Top = 84.417310629999970000
          Width = 27.590551180000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."dest_UF"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo41: TfrxMemoView
          Tag = 1
          Left = 559.370149530000000000
          Top = 76.346454250000030000
          Width = 204.094488190000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Tag = 1
          Left = 559.370149530000000000
          Top = 84.417310629999970000
          Width = 204.094488190000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_IE_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 109.606370000000000000
          Width = 124.724409450000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCRI'#199#195'O DA CORRE'#199#195'O:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 124.724490000000000000
          Width = 733.606599450000000000
          Height = 491.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsNFeEveRCCe."xCorrecao"]')
          ParentFont = False
          WordBreak = True
        end
        object Memo12: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 616.063390000000000000
          Width = 124.724409450000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CONDI'#199#195'O DE USO ACEITA:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 631.181509999999900000
          Width = 733.606599450000000000
          Height = 188.976377950000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[xCondUso]')
          ParentFont = False
          WordBreak = True
        end
      end
    end
  end
  object frxDsA: TfrxDBDataset
    UserName = 'frxDsA'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NO_Cli=NO_Cli'
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'LoteEnv=LoteEnv'
      'Id=Id'
      'ide_natOp=ide_natOp'
      'ide_serie=ide_serie'
      'ide_nNF=ide_nNF'
      'ide_dEmi=ide_dEmi'
      'ide_tpNF=ide_tpNF'
      'ICMSTot_vProd=ICMSTot_vProd'
      'ICMSTot_vNF=ICMSTot_vNF'
      'nProt=nProt'
      'xMotivo=xMotivo'
      'dhRecbto=dhRecbto'
      'IDCtrl=IDCtrl'
      'dest_CNPJ=dest_CNPJ'
      'dest_CPF=dest_CPF'
      'dest_xNome=dest_xNome'
      'versao=versao'
      'ICMSTot_vST=ICMSTot_vST'
      'ICMSTot_vFrete=ICMSTot_vFrete'
      'ICMSTot_vSeg=ICMSTot_vSeg'
      'ICMSTot_vIPI=ICMSTot_vIPI'
      'ICMSTot_vOutro=ICMSTot_vOutro'
      'ICMSTot_vDesc=ICMSTot_vDesc'
      'ICMSTot_vBC=ICMSTot_vBC'
      'ICMSTot_vICMS=ICMSTot_vICMS'
      'ide_tpEmis=ide_tpEmis'
      'NOME_tpEmis=NOME_tpEmis'
      'NOME_tpNF=NOME_tpNF'
      'infCanc_xJust=infCanc_xJust'
      'Status=Status'
      'infCanc_dhRecbto=infCanc_dhRecbto'
      'infCanc_nProt=infCanc_nProt'
      'cStat=cStat'
      'ID_TXT=ID_TXT'
      'EMIT_ENDERECO=EMIT_ENDERECO'
      'EMIT_FONE_TXT=EMIT_FONE_TXT'
      'EMIT_IE_TXT=EMIT_IE_TXT'
      'TRANSPORTA_CNPJ_CPF_TXT=TRANSPORTA_CNPJ_CPF_TXT'
      'TRANSPORTA_IE_TXT=TRANSPORTA_IE_TXT'
      'EMIT_CNPJ_CPF_TXT=EMIT_CNPJ_CPF_TXT'
      'EMIT_IEST_TXT=EMIT_IEST_TXT'
      'DEST_ENDERECO=DEST_ENDERECO'
      'DEST_CEP_TXT=DEST_CEP_TXT'
      'DEST_FONE_TXT=DEST_FONE_TXT'
      'DEST_CNPJ_CPF_TXT=DEST_CNPJ_CPF_TXT'
      'DEST_IE_TXT=DEST_IE_TXT'
      'MODFRETE_TXT=MODFRETE_TXT'
      'DEST_XMUN_TXT=DEST_XMUN_TXT'
      'DOC_SEM_VLR_JUR=DOC_SEM_VLR_JUR'
      'ide_DSaiEnt_Txt=ide_DSaiEnt_Txt'
      'DOC_SEM_VLR_FIS=DOC_SEM_VLR_FIS'
      'emit_xLgr=emit_xLgr'
      'emit_nro=emit_nro'
      'emit_xCpl=emit_xCpl'
      'emit_xBairro=emit_xBairro'
      'emit_xMun=emit_xMun'
      'emit_UF=emit_UF'
      'emit_CEP=emit_CEP'
      'emit_xPais=emit_xPais'
      'emit_IE=emit_IE'
      'emit_IEST=emit_IEST'
      'emit_CNPJ=emit_CNPJ'
      'emit_fone=emit_fone'
      'dest_xLgr=dest_xLgr'
      'dest_nro=dest_nro'
      'dest_xCpl=dest_xCpl'
      'dest_xMun=dest_xMun'
      'dest_CEP=dest_CEP'
      'dest_fone=dest_fone'
      'dest_IE=dest_IE'
      'dest_UF=dest_UF'
      'transporta_CNPJ=transporta_CNPJ'
      'transporta_CPF=transporta_CPF'
      'transporta_IE=transporta_IE'
      'transporta_UF=transporta_UF'
      'ide_tpAmb=ide_tpAmb'
      'infProt_nProt=infProt_nProt'
      'infProt_dhRecbto=infProt_dhRecbto'
      'ide_DSaiEnt=ide_DSaiEnt'
      'ModFrete=ModFrete'
      'emit_xNome=emit_xNome'
      'dest_xBairro=dest_xBairro')
    BCDToCurrency = False
    Left = 108
    Top = 132
  end
  object frxDsCTeEveRCCe: TfrxDBDataset
    UserName = 'frxDsNFeEveRCCe'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'Controle=Controle'
      'Conta=Conta'
      'nCondUso=nCondUso'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrCTeEveRCCe
    BCDToCurrency = False
    Left = 576
    Top = 176
  end
  object frxDsCTeEveRCab: TfrxDBDataset
    UserName = 'frxDsNFeEveRCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'Controle=Controle'
      'EventoLote=EventoLote'
      'Id=Id'
      'cOrgao=cOrgao'
      'tpAmb=tpAmb'
      'TipoEnt=TipoEnt'
      'CNPJ=CNPJ'
      'CPF=CPF'
      'chNFe=chNFe'
      'dhEvento=dhEvento'
      'verEvento=verEvento'
      'tpEvento=tpEvento'
      'nSeqEvento=nSeqEvento'
      'versao=versao'
      'descEvento=descEvento'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'TZD_UTC=TZD_UTC'
      'tpAmb_TXT=tpAmb_TXT'
      'Status=Status'
      'XML_Eve=XML_Eve'
      'XML_retEve=XML_retEve'
      'ret_versao=ret_versao'
      'ret_Id=ret_Id'
      'ret_tpAmb=ret_tpAmb'
      'ret_verAplic=ret_verAplic'
      'ret_cOrgao=ret_cOrgao'
      'ret_cStat=ret_cStat'
      'ret_xMotivo=ret_xMotivo'
      'ret_chNFe=ret_chNFe'
      'ret_tpEvento=ret_tpEvento'
      'ret_xEvento=ret_xEvento'
      'ret_nSeqEvento=ret_nSeqEvento'
      'ret_CNPJDest=ret_CNPJDest'
      'ret_CPFDest=ret_CPFDest'
      'ret_emailDest=ret_emailDest'
      'ret_dhRegEvento=ret_dhRegEvento'
      'ret_TZD_UTC=ret_TZD_UTC'
      'ret_nProt=ret_nProt')
    DataSet = QrCTeEveRCab
    BCDToCurrency = False
    Left = 576
    Top = 128
  end
  object PMEPEC: TPopupMenu
    OnPopup = PMEPECPopup
    Left = 284
    Top = 396
    object IncluiEPEC1: TMenuItem
      Caption = '&Inclui EPEC'
      OnClick = IncluiEPEC1Click
    end
    object ExcluiEPEC1: TMenuItem
      Caption = '&Exclui EPEC'
      OnClick = ExcluiEPEC1Click
    end
  end
  object QrCTeEveREPEC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cteeverepec'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      'ORDER BY Conta DESC')
    Left = 472
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrCTeEveREPECFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeEveREPECFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeEveREPECEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeEveREPECControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeEveREPECConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCTeEveREPECnJust: TIntegerField
      FieldName = 'nJust'
    end
    object QrCTeEveREPECxJust: TWideStringField
      FieldName = 'xJust'
      Size = 255
    end
    object QrCTeEveREPECvICMS: TFloatField
      FieldName = 'vICMS'
    end
    object QrCTeEveREPECvTPrest: TFloatField
      FieldName = 'vTPrest'
    end
    object QrCTeEveREPECvCarga: TFloatField
      FieldName = 'vCarga'
    end
    object QrCTeEveREPECCodInfoTomad: TIntegerField
      FieldName = 'CodInfoTomad'
    end
    object QrCTeEveREPECToma04_toma: TSmallintField
      FieldName = 'Toma04_toma'
    end
    object QrCTeEveREPECToma04_UF: TWideStringField
      FieldName = 'Toma04_UF'
      Size = 2
    end
    object QrCTeEveREPECToma04_CNPJ: TWideStringField
      FieldName = 'Toma04_CNPJ'
      Size = 18
    end
    object QrCTeEveREPECToma04_CPF: TWideStringField
      FieldName = 'Toma04_CPF'
      Size = 18
    end
    object QrCTeEveREPECToma04_IE: TWideStringField
      FieldName = 'Toma04_IE'
      Size = 14
    end
    object QrCTeEveREPECModal: TSmallintField
      FieldName = 'Modal'
    end
    object QrCTeEveREPECUFIni: TWideStringField
      FieldName = 'UFIni'
      Size = 2
    end
    object QrCTeEveREPECUFFim: TWideStringField
      FieldName = 'UFFim'
      Size = 2
    end
    object QrCTeEveREPECLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeEveREPECDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeEveREPECDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeEveREPECUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeEveREPECUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeEveREPECAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeEveREPECAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsCTeEveREPEC: TDataSource
    DataSet = QrCTeEveREPEC
    Left = 472
    Top = 180
  end
  object QrA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrAAfterOpen
    BeforeClose = QrABeforeClose
    AfterScroll = QrAAfterScroll
    OnCalcFields = QrACalcFields
    SQL.Strings = (
      'SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cli,'
      'nfa.FatID, nfa.FatNum, nfa.Empresa, nfa.LoteEnv, nfa.Id,'
      'ide_natOp, ide_serie, ide_nNF, ide_dEmi, ide_tpNF,'
      'ICMSTot_vProd, ICMSTot_vNF, dest_CNPJ, dest_CPF,'
      'dest_xNome,'
      ''
      'emit_xNome, emit_CPF, CodInfoEmit, CodInfoDest,'
      'emit_xLgr, emit_nro, emit_xCpl, emit_xBairro,'
      'emit_xMun, emit_UF, emit_CEP, emit_xPais,'
      'emit_IE, emit_IEST, emit_CNPJ, emit_fone,'
      'dest_xLgr, dest_nro, dest_xCpl, dest_xBairro, dest_xMun,'
      'dest_CEP, dest_fone, dest_IE, dest_UF,'
      'transporta_CNPJ, transporta_CPF,'
      'transporta_IE, transporta_UF,'
      'ide_tpAmb, infProt_nProt, infProt_dhRecbto,'
      'ide_DSaiEnt, ModFrete,'
      ''
      'IF(Importado>0, Status,'
      'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat)) cStat,'
      'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt,'
      'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo,'
      
        'IF(infCanc_cStat>0, infCanc_dhRecbto, infProt_dhRecbto) dhRecbto' +
        ','
      'IDCtrl, versao,'
      'ide_tpEmis, infCanc_xJust, Status,'
      'ICMSTot_vST, ICMSTot_vFrete, ICMSTot_vSeg,'
      'ICMSTot_vIPI, ICMSTot_vOutro, ICMSTot_vDesc,'
      'ICMSTot_vBC, ICMSTot_vICMS,'
      'infCanc_dhRecbto, infCanc_nProt'
      'FROM nfecaba nfa'
      'LEFT JOIN entidades cli ON'
      'cli.Codigo=nfa.CodInfoDest'
      'WHERE nfa.FatID=1'
      'AND nfa.FatNum=1883'
      'AND nfa.Empresa=-11'
      '')
    Left = 52
    Top = 132
    object QrANO_Cli: TWideStringField
      FieldName = 'NO_Cli'
      Size = 100
    end
    object QrAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrAide_nCT: TIntegerField
      FieldName = 'ide_nCT'
    end
    object QrAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrAide_tpCTe: TSmallintField
      FieldName = 'ide_tpCTe'
    end
    object QrAnProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrAxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrAdhRecbto: TWideStringField
      FieldName = 'dhRecbto'
      Required = True
      Size = 19
    end
    object QrAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrAdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrAdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrAdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrAversao: TFloatField
      FieldName = 'versao'
    end
    object QrAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrANOME_tpEmis: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_tpEmis'
      Size = 30
      Calculated = True
    end
    object QrANOME_tpCTe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_tpCTe'
      Size = 1
      Calculated = True
    end
    object QrAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrAcStat: TFloatField
      FieldName = 'cStat'
    end
    object QrAcSitCTe: TSmallintField
      FieldName = 'cSitCTe'
    end
    object QrANO_Emi: TWideStringField
      FieldName = 'NO_Emi'
      Size = 100
    end
    object QrANO_Terceiro: TWideStringField
      FieldName = 'NO_Terceiro'
      Size = 100
    end
    object QrAvPrest_vTPrest: TFloatField
      FieldName = 'vPrest_vTPrest'
    end
    object QrAvPrest_vRec: TFloatField
      FieldName = 'vPrest_vRec'
    end
    object QrAICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrAide_Modal: TSmallintField
      FieldName = 'ide_Modal'
    end
    object QrACodInfoEmite: TIntegerField
      FieldName = 'CodInfoEmite'
    end
    object QrAinfCarga_vCarga: TFloatField
      FieldName = 'infCarga_vCarga'
    end
    object QrACodInfoTomad: TIntegerField
      FieldName = 'CodInfoTomad'
    end
    object QrAide_UFIni: TWideStringField
      FieldName = 'ide_UFIni'
      Size = 2
    end
    object QrAide_UFFim: TWideStringField
      FieldName = 'ide_UFFim'
      Size = 2
    end
    object QrAtoma99_toma: TSmallintField
      FieldName = 'toma99_toma'
    end
  end
  object QrOpcoesCTe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etc0.Nome NO_ETC_0, etc1.Nome NO_ETC_1,  ctr.*'
      'FROM paramsemp ctr'
      'LEFT JOIN entitipcto etc0 ON etc0.Codigo=ctr.EntiTipCto'
      'LEFT JOIN entitipcto etc1 ON etc1.Codigo=ctr.EntiTipCt1'
      'WHERE ctr.Codigo=:P0')
    Left = 676
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOpcoesCTeCTeversao: TFloatField
      FieldName = 'CTeversao'
    end
    object QrOpcoesCTeCTeide_mod: TSmallintField
      FieldName = 'CTeide_mod'
    end
    object QrOpcoesCTeSimplesFed: TSmallintField
      FieldName = 'SimplesFed'
    end
    object QrOpcoesCTeDirCTeGer: TWideStringField
      FieldName = 'DirCTeGer'
      Size = 255
    end
    object QrOpcoesCTeDirCTeAss: TWideStringField
      FieldName = 'DirCTeAss'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEnvLot: TWideStringField
      FieldName = 'DirCTeEnvLot'
      Size = 255
    end
    object QrOpcoesCTeDirCTeRec: TWideStringField
      FieldName = 'DirCTeRec'
      Size = 255
    end
    object QrOpcoesCTeDirCTePedRec: TWideStringField
      FieldName = 'DirCTePedRec'
      Size = 255
    end
    object QrOpcoesCTeDirCTeProRec: TWideStringField
      FieldName = 'DirCTeProRec'
      Size = 255
    end
    object QrOpcoesCTeDirCTeDen: TWideStringField
      FieldName = 'DirCTeDen'
      Size = 255
    end
    object QrOpcoesCTeDirCTePedCan: TWideStringField
      FieldName = 'DirCTePedCan'
      Size = 255
    end
    object QrOpcoesCTeDirCTeCan: TWideStringField
      FieldName = 'DirCTeCan'
      Size = 255
    end
    object QrOpcoesCTeDirCTePedInu: TWideStringField
      FieldName = 'DirCTePedInu'
      Size = 255
    end
    object QrOpcoesCTeDirCTeInu: TWideStringField
      FieldName = 'DirCTeInu'
      Size = 255
    end
    object QrOpcoesCTeDirCTePedSit: TWideStringField
      FieldName = 'DirCTePedSit'
      Size = 255
    end
    object QrOpcoesCTeDirCTeSit: TWideStringField
      FieldName = 'DirCTeSit'
      Size = 255
    end
    object QrOpcoesCTeDirCTePedSta: TWideStringField
      FieldName = 'DirCTePedSta'
      Size = 255
    end
    object QrOpcoesCTeDirCTeSta: TWideStringField
      FieldName = 'DirCTeSta'
      Size = 255
    end
    object QrOpcoesCTeCTeUF_WebServ: TWideStringField
      FieldName = 'CTeUF_WebServ'
      Size = 2
    end
    object QrOpcoesCTeCTeUF_Servico: TWideStringField
      FieldName = 'CTeUF_Servico'
      Size = 10
    end
    object QrOpcoesCTePathLogoCTe: TWideStringField
      FieldName = 'PathLogoCTe'
      Size = 255
    end
    object QrOpcoesCTeCtaFretPrest: TIntegerField
      FieldName = 'CtaFretPrest'
    end
    object QrOpcoesCTeTxtFretPrest: TWideStringField
      FieldName = 'TxtFretPrest'
      Size = 100
    end
    object QrOpcoesCTeDupFretPrest: TWideStringField
      FieldName = 'DupFretPrest'
      Size = 3
    end
    object QrOpcoesCTeCTeSerNum: TWideStringField
      FieldName = 'CTeSerNum'
      Size = 255
    end
    object QrOpcoesCTeCTeSerVal: TDateField
      FieldName = 'CTeSerVal'
    end
    object QrOpcoesCTeCTeSerAvi: TSmallintField
      FieldName = 'CTeSerAvi'
    end
    object QrOpcoesCTeDirDACTes: TWideStringField
      FieldName = 'DirDACTes'
      Size = 255
    end
    object QrOpcoesCTeDirCTeProt: TWideStringField
      FieldName = 'DirCTeProt'
      Size = 255
    end
    object QrOpcoesCTeCTePreMailAut: TIntegerField
      FieldName = 'CTePreMailAut'
    end
    object QrOpcoesCTeCTePreMailCan: TIntegerField
      FieldName = 'CTePreMailCan'
    end
    object QrOpcoesCTeCTePreMailEveCCe: TIntegerField
      FieldName = 'CTePreMailEveCCe'
    end
    object QrOpcoesCTeCTeVerStaSer: TFloatField
      FieldName = 'CTeVerStaSer'
    end
    object QrOpcoesCTeCTeVerEnvLot: TFloatField
      FieldName = 'CTeVerEnvLot'
    end
    object QrOpcoesCTeCTeVerConLot: TFloatField
      FieldName = 'CTeVerConLot'
    end
    object QrOpcoesCTeCTeVerCanCTe: TFloatField
      FieldName = 'CTeVerCanCTe'
    end
    object QrOpcoesCTeCTeVerInuNum: TFloatField
      FieldName = 'CTeVerInuNum'
    end
    object QrOpcoesCTeCTeVerConCTe: TFloatField
      FieldName = 'CTeVerConCTe'
    end
    object QrOpcoesCTeCTeVerLotEve: TFloatField
      FieldName = 'CTeVerLotEve'
    end
    object QrOpcoesCTeCTeide_tpImp: TSmallintField
      FieldName = 'CTeide_tpImp'
    end
    object QrOpcoesCTeCTeide_tpAmb: TSmallintField
      FieldName = 'CTeide_tpAmb'
    end
    object QrOpcoesCTeCTeAppCode: TSmallintField
      FieldName = 'CTeAppCode'
    end
    object QrOpcoesCTeCTeAssDigMode: TSmallintField
      FieldName = 'CTeAssDigMode'
    end
    object QrOpcoesCTeCTeEntiTipCto: TIntegerField
      FieldName = 'CTeEntiTipCto'
    end
    object QrOpcoesCTeCTeEntiTipCt1: TIntegerField
      FieldName = 'CTeEntiTipCt1'
    end
    object QrOpcoesCTeMyEmailCTe: TWideStringField
      FieldName = 'MyEmailCTe'
      Size = 255
    end
    object QrOpcoesCTeDirCTeSchema: TWideStringField
      FieldName = 'DirCTeSchema'
      Size = 255
    end
    object QrOpcoesCTeDirCTeRWeb: TWideStringField
      FieldName = 'DirCTeRWeb'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEveEnvLot: TWideStringField
      FieldName = 'DirCTeEveEnvLot'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEveRetLot: TWideStringField
      FieldName = 'DirCTeEveRetLot'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEvePedCCe: TWideStringField
      FieldName = 'DirCTeEvePedCCe'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEveRetCCe: TWideStringField
      FieldName = 'DirCTeEveRetCCe'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEveProcCCe: TWideStringField
      FieldName = 'DirCTeEveProcCCe'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEvePedCan: TWideStringField
      FieldName = 'DirCTeEvePedCan'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEveRetCan: TWideStringField
      FieldName = 'DirCTeEveRetCan'
      Size = 255
    end
    object QrOpcoesCTeDirCTeRetCTeDes: TWideStringField
      FieldName = 'DirCTeRetCTeDes'
      Size = 255
    end
    object QrOpcoesCTeCTeCTeVerConDes: TFloatField
      FieldName = 'CTeCTeVerConDes'
    end
    object QrOpcoesCTeDirCTeEvePedMDe: TWideStringField
      FieldName = 'DirCTeEvePedMDe'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEveRetMDe: TWideStringField
      FieldName = 'DirCTeEveRetMDe'
      Size = 255
    end
    object QrOpcoesCTeDirDowCTeDes: TWideStringField
      FieldName = 'DirDowCTeDes'
      Size = 255
    end
    object QrOpcoesCTeDirDowCTeCTe: TWideStringField
      FieldName = 'DirDowCTeCTe'
      Size = 255
    end
    object QrOpcoesCTeCTeInfCpl: TIntegerField
      FieldName = 'CTeInfCpl'
    end
    object QrOpcoesCTeCTeVerConsCad: TFloatField
      FieldName = 'CTeVerConsCad'
    end
    object QrOpcoesCTeCTeVerDistDFeInt: TFloatField
      FieldName = 'CTeVerDistDFeInt'
    end
    object QrOpcoesCTeCTeVerDowCTe: TFloatField
      FieldName = 'CTeVerDowCTe'
    end
    object QrOpcoesCTeDirDowCTeCnf: TWideStringField
      FieldName = 'DirDowCTeCnf'
      Size = 255
    end
    object QrOpcoesCTeCTeItsLin: TSmallintField
      FieldName = 'CTeItsLin'
    end
    object QrOpcoesCTeCTeFTRazao: TSmallintField
      FieldName = 'CTeFTRazao'
    end
    object QrOpcoesCTeCTeFTEnder: TSmallintField
      FieldName = 'CTeFTEnder'
    end
    object QrOpcoesCTeCTeFTFones: TSmallintField
      FieldName = 'CTeFTFones'
    end
    object QrOpcoesCTeCTeMaiusc: TSmallintField
      FieldName = 'CTeMaiusc'
    end
    object QrOpcoesCTeCTeShowURL: TWideStringField
      FieldName = 'CTeShowURL'
      Size = 255
    end
    object QrOpcoesCTeCTetpEmis: TSmallintField
      FieldName = 'CTetpEmis'
    end
    object QrOpcoesCTeCTeSCAN_Ser: TIntegerField
      FieldName = 'CTeSCAN_Ser'
    end
    object QrOpcoesCTeCTeSCAN_nCT: TIntegerField
      FieldName = 'CTeSCAN_nCT'
    end
    object QrOpcoesCTeCTe_indFinalCpl: TSmallintField
      FieldName = 'CTe_indFinalCpl'
    end
  end
  object QrCTeEveRCCeIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT LEFT(its.ValorAlterado, 255) Valor255, its.*  '
      'FROM cteevercceits its '
      'WHERE its.FatID=:P0'
      'AND its.FatNum=:P1'
      'AND its.Empresa=:P2'
      'AND its.Controle=:P3'
      'AND its.Conta=:P4')
    Left = 376
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrCTeEveRCCeItsFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeEveRCCeItsFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeEveRCCeItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeEveRCCeItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeEveRCCeItsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCTeEveRCCeItsIDItem: TIntegerField
      FieldName = 'IDItem'
    end
    object QrCTeEveRCCeItsLayC_Grupo: TIntegerField
      FieldName = 'LayC_Grupo'
    end
    object QrCTeEveRCCeItsLayI_Versao: TFloatField
      FieldName = 'LayI_Versao'
    end
    object QrCTeEveRCCeItsLayI_Grupo: TIntegerField
      FieldName = 'LayI_Grupo'
    end
    object QrCTeEveRCCeItsLayI_Codigo: TIntegerField
      FieldName = 'LayI_Codigo'
    end
    object QrCTeEveRCCeItsGrupoAlterado: TWideStringField
      FieldName = 'GrupoAlterado'
    end
    object QrCTeEveRCCeItsCampoAlterado: TWideStringField
      FieldName = 'CampoAlterado'
    end
    object QrCTeEveRCCeItsValorAlterado: TWideMemoField
      FieldName = 'ValorAlterado'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCTeEveRCCeItsnroItemAlterado: TIntegerField
      FieldName = 'nroItemAlterado'
    end
    object QrCTeEveRCCeItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeEveRCCeItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeEveRCCeItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeEveRCCeItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeEveRCCeItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeEveRCCeItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeEveRCCeItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCTeEveRCCeItsValor255: TWideStringField
      FieldName = 'Valor255'
      Size = 255
    end
  end
  object DsCTeEveRCCeIts: TDataSource
    DataSet = QrCTeEveRCCeIts
    Left = 376
    Top = 64
  end
end
