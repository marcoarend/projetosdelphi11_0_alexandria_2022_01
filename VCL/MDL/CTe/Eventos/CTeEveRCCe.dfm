object FmCTeEveRCCe: TFmCTeEveRCCe
  Left = 339
  Top = 185
  Caption = 'CTe-EVENT-002 :: Carta de Corre'#231#227'o CT-e'
  ClientHeight = 554
  ClientWidth = 808
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 808
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 760
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 712
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 289
        Height = 32
        Caption = 'Carta de Corre'#231#227'o CT-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 289
        Height = 32
        Caption = 'Carta de Corre'#231#227'o CT-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 289
        Height = 32
        Caption = 'Carta de Corre'#231#227'o CT-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Painel1: TPanel
    Left = 0
    Top = 48
    Width = 808
    Height = 392
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 808
      Height = 392
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 808
        Height = 105
        Align = alTop
        TabOrder = 0
        object RGnCondUso: TdmkRadioGroup
          Left = 2
          Top = 15
          Width = 111
          Height = 88
          Align = alLeft
          Caption = ' Condi'#231#227'o de uso: '
          ItemIndex = 0
          Items.Strings = (
            'Sem condi'#231#227'o'
            'Modelo 1')
          TabOrder = 0
          OnClick = RGnCondUsoClick
          QryCampo = 'nCondUso'
          UpdCampo = 'nCondUso'
          UpdType = utYes
          OldValor = 0
        end
        object MeCondicao: TMemo
          Left = 113
          Top = 15
          Width = 693
          Height = 88
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 1
          OnChange = MeCondicaoChange
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 105
        Width = 808
        Height = 287
        Align = alClient
        Caption = ' Itens a serem corrigidos: '
        TabOrder = 1
        object Memo1: TMemo
          Left = 2
          Top = 225
          Width = 804
          Height = 60
          Align = alBottom
          ReadOnly = True
          TabOrder = 0
        end
        object DBGrid1: TDBGrid
          Left = 2
          Top = 15
          Width = 591
          Height = 179
          Align = alLeft
          DataSource = DsCTeEveRCCeIts
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'IDItem'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GrupoAlterado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CampoAlterado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor255'
              Title.Caption = 'Valor alterado (primeiros 255 caracteres)'
              Width = 236
              Visible = True
            end>
        end
        object Panel2: TPanel
          Left = 2
          Top = 194
          Width = 804
          Height = 31
          Align = alBottom
          TabOrder = 2
          object CkCondicao: TCheckBox
            Left = 8
            Top = 4
            Width = 413
            Height = 17
            Caption = 
              'Li as condi'#231#245'es de uso e os avisos e estou ciente do conte'#250'do e ' +
              'das implica'#231#245'es.'
            Color = clBtnFace
            ParentColor = False
            TabOrder = 0
            OnClick = CkCondicaoClick
          end
        end
        object DBMemo1: TDBMemo
          Left = 593
          Top = 15
          Width = 213
          Height = 179
          Align = alClient
          DataField = 'ValorAlterado'
          DataSource = DsCTeEveRCCeIts
          TabOrder = 3
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 440
    Width = 808
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 804
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 609
        Height = 32
        Caption = 
          'Descreva a corre'#231#227'o a ser considerada. Lembre-se que a corre'#231#227'o ' +
          'mais recente substituir'#225' as anteriores!'#13#10
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 609
        Height = 32
        Caption = 
          'Descreva a corre'#231#227'o a ser considerada. Lembre-se que a corre'#231#227'o ' +
          'mais recente substituir'#225' as anteriores!'#13#10
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 484
    Width = 808
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 662
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 660
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtEnvia: TBitBtn
        Left = 260
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Envia'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtEnviaClick
      end
      object BtItens: TBitBtn
        Left = 140
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Itens'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtItensClick
      end
      object BtCria: TBitBtn
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Cria'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtCriaClick
      end
    end
  end
  object PMItens: TPopupMenu
    OnPopup = PMItensPopup
    Left = 224
    Top = 88
    object IncluiItem1: TMenuItem
      Caption = '&Inclui item'
      OnClick = IncluiItem1Click
    end
    object AlteraItem1: TMenuItem
      Caption = '&Altera item'
      OnClick = AlteraItem1Click
    end
    object ExcluiItem1: TMenuItem
      Caption = '&Exclui Item'
      OnClick = ExcluiItem1Click
    end
  end
  object QrCTeEveRCCeIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCTeEveRCCeItsAfterOpen
    AfterClose = QrCTeEveRCCeItsAfterClose
    SQL.Strings = (
      'SELECT LEFT(its.ValorAlterado, 255) Valor255, its.*  '
      'FROM cteevercceits its '
      'WHERE its.FatID=:P0'
      'AND its.FatNum=:P1'
      'AND its.Empresa=:P2'
      'AND its.Controle=:P3'
      'AND its.Conta=:P4')
    Left = 376
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrCTeEveRCCeItsFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeEveRCCeItsFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeEveRCCeItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeEveRCCeItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeEveRCCeItsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCTeEveRCCeItsIDItem: TIntegerField
      FieldName = 'IDItem'
    end
    object QrCTeEveRCCeItsLayC_Grupo: TIntegerField
      FieldName = 'LayC_Grupo'
    end
    object QrCTeEveRCCeItsLayI_Versao: TFloatField
      FieldName = 'LayI_Versao'
    end
    object QrCTeEveRCCeItsLayI_Grupo: TIntegerField
      FieldName = 'LayI_Grupo'
    end
    object QrCTeEveRCCeItsLayI_Codigo: TIntegerField
      FieldName = 'LayI_Codigo'
    end
    object QrCTeEveRCCeItsGrupoAlterado: TWideStringField
      FieldName = 'GrupoAlterado'
    end
    object QrCTeEveRCCeItsCampoAlterado: TWideStringField
      FieldName = 'CampoAlterado'
    end
    object QrCTeEveRCCeItsValorAlterado: TWideMemoField
      FieldName = 'ValorAlterado'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCTeEveRCCeItsnroItemAlterado: TIntegerField
      FieldName = 'nroItemAlterado'
    end
    object QrCTeEveRCCeItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeEveRCCeItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeEveRCCeItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeEveRCCeItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeEveRCCeItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeEveRCCeItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeEveRCCeItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCTeEveRCCeItsValor255: TWideStringField
      FieldName = 'Valor255'
      Size = 255
    end
  end
  object DsCTeEveRCCeIts: TDataSource
    DataSet = QrCTeEveRCCeIts
    Left = 376
    Top = 64
  end
end
