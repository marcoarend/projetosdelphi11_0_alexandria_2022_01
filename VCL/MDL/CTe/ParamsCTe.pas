unit ParamsCTe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkValUsu, dmkRadioGroup, dmkImage, UnDmkEnums;

type
  TFmParamsCTe = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    QrPesqPrc: TmySQLQuery;
    QrPesqPrcPreco: TFloatField;
    Panel3: TPanel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdSequencial: TdmkEdit;
    EdSerieCTe: TdmkEdit;
    Label6: TLabel;
    EdControle: TdmkEdit;
    RGIncSeqAuto: TdmkRadioGroup;
    EdMaxSeqLib: TdmkEdit;
    Label7: TLabel;
    Panel4: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmParamsCTe: TFmParamsCTe;

implementation

uses UnMyObjects, Module, ParamsEmp, UMySQLModule;

{$R *.DFM}

procedure TFmParamsCTe.BtOKClick(Sender: TObject);
var
  Codigo, Controle, SerieCTe, Sequencial, MaxSeqLib, IncSeqAuto: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  SerieCTe       := EdSerieCTe.ValueVariant;
  Sequencial     := EdSequencial.ValueVariant;
  MaxSeqLib      := EdMaxSeqLib.ValueVariant;
  IncSeqAuto     := RGIncSeqAuto.ItemIndex;
  //
  if SerieCTe >= 890 then
  begin
    Geral.MB_Aviso('A serie deve ser inferior a 900!' + sLineBreak +
    'Para cadastrar a s�rie para notas em conting�ncia utilize o campo correto!');
    Exit;
  end;
  Controle := UMyMod.BPGS1I32('paramscte', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'paramscte', False, [
  'Codigo', 'SerieCTe', 'Sequencial',
  'MaxSeqLib', 'IncSeqAuto'], [
  'Controle'], [
  Codigo, SerieCTe, Sequencial,
  MaxSeqLib, IncSeqAuto], [
  Controle], True) then
  begin
    FmParamsEmp.ReopenParamsCTe(Controle);
    if CkContinuar.Checked then
    begin
      Geral.MB_Info(DmkEnums.NomeTipoSQL(ImgTipo.SQLType) +
        ' de S�rie de CT-e realizado com sucesso!');
      ImgTipo.SQLType             := stIns;
      EdSerieCTe.ValueVariant     := 0;
      EdSequencial.ValueVariant   := 0;
      EdControle.ValueVariant     := 0;
      EdSerieCTe.SetFocus;
    end else Close;
  end;
end;

procedure TFmParamsCTe.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmParamsCTe.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  DBEdCodigo.DataSource := FmParamsEmp.DsParamsEmp;
  DBEdit1.DataSource    := FmParamsEmp.DsParamsEmp;
  DBEdNome.DataSource   := FmParamsEmp.DsParamsEmp;
end;

procedure TFmParamsCTe.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmParamsCTe.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
