unit ModuleCTe_0000;

interface

uses
(*
  System.SysUtils,
  System.Classes,
  Data.DB, mySQLDbTables,
  UnInternalConsts, DmkGeral,
*)//uses
  Classes, mySQLDbTables, DB, SysUtils, UnInternalConsts, DmkGeral,
  Forms, Controls, Types,
  StdCtrls, Variants, Dialogs, ComCtrls, UrlMon, InvokeRegistry, dmkEdit,
  SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, JwaWinCrypt, WinInet, DateUtils,
  OleCtrls, SHDocVw, Grids, DBGrids, Graphics, DmkDAC_PF, AdvToolBar,
  UnDmkProcFunc, UnDmkEnums, UnMyObjects, UnCTeListas;

type
  TDmCTe_0000 = class(TDataModule)
    QrEmpresa: TmySQLQuery;
    QrEmpresaCodigo: TIntegerField;
    QrEmpresaTipo: TSmallintField;
    QrEmpresaCNPJ: TWideStringField;
    QrEmpresaCPF: TWideStringField;
    QrEmpresaIE: TWideStringField;
    QrEmpresaRG: TWideStringField;
    QrEmpresaCNAE: TWideStringField;
    QrEmpresaNO_ENT: TWideStringField;
    QrEmpresaFANTASIA: TWideStringField;
    QrEmpresaRUA: TWideStringField;
    QrEmpresaCOMPL: TWideStringField;
    QrEmpresaBAIRRO: TWideStringField;
    QrEmpresaTe1: TWideStringField;
    QrEmpresaNO_LOGRAD: TWideStringField;
    QrEmpresaNO_Munici: TWideStringField;
    QrEmpresaNO_UF: TWideStringField;
    QrEmpresaDTB_UF: TWideStringField;
    QrEmpresaNO_Pais: TWideStringField;
    QrEmpresaIEST: TWideStringField;
    QrEmpresaSUFRAMA: TWideStringField;
    QrEmpresaFilial: TIntegerField;
    QrEmpresaNIRE: TWideStringField;
    QrEmpresaNumero: TFloatField;
    QrEmpresaCEP: TFloatField;
    QrEmpresaCodiPais: TFloatField;
    QrEmpresaCodMunici: TFloatField;
    QrEmpresaUF: TFloatField;
    QrFilial: TmySQLQuery;
    QrFilialCerDigital: TWideMemoField;
    QrOpcoesCTe: TmySQLQuery;
    QrFrtFatCab: TmySQLQuery;
    QrFrtFatCabCodigo: TIntegerField;
    QrFrtFatCabFrtRegFisC: TIntegerField;
    QrFrtFatCabTomador: TIntegerField;
    QrFrtFatCabRemetente: TIntegerField;
    QrFrtFatCabExpedidor: TIntegerField;
    QrFrtFatCabRecebedor: TIntegerField;
    QrFrtFatCabDestinatario: TIntegerField;
    QrFrtFatCabToma4: TIntegerField;
    QrFrtFatCabTabelaPrc: TIntegerField;
    QrFrtFatCabCondicaoPg: TIntegerField;
    QrFrtFatCabCartEmiss: TIntegerField;
    QrFrtFatCabFrtRegFatC: TIntegerField;
    QrFrtFatCabForPag: TSmallintField;
    QrFrtFatCabModal: TSmallintField;
    QrFrtFatCabTpServ: TSmallintField;
    QrFrtFatCabCMunEnv: TIntegerField;
    QrFrtFatCabUFEnv: TWideStringField;
    QrFrtFatCabCMunIni: TIntegerField;
    QrFrtFatCabUFIni: TWideStringField;
    QrFrtFatCabCMunFim: TIntegerField;
    QrFrtFatCabUFFim: TWideStringField;
    QrFrtFatCabToma: TSmallintField;
    QrFrtFatCabCaracAd: TIntegerField;
    QrFrtFatCabCaracSer: TIntegerField;
    QrFrtFatCabxObs: TWideMemoField;
    QrFrtFatCabRetira: TSmallintField;
    QrFrtFatCabvCarga: TFloatField;
    QrFrtFatCabLk: TIntegerField;
    QrFrtFatCabDataCad: TDateField;
    QrFrtFatCabDataAlt: TDateField;
    QrFrtFatCabUserCad: TIntegerField;
    QrFrtFatCabUserAlt: TIntegerField;
    QrFrtFatCabAlterWeb: TSmallintField;
    QrFrtFatCabAtivo: TSmallintField;
    QrFrtFatCabEmpresa: TIntegerField;
    QrFrtFatCabAbertura: TDateTimeField;
    QrFrtFatCabEncerrou: TDateTimeField;
    QrFrtFatCabvTPrest: TFloatField;
    QrFrtFatCabvRec: TFloatField;
    QrFrtFatCabDataFat: TDateTimeField;
    QrFrtFatCabStatus: TSmallintField;
    QrFrtFatCabSerieDesfe: TIntegerField;
    QrFrtFatCabCTDesfeita: TIntegerField;
    _QrDest_: TmySQLQuery;
    QrCTeLayI: TmySQLQuery;
    QrCTeLayINO_Grupo: TWideStringField;
    QrCTeLayIVersao: TFloatField;
    QrCTeLayIGrupo: TIntegerField;
    QrCTeLayICodigo: TIntegerField;
    QrCTeLayICampo: TWideStringField;
    QrCTeLayINivel: TIntegerField;
    QrCTeLayIDescricao: TWideStringField;
    QrCTeLayIElemento: TWideStringField;
    QrCTeLayITipo: TWideStringField;
    QrCTeLayIOcorMin: TSmallintField;
    QrCTeLayIOcorMax: TIntegerField;
    QrCTeLayITamMin: TSmallintField;
    QrCTeLayITamMax: TIntegerField;
    QrCTeLayITamVar: TWideStringField;
    QrCTeLayIObservacao: TWideMemoField;
    QrCTeLayIInfoVazio: TSmallintField;
    QrCTeLayIDominio: TWideStringField;
    QrCTeLayIExpReg: TWideStringField;
    QrCTeLayIFormatStr: TWideStringField;
    QrCTeLayIDeciCasas: TSmallintField;
    QrCTeLayIAlterWeb: TSmallintField;
    QrCTeLayIAtivo: TSmallintField;
    _QrDest_Codigo: TIntegerField;
    _QrDest_Tipo: TSmallintField;
    _QrDest_CNPJ: TWideStringField;
    _QrDest_CPF: TWideStringField;
    _QrDest_EstrangDef: TSmallintField;
    _QrDest_EstrangTip: TSmallintField;
    _QrDest_EstrangNum: TWideStringField;
    _QrDest_indIEDest: TSmallintField;
    _QrDest_IE: TWideStringField;
    _QrDest_RG: TWideStringField;
    _QrDest_NIRE: TWideStringField;
    _QrDest_CNAE: TWideStringField;
    _QrDest_L_CNPJ: TWideStringField;
    _QrDest_L_Ativo: TSmallintField;
    _QrDest_LLograd: TSmallintField;
    _QrDest_LRua: TWideStringField;
    _QrDest_LCompl: TWideStringField;
    _QrDest_LNumero: TIntegerField;
    _QrDest_LBairro: TWideStringField;
    _QrDest_LCidade: TWideStringField;
    _QrDest_LUF: TSmallintField;
    _QrDest_NO_LLOGRAD: TWideStringField;
    _QrDest_LCodMunici: TIntegerField;
    _QrDest_NO_LMunici: TWideStringField;
    _QrDest_NO_LUF: TWideStringField;
    _QrDest_UF: TFloatField;
    _QrDest_NO_ENT: TWideStringField;
    _QrDest_FANTASIA: TWideStringField;
    _QrDest_RUA: TWideStringField;
    _QrDest_Numero: TFloatField;
    _QrDest_COMPL: TWideStringField;
    _QrDest_BAIRRO: TWideStringField;
    _QrDest_CEP: TFloatField;
    _QrDest_Te1: TWideStringField;
    _QrDest_CodiPais: TFloatField;
    _QrDest_CodMunici: TFloatField;
    _QrDest_NO_LOGRAD: TWideStringField;
    _QrDest_NO_Munici: TWideStringField;
    _QrDest_NO_UF: TWideStringField;
    _QrDest_NO_Pais: TWideStringField;
    _QrDest_SUFRAMA: TWideStringField;
    QrOpcoesCTeCTeversao: TFloatField;
    QrOpcoesCTeCTeide_mod: TSmallintField;
    _QrFrtRegFisC_: TmySQLQuery;
    _QrFrtRegFisC_Codigo: TIntegerField;
    _QrFrtRegFisC_Nome: TWideStringField;
    _QrFrtRegFisC_NatOp: TWideStringField;
    _QrFrtRegFisC_CFOP: TWideStringField;
    _QrFrtRegFisC_TpCTe: TSmallintField;
    _QrFrtRegFisC_MyCST: TSmallintField;
    QrOpcoesCTeSimplesFed: TSmallintField;
    QrOpcoesCTeDirCTeGer: TWideStringField;
    QrOpcoesCTeDirCTeAss: TWideStringField;
    QrOpcoesCTeDirCTeEnvLot: TWideStringField;
    QrOpcoesCTeDirCTeRec: TWideStringField;
    QrOpcoesCTeDirCTePedRec: TWideStringField;
    QrOpcoesCTeDirCTeProRec: TWideStringField;
    QrOpcoesCTeDirCTeDen: TWideStringField;
    QrOpcoesCTeDirCTePedCan: TWideStringField;
    QrOpcoesCTeDirCTeCan: TWideStringField;
    QrOpcoesCTeDirCTePedInu: TWideStringField;
    QrOpcoesCTeDirCTeInu: TWideStringField;
    QrOpcoesCTeDirCTePedSit: TWideStringField;
    QrOpcoesCTeDirCTeSit: TWideStringField;
    QrOpcoesCTeDirCTePedSta: TWideStringField;
    QrOpcoesCTeDirCTeSta: TWideStringField;
    QrOpcoesCTeCTeUF_WebServ: TWideStringField;
    QrOpcoesCTeCTeUF_Servico: TWideStringField;
    QrOpcoesCTePathLogoCTe: TWideStringField;
    QrOpcoesCTeCtaFretPrest: TIntegerField;
    QrOpcoesCTeTxtFretPrest: TWideStringField;
    QrOpcoesCTeDupFretPrest: TWideStringField;
    QrOpcoesCTeCTeSerNum: TWideStringField;
    QrOpcoesCTeCTeSerVal: TDateField;
    QrOpcoesCTeCTeSerAvi: TSmallintField;
    QrOpcoesCTeDirDACTes: TWideStringField;
    QrOpcoesCTeDirCTeProt: TWideStringField;
    QrOpcoesCTeCTePreMailAut: TIntegerField;
    QrOpcoesCTeCTePreMailCan: TIntegerField;
    QrOpcoesCTeCTePreMailEveCCe: TIntegerField;
    QrOpcoesCTeCTeVerStaSer: TFloatField;
    QrOpcoesCTeCTeVerEnvLot: TFloatField;
    QrOpcoesCTeCTeVerConLot: TFloatField;
    QrOpcoesCTeCTeVerCanCTe: TFloatField;
    QrOpcoesCTeCTeVerInuNum: TFloatField;
    QrOpcoesCTeCTeVerConCTe: TFloatField;
    QrOpcoesCTeCTeVerLotEve: TFloatField;
    QrOpcoesCTeCTeide_tpImp: TSmallintField;
    QrOpcoesCTeCTeide_tpAmb: TSmallintField;
    QrOpcoesCTeCTeAppCode: TSmallintField;
    QrOpcoesCTeCTeAssDigMode: TSmallintField;
    QrOpcoesCTeCTeEntiTipCto: TIntegerField;
    QrOpcoesCTeCTeEntiTipCt1: TIntegerField;
    QrOpcoesCTeMyEmailCTe: TWideStringField;
    QrOpcoesCTeDirCTeSchema: TWideStringField;
    QrOpcoesCTeDirCTeRWeb: TWideStringField;
    QrOpcoesCTeDirCTeEveEnvLot: TWideStringField;
    QrOpcoesCTeDirCTeEveRetLot: TWideStringField;
    QrOpcoesCTeDirCTeEvePedCCe: TWideStringField;
    QrOpcoesCTeDirCTeEveRetCCe: TWideStringField;
    QrOpcoesCTeDirCTeEveProcCCe: TWideStringField;
    QrOpcoesCTeDirCTeEvePedCan: TWideStringField;
    QrOpcoesCTeDirCTeEveRetCan: TWideStringField;
    QrOpcoesCTeDirCTeRetCTeDes: TWideStringField;
    QrOpcoesCTeCTeCTeVerConDes: TFloatField;
    QrOpcoesCTeDirCTeEvePedMDe: TWideStringField;
    QrOpcoesCTeDirCTeEveRetMDe: TWideStringField;
    QrOpcoesCTeDirDowCTeDes: TWideStringField;
    QrOpcoesCTeDirDowCTeCTe: TWideStringField;
    QrOpcoesCTeCTeInfCpl: TIntegerField;
    QrOpcoesCTeCTeVerConsCad: TFloatField;
    QrOpcoesCTeCTeVerDistDFeInt: TFloatField;
    QrOpcoesCTeCTeVerDowCTe: TFloatField;
    QrOpcoesCTeDirDowCTeCnf: TWideStringField;
    QrOpcoesCTeCTeItsLin: TSmallintField;
    QrOpcoesCTeCTeFTRazao: TSmallintField;
    QrOpcoesCTeCTeFTEnder: TSmallintField;
    QrOpcoesCTeCTeFTFones: TSmallintField;
    QrOpcoesCTeCTeMaiusc: TSmallintField;
    QrOpcoesCTeCTeShowURL: TWideStringField;
    QrOpcoesCTeCTetpEmis: TSmallintField;
    QrOpcoesCTeCTeSCAN_Ser: TIntegerField;
    QrOpcoesCTeCTeSCAN_nCT: TIntegerField;
    QrOpcoesCTeCTe_indFinalCpl: TSmallintField;
    QrFrtFatCabrefCTE: TWideStringField;
    QrFrtFatCabXDetRetira: TWideStringField;
    QrEntiCTe: TmySQLQuery;
    QrEntiCTeCNPJ: TWideStringField;
    QrEntiCTeCPF: TWideStringField;
    QrEntiCTeIE: TWideStringField;
    QrEntiCTexNome: TWideStringField;
    QrEntiCTexFant: TWideStringField;
    QrEntiCTefone: TWideStringField;
    QrEntiCTeTipo: TSmallintField;
    QrEntiCTexLgr: TWideStringField;
    QrEntiCTexCpl: TWideStringField;
    QrEntiCTexBairro: TWideStringField;
    QrEntiCTeCMun: TFloatField;
    QrEntiCTexMun: TWideStringField;
    QrEntiCTeCEP: TFloatField;
    QrEntiCTeUF: TWideStringField;
    QrEntiCTeCPais: TFloatField;
    QrEntiCTexPais: TWideStringField;
    QrEntiCTeEmail: TWideStringField;
    QrEntiCTeNro: TWideStringField;
    QrFrtFatCabEntiEmi: TIntegerField;
    _QrFrtRegFisC_ICMS_pICMS: TFloatField;
    _QrFrtRegFisC_ICMS_pRedBC: TFloatField;
    _QrFrtRegFisC_ICMS_pICMSSTRet: TFloatField;
    _QrFrtRegFisC_ICMS_pRedBCOutraUF: TFloatField;
    _QrFrtRegFisC_ICMS_pICMSOutraUF: TFloatField;
    _QrFrtRegFisC_iTotTrib_Fonte: TIntegerField;
    _QrFrtRegFisC_iTotTrib_Tabela: TIntegerField;
    _QrFrtRegFisC_iTotTrib_Ex: TIntegerField;
    _QrFrtRegFisC_iTotTrib_Codigo: TLargeintField;
    QrIBPTax: TmySQLQuery;
    QrIBPTaxCodigo: TLargeintField;
    QrIBPTaxEx: TIntegerField;
    QrIBPTaxTabela: TIntegerField;
    QrIBPTaxNome: TWideStringField;
    QrIBPTaxDescricao: TWideMemoField;
    QrIBPTaxAliqNac: TFloatField;
    QrIBPTaxAliqImp: TFloatField;
    QrIBPTaxVersao: TWideStringField;
    QrIBPTaxLk: TIntegerField;
    QrIBPTaxDataCad: TDateField;
    QrIBPTaxDataAlt: TDateField;
    QrIBPTaxUserCad: TIntegerField;
    QrIBPTaxUserAlt: TIntegerField;
    QrIBPTaxAlterWeb: TSmallintField;
    QrIBPTaxAtivo: TSmallintField;
    QrFrtFatCabFrtProPred: TIntegerField;
    QrFrtFatCabxOutCat: TWideStringField;
    QrFrtFatCabvOrig: TFloatField;
    QrFrtFatCabvDesc: TFloatField;
    QrFrtFatCabvLiq: TFloatField;
    QrCTeCabA: TmySQLQuery;
    QrEntiCTeSUFRAMA: TWideStringField;
    QrFrtFatInfQ: TmySQLQuery;
    QrFrtFatInfQCodigo: TIntegerField;
    QrFrtFatInfQControle: TIntegerField;
    QrFrtFatInfQCUnid: TSmallintField;
    QrFrtFatInfQMyTpMed: TIntegerField;
    QrFrtFatInfQQCarga: TFloatField;
    QrFrtFatInfQLk: TIntegerField;
    QrFrtFatInfQDataCad: TDateField;
    QrFrtFatInfQDataAlt: TDateField;
    QrFrtFatInfQUserCad: TIntegerField;
    QrFrtFatInfQUserAlt: TIntegerField;
    QrFrtFatInfQAlterWeb: TSmallintField;
    QrFrtFatInfQAtivo: TSmallintField;
    QrFrtFatInfQNO_CUnid: TWideStringField;
    QrFrtFatInfQNO_MyTpMed: TWideStringField;
    QrFrtFatInfDoc: TmySQLQuery;
    QrFrtFatInfDocCodigo: TIntegerField;
    QrFrtFatInfDocControle: TIntegerField;
    QrFrtFatInfDocMyTpDocInf: TSmallintField;
    QrFrtFatInfDocInfNFe_chave: TWideStringField;
    QrFrtFatInfDocInfNFe_PIN: TIntegerField;
    QrFrtFatInfDocInfNFe_dPrev: TDateField;
    QrFrtFatInfDocInfNF_nRoma: TWideStringField;
    QrFrtFatInfDocInfNF_nPed: TWideStringField;
    QrFrtFatInfDocInfNF_mod: TSmallintField;
    QrFrtFatInfDocInfNF_serie: TWideStringField;
    QrFrtFatInfDocInfNF_nDoc: TWideStringField;
    QrFrtFatInfDocInfNF_dEmi: TDateField;
    QrFrtFatInfDocInfNF_vBC: TFloatField;
    QrFrtFatInfDocInfNF_vICMS: TFloatField;
    QrFrtFatInfDocInfNF_vBCST: TFloatField;
    QrFrtFatInfDocInfNF_vST: TFloatField;
    QrFrtFatInfDocInfNF_vProd: TFloatField;
    QrFrtFatInfDocInfNF_vNF: TFloatField;
    QrFrtFatInfDocInfNF_nCFOP: TIntegerField;
    QrFrtFatInfDocInfNF_nPeso: TFloatField;
    QrFrtFatInfDocInfNF_PIN: TIntegerField;
    QrFrtFatInfDocInfNF_dPrev: TDateField;
    QrFrtFatInfDocInfOutros_tpDoc: TSmallintField;
    QrFrtFatInfDocInfOutros_descOutros: TWideStringField;
    QrFrtFatInfDocInfOutros_nDoc: TWideStringField;
    QrFrtFatInfDocInfOutros_dEmi: TDateField;
    QrFrtFatInfDocInfOutros_vDocFisc: TFloatField;
    QrFrtFatInfDocInfOutros_dPrev: TDateField;
    QrFrtFatInfDocLk: TIntegerField;
    QrFrtFatInfDocDataCad: TDateField;
    QrFrtFatInfDocDataAlt: TDateField;
    QrFrtFatInfDocUserCad: TIntegerField;
    QrFrtFatInfDocUserAlt: TIntegerField;
    QrFrtFatInfDocAlterWeb: TSmallintField;
    QrFrtFatInfDocAtivo: TSmallintField;
    QrFrtFOCAtrDef: TmySQLQuery;
    QrFrtFOCAtrDefID_Item: TIntegerField;
    QrFrtFOCAtrDefID_Sorc: TIntegerField;
    QrFrtFOCAtrDefAtrCad: TIntegerField;
    QrFrtFOCAtrDefATRITS: TFloatField;
    QrFrtFOCAtrDefCU_CAD: TIntegerField;
    QrFrtFOCAtrDefCU_ITS: TLargeintField;
    QrFrtFOCAtrDefAtrTxt: TWideStringField;
    QrFrtFOCAtrDefNO_CAD: TWideStringField;
    QrFrtFOCAtrDefNO_ITS: TWideStringField;
    QrFrtFOCAtrDefAtrTyp: TSmallintField;
    QrFrtFatPeri: TmySQLQuery;
    QrFrtFatPeriCodigo: TIntegerField;
    QrFrtFatPeriControle: TIntegerField;
    QrFrtFatPeriProdutCad: TIntegerField;
    QrFrtFatPeriqTotProd: TWideStringField;
    QrFrtFatPeriqVolTipo: TWideStringField;
    QrFrtFatPeriNome: TWideStringField;
    QrFrtFatPerinONU: TWideStringField;
    QrFrtFatPeriXNomeAE: TWideStringField;
    QrFrtFatPeriPERI_grEmb: TWideStringField;
    QrFrtFatPeriPontoFulgor: TWideStringField;
    QrFrtFatPeriXClaRisco: TWideStringField;
    QrFrtFatDup: TmySQLQuery;
    QrFrtFatDupDuplicata: TWideStringField;
    QrFrtFatDupVencimento: TDateField;
    QrFrtFatDupCredito: TFloatField;
    QrFrtFatInfCTeSub: TmySQLQuery;
    QrFrtFatInfCTeSubCodigo: TIntegerField;
    QrFrtFatInfCTeSubChCTe: TWideStringField;
    QrFrtFatInfCTeSubContribICMS: TSmallintField;
    QrFrtFatInfCTeSubTipoDoc: TSmallintField;
    QrFrtFatInfCTeSubrefNFe: TWideStringField;
    QrFrtFatInfCTeSubCNPJ: TWideStringField;
    QrFrtFatInfCTeSubCPF: TWideStringField;
    QrFrtFatInfCTeSubmod_: TWideStringField;
    QrFrtFatInfCTeSubserie: TIntegerField;
    QrFrtFatInfCTeSubsubserie: TIntegerField;
    QrFrtFatInfCTeSubnro: TIntegerField;
    QrFrtFatInfCTeSubvalor: TFloatField;
    QrFrtFatInfCTeSubdEmi: TDateField;
    QrFrtFatInfCTeSubrefCTe: TWideStringField;
    QrFrtFatInfCTeSubrefCTeAnu: TWideStringField;
    QrFrtFatInfCTeSubLk: TIntegerField;
    QrFrtFatInfCTeSubDataCad: TDateField;
    QrFrtFatInfCTeSubDataAlt: TDateField;
    QrFrtFatInfCTeSubUserCad: TIntegerField;
    QrFrtFatInfCTeSubUserAlt: TIntegerField;
    QrFrtFatInfCTeSubAlterWeb: TSmallintField;
    QrFrtFatInfCTeSubAtivo: TSmallintField;
    QrFrtFatInfCTeAnu: TmySQLQuery;
    QrFrtFatInfCTeAnuCodigo: TIntegerField;
    QrFrtFatInfCTeAnuChCTe: TWideStringField;
    QrFrtFatInfCTeAnudEmi: TDateField;
    QrFrtFatInfCTeComp: TmySQLQuery;
    QrFrtFatInfCTeCompCodigo: TIntegerField;
    QrFrtFatInfCTeCompChave: TWideStringField;
    QrFrtFatAutXml: TmySQLQuery;
    QrFrtFatAutXmlCodigo: TIntegerField;
    QrFrtFatAutXmlControle: TIntegerField;
    QrFrtFatAutXmlAddForma: TSmallintField;
    QrFrtFatAutXmlAddIDCad: TIntegerField;
    QrFrtFatAutXmlTipo: TSmallintField;
    QrFrtFatAutXmlCNPJ: TWideStringField;
    QrFrtFatAutXmlCPF: TWideStringField;
    QrFrtFatAutXmlLk: TIntegerField;
    QrFrtFatAutXmlDataCad: TDateField;
    QrFrtFatAutXmlDataAlt: TDateField;
    QrFrtFatAutXmlUserCad: TIntegerField;
    QrFrtFatAutXmlUserAlt: TIntegerField;
    QrFrtFatAutXmlAlterWeb: TSmallintField;
    QrFrtFatAutXmlAtivo: TSmallintField;
    QrTrnsCad: TmySQLQuery;
    QrTrnsCadCodigo: TIntegerField;
    QrTrnsCadRNTRC: TWideStringField;
    QrTrnsCadLk: TIntegerField;
    QrTrnsCadDataCad: TDateField;
    QrTrnsCadDataAlt: TDateField;
    QrTrnsCadUserCad: TIntegerField;
    QrTrnsCadUserAlt: TIntegerField;
    QrTrnsCadAlterWeb: TSmallintField;
    QrTrnsCadAtivo: TSmallintField;
    QrFrtFatCabLocColeta: TIntegerField;
    QrFrtFatCabLocEnt: TIntegerField;
    QrFrtFatCabParamsCTe: TIntegerField;
    QrFrtFatCabDataPrev: TDateTimeField;
    QrFrtFatCabLota: TSmallintField;
    QrFrtFatCabCIOT: TLargeintField;
    QrCTeCabAFatID: TIntegerField;
    QrCTeCabAFatNum: TIntegerField;
    QrCTeCabAEmpresa: TIntegerField;
    QrCTeCabAIDCtrl: TIntegerField;
    QrCTeCabALoteEnv: TIntegerField;
    QrCTeCabAversao: TFloatField;
    QrCTeCabAId: TWideStringField;
    QrCTeCabAide_cUF: TSmallintField;
    QrCTeCabAide_cCT: TIntegerField;
    QrCTeCabAide_CFOP: TIntegerField;
    QrCTeCabAide_natOp: TWideStringField;
    QrCTeCabAide_forPag: TSmallintField;
    QrCTeCabAide_mod: TSmallintField;
    QrCTeCabAide_serie: TIntegerField;
    QrCTeCabAide_nCT: TIntegerField;
    QrCTeCabAide_dEmi: TDateField;
    QrCTeCabAide_hEmi: TTimeField;
    QrCTeCabAide_dhEmiTZD: TFloatField;
    QrCTeCabAide_tpImp: TSmallintField;
    QrCTeCabAide_tpEmis: TSmallintField;
    QrCTeCabAide_cDV: TSmallintField;
    QrCTeCabAide_tpAmb: TSmallintField;
    QrCTeCabAide_tpCTe: TSmallintField;
    QrCTeCabAide_procEmi: TSmallintField;
    QrCTeCabAide_verProc: TWideStringField;
    QrCTeCabAide_refCTE: TWideStringField;
    QrCTeCabAide_cMunEnv: TIntegerField;
    QrCTeCabAide_xMunEnv: TWideStringField;
    QrCTeCabAide_UFEnv: TWideStringField;
    QrCTeCabAide_Modal: TSmallintField;
    QrCTeCabAide_tpServ: TSmallintField;
    QrCTeCabAide_cMunIni: TIntegerField;
    QrCTeCabAide_xMunIni: TWideStringField;
    QrCTeCabAide_UFIni: TWideStringField;
    QrCTeCabAide_cMunFim: TIntegerField;
    QrCTeCabAide_xMunFim: TWideStringField;
    QrCTeCabAide_UFFim: TWideStringField;
    QrCTeCabAide_Retira: TSmallintField;
    QrCTeCabAide_xDetRetira: TWideStringField;
    QrCTeCabAtoma99_toma: TSmallintField;
    QrCTeCabAtoma04_toma: TSmallintField;
    QrCTeCabAtoma04_CNPJ: TWideStringField;
    QrCTeCabAtoma04_CPF: TWideStringField;
    QrCTeCabAtoma04_IE: TWideStringField;
    QrCTeCabAtoma04_xNome: TWideStringField;
    QrCTeCabAtoma04_xFant: TWideStringField;
    QrCTeCabAtoma04_fone: TWideStringField;
    QrCTeCabAide_dhCont: TDateTimeField;
    QrCTeCabAide_dhContTZD: TFloatField;
    QrCTeCabAide_xJust: TWideStringField;
    QrCTeCabAcompl_xCaracAd: TWideStringField;
    QrCTeCabAcompl_xCaracSer: TWideStringField;
    QrCTeCabAcompl_xEmi: TWideStringField;
    QrCTeCabAentrega_tpPer: TSmallintField;
    QrCTeCabAentrega_dProg: TDateField;
    QrCTeCabAentrega_dIni: TDateField;
    QrCTeCabAentrega_dFim: TDateField;
    QrCTeCabAentrega_tpHor: TSmallintField;
    QrCTeCabAentrega_hProg: TTimeField;
    QrCTeCabAentrega_hIni: TDateField;
    QrCTeCabAentrega_hFim: TDateField;
    QrCTeCabAide_origCalc: TWideStringField;
    QrCTeCabAide_destCalc: TWideStringField;
    QrCTeCabAide_xObs: TWideMemoField;
    QrCTeCabAemit_CNPJ: TWideStringField;
    QrCTeCabAemit_IE: TWideStringField;
    QrCTeCabAemit_xNome: TWideStringField;
    QrCTeCabAemit_xFant: TWideStringField;
    QrCTeCabAemit_xLgr: TWideStringField;
    QrCTeCabAemit_nro: TWideStringField;
    QrCTeCabAemit_xCpl: TWideStringField;
    QrCTeCabAemit_xBairro: TWideStringField;
    QrCTeCabAemit_cMun: TIntegerField;
    QrCTeCabAemit_xMun: TWideStringField;
    QrCTeCabAemit_CEP: TIntegerField;
    QrCTeCabAemit_UF: TWideStringField;
    QrCTeCabAemit_fone: TWideStringField;
    QrCTeCabArem_CNPJ: TWideStringField;
    QrCTeCabArem_CPF: TWideStringField;
    QrCTeCabArem_IE: TWideStringField;
    QrCTeCabArem_xNome: TWideStringField;
    QrCTeCabArem_xFant: TWideStringField;
    QrCTeCabArem_fone: TWideStringField;
    QrCTeCabArem_xLgr: TWideStringField;
    QrCTeCabArem_nro: TWideStringField;
    QrCTeCabArem_xCpl: TWideStringField;
    QrCTeCabArem_xBairro: TWideStringField;
    QrCTeCabArem_cMun: TIntegerField;
    QrCTeCabArem_xMun: TWideStringField;
    QrCTeCabArem_CEP: TIntegerField;
    QrCTeCabArem_UF: TWideStringField;
    QrCTeCabArem_cPais: TIntegerField;
    QrCTeCabArem_xPais: TWideStringField;
    QrCTeCabArem_email: TWideStringField;
    QrCTeCabAdest_CNPJ: TWideStringField;
    QrCTeCabAdest_CPF: TWideStringField;
    QrCTeCabAdest_IE: TWideStringField;
    QrCTeCabAdest_xNome: TWideStringField;
    QrCTeCabAdest_fone: TWideStringField;
    QrCTeCabAdest_ISUF: TWideStringField;
    QrCTeCabAdest_xLgr: TWideStringField;
    QrCTeCabAdest_nro: TWideStringField;
    QrCTeCabAdest_xCpl: TWideStringField;
    QrCTeCabAdest_xBairro: TWideStringField;
    QrCTeCabAdest_cMun: TIntegerField;
    QrCTeCabAdest_xMun: TWideStringField;
    QrCTeCabAdest_CEP: TIntegerField;
    QrCTeCabAdest_UF: TWideStringField;
    QrCTeCabAdest_cPais: TIntegerField;
    QrCTeCabAdest_xPais: TWideStringField;
    QrCTeCabAdest_email: TWideStringField;
    QrCTeCabAvPrest_vTPrest: TFloatField;
    QrCTeCabAvPrest_vRec: TFloatField;
    QrCTeCabAICMS_CST: TSmallintField;
    QrCTeCabAICMS_vBC: TFloatField;
    QrCTeCabAICMS_pICMS: TFloatField;
    QrCTeCabAICMS_vICMS: TFloatField;
    QrCTeCabAICMS_pRedBC: TFloatField;
    QrCTeCabAICMS_vBCSTRet: TFloatField;
    QrCTeCabAICMS_vICMSSTRet: TFloatField;
    QrCTeCabAICMS_pICMSSTRet: TFloatField;
    QrCTeCabAICMS_vCred: TFloatField;
    QrCTeCabAICMS_pRedBCOutraUF: TFloatField;
    QrCTeCabAICMS_vBCOutraUF: TFloatField;
    QrCTeCabAICMS_pICMSOutraUF: TFloatField;
    QrCTeCabAICMS_vICMSOutraUF: TFloatField;
    QrCTeCabAICMSSN_indSN: TSmallintField;
    QrCTeCabAimp_vTotTrib: TFloatField;
    QrCTeCabAimp_infAdFisco: TWideMemoField;
    QrCTeCabAinfCarga_vCarga: TFloatField;
    QrCTeCabAinfCarga_xOutCat: TWideStringField;
    QrCTeCabAcobr_Fat_nFat: TWideStringField;
    QrCTeCabAcobr_Fat_vOrig: TFloatField;
    QrCTeCabAcobr_Fat_vDesc: TFloatField;
    QrCTeCabAcobr_Fat_vLiq: TFloatField;
    QrCTeCabAinfCTeComp_chave: TWideStringField;
    QrCTeCabAinfCTeAnu_chCte: TWideStringField;
    QrCTeCabAinfCTeAnu_dEmi: TDateField;
    QrCTeCabAprotCTe_versao: TFloatField;
    QrCTeCabAinfProt_Id: TWideStringField;
    QrCTeCabAinfProt_tpAmb: TSmallintField;
    QrCTeCabAinfProt_verAplic: TWideStringField;
    QrCTeCabAinfProt_dhRecbto: TDateTimeField;
    QrCTeCabAinfProt_dhRecbtoTZD: TFloatField;
    QrCTeCabAinfProt_nProt: TWideStringField;
    QrCTeCabAinfProt_digVal: TWideStringField;
    QrCTeCabAinfProt_cStat: TIntegerField;
    QrCTeCabAinfProt_xMotivo: TWideStringField;
    QrCTeCabAretCancCTe_versao: TFloatField;
    QrCTeCabAinfCanc_Id: TWideStringField;
    QrCTeCabAinfCanc_tpAmb: TSmallintField;
    QrCTeCabAinfCanc_verAplic: TWideStringField;
    QrCTeCabAinfCanc_dhRecbto: TDateTimeField;
    QrCTeCabAinfCanc_dhRecbtoTZD: TFloatField;
    QrCTeCabAinfCanc_nProt: TWideStringField;
    QrCTeCabAinfCanc_digVal: TWideStringField;
    QrCTeCabAinfCanc_cStat: TIntegerField;
    QrCTeCabAinfCanc_xMotivo: TWideStringField;
    QrCTeCabAinfCanc_cJust: TIntegerField;
    QrCTeCabAinfCanc_xJust: TWideStringField;
    QrCTeCabAFisRegCad: TIntegerField;
    QrCTeCabACartEmiss: TIntegerField;
    QrCTeCabATabelaPrc: TIntegerField;
    QrCTeCabACondicaoPg: TIntegerField;
    QrCTeCabACodInfoRemet: TIntegerField;
    QrCTeCabACodInfoExped: TIntegerField;
    QrCTeCabACodInfoReceb: TIntegerField;
    QrCTeCabACodInfoDesti: TIntegerField;
    QrCTeCabACodInfoToma4: TIntegerField;
    QrCTeCabACriAForca: TSmallintField;
    QrCTeCabAcSitCTe: TSmallintField;
    QrCTeCabAinfCCe_verAplic: TWideStringField;
    QrCTeCabAinfCCe_cOrgao: TSmallintField;
    QrCTeCabAinfCCe_tpAmb: TSmallintField;
    QrCTeCabAinfCCe_CNPJ: TWideStringField;
    QrCTeCabAinfCCe_CPF: TWideStringField;
    QrCTeCabAinfCCe_chCTe: TWideStringField;
    QrCTeCabAinfCCe_dhEvento: TDateTimeField;
    QrCTeCabAinfCCe_dhEventoTZD: TFloatField;
    QrCTeCabAinfCCe_tpEvento: TIntegerField;
    QrCTeCabAinfCCe_nSeqEvento: TIntegerField;
    QrCTeCabAinfCCe_verEvento: TFloatField;
    QrCTeCabAinfCCe_cStat: TIntegerField;
    QrCTeCabAinfCCe_dhRegEvento: TDateTimeField;
    QrCTeCabAinfCCe_dhRegEventoTZD: TFloatField;
    QrCTeCabAinfCCe_nProt: TWideStringField;
    QrCTeCabAinfCCe_nCondUso: TIntegerField;
    QrCTeCabAiTotTrib_Fonte: TIntegerField;
    QrCTeCabAiTotTrib_Tabela: TIntegerField;
    QrCTeCabAiTotTrib_Ex: TIntegerField;
    QrCTeCabAiTotTrib_Codigo: TLargeintField;
    QrCTeCabAvBasTrib: TFloatField;
    QrCTeCabApTotTrib: TFloatField;
    QrCTeCabAverTotTrib: TWideStringField;
    QrCTeCabAtpAliqTotTrib: TSmallintField;
    QrCTeCabACTeMyCST: TSmallintField;
    QrCTeCabAinfCarga_proPred: TWideStringField;
    QrCTeCabAinfCteSub_chCte: TWideStringField;
    QrCTeCabAinfCteSub_refNFe: TWideStringField;
    QrCTeCabAinfCteSub_CNPJ: TWideStringField;
    QrCTeCabAinfCteSub_CPF: TWideStringField;
    QrCTeCabAinfCteSub_mod: TWideStringField;
    QrCTeCabAinfCteSub_serie: TIntegerField;
    QrCTeCabAinfCteSub_subserie: TIntegerField;
    QrCTeCabAinfCteSub_nro: TIntegerField;
    QrCTeCabAinfCteSub_valor: TFloatField;
    QrCTeCabAinfCteSub_dEmi: TDateField;
    QrCTeCabAinfCteSub_refCTe: TWideStringField;
    QrCTeCabAinfCteSub_refCTeAnu: TWideStringField;
    QrCTeCabAStatus: TIntegerField;
    QrFilialDirCTeGer: TWideStringField;
    QrFilialDirCTeAss: TWideStringField;
    QrFilialDirCTeEnvLot: TWideStringField;
    QrFilialDirCTeRec: TWideStringField;
    QrFilialDirCTePedRec: TWideStringField;
    QrFilialDirCTeProRec: TWideStringField;
    QrFilialDirCTeDen: TWideStringField;
    QrFilialDirCTePedCan: TWideStringField;
    QrFilialDirCTeCan: TWideStringField;
    QrFilialDirCTePedInu: TWideStringField;
    QrFilialDirCTeInu: TWideStringField;
    QrFilialDirCTePedSit: TWideStringField;
    QrFilialDirCTeSit: TWideStringField;
    QrFilialDirCTePedSta: TWideStringField;
    QrFilialDirCTeSta: TWideStringField;
    QrFilialCTeUF_WebServ: TWideStringField;
    QrFilialCTeUF_Servico: TWideStringField;
    QrFilialPathLogoCTe: TWideStringField;
    QrFilialCtaFretPrest: TIntegerField;
    QrFilialTxtFretPrest: TWideStringField;
    QrFilialDupFretPrest: TWideStringField;
    QrFilialCTeSerNum: TWideStringField;
    QrFilialCTeSerVal: TDateField;
    QrFilialCTeSerAvi: TSmallintField;
    QrFilialDirDACTes: TWideStringField;
    QrFilialDirCTeProt: TWideStringField;
    QrFilialCTePreMailAut: TIntegerField;
    QrFilialCTePreMailCan: TIntegerField;
    QrFilialCTePreMailEveCCe: TIntegerField;
    QrFilialCTeversao: TFloatField;
    QrFilialCTeVerStaSer: TFloatField;
    QrFilialCTeVerEnvLot: TFloatField;
    QrFilialCTeVerConLot: TFloatField;
    QrFilialCTeVerCanCTe: TFloatField;
    QrFilialCTeVerInuNum: TFloatField;
    QrFilialCTeVerConCTe: TFloatField;
    QrFilialCTeVerLotEve: TFloatField;
    QrFilialCTeide_mod: TSmallintField;
    QrFilialCTeide_tpImp: TSmallintField;
    QrFilialCTeide_tpAmb: TSmallintField;
    QrFilialCTeAppCode: TSmallintField;
    QrFilialCTeAssDigMode: TSmallintField;
    QrFilialCTeEntiTipCto: TIntegerField;
    QrFilialCTeEntiTipCt1: TIntegerField;
    QrFilialMyEmailCTe: TWideStringField;
    QrFilialDirCTeSchema: TWideStringField;
    QrFilialDirCTeRWeb: TWideStringField;
    QrFilialDirCTeEveEnvLot: TWideStringField;
    QrFilialDirCTeEveRetLot: TWideStringField;
    QrFilialDirCTeEvePedCCe: TWideStringField;
    QrFilialDirCTeEveRetCCe: TWideStringField;
    QrFilialDirCTeEveProcCCe: TWideStringField;
    QrFilialDirCTeEvePedCan: TWideStringField;
    QrFilialDirCTeEveRetCan: TWideStringField;
    QrFilialDirCTeRetCTeDes: TWideStringField;
    QrFilialCTeCTeVerConDes: TFloatField;
    QrFilialDirCTeEvePedMDe: TWideStringField;
    QrFilialDirCTeEveRetMDe: TWideStringField;
    QrFilialDirDowCTeDes: TWideStringField;
    QrFilialDirDowCTeCTe: TWideStringField;
    QrFilialCTeInfCpl: TIntegerField;
    QrFilialCTeVerConsCad: TFloatField;
    QrFilialCTeVerDistDFeInt: TFloatField;
    QrFilialCTeVerDowCTe: TFloatField;
    QrFilialDirDowCTeCnf: TWideStringField;
    QrFilialCTeItsLin: TSmallintField;
    QrFilialCTeFTRazao: TSmallintField;
    QrFilialCTeFTEnder: TSmallintField;
    QrFilialCTeFTFones: TSmallintField;
    QrFilialCTeMaiusc: TSmallintField;
    QrFilialCTeShowURL: TWideStringField;
    QrFilialCTetpEmis: TSmallintField;
    QrFilialCTeSCAN_Ser: TIntegerField;
    QrFilialCTeSCAN_nCT: TIntegerField;
    QrFilialCTe_indFinalCpl: TSmallintField;
    QrFilialParamsCTe: TIntegerField;
    QrFilialSimplesFed: TSmallintField;
    QrCTeArq: TmySQLQuery;
    QrCTeArqIDCtrl_CabA: TIntegerField;
    QrCTeArqIDCtrl_Arq: TIntegerField;
    QrCTeArqId: TWideStringField;
    QrCTeArqFatID: TIntegerField;
    QrCTeArqFatNum: TIntegerField;
    QrCTeArqEmpresa: TIntegerField;
    QrOpcoesCTeTZD_UTC_Auto_TermoAceite: TIntegerField;
    QrOpcoesCTeTZD_UTC_Auto_DataHoraAceite: TDateTimeField;
    QrOpcoesCTeTZD_UTC_Auto_UsuarioAceite: TIntegerField;
    QrOpcoesCTeTZD_UTC_Auto: TSmallintField;
    QrOpcoesCTeTZD_UTC: TFloatField;
    QrOpcoesCTehVeraoAsk: TDateField;
    QrOpcoesCTehVeraoIni: TDateField;
    QrOpcoesCTehVeraoFim: TDateField;
    QrOpcoesCTeParamsCTe: TIntegerField;
    QrCTECab2Toma04: TmySQLQuery;
    QrCTECab2Toma04FatID: TIntegerField;
    QrCTECab2Toma04FatNum: TIntegerField;
    QrCTECab2Toma04Empresa: TIntegerField;
    QrCTECab2Toma04Controle: TIntegerField;
    QrCTECab2Toma04enderToma_xLgr: TWideStringField;
    QrCTECab2Toma04enderToma_nro: TWideStringField;
    QrCTECab2Toma04enderToma_xCpl: TWideStringField;
    QrCTECab2Toma04enderToma_xBairro: TWideStringField;
    QrCTECab2Toma04enderToma_cMun: TIntegerField;
    QrCTECab2Toma04enderToma_xMun: TWideStringField;
    QrCTECab2Toma04enderToma_CEP: TIntegerField;
    QrCTECab2Toma04enderToma_UF: TWideStringField;
    QrCTECab2Toma04enderToma_cPais: TIntegerField;
    QrCTECab2Toma04enderToma_xPais: TWideStringField;
    QrCTECab2Toma04enderToma_email: TWideStringField;
    QrCTECab2Toma04Lk: TIntegerField;
    QrCTECab2Toma04DataCad: TDateField;
    QrCTECab2Toma04DataAlt: TDateField;
    QrCTECab2Toma04UserCad: TIntegerField;
    QrCTECab2Toma04UserAlt: TIntegerField;
    QrCTECab2Toma04AlterWeb: TSmallintField;
    QrCTECab2Toma04Ativo: TSmallintField;
    QrCTeIt2ObsCont: TmySQLQuery;
    QrCTeIt2ObsContFatID: TIntegerField;
    QrCTeIt2ObsContFatNum: TIntegerField;
    QrCTeIt2ObsContEmpresa: TIntegerField;
    QrCTeIt2ObsContControle: TIntegerField;
    QrCTeIt2ObsContxCampo: TWideStringField;
    QrCTeIt2ObsContxTexto: TWideStringField;
    QrCTeIt2ObsContLk: TIntegerField;
    QrCTeIt2ObsContDataCad: TDateField;
    QrCTeIt2ObsContDataAlt: TDateField;
    QrCTeIt2ObsContUserCad: TIntegerField;
    QrCTeIt2ObsContUserAlt: TIntegerField;
    QrCTeIt2ObsContAlterWeb: TSmallintField;
    QrCTeIt2ObsContAtivo: TSmallintField;
    QrCTeIt2ObsFisco: TmySQLQuery;
    QrCTeIt2ObsFiscoFatID: TIntegerField;
    QrCTeIt2ObsFiscoFatNum: TIntegerField;
    QrCTeIt2ObsFiscoEmpresa: TIntegerField;
    QrCTeIt2ObsFiscoControle: TIntegerField;
    QrCTeIt2ObsFiscoxCampo: TWideStringField;
    QrCTeIt2ObsFiscoxTexto: TWideStringField;
    QrCTeIt2ObsFiscoLk: TIntegerField;
    QrCTeIt2ObsFiscoDataCad: TDateField;
    QrCTeIt2ObsFiscoDataAlt: TDateField;
    QrCTeIt2ObsFiscoUserCad: TIntegerField;
    QrCTeIt2ObsFiscoUserAlt: TIntegerField;
    QrCTeIt2ObsFiscoAlterWeb: TSmallintField;
    QrCTeIt2ObsFiscoAtivo: TSmallintField;
    QrCTeCab2LocColeta: TmySQLQuery;
    QrCTeCab1Exped: TmySQLQuery;
    QrCTeCab1Receb: TmySQLQuery;
    QrCTeCab2LocEnt: TmySQLQuery;
    QrCTeCab2LocColetaFatID: TIntegerField;
    QrCTeCab2LocColetaFatNum: TIntegerField;
    QrCTeCab2LocColetaEmpresa: TIntegerField;
    QrCTeCab2LocColetaControle: TIntegerField;
    QrCTeCab2LocColetaCNPJ: TWideStringField;
    QrCTeCab2LocColetaCPF: TWideStringField;
    QrCTeCab2LocColetaxNome: TWideStringField;
    QrCTeCab2LocColetaxLgr: TWideStringField;
    QrCTeCab2LocColetanro: TWideStringField;
    QrCTeCab2LocColetaxCpl: TWideStringField;
    QrCTeCab2LocColetaxBairro: TWideStringField;
    QrCTeCab2LocColetacMun: TIntegerField;
    QrCTeCab2LocColetaxMun: TWideStringField;
    QrCTeCab2LocColetaUF: TWideStringField;
    QrCTeCab2LocColetaLk: TIntegerField;
    QrCTeCab2LocColetaDataCad: TDateField;
    QrCTeCab2LocColetaDataAlt: TDateField;
    QrCTeCab2LocColetaUserCad: TIntegerField;
    QrCTeCab2LocColetaUserAlt: TIntegerField;
    QrCTeCab2LocColetaAlterWeb: TSmallintField;
    QrCTeCab2LocColetaAtivo: TSmallintField;
    QrCTeCab1ExpedFatID: TIntegerField;
    QrCTeCab1ExpedFatNum: TIntegerField;
    QrCTeCab1ExpedEmpresa: TIntegerField;
    QrCTeCab1ExpedControle: TIntegerField;
    QrCTeCab1ExpedCNPJ: TWideStringField;
    QrCTeCab1ExpedCPF: TWideStringField;
    QrCTeCab1ExpedIE: TWideStringField;
    QrCTeCab1ExpedxNome: TWideStringField;
    QrCTeCab1Expedfone: TWideStringField;
    QrCTeCab1ExpedxLgr: TWideStringField;
    QrCTeCab1Expednro: TWideStringField;
    QrCTeCab1ExpedxCpl: TWideStringField;
    QrCTeCab1ExpedxBairro: TWideStringField;
    QrCTeCab1ExpedcMun: TIntegerField;
    QrCTeCab1ExpedxMun: TWideStringField;
    QrCTeCab1ExpedCEP: TIntegerField;
    QrCTeCab1ExpedUF: TWideStringField;
    QrCTeCab1ExpedcPais: TIntegerField;
    QrCTeCab1ExpedxPais: TWideStringField;
    QrCTeCab1Expedemail: TWideStringField;
    QrCTeCab1ExpedLk: TIntegerField;
    QrCTeCab1ExpedDataCad: TDateField;
    QrCTeCab1ExpedDataAlt: TDateField;
    QrCTeCab1ExpedUserCad: TIntegerField;
    QrCTeCab1ExpedUserAlt: TIntegerField;
    QrCTeCab1ExpedAlterWeb: TSmallintField;
    QrCTeCab1ExpedAtivo: TSmallintField;
    QrCTeCab1RecebFatID: TIntegerField;
    QrCTeCab1RecebFatNum: TIntegerField;
    QrCTeCab1RecebEmpresa: TIntegerField;
    QrCTeCab1RecebControle: TIntegerField;
    QrCTeCab1RecebCNPJ: TWideStringField;
    QrCTeCab1RecebCPF: TWideStringField;
    QrCTeCab1RecebIE: TWideStringField;
    QrCTeCab1RecebxNome: TWideStringField;
    QrCTeCab1Recebfone: TWideStringField;
    QrCTeCab1RecebxLgr: TWideStringField;
    QrCTeCab1Recebnro: TWideStringField;
    QrCTeCab1RecebxCpl: TWideStringField;
    QrCTeCab1RecebxBairro: TWideStringField;
    QrCTeCab1RecebcMun: TIntegerField;
    QrCTeCab1RecebxMun: TWideStringField;
    QrCTeCab1RecebCEP: TIntegerField;
    QrCTeCab1RecebUF: TWideStringField;
    QrCTeCab1RecebcPais: TIntegerField;
    QrCTeCab1RecebxPais: TWideStringField;
    QrCTeCab1Recebemail: TWideStringField;
    QrCTeCab1RecebLk: TIntegerField;
    QrCTeCab1RecebDataCad: TDateField;
    QrCTeCab1RecebDataAlt: TDateField;
    QrCTeCab1RecebUserCad: TIntegerField;
    QrCTeCab1RecebUserAlt: TIntegerField;
    QrCTeCab1RecebAlterWeb: TSmallintField;
    QrCTeCab1RecebAtivo: TSmallintField;
    QrCTeCab2LocEntFatID: TIntegerField;
    QrCTeCab2LocEntFatNum: TIntegerField;
    QrCTeCab2LocEntEmpresa: TIntegerField;
    QrCTeCab2LocEntControle: TIntegerField;
    QrCTeCab2LocEntCNPJ: TWideStringField;
    QrCTeCab2LocEntCPF: TWideStringField;
    QrCTeCab2LocEntxNome: TWideStringField;
    QrCTeCab2LocEntxLgr: TWideStringField;
    QrCTeCab2LocEntnro: TWideStringField;
    QrCTeCab2LocEntxCpl: TWideStringField;
    QrCTeCab2LocEntxBairro: TWideStringField;
    QrCTeCab2LocEntcMun: TIntegerField;
    QrCTeCab2LocEntxMun: TWideStringField;
    QrCTeCab2LocEntUF: TWideStringField;
    QrCTeCab2LocEntLk: TIntegerField;
    QrCTeCab2LocEntDataCad: TDateField;
    QrCTeCab2LocEntDataAlt: TDateField;
    QrCTeCab2LocEntUserCad: TIntegerField;
    QrCTeCab2LocEntUserAlt: TIntegerField;
    QrCTeCab2LocEntAlterWeb: TSmallintField;
    QrCTeCab2LocEntAtivo: TSmallintField;
    QrCTeIt2Comp: TmySQLQuery;
    QrCTeIt3InfQ: TmySQLQuery;
    QrCTeIt2InfDoc: TmySQLQuery;
    QrCTeIt2CompFatID: TIntegerField;
    QrCTeIt2CompFatNum: TIntegerField;
    QrCTeIt2CompEmpresa: TIntegerField;
    QrCTeIt2CompControle: TIntegerField;
    QrCTeIt2CompxNome: TWideStringField;
    QrCTeIt2CompvComp: TFloatField;
    QrCTeIt2CompLk: TIntegerField;
    QrCTeIt2CompDataCad: TDateField;
    QrCTeIt2CompDataAlt: TDateField;
    QrCTeIt2CompUserCad: TIntegerField;
    QrCTeIt2CompUserAlt: TIntegerField;
    QrCTeIt2CompAlterWeb: TSmallintField;
    QrCTeIt2CompAtivo: TSmallintField;
    QrCTeIt3InfQFatID: TIntegerField;
    QrCTeIt3InfQFatNum: TIntegerField;
    QrCTeIt3InfQEmpresa: TIntegerField;
    QrCTeIt3InfQControle: TIntegerField;
    QrCTeIt3InfQcUnid: TSmallintField;
    QrCTeIt3InfQtpMed: TWideStringField;
    QrCTeIt3InfQqCarga: TFloatField;
    QrCTeIt3InfQLk: TIntegerField;
    QrCTeIt3InfQDataCad: TDateField;
    QrCTeIt3InfQDataAlt: TDateField;
    QrCTeIt3InfQUserCad: TIntegerField;
    QrCTeIt3InfQUserAlt: TIntegerField;
    QrCTeIt3InfQAlterWeb: TSmallintField;
    QrCTeIt3InfQAtivo: TSmallintField;
    QrCTeMoRodoCab: TmySQLQuery;
    QrCTeIt2InfDocFatID: TIntegerField;
    QrCTeIt2InfDocFatNum: TIntegerField;
    QrCTeIt2InfDocEmpresa: TIntegerField;
    QrCTeIt2InfDocControle: TIntegerField;
    QrCTeIt2InfDocMyTpDocInf: TSmallintField;
    QrCTeIt2InfDocInfNFe_chave: TWideStringField;
    QrCTeIt2InfDocInfNFe_PIN: TIntegerField;
    QrCTeIt2InfDocInfNFe_dPrev: TDateField;
    QrCTeIt2InfDocInfNF_nRoma: TWideStringField;
    QrCTeIt2InfDocInfNF_nPed: TWideStringField;
    QrCTeIt2InfDocInfNF_mod: TSmallintField;
    QrCTeIt2InfDocInfNF_serie: TWideStringField;
    QrCTeIt2InfDocInfNF_nDoc: TWideStringField;
    QrCTeIt2InfDocInfNF_dEmi: TDateField;
    QrCTeIt2InfDocInfNF_vBC: TFloatField;
    QrCTeIt2InfDocInfNF_vICMS: TFloatField;
    QrCTeIt2InfDocInfNF_vBCST: TFloatField;
    QrCTeIt2InfDocInfNF_vST: TFloatField;
    QrCTeIt2InfDocInfNF_vProd: TFloatField;
    QrCTeIt2InfDocInfNF_vNF: TFloatField;
    QrCTeIt2InfDocInfNF_nCFOP: TIntegerField;
    QrCTeIt2InfDocInfNF_nPeso: TFloatField;
    QrCTeIt2InfDocInfNF_PIN: TIntegerField;
    QrCTeIt2InfDocInfNF_dPrev: TDateField;
    QrCTeIt2InfDocInfOutros_tpDoc: TSmallintField;
    QrCTeIt2InfDocInfOutros_descOutros: TWideStringField;
    QrCTeIt2InfDocInfOutros_nDoc: TWideStringField;
    QrCTeIt2InfDocInfOutros_dEmi: TDateField;
    QrCTeIt2InfDocInfOutros_vDocFisc: TFloatField;
    QrCTeIt2InfDocInfOutros_dPrev: TDateField;
    QrCTeIt2InfDocLk: TIntegerField;
    QrCTeIt2InfDocDataCad: TDateField;
    QrCTeIt2InfDocDataAlt: TDateField;
    QrCTeIt2InfDocUserCad: TIntegerField;
    QrCTeIt2InfDocUserAlt: TIntegerField;
    QrCTeIt2InfDocAlterWeb: TSmallintField;
    QrCTeIt2InfDocAtivo: TSmallintField;
    QrCteIt2Peri: TmySQLQuery;
    QrCTeMoRodoCabFatID: TIntegerField;
    QrCTeMoRodoCabFatNum: TIntegerField;
    QrCTeMoRodoCabEmpresa: TIntegerField;
    QrCTeMoRodoCabControle: TIntegerField;
    QrCTeMoRodoCabdPrev: TDateField;
    QrCTeMoRodoCablota: TSmallintField;
    QrCTeMoRodoCabCIOT: TLargeintField;
    QrCTeMoRodoCabLk: TIntegerField;
    QrCTeMoRodoCabDataCad: TDateField;
    QrCTeMoRodoCabDataAlt: TDateField;
    QrCTeMoRodoCabUserCad: TIntegerField;
    QrCTeMoRodoCabUserAlt: TIntegerField;
    QrCTeMoRodoCabAlterWeb: TSmallintField;
    QrCTeMoRodoCabAtivo: TSmallintField;
    QrCTeMoRodoCabRNTRC: TWideStringField;
    QrCTeIt3Dup: TmySQLQuery;
    QrCteIt2PeriFatID: TIntegerField;
    QrCteIt2PeriFatNum: TIntegerField;
    QrCteIt2PeriEmpresa: TIntegerField;
    QrCteIt2PeriControle: TIntegerField;
    QrCteIt2PerinONU: TWideStringField;
    QrCteIt2PerixNomeAE: TWideStringField;
    QrCteIt2PerixClaRisco: TWideStringField;
    QrCteIt2PerigrEmb: TWideStringField;
    QrCteIt2PeriqTotProd: TWideStringField;
    QrCteIt2PeriqVolTipo: TWideStringField;
    QrCteIt2PeripontoFulgor: TWideStringField;
    QrCteIt2PeriLk: TIntegerField;
    QrCteIt2PeriDataCad: TDateField;
    QrCteIt2PeriDataAlt: TDateField;
    QrCteIt2PeriUserCad: TIntegerField;
    QrCteIt2PeriUserAlt: TIntegerField;
    QrCteIt2PeriAlterWeb: TSmallintField;
    QrCteIt2PeriAtivo: TSmallintField;
    QrCTeIt1AutXml: TmySQLQuery;
    QrCTeIt3DupFatID: TIntegerField;
    QrCTeIt3DupFatNum: TIntegerField;
    QrCTeIt3DupEmpresa: TIntegerField;
    QrCTeIt3DupControle: TIntegerField;
    QrCTeIt3DupnDup: TWideStringField;
    QrCTeIt3DupdVenc: TDateField;
    QrCTeIt3DupvDup: TFloatField;
    QrCTeIt3DupLk: TIntegerField;
    QrCTeIt3DupDataCad: TDateField;
    QrCTeIt3DupDataAlt: TDateField;
    QrCTeIt3DupUserCad: TIntegerField;
    QrCTeIt3DupUserAlt: TIntegerField;
    QrCTeIt3DupAlterWeb: TSmallintField;
    QrCTeIt3DupAtivo: TSmallintField;
    QrCTeCabAinfCteSub_ContribICMS: TSmallintField;
    QrCTeCabAinfCteSub_TipoDoc: TSmallintField;
    QrCTeIt1AutXmlFatID: TIntegerField;
    QrCTeIt1AutXmlFatNum: TIntegerField;
    QrCTeIt1AutXmlEmpresa: TIntegerField;
    QrCTeIt1AutXmlControle: TIntegerField;
    QrCTeIt1AutXmlAddForma: TSmallintField;
    QrCTeIt1AutXmlAddIDCad: TIntegerField;
    QrCTeIt1AutXmlTipo: TSmallintField;
    QrCTeIt1AutXmlCNPJ: TWideStringField;
    QrCTeIt1AutXmlCPF: TWideStringField;
    QrCTeIt1AutXmlLk: TIntegerField;
    QrCTeIt1AutXmlDataCad: TDateField;
    QrCTeIt1AutXmlDataAlt: TDateField;
    QrCTeIt1AutXmlUserCad: TIntegerField;
    QrCTeIt1AutXmlUserAlt: TIntegerField;
    QrCTeIt1AutXmlAlterWeb: TSmallintField;
    QrCTeIt1AutXmlAtivo: TSmallintField;
    QrCTeLEnC: TmySQLQuery;
    QrCTeLEnCId: TWideStringField;
    QrCTeLEnCFatID: TIntegerField;
    QrCTeLEnCFatNum: TIntegerField;
    QrAtoArq: TmySQLQuery;
    QrAtoArqIDCtrl: TIntegerField;
    QrCTeAut: TmySQLQuery;
    QrCTeAutIDCtrl: TIntegerField;
    QrCTeAutLoteEnv: TIntegerField;
    QrCTeAutId: TWideStringField;
    QrCTeCan: TmySQLQuery;
    QrCTeCanIDCtrl: TIntegerField;
    QrCTeCanId: TWideStringField;
    QrCTeInu: TmySQLQuery;
    QrCTeInuCodigo: TIntegerField;
    QrCTeInuCodUsu: TIntegerField;
    QrCTeInuNome: TWideStringField;
    QrCTeInuEmpresa: TIntegerField;
    QrCTeInuDataC: TDateField;
    QrCTeInuversao: TFloatField;
    QrCTeInuId: TWideStringField;
    QrCTeInutpAmb: TSmallintField;
    QrCTeInucUF: TSmallintField;
    QrCTeInuano: TSmallintField;
    QrCTeInuCNPJ: TWideStringField;
    QrCTeInumodelo: TSmallintField;
    QrCTeInuSerie: TIntegerField;
    QrCTeInunCTIni: TIntegerField;
    QrCTeInunCTFim: TIntegerField;
    QrCTeInuJustif: TIntegerField;
    QrCTeInuxJust: TWideStringField;
    QrCTeInucStat: TIntegerField;
    QrCTeInuxMotivo: TWideStringField;
    QrCTeInudhRecbto: TDateTimeField;
    QrCTeInunProt: TWideStringField;
    QrCTeInuLk: TIntegerField;
    QrCTeInuDataCad: TDateField;
    QrCTeInuDataAlt: TDateField;
    QrCTeInuUserCad: TIntegerField;
    QrCTeInuUserAlt: TIntegerField;
    QrCTeInuAlterWeb: TSmallintField;
    QrCTeInuAtivo: TSmallintField;
    QrCTeInuXML_Inu: TWideMemoField;
    QrFrtFatSeg: TmySQLQuery;
    QrFrtFatSegCodigo: TIntegerField;
    QrFrtFatSegControle: TIntegerField;
    QrFrtFatSegrespSeg: TSmallintField;
    QrFrtFatSegxSeg: TWideStringField;
    QrFrtFatSegnApol: TWideStringField;
    QrFrtFatSegnAver: TWideStringField;
    QrFrtFatSegvCarga: TFloatField;
    QrFrtFatSegLk: TIntegerField;
    QrFrtFatSegDataCad: TDateField;
    QrFrtFatSegDataAlt: TDateField;
    QrFrtFatSegUserCad: TIntegerField;
    QrFrtFatSegUserAlt: TIntegerField;
    QrFrtFatSegAlterWeb: TSmallintField;
    QrFrtFatSegAtivo: TSmallintField;
    QrCteIt2Seg: TmySQLQuery;
    QrCteIt2SegFatID: TIntegerField;
    QrCteIt2SegFatNum: TIntegerField;
    QrCteIt2SegEmpresa: TIntegerField;
    QrCteIt2SegControle: TIntegerField;
    QrCteIt2SegrespSeg: TSmallintField;
    QrCteIt2SegxSeg: TWideStringField;
    QrCteIt2SegnApol: TWideStringField;
    QrCteIt2SegnAver: TWideStringField;
    QrCteIt2SegvCarga: TFloatField;
    QrCTeMoRodoVPed: TmySQLQuery;
    QrCTeMoRodoVeic: TmySQLQuery;
    QrCTeMoRodoLacr: TmySQLQuery;
    QrCTeMoRodoMoto: TmySQLQuery;
    QrCTeMoRodoMotoFatID: TIntegerField;
    QrCTeMoRodoMotoFatNum: TIntegerField;
    QrCTeMoRodoMotoEmpresa: TIntegerField;
    QrCTeMoRodoMotoControle: TIntegerField;
    QrCTeMoRodoMotoConta: TIntegerField;
    QrCTeMoRodoMotoxNome: TWideStringField;
    QrCTeMoRodoMotoCPF: TWideStringField;
    QrCTeMoRodoMotoLk: TIntegerField;
    QrCTeMoRodoMotoDataCad: TDateField;
    QrCTeMoRodoMotoDataAlt: TDateField;
    QrCTeMoRodoMotoUserCad: TIntegerField;
    QrCTeMoRodoMotoUserAlt: TIntegerField;
    QrCTeMoRodoMotoAlterWeb: TSmallintField;
    QrCTeMoRodoMotoAtivo: TSmallintField;
    QrCTeMoRodoLacrFatID: TIntegerField;
    QrCTeMoRodoLacrFatNum: TIntegerField;
    QrCTeMoRodoLacrEmpresa: TIntegerField;
    QrCTeMoRodoLacrControle: TIntegerField;
    QrCTeMoRodoLacrConta: TIntegerField;
    QrCTeMoRodoLacrnLacre: TWideStringField;
    QrCTeMoRodoLacrLk: TIntegerField;
    QrCTeMoRodoLacrDataCad: TDateField;
    QrCTeMoRodoLacrDataAlt: TDateField;
    QrCTeMoRodoLacrUserCad: TIntegerField;
    QrCTeMoRodoLacrUserAlt: TIntegerField;
    QrCTeMoRodoLacrAlterWeb: TSmallintField;
    QrCTeMoRodoLacrAtivo: TSmallintField;
    QrCTeMoRodoVeicFatID: TIntegerField;
    QrCTeMoRodoVeicFatNum: TIntegerField;
    QrCTeMoRodoVeicEmpresa: TIntegerField;
    QrCTeMoRodoVeicControle: TIntegerField;
    QrCTeMoRodoVeicConta: TIntegerField;
    QrCTeMoRodoVeiccInt: TIntegerField;
    QrCTeMoRodoVeicRENAVAM: TWideStringField;
    QrCTeMoRodoVeicplaca: TWideStringField;
    QrCTeMoRodoVeictara: TIntegerField;
    QrCTeMoRodoVeiccapKG: TIntegerField;
    QrCTeMoRodoVeiccapM3: TIntegerField;
    QrCTeMoRodoVeictpProp: TWideStringField;
    QrCTeMoRodoVeictpVeic: TSmallintField;
    QrCTeMoRodoVeictpRod: TSmallintField;
    QrCTeMoRodoVeictpCar: TSmallintField;
    QrCTeMoRodoVeicUF: TWideStringField;
    QrCTeMoRodoVeicLk: TIntegerField;
    QrCTeMoRodoVeicDataCad: TDateField;
    QrCTeMoRodoVeicDataAlt: TDateField;
    QrCTeMoRodoVeicUserCad: TIntegerField;
    QrCTeMoRodoVeicUserAlt: TIntegerField;
    QrCTeMoRodoVeicAlterWeb: TSmallintField;
    QrCTeMoRodoVeicAtivo: TSmallintField;
    QrCTeMoRodoVPedFatID: TIntegerField;
    QrCTeMoRodoVPedFatNum: TIntegerField;
    QrCTeMoRodoVPedEmpresa: TIntegerField;
    QrCTeMoRodoVPedControle: TIntegerField;
    QrCTeMoRodoVPedConta: TIntegerField;
    QrCTeMoRodoVPedCNPJForn: TWideStringField;
    QrCTeMoRodoVPednCompra: TWideStringField;
    QrCTeMoRodoVPedCNPJPg: TWideStringField;
    QrCTeMoRodoVPedvValePed: TFloatField;
    QrCTeMoRodoVPedLk: TIntegerField;
    QrCTeMoRodoVPedDataCad: TDateField;
    QrCTeMoRodoVPedDataAlt: TDateField;
    QrCTeMoRodoVPedUserCad: TIntegerField;
    QrCTeMoRodoVPedUserAlt: TIntegerField;
    QrCTeMoRodoVPedAlterWeb: TSmallintField;
    QrCTeMoRodoVPedAtivo: TSmallintField;
    QrCTeMoRodoProp: TmySQLQuery;
    QrCTeMoRodoOcc: TmySQLQuery;
    QrCTeMoRodoOccFatID: TIntegerField;
    QrCTeMoRodoOccFatNum: TIntegerField;
    QrCTeMoRodoOccEmpresa: TIntegerField;
    QrCTeMoRodoOccControle: TIntegerField;
    QrCTeMoRodoOccConta: TIntegerField;
    QrCTeMoRodoOccserie: TWideStringField;
    QrCTeMoRodoOccnOcc: TIntegerField;
    QrCTeMoRodoOccdEmi: TDateField;
    QrCTeMoRodoOccemiOcc_CNPJ: TWideStringField;
    QrCTeMoRodoOccemiOcc_cInt: TIntegerField;
    QrCTeMoRodoOccemiOcc_IE: TWideStringField;
    QrCTeMoRodoOccemiOcc_UF: TWideStringField;
    QrCTeMoRodoOccemiOcc_fone: TWideStringField;
    QrCTeMoRodoOccLk: TIntegerField;
    QrCTeMoRodoOccDataCad: TDateField;
    QrCTeMoRodoOccDataAlt: TDateField;
    QrCTeMoRodoOccUserCad: TIntegerField;
    QrCTeMoRodoOccUserAlt: TIntegerField;
    QrCTeMoRodoOccAlterWeb: TSmallintField;
    QrCTeMoRodoOccAtivo: TSmallintField;
    QrCTeMoRodoPropFatID: TIntegerField;
    QrCTeMoRodoPropFatNum: TIntegerField;
    QrCTeMoRodoPropEmpresa: TIntegerField;
    QrCTeMoRodoPropControle: TIntegerField;
    QrCTeMoRodoPropConta: TIntegerField;
    QrCTeMoRodoPropCPF: TWideStringField;
    QrCTeMoRodoPropCNPJ: TWideStringField;
    QrCTeMoRodoPropxNome: TWideStringField;
    QrCTeMoRodoPropIE: TWideStringField;
    QrCTeMoRodoPropUF: TWideStringField;
    QrCTeMoRodoProptpProp: TSmallintField;
    QrCTeMoRodoPropLk: TIntegerField;
    QrCTeMoRodoPropDataCad: TDateField;
    QrCTeMoRodoPropDataAlt: TDateField;
    QrCTeMoRodoPropUserCad: TIntegerField;
    QrCTeMoRodoPropUserAlt: TIntegerField;
    QrCTeMoRodoPropAlterWeb: TSmallintField;
    QrCTeMoRodoPropAtivo: TSmallintField;
    QrCTeMoRodoPropRNTRC: TWideStringField;
    QrFrtFatMoRodoVPed: TmySQLQuery;
    QrFrtFatMoRodoVPedCodigo: TIntegerField;
    QrFrtFatMoRodoVPedControle: TIntegerField;
    QrFrtFatMoRodoVPedCNPJForn: TWideStringField;
    QrFrtFatMoRodoVPednCompra: TWideStringField;
    QrFrtFatMoRodoVPedCNPJPg: TWideStringField;
    QrFrtFatMoRodoVPedvValePed: TFloatField;
    QrFrtFatMoRodoVPedLk: TIntegerField;
    QrFrtFatMoRodoVPedDataCad: TDateField;
    QrFrtFatMoRodoVPedDataAlt: TDateField;
    QrFrtFatMoRodoVPedUserCad: TIntegerField;
    QrFrtFatMoRodoVPedUserAlt: TIntegerField;
    QrFrtFatMoRodoVPedAlterWeb: TSmallintField;
    QrFrtFatMoRodoVPedAtivo: TSmallintField;
    QrFrtFatMoRodoVeic: TmySQLQuery;
    QrFrtFatMoRodoVeicCodigo: TIntegerField;
    QrFrtFatMoRodoVeicControle: TIntegerField;
    QrFrtFatMoRodoVeiccInt: TIntegerField;
    QrFrtFatMoRodoVeicLk: TIntegerField;
    QrFrtFatMoRodoVeicDataCad: TDateField;
    QrFrtFatMoRodoVeicDataAlt: TDateField;
    QrFrtFatMoRodoVeicUserCad: TIntegerField;
    QrFrtFatMoRodoVeicUserAlt: TIntegerField;
    QrFrtFatMoRodoVeicAlterWeb: TSmallintField;
    QrFrtFatMoRodoVeicAtivo: TSmallintField;
    QrFrtFatMoRodoLacr: TmySQLQuery;
    QrFrtFatMoRodoLacrCodigo: TIntegerField;
    QrFrtFatMoRodoLacrControle: TIntegerField;
    QrFrtFatMoRodoLacrnLacre: TWideStringField;
    QrFrtFatMoRodoLacrLk: TIntegerField;
    QrFrtFatMoRodoLacrDataCad: TDateField;
    QrFrtFatMoRodoLacrDataAlt: TDateField;
    QrFrtFatMoRodoLacrUserCad: TIntegerField;
    QrFrtFatMoRodoLacrUserAlt: TIntegerField;
    QrFrtFatMoRodoLacrAlterWeb: TSmallintField;
    QrFrtFatMoRodoLacrAtivo: TSmallintField;
    QrFrtFatMoRodoMoto: TmySQLQuery;
    QrFrtFatMoRodoMotoCodigo: TIntegerField;
    QrFrtFatMoRodoMotoControle: TIntegerField;
    QrFrtFatMoRodoMotoxNome: TWideStringField;
    QrFrtFatMoRodoMotoCPF: TWideStringField;
    QrFrtFatMoRodoMotoLk: TIntegerField;
    QrFrtFatMoRodoMotoDataCad: TDateField;
    QrFrtFatMoRodoMotoDataAlt: TDateField;
    QrFrtFatMoRodoMotoUserCad: TIntegerField;
    QrFrtFatMoRodoMotoUserAlt: TIntegerField;
    QrFrtFatMoRodoMotoAlterWeb: TSmallintField;
    QrFrtFatMoRodoMotoAtivo: TSmallintField;
    QrTrnsIts: TmySQLQuery;
    QrTrnsItsCodigo: TIntegerField;
    QrTrnsItsControle: TIntegerField;
    QrTrnsItsNome: TWideStringField;
    QrTrnsItsRENAVAM: TWideStringField;
    QrTrnsItsPlaca: TWideStringField;
    QrTrnsItsTaraKg: TIntegerField;
    QrTrnsItsCapKg: TIntegerField;
    QrTrnsItsTpVeic: TSmallintField;
    QrTrnsItsTpRod: TSmallintField;
    QrTrnsItsTpCar: TSmallintField;
    QrTrnsItsLk: TIntegerField;
    QrTrnsItsDataCad: TDateField;
    QrTrnsItsDataAlt: TDateField;
    QrTrnsItsUserCad: TIntegerField;
    QrTrnsItsUserAlt: TIntegerField;
    QrTrnsItsAlterWeb: TSmallintField;
    QrTrnsItsAtivo: TSmallintField;
    QrTrnsItscapM3: TIntegerField;
    QrTrnsItsUF: TWideStringField;
    QrTerceiro: TmySQLQuery;
    QrTerceiroRNTRC: TWideStringField;
    QrTerceiroNO_ENT: TWideStringField;
    QrTerceiroCPF: TWideStringField;
    QrTerceiroCNPJ: TWideStringField;
    QrTerceiroIE: TWideStringField;
    QrTerceiroUF: TWideStringField;
    QrTrnsCadTpProp: TSmallintField;
    QrFrtFatComp: TmySQLQuery;
    QrEveTp: TmySQLQuery;
    QrEveTpnSeqEvento: TSmallintField;
    QrCTeInut: TmySQLQuery;
    QrCTeInutCodigo: TIntegerField;
    QrEveIts: TmySQLQuery;
    QrEveItsControle: TIntegerField;
    QrEveSub: TmySQLQuery;
    QrEveSubSubCtrl: TIntegerField;
    QrLoc: TmySQLQuery;
    QrLoc2: TmySQLQuery;
    QrFrtFatDupControle: TIntegerField;
    QrFrtFatCabNome: TWideStringField;
    QrFrtFatCabNatOp: TWideStringField;
    QrFrtFatCabCFOP: TWideStringField;
    QrFrtFatCabTpCTe: TSmallintField;
    QrFrtFatCabMyCST: TSmallintField;
    QrFrtFatCabICMS_pICMS: TFloatField;
    QrFrtFatCabICMS_pRedBC: TFloatField;
    QrFrtFatCabICMS_pICMSSTRet: TFloatField;
    QrFrtFatCabICMS_pRedBCOutraUF: TFloatField;
    QrFrtFatCabICMS_pICMSOutraUF: TFloatField;
    QrFrtFatCabiTotTrib_Fonte: TIntegerField;
    QrFrtFatCabiTotTrib_Tabela: TIntegerField;
    QrFrtFatCabiTotTrib_Ex: TIntegerField;
    QrFrtFatCabiTotTrib_Codigo: TLargeintField;
    QrOpcoesCTeCNPJ_CPF: TWideStringField;
    QrOpcoesCTefaturasep: TWideStringField;
    QrOpcoesCTefaturaseq: TSmallintField;
    QrOpcoesCTeFaturaNum: TSmallintField;
    QrTerceiroTipo: TSmallintField;
    QrCTeCabAinfCCe_CCeConta: TIntegerField;
    QrArq: TmySQLQuery;
    QrArqFatID: TIntegerField;
    QrArqFatNum: TIntegerField;
    QrArqEmpresa: TIntegerField;
    QrArqIDCtrl: TIntegerField;
    QrArqXML_CTe: TWideMemoField;
    QrArqXML_Aut: TWideMemoField;
    QrArqXML_Can: TWideMemoField;
    QrArqLk: TIntegerField;
    QrArqDataCad: TDateField;
    QrArqDataAlt: TDateField;
    QrArqUserCad: TIntegerField;
    QrArqAlterWeb: TSmallintField;
    QrArqUserAlt: TIntegerField;
    QrArqAtivo: TSmallintField;
    QrLocEve: TmySQLQuery;
    QrLocEveITENS: TLargeintField;
    QrFrtFatCompCodigo: TIntegerField;
    QrFrtFatCompControle: TIntegerField;
    QrFrtFatCompCodComp: TIntegerField;
    QrFrtFatCompxNome: TWideStringField;
    QrFrtFatCompvComp: TFloatField;
    QrFrtFatCompLk: TIntegerField;
    QrFrtFatCompDataCad: TDateField;
    QrFrtFatCompDataAlt: TDateField;
    QrFrtFatCompUserCad: TIntegerField;
    QrFrtFatCompUserAlt: TIntegerField;
    QrFrtFatCompAlterWeb: TSmallintField;
    QrFrtFatCompAtivo: TSmallintField;
    QrOpcoesCTeCTeVerEPEC: TFloatField;
    QrFilialCTeVerEPEC: TFloatField;
    QrFilialDirCTeEvePedEPEC: TWideStringField;
    QrFilialDirCTeEveRetEPEC: TWideStringField;
    QrFilialCTeUF_EPEC: TWideStringField;
    QrFilialCTeUF_Conting: TWideStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrOpcoesCTeAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FMsg: String;
    FLastVerCTeLayI: Double;
    //
  procedure MensagemCalcularManualCST(CST: Integer; Texto: String);
  public
    { Public declarations }
    procedure AbreXML_NoNavegadorPadrao(Empresa: Integer; Ext, Arq: String;
              Assinado: Boolean);
    procedure AbreXML_NoTWebBrowser(Empresa: Integer; Ext, Arq: String;
              Assinado: Boolean; WebBrowser: TWebBrowser);
    function  AtualizaXML_No_BD_Aut(const IDCtrl, LoteEnv: Integer; const Id:
              String; var Dir, Aviso: String): Boolean;
    function  AtualizaXML_No_BD_Can(const IDCtrl: Integer; const Id:
              String; var Dir, Aviso: String): Boolean;
    function  AtualizaXML_No_BD_ConsultaCTe(const Chave, Id: String;
              const IDCtrl: Integer; var Dir, Aviso: String): Boolean;
    function  AtualizaXML_No_BD_Inu(const Codigo: Integer; var Dir, Aviso:
              String): Boolean;
    function  AtualizaXML_No_BD_Tudo(InfoTermino: Boolean): Boolean;
    function  AtualizaXML_No_BD_CTe(const IDCtrl: Integer; var Dir, Aviso:
              String): Boolean;
    function  AssinarArquivoXML(XMLGerado_Arq, XMLGerado_Dir: String; Empresa,
              TipoDoc, IDCtrl: Integer; var XMLAssinado_Dir: String): Boolean;
    function  CancelaFaturamento(ChaveCTe: String): Boolean;
    procedure ColoreStatusCTeEmDBGrid(const DBGrid: TDBGrid;
              const Rect: TRect; DataCol: Integer; Column: TColumn; State:
              TGridDrawState; const Status: Integer);
    procedure ColoreSitConfCTeEmDBGrid(const DBGrid: TDBGrid;
              const Rect: TRect; DataCol: Integer; Column: TColumn; State:
              TGridDrawState; const SitConf: Integer);
    function  ERf(Grupo, Codigo: Integer; Valor: Double; Casas: Integer): Double;
    function  ERi(Grupo, Codigo, Valor: Integer): Integer;
    function  ERi64(Grupo, Codigo: Integer; Valor: Int64): Int64;
    function  ERx(Grupo, Codigo: Integer; Valor: String): String;
    function  Evento_Obtem_nSeqEvento(FatID, FatNum, Empresa, tpEvento: Integer):
              Integer;
    procedure EventoObtemSub(const Controle: Integer; const IdLote: String;
              var SubCtrl: Integer; var SQLType: TSQLType);
    function  ExcluiDadosTabelasCTe(FatID, FatNum: Integer): Boolean;
    function  EventoObtemCtrl(EventoLote, tpEvento, nSeqEvento: Integer;
              chCTe: String): Integer;
    function  FormataLoteCTe(Lote: Integer): String;
    function  InsereDadosDeFaturaNasTabelasCTe(FatID, FatNum, Empresa, SerieCTe,
              NumeroCTe: Integer; DataFat: TDateTime; HoraFat: TTime;
              LaAviso1, LaAviso2: TLabel): Boolean;
    function  InsereDadosCTeCab(const FatID, FatNum, Empresa, IDCtrl_Atual,
              VersaoCTe, SerieCTe, NumeroCTe: Integer; Recria: Boolean;
              DataFat: TDateTime; HoraFat: TTime; var IDCtrl: Integer): Boolean;
    function  LocalizaCTeInfoEveCCe(const ChCTe: String; nSeqEvento: Integer; var
              CNPJ, CPF: String; var
              dhEvento: TDateTime; var dhEventoTZD, verEvento: Double; var
              nCondUso: Integer; var CTeEveRCCeConta: Integer): Boolean;
    function  LocalizaCTeInfoEveCan(const ChCTe: String; var Id, xJust: String;
              var cJust: Integer): Boolean;
    function  LocalizaCTeInfoEveEPEC(const ChCTe: String; var CNPJ, Id, xJust: String;
              var cJust, CTeEveREPECConta: Integer; var vICMS, vTPrest, vCarga:
              Double; var CodInfoTomad, Toma04_toma: Integer; var Toma04_UF,
              Toma04_CNPJ, Toma04_CPF, Toma04_IE: String; var Modal: Integer;
              var UFIni, UFFim: String; var verEvento, VersaoEnv: Double;
              var dhEvento: TDateTime): Boolean;
    function  MontaID_Inutilizacao(const cUF, Ano, emitCNPJ, Modelo, Serie, nNFIni,
              nNFFim: String; var Id: String): Boolean;
    function  ObtemDirXML(const Ext: String; var Dir: String; Assinado:
              Boolean): Boolean;
    function  ObtemNome(Tabela: String; Codigo: Integer): String;
    function  ObtemXMun(Codigo: Integer): String;
    function  MensagemDeID_CTe(Grupo, Codigo: Integer; Valor, Texto:
              String; Seq_Chamou: Integer): String;
    procedure ReopenCTeArq(IDCtrl: Integer);
    procedure ReopenCTeLayI();
    function  ReopenEmpresa(Empresa: Integer): Boolean;
    function  ReopenOpcoesCTe(QrOpCTe: TmySQLQuery; Empresa: Integer; Avisa: Boolean): Boolean;
    function  ReopenEntidadeCTe(Entidade: Integer; Qry: TmySQLQuery): Boolean;
    function  VerificaTabelaLayI(): Boolean;
    function  DefX(Grupo, Codigo: Integer; Valor: String): String;
    function  DefI(Grupo, Codigo: Integer; ValMin, ValMax, Numero: Integer): Integer;
    function  DefDomI(Grupo, Codigo, Valor: Integer): Integer;
    function  DefDomX(Grupo, Codigo: Integer; Valor: String): String;
    function  DefMsg(Grupo, Codigo: Integer; MsgExtra, Valor: String): Boolean;
    //
    function  Obtem_Arquivo_XML_(Empresa: Integer; Ext, Arq: String; Assinado: Boolean): String;
    procedure ObtemValorAproximadoDeTributos(const Fonte, Tabela, Ex: Integer;
              Codigo: Int64; UF: String; (*Importado: Boolean;*)
              const vServ, vDesc: Double;
              var vBasTrib, pTotTrib, vTotTrib: Double;
              (*var tabTotTrib: TIBPTaxTabs;*) var verTotTrib: String;
              var tpAliqTotTrib: Integer (*; var fontTotTrib: TIBPTaxFont*));
    function  ReopenCTeCabA(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeCab2Toma04(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeCab2LocColeta(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeCab1Exped(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeCab1Receb(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeCab2LocEnt(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeIt2ObsCont(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeIt2ObsFisco(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeIt2Comp(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeIt3InfQ(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeIt2InfDoc(FatID, FatNum, Empresa: Integer;
              CTeMyTpDocInf: TCTeMyTpDocInf): Integer;
    function  ReopenCTeIt2Peri(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeIt3Dup(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeIt1AutXml(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeIt2Seg(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeMoRodoCab(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeMoRodoLacr(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeMoRodoMoto(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeMoRodoVeic(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeMoRodoVPed(FatID, FatNum, Empresa: Integer): Integer;
    function  ReopenCTeMoRodoProp(FatID, FatNum, Empresa, Conta: Integer): Integer;
    function  ReopenCTeMoRodoOcc(FatID, FatNum, Empresa: Integer): Integer;
    //
    procedure ReopenEntiCTe(Entidade: Integer);
    procedure ReopenFrtFatComp(Codigo: Integer);
    procedure ReopenFrtFatInfQ(Codigo: Integer);
    procedure ReopenFrtFatInfDoc(Codigo: Integer);
    procedure ReopenFrtFOCAtrDef(Codigo: Integer);
    procedure ReopenFrtFatPeri(Codigo: Integer);
    procedure ReopenFrtFatDup(Codigo: Integer; TabLctA: String);
    procedure ReopenFrtFatInfCTeSub(Codigo: Integer);
    procedure ReopenFrtFatInfCTeComp(Codigo: Integer);
    procedure ReopenFrtFatInfCTeAnu(Codigo: Integer);
    procedure ReopenFrtFatAutXml(Codigo: Integer);
    procedure ReopenFrtFatSeg(Codigo: Integer);
    procedure ReopenTrnsCad(Codigo: Integer);
    procedure ReopenTrnsIts(Controle: Integer);
    procedure ReopenFrtFatMoRodoLacr(Codigo: Integer);
    procedure ReopenFrtFatMoRodoMoto(Codigo: Integer);
    procedure ReopenFrtFatMoRodoVeic(Codigo: Integer);
    procedure ReopenFrtFatMoRodoVPed(Codigo: Integer);
    //
    function  ReopenLocEve(Id: String; Evento: Integer): Boolean;
    procedure ReopenTerceiro(Codigo: Integer);
    //
    function  SalvaXML(Ext, Arq: String; Texto: String; rtfRetWS:
              TMemo; Assinado: Boolean): String;
    function  StepCTeCab(FatID, FatNum, Empresa: Integer; Status:
              TCTeMyStatus; LaAviso1, LaAviso2: TLabel): Boolean;
    function  TituloArqXML(const Ext: String; var Titulo, Descri: String): Boolean;

  end;

var
  DmCTe_0000: TDmCTe_0000;
  //
  sVersaoConsumoWS_CTe: array[0..MaxTipoConsumoWS_CTe] of string;

implementation

uses Module, UMySQLModule, ModuleGeral, UnCTe_PF, MyDBCheck, Entidade2, UnXXe_PF;

{$R *.dfm}

{ TDmCTe_0000 }

procedure TDmCTe_0000.AbreXML_NoNavegadorPadrao(Empresa: Integer; Ext,
  Arq: String; Assinado: Boolean);
var
  Dir, Arquivo: String;
  PWC: array[0..127] of WideChar;
  Tam: Integer;
begin
  if not ReopenEmpresa(Empresa) then Exit;
  if ObtemDirXML(Ext, Dir, Assinado) then
  begin
    Arquivo := Dir + Arq + Ext;
    Tam := Length(Arquivo) + 1;
    StringToWideChar(Arquivo, PWC, Tam);
    try
      HlinkNavigateString(nil, PWC);
    finally
      //
    end;
  end;
end;

procedure TDmCTe_0000.AbreXML_NoTWebBrowser(Empresa: Integer; Ext, Arq: String;
  Assinado: Boolean; WebBrowser: TWebBrowser);
var
  Dir, Arquivo: String;
begin
  if not ReopenEmpresa(Empresa) then Exit;
  if ObtemDirXML(Ext, Dir, Assinado) then
  begin
    Arquivo := Dir + Arq + Ext;
    WebBrowser.Navigate(Arquivo);
  end;
end;

function TDmCTe_0000.AssinarArquivoXML(XMLGerado_Arq, XMLGerado_Dir: String;
  Empresa, TipoDoc, IDCtrl: Integer; var XMLAssinado_Dir: String): Boolean;
var
  fEntrada: file;
  NumRead, i: integer;
  buf: char;
  xmldoc, ref: String;
  Ext, FonteXML: String;
  //
  NumeroSerial: String;
  Cert: ICertificate2;
  Assinada: String;
  Aviso, Dir: String;
begin
  Result := False;
  // Abrir    Arquivo := XMLGerado_Dir + XMLGerado_Arq;
  if not Geral.VerificaDir(XMLGerado_Dir, '\', 'XML gerado', True) then Exit;
  FonteXML := XMLGerado_Dir + XMLGerado_Arq;
  if not FileExists(FonteXML) then
  begin
    Geral.MB_Aviso('Arquivo n�o encontrado: ' + FonteXML);
    Exit;
  end;
  //
  AssignFile(fEntrada, XMLGerado_Dir + XMLGerado_Arq);
  Reset(fEntrada, 1);
  xmlDoc := '';

  for i := 1 to FileSize(fEntrada) do
  begin
    Blockread(fEntrada, buf, 1, NumRead);
    xmlDoc := xmlDoc + buf;
  end;

  CloseFile(fEntrada);

  //

  if xmlDoc <> '' then
  begin
    ReopenEmpresa(Empresa);
    if QrFilialCTeSerNum.Value <> '' then
    begin
      NumeroSerial := QrFilialCTeSerNum.Value;
      //
      case TipoDoc of
        0: ref := 'infCte';
        //1: ref := 'infCanc';
        //2: ref := 'infInut';
        else
        begin
          Geral.MB_Erro('"ref" indefinido. "TipoDoc" n�o implementado!');
          ref := '???';
        end;
      end;

      case TipoDoc of
        0: Ext := CTE_EXT_CTE_XML;
        //1: Ext := CTE_EXT_CAN_XML;
        //2: Ext := CTE_EXT_INU_XML;
        else
        begin
          Geral.MB_Erro('"Ext" indefinido. "TipoDoc" n�o implementado!');
          Ext := '???';
        end;
      end;
      if XXe_PF.ObtemCertificado(NumeroSerial, Cert) then
      begin
        Result := CTe_PF.AssinarMSXML(xmlDoc, Cert, Assinada);
        if Result then
        begin
          SalvaXML(Ext, XMLGerado_Arq,  Assinada, nil, True);
          if TipoDoc = 0 then
          begin
            DModG.ReopenParamsEmp(Empresa);
            Dir := QrOpcoesCTeDirCTeAss.Value;
            Aviso := '';
            AtualizaXML_No_BD_CTe(IDCtrl, Dir, Aviso);
            if Aviso <> '' then Geral.MB_Aviso(
            'O arquivo abaixo n�o foi localizado para ser adicionado ao banco de dados:'
             + sLineBreak + Aviso);
          end;
        end;
      end;// else Result := False;
    end
    else
      Geral.MB_Aviso('Serial do Certificado n�o informado...');
  end
  else
    Geral.MB_Aviso('Documento XML para assinatura n�o informado...');
  Result := True;
end;

function TDmCTe_0000.AtualizaXML_No_BD_Aut(const IDCtrl, LoteEnv: Integer;
  const Id: String; var Dir, Aviso: String): Boolean;
var
  Arq, DirArq, Buf, Prot: String;
  XML_Aut: String;
  Texto: TStringList;
  XML_LoteEnv: PChar;
  //FatID, FatNum, Empresa,
  Ini, Fim: Integer;
begin
  Arq := FormataLoteCTe(LoteEnv) + CTE_EXT_PRO_REC_XML;
  Geral.VerificaDir(Dir, '\', 'Lote de CTe', True);
  DirArq := Dir + Arq;
  if not FileExists(DirArq) then
  begin
    Aviso := Aviso + DirArq + sLineBreak;
  end else begin
    buf := '';
    Texto := TStringList.Create;
    Texto.Clear;
    Texto.LoadFromFile(DirArq);
    XML_LoteEnv := PChar(Texto.Text);
    buf := Copy(XML_LoteEnv, 1);
    Ini := pos('<protCTe', buf);
    while Ini > 0 do
    begin
      Prot := '';
      Geral.SeparaPrimeiraOcorrenciaDeTexto('<protCTe', buf, Prot, buf);
      if Prot <> '' then
      begin
        Fim := pos('</protCTe>', buf);
        XML_Aut := '<' + Copy(buf, 1, Fim + Length('</protCTe>') - 1);
        XML_Aut := Geral.WideStringToSQLString(XML_Aut);
        if (pos(Id, XML_Aut) > 0) and (pos('<cStat>100</cStat>', XML_Aut) > 0) then
        begin
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'CTeArq', False, [
          'XML_Aut'], ['IDCtrl'], [XML_Aut], [IDCtrl], True);
        end;
      end;
      Ini := pos('<protCTe', buf);
    end;
    Texto.Free;
  end;
  Result := True;
end;

function TDmCTe_0000.AtualizaXML_No_BD_Can(const IDCtrl: Integer;
  const Id: String; var Dir, Aviso: String): Boolean;
var
  Arq, DirArq, Buf: String;
  XML_Can: String;
  Texto: TStringList;
  XML_All: PChar;
  Ini: Integer;
begin
  //Geral.MB_Aviso('"TDmCTe_0000.AtualizaXML_No_BD_Can()" n�o implementado!');
  ReopenCTeArq(IDCtrl);
  //
  //if QrCTeArqIDCtrl_Arq.Value = 0 then
  //begin
    Arq := QrCTeArqId.Value + CTE_EXT_EVE_RET_CAN_XML;
    Geral.VerificaDir(Dir, '\', 'CTe cancelada', True);
    DirArq := Dir + Arq;
    if not FileExists(DirArq) then
    begin
      Aviso := Aviso + DirArq + sLineBreak;
    end else begin
      buf := '';
      Texto := TStringList.Create;
      Texto.Clear;
      Texto.LoadFromFile(DirArq);
      XML_All := PChar(Texto.Text);
      buf          := Copy(XML_All, 1);
      //Ini          := pos('<retCancCTe', buf);
      Ini          := pos('<retEventoCTe', buf);
      XML_Can      := Copy(buf, Ini);
      XML_Can      := Geral.WideStringToSQLString(XML_Can);
      //
      //Geral.MensagemBox(XML_Can, 'Ativo', MB_OK+MB_ICONWARNING);
      if XML_Can <> '' then
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctearq', False, [
      'XML_Can'], ['IDCtrl'], [XML_Can ], [ IDCtrl ], True);

      Texto.Free;
    end;
  //end;
  Result := True;
end;

function TDmCTe_0000.AtualizaXML_No_BD_ConsultaCTe(const Chave, Id: String;
  const IDCtrl: Integer; var Dir, Aviso: String): Boolean;
var
  Arq, DirArq, Buf, Prot: String;
  XML_Sit: String;
  Texto: TStringList;
  XML_ConsultaCTe: PChar;
  //FatID, FatNum, Empresa,
  Ini, Fim: Integer;
begin
  Arq := Chave + CTE_EXT_SIT_XML;
  Geral.VerificaDir(Dir, '\', 'Situa��o de CTe', True);
  DirArq := Dir + Arq;
  if not FileExists(DirArq) then
  begin
    Aviso := Aviso + DirArq + sLineBreak;
  end else begin
    buf := '';
    Texto := TStringList.Create;
    Texto.Clear;
    Texto.LoadFromFile(DirArq);
    XML_ConsultaCTe := PChar(Texto.Text);
    buf := Copy(XML_ConsultaCTe, 1);
    Ini := pos('<protCTe', buf);
    if Ini > 0 then
    begin
      Prot := '';
      Geral.SeparaPrimeiraOcorrenciaDeTexto('<protCTe', buf, Prot, buf);
      if Prot <> '' then
      begin
        Fim := pos('</protCTe>', buf);
        XML_Sit := '<' + Copy(buf, 1, Fim + Length('</protCTe>') - 1);
        XML_Sit := Geral.WideStringToSQLString(XML_Sit);
        //
        // Autorizado
        if (pos(Id, XML_Sit) > 0) and (pos('<cStat>100</cStat>', XML_Sit) > 0) then
        begin
          QrArq.Close;
          QrArq.Params[00].AsInteger := IDCtrl;
          QrArq.Open;
          if QrArqXML_Aut.Value = '' then
          begin
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'CTeArq', False, [
            'XML_Aut'], ['IDCtrl'], [XML_Sit], [IDCtrl], True);
          end;
        end;
      end;
    end
    else
    begin
//<retCancCTe versao="2.00" xmlns="http://www.portalfiscal.inf.br/nfe"><infCanc Id="ID415100008891288"><tpAmb>1</tpAmb><verAplic>SVAN_4.00</verAplic><cStat>101</cStat><xMotivo>Cancelamento de NF-e homologado</xMotivo><cUF>15</cUF><chCTe>15100804333952000188550010000005458605003992</chCTe><dhRecbto>2010-09-01T15:17:44</dhRecbto><nProt>415100008891288</nProt></infCanc></retCancCTe>
//<retConsSitCTe versao="1.07" xmlns="http://www.portalfiscal.inf.br/nfe"><infProt Id="ID15100404333952000188550010000000019456724199"><tpAmb>1</tpAmb><verAplic>4.00</verAplic><cStat>101</cStat><xMotivo>Cancelamento de NF-e homologado</xMotivo><cUF>15</cUF><chCTe>15100404333952000188550010000000019456724199</chCTe><dhRecbto>2010-04-06T09:01:06</dhRecbto><nProt>415100002950345</nProt><digVal>FJMyliJj5bz1SF4co5msqKc6NhE=</digVal></infProt></retConsSitCTe>
      Ini := pos('<retConsSitCTe', buf);
      if Ini > 0 then
      begin
        Prot := '';
        Geral.SeparaPrimeiraOcorrenciaDeTexto('<retConsSitCTe', buf, Prot, buf);
        if Prot <> '' then
        begin
          Fim := pos('</retConsSitCTe>', buf);
          XML_Sit := '<' + Copy(buf, 1, Fim + Length('</retConsSitCTe>') - 1);
          XML_Sit := Geral.WideStringToSQLString(XML_Sit);
          //
          // Autorizado
          // neste caso o ID � a pr�pria chave
          if ((pos(Id, XML_Sit) > 0) or (pos(Chave, XML_Sit) > 0)) and (pos('<cStat>100</cStat>', XML_Sit) > 0) then
          begin
            QrArq.Close;
            QrArq.Params[00].AsInteger := IDCtrl;
            QrArq.Open;
            if QrArqXML_Can.Value = '' then
            begin
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'CTeArq', False, [
              'XML_Aut'], ['IDCtrl'], [XML_Sit], [IDCtrl], True);
            end;
          end;
          //
          // Cancelado
          // neste caso o ID � a pr�pria chave
          if ((pos(Id, XML_Sit) > 0) or (pos(Chave, XML_Sit) > 0)) and (pos('<cStat>101</cStat>', XML_Sit) > 0) then
          begin
            QrArq.Close;
            QrArq.Params[00].AsInteger := IDCtrl;
            QrArq.Open;
            if QrArqXML_Can.Value = '' then
            begin
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'CTeArq', False, [
              'XML_Can'], ['IDCtrl'], [XML_Sit], [IDCtrl], True);
            end;
          end;
        end;
      end;
    end;
    //
    Texto.Free;
  end;
  Result := True;
end;

function TDmCTe_0000.AtualizaXML_No_BD_CTe(const IDCtrl: Integer; var Dir,
  Aviso: String): Boolean;
var
  Arq, DirArq, Buf: String;
  XML_CTe: String;
  Texto: TStringList;
  XML_Assinado: PChar;
  FatID, FatNum, Empresa: Integer;
  //
  SQLType: TSQLType;
begin
  ReopenCTeArq(IDCtrl);
  //
  Arq := QrCTeArqId.Value + CTE_EXT_CTE_XML;
  Geral.VerificaDir(Dir, '\', 'CTe assinada', True);
  DirArq := Dir + Arq;
  if not FileExists(DirArq) then
  begin
    Aviso := Aviso + DirArq + sLineBreak;
  end else begin
    buf := '';
    Texto := TStringList.Create;
    Texto.Clear;
    Texto.LoadFromFile(DirArq);
    XML_Assinado := PChar(Texto.Text);
    buf          := Copy(XML_Assinado, 1);
    XML_CTe      := Geral.WideStringToSQLString(buf);
    //
    FatID        := QrCTeArqFatID.Value;
    FatNum       := QrCTeArqFatNum.Value;
    Empresa      := QrCTeArqEmpresa.Value;
    //
    if QrCTeArqIDCtrl_Arq.Value = 0 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM ctearq WHERE IDCtrl=:P0');
      Dmod.QrUpd.Params[00].AsInteger := IDCtrl;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM ctearq ');
      Dmod.QrUpd.SQL.Add('WHERE FatID=:P0 AND FatNum=:P1 AND Empresa=:P2');
      Dmod.QrUpd.Params[00].AsInteger := FatID;
      Dmod.QrUpd.Params[01].AsInteger := FatNum;
      Dmod.QrUpd.Params[02].AsInteger := Empresa;
      Dmod.QrUpd.ExecSQL;
      //
      SQLType := stIns;
    end else begin
      SQLType := stUpd;
    end;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctearq', False, [
    'IDCtrl', 'XML_CTe'], ['FatID', 'FatNum', 'Empresa'], [
     IDCtrl,   XML_CTe ], [ FatID,   FatNum,   Empresa ], True);
    Texto.Free;
  end;
  Result := True;
end;

function TDmCTe_0000.AtualizaXML_No_BD_Inu(const Codigo: Integer; var Dir,
  Aviso: String): Boolean;
var
  Arq, DirArq, Buf: String;
  Id, XML_Inu: String;
  Texto: TStringList;
  XML_Txt: PChar;
  //FatID, FatNum, Empresa: Integer;
begin
  MontaID_Inutilizacao(FormatFloat('00', QrCTeInucUF.Value),
  FormatFloat('00', QrCTeInuano.Value), QrCTeInuCNPJ.Value,
  FormatFloat('00', QrCTeInumodelo.Value),
  FormatFloat('0', QrCTeInuSerie.Value),
  FormatFloat('000000000', QrCTeInunCTIni.Value),
  FormatFloat('000000000', QrCTeInunCTFim.Value), Id);
  //
  Arq := Id + CTE_EXT_INU_XML;
  Geral.VerificaDir(Dir, '\', 'CTe inutilizada', True);
  DirArq := Dir + Arq;
  if not FileExists(DirArq) then
  begin
    Aviso := Aviso + DirArq + sLineBreak;
  end else begin
    buf := '';
    Texto := TStringList.Create;
    Texto.Clear;
    Texto.LoadFromFile(DirArq);
    XML_Txt      := PChar(Texto.Text);
    buf          := Copy(XML_Txt, 1);
    XML_Inu      := Geral.WideStringToSQLString(buf);
    //
    if XML_Inu <> '' then

    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cteinut', False, [
    'XML_Inu'], ['Codigo'], [XML_Inu ], [Codigo], True);
    Texto.Free;
  end;
  Result := true;
end;

function TDmCTe_0000.AtualizaXML_No_BD_Tudo(InfoTermino: Boolean): Boolean;
var
  Aviso, CNPJ_CPF, Dir: String;
begin
  Screen.Cursor := crHourGlass;
  //
  if VAR_LIB_EMPRESA_SEL = 0 then
  begin
    Geral.MB_Aviso('Empresa logada n�o definida!');
    Result := False;
    Exit;
  end;
  //
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  if not ReopenOpcoesCTe(QrOpcoesCTe, VAR_LIB_EMPRESA_SEL, True) then Exit;
  CNPJ_CPF := QrOpcoesCTeCNPJ_CPF.Value;
  if CNPJ_CPF = '' then
  begin
    Geral.MB_Aviso('CNPJ ou CPF da Empresa logada n�o definido!');
    Result := False;
    Exit;
  end;
  Aviso := '';

  //

{ TODO -cCTe : ????TODO  }
  // Permitir cliente editar nfearq e dizer que nao quer mais que procure o XML
  Dir := QrOpcoesCTeDirCteAss.Value;
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtoArq, Dmod.MyDB, [
  'SELECT caba.IDCtrl ',
  'FROM ctecaba caba ',
  'LEFT JOIN ctearq arq ON arq.IDCtrl=caba.IDCtrl ',
  'WHERE arq.IDCtrl IS NULL ',
  'AND Id <> "" ',
  'AND emit_CNPJ="' + CNPJ_CPF + '" ',
(*
  'AND ( ',
  '  emit_CNPJ="' + CNPJ_CPF + '" ',
  '  OR ',
  '  emit_CPF="' + CNPJ_CPF + '" ',
  ') ',
*)
  '']);
  //
  QrAtoArq.First;
  while not QrAtoArq.Eof do
  begin
    AtualizaXML_No_BD_CTe(QrAtoArqIDCtrl.Value, Dir, Aviso);
    //
    QrAtoArq.Next;
  end;

  //

  Dir := QrOpcoesCTeDirCteProRec.Value;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeAut, Dmod.MyDB, [
  'SELECT caba.IDCtrl, caba.LoteEnv, caba.Id' ,
  'FROM ctecaba caba' ,
  'LEFT JOIN ctearq arq ON arq.IDCtrl=caba.IDCtrl' ,
  'WHERE arq.IDCtrl IS NOT NULL' ,
  'AND caba.infProt_cStat=100' ,
  'AND arq.XML_Aut IS NULL' ,
  'AND emit_CNPJ="' + CNPJ_CPF + '" ',
  (*'AND (' ,
  '  emit_CNPJ=:P0' ,
  '  OR' ,
  '  emit_CPF=:P1' ,
  ')' ,*)
  '']);
  //
  QrCTeAut.First;
  while not QrCTeAut.Eof do
  begin
    AtualizaXML_No_BD_Aut(QrCTeAutIDCtrl.Value, QrCTeAutLoteEnv.Value,
      QrCTeAutId.Value, Dir, Aviso);
    //
    QrCTeAut.Next;
  end;

  //

  Dir := QrOpcoesCTeDirCTeEveRetCan.Value;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeCan, Dmod.MyDB, [
  'SELECT caba.IDCtrl, caba.Id' ,
  'FROM ctecaba caba' ,
  'LEFT JOIN ctearq arq ON arq.IDCtrl=caba.IDCtrl' ,
  'WHERE arq.IDCtrl IS NOT NULL' ,
  'AND caba.infCanc_cStat=101' ,
  'AND emit_CNPJ="' + CNPJ_CPF + '" ',
(*
  'AND (' ,
  '  emit_CNPJ=:P0' ,
  '  OR' ,
  '  emit_CPF=:P1' ,
  ')' ,
*)
  '']);
  //
  QrCTeCan.First;
  while not QrCTeCan.Eof do
  begin
    AtualizaXML_No_BD_Can(QrCTeCanIDCtrl.Value, QrCTeCanId.Value, Dir, Aviso);
    //
    QrCTeCan.Next;
  end;

  //

  Dir := QrOpcoesCTeDirCteInu.Value;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeInu, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteinut ',
  'WHERE XML_Inu IS NULL ',
  'AND cStat=102 ',
  '']);
  //
  QrCTeInu.First;
  while not QrCTeInu.Eof do
  begin
    AtualizaXML_No_BD_Inu(QrCTeInuCodigo.Value, Dir, Aviso);
    //
    QrCTeInu.Next;
  end;

  //
  if Aviso <> '' then Geral.MB_Aviso(
  'Os arquivos abaixo n�o foram localizados:' + sLineBreak + Aviso);

  if InfoTermino then
    Geral.MB_Aviso('Verifica��o finalizada');

  Screen.Cursor := crDefault;
  Result := true;
end;

function TDmCTe_0000.CancelaFaturamento(ChaveCTe: String): Boolean;

  procedure AtualizaLct(Lancto, LctSit: Integer; Tabela: String);
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False,
      ['Debito', 'Credito', 'Sit'], ['Controle'], [0, 0, LctSit], [Lancto], True);
  end;

const
  Baixa   = 0;
  ValiStq = 101; //Cancelado
  LctSit  = 4; //Cancelado
var
  IDCtrl, Lancto, FatId, FatNum, Filial: Integer;
  Tabela, TabLctA, TabLctB, TabLctD: String;
begin
  Result := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT cte.FatID, cte.FatNum, cli.CodFilial Filial ',
    'FROM ctecaba cte ',
    'LEFT JOIN enticliint cli ON cli.CodEnti = cte.Empresa ',
    'WHERE cte.Id="' + ChaveCTe + '"',
    '']);
  if QrLoc.RecordCount > 0 then
  begin
    FatId  := QrLoc.FieldByName('FatID').AsInteger;
    FatNum := QrLoc.FieldByName('FatNum').AsInteger;
    Filial := QrLoc.FieldByName('Filial').AsInteger;
  end else
    Exit;
  //
  try
    QrLoc.DisableControls;
    Screen.Cursor := crHourGlass;
    //
{
    //Ini => Cancela estoque
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT IDCtrl, "stqmovitsa" Tabela ',
      'FROM stqmovitsa ',
      'WHERE Tipo=1 ',
      'AND OriCodi=' + Geral.FF0(FatNum),
      ' UNION ',
      'SELECT IDCtrl, "stqmovitsb" Tabela ',
      'FROM stqmovitsb ',
      'WHERE Tipo=1 ',
      'AND OriCodi=' + Geral.FF0(FatNum),
      '']);
    if QrLoc.RecordCount > 0 then
    begin
      QrLoc.First;
      while not QrLoc.Eof do
      begin
        IDCtrl := QrLoc.FieldByName('IDCtrl').AsInteger;
        Tabela := LowerCase(QrLoc.FieldByName('Tabela').AsString);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False,
          ['Baixa', 'ValiStq'], ['IDCtrl'], [Baixa, ValiStq], [IDCtrl], False);
        //
        QrLoc.Next;
      end;
    end;
    //Fim => Cancela estoque
}
    //Ini => Cancela financeiro
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT CAST(Lancto AS SIGNED) Lancto',
      'FROM cteit3dup ',
      'WHERE FatID=' + Geral.FF0(FatId),
      'AND FatNum=' + Geral.FF0(FatNum),
      '']);
    if QrLoc.RecordCount > 0 then
    begin
      QrLoc.First;
      //
      while not QrLoc.Eof do
      begin
        Lancto := QrLoc.FieldByName('Lancto').AsLargeInt;
        //
        if Lancto <> 0 then
        begin
          {$IfDef DEFINE_VARLCT}
            //Financeiro novo
            try
              QrLoc2.DisableControls;
              //
              TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
              TabLctB := DModG.NomeTab(TMeuDB, ntLct, False, ttB, Filial);
              TabLctD := DModG.NomeTab(TMeuDB, ntLct, False, ttD, Filial);
              //
              UnDmkDAC_PF.AbreMySQLQuery0(QrLoc2, Dmod.MyDB, [
                'SELECT Controle ',
                'FROM ' + TabLctA,
                'WHERE Controle = ' + Geral.FF0(Lancto),
                '']);
              if QrLoc2.RecordCount > 0 then
                AtualizaLct(Lancto, LctSit, TabLctA);
              //
              UnDmkDAC_PF.AbreMySQLQuery0(QrLoc2, Dmod.MyDB, [
                'SELECT Controle ',
                'FROM ' + TabLctB,
                'WHERE Controle = ' + Geral.FF0(Lancto),
                '']);
              if QrLoc2.RecordCount > 0 then
                AtualizaLct(Lancto, LctSit, TabLctB);
              ///
              UnDmkDAC_PF.AbreMySQLQuery0(QrLoc2, Dmod.MyDB, [
                'SELECT Controle ',
                'FROM ' + TabLctD,
                'WHERE Controle = ' + Geral.FF0(Lancto),
                '']);
              if QrLoc2.RecordCount > 0 then
                AtualizaLct(Lancto, LctSit, TabLctD);
            finally
              QrLoc2.EnableControls;
            end;
          {$Else}
            //Financeiro antigo
            AtualizaLct(Lancto, LctSit, VAR_LCT);
          {$EndIf}
        end;
        QrLoc.Next;
      end;
    end;
    //Fim => Cancela financeiro
  finally
    Screen.Cursor := crDefault;
    QrLoc.EnableControls;
    Result := True;
  end;
end;

procedure TDmCTe_0000.ColoreSitConfCTeEmDBGrid(const DBGrid: TDBGrid;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState;
  const SitConf: Integer);
var
  Txt, Bak: TColor;
begin
  if (Column.FieldName = 'NO_cSitConf') then
  begin
    //Txt := clWindow;
    //Bak := clBlack;
    case SitConf of
      -1:  begin Txt := clYellow ; Bak := clBlack  end;
      0:   begin Txt := clFuchsia; Bak := clWindow end;
      1:   begin Txt := clBlue   ; Bak := clWindow end;
      2:   begin Txt := clBlack  ; Bak := clRed;   end;
      3:   begin Txt := clBlack  ; Bak := clWindow;end;
      4:   begin Txt := $000080FF; Bak := clWindow end; // Laranja intenso
      else begin Txt := clRed    ; Bak := clWindow end;
    end;
    MyObjects.DesenhaTextoEmDBGrid(DBGrid, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TDmCTe_0000.ColoreStatusCTeEmDBGrid(const DBGrid: TDBGrid;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState;
  const Status: Integer);
var
  Txt, Bak: TColor;
begin
  if (Column.FieldName = 'cStat') then
  begin
    Txt := clWindow;
    Bak := clBlack;
    case Status of
      100:      begin Txt := clBlue   ; Bak := clWindow end;
      101..102: begin Txt := clFuchsia; Bak := clWindow end;
      103..199: begin Txt := clBlack  ; Bak := clWindow end;
      201..299: begin Txt := clRed    ; Bak := clWindow end;
      301..399: begin Txt := clWindow ; Bak := clRed;   end;
      401..999: begin Txt := clRed    ; Bak := clWindow end;
    end;
    MyObjects.DesenhaTextoEmDBGrid(DBGrid, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end
  else
  if (Column.FieldName = 'NO_cSitCTe') then
  begin
    //Txt := clWindow;
    //Bak := clBlack;
    case Status of
      //-1,0:begin Txt := clBlack  ; Bak := clWindow end;
      1:   begin Txt := clBlue   ; Bak := clWindow end;
      2:   begin Txt := clFuchsia; Bak := clWindow end;
      3:   begin Txt := clWindow ; Bak := clRed;   end;
      else begin Txt := clRed    ; Bak := clWindow end;
    end;
    MyObjects.DesenhaTextoEmDBGrid(DBGrid, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TDmCTe_0000.DataModuleCreate(Sender: TObject);
begin
  FLastVerCTeLayI := 0;
end;

function TDmCTe_0000.DefDomI(Grupo, Codigo, Valor: Integer): Integer;
  function Txt: String;
  begin
    Result := Geral.FFN(Valor, QrCTeLayITamMin.Value);
  end;
var
  Dominio, Msg, Campo: String;
begin
  Result := Valor;
  if QrCTeLayI.Locate('Grupo;Codigo', VarArrayOf([Grupo, Codigo]), []) then
  begin
    if (QrCTeLayIOcorMin.Value > 0) then
    begin
      Dominio := Trim(QrCTeLayIDominio.Value);
      if Dominio <> '' then
      begin
        Campo := QrCTeLayICampo.Value;
        //if
        CTe_PF.VDom(Grupo, Codigo, Campo, Txt, Dominio, FMsg)// = False then
        (*begin
          FMsg := FMsg + 'CT-e item #' + Geral.FF0(ID) + ' campo "' +
           + '": ' + Msg;
        end;
        *)
      end;
    end;
  end else FMsg := FMsg + 'N�o foi poss�vel definir o item #' +
  Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo) + ' pelo layout da CT-e! [1]';
end;

function TDmCTe_0000.DefDomX(Grupo, Codigo: Integer; Valor: String): String;
var
  Dominio, Msg, Campo: String;
begin
  Result := Valor;
  if QrCTeLayI.Locate('Grupo;Codigo', VarArrayOf([Grupo, Codigo]), []) then
  begin
    if (QrCTeLayIOcorMin.Value > 0) then
    begin
      Dominio := Trim(QrCTeLayIDominio.Value);
      if Dominio <> '' then
      begin
        Campo := QrCTeLayICampo.Value;
        CTe_PF.VDom(Grupo, Codigo, Campo, Valor, Dominio, FMsg);
      end;
    end;
  end else FMsg := FMsg + 'N�o foi poss�vel definir o item #' +
  Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo) + ' pelo layout da CT-e! [2]';
(*
var
  Str: String;
begin
  Result := Valor;
  if QrCTeLayI.Locate('Codigo', ID, [loCaseInsensitive]) then
  begin
    Str := Trim(Valor);
    if (QrCTeLayIOcorMin.Value > 0) then
    begin
      if (Str = '') then
      begin
        //if FMsg = '' then
          FMsg := FMsg + MensagemDeID_CTe(ID, Valor, 'Texto n�o definido!', 6);
      end else
      if (QrCTeLayITamMin.Value < Length(Valor)) then
      begin
        //if FMsg = '' then
          FMsg := FMsg + MensagemDeID_CTe(ID, Valor, 'Tamanho do abaixo do m�nimo!', 7);
      end else
      if (QrCTeLayITamMax.Value > Length(Valor)) then
      begin
        //if FMsg = '' then
          FMsg := FMsg + MensagemDeID_CTe(ID, Valor, 'Tamanho do texto acima do m�ximo!', 8);
      end;
    end;
  end else FMsg := FMsg + 'N�o foi poss�vel definir o item #' +
  Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo) + ' pelo layout da CT-e! [?]';
*)
end;

function TDmCTe_0000.DefI(Grupo, Codigo: Integer; ValMin, ValMax,
  Numero: Integer): Integer;
  function Txt: String;
  begin
    Result := IntToStr(Numero);
  end;
begin
  Result := Numero;
  if QrCTeLayI.Locate('Grupo;Codigo', VarArrayOf([Grupo, Codigo]), []) then
  begin
    if (QrCTeLayIOcorMin.Value > 0) and (Numero < ValMin) then
    begin
      //if FMsg = '' then
        FMsg := FMsg + MensagemDeID_CTe(Grupo, Codigo, Txt, 'Tamanho do integer abaixo do m�nimo!', 4);
    end else
    if (QrCTeLayIOcorMin.Value > 0) and (Numero > ValMax) then
    begin
      //if FMsg = '' then
        FMsg := FMsg + MensagemDeID_CTe(Grupo, Codigo, Txt, 'Tamanho do integer acima do m�ximo!', 5);
    end;
  end else FMsg := FMsg + 'N�o foi poss�vel definir o item #' +
  Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo) + ' pelo layout da CT-e! [3]';
end;

function TDmCTe_0000.DefMsg(Grupo, Codigo: Integer; MsgExtra, Valor: String): Boolean;
(*
  procedure AlteraDest;
  var
    Dest: Integer;
  begin
    //
    if QrDest.State <> dsInactive then
      Dest := QrDestCodigo.Value
    else
      Dest := 0;
    //
    if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
    begin
      if Dest <> 0 then
        FmEntidade2.LocCod(Dest, Dest);
      FmEntidade2.ShowModal;
      FmEntidade2.Destroy;
    end;
    if QrDest.State <> dsInactive then
    begin
      QrDest.Close;
      QrDest.Open;
    end;
  end;
*)
var
  Msg1, Msg2: String;
  Cod: String;
begin
  Msg1 := MensagemDeID_CTe(Grupo, Codigo, Valor, '[Extra] ' + MsgExtra, 6);
  //if FMsg = '' then FMsg := Msg1;
  FMsg := FMsg + Msg1;

  //  Redirecionamento
//  if Uppercase(ID[1]) = 'E' then // Emitente
(* Parei aqui!
  if ID in ([?,?,?,?]) then // Emitente
  begin
    Cod := IntToStr(QrDestCodigo.Value);
    Msg2 := 'Desejo alterar agora o cadastro da entidade n� ' + Cod;
    dmkPF.MessageDlgCheck(Msg1, mtConfirmation, [mbOK], 0, mrOK,
      True, True, Msg2, @AlteraDest);
    FMsg := 'Exit';
  end;
*)
  //
  Result := True;
end;

function TDmCTe_0000.DefX(Grupo, Codigo: Integer; Valor: String): String;
var
  t: Integer;
  OK: Boolean;
begin
  //Result := ValidaTexto_XML(Texto, Codigo, ID);
  Result := Valor;
  if QrCTeLayI.Locate('Grupo;Codigo', VarArrayOf([Grupo, Codigo]), []) then
  begin
    t := Length(Result);
    OK := False;
    if (QrCTeLayIOcorMin.Value > 0) or (t > 0) then
    begin
      if QrCTeLayITamVar.Value <> '' then
      begin
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('SELECT ' + FormatFloat('0', T) + ' in (' +
        QrCTeLayITamVar.Value + ') Tem');
        UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB, 'TDmCTe_0000.DefX()');
        OK := Dmod.QrAux.FieldByName('Tem').AsInteger = 1;
      end;
      if not OK then
      begin
        if t < QrCTeLayITamMin.Value then
          DefMsg(Grupo, Codigo, 'Tamanho do texto abaixo do m�nimo!', Result);
        if t > QrCTeLayITamMax.Value then
        begin
          if QrCTeLayITamMin.Value = QrCTeLayITamMax.Value then
            DefMsg(Grupo, Codigo, 'Tamanho do texto difere do esperado!', Result)
          else
            Result := Copy(Result, 1, QrCTeLayITamMax.Value);
        end;
      end;
    end;
  end else DefMsg(Grupo, Codigo, 'Item n�o localizado na tabela "CTeLayI"', Valor);
end;

function TDmCTe_0000.ERf(Grupo, Codigo: Integer; Valor: Double; Casas: Integer): Double;
begin
  Result := Valor;
  ERx(Grupo, Codigo, Geral.FFT_Dot(Valor, Casas, siNegativo));
end;

function TDmCTe_0000.ERi(Grupo, Codigo, Valor: Integer): Integer;
begin
  Result := Valor;
  ERx(Grupo, Codigo, Geral.FF0(Valor));
end;

function TDmCTe_0000.ERi64(Grupo, Codigo: Integer; Valor: Int64): Int64;
begin
  Result := Valor;
  ERx(Grupo, Codigo, IntToStr(Valor));
end;

function TDmCTe_0000.ERx(Grupo, Codigo: Integer; Valor: String): String;
var
  Campo, ExpressaoRegular: String;
begin
  Result := Valor;
  if QrCTeLayI.Locate('Grupo;Codigo', VarArrayOf([Grupo, Codigo]), []) then
  begin
    if (QrCTeLayIOcorMin.Value > 0) then
    begin
      ExpressaoRegular := Trim(QrCTeLayIExpReg.Value);
      if ExpressaoRegular <> '' then
      begin
        Campo := QrCTeLayICampo.Value;
        CTe_PF.VRegExp(Grupo, Codigo, Campo, Valor, ExpressaoRegular, FMsg);
      end;
    end;
  end else FMsg := FMsg + 'N�o foi poss�vel definir o item #' +
  Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo) + ' pelo layout da CT-e! [4]';
end;

function TDmCTe_0000.EventoObtemCtrl(EventoLote, tpEvento, nSeqEvento: Integer;
  chCTe: String): Integer;
var
  Msg: String;
begin
  Result := 0;
  Msg := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrEveIts, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM cteevercab ',
  'WHERE EventoLote=' + Geral.FF0(EventoLote),
  'AND tpEvento=' + Geral.FF0(tpEvento),
  'AND nSeqEvento=' + Geral.FF0(nSeqEvento),
  'AND chCTe="' + chCTe + '"',
  '']);
  //
  case QrEveIts.RecordCount of
    0: Msg := 'Item de evento n�o localizado!';
    1: Result := QrEveItsControle.Value;
    else Msg := 'Item de evento duplicado!';
  end;
  if Msg <> '' then
    Geral.MB_Erro(Msg);
end;

procedure TDmCTe_0000.EventoObtemSub(const Controle: Integer;
  const IdLote: String; var SubCtrl: Integer; var SQLType: TSQLType);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEveSub, Dmod.MyDB, [
  'SELECT SubCtrl ',
  'FROM cteeverret ',
  'WHERE Controle=' + Geral.FF0(Controle),
  'AND ret_Id="' + IdLote + '"',
  '']);
  //
  if QrEveSub.RecordCount = 0 then
  begin
    SQLType := stIns;
    SubCtrl := DModG.BuscaProximoCodigoInt('ctectrl', 'cteeverret', '', 0)
  end else begin
    SQLType := stUpd;
    SubCtrl := QrEveSubSubCtrl.Value;
  end;
end;

function TDmCTe_0000.Evento_Obtem_nSeqEvento(FatID, FatNum, Empresa,
  tpEvento: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEveTp, Dmod.MyDB, [
  'SELECT nSeqEvento ',
  'FROM cteevercab ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND tpEvento=' + Geral.FF0(tpEvento),
  'ORDER BY nSeqEvento DESC ',
  '']);
  if QrEveTp.RecordCount = 0 then
    Result := 1
  else
    Result := QrEveTpnSeqEvento.Value + 1;
end;

function TDmCTe_0000.ExcluiDadosTabelasCTe(FatID, FatNum: Integer): Boolean;
  function SQLDeleteTabela(Tabela:String): String;
  begin
    Result := 'DELETE FROM ' + Lowercase(Tabela) + ' WHERE FatID=' +
    Geral.FF0(FatID) + ' AND FatNum=' + Geral.FF0(FatNum) + ';';
  end;
begin
  Result := UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  SQLDeleteTabela('ctecaba'),
  //
  SQLDeleteTabela('ctecab1exped'),
  SQLDeleteTabela('ctecab1receb'),
  SQLDeleteTabela('ctecab1receb'),
  SQLDeleteTabela('ctecab2locent'),
  SQLDeleteTabela('ctecab2toma04'),
  //
  SQLDeleteTabela('cteit1autxml'),
  SQLDeleteTabela('cteit2comp'),
  //SQLDeleteTabela('cteit2infctesub'),
  SQLDeleteTabela('cteit2obscont'),
  SQLDeleteTabela('cteit2obsfisco'),
  SQLDeleteTabela('cteit2infdoc'),
  SQLDeleteTabela('cteit2peri'),
  SQLDeleteTabela('cteit2seg'),
  SQLDeleteTabela('cteit3dup'),
  SQLDeleteTabela('cteit3infq'),
  SQLDeleteTabela('ctemorodocab'),
  SQLDeleteTabela('ctemorodolacr'),
  SQLDeleteTabela('ctemorodomoto'),
  SQLDeleteTabela('ctemorodoveic'),
  SQLDeleteTabela('ctemorodovped'),
  SQLDeleteTabela('ctemorodoocc'),
  SQLDeleteTabela('ctemorodoprop'),
  '']);
end;

function TDmCTe_0000.FormataLoteCTe(Lote: Integer): String;
begin
  Result :=  FormatFloat('000000000', Lote);
end;

function TDmCTe_0000.InsereDadosCTeCab(const FatID, FatNum, Empresa,
  IDCtrl_Atual, VersaoCTe, SerieCTe, NumeroCTe: Integer;
  Recria: Boolean; DataFat: TDateTime; HoraFat: TTime; var IDCtrl: Integer): Boolean;
const
  vDesc = 0.00;
var
  Id, ide_natOp, ide_dEmi, ide_hEmi, ide_verProc, ide_refCTE, ide_xMunEnv,
  ide_UFEnv, ide_xMunIni, ide_UFIni, ide_xMunFim, ide_UFFim, ide_xDetRetira,
  toma04_CNPJ, toma04_CPF, toma04_IE, toma04_xNome, toma04_xFant, toma04_fone,
  ide_dhCont, ide_xJust, compl_xCaracAd, compl_xCaracSer, compl_xEmi,
  entrega_dProg, entrega_dIni, entrega_dFim, entrega_hProg, entrega_hIni,
  entrega_hFim, ide_origCalc, ide_destCalc, ide_xObs, emit_CNPJ, emit_IE,
  emit_xNome, emit_xFant, emit_xLgr, emit_nro, emit_xCpl, emit_xBairro,
  emit_xMun, emit_UF, emit_fone, rem_CNPJ, rem_CPF, rem_IE, rem_xNome,
  rem_xFant, rem_fone, rem_xLgr, rem_nro, rem_xCpl, rem_xBairro, rem_xMun,
  rem_UF, rem_xPais, rem_email, dest_CNPJ, dest_CPF, dest_IE, dest_xNome,
  dest_fone, dest_ISUF, dest_xLgr, dest_nro, dest_xCpl, dest_xBairro,
  dest_xMun, dest_UF, dest_xPais, dest_email, imp_infAdFisco, infCarga_proPred,
  infCarga_xOutCat, cobr_Fat_nFat, infCTeComp_chave, infCTeAnu_chCte,
  infCTeAnu_dEmi, (*infProt_Id, infProt_verAplic, infProt_dhRecbto, infProt_nProt,
  infProt_digVal, infProt_xMotivo, infCanc_Id, infCanc_verAplic,
  infCanc_dhRecbto, infCanc_nProt, infCanc_digVal, infCanc_xMotivo,
  infCanc_xJust, eveEPEC_Id, eveEPEC_verAplic, eveEPEC_xMotivo, eveEPEC_chCTe,
  eveEPEC_xEvento, eveEPEC_CNPJDest, eveEPEC_CPFDest, eveEPEC_emailDest,
  eveEPEC_dhRegEvento, eveEPEC_nProt, infCCe_verAplic, infCCe_CNPJ, infCCe_CPF,
  infCCe_chCTe, infCCe_dhEvento, infCCe_x Correcao, infCCe_dhRegEvento,
  infCCe_nProt,*) verTotTrib: String;
  LoteEnv, ide_cUF, ide_cCT, ide_CFOP, ide_forPag, ide_mod, ide_serie,
  ide_nCT, ide_tpImp, ide_tpEmis, ide_cDV, ide_tpAmb, ide_tpCTe, ide_procEmi,
  ide_cMunEnv, ide_Modal, ide_tpServ, ide_cMunIni, ide_cMunFim, ide_Retira,
  toma99_toma, toma04_toma, entrega_tpPer, entrega_tpHor, emit_cMun, emit_CEP,
  rem_cMun, rem_CEP, rem_cPais, dest_cMun, dest_CEP, dest_cPais, ICMS_CST,
  ICMSSN_indSN, (*infProt_tpAmb, infProt_cStat, infCanc_tpAmb, infCanc_cStat,
  infCanc_cJust,*) FisRegCad, CartEmiss, TabelaPrc, CondicaoPg,
  CodInfoEmite, CodInfoTomad, CodInfoRemet,
  CodInfoExped, CodInfoReceb, CodInfoDesti, CodInfoToma4, CriAForca,
  (*eveEPEC_tpAmb, eveEPEC_cOrgao, eveEPEC_cStat, eveEPEC_tpEvento, eveEPEC_nSeqEvento,
  cSitCTe, cSitConf, infCCe_cOrgao, infCCe_tpAmb, infCCe_tpEvento,
  infCCe_nSeqEvento, infCCe_cStat, infCCe_nCondUso,*) iTotTrib_Fonte,
  iTotTrib_Tabela, iTotTrib_Ex, tpAliqTotTrib, CTeMyCST: Integer;
  iTotTrib_Codigo: Int64;
  versao, ide_dhEmiTZD, ide_dhContTZD, vPrest_vTPrest, vPrest_vRec, ICMS_vBC,
  ICMS_pICMS, ICMS_vICMS, ICMS_pRedBC, ICMS_vBCSTRet, ICMS_vICMSSTRet,
  ICMS_pICMSSTRet, ICMS_vCred, ICMS_pRedBCOutraUF, ICMS_vBCOutraUF,
  ICMS_pICMSOutraUF, ICMS_vICMSOutraUF, imp_vTotTrib, infCarga_vCarga,
  cobr_Fat_vOrig, cobr_Fat_vDesc, cobr_Fat_vLiq, (*protCTe_versao,
  infProt_dhRecbtoTZD, retCancCTe_versao, infCanc_dhRecbtoTZD, eveEPEC_TZD_UTC,
  infCCe_dhEventoTZD, infCCe_verEvento, infCCe_dhRegEventoTZD,*)
  vBasTrib, pTotTrib: Double;
  SQLType: TSQLType;
  //Dest: Integer;
  //
  // Toma
  enderToma_xLgr, enderToma_nro, enderToma_xCpl, enderToma_xBairro,
  enderToma_xMun, enderToma_UF, enderToma_xPais, enderToma_email: String;
  Controle, enderToma_cMun, enderToma_CEP, enderToma_cPais: Integer;
  //
  ValCalc: Double;
  Filial, UF: Integer;
  TabLctA: String;
  ChaveDeAcesso, DV(*, Ver_Txt*): String;
  // cteit3infq
  cUnid: Integer;
  tpMed: String;
  qCarga: Double;
  //
  // cteit3infdoc
  InfNFe_chave, InfNFe_dPrev, InfNF_nRoma, InfNF_nPed, InfNF_serie, InfNF_nDoc,
  InfNF_dEmi, InfNF_dPrev, InfOutros_descOutros, InfOutros_nDoc, InfOutros_dEmi,
  InfOutros_dPrev: String;
  MyTpDocInf, InfNFe_PIN, InfNF_mod, InfNF_nCFOP, InfNF_PIN,
  InfOutros_tpDoc: Integer;
  InfNF_vBC, InfNF_vICMS, InfNF_vBCST, InfNF_vST, InfNF_vProd, InfNF_vNF,
  InfNF_nPeso, InfOutros_vDocFisc: Double;
  // cteit2obscont
  xCampo, xTexto: String;
  // cteit2peri
  nONU, xNomeAE, xClaRisco, grEmb, qTotProd, qVolTipo, pontoFulgor: String;
  // cteit3dup
  nDup, dVenc: String;
  vDup: Double;
  Lancto: Int64;
  //infctesub
  infCteSub_chCte, infCteSub_refNFe, infCteSub_CNPJ, infCteSub_CPF,
  infCteSub_mod, infCteSub_dEmi, infCteSub_refCTe, infCteSub_refCTeAnu: String;
  infCteSub_ContribICMS, infCteSub_TipoDoc, infCteSub_serie, infCteSub_subserie,
  infCteSub_nro: Integer;
  infCteSub_valor: Double;
  // autxml
  CNPJ, CPF: String;
  AddForma, AddIDCad, Tipo: Integer;
  // seg
  xSeg, nApol, nAver: String;
  respSeg: Integer;
  vCarga: Double;
  // ctemorodocab
  RNTRC, dPrev: String;
  lota: Integer;
  CIOT: Int64;
  // rodolacr
  nLacre: String;
  // rodomoto
  xNome(*, CPF*): String;
  // rodoveic
  RENAVAM, placa, VeicTpProp(*, UF*): String;
  // rodoprop
  (*CPF, CNPJ, xNome, RNTRC*) IE, UFx: String;
  Conta, PropTpProp, cInt, tara, capKG, capM3, tpVeic, tpRod, tpCar: Integer;
  // rodovped
  CNPJForn, nCompra, CNPJPg: String;
  vValePed: Double;
  // exped
  fone, xLgr, nro, xCpl, xBairro, xMun, xPais, email: String;
  cMun, CEP, cPais: Integer;
  // Comp
  vComp: Double;
begin
  Result := False;
  if FatID <> VAR_FATID_5001 then
  begin
    Geral.MB_Aviso('"FatID" ' + Geral.FF0(FatID) +
    ' n�o implementado em adi��o de dados de CT-e nas tabelas!');
    Exit;
  end;
  ReopenCTeLayI();
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatcab ',
  'WHERE Codigo=' + Geral.FF0(FatNum),
  '']);
(*
  //Regra fiscal
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtRegFisC, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtregfisc ',
  'WHERE Codigo=' + Geral.FF0(QrFrtFatCabFrtRegFisC.Value),
  '']);
*)
(*
  //Dados anteriores
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeCabA, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctecaba ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  //Geral.MB_SQL(nil, QrCTeCabA);
  IDCtrl := QrCTeCabAIDCtrl.Value;
*)
  FMsg := '';
  //
  SQLType        := stIns;
  //FatID          := ;
  //FatNum         := ;
  if IDCtrl_Atual = 0 then
    IDCtrl := UMyMod.Busca_IDCtrl_CTe(stIns, IDCtrl)
  else
    IDCtrl := IDCtrl_Atual;
  //
  LoteEnv        := 0;

  //
  //
  //Dest := QrFrtFatCabDestinatario.Value;
  //ReopenEntidadeCTe(Dest, QrDest);
  //ReopenEntidadeCTe(QrFrtFatCabRemetente.Value, QrReme);
  if not ReopenOpcoesCTe(QrOpcoesCTe, Empresa, True) then Exit;
(*
  ReopenEntidadeCTe(Cliente, Qr???);
  ReopenEntidadeCTe(Cliente, Qr???);
  ReopenEntidadeCTe(Cliente, Qr???);
  ReopenEntidadeCTe(Cliente, Qr???);
  ReopenEntidadeCTe(Cliente, Qr???);
*)
  //if QrDestCodigo.Value <> Dest then
  //  FMsg := FMsg + 'Destinat�rio n�o localizado!';
  //
  ReopenEmpresa(Empresa);
  if QrEmpresaCodigo.Value <> Empresa then
    FMsg := FMsg + 'Empresa n�o localizada!';
  // S� simples federal! - Parei aqui
  if QrFilialSimplesFed.Value = 0 then
    Geral.MB_Aviso('C�lculo de impostos em fase de implementa��o!' + sLineBreak +
    'Verifique a exatid�o dos valores e comunique � DERMATEK qualquer erro!');
  //
  if Empresa = 0 then
    FMsg := FMsg + 'Empresa inv�lida!';
  //
  //
  UF := Geral.IMV(QrEmpresaDTB_UF.Value);
  //Ver_Txt := Geral.FFT(QrOpcoesCTeCTeversao.Value, 2, siPositivo);


(*
  CTe_PF.MontaChaveDeAcesso(UF, Emissao, QrEntiCTeCNPJ.Value,
  QrOpcoesCTeCTeide_mod.Value, SerieCTe, NumeroCTe, QrCTeCabAide_cCT.Value,
  DV, ChaveDeAcesso, QrCTeCabAide_cCT.Value, ide_tpEmis: Integer;
  versao: Double): Boolean;
*)
// {IfNDef _tmp_EhLgistic}
{ TODO 5 -cCTe : **TpEmis** > colocar no FrtFatCab! }
  CTe_PF.MontaChaveDeAcesso((*ide_cUF*)UF,  DataFat,
    QrEmpresaCNPJ.Value, (*ide_mod*)QrOpcoesCTeCTeide_mod.Value,
    (*ide_Serie*)SerieCTe, NumeroCTe, ide_cCT, DV, ChaveDeAcesso,
    QrCTeCabAide_cCT.Value, QrOpcoesCTeCTetpEmis.Value,
    QrOpcoesCTeCTeversao.Value);
  //if Dest = 0 then
  //  FMsg := FMsg + 'Destinat�rio inv�lido!';
  //
  // Cabecalho InfCte => (*1*)
  versao := ERf(0, 2, QrOpcoesCTeCTeversao.Value, 2);

  (*3*)Id             := ChaveDeAcesso;
  ERx(0, 3, 'CTe' + Id);
  if Trunc((versao * 100) + 0.5) <> VersaoCTe then
    FMsg := FMsg + 'Vers�o CT-e n�o suportada neste aplicativo: ' +
    FloatToStr(Versao);
  // Cabecalho ide => (*4*)
  ide_cUF := Geral.IMV(QrEmpresaDTB_UF.Value);
  ide_cUF := DefDomI(0, 5, ide_cUF);
  ide_cCT := ERi(0, 6, ide_cCT);
  //ide_CFOP       := ERi(0, 7, Geral.IMV(Geral.SoNumero_TT(QrFrtRegFisCCFOP.Value)));
  ide_CFOP       := ERi(0, 7, Geral.IMV(Geral.SoNumero_TT(QrFrtFatCabCFOP.Value)));
  //(*Verific. dupla*)CTe_PF.VDom(0,7, 'CFOP', QrFrtRegFisCCFOP.Value, 'CFOP', FMsg);
  (*Verific. dupla*)CTe_PF.VDom(0,7, 'CFOP', QrFrtFatCabCFOP.Value, 'CFOP', FMsg);
  //
  //ide_natOp      := ERx(*DefX*)(0, 8, QrFrtRegFisCnatOp.Value);
  ide_natOp      := ERx(*DefX*)(0, 8, QrFrtFatCabnatOp.Value);
  ide_forPag     := DefDomI(0, 9, QrFrtFatCabForPag.Value);
  ide_mod        := DefDomI(0, 10, QrOpcoesCTeCTeide_mod.Value);
  ide_serie      := ERi(0, 11, SerieCTe);
  ide_nCT        := ERi(0, 12, NumeroCTe);
  ide_dEmi       := (*13*)Geral.FDT(DataFat, 1);
  ide_hEmi       := (*13*)Geral.FDT(HoraFat, 100);
  ide_dhEmiTZD   := (*13*)QrOpcoesCTeTZD_UTC.Value;

  ide_tpImp      := DefDomI(0, 14, QrOpcoesCTeCTeide_tpImp.Value);
  ide_tpEmis     := DefDomI(0, 15, QrOpcoesCTeCTetpEmis.Value);
  if ide_tpEmis <> 1 then
  begin
    Geral.MB_Aviso('Tipo de emiss�o difere de "1 - Normal".' + slinebreak +
    'O tipo de emiss�o selecionado foi:' + sLineBreak +
    sCTeTpEmis[ide_tpEmis]);
  end;
  ide_cDV        := ERi(0, 16,  Geral.IMV(DV));
  ide_tpAmb      := DefDomI(0, 17, QrOpcoesCTeCTeide_tpAmb.Value);
  //ide_tpCTe      := DefDomI(0, 18, QrFrtRegFisCTpCTe.Value);
  ide_tpCTe      := DefDomI(0, 18, QrFrtFatCabTpCTe.Value);
  ide_procEmi    := DefDomI(0, 19, 0); // Emissao com aplicativo do contribuinte
  ide_verProc    := ERx(0, 20, DBCheck.Obtem_verProc);
  ide_refCTE     := ERx(0, 21, QrFrtFatCabrefCTE.Value);
  ide_cMunEnv    := ERi(0, 22, QrFrtFatCabCMunEnv.Value);
  ide_xMunEnv    := ERx(0, 23, ObtemXMun(QrFrtFatCabCMunEnv.Value));
  ide_UFEnv      := DefDomX(0, 24, QrFrtFatCabUFEnv.Value);
  ide_Modal      := DefDomI(0, 25, QrFrtFatCabModal.Value);
  ide_tpServ     := DefDomI(0, 26, QrFrtFatCabTpServ.Value);
  ide_cMunIni    := ERi(0, 27, QrFrtFatCabCMunIni.Value);
  ide_xMunIni    := ERx(0, 28, ObtemXMun(QrFrtFatCabCMunIni.Value));
  ide_UFIni      := DefDomX(0, 29, QrFrtFatCabUFIni.Value);
  ide_cMunFim    := ERi(0, 30, QrFrtFatCabCMunFim.Value);
  ide_xMunFim    := ERx(0, 31, ObtemXMun(QrFrtFatCabCMunFim.Value));
  ide_UFFim      := DefDomX(0, 32, QrFrtFatCabUFFim.Value);
  ide_Retira     := DefDomI(0, 33, QrFrtFatCabRetira.Value);
  ide_xDetRetira := ERx(0, 34, QrFrtFatCabXDetRetira.Value);
  (*35*) // toma03
  (*36*) // toma
  (*37*) // toma4
  (*38*) // toma
  //
  toma99_toma    := QrFrtFatCabToma.Value;
  toma04_toma    := QrFrtFatCabToma4.Value;
  toma04_CNPJ       := '';
  toma04_CPF        := '';
  toma04_IE         := '';
  toma04_xNome      := '';
  toma04_xFant      := '';
  toma04_fone       := '';
  enderToma_xLgr    := '';
  enderToma_nro     := '';
  enderToma_xCpl    := '';
  enderToma_xBairro := '';
  enderToma_cMun    := 0;
  enderToma_xMun    := '';
  enderToma_CEP     := 0;
  enderToma_UF      := '';
  enderToma_cPais   := 0;
  enderToma_xPais   := '';
  enderToma_email   := '';
  if TCTeToma(toma99_toma) = ctetomaOutros then
  begin
    ReopenEntiCTe(toma04_toma);
    if QrEntiCTeTipo.Value = 0 then
      toma04_CNPJ       := ERx(0, 39, Geral.SoNumero_TT(QrEntiCTeCNPJ.Value))
    else
      toma04_CPF        := ERx(0, 40, Geral.SoNumero_TT(QrEntiCTeCPF.Value));
    //
    toma04_IE         := ERx(0, 41, Geral.SoNumeroELetra_TT(QrEntiCTeIE.Value));
    toma04_xNome      := ERx(0, 42, QrEntiCTexNome.Value);
    toma04_xFant      := ERx(0, 43, QrEntiCTexFant.Value);
    toma04_fone       := ERx(0, 44, Geral.SoNumero_TT(QrEntiCTeFone.Value));
    //
    enderToma_xLgr    := ERx(0, 46, QrEntiCTexLgr.Value);
    enderToma_nro     := ERx(0, 47, QrEntiCTeNro.Value);
    enderToma_xCpl    := ERx(0, 48, QrEntiCTexCpl.Value);
    enderToma_xBairro := ERx(0, 49, QrEntiCTexBairro.Value);
    enderToma_cMun    := ERi(0, 50, Trunc(QrEntiCTecMun.Value));
    enderToma_xMun    := ERx(0, 51, QrEntiCTexMun.Value);
    enderToma_CEP     := ERi(0, 52, Trunc(QrEntiCTeCEP.Value));
    enderToma_UF      := DefDomX(0, 53, QrEntiCTeUF.Value);
    enderToma_cPais   := ERi(0, 54, Trunc(QrEntiCTecPais.Value));
    enderToma_xPais   := ERx(0, 55, QrEntiCTexPais.Value);
    enderToma_email   := ERx(0, 56, QrEntiCTeemail.Value);
  end;
  (*57*)ide_dhCont          := '00:00:00';  //MyBookMark Parei Aqui!
  (*57*)ide_dhContTZD       := 0.00;   //MyBookMark Parei Aqui!
  (*58*)ide_xJust           := '';      //MyBookMark Parei Aqui!
  if QrFrtFatCabCaracAd.Value = 0 then
    compl_xCaracAd    := ''
  else
    compl_xCaracAd    := ERx(0, 60, ObtemNome('FrtCaracAd', QrFrtFatCabCaracAd.Value));
  if QrFrtFatCabCaracSer.Value = 0 then
    compl_xCaracSer   := ''
  else
    compl_xCaracSer   := ERx(0, 61, ObtemNome('FrtCaracSer', QrFrtFatCabCaracSer.Value));
  if QrFrtFatCabEntiEmi.Value = 0 then
    compl_xEmi        := ''
  else
    compl_xEmi        := ERx(0, 62, Geral.FF0(QrFrtFatCabEntiEmi.Value));
////////////////////////////////////////////////////////////////////////////////
  // Cabecalho fluxo => (*63*) obrigatorio para aereo
  //                    (*64*) xOrig
  // Cabecalho pass =>  (*65*) obrigatorio para aereo
  //                    (*66*) xPass
  (*67*)// xDest  obrigatorio para aereo
////////////////////////////////////////////////////////////////////////////////
  (*68*)// xRota  Codigo da rota de entrega
////////////////////////////////////////////////////////////////////////////////
  //(*69*) : Cabecalho entrega - Opcional
  //(*70*) : Sub Cabecalho semData
  (*71*)//entrega_tpPer       := ;
  //(*72*) : Sub Cabecalho comData
  (*73*)//entrega_tpPer       := ;
  (*74*)entrega_dProg       := '0000-00-00'; //MyBookMark Parei Aqui!
  //(*75*) : Sub Cabecalho noPeriodo
  (*76*)//entrega_tpPer       := ;
  (*77*)entrega_dIni        := '0000-00-00'; //MyBookMark Parei Aqui!
  (*78*)entrega_dFim        := '0000-00-00'; //MyBookMark Parei Aqui!
  //(*79*) : Sub Cabecalho semHora
  (*80*)//entrega_tpHor       := ;
  //(*81*) : Sub Cabecalho comHora
  (*82*)//entrega_tpHor       := ;
  (*83*)entrega_hProg       := '00:00:00';  //MyBookMark Parei Aqui!
  //(*84*) : Sub Cabecalho noInter
  (*85*)//entrega_tpHor       := ;
  (*86*)entrega_hIni        := '00:00:00';  //MyBookMark Parei Aqui!
  (*87*)entrega_hFim        := '00:00:00';  //MyBookMark Parei Aqui!
  //
  (*88*)//ide_origCalc   := ;
  (*89*)//ide_destCalc   := ;
////////////////////////////////////////////////////////////////////////////////
///
///
  ide_xObs       := ERx(0, 90, QrFrtFatCabxObs.Value);
///
///
////////////////////////////////////////////////////////////////////////////////
  (*91*)// ObsCont Tabela Separada direta no faturamento FrtFatCab
        (*92*)// xCampo
        (*93*)// xTexto
////////////////////////////////////////////////////////////////////////////////
 { TODO 0 -cCTe : ObsFisco Tabela Separada}
  (*94*)// ObsFisco Tabela Separada
        (*95*)// xCampo
        (*96*)// xTexto
////////////////////////////////////////////////////////////////////////////////
///
  (*97*)// Cabecalho emit
  ReopenEntiCTe(QrFrtFatCabEmpresa.Value);
  emit_CNPJ      := ERx(0, 98, Geral.SoNumero_TT(QrEntiCTeCNPJ.Value));
  emit_IE        := ERx(0, 99, Geral.SoNumeroELetra_TT(QrEntiCTeIE.Value));
  emit_xNome     := ERx(0, 100, QrEntiCTexNome.Value);
  emit_xFant     := ERx(0, 101, QrEntiCTexFant.Value);
  (*102*)// Sub Cabecalho enderEmit
  emit_xLgr      := ERx(0, 103, QrEntiCTexLgr.Value);
  emit_nro       := ERx(0, 104, QrEntiCTeNro.Value);
  emit_xCpl      := ERx(0, 105, QrEntiCTexCpl.Value);
  emit_xBairro   := ERx(0, 106, QrEntiCTexBairro.Value);
  emit_cMun      := ERi(0, 107, Trunc(QrEntiCTeCMun.Value));
  emit_xMun      := ERx(0, 108, QrEntiCTexMun.Value);
  emit_CEP       := ERi(0, 109, Trunc(QrEntiCTeCEP.Value));
  emit_UF        := DefDomX(0, 110, QrEntiCTeUF.Value);
  emit_fone      := ERx(0, 111, Geral.SoNumero_TT(QrEntiCTeFone.Value));
///
////////////////////////////////////////////////////////////////////////////////
///
  (*112*)// Cabecalho rem - Opcional s� no redespacho intermedi�rio, no restante � obrigat�rio
  rem_CNPJ       := '';
  rem_CPF        := '';
  ReopenEntiCTe(QrFrtFatCabRemetente.Value);
  //
  if QrEntiCTeTipo.Value = 0 then
    rem_CNPJ     := ERx(0, 113, Geral.SoNumero_TT(QrEntiCTeCNPJ.Value))
  else
    rem_CPF      := ERx(0, 114, Geral.SoNumero_TT(QrEntiCTeCPF.Value));
  //
  rem_IE         := ERx(0, 115, Geral.SoNumeroELetra_TT(QrEntiCTeIE.Value));
  rem_xNome      := ERx(0, 116, QrEntiCTexNome.Value);
  rem_xFant      := ERx(0, 117, QrEntiCTexFant.Value);
  rem_fone       := ERx(0, 118, Geral.SoNumero_TT(QrEntiCTeFone.Value));
  (*119*)// Sub Cabecalho enderReme
  rem_xLgr    := ERx(0, 120, QrEntiCTexLgr.Value);
  rem_nro     := ERx(0, 121, QrEntiCTeNro.Value);
  rem_xCpl    := ERx(0, 122, QrEntiCTexCpl.Value);
  rem_xBairro := ERx(0, 123, QrEntiCTexBairro.Value);
  rem_cMun    := ERi(0, 124, Trunc(QrEntiCTecMun.Value));
  rem_xMun    := ERx(0, 125, QrEntiCTexMun.Value);
  rem_CEP     := ERi(0, 126, Trunc(QrEntiCTeCEP.Value));
  rem_UF      := DefDomX(0, 127, QrEntiCTeUF.Value);
  rem_cPais   := ERi(0, 128, Trunc(QrEntiCTecPais.Value));
  rem_xPais   := ERx(0, 129, QrEntiCTexPais.Value);
  rem_email   := ERx(0, 130, QrEntiCTeemail.Value);
///
///
////////////////////////////////////////////////////////////////////////////////
 { TODO 0 -cCTe : ctecab2loccoleta Tabela Separada}
  (*131*)// ctecab2loccoleta Tabela Separada
        (*132*)// CNPJ
        (*133*)// IE
        (*134*)// xNome
        (*135*)// xLgr
        (*136*)// nro
        (*137*)// xCpl
        (*138*)// xBairro
        (*149*)// cMun
        (*140*)// xMun
        (*141*)// UF
 { TODO 0 -cCTe : ctecab1exped Tabela Separada}
  (*142*)// ctecab1exped Tabela Separada
        (*143*)// CNPJ
        (*144*)// CPF
        (*145*)// IE
        (*146*)// xNome
        (*147*)// fone
        (*148*)// Sub Cabecalho enderExped
          (*149*)//  xLgr
          (*150*)//  nro
          (*151*)//  xCpl
          (*152*)//  xBairro
          (*153*)//  cMun
          (*154*)//  xMun
          (*155*)//  CEP
          (*156*)//  UF
          (*157*)//  cPais
          (*158*)//  xPais
        (*159*)//  email
 { TODO 0 -cCTe : ctecab1receb Tabela Separada}
  (*160*)// ctecab1receb Tabela Separada
        (*161*)// CNPJ
        (*162*)// CPF
        (*163*)// IE
        (*164*)// xNome
        (*165*)// fone
        (*166*)// Sub Cabecalho enderExped
          (*167*)//  xLgr
          (*168*)//  nro
          (*169*)//  xCpl
          (*170*)//  xBairro
          (*171*)//  cMun
          (*172*)//  xMun
          (*173*)//  CEP
          (*174*)//  UF
          (*175*)//  cPais
          (*176*)//  xPais
        (*177*)//  email
///
////////////////////////////////////////////////////////////////////////////////
///
  (*178*)// Cabecalho dest - Opcional s� no redespacho intermedi�rio, no restante � obrigat�rio
  dest_CNPJ       := '';
  dest_CPF        := '';
  ReopenEntiCTe(QrFrtFatCabDestinatario.Value);
  //
  if QrEntiCTeTipo.Value = 0 then
    dest_CNPJ     := ERx(0, 179, Geral.SoNumero_TT(QrEntiCTeCNPJ.Value))
  else
    dest_CPF      := ERx(0, 180, Geral.SoNumero_TT(QrEntiCTeCPF.Value));
  //
  dest_IE         := ERx(0, 181, Geral.SoNumeroELetra_TT(QrEntiCTeIE.Value));
  dest_xNome      := ERx(0, 182, QrEntiCTexNome.Value);
  dest_fone       := ERx(0, 183, Geral.SoNumero_TT(QrEntiCTeFone.Value));
  dest_ISUF       := ERx(0, 184, Geral.SoNumero_TT(QrEntiCTeSUFRAMA.Value));
  (*185*)// Sub Cabecalho enderDest
  dest_xLgr    := ERx(0, 186, QrEntiCTexLgr.Value);
  dest_nro     := ERx(0, 187, QrEntiCTeNro.Value);
  dest_xCpl    := ERx(0, 188, QrEntiCTexCpl.Value);
  dest_xBairro := ERx(0, 189, QrEntiCTexBairro.Value);
  dest_cMun    := ERi(0, 190, Trunc(QrEntiCTecMun.Value));
  dest_xMun    := ERx(0, 191, QrEntiCTexMun.Value);
  dest_CEP     := ERi(0, 192, Trunc(QrEntiCTeCEP.Value));
  dest_UF      := DefDomX(0, 193, QrEntiCTeUF.Value);
  dest_cPais   := ERi(0, 194, Trunc(QrEntiCTecPais.Value));
  dest_xPais   := ERx(0, 195, QrEntiCTexPais.Value);
  dest_email   := ERx(0, 196, QrEntiCTeemail.Value);
///
///
////////////////////////////////////////////////////////////////////////////////
///
///
 { TODO 0 -cCTe : ctecab2locent Tabela Separada}
  (*197*)// ctecab2locent Tabela Separada
        (*198*)// CNPJ
        (*199*)// CPF
        (*200*)// xNome
        (*201*)// xLgr
        (*202*)// nro
        (*203*)// xCpl
        (*204*)// xBairro
        (*205*)// cMun
        (*206*)// xMun
        (*207*)// UF
///
///
////////////////////////////////////////////////////////////////////////////////
///
///
  (*208*)// Cabecalho vPrest - Obrigat�rio
  vPrest_vTPrest := ERf(0, 209, QrFrtFatCabvTPrest.Value, 2);
  vPrest_vRec    := ERf(0, 210, QrFrtFatCabvRec.Value, 2);
///
///
////////////////////////////////////////////////////////////////////////////////
  (*211*)// cteit2comp Tabela Separada
        (*212*)// xNome
        (*213*)// vComp
///
///
////////////////////////////////////////////////////////////////////////////////
  (*214*)// imp - Impostos
   (*215*)// Cabecalho ICMS
  ICMS_CST           := 0;
  ICMS_vBC           := 0;
  ICMS_pICMS         := 0;
  ICMS_vICMS         := 0;
  ICMS_pRedBC        := 0;
  ICMS_vBCSTRet      := 0;
  ICMS_vICMSSTRet    := 0;
  ICMS_pICMSSTRet    := 0;
  ICMS_vCred         := 0;
  ICMS_pRedBCOutraUF := 0;
  ICMS_vBCOutraUF    := 0;
  ICMS_pICMSOutraUF  := 0;
  ICMS_vICMSOutraUF  := 0;
  ICMSSN_indSN       := 0;
  imp_vTotTrib       := 0;
  //CTeMyCST           := QrFrtRegFisCMyCST.Value;
  CTeMyCST           := QrFrtFatCabMyCST.Value;
  case TCTeMyCST(CTeMyCST) of
    (*0*)ctemycstIndefinido: FMsg := FMsg + 'CST indefinido!';
    (*1*)ctemycstCST00Normal: (*216*)// ICMS00
    begin
     ICMS_CST           := DefDomI(0, 217, 0);
     ICMS_vBC           := ERf(0, 218, vPrest_vTPrest, 2);
     //ICMS_pICMS         := ERf(0, 219, QrFrtRegFisCICMS_pICMS.Value, 2);
     ICMS_pICMS         := ERf(0, 219, QrFrtFatCAbICMS_pICMS.Value, 2);
     ValCalc            := Geral.RoundC(ICMS_vBC * ICMS_pICMS / 100, 2);
     ICMS_vICMS         := ERf(0, 220, ValCalc, 2);
    end;
   (*2*)ctemycstCST20BCRed: (*221*)// ICMS20
   begin
     ICMS_CST           := DefDomI(0, 222, 20);
     //ICMS_pRedBC        := ERf(0, 223, QrFrtRegFisCICMS_pRedBC.Value, 2);
     ICMS_pRedBC        := ERf(0, 223, QrFrtFatCabICMS_pRedBC.Value, 2);
     ValCalc            := Geral.RoundC(vPrest_vTPrest * (100 - ICMS_pRedBC) / 100, 2);
     ICMS_vBC           := ERf(0, 224, ValCalc, 2);
     //ICMS_pICMS         := ERf(0, 225, QrFrtRegFisCICMS_pICMS.Value, 2);
     ICMS_pICMS         := ERf(0, 225, QrFrtFatCabICMS_pICMS.Value, 2);
     ValCalc            := Geral.RoundC(ICMS_vBC * ICMS_pICMS / 100, 2);
     ICMS_vICMS         := ERf(0, 226, ValCalc, 2);
   end;
   (*3*)ctemycstCST40Isensao: (*227*) // ICMS45.40
   begin
     ICMS_CST           := DefDomI(0, 228, 40);
   end;
   (*4*)ctemycstCST41NaoTrib: (*227*) // ICMS45.41
   begin
     ICMS_CST           := DefDomI(0, 228, 41);
   end;
   (*5*)ctemycstCST51Diferido: (*227*) // ICMS45.51
   begin
     ICMS_CST           := DefDomI(0, 228, 51);
   end;
   (*6*)ctemycstCST60ST: (*229*) // ICMS60
   begin
     ICMS_CST           := DefDomI(0, 230, 60);
     MensagemCalcularManualCST(ICMS_CST, 'Tributa��o pelo ICMS60 - ICMS ' +
     'cobrado por substitui��o tribut�ria.Responsabilidade do recolhimento ' +
     'do ICMS atribu�do ao tomador ou 3� por ST');
(*
     ICMS_vBCSTRet      := DefDomI(0, 231, ?);
     ICMS_vICMSSTRet    := DefDomI(0, 232, ?);
     ICMS_pICMSSTRet    := DefDomI(0, 233, ?);
     ICMS_vCred         := DefDomI(0, 234, ?);
*)
   end;
   (*7*)ctemycstCST90Outros:  (*235*) // ICMS90 - Outros
   begin
     ICMS_CST           := DefDomI(0, 236, 90);
     MensagemCalcularManualCST(ICMS_CST, 'ICMS outros');
(*
     ICMS_pRedBC        := DefDomI(0, 237, ?);
     ICMS_vBC           := DefDomI(0, 238, ?);
     ICMS_pICMS         := DefDomI(0, 239, ?);
     ICMS_vICMS         := DefDomI(0, 240, ?);
     ICMS_vCred         := DefDomI(0, 241, ?);
*)
   end;
   (*8*)ctemycstCST90OutraUF: (*242*) // ICMSOutraUF
   begin
     ICMS_CST           := DefDomI(0, 243, 90);
     MensagemCalcularManualCST(ICMS_CST, 'ICMS devido � UF de origem da ' +
     'presta��o, quando diferente da UF do emitente');
(*
     ICMS_pRedBCOutraUF := DefDomI(0, 244, ?);
     ICMS_vBCOutraUF    := DefDomI(0, 245, ?);
     ICMS_pICMSOutraUF  := DefDomI(0, 246, ?);
     ICMS_vICMSOutraUF  := DefDomI(0, 247, ?);
*)
   end;
   (*9*)ctemycstCSTSimplesNacional: (*248*) // ICMSSN
   begin
     ICMSSN_indSN       := DefDomI(0, 249, 1);
   end;
   else
     FMsg := FMsg + 'CST n�o implementado!';
  end;
  //
  // Lei da transparencia
  //iTotTrib_Fonte        := QrFrtRegFisCiTotTrib_Fonte.Value;
  iTotTrib_Fonte        := QrFrtFatCabiTotTrib_Fonte.Value;
  //iTotTrib_Tabela       := QrFrtRegFisCiTotTrib_Tabela.Value;
  iTotTrib_Tabela       := QrFrtFatCabiTotTrib_Tabela.Value;
  //iTotTrib_Ex           := QrFrtRegFisCiTotTrib_Ex.Value;
  iTotTrib_Ex           := QrFrtFatCabiTotTrib_Ex.Value;
  //iTotTrib_Codigo       := QrFrtRegFisCiTotTrib_Codigo.Value;
  iTotTrib_Codigo       := QrFrtFatCabiTotTrib_Codigo.Value;
  //
  ObtemValorAproximadoDeTributos(iTotTrib_Fonte, iTotTrib_Tabela, iTotTrib_Ex,
  iTotTrib_Codigo, emit_UF, (*Importado: Boolean;*) vPrest_vTPrest, vDesc,
  vBasTrib, pTotTrib, imp_vTotTrib, (*tabTotTrib: TIBPTaxTabs;*) verTotTrib,
  tpAliqTotTrib (*fontTotTrib: TIBPTaxFont*));
  imp_vTotTrib          := ERf(0, 250, imp_vTotTrib, 2);
  // Fim Lei da transparencia
  //
  imp_infAdFisco   := ''; //ERx(0, 251, ''); // Usa?
  (*252*)// Cabecalho infCTeNorm Informacoes de CTe Normal
//
      infCteSub_ContribICMS := -1;
      infCteSub_TipoDoc     := -1;
      infCteSub_chCte       := '';
      infCteSub_refNFe      := '';
      infCteSub_CNPJ        := '';
      infCteSub_CPF         := '';
      infCteSub_mod         := '';
      infCteSub_serie       := 0;
      infCteSub_subserie    := 0;
      infCteSub_nro         := 0;
      infCteSub_valor       := 0;
      infCteSub_dEmi        := '0000-00-00';
      infCteSub_refCTe      := '';
      infCteSub_refCTeAnu   := '';
//
    (*411*) infCTeComp_chave := '';
//
    (*413*) infCTeAnu_chCte := '';
    (*414*) infCTeAnu_dEmi  := '0000-00-00';
//
  if (TCTeTpCTe(ide_TpCTe) = ctetpcteNormal)
  or (TCTeTpCTe(ide_TpCTe) = ctetpcteSubstituto) then
  begin
    (*253*)// Sub Cabecalho Carga - Obrigat�rio
    infCarga_vCarga  := ERf(0, 254, QrFrtFatCabvCarga.Value, 2);
    infCarga_proPred  := ERx(0, 255, ObtemNome('FrtProPred', QrFrtFatCabFrtProPred.Value));
    if QrFrtFatCabxOutCat.Value <> '' then
      infCarga_xOutCat := ERx(0, 256, QrFrtFatCabxOutCat.Value)
    else
      infCarga_xOutCat := '';
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
    (*257*)// cteit3infq Tabela Separada
          (*258*)// cUnid
          (*259*)// tpMed
          (*260*)// qCarga
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  (*261*)//infDoc : Documentos transportados pelo CT-e
  ///
  ////////////////////////////////////////////////////////////////////////////////
    (*262*)// infNF => usar frtfatinfdoc - Tabela Separada
          (*263*)//
          (*...*)
          (*296*)
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  ///
    (*297*)// infNFe => usar frtfatinfdoc - Tabela Separada
          (*298*)//
          (*...*)
          (*318*)
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  ///
    (*319*)// infOutros => usar frtfatinfdoc - Tabela Separada
          (*320*)//
          (*...*)
          (*343*)
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  ///
    (*344*)// docAnt => usar frtfatDocAnt - Tabela Separada
          (*345*)//
          (*...*)
          (*359*)
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  ///
    (*360*)// seg => usar frtfatSeg - Tabela Separada
          (*360*)//
          (*...*)
          (*365*)
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  ///
    (*366*)// infModal => usar frtfat??? - Tabela Separada
          (*367*)//
          (*368*)
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  ///
    (*369*)// peri => usar frtfat??? - Tabela Separada
          (*370*)//
          (*...*)
          (*376*)
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  ///
    (*377*)// veicNovos => usar frtfat??? - Tabela Separada
          (*378*)//
          (*...*)
          (*383*)
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  ///
    (*384*)// cobr => Cobran�a
      (*385*)// fat => Dados Fatura
    Filial  := DmodG.ObtemFilialDeEntidade(Empresa);
    TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
  (*
    UnDmkDAC_PF.AbreMySQLQuery0(QrFatYTot, Dmod.MyDB, [
    'SELECT FatID, FatNum, SUM(Credito) Valor ',
    'FROM ' + TabLctA,
    'WHERE FatID = ' + FormatFloat('0', FatID) +
    'AND FatNum=' + FormatFloat('0', FatNum) +
    'AND CliInt=' + FormatFloat('0', Empresa) +
    'GROUP BY CliInt',
    '']);
  *)
    cobr_Fat_nFat := dmkPF.MontaDuplicata(
      //IDDuplicata,
      //FormatFloat('000000', FatNum),
      FatNum,
      //NumeroNF,
      //FormatFloat('000000', ide_nCT),
      ide_nCT,
      //Parcela,
      0,
      //FaturaNum,
      QrOpcoesCTeFaturaNum.Value,
      //FaturaSeq,
      QrOpcoesCTeFaturaSeq.Value,
      //TpDuplicata,
      QrOpcoesCTeDupFretPrest.Value,
      //FaturaSep
      QrOpcoesCTeFaturaSep.Value,
      // ApenasFatura
      True);
    cobr_Fat_nFat      := ERx(0, 386, cobr_Fat_nFat);
    cobr_Fat_vOrig     := ERf(0, 387, QrFrtFatCabvOrig.Value, 2);
    cobr_Fat_vDesc     := ERf(0, 388, QrFrtFatCabvDesc.Value, 2);
    cobr_Fat_vLiq      := ERf(0, 389, QrFrtFatCabvLiq.Value, 2);
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  ///
    (*390*)// dup => usar frtfat??? - Dados das duplicatas
          (*391*)//
          (*392*)
          (*393*)
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  ///
    (*394*)// infCteSub => usar frtfat??? - Dados das CTe de substituicao
    if TCTeTpCTe(ide_tpCTe) = ctetpcteSubstituto then
    begin
      ReopenFrtFatInfCTeSub(FatNum);
      //
      infCteSub_ContribICMS := QrFrtFatInfCTeSubContribICMS.Value;
      infCteSub_TipoDoc     := QrFrtFatInfCTeSubTipoDoc.Value;
      //
      infCteSub_chCte     := ERx(0, 395, QrFrtFatInfCTeSubChCTe.Value);
        (*396*) // tomaICMS
      infCteSub_refNFe    := ERx(0, 397, QrFrtFatInfCTeSubrefNFe.Value);
          (*398*) // refNF
      infCteSub_CNPJ      := ERx(0, 399, Geral.SoNumero_TT(QrFrtFatInfCTeSubCNPJ.Value));
      infCteSub_CPF       := ERx(0, 400, Geral.SoNumero_TT(QrFrtFatInfCTeSubCPF.Value));
      infCteSub_mod       := ERx(0, 401, QrFrtFatInfCTeSubmod_.Value);
      infCteSub_serie     := ERi(0, 402, QrFrtFatInfCTeSubserie.Value);
      infCteSub_subserie  := ERi(0, 403, QrFrtFatInfCTeSubsubserie.Value);
      infCteSub_nro       := ERi(0, 404, QrFrtFatInfCTeSubnro.Value);
      infCteSub_valor     := ERf(0, 405, QrFrtFatInfCTeSubvalor.Value, 2);
      infCteSub_dEmi      := ERx(0, 406, Geral.FDT(QrFrtFatInfCTeSubdEmi.Value, 1));
      infCteSub_refCTe    := ERx(0, 407, QrFrtFatInfCTeSubrefCTe.Value);
      (*408*) // tomaNaoICMS
      infCteSub_refCTeAnu := ERx(0, 409, QrFrtFatInfCTeSubrefCTeAnu.Value);
    end else
    begin
      infCteSub_ContribICMS := -1;
      infCteSub_TipoDoc     := -1;
      infCteSub_chCte       := '';
      infCteSub_refNFe      := '';
      infCteSub_CNPJ        := '';
      infCteSub_CPF         := '';
      infCteSub_mod         := '';
      infCteSub_serie       := 0;
      infCteSub_subserie    := 0;
      infCteSub_nro         := 0;
      infCteSub_valor       := 0;
      infCteSub_dEmi        := '0000-00-00';
      infCteSub_refCTe      := '';
      infCteSub_refCTeAnu   := '';
    end;
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  ///
  end;
  (*410*)// infCteComp => Detalhamento de CT-e complementado
  if TCTeTpCTe(ide_tpCTe) = ctetpcteComplValrs then
  begin
    ReopenFrtFatInfCTeComp(FatNum);
    (*411*) infCTeComp_chave := ERx(0, 411, QrFrtFatInfCTeCompChave.Value);
  end else
    (*411*) infCTeComp_chave := '';
///
///
////////////////////////////////////////////////////////////////////////////////
///
///
  (*412*)// infCteAnu => Detalhamento de CT-e de anula��o
  if TCTeTpCTe(ide_tpCTe) = ctetpcteAnulacao then
  begin
    ReopenFrtFatInfCTeAnu(FatNum);
    (*413*) infCTeAnu_chCte := ERx(0, 413, QrFrtFatInfCTeAnuChCTe.Value);
    (*414*) infCTeAnu_dEmi  := ERx(0, 414, Geral.FDT(QrFrtFatInfCTeAnudEmi.Value, 1));
  end else
  begin
    (*413*) infCTeAnu_chCte := '';
    (*414*) infCTeAnu_dEmi  := '0000-00-00';
  end;
///
///
////////////////////////////////////////////////////////////////////////////////
///
///
  (*415*)// autXML => usar frtfat??? - Autorizados para download do XML
        (*416*)//
        (*417*)
///
///
////////////////////////////////////////////////////////////////////////////////
///
///
///
(*
  (**)//protCTe_versao := ;
  (**)//infProt_Id     := ;
  (**)//infProt_tpAmb  := ;
  (**)//infProt_verAplic:= ;
  (**)//infProt_dhRecbto:= ;
  (**)//infProt_dhRecbtoTZD:= ;
  (**)//infProt_nProt  := ;
  (**)//infProt_digVal := ;
  (**)//infProt_cStat  := ;
  (**)//infProt_xMotivo:= ;
  (**)//retCancCTe_versao:= ;
  (**)//infCanc_Id     := ;
  (**)//infCanc_tpAmb  := ;
  (**)//infCanc_verAplic:= ;
  (**)//infCanc_dhRecbto:= ;
  (**)//infCanc_dhRecbtoTZD:= ;
  (**)//infCanc_nProt  := ;
  (**)//infCanc_digVal := ;
  (**)//infCanc_cStat  := ;
  (**)//infCanc_xMotivo:= ;
  (**)//infCanc_cJust  := ;
  (**)//infCanc_xJust  := ;
  FisRegCad      := QrFrtFatCabFrtRegFisC.Value;
  CartEmiss      := QrFrtFatCabCartEmiss.Value;
  TabelaPrc      := QrFrtFatCabTabelaPrc.Value;
  CondicaoPg     := QrFrtFatCabCondicaoPg.Value;
  CodInfoEmite   := QrFrtFatCabEmpresa.Value;
  CodInfoTomad   := QrFrtFatCabTomador.Value;
  CodInfoRemet   := QrFrtFatCabRemetente.Value;
  CodInfoExped   := QrFrtFatCabExpedidor.Value;
  CodInfoReceb   := QrFrtFatCabRecebedor.Value;
  CodInfoDesti   := QrFrtFatCabDestinatario.Value;
  CodInfoToma4   := QrFrtFatCabToma4.Value;
  CriAForca      := 0;
  (**)//eveEPEC_Id      := ;
  (**)//eveEPEC_tpAmb   := ;
  (**)//eveEPEC_verAplic:= ;
  (**)//eveEPEC_cOrgao  := ;
  (**)//eveEPEC_cStat   := ;
  (**)//eveEPEC_xMotivo := ;
  (**)//eveEPEC_chCTe   := ;
  (**)//eveEPEC_tpEvento:= ;
  (**)//eveEPEC_xEvento := ;
  (**)//eveEPEC_nSeqEvento:= ;
  (**)//eveEPEC_CNPJDest:= ;
  (**)//eveEPEC_CPFDest := ;
  (**)//eveEPEC_emailDest:= ;
  (**)//eveEPEC_dhRegEvento:= ;
  (**)//eveEPEC_TZD_UTC := ;
  (**)//eveEPEC_nProt   := ;
  (**)//cSitCTe        := ;
  (**)//cSitConf       := ;
  (**)//infCCe_verAplic:= ;
  (**)//infCCe_cOrgao  := ;
  (**)//infCCe_tpAmb   := ;
  (**)//infCCe_CNPJ    := ;
  (**)//infCCe_CPF     := ;
  (**)//infCCe_chCTe   := ;
  (**)//infCCe_dhEvento:= ;
  (**)//infCCe_dhEventoTZD:= ;
  (**)//infCCe_tpEvento:= ;
  (**)//infCCe_nSeqEvento:= ;
  (**)//infCCe_verEvento:= ;
  (**)//infCCe_x Correcao:= ;
  (**)//infCCe_cStat   := ;
  (**)//infCCe_dhRegEvento:= ;
  (**)//infCCe_dhRegEventoTZD:= ;
  (**)//infCCe_nProt   := ;
  (**)//infCCe_nCondUso:= ;







  //
(*
? := UMyMod.BuscaEmLivreY_Def('ctecaba', 'FatID', 'FatNum', 'Empresa', SQLType, CodAtual?);
ou > ? :=
*)

  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctecaba', False, [
  'FatID', 'FatNum', 'Empresa',
  'LoteEnv', 'versao',
  'Id', 'ide_cUF', 'ide_cCT',
  'ide_CFOP', 'ide_natOp', 'ide_forPag',
  'ide_mod', 'ide_serie', 'ide_nCT',
  'ide_dEmi', 'ide_hEmi', 'ide_dhEmiTZD',
  'ide_tpImp', 'ide_tpEmis', 'ide_cDV',
  'ide_tpAmb', 'ide_tpCTe', 'ide_procEmi',
  'ide_verProc', 'ide_refCTE', 'ide_cMunEnv',
  'ide_xMunEnv', 'ide_UFEnv', 'ide_Modal',
  'ide_tpServ', 'ide_cMunIni', 'ide_xMunIni',
  'ide_UFIni', 'ide_cMunFim', 'ide_xMunFim',
  'ide_UFFim', 'ide_Retira', 'ide_xDetRetira',
  'toma99_toma', 'toma04_toma', 'toma04_CNPJ',
  'toma04_CPF', 'toma04_IE', 'toma04_xNome',
  'toma04_xFant', 'toma04_fone', 'ide_dhCont',
  'ide_dhContTZD', 'ide_xJust', 'compl_xCaracAd',
  'compl_xCaracSer', 'compl_xEmi', 'entrega_tpPer',
  'entrega_dProg', 'entrega_dIni', 'entrega_dFim',
  'entrega_tpHor', 'entrega_hProg', 'entrega_hIni',
  'entrega_hFim', 'ide_origCalc', 'ide_destCalc',
  'ide_xObs', 'emit_CNPJ', 'emit_IE',
  'emit_xNome', 'emit_xFant', 'emit_xLgr',
  'emit_nro', 'emit_xCpl', 'emit_xBairro',
  'emit_cMun', 'emit_xMun', 'emit_CEP',
  'emit_UF', 'emit_fone', 'rem_CNPJ',
  'rem_CPF', 'rem_IE', 'rem_xNome',
  'rem_xFant', 'rem_fone', 'rem_xLgr',
  'rem_nro', 'rem_xCpl', 'rem_xBairro',
  'rem_cMun', 'rem_xMun', 'rem_CEP',
  'rem_UF', 'rem_cPais', 'rem_xPais',
  'rem_email', 'dest_CNPJ', 'dest_CPF',
  'dest_IE', 'dest_xNome', 'dest_fone',
  'dest_ISUF', 'dest_xLgr', 'dest_nro',
  'dest_xCpl', 'dest_xBairro', 'dest_cMun',
  'dest_xMun', 'dest_CEP', 'dest_UF',
  'dest_cPais', 'dest_xPais', 'dest_email',
  'vPrest_vTPrest', 'vPrest_vRec', 'ICMS_CST',
  'ICMS_vBC', 'ICMS_pICMS', 'ICMS_vICMS',
  'ICMS_pRedBC', 'ICMS_vBCSTRet', 'ICMS_vICMSSTRet',
  'ICMS_pICMSSTRet', 'ICMS_vCred', 'ICMS_pRedBCOutraUF',
  'ICMS_vBCOutraUF', 'ICMS_pICMSOutraUF', 'ICMS_vICMSOutraUF',
  'ICMSSN_indSN', 'imp_vTotTrib', 'imp_infAdFisco',
  'infCarga_vCarga', 'infCarga_proPred', 'infCarga_xOutCat',
  'cobr_Fat_nFat', 'cobr_Fat_vOrig', 'cobr_Fat_vDesc',
  'cobr_Fat_vLiq',

  'infCteSub_ContribICMS', 'infCteSub_TipoDoc', 'infCteSub_chCte',
  'infCteSub_refNFe', 'infCteSub_CNPJ', 'infCteSub_CPF',
  'infCteSub_mod', 'infCteSub_serie', 'infCteSub_subserie',
  'infCteSub_nro', 'infCteSub_valor', 'infCteSub_dEmi',
  'infCteSub_refCTe', 'infCteSub_refCTeAnu',


  'infCTeComp_chave', 'infCTeAnu_chCte', 'infCTeAnu_dEmi',
  (*'protCTe_versao', 'infProt_Id',
  'infProt_tpAmb', 'infProt_verAplic', 'infProt_dhRecbto',
  'infProt_dhRecbtoTZD', 'infProt_nProt', 'infProt_digVal',
  'infProt_cStat', 'infProt_xMotivo', 'retCancCTe_versao',
  'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
  'infCanc_dhRecbto', 'infCanc_dhRecbtoTZD', 'infCanc_nProt',
  'infCanc_digVal', 'infCanc_cStat', 'infCanc_xMotivo',
  'infCanc_cJust', 'infCanc_xJust',*) 'FisRegCad',
  'CartEmiss', 'TabelaPrc', 'CondicaoPg',
  'CodInfoEmite', 'CodInfoTomad',
  'CodInfoRemet', 'CodInfoExped', 'CodInfoReceb',
  'CodInfoDesti', 'CodInfoToma4', 'CriAForca',
  (*'eveEPEC_Id', 'eveEPEC_tpAmb', 'eveEPEC_verAplic',
  'eveEPEC_cOrgao', 'eveEPEC_cStat', 'eveEPEC_xMotivo',
  'eveEPEC_chCTe', 'eveEPEC_tpEvento', 'eveEPEC_xEvento',
  'eveEPEC_nSeqEvento', 'eveEPEC_CNPJDest', 'eveEPEC_CPFDest',
  'eveEPEC_emailDest', 'eveEPEC_dhRegEvento', 'eveEPEC_TZD_UTC',
  'eveEPEC_nProt', 'cSitCTe', 'cSitConf',
  'infCCe_verAplic', 'infCCe_cOrgao', 'infCCe_tpAmb',
  'infCCe_CNPJ', 'infCCe_CPF', 'infCCe_chCTe',
  'infCCe_dhEvento', 'infCCe_dhEventoTZD', 'infCCe_tpEvento',
  'infCCe_nSeqEvento', 'infCCe_verEvento', infCCe_x Correcao,
  'infCCe_cStat', 'infCCe_dhRegEvento', 'infCCe_dhRegEventoTZD',
  'infCCe_nProt', 'infCCe_nCondUso',*) 'iTotTrib_Fonte',
  'iTotTrib_Tabela', 'iTotTrib_Ex', 'iTotTrib_Codigo',
  'vBasTrib', 'pTotTrib', 'verTotTrib',
  'tpAliqTotTrib', 'CTeMyCST'], [
  'IDCtrl'
  ], [
  FatID, FatNum, Empresa, LoteEnv, versao,
  Id, ide_cUF, ide_cCT,
  ide_CFOP, ide_natOp, ide_forPag,
  ide_mod, ide_serie, ide_nCT,
  ide_dEmi, ide_hEmi, ide_dhEmiTZD,
  ide_tpImp, ide_tpEmis, ide_cDV,
  ide_tpAmb, ide_tpCTe, ide_procEmi,
  ide_verProc, ide_refCTE, ide_cMunEnv,
  ide_xMunEnv, ide_UFEnv, ide_Modal,
  ide_tpServ, ide_cMunIni, ide_xMunIni,
  ide_UFIni, ide_cMunFim, ide_xMunFim,
  ide_UFFim, ide_Retira, ide_xDetRetira,
  toma99_toma, toma04_toma, toma04_CNPJ,
  toma04_CPF, toma04_IE, toma04_xNome,
  toma04_xFant, toma04_fone, ide_dhCont,
  ide_dhContTZD, ide_xJust, compl_xCaracAd,
  compl_xCaracSer, compl_xEmi, entrega_tpPer,
  entrega_dProg, entrega_dIni, entrega_dFim,
  entrega_tpHor, entrega_hProg, entrega_hIni,
  entrega_hFim, ide_origCalc, ide_destCalc,
  ide_xObs, emit_CNPJ, emit_IE,
  emit_xNome, emit_xFant, emit_xLgr,
  emit_nro, emit_xCpl, emit_xBairro,
  emit_cMun, emit_xMun, emit_CEP,
  emit_UF, emit_fone, rem_CNPJ,
  rem_CPF, rem_IE, rem_xNome,
  rem_xFant, rem_fone, rem_xLgr,
  rem_nro, rem_xCpl, rem_xBairro,
  rem_cMun, rem_xMun, rem_CEP,
  rem_UF, rem_cPais, rem_xPais,
  rem_email, dest_CNPJ, dest_CPF,
  dest_IE, dest_xNome, dest_fone,
  dest_ISUF, dest_xLgr, dest_nro,
  dest_xCpl, dest_xBairro, dest_cMun,
  dest_xMun, dest_CEP, dest_UF,
  dest_cPais, dest_xPais, dest_email,
  vPrest_vTPrest, vPrest_vRec, ICMS_CST,
  ICMS_vBC, ICMS_pICMS, ICMS_vICMS,
  ICMS_pRedBC, ICMS_vBCSTRet, ICMS_vICMSSTRet,
  ICMS_pICMSSTRet, ICMS_vCred, ICMS_pRedBCOutraUF,
  ICMS_vBCOutraUF, ICMS_pICMSOutraUF, ICMS_vICMSOutraUF,
  ICMSSN_indSN, imp_vTotTrib, imp_infAdFisco,
  infCarga_vCarga, infCarga_proPred, infCarga_xOutCat,
  cobr_Fat_nFat, cobr_Fat_vOrig, cobr_Fat_vDesc,
  cobr_Fat_vLiq,

  infCteSub_ContribICMS, infCteSub_TipoDoc, infCteSub_chCte,
  infCteSub_refNFe, infCteSub_CNPJ, infCteSub_CPF,
  infCteSub_mod, infCteSub_serie, infCteSub_subserie,
  infCteSub_nro, infCteSub_valor, infCteSub_dEmi,
  infCteSub_refCTe, infCteSub_refCTeAnu,

  infCTeComp_chave, infCTeAnu_chCte, infCTeAnu_dEmi,
  (*protCTe_versao, infProt_Id,
  infProt_tpAmb, infProt_verAplic, infProt_dhRecbto,
  infProt_dhRecbtoTZD, infProt_nProt, infProt_digVal,
  infProt_cStat, infProt_xMotivo, retCancCTe_versao,
  infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
  infCanc_dhRecbto, infCanc_dhRecbtoTZD, infCanc_nProt,
  infCanc_digVal, infCanc_cStat, infCanc_xMotivo,
  infCanc_cJust, infCanc_xJust,*) FisRegCad,
  CartEmiss, TabelaPrc, CondicaoPg,
  CodInfoEmite, CodInfoTomad,
  CodInfoRemet, CodInfoExped, CodInfoReceb,
  CodInfoDesti, CodInfoToma4, CriAForca,
  (*eveEPEC_Id, eveEPEC_tpAmb, eveEPEC_verAplic,
  eveEPEC_cOrgao, eveEPEC_cStat, eveEPEC_xMotivo,
  eveEPEC_chCTe, eveEPEC_tpEvento, eveEPEC_xEvento,
  eveEPEC_nSeqEvento, eveEPEC_CNPJDest, eveEPEC_CPFDest,
  eveEPEC_emailDest, eveEPEC_dhRegEvento, eveEPEC_TZD_UTC,
  eveEPEC_nProt, cSitCTe, cSitConf,
  infCCe_verAplic, infCCe_cOrgao, infCCe_tpAmb,
  infCCe_CNPJ, infCCe_CPF, infCCe_chCTe,
  infCCe_dhEvento, infCCe_dhEventoTZD, infCCe_tpEvento,
  infCCe_nSeqEvento, infCCe_verEvento, infCCe_x Correcao,
  infCCe_cStat, infCCe_dhRegEvento, infCCe_dhRegEventoTZD,
  infCCe_nProt, infCCe_nCondUso,*) iTotTrib_Fonte,
  iTotTrib_Tabela, iTotTrib_Ex, iTotTrib_Codigo,
  vBasTrib, pTotTrib, verTotTrib,
  tpAliqTotTrib, CTeMyCST], [
  IDCtrl], True) then
  begin
    Controle := IDCtrl;
    //
///
///
////////////////////////////////////////////////////////////////////////////////
  (*37*) // toma4
    if TCTeToma(toma99_toma) = ctetomaOutros then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ctecab2toma04', False, [
      'FatID', 'FatNum', 'Empresa',
      'enderToma_xLgr', 'enderToma_nro', 'enderToma_xCpl',
      'enderToma_xBairro', 'enderToma_cMun', 'enderToma_xMun',
      'enderToma_CEP', 'enderToma_UF', 'enderToma_cPais',
      'enderToma_xPais', 'enderToma_email'], [
      'Controle'], [
      FatID, FatNum, Empresa,
      enderToma_xLgr, enderToma_nro, enderToma_xCpl,
      enderToma_xBairro, enderToma_cMun, enderToma_xMun,
      enderToma_CEP, enderToma_UF, enderToma_cPais,
      enderToma_xPais, enderToma_email], [
      Controle], True);
    end;
  end;
///
///
////////////////////////////////////////////////////////////////////////////////
  (*91*) // obsCont
  ReopenFrtFOCAtrDef(FatNum);
  while not QrFrtFOCAtrDef.Eof do
  begin
    SQLType        := SQLType;
    Controle       := 0;
    xCampo         := ERx(0, 92, QrFrtFOCAtrDefNO_CAD.Value);
    xTexto         := ERx(0, 93, QrFrtFOCAtrDefNO_ITS.Value);
    //
    Controle := UMyMod.BPGS1I32('cteit2obscont', 'Controle', '', '', tsPos, SQLType, Controle);
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cteit2obscont', False, [
    'xCampo', 'xTexto'], [
    'FatID', 'FatNum', 'Empresa', 'Controle'], [
    xCampo, xTexto], [
    FatID, FatNum, Empresa, Controle], True);
    // then;
    //
    QrFrtFOCAtrDef.Next;
  end;
  (*142*)// ctecab1exped
  if QrFrtFatCabExpedidor.Value <> 0 then
  begin
    ReopenEntiCTe(QrFrtFatCabExpedidor.Value);
    SQLType        := SQLType;
    Controle       := QrFrtFatCabCodigo.Value;
    CNPJ           := '';
    CPF            := '';
    if QrEntiCTeTipo.Value = 0 then
      CNPJ       := ERx(0, 143, Geral.SoNumero_TT(QrEntiCTeCNPJ.Value))
    else
      CPF        := ERx(0, 144, Geral.SoNumero_TT(QrEntiCTeCPF.Value));
    //
    IE             := ERx(0, 145, QrEntiCTeIE.Value);
    xNome          := ERx(0, 146, QrEntiCTexNome.Value);
    fone           := ERx(0, 147, QrEntiCTefone.Value);
   (*148*) // Sub Cabecalho enderExped
      xLgr           := ERx(0, 149, QrEntiCTexLgr.Value);
      nro            := ERx(0, 150, QrEntiCTeNro.Value);
      xCpl           := ERx(0, 151, QrEntiCTexCpl.Value);
      xBairro        := ERx(0, 152, QrEntiCTexBairro.Value);
      cMun           := ERi(0, 153, Trunc(QrEntiCTeCMun.Value));
      xMun           := ERx(0, 154, QrEntiCTexMun.Value);
      CEP            := ERi(0, 155, Trunc(QrEntiCTeCEP.Value));
      UFx            := DefDomX(0, 156, QrEntiCTeUF.Value);
      cPais          := ERi(0, 157, Trunc(QrEntiCTeCPais.Value));
      xPais          := ERx(0, 158, QrEntiCTexPais.Value);
      email          := ERx(0, 159, QrEntiCTeEmail.Value);
    //Controle := UMyMod.BPGS1I32('ctecab1exped', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctecab1exped', False, [
    'FatID', 'FatNum', 'Empresa',
    'CNPJ', 'CPF', 'IE',
    'xNome', 'fone', 'xLgr',
    'nro', 'xCpl', 'xBairro',
    'cMun', 'xMun', 'CEP',
    'UF', 'cPais', 'xPais',
    'email'], [
    'Controle'], [
    FatID, FatNum, Empresa,
    CNPJ, CPF, IE,
    xNome, fone, xLgr,
    nro, xCpl, xBairro,
    cMun, xMun, CEP,
    UFx, cPais, xPais,
    email], [
    Controle], True);
  end;
  (*160*)// ctecab1receb
  if QrFrtFatCabRecebedor.Value <> 0 then
  begin
    ReopenEntiCTe(QrFrtFatCabRecebedor.Value);
    SQLType        := SQLType;
    Controle       := QrFrtFatCabCodigo.Value;
    CNPJ           := '';
    CPF            := '';
    if QrEntiCTeTipo.Value = 0 then
      CNPJ       := ERx(0, 161, Geral.SoNumero_TT(QrEntiCTeCNPJ.Value))
    else
      CPF        := ERx(0, 162, Geral.SoNumero_TT(QrEntiCTeCPF.Value));
    //
    IE             := ERx(0, 163, QrEntiCTeIE.Value);
    xNome          := ERx(0, 164, QrEntiCTexNome.Value);
    fone           := ERx(0, 165, QrEntiCTefone.Value);
   (*166*) // Sub Cabecalho enderReceb
      xLgr           := ERx(0, 167, QrEntiCTexLgr.Value);
      nro            := ERx(0, 168, QrEntiCTeNro.Value);
      xCpl           := ERx(0, 169, QrEntiCTexCpl.Value);
      xBairro        := ERx(0, 170, QrEntiCTexBairro.Value);
      cMun           := ERi(0, 171, Trunc(QrEntiCTeCMun.Value));
      xMun           := ERx(0, 172, QrEntiCTexMun.Value);
      CEP            := ERi(0, 173, Trunc(QrEntiCTeCEP.Value));
      UFx            := DefDomX(0, 174, QrEntiCTeUF.Value);
      cPais          := ERi(0, 175, Trunc(QrEntiCTeCPais.Value));
      xPais          := ERx(0, 176, QrEntiCTexPais.Value);
      email          := ERx(0, 177, QrEntiCTeEmail.Value);
    //Controle := UMyMod.BPGS1I32('ctecab1exped', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctecab1receb', False, [
    'FatID', 'FatNum', 'Empresa',
    'CNPJ', 'CPF', 'IE',
    'xNome', 'fone', 'xLgr',
    'nro', 'xCpl', 'xBairro',
    'cMun', 'xMun', 'CEP',
    'UF', 'cPais', 'xPais',
    'email'], [
    'Controle'], [
    FatID, FatNum, Empresa,
    CNPJ, CPF, IE,
    xNome, fone, xLgr,
    nro, xCpl, xBairro,
    cMun, xMun, CEP,
    UFx, cPais, xPais,
    email], [
    Controle], True);
  end;
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  (*211*)// cteit2comp Componentes do Valor da Presta��o
  ReopenFrtFatComp(FatNum);
  QrFrtFatComp.First;
  while not QrFrtFatComp.Eof do
  begin
    //Controle := UMyMod.BPGS1I32('cteit3Comp', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
    Controle := QrFrtFatCompControle.Value;
    xNome    := ERx(0, 212, QrFrtFatCompxNome.Value);
    vComp    := ERf(0, 213, QrFrtFatCompvComp.Value, 2);
    //
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cteit2comp', False, [
    'FatID', 'FatNum', 'Empresa',
    'xNome', 'vComp'], [
    'Controle'], [
    FatID, FatNum, Empresa,
    xNome, vComp], [
    Controle], True);
    //
    QrFrtFatComp.Next;
  end;
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  if (TCTeTpCTe(ide_TpCTe) = ctetpcteNormal)
  or (TCTeTpCTe(ide_TpCTe) = ctetpcteSubstituto) then
  begin
    (*257*)// cteit3infq Tabela Separada
    ReopenFrtFatInfQ(FatNum);
    QrFrtFatInfQ.First;
    while not QrFrtFatInfQ.Eof do
    begin
      //Controle := UMyMod.BPGS1I32('cteit3infq', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
      Controle := QrFrtFatInfQControle.Value;
      cUnid    := ERi(0, 258, QrFrtFatInfQCUnid.Value);
      tpMed    := ERx(0, 259, QrFrtFatInfQNO_CUnid.Value);
      qCarga   := ERf(0, 260, QrFrtFatInfQQCarga.Value, 4);
      //
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cteit3infq', False, [
      'FatID', 'FatNum', 'Empresa',
      'cUnid', 'tpMed', 'qCarga'], [
      'Controle'], [
      FatID, FatNum, Empresa,
      cUnid, tpMed, qCarga], [
      Controle], True);
      //
      QrFrtFatInfQ.Next;
    end;
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  (*261*)//infDoc : Documentos transportados pelo CT-e
  ///
  ////////////////////////////////////////////////////////////////////////////////
    (*262*)// infNF => usar frtfatinfdoc - Tabela Separada
    (*297*)// infNFe => usar frtfatinfdoc - Tabela Separada
    (*319*)// infOutros => usar frtfatinfdoc - Tabela Separada
    ReopenFrtFatInfDoc(FatNum);
    QrFrtFatInfDoc.First;
    while not QrFrtFatInfDoc.Eof do
    begin
      InfNFe_chave         := '';
      InfNFe_PIN           := 0;
      InfNFe_dPrev         := '0000-00-00';
      InfNF_nRoma          := '';
      InfNF_nPed           := '';
      InfNF_mod            := 0;
      InfNF_serie          := '';
      InfNF_nDoc           := '';
      InfNF_dEmi           := '0000-00-00';
      InfNF_vBC            := 0;
      InfNF_vICMS          := 0;
      InfNF_vBCST          := 0;
      InfNF_vST            := 0;
      InfNF_vProd          := 0;
      InfNF_vNF            := 0;
      InfNF_nCFOP          := 0;
      InfNF_nPeso          := 0;
      InfNF_PIN            := 0;
      InfNF_dPrev          := '0000-00-00';
      InfOutros_tpDoc      := 0;
      InfOutros_descOutros := '';
      InfOutros_nDoc       := '';
      InfOutros_dEmi       := '0000-00-00';
      InfOutros_vDocFisc   := 0;
      InfOutros_dPrev      := '0000-00-00';
      //Controle := UMyMod.BPGS1I32('cteit2infdoc', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
      Controle             := QrFrtFatInfDocControle.Value;
      MyTpDocInf           := QrFrtFatInfDocMyTpDocInf.Value;
      case TCTeMyTpDocInf(MyTpDocInf) of
        ctemtdiNFe:
        begin
          InfNFe_chave         := ERx(0, 298, QrFrtFatInfDocInfNFe_chave.Value);
          InfNFe_PIN           := ERi(0, 299, QrFrtFatInfDocInfNFe_PIN.Value);
          InfNFe_dPrev         := ERx(0, 230, Geral.FDT(QrFrtFatInfDocInfNFe_dPrev.Value, 1));
        end;
        ctemtdiNFsOld:
        begin
          InfNF_nRoma          := ERx(0, 263, QrFrtFatInfDocInfNF_nRoma.Value);
          InfNF_nPed           := ERx(0, 264, QrFrtFatInfDocInfNF_nPed.Value);
          InfNF_mod            := ERi(0, 265, QrFrtFatInfDocInfNF_mod.Value);
          InfNF_serie          := ERx(0, 266, QrFrtFatInfDocInfNF_serie.Value);
          InfNF_nDoc           := ERx(0, 267, QrFrtFatInfDocInfNF_nDoc.Value);
          InfNF_dEmi           := ERx(0, 268, Geral.FDT(QrFrtFatInfDocInfNF_dEmi.Value, 1));
          InfNF_vBC            := ERf(0, 269, QrFrtFatInfDocInfNF_vBC.Value, 2);
          InfNF_vICMS          := ERf(0, 270, QrFrtFatInfDocInfNF_vICMS.Value, 2);
          InfNF_vBCST          := ERf(0, 271, QrFrtFatInfDocInfNF_vBCST.Value, 2);
          InfNF_vST            := ERf(0, 272, QrFrtFatInfDocInfNF_vST.Value, 2);
          InfNF_vProd          := ERf(0, 273, QrFrtFatInfDocInfNF_vProd.Value, 2);
          InfNF_vNF            := ERf(0, 274, QrFrtFatInfDocInfNF_vNF.Value, 2);
          InfNF_nCFOP          := ERi(0, 275, QrFrtFatInfDocInfNF_nCFOP.Value);
          InfNF_nPeso          := ERf(0, 276, QrFrtFatInfDocInfNF_nPeso.Value, 3);
          InfNF_PIN            := ERi(0, 277, QrFrtFatInfDocInfNF_PIN.Value);
          InfNF_dPrev          := ERx(0, 278, Geral.FDT(QrFrtFatInfDocInfNF_dPrev.Value, 1));
        end;
        ctemtdiOutrosDoc:
        begin
          InfOutros_tpDoc      := ERi(0, 320, QrFrtFatInfDocInfOutros_tpDoc.Value);
          InfOutros_descOutros := ERx(0, 321, QrFrtFatInfDocInfOutros_descOutros.Value);
          InfOutros_nDoc       := ERx(0, 322, QrFrtFatInfDocInfOutros_nDoc.Value);
          InfOutros_dEmi       := ERx(0, 323, Geral.FDT(QrFrtFatInfDocInfOutros_dEmi.Value, 1));
          InfOutros_vDocFisc   := ERf(0, 324, QrFrtFatInfDocInfOutros_vDocFisc.Value, 2);
          InfOutros_dPrev      := ERx(0, 325, Geral.FDT(QrFrtFatInfDocInfOutros_dPrev.Value, 1));
        end
        else Geral.MB_Erro('"MyTpDocInf" - Nenhum tipo de documento foi definido!');
      end;
      //
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cteit2infdoc', False, [
      'FatID', 'FatNum', 'Empresa',
      'MyTpDocInf', 'InfNFe_chave', 'InfNFe_PIN',
      'InfNFe_dPrev', 'InfNF_nRoma', 'InfNF_nPed',
      'InfNF_mod', 'InfNF_serie', 'InfNF_nDoc',
      'InfNF_dEmi', 'InfNF_vBC', 'InfNF_vICMS',
      'InfNF_vBCST', 'InfNF_vST', 'InfNF_vProd',
      'InfNF_vNF', 'InfNF_nCFOP', 'InfNF_nPeso',
      'InfNF_PIN', 'InfNF_dPrev', 'InfOutros_tpDoc',
      'InfOutros_descOutros', 'InfOutros_nDoc', 'InfOutros_dEmi',
      'InfOutros_vDocFisc', 'InfOutros_dPrev'], [
      'Controle'], [
      FatID, FatNum, Empresa,
      MyTpDocInf, InfNFe_chave, InfNFe_PIN,
      InfNFe_dPrev, InfNF_nRoma, InfNF_nPed,
      InfNF_mod, InfNF_serie, InfNF_nDoc,
      InfNF_dEmi, InfNF_vBC, InfNF_vICMS,
      InfNF_vBCST, InfNF_vST, InfNF_vProd,
      InfNF_vNF, InfNF_nCFOP, InfNF_nPeso,
      InfNF_PIN, InfNF_dPrev, InfOutros_tpDoc,
      InfOutros_descOutros, InfOutros_nDoc, InfOutros_dEmi,
      InfOutros_vDocFisc, InfOutros_dPrev], [
      Controle], True);
      //
      QrFrtFatInfDoc.Next;
    end;
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  ///
    (*360*)// seg => usar frtfatseg - Tabela Separada
    ReopenFrtFatSeg(FatNum);
    QrFrtFatSeg.First;
    while not QrFrtFatSeg.Eof do
    begin
      (*****)Controle       := QrFrtFatSegControle.Value;
      respSeg        := ERi(0, 361, QrFrtFatSegrespSeg.Value);
      xSeg           := ERx(0, 362, QrFrtFatSegxSeg.Value);
      nApol          := ERx(0, 363, QrFrtFatSegnApol.Value);
      nAver          := ERx(0, 364, QrFrtFatSegnAver.Value);
      vCarga         := ERf(0, 365, QrFrtFatSegvCarga.Value, 2);
      //Controle := UMyMod.BPGS1I32('cteit2seg', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cteit2seg', False, [
      'FatID', 'FatNum', 'Empresa',
      'respSeg', 'xSeg', 'nApol',
      'nAver', 'vCarga'], [
      'Controle'], [
      FatID, FatNum, Empresa,
      respSeg, xSeg, nApol,
      nAver, vCarga], [
      Controle], True);
      //
      QrFrtFatSeg.Next;
    end;
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  ///
    (*369*)// peri => usar frtfatperi - Tabela Separada
    ReopenFrtFatPeri(FatNum);
    QrFrtFatPeri.First;
    while not QrFrtFatPeri.Eof do
    begin
      (*****)Controle       := QrFrtFatPeriControle.Value;
      nONU           := ERx(0, 370, QrFrtFatPerinONU.Value);
      xNomeAE        := ERx(0, 371, QrFrtFatPerixNomeAE.Value);
      xClaRisco      := ERx(0, 372, QrFrtFatPerixClaRisco.Value);
      grEmb          := ERx(0, 373, QrFrtFatPeriPERI_grEmb.Value);
      qTotProd       := ERx(0, 374, QrFrtFatPeriqTotProd.Value);
      qVolTipo       := ERx(0, 375, QrFrtFatPeriqVolTipo.Value);
      pontoFulgor    := ERx(0, 376, QrFrtFatPeripontoFulgor.Value);
      //Controle := UMyMod.BPGS1I32('cteit2peri', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cteit2peri', False, [
      'FatID', 'FatNum', 'Empresa',
      'nONU', 'xNomeAE', 'xClaRisco',
      'grEmb', 'qTotProd', 'qVolTipo',
      'pontoFulgor'], [
      'Controle'], [
      FatID, FatNum, Empresa,
      nONU, xNomeAE, xClaRisco,
      grEmb, qTotProd, qVolTipo,
      pontoFulgor], [
      Controle], True);
      //
      QrFrtFatPeri.Next;
    end;
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  ///
    (*390*)// dup => usar frtfat??? - Dados das duplicatas
    ReopenFrtFatDup(FatNum, TabLctA);
    QrFrtFatDup.First;
    while not QrFrtFatDup.Eof do
    begin
      (*****)Controle       := UMyMod.BPGS1I32('cteit3dup', 'Controle', '', '', tsPos, SQLType, 0);
      nDup           := ERx(0, 391, QrFrtFatDupDuplicata.Value);
      dVenc          := ERx(0, 392, Geral.FDT(QrFrtFatDupVencimento.Value, 1));
      vDup           := ERf(0, 393, QrFrtFatDupCredito.Value, 2);
      Lancto         := QrFrtFatDupControle.Value;
      //
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cteit3dup', False, [
      'FatID', 'FatNum', 'Empresa',
      'nDup', 'dVenc', 'vDup',
      'Lancto'], [
      'Controle'], [
      FatID, FatNum, Empresa,
      nDup, dVenc, vDup,
      Lancto], [
      Controle], True);
      //
      QrFrtFatDup.Next;
    end;
  end; // Fim CTe Normal e Substituicao
///
///
///
////////////////////////////////////////////////////////////////////////////////
///
///
  (*415*)// autXML => usar frtfat??? - Autorizados para download do XML
  ReopenFrtFatAutXml(FatNum);
  QrFrtFatAutXml.First;
  while not QrFrtFatAutXml.Eof do
  begin
    //(*****)Controle       := UMyMod.BPGS1I32('cteit3AutXml', 'Controle', '', '', tsPos, SQLType, 0);
    Controle              := QrFrtFatAutXmlControle.Value;
    AddForma              := QrFrtFatAutXmlAddForma.Value;
    AddIDCad              := QrFrtFatAutXmlAddIDCad.Value;
    Tipo                  := QrFrtFatAutXmlTipo.Value;
    CNPJ           := ERx(0, 416, QrFrtFatAutXmlCNPJ.Value);
    CPF            := ERx(0, 417, QrFrtFatAutXmlCPF.Value);
    //
    //Controle := UMyMod.BPGS1I32('cteit1autxml', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cteit1autxml', False, [
    'FatID', 'FatNum', 'Empresa',
    'AddForma', 'AddIDCad', 'Tipo',
    'CNPJ', 'CPF'], [
    'Controle'], [
    FatID, FatNum, Empresa,
    AddForma, AddIDCad, Tipo,
    CNPJ, CPF], [
    Controle], True);
    //
    QrFrtFatAutXml.Next;
  end;
///
///
////////////////////////////////////////////////////////////////////////////////
///
  case TCTeModal(ide_Modal) of
    //ctemodalIndefinido=0,
    ctemodalRodoviario:
    begin
      ReopenTrnsCad(Empresa);
      Controle            := IDCtrl;
      RNTRC               := ERx(1, 02, QrTrnsCadRNTRC.Value);
      dPrev               := ERx(1, 03, Geral.FDT(QrFrtFatCabDataPrev.Value, 1));
      lota                := ERi(1, 04, QrFrtFatCabLota.Value);
      CIOT                := ERi64(1, 05, QrFrtFatCabCIOT.Value);
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctemorodocab', False, [
      'FatID', 'FatNum', 'Empresa',
      'RNTRC', 'dPrev', 'lota',
      'CIOT'], [
      'Controle'], [
      FatID, FatNum, Empresa,
      RNTRC, dPrev, lota,
      CIOT], [
      Controle], True); // then
      //
      (*16*) // Vale pedagio
      ReopenFrtFatMoRodoVPed(FatNum);
      while not QrFrtFatMoRodoVPed.Eof do
      begin
        Conta          := QrFrtFatMoRodoVPedControle.Value;
        CNPJForn       := ERx(1, 17, Geral.SoNumero_TT(QrFrtFatMoRodoVPedCNPJForn.Value));
        nCompra        := ERx(1, 18, QrFrtFatMoRodoVPednCompra.Value);
        CNPJPg         := Geral.SoNumero_TT(QrFrtFatMoRodoVPedCNPJPg.Value);
        if CNPJPg <> '' then
          CNPJPg         := ERx(1, 19, CNPJPg)
        else
          CNPJPg         := '';
        vValePed       := ERf(1, 20, QrFrtFatMoRodoVPedvValePed.Value, 2);
        //
        //Conta := UMyMod.BPGS1I32('ctemorodovped', 'Conta', '', '', tsPos, stIns, Conta);
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctemorodovped', False, [
        'FatID', 'FatNum', 'Empresa',
        'Controle', 'CNPJForn', 'nCompra',
        'CNPJPg', 'vValePed'], [
        'Conta'], [
        FatID, FatNum, Empresa,
        Controle, CNPJForn, nCompra,
        CNPJPg, vValePed], [
        Conta], True);
        //
        QrFrtFatMoRodoVPed.Next;
      end;
      //
      (*21*) // veic - Dados dos Veiculos
      ReopenFrtFatMoRodoVeic(FatNum);
      while not QrFrtFatMoRodoVeic.Eof do
      begin
        Conta          := QrFrtFatMoRodoVeicControle.Value;
        cInt           := ERi(1, 22, QrFrtFatMoRodoVeicCInt.Value);
        ReopenTrnsIts(cInt);
        RENAVAM        := ERx(1, 23, QrTrnsItsRENAVAM.Value);
        placa          := ERx(1, 24, Geral.SoNumeroELetra_TT(QrTrnsItsplaca.Value));
        tara           := ERi(1, 25, QrTrnsItsTaraKg.Value);
        capKG          := ERi(1, 26, QrTrnsItscapKG.Value);
        capM3          := ERi(1, 27, QrTrnsItscapM3.Value);
        if QrTrnsItsCodigo.Value <> Empresa then
          VeicTpProp       := ERx(1, 28, 'T')
        else
        begin
          VeicTpProp       := ERx(1, 28, 'P');
        tpVeic         := ERi(1, 29, QrTrnsItstpVeic.Value);
        tpRod          := ERi(1, 30, QrTrnsItstpRod.Value);
        tpCar          := ERi(1, 31, QrTrnsItstpCar.Value);
        UFx            := ERx(1, 32, QrTrnsItsUF.Value);
          //
        end;
        //
        //Conta := UMyMod.BPGS1I32('ctemorodoVeic', 'Conta', '', '', tsPos, stIns, Conta);
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctemorodoveic', False, [
        'FatID', 'FatNum', 'Empresa',
        'Controle', 'cInt', 'RENAVAM',
        'placa', 'tara', 'capKG',
        'capM3', 'tpProp', 'tpVeic',
        'tpRod', 'tpCar', 'UF'], [
        'Conta'], [
        FatID, FatNum, Empresa,
        Controle, cInt, RENAVAM,
        placa, tara, capKG,
        capM3, VeicTpProp, tpVeic,
        tpRod, tpCar, UFx], [
        Conta], True);
        (*33*) // prop
        if (QrTrnsItsCodigo.Value <> 0) and (QrTrnsItsCodigo.Value <> Empresa) then
        begin
          ReopenTerceiro(QrTrnsItsCodigo.Value);
          CPF            := '';
          CNPJ           := '';
          if QrTerceiroTipo.Value = 1 then
            CPF          := ERx(1, 34, Geral.SoNumero_TT(QrTerceiroCPF.Value))
          else
            CNPJ         := ERx(1, 35, Geral.SoNumero_TT(QrTerceiroCNPJ.Value));
          RNTRC          := ERx(1, 36, QrTrnsCadRNTRC.Value);
          xNome          := ERx(1, 37, QrTerceiroNO_ENT.Value);
          IE             := ERx(1, 38, QrTerceiroIE.Value);
          UFx            := ERx(1, 39, QrTerceiroUF.Value);
          PropTpProp     := ERi(1, 40, QrTrnsCadTpProp.Value);
          //Conta := UMyMod.BPGS1I32('ctemorodoprop', 'Conta', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
          //if
          UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctemorodoprop', False, [
          'FatID', 'FatNum', 'Empresa',
          'Controle', 'CPF', 'CNPJ',
          'xNome', 'IE', 'UF',
          'tpProp', 'RNTRC'], [
          'Conta'], [
          FatID, FatNum, Empresa,
          Controle, CPF, CNPJ,
          xNome, IE, UFx,
          PropTpProp, RNTRC], [
          Conta], True);
        end;
        QrFrtFatMoRodoVeic.Next;
      end;
      //
      (*41*) // lacRodo - Lacres
      ReopenFrtFatMoRodoLacr(FatNum);
      while not QrFrtFatMoRodoLacr.Eof do
      begin
        Conta          := QrFrtFatMoRodoLacrControle.Value;
        nLacre         := ERx(1, 42, QrFrtFatMoRodoLacrnLacre.Value);
        //
        //Conta := UMyMod.BPGS1I32('ctemorodolacr', 'Conta', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
        //if
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctemorodolacr', False, [
        'FatID', 'FatNum', 'Empresa',
        'Controle', 'nLacre'], [
        'Conta'], [
        FatID, FatNum, Empresa,
        Controle, nLacre], [
        Conta], True);
        //
        QrFrtFatMoRodoLacr.Next;
      end;
      //
      (*43*) // moto - Motoristas
      ReopenFrtFatMoRodoMoto(FatNum);
      while not QrFrtFatMoRodoMoto.Eof do
      begin
        Conta          := QrFrtFatMoRodoMotoControle.Value;
        xNome          := ERx(1, 44, QrFrtFatMoRodoMotoxNome.Value);
        CPF            := ERx(1, 45, Geral.SoNumero_TT(QrFrtFatMoRodoMotoCPF.Value));
        //
        //Conta := UMyMod.BPGS1I32('ctemorodomoto', 'Conta', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
        //if
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ctemorodomoto', False, [
        'FatID', 'FatNum', 'Empresa',
        'Controle', 'xNome', 'CPF'], [
        'Conta'], [
        FatID, FatNum, Empresa,
        Controle, xNome, CPF], [
        Conta], True);
        //
        QrFrtFatMoRodoMoto.Next;
      end;
      //
    end;
    //ctemodalAereo=2,
    //ctemodalAquaviario=3, ctemodalFerroviario=4,
    //ctemodalDutoviario=5, ctemodalMultimodal=6
    else
    begin
      Result := False;
      Geral.MB_Aviso('Tipo de Modal n�o implementado!');
    end;
  end;
  if Trim(FMsg) <> '' then
  begin
    Geral.MB_Aviso(FMsg);
  end else
    Result := True;
end;

function TDmCTe_0000.InsereDadosDeFaturaNasTabelasCTe(
  FatID, FatNum, Empresa, SerieCTe, NumeroCTe: Integer; DataFat: TDateTime;
  HoraFat: TTime; LaAviso1, LaAviso2: TLabel): Boolean;
const
  Recria = False; // ou True???
var
  IDCtrl_Anterior, IDCtrl, VersaoCTe: Integer;
  Continua: Boolean;
begin
  Result := False;
  //Dados anteriores
  ReopenCTeCabA(FatID, FatNum, Empresa);
  IDCtrl_Anterior := QrCTeCabAIDCtrl.Value;
  VersaoCTe := CTe_PF.VersaoCTeEmUso();
  //
  Continua := ExcluiDadosTabelasCTe(FatID, FatNum);
  //
(*
  SQLDeleteTabela('ctecaba'),
  //
*)
  if Continua then
    Continua := InsereDadosCteCab(FatID, FatNum, Empresa, IDCtrl_Anterior,
      VersaoCTe, SerieCTe, NumeroCTe, Recria, DataFat, HoraFat, IDCtrl);
  if Continua then
    Continua := StepCTeCab(FatID, FatNum, Empresa,
    ctemystatusCTeDados, LaAviso1, LaAviso2);

(*
  SQLDeleteTabela('ctecab1exped'),
  SQLDeleteTabela('ctecab1receb'),
  SQLDeleteTabela('ctecab1receb'),
  SQLDeleteTabela('ctecab2locent'),
  SQLDeleteTabela('ctecab2toma04'),
  //
  SQLDeleteTabela('cteit1autxml'),
  SQLDeleteTabela('cteit2comp'),
  //SQLDeleteTabela('cteit2infctesub'),
  SQLDeleteTabela('cteit2obscont'),
  SQLDeleteTabela('cteit2obsfisco'),
  SQLDeleteTabela('cteit2peri'),
  SQLDeleteTabela('cteit2seg'),
  SQLDeleteTabela('cteit3dup'),
  SQLDeleteTabela('cteit3infdoc'),
  SQLDeleteTabela('cteit3infq'),
*)
  Result := Continua = True;
end;

function TDmCTe_0000.LocalizaCTeInfoEveCan(const ChCTe: String; var Id,
  xJust: String; var cJust: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Id    := '';
  xJust := '';
  cJust := 0;
  Result := False;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cab.Id, can.nJust, can.xJust',
    'FROM cteevercab cab',
    'LEFT JOIN cteevercan can ON can.Controle = cab.Controle',
    'WHERE cab.chCTe="' + ChCTe + '"',
    'GROUP BY cab.Id',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Id     := Qry.FieldByName('Id').AsString;
      xJust  := Qry.FieldByName('xJust').AsString;
      cJust  := Qry.FieldByName('nJust').AsInteger;
      Result := True;
    end;
  finally
    Qry.Free;
  end;
end;

function TDmCTe_0000.LocalizaCTeInfoEveCCe(const ChCTe: String;
  nSeqEvento: Integer; var CNPJ, CPF: String; var dhEvento: TDateTime;
  var dhEventoTZD, verEvento: Double; var nCondUso: Integer;
  var CTeEveRCCeConta: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  CNPJ            := '';
  CPF             := '';
  dhEvento        := 0;
  dhEventoTZD     := 0;
  verEvento       := 0;
  nCondUso        := 0;
  CTeEveRCCeConta := 0;
  //
  Result   := False;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cab.Controle,',
    'cab.CNPJ, cab.CPF, cab.dhEvento, cab.TZD_UTC, cab.verEvento,',
    'cce.nCondUso, cce.Conta, cab.nSeqEvento ',
    'FROM cteevercab cab ',
    'LEFT JOIN cteevercce cce ON cce.Controle = cab.Controle',
    'WHERE cab.chCTe="' + ChCTe + '"',
    'AND cab.nSeqEvento=' + Geral.FF0(nSeqEvento),
    'ORDER BY cce.Conta DESC',
    '']);
    if Qry.RecordCount > 0 then
    begin
      CNPJ            := Qry.FieldByName('CNPJ').AsString;
      CPF             := Qry.FieldByName('CPF').AsString;
      dhEvento        := Qry.FieldByName('dhEvento').AsDateTime;
      dhEventoTZD     := Qry.FieldByName('TZD_UTC').AsFloat;
      verEvento       := Qry.FieldByName('verEvento').AsFloat;
      nCondUso        := Qry.FieldByName('nCondUso').AsInteger;
      CTeEveRCCeConta := Qry.FieldByName('Conta').AsInteger;
      //
      Result := True;
    end;
  finally
    Qry.Free;
  end;
end;

function TDmCTe_0000.LocalizaCTeInfoEveEPEC(const ChCTe: String; var CNPJ, Id,
  xJust: String; var cJust, CTeEveREPECConta: Integer; var vICMS, vTPrest, vCarga:
  Double; var CodInfoTomad, Toma04_toma: Integer; var Toma04_UF, Toma04_CNPJ,
  Toma04_CPF, Toma04_IE: String; var Modal: Integer; var UFIni, UFFim: String;
  var verEvento, VersaoEnv: Double; var dhEvento: TDateTime):
  Boolean;
var
  Qry: TmySQLQuery;
begin
  Id    := '';
  xJust := '';
  cJust := 0;
  CTeEveREPECConta := 0;
  Result := False;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cab.CNPJ, cab.Id, cab.verEvento, cab.versao, ',
    'cab.dhEvento, epec.nJust, epec.xJust, epec.Conta, ',
    'epec.vICMS, epec.vTPrest, epec.vCarga, epec.CodInfoTomad, ',
    'epec.Toma04_toma, epec.Toma04_UF, epec.Toma04_CNPJ, ',
    'epec.Toma04_CPF, epec.Toma04_IE, epec.Modal, ',
    'epec.UFIni, epec.UFFim ',
    'FROM cteevercab cab',
    'LEFT JOIN cteeverepec epec ON epec.Controle = cab.Controle',
    'WHERE cab.chCTe="' + ChCTe + '"',
    'GROUP BY cab.Id',
    '']);
    if Qry.RecordCount > 0 then
    begin
      CNPJ             := Qry.FieldByName('CNPJ').AsString;
      Id               := Qry.FieldByName('Id').AsString;
      xJust            := Qry.FieldByName('xJust').AsString;
      cJust            := Qry.FieldByName('nJust').AsInteger;
      dhEvento         := Qry.FieldByName('dhEvento').AsDateTime;
      CTeEveREPECConta := Qry.FieldByName('Conta').AsInteger;
      vICMS            := Qry.FieldByName('vICMS').AsFloat;
      vTPrest          := Qry.FieldByName('vTPrest').AsFloat;
      vCarga           := Qry.FieldByName('vCarga').AsFloat;
      CodInfoTomad     := Qry.FieldByName('CodInfoTomad').AsInteger;
      Toma04_toma      := Qry.FieldByName('Toma04_toma').AsInteger;
      Toma04_UF        := Qry.FieldByName('Toma04_UF').AsString;
      Toma04_CNPJ      := Qry.FieldByName('Toma04_CNPJ').AsString;
      Toma04_CPF       := Qry.FieldByName('Toma04_CPF').AsString;
      Toma04_IE        := Qry.FieldByName('Toma04_IE').AsString;
      Modal            := Qry.FieldByName('Modal').AsInteger;
      UFIni            := Qry.FieldByName('UFIni').AsString;
      UFFim            := Qry.FieldByName('UFFim').AsString;
      verEvento        := Qry.FieldByName('verEvento').AsFloat;
      VersaoEnv        := Qry.FieldByName('versao').AsFloat;
      //
      Result := True;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TDmCTe_0000.MensagemCalcularManualCST(CST: Integer; Texto: String);
begin
    Geral.MB_Aviso('Calcule os impostos manualmente, pois o CST do ' +
    'ICMS selecionado (' + IntToStr(CST) + ' - ' + Texto +
    ') n�o tem seu c�culo implementado ' +
    'neste aplicativo! caso deseje automatizar o c�culo '  +
    'INFORME A DERMATEK SOBRE A F�RMULA DE C�LCULO');
end;

function TDmCTe_0000.MensagemDeID_CTe(Grupo, Codigo: Integer; Valor, Texto:
  String; Seq_Chamou: Integer): String;
var
  Txt, Obs: String;
begin
  if Valor = '' then
    Txt := '(valor n�o definido)'
  else
    Txt := Valor;
  //
  Result := 'TDmCTe_0000.MensagemDeID_CTe()'+sLineBreak+
            'Seq Chamada: ' + FormatFloat('0', Seq_Chamou) + sLineBreak +
            'N�o foi poss�vel definir o item abaixo pelo layout da CT-e:' +
  sLineBreak +
  sLineBreak + 'Campo: ' + QrCTeLayIDescricao.Value +
  sLineBreak + 'Valor: ' + Txt;
  //
  //if Trim(Texto) <> '' then
  Result := Result + sLineBreak + 'Motivo: ' + Texto;
  //
  Obs := Trim(QrCTeLayIObservacao.Value);
  if Obs <> '' then
  Result := Result + sLineBreak + 'Observa��es: ' + Obs;
  //
  Result := Result + sLineBreak + sLineBreak +
    'ID MOC # ' + Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo);
end;

function TDmCTe_0000.MontaID_Inutilizacao(const cUF, Ano, emitCNPJ, Modelo,
  Serie, nNFIni, nNFFim: String; var Id: String): Boolean;
var
  K: Integer;
begin
  Id := 'ID' + cUF + (*Ano +*) emitCNPJ + Modelo + Serie +
    XXe_PF.StrZero(StrToInt(nNFIni), 9,0) +
    XXe_PF.StrZero(StrToInt(nNFFim), 9,0);
  K := Length(Id);
  if K in ([39, 40, 41]) then
    Result := True
  else begin
    Result := False;
    Geral.MB_Erro('ID de inutiliza��o com tamanho inv�lido: "' +
    Id + '". Deveria ter 39, 40 ou 41 carateres e tem ' + IntToStr(K) + '.');
  end;
end;

function TDmCTe_0000.ObtemDirXML(const Ext: String; var Dir: String;
  Assinado: Boolean): Boolean;
var
  Titulo, Descri: String;
begin
  //Result := False;
{
6.2 Padr�o de Nomes para os Arquivos
Visando facilitar o processo de guarda dos arquivos pelos leg�timos
interessados, criou-se um padr�o de nome para os diversos tipos de arquivos
utilizados pelo sistema CT-e. S�o eles:
� CT-e: O nome do arquivo ser� a chave de acesso completa com extens�o �-cte.xml�;
� Envio de Lote de CT-e: O nome do arquivo ser� o n�mero do lote com extens�o
�-envlot.xml�;
� Recibo: O nome do arquivo ser� o n�mero do lote com extens�o �-rec.xml�;
� Pedido do Resultado do Processamento do Lote de CT-e: O nome do arquivo ser� o
n�mero do recibo com extens�o �-ped-rec.xml�;
� Resultado do Processamento do Lote de CT-e: O nome do arquivo ser� o n�mero do
recibo com extens�o �-pro-rec.xml�;
� Denega��o de Uso: O nome do arquivo ser� a chave de acesso completa com extens�o
�-den.xml�;
� Pedido de Inutiliza��o de Numera��o: O nome do arquivo ser� composto por: UF +
Ano de inutiliza��o + CNPJ do emitente + Modelo + S�rie + N�mero Inicial + N�mero
Final com extens�o �-ped-inu.xml�;
� Inutiliza��o de Numera��o: O nome do arquivo ser� composto por: Ano de inutiliza��o
+ CNPJ do emitente + Modelo + S�rie + N�mero Inicial + N�mero Final com extens�o
�-inu.xml�;
� Pedido de Consulta Situa��o Atual do CT-e: O nome do arquivo ser� a chave de
acesso completa com extens�o �-ped-sit.xml�;
� Situa��o Atual do CT-e: O nome do arquivo ser� a chave de acesso completa com
extens�o �-sit.xml�;
� Pedido de Consulta do Status do Servi�o: O nome do arquivo ser�:
�AAAAMMDDTHHMMSS� do momento da consulta com extens�o �-ped-sta.xml�;
� Status do Servi�o: O nome do arquivo ser�: �AAAAMMDDTHHMMSS� do momento da
consulta com extens�o �-sta.xml�;
� Pedido de Registro de Evento de CT-e: O nome do arquivo ser� a chave de acesso
completa com extens�o �-ped-eve.xml�;
� Registro de Evento de CT-e: O nome do arquivo ser� a chave de acesso completa com
extens�o �-eve.xml�;
}
{
  CTE_EXT_CTE_XML     = '-cte.xml';
  CTE_EXT_ENV_LOT_XML = '-envlot.xml';
  CTE_EXT_REC_XML     = '-rec.xml';
  CTE_EXT_PED_REC_XML = '-ped-rec.xml';
  CTE_EXT_PRO_REC_XML = '-pro-rec.xml';
  CTE_EXT_DEN_XML     = '-den.xml';
  CTE_EXT_PED_INU_XML = '-ped-inu.xml';
  CTE_EXT_INU_XML     = '-inu.xml';
  CTE_EXT_PED_SIT_XML = '-ped-sit.xml';
  CTE_EXT_SIT_XML     = '-sit.xml';
  CTE_EXT_PED_STA_XML = '-ped-sta.xml';
  CTE_EXT_STA_XML     = '-sta.xml';
  CTE_EXT_PED_EVE_XML = '-ped-eve.xml';
  CTE_EXT_EVE_XML     = '-eve.xml';
}

  Dir := '';
  if Ext = CTE_EXT_CTE_XML  then
  begin
    if Assinado then
      Dir := QrFilialDirCTeAss.Value
    else
      Dir := QrFilialDirCTeGer.Value;
  end else
    if Ext = CTE_EXT_ENV_LOT_XML then
      Dir := QrFilialDirCTeEnvLot.Value
  else
    if Ext = CTE_EXT_REC_XML then
      Dir := QrFilialDirCTeRec.Value
  else
    if Ext = CTE_EXT_PRO_REC_XML then
      Dir := QrFilialDirCTeProRec.Value
  else
    if Ext = CTE_EXT_DEN_XML then
      Dir := QrFilialDirCTeDen.Value
(*
  else
    if Ext = CTE_EXT_PED_CAN_XML then
      Dir := QrFilialDirCTePedCan.Value
  else
    if Ext = CTE_EXT_CAN_XML then
      Dir := QrFilialDirCTeCan.Value
*)
  else
    if Ext = CTE_EXT_EVE_RET_CAN_XML then
      Dir := QrFilialDirCTeCan.Value
  else
    if Ext = CTE_EXT_PED_INU_XML then
      Dir := QrFilialDirCTePedInu.Value
  else
    if Ext = CTE_EXT_INU_XML then
      Dir := QrFilialDirCTeInu.Value
  else
    if Ext = CTE_EXT_PED_SIT_XML then
      Dir := QrFilialDirCTePedSit.Value
  else
    if Ext = CTE_EXT_SIT_XML then
      Dir := QrFilialDirCTeSit.Value
  else
    if Ext = CTE_EXT_PED_STA_XML then
      Dir := QrFilialDirCTePedSta.Value
  else
    if Ext = CTE_EXT_STA_XML then
      Dir := QrFilialDirCTeSta.Value
(*
  else
    if Ext = CTE_EXT_CTE_WEB_XML then
      Dir := QrFilialDirCTeCTERWeb.Value
*)
  else
    if Ext = CTE_EXT_PED_EVE_XML then
      Dir := QrFilialDirCTeEveEnvLot.Value
  else
    if Ext = CTE_EXT_EVE_XML then
      Dir := QrFilialDirCTeEveRetLot.Value
  else
    if Ext = CTE_EXT_EVE_ENV_CCE_XML then
      Dir := QrFilialDirCTeEvePedCCe.Value
  else
    if Ext = CTE_EXT_EVE_RET_CCE_XML then
      Dir := QrFilialDirCTeEveRetCCe.Value
  else
    if Ext = CTE_EXT_EVE_ENV_CAN_XML then
      Dir := QrFilialDirCTeEvePedCan.Value
  else
    if Ext = CTE_EXT_EVE_RET_CAN_XML then
      Dir := QrFilialDirCTeEveRetCan.Value
  else
    if Ext = CTE_EXT_EVE_ENV_EPEC_XML then
      Dir := QrFilialDirCTeEvePedEPEC.Value
  else
    if Ext = CTE_EXT_EVE_RET_EPEC_XML then
      Dir := QrFilialDirCTeEveRetEPEC.Value
(*  else
    if Ext = CTE_EXT_RET_CTE_DES_XML then
      Dir := QrFilialDirCTeRetCTEDes.Value
  else
    if Ext = CTE_EXT_EVE_ENV_MDE_XML then
      Dir := QrFilialDirCTeEvePedMDe.Value
  else
    if Ext = CTE_EXT_EVE_RET_MDE_XML then
      Dir := QrFilialDirCTeEveRetMDe.Value
  else
    if Ext = CTE_EXT_RET_DOW_CTE_DES_XML then
      Dir := QrFilialDirCTeDowCTEDes.Value
  else
    if Ext = CTE_EXT_RET_DOW_CTE_CTE_XML then
      Dir := QrFilialDirCTeDowCTECTE.Value
  else
    if Ext = CTE_EXT_PED_DFE_DIS_INT_XML then
      Dir := QrFilialDirCTeDistDFeInt.Value
  else
    if Ext = CTE_EXT_RET_DFE_DIS_INT_XML then
      Dir := QrFilialDirCTeRetDistDFeInt.Value
  else
    if Ext = CTE_EXT_RET_DOW_CTE_CNF_XML then
      Dir := QrFilialDirCTeDowCTECnf.Value
  else
    if Ext = CTE_EXT_RET_CNF_CTE_CTE_XML then
      Dir := QrFilialDirCTeDowCTECTE.Value
*)
  //

  else
  Geral.MB_Erro('Tipo de (final de) nome de arquivo XML desconhecido: ' + Ext);
  //
  if Dir <> '' then
  begin
    TituloArqXML(Ext, Titulo, Descri);
    Result := Geral.VerificaDir(Dir, '\', Titulo + sLineBreak + Descri, True);
  end else
  begin
    Geral.MB_Aviso(
    'Diret�rio XML n�o definido no cadastro da filial para o tipo: ' + Ext);
    Result := False;
  end;
end;

function TDmCTe_0000.ObtemNome(Tabela: String; Codigo: Integer): String;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Nome ',
    'FROM ' + LowerCase(Tabela),
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    //
    Result := Qry.FieldByName('Nome').AsString;
  finally
    Qry.Free;
  end;
end;

procedure TDmCTe_0000.ObtemValorAproximadoDeTributos(const Fonte, Tabela,
  Ex: Integer; Codigo: Int64; UF: String; const vServ, vDesc: Double;
  var vBasTrib, pTotTrib, vTotTrib: Double; var verTotTrib: String;
  var tpAliqTotTrib: Integer);
const
  Importado = False;
  MsgCab = 'N�o � poss�vel obter o % de impostos da tabela IBPTax!' + sLineBreak;
var
  MsgIts: String;
begin
  pTotTrib      := 0;
  vTotTrib      := 0.0000;
  //tabTotTrib    := TIBPTaxTabs.ibptaxtabNCM;
  verTotTrib    := '';
  tpAliqTotTrib := Integer(TIBPTaxOrig.ibptaxoriNaoInfo);
  //fontTotTrib   := TIBPTaxFont.ibptaxfontNaoInfo;
  //
  if MyObjects.FIC(Fonte <> 1, nil, MsgCab + 'Fonte n�o � o IBPTax!') then Exit;
  if MyObjects.FIC(Tabela > 2, nil, MsgCab + 'Tabela desconhecida!') then Exit;
  if MyObjects.FIC(UF = '', nil, MsgCab + 'UF n�o informada!') then Exit;
  //
    UnDmkDAC_PF.AbreMySQLQuery0(QrIBPTax, DModG.AllID_DB, [
    'SELECT *  ',
    'FROM ibptax ',
    'WHERE Tabela=' + Geral.FF0(Tabela),
    'AND Ex=' + Geral.FF0(Ex),
    'AND Codigo=' + Geral.FF0(Codigo),
    'AND UF = "' + UF + '" ',
    'ORDER BY VigenFim DESC ',
    '']);
    if QrIBPTax.RecordCount > 0 then
    begin
      if not Importado then
      begin
        pTotTrib      := QrIBPTaxAliqNac.Value;
        tpAliqTotTrib := Integer(TIBPTaxOrig.ibptaxoriNacional);
      end else
      begin
        pTotTrib      := QrIBPTaxAliqImp.Value;
        tpAliqTotTrib := Integer(TIBPTaxOrig.ibptaxoriImportado);
      end;
(*==============================================================================
LEI N� 12.741, DE 8 DE DEZEMBRO DE 2012.
DECRETO N� 8.264, DE 5 DE JUNHO DE 2014
� 1�  Em rela��o � estimativa do valor dos tributos referidos no caput, n�o
ser�o computados valores que tenham sido eximidos por for�a de imunidades,
isen��es, redu��es e n�o incid�ncias eventualmente ocorrentes.
================================================================================
Manual de Integra��o de Olho no Imposto - vers�o 0.0.6
b) Quando temos desconto no valor total do cupom como fica?
- No caso de desconto pelo total, para efeitos de c�lculo do valor do imposto o
valor total descontado deve ser atribu�do item a item, e, sobre o valor com
desconto incondicional deve ser aplicada a al�quota aproximada disponibilizada
pelo IBPT
==============================================================================*)
      vBasTrib      := vServ - vDesc;
      vTotTrib      := vBasTrib * pTotTrib / 100;
      //tabTotTrib    := TIBPTaxTabs.ibptaxtabLC116;
      verTotTrib    := QrIBPTaxVersao.Value;
      //fontTotTrib   := TIBPTaxFont.ibptaxfontIBPTax;
    end else
    begin
      Geral.MB_Aviso('N�o foi poss�vel obter o % de impostos da tabela IBPTax!' +
      slineBreak + 'Fonte: ' + Geral.FF0(Fonte) +
      slineBreak + 'Tabela: ' + Geral.FF0(Tabela) +
      slineBreak + 'C�digo: ' + Geral.FF0(Codigo) +
      slineBreak + 'Ex: ' + Geral.FF0(Ex) +
      sLineBreak + 'UF:' + UF);
  end;
end;

function TDmCTe_0000.ObtemXMun(Codigo: Integer): String;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DmodG.AllID_DB, [
    'SELECT Nome ',
    'FROM dtb_munici ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    //
    Result := Qry.FieldByName('Nome').AsString;
  finally
    Qry.Free;
  end;
end;

function TDmCTe_0000.Obtem_Arquivo_XML_(Empresa: Integer; Ext, Arq: String;
  Assinado: Boolean): String;
var
  Dir: String;
begin
  Result := '';
  //
  if not ReopenEmpresa(Empresa) then Exit;
  //
  if ObtemDirXML(Ext, Dir, Assinado) then
    Result := Dir + Arq + Ext;
end;

procedure TDmCTe_0000.QrOpcoesCTeAfterOpen(DataSet: TDataSet);
const
  Formata = True;
  //
  function Fmt(Val: Double): String;
  begin
    if Formata then
      Result := Geral.FFT_Dot(Val, 2, siNegativo)
    else
      Result := Geral.FFI(Val);
  end;
begin
  VAR_TXT_verscteStatusServico        := Fmt(QrOpcoesCTeCTeVerStaSer.Value);
  VAR_TXT_verscteRecepcao             := Fmt(QrOpcoesCTeCTeVerConLot.Value);
  VAR_TXT_verscteRetRecepcao          := Fmt(QrOpcoesCTeCTeVerConLot.Value);
  VAR_TXT_verscteEveCancelamento      := Fmt(QrOpcoesCTeCTeVerLotEve.Value);
  VAR_TXT_versctePedInutilizacao      := Fmt(QrOpcoesCTeCTeVerLotEve.Value);
  VAR_TXT_verscteEveCancelamento      := Fmt(QrOpcoesCTeCTeVerConCTe.Value);
  VAR_TXT_versctePedInutilizacao      := Fmt(QrOpcoesCTeCTeVerLotEve.Value);
  VAR_TXT_verscteConsultaCadastro     := Fmt(QrOpcoesCTeCTeVerConsCad.Value);
  VAR_TXT_verscteEveEPEC              := Fmt(QrOpcoesCTeCTeVerLotEve.Value);
  //
  //MaxVersaoConsumoWS_CTe = Integer(High(TTipoConsumoWS_CTe));
/// ATEN��O!!! MUDAR ABAIXO O SEQUENCIAL!!
////////////////////////////////////////////////////////////////////////////////
  sVersaoConsumoWS_CTe[-MaxTipoConsumoWS_CTe +  8] := VAR_TXT_verscteStatusServico;
  sVersaoConsumoWS_CTe[-MaxTipoConsumoWS_CTe +  9] := VAR_TXT_verscteRecepcao;
  sVersaoConsumoWS_CTe[-MaxTipoConsumoWS_CTe + 10] := VAR_TXT_verscteRetRecepcao;
  sVersaoConsumoWS_CTe[-MaxTipoConsumoWS_CTe + 11] := VAR_TXT_verscteEveCancelamento;
  sVersaoConsumoWS_CTe[-MaxTipoConsumoWS_CTe + 12] := VAR_TXT_versctePedInutilizacao;
  sVersaoConsumoWS_CTe[-MaxTipoConsumoWS_CTe + 13] := VAR_TXT_verscteConsultaCTe;
  sVersaoConsumoWS_CTe[-MaxTipoConsumoWS_CTe + 14] := VAR_TXT_verscteEveCartaDeCorrecao;
  sVersaoConsumoWS_CTe[-MaxTipoConsumoWS_CTe + 15] := VAR_TXT_verscteConsultaCadastro;
  sVersaoConsumoWS_CTe[-MaxTipoConsumoWS_CTe + 16] := VAR_TXT_verscteEveEPEC;
  //
end;

procedure TDmCTe_0000.ReopenCTeArq(IDCtrl: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeArq, Dmod.MyDB, [
  'SELECT caba.IDCtrl IDCtrl_CabA, arq.IDCtrl IDCtrl_Arq, ',
  'caba.Id, caba.FatID, caba.FatNum, caba.Empresa ',
  'FROM ctecaba caba ',
  'LEFT JOIN ctearq arq ON arq.IDCtrl=caba.IDCtrl ',
  'WHERE caba.IDCtrl=' + Geral.FF0(IDCtrl),
  '']);
end;

function TDmCTe_0000.ReopenCTeCab1Exped(FatID, FatNum,
  Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeCab1Exped, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctecab1exped ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTeCab1Receb(FatID, FatNum,
  Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeCab1Receb, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctecab1receb ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTeCab2LocColeta(FatID, FatNum,
  Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeCab2LocColeta, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctecab2loccoleta ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTeCab2LocEnt(FatID, FatNum,
  Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeCab2LocEnt, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctecab2locent ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTECab2Toma04(FatID, FatNum,
  Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeCab2Toma04, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctecab2toma04 ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTeCabA(FatID, FatNum, Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeCabA, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctecaba ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  //
  if QrCTeCabA.RecordCount = 0 then
    Result := -1
  else
    Result := QrCTeCabAStatus.Value;
end;

function TDmCTe_0000.ReopenCTeIt1AutXml(FatID, FatNum,
  Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeIt1AutXml, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteit1autxml ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTeIt2Comp(FatID, FatNum, Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeIt2Comp, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteit2comp ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTeIt2InfDoc(FatID, FatNum, Empresa: Integer;
  CTeMyTpDocInf: TCTeMyTpDocInf): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeIt2InfDoc, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteit2infdoc ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND MyTpDocInf=' + Geral.FF0(Integer(CTeMyTpDocInf)),
  '']);
end;

function TDmCTe_0000.ReopenCTeIt2ObsCont(FatID, FatNum,
  Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeIt2ObsCont, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteit2obscont ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTeIt2ObsFisco(FatID, FatNum,
  Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeIt2ObsFisco, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteit2obscont ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTeIt2Peri(FatID, FatNum, Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeIt2Peri, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteit2peri ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTeIt2Seg(FatID, FatNum, Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeIt2Seg, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteit2seg ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTeIt3Dup(FatID, FatNum, Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeIt3Dup, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteit3dup ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTeIt3InfQ(FatID, FatNum, Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeIt3InfQ, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteit3infq ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmCTe_0000.ReopenCTeLayI();
begin
  if (QrCTeLayI.State = dsInactive) or
  (FLastVerCTeLayI <> QrOpcoesCTeCTeVersao.Value) then
  begin
    FLastVerCTeLayI := QrOpcoesCTeCTeVersao.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCTeLayI, Dmod.MyDB, [
    'SELECT layc.Nome NO_Grupo, layi.* ',
    'FROM ctelayi layi ',
    'LEFT JOIN ctelayc layc ON layc.Grupo=layi.Grupo ',
    'WHERE layi.Versao=' + Geral.FFT_Dot(FLastVerCTeLayI, 2, siNegativo),
    '']);
  end;
  VerificaTabelaLayI();
end;

function TDmCTe_0000.ReopenCTeMoRodoCab(FatID, FatNum,
  Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeMoRodoCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctemorodocab ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTeMoRodoLacr(FatID, FatNum,
  Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeMoRodoLacr, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctemorodolacr ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTeMoRodoMoto(FatID, FatNum,
  Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeMoRodoMoto, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctemorodomoto ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTeMoRodoOcc(FatID, FatNum,
  Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeMoRodoOcc, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctemorodoocc ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTeMoRodoProp(FatID, FatNum,
  Empresa, Conta: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeMoRodoProp, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctemorodoprop ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND Conta=' + Geral.FF0(Conta),
  '']);
end;

function TDmCTe_0000.ReopenCTeMoRodoVeic(FatID, FatNum,
  Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeMoRodoVeic, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctemorodoveic ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenCTeMoRodoVPed(FatID, FatNum,
  Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeMoRodoVPed, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctemorodovped ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmCTe_0000.ReopenEmpresa(Empresa: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmpresa, Dmod.MyDB, [
  'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, ',
  'en.IE, en.IEST, en.RG, en.NIRE, en.CNAE,  uf.DTB DTB_UF, ',
  'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT, ',
  'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA, ',
  'IF(en.Tipo=0,en.ERua,en.PRua) RUA, ',
  'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero, ',
  'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL, ',
  'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO, ',
  'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP, ',
  'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1, ',
  'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF, ',
  'IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) + 0.000 CodiPais, ',

  'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici, ',
  'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF, ',
  'bpa.Nome NO_Pais, en.SUFRAMA, en.Filial ',

  'FROM entidades en ',
  'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en.PLograd) ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) ',
  'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF) ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais bpa ON bpa.Codigo=IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) ',
  'WHERE en.Codigo=' + Geral.FF0(Empresa),
  '']);
  //
  QrFilial.Close;
  QrFilial.Params[0].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrFilial, Dmod.MyDB);
  //
  Result := (QrEmpresa.RecordCount > 0) and (QrFilial.RecordCount > 0);
  if not Result then Geral.MB_Aviso(
  'N�o foi poss�vel reabrir as tabelas da empresa ' + IntToStr(Empresa) + '!');
end;

procedure TDmCTe_0000.ReopenEntiCTe(Entidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntiCTe, Dmod.MyDB, [
  'SELECT ent.Tipo, ent.SUFRAMA, ',
  'IF(ent.Tipo=0, ent.CNPJ, "") CNPJ,     ',
  'IF(ent.Tipo=1, ent.CPF, "") CPF,     ',
  'IF(ent.Tipo=0, ent.IE, "") IE,     ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) xNome,     ',
  'IF(ent.Tipo=0, ent.Fantasia, ent.Apelido) xFant,     ',
  'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Fone, ',
  'IF(ent.Tipo=0, ent.ERua, ent.PRua) xLgr, ',
  'IF(IF(ent.Tipo=0, ent.ENumero, ent.PNumero) <>0, IF(ent.Tipo=0, ent.ENumero, ent.PNumero), "S/N") Nro,',
  'IF(ent.Tipo=0, ent.ECompl,   ent.PCompl) xCpl, ',
  'IF(ent.Tipo=0, ent.EBairro,   ent.PBairro) xBairro, ',
  'IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici) + 0.000 CMun, ',
  'mun.Nome xMun, ',
  'IF(ent.Tipo=0,  ent.ECEP, ent.PCEP) + 0.000 CEP, ',
  'ufs.Nome UF,',
  'IF(ent.Tipo=0, ent.ECodiPais, ent.PCodiPais) + 0.000 CPais,',
  'bpa.Nome xPais, ',
  'IF(ent.Tipo=0, ent.EEmail, ent.PEmail) Email',
  'FROM entidades ent ',
  'LEFT JOIN loclogiall.dtb_munici mun ON mun.Codigo=IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici) ',
  'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF) ',
  'LEFT JOIN loclogiall.bacen_pais bpa ON bpa.Codigo=IF(ent.Tipo=0, ent.ECodiPais, ent.PCodiPais) ',
  'WHERE ent.Codigo=' + Geral.FF0(Entidade),
  '']);
  //Geral.MB_SQL(nil, QrEntiCTe);
end;

function TDmCTe_0000.ReopenEntidadeCTe(Entidade: Integer;
  Qry: TmySQLQuery): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, ',
  'en.EstrangDef, en.EstrangTip, en.EstrangNum, en.indIEDest,',
  'en.IE, en.RG, en.NIRE, en.CNAE, L_CNPJ, L_Ativo, ',
  'LLograd, LRua, LCompl, LNumero, LBairro, LCidade, ',
  'LUF, l2.Nome NO_LLOGRAD, LCodMunici, ',
  'ml.Nome NO_LMunici, ul.Nome NO_LUF, ',
  'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF, ',
  'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT, ',
  'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA, ',
  'IF(en.Tipo=0,en.ERua,en.PRua) RUA, ',
  'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero, ',
  'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL, ',
  'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO, ',
  'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP, ',
  'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1, ',
  'IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) + 0.000 CodiPais, ',
  'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici, ',
  'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF, ',
  'bpa.Nome NO_Pais, en.SUFRAMA ',
  'FROM entidades en ',
  'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en.PLograd) ',
  'LEFT JOIN listalograd l2 ON l2.Codigo=en.LLograd ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici ml ON ml.Codigo=en.LCodMunici ',
  'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF) ',
  'LEFT JOIN ufs ul ON ul.Codigo=en.LUF ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais bpa ON bpa.Codigo=IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) ',
  'WHERE en.Codigo=' + Geral.FF0(Entidade),
  '']);
  Result := (Qry.RecordCount > 0);
  if not Result then
    Geral.MB_Aviso('N�o foi poss�vel reabrir a tabela da entidade CT-e ' + Geral.FF0(Entidade) + '!');
end;

procedure TDmCTe_0000.ReopenFrtFatAutXml(Codigo: Integer);
var
  ATT_AddForma: String;
begin
  ATT_AddForma := dmkPF.ArrayToTexto('cga.AddForma', 'NO_AddForma', pvPos, True,
    sNFeautXML);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatAutXml, Dmod.MyDB, [
  'SELECT cga.*, ',
  ATT_AddForma,
  'IF(cga.Tipo=0, "CNPJ", "CPF") TpDOC,',
  'CASE cga.AddForma ',
  '  WHEN 0 THEN "(N/I)"  ',
  '  WHEN 1 THEN eco.Nome ',
  '  WHEN 2 THEN IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ',
  '  WHEN 3 THEN " "  ',
  '  ELSE "???" END NO_ENT_ECO, ',
  'IF(cga.Tipo=0, cga.CNPJ, cga.CPF) CNPJ_CPF ',
  'FROM frtfatautxml cga ',
  'LEFT JOIN entidades ent ON ent.Codigo=cga.AddIDCad',
  'LEFT JOIN enticontat eco ON eco.Controle=cga.AddIDCad',
  'WHERE cga.Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmCTe_0000.ReopenFrtFatComp(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatComp, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatcomp ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmCTe_0000.ReopenFrtFatDup(Codigo: Integer; TabLctA: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatDup, Dmod.MyDB, [
  'SELECT Duplicata, Vencimento, Credito, Controle ',
  'FROM ' + TabLctA,
  'WHERE FatID=' + Geral.FF0(VAR_FatID_5001),
  'AND FatNum=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmCTe_0000.ReopenFrtFatInfCTeAnu(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatInfCTeAnu, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatinfcteanu ',
  'WHERE Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  '']);
end;

procedure TDmCTe_0000.ReopenFrtFatInfCTeComp(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatInfCTeComp, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatinfctecomp ',
  'WHERE Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  '']);
end;

procedure TDmCTe_0000.ReopenFrtFatInfCTeSub(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatInfCTeSub, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatinfctesub ',
  'WHERE Codigo=' + Geral.FF0(QrFrtFatCabCodigo.Value),
  '']);
end;

procedure TDmCTe_0000.ReopenFrtFatInfDoc(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatInfDoc, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatinfdoc ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmCTe_0000.ReopenFrtFatInfQ(Codigo: Integer);
var
  ATT_CTeCUnid, ATT_CTeModal, ATT_CTeTpServ, ATT_CTeToma: String;
begin
  ATT_CTeCUnid := dmkPF.ArrayToTexto('fiq.CUnid', 'NO_CUnid', pvPos, True,
    sCTeCUnid);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatInfQ, Dmod.MyDB, [
  'SELECT ',
  ATT_CTeCUnid,
  'mtm.Nome NO_MyTpMed, ',
  'fiq.* ',
  'FROM frtfatinfq fiq',
  'LEFT JOIN trnsmytpmed mtm ON mtm.Codigo=fiq.MyTpMed ',
  'WHERE fiq.Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmCTe_0000.ReopenFrtFatMoRodoLacr(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatMoRodoLacr, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatmorodolacr ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmCTe_0000.ReopenFrtFatMoRodoMoto(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatMoRodoMoto, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatmorodomoto ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmCTe_0000.ReopenFrtFatMoRodoVeic(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatMoRodoVeic, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatmorodoveic ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmCTe_0000.ReopenFrtFatMoRodoVPed(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatMoRodoVPed, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatmorodovped ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmCTe_0000.ReopenFrtFatPeri(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatPeri, Dmod.MyDB, [
  'SELECT ffp.Codigo, ffp.Controle, ffp.ProdutCad, ',
  'ffp.qTotProd, ffp.qVolTipo, prc.XClaRisco, ',
  'prc.Nome, prc.nONU, prc.XNomeAE, ',
  'prc.GrEmb PERI_grEmb, prc.PontoFulgor ',
  'FROM frtfatperi ffp ',
  'LEFT JOIN produtcad prc ON prc.Codigo=ffp.ProdutCad ',
  'WHERE ffp.Codigo=' + Geral.FF0(Codigo),
  'ORDER BY ffp.Controle ',
  '']);
end;

procedure TDmCTe_0000.ReopenFrtFatSeg(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtFatSeg, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtfatseg ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmCTe_0000.ReopenFrtFOCAtrDef(Codigo: Integer);
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrFrtFOCAtrDef, Dmod.MyDB, [
  'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ATRITS, ',
  'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,',
  'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp',
  'FROM frtfocatrdef def',
  'LEFT JOIN frtfocatrits its ON its.Controle=def.AtrIts ',
  'LEFT JOIN frtfocatrcad cad ON cad.Codigo=def.AtrCad ',
  'WHERE def.ID_Sorc=' + Geral.FF0(Codigo),
  ' ',
  'UNION  ',
  ' ',
  'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS, ',
  'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt AtrTxt,',
  'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp ',
  'FROM frtfocatrtxt def ',
  'LEFT JOIN frtfocatrcad cad ON cad.Codigo=def.AtrCad ',
  'WHERE def.ID_Sorc=' + Geral.FF0(Codigo),
  ' ',
  'ORDER BY NO_CAD, NO_ITS ',
  '']);
end;

function TDmCTe_0000.ReopenLocEve(Id: String; Evento: Integer): Boolean;
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrLocEve, Dmod.MyDB, [
  'SELECT COUNT(*) ITENS ',
  'FROM cteevercab ',
  'WHERE chCTe="' + Id + '" ',
  'AND tpEvento=' + Geral.FF0(Evento),
  'AND ret_cStat in (135,136) ',
  '']);
  //
  Result := QrLocEveITENS.Value >= 1;
end;

function TDmCTe_0000.ReopenOpcoesCTe(QrOpCTe: TmySQLQuery; Empresa: Integer; Avisa: Boolean): Boolean;
begin
  if (Empresa > 0) or (Empresa <> VAR_LIB_EMPRESA_SEL) then
    Geral.MB_Aviso('CUIDADO!!! Empresa = ' + IntToStr(Empresa) + ' (Op��es CT-e)');
  UnDmkDAC_PF.AbreMySQLQuery0(QrOpCTe, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, ',
  //' etc0.Nome NO_ETC_0, etc1.Nome NO_ETC_1, ',
  'emp.*' ,
  'FROM paramsemp emp' ,
  'LEFT JOIN entidades ent ON ent.Codigo=emp.Codigo ',
  //'LEFT JOIN entitipcto etc0 ON etc0.Codigo=emp.EntiTipCto' ,
  //'LEFT JOIN entitipcto etc1 ON etc1.Codigo=emp.EntiTipCt1' ,
  'WHERE emp.Codigo=' + Geral.FF0(Empresa),
  '']);
  Result := QrOpCTe.RecordCount > 0;
  if not Result and Avisa then
    Geral.MB_Aviso('N�o existem op��es de CT-e cadastradas!');
(*
object QrOpcoesCTeNO_ETC_0: TWideStringField
  FieldName = 'NO_ETC_0'
  Size = 30
end
object QrOpcoesCTeNO_ETC_1: TWideStringField
  FieldName = 'NO_ETC_1'
  Size = 30
end
*)
end;

procedure TDmCTe_0000.ReopenTerceiro(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTerceiro, Dmod.MyDB, [
  'SELECT tcb.RNTRC, ent.Tipo, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
  'IF(ent.Tipo=0, "", ent.CPF) CPF, ',
  'IF(ent.Tipo=0, ent.CNPJ, "") CNPJ, ',
  'IF(ent.Tipo=0, ent.IE, "") IE, ',
  'ufs.Nome UF, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
  'ent.CPF, ent.CNPJ  ',
  'FROM entidades ent ',
  'LEFT JOIN ufs ufs ON ufs.Codigo=(IF(ent.Tipo=0, ent.EUF, ent.PUF)) ',
  'LEFT JOIN trnscad tcb ON tcb.Codigo=ent.Codigo ',
  'WHERE ent.Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmCTe_0000.ReopenTrnsCad(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTrnsCad, Dmod.MyDB, [
  'SELECT * ',
  'FROM trnscad ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmCTe_0000.ReopenTrnsIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTrnsIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM trnsits ',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
end;

function TDmCTe_0000.SalvaXML(Ext, Arq, Texto: String; rtfRetWS: TMemo;
  Assinado: Boolean): String;
var
  Dir, Arquivo, xxx: String;
  TxtXML: TextFile;
  P: Integer;
begin
  if ObtemDirXML(Ext, Dir, Assinado) then
  begin
    P := pos(Ext, Arq);
    if P = 0 then
    begin
      // Usar extens�o correta para xml de CTe!
      if (Ext = CTE_EXT_RET_DOW_CTE_CTE_XML)
      or (Ext = CTE_EXT_RET_CNF_CTE_CTE_XML) then
        xxx := CTE_EXT_CTE_XML
      else
        xxx := Ext;
      Arquivo := Dir + Arq + xxx
    end else
      Arquivo := Dir + Arq; // j� tem Ext ?
    //
    AssignFile(TxtXML, Arquivo);
    Rewrite(TxtXML);
    Write(TxtXML, Texto);
    CloseFile(TxtXML);
    //
    Result := Arquivo;
    if rtfRetWS <> nil then
      rtfRetWS.Text := Texto;
  end else
    Result := '';
end;

function TDmCTe_0000.StepCTeCab(FatID, FatNum, Empresa: Integer; Status:
  TCTeMyStatus; LaAviso1, LaAviso2: TLabel): Boolean;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  'Atualizando status da CTe para ' + IntToStr(Integer(Status)));
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctecaba', False, [
  'Status'], [
  'FatID', 'FatNum', 'Empresa'], [
  Integer(Status)], [
  FatID, FatNum, Empresa], True);
end;

function TDmCTe_0000.TituloArqXML(const Ext: String; var Titulo,
  Descri: String): Boolean;
begin
  Result := True;
  if Ext = CTE_EXT_CTE_XML then
  begin
    Titulo := 'CT-e';
    Descri := 'O nome do arquivo ser� a chave de acesso completa com extens�o �-cte.xml�';
  end else
  if Ext = CTE_EXT_ENV_LOT_XML then
  begin
    Titulo := 'Envio de Lote de CT-e';
    Descri := 'O nome do arquivo ser� o n�mero do lote com extens�o �-env-lot.xml�';
  end else
  if Ext = CTE_EXT_REC_XML then
  begin
    Titulo := 'Recibo';
    Descri := 'O nome do arquivo ser� o n�mero do lote com extens�o �-rec.xml�';
  end else
  if Ext = CTE_EXT_PED_REC_XML then
  begin
    Titulo := 'Pedido do Resultado do Processamento do Lote de CT-e';
    Descri := 'O nome do arquivo ser� o n�mero do recibo com extens�o �-ped-rec.xml�';
  end else
  if Ext = CTE_EXT_PRO_REC_XML then
  begin
    Titulo := 'Resultado do Processamento do Lote de CT-e';
    Descri := 'O nome do arquivo ser� o n�mero do recibo com extens�o �-pro-rec.xml�';
  end else
  if Ext = CTE_EXT_DEN_XML then
  begin
    Titulo := 'Denega��o de Uso';
    Descri := 'O nome do arquivo ser� a chave de acesso completa com extens�o �-den.xml�';
(*
  end else
  if Ext = CTE_EXT_PED_CAN_XML then
  begin
    Titulo := 'Pedido de Cancelamento de CT-e';
    Descri := 'O nome do arquivo ser� a chave de acesso completa com extens�o �-ped-can.xml�';
  end else
  if Ext = CTE_EXT_CAN_XML then
  begin
    Titulo := 'Cancelamento de CT-e';
    Descri := 'O nome do arquivo ser� a chave de acesso completa com extens�o �-can.xml�';
*)
  end else
  if Ext = CTE_EXT_PED_INU_XML then
  begin
    Titulo := 'Pedido de Inutiliza��o de Numera��o';
    Descri := 'O nome do arquivo ser� composto por: UF + Ano de inutiliza��o + CNPJ do emitente + Modelo + S�rie + N�mero Inicial + N�mero Final com extens�o �-ped-inu.xml�';
  end else
  if Ext = CTE_EXT_INU_XML then
  begin
    Titulo := 'Inutiliza��o de Numera��o';
    Descri := 'O nome do arquivo ser� composto por: Ano de inutiliza��o + CNPJ do emitente + Modelo + S�rie + N�mero Inicial + N�mero Final com extens�o �-inu.xml�';
  end else
  if Ext = CTE_EXT_PED_SIT_XML then
  begin
    Titulo := 'Pedido de Consulta Situa��o Atual da CT-e';
    Descri := 'O nome do arquivo ser� a chave de acesso completa com extens�o �-ped-sit.xml�';
  end else
  if Ext = CTE_EXT_SIT_XML then
  begin
    Titulo := 'Situa��o Atual da CT-e';
    Descri := 'O nome do arquivo ser� a chave de acesso completa com extens�o �-sit.xml�';
  end else
  if Ext = CTE_EXT_PED_STA_XML then
  begin
    Titulo := 'Pedido de Consulta do Status do Servi�o';
    Descri := 'O nome do arquivo ser�: �AAAAMMDDTHHMMSS� do momento da consulta com extens�o �-ped-sta.xml�';
  end else
  if Ext = CTE_EXT_STA_XML then
  begin
    Titulo := 'Status do Servi�o';
    Descri := 'O nome do arquivo ser�: �AAAAMMDDTHHMMSS� do momento da consulta com extens�o �-sta.xml�';
  end else
  begin
    Result := False;
    Titulo := 'Tipo de arquivo XML desconhecido para CT-e';
    Descri := 'AVISE A DERMATEK!';
  end;
end;

function TDmCTe_0000.VerificaTabelaLayI(): Boolean;
begin
  Result := QrCTeLayI.RecordCount > 0;
  if not result then
  begin
    QrCTeLayI.Close;
    Geral.MB_Aviso('Tabela "CTeLayI" vazia!');
  end;
end;

end.
