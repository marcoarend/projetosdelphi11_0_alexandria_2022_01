object DmCTe_0000: TDmCTe_0000
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 803
  Width = 1010
  object QrEmpresa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, '
      'en.IE, en.IEST, en.RG, en.NIRE, en.CNAE,  uf.DTB DTB_UF,'
      'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT,'
      'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA,'
      'IF(en.Tipo=0,en.ERua,en.PRua) RUA,'
      'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero,'
      'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL,'
      'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO,'
      'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP,'
      'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1,'
      'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF,'
      'IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) + 0.000 CodiPais,'
      ''
      'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici,'
      'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF,'
      'bpa.Nome NO_Pais, en.SUFRAMA, en.Filial'
      ''
      'FROM entidades en'
      
        'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en' +
        '.PLograd) '
      
        'LEFT JOIN dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,' +
        'en.PCodMunici)'
      'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF)'
      
        'LEFT JOIN bacen_pais bpa ON bpa.Codigo=IF(en.Tipo=0,en.ECodiPais' +
        ',en.PCodiPais)'
      'WHERE en.Codigo=:P0'
      ''
      '')
    Left = 32
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmpresaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmpresaTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmpresaCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEmpresaCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEmpresaIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEmpresaRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrEmpresaCNAE: TWideStringField
      FieldName = 'CNAE'
      Size = 7
    end
    object QrEmpresaNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
    object QrEmpresaFANTASIA: TWideStringField
      FieldName = 'FANTASIA'
      Required = True
      Size = 60
    end
    object QrEmpresaRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEmpresaCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEmpresaBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEmpresaTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrEmpresaNO_LOGRAD: TWideStringField
      FieldName = 'NO_LOGRAD'
      Size = 10
    end
    object QrEmpresaNO_Munici: TWideStringField
      FieldName = 'NO_Munici'
      Size = 100
    end
    object QrEmpresaNO_UF: TWideStringField
      FieldName = 'NO_UF'
      Required = True
      Size = 2
    end
    object QrEmpresaDTB_UF: TWideStringField
      FieldName = 'DTB_UF'
      Required = True
      Size = 2
    end
    object QrEmpresaNO_Pais: TWideStringField
      FieldName = 'NO_Pais'
      Size = 100
    end
    object QrEmpresaIEST: TWideStringField
      FieldName = 'IEST'
    end
    object QrEmpresaSUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Size = 9
    end
    object QrEmpresaFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrEmpresaNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrEmpresaNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrEmpresaCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEmpresaCodiPais: TFloatField
      FieldName = 'CodiPais'
    end
    object QrEmpresaCodMunici: TFloatField
      FieldName = 'CodMunici'
    end
    object QrEmpresaUF: TFloatField
      FieldName = 'UF'
    end
  end
  object QrFilial: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM paramsemp'
      'WHERE Codigo=:P0')
    Left = 32
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFilialCerDigital: TWideMemoField
      FieldName = 'CerDigital'
      Origin = 'paramsemp.CerDigital'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFilialDirCTeGer: TWideStringField
      FieldName = 'DirCTeGer'
      Size = 255
    end
    object QrFilialDirCTeAss: TWideStringField
      FieldName = 'DirCTeAss'
      Size = 255
    end
    object QrFilialDirCTeEnvLot: TWideStringField
      FieldName = 'DirCTeEnvLot'
      Size = 255
    end
    object QrFilialDirCTeRec: TWideStringField
      FieldName = 'DirCTeRec'
      Size = 255
    end
    object QrFilialDirCTePedRec: TWideStringField
      FieldName = 'DirCTePedRec'
      Size = 255
    end
    object QrFilialDirCTeProRec: TWideStringField
      FieldName = 'DirCTeProRec'
      Size = 255
    end
    object QrFilialDirCTeDen: TWideStringField
      FieldName = 'DirCTeDen'
      Size = 255
    end
    object QrFilialDirCTePedCan: TWideStringField
      FieldName = 'DirCTePedCan'
      Size = 255
    end
    object QrFilialDirCTeCan: TWideStringField
      FieldName = 'DirCTeCan'
      Size = 255
    end
    object QrFilialDirCTePedInu: TWideStringField
      FieldName = 'DirCTePedInu'
      Size = 255
    end
    object QrFilialDirCTeInu: TWideStringField
      FieldName = 'DirCTeInu'
      Size = 255
    end
    object QrFilialDirCTePedSit: TWideStringField
      FieldName = 'DirCTePedSit'
      Size = 255
    end
    object QrFilialDirCTeSit: TWideStringField
      FieldName = 'DirCTeSit'
      Size = 255
    end
    object QrFilialDirCTePedSta: TWideStringField
      FieldName = 'DirCTePedSta'
      Size = 255
    end
    object QrFilialDirCTeSta: TWideStringField
      FieldName = 'DirCTeSta'
      Size = 255
    end
    object QrFilialCTeUF_WebServ: TWideStringField
      FieldName = 'CTeUF_WebServ'
      Size = 2
    end
    object QrFilialCTeUF_Servico: TWideStringField
      FieldName = 'CTeUF_Servico'
      Size = 10
    end
    object QrFilialPathLogoCTe: TWideStringField
      FieldName = 'PathLogoCTe'
      Size = 255
    end
    object QrFilialCtaFretPrest: TIntegerField
      FieldName = 'CtaFretPrest'
    end
    object QrFilialTxtFretPrest: TWideStringField
      FieldName = 'TxtFretPrest'
      Size = 100
    end
    object QrFilialDupFretPrest: TWideStringField
      FieldName = 'DupFretPrest'
      Size = 3
    end
    object QrFilialCTeSerNum: TWideStringField
      FieldName = 'CTeSerNum'
      Size = 255
    end
    object QrFilialCTeSerVal: TDateField
      FieldName = 'CTeSerVal'
    end
    object QrFilialCTeSerAvi: TSmallintField
      FieldName = 'CTeSerAvi'
    end
    object QrFilialDirDACTes: TWideStringField
      FieldName = 'DirDACTes'
      Size = 255
    end
    object QrFilialDirCTeProt: TWideStringField
      FieldName = 'DirCTeProt'
      Size = 255
    end
    object QrFilialCTePreMailAut: TIntegerField
      FieldName = 'CTePreMailAut'
    end
    object QrFilialCTePreMailCan: TIntegerField
      FieldName = 'CTePreMailCan'
    end
    object QrFilialCTePreMailEveCCe: TIntegerField
      FieldName = 'CTePreMailEveCCe'
    end
    object QrFilialCTeversao: TFloatField
      FieldName = 'CTeversao'
    end
    object QrFilialCTeVerStaSer: TFloatField
      FieldName = 'CTeVerStaSer'
    end
    object QrFilialCTeVerEnvLot: TFloatField
      FieldName = 'CTeVerEnvLot'
    end
    object QrFilialCTeVerConLot: TFloatField
      FieldName = 'CTeVerConLot'
    end
    object QrFilialCTeVerCanCTe: TFloatField
      FieldName = 'CTeVerCanCTe'
    end
    object QrFilialCTeVerInuNum: TFloatField
      FieldName = 'CTeVerInuNum'
    end
    object QrFilialCTeVerConCTe: TFloatField
      FieldName = 'CTeVerConCTe'
    end
    object QrFilialCTeVerLotEve: TFloatField
      FieldName = 'CTeVerLotEve'
    end
    object QrFilialCTeide_mod: TSmallintField
      FieldName = 'CTeide_mod'
    end
    object QrFilialCTeide_tpImp: TSmallintField
      FieldName = 'CTeide_tpImp'
    end
    object QrFilialCTeide_tpAmb: TSmallintField
      FieldName = 'CTeide_tpAmb'
    end
    object QrFilialCTeAppCode: TSmallintField
      FieldName = 'CTeAppCode'
    end
    object QrFilialCTeAssDigMode: TSmallintField
      FieldName = 'CTeAssDigMode'
    end
    object QrFilialCTeEntiTipCto: TIntegerField
      FieldName = 'CTeEntiTipCto'
    end
    object QrFilialCTeEntiTipCt1: TIntegerField
      FieldName = 'CTeEntiTipCt1'
    end
    object QrFilialMyEmailCTe: TWideStringField
      FieldName = 'MyEmailCTe'
      Size = 255
    end
    object QrFilialDirCTeSchema: TWideStringField
      FieldName = 'DirCTeSchema'
      Size = 255
    end
    object QrFilialDirCTeRWeb: TWideStringField
      FieldName = 'DirCTeRWeb'
      Size = 255
    end
    object QrFilialDirCTeEveEnvLot: TWideStringField
      FieldName = 'DirCTeEveEnvLot'
      Size = 255
    end
    object QrFilialDirCTeEveRetLot: TWideStringField
      FieldName = 'DirCTeEveRetLot'
      Size = 255
    end
    object QrFilialDirCTeEvePedCCe: TWideStringField
      FieldName = 'DirCTeEvePedCCe'
      Size = 255
    end
    object QrFilialDirCTeEveRetCCe: TWideStringField
      FieldName = 'DirCTeEveRetCCe'
      Size = 255
    end
    object QrFilialDirCTeEveProcCCe: TWideStringField
      FieldName = 'DirCTeEveProcCCe'
      Size = 255
    end
    object QrFilialDirCTeEvePedCan: TWideStringField
      FieldName = 'DirCTeEvePedCan'
      Size = 255
    end
    object QrFilialDirCTeEveRetCan: TWideStringField
      FieldName = 'DirCTeEveRetCan'
      Size = 255
    end
    object QrFilialDirCTeRetCTeDes: TWideStringField
      FieldName = 'DirCTeRetCTeDes'
      Size = 255
    end
    object QrFilialCTeCTeVerConDes: TFloatField
      FieldName = 'CTeCTeVerConDes'
    end
    object QrFilialDirCTeEvePedMDe: TWideStringField
      FieldName = 'DirCTeEvePedMDe'
      Size = 255
    end
    object QrFilialDirCTeEveRetMDe: TWideStringField
      FieldName = 'DirCTeEveRetMDe'
      Size = 255
    end
    object QrFilialDirDowCTeDes: TWideStringField
      FieldName = 'DirDowCTeDes'
      Size = 255
    end
    object QrFilialDirDowCTeCTe: TWideStringField
      FieldName = 'DirDowCTeCTe'
      Size = 255
    end
    object QrFilialCTeInfCpl: TIntegerField
      FieldName = 'CTeInfCpl'
    end
    object QrFilialCTeVerConsCad: TFloatField
      FieldName = 'CTeVerConsCad'
    end
    object QrFilialCTeVerDistDFeInt: TFloatField
      FieldName = 'CTeVerDistDFeInt'
    end
    object QrFilialCTeVerDowCTe: TFloatField
      FieldName = 'CTeVerDowCTe'
    end
    object QrFilialDirDowCTeCnf: TWideStringField
      FieldName = 'DirDowCTeCnf'
      Size = 255
    end
    object QrFilialCTeItsLin: TSmallintField
      FieldName = 'CTeItsLin'
    end
    object QrFilialCTeFTRazao: TSmallintField
      FieldName = 'CTeFTRazao'
    end
    object QrFilialCTeFTEnder: TSmallintField
      FieldName = 'CTeFTEnder'
    end
    object QrFilialCTeFTFones: TSmallintField
      FieldName = 'CTeFTFones'
    end
    object QrFilialCTeMaiusc: TSmallintField
      FieldName = 'CTeMaiusc'
    end
    object QrFilialCTeShowURL: TWideStringField
      FieldName = 'CTeShowURL'
      Size = 255
    end
    object QrFilialCTetpEmis: TSmallintField
      FieldName = 'CTetpEmis'
    end
    object QrFilialCTeSCAN_Ser: TIntegerField
      FieldName = 'CTeSCAN_Ser'
    end
    object QrFilialCTeSCAN_nCT: TIntegerField
      FieldName = 'CTeSCAN_nCT'
    end
    object QrFilialCTe_indFinalCpl: TSmallintField
      FieldName = 'CTe_indFinalCpl'
    end
    object QrFilialParamsCTe: TIntegerField
      FieldName = 'ParamsCTe'
    end
    object QrFilialSimplesFed: TSmallintField
      FieldName = 'SimplesFed'
    end
    object QrFilialCTeVerEPEC: TFloatField
      FieldName = 'CTeVerEPEC'
    end
    object QrFilialDirCTeEvePedEPEC: TWideStringField
      FieldName = 'DirCTeEvePedEPEC'
      Size = 255
    end
    object QrFilialDirCTeEveRetEPEC: TWideStringField
      FieldName = 'DirCTeEveRetEPEC'
      Size = 255
    end
    object QrFilialCTeUF_EPEC: TWideStringField
      FieldName = 'CTeUF_EPEC'
      Size = 10
    end
    object QrFilialCTeUF_Conting: TWideStringField
      FieldName = 'CTeUF_Conting'
      Size = 10
    end
  end
  object QrOpcoesCTe: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrOpcoesCTeAfterOpen
    SQL.Strings = (
      'SELECT etc0.Nome NO_ETC_0, etc1.Nome NO_ETC_1,  ctr.*'
      'FROM paramsemp ctr'
      'LEFT JOIN entitipcto etc0 ON etc0.Codigo=ctr.EntiTipCto'
      'LEFT JOIN entitipcto etc1 ON etc1.Codigo=ctr.EntiTipCt1'
      'WHERE ctr.Codigo=:P0')
    Left = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOpcoesCTeCTeversao: TFloatField
      FieldName = 'CTeversao'
    end
    object QrOpcoesCTeCTeide_mod: TSmallintField
      FieldName = 'CTeide_mod'
    end
    object QrOpcoesCTeSimplesFed: TSmallintField
      FieldName = 'SimplesFed'
    end
    object QrOpcoesCTeDirCTeGer: TWideStringField
      FieldName = 'DirCTeGer'
      Size = 255
    end
    object QrOpcoesCTeDirCTeAss: TWideStringField
      FieldName = 'DirCTeAss'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEnvLot: TWideStringField
      FieldName = 'DirCTeEnvLot'
      Size = 255
    end
    object QrOpcoesCTeDirCTeRec: TWideStringField
      FieldName = 'DirCTeRec'
      Size = 255
    end
    object QrOpcoesCTeDirCTePedRec: TWideStringField
      FieldName = 'DirCTePedRec'
      Size = 255
    end
    object QrOpcoesCTeDirCTeProRec: TWideStringField
      FieldName = 'DirCTeProRec'
      Size = 255
    end
    object QrOpcoesCTeDirCTeDen: TWideStringField
      FieldName = 'DirCTeDen'
      Size = 255
    end
    object QrOpcoesCTeDirCTePedCan: TWideStringField
      FieldName = 'DirCTePedCan'
      Size = 255
    end
    object QrOpcoesCTeDirCTeCan: TWideStringField
      FieldName = 'DirCTeCan'
      Size = 255
    end
    object QrOpcoesCTeDirCTePedInu: TWideStringField
      FieldName = 'DirCTePedInu'
      Size = 255
    end
    object QrOpcoesCTeDirCTeInu: TWideStringField
      FieldName = 'DirCTeInu'
      Size = 255
    end
    object QrOpcoesCTeDirCTePedSit: TWideStringField
      FieldName = 'DirCTePedSit'
      Size = 255
    end
    object QrOpcoesCTeDirCTeSit: TWideStringField
      FieldName = 'DirCTeSit'
      Size = 255
    end
    object QrOpcoesCTeDirCTePedSta: TWideStringField
      FieldName = 'DirCTePedSta'
      Size = 255
    end
    object QrOpcoesCTeDirCTeSta: TWideStringField
      FieldName = 'DirCTeSta'
      Size = 255
    end
    object QrOpcoesCTeCTeUF_WebServ: TWideStringField
      FieldName = 'CTeUF_WebServ'
      Size = 2
    end
    object QrOpcoesCTeCTeUF_Servico: TWideStringField
      FieldName = 'CTeUF_Servico'
      Size = 10
    end
    object QrOpcoesCTePathLogoCTe: TWideStringField
      FieldName = 'PathLogoCTe'
      Size = 255
    end
    object QrOpcoesCTeCtaFretPrest: TIntegerField
      FieldName = 'CtaFretPrest'
    end
    object QrOpcoesCTeTxtFretPrest: TWideStringField
      FieldName = 'TxtFretPrest'
      Size = 100
    end
    object QrOpcoesCTeDupFretPrest: TWideStringField
      FieldName = 'DupFretPrest'
      Size = 3
    end
    object QrOpcoesCTeCTeSerNum: TWideStringField
      FieldName = 'CTeSerNum'
      Size = 255
    end
    object QrOpcoesCTeCTeSerVal: TDateField
      FieldName = 'CTeSerVal'
    end
    object QrOpcoesCTeCTeSerAvi: TSmallintField
      FieldName = 'CTeSerAvi'
    end
    object QrOpcoesCTeDirDACTes: TWideStringField
      FieldName = 'DirDACTes'
      Size = 255
    end
    object QrOpcoesCTeDirCTeProt: TWideStringField
      FieldName = 'DirCTeProt'
      Size = 255
    end
    object QrOpcoesCTeCTePreMailAut: TIntegerField
      FieldName = 'CTePreMailAut'
    end
    object QrOpcoesCTeCTePreMailCan: TIntegerField
      FieldName = 'CTePreMailCan'
    end
    object QrOpcoesCTeCTePreMailEveCCe: TIntegerField
      FieldName = 'CTePreMailEveCCe'
    end
    object QrOpcoesCTeCTeVerStaSer: TFloatField
      FieldName = 'CTeVerStaSer'
    end
    object QrOpcoesCTeCTeVerEnvLot: TFloatField
      FieldName = 'CTeVerEnvLot'
    end
    object QrOpcoesCTeCTeVerConLot: TFloatField
      FieldName = 'CTeVerConLot'
    end
    object QrOpcoesCTeCTeVerCanCTe: TFloatField
      FieldName = 'CTeVerCanCTe'
    end
    object QrOpcoesCTeCTeVerInuNum: TFloatField
      FieldName = 'CTeVerInuNum'
    end
    object QrOpcoesCTeCTeVerConCTe: TFloatField
      FieldName = 'CTeVerConCTe'
    end
    object QrOpcoesCTeCTeVerLotEve: TFloatField
      FieldName = 'CTeVerLotEve'
    end
    object QrOpcoesCTeCTeide_tpImp: TSmallintField
      FieldName = 'CTeide_tpImp'
    end
    object QrOpcoesCTeCTeide_tpAmb: TSmallintField
      FieldName = 'CTeide_tpAmb'
    end
    object QrOpcoesCTeCTeAppCode: TSmallintField
      FieldName = 'CTeAppCode'
    end
    object QrOpcoesCTeCTeAssDigMode: TSmallintField
      FieldName = 'CTeAssDigMode'
    end
    object QrOpcoesCTeCTeEntiTipCto: TIntegerField
      FieldName = 'CTeEntiTipCto'
    end
    object QrOpcoesCTeCTeEntiTipCt1: TIntegerField
      FieldName = 'CTeEntiTipCt1'
    end
    object QrOpcoesCTeMyEmailCTe: TWideStringField
      FieldName = 'MyEmailCTe'
      Size = 255
    end
    object QrOpcoesCTeDirCTeSchema: TWideStringField
      FieldName = 'DirCTeSchema'
      Size = 255
    end
    object QrOpcoesCTeDirCTeRWeb: TWideStringField
      FieldName = 'DirCTeRWeb'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEveEnvLot: TWideStringField
      FieldName = 'DirCTeEveEnvLot'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEveRetLot: TWideStringField
      FieldName = 'DirCTeEveRetLot'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEvePedCCe: TWideStringField
      FieldName = 'DirCTeEvePedCCe'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEveRetCCe: TWideStringField
      FieldName = 'DirCTeEveRetCCe'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEveProcCCe: TWideStringField
      FieldName = 'DirCTeEveProcCCe'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEvePedCan: TWideStringField
      FieldName = 'DirCTeEvePedCan'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEveRetCan: TWideStringField
      FieldName = 'DirCTeEveRetCan'
      Size = 255
    end
    object QrOpcoesCTeDirCTeRetCTeDes: TWideStringField
      FieldName = 'DirCTeRetCTeDes'
      Size = 255
    end
    object QrOpcoesCTeCTeCTeVerConDes: TFloatField
      FieldName = 'CTeCTeVerConDes'
    end
    object QrOpcoesCTeDirCTeEvePedMDe: TWideStringField
      FieldName = 'DirCTeEvePedMDe'
      Size = 255
    end
    object QrOpcoesCTeDirCTeEveRetMDe: TWideStringField
      FieldName = 'DirCTeEveRetMDe'
      Size = 255
    end
    object QrOpcoesCTeDirDowCTeDes: TWideStringField
      FieldName = 'DirDowCTeDes'
      Size = 255
    end
    object QrOpcoesCTeDirDowCTeCTe: TWideStringField
      FieldName = 'DirDowCTeCTe'
      Size = 255
    end
    object QrOpcoesCTeCTeInfCpl: TIntegerField
      FieldName = 'CTeInfCpl'
    end
    object QrOpcoesCTeCTeVerConsCad: TFloatField
      FieldName = 'CTeVerConsCad'
    end
    object QrOpcoesCTeCTeVerDistDFeInt: TFloatField
      FieldName = 'CTeVerDistDFeInt'
    end
    object QrOpcoesCTeCTeVerDowCTe: TFloatField
      FieldName = 'CTeVerDowCTe'
    end
    object QrOpcoesCTeDirDowCTeCnf: TWideStringField
      FieldName = 'DirDowCTeCnf'
      Size = 255
    end
    object QrOpcoesCTeCTeItsLin: TSmallintField
      FieldName = 'CTeItsLin'
    end
    object QrOpcoesCTeCTeFTRazao: TSmallintField
      FieldName = 'CTeFTRazao'
    end
    object QrOpcoesCTeCTeFTEnder: TSmallintField
      FieldName = 'CTeFTEnder'
    end
    object QrOpcoesCTeCTeFTFones: TSmallintField
      FieldName = 'CTeFTFones'
    end
    object QrOpcoesCTeCTeMaiusc: TSmallintField
      FieldName = 'CTeMaiusc'
    end
    object QrOpcoesCTeCTeShowURL: TWideStringField
      FieldName = 'CTeShowURL'
      Size = 255
    end
    object QrOpcoesCTeCTetpEmis: TSmallintField
      FieldName = 'CTetpEmis'
    end
    object QrOpcoesCTeCTeSCAN_Ser: TIntegerField
      FieldName = 'CTeSCAN_Ser'
    end
    object QrOpcoesCTeCTeSCAN_nCT: TIntegerField
      FieldName = 'CTeSCAN_nCT'
    end
    object QrOpcoesCTeCTe_indFinalCpl: TSmallintField
      FieldName = 'CTe_indFinalCpl'
    end
    object QrOpcoesCTeTZD_UTC_Auto_TermoAceite: TIntegerField
      FieldName = 'TZD_UTC_Auto_TermoAceite'
    end
    object QrOpcoesCTeTZD_UTC_Auto_DataHoraAceite: TDateTimeField
      FieldName = 'TZD_UTC_Auto_DataHoraAceite'
    end
    object QrOpcoesCTeTZD_UTC_Auto_UsuarioAceite: TIntegerField
      FieldName = 'TZD_UTC_Auto_UsuarioAceite'
    end
    object QrOpcoesCTeTZD_UTC_Auto: TSmallintField
      FieldName = 'TZD_UTC_Auto'
    end
    object QrOpcoesCTeTZD_UTC: TFloatField
      FieldName = 'TZD_UTC'
    end
    object QrOpcoesCTehVeraoAsk: TDateField
      FieldName = 'hVeraoAsk'
    end
    object QrOpcoesCTehVeraoIni: TDateField
      FieldName = 'hVeraoIni'
    end
    object QrOpcoesCTehVeraoFim: TDateField
      FieldName = 'hVeraoFim'
    end
    object QrOpcoesCTeParamsCTe: TIntegerField
      FieldName = 'ParamsCTe'
    end
    object QrOpcoesCTefaturasep: TWideStringField
      FieldName = 'faturasep'
      Origin = 'paramsemp.FaturaSep'
      Size = 1
    end
    object QrOpcoesCTefaturaseq: TSmallintField
      FieldName = 'faturaseq'
      Origin = 'paramsemp.FaturaSeq'
    end
    object QrOpcoesCTeFaturaNum: TSmallintField
      FieldName = 'FaturaNum'
      Origin = 'paramsemp.FaturaNum'
    end
    object QrOpcoesCTeCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrOpcoesCTeCTeVerEPEC: TFloatField
      FieldName = 'CTeVerEPEC'
    end
  end
  object QrFrtFatCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatcab'
      'WHERE Codigo=:P0')
    Left = 112
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatCabrefCTE: TWideStringField
      FieldName = 'refCTE'
      Size = 44
    end
    object QrFrtFatCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatCabFrtRegFisC: TIntegerField
      FieldName = 'FrtRegFisC'
    end
    object QrFrtFatCabTomador: TIntegerField
      FieldName = 'Tomador'
    end
    object QrFrtFatCabRemetente: TIntegerField
      FieldName = 'Remetente'
    end
    object QrFrtFatCabExpedidor: TIntegerField
      FieldName = 'Expedidor'
    end
    object QrFrtFatCabRecebedor: TIntegerField
      FieldName = 'Recebedor'
    end
    object QrFrtFatCabDestinatario: TIntegerField
      FieldName = 'Destinatario'
    end
    object QrFrtFatCabToma4: TIntegerField
      FieldName = 'Toma4'
    end
    object QrFrtFatCabTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
    end
    object QrFrtFatCabCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrFrtFatCabCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrFrtFatCabFrtRegFatC: TIntegerField
      FieldName = 'FrtRegFatC'
    end
    object QrFrtFatCabForPag: TSmallintField
      FieldName = 'ForPag'
    end
    object QrFrtFatCabModal: TSmallintField
      FieldName = 'Modal'
    end
    object QrFrtFatCabTpServ: TSmallintField
      FieldName = 'TpServ'
    end
    object QrFrtFatCabCMunEnv: TIntegerField
      FieldName = 'CMunEnv'
    end
    object QrFrtFatCabUFEnv: TWideStringField
      FieldName = 'UFEnv'
      Size = 2
    end
    object QrFrtFatCabCMunIni: TIntegerField
      FieldName = 'CMunIni'
    end
    object QrFrtFatCabUFIni: TWideStringField
      FieldName = 'UFIni'
      Size = 2
    end
    object QrFrtFatCabCMunFim: TIntegerField
      FieldName = 'CMunFim'
    end
    object QrFrtFatCabUFFim: TWideStringField
      FieldName = 'UFFim'
      Size = 2
    end
    object QrFrtFatCabToma: TSmallintField
      FieldName = 'Toma'
    end
    object QrFrtFatCabCaracAd: TIntegerField
      FieldName = 'CaracAd'
    end
    object QrFrtFatCabCaracSer: TIntegerField
      FieldName = 'CaracSer'
    end
    object QrFrtFatCabxObs: TWideMemoField
      FieldName = 'xObs'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFrtFatCabRetira: TSmallintField
      FieldName = 'Retira'
    end
    object QrFrtFatCabvCarga: TFloatField
      FieldName = 'vCarga'
    end
    object QrFrtFatCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFrtFatCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrFrtFatCabAbertura: TDateTimeField
      FieldName = 'Abertura'
    end
    object QrFrtFatCabEncerrou: TDateTimeField
      FieldName = 'Encerrou'
    end
    object QrFrtFatCabvTPrest: TFloatField
      FieldName = 'vTPrest'
    end
    object QrFrtFatCabvRec: TFloatField
      FieldName = 'vRec'
    end
    object QrFrtFatCabDataFat: TDateTimeField
      FieldName = 'DataFat'
    end
    object QrFrtFatCabStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrFrtFatCabSerieDesfe: TIntegerField
      FieldName = 'SerieDesfe'
    end
    object QrFrtFatCabCTDesfeita: TIntegerField
      FieldName = 'CTDesfeita'
    end
    object QrFrtFatCabXDetRetira: TWideStringField
      FieldName = 'XDetRetira'
      Size = 160
    end
    object QrFrtFatCabEntiEmi: TIntegerField
      FieldName = 'EntiEmi'
    end
    object QrFrtFatCabFrtProPred: TIntegerField
      FieldName = 'FrtProPred'
    end
    object QrFrtFatCabxOutCat: TWideStringField
      FieldName = 'xOutCat'
      Size = 30
    end
    object QrFrtFatCabvOrig: TFloatField
      FieldName = 'vOrig'
    end
    object QrFrtFatCabvDesc: TFloatField
      FieldName = 'vDesc'
    end
    object QrFrtFatCabvLiq: TFloatField
      FieldName = 'vLiq'
    end
    object QrFrtFatCabLocColeta: TIntegerField
      FieldName = 'LocColeta'
    end
    object QrFrtFatCabLocEnt: TIntegerField
      FieldName = 'LocEnt'
    end
    object QrFrtFatCabParamsCTe: TIntegerField
      FieldName = 'ParamsCTe'
    end
    object QrFrtFatCabDataPrev: TDateTimeField
      FieldName = 'DataPrev'
    end
    object QrFrtFatCabLota: TSmallintField
      FieldName = 'Lota'
    end
    object QrFrtFatCabCIOT: TLargeintField
      FieldName = 'CIOT'
    end
    object QrFrtFatCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrFrtFatCabNatOp: TWideStringField
      FieldName = 'NatOp'
      Size = 60
    end
    object QrFrtFatCabCFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 5
    end
    object QrFrtFatCabTpCTe: TSmallintField
      FieldName = 'TpCTe'
    end
    object QrFrtFatCabMyCST: TSmallintField
      FieldName = 'MyCST'
    end
    object QrFrtFatCabICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrFrtFatCabICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrFrtFatCabICMS_pICMSSTRet: TFloatField
      FieldName = 'ICMS_pICMSSTRet'
    end
    object QrFrtFatCabICMS_pRedBCOutraUF: TFloatField
      FieldName = 'ICMS_pRedBCOutraUF'
    end
    object QrFrtFatCabICMS_pICMSOutraUF: TFloatField
      FieldName = 'ICMS_pICMSOutraUF'
    end
    object QrFrtFatCabiTotTrib_Fonte: TIntegerField
      FieldName = 'iTotTrib_Fonte'
    end
    object QrFrtFatCabiTotTrib_Tabela: TIntegerField
      FieldName = 'iTotTrib_Tabela'
    end
    object QrFrtFatCabiTotTrib_Ex: TIntegerField
      FieldName = 'iTotTrib_Ex'
    end
    object QrFrtFatCabiTotTrib_Codigo: TLargeintField
      FieldName = 'iTotTrib_Codigo'
    end
  end
  object _QrDest_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, '
      'en.EstrangDef, en.EstrangTip, en.EstrangNum, en.indIEDest,'
      'en.IE, en.RG, en.NIRE, en.CNAE, L_CNPJ, L_Ativo, '
      'LLograd, LRua, LCompl, LNumero, LBairro, LCidade, '
      'LUF, l2.Nome NO_LLOGRAD, LCodMunici, '
      'ml.Nome NO_LMunici, ul.Nome NO_LUF, '
      'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF, '
      'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT, '
      'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA, '
      'IF(en.Tipo=0,en.ERua,en.PRua) RUA, '
      'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero, '
      'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL, '
      'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO, '
      'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP, '
      'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1, '
      'IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) + 0.000 CodiPais, '
      'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici, '
      'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF, '
      'bpa.Nome NO_Pais, en.SUFRAMA '
      'FROM entidades en '
      
        'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en' +
        '.PLograd) '
      'LEFT JOIN listalograd l2 ON l2.Codigo=en.LLograd '
      
        'LEFT JOIN loclogiall.dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.' +
        'ECodMunici,en.PCodMunici) '
      'LEFT JOIN loclogiall.dtb_munici ml ON ml.Codigo=en.LCodMunici '
      'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF) '
      'LEFT JOIN ufs ul ON ul.Codigo=en.LUF '
      
        'LEFT JOIN loclogiall.bacen_pais bpa ON bpa.Codigo=IF(en.Tipo=0,e' +
        'n.ECodiPais,en.PCodiPais) '
      'WHERE en.Codigo=2')
    Left = 112
    Top = 100
    object _QrDest_Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object _QrDest_Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object _QrDest_CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object _QrDest_CPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object _QrDest_EstrangDef: TSmallintField
      FieldName = 'EstrangDef'
    end
    object _QrDest_EstrangTip: TSmallintField
      FieldName = 'EstrangTip'
    end
    object _QrDest_EstrangNum: TWideStringField
      FieldName = 'EstrangNum'
    end
    object _QrDest_indIEDest: TSmallintField
      FieldName = 'indIEDest'
    end
    object _QrDest_IE: TWideStringField
      FieldName = 'IE'
    end
    object _QrDest_RG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object _QrDest_NIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object _QrDest_CNAE: TWideStringField
      FieldName = 'CNAE'
      Size = 7
    end
    object _QrDest_L_CNPJ: TWideStringField
      FieldName = 'L_CNPJ'
      Size = 14
    end
    object _QrDest_L_Ativo: TSmallintField
      FieldName = 'L_Ativo'
    end
    object _QrDest_LLograd: TSmallintField
      FieldName = 'LLograd'
    end
    object _QrDest_LRua: TWideStringField
      FieldName = 'LRua'
      Size = 60
    end
    object _QrDest_LCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 60
    end
    object _QrDest_LNumero: TIntegerField
      FieldName = 'LNumero'
    end
    object _QrDest_LBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 60
    end
    object _QrDest_LCidade: TWideStringField
      FieldName = 'LCidade'
      Size = 60
    end
    object _QrDest_LUF: TSmallintField
      FieldName = 'LUF'
    end
    object _QrDest_NO_LLOGRAD: TWideStringField
      FieldName = 'NO_LLOGRAD'
      Size = 15
    end
    object _QrDest_LCodMunici: TIntegerField
      FieldName = 'LCodMunici'
    end
    object _QrDest_NO_LMunici: TWideStringField
      FieldName = 'NO_LMunici'
      Size = 100
    end
    object _QrDest_NO_LUF: TWideStringField
      FieldName = 'NO_LUF'
      Required = True
      Size = 2
    end
    object _QrDest_UF: TFloatField
      FieldName = 'UF'
      Required = True
    end
    object _QrDest_NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object _QrDest_FANTASIA: TWideStringField
      FieldName = 'FANTASIA'
      Size = 60
    end
    object _QrDest_RUA: TWideStringField
      FieldName = 'RUA'
      Size = 60
    end
    object _QrDest_Numero: TFloatField
      FieldName = 'Numero'
    end
    object _QrDest_COMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object _QrDest_BAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
    object _QrDest_CEP: TFloatField
      FieldName = 'CEP'
    end
    object _QrDest_Te1: TWideStringField
      FieldName = 'Te1'
    end
    object _QrDest_CodiPais: TFloatField
      FieldName = 'CodiPais'
      Required = True
    end
    object _QrDest_CodMunici: TFloatField
      FieldName = 'CodMunici'
      Required = True
    end
    object _QrDest_NO_LOGRAD: TWideStringField
      FieldName = 'NO_LOGRAD'
      Size = 15
    end
    object _QrDest_NO_Munici: TWideStringField
      FieldName = 'NO_Munici'
      Required = True
      Size = 100
    end
    object _QrDest_NO_UF: TWideStringField
      FieldName = 'NO_UF'
      Required = True
      Size = 2
    end
    object _QrDest_NO_Pais: TWideStringField
      FieldName = 'NO_Pais'
      Required = True
      Size = 100
    end
    object _QrDest_SUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Size = 9
    end
  end
  object QrCTeLayI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT layc.Nome NO_Grupo, layi.* '
      'FROM ctelayi layi '
      'LEFT JOIN ctelayc layc ON layc.Grupo=layi.Grupo '
      'WHERE layi.Versao>0')
    Left = 112
    Top = 4
    object QrCTeLayINO_Grupo: TWideStringField
      FieldName = 'NO_Grupo'
      Size = 100
    end
    object QrCTeLayIVersao: TFloatField
      FieldName = 'Versao'
    end
    object QrCTeLayIGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrCTeLayICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCTeLayICampo: TWideStringField
      FieldName = 'Campo'
      Size = 30
    end
    object QrCTeLayINivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrCTeLayIDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object QrCTeLayIElemento: TWideStringField
      FieldName = 'Elemento'
      Size = 10
    end
    object QrCTeLayITipo: TWideStringField
      FieldName = 'Tipo'
      Size = 1
    end
    object QrCTeLayIOcorMin: TSmallintField
      FieldName = 'OcorMin'
    end
    object QrCTeLayIOcorMax: TIntegerField
      FieldName = 'OcorMax'
    end
    object QrCTeLayITamMin: TSmallintField
      FieldName = 'TamMin'
    end
    object QrCTeLayITamMax: TIntegerField
      FieldName = 'TamMax'
    end
    object QrCTeLayITamVar: TWideStringField
      FieldName = 'TamVar'
      Size = 30
    end
    object QrCTeLayIObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCTeLayIInfoVazio: TSmallintField
      FieldName = 'InfoVazio'
    end
    object QrCTeLayIDominio: TWideStringField
      FieldName = 'Dominio'
      Size = 10
    end
    object QrCTeLayIExpReg: TWideStringField
      FieldName = 'ExpReg'
      Size = 10
    end
    object QrCTeLayIFormatStr: TWideStringField
      FieldName = 'FormatStr'
      Size = 30
    end
    object QrCTeLayIDeciCasas: TSmallintField
      FieldName = 'DeciCasas'
    end
    object QrCTeLayIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeLayIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object _QrFrtRegFisC_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtregfisc')
    Left = 112
    Top = 148
    object _QrFrtRegFisC_Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object _QrFrtRegFisC_Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object _QrFrtRegFisC_NatOp: TWideStringField
      FieldName = 'NatOp'
      Size = 60
    end
    object _QrFrtRegFisC_CFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 5
    end
    object _QrFrtRegFisC_TpCTe: TSmallintField
      FieldName = 'TpCTe'
    end
    object _QrFrtRegFisC_MyCST: TSmallintField
      FieldName = 'MyCST'
    end
    object _QrFrtRegFisC_ICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object _QrFrtRegFisC_ICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object _QrFrtRegFisC_ICMS_pICMSSTRet: TFloatField
      FieldName = 'ICMS_pICMSSTRet'
    end
    object _QrFrtRegFisC_ICMS_pRedBCOutraUF: TFloatField
      FieldName = 'ICMS_pRedBCOutraUF'
    end
    object _QrFrtRegFisC_ICMS_pICMSOutraUF: TFloatField
      FieldName = 'ICMS_pICMSOutraUF'
    end
    object _QrFrtRegFisC_iTotTrib_Fonte: TIntegerField
      FieldName = 'iTotTrib_Fonte'
    end
    object _QrFrtRegFisC_iTotTrib_Tabela: TIntegerField
      FieldName = 'iTotTrib_Tabela'
    end
    object _QrFrtRegFisC_iTotTrib_Ex: TIntegerField
      FieldName = 'iTotTrib_Ex'
    end
    object _QrFrtRegFisC_iTotTrib_Codigo: TLargeintField
      FieldName = 'iTotTrib_Codigo'
    end
  end
  object QrEntiCTe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Tipo,'
      'IF(ent.Tipo=0, ent.CNPJ, "") CNPJ,     '
      'IF(ent.Tipo=1, ent.CPF, "") CPF,     '
      'IF(ent.Tipo=0, ent.IE, "") IE,     '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) xNome,     '
      'IF(ent.Tipo=0, ent.Fantasia, ent.Apelido) xFant,     '
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Fone, '
      'IF(ent.Tipo=0, ent.ERua, ent.PRua) xLgr, '
      
        'IF(IF(ent.Tipo=0, ent.ENumero, ent.PNumero) <>0, IF(ent.Tipo=0, ' +
        'ent.ENumero, ent.PNumero), "S/N") Nro, '
      'IF(ent.Tipo=0, ent.ECompl,   ent.PCompl) xCpl, '
      'IF(ent.Tipo=0, ent.EBairro,   ent.PBairro) xBairro, '
      'IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici) + 0.000 CMun, '
      'mun.Nome xMun, '
      'IF(ent.Tipo=0,  ent.ECEP, ent.PCEP) + 0.000 CEP, '
      'ufs.Nome UF,'
      'IF(ent.Tipo=0, ent.ECodiPais, ent.PCodiPais) + 0.000 CPais,'
      'bpa.Nome xPais, '
      'IF(ent.Tipo=0, ent.EEmail, ent.PEmail) Email'
      'FROM entidades ent '
      
        'LEFT JOIN loclogiall.dtb_munici mun ON mun.Codigo=IF(ent.Tipo=0,' +
        ' ent.ECodMunici, ent.PCodMunici) '
      
        'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)' +
        ' '
      
        'LEFT JOIN loclogiall.bacen_pais bpa ON bpa.Codigo=IF(ent.Tipo=0,' +
        ' ent.ECodiPais, ent.PCodiPais) '
      'WHERE ent.Codigo=:P0')
    Left = 112
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiCTeCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEntiCTeCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEntiCTeIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEntiCTexNome: TWideStringField
      FieldName = 'xNome'
      Size = 100
    end
    object QrEntiCTexFant: TWideStringField
      FieldName = 'xFant'
      Size = 60
    end
    object QrEntiCTefone: TWideStringField
      FieldName = 'fone'
    end
    object QrEntiCTeTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntiCTexLgr: TWideStringField
      FieldName = 'xLgr'
      Size = 60
    end
    object QrEntiCTexCpl: TWideStringField
      FieldName = 'xCpl'
      Size = 60
    end
    object QrEntiCTexBairro: TWideStringField
      FieldName = 'xBairro'
      Size = 60
    end
    object QrEntiCTeCMun: TFloatField
      FieldName = 'CMun'
      Required = True
    end
    object QrEntiCTexMun: TWideStringField
      FieldName = 'xMun'
      Size = 100
    end
    object QrEntiCTeCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEntiCTeUF: TWideStringField
      FieldName = 'UF'
      Required = True
      Size = 2
    end
    object QrEntiCTeCPais: TFloatField
      FieldName = 'CPais'
      Required = True
    end
    object QrEntiCTexPais: TWideStringField
      FieldName = 'xPais'
      Size = 100
    end
    object QrEntiCTeEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrEntiCTeNro: TWideStringField
      FieldName = 'Nro'
      Size = 60
    end
    object QrEntiCTeSUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Size = 9
    end
  end
  object QrIBPTax: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM ibptax'
      'WHERE Tabela=0'
      'AND Codigo=5021019'
      'AND Ex=0')
    Left = 112
    Top = 244
    object QrIBPTaxCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrIBPTaxEx: TIntegerField
      FieldName = 'Ex'
    end
    object QrIBPTaxTabela: TIntegerField
      FieldName = 'Tabela'
    end
    object QrIBPTaxNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrIBPTaxDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrIBPTaxAliqNac: TFloatField
      FieldName = 'AliqNac'
    end
    object QrIBPTaxAliqImp: TFloatField
      FieldName = 'AliqImp'
    end
    object QrIBPTaxVersao: TWideStringField
      FieldName = 'Versao'
    end
    object QrIBPTaxLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrIBPTaxDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrIBPTaxDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrIBPTaxUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrIBPTaxUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrIBPTaxAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrIBPTaxAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeCabA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctecaba'
      'WHERE FatID=:p0')
    Left = 204
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end>
    object QrCTeCabAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCTeCabALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrCTeCabAversao: TFloatField
      FieldName = 'versao'
    end
    object QrCTeCabAId: TWideStringField
      FieldName = 'Id'
      Size = 47
    end
    object QrCTeCabAide_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrCTeCabAide_cCT: TIntegerField
      FieldName = 'ide_cCT'
    end
    object QrCTeCabAide_CFOP: TIntegerField
      FieldName = 'ide_CFOP'
    end
    object QrCTeCabAide_natOp: TWideStringField
      FieldName = 'ide_natOp'
      Size = 60
    end
    object QrCTeCabAide_forPag: TSmallintField
      FieldName = 'ide_forPag'
    end
    object QrCTeCabAide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrCTeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrCTeCabAide_nCT: TIntegerField
      FieldName = 'ide_nCT'
    end
    object QrCTeCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrCTeCabAide_hEmi: TTimeField
      FieldName = 'ide_hEmi'
    end
    object QrCTeCabAide_dhEmiTZD: TFloatField
      FieldName = 'ide_dhEmiTZD'
    end
    object QrCTeCabAide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
    end
    object QrCTeCabAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrCTeCabAide_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrCTeCabAide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrCTeCabAide_tpCTe: TSmallintField
      FieldName = 'ide_tpCTe'
    end
    object QrCTeCabAide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrCTeCabAide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrCTeCabAide_refCTE: TWideStringField
      FieldName = 'ide_refCTE'
      Size = 44
    end
    object QrCTeCabAide_cMunEnv: TIntegerField
      FieldName = 'ide_cMunEnv'
    end
    object QrCTeCabAide_xMunEnv: TWideStringField
      FieldName = 'ide_xMunEnv'
      Size = 60
    end
    object QrCTeCabAide_UFEnv: TWideStringField
      FieldName = 'ide_UFEnv'
      Size = 2
    end
    object QrCTeCabAide_Modal: TSmallintField
      FieldName = 'ide_Modal'
    end
    object QrCTeCabAide_tpServ: TSmallintField
      FieldName = 'ide_tpServ'
    end
    object QrCTeCabAide_cMunIni: TIntegerField
      FieldName = 'ide_cMunIni'
    end
    object QrCTeCabAide_xMunIni: TWideStringField
      FieldName = 'ide_xMunIni'
      Size = 60
    end
    object QrCTeCabAide_UFIni: TWideStringField
      FieldName = 'ide_UFIni'
      Size = 2
    end
    object QrCTeCabAide_cMunFim: TIntegerField
      FieldName = 'ide_cMunFim'
    end
    object QrCTeCabAide_xMunFim: TWideStringField
      FieldName = 'ide_xMunFim'
      Size = 60
    end
    object QrCTeCabAide_UFFim: TWideStringField
      FieldName = 'ide_UFFim'
      Size = 2
    end
    object QrCTeCabAide_Retira: TSmallintField
      FieldName = 'ide_Retira'
    end
    object QrCTeCabAide_xDetRetira: TWideStringField
      FieldName = 'ide_xDetRetira'
      Size = 160
    end
    object QrCTeCabAtoma99_toma: TSmallintField
      FieldName = 'toma99_toma'
    end
    object QrCTeCabAtoma04_toma: TSmallintField
      FieldName = 'toma04_toma'
    end
    object QrCTeCabAtoma04_CNPJ: TWideStringField
      FieldName = 'toma04_CNPJ'
      Size = 14
    end
    object QrCTeCabAtoma04_CPF: TWideStringField
      FieldName = 'toma04_CPF'
      Size = 11
    end
    object QrCTeCabAtoma04_IE: TWideStringField
      FieldName = 'toma04_IE'
      Size = 14
    end
    object QrCTeCabAtoma04_xNome: TWideStringField
      FieldName = 'toma04_xNome'
      Size = 60
    end
    object QrCTeCabAtoma04_xFant: TWideStringField
      FieldName = 'toma04_xFant'
      Size = 60
    end
    object QrCTeCabAtoma04_fone: TWideStringField
      FieldName = 'toma04_fone'
      Size = 14
    end
    object QrCTeCabAide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
    end
    object QrCTeCabAide_dhContTZD: TFloatField
      FieldName = 'ide_dhContTZD'
    end
    object QrCTeCabAide_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Size = 255
    end
    object QrCTeCabAcompl_xCaracAd: TWideStringField
      FieldName = 'compl_xCaracAd'
      Size = 15
    end
    object QrCTeCabAcompl_xCaracSer: TWideStringField
      FieldName = 'compl_xCaracSer'
      Size = 30
    end
    object QrCTeCabAcompl_xEmi: TWideStringField
      FieldName = 'compl_xEmi'
    end
    object QrCTeCabAentrega_tpPer: TSmallintField
      FieldName = 'entrega_tpPer'
    end
    object QrCTeCabAentrega_dProg: TDateField
      FieldName = 'entrega_dProg'
    end
    object QrCTeCabAentrega_dIni: TDateField
      FieldName = 'entrega_dIni'
    end
    object QrCTeCabAentrega_dFim: TDateField
      FieldName = 'entrega_dFim'
    end
    object QrCTeCabAentrega_tpHor: TSmallintField
      FieldName = 'entrega_tpHor'
    end
    object QrCTeCabAentrega_hProg: TTimeField
      FieldName = 'entrega_hProg'
    end
    object QrCTeCabAentrega_hIni: TDateField
      FieldName = 'entrega_hIni'
    end
    object QrCTeCabAentrega_hFim: TDateField
      FieldName = 'entrega_hFim'
    end
    object QrCTeCabAide_origCalc: TWideStringField
      FieldName = 'ide_origCalc'
      Size = 40
    end
    object QrCTeCabAide_destCalc: TWideStringField
      FieldName = 'ide_destCalc'
      Size = 40
    end
    object QrCTeCabAide_xObs: TWideMemoField
      FieldName = 'ide_xObs'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCTeCabAemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrCTeCabAemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrCTeCabAemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrCTeCabAemit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Size = 60
    end
    object QrCTeCabAemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Size = 60
    end
    object QrCTeCabAemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Size = 60
    end
    object QrCTeCabAemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Size = 60
    end
    object QrCTeCabAemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Size = 60
    end
    object QrCTeCabAemit_cMun: TIntegerField
      FieldName = 'emit_cMun'
    end
    object QrCTeCabAemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrCTeCabAemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
    end
    object QrCTeCabAemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrCTeCabAemit_fone: TWideStringField
      FieldName = 'emit_fone'
      Size = 14
    end
    object QrCTeCabArem_CNPJ: TWideStringField
      FieldName = 'rem_CNPJ'
      Size = 14
    end
    object QrCTeCabArem_CPF: TWideStringField
      FieldName = 'rem_CPF'
      Size = 11
    end
    object QrCTeCabArem_IE: TWideStringField
      FieldName = 'rem_IE'
      Size = 14
    end
    object QrCTeCabArem_xNome: TWideStringField
      FieldName = 'rem_xNome'
      Size = 60
    end
    object QrCTeCabArem_xFant: TWideStringField
      FieldName = 'rem_xFant'
      Size = 60
    end
    object QrCTeCabArem_fone: TWideStringField
      FieldName = 'rem_fone'
      Size = 14
    end
    object QrCTeCabArem_xLgr: TWideStringField
      FieldName = 'rem_xLgr'
      Size = 60
    end
    object QrCTeCabArem_nro: TWideStringField
      FieldName = 'rem_nro'
      Size = 60
    end
    object QrCTeCabArem_xCpl: TWideStringField
      FieldName = 'rem_xCpl'
      Size = 60
    end
    object QrCTeCabArem_xBairro: TWideStringField
      FieldName = 'rem_xBairro'
      Size = 60
    end
    object QrCTeCabArem_cMun: TIntegerField
      FieldName = 'rem_cMun'
    end
    object QrCTeCabArem_xMun: TWideStringField
      FieldName = 'rem_xMun'
      Size = 60
    end
    object QrCTeCabArem_CEP: TIntegerField
      FieldName = 'rem_CEP'
    end
    object QrCTeCabArem_UF: TWideStringField
      FieldName = 'rem_UF'
      Size = 2
    end
    object QrCTeCabArem_cPais: TIntegerField
      FieldName = 'rem_cPais'
    end
    object QrCTeCabArem_xPais: TWideStringField
      FieldName = 'rem_xPais'
      Size = 60
    end
    object QrCTeCabArem_email: TWideStringField
      FieldName = 'rem_email'
      Size = 60
    end
    object QrCTeCabAdest_CNPJ: TWideStringField
      FieldName = 'dest_CNPJ'
      Size = 14
    end
    object QrCTeCabAdest_CPF: TWideStringField
      FieldName = 'dest_CPF'
      Size = 11
    end
    object QrCTeCabAdest_IE: TWideStringField
      FieldName = 'dest_IE'
      Size = 14
    end
    object QrCTeCabAdest_xNome: TWideStringField
      FieldName = 'dest_xNome'
      Size = 60
    end
    object QrCTeCabAdest_fone: TWideStringField
      FieldName = 'dest_fone'
      Size = 14
    end
    object QrCTeCabAdest_ISUF: TWideStringField
      FieldName = 'dest_ISUF'
      Size = 9
    end
    object QrCTeCabAdest_xLgr: TWideStringField
      FieldName = 'dest_xLgr'
      Size = 60
    end
    object QrCTeCabAdest_nro: TWideStringField
      FieldName = 'dest_nro'
      Size = 60
    end
    object QrCTeCabAdest_xCpl: TWideStringField
      FieldName = 'dest_xCpl'
      Size = 60
    end
    object QrCTeCabAdest_xBairro: TWideStringField
      FieldName = 'dest_xBairro'
      Size = 60
    end
    object QrCTeCabAdest_cMun: TIntegerField
      FieldName = 'dest_cMun'
    end
    object QrCTeCabAdest_xMun: TWideStringField
      FieldName = 'dest_xMun'
      Size = 60
    end
    object QrCTeCabAdest_CEP: TIntegerField
      FieldName = 'dest_CEP'
    end
    object QrCTeCabAdest_UF: TWideStringField
      FieldName = 'dest_UF'
      Size = 2
    end
    object QrCTeCabAdest_cPais: TIntegerField
      FieldName = 'dest_cPais'
    end
    object QrCTeCabAdest_xPais: TWideStringField
      FieldName = 'dest_xPais'
      Size = 60
    end
    object QrCTeCabAdest_email: TWideStringField
      FieldName = 'dest_email'
      Size = 60
    end
    object QrCTeCabAvPrest_vTPrest: TFloatField
      FieldName = 'vPrest_vTPrest'
    end
    object QrCTeCabAvPrest_vRec: TFloatField
      FieldName = 'vPrest_vRec'
    end
    object QrCTeCabAICMS_CST: TSmallintField
      FieldName = 'ICMS_CST'
    end
    object QrCTeCabAICMS_vBC: TFloatField
      FieldName = 'ICMS_vBC'
    end
    object QrCTeCabAICMS_pICMS: TFloatField
      FieldName = 'ICMS_pICMS'
    end
    object QrCTeCabAICMS_vICMS: TFloatField
      FieldName = 'ICMS_vICMS'
    end
    object QrCTeCabAICMS_pRedBC: TFloatField
      FieldName = 'ICMS_pRedBC'
    end
    object QrCTeCabAICMS_vBCSTRet: TFloatField
      FieldName = 'ICMS_vBCSTRet'
    end
    object QrCTeCabAICMS_vICMSSTRet: TFloatField
      FieldName = 'ICMS_vICMSSTRet'
    end
    object QrCTeCabAICMS_pICMSSTRet: TFloatField
      FieldName = 'ICMS_pICMSSTRet'
    end
    object QrCTeCabAICMS_vCred: TFloatField
      FieldName = 'ICMS_vCred'
    end
    object QrCTeCabAICMS_pRedBCOutraUF: TFloatField
      FieldName = 'ICMS_pRedBCOutraUF'
    end
    object QrCTeCabAICMS_vBCOutraUF: TFloatField
      FieldName = 'ICMS_vBCOutraUF'
    end
    object QrCTeCabAICMS_pICMSOutraUF: TFloatField
      FieldName = 'ICMS_pICMSOutraUF'
    end
    object QrCTeCabAICMS_vICMSOutraUF: TFloatField
      FieldName = 'ICMS_vICMSOutraUF'
    end
    object QrCTeCabAICMSSN_indSN: TSmallintField
      FieldName = 'ICMSSN_indSN'
    end
    object QrCTeCabAimp_vTotTrib: TFloatField
      FieldName = 'imp_vTotTrib'
    end
    object QrCTeCabAimp_infAdFisco: TWideMemoField
      FieldName = 'imp_infAdFisco'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCTeCabAinfCarga_vCarga: TFloatField
      FieldName = 'infCarga_vCarga'
    end
    object QrCTeCabAinfCarga_xOutCat: TWideStringField
      FieldName = 'infCarga_xOutCat'
      Size = 30
    end
    object QrCTeCabAcobr_Fat_nFat: TWideStringField
      FieldName = 'cobr_Fat_nFat'
      Size = 60
    end
    object QrCTeCabAcobr_Fat_vOrig: TFloatField
      FieldName = 'cobr_Fat_vOrig'
    end
    object QrCTeCabAcobr_Fat_vDesc: TFloatField
      FieldName = 'cobr_Fat_vDesc'
    end
    object QrCTeCabAcobr_Fat_vLiq: TFloatField
      FieldName = 'cobr_Fat_vLiq'
    end
    object QrCTeCabAinfCTeComp_chave: TWideStringField
      FieldName = 'infCTeComp_chave'
      Size = 44
    end
    object QrCTeCabAinfCTeAnu_chCte: TWideStringField
      FieldName = 'infCTeAnu_chCte'
      Size = 44
    end
    object QrCTeCabAinfCTeAnu_dEmi: TDateField
      FieldName = 'infCTeAnu_dEmi'
    end
    object QrCTeCabAprotCTe_versao: TFloatField
      FieldName = 'protCTe_versao'
    end
    object QrCTeCabAinfProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 30
    end
    object QrCTeCabAinfProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
    end
    object QrCTeCabAinfProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
      Size = 30
    end
    object QrCTeCabAinfProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
    end
    object QrCTeCabAinfProt_dhRecbtoTZD: TFloatField
      FieldName = 'infProt_dhRecbtoTZD'
    end
    object QrCTeCabAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
    object QrCTeCabAinfProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Size = 28
    end
    object QrCTeCabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrCTeCabAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrCTeCabAretCancCTe_versao: TFloatField
      FieldName = 'retCancCTe_versao'
    end
    object QrCTeCabAinfCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Size = 54
    end
    object QrCTeCabAinfCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
    end
    object QrCTeCabAinfCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Size = 30
    end
    object QrCTeCabAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrCTeCabAinfCanc_dhRecbtoTZD: TFloatField
      FieldName = 'infCanc_dhRecbtoTZD'
    end
    object QrCTeCabAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrCTeCabAinfCanc_digVal: TWideStringField
      FieldName = 'infCanc_digVal'
      Size = 28
    end
    object QrCTeCabAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrCTeCabAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrCTeCabAinfCanc_cJust: TIntegerField
      FieldName = 'infCanc_cJust'
    end
    object QrCTeCabAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrCTeCabAFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
    end
    object QrCTeCabACartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrCTeCabATabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
    end
    object QrCTeCabACondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrCTeCabACodInfoRemet: TIntegerField
      FieldName = 'CodInfoRemet'
    end
    object QrCTeCabACodInfoExped: TIntegerField
      FieldName = 'CodInfoExped'
    end
    object QrCTeCabACodInfoReceb: TIntegerField
      FieldName = 'CodInfoReceb'
    end
    object QrCTeCabACodInfoDesti: TIntegerField
      FieldName = 'CodInfoDesti'
    end
    object QrCTeCabACodInfoToma4: TIntegerField
      FieldName = 'CodInfoToma4'
    end
    object QrCTeCabACriAForca: TSmallintField
      FieldName = 'CriAForca'
    end
    object QrCTeCabAcSitCTe: TSmallintField
      FieldName = 'cSitCTe'
    end
    object QrCTeCabAinfCCe_verAplic: TWideStringField
      FieldName = 'infCCe_verAplic'
      Size = 30
    end
    object QrCTeCabAinfCCe_cOrgao: TSmallintField
      FieldName = 'infCCe_cOrgao'
    end
    object QrCTeCabAinfCCe_tpAmb: TSmallintField
      FieldName = 'infCCe_tpAmb'
    end
    object QrCTeCabAinfCCe_CNPJ: TWideStringField
      FieldName = 'infCCe_CNPJ'
      Size = 18
    end
    object QrCTeCabAinfCCe_CPF: TWideStringField
      FieldName = 'infCCe_CPF'
      Size = 18
    end
    object QrCTeCabAinfCCe_chCTe: TWideStringField
      FieldName = 'infCCe_chCTe'
      Size = 44
    end
    object QrCTeCabAinfCCe_dhEvento: TDateTimeField
      FieldName = 'infCCe_dhEvento'
    end
    object QrCTeCabAinfCCe_dhEventoTZD: TFloatField
      FieldName = 'infCCe_dhEventoTZD'
    end
    object QrCTeCabAinfCCe_tpEvento: TIntegerField
      FieldName = 'infCCe_tpEvento'
    end
    object QrCTeCabAinfCCe_nSeqEvento: TIntegerField
      FieldName = 'infCCe_nSeqEvento'
    end
    object QrCTeCabAinfCCe_verEvento: TFloatField
      FieldName = 'infCCe_verEvento'
    end
    object QrCTeCabAinfCCe_CCeConta: TIntegerField
      FieldName = 'infCCe_CCeConta'
    end
    object QrCTeCabAinfCCe_cStat: TIntegerField
      FieldName = 'infCCe_cStat'
    end
    object QrCTeCabAinfCCe_dhRegEvento: TDateTimeField
      FieldName = 'infCCe_dhRegEvento'
    end
    object QrCTeCabAinfCCe_dhRegEventoTZD: TFloatField
      FieldName = 'infCCe_dhRegEventoTZD'
    end
    object QrCTeCabAinfCCe_nProt: TWideStringField
      FieldName = 'infCCe_nProt'
      Size = 15
    end
    object QrCTeCabAinfCCe_nCondUso: TIntegerField
      FieldName = 'infCCe_nCondUso'
    end
    object QrCTeCabAiTotTrib_Fonte: TIntegerField
      FieldName = 'iTotTrib_Fonte'
    end
    object QrCTeCabAiTotTrib_Tabela: TIntegerField
      FieldName = 'iTotTrib_Tabela'
    end
    object QrCTeCabAiTotTrib_Ex: TIntegerField
      FieldName = 'iTotTrib_Ex'
    end
    object QrCTeCabAiTotTrib_Codigo: TLargeintField
      FieldName = 'iTotTrib_Codigo'
    end
    object QrCTeCabAvBasTrib: TFloatField
      FieldName = 'vBasTrib'
    end
    object QrCTeCabApTotTrib: TFloatField
      FieldName = 'pTotTrib'
    end
    object QrCTeCabAverTotTrib: TWideStringField
      FieldName = 'verTotTrib'
      Required = True
      Size = 30
    end
    object QrCTeCabAtpAliqTotTrib: TSmallintField
      FieldName = 'tpAliqTotTrib'
    end
    object QrCTeCabACTeMyCST: TSmallintField
      FieldName = 'CTeMyCST'
    end
    object QrCTeCabAinfCarga_proPred: TWideStringField
      FieldName = 'infCarga_proPred'
      Size = 60
    end
    object QrCTeCabAinfCteSub_chCte: TWideStringField
      FieldName = 'infCteSub_chCte'
      Size = 44
    end
    object QrCTeCabAinfCteSub_refNFe: TWideStringField
      FieldName = 'infCteSub_refNFe'
      Size = 44
    end
    object QrCTeCabAinfCteSub_CNPJ: TWideStringField
      FieldName = 'infCteSub_CNPJ'
      Size = 14
    end
    object QrCTeCabAinfCteSub_CPF: TWideStringField
      FieldName = 'infCteSub_CPF'
      Size = 11
    end
    object QrCTeCabAinfCteSub_mod: TWideStringField
      FieldName = 'infCteSub_mod'
      Size = 2
    end
    object QrCTeCabAinfCteSub_serie: TIntegerField
      FieldName = 'infCteSub_serie'
    end
    object QrCTeCabAinfCteSub_subserie: TIntegerField
      FieldName = 'infCteSub_subserie'
    end
    object QrCTeCabAinfCteSub_nro: TIntegerField
      FieldName = 'infCteSub_nro'
    end
    object QrCTeCabAinfCteSub_valor: TFloatField
      FieldName = 'infCteSub_valor'
    end
    object QrCTeCabAinfCteSub_dEmi: TDateField
      FieldName = 'infCteSub_dEmi'
    end
    object QrCTeCabAinfCteSub_refCTe: TWideStringField
      FieldName = 'infCteSub_refCTe'
      Size = 44
    end
    object QrCTeCabAinfCteSub_refCTeAnu: TWideStringField
      FieldName = 'infCteSub_refCTeAnu'
      Size = 44
    end
    object QrCTeCabAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrCTeCabAinfCteSub_ContribICMS: TSmallintField
      FieldName = 'infCteSub_ContribICMS'
    end
    object QrCTeCabAinfCteSub_TipoDoc: TSmallintField
      FieldName = 'infCteSub_TipoDoc'
    end
  end
  object QrFrtFatInfQ: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      
        ' ELT(fiq.CUnid + 1,"M3","KG","TON","UNIDADE","LITROS","MMBTU") N' +
        'O_CUnid,'
      'mtm.Nome NO_MyTpMed, '
      'fiq.* '
      'FROM frtfatinfq fiq'
      'LEFT JOIN trnsmytpmed mtm ON mtm.Codigo=fiq.MyTpMed '
      'WHERE fiq.Codigo=1')
    Left = 204
    Top = 49
    object QrFrtFatInfQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatInfQControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatInfQCUnid: TSmallintField
      FieldName = 'CUnid'
    end
    object QrFrtFatInfQMyTpMed: TIntegerField
      FieldName = 'MyTpMed'
    end
    object QrFrtFatInfQQCarga: TFloatField
      FieldName = 'QCarga'
    end
    object QrFrtFatInfQLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatInfQDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatInfQDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatInfQUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatInfQUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatInfQAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatInfQAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFrtFatInfQNO_CUnid: TWideStringField
      DisplayWidth = 15
      FieldName = 'NO_CUnid'
      Size = 15
    end
    object QrFrtFatInfQNO_MyTpMed: TWideStringField
      FieldName = 'NO_MyTpMed'
      Size = 60
    end
  end
  object QrFrtFatInfDoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatinfdoc'
      'WHERE Codigo =:P0')
    Left = 204
    Top = 101
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatInfDocCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatInfDocControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatInfDocMyTpDocInf: TSmallintField
      FieldName = 'MyTpDocInf'
    end
    object QrFrtFatInfDocInfNFe_chave: TWideStringField
      FieldName = 'InfNFe_chave'
      Size = 44
    end
    object QrFrtFatInfDocInfNFe_PIN: TIntegerField
      FieldName = 'InfNFe_PIN'
    end
    object QrFrtFatInfDocInfNFe_dPrev: TDateField
      FieldName = 'InfNFe_dPrev'
    end
    object QrFrtFatInfDocInfNF_nRoma: TWideStringField
      FieldName = 'InfNF_nRoma'
    end
    object QrFrtFatInfDocInfNF_nPed: TWideStringField
      FieldName = 'InfNF_nPed'
    end
    object QrFrtFatInfDocInfNF_mod: TSmallintField
      FieldName = 'InfNF_mod'
    end
    object QrFrtFatInfDocInfNF_serie: TWideStringField
      FieldName = 'InfNF_serie'
      Size = 3
    end
    object QrFrtFatInfDocInfNF_nDoc: TWideStringField
      FieldName = 'InfNF_nDoc'
    end
    object QrFrtFatInfDocInfNF_dEmi: TDateField
      FieldName = 'InfNF_dEmi'
    end
    object QrFrtFatInfDocInfNF_vBC: TFloatField
      FieldName = 'InfNF_vBC'
    end
    object QrFrtFatInfDocInfNF_vICMS: TFloatField
      FieldName = 'InfNF_vICMS'
    end
    object QrFrtFatInfDocInfNF_vBCST: TFloatField
      FieldName = 'InfNF_vBCST'
    end
    object QrFrtFatInfDocInfNF_vST: TFloatField
      FieldName = 'InfNF_vST'
    end
    object QrFrtFatInfDocInfNF_vProd: TFloatField
      FieldName = 'InfNF_vProd'
    end
    object QrFrtFatInfDocInfNF_vNF: TFloatField
      FieldName = 'InfNF_vNF'
    end
    object QrFrtFatInfDocInfNF_nCFOP: TIntegerField
      FieldName = 'InfNF_nCFOP'
    end
    object QrFrtFatInfDocInfNF_nPeso: TFloatField
      FieldName = 'InfNF_nPeso'
    end
    object QrFrtFatInfDocInfNF_PIN: TIntegerField
      FieldName = 'InfNF_PIN'
    end
    object QrFrtFatInfDocInfNF_dPrev: TDateField
      FieldName = 'InfNF_dPrev'
    end
    object QrFrtFatInfDocInfOutros_tpDoc: TSmallintField
      FieldName = 'InfOutros_tpDoc'
    end
    object QrFrtFatInfDocInfOutros_descOutros: TWideStringField
      FieldName = 'InfOutros_descOutros'
      Size = 100
    end
    object QrFrtFatInfDocInfOutros_nDoc: TWideStringField
      FieldName = 'InfOutros_nDoc'
    end
    object QrFrtFatInfDocInfOutros_dEmi: TDateField
      FieldName = 'InfOutros_dEmi'
    end
    object QrFrtFatInfDocInfOutros_vDocFisc: TFloatField
      FieldName = 'InfOutros_vDocFisc'
    end
    object QrFrtFatInfDocInfOutros_dPrev: TDateField
      FieldName = 'InfOutros_dPrev'
    end
    object QrFrtFatInfDocLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatInfDocDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatInfDocDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatInfDocUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatInfDocUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatInfDocAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatInfDocAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrFrtFOCAtrDef: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ''
      
        'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ' +
        'ATRITS, '
      'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,'
      'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp'
      'FROM frtfocatrdef def'
      'LEFT JOIN frtfocatrits its ON its.Controle=def.AtrIts '
      'LEFT JOIN frtfocatrcad cad ON cad.Codigo=def.AtrCad '
      'WHERE def.ID_Sorc=1'
      ' '
      'UNION  '
      ' '
      'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS, '
      'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt AtrTxt,'
      'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp '
      'FROM frtfocatrtxt def '
      'LEFT JOIN frtfocatrcad cad ON cad.Codigo=def.AtrCad '
      'WHERE def.ID_Sorc=1'
      ' '
      'ORDER BY NO_CAD, NO_ITS ')
    Left = 204
    Top = 148
    object QrFrtFOCAtrDefID_Item: TIntegerField
      FieldName = 'ID_Item'
      Required = True
    end
    object QrFrtFOCAtrDefID_Sorc: TIntegerField
      FieldName = 'ID_Sorc'
      Required = True
    end
    object QrFrtFOCAtrDefAtrCad: TIntegerField
      FieldName = 'AtrCad'
      Required = True
    end
    object QrFrtFOCAtrDefATRITS: TFloatField
      FieldName = 'ATRITS'
      Required = True
    end
    object QrFrtFOCAtrDefCU_CAD: TIntegerField
      FieldName = 'CU_CAD'
    end
    object QrFrtFOCAtrDefCU_ITS: TLargeintField
      FieldName = 'CU_ITS'
    end
    object QrFrtFOCAtrDefAtrTxt: TWideStringField
      FieldName = 'AtrTxt'
      Size = 255
    end
    object QrFrtFOCAtrDefNO_CAD: TWideStringField
      FieldName = 'NO_CAD'
      Size = 30
    end
    object QrFrtFOCAtrDefNO_ITS: TWideStringField
      FieldName = 'NO_ITS'
      Size = 255
    end
    object QrFrtFOCAtrDefAtrTyp: TSmallintField
      FieldName = 'AtrTyp'
    end
  end
  object QrFrtFatPeri: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ffp.Codigo, ffp.Controle, ffp.ProdutCad, '
      'ffp.qTotProd, ffp.qVolTipo,  '
      'prc.Nome, prc.nONU, prc.XNomeAE,'
      'prc.NClaRisco, prc.GrEmb PERI_grEmb, prc.PontoFulgor'
      'FROM frtfatperi ffp'
      'LEFT JOIN produtcad prc ON prc.Codigo=ffp.ProdutCad'
      'ORDER BY ffp.Controle')
    Left = 204
    Top = 196
    object QrFrtFatPeriCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatPeriControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatPeriProdutCad: TIntegerField
      FieldName = 'ProdutCad'
    end
    object QrFrtFatPeriqTotProd: TWideStringField
      FieldName = 'qTotProd'
    end
    object QrFrtFatPeriqVolTipo: TWideStringField
      FieldName = 'qVolTipo'
      Size = 60
    end
    object QrFrtFatPeriNome: TWideStringField
      FieldName = 'Nome'
      Size = 150
    end
    object QrFrtFatPerinONU: TWideStringField
      FieldName = 'nONU'
      Size = 4
    end
    object QrFrtFatPeriXNomeAE: TWideStringField
      FieldName = 'XNomeAE'
      Size = 150
    end
    object QrFrtFatPeriPERI_grEmb: TWideStringField
      FieldName = 'PERI_grEmb'
      Size = 6
    end
    object QrFrtFatPeriPontoFulgor: TWideStringField
      FieldName = 'PontoFulgor'
      Size = 6
    end
    object QrFrtFatPeriXClaRisco: TWideStringField
      FieldName = 'XClaRisco'
      Size = 40
    end
  end
  object QrFrtFatDup: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Duplicata, Vencimento, Credito'
      'FROM lct0001a')
    Left = 204
    Top = 244
    object QrFrtFatDupDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrFrtFatDupVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrFrtFatDupCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrFrtFatDupControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrFrtFatInfCTeSub: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatinfctesub'
      'WHERE Codigo=:P0')
    Left = 300
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatInfCTeSubCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatInfCTeSubChCTe: TWideStringField
      FieldName = 'ChCTe'
      Size = 44
    end
    object QrFrtFatInfCTeSubContribICMS: TSmallintField
      FieldName = 'ContribICMS'
    end
    object QrFrtFatInfCTeSubTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrFrtFatInfCTeSubrefNFe: TWideStringField
      FieldName = 'refNFe'
      Size = 44
    end
    object QrFrtFatInfCTeSubCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrFrtFatInfCTeSubCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrFrtFatInfCTeSubmod_: TWideStringField
      FieldName = 'mod_'
      Size = 2
    end
    object QrFrtFatInfCTeSubserie: TIntegerField
      FieldName = 'serie'
    end
    object QrFrtFatInfCTeSubsubserie: TIntegerField
      FieldName = 'subserie'
    end
    object QrFrtFatInfCTeSubnro: TIntegerField
      FieldName = 'nro'
    end
    object QrFrtFatInfCTeSubvalor: TFloatField
      FieldName = 'valor'
    end
    object QrFrtFatInfCTeSubdEmi: TDateField
      FieldName = 'dEmi'
    end
    object QrFrtFatInfCTeSubrefCTe: TWideStringField
      FieldName = 'refCTe'
      Size = 44
    end
    object QrFrtFatInfCTeSubrefCTeAnu: TWideStringField
      FieldName = 'refCTeAnu'
      Size = 44
    end
    object QrFrtFatInfCTeSubLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatInfCTeSubDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatInfCTeSubDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatInfCTeSubUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatInfCTeSubUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatInfCTeSubAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatInfCTeSubAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrFrtFatInfCTeAnu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatinfctesub'
      'WHERE Codigo=:P0')
    Left = 300
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatInfCTeAnuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatInfCTeAnuChCTe: TWideStringField
      FieldName = 'ChCTe'
      Size = 44
    end
    object QrFrtFatInfCTeAnudEmi: TDateField
      FieldName = 'dEmi'
    end
  end
  object QrFrtFatInfCTeComp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatinfctesub'
      'WHERE Codigo=:P0')
    Left = 300
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatInfCTeCompCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatInfCTeCompChave: TWideStringField
      FieldName = 'Chave'
      Size = 44
    end
  end
  object QrFrtFatAutXml: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM frtfatautxml')
    Left = 204
    Top = 292
    object QrFrtFatAutXmlCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatAutXmlControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatAutXmlAddForma: TSmallintField
      FieldName = 'AddForma'
    end
    object QrFrtFatAutXmlAddIDCad: TIntegerField
      FieldName = 'AddIDCad'
    end
    object QrFrtFatAutXmlTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrFrtFatAutXmlCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrFrtFatAutXmlCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrFrtFatAutXmlLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatAutXmlDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatAutXmlDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatAutXmlUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatAutXmlUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatAutXmlAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatAutXmlAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrTrnsCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM trnscad'
      'WHERE Codigo=:P0')
    Left = 32
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTrnsCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTrnsCadRNTRC: TWideStringField
      FieldName = 'RNTRC'
      Size = 8
    end
    object QrTrnsCadLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTrnsCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTrnsCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTrnsCadUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTrnsCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTrnsCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTrnsCadAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTrnsCadTpProp: TSmallintField
      FieldName = 'TpProp'
    end
  end
  object QrCTeArq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT caba.IDCtrl IDCtrl_CabA, arq.IDCtrl IDCtrl_Arq, '
      'caba.Id, caba.FatID, caba.FatNum, caba.Empresa'
      'FROM ctecaba caba'
      'LEFT JOIN ctearq arq ON arq.IDCtrl=caba.IDCtrl'
      'WHERE caba.IDCtrl=:P0')
    Left = 856
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCTeArqIDCtrl_CabA: TIntegerField
      FieldName = 'IDCtrl_CabA'
    end
    object QrCTeArqIDCtrl_Arq: TIntegerField
      FieldName = 'IDCtrl_Arq'
      Required = True
    end
    object QrCTeArqId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrCTeArqFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeArqFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeArqEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrCTECab2Toma04: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctecab2toma04'
      'WHERE FatID=0'
      'AND FatNum=0'
      'AND Empresa=0')
    Left = 416
    Top = 4
    object QrCTECab2Toma04FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTECab2Toma04FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTECab2Toma04Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTECab2Toma04Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTECab2Toma04enderToma_xLgr: TWideStringField
      FieldName = 'enderToma_xLgr'
      Size = 255
    end
    object QrCTECab2Toma04enderToma_nro: TWideStringField
      FieldName = 'enderToma_nro'
      Size = 60
    end
    object QrCTECab2Toma04enderToma_xCpl: TWideStringField
      FieldName = 'enderToma_xCpl'
      Size = 60
    end
    object QrCTECab2Toma04enderToma_xBairro: TWideStringField
      FieldName = 'enderToma_xBairro'
      Size = 60
    end
    object QrCTECab2Toma04enderToma_cMun: TIntegerField
      FieldName = 'enderToma_cMun'
    end
    object QrCTECab2Toma04enderToma_xMun: TWideStringField
      FieldName = 'enderToma_xMun'
      Size = 60
    end
    object QrCTECab2Toma04enderToma_CEP: TIntegerField
      FieldName = 'enderToma_CEP'
    end
    object QrCTECab2Toma04enderToma_UF: TWideStringField
      FieldName = 'enderToma_UF'
      Size = 2
    end
    object QrCTECab2Toma04enderToma_cPais: TIntegerField
      FieldName = 'enderToma_cPais'
    end
    object QrCTECab2Toma04enderToma_xPais: TWideStringField
      FieldName = 'enderToma_xPais'
      Size = 60
    end
    object QrCTECab2Toma04enderToma_email: TWideStringField
      FieldName = 'enderToma_email'
      Size = 60
    end
    object QrCTECab2Toma04Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTECab2Toma04DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTECab2Toma04DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTECab2Toma04UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTECab2Toma04UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTECab2Toma04AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTECab2Toma04Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeIt2ObsCont: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cteit2obscont'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 416
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeIt2ObsContFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeIt2ObsContFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeIt2ObsContEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeIt2ObsContControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeIt2ObsContxCampo: TWideStringField
      FieldName = 'xCampo'
    end
    object QrCTeIt2ObsContxTexto: TWideStringField
      FieldName = 'xTexto'
      Size = 60
    end
    object QrCTeIt2ObsContLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeIt2ObsContDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeIt2ObsContDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeIt2ObsContUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeIt2ObsContUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeIt2ObsContAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeIt2ObsContAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeIt2ObsFisco: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cteit2obscont'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 416
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeIt2ObsFiscoFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeIt2ObsFiscoFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeIt2ObsFiscoEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeIt2ObsFiscoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeIt2ObsFiscoxCampo: TWideStringField
      FieldName = 'xCampo'
    end
    object QrCTeIt2ObsFiscoxTexto: TWideStringField
      FieldName = 'xTexto'
      Size = 60
    end
    object QrCTeIt2ObsFiscoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeIt2ObsFiscoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeIt2ObsFiscoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeIt2ObsFiscoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeIt2ObsFiscoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeIt2ObsFiscoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeIt2ObsFiscoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeCab2LocColeta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctecab2loccoleta'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 416
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeCab2LocColetaFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeCab2LocColetaFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeCab2LocColetaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeCab2LocColetaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeCab2LocColetaCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrCTeCab2LocColetaCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrCTeCab2LocColetaxNome: TWideStringField
      FieldName = 'xNome'
      Size = 60
    end
    object QrCTeCab2LocColetaxLgr: TWideStringField
      FieldName = 'xLgr'
      Size = 255
    end
    object QrCTeCab2LocColetanro: TWideStringField
      FieldName = 'nro'
      Size = 60
    end
    object QrCTeCab2LocColetaxCpl: TWideStringField
      FieldName = 'xCpl'
      Size = 60
    end
    object QrCTeCab2LocColetaxBairro: TWideStringField
      FieldName = 'xBairro'
      Size = 60
    end
    object QrCTeCab2LocColetacMun: TIntegerField
      FieldName = 'cMun'
    end
    object QrCTeCab2LocColetaxMun: TWideStringField
      FieldName = 'xMun'
      Size = 60
    end
    object QrCTeCab2LocColetaUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrCTeCab2LocColetaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeCab2LocColetaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeCab2LocColetaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeCab2LocColetaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeCab2LocColetaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeCab2LocColetaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeCab2LocColetaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeCab1Exped: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctecab1exped'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 412
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeCab1ExpedFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeCab1ExpedFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeCab1ExpedEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeCab1ExpedControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeCab1ExpedCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrCTeCab1ExpedCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrCTeCab1ExpedIE: TWideStringField
      FieldName = 'IE'
      Size = 14
    end
    object QrCTeCab1ExpedxNome: TWideStringField
      FieldName = 'xNome'
      Size = 60
    end
    object QrCTeCab1Expedfone: TWideStringField
      FieldName = 'fone'
      Size = 14
    end
    object QrCTeCab1ExpedxLgr: TWideStringField
      FieldName = 'xLgr'
      Size = 255
    end
    object QrCTeCab1Expednro: TWideStringField
      FieldName = 'nro'
      Size = 60
    end
    object QrCTeCab1ExpedxCpl: TWideStringField
      FieldName = 'xCpl'
      Size = 60
    end
    object QrCTeCab1ExpedxBairro: TWideStringField
      FieldName = 'xBairro'
      Size = 60
    end
    object QrCTeCab1ExpedcMun: TIntegerField
      FieldName = 'cMun'
    end
    object QrCTeCab1ExpedxMun: TWideStringField
      FieldName = 'xMun'
      Size = 60
    end
    object QrCTeCab1ExpedCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrCTeCab1ExpedUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrCTeCab1ExpedcPais: TIntegerField
      FieldName = 'cPais'
    end
    object QrCTeCab1ExpedxPais: TWideStringField
      FieldName = 'xPais'
      Size = 60
    end
    object QrCTeCab1Expedemail: TWideStringField
      FieldName = 'email'
      Size = 60
    end
    object QrCTeCab1ExpedLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeCab1ExpedDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeCab1ExpedDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeCab1ExpedUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeCab1ExpedUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeCab1ExpedAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeCab1ExpedAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeCab1Receb: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctecab1receb'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 412
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeCab1RecebFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeCab1RecebFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeCab1RecebEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeCab1RecebControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeCab1RecebCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrCTeCab1RecebCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrCTeCab1RecebIE: TWideStringField
      FieldName = 'IE'
      Size = 14
    end
    object QrCTeCab1RecebxNome: TWideStringField
      FieldName = 'xNome'
      Size = 60
    end
    object QrCTeCab1Recebfone: TWideStringField
      FieldName = 'fone'
      Size = 14
    end
    object QrCTeCab1RecebxLgr: TWideStringField
      FieldName = 'xLgr'
      Size = 255
    end
    object QrCTeCab1Recebnro: TWideStringField
      FieldName = 'nro'
      Size = 60
    end
    object QrCTeCab1RecebxCpl: TWideStringField
      FieldName = 'xCpl'
      Size = 60
    end
    object QrCTeCab1RecebxBairro: TWideStringField
      FieldName = 'xBairro'
      Size = 60
    end
    object QrCTeCab1RecebcMun: TIntegerField
      FieldName = 'cMun'
    end
    object QrCTeCab1RecebxMun: TWideStringField
      FieldName = 'xMun'
      Size = 60
    end
    object QrCTeCab1RecebCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrCTeCab1RecebUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrCTeCab1RecebcPais: TIntegerField
      FieldName = 'cPais'
    end
    object QrCTeCab1RecebxPais: TWideStringField
      FieldName = 'xPais'
      Size = 60
    end
    object QrCTeCab1Recebemail: TWideStringField
      FieldName = 'email'
      Size = 60
    end
    object QrCTeCab1RecebLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeCab1RecebDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeCab1RecebDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeCab1RecebUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeCab1RecebUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeCab1RecebAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeCab1RecebAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeCab2LocEnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctecab2locent'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 412
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeCab2LocEntFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeCab2LocEntFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeCab2LocEntEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeCab2LocEntControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeCab2LocEntCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrCTeCab2LocEntCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrCTeCab2LocEntxNome: TWideStringField
      FieldName = 'xNome'
      Size = 60
    end
    object QrCTeCab2LocEntxLgr: TWideStringField
      FieldName = 'xLgr'
      Size = 255
    end
    object QrCTeCab2LocEntnro: TWideStringField
      FieldName = 'nro'
      Size = 60
    end
    object QrCTeCab2LocEntxCpl: TWideStringField
      FieldName = 'xCpl'
      Size = 60
    end
    object QrCTeCab2LocEntxBairro: TWideStringField
      FieldName = 'xBairro'
      Size = 60
    end
    object QrCTeCab2LocEntcMun: TIntegerField
      FieldName = 'cMun'
    end
    object QrCTeCab2LocEntxMun: TWideStringField
      FieldName = 'xMun'
      Size = 60
    end
    object QrCTeCab2LocEntUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrCTeCab2LocEntLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeCab2LocEntDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeCab2LocEntDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeCab2LocEntUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeCab2LocEntUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeCab2LocEntAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeCab2LocEntAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeIt2Comp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cteit2comp'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 412
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeIt2CompFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeIt2CompFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeIt2CompEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeIt2CompControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeIt2CompxNome: TWideStringField
      FieldName = 'xNome'
      Size = 15
    end
    object QrCTeIt2CompvComp: TFloatField
      FieldName = 'vComp'
    end
    object QrCTeIt2CompLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeIt2CompDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeIt2CompDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeIt2CompUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeIt2CompUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeIt2CompAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeIt2CompAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeIt3InfQ: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cteit3infq'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 412
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeIt3InfQFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeIt3InfQFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeIt3InfQEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeIt3InfQControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeIt3InfQcUnid: TSmallintField
      FieldName = 'cUnid'
    end
    object QrCTeIt3InfQtpMed: TWideStringField
      FieldName = 'tpMed'
    end
    object QrCTeIt3InfQqCarga: TFloatField
      FieldName = 'qCarga'
    end
    object QrCTeIt3InfQLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeIt3InfQDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeIt3InfQDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeIt3InfQUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeIt3InfQUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeIt3InfQAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeIt3InfQAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeIt2InfDoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cteit2infdoc'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 412
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeIt2InfDocFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeIt2InfDocFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeIt2InfDocEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeIt2InfDocControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeIt2InfDocMyTpDocInf: TSmallintField
      FieldName = 'MyTpDocInf'
    end
    object QrCTeIt2InfDocInfNFe_chave: TWideStringField
      FieldName = 'InfNFe_chave'
      Size = 44
    end
    object QrCTeIt2InfDocInfNFe_PIN: TIntegerField
      FieldName = 'InfNFe_PIN'
    end
    object QrCTeIt2InfDocInfNFe_dPrev: TDateField
      FieldName = 'InfNFe_dPrev'
    end
    object QrCTeIt2InfDocInfNF_nRoma: TWideStringField
      FieldName = 'InfNF_nRoma'
    end
    object QrCTeIt2InfDocInfNF_nPed: TWideStringField
      FieldName = 'InfNF_nPed'
    end
    object QrCTeIt2InfDocInfNF_mod: TSmallintField
      FieldName = 'InfNF_mod'
    end
    object QrCTeIt2InfDocInfNF_serie: TWideStringField
      FieldName = 'InfNF_serie'
      Size = 3
    end
    object QrCTeIt2InfDocInfNF_nDoc: TWideStringField
      FieldName = 'InfNF_nDoc'
    end
    object QrCTeIt2InfDocInfNF_dEmi: TDateField
      FieldName = 'InfNF_dEmi'
    end
    object QrCTeIt2InfDocInfNF_vBC: TFloatField
      FieldName = 'InfNF_vBC'
    end
    object QrCTeIt2InfDocInfNF_vICMS: TFloatField
      FieldName = 'InfNF_vICMS'
    end
    object QrCTeIt2InfDocInfNF_vBCST: TFloatField
      FieldName = 'InfNF_vBCST'
    end
    object QrCTeIt2InfDocInfNF_vST: TFloatField
      FieldName = 'InfNF_vST'
    end
    object QrCTeIt2InfDocInfNF_vProd: TFloatField
      FieldName = 'InfNF_vProd'
    end
    object QrCTeIt2InfDocInfNF_vNF: TFloatField
      FieldName = 'InfNF_vNF'
    end
    object QrCTeIt2InfDocInfNF_nCFOP: TIntegerField
      FieldName = 'InfNF_nCFOP'
    end
    object QrCTeIt2InfDocInfNF_nPeso: TFloatField
      FieldName = 'InfNF_nPeso'
    end
    object QrCTeIt2InfDocInfNF_PIN: TIntegerField
      FieldName = 'InfNF_PIN'
    end
    object QrCTeIt2InfDocInfNF_dPrev: TDateField
      FieldName = 'InfNF_dPrev'
    end
    object QrCTeIt2InfDocInfOutros_tpDoc: TSmallintField
      FieldName = 'InfOutros_tpDoc'
    end
    object QrCTeIt2InfDocInfOutros_descOutros: TWideStringField
      FieldName = 'InfOutros_descOutros'
      Size = 100
    end
    object QrCTeIt2InfDocInfOutros_nDoc: TWideStringField
      FieldName = 'InfOutros_nDoc'
    end
    object QrCTeIt2InfDocInfOutros_dEmi: TDateField
      FieldName = 'InfOutros_dEmi'
    end
    object QrCTeIt2InfDocInfOutros_vDocFisc: TFloatField
      FieldName = 'InfOutros_vDocFisc'
    end
    object QrCTeIt2InfDocInfOutros_dPrev: TDateField
      FieldName = 'InfOutros_dPrev'
    end
    object QrCTeIt2InfDocLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeIt2InfDocDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeIt2InfDocDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeIt2InfDocUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeIt2InfDocUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeIt2InfDocAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeIt2InfDocAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeMoRodoCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctemorodocab'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 536
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeMoRodoCabFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeMoRodoCabFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeMoRodoCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeMoRodoCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeMoRodoCabdPrev: TDateField
      FieldName = 'dPrev'
    end
    object QrCTeMoRodoCablota: TSmallintField
      FieldName = 'lota'
    end
    object QrCTeMoRodoCabCIOT: TLargeintField
      FieldName = 'CIOT'
    end
    object QrCTeMoRodoCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeMoRodoCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeMoRodoCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeMoRodoCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeMoRodoCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeMoRodoCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeMoRodoCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCTeMoRodoCabRNTRC: TWideStringField
      FieldName = 'RNTRC'
      Size = 8
    end
  end
  object QrCteIt2Peri: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cteit2peri'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 412
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCteIt2PeriFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCteIt2PeriFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCteIt2PeriEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCteIt2PeriControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCteIt2PerinONU: TWideStringField
      FieldName = 'nONU'
      Size = 4
    end
    object QrCteIt2PerixNomeAE: TWideStringField
      FieldName = 'xNomeAE'
      Size = 150
    end
    object QrCteIt2PerixClaRisco: TWideStringField
      FieldName = 'xClaRisco'
      Size = 40
    end
    object QrCteIt2PerigrEmb: TWideStringField
      FieldName = 'grEmb'
      Size = 6
    end
    object QrCteIt2PeriqTotProd: TWideStringField
      FieldName = 'qTotProd'
    end
    object QrCteIt2PeriqVolTipo: TWideStringField
      FieldName = 'qVolTipo'
      Size = 60
    end
    object QrCteIt2PeripontoFulgor: TWideStringField
      FieldName = 'pontoFulgor'
      Size = 6
    end
    object QrCteIt2PeriLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCteIt2PeriDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCteIt2PeriDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCteIt2PeriUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCteIt2PeriUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCteIt2PeriAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCteIt2PeriAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeIt3Dup: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cteit3dup'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 412
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeIt3DupFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeIt3DupFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeIt3DupEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeIt3DupControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeIt3DupnDup: TWideStringField
      FieldName = 'nDup'
      Size = 60
    end
    object QrCTeIt3DupdVenc: TDateField
      FieldName = 'dVenc'
    end
    object QrCTeIt3DupvDup: TFloatField
      FieldName = 'vDup'
    end
    object QrCTeIt3DupLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeIt3DupDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeIt3DupDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeIt3DupUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeIt3DupUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeIt3DupAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeIt3DupAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeIt1AutXml: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cteit1autxml'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 412
    Top = 580
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeIt1AutXmlFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeIt1AutXmlFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeIt1AutXmlEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeIt1AutXmlControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeIt1AutXmlAddForma: TSmallintField
      FieldName = 'AddForma'
    end
    object QrCTeIt1AutXmlAddIDCad: TIntegerField
      FieldName = 'AddIDCad'
    end
    object QrCTeIt1AutXmlTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCTeIt1AutXmlCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrCTeIt1AutXmlCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrCTeIt1AutXmlLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeIt1AutXmlDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeIt1AutXmlDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeIt1AutXmlUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeIt1AutXmlUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeIt1AutXmlAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeIt1AutXmlAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeLEnC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Id, FatID, FatNum'
      'FROM ctecaba'
      'WHERE LoteEnv=:P0'
      '')
    Left = 28
    Top = 332
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCTeLEnCId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrCTeLEnCFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeLEnCFatNum: TIntegerField
      FieldName = 'FatNum'
    end
  end
  object QrAtoArq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT caba.IDCtrl'
      'FROM ctecaba caba'
      'LEFT JOIN ctearq arq ON arq.IDCtrl=caba.IDCtrl'
      'WHERE arq.IDCtrl IS NULL'
      'AND Id <> ""'
      'AND ('
      '  emit_CNPJ=:P0'
      '  OR'
      '  emit_CPF=:P1'
      ')')
    Left = 856
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAtoArqIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
  end
  object QrCTeAut: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT caba.IDCtrl, caba.LoteEnv, caba.Id'
      'FROM ctecaba caba'
      'LEFT JOIN ctearq arq ON arq.IDCtrl=caba.IDCtrl'
      'WHERE arq.IDCtrl IS NOT NULL'
      'AND caba.infProt_cStat=100'
      'AND arq.XML_Aut IS NULL'
      'AND ('
      '  emit_CNPJ=:P0'
      '  OR'
      '  emit_CPF=:P1'
      ')')
    Left = 856
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCTeAutIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCTeAutLoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrCTeAutId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
  end
  object QrCTeCan: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT caba.IDCtrl, caba.Id'
      'FROM ctecaba caba'
      'LEFT JOIN ctearq arq ON arq.IDCtrl=caba.IDCtrl'
      'WHERE arq.IDCtrl IS NOT NULL'
      'AND caba.infCanc_cStat=101'
      'AND ('
      '  emit_CNPJ=:P0'
      '  OR'
      '  emit_CPF=:P1'
      ')')
    Left = 856
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCTeCanIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCTeCanId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
  end
  object QrCTeInu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cteinut'
      'WHERE XML_Inu IS NULL'
      'AND cStat=102')
    Left = 856
    Top = 192
    object QrCTeInuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCTeInuCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrCTeInuNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCTeInuEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeInuDataC: TDateField
      FieldName = 'DataC'
    end
    object QrCTeInuversao: TFloatField
      FieldName = 'versao'
    end
    object QrCTeInuId: TWideStringField
      FieldName = 'Id'
      Size = 30
    end
    object QrCTeInutpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrCTeInucUF: TSmallintField
      FieldName = 'cUF'
    end
    object QrCTeInuano: TSmallintField
      FieldName = 'ano'
    end
    object QrCTeInuCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrCTeInumodelo: TSmallintField
      FieldName = 'modelo'
    end
    object QrCTeInuSerie: TIntegerField
      FieldName = 'Serie'
    end
    object QrCTeInunCTIni: TIntegerField
      FieldName = 'nCTIni'
    end
    object QrCTeInunCTFim: TIntegerField
      FieldName = 'nCTFim'
    end
    object QrCTeInuJustif: TIntegerField
      FieldName = 'Justif'
    end
    object QrCTeInuxJust: TWideStringField
      FieldName = 'xJust'
      Size = 255
    end
    object QrCTeInucStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrCTeInuxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrCTeInudhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrCTeInunProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrCTeInuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeInuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeInuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeInuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeInuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeInuAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeInuAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCTeInuXML_Inu: TWideMemoField
      FieldName = 'XML_Inu'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrFrtFatSeg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'ffs.* '
      'FROM frtfatseg ffs'
      'WHERE ffs.Codigo=1')
    Left = 204
    Top = 337
    object QrFrtFatSegCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatSegControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatSegrespSeg: TSmallintField
      FieldName = 'respSeg'
    end
    object QrFrtFatSegxSeg: TWideStringField
      FieldName = 'xSeg'
      Size = 30
    end
    object QrFrtFatSegnApol: TWideStringField
      FieldName = 'nApol'
    end
    object QrFrtFatSegnAver: TWideStringField
      FieldName = 'nAver'
    end
    object QrFrtFatSegvCarga: TFloatField
      FieldName = 'vCarga'
    end
    object QrFrtFatSegLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatSegDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatSegDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatSegUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatSegUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatSegAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatSegAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCteIt2Seg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'ffs.* '
      'FROM frtfatseg ffs'
      'WHERE ffs.Codigo=1')
    Left = 412
    Top = 625
    object QrCteIt2SegFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCteIt2SegFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCteIt2SegEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCteIt2SegControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCteIt2SegrespSeg: TSmallintField
      FieldName = 'respSeg'
    end
    object QrCteIt2SegxSeg: TWideStringField
      FieldName = 'xSeg'
      Size = 30
    end
    object QrCteIt2SegnApol: TWideStringField
      FieldName = 'nApol'
    end
    object QrCteIt2SegnAver: TWideStringField
      FieldName = 'nAver'
    end
    object QrCteIt2SegvCarga: TFloatField
      FieldName = 'vCarga'
    end
  end
  object QrCTeMoRodoVPed: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctemorodovped'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 540
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeMoRodoVPedFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeMoRodoVPedFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeMoRodoVPedEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeMoRodoVPedControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeMoRodoVPedConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCTeMoRodoVPedCNPJForn: TWideStringField
      FieldName = 'CNPJForn'
      Size = 14
    end
    object QrCTeMoRodoVPednCompra: TWideStringField
      FieldName = 'nCompra'
    end
    object QrCTeMoRodoVPedCNPJPg: TWideStringField
      FieldName = 'CNPJPg'
      Size = 14
    end
    object QrCTeMoRodoVPedvValePed: TFloatField
      FieldName = 'vValePed'
    end
    object QrCTeMoRodoVPedLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeMoRodoVPedDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeMoRodoVPedDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeMoRodoVPedUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeMoRodoVPedUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeMoRodoVPedAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeMoRodoVPedAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeMoRodoVeic: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctemorodoveic'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 540
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeMoRodoVeicFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeMoRodoVeicFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeMoRodoVeicEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeMoRodoVeicControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeMoRodoVeicConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCTeMoRodoVeiccInt: TIntegerField
      FieldName = 'cInt'
    end
    object QrCTeMoRodoVeicRENAVAM: TWideStringField
      FieldName = 'RENAVAM'
      Size = 11
    end
    object QrCTeMoRodoVeicplaca: TWideStringField
      FieldName = 'placa'
      Size = 7
    end
    object QrCTeMoRodoVeictara: TIntegerField
      FieldName = 'tara'
    end
    object QrCTeMoRodoVeiccapKG: TIntegerField
      FieldName = 'capKG'
    end
    object QrCTeMoRodoVeiccapM3: TIntegerField
      FieldName = 'capM3'
    end
    object QrCTeMoRodoVeictpProp: TWideStringField
      FieldName = 'tpProp'
      Size = 1
    end
    object QrCTeMoRodoVeictpVeic: TSmallintField
      FieldName = 'tpVeic'
    end
    object QrCTeMoRodoVeictpRod: TSmallintField
      FieldName = 'tpRod'
    end
    object QrCTeMoRodoVeictpCar: TSmallintField
      FieldName = 'tpCar'
    end
    object QrCTeMoRodoVeicUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrCTeMoRodoVeicLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeMoRodoVeicDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeMoRodoVeicDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeMoRodoVeicUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeMoRodoVeicUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeMoRodoVeicAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeMoRodoVeicAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeMoRodoLacr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctemorodolacr'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 540
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeMoRodoLacrFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeMoRodoLacrFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeMoRodoLacrEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeMoRodoLacrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeMoRodoLacrConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCTeMoRodoLacrnLacre: TWideStringField
      FieldName = 'nLacre'
    end
    object QrCTeMoRodoLacrLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeMoRodoLacrDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeMoRodoLacrDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeMoRodoLacrUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeMoRodoLacrUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeMoRodoLacrAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeMoRodoLacrAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeMoRodoMoto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctemorodomoto'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 540
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeMoRodoMotoFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeMoRodoMotoFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeMoRodoMotoEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeMoRodoMotoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeMoRodoMotoConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCTeMoRodoMotoxNome: TWideStringField
      FieldName = 'xNome'
    end
    object QrCTeMoRodoMotoCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrCTeMoRodoMotoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeMoRodoMotoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeMoRodoMotoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeMoRodoMotoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeMoRodoMotoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeMoRodoMotoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeMoRodoMotoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrCTeMoRodoProp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctemorodoprop'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 544
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeMoRodoPropFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeMoRodoPropFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeMoRodoPropEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeMoRodoPropControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeMoRodoPropConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCTeMoRodoPropCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrCTeMoRodoPropCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrCTeMoRodoPropxNome: TWideStringField
      FieldName = 'xNome'
      Size = 60
    end
    object QrCTeMoRodoPropIE: TWideStringField
      FieldName = 'IE'
      Size = 14
    end
    object QrCTeMoRodoPropUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrCTeMoRodoProptpProp: TSmallintField
      FieldName = 'tpProp'
    end
    object QrCTeMoRodoPropLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeMoRodoPropDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeMoRodoPropDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeMoRodoPropUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeMoRodoPropUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeMoRodoPropAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeMoRodoPropAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCTeMoRodoPropRNTRC: TWideStringField
      FieldName = 'RNTRC'
      Size = 8
    end
  end
  object QrCTeMoRodoOcc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctemorodoocc'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 544
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCTeMoRodoOccFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrCTeMoRodoOccFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrCTeMoRodoOccEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCTeMoRodoOccControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCTeMoRodoOccConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCTeMoRodoOccserie: TWideStringField
      FieldName = 'serie'
      Size = 3
    end
    object QrCTeMoRodoOccnOcc: TIntegerField
      FieldName = 'nOcc'
    end
    object QrCTeMoRodoOccdEmi: TDateField
      FieldName = 'dEmi'
    end
    object QrCTeMoRodoOccemiOcc_CNPJ: TWideStringField
      FieldName = 'emiOcc_CNPJ'
      Size = 14
    end
    object QrCTeMoRodoOccemiOcc_cInt: TIntegerField
      FieldName = 'emiOcc_cInt'
    end
    object QrCTeMoRodoOccemiOcc_IE: TWideStringField
      FieldName = 'emiOcc_IE'
      Size = 14
    end
    object QrCTeMoRodoOccemiOcc_UF: TWideStringField
      FieldName = 'emiOcc_UF'
      Size = 2
    end
    object QrCTeMoRodoOccemiOcc_fone: TWideStringField
      FieldName = 'emiOcc_fone'
      Size = 14
    end
    object QrCTeMoRodoOccLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCTeMoRodoOccDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCTeMoRodoOccDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCTeMoRodoOccUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCTeMoRodoOccUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCTeMoRodoOccAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCTeMoRodoOccAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrFrtFatMoRodoVPed: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatmorodovped'
      'WHERE Codigo=:P0')
    Left = 204
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatMoRodoVPedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatMoRodoVPedControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatMoRodoVPedCNPJForn: TWideStringField
      FieldName = 'CNPJForn'
      Size = 14
    end
    object QrFrtFatMoRodoVPednCompra: TWideStringField
      FieldName = 'nCompra'
    end
    object QrFrtFatMoRodoVPedCNPJPg: TWideStringField
      FieldName = 'CNPJPg'
      Size = 14
    end
    object QrFrtFatMoRodoVPedvValePed: TFloatField
      FieldName = 'vValePed'
    end
    object QrFrtFatMoRodoVPedLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatMoRodoVPedDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatMoRodoVPedDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatMoRodoVPedUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatMoRodoVPedUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatMoRodoVPedAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatMoRodoVPedAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrFrtFatMoRodoVeic: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatmorodoveic'
      'WHERE Codigo=:P0')
    Left = 204
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatMoRodoVeicCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatMoRodoVeicControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatMoRodoVeiccInt: TIntegerField
      FieldName = 'cInt'
    end
    object QrFrtFatMoRodoVeicLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatMoRodoVeicDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatMoRodoVeicDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatMoRodoVeicUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatMoRodoVeicUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatMoRodoVeicAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatMoRodoVeicAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrFrtFatMoRodoLacr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatmorodolacr'
      'WHERE Codigo=:P0')
    Left = 204
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatMoRodoLacrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatMoRodoLacrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatMoRodoLacrnLacre: TWideStringField
      FieldName = 'nLacre'
    end
    object QrFrtFatMoRodoLacrLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatMoRodoLacrDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatMoRodoLacrDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatMoRodoLacrUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatMoRodoLacrUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatMoRodoLacrAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatMoRodoLacrAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrFrtFatMoRodoMoto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatmorodomoto'
      'WHERE Codigo=:P0')
    Left = 204
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatMoRodoMotoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatMoRodoMotoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatMoRodoMotoxNome: TWideStringField
      FieldName = 'xNome'
    end
    object QrFrtFatMoRodoMotoCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrFrtFatMoRodoMotoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatMoRodoMotoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatMoRodoMotoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatMoRodoMotoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatMoRodoMotoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatMoRodoMotoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatMoRodoMotoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrTrnsIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM trnsits'
      'WHERE Codigo=:P0')
    Left = 32
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTrnsItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTrnsItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTrnsItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrTrnsItsRENAVAM: TWideStringField
      FieldName = 'RENAVAM'
      Size = 11
    end
    object QrTrnsItsPlaca: TWideStringField
      FieldName = 'Placa'
      Size = 11
    end
    object QrTrnsItsTaraKg: TIntegerField
      FieldName = 'TaraKg'
    end
    object QrTrnsItsCapKg: TIntegerField
      FieldName = 'CapKg'
    end
    object QrTrnsItsTpVeic: TSmallintField
      FieldName = 'TpVeic'
    end
    object QrTrnsItsTpRod: TSmallintField
      FieldName = 'TpRod'
    end
    object QrTrnsItsTpCar: TSmallintField
      FieldName = 'TpCar'
    end
    object QrTrnsItscapM3: TIntegerField
      FieldName = 'capM3'
    end
    object QrTrnsItsUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrTrnsItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTrnsItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTrnsItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTrnsItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTrnsItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTrnsItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTrnsItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrTerceiro: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tcb.RNTRC,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'IF(ent.Tipo=0, "", ent.CPF) CPF,'
      'IF(ent.Tipo=0, ent.CNPJ, "") CNPJ,'
      'IF(ent.Tipo=0, ent.IE, "") IE,'
      'ufs.Nome UF'
      ''
      'FROM entidades ent'
      
        'LEFT JOIN ufs ufs ON ufs.Codigo=(IF(ent.Tipo=0, ent.EUF, ent.PUF' +
        '))'
      'LEFT JOIN trnscad tcb ON tcb.Codigo=ent.Codigo'
      'WHERE ent.Codigo=:P0')
    Left = 32
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerceiroTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTerceiroRNTRC: TWideStringField
      FieldName = 'RNTRC'
      Size = 8
    end
    object QrTerceiroNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrTerceiroCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTerceiroCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTerceiroIE: TWideStringField
      FieldName = 'IE'
    end
    object QrTerceiroUF: TWideStringField
      FieldName = 'UF'
      Required = True
      Size = 2
    end
  end
  object QrFrtFatComp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtfatcomp'
      'WHERE Codigo=:P0')
    Left = 204
    Top = 577
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtFatCompCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtFatCompControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtFatCompCodComp: TIntegerField
      FieldName = 'CodComp'
    end
    object QrFrtFatCompxNome: TWideStringField
      FieldName = 'xNome'
      Size = 15
    end
    object QrFrtFatCompvComp: TFloatField
      FieldName = 'vComp'
    end
    object QrFrtFatCompLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtFatCompDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtFatCompDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtFatCompUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtFatCompUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtFatCompAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtFatCompAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrEveTp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nSeqEvento '
      'FROM cteevercab'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND tpEvento=:P3'
      'ORDER BY nSeqEvento DESC'
      ''
      '')
    Left = 856
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrEveTpnSeqEvento: TSmallintField
      FieldName = 'nSeqEvento'
    end
  end
  object QrCTeInut: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM cteinut'
      'WHERE Empresa=:P0'
      'AND cUF=:P1'
      'AND ano=:P2'
      'AND CNPJ=:P3'
      'AND modelo=:P4'
      'AND Serie=:P5'
      'AND nCTIni=:P6'
      'AND nCTFim=:P7')
    Left = 644
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end>
    object QrCTeInutCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrEveIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM cteevercab'
      'WHERE EventoLote=:P0'
      'AND tpEvento=:P1'
      'AND nSeqEvento=:P2')
    Left = 856
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEveItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrEveSub: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SubCtrl'
      'FROM cteeverret'
      'WHERE Controle=:P0'
      'AND ret_Id=:P1')
    Left = 856
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEveSubSubCtrl: TIntegerField
      FieldName = 'SubCtrl'
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 528
    Top = 368
  end
  object QrLoc2: TmySQLQuery
    Database = Dmod.MyDB
    Left = 557
    Top = 368
  end
  object QrArq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctearq'
      'WHERE IDCtrl=:P0'
      ''
      '')
    Left = 856
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrArqFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfearq.FatID'
    end
    object QrArqFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfearq.FatNum'
    end
    object QrArqEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfearq.Empresa'
    end
    object QrArqIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'nfearq.IDCtrl'
    end
    object QrArqXML_CTe: TWideMemoField
      FieldName = 'XML_CTe'
      Origin = 'nfearq.XML_CTe'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqXML_Aut: TWideMemoField
      FieldName = 'XML_Aut'
      Origin = 'nfearq.XML_Aut'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqXML_Can: TWideMemoField
      FieldName = 'XML_Can'
      Origin = 'nfearq.XML_Can'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfearq.Lk'
    end
    object QrArqDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfearq.DataCad'
    end
    object QrArqDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfearq.DataAlt'
    end
    object QrArqUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfearq.UserCad'
    end
    object QrArqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfearq.AlterWeb'
    end
    object QrArqUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfearq.UserAlt'
    end
    object QrArqAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfearq.Ativo'
    end
  end
  object QrLocEve: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(*) ITENS'
      'FROM nfeevercab'
      'WHERE chNFe="41130310783968000195550010000001531769037263"'
      'AND tpEvento=110110'
      'AND ret_cStat in (135,136)')
    Left = 856
    Top = 436
    object QrLocEveITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
end
