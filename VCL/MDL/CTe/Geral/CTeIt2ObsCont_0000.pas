unit CTeIt2ObsCont_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmCTeIt2ObsCont_0000 = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdxCampo: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    EdxTexto: TdmkEdit;
    Label3: TLabel;
    EdFatID: TdmkEdit;
    EdFatNum: TdmkEdit;
    EdEmpresa: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCTeIt2ObsCont_0000(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmCTeIt2ObsCont_0000: TFmCTeIt2ObsCont_0000;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmCTeIt2ObsCont_0000.BtOKClick(Sender: TObject);
var
  xCampo, xTexto: String;
  FatID, FatNum, Empresa, Controle: Integer;
  SQLType: TSQLType;
begin
  xCampo := EdxCampo.Text;
  if MyObjects.FIC(Trim(xCampo) = '', EdxCampo, 'Informe o nome do campo!') then
    Exit;
  if MyObjects.FIC(Trim(xCampo) = '', EdxCampo, 'Informe o valor do campo!') then
    Exit;
  SQLType        := ImgTipo.SQLType;
  FatID          := EdFatID.ValueVariant;
  FatNum         := EdFatNum.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  Controle       := EdControle.ValueVariant;
  xCampo         := EdxCampo.ValueVariant;
  xTexto         := EdxTexto.ValueVariant;
  //
  Controle := UMyMod.BPGS1I32('cteit2obscont', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'cteit2obscont', False, [
  'FatID', 'FatNum', 'Empresa',
  'xCampo', 'xTexto'], [
  'Controle'], [
  FatID, FatNum, Empresa,
  xCampo, xTexto], [
  Controle], True) then
  begin
    ReopenCTeIt2ObsCont_0000(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      EdxCampo.ValueVariant      := '';
      EdxTexto.ValueVariant      := '';
      EdxCampo.SetFocus;
    end else Close;
  end;
end;

procedure TFmCTeIt2ObsCont_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCTeIt2ObsCont_0000.FormActivate(Sender: TObject);
begin
(*
  DBEdCodigo.DataSource := FDsCab;
  DBEdCodUso.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
*)
  MyObjects.CorIniComponente();
end;

procedure TFmCTeIt2ObsCont_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCTeIt2ObsCont_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCTeIt2ObsCont_0000.ReopenCTeIt2ObsCont_0000(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
