unit CTeInut_0000;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral, UnDmkProcFunc, UnGOTOy, UnInternalConsts,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,
  dmkGeral, dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkDBLookupComboBox,
  dmkEditCB, dmkValUsu, Grids, DBGrids, Menus, ComCtrls, dmkEditDateTimePicker,
  OleCtrls, SHDocVw, dmkImage, UnDmkEnums;

type
  TFmCTeInut_0000 = class(TForm)
    PainelDados: TPanel;
    DsCTeInut: TDataSource;
    QrCTeInut: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrCTeInutCodigo: TIntegerField;
    QrCTeInutNome: TWideStringField;
    QrCTeInutEmpresa: TIntegerField;
    QrCTeInutversao: TFloatField;
    QrCTeInutId: TWideStringField;
    QrCTeInuttpAmb: TSmallintField;
    QrCTeInutcUF: TSmallintField;
    QrCTeInutano: TSmallintField;
    QrCTeInutCNPJ: TWideStringField;
    QrCTeInutmodelo: TSmallintField;
    QrCTeInutSerie: TIntegerField;
    QrCTeInutnCTIni: TIntegerField;
    QrCTeInutnCTFim: TIntegerField;
    QrCTeInutJustif: TIntegerField;
    QrCTeInutxJust: TWideStringField;
    QrCTeInutcStat: TIntegerField;
    QrCTeInutxMotivo: TWideStringField;
    QrCTeInutLk: TIntegerField;
    QrCTeInutDataCad: TDateField;
    QrCTeInutDataAlt: TDateField;
    QrCTeInutUserCad: TIntegerField;
    QrCTeInutUserAlt: TIntegerField;
    QrCTeInutAlterWeb: TSmallintField;
    QrCTeInutAtivo: TSmallintField;
    QrCTeInutCodUsu: TIntegerField;
    dmkLabel1: TdmkLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    dmkLabel2: TdmkLabel;
    EdSerie: TdmkEdit;
    EdnCTIni: TdmkEdit;
    EdnCTFim: TdmkEdit;
    dmkLabel3: TdmkLabel;
    dmkLabel4: TdmkLabel;
    dmkLabel5: TdmkLabel;
    QrCTeJust: TmySQLQuery;
    QrCTeJustCodigo: TIntegerField;
    QrCTeJustNome: TWideStringField;
    DsCTeJust: TDataSource;
    CBCTeJust: TdmkDBLookupComboBox;
    EdCTeJust: TdmkEditCB;
    EdModelo: TdmkEdit;
    dmkLabel6: TdmkLabel;
    dmkVUFilial: TdmkValUsu;
    dmkVUCTeJust: TdmkValUsu;
    dmkLabel7: TdmkLabel;
    dmkLabel9: TdmkLabel;
    dmkLabel10: TdmkLabel;
    dmkLabel11: TdmkLabel;
    dmkLabel12: TdmkLabel;
    DBEdit2: TDBEdit;
    QrCTeInutNO_Empresa: TWideStringField;
    QrCTeInutNO_Justif: TWideStringField;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label5: TLabel;
    DBEdit9: TDBEdit;
    QrCTeInutMsg: TmySQLQuery;
    DsCTeInutMsg: TDataSource;
    PMDados: TPopupMenu;
    Incluinovoprocesso1: TMenuItem;
    Alteradadosdoprocessoatual1: TMenuItem;
    TPDataC: TdmkEditDateTimePicker;
    Label4: TLabel;
    Label6: TLabel;
    QrCTeInutDataC: TDateField;
    DBEdit10: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdCodigo: TDBEdit;
    QrCTeInutMsgCodigo: TIntegerField;
    QrCTeInutMsgControle: TIntegerField;
    QrCTeInutMsgversao: TFloatField;
    QrCTeInutMsgId: TWideStringField;
    QrCTeInutMsgtpAmb: TSmallintField;
    QrCTeInutMsgverAplic: TWideStringField;
    QrCTeInutMsgcStat: TIntegerField;
    QrCTeInutMsgxMotivo: TWideStringField;
    QrCTeInutMsgcUF: TSmallintField;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    DBEdit11: TDBEdit;
    Label11: TLabel;
    DBEdit12: TDBEdit;
    Label12: TLabel;
    DBEdit13: TDBEdit;
    Label13: TLabel;
    DBEdit14: TDBEdit;
    Label14: TLabel;
    DBEdit15: TDBEdit;
    Label15: TLabel;
    DBEdit16: TDBEdit;
    Label16: TLabel;
    DBEdit17: TDBEdit;
    Label17: TLabel;
    DBEdit18: TDBEdit;
    QrCTeInutNO_Ambiente: TWideStringField;
    DBEdit19: TDBEdit;
    Label18: TLabel;
    DBEdit20: TDBEdit;
    Label19: TLabel;
    DBEdit21: TDBEdit;
    Label20: TLabel;
    DBEdit22: TDBEdit;
    Label21: TLabel;
    DBEdit23: TDBEdit;
    QrCTeInutdhRecbto: TDateTimeField;
    QrCTeInutnProt: TWideStringField;
    Label22: TLabel;
    DBEdit24: TDBEdit;
    Label23: TLabel;
    DBEdit25: TDBEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GradeMsg: TDBGrid;
    TabSheet2: TTabSheet;
    QrCTeInutXML_Inu: TWideMemoField;
    DBMemo1: TDBMemo;
    TabSheet3: TTabSheet;
    WBResposta: TWebBrowser;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel4: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtDados: TBitBtn;
    BtSolicita: TBitBtn;
    BtLeXML: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    SpeedButton5: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCTeInutAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCTeInutBeforeOpen(DataSet: TDataSet);
    procedure BtDadosClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrCTeInutAfterScroll(DataSet: TDataSet);
    procedure Incluinovoprocesso1Click(Sender: TObject);
    procedure Alteradadosdoprocessoatual1Click(Sender: TObject);
    procedure BtSolicitaClick(Sender: TObject);
    procedure BtLeXMLClick(Sender: TObject);
    procedure QrCTeInutBeforeClose(DataSet: TDataSet);
    procedure PageControl1Change(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure PMDadosPopup(Sender: TObject);
  private
    FEmpresa, FLote, FnCTIni, FnCTFim, FSerie, FModelo, FAno, FJustif: Integer;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure DefineVarsDePreparacao();
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenCTeInutMsg(Controle: Integer);
  end;

var
  FmCTeInut_0000: TFmCTeInut_0000;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, ModuleCTe_0000, CTeSteps_0200a,
  CTeJust, UnCTe_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCTeInut_0000.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCTeInut_0000.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCTeInutCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCTeInut_0000.DefParams;
begin
  VAR_GOTOTABELA := 'CTeInut';
  VAR_GOTOMYSQLTABLE := QrCTeInut;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial, ent.Nome) NO_Empresa,');
  VAR_SQLx.Add('jus.Nome NO_Justif, inu.*');
  VAR_SQLx.Add('FROM cteinut inu');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=inu.Empresa');
  VAR_SQLx.Add('LEFT JOIN ctejust jus ON jus.Codigo=inu.Justif');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE inu.Codigo > 0');
  VAR_SQLx.Add('AND inu.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND inu.Codigo=:P0');
  //
  VAR_SQL2.Add('AND inu.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND inu.Nome Like :P0');
  //
end;

procedure TFmCTeInut_0000.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'cteinut', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmCTeInut_0000.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCTeInut_0000.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 2 then
    MyObjects.LoadXML(TMemo(DBMemo1), WBResposta, PageControl1, -1);
end;

procedure TFmCTeInut_0000.PMDadosPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrCTeInut.State <> dsInactive) and (QrCTeInut.RecordCount > 0);
  //
  Alteradadosdoprocessoatual1.Enabled := Enab;
end;

procedure TFmCTeInut_0000.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmCTeInut.AlteraRegistro;
var
  CTeInut : Integer;
begin
  CTeInut := QrCTeInutCodigo.Value;
  if QrCTeInutCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(CTeInut, Dmod.MyDB, 'CTeInut', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(CTeInut, Dmod.MyDB, 'CTeInut', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCTeInut.IncluiRegistro;
var
  Cursor : TCursor;
  CTeInut : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    CTeInut := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'CTeInut', 'CTeInut', 'Codigo');
    if Length(FormatFloat(FFormatFloat, CTeInut))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro('Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, CTeInut);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmCTeInut_0000.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCTeInut_0000.ReopenCTeInutMsg(Controle: Integer);
begin
  QrCTeInutMsg.Close;
  QrCTeInutMsg.Params[0].AsInteger := QrCTeInutCodigo.Value;
  QrCTeInutMsg.Open;
end;

procedure TFmCTeInut_0000.DefineONomeDoForm;
begin
end;

procedure TFmCTeInut_0000.DefineVarsDePreparacao;
begin
  FEmpresa := QrCTeInutEmpresa.Value;
  FLote    := QrCTeInutCodigo.Value;
  FnCTIni  := QrCTeInutnCTIni.Value;
  FnCTFim  := QrCTeInutnCTFim.Value;
  FSerie   := QrCTeInutSerie.Value;
  FModelo  := QrCTeInutmodelo.Value;
  FAno     := Geral.IMV(FormatDateTime('yy', QrCTeInutDataC.Value));
  FJustif  := QrCTeInutJustif.Value;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCTeInut_0000.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCTeInut_0000.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCTeInut_0000.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCTeInut_0000.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCTeInut_0000.SpeedButton5Click(Sender: TObject);
var
  CTeJust: Integer;
begin
  VAR_CADASTRO := 0;
  CTeJust      := EdCTeJust.ValueVariant;
  //
  if DBCheck.CriaFm(TFmCTeJust, FmCTeJust, afmoNegarComAviso) then
  begin
    if CTeJust <> 0 then
      FmCTeJust.LocCod(CTeJust, CTeJust);
    FmCTeJust.ShowModal;
    FmCTeJust.Destroy;
  end;
  //
  if VAR_CADASTRO > 0 then
  begin
    //Para n�o perder a SQL pois vaira no reopen de acordo com a aplica��o
    QrCTeJust.Close;
    QrCTeJust.Open;
    //
    EdCTeJust.ValueVariant := VAR_CADASTRO;
    CBCTeJust.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmCTeInut_0000.Alteradadosdoprocessoatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrCTeInut, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'cteinut');
end;

procedure TFmCTeInut_0000.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCTeInutCodigo.Value;
  Close;
end;

procedure TFmCTeInut_0000.BtSolicitaClick(Sender: TObject);
var
  Numeros: String;
begin
  if (QrCTeInut.State <> dsInactive) and (QrCTeInut.RecordCount > 0) then
  begin
    DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
    //
    DefineVarsDePreparacao();
    if (FnCTIni = FnCTFim) then
      Numeros := 'o n�mero ' + IntTostr(FnCTIni)
    else
      Numeros := 'os n�meros de ' + IntTostr(FnCTIni) + ' at� ' + IntTostr(FnCTFim);
    if Geral.MB_Pergunta('Deseja realmente inutilizar ' + Numeros +
    ' da s�rie ' + Geral.FF0(QrCTeInutSerie.Value) + ' do modelo ' + Geral.FF0(
    QrCTeInutmodelo.Value) + '?') = ID_YES then
    begin
      CTe_PF.MostraFormStepsCTe_Inutilizacao(FEmpresa, FLote, FAno, FModelo,
      FSerie, FnCTIni, FnCTFim, FJustif);
    end;
  end;
end;

procedure TFmCTeInut_0000.BtConfirmaClick(Sender: TObject);
var
  Codigo, Serie, CTIni, CTFim: Integer;
  Nome: String;
begin
  Nome  := EdNome.ValueVariant;
  Serie := EdSerie.ValueVariant;
  CTIni := EdnCTIni.ValueVariant;
  CTFim := EdnCTFim.ValueVariant;
  //
  if MyObjects.FIC(EdCodUsu.ValueVariant = 0, EdCodUsu, 'Informe o C�digo!') then
    Exit;
  if MyObjects.FIC(EdCTeJust.ValueVariant = 0, EdCTeJust, 'Informe a justificativa!') then
    Exit;
  //if MyObjects.FIC(Serie = 0, EdSerie, 'Informe a S�rie do CT!') then
    //Exit;
  if MyObjects.FIC(CTIni = 0, EdnCTIni, 'Informe o N� CT incial!') then
    Exit;
  if MyObjects.FIC(CTFim = 0, EdnCTFim, 'Informe o N� CT final!' + sLineBreak +
    'OBS.: Caso queira inutilizar apenas um n�mero repita o n�mero preenchido no campo "N� CT incial".') then
    Exit;
  //
  Codigo := DModG.BuscaProximoCodigoInt('ctectrl', 'cteinut', '', 0);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmCTeInut_0000, PainelEdit,
    'CTeInut', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCTeInut_0000.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'CTeInut', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CTeInut', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CTeInut', 'Codigo');
end;

procedure TFmCTeInut_0000.BtLeXMLClick(Sender: TObject);
begin
  if (QrCTeInut.State <> dsInactive) and (QrCTeInut.RecordCount > 0) then
  begin
    DefineVarsDePreparacao();
    if DBCheck.CriaFm(TFmCTeSteps_0200a, FmCTeSteps_0200a, afmoNegarComAviso) then
    begin
      //
      //FmCTeSteps_0200a.PnLoteEnv.Visible := True;
      FmCTeSteps_0200a.Show;
      //
      FmCTeSteps_0200a.RGAcao.ItemIndex := 4; // Pedido de inutiliza��o
      FmCTeSteps_0200a.CkSoLer.Checked := True;
      FmCTeSteps_0200a.PreparaInutilizaNumerosCTe(FEmpresa, FLote, FAno, FModelo,
        FSerie, FnCTIni, FnCTFim, FJustif);
      //
      //
      //FmCTeSteps_0200a.Destroy;
      //
    end;
  end;
end;

procedure TFmCTeInut_0000.BtDadosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDados, BtDados);
end;

procedure TFmCTeInut_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  Screen.Cursor                := crHourGlass;
  PainelEdit.Align             := alClient;
  PageControl1.Align           := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
  TPDataC.Date      := Date;
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdFilial, CBFilial);
  QrCTeJust.Close;
  QrCTeJust.Params[0].AsInteger := 2; // inutiliza��o
  QrCTeJust.Open;
  //
  {
  QrClientes.Open;
  TPDataI.Date := Date - 31;
  TPDataF.Date := Date;
  //
  ReopenCTeCabA(0);
  }
  //
  Screen.Cursor := crDefault;
end;

procedure TFmCTeInut_0000.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCTeInutCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCTeInut_0000.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmCTeInut_0000.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCTeInut_0000.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCTeInutCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmCTeInut_0000.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCTeInut_0000.QrCTeInutAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCTeInut_0000.QrCTeInutAfterScroll(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := QrCTeInutcStat.Value <> 102;
  Alteradadosdoprocessoatual1.Enabled := Habilita;
  BtSolicita.Enabled := Habilita;
  ReopenCTeInutMsg(0);
  if PageControl1.ActivePageIndex = 2 then
    MyObjects.LoadXML(TMemo(DBMemo1), WBResposta, PageControl1, -1);
end;

procedure TFmCTeInut_0000.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCTeInut_0000.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCTeInutCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'CTeInut', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCTeInut_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCTeInut_0000.Incluinovoprocesso1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrCTeInut, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'cteinut');
  EdModelo.ValueVariant := 55;
  TPDataC.Date := Date;
end;

procedure TFmCTeInut_0000.QrCTeInutBeforeClose(DataSet: TDataSet);
begin
  Alteradadosdoprocessoatual1.Enabled := False;
  BtSolicita.Enabled := False;
  QrCTeInutMsg.Close;
end;

procedure TFmCTeInut_0000.QrCTeInutBeforeOpen(DataSet: TDataSet);
begin
  QrCTeInutCodigo.DisplayFormat := FFormatFloat;
end;

end.

