unit UnCTeListas;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  UnInternalConsts2, ComCtrls, dmkGeral, dmkEdit, Variants, UnDmkEnums;

var
  VAR_TXT_verscteStatusServico:       string = '2.00';
  VAR_TXT_verscteRecepcao:            string = '2.00';
  VAR_TXT_verscteRetRecepcao:         string = '2.00';
  VAR_TXT_verscteEveCancelamento:     string = '2.00';
  VAR_TXT_versctePedInutilizacao:     string = '2.00';
  VAR_TXT_verscteConsultaCTe:         string = '2.00';
  VAR_TXT_verscteEveCartaDeCorrecao:  string = '2.00';
  VAR_TXT_verscteConsultaCadastro:    string = '2.00';
  VAR_TXT_verscteEveEPEC:             string = '2.00';

const
  CO_MAX_ITENS_CTE_ObsCont   = 10;
  CO_MAX_ITENS_CTE_ObsFisco  = 10;
  //
  CO_TXT_tpVeicTracao        = 'Tra��o';
  CO_TXT_tpVeicReboque       = 'Reboque';
  MaxCTeTpVeic = Integer(High(TCTeTpVeic));
  sCTeTpVeic: array[0..MaxCTeTpVeic] of string = (
    CO_TXT_tpVeicTracao        , // 0
    CO_TXT_tpVeicReboque         // 1
  );

  CO_TXT_Sim = 'SIM';
  CO_TXT_Nao = 'N�O';
  MaxCteRodoLota = Integer(High(TCTeRodoLota));
  sCTeRodoLota: array[0..MaxCTeRodoLota] of string = (
    CO_TXT_Nao        , // 0
    CO_TXT_Sim         // 1
  );

  CO_TXT_tpRodNaoAplicael    = 'n�o aplic�vel';
  CO_TXT_tpRodTruck          = 'Truck';
  CO_TXT_tpRodToco           = 'Toco';
  CO_TXT_tpRodCavaloMec      = 'Cavalo Mec�nico';
  CO_TXT_tpRodVAN            = 'VAN';
  CO_TXT_tpRodUtilitaio      = 'Utilit�rio';
  CO_TXT_tpRodOutros         = 'Outros';
  MaxCTeTpRod = Integer(High(TCTeTpRod));
  sCTeTpRod: array[0..MaxCTeTpRod] of string = (
    CO_TXT_tpRodNaoAplicael    , //00
    CO_TXT_tpRodTruck          , //01
    CO_TXT_tpRodToco           , //02
    CO_TXT_tpRodCavaloMec      , //03
    CO_TXT_tpRodVAN            , //04
    CO_TXT_tpRodUtilitaio      , //05
    CO_TXT_tpRodOutros           //06
  );

  CO_TXT_tpCarNaoAplicael    = 'n�o aplic�vel';
  CO_TXT_tpCarAberta         = 'Aberta';
  CO_TXT_tpCarFechadaBau     = 'Fechada/Ba�';
  CO_TXT_tpCarGranelera      = 'Granelera';
  CO_TXT_tpCarPortaContainer = 'Porta Container';
  CO_TXT_tpCarSider          =  'Sider';
  MaxCTeTpCar = Integer(High(TCTeTpCar));
  sCTeTpCar: array[0..MaxCTeTpCar] of string = (
    CO_TXT_tpCarNaoAplicael    , // 00
    CO_TXT_tpCarAberta         , // 01
    CO_TXT_tpCarFechadaBau     , // 02
    CO_TXT_tpCarGranelera      , // 03
    CO_TXT_tpCarPortaContainer , // 04
    CO_TXT_tpCarSider           // 05
  );

  CO_TXT_forpagPagPago    = 'Pago';
  CO_TXT_forpagAPagar     = 'A pagar';
  CO_TXT_forpagOutros     = 'Outros';
  MaxCTeForPag = Integer(High(TCTeForPag));
  sCTeForPag: array[0..MaxCTeForPag] of string = (
    CO_TXT_forpagPagPago    ,
    CO_TXT_forpagAPagar     ,
    CO_TXT_forpagOutros
  );

  CO_TXT_tpcteNormal         = 'CT-e Normal';
  CO_TXT_tpcteComplValrs     = 'CT-e de Complemento de Valores';
  CO_TXT_tpcteAnulacao       = 'CT-e de Anula��o';
  CO_TXT_tpcteSubstituto     = 'CT-e Substituto';
  MaxCTeTpCTe = Integer(High(TCTeTpCTe));
  sCTeTpCTe: array[0..MaxCTeTpCte] of string = (
    CO_TXT_tpcteNormal       , // 00
    CO_TXT_tpcteComplValrs   , // 01
    CO_TXT_tpcteAnulacao     , // 02
    CO_TXT_tpcteSubstituto    // 03
  );

  CO_TXT_ctemodalIndefinido     = 'Indefinido';
  CO_TXT_ctemodalRodoviario     = 'Rodovi�rio';
  CO_TXT_ctemodalAereo          = 'A�reo';
  CO_TXT_ctemodalAquaviario     = 'Aquavi�rio';
  CO_TXT_ctemodalFerroviario    = 'Ferrovi�rio';
  CO_TXT_ctemodalDutoviario     = 'Dutovi�rio';
  CO_TXT_ctemodalMultimodal     = 'Multimodal';
  MaxCTemodal = Integer(High(TCTemodal));
  sCTemodal: array[0..MaxCTemodal] of string = (
  CO_TXT_ctemodalIndefinido     ,
  CO_TXT_ctemodalRodoviario     ,
  CO_TXT_ctemodalAereo          ,
  CO_TXT_ctemodalAquaviario     ,
  CO_TXT_ctemodalFerroviario    ,
  CO_TXT_ctemodalDutoviario     ,
  CO_TXT_ctemodalMultimodal
  );

  CO_TXT_tpservNormal             = 'Normal';
  CO_TXT_tpservSubcontratacao     = 'Subcontrata��o';
  CO_TXT_tpservRedespacho         = 'Redespacho';
  CO_TXT_tpservRedespInterm       = 'Redespacho intermedi�rio';
  CO_TXT_tpservServVincMultimodal = 'Servi�o Vinculado a Multimodal';
  MaxCTetpserv = Integer(High(TCTetpserv));
  sCTetpserv: array[0..MaxCTetpserv] of string = (
  CO_TXT_tpservNormal              ,
  CO_TXT_tpservSubcontratacao      ,
  CO_TXT_tpservRedespacho          ,
  CO_TXT_tpservRedespInterm        ,
  CO_TXT_tpservServVincMultimodal
  );

  CO_TXT_TomaRemetente        = 'Remetente';
  CO_TXT_TomaExpedidor        = 'Expedidor';
  CO_TXT_TomaRecebedor        = 'Recebedor';
  CO_TXT_TomaDestinatario     = 'Destinat�rio';
  CO_TXT_TomaOutros           = 'Outros';
  MaxCTeToma = Integer(High(TCTeToma));
  sCTeToma: array[0..MaxCTeToma] of string = (
  CO_TXT_TomaRemetente     ,
  CO_TXT_TomaExpedidor     ,
  CO_TXT_TomaRecebedor     ,
  CO_TXT_TomaDestinatario  ,
  CO_TXT_TomaOutros
  );

  CO_TXT_mycstIndefinido         =  'N�o definido';
  CO_TXT_mycstCST00Normal        =  '00-Tributa��o Normal do ICMS';
  CO_TXT_mycstCST20BCRed         =  '20-tributa��o com BC reduzida do ICMS';
  CO_TXT_mycstCST40Isensao       =  '40-ICMS isen��o;';
  CO_TXT_mycstCST41NaoTrib       =  '41-ICMS n�o tributada;';
  CO_TXT_mycstCST51Diferido      =  '51-ICMS diferido';
  CO_TXT_mycstCST60ST            =  '60-ICMS cobrado anteriormente por substitui��o tribut�ria';
  CO_TXT_mycstCST90Outros        =  '90-ICMS outros - devido � UF de origem da presta��o';
  CO_TXT_mycstCST90OutraUF       =  '90-ICMS outros';
  CO_TXT_mycstCSTSimplesNacional =  'Simples Nacional';
  MaxCTeMyCST = Integer(High(TCTeMyCST));
  sCTeMyCST: array[0..MaxCTeMyCST] of string = (
    CO_TXT_mycstIndefinido         , // 0
    CO_TXT_mycstCST00Normal        , // 1
    CO_TXT_mycstCST20BCRed         , // 2
    CO_TXT_mycstCST40Isensao       , // 3
    CO_TXT_mycstCST41NaoTrib       , // 4
    CO_TXT_mycstCST51Diferido      , // 5
    CO_TXT_mycstCST60ST            , // 6
    CO_TXT_mycstCST90Outros        , // 7
    CO_TXT_mycstCST90OutraUF       , // 8
    CO_TXT_mycstCSTSimplesNacional  // 9
  );

  CO_TXT_CUnidM3        = 'M3';
  CO_TXT_CUnidKG        = 'KG';
  CO_TXT_CUnidTON       = 'TON';
  CO_TXT_CUnidUNIDADE   = 'UNIDADE';
  CO_TXT_CUnidLITROS    = 'LITROS';
  CO_TXT_CUnidMMBTU     = 'MMBTU';
  MaxCTeCUnid = Integer(High(TCTeCUnid));
  sCTeCUnid: array[0..MaxCTeCUnid] of string = (
  CO_TXT_CUnidM3       ,
  CO_TXT_CUnidKG       ,
  CO_TXT_CUnidTON      ,
  CO_TXT_CUnidUNIDADE  ,
  CO_TXT_CUnidLITROS   ,
  CO_TXT_CUnidMMBTU
  );

  CO_TXT_tcwscteStatusServico        = 'CteStatusServico';
  CO_TXT_tcwscteRecepcao             = 'CteRecepcao';
  CO_TXT_tcwscteRetRecepcao          = 'CteRetRecepcao';
  CO_TXT_tcwscteEveCancelamento      = 'CteRecepcaoEvento';
  CO_TXT_tcwsctePediInutilizacao     = 'CteInutilizacao';
  CO_TXT_tcwscteConsultaCTe          = 'CteConsultaProtocolo';
  CO_TXT_tcwscteEveCartaDeCorrecao   = 'CteRecepcaoEvento';
  CO_TXT_tcwscteConsultaCadastro     = 'CteConsultaCadastro';
  CO_TXT_tcwscteEveEPEC              = 'CteRecepcaoEvento';
  MaxTipoConsumoWS_CTe = Integer(High(TTipoConsumoWS_CTe));
  sTipoConsumoWS_CTe: array[0..MaxTipoConsumoWS_CTe] of string = (
  CO_TXT_tcwscteStatusServico,
  CO_TXT_tcwscteRecepcao,
  CO_TXT_tcwscteRetRecepcao,
  CO_TXT_tcwscteEveCancelamento,
  CO_TXT_tcwsctePediInutilizacao,
  CO_TXT_tcwscteConsultaCTe,
  CO_TXT_tcwscteEveCartaDeCorrecao,
  CO_TXT_tcwscteConsultaCadastro,
  CO_TXT_tcwscteEveEPEC
  );

////////////////////////////////////////////////////////////////////////////////
//veja CO_TXT_verscte...
  MaxVersaoConsumoWS_CTe = Integer(High(TTipoConsumoWS_CTe));
////////////////////////////////////////////////////////////////////////////////
(*
  CO_TXT_verscteStatusServico        = '2.00';
  CO_TXT_verscteRecepcao             = '2.00';
  CO_TXT_verscteRetRecepcao          = '2.00';
  MaxVersaoConsumoWS_CTe = Integer(High(TTipoConsumoWS_CTe));
  sVersaoConsumoWS_CTe: array[0..MaxTipoConsumoWS_CTe] of string = (
  CO_TXT_verscteStatusServico,
  CO_TXT_verscteRecepcao,
  CO_TXT_verscteRetRecepcao
  );
*)
  CO_TXT_cteptpTAC_Agregado     = 'TAC - Agregado';
  CO_TXT_cteptpTAC_Independente = 'TAC - Independente';
  CO_TXT_cteptpTAC_Outros       = 'Outros';
  MaxCTePropTpProp = Integer(High(TCTePropTpProp));
  sCTePropTpProp: array[0..MaxCTePropTpProp] of string = (
    CO_TXT_cteptpTAC_Agregado,
    CO_TXT_cteptpTAC_Independente,
    CO_TXT_cteptpTAC_Outros
  );
  //
  CO_TXT_ctersRemetente        = 'Remetente';
  CO_TXT_ctersExpedidor        = 'Expedidor';
  CO_TXT_ctersRecebedor        = 'Recebedor';
  CO_TXT_ctersDestinatario     = 'Destinat�io';
  CO_TXT_ctersEmitenteDoCTe    = 'Emitente do CT-e';
  CO_TXT_ctersTomadorDoServico = 'Tomador do Servico';
  MaxCTerespSeg = Integer(High(TCTerespSeg));
  sCTerespSeg: array[0..MaxCTerespSeg] of string = (
    CO_TXT_ctersRemetente,
    CO_TXT_ctersExpedidor,
    CO_TXT_ctersRecebedor,
    CO_TXT_ctersDestinatario,
    CO_TXT_ctersEmitenteDoCTe,
    CO_TXT_ctersTomadorDoServico
  );

  CO_TXT_ctetpemis0                = '0 - Inexistente';
  CO_TXT_ctetpemisNormal           = '1 - Normal';
  CO_TXT_ctetpemis2                = '2 - Nao implementado';
  CO_TXT_ctetpemis3                = '3 - Nao implementado';
  CO_TXT_ctetpemisEPEC_SVC         = '4 - EPEC pela SVC';
  CO_TXT_ctetpemisContingenciaFSDA = '5 - Conting�ncia FSDA';
  CO_TXT_ctetpemis6                = '6 - Nao implementado';
  CO_TXT_ctetpemisSVC_RS           = '7 - SVC-RS';
  CO_TXT_ctetpemisSVC_SP           = '8 - SVC-SP';
  MaxCTeTpEmis = Integer(High(TCTeTpEmis));
  sCTeTpEmis: array[0..MaxCTeTpEmis] of string = (
  CO_TXT_ctetpemis0                ,
  CO_TXT_ctetpemisNormal           ,
  CO_TXT_ctetpemis2                ,
  CO_TXT_ctetpemis3                ,
  CO_TXT_ctetpemisEPEC_SVC         ,
  CO_TXT_ctetpemisContingenciaFSDA ,
  CO_TXT_ctetpemis6                ,
  CO_TXT_ctetpemisSVC_RS           ,
  CO_TXT_ctetpemisSVC_SP
  );

type
  TUnCTeListas = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    //function  ArrayEnquadramentoLegalIPI(): MyArrayLista;

  end;

var
  CTeListas: TUnCTeListas;

implementation

{ TUnCTeListas }

(*
function TUnCTeListas.ArrayEnquadramentoLegalIPI: MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo,
  Grupo, Descricao: String);
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Grupo;
    Res[Linha][2] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
  AddLinha(Result, Linha, '001', 'Imunidade ', 'Livros, jornais, peri�dicos e o papel destinado � sua impress�o - Art. 18 Inciso I do Decreto 7.212/2010');
  ...
  AddLinha(Result, Linha, '999', 'Outros', 'Tributa��o normal IPI; Outros;');
end;
*)

end.
