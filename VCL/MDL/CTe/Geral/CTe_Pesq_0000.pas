unit CTe_Pesq_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, dmkEdit, dmkEditCB,
  DBCtrls, dmkDBLookupComboBox, DB, mySQLDbTables, ComCtrls,
  dmkEditDateTimePicker, Grids, DBGrids, dmkDBGrid, Variants, Menus, dmkMemo,
  frxClass, frxDBSet, dmkGeral, frxBarcode, UnDmkProcFunc, dmkImage,
  dmkCheckGroup, frxExportPDF, UnDmkEnums, dmkCompoStore, UnCTeListas;

type
  THackDBGrid = class(TdmkDBGrid);
  TFmCTe_Pesq_0000 = class(TForm)
    PnDados: TPanel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_Enti: TWideStringField;
    DsClientes: TDataSource;
    dmkDBGrid1: TdmkDBGrid;
    QrClientesTipo: TSmallintField;
    QrClientesCNPJ: TWideStringField;
    QrClientesCPF: TWideStringField;
    PMLeArq: TPopupMenu;
    Cancelamento1: TMenuItem;
    PMImprime: TPopupMenu;
    CampospreenchidosnoXMLdaCTe1: TMenuItem;
    QrCTeCabA: TmySQLQuery;
    QrCTeCabANO_Cli: TWideStringField;
    QrCTeCabAFatID: TIntegerField;
    QrCTeCabAFatNum: TIntegerField;
    QrCTeCabAEmpresa: TIntegerField;
    QrCTeCabALoteEnv: TIntegerField;
    QrCTeCabAId: TWideStringField;
    QrCTeCabAide_natOp: TWideStringField;
    QrCTeCabAide_serie: TIntegerField;
    QrCTeCabAide_nCT: TIntegerField;
    QrCTeCabAide_dEmi: TDateField;
    QrCTeCabAide_tpCTe: TSmallintField;
    QrCTeCabAnProt: TWideStringField;
    QrCTeCabAxMotivo: TWideStringField;
    QrCTeCabAdhRecbto: TWideStringField;
    QrCTeCabAIDCtrl: TIntegerField;
    DsCTeCabA: TDataSource;
    frxDsCTeCabA: TfrxDBDataset;
    QrCTeCabAMsg: TmySQLQuery;
    QrCTeCabAMsgFatID: TIntegerField;
    QrCTeCabAMsgFatNum: TIntegerField;
    QrCTeCabAMsgEmpresa: TIntegerField;
    QrCTeCabAMsgControle: TIntegerField;
    QrCTeCabAMsgSolicit: TIntegerField;
    QrCTeCabAMsgId: TWideStringField;
    QrCTeCabAMsgtpAmb: TSmallintField;
    QrCTeCabAMsgverAplic: TWideStringField;
    QrCTeCabAMsgdhRecbto: TDateTimeField;
    QrCTeCabAMsgnProt: TWideStringField;
    QrCTeCabAMsgdigVal: TWideStringField;
    QrCTeCabAMsgcStat: TIntegerField;
    QrCTeCabAMsgxMotivo: TWideStringField;
    DsCTeCabAMsg: TDataSource;
    frxDsA: TfrxDBDataset;
    QrA: TmySQLQuery;
    QrCTeXMLI: TmySQLQuery;
    QrCTeXMLICodigo: TWideStringField;
    QrCTeXMLIID: TWideStringField;
    QrCTeXMLIValor: TWideStringField;
    QrCTeXMLIPai: TWideStringField;
    QrCTeXMLIDescricao: TWideStringField;
    QrCTeXMLICampo: TWideStringField;
    frxDsCTeXMLI: TfrxDBDataset;
    frxCampos: TfrxReport;
    QrArq: TmySQLQuery;
    QrArqFatID: TIntegerField;
    QrArqFatNum: TIntegerField;
    QrArqEmpresa: TIntegerField;
    QrArqIDCtrl: TIntegerField;
    QrArqXML_CTe: TWideMemoField;
    QrArqXML_Aut: TWideMemoField;
    QrArqXML_Can: TWideMemoField;
    QrArqLk: TIntegerField;
    QrArqDataCad: TDateField;
    QrArqDataAlt: TDateField;
    QrArqUserCad: TIntegerField;
    QrArqUserAlt: TIntegerField;
    QrArqAlterWeb: TSmallintField;
    QrArqAtivo: TSmallintField;
    PMArq: TPopupMenu;
    CTe1: TMenuItem;
    Autorizao1: TMenuItem;
    Cancelamento2: TMenuItem;
    QrCTeCabAdest_CNPJ: TWideStringField;
    QrCTeCabAdest_CPF: TWideStringField;
    Panel3: TPanel;
    QrCTeCabAdest_xNome: TWideStringField;
    QrCTeCabAversao: TFloatField;
    N1: TMenuItem;
    DiretriodoarquivoXML1: TMenuItem;
    frxListaCTes: TfrxReport;
    N2: TMenuItem;
    Listadasnotaspesquisadas1: TMenuItem;
    QrCTeCabAide_tpEmis: TSmallintField;
    QrCTeCabANOME_tpEmis: TWideStringField;
    QrCTeCabANOME_tpCTe: TWideStringField;
    QrCTeCabAinfCanc_xJust: TWideStringField;
    QrCTeCabAStatus: TIntegerField;
    QrCTe_100: TmySQLQuery;
    frxDsCTe_100: TfrxDBDataset;
    QrCTe_XXX: TmySQLQuery;
    frxDsCTe_XXX: TfrxDBDataset;
    QrCTe_XXXOrdem: TIntegerField;
    QrCTe_XXXcStat: TIntegerField;
    QrCTe_XXXide_nNF: TIntegerField;
    QrCTe_XXXide_serie: TIntegerField;
    QrCTe_XXXide_AAMM_AA: TWideStringField;
    QrCTe_XXXide_AAMM_MM: TWideStringField;
    QrCTe_XXXide_dEmi: TDateField;
    QrCTe_XXXNOME_tpEmis: TWideStringField;
    QrCTe_XXXNOME_tpNF: TWideStringField;
    QrCTe_XXXStatus: TIntegerField;
    QrCTe_XXXMotivo: TWideStringField;
    QrCTe_100ide_nNF: TIntegerField;
    QrCTe_100ide_serie: TIntegerField;
    QrCTe_100ide_dEmi: TDateField;
    QrCTe_100ICMSTot_vProd: TFloatField;
    QrCTe_100ICMSTot_vST: TFloatField;
    QrCTe_100ICMSTot_vFrete: TFloatField;
    QrCTe_100ICMSTot_vSeg: TFloatField;
    QrCTe_100ICMSTot_vIPI: TFloatField;
    QrCTe_100ICMSTot_vOutro: TFloatField;
    QrCTe_100ICMSTot_vDesc: TFloatField;
    QrCTe_100ICMSTot_vNF: TFloatField;
    QrCTe_100ICMSTot_vBC: TFloatField;
    QrCTe_100ICMSTot_vICMS: TFloatField;
    QrCTe_100NOME_tpEmis: TWideStringField;
    QrCTe_100NOME_tpNF: TWideStringField;
    QrCTe_100Ordem: TIntegerField;
    QrCTe_XXXNOME_ORDEM: TWideStringField;
    QrCTeCabAinfCanc_dhRecbto: TDateTimeField;
    QrCTeCabAinfCanc_nProt: TWideStringField;
    QrCTe_101: TmySQLQuery;
    frxDsCTe_101: TfrxDBDataset;
    QrCTe_101Ordem: TIntegerField;
    QrCTe_101ide_nNF: TIntegerField;
    QrCTe_101ide_serie: TIntegerField;
    QrCTe_101ide_AAMM_AA: TWideStringField;
    QrCTe_101ide_AAMM_MM: TWideStringField;
    QrCTe_101ide_dEmi: TDateField;
    QrCTe_101NOME_tpEmis: TWideStringField;
    QrCTe_101NOME_tpNF: TWideStringField;
    QrCTe_101infCanc_dhRecbto: TDateTimeField;
    QrCTe_101infCanc_nProt: TWideStringField;
    QrCTe_101Motivo: TWideStringField;
    QrCTe_101Id: TWideStringField;
    QrCTe_101Id_TXT: TWideStringField;
    PreviewdaCTe1: TMenuItem;
    QrCTe_100ICMSTot_vPIS: TFloatField;
    QrCTe_100ICMSTot_vCOFINS: TFloatField;
    QrCTeCabAcStat: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    BtEnvia: TBitBtn;
    BtEvento: TBitBtn;
    BtInutiliza: TBitBtn;
    BtLeArq: TBitBtn;
    BtArq: TBitBtn;
    BtConsulta: TBitBtn;
    BtExclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    frxA4A_002: TfrxReport;
    Splitter2: TSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGEventos: TDBGrid;
    TabSheet2: TTabSheet;
    DBGMensagens: TDBGrid;
    QrCTeEveRRet: TmySQLQuery;
    DsCTeEveRRet: TDataSource;
    QrCTeEveRRetControle: TIntegerField;
    QrCTeEveRRetSubCtrl: TIntegerField;
    QrCTeEveRRetret_versao: TFloatField;
    QrCTeEveRRetret_Id: TWideStringField;
    QrCTeEveRRetret_tpAmb: TSmallintField;
    QrCTeEveRRetret_verAplic: TWideStringField;
    QrCTeEveRRetret_cOrgao: TSmallintField;
    QrCTeEveRRetret_cStat: TIntegerField;
    QrCTeEveRRetret_xMotivo: TWideStringField;
    QrCTeEveRRetret_chCTe: TWideStringField;
    QrCTeEveRRetret_tpEvento: TIntegerField;
    QrCTeEveRRetret_xEvento: TWideStringField;
    QrCTeEveRRetret_nSeqEvento: TIntegerField;
    QrCTeEveRRetret_CNPJDest: TWideStringField;
    QrCTeEveRRetret_CPFDest: TWideStringField;
    QrCTeEveRRetret_emailDest: TWideStringField;
    QrCTeEveRRetret_dhRegEvento: TDateTimeField;
    QrCTeEveRRetret_TZD_UTC: TFloatField;
    QrCTeEveRRetret_nProt: TWideStringField;
    QrCTeEveRRetNO_EVENTO: TWideStringField;
    Panel5: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    BtReabre: TBitBtn;
    Label1: TLabel;
    TPDataI: TdmkEditDateTimePicker;
    Label2: TLabel;
    TPDataF: TdmkEditDateTimePicker;
    Ck100e101: TCheckBox;
    Panel6: TPanel;
    dmkLabel1: TdmkLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    Label14: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    RGAmbiente: TRadioGroup;
    RGQuemEmit: TRadioGroup;
    QrCTeCabAcSitCTe: TSmallintField;
    Panel7: TPanel;
    frxA4A_003: TfrxReport;
    frxPDFExport: TfrxPDFExport;
    CSTabSheetChamou: TdmkCompoStore;
    QrCTeCabANO_Emi: TWideStringField;
    QrCTeCabANO_Terceiro: TWideStringField;
    N3: TMenuItem;
    ImprimeSomenteadmin1: TMenuItem;
    CkideNatOp: TCheckBox;
    frxListaCTesB: TfrxReport;
    QrCTe_100ide_NatOP: TWideStringField;
    QrCTeCabXVol: TmySQLQuery;
    QrCTeCabXVolqVol: TFloatField;
    QrCTeCabXVolesp: TWideStringField;
    QrCTeCabXVolmarca: TWideStringField;
    QrCTeCabXVolnVol: TWideStringField;
    QrCTeCabXVolPesoL: TFloatField;
    QrCTeCabXVolPesoB: TFloatField;
    QrCTe_100qVol: TFloatField;
    QrCTe_100esp: TWideStringField;
    QrCTe_100marca: TWideStringField;
    QrCTe_100nVol: TWideStringField;
    QrCTe_100PesoL: TFloatField;
    QrCTe_100PesoB: TFloatField;
    N4: TMenuItem;
    este1: TMenuItem;
    porNatOp1: TMenuItem;
    porCFOP1: TMenuItem;
    PB1: TProgressBar;
    N5: TMenuItem;
    ExportaporNatOptotaisCTe1: TMenuItem;
    ExportaporCFOPtotaiseitensCTe1: TMenuItem;
    QrCTeCabAvPrest_vTPrest: TFloatField;
    QrCTeCabAvPrest_vRec: TFloatField;
    QrCTeCabAICMS_vICMS: TFloatField;
    QrAFatID: TIntegerField;
    QrAFatNum: TIntegerField;
    QrAEmpresa: TIntegerField;
    QrAIDCtrl: TIntegerField;
    QrALoteEnv: TIntegerField;
    QrAversao: TFloatField;
    QrAId: TWideStringField;
    QrAide_cUF: TSmallintField;
    QrAide_cCT: TIntegerField;
    QrAide_CFOP: TIntegerField;
    QrAide_natOp: TWideStringField;
    QrAide_forPag: TSmallintField;
    QrAide_mod: TSmallintField;
    QrAide_serie: TIntegerField;
    QrAide_nCT: TIntegerField;
    QrAide_dEmi: TDateField;
    QrAide_hEmi: TTimeField;
    QrAide_dhEmiTZD: TFloatField;
    QrAide_tpImp: TSmallintField;
    QrAide_tpEmis: TSmallintField;
    QrAide_cDV: TSmallintField;
    QrAide_tpAmb: TSmallintField;
    QrAide_tpCTe: TSmallintField;
    QrAide_procEmi: TSmallintField;
    QrAide_verProc: TWideStringField;
    QrAide_refCTE: TWideStringField;
    QrAide_cMunEnv: TIntegerField;
    QrAide_xMunEnv: TWideStringField;
    QrAide_UFEnv: TWideStringField;
    QrAide_Modal: TSmallintField;
    QrAide_tpServ: TSmallintField;
    QrAide_cMunIni: TIntegerField;
    QrAide_xMunIni: TWideStringField;
    QrAide_UFIni: TWideStringField;
    QrAide_cMunFim: TIntegerField;
    QrAide_xMunFim: TWideStringField;
    QrAide_UFFim: TWideStringField;
    QrAide_Retira: TSmallintField;
    QrAtoma99_toma: TSmallintField;
    QrAtoma04_toma: TSmallintField;
    QrAtoma04_CNPJ: TWideStringField;
    QrAtoma04_CPF: TWideStringField;
    QrAtoma04_IE: TWideStringField;
    QrAtoma04_xNome: TWideStringField;
    QrAtoma04_xFant: TWideStringField;
    QrAtoma04_fone: TWideStringField;
    QrAide_dhCont: TDateTimeField;
    QrAide_dhContTZD: TFloatField;
    QrAide_xJust: TWideStringField;
    QrAcompl_xCaracAd: TWideStringField;
    QrAcompl_xCaracSer: TWideStringField;
    QrAcompl_xEmi: TWideStringField;
    QrAentrega_tpPer: TSmallintField;
    QrAentrega_dProg: TDateField;
    QrAentrega_dIni: TDateField;
    QrAentrega_dFim: TDateField;
    QrAentrega_tpHor: TSmallintField;
    QrAentrega_hProg: TTimeField;
    QrAentrega_hIni: TDateField;
    QrAentrega_hFim: TDateField;
    QrAide_origCalc: TWideStringField;
    QrAide_destCalc: TWideStringField;
    QrAide_xObs: TWideMemoField;
    QrAemit_CNPJ: TWideStringField;
    QrAemit_IE: TWideStringField;
    QrAemit_xNome: TWideStringField;
    QrAemit_xFant: TWideStringField;
    QrAemit_xLgr: TWideStringField;
    QrAemit_nro: TWideStringField;
    QrAemit_xCpl: TWideStringField;
    QrAemit_xBairro: TWideStringField;
    QrAemit_cMun: TIntegerField;
    QrAemit_xMun: TWideStringField;
    QrAemit_CEP: TIntegerField;
    QrAemit_UF: TWideStringField;
    QrAemit_fone: TWideStringField;
    QrArem_CNPJ: TWideStringField;
    QrArem_CPF: TWideStringField;
    QrArem_IE: TWideStringField;
    QrArem_xNome: TWideStringField;
    QrArem_xFant: TWideStringField;
    QrArem_fone: TWideStringField;
    QrArem_xLgr: TWideStringField;
    QrArem_nro: TWideStringField;
    QrArem_xCpl: TWideStringField;
    QrArem_xBairro: TWideStringField;
    QrArem_cMun: TIntegerField;
    QrArem_xMun: TWideStringField;
    QrArem_CEP: TIntegerField;
    QrArem_UF: TWideStringField;
    QrArem_cPais: TIntegerField;
    QrArem_xPais: TWideStringField;
    QrArem_email: TWideStringField;
    QrAdest_CNPJ: TWideStringField;
    QrAdest_CPF: TWideStringField;
    QrAdest_IE: TWideStringField;
    QrAdest_xNome: TWideStringField;
    QrAdest_fone: TWideStringField;
    QrAdest_ISUF: TWideStringField;
    QrAdest_xLgr: TWideStringField;
    QrAdest_nro: TWideStringField;
    QrAdest_xCpl: TWideStringField;
    QrAdest_xBairro: TWideStringField;
    QrAdest_cMun: TIntegerField;
    QrAdest_xMun: TWideStringField;
    QrAdest_CEP: TIntegerField;
    QrAdest_UF: TWideStringField;
    QrAdest_cPais: TIntegerField;
    QrAdest_xPais: TWideStringField;
    QrAdest_email: TWideStringField;
    QrAvPrest_vTPrest: TFloatField;
    QrAvPrest_vRec: TFloatField;
    QrAICMS_CST: TSmallintField;
    QrAICMS_vBC: TFloatField;
    QrAICMS_pICMS: TFloatField;
    QrAICMS_vICMS: TFloatField;
    QrAICMS_pRedBC: TFloatField;
    QrAICMS_vBCSTRet: TFloatField;
    QrAICMS_vICMSSTRet: TFloatField;
    QrAICMS_pICMSSTRet: TFloatField;
    QrAICMS_vCred: TFloatField;
    QrAICMS_pRedBCOutraUF: TFloatField;
    QrAICMS_vBCOutraUF: TFloatField;
    QrAICMS_pICMSOutraUF: TFloatField;
    QrAICMS_vICMSOutraUF: TFloatField;
    QrAICMSSN_indSN: TSmallintField;
    QrAinfCarga_vCarga: TFloatField;
    QrAinfCarga_xOutCat: TWideStringField;
    QrAcobr_Fat_nFat: TWideStringField;
    QrAcobr_Fat_vOrig: TFloatField;
    QrAcobr_Fat_vDesc: TFloatField;
    QrAcobr_Fat_vLiq: TFloatField;
    QrAinfCTeComp_chave: TWideStringField;
    QrAinfCTeAnu_chCte: TWideStringField;
    QrAinfCTeAnu_dEmi: TDateField;
    QrAprotCTe_versao: TFloatField;
    QrAinfProt_Id: TWideStringField;
    QrAinfProt_tpAmb: TSmallintField;
    QrAinfProt_verAplic: TWideStringField;
    QrAinfProt_dhRecbto: TDateTimeField;
    QrAinfProt_dhRecbtoTZD: TFloatField;
    QrAinfProt_nProt: TWideStringField;
    QrAinfProt_digVal: TWideStringField;
    QrAinfProt_cStat: TIntegerField;
    QrAinfProt_xMotivo: TWideStringField;
    QrAretCancCTe_versao: TFloatField;
    QrAinfCanc_Id: TWideStringField;
    QrAinfCanc_tpAmb: TSmallintField;
    QrAinfCanc_verAplic: TWideStringField;
    QrAinfCanc_dhRecbto: TDateTimeField;
    QrAinfCanc_dhRecbtoTZD: TFloatField;
    QrAinfCanc_nProt: TWideStringField;
    QrAinfCanc_digVal: TWideStringField;
    QrAinfCanc_cStat: TIntegerField;
    QrAinfCanc_xMotivo: TWideStringField;
    QrAinfCanc_cJust: TIntegerField;
    QrAinfCanc_xJust: TWideStringField;
    QrAFisRegCad: TIntegerField;
    QrACartEmiss: TIntegerField;
    QrATabelaPrc: TIntegerField;
    QrACondicaoPg: TIntegerField;
    QrACodInfoRemet: TIntegerField;
    QrACodInfoExped: TIntegerField;
    QrACodInfoReceb: TIntegerField;
    QrACodInfoDesti: TIntegerField;
    QrACodInfoToma4: TIntegerField;
    QrACriAForca: TSmallintField;
    QrAcSitCTe: TSmallintField;
    QrAinfCCe_verAplic: TWideStringField;
    QrAinfCCe_cOrgao: TSmallintField;
    QrAinfCCe_tpAmb: TSmallintField;
    QrAinfCCe_CNPJ: TWideStringField;
    QrAinfCCe_CPF: TWideStringField;
    QrAinfCCe_chCTe: TWideStringField;
    QrAinfCCe_dhEvento: TDateTimeField;
    QrAinfCCe_dhEventoTZD: TFloatField;
    QrAinfCCe_tpEvento: TIntegerField;
    QrAinfCCe_nSeqEvento: TIntegerField;
    QrAinfCCe_verEvento: TFloatField;
    QrAinfCCe_cStat: TIntegerField;
    QrAinfCCe_dhRegEvento: TDateTimeField;
    QrAinfCCe_dhRegEventoTZD: TFloatField;
    QrAinfCCe_nProt: TWideStringField;
    QrAinfCCe_nCondUso: TIntegerField;
    QrALk: TIntegerField;
    QrADataCad: TDateField;
    QrADataAlt: TDateField;
    QrAUserCad: TIntegerField;
    QrAUserAlt: TIntegerField;
    QrAAlterWeb: TSmallintField;
    QrAAtivo: TSmallintField;
    QrAiTotTrib_Fonte: TIntegerField;
    QrAiTotTrib_Tabela: TIntegerField;
    QrAiTotTrib_Ex: TIntegerField;
    QrAiTotTrib_Codigo: TLargeintField;
    QrAvBasTrib: TFloatField;
    QrApTotTrib: TFloatField;
    QrAverTotTrib: TWideStringField;
    QrAtpAliqTotTrib: TSmallintField;
    QrACTeMyCST: TSmallintField;
    QrAinfCarga_proPred: TWideStringField;
    QrAinfCteSub_chCte: TWideStringField;
    QrAinfCteSub_refNFe: TWideStringField;
    QrAinfCteSub_CNPJ: TWideStringField;
    QrAinfCteSub_CPF: TWideStringField;
    QrAinfCteSub_mod: TWideStringField;
    QrAinfCteSub_serie: TIntegerField;
    QrAinfCteSub_subserie: TIntegerField;
    QrAinfCteSub_nro: TIntegerField;
    QrAinfCteSub_valor: TFloatField;
    QrAinfCteSub_dEmi: TDateField;
    QrAinfCteSub_refCTe: TWideStringField;
    QrAinfCteSub_refCTeAnu: TWideStringField;
    QrAStatus: TIntegerField;
    QrAide_xDetRetira: TWideStringField;
    QrAimp_vTotTrib: TFloatField;
    QrAimp_infAdFisco: TWideMemoField;
    QrAinfCteSub_ContribICMS: TSmallintField;
    QrAinfCteSub_TipoDoc: TSmallintField;
    QrACodInfoEmite: TIntegerField;
    QrACodInfoTomad: TIntegerField;
    QrAID_TXT: TWideStringField;
    QrAEMIT_ENDERECO: TWideStringField;
    QrAEMIT_FONE_TXT: TWideStringField;
    QrAEMIT_IE_TXT: TWideStringField;
    QrAEMIT_CNPJ_TXT: TWideStringField;
    QrADEST_CNPJ_CPF_TXT: TWideStringField;
    QrADEST_ENDERECO: TWideStringField;
    QrADEST_CEP_TXT: TWideStringField;
    QrADEST_FONE_TXT: TWideStringField;
    QrADEST_IE_TXT: TWideStringField;
    QrADEST_XMUN_TXT: TWideStringField;
    QrAEXPED_CNPJ_CPF_TXT: TWideStringField;
    QrAEXPED_ENDERECO: TWideStringField;
    QrAEXPED_CEP_TXT: TWideStringField;
    QrAEXPED_FONE_TXT: TWideStringField;
    QrAEXPED_IE_TXT: TWideStringField;
    QrAEXPED_XMUN_TXT: TWideStringField;
    QrARECEB_CNPJ_CPF_TXT: TWideStringField;
    QrARECEB_ENDERECO: TWideStringField;
    QrARECEB_CEP_TXT: TWideStringField;
    QrARECEB_FONE_TXT: TWideStringField;
    QrARECEB_IE_TXT: TWideStringField;
    QrARECEB_XMUN_TXT: TWideStringField;
    QrATOMAD_CNPJ_CPF_TXT: TWideStringField;
    QrATOMAD_ENDERECO: TWideStringField;
    QrATOMAD_CEP_TXT: TWideStringField;
    QrATOMAD_FONE_TXT: TWideStringField;
    QrATOMAD_IE_TXT: TWideStringField;
    QrATOMAD_XMUN_TXT: TWideStringField;
    QrAREMET_CNPJ_CPF_TXT: TWideStringField;
    QrAREMET_ENDERECO: TWideStringField;
    QrAREMET_CEP_TXT: TWideStringField;
    QrAREMET_FONE_TXT: TWideStringField;
    QrAREMET_IE_TXT: TWideStringField;
    QrAREMET_XMUN_TXT: TWideStringField;
    Qr1Receb: TmySQLQuery;
    Qr1RecebFatID: TIntegerField;
    Qr1RecebFatNum: TIntegerField;
    Qr1RecebEmpresa: TIntegerField;
    Qr1RecebControle: TIntegerField;
    Qr1RecebCNPJ: TWideStringField;
    Qr1RecebCPF: TWideStringField;
    Qr1RecebIE: TWideStringField;
    Qr1RecebxNome: TWideStringField;
    Qr1Recebfone: TWideStringField;
    Qr1RecebxLgr: TWideStringField;
    Qr1Recebnro: TWideStringField;
    Qr1RecebxCpl: TWideStringField;
    Qr1RecebxBairro: TWideStringField;
    Qr1RecebcMun: TIntegerField;
    Qr1RecebxMun: TWideStringField;
    Qr1RecebCEP: TIntegerField;
    Qr1RecebUF: TWideStringField;
    Qr1RecebcPais: TIntegerField;
    Qr1RecebxPais: TWideStringField;
    Qr1Recebemail: TWideStringField;
    Qr1RecebLk: TIntegerField;
    Qr1RecebDataCad: TDateField;
    Qr1RecebDataAlt: TDateField;
    Qr1RecebUserCad: TIntegerField;
    Qr1RecebUserAlt: TIntegerField;
    Qr1RecebAlterWeb: TSmallintField;
    Qr1RecebAtivo: TSmallintField;
    Qr1Exped: TmySQLQuery;
    Qr1ExpedFatID: TIntegerField;
    Qr1ExpedFatNum: TIntegerField;
    Qr1ExpedEmpresa: TIntegerField;
    Qr1ExpedControle: TIntegerField;
    Qr1ExpedCNPJ: TWideStringField;
    Qr1ExpedCPF: TWideStringField;
    Qr1ExpedIE: TWideStringField;
    Qr1ExpedxNome: TWideStringField;
    Qr1Expedfone: TWideStringField;
    Qr1ExpedxLgr: TWideStringField;
    Qr1Expednro: TWideStringField;
    Qr1ExpedxCpl: TWideStringField;
    Qr1ExpedxBairro: TWideStringField;
    Qr1ExpedcMun: TIntegerField;
    Qr1ExpedxMun: TWideStringField;
    Qr1ExpedCEP: TIntegerField;
    Qr1ExpedUF: TWideStringField;
    Qr1ExpedcPais: TIntegerField;
    Qr1ExpedxPais: TWideStringField;
    Qr1Expedemail: TWideStringField;
    Qr1ExpedLk: TIntegerField;
    Qr1ExpedDataCad: TDateField;
    Qr1ExpedDataAlt: TDateField;
    Qr1ExpedUserCad: TIntegerField;
    Qr1ExpedUserAlt: TIntegerField;
    Qr1ExpedAlterWeb: TSmallintField;
    Qr1ExpedAtivo: TSmallintField;
    QrATOMA4_CNPJ_CPF_TXT: TWideStringField;
    QrATOMA4_ENDERECO: TWideStringField;
    QrATOMA4_CEP_TXT: TWideStringField;
    QrATOMA4_FONE_TXT: TWideStringField;
    QrATOMA4_IE_TXT: TWideStringField;
    QrATOMA4_XMUN_TXT: TWideStringField;
    Qr2Toma4: TmySQLQuery;
    Qr2Toma4FatID: TIntegerField;
    Qr2Toma4FatNum: TIntegerField;
    Qr2Toma4Empresa: TIntegerField;
    Qr2Toma4Controle: TIntegerField;
    Qr2Toma4enderToma_xLgr: TWideStringField;
    Qr2Toma4enderToma_nro: TWideStringField;
    Qr2Toma4enderToma_xCpl: TWideStringField;
    Qr2Toma4enderToma_xBairro: TWideStringField;
    Qr2Toma4enderToma_cMun: TIntegerField;
    Qr2Toma4enderToma_xMun: TWideStringField;
    Qr2Toma4enderToma_CEP: TIntegerField;
    Qr2Toma4enderToma_UF: TWideStringField;
    Qr2Toma4enderToma_cPais: TIntegerField;
    Qr2Toma4enderToma_xPais: TWideStringField;
    Qr2Toma4enderToma_email: TWideStringField;
    Qr2Toma4Lk: TIntegerField;
    Qr2Toma4DataCad: TDateField;
    Qr2Toma4DataAlt: TDateField;
    Qr2Toma4UserCad: TIntegerField;
    Qr2Toma4UserAlt: TIntegerField;
    Qr2Toma4AlterWeb: TSmallintField;
    Qr2Toma4Ativo: TSmallintField;
    QrAREMET_NOME: TWideStringField;
    QrADEST_NOME: TWideStringField;
    QrAEXPED_NOME: TWideStringField;
    QrARECEB_NOME: TWideStringField;
    QrATOMAD_NOME: TWideStringField;
    QrATOMA4_NOME: TWideStringField;
    QrADEST_XPAIS_TXT: TWideStringField;
    QrAEXPED_XPAIS_TXT: TWideStringField;
    QrARECEB_XPAIS_TXT: TWideStringField;
    QrATOMAD_XPAIS_TXT: TWideStringField;
    QrATOMA4_XPAIS_TXT: TWideStringField;
    QrAREMET_XPAIS_TXT: TWideStringField;
    QrADOC_SEM_VLR_FIS: TWideStringField;
    QrADOC_SEM_VLR_JUR: TWideStringField;
    QrAEXPED_UF: TWideStringField;
    QrARECEB_UF: TWideStringField;
    Qr3InfQ: TmySQLQuery;
    Qr3InfQFatID: TIntegerField;
    Qr3InfQFatNum: TIntegerField;
    Qr3InfQEmpresa: TIntegerField;
    Qr3InfQControle: TIntegerField;
    Qr3InfQcUnid: TSmallintField;
    Qr3InfQtpMed: TWideStringField;
    Qr3InfQqCarga: TFloatField;
    Qr3InfQLk: TIntegerField;
    Qr3InfQDataCad: TDateField;
    Qr3InfQDataAlt: TDateField;
    Qr3InfQUserCad: TIntegerField;
    Qr3InfQUserAlt: TIntegerField;
    Qr3InfQAlterWeb: TSmallintField;
    Qr3InfQAtivo: TSmallintField;
    Qr2Seg: TmySQLQuery;
    Qr2SegFatID: TIntegerField;
    Qr2SegFatNum: TIntegerField;
    Qr2SegEmpresa: TIntegerField;
    Qr2SegControle: TIntegerField;
    Qr2SegrespSeg: TSmallintField;
    Qr2SegxSeg: TWideStringField;
    Qr2SegnApol: TWideStringField;
    Qr2SegnAver: TWideStringField;
    Qr2SegvCarga: TFloatField;
    Qr2Comp: TmySQLQuery;
    Qr2CompFatID: TIntegerField;
    Qr2CompFatNum: TIntegerField;
    Qr2CompEmpresa: TIntegerField;
    Qr2CompControle: TIntegerField;
    Qr2CompxNome: TWideStringField;
    Qr2CompvComp: TFloatField;
    Qr2CompLk: TIntegerField;
    Qr2CompDataCad: TDateField;
    Qr2CompDataAlt: TDateField;
    Qr2CompUserCad: TIntegerField;
    Qr2CompUserAlt: TIntegerField;
    Qr2CompAlterWeb: TSmallintField;
    Qr2CompAtivo: TSmallintField;
    Qr2InfDoc: TmySQLQuery;
    Qr2InfDocFatID: TIntegerField;
    Qr2InfDocFatNum: TIntegerField;
    Qr2InfDocEmpresa: TIntegerField;
    Qr2InfDocControle: TIntegerField;
    Qr2InfDocMyTpDocInf: TSmallintField;
    Qr2InfDocInfNFe_chave: TWideStringField;
    Qr2InfDocInfNFe_PIN: TIntegerField;
    Qr2InfDocInfNFe_dPrev: TDateField;
    Qr2InfDocInfNF_nRoma: TWideStringField;
    Qr2InfDocInfNF_nPed: TWideStringField;
    Qr2InfDocInfNF_mod: TSmallintField;
    Qr2InfDocInfNF_serie: TWideStringField;
    Qr2InfDocInfNF_nDoc: TWideStringField;
    Qr2InfDocInfNF_dEmi: TDateField;
    Qr2InfDocInfNF_vBC: TFloatField;
    Qr2InfDocInfNF_vICMS: TFloatField;
    Qr2InfDocInfNF_vBCST: TFloatField;
    Qr2InfDocInfNF_vST: TFloatField;
    Qr2InfDocInfNF_vProd: TFloatField;
    Qr2InfDocInfNF_vNF: TFloatField;
    Qr2InfDocInfNF_nCFOP: TIntegerField;
    Qr2InfDocInfNF_nPeso: TFloatField;
    Qr2InfDocInfNF_PIN: TIntegerField;
    Qr2InfDocInfNF_dPrev: TDateField;
    Qr2InfDocInfOutros_tpDoc: TSmallintField;
    Qr2InfDocInfOutros_descOutros: TWideStringField;
    Qr2InfDocInfOutros_nDoc: TWideStringField;
    Qr2InfDocInfOutros_dEmi: TDateField;
    Qr2InfDocInfOutros_vDocFisc: TFloatField;
    Qr2InfDocInfOutros_dPrev: TDateField;
    Qr2InfDocLk: TIntegerField;
    Qr2InfDocDataCad: TDateField;
    Qr2InfDocDataAlt: TDateField;
    Qr2InfDocUserCad: TIntegerField;
    Qr2InfDocUserAlt: TIntegerField;
    Qr2InfDocAlterWeb: TSmallintField;
    Qr2InfDocAtivo: TSmallintField;
    QrATOMAD_UF: TWideStringField;
    QrATOMA4_UF: TWideStringField;
    QrRodoVeic: TmySQLQuery;
    QrRodoProp: TmySQLQuery;
    QrRodoVeicFatID: TIntegerField;
    QrRodoVeicFatNum: TIntegerField;
    QrRodoVeicEmpresa: TIntegerField;
    QrRodoVeicControle: TIntegerField;
    QrRodoVeicConta: TIntegerField;
    QrRodoVeiccInt: TIntegerField;
    QrRodoVeicRENAVAM: TWideStringField;
    QrRodoVeicplaca: TWideStringField;
    QrRodoVeictara: TIntegerField;
    QrRodoVeiccapKG: TIntegerField;
    QrRodoVeiccapM3: TIntegerField;
    QrRodoVeictpProp: TWideStringField;
    QrRodoVeictpVeic: TSmallintField;
    QrRodoVeictpRod: TSmallintField;
    QrRodoVeictpCar: TSmallintField;
    QrRodoVeicUF: TWideStringField;
    QrRodoVeicLk: TIntegerField;
    QrRodoVeicDataCad: TDateField;
    QrRodoVeicDataAlt: TDateField;
    QrRodoVeicUserCad: TIntegerField;
    QrRodoVeicUserAlt: TIntegerField;
    QrRodoVeicAlterWeb: TSmallintField;
    QrRodoVeicAtivo: TSmallintField;
    QrRodoPropFatID: TIntegerField;
    QrRodoPropFatNum: TIntegerField;
    QrRodoPropEmpresa: TIntegerField;
    QrRodoPropControle: TIntegerField;
    QrRodoPropConta: TIntegerField;
    QrRodoPropCPF: TWideStringField;
    QrRodoPropCNPJ: TWideStringField;
    QrRodoPropxNome: TWideStringField;
    QrRodoPropIE: TWideStringField;
    QrRodoPropUF: TWideStringField;
    QrRodoProptpProp: TSmallintField;
    QrRodoPropLk: TIntegerField;
    QrRodoPropDataCad: TDateField;
    QrRodoPropDataAlt: TDateField;
    QrRodoPropUserCad: TIntegerField;
    QrRodoPropUserAlt: TIntegerField;
    QrRodoPropAlterWeb: TSmallintField;
    QrRodoPropAtivo: TSmallintField;
    QrRodoPropRNTRC: TWideStringField;
    QrRodoLacr: TmySQLQuery;
    QrRodoCab: TmySQLQuery;
    QrRodoMoto: TmySQLQuery;
    QrRodoCabFatID: TIntegerField;
    QrRodoCabFatNum: TIntegerField;
    QrRodoCabEmpresa: TIntegerField;
    QrRodoCabControle: TIntegerField;
    QrRodoCabdPrev: TDateField;
    QrRodoCablota: TSmallintField;
    QrRodoCabCIOT: TLargeintField;
    QrRodoCabLk: TIntegerField;
    QrRodoCabDataCad: TDateField;
    QrRodoCabDataAlt: TDateField;
    QrRodoCabUserCad: TIntegerField;
    QrRodoCabUserAlt: TIntegerField;
    QrRodoCabAlterWeb: TSmallintField;
    QrRodoCabAtivo: TSmallintField;
    QrRodoCabRNTRC: TWideStringField;
    QrRodoLacrFatID: TIntegerField;
    QrRodoLacrFatNum: TIntegerField;
    QrRodoLacrEmpresa: TIntegerField;
    QrRodoLacrControle: TIntegerField;
    QrRodoLacrConta: TIntegerField;
    QrRodoLacrnLacre: TWideStringField;
    QrRodoLacrLk: TIntegerField;
    QrRodoLacrDataCad: TDateField;
    QrRodoLacrDataAlt: TDateField;
    QrRodoLacrUserCad: TIntegerField;
    QrRodoLacrUserAlt: TIntegerField;
    QrRodoLacrAlterWeb: TSmallintField;
    QrRodoLacrAtivo: TSmallintField;
    QrRodoMotoFatID: TIntegerField;
    QrRodoMotoFatNum: TIntegerField;
    QrRodoMotoEmpresa: TIntegerField;
    QrRodoMotoControle: TIntegerField;
    QrRodoMotoConta: TIntegerField;
    QrRodoMotoxNome: TWideStringField;
    QrRodoMotoCPF: TWideStringField;
    QrRodoMotoLk: TIntegerField;
    QrRodoMotoDataCad: TDateField;
    QrRodoMotoDataAlt: TDateField;
    QrRodoMotoUserCad: TIntegerField;
    QrRodoMotoUserAlt: TIntegerField;
    QrRodoMotoAlterWeb: TSmallintField;
    QrRodoMotoAtivo: TSmallintField;
    QrRodoVPed: TmySQLQuery;
    QrRodoVPedFatID: TIntegerField;
    QrRodoVPedFatNum: TIntegerField;
    QrRodoVPedEmpresa: TIntegerField;
    QrRodoVPedControle: TIntegerField;
    QrRodoVPedConta: TIntegerField;
    QrRodoVPedCNPJForn: TWideStringField;
    QrRodoVPednCompra: TWideStringField;
    QrRodoVPedCNPJPg: TWideStringField;
    QrRodoVPedvValePed: TFloatField;
    QrRodoVPedLk: TIntegerField;
    QrRodoVPedDataCad: TDateField;
    QrRodoVPedDataAlt: TDateField;
    QrRodoVPedUserCad: TIntegerField;
    QrRodoVPedUserAlt: TIntegerField;
    QrRodoVPedAlterWeb: TSmallintField;
    QrRodoVPedAtivo: TSmallintField;
    Qr2ObsCont: TmySQLQuery;
    Qr2ObsContFatID: TIntegerField;
    Qr2ObsContFatNum: TIntegerField;
    Qr2ObsContEmpresa: TIntegerField;
    Qr2ObsContControle: TIntegerField;
    Qr2ObsContxCampo: TWideStringField;
    Qr2ObsContxTexto: TWideStringField;
    Qr2ObsContLk: TIntegerField;
    Qr2ObsContDataCad: TDateField;
    Qr2ObsContDataAlt: TDateField;
    Qr2ObsContUserCad: TIntegerField;
    Qr2ObsContUserAlt: TIntegerField;
    Qr2ObsContAlterWeb: TSmallintField;
    Qr2ObsContAtivo: TSmallintField;
    Qr2ObsFisco: TmySQLQuery;
    Qr2ObsFiscoFatID: TIntegerField;
    Qr2ObsFiscoFatNum: TIntegerField;
    Qr2ObsFiscoEmpresa: TIntegerField;
    Qr2ObsFiscoControle: TIntegerField;
    Qr2ObsFiscoxCampo: TWideStringField;
    Qr2ObsFiscoxTexto: TWideStringField;
    Qr2ObsFiscoLk: TIntegerField;
    Qr2ObsFiscoDataCad: TDateField;
    Qr2ObsFiscoDataAlt: TDateField;
    Qr2ObsFiscoUserCad: TIntegerField;
    Qr2ObsFiscoUserAlt: TIntegerField;
    Qr2ObsFiscoAlterWeb: TSmallintField;
    Qr2ObsFiscoAtivo: TSmallintField;
    QrOpcoesCTe: TmySQLQuery;
    QrOpcoesCTeCTeversao: TFloatField;
    QrOpcoesCTeCTeide_mod: TSmallintField;
    QrOpcoesCTeSimplesFed: TSmallintField;
    QrOpcoesCTeDirCTeGer: TWideStringField;
    QrOpcoesCTeDirCTeAss: TWideStringField;
    QrOpcoesCTeDirCTeEnvLot: TWideStringField;
    QrOpcoesCTeDirCTeRec: TWideStringField;
    QrOpcoesCTeDirCTePedRec: TWideStringField;
    QrOpcoesCTeDirCTeProRec: TWideStringField;
    QrOpcoesCTeDirCTeDen: TWideStringField;
    QrOpcoesCTeDirCTePedCan: TWideStringField;
    QrOpcoesCTeDirCTeCan: TWideStringField;
    QrOpcoesCTeDirCTePedInu: TWideStringField;
    QrOpcoesCTeDirCTeInu: TWideStringField;
    QrOpcoesCTeDirCTePedSit: TWideStringField;
    QrOpcoesCTeDirCTeSit: TWideStringField;
    QrOpcoesCTeDirCTePedSta: TWideStringField;
    QrOpcoesCTeDirCTeSta: TWideStringField;
    QrOpcoesCTeCTeUF_WebServ: TWideStringField;
    QrOpcoesCTeCTeUF_Servico: TWideStringField;
    QrOpcoesCTePathLogoCTe: TWideStringField;
    QrOpcoesCTeCtaFretPrest: TIntegerField;
    QrOpcoesCTeTxtFretPrest: TWideStringField;
    QrOpcoesCTeDupFretPrest: TWideStringField;
    QrOpcoesCTeCTeSerNum: TWideStringField;
    QrOpcoesCTeCTeSerVal: TDateField;
    QrOpcoesCTeCTeSerAvi: TSmallintField;
    QrOpcoesCTeDirDACTes: TWideStringField;
    QrOpcoesCTeDirCTeProt: TWideStringField;
    QrOpcoesCTeCTePreMailAut: TIntegerField;
    QrOpcoesCTeCTePreMailCan: TIntegerField;
    QrOpcoesCTeCTePreMailEveCCe: TIntegerField;
    QrOpcoesCTeCTeVerStaSer: TFloatField;
    QrOpcoesCTeCTeVerEnvLot: TFloatField;
    QrOpcoesCTeCTeVerConLot: TFloatField;
    QrOpcoesCTeCTeVerCanCTe: TFloatField;
    QrOpcoesCTeCTeVerInuNum: TFloatField;
    QrOpcoesCTeCTeVerConCTe: TFloatField;
    QrOpcoesCTeCTeVerLotEve: TFloatField;
    QrOpcoesCTeCTeide_tpImp: TSmallintField;
    QrOpcoesCTeCTeide_tpAmb: TSmallintField;
    QrOpcoesCTeCTeAppCode: TSmallintField;
    QrOpcoesCTeCTeAssDigMode: TSmallintField;
    QrOpcoesCTeCTeEntiTipCto: TIntegerField;
    QrOpcoesCTeCTeEntiTipCt1: TIntegerField;
    QrOpcoesCTeMyEmailCTe: TWideStringField;
    QrOpcoesCTeDirCTeSchema: TWideStringField;
    QrOpcoesCTeDirCTeRWeb: TWideStringField;
    QrOpcoesCTeDirCTeEveEnvLot: TWideStringField;
    QrOpcoesCTeDirCTeEveRetLot: TWideStringField;
    QrOpcoesCTeDirCTeEvePedCCe: TWideStringField;
    QrOpcoesCTeDirCTeEveRetCCe: TWideStringField;
    QrOpcoesCTeDirCTeEveProcCCe: TWideStringField;
    QrOpcoesCTeDirCTeEvePedCan: TWideStringField;
    QrOpcoesCTeDirCTeEveRetCan: TWideStringField;
    QrOpcoesCTeDirCTeRetCTeDes: TWideStringField;
    QrOpcoesCTeCTeCTeVerConDes: TFloatField;
    QrOpcoesCTeDirCTeEvePedMDe: TWideStringField;
    QrOpcoesCTeDirCTeEveRetMDe: TWideStringField;
    QrOpcoesCTeDirDowCTeDes: TWideStringField;
    QrOpcoesCTeDirDowCTeCTe: TWideStringField;
    QrOpcoesCTeCTeInfCpl: TIntegerField;
    QrOpcoesCTeCTeVerConsCad: TFloatField;
    QrOpcoesCTeCTeVerDistDFeInt: TFloatField;
    QrOpcoesCTeCTeVerDowCTe: TFloatField;
    QrOpcoesCTeDirDowCTeCnf: TWideStringField;
    QrOpcoesCTeCTeItsLin: TSmallintField;
    QrOpcoesCTeCTeFTRazao: TSmallintField;
    QrOpcoesCTeCTeFTEnder: TSmallintField;
    QrOpcoesCTeCTeFTFones: TSmallintField;
    QrOpcoesCTeCTeMaiusc: TSmallintField;
    QrOpcoesCTeCTeShowURL: TWideStringField;
    QrOpcoesCTeCTetpEmis: TSmallintField;
    QrOpcoesCTeCTeSCAN_Ser: TIntegerField;
    QrOpcoesCTeCTeSCAN_nCT: TIntegerField;
    QrOpcoesCTeCTe_indFinalCpl: TSmallintField;
    QrOpcoesCTeTZD_UTC_Auto_TermoAceite: TIntegerField;
    QrOpcoesCTeTZD_UTC_Auto_DataHoraAceite: TDateTimeField;
    QrOpcoesCTeTZD_UTC_Auto_UsuarioAceite: TIntegerField;
    QrOpcoesCTeTZD_UTC_Auto: TSmallintField;
    QrOpcoesCTeTZD_UTC: TFloatField;
    QrOpcoesCTehVeraoAsk: TDateField;
    QrOpcoesCTehVeraoIni: TDateField;
    QrOpcoesCTehVeraoFim: TDateField;
    QrOpcoesCTeParamsCTe: TIntegerField;
    QrOpcoesCTeCodigo: TIntegerField;
    frxA4A_001RodoFra: TfrxReport;
    frxA4A_001RodoLot: TfrxReport;
    QrCTeCabAide_Modal: TSmallintField;
    QrAinfCCe_CCeConta: TIntegerField;
    QrAinfProt_dhEvento: TDateTimeField;
    QrAinfProt_dhRegEvento: TDateTimeField;
    QrAinfCanc_dhEvento: TDateTimeField;
    QrAinfCanc_dhRegEvento: TDateTimeField;
    QrAeveEPEC_Id: TWideStringField;
    QrAeveEPEC_tpAmb: TSmallintField;
    QrAeveEPEC_verAplic: TWideStringField;
    QrAeveEPEC_cOrgao: TSmallintField;
    QrAeveEPEC_cStat: TIntegerField;
    QrAeveEPEC_xMotivo: TWideStringField;
    QrAeveEPEC_chCTe: TWideStringField;
    QrAeveEPEC_tpEvento: TIntegerField;
    QrAeveEPEC_xEvento: TWideStringField;
    QrAeveEPEC_nSeqEvento: TSmallintField;
    QrAeveEPEC_dhRegEvento: TDateTimeField;
    QrAeveEPEC_nProt: TWideStringField;
    QrAeveEPEC_versaoEnv: TFloatField;
    QrAeveEPEC_CNPJ: TWideStringField;
    QrAeveEPEC_dhEvento: TDateTimeField;
    QrAeveEPEC_versaoEvento: TFloatField;
    QrAeveEPEC_cJust: TIntegerField;
    QrAeveEPEC_xJust: TWideStringField;
    QrAeveEPEC_vICMS: TFloatField;
    QrAeveEPEC_vTPrest: TFloatField;
    QrAeveEPEC_vCarga: TFloatField;
    QrAeveEPEC_versaoRet: TFloatField;
    QrAeveEPEC_CTeEveREPECConta: TIntegerField;
    QrAeveEPEC_CodInfoTomad: TIntegerField;
    QrAeveEPEC_Toma04_toma: TSmallintField;
    QrAeveEPEC_Toma04_UF: TWideStringField;
    QrAeveEPEC_Toma04_CNPJ: TWideStringField;
    QrAeveEPEC_Toma04_CPF: TWideStringField;
    QrAeveEPEC_Toma04_IE: TWideStringField;
    QrAeveEPEC_Modal: TSmallintField;
    QrAeveEPEC_UFIni: TWideStringField;
    QrAeveEPEC_UFFim: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdFilialExit(Sender: TObject);
    procedure EdFilialChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdClienteExit(Sender: TObject);
    procedure TPDataIChange(Sender: TObject);
    procedure TPDataIClick(Sender: TObject);
    procedure Cancelamento1Click(Sender: TObject);
    procedure QrClientesBeforeClose(DataSet: TDataSet);
    procedure BtLeArqClick(Sender: TObject);
    procedure dmkDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtConsultaClick(Sender: TObject);
    procedure BtInutilizaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure CampospreenchidosnoXMLdaCTe1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrACalcFields(DataSet: TDataSet);
    procedure QrCTeCabAAfterOpen(DataSet: TDataSet);
    procedure QrCTeCabAAfterScroll(DataSet: TDataSet);
    procedure QrCTeCabABeforeClose(DataSet: TDataSet);
    procedure QrIAfterScroll(DataSet: TDataSet);
    procedure QrYAfterOpen(DataSet: TDataSet);
    procedure QrCTeYItsCalcFields(DataSet: TDataSet);
    procedure QrXVolAfterOpen(DataSet: TDataSet);
    procedure BtArqClick(Sender: TObject);
    procedure PMArqPopup(Sender: TObject);
    procedure CTe1Click(Sender: TObject);
    procedure Autorizao1Click(Sender: TObject);
    procedure Cancelamento2Click(Sender: TObject);
    procedure BtEnviaClick(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure CkEmitClick(Sender: TObject);
    procedure RGAmbienteClick(Sender: TObject);
    procedure DiretriodoarquivoXML1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure Listadasnotaspesquisadas1Click(Sender: TObject);
    procedure QrCTeCabACalcFields(DataSet: TDataSet);
    procedure QrCTe_XXXCalcFields(DataSet: TDataSet);
    procedure frxListaCTesGetValue(const VarName: string; var Value: Variant);
    procedure QrCTe_101CalcFields(DataSet: TDataSet);
    procedure PreviewdaCTe1Click(Sender: TObject);
    procedure BtEventoClick(Sender: TObject);
    procedure QrCTeEveRRetCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure dmkDBGrid1DblClick(Sender: TObject);
    procedure ImprimeSomenteadmin1Click(Sender: TObject);
    procedure porCFOP1Click(Sender: TObject);
    procedure porNatOp1Click(Sender: TObject);
    procedure ExportaporNatOptotaisCTe1Click(Sender: TObject);
    procedure ExportaporCFOPtotaiseitensCTe1Click(Sender: TObject);
    procedure frxA4A_001RodoLotGetValue(const VarName: string; var Value: Variant);
    procedure QrRodoVeicAfterScroll(DataSet: TDataSet);
    procedure QrRodoVeicBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    // F N F e YIts: String;
    FLote, FEmpresa, FIDCtrl: Integer;
    VARF_SEG_NOME_01, VARF_SEG_NOME_02,
    VARF_SEG_RESP_01, VARF_SEG_RESP_02,
    VARF_SEG_APOL_01, VARF_SEG_APOL_02,
    VARF_SEG_AVER_01, VARF_SEG_AVER_02,
    VARF_UMED_QTDE_01, VARF_UMED_QTDE_02, VARF_UMED_QTDE_03, VARF_UMED_QTDE_04,
    VARF_UMED_QTDE_05, VARF_UMED_QTDE_06, VARF_UMED_QTDE_07, VARF_UMED_QTDE_08,
    VARF_CMPVL_NOM_01, VARF_CMPVL_VLR_01, VARF_CMPVL_NOM_02, VARF_CMPVL_VLR_02,
    VARF_CMPVL_NOM_03, VARF_CMPVL_VLR_03, VARF_CMPVL_NOM_04, VARF_CMPVL_VLR_04,
    VARF_CMPVL_NOM_05, VARF_CMPVL_VLR_05, VARF_CMPVL_NOM_06, VARF_CMPVL_VLR_06,
    VARF_CMPVL_NOM_07, VARF_CMPVL_VLR_07, VARF_CMPVL_NOM_08, VARF_CMPVL_VLR_08,
    VARF_CMPVL_NOM_09, VARF_CMPVL_VLR_09, VARF_CMPVL_NOM_10, VARF_CMPVL_VLR_10,
    VARF_CMPVL_NOM_11, VARF_CMPVL_VLR_11, VARF_CMPVL_NOM_12, VARF_CMPVL_VLR_12,
    VARF_CMPVL_NOM_13, VARF_CMPVL_VLR_13, VARF_CMPVL_NOM_14, VARF_CMPVL_VLR_14,
    VARF_CMPVL_NOM_15, VARF_CMPVL_VLR_15, VARF_CMPVL_NOM_16, VARF_CMPVL_VLR_16,
    VARF_DOCUM_TIPO_01, VARF_DOCUM_DOCU_01, VARF_DOCUM_SRNR_01,
    VARF_DOCUM_TIPO_02, VARF_DOCUM_DOCU_02, VARF_DOCUM_SRNR_02,
    VARF_DOCUM_TIPO_03, VARF_DOCUM_DOCU_03, VARF_DOCUM_SRNR_03,
    VARF_DOCUM_TIPO_04, VARF_DOCUM_DOCU_04, VARF_DOCUM_SRNR_04,
    VARF_DOCUM_TIPO_05, VARF_DOCUM_DOCU_05, VARF_DOCUM_SRNR_05,
    VARF_DOCUM_TIPO_06, VARF_DOCUM_DOCU_06, VARF_DOCUM_SRNR_06,
    VARF_DOCUM_TIPO_07, VARF_DOCUM_DOCU_07, VARF_DOCUM_SRNR_07,
    VARF_DOCUM_TIPO_08, VARF_DOCUM_DOCU_08, VARF_DOCUM_SRNR_08,
    VARF_DOCUM_TIPO_09, VARF_DOCUM_DOCU_09, VARF_DOCUM_SRNR_09,
    VARF_DOCUM_TIPO_10, VARF_DOCUM_DOCU_10, VARF_DOCUM_SRNR_10,
    VARF_DOCUM_TIPO_11, VARF_DOCUM_DOCU_11, VARF_DOCUM_SRNR_11,
    VARF_DOCUM_TIPO_12, VARF_DOCUM_DOCU_12, VARF_DOCUM_SRNR_12,
    VARF_DOCUM_TIPO_13, VARF_DOCUM_DOCU_13, VARF_DOCUM_SRNR_13,
    VARF_DOCUM_TIPO_14, VARF_DOCUM_DOCU_14, VARF_DOCUM_SRNR_14,
    VARF_DOCUM_TIPO_15, VARF_DOCUM_DOCU_15, VARF_DOCUM_SRNR_15,
    VARF_DOCUM_TIPO_16, VARF_DOCUM_DOCU_16, VARF_DOCUM_SRNR_16,
    VARF_DOCUM_TIPO_17, VARF_DOCUM_DOCU_17, VARF_DOCUM_SRNR_17,
    VARF_DOCUM_TIPO_18, VARF_DOCUM_DOCU_18, VARF_DOCUM_SRNR_18,
    VARF_DOCUM_TIPO_19, VARF_DOCUM_DOCU_19, VARF_DOCUM_SRNR_19,
    VARF_DOCUM_TIPO_20, VARF_DOCUM_DOCU_20, VARF_DOCUM_SRNR_20,
    VARF_TPVEIC_TIPO_01, VARF_TPVEIC_PLACA_01, VARF_TPVEIC_UF_01, VARF_TPVEIC_RNTRC_01,
    VARF_TPVEIC_TIPO_02, VARF_TPVEIC_PLACA_02, VARF_TPVEIC_UF_02, VARF_TPVEIC_RNTRC_02,
    VARF_TPVEIC_TIPO_03, VARF_TPVEIC_PLACA_03, VARF_TPVEIC_UF_03, VARF_TPVEIC_RNTRC_03,
    VARF_TPVEIC_TIPO_04, VARF_TPVEIC_PLACA_04, VARF_TPVEIC_UF_04, VARF_TPVEIC_RNTRC_04,
    VARF_VPEDAG_CNPJR_01, VARF_VPEDAG_CNPJF_01, VARF_VPEDAG_NCOMP_01,
    VARF_VPEDAG_CNPJR_02, VARF_VPEDAG_CNPJF_02, VARF_VPEDAG_NCOMP_02,
    VARF_VPEDAG_CNPJR_03, VARF_VPEDAG_CNPJF_03, VARF_VPEDAG_NCOMP_03,
    VARF_VPEDAG_CNPJR_04, VARF_VPEDAG_CNPJF_04, VARF_VPEDAG_NCOMP_04,
    VARF_LACRODO_LACRES, VARF_OBS_CONT, VARF_OBS_FISCO,
    VARF_MOTO_NOME_01,VARF_MOTO_NOME_02, VARF_MOTO_CPF_01, VARF_MOTO_CPF_02,
    FChaveCTe, FProtocolo: String;
    // CabY
    FFaturas: array[0..14] of array[0..2] of String;
    // CabXVol
    Fesp, Fmarca, FnVol: String;
    FpesoL, FpesoB, FqVol: Double;
    // Impress�o de lista de CTes
    F_CTe_100, F_CTe_101, F_CTe_XXX: String;
    //
    FSo_O_ID: Boolean;
    //
    procedure DefineFrx(var Report: TfrxReport; const OQueFaz: TfrxImpComo);
    procedure DefineVarsDePreparacao();
    procedure ImprimeCTe(Preview: Boolean);
    procedure ImprimeListaNova(PorCFOP, Exporta: Boolean);
    procedure ObtemdadosInfDoc(var Tipo, Docu, SrNr: String);
  public
    { Public declarations }
    FDACTEImpresso, FMailEnviado: Boolean;
    //
    procedure FechaCTeCabA();
    procedure ReopenCTeCabA(IDCtrl: Integer; So_O_ID: Boolean);
    procedure ReopenCTeCabAMsg();
    procedure ReopenCTeEveRRet();
    procedure ReopenCTeIt3InfQ();
    procedure ReopenCTeIt2Seg();
    procedure ReopenCTeIt2Comp();
    procedure ReopenCTeIt2InfDoc();
    procedure ReopenCTeMoRodoCab();
    procedure ReopenCTeMoRodoVeic();
    procedure ReopenCTeMoRodoProp();
    procedure ReopenCTeMoRodoMoto();
    procedure ReopenCTeMoRodoLacr();
    procedure ReopenCTeMoRodoVPed();
    procedure ReopenCTeIt2ObsCont();
    procedure ReopenCTeIt2ObsFisco();
    //
    function  DefineQual_frxCTe(OQueFaz: TfrxImpComo): TfrxReport;
  end;

  var
  FmCTe_Pesq_0000: TFmCTe_Pesq_0000;

implementation

uses UnMyObjects, ModuleGeral, UnInternalConsts, MyDBCheck, UCreate,
  (*CTeInut_0000a,*) UMySQLModule, Module, ModuleCTe_0000, CTeSteps_0200a,
  CTeEveRCab, MeuFrx, DmkDAC_PF, UnMailEnv,
  MyGlyfs, Principal, UnCTe_PF, UnXXe_PF;

{$R *.DFM}

procedure TFmCTe_Pesq_0000.BtConsultaClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  //
  DefineVarsDePreparacao();
  CTe_PF.MostraFormStepsCTe_ConsultaCTe(FEmpresa, FIDCtrl, FChaveCTe, nil);
  //
  ReopenCTeCabA(FIDCtrl, False);
end;

procedure TFmCTe_Pesq_0000.BtImprimeClick(Sender: TObject);
begin
  ImprimeCTe(False);
end;

procedure TFmCTe_Pesq_0000.DefineFrx(var Report: TfrxReport; const OQueFaz: TfrxImpComo);
var
  I: Integer;
  //Page: TfrxReportPage;
begin
  Screen.Cursor := crHourGlass;
  //
  DModG.ReopenParamsEmp(DmodG.QrEmpresasCodigo.Value);
  UnDmkDAC_PF.AbreMySQLQuery0(QrA, Dmod.MyDB, [
  'SELECT * ' ,
  'FROM ctecaba' ,
  'WHERE FatID=' + Geral.FF0(QrCTeCabAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrCTeCabAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrCTeCabAEmpresa.Value),
  '']);
  //
  ReopenCTeIt3InfQ();
  VARF_UMED_QTDE_01 := ''; VARF_UMED_QTDE_02 := '';
  VARF_UMED_QTDE_03 := ''; VARF_UMED_QTDE_04 := '';
  VARF_UMED_QTDE_05 := ''; VARF_UMED_QTDE_06 := '';
  VARF_UMED_QTDE_07 := ''; VARF_UMED_QTDE_08 := '';
  Qr3InfQ.First;
  while not Qr3InfQ.Eof do
  begin
    if Qr3InfQ.RecNo = 1 then
      VARF_UMED_QTDE_01 := FloatToStr(Qr3InfQqCarga.Value) + Qr3InfQtpMed.Value;
    if Qr3InfQ.RecNo = 2 then
      VARF_UMED_QTDE_02 := FloatToStr(Qr3InfQqCarga.Value) + Qr3InfQtpMed.Value;
    if Qr3InfQ.RecNo = 3 then
      VARF_UMED_QTDE_03 := FloatToStr(Qr3InfQqCarga.Value) + Qr3InfQtpMed.Value;
    if Qr3InfQ.RecNo = 4 then
      VARF_UMED_QTDE_04 := FloatToStr(Qr3InfQqCarga.Value) + Qr3InfQtpMed.Value;
    if Qr3InfQ.RecNo = 5 then
      VARF_UMED_QTDE_05 := FloatToStr(Qr3InfQqCarga.Value) + Qr3InfQtpMed.Value;
    if Qr3InfQ.RecNo = 6 then
      VARF_UMED_QTDE_06 := FloatToStr(Qr3InfQqCarga.Value) + Qr3InfQtpMed.Value;
    if Qr3InfQ.RecNo = 7 then
      VARF_UMED_QTDE_07 := FloatToStr(Qr3InfQqCarga.Value) + Qr3InfQtpMed.Value;
    if Qr3InfQ.RecNo = 8 then
      VARF_UMED_QTDE_08 := FloatToStr(Qr3InfQqCarga.Value) + Qr3InfQtpMed.Value;
    //
    Qr3InfQ.Next;
  end;
  ReopenCTeIt2Seg();
  VARF_SEG_NOME_01 := '';
  VARF_SEG_RESP_01 := '';
  VARF_SEG_APOL_01 := '';
  VARF_SEG_AVER_01 := '';
  VARF_SEG_NOME_02 := '';
  VARF_SEG_RESP_02 := '';
  VARF_SEG_APOL_02 := '';
  VARF_SEG_AVER_02 := '';
  while not Qr2Seg.Eof do
  begin
    if Qr2Seg.RecNo = 1 then
    begin
      VARF_SEG_NOME_01 := Qr2SegxSeg.Value;
      VARF_SEG_RESP_01 := sCTeRespSeg[Qr2SegrespSeg.Value];
      VARF_SEG_APOL_01 := Qr2SegnApol.Value;
      VARF_SEG_AVER_01 := Qr2SegnAver.Value;
    end;
    if Qr2Seg.RecNo = 2 then
    begin
      VARF_SEG_NOME_02 := Qr2SegxSeg.Value;
      VARF_SEG_RESP_02 := sCTeRespSeg[Qr2SegrespSeg.Value];
      VARF_SEG_APOL_02 := Qr2SegnApol.Value;
      VARF_SEG_AVER_02 := Qr2SegnAver.Value;
    end;
    //
    Qr2Seg.Next;
  end;
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenCTeIt2Comp();
  VARF_CMPVL_NOM_01 := ''; VARF_CMPVL_VLR_01 := '';
  VARF_CMPVL_NOM_02 := ''; VARF_CMPVL_VLR_02 := '';
  VARF_CMPVL_NOM_03 := ''; VARF_CMPVL_VLR_03 := '';
  VARF_CMPVL_NOM_04 := ''; VARF_CMPVL_VLR_04 := '';
  VARF_CMPVL_NOM_05 := ''; VARF_CMPVL_VLR_05 := '';
  VARF_CMPVL_NOM_06 := ''; VARF_CMPVL_VLR_06 := '';
  VARF_CMPVL_NOM_07 := ''; VARF_CMPVL_VLR_07 := '';
  VARF_CMPVL_NOM_08 := ''; VARF_CMPVL_VLR_08 := '';
  VARF_CMPVL_NOM_09 := ''; VARF_CMPVL_VLR_09 := '';
  VARF_CMPVL_NOM_10 := ''; VARF_CMPVL_VLR_10 := '';
  VARF_CMPVL_NOM_11 := ''; VARF_CMPVL_VLR_11 := '';
  VARF_CMPVL_NOM_12 := ''; VARF_CMPVL_VLR_12 := '';
  VARF_CMPVL_NOM_13 := ''; VARF_CMPVL_VLR_13 := '';
  VARF_CMPVL_NOM_14 := ''; VARF_CMPVL_VLR_14 := '';
  VARF_CMPVL_NOM_15 := ''; VARF_CMPVL_VLR_15 := '';
  VARF_CMPVL_NOM_16 := ''; VARF_CMPVL_VLR_16 := '';
  while not Qr2Comp.Eof do
  begin
    if Qr2Comp.RecNo = 1 then
    begin
      VARF_CMPVL_NOM_01 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_01 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 2 then
    begin
      VARF_CMPVL_NOM_02 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_02 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 3 then
    begin
      VARF_CMPVL_NOM_03 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_03 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 4 then
    begin
      VARF_CMPVL_NOM_04 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_04 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 5 then
    begin
      VARF_CMPVL_NOM_05 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_05 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 6 then
    begin
      VARF_CMPVL_NOM_06 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_06 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 7 then
    begin
      VARF_CMPVL_NOM_07 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_07 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 8 then
    begin
      VARF_CMPVL_NOM_08 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_08 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 9 then
    begin
      VARF_CMPVL_NOM_09 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_09 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 10 then
    begin
      VARF_CMPVL_NOM_10 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_10 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 11 then
    begin
      VARF_CMPVL_NOM_11 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_11 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 12 then
    begin
      VARF_CMPVL_NOM_12 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_12 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 13 then
    begin
      VARF_CMPVL_NOM_13 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_13 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 14 then
    begin
      VARF_CMPVL_NOM_14 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_14 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 15 then
    begin
      VARF_CMPVL_NOM_15 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_15 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 16 then
    begin
      VARF_CMPVL_NOM_16 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_16 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    ;
    //
    Qr2Comp.Next;
  end;
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenCTeIt2InfDoc();
  VARF_DOCUM_TIPO_01 := ''; VARF_DOCUM_DOCU_01 := ''; VARF_DOCUM_SRNR_01 := '';
  VARF_DOCUM_TIPO_02 := ''; VARF_DOCUM_DOCU_02 := ''; VARF_DOCUM_SRNR_02 := '';
  VARF_DOCUM_TIPO_03 := ''; VARF_DOCUM_DOCU_03 := ''; VARF_DOCUM_SRNR_03 := '';
  VARF_DOCUM_TIPO_04 := ''; VARF_DOCUM_DOCU_04 := ''; VARF_DOCUM_SRNR_04 := '';
  VARF_DOCUM_TIPO_05 := ''; VARF_DOCUM_DOCU_05 := ''; VARF_DOCUM_SRNR_05 := '';
  VARF_DOCUM_TIPO_06 := ''; VARF_DOCUM_DOCU_06 := ''; VARF_DOCUM_SRNR_06 := '';
  VARF_DOCUM_TIPO_07 := ''; VARF_DOCUM_DOCU_07 := ''; VARF_DOCUM_SRNR_07 := '';
  VARF_DOCUM_TIPO_08 := ''; VARF_DOCUM_DOCU_08 := ''; VARF_DOCUM_SRNR_08 := '';
  VARF_DOCUM_TIPO_09 := ''; VARF_DOCUM_DOCU_09 := ''; VARF_DOCUM_SRNR_09 := '';
  VARF_DOCUM_TIPO_10 := ''; VARF_DOCUM_DOCU_10 := ''; VARF_DOCUM_SRNR_10 := '';
  VARF_DOCUM_TIPO_11 := ''; VARF_DOCUM_DOCU_11 := ''; VARF_DOCUM_SRNR_11 := '';
  VARF_DOCUM_TIPO_12 := ''; VARF_DOCUM_DOCU_12 := ''; VARF_DOCUM_SRNR_12 := '';
  VARF_DOCUM_TIPO_13 := ''; VARF_DOCUM_DOCU_13 := ''; VARF_DOCUM_SRNR_13 := '';
  VARF_DOCUM_TIPO_14 := ''; VARF_DOCUM_DOCU_14 := ''; VARF_DOCUM_SRNR_14 := '';
  VARF_DOCUM_TIPO_15 := ''; VARF_DOCUM_DOCU_15 := ''; VARF_DOCUM_SRNR_15 := '';
  VARF_DOCUM_TIPO_16 := ''; VARF_DOCUM_DOCU_16 := ''; VARF_DOCUM_SRNR_16 := '';
  VARF_DOCUM_TIPO_17 := ''; VARF_DOCUM_DOCU_17 := ''; VARF_DOCUM_SRNR_17 := '';
  VARF_DOCUM_TIPO_18 := ''; VARF_DOCUM_DOCU_18 := ''; VARF_DOCUM_SRNR_18 := '';
  VARF_DOCUM_TIPO_19 := ''; VARF_DOCUM_DOCU_19 := ''; VARF_DOCUM_SRNR_19 := '';
  VARF_DOCUM_TIPO_20 := ''; VARF_DOCUM_DOCU_20 := ''; VARF_DOCUM_SRNR_20 := '';
  //
  while not Qr2InfDoc.Eof do
  begin
    if Qr2InfDoc.RecNo = 1 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_01, VARF_DOCUM_DOCU_01, VARF_DOCUM_SRNR_01)
    else
    if Qr2InfDoc.RecNo = 2 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_02, VARF_DOCUM_DOCU_02, VARF_DOCUM_SRNR_02)
    else
    if Qr2InfDoc.RecNo = 3 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_03, VARF_DOCUM_DOCU_03, VARF_DOCUM_SRNR_03)
    else
    if Qr2InfDoc.RecNo = 4 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_04, VARF_DOCUM_DOCU_04, VARF_DOCUM_SRNR_04)
    else
    if Qr2InfDoc.RecNo = 5 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_05, VARF_DOCUM_DOCU_05, VARF_DOCUM_SRNR_05)
    else
    if Qr2InfDoc.RecNo = 6 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_06, VARF_DOCUM_DOCU_06, VARF_DOCUM_SRNR_06)
    else
    if Qr2InfDoc.RecNo = 7 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_07, VARF_DOCUM_DOCU_07, VARF_DOCUM_SRNR_07)
    else
    if Qr2InfDoc.RecNo = 8 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_08, VARF_DOCUM_DOCU_08, VARF_DOCUM_SRNR_08)
    else
    if Qr2InfDoc.RecNo = 9 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_09, VARF_DOCUM_DOCU_09, VARF_DOCUM_SRNR_09)
    else
    if Qr2InfDoc.RecNo = 10 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_10, VARF_DOCUM_DOCU_10, VARF_DOCUM_SRNR_10)
    else
    if Qr2InfDoc.RecNo = 11 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_11, VARF_DOCUM_DOCU_11, VARF_DOCUM_SRNR_11)
    else
    if Qr2InfDoc.RecNo = 12 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_12, VARF_DOCUM_DOCU_12, VARF_DOCUM_SRNR_12)
    else
    if Qr2InfDoc.RecNo = 13 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_13, VARF_DOCUM_DOCU_13, VARF_DOCUM_SRNR_13)
    else
    if Qr2InfDoc.RecNo = 14 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_14, VARF_DOCUM_DOCU_14, VARF_DOCUM_SRNR_14)
    else
    if Qr2InfDoc.RecNo = 15 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_15, VARF_DOCUM_DOCU_15, VARF_DOCUM_SRNR_15)
    else
    if Qr2InfDoc.RecNo = 16 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_16, VARF_DOCUM_DOCU_16, VARF_DOCUM_SRNR_16)
    else
    if Qr2InfDoc.RecNo = 17 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_17, VARF_DOCUM_DOCU_17, VARF_DOCUM_SRNR_17)
    else
    if Qr2InfDoc.RecNo = 18 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_18, VARF_DOCUM_DOCU_18, VARF_DOCUM_SRNR_18)
    else
    if Qr2InfDoc.RecNo = 19 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_19, VARF_DOCUM_DOCU_19, VARF_DOCUM_SRNR_19)
    else
    if Qr2InfDoc.RecNo = 20 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_20, VARF_DOCUM_DOCU_20, VARF_DOCUM_SRNR_20)
    ;
    //
    Qr2InfDoc.Next;
  end;
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenCTeMoRodoCab();
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenCTeMoRodoVeic();
  VARF_TPVEIC_TIPO_01 := ''; VARF_TPVEIC_PLACA_01 := ''; VARF_TPVEIC_UF_01 := ''; VARF_TPVEIC_RNTRC_01 := '';
  VARF_TPVEIC_TIPO_02 := ''; VARF_TPVEIC_PLACA_02 := ''; VARF_TPVEIC_UF_02 := ''; VARF_TPVEIC_RNTRC_02 := '';
  VARF_TPVEIC_TIPO_03 := ''; VARF_TPVEIC_PLACA_03 := ''; VARF_TPVEIC_UF_03 := ''; VARF_TPVEIC_RNTRC_03 := '';
  VARF_TPVEIC_TIPO_04 := ''; VARF_TPVEIC_PLACA_04 := ''; VARF_TPVEIC_UF_04 := ''; VARF_TPVEIC_RNTRC_04 := '';
  while not QrRodoVeic.Eof do
  begin
    if QrRodoVeic.RecNo = 1 then
    begin
      VARF_TPVEIC_TIPO_01  := sCTeTpVeic[QrRodoVeictpVeic.Value];
      VARF_TPVEIC_PLACA_01 := QrRodoVeicplaca.Value;
      VARF_TPVEIC_UF_01    := QrRodoVeicUF.Value;
      if QrRodoVeictpProp.Value = 'T' then
        VARF_TPVEIC_RNTRC_01 := QrRodoPropRNTRC.Value
      else
        VARF_TPVEIC_RNTRC_01 := QrRodoCabRNTRC.Value;
    end;
    if QrRodoVeic.RecNo = 2 then
    begin
      VARF_TPVEIC_TIPO_02  := sCTeTpVeic[QrRodoVeictpVeic.Value];
      VARF_TPVEIC_PLACA_02 := QrRodoVeicplaca.Value;
      VARF_TPVEIC_UF_02    := QrRodoVeicUF.Value;
      if QrRodoVeictpProp.Value = 'T' then
        VARF_TPVEIC_RNTRC_02 := QrRodoPropRNTRC.Value
      else
        VARF_TPVEIC_RNTRC_02 := QrRodoCabRNTRC.Value;
    end;
    if QrRodoVeic.RecNo = 3 then
    begin
      VARF_TPVEIC_TIPO_03  := sCTeTpVeic[QrRodoVeictpVeic.Value];
      VARF_TPVEIC_PLACA_03 := QrRodoVeicplaca.Value;
      VARF_TPVEIC_UF_03    := QrRodoVeicUF.Value;
      if QrRodoVeictpProp.Value = 'T' then
        VARF_TPVEIC_RNTRC_03 := QrRodoPropRNTRC.Value
      else
        VARF_TPVEIC_RNTRC_03 := QrRodoCabRNTRC.Value;
    end;
    if QrRodoVeic.RecNo = 4 then
    begin
      VARF_TPVEIC_TIPO_04  := sCTeTpVeic[QrRodoVeictpVeic.Value];
      VARF_TPVEIC_PLACA_04 := QrRodoVeicplaca.Value;
      VARF_TPVEIC_UF_04    := QrRodoVeicUF.Value;
      if QrRodoVeictpProp.Value = 'T' then
        VARF_TPVEIC_RNTRC_04 := QrRodoPropRNTRC.Value
      else
        VARF_TPVEIC_RNTRC_04 := QrRodoCabRNTRC.Value;
    end;
    QrRodoVeic.Next;
  end;
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenCTeMoRodoVPed();
  VARF_VPEDAG_CNPJR_01 := ''; VARF_VPEDAG_CNPJF_01 := ''; VARF_VPEDAG_NCOMP_01 := '';
  VARF_VPEDAG_CNPJR_02 := ''; VARF_VPEDAG_CNPJF_02 := ''; VARF_VPEDAG_NCOMP_02 := '';
  VARF_VPEDAG_CNPJR_03 := ''; VARF_VPEDAG_CNPJF_03 := ''; VARF_VPEDAG_NCOMP_03 := '';
  VARF_VPEDAG_CNPJR_04 := ''; VARF_VPEDAG_CNPJF_04 := ''; VARF_VPEDAG_NCOMP_04 := '';
  while not QrRodoVPed.Eof do
  begin
    if QrRodoVPed.RecNo = 1 then
    begin
      VARF_VPEDAG_CNPJR_01 := Geral.FormataCNPJ_TT_NFe(QrRodoVPedCNPJPg.Value);
      VARF_VPEDAG_CNPJF_01 := Geral.FormataCNPJ_TT_NFe(QrRodoVPedCNPJForn.Value);
      VARF_VPEDAG_NCOMP_01 := QrRodoVPednCompra.Value;
    end;
    if QrRodoVPed.RecNo = 2 then
    begin
      VARF_VPEDAG_CNPJR_02 := Geral.FormataCNPJ_TT_NFe(QrRodoVPedCNPJPg.Value);
      VARF_VPEDAG_CNPJF_02 := Geral.FormataCNPJ_TT_NFe(QrRodoVPedCNPJForn.Value);
      VARF_VPEDAG_NCOMP_02 := QrRodoVPednCompra.Value;
    end;
    if QrRodoVPed.RecNo = 3 then
    begin
      VARF_VPEDAG_CNPJR_03 := Geral.FormataCNPJ_TT_NFe(QrRodoVPedCNPJPg.Value);
      VARF_VPEDAG_CNPJF_03 := Geral.FormataCNPJ_TT_NFe(QrRodoVPedCNPJForn.Value);
      VARF_VPEDAG_NCOMP_03 := QrRodoVPednCompra.Value;
    end;
    if QrRodoVPed.RecNo = 4 then
    begin
      VARF_VPEDAG_CNPJR_04 := Geral.FormataCNPJ_TT(QrRodoVPedCNPJPg.Value);
      VARF_VPEDAG_CNPJF_04 := Geral.FormataCNPJ_TT(QrRodoVPedCNPJForn.Value);
      VARF_VPEDAG_NCOMP_04 := QrRodoVPednCompra.Value;
    end;
    QrRodoVPed.Next;
  end;
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenCTeMoRodoMoto();
  VARF_MOTO_NOME_01 := ''; VARF_MOTO_CPF_01 := '';
  VARF_MOTO_NOME_02 := ''; VARF_MOTO_CPF_02 := '';
  while not QrRodoMoto.Eof do
  begin
    if QrRodoMoto.RecNo = 1 then
    begin
      VARF_MOTO_NOME_01 := QrRodoMotoxNome.Value;
      VARF_MOTO_CPF_01  := Geral.FormataCNPJ_TT(QrRodoMotoCPF.Value);
    end;
    QrRodoMoto.Next;
    if QrRodoMoto.RecNo = 2 then
    begin
      VARF_MOTO_NOME_02 := QrRodoMotoxNome.Value;
      VARF_MOTO_CPF_02  := Geral.FormataCNPJ_TT(QrRodoMotoCPF.Value);
    end;
    QrRodoMoto.Next;
  end;
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenCTeMoRodoLacr();
  VARF_LACRODO_LACRES := '';
  while not QrRodoLacr.Eof do
  begin
    if VARF_LACRODO_LACRES <> '' then
      VARF_LACRODO_LACRES := VARF_LACRODO_LACRES + ', ';
    VARF_LACRODO_LACRES := VARF_LACRODO_LACRES + QrRodoLacrnLacre.Value;
    //
    QrRodoLacr.Next;
  end;
  if VARF_LACRODO_LACRES <> '' then
    VARF_LACRODO_LACRES := VARF_LACRODO_LACRES + '.';
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenCTeIt2ObsCont();
  VARF_OBS_CONT := '';
  while not Qr2ObsCont.Eof do
  begin
    if VARF_OBS_CONT <> '' then
      VARF_OBS_CONT := VARF_OBS_CONT + '. ';
    VARF_OBS_CONT := VARF_OBS_CONT + Qr2ObsContxTexto.Value;
    //
    Qr2ObsCont.Next;
  end;
  if VARF_OBS_CONT <> '' then
    VARF_OBS_CONT := VARF_OBS_CONT + '. ';
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenCTeIt2ObsFisco();
  VARF_OBS_FISCO := '';
  while not Qr2ObsFisco.Eof do
  begin
    if VARF_OBS_FISCO <> '' then
      VARF_OBS_FISCO := VARF_OBS_FISCO + ', ';
    VARF_OBS_FISCO := VARF_OBS_FISCO + Qr2ObsFiscoxTexto.Value;
    //
    Qr2ObsFisco.Next;
  end;
  if VARF_OBS_FISCO <> '' then
    VARF_OBS_FISCO := VARF_OBS_FISCO + '. ';
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  if TCTeRodoLota(QrRodoCabLota.Value) = cterodolotaSim then
    Report := frxA4A_001RodoLot
  else
    Report := frxA4A_001RodoFra;
  //
  case OQueFaz of
    ficMostra: //MyObjects.frxMostra(Report, 'CTe' + QrAId.Value);
    begin
      Report.PreviewOptions.ZoomMode := zmPageWidth;
      MyObjects.frxPrepara(Report, 'CTe' + QrAId.Value);
      for I := 0 to Report.PreviewPages.Count -1 do
      begin
        Report.PreviewPages.Page[I].PaperSize := DMPAPER_A4;
        Report.PreviewPages.ModifyPage(I, Report.PreviewPages.Page[I]);
      end;
      FmMeuFrx.ShowModal;
      FmMeuFrx.Destroy;
      FDACTEImpresso := True;
    end;
    {ficImprime:
    ficSalva}
    ficNone: ;// N�o faz nada!
    ficExporta: MyObjects.frxPrepara(Report, 'CTe' + QrAId.Value);
    else Geral.MB_Erro('A��o n�o definida para frx da DACTE!');
  end;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmCTe_Pesq_0000.BtInutilizaClick(Sender: TObject);
begin
{ TODO -cCTe : Inutilizacao }
{
  if DBCheck.CriaFm(TFmCTeInut_0000a, FmCTeInut_0000a, afmoNegarComAviso) then
  begin
    FmCTeInut_0000a.ShowModal;
    FmCTeInut_0000a.Destroy;
  end;
}
end;

procedure TFmCTe_Pesq_0000.Autorizao1Click(Sender: TObject);
begin
  Geral.MensagemBox(QrArqXML_Aut.Value, 'XML Autoriza��o', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmCTe_Pesq_0000.BtReabreClick(Sender: TObject);
var
  IDCtrl: Integer;
begin
  IDCtrl := 0;
  if (QrCTeCabA.State <> dsInactive) then IDCtrl := QrCTeCabAIDCtrl.Value;
  ReopenCTeCabA(IDCtrl, False);
end;

procedure TFmCTe_Pesq_0000.BtEnviaClick(Sender: TObject);
  procedure ExportaXMLeDACTE(const CNPJ_CPF: String; var DiretDACTE, DiretXML: String);
  var
    Id, ArqDACTE, ArqXML, DirCTeXML, DirDACTEs, XML_Distribuicao: String;
    Frx: TfrxReport;
    ExportouDACTE, ExportouXML: Boolean;
  begin
    ExportouDACTE := False;
    ExportouXML   := False;
    DiretDACTE    := '';
    DiretXML      := '';
    Id            := Geral.SoNumero_TT(QrCTeCabAId.Value);
    DirCTeXML     := DmCTe_0000.QrFilialDirCTeProt.Value;
    DirDACTEs     := DmCTe_0000.QrFilialDirDACTEs.Value;
    //
    if DirCTeXML = '' then
      DirCTeXML := 'C:\Dermatek\CTe\Clientes';
    if DirDACTEs = '' then
      DirDACTEs := 'C:\Dermatek\CTe\Clientes';
    //
    Geral.VerificaDir(DirCTeXML, '\', '', True);
    DirCTeXML := DirCTeXML + CNPJ_CPF;
    Geral.VerificaDir(DirDACTEs, '\', '', True);
    DirDACTEs := DirDACTEs + CNPJ_CPF;
    //
    // DACTE
    //
    if QrCTeCabAcStat.Value = 100 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando arquivo PDF do DACTE');
      //
      Frx      := DefineQual_frxCTe(ficNone);
      ArqDACTE := DirDACTEs;
      //
      Geral.VerificaDir(ArqDACTE, '\', 'Diret�rio tempor�rio do DACTE', True);
      //
      ArqDACTE := ArqDACTE + Id + '.pdf';
      //
      try
        frxPDFExport.FileName := ArqDACTE;
        MyObjects.frxPrepara(Frx, 'CTe' + ID);
        Frx.Export(frxPDFExport);
        //
        ExportouDACTE := True;
      except
        ExportouDACTE := False;
      end;
    end;
    //
    // XML
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando arquivo XML da CTe');
    //
    ArqXML := DirCTeXML;
    //
    Geral.VerificaDir(ArqXML, '\', 'Diret�rio tempor�rio do XML de CTe', True);
    //
    ArqXML := ArqXML + ID + '.xml';
    //
    DmCTe_0000.QrArq.Close;
    DmCTe_0000.QrArq.Params[0].AsInteger := QrCTeCabAIDCtrl.Value;
    DmCTe_0000.QrArq.Open;
    //
    if CTe_PF.XML_DistribuiCTe(Id, Trunc(QrCTeCabAcStat.Value),
      DmCTe_0000.QrArqXML_CTe.Value, DmCTe_0000.QrArqXML_Aut.Value,
      DmCTe_0000.QrArqXML_Can.Value, True, XML_Distribuicao,
      QrCTeCabAversao.Value, 'na base de dados')
    then
      ExportouXML := CTe_PF.SalvaXML(ArqXML, XML_Distribuicao);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    if ExportouXML then
      DiretXML := ArqXML;
    if ExportouDACTE then
      DiretDACTE := ArqDACTE;
  end;
var
  CNPJ_CPF, DACTE, XML: String;
  Entidade: Integer;
begin
  DmCTe_0000.ReopenOpcoesCTe(QrOpcoesCTe, DModG.QrEmpresasCodigo.Value, True);
  //
  if QrCTeCabAdest_CNPJ.Value <> '' then
    CNPJ_CPF := QrCTeCabAdest_CNPJ.Value
  else
    CNPJ_CPF := QrCTeCabAdest_CPF.Value;
  //
  ExportaXMLeDACTE(CNPJ_CPF, DACTE, XML);
  //
  CNPJ_CPF := Geral.SoNumero_TT(CNPJ_CPF);
  //
  DModG.ObtemEntidadeDeCNPJCFP(CNPJ_CPF, Entidade);
  //
  if MyObjects.FIC(DACTE = '', nil, 'Falha ao exportar DACTE!' + sLineBreak + 'O e-mail n�o poder� ser enviado!') then Exit;
  if MyObjects.FIC(XML = '', nil, 'Falha ao exportar XML!' + sLineBreak + 'O e-mail n�o poder� ser enviado!') then Exit;
  //
  if UMailEnv.Monta_e_Envia_Mail([Entidade, DModG.QrEmpresasCodigo.Value,
    QrCTeCabAcStat.Value], meCTe, [DACTE, XML], [QrCTeCabAdest_xNome.Value,
    QrCTeCabAId.Value], True) then
  begin
    FMailEnviado := True;
    Geral.MB_Aviso('E-mail enviado com sucesso!');
  end;
end;

procedure TFmCTe_Pesq_0000.BtEventoClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCTeEveRCab, FmCTeEveRCab, afmoNegarComAviso) then
  begin
    FmCTeEveRCab.ReopenCTeCabA(
      QrCTeCabAFatID.Value, QrCTeCabAFatNum.Value, QrCTeCabAEmpresa.Value);
    FmCTeEveRCab.ShowModal;
    FmCTeEveRCab.Destroy;
  end;
end;

procedure TFmCTe_Pesq_0000.BtExcluiClick(Sender: TObject);
{ TODO -cCTe : Exclusao CTe }
{
  function VerificaSeNumeroFoiInutilizado(CTeNum: Integer): Boolean;
  begin
    //True  = Libera Exclus�o
    //False = Bloqueia Exclus�o
    Result := False;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
      'SELECT nNFIni, nNFFim ',
      'FROM cteinut ',
      'WHERE ' + Geral.FF0(CTeNum) + ' BETWEEN nNFIni AND nNFFim ',
      '']);
    if DModG.QrAux.RecordCount > 0 then
      Result := True;    
  end;
}
var
  Status, FatID, FatNum, Empresa, ide_nNF: Integer;
  ChaveCTe: String;
begin
{
  if (QrCTeCabA.State = dsInactive) or (QrCTeCabA.RecordCount = 0) then Exit;
  //
  Status   := Trunc(QrCTeCabAcStat.Value);
  FatID    := QrCTeCabAFatID.Value;
  FatNum   := QrCTeCabAFatNum.Value;
  Empresa  := QrCTeCabAEmpresa.Value;
  ChaveCTe := QrCTeCabAId.Value;
  //
  if Geral.MensagemBox('Deseja realmente excluir a CTe:' + sLineBreak +
  'S�rie: ' + FormatFloat('000', QrCTeCabAide_serie.Value) + sLineBreak +
  'N�: ' + FormatFloat('000', QrCTeCabAide_nNF.Value) + sLineBreak + sLineBreak +
  'ESTE PROCEDIMENTO N�O PODER� SER REVERTIDO!',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if VerificaSeNumeroFoiInutilizado(QrCTeCabAide_nNF.Value) then
    begin
      if DmCTe_0000.CancelaFaturamento(ChaveCTe) then //Primeiro exclui o faturamento e estoque depois a nota
      begin
        DmCTe_0000.ExcluiCTe(Status, FatID, FatNum, Empresa, False);
        //
        QrCTeCabA.Next;
        ide_nNF := UMyMod.ProximoRegistro(QrCTeCabA, 'ide_nNF', QrCTeCabAide_nNF.Value);
        //
        BtReabreClick(Self);
        QrCTeCabA.Locate('ide_nNF', ide_nNF, []);
        Geral.MensagemBox('CTe exclu�da com sucesso!',
        'Informa��o', MB_OK+MB_ICONINFORMATION);
      end;
    end else
      Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
        'Motivo: Este n�mero n�o foi inutilizado!' + sLineBreak +
        'Inutilize este n�mero e tente novamente!');
  end;
}
end;

procedure TFmCTe_Pesq_0000.BtArqClick(Sender: TObject);
begin
  QrArq.Close;
  QrArq.Params[0].AsInteger := QrCTeCabAIDCtrl.Value;
  QrArq.Open;
  //
  MyObjects.MostraPopUpDeBotao(PMArq, BtArq);
end;

procedure TFmCTe_Pesq_0000.BtLeArqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLeArq, BtLeArq);
end;

procedure TFmCTe_Pesq_0000.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTROX := QrCTeCabAId.Value;
  //
  if TFmCTe_Pesq_0000(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmCTe_Pesq_0000.CampospreenchidosnoXMLdaCTe1Click(Sender: TObject);
begin
  QrCTeXMLI.Close;
  QrCTeXMLI.Params[00].AsInteger := QrCTeCabAFatID.Value;
  QrCTeXMLI.Params[01].AsInteger := QrCTeCabAFatNum.Value;
  QrCTeXMLI.Params[02].AsInteger := QrCTeCabAEmpresa.Value;
  QrCTeXMLI.Open;
  //
  MyObjects.frxMostra(frxCampos, 'Campos preenchidos no XML');
end;

procedure TFmCTe_Pesq_0000.Cancelamento1Click(Sender: TObject);
const
  SohLer = True;
begin
  //CTe_PF.MostraFormStepsCTe_CancelaCTe(SohLer);
end;

procedure TFmCTe_Pesq_0000.Cancelamento2Click(Sender: TObject);
begin
  Geral.MensagemBox(QrArqXML_Can.Value, 'XML Cancelamento', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmCTe_Pesq_0000.CkEmitClick(Sender: TObject);
begin
  FechaCTeCabA;
end;

procedure TFmCTe_Pesq_0000.DefineVarsDePreparacao();
begin
  FLote      := QrCTeCabALoteEnv.Value;
  FEmpresa   := QrCTeCabAEmpresa.Value;
  FChaveCTe  := QrCTeCabAId.Value;
  FProtocolo := QrCTeCabAnProt.Value;
  FIDCtrl    := QrCTeCabAIDCtrl.Value;
end;

function TFmCTe_Pesq_0000.DefineQual_frxCTe(OQueFaz: TfrxImpComo): TfrxReport;
var
  Report: TfrxReport;
begin
  if TCTeModal(QrCTeCabAide_Modal.Value) = ctemodalRodoviario then
  begin
    FDACTEImpresso := False;
    DefineFrx(Report, OQueFaz);
    MyObjects.frxDefineDataSets(Report, [
    frxDsA
    ]);
    Result := Report;
  end;
end;

procedure TFmCTe_Pesq_0000.DiretriodoarquivoXML1Click(Sender: TObject);
var
  Dir, Arq, Ext, Arquivo: String;
begin
  if not DmCTe_0000.ReopenEmpresa(DModG.QrEmpresasCodigo.Value) then Exit;
  //
  Arq := QrCTeCabAId.Value;
  Ext := CTE_EXT_CTE_XML;
  //
  if DmCTe_0000.ObtemDirXML(Ext, Dir, True) then
  begin
    Arquivo := Dir + Arq + Ext;
    Geral.MensagemBox(Arquivo, 'Caminho do arquivo XML', MB_OK+MB_ICONWARNING);
  end; 
end;

procedure TFmCTe_Pesq_0000.dmkDBGrid1DblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := Uppercase(dmkDBGrid1.Columns[THackDBGrid(dmkDBGrid1).Col -1].FieldName);
  //
  if Campo = Uppercase('Id') then
  begin
    if (QrCTeCabA.State <> dsInactive) and (QrCTeCabA.RecordCount > 0) then
      Geral.MB_Aviso('Chave de acesso da CT-e:' + sLineBreak + QrCTeCabAId.Value);
  end;
end;

procedure TFmCTe_Pesq_0000.dmkDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  DmCTe_0000.ColoreStatusCTeEmDBGrid(TDBGrid(dmkDBGrid1), Rect, DataCol, Column,
  State, Trunc(QrCTeCabAcStat.Value));
  //
(*
  DmCTe_0000.ColoreSit ConfCTeEmDBGrid(TDBGrid(dmkDBGrid1), Rect, DataCol, Column,
  State, QrCTeCabAcSit Conf.Value);
*)
end;

procedure TFmCTe_Pesq_0000.EdClienteChange(Sender: TObject);
begin
  if not EdFilial.Focused then
    FechaCTeCabA;
end;

procedure TFmCTe_Pesq_0000.EdClienteExit(Sender: TObject);
begin
  FechaCTeCabA;
end;

procedure TFmCTe_Pesq_0000.EdFilialChange(Sender: TObject);
begin
  if not EdFilial.Focused then
    FechaCTeCabA;
end;

procedure TFmCTe_Pesq_0000.EdFilialExit(Sender: TObject);
begin
  FechaCTeCabA;
end;

procedure TFmCTe_Pesq_0000.ExportaporCFOPtotaiseitensCTe1Click(Sender: TObject);
begin
  ImprimeListaNova(True, True);
end;

procedure TFmCTe_Pesq_0000.ExportaporNatOptotaisCTe1Click(Sender: TObject);
begin
  ImprimeListaNova(False, True);
end;

procedure TFmCTe_Pesq_0000.FormActivate(Sender: TObject);
begin
{ *****
  if TFmCTe_Pesq_0000(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
  //Corrige lan�amentos financeiros de notas com desconto
  UMyMod.AbreQuery(DModG.QrCtrlGeral, Dmod.MyDB);
  if DModG.QrCtrlGeralAtualizouLctsCTe.AsInteger = 0 then
    DmCTe_0000.CorrigeLctosFaturaCTes(PB1);
  if DModG.QrCtrlGeralAtualizouCTeCan.AsInteger = 0 then
    DmCTe_0000.AtualizaTodasNotasCanceladas(PB1);
}
end;

procedure TFmCTe_Pesq_0000.FormCreate(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  ImgTipo.SQLType := stPsq;
  FDACTEImpresso := False;
  FMailEnviado := False;
  PageControl1.ActivePageIndex := 0;
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdFilial, CBFilial);
  QrClientes.Open;
  TPDataI.Date := Date - 10; // m�x permitido pelo fisco
  TPDataF.Date := Date;
  //
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  CBFilial.ListSource := DModG.DsEmpresas;
  RGAmbiente.ItemIndex := DModG.QrParamsEmpide_tpAmb.Value;
  //
  ReopenCTeCabA(0, False);
  //
  Screen.Cursor := crDefault;
end;

procedure TFmCTe_Pesq_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCTe_Pesq_0000.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
end;

procedure TFmCTe_Pesq_0000.frxA4A_001RodoLotGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'LogoCTeExiste' then Value := FileExists(QrOpcoesCTePathLogoCTe.Value) else
  if VarName = 'LogoCTePath' then Value := QrOpcoesCTePathLogoCTe.Value else
  if VarName = 'VARF_TPCTE_TXT' then Value := sCTetpCTe[QrAide_tpCTe.Value];
  if VarName = 'VARF_TPSERV_TXT' then Value := sCTetpserv[QrAide_tpServ.Value];
  if VarName = 'VARF_TOMA99_TXT' then Value := sCTeToma[QrAtoma99_toma.Value];
  if VarName = 'VARF_FORPAG_TXT' then Value := sCTeforpag[QrAide_forPag.Value];
  //
  if VarName = 'VARF_UMED_QTDE_01' then Value := VARF_UMED_QTDE_01;
  if VarName = 'VARF_UMED_QTDE_02' then Value := VARF_UMED_QTDE_02;
  if VarName = 'VARF_UMED_QTDE_03' then Value := VARF_UMED_QTDE_03;
  if VarName = 'VARF_UMED_QTDE_04' then Value := VARF_UMED_QTDE_04;
  if VarName = 'VARF_UMED_QTDE_05' then Value := VARF_UMED_QTDE_05;
  if VarName = 'VARF_UMED_QTDE_06' then Value := VARF_UMED_QTDE_06;
  if VarName = 'VARF_UMED_QTDE_07' then Value := VARF_UMED_QTDE_07;
  if VarName = 'VARF_UMED_QTDE_08' then Value := VARF_UMED_QTDE_08;
  //
  if VarName = 'VARF_SEG_NOME_01' then Value := VARF_SEG_NOME_01;
  if VarName = 'VARF_SEG_RESP_01' then Value := VARF_SEG_RESP_01;
  if VarName = 'VARF_SEG_APOL_01' then Value := VARF_SEG_APOL_01;
  if VarName = 'VARF_SEG_AVER_01' then Value := VARF_SEG_AVER_01;

  if VarName = 'VARF_SEG_NOME_02' then Value := VARF_SEG_NOME_02;
  if VarName = 'VARF_SEG_RESP_02' then Value := VARF_SEG_RESP_02;
  if VarName = 'VARF_SEG_APOL_02' then Value := VARF_SEG_APOL_02;
  if VarName = 'VARF_SEG_AVER_02' then Value := VARF_SEG_AVER_02;
  //
  if VarName = 'VARF_CMPVL_NOM_01' then Value := VARF_CMPVL_NOM_01;
  if VarName = 'VARF_CMPVL_VLR_01' then Value := VARF_CMPVL_VLR_01;
  if VarName = 'VARF_CMPVL_NOM_02' then Value := VARF_CMPVL_NOM_02;
  if VarName = 'VARF_CMPVL_VLR_02' then Value := VARF_CMPVL_VLR_02;
  if VarName = 'VARF_CMPVL_NOM_03' then Value := VARF_CMPVL_NOM_03;
  if VarName = 'VARF_CMPVL_VLR_03' then Value := VARF_CMPVL_VLR_03;
  if VarName = 'VARF_CMPVL_NOM_04' then Value := VARF_CMPVL_NOM_04;
  if VarName = 'VARF_CMPVL_VLR_04' then Value := VARF_CMPVL_VLR_04;
  if VarName = 'VARF_CMPVL_NOM_05' then Value := VARF_CMPVL_NOM_05;
  if VarName = 'VARF_CMPVL_VLR_05' then Value := VARF_CMPVL_VLR_05;
  if VarName = 'VARF_CMPVL_NOM_06' then Value := VARF_CMPVL_NOM_06;
  if VarName = 'VARF_CMPVL_VLR_06' then Value := VARF_CMPVL_VLR_06;
  if VarName = 'VARF_CMPVL_NOM_07' then Value := VARF_CMPVL_NOM_07;
  if VarName = 'VARF_CMPVL_VLR_07' then Value := VARF_CMPVL_VLR_07;
  if VarName = 'VARF_CMPVL_NOM_08' then Value := VARF_CMPVL_NOM_08;
  if VarName = 'VARF_CMPVL_VLR_08' then Value := VARF_CMPVL_VLR_08;
  if VarName = 'VARF_CMPVL_NOM_09' then Value := VARF_CMPVL_NOM_09;
  if VarName = 'VARF_CMPVL_VLR_09' then Value := VARF_CMPVL_VLR_09;
  if VarName = 'VARF_CMPVL_NOM_10' then Value := VARF_CMPVL_NOM_10;
  if VarName = 'VARF_CMPVL_VLR_10' then Value := VARF_CMPVL_VLR_10;
  if VarName = 'VARF_CMPVL_NOM_11' then Value := VARF_CMPVL_NOM_11;
  if VarName = 'VARF_CMPVL_VLR_11' then Value := VARF_CMPVL_VLR_11;
  if VarName = 'VARF_CMPVL_NOM_12' then Value := VARF_CMPVL_NOM_12;
  if VarName = 'VARF_CMPVL_VLR_12' then Value := VARF_CMPVL_VLR_12;
  if VarName = 'VARF_CMPVL_NOM_13' then Value := VARF_CMPVL_NOM_13;
  if VarName = 'VARF_CMPVL_VLR_13' then Value := VARF_CMPVL_VLR_13;
  if VarName = 'VARF_CMPVL_NOM_14' then Value := VARF_CMPVL_NOM_14;
  if VarName = 'VARF_CMPVL_VLR_14' then Value := VARF_CMPVL_VLR_14;
  if VarName = 'VARF_CMPVL_NOM_15' then Value := VARF_CMPVL_NOM_15;
  if VarName = 'VARF_CMPVL_VLR_15' then Value := VARF_CMPVL_VLR_15;
  if VarName = 'VARF_CMPVL_NOM_16' then Value := VARF_CMPVL_NOM_16;
  if VarName = 'VARF_CMPVL_VLR_16' then Value := VARF_CMPVL_VLR_16;
  //
  if VarName = 'VARF_CST_TXT' then Value := sCTeMyCST[QrACTeMyCST.Value];
  //
  if VarName = 'VARF_DOCUM_TIPO_01' then Value := VARF_DOCUM_TIPO_01;
  if VarName = 'VARF_DOCUM_DOCU_01' then Value := VARF_DOCUM_DOCU_01;
  if VarName = 'VARF_DOCUM_SRNR_01' then Value := VARF_DOCUM_SRNR_01;
  if VarName = 'VARF_DOCUM_TIPO_02' then Value := VARF_DOCUM_TIPO_02;
  if VarName = 'VARF_DOCUM_DOCU_02' then Value := VARF_DOCUM_DOCU_02;
  if VarName = 'VARF_DOCUM_SRNR_02' then Value := VARF_DOCUM_SRNR_02;
  if VarName = 'VARF_DOCUM_TIPO_03' then Value := VARF_DOCUM_TIPO_03;
  if VarName = 'VARF_DOCUM_DOCU_03' then Value := VARF_DOCUM_DOCU_03;
  if VarName = 'VARF_DOCUM_SRNR_03' then Value := VARF_DOCUM_SRNR_03;
  if VarName = 'VARF_DOCUM_TIPO_04' then Value := VARF_DOCUM_TIPO_04;
  if VarName = 'VARF_DOCUM_DOCU_04' then Value := VARF_DOCUM_DOCU_04;
  if VarName = 'VARF_DOCUM_SRNR_04' then Value := VARF_DOCUM_SRNR_04;
  if VarName = 'VARF_DOCUM_TIPO_05' then Value := VARF_DOCUM_TIPO_05;
  if VarName = 'VARF_DOCUM_DOCU_05' then Value := VARF_DOCUM_DOCU_05;
  if VarName = 'VARF_DOCUM_SRNR_05' then Value := VARF_DOCUM_SRNR_05;
  if VarName = 'VARF_DOCUM_TIPO_06' then Value := VARF_DOCUM_TIPO_06;
  if VarName = 'VARF_DOCUM_DOCU_06' then Value := VARF_DOCUM_DOCU_06;
  if VarName = 'VARF_DOCUM_SRNR_06' then Value := VARF_DOCUM_SRNR_06;
  if VarName = 'VARF_DOCUM_TIPO_07' then Value := VARF_DOCUM_TIPO_07;
  if VarName = 'VARF_DOCUM_DOCU_07' then Value := VARF_DOCUM_DOCU_07;
  if VarName = 'VARF_DOCUM_SRNR_07' then Value := VARF_DOCUM_SRNR_07;
  if VarName = 'VARF_DOCUM_TIPO_08' then Value := VARF_DOCUM_TIPO_08;
  if VarName = 'VARF_DOCUM_DOCU_08' then Value := VARF_DOCUM_DOCU_08;
  if VarName = 'VARF_DOCUM_SRNR_08' then Value := VARF_DOCUM_SRNR_08;
  if VarName = 'VARF_DOCUM_TIPO_09' then Value := VARF_DOCUM_TIPO_09;
  if VarName = 'VARF_DOCUM_DOCU_09' then Value := VARF_DOCUM_DOCU_09;
  if VarName = 'VARF_DOCUM_SRNR_09' then Value := VARF_DOCUM_SRNR_09;
  if VarName = 'VARF_DOCUM_TIPO_10' then Value := VARF_DOCUM_TIPO_10;
  if VarName = 'VARF_DOCUM_DOCU_10' then Value := VARF_DOCUM_DOCU_10;
  if VarName = 'VARF_DOCUM_SRNR_10' then Value := VARF_DOCUM_SRNR_10;
  if VarName = 'VARF_DOCUM_TIPO_11' then Value := VARF_DOCUM_TIPO_11;
  if VarName = 'VARF_DOCUM_DOCU_11' then Value := VARF_DOCUM_DOCU_11;
  if VarName = 'VARF_DOCUM_SRNR_11' then Value := VARF_DOCUM_SRNR_11;
  if VarName = 'VARF_DOCUM_TIPO_12' then Value := VARF_DOCUM_TIPO_12;
  if VarName = 'VARF_DOCUM_DOCU_12' then Value := VARF_DOCUM_DOCU_12;
  if VarName = 'VARF_DOCUM_SRNR_12' then Value := VARF_DOCUM_SRNR_12;
  if VarName = 'VARF_DOCUM_TIPO_13' then Value := VARF_DOCUM_TIPO_13;
  if VarName = 'VARF_DOCUM_DOCU_13' then Value := VARF_DOCUM_DOCU_13;
  if VarName = 'VARF_DOCUM_SRNR_13' then Value := VARF_DOCUM_SRNR_13;
  if VarName = 'VARF_DOCUM_TIPO_14' then Value := VARF_DOCUM_TIPO_14;
  if VarName = 'VARF_DOCUM_DOCU_14' then Value := VARF_DOCUM_DOCU_14;
  if VarName = 'VARF_DOCUM_SRNR_14' then Value := VARF_DOCUM_SRNR_14;
  if VarName = 'VARF_DOCUM_TIPO_15' then Value := VARF_DOCUM_TIPO_15;
  if VarName = 'VARF_DOCUM_DOCU_15' then Value := VARF_DOCUM_DOCU_15;
  if VarName = 'VARF_DOCUM_SRNR_15' then Value := VARF_DOCUM_SRNR_15;
  if VarName = 'VARF_DOCUM_TIPO_16' then Value := VARF_DOCUM_TIPO_16;
  if VarName = 'VARF_DOCUM_DOCU_16' then Value := VARF_DOCUM_DOCU_16;
  if VarName = 'VARF_DOCUM_SRNR_16' then Value := VARF_DOCUM_SRNR_16;
  if VarName = 'VARF_DOCUM_TIPO_17' then Value := VARF_DOCUM_TIPO_17;
  if VarName = 'VARF_DOCUM_DOCU_17' then Value := VARF_DOCUM_DOCU_17;
  if VarName = 'VARF_DOCUM_SRNR_17' then Value := VARF_DOCUM_SRNR_17;
  if VarName = 'VARF_DOCUM_TIPO_18' then Value := VARF_DOCUM_TIPO_18;
  if VarName = 'VARF_DOCUM_DOCU_18' then Value := VARF_DOCUM_DOCU_18;
  if VarName = 'VARF_DOCUM_SRNR_18' then Value := VARF_DOCUM_SRNR_18;
  if VarName = 'VARF_DOCUM_TIPO_19' then Value := VARF_DOCUM_TIPO_19;
  if VarName = 'VARF_DOCUM_DOCU_19' then Value := VARF_DOCUM_DOCU_19;
  if VarName = 'VARF_DOCUM_SRNR_19' then Value := VARF_DOCUM_SRNR_19;
  if VarName = 'VARF_DOCUM_TIPO_20' then Value := VARF_DOCUM_TIPO_20;
  if VarName = 'VARF_DOCUM_DOCU_20' then Value := VARF_DOCUM_DOCU_20;
  if VarName = 'VARF_DOCUM_SRNR_20' then Value := VARF_DOCUM_SRNR_20;
  //
  if VarName = 'VARF_RODOCAD_RNTRC' then Value := QrRodoCabRNTRC.Value;
  if VarName = 'VARF_RODOCAD_CIOT'  then Value := QrRodoCabCIOT.Value;
  if VarName = 'VARF_RODOCAD_LOTA'  then Value := sCTeRodoLota[QrRodoCablota.Value];
  if VarName = 'VARF_RODOCAD_DTPRV' then Value := Geral.FDT(QrRodoCabdPrev.Value, 2);
  //
  if VarName = 'VARF_TPVEIC_TIPO_01' then Value := VARF_TPVEIC_TIPO_01;
  if VarName = 'VARF_TPVEIC_PLACA_01' then Value := VARF_TPVEIC_PLACA_01;
  if VarName = 'VARF_TPVEIC_UF_01' then Value := VARF_TPVEIC_UF_01;
  if VarName = 'VARF_TPVEIC_RNTRC_01' then Value := VARF_TPVEIC_RNTRC_01;
  if VarName = 'VARF_TPVEIC_TIPO_02' then Value := VARF_TPVEIC_TIPO_02;
  if VarName = 'VARF_TPVEIC_PLACA_02' then Value := VARF_TPVEIC_PLACA_02;
  if VarName = 'VARF_TPVEIC_UF_02' then Value := VARF_TPVEIC_UF_02;
  if VarName = 'VARF_TPVEIC_RNTRC_02' then Value := VARF_TPVEIC_RNTRC_02;
  if VarName = 'VARF_TPVEIC_TIPO_03' then Value := VARF_TPVEIC_TIPO_03;
  if VarName = 'VARF_TPVEIC_PLACA_03' then Value := VARF_TPVEIC_PLACA_03;
  if VarName = 'VARF_TPVEIC_UF_03' then Value := VARF_TPVEIC_UF_03;
  if VarName = 'VARF_TPVEIC_RNTRC_03' then Value := VARF_TPVEIC_RNTRC_03;
  if VarName = 'VARF_TPVEIC_TIPO_04' then Value := VARF_TPVEIC_TIPO_04;
  if VarName = 'VARF_TPVEIC_PLACA_04' then Value := VARF_TPVEIC_PLACA_04;
  if VarName = 'VARF_TPVEIC_UF_04' then Value := VARF_TPVEIC_UF_04;
  if VarName = 'VARF_TPVEIC_RNTRC_04' then Value := VARF_TPVEIC_RNTRC_04;
  //
  if VarName = 'VARF_VPEDAG_CNPJR_01' then Value := VARF_VPEDAG_CNPJR_01;
  if VarName = 'VARF_VPEDAG_CNPJF_01' then Value := VARF_VPEDAG_CNPJF_01;
  if VarName = 'VARF_VPEDAG_NCOMP_01' then Value := VARF_VPEDAG_NCOMP_01;
  if VarName = 'VARF_VPEDAG_CNPJR_02' then Value := VARF_VPEDAG_CNPJR_02;
  if VarName = 'VARF_VPEDAG_CNPJF_02' then Value := VARF_VPEDAG_CNPJF_02;
  if VarName = 'VARF_VPEDAG_NCOMP_02' then Value := VARF_VPEDAG_NCOMP_02;
  if VarName = 'VARF_VPEDAG_CNPJR_03' then Value := VARF_VPEDAG_CNPJR_03;
  if VarName = 'VARF_VPEDAG_CNPJF_03' then Value := VARF_VPEDAG_CNPJF_03;
  if VarName = 'VARF_VPEDAG_NCOMP_03' then Value := VARF_VPEDAG_NCOMP_03;
  if VarName = 'VARF_VPEDAG_CNPJR_04' then Value := VARF_VPEDAG_CNPJR_04;
  if VarName = 'VARF_VPEDAG_CNPJF_04' then Value := VARF_VPEDAG_CNPJF_04;
  if VarName = 'VARF_VPEDAG_NCOMP_04' then Value := VARF_VPEDAG_NCOMP_04;
  //
  if VarName = 'VARF_MOTO_NOME_01' then Value := VARF_MOTO_NOME_01;
  if VarName = 'VARF_MOTO_NOME_02' then Value := VARF_MOTO_NOME_02;
  if VarName = 'VARF_MOTO_CPF_01' then Value := VARF_MOTO_CPF_01;
  if VarName = 'VARF_MOTO_CPF_02' then Value := VARF_MOTO_CPF_02;
  //
  if VarName = 'VARF_LACRODO_LACRES' then Value := VARF_LACRODO_LACRES;
  //
  if VarName = 'VARF_OBS_CONT' then Value := VARF_OBS_CONT;
  if VarName = 'VARF_OBS_FISCO' then Value := VARF_OBS_FISCO;
  //
end;

procedure TFmCTe_Pesq_0000.frxListaCTesGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := CBFilial.Text
  else
  if VarName = 'VARF_TERCEIRO' then
    Value := dmkPF.ParValueCodTxt('', CBCliente.Text, EdCliente.ValueVariant)
  else
  if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp2(
    TPDataI.Date, TPDataF.Date, True, True, '', '', '')
  else
  if VarName = 'VARF_STATUS' then
  begin
    if Ck100e101.Checked then
      Value := 'AUTORIZADAS E CANCELADAS'
    else
      Value := 'QUALQUER';
  end
  else
  if VarName = 'VARF_APENAS_EMP' then
  begin
    case RGQuemEmit.ItemIndex of
      0: Value := 'Notas Fiscais Emitidas';
      1: Value := 'Notas Fiscais Recebidas';
      2: Value := 'Notas Fiscais Emitidas e Recebidas';
      else Value := 'Notas Fiscais ????';
    end;
  end
  else
  if VarName = 'VARF_AMBIENTE' then
  begin
    case RGAmbiente.ItemIndex of
      0: Value := 'AMBOS';
      1: Value := 'PRODU��O';
      2: Value := 'HOMOLOGA��O';
    end;
  end
end;

procedure TFmCTe_Pesq_0000.ImprimeListaNova(PorCFOP, Exporta: Boolean);
begin
{ *****
  CTe_PF.MostraFormCTe_Pesq_0000_ImpLista(PorCFOP,
  EdFilial.ValueVariant, TPDataI.Date, TPDataF.Date, RGQuemEmit.ItemIndex,
  RGAmbiente.ItemIndex, EdCliente.ValueVariant, QrClientesTipo.Value,
  RGOrdem1.ItemIndex, RGOrdem2.ItemIndex, Ck100e101.Checked,
  QrClientesCNPJ.Value, QrClientesCPF.Value, CGcSit Conf, CkideNatOp.Checked,
  CBFilial.Text, CBCliente.Text, Exporta);
}
end;

procedure TFmCTe_Pesq_0000.ImprimeCTe(Preview: Boolean);
var
  I: Integer;
  Report: TfrxReport;
  Memo: TfrxMemoView;
begin
  //Report := frxA4A_002;
    Report := DefineQual_frxCTe(ficNone);
  try
    for I := 0 to Report.ComponentCount - 1 do
    begin
      if Report.Components[I] is TfrxMemoView then
      begin
        Memo := TfrxMemoView(Report.Components[I]);
        case Memo.Tag of
          0: Memo.Visible := not Preview;
          1: ; // Deixa como est�
          2: Memo.Visible := Preview;
        end;
      end
      else if (Report.Components[I] is TfrxLineView) and (Preview) then
        TfrxLineView(Report.Components[I]).Visible := False
      else if (Report.Components[I] is TfrxBarCodeView) and (Preview) then
        TfrxBarCodeView(Report.Components[I]).Visible := False
    end;
    DefineFrx(Report, ficMostra);
  finally
    //Report.Free;
  end;
end;

procedure TFmCTe_Pesq_0000.ImprimeSomenteadmin1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  BtImprimeClick(Self);
end;

procedure TFmCTe_Pesq_0000.Listadasnotaspesquisadas1Click(Sender: TObject);
var
  ide_nNF, ide_serie, cStat, Ordem, Status: Integer;
  ide_dEmi: String;
  ICMSTot_vProd, ICMSTot_vST, ICMSTot_vFrete,
  ICMSTot_vSeg, ICMSTot_vIPI, ICMSTot_vOutro,
  ICMSTot_vDesc, ICMSTot_vNF, ICMSTot_vBC,
  ICMSTot_vICMS, ICMSTot_vPIS, ICMSTot_vCOFINS: Double;
  NOME_tpEmis, NOME_tpNF, Motivo,
  ide_AAMM_AA, ide_AAMM_MM, Id, infCanc_dhRecbto, infCanc_nProt,
  ide_natOp, Ordem100: String;
  //
  esp, marca, nVol: String;
  qVol, PesoL, PesoB: Double;
begin
{ TODO : Incluir Notas inutilizadas }
{ *****
  Screen.Cursor := crHourGlass;
  try
    F_CTe_100 := GradeCriar.RecriaTempTableNovo(ntrttCTe_100, DmodG.QrUpdPID1, False);
    F_CTe_101 := GradeCriar.RecriaTempTableNovo(ntrttCTe_101, DmodG.QrUpdPID1, False);
    F_CTe_XXX := GradeCriar.RecriaTempTableNovo(ntrttCTe_XXX, DmodG.QrUpdPID1, False);
    //
    QrCTeCabA.First;
    while not QrCTeCabA.Eof do
    begin
      // 0..999
      cStat            := Trunc(QrCTeCabAcStat.Value);
      ide_nNF          := QrCTeCabAide_nNF.Value;
      ide_serie        := QrCTeCabAide_serie.Value;
      ide_dEmi         := Geral.FDT(QrCTeCabAide_dEmi.Value, 1);
      // 100
      ICMSTot_vProd    := QrCTeCabAICMSTot_vProd.Value;
      ICMSTot_vST      := QrCTeCabAICMSTot_vST.Value;
      ICMSTot_vFrete   := QrCTeCabAICMSTot_vFrete.Value;
      ICMSTot_vSeg     := QrCTeCabAICMSTot_vSeg.Value;
      ICMSTot_vIPI     := QrCTeCabAICMSTot_vIPI.Value;
      ICMSTot_vOutro   := QrCTeCabAICMSTot_vOutro.Value;
      ICMSTot_vDesc    := QrCTeCabAICMSTot_vDesc.Value;
      ICMSTot_vNF      := QrCTeCabAICMSTot_vNF.Value;
      ICMSTot_vBC      := QrCTeCabAICMSTot_vBC.Value;
      ICMSTot_vICMS    := QrCTeCabAICMSTot_vICMS.Value;
      ICMSTot_vPIS     := QrCTeCabAICMSTot_vPIS.Value;
      ICMSTot_vCOFINS  := QrCTeCabAICMSTot_vCOFINS.Value;
      NOME_tpEmis      := QrCTeCabANOME_tpEmis.Value;
      NOME_tpNF        := QrCTeCabANOME_tpNF.Value;
      ide_natOp        := QrCTeCabAide_natOp.Value;
      // 101
      ide_AAMM_AA      := Geral.FDT(QrCTeCabAide_dEmi.Value, 21);
      ide_AAMM_MM      := Geral.FDT(QrCTeCabAide_dEmi.Value, 22);
      Motivo           := QrCTeCabAinfCanc_xJust.Value;
      Id               := QrCTeCabAId.Value;
      infCanc_dhRecbto := Geral.FDT(QrCTeCabAinfCanc_dhRecbto.Value, 9);
      infCanc_nProt    := QrCTeCabAinfCanc_nProt.Value;
      // Outros
      Status           := QrCTeCabAStatus.Value;
      //
      // 2015-10-17
      UnDmkDAC_PF.AbreMySQLQuery0(QrCTeCabXVol, Dmod.MyDB, [
      'SELECT SUM(qVol) qVol, esp, marca,  ',
      'nVol, SUM(PesoL) PesoL, SUM(PesoB) PesoB  ',
      'FROM ctecabxvol ',
      'WHERE FatID=' + Geral.FF0(QrCTeCabAFatID.Value),
      'AND FatNum=' + Geral.FF0(QrCTeCabAFatNum.Value),
      'AND Empresa=' + Geral.FF0(QrCTeCabAEmpresa.Value),
      '']);
      qVol             := QrCTeCabXVolqVol.Value;
      esp              := QrCTeCabXVolesp.Value;
      marca            := QrCTeCabXVolmarca.Value;
      nVol             := QrCTeCabXVolnVol.Value;
      PesoL            := QrCTeCabXVolPesoL.Value;
      PesoB            := QrCTeCabXVolPesoB.Value;
      // Fim 2015-10-17
      case cStat of
        100:
        begin
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_CTe_100, True, [
          'Ordem',
          'ide_nNF', 'ide_serie', 'ide_dEmi',
          'ICMSTot_vProd', 'ICMSTot_vST', 'ICMSTot_vFrete',
          'ICMSTot_vSeg', 'ICMSTot_vIPI', 'ICMSTot_vOutro',
          'ICMSTot_vDesc', 'ICMSTot_vNF', 'ICMSTot_vBC',
          'ICMSTot_vICMS', 'ICMSTot_vPIS', 'ICMSTot_vCOFINS',
          'NOME_tpEmis', 'NOME_tpNF', 'ide_natOp',
          'qVol', 'esp', 'marca',
          'nVol', 'PesoL', 'PesoB'], [
          ], [
          0,
          ide_nNF, ide_serie, ide_dEmi,
          ICMSTot_vProd, ICMSTot_vST, ICMSTot_vFrete,
          ICMSTot_vSeg, ICMSTot_vIPI, ICMSTot_vOutro,
          ICMSTot_vDesc, ICMSTot_vNF, ICMSTot_vBC,
          ICMSTot_vICMS, ICMSTot_vPIS, ICMSTot_vCOFINS,
          NOME_tpEmis, NOME_tpNF, ide_natOp,
          qVol, esp, marca,
          nVol, PesoL, PesoB
          ], [
          ], False);
        end;
        101:
        begin
          Ordem := 0;
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_CTe_101, False, [
          'Ordem', 'ide_nNF',
          'ide_serie', 'ide_AAMM_AA', 'ide_AAMM_MM',
          'ide_dEmi', 'NOME_tpEmis', 'NOME_tpNF',
          'Id', 'infCanc_dhRecbto', 'infCanc_nProt', 'Motivo'], [
          ], [
          Ordem, ide_nNF,
          ide_serie, ide_AAMM_AA, ide_AAMM_MM,
          ide_dEmi, NOME_tpEmis, NOME_tpNF,
          Id, infCanc_dhRecbto, infCanc_nProt, Motivo], [
          ], False);
        end;
        else begin
          if Status < 100 then
            Ordem := 2
          else
            Ordem := 1;
          Motivo := CTe_PF.Texto_StatusCTe(Status, QrCTeCabAversao.Value);
          //
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_CTe_XXX, False, [
          'Ordem', 'cStat', 'ide_nNF',
          'ide_serie', 'ide_AAMM_AA', 'ide_AAMM_MM',
          'ide_dEmi', 'NOME_tpEmis', 'NOME_tpNF',
          'Status', 'Motivo'], [
          ], [
          Ordem, cStat, ide_nNF,
          ide_serie, ide_AAMM_AA, ide_AAMM_MM,
          ide_dEmi, NOME_tpEmis, NOME_tpNF,
          Status, Motivo], [
          ], False);
        end;
      end;
      //
      QrCTeCabA.Next;
    end;
    //
    if CkideNatOp.Checked then
      Ordem100 := 'ORDER BY ide_NatOp, ide_nNF'
    else
      Ordem100 := 'ORDER BY ide_nNF';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCTe_100, DModG.MyPID_DB, [
    'SELECT * FROM cte_100 ',
    Ordem100,
    '']);
    //
    QrCTe_101.Close;
    QrCTe_101.Database := DModG.MyPID_DB;
    QrCTe_101.Open;
    //
    QrCTe_XXX.Database := DModG.MyPID_DB;
    if CkideNatOp.Checked then
      MyObjects.frxMostra(frxListaCTesB, 'Lista de CT-e(s)')
    else
      MyObjects.frxMostra(frxListaCTes, 'Lista de CT-e(s)');
  finally
    Screen.Cursor := crDefault;
  end;
}
end;

procedure TFmCTe_Pesq_0000.CTe1Click(Sender: TObject);
begin
  Geral.MensagemBox(QrArqXML_CTe.Value, 'XML CTe', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmCTe_Pesq_0000.ObtemdadosInfDoc(var Tipo, Docu, SrNr: String);
begin
  case TCTeMyTpDocInf(Qr2InfDocMyTpDocInf.Value) of
    ctemtdiNFe:
    begin
      Tipo := 'NFe';
      Docu := Qr2InfDocInfNFe_chave.Value;
      SrNr := '';
    end;
    ctemtdiNFsOld:
    begin
      Tipo := Geral.FF0(Qr2InfDocInfNF_mod.Value);
      Docu := QrAREMET_CNPJ_CPF_TXT.Value;
      SrNr := Qr2InfDocInfNF_serie.Value + ' / ' + Qr2InfDocInfNF_nDoc.Value;
    end;
    ctemtdiOutrosDoc:
    begin
      Tipo := Geral.FF0(Qr2InfDocInfOutros_tpDoc.Value);
      Docu := Qr2InfDocInfOutros_descOutros.Value;
      SrNr := Qr2InfDocInfOutros_nDoc.Value;
    end;
    else
    begin
      Tipo := '';
      Docu := '';
      SrNr := '';
    end;
  end;
end;

procedure TFmCTe_Pesq_0000.PMArqPopup(Sender: TObject);
begin
  CTe1         .Enabled := QrArqXML_CTe.Value <> '';
  Autorizao1   .Enabled := QrArqXML_Aut.Value <> '';
  Cancelamento2.Enabled := QrArqXML_Can.Value <> '';
end;

procedure TFmCTe_Pesq_0000.porCFOP1Click(Sender: TObject);
begin
  ImprimeListaNova(True, False);
end;

procedure TFmCTe_Pesq_0000.porNatOp1Click(Sender: TObject);
begin
  ImprimeListaNova(False, False);
end;

procedure TFmCTe_Pesq_0000.PreviewdaCTe1Click(Sender: TObject);
begin
  ImprimeCTe(True);
end;

procedure TFmCTe_Pesq_0000.QrACalcFields(DataSet: TDataSet);
begin
  QrAId_TXT.Value := XXe_PF.FormataID_XXe(QrAId.Value);
  //
  QrAEMIT_ENDERECO.Value := QrAemit_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAemit_xLgr.Value,
    QrAemit_nro.Value, True) + ' ' + QrAemit_xCpl.Value + sLineBreak +
    QrAemit_xBairro.Value + sLineBreak + QrAemit_xMun.Value + ' - ' +
    QrAemit_UF.Value + sLineBreak +  'CEP: ' + Geral.FormataCEP_TT(IntToStr(
    QrAemit_CEP.Value))(* + '  ' + QrAemit_xPais.Value*);
  QrAEMIT_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAemit_fone.Value);
  //
  QrAEMIT_IE_TXT.Value := Geral.Formata_IE(QrAemit_IE.Value, QrAemit_UF.Value, '??');
  //if DModG.QrParamsEmpCTeShowURL.Value <> ''  then    /
  if QrOpcoesCTeCTeShowURL.Value <> ''  then
    QrAEMIT_ENDERECO.Value := QrAEMIT_ENDERECO.Value + sLineBreak +
      QrOpcoesCTeCTeShowURL.Value;
  QrAEMIT_CNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrAemit_CNPJ.Value);
  //
  //
    //
    ////// REMETENTE
    //
  //
  if QrACodInfoRemet.Value <> 0 then
  begin
    QrAREMET_NOME.Value := QrArem_xNome.Value;
    if QrARem_CNPJ.Value <> '' then
      QrAREMET_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrARem_CNPJ.Value)
    else
      QrAREMET_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrARem_CPF.Value);
    QrAREMET_ENDERECO.Value := QrARem_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
      QrARem_xLgr.Value, QrARem_nro.Value, True) + ' ' + QrARem_xCpl.Value;
    //if DModG.QrParamsEmpCTeMaiusc.Value = 1  then
    if QrOpcoesCTeCTeMaiusc.Value = 1 then
    begin
      QrAREMET_ENDERECO.Value := AnsiUpperCase(QrAREMET_ENDERECO.Value);
      QrAREMET_XMUN_TXT.Value := AnsiUpperCase(QrARem_xMun.Value);
      QrAREMET_XPAIS_TXT.Value := AnsiUpperCase(QrARem_xPais.Value);
    end else
    begin
      QrAREMET_XMUN_TXT.Value := QrARem_xMun.Value;
      QrAREMET_XPAIS_TXT.Value := QrARem_xPais.Value;
    end;
    QrAREMET_CEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrARem_CEP.Value));
    QrAREMET_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrARem_fone.Value);
    QrAREMET_IE_TXT.Value := Geral.Formata_IE(QrARem_IE.Value, QrARem_UF.Value, '??');
    //
    //
  end else
  begin
    QrAREMET_NOME.Value         := '';
    QrAREMET_CNPJ_CPF_TXT.Value := '';
    QrAREMET_ENDERECO.Value     := '';
    QrAREMET_CEP_TXT.Value      := '';
    QrAREMET_FONE_TXT.Value     := '';
    QrAREMET_IE_TXT.Value       := '';
    QrAREMET_XMUN_TXT.Value     := '';
    QrAREMET_XPAIS_TXT.Value    := '';
  end;
  //
    //
    ////// DESTINAT�RIO
    //
  //
    QrADEST_NOME.Value := QrAdest_xNome.Value;
  if QrAdest_CNPJ.Value <> '' then
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CNPJ.Value)
  else
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CPF.Value);
  QrADEST_ENDERECO.Value := QrAdest_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAdest_xLgr.Value, QrAdest_nro.Value, True) + ' ' + QrAdest_xCpl.Value;
  //if DModG.QrParamsEmpCTeMaiusc.Value = 1  then  /
  if QrOpcoesCTeCTeMaiusc.Value = 1  then
  begin
    QrADEST_ENDERECO.Value := AnsiUpperCase(QrADEST_ENDERECO.Value);
    QrADEST_XMUN_TXT.Value := AnsiUpperCase(QrAdest_xMun.Value);
    QrADEST_XPAIS_TXT.Value := AnsiUpperCase(QrADest_xPais.Value);
  end else
  begin
    QrADEST_XMUN_TXT.Value := QrAdest_xMun.Value;
    QrADEST_XPAIS_TXT.Value := QrADest_xPais.Value;
  end;
  QrADEST_CEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrAdest_CEP.Value));
  QrADEST_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAdest_fone.Value);
  //
  QrADEST_IE_TXT.Value := Geral.Formata_IE(QrAdest_IE.Value, QrAdest_UF.Value, '??');
  //
  //
    //
    ////// EXPEDIDOR
    //
  //
  if QrACodInfoExped.Value <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr1Exped, Dmod.MyDB, [
    'SELECT * ',
    'FROM ctecab1exped ',
    'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
    'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
    'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
    '']);
    QrAEXPED_NOME.Value := Qr1ExpedxNome.Value;
    if Qr1ExpedCNPJ.Value <> '' then
      QrAEXPED_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(Qr1ExpedCNPJ.Value)
    else
      QrAEXPED_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(Qr1ExpedCPF.Value);
    QrAEXPED_ENDERECO.Value := Qr1ExpedxLgr.Value + ', ' + Geral.FormataNumeroDeRua(
      Qr1ExpedxLgr.Value, Qr1Expednro.Value, True) + ' ' + Qr1ExpedxCpl.Value;
    //if DModG.QrParamsEmpCTeMaiusc.Value = 1  then
    if QrOpcoesCTeCTeMaiusc.Value = 1  then
    begin
      QrAEXPED_ENDERECO.Value := AnsiUpperCase(QrAEXPED_ENDERECO.Value);
      QrAEXPED_XMUN_TXT.Value := AnsiUpperCase(Qr1ExpedxMun.Value);
      QrAEXPED_XPAIS_TXT.Value := AnsiUpperCase(Qr1ExpedxPais.Value);
    end else
    begin
      QrAEXPED_XMUN_TXT.Value := Qr1ExpedxMun.Value;
      QrAEXPED_XPAIS_TXT.Value := Qr1ExpedxPais.Value;
    end;
    QrAEXPED_CEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(Qr1ExpedCEP.Value));
    QrAEXPED_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(Qr1Expedfone.Value);
    QrAEXPED_IE_TXT.Value := Geral.Formata_IE(Qr1ExpedIE.Value, Qr1ExpedUF.Value, '??');
    QrAEXPED_UF.Value := Qr1ExpedUF.Value;
    //
    //
  end else
  begin
    QrAEXPED_NOME.Value         := '';
    QrAEXPED_CNPJ_CPF_TXT.Value := '';
    QrAEXPED_ENDERECO.Value     := '';
    QrAEXPED_CEP_TXT.Value      := '';
    QrAEXPED_FONE_TXT.Value     := '';
    QrAEXPED_IE_TXT.Value       := '';
    QrAEXPED_XMUN_TXT.Value     := '';
    QrAEXPED_XPAIS_TXT.Value    := '';
  end;
  //
  //
    //
    ////// RECEBEDOR
    //
  //
  if QrACodInfoReceb.Value <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr1Receb, Dmod.MyDB, [
    'SELECT * ',
    'FROM ctecab1receb ',
    'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
    'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
    'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
    '']);
    QrARECEB_NOME.Value         := Qr1RecebxNome.Value;
    if Qr1RecebCNPJ.Value <> '' then
      QrARECEB_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(Qr1RecebCNPJ.Value)
    else
      QrARECEB_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(Qr1RecebCPF.Value);
    QrARECEB_ENDERECO.Value := Qr1RecebxLgr.Value + ', ' + Geral.FormataNumeroDeRua(
      Qr1RecebxLgr.Value, Qr1Recebnro.Value, True) + ' ' + Qr1RecebxCpl.Value;
    //if DModG.QrParamsEmpCTeMaiusc.Value = 1  then
    //if DModG.QrParamsEmpCTeMaiusc.Value = 1  then
    if QrOpcoesCTeCTeMaiusc.Value = 1  then
    begin
      QrARECEB_ENDERECO.Value := AnsiUpperCase(QrARECEB_ENDERECO.Value);
      QrARECEB_XMUN_TXT.Value := AnsiUpperCase(Qr1RecebxMun.Value);
      QrARECEB_XPAIS_TXT.Value := AnsiUpperCase(Qr1RecebxPais.Value);
    end else
    begin
      QrARECEB_XMUN_TXT.Value := Qr1RecebxMun.Value;
      QrARECEB_XPAIS_TXT.Value := Qr1RecebxPais.Value;
    end;
    QrARECEB_CEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(Qr1RecebCEP.Value));
    QrARECEB_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(Qr1Recebfone.Value);
    QrARECEB_IE_TXT.Value := Geral.Formata_IE(Qr1RecebIE.Value, Qr1RecebUF.Value, '??');
    QrARECEB_UF.Value := Qr1RecebUF.Value;
    //
    //
  end else
  begin
    QrARECEB_NOME.Value         := '';
    QrARECEB_CNPJ_CPF_TXT.Value := '';
    QrARECEB_ENDERECO.Value     := '';
    QrARECEB_CEP_TXT.Value      := '';
    QrARECEB_FONE_TXT.Value     := '';
    QrARECEB_IE_TXT.Value       := '';
    QrARECEB_XMUN_TXT.Value     := '';
    QrAEXPED_XPAIS_TXT.Value    := '';
  end;
  //
  //
    //
    ////// OUTRO (TOMA4)
    //
  //
  if QrACodInfoToma4.Value <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr2Toma4, Dmod.MyDB, [
    'SELECT * ',
    'FROM ctecab2toma04 ',
    'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
    'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
    'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
    '']);
    QrATOMA4_NOME.Value         := QrAtoma04_xNome.Value;
    if QrAtoma04_CNPJ.Value <> '' then
      QrATOMA4_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAtoma04_CNPJ.Value)
    else
      QrATOMA4_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAtoma04_CPF.Value);
    QrATOMA4_ENDERECO.Value := Qr2Toma4enderToma_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
      Qr2Toma4enderToma_xLgr.Value, Qr2Toma4enderToma_nro.Value, True) + ' ' + Qr2Toma4enderToma_xCpl.Value;
    //if DModG.QrParamsEmpCTeMaiusc.Value = 1  then
    if QrOpcoesCTeCTeMaiusc.Value = 1  then
    begin
      QrATOMA4_ENDERECO.Value := AnsiUpperCase(QrATOMA4_ENDERECO.Value);
      QrATOMA4_XMUN_TXT.Value := AnsiUpperCase(Qr2Toma4enderToma_xMun.Value);
      QrATOMA4_XPAIS_TXT.Value := AnsiUpperCase(Qr2Toma4enderToma_xPais.Value);
    end else
    begin
      QrATOMA4_XMUN_TXT.Value := Qr2Toma4enderToma_xMun.Value;
      QrATOMA4_XPAIS_TXT.Value := Qr2Toma4enderToma_xPais.Value;
    end;
    QrATOMA4_CEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(Qr2Toma4enderToma_CEP.Value));
    QrATOMA4_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAToma04_fone.Value);
    QrATOMA4_IE_TXT.Value := Geral.Formata_IE(QrAtoma04_IE.Value, Qr2Toma4enderToma_UF.Value, '??');
    QrATOMA4_UF.Value := Qr2Toma4enderToma_UF.Value;
    //
    //
  end else
  begin
    QrATOMA4_NOME.Value         := '';
    QrATOMA4_CNPJ_CPF_TXT.Value := '';
    QrATOMA4_ENDERECO.Value     := '';
    QrATOMA4_CEP_TXT.Value      := '';
    QrATOMA4_FONE_TXT.Value     := '';
    QrATOMA4_IE_TXT.Value       := '';
    QrATOMA4_XMUN_TXT.Value     := '';
    QrATOMA4_XPAIS_TXT.Value    := '';
  end;
    //
    //
  //
    //
    ////// TOMADOR
    //
  //
  case TCTeToma(QrAtoma99_toma.Value) of
    ctetomaRemetente:
    begin
      QrATOMAD_NOME.Value         := QrAREMET_NOME.Value;
      QrATOMAD_CNPJ_CPF_TXT.Value := QrAREMET_CNPJ_CPF_TXT.Value;
      QrATOMAD_ENDERECO.Value     := QrAREMET_ENDERECO.Value    ;
      QrATOMAD_CEP_TXT.Value      := QrAREMET_CEP_TXT.Value     ;
      QrATOMAD_FONE_TXT.Value     := QrAREMET_FONE_TXT.Value    ;
      QrATOMAD_IE_TXT.Value       := QrAREMET_IE_TXT.Value      ;
      QrATOMAD_XMUN_TXT.Value     := QrAREMET_XMUN_TXT.Value    ;
      QrATOMAD_XPAIS_TXT.Value    := QrAREMET_XPAIS_TXT.Value   ;
      QrATOMAD_UF.Value           := QrArem_UF.Value;
    end;
    ctetomaExpedidor:
    begin
      QrATOMAD_NOME.Value         := QrAEXPED_NOME.Value;
      QrATOMAD_CNPJ_CPF_TXT.Value := QrAEXPED_CNPJ_CPF_TXT.Value;
      QrATOMAD_ENDERECO.Value     := QrAEXPED_ENDERECO.Value    ;
      QrATOMAD_CEP_TXT.Value      := QrAEXPED_CEP_TXT.Value     ;
      QrATOMAD_FONE_TXT.Value     := QrAEXPED_FONE_TXT.Value    ;
      QrATOMAD_IE_TXT.Value       := QrAEXPED_IE_TXT.Value      ;
      QrATOMAD_XMUN_TXT.Value     := QrAEXPED_XMUN_TXT.Value    ;
      QrATOMAD_XPAIS_TXT.Value    := QrAEXPED_XPAIS_TXT.Value   ;
      QrATOMAD_UF.Value           := QrAEXPED_UF.Value;
    end;
    ctetomaRecebedor:
    begin
      QrATOMAD_NOME.Value         := QrARECEB_NOME.Value;
      QrATOMAD_CNPJ_CPF_TXT.Value := QrARECEB_CNPJ_CPF_TXT.Value;
      QrATOMAD_ENDERECO.Value     := QrARECEB_ENDERECO.Value    ;
      QrATOMAD_CEP_TXT.Value      := QrARECEB_CEP_TXT.Value     ;
      QrATOMAD_FONE_TXT.Value     := QrARECEB_FONE_TXT.Value    ;
      QrATOMAD_IE_TXT.Value       := QrARECEB_IE_TXT.Value      ;
      QrATOMAD_XMUN_TXT.Value     := QrARECEB_XMUN_TXT.Value    ;
      QrATOMAD_XPAIS_TXT.Value    := QrARECEB_XPAIS_TXT.Value   ;
      QrATOMAD_UF.Value           := QrARECEB_UF.Value;
    end;
    ctetomaDestinatario:
    begin
      QrATOMAD_NOME.Value         := QrADEST_NOME.Value;
      QrATOMAD_CNPJ_CPF_TXT.Value := QrADEST_CNPJ_CPF_TXT.Value;
      QrATOMAD_ENDERECO.Value     := QrADEST_ENDERECO.Value    ;
      QrATOMAD_CEP_TXT.Value      := QrADEST_CEP_TXT.Value     ;
      QrATOMAD_FONE_TXT.Value     := QrADEST_FONE_TXT.Value    ;
      QrATOMAD_IE_TXT.Value       := QrADEST_IE_TXT.Value      ;
      QrATOMAD_XMUN_TXT.Value     := QrADEST_XMUN_TXT.Value    ;
      QrATOMAD_XPAIS_TXT.Value    := QrADEST_XPAIS_TXT.Value   ;
      QrATOMAD_UF.Value           := QrAdest_UF.Value;
    end;
    ctetomaOutros:
    begin
      QrATOMAD_NOME.Value         := QrATOMA4_NOME.Value;
      QrATOMAD_CNPJ_CPF_TXT.Value := QrATOMA4_CNPJ_CPF_TXT.Value;
      QrATOMAD_ENDERECO.Value     := QrATOMA4_ENDERECO.Value    ;
      QrATOMAD_CEP_TXT.Value      := QrATOMA4_CEP_TXT.Value     ;
      QrATOMAD_FONE_TXT.Value     := QrATOMA4_FONE_TXT.Value    ;
      QrATOMAD_IE_TXT.Value       := QrATOMA4_IE_TXT.Value      ;
      QrATOMAD_XMUN_TXT.Value     := QrATOMA4_XMUN_TXT.Value    ;
      QrATOMAD_XPAIS_TXT.Value    := QrATOMA4_XPAIS_TXT.Value   ;
      QrATOMAD_UF.Value           := QrATOMA4_UF.Value;
    end;
  end;
  //
  //
  if QrAide_tpAmb.Value = 2 then
  begin
    QrADOC_SEM_VLR_JUR.Value :=
      'CT-e emitida em ambiente de homologa��o. N�O POSSUI VALIDADE JUR�DICA';
    if (QrAStatus.Value = 90) and (QrAeveEPEC_cStat.Value=136) then
      QrADOC_SEM_VLR_FIS.Value := 'Emiss�o Pr�via EPEC em Homologa��o SEM VALOR FISCAL'
    else
      QrADOC_SEM_VLR_FIS.Value := 'SEM VALOR FISCAL';
  end else
  begin
    QrADOC_SEM_VLR_JUR.Value := '';
    if (QrAStatus.Value = 90) and (QrAeveEPEC_cStat.Value=136) then
      QrADOC_SEM_VLR_FIS.Value :=
      'Impress�o em conting�ncia. Obrigat�ria a autoriza��o em 168 horas ap�s esta emiss�o ('
      + Geral.FDT(QrAeveEPEC_dhRegEvento.Value, 00) + ')'
    else
      QrADOC_SEM_VLR_FIS.Value := QrAinfProt_nProt.Value + ' ' + Geral.FDT(QrAinfProt_dhRecbto.Value, 00);
  end;
{

object QrATOMA4_CNPJ_CPF_TXT: TWideStringField
  FieldKind = fkCalculated
  FieldName = 'TOMA4_CNPJ_CPF_TXT'
  Size = 100
  Calculated = True
end
object QrATOMA4_ENDERECO: TWideStringField
  FieldKind = fkCalculated
  FieldName = 'TOMA4_ENDERECO'
  Size = 255
  Calculated = True
end
object QrATOMA4_CEP_TXT: TWideStringField
  FieldKind = fkCalculated
  FieldName = 'TOMA4_CEP_TXT'
  Calculated = True
end
object QrATOMA4_FONE_TXT: TWideStringField
  FieldKind = fkCalculated
  FieldName = 'TOMA4_FONE_TXT'
  Size = 50
  Calculated = True
end
object QrATOMA4_IE_TXT: TWideStringField
  FieldKind = fkCalculated
  FieldName = 'TOMA4_IE_TXT'
  Size = 50
  Calculated = True
end
object QrATOMA4_XMUN_TXT: TWideStringField
  FieldKind = fkCalculated
  FieldName = 'TOMA4_XMUN_TXT'
  Size = 60
  Calculated = True
end









object QrATRANSPORTA_CNPJ_CPF_TXT: TWideStringField
  FieldKind = fkCalculated
  FieldName = 'TRANSPORTA_CNPJ_CPF_TXT'
  Size = 50
  Calculated = True
end
object QrATRANSPORTA_IE_TXT: TWideStringField
  FieldKind = fkCalculated
  FieldName = 'TRANSPORTA_IE_TXT'
  Size = 50
  Calculated = True
end
object QrAide_DSaiEnt_Txt: TWideStringField
  FieldKind = fkCalculated
  FieldName = 'ide_DSaiEnt_Txt'
  Size = 10
  Calculated = True
end
object QrAMODFRETE_TXT: TWideStringField
  FieldKind = fkCalculated
  FieldName = 'MODFRETE_TXT'
  Calculated = True
end
}

end;

procedure TFmCTe_Pesq_0000.QrClientesBeforeClose(DataSet: TDataSet);
begin
  QrCTeCabAMsg.Close;
  BtLeArq.Enabled     := False;
  BtImprime.Enabled   := False;
  BtEnvia.Enabled     := False;
  BtEvento.Enabled    := False;
  BtInutiliza.Enabled := False;
  BtExclui.Enabled    := False;
end;

procedure TFmCTe_Pesq_0000.QrIAfterScroll(DataSet: TDataSet);
begin
{
  QrM.Close;
  QrM.Params[00].AsInteger := QrCTeCabAFatID.Value;
  QrM.Params[01].AsInteger := QrCTeCabAFatNum.Value;
  QrM.Params[02].AsInteger := QrCTeCabAEmpresa.Value;
  QrM.Params[03].AsInteger := QrInItem.Value;
  QrM.Open;
  //
  QrN.Close;
  QrN.Params[00].AsInteger := QrCTeCabAFatID.Value;
  QrN.Params[01].AsInteger := QrCTeCabAFatNum.Value;
  QrN.Params[02].AsInteger := QrCTeCabAEmpresa.Value;
  QrN.Params[03].AsInteger := QrInItem.Value;
  QrN.Open;
  //
  QrO.Close;
  QrO.Params[00].AsInteger := QrCTeCabAFatID.Value;
  QrO.Params[01].AsInteger := QrCTeCabAFatNum.Value;
  QrO.Params[02].AsInteger := QrCTeCabAEmpresa.Value;
  QrO.Params[03].AsInteger := QrInItem.Value;
  QrO.Open;
  //
  QrV.Close;
  QrV.Params[00].AsInteger := QrCTeCabAFatID.Value;
  QrV.Params[01].AsInteger := QrCTeCabAFatNum.Value;
  QrV.Params[02].AsInteger := QrCTeCabAEmpresa.Value;
  QrV.Params[03].AsInteger := QrInItem.Value;
  QrV.Open;
  //
}
end;

procedure TFmCTe_Pesq_0000.QrRodoVeicAfterScroll(DataSet: TDataSet);
begin
  ReopenCTeMoRodoProp();
end;

procedure TFmCTe_Pesq_0000.QrRodoVeicBeforeClose(DataSet: TDataSet);
begin
  QrRodoProp.Close;
end;

procedure TFmCTe_Pesq_0000.QrCTeCabAAfterOpen(DataSet: TDataSet);
begin
  PnDados.Visible    := True;
  BtConsulta.Enabled := QrCTeCabA.RecordCount > 0;
  SbImprime.Enabled  := QrCTeCabA.RecordCount > 0;
  SbNovo.Enabled     := QrCTeCabA.RecordCount > 0;
  if QrCTeCabA.recordCount > 0 then
    if not DmCTe_0000.ReopenOpcoesCTe(QrOpcoesCTe,
      QrCTeCabAEmpresa.Value, True) then Close;
end;

procedure TFmCTe_Pesq_0000.QrCTeCabAAfterScroll(DataSet: TDataSet);
begin
  BtLeArq.Enabled     := True;
  BtImprime.Enabled   := False;
  BtEnvia.Enabled     := False;
  BtEvento.Enabled    := False;
  BtInutiliza.Enabled := False;
  BtExclui.Enabled    := False;
  case Trunc(QrCTeCabAcStat.Value) of
    0..89, 91..99, 201..203, 205..999:  //No status 204 Rejei��o: Duplicidade de CT-e n�o deixar excluir
    begin
      BtExclui.Enabled := True;
      BtEvento.Enabled  := True;
    end;
    90:
    begin
      BtImprime.Enabled := True;
      BtEvento.Enabled  := False;
      BtEnvia.Enabled   := False;
    end;
    100:
    begin
      BtImprime.Enabled := True;
      BtEvento.Enabled  := True;
      BtEnvia.Enabled   := True;
    end;
    101:
    begin
      BtEnvia.Enabled   := True;
      BtEvento.Enabled  := True;
    end;
    //100: BtImprime.Enabled := True;
  end;
  ReopenCTeEveRRet();
  ReopenCTeCabAMsg();
end;

procedure TFmCTe_Pesq_0000.QrCTeCabABeforeClose(DataSet: TDataSet);
begin
  PnDados.Visible    := False;
  BtConsulta.Enabled := False;
  SbImprime.Enabled  := False;
  SbNovo.Enabled     := False;
  QrCTeCabAMsg.Close;
  QrCTeEveRRet.Close;
end;

procedure TFmCTe_Pesq_0000.QrCTeCabACalcFields(DataSet: TDataSet);
begin
  QrCTeCabANome_tpEmis.Value := sCTeTpEmis[QrCTeCabAide_tpEmis.Value];
  case QrCTeCabAide_tpCTe.Value of
    0: QrCTeCabANome_tpCTe.Value := 'E';
    1: QrCTeCabANome_tpCTe.Value := 'S';
    else QrCTeCabANome_tpCTe.Value := '?';
  end;
end;

procedure TFmCTe_Pesq_0000.QrCTeEveRRetCalcFields(DataSet: TDataSet);
begin
{ *****
  if QrCTeEveRRetret_xEvento.Value <> '' then
    QrCTeEveRRetNO_EVENTO.Value := QrCTeEveRRetret_xEvento.Value
  else
    QrCTeEveRRetNO_EVENTO.Value :=
      CTe_PF.Obtem_DeEvento_Nome(QrCTeEveRRetret_tpEvento.Value);
}
end;

procedure TFmCTe_Pesq_0000.QrCTeYItsCalcFields(DataSet: TDataSet);
begin
{
  QrCTeYItsxVenc1.Value := dmkPF.FDT_NULO(QrCTeYItsdVenc1.Value, 2);
  QrCTeYItsxVenc2.Value := dmkPF.FDT_NULO(QrCTeYItsdVenc2.Value, 2);
  QrCTeYItsxVenc3.Value := dmkPF.FDT_NULO(QrCTeYItsdVenc3.Value, 2);
  QrCTeYItsxVenc4.Value := dmkPF.FDT_NULO(QrCTeYItsdVenc4.Value, 2);
  QrCTeYItsxVenc5.Value := dmkPF.FDT_NULO(QrCTeYItsdVenc5.Value, 2);
}
end;

procedure TFmCTe_Pesq_0000.QrCTe_101CalcFields(DataSet: TDataSet);
begin
{ *****
  QrCTe_101Id_TXT.Value := XXe_PF.FormataID_XXe(QrCTe_101Id.Value);
}
end;

procedure TFmCTe_Pesq_0000.QrCTe_XXXCalcFields(DataSet: TDataSet);
begin
  case QrCTe_XXXOrdem.Value of
    0: QrCTe_XXXNOME_ORDEM.Value := 'CANCELADA';
    1: QrCTe_XXXNOME_ORDEM.Value := 'REJEITADA';
    2: QrCTe_XXXNOME_ORDEM.Value := 'N�O ENVIADA';
    else QrCTe_XXXNOME_ORDEM.Value := '? ? ? ? ?';
  end;
end;

procedure TFmCTe_Pesq_0000.QrXVolAfterOpen(DataSet: TDataSet);
begin
{
  Fesp    := '';
  Fmarca  := '';
  FnVol   := '';
  //
  FpesoL  := 0;
  FpesoB  := 0;
  FqVol   := 0;
  //
  while not QrXVol.Eof do
  begin
    if (QrXVolesp.Value <> '') and (Fesp <> '') then Fesp := Fesp + '/';
    Fesp    := Fesp    + QrXVolesp.Value;
    if (QrXVolmarca.Value <> '') and (Fmarca <> '') then Fmarca := Fmarca + '/';
    Fmarca  := Fmarca  + QrXVolmarca.Value;
    if (QrXVolnVol.Value <> '') and (FnVol <> '') then FnVol := FnVol + '/';
    FnVol   := FnVol   + QrXVolnVol.Value;
    //         //
    FpesoL  := FpesoL  + QrXVolpesoL.Value;
    FpesoB  := FpesoB  + QrXVolpesoB.Value;
    FqVol   := FqVol   + QrXVolqVol.Value;
    //
    QrXVol.Next;
  end;
}
end;

procedure TFmCTe_Pesq_0000.QrYAfterOpen(DataSet: TDataSet);
  function CorrigeDataVazio(Data: String): String;
  begin
    if Data = '' then
      Result := '0000-00-00'
    else
      Result := Data;
  end;
  function CorrigeNumeroVazio(Numero: String): String;
  begin
    if Numero = '' then
      Result := '0'
    else
      Result := Numero;
  end;
  function ProximoSeq(var nSeq: Integer): String;
  begin
    nSeq := nSeq + 1;
    Result := FormatFloat('0', nSeq);
  end;
const
  Fields = 03; // (nDup, dVencv, Dup) + 1 Seq  = 4 no total
  //MaxRow = 02;
  MaxCol = 14; // 5 grupos de 3 campos > 0..14 = 15 itens
  //
var
  Seq, MaxRow, Linha, Col, Row, ColsInRow: Integer;
  Faturas: array of array of String;
  Seq1, nDup1, dVenc1, vDup1,
  Seq2, nDup2, dVenc2, vDup2,
  Seq3, nDup3, dVenc3, vDup3,
  Seq4, nDup4, dVenc4, vDup4,
  Seq5, nDup5, dVenc5, vDup5: String;
begin
{
  ColsInRow := (MaxCol + 1) div Fields;
  // Aumentar para arredondar para cima
  MaxRow := QrY.RecordCount + ColsInRow - 1;
  // Total de linhas
  MaxRow := (MaxRow div ColsInRow) - 1; // > 0..?

  //
  //  Limpar vari�veis
  SetLength(Faturas, MaxCol + 1, MaxRow + 1);
  for Col := 0 to MaxCol do
    for Row := 0 to MaxRow do
      FFaturas[Col,Row] := '';

//begin
  //

  Col := 0;
  Row := 0;
  QrY.First;
  while not QrY.Eof do
  begin
    Faturas[Col, Row] :=  QrYnDup.Value;
    Col := Col + 1;
    Faturas[Col, Row] :=  Geral.FDT(QrYdVenc.Value, 1);
    Col := Col + 1;
    Faturas[Col, Row] :=  dmkPF.FFP(QrYvDup.Value, 2);
    //
    if Col = MaxCol then
    begin
      Col := 0;
      Row := Row + 1;
    end else
      Col := Col + 1;
    //
    QrY.Next;
  end;

  //

  F N F eYIts := UCriar.RecriaTempTable(' N F e YIts', DmodG.QrUpdPID1, False);

  Seq := 0;
  if MaxRow > -1 then
  begin
    for Row := 0 to MaxRow do
    begin
      Linha  := Row + 1;
      Seq1   := CorrigeNumeroVazio(ProximoSeq(Seq));
      nDup1  := Faturas[00, Row];
      dVenc1 := CorrigeDataVazio(Faturas[01, Row]);
      vDup1  := CorrigeNumeroVazio(Faturas[02, Row]);

      Seq2   := CorrigeNumeroVazio(ProximoSeq(Seq));
      nDup2  := Faturas[03, Row];
      dVenc2 := CorrigeDataVazio(Faturas[04, Row]);
      vDup2  := CorrigeNumeroVazio(Faturas[05, Row]);

      Seq3   := CorrigeNumeroVazio(ProximoSeq(Seq));
      nDup3  := Faturas[06, Row];
      dVenc3 := CorrigeDataVazio(Faturas[07, Row]);
      vDup3  := CorrigeNumeroVazio(Faturas[08, Row]);

      Seq4   := CorrigeNumeroVazio(ProximoSeq(Seq));
      nDup4  := Faturas[09, Row];
      dVenc4 := CorrigeDataVazio(Faturas[10, Row]);
      vDup4  := CorrigeNumeroVazio(Faturas[11, Row]);

      Seq5   := CorrigeNumeroVazio(ProximoSeq(Seq));
      nDup5  := Faturas[12, Row];
      dVenc5 := CorrigeDataVazio(Faturas[13, Row]);
      vDup5  := CorrigeNumeroVazio(Faturas[14, Row]);
      //
      //for Col := 0 to MaxCol do
      //if
      UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'nfeyits', False, [
        'Seq1', 'nDup1', 'dVenc1', 'vDup1',
        'Seq2', 'nDup2', 'dVenc2', 'vDup2',
        'Seq3', 'nDup3', 'dVenc3', 'vDup3',
        'Seq4', 'nDup4', 'dVenc4', 'vDup4',
        'Seq5', 'nDup5', 'dVenc5', 'vDup5',
        'Linha'], [
      ], [
        Seq1, nDup1, dVenc1, vDup1,
        Seq2, nDup2, dVenc2, vDup2,
        Seq3, nDup3, dVenc3, vDup3,
        Seq4, nDup4, dVenc4, vDup4,
        Seq5, nDup5, dVenc5, vDup5,
        Linha], [
      ], False);
    end;
  end else begin
    Seq1    := CorrigeNumeroVazio('');
    nDup1   := '';
    dVenc1  := CorrigeDataVazio('');
    vDup1   := CorrigeNumeroVazio('');
    Seq2    := CorrigeNumeroVazio('');
    nDup2   := '';
    dVenc2  := CorrigeDataVazio('');
    vDup2   := CorrigeNumeroVazio('');
    Seq3    := CorrigeNumeroVazio('');
    nDup3   := '';
    dVenc3  := CorrigeDataVazio('');
    vDup3   := CorrigeNumeroVazio('');
    Seq4    := CorrigeNumeroVazio('');
    nDup4   := '';
    dVenc4  := CorrigeDataVazio('');
    vDup4   := CorrigeNumeroVazio('');
    Seq5    := CorrigeNumeroVazio('');
    nDup5   := '';
    dVenc5  := CorrigeDataVazio('');
    vDup5   := CorrigeNumeroVazio('');
    Linha   := 1;
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'nfeyits', False, [
      'Seq1', 'nDup1', 'dVenc1', 'vDup1',
      'Seq2', 'nDup2', 'dVenc2', 'vDup2',
      'Seq3', 'nDup3', 'dVenc3', 'vDup3',
      'Seq4', 'nDup4', 'dVenc4', 'vDup4',
      'Seq5', 'nDup5', 'dVenc5', 'vDup5',
      'Linha'], [
    ], [
      Seq1, nDup1, dVenc1, vDup1,
      Seq2, nDup2, dVenc2, vDup2,
      Seq3, nDup3, dVenc3, vDup3,
      Seq4, nDup4, dVenc4, vDup4,
      Seq5, nDup5, dVenc5, vDup5,
      Linha], [
    ], False);
  end;
  QrCTeYIts.Close;
  QrCTeYIts.Open;
}
end;

procedure TFmCTe_Pesq_0000.FechaCTeCabA();
begin
  QrCTeCabA.Close;
end;

procedure TFmCTe_Pesq_0000.RGOrdem1Click(Sender: TObject);
begin
  FechaCTeCabA;
end;

procedure TFmCTe_Pesq_0000.RGOrdem2Click(Sender: TObject);
begin
  FechaCTeCabA;
end;

procedure TFmCTe_Pesq_0000.ReopenCTeCabA(IDCtrl: Integer; So_O_ID: Boolean);
var
  Empresa, DOC1, Ord2: String;
begin
  Screen.Cursor := crHourGlass;
  FSo_o_ID := So_O_ID;
  if (EdFilial.ValueVariant <> Null) and (EdFilial.ValueVariant <> 0) then
    Empresa := FormatFloat('0', DModG.QrEmpresasCodigo.Value)
  else Empresa := '';
  //

  QrCTeCabA.Close;
  if Empresa <> '' then
  begin
    QrCTeCabA.SQL.Clear;
    QrCTeCabA.SQL.Add('SELECT IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cli,');
    QrCTeCabA.SQL.Add('IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome) NO_Emi,');
    QrCTeCabA.SQL.Add('IF(nfa.FatID=' + Geral.FF0(VAR_FATID_0053) + ', ');
    QrCTeCabA.SQL.Add('IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome),');
    QrCTeCabA.SQL.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome)) NO_Terceiro,');
    QrCTeCabA.SQL.Add('nfa.FatID, nfa.FatNum, nfa.Empresa, nfa.LoteEnv, nfa.Id,');
    QrCTeCabA.SQL.Add('ide_natOp, ide_serie, ide_nCT, ide_dEmi, ide_tpCTe,');
    QrCTeCabA.SQL.Add('vPrest_vTPrest, vPrest_vRec, dest_CNPJ, dest_CPF, ');
    QrCTeCabA.SQL.Add('dest_xNome, ');
    QrCTeCabA.SQL.Add('IF(infCanc_cStat>0, infCanc_cStat, IF(infProt_cStat>0, ');
    QrCTeCabA.SQL.Add('infProt_cStat, Status)) + 0.000 cStat,');
    QrCTeCabA.SQL.Add('IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) + 0.000 cStat,');
    QrCTeCabA.SQL.Add('IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt,');
    QrCTeCabA.SQL.Add('IF(infCanc_cStat>0, infCanc_xMotivo, IF(infProt_cStat>0, ');
    QrCTeCabA.SQL.Add('infProt_xMotivo, IF(eveEPEC_cStat>0, eveEPEC_xMotivo, ""))) xMotivo,');
    QrCTeCabA.SQL.Add('IF(infCanc_cStat>0, ' +
    'DATE_FORMAT(infCanc_dhRecbto, "%d/%m/%Y %H:%i:%S"), ' +
    'DATE_FORMAT(infProt_dhRecbto, "%d/%m/%Y %H:%i:%S")) dhRecbto,');
    QrCTeCabA.SQL.Add('IDCtrl, versao, ');
    QrCTeCabA.SQL.Add('ide_tpEmis, infCanc_xJust, Status, ');
    QrCTeCabA.SQL.Add('ICMS_vICMS, ');
    QrCTeCabA.SQL.Add('cSitCTe, ');
    QrCTeCabA.SQL.Add('nfa.infCanc_dhRecbto, nfa.infCanc_nProt, ');
    QrCTeCabA.SQL.Add('nfa.ide_Modal');
    QrCTeCabA.SQL.Add('FROM ctecaba nfa');
    QrCTeCabA.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=nfa.CodInfoTomad');
    QrCTeCabA.SQL.Add('LEFT JOIN entidades emi ON emi.Codigo=nfa.CodInfoEmite');
    QrCTeCabA.SQL.Add('');
    if FSo_O_ID then
    begin
      QrCTeCabA.SQL.Add('WHERE IDCtrl=' + FormatFloat('0', IDCtrl));
      QrCTeCabA.Open;
    // Fim 2011-08-24
    end else begin
      QrCTeCabA.SQL.Add(dmkPF.SQL_Periodo('WHERE ide_dEmi ', TPDataI.Date, TPDataF.Date, True, True));
      QrCTeCabA.SQL.Add('AND nfa.Empresa = ' + Empresa);
      case RGQuemEmit.ItemIndex of
        0: QrCTeCabA.SQL.Add('AND nfa.emit_CNPJ="' + DModG.QrEmpresasCNPJ_CPF.Value + '"');
        1: QrCTeCabA.SQL.Add('AND (nfa.emit_CNPJ<>"' + DModG.QrEmpresasCNPJ_CPF.Value + '" OR nfa.emit_CNPJ IS NULL)');
      end;
      if Ck100e101.Checked  then
        QrCTeCabA.SQL.Add('AND (IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat)) in (100,101)');
      case RGAmbiente.ItemIndex of
        0: ; // Tudo
        1: QrCTeCabA.SQL.Add('AND nfa.ide_tpAmb=1');
        2: QrCTeCabA.SQL.Add('AND nfa.ide_tpAmb=2');
      end;
      if EdCliente.ValueVariant <> 0 then
      begin
        if QrClientesTipo.Value = 0 then
        begin
          Doc1 := Geral.TFD(QrClientesCNPJ.Value, 14, siPositivo);
          QrCTeCabA.SQL.Add('AND dest_CNPJ="' + Doc1 + '"');
        end else begin
          Doc1 := Geral.TFD(QrClientesCPF.Value, 11, siPositivo);
          QrCTeCabA.SQL.Add('AND dest_CPF="' + Doc1 + '"');
        end;
      end;
      case RGOrdem2.ItemIndex of
        0: Ord2 := '';
        1: Ord2 := 'DESC';
      end;
      case RGOrdem1.ItemIndex of
        0: QrCTeCabA.SQL.Add('ORDER BY nfa.ide_dEmi ' + Ord2 + ', nfa.ide_nCT ' + Ord2);
        1: QrCTeCabA.SQL.Add('ORDER BY nfa.ide_nCT ' + Ord2 + ', nfa.ide_dEmi ' + Ord2 );
        2: QrCTeCabA.SQL.Add('ORDER BY cStat ' + Ord2 + ', nfa.ide_nNF ' + Ord2);
        3: QrCTeCabA.SQL.Add('ORDER BY cStat ' + Ord2 + ', nfa.ide_dEmi ' + Ord2);
      end;
      UnDmkDAC_PF.AbreQuery(QrCTeCabA, Dmod.MyDB);
      //Geral.MB_SQL(Self, QrCTeCabA);
      QrCTeCabA.Locate('IDCtrl', IDCtrl, []);
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmCTe_Pesq_0000.ReopenCTeCabAMsg;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeCabAMsg, Dmod.MyDB, [
  'SELECT * ' ,
  'FROM ctecabamsg ' ,
  'WHERE FatID=' + Geral.FF0(QrCTeCabAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrCTeCabAFatID.Value),
  'AND Empresa=' + Geral.FF0(QrCTeCabAFatID.Value),
  'ORDER BY Controle DESC ',
  '']);
(*
  QrCTeCabAMsg.Close;
  QrCTeCabAMsg.Params[00].AsInteger := QrCTeCabAFatID.Value;
  QrCTeCabAMsg.Params[01].AsInteger := QrCTeCabAFatNum.Value;
  QrCTeCabAMsg.Params[02].AsInteger := QrCTeCabAEmpresa.Value;
  QrCTeCabAMsg.Open;
*)
end;

procedure TFmCTe_Pesq_0000.ReopenCTeEveRRet();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeEveRRet, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteeverret',
  'WHERE ret_chCTe="' + QrCTeCabAId.Value + '" ',
  'ORDER BY ret_dhRegEvento DESC',
  '']);
end;

procedure TFmCTe_Pesq_0000.ReopenCTeIt2Comp;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr2Comp, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteit2comp ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmCTe_Pesq_0000.ReopenCTeIt2InfDoc();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr2InfDoc, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteit2infdoc ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmCTe_Pesq_0000.ReopenCTeIt2ObsCont();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr2ObsCont, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteit2obscont ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmCTe_Pesq_0000.ReopenCTeIt2ObsFisco();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr2ObsFisco, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteit2obsfisco ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmCTe_Pesq_0000.ReopenCTeIt2Seg();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr2Seg, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteit2seg ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmCTe_Pesq_0000.ReopenCTeIt3InfQ();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr3InfQ, Dmod.MyDB, [
  'SELECT * ',
  'FROM cteit3infq ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmCTe_Pesq_0000.ReopenCTeMoRodoCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRodoCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctemorodocab ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmCTe_Pesq_0000.ReopenCTeMoRodoLacr();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRodoLacr, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctemorodolacr ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmCTe_Pesq_0000.ReopenCTeMoRodoMoto();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRodoMoto, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctemorodomoto ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmCTe_Pesq_0000.ReopenCTeMoRodoProp();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRodoProp, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctemorodoprop ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  'AND Controle=' + Geral.FF0(QrRodoVeicControle.Value),
  'AND Conta=' + Geral.FF0(QrRodoVeicConta.Value),
  '']);
end;

procedure TFmCTe_Pesq_0000.ReopenCTeMoRodoVeic();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRodoVeic, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctemorodoveic ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmCTe_Pesq_0000.ReopenCTeMoRodoVPed();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRodoVPed, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctemorodovped ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmCTe_Pesq_0000.RGAmbienteClick(Sender: TObject);
begin
  FechaCTeCabA;
end;

procedure TFmCTe_Pesq_0000.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmCTe_Pesq_0000.TPDataIChange(Sender: TObject);
begin
  FechaCTeCabA;
end;

procedure TFmCTe_Pesq_0000.TPDataIClick(Sender: TObject);
begin
  FechaCTeCabA;
end;

{
infAdProd na descri��o do produto
informa��es adicionais nas complementares
QrV - fazer Informa��es adicionais


No evento OnCalcFields da Query QrACalcFields: Modificar �QrADOC_SEM_VLR_FIS.Value := QrAinfProt_nProt.Value;� para �QrADOC_SEM_VLR_FIS.Value := QrAinfProt_nProt.Value + ' ' + Geral.FDT(QrAinfProt_dhRecbto.Value, 00);�
No componentes frxA4A_002, frxReport1, frxA4A_000, frxA4A_001 nos labels que mostram o �Protocolo de autoriza��o de uso� mudar a fonte de 12px para 10px;
}

{
object QrOpcoesCTeNO_ETC_0: TWideStringField
  FieldName = 'NO_ETC_0'
  Size = 30
end
object QrOpcoesCTeNO_ETC_1: TWideStringField
  FieldName = 'NO_ETC_1'
  Size = 30
end
}

end.

