unit CTeIt1AutXml_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGrid, Clipbrd, Variants, dmkDBGridZTO, UnDmkEnums,
  dmkRadioGroup, UnDmkProcFunc;

type
  TFmCTeIt1AutXml_0000 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    QrCTeIt1AutXml: TmySQLQuery;
    DsCTeIt1AutXml: TDataSource;
    PnDados: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    DBGrid1: TdmkDBGridZTO;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnEdita: TPanel;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel8: TPanel;
    BtDesiste: TBitBtn;
    Panel6: TPanel;
    Label7: TLabel;
    EdControle: TdmkEdit;
    Panel5: TPanel;
    RGTipo: TdmkRadioGroup;
    PnCPF: TPanel;
    Label8: TLabel;
    EdCPF: TdmkEdit;
    PnCNPJ: TPanel;
    Label11: TLabel;
    EdCNPJ: TdmkEdit;
    QrCTeIt1AutXmlFatID: TIntegerField;
    QrCTeIt1AutXmlFatNum: TIntegerField;
    QrCTeIt1AutXmlEmpresa: TIntegerField;
    QrCTeIt1AutXmlControle: TIntegerField;
    QrCTeIt1AutXmlAddForma: TSmallintField;
    QrCTeIt1AutXmlAddIDCad: TSmallintField;
    QrCTeIt1AutXmlTipo: TSmallintField;
    QrCTeIt1AutXmlCNPJ: TWideStringField;
    QrCTeIt1AutXmlCPF: TWideStringField;
    QrCTeIt1AutXmlLk: TIntegerField;
    QrCTeIt1AutXmlDataCad: TDateField;
    QrCTeIt1AutXmlDataAlt: TDateField;
    QrCTeIt1AutXmlUserCad: TIntegerField;
    QrCTeIt1AutXmlUserAlt: TIntegerField;
    QrCTeIt1AutXmlAlterWeb: TSmallintField;
    QrCTeIt1AutXmlAtivo: TSmallintField;
    QrCTeIt1AutXmlCNPJ_CPF: TWideStringField;
    QrCTeIt1AutXmlITEM: TIntegerField;
    QrCTeIt1AutXmlTpDOC: TWideStringField;
    QrCTeIt1AutXmlNO_AddForma: TWideStringField;
    QrCTeIt1AutXmlNO_ENT_ECO: TWideStringField;
    SbEntidade: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrCTeIt1AutXmlAfterOpen(DataSet: TDataSet);
    procedure QrCTeIt1AutXmlBeforeClose(DataSet: TDataSet);
    procedure RGTipoClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrCTeIt1AutXmlCalcFields(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbEntidadeClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    function AtualizaListaContatos(): Boolean;
  public
    { Public declarations }
    FInserePadroes: Boolean;
    FFatID, FFatNum, FEmpresa, FDestRem: Integer;
    //
    procedure ReopenCTeIt1AutXml(Controle: Integer);
  end;

  var
  FmCTeIt1AutXml_0000: TFmCTeIt1AutXml_0000;

implementation

uses UnMyObjects, Module, UMySQLModule, CfgCadLista, UnGOTOy, ModuleGeral;

{$R *.DFM}

procedure TFmCTeIt1AutXml_0000.BtAlteraClick(Sender: TObject);
begin
  if QrCTeIt1AutXml.RecordCount > 0 then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCTeIt1AutXml, [PnDados],
    [PnEdita], RGTipo, ImgTipo,  'cteit1autxml');
  end;
end;

procedure TFmCTeIt1AutXml_0000.BtConfirmaClick(Sender: TObject);
var
  CNPJ, CPF: String;
  FatID, FatNum, Empresa, Controle, AddForma, AddIDCad, Tipo: Integer;
begin
  FatID    := FFatID;
  FatNum   := FFatNum;
  Empresa  := FEmpresa;
  Controle := EdControle.ValueVariant;
  Tipo     := RGTipo.ItemIndex;
  AddForma := Integer(TNFeautXML.naxAvulso);
  AddIDCad := 0;
  //
  if Tipo = 0 then
  begin
    CNPJ := Geral.SoNumero_TT(EdCNPJ.Text);
    CPF  := '';
    //
    if MyObjects.FIC(CNPJ = '', EdCNPJ, 'Informe o CNPJ!') then
      Exit;
    if MyObjects.FIC(Length(CNPJ) <> 14, EdCNPJ, 'O CNPJ deve conter 14 algarismos!') then
      Exit;
  end else begin
    CNPJ := '';
    CPF  := Geral.SoNumero_TT(EdCPF.Text);
    //
    if MyObjects.FIC(CPF = '', EdCPF, 'Informe o CPF!') then
      Exit;
    if MyObjects.FIC(Length(CPF) <> 11, EdCPF, 'O CPF deve conter 11 algarismos!') then
      Exit;
  end;
  //
  Controle := UMyMod.BPGS1I32('cteit1autxml', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'cteit1autxml', False, [
    'FatID', 'FatNum', 'Empresa',
    'AddForma', 'AddIDCad', 'Tipo',
    'CNPJ', 'CPF'], [
    'Controle'], [
    FatID, FatNum, Empresa,
    AddForma, AddIDCad, Tipo,
    CNPJ, CPF], [
    Controle], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    //
    ReopenCTeIt1AutXml(Controle);
  end;
end;

procedure TFmCTeIt1AutXml_0000.BtDesisteClick(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
end;

procedure TFmCTeIt1AutXml_0000.BtExcluiClick(Sender: TObject);
var
  Controle: Integer;
begin
  if QrCTeIt1AutXml.RecordCount > 0 then
  begin
    if UMyMod.ExcluiRegistroInt1('Confirma a retirada do setor selecionado?',
    'cteit1autxml', 'Controle', QrCTeIt1AutXmlControle.Value, Dmod.MyDB) = ID_YES then
    begin
      Controle := GOTOy.LocalizaPriorNextIntQr(QrCTeIt1AutXml,
        QrCTeIt1AutXmlControle, QrCTeIt1AutXmlControle.Value);
      ReopenCTeIt1AutXml(Controle);
    end;
  end;
end;

procedure TFmCTeIt1AutXml_0000.BtIncluiClick(Sender: TObject);
begin
  if QrCTeIt1AutXml.RecordCount < 10 then
  begin
    UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCTeIt1AutXml, [PnDados],
    [PnEdita], RGTipo, ImgTipo,  'cteit1autxml');
  end else
    Geral.MB_Aviso('Limite de itens (10) atingido!');
end;

procedure TFmCTeIt1AutXml_0000.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCTeIt1AutXml_0000.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.CorIniComponente();
end;

procedure TFmCTeIt1AutXml_0000.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Align   := alClient;
end;

procedure TFmCTeIt1AutXml_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCTeIt1AutXml_0000.FormShow(Sender: TObject);
begin
  if FInserePadroes then
  begin
    SbEntidade.Visible := True;
    //
    Screen.Cursor := crHourGlass;
    try
      if not AtualizaListaContatos() then
        Geral.MB_Aviso('Falha ao atualizar lista de contatos!');
    finally
      Screen.Cursor := crDefault;
    end;
  end else
    SbEntidade.Visible := False;
end;

function TFmCTeIt1AutXml_0000.AtualizaListaContatos(): Boolean;
var
  Qry, QryLoc: TmySQLQuery;
  SQLCompl, CPF, CNPJ: String;
  Tipo: Integer;

  procedure InsereAtual();
  var
    CNPJ, CPF: String;
    Controle, AddForma, AddIDCad, Tipo: Integer;
  begin
    Controle := 0;
    Tipo     := Qry.FieldByName('Tipo').AsInteger;
    AddForma := Integer(TNFeautXML.naxEntiContat); // EntiContat.Controle
    AddIDCad := Qry.FieldByName('Controle').AsInteger;
    //
    if Tipo = 0 then
    begin
      CNPJ := Geral.SoNumero_TT(Qry.FieldByName('CNPJ').AsString);
      CPF  := '';
      //
      if MyObjects.FIC(Length(CNPJ) <> 14, nil,
      'O CNPJ ' + CNPJ + ' n�o foi inclu�do pois n�p possui 14 algarismos!') then
        Exit;
    end else begin
      CNPJ := '';
      CPF  := Geral.SoNumero_TT(Qry.FieldByName('CPF').AsString);
      //
      if MyObjects.FIC(Length(CPF) <> 11, nil,
      'O CPF ' + CPF + ' n�o foi inclu�do pois n�p possui 11 algarismos!') then
        Exit;
    end;
    //
    Controle := UMyMod.BPGS1I32('cteit1autxml', 'Controle', '', '', tsPos, stIns, Controle);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'cteit1autxml', False, [
      'CNPJ', 'CPF', 'AddForma',
      'AddIDCad', 'Tipo'], [
      'FatID', 'FatNum', 'Empresa', 'Controle'], [
      CNPJ, CPF, AddForma,
      AddIDCad, Tipo], [
      FFatID, FFatNum, FEmpresa, Controle], True);
  end;

begin
  try
    Result := True;
    //
    Qry    := TmySQLQuery.Create(Dmod);
    QryLoc := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Controle, Tipo, CNPJ, CPF ',
        'FROM enticontat ',
        'WHERE Codigo=' + Geral.FF0(FDestRem),
        'AND Aplicacao & 1 ',
        'And Ativo=1 ',
        'LIMIT 10 ',
        '']);
      if Qry.RecordCount = 0 then
        Geral.MB_Aviso(
          'Destinat�rio / remetente sem emails cadastrados para autoriza��o de obten��o de XML!')
      else
      begin
        Qry.First;
        //
        while not Qry.Eof do
        begin
          Tipo := Qry.FieldByName('Tipo').AsInteger;
          CPF  := Qry.FieldByName('CPF').AsString;
          CNPJ := Qry.FieldByName('CNPJ').AsString;
          //
          if Tipo = 0 then
            SQLCompl := 'AND CNPJ="' + CNPJ + '"'
          else
            SQLCompl := 'AND CPF="' + CPF + '"';
          //
          UnDmkDAC_PF.AbreMySQLQuery0(QryLoc, Dmod.MyDB, [
            'SELECT * FROM cteit1autxml ',
            'WHERE FatID=' + Geral.FF0(FFatID),
            'AND FatNum=' + Geral.FF0(FFatNum),
            'AND Empresa=' + Geral.FF0(FEmpresa),
            SQLCompl,
            '']);
          if QryLoc.RecordCount = 0 then
            InsereAtual();
          //
          Qry.Next;
        end;
        ReopenCTeIt1AutXml(0);
      end;
    finally
      Qry.Free;
      QryLoc.Free;
    end;
  except
    Result := False;
  end;
end;

procedure TFmCTeIt1AutXml_0000.QrCTeIt1AutXmlAfterOpen(DataSet: TDataSet);
begin
  BtInclui.Enabled := QrCTeIt1AutXml.RecordCount < 10;
end;

procedure TFmCTeIt1AutXml_0000.QrCTeIt1AutXmlBeforeClose(DataSet: TDataSet);
begin
  BtInclui.Enabled := False;
end;

procedure TFmCTeIt1AutXml_0000.QrCTeIt1AutXmlCalcFields(DataSet: TDataSet);
begin
  QrCTeIt1AutXmlITEM.Value := QrCTeIt1AutXml.RecNo;
end;

procedure TFmCTeIt1AutXml_0000.ReopenCTeIt1AutXml(Controle: Integer);
var
  ATT_AddForma: String;
begin
  ATT_AddForma := dmkPF.ArrayToTexto('cga.AddForma', 'NO_AddForma', pvPos, True,
    sNFeautXML);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTeIt1AutXml, Dmod.MyDB, [
  'SELECT cga.*, ',
  ATT_AddForma,
  'IF(cga.Tipo=0, "CNPJ", "CPF") TpDOC, ',
  'CASE cga.AddForma ',
  '  WHEN 0 THEN "(N/I)"  ',
  '  WHEN 1 THEN eco.Nome ',
  '  WHEN 2 THEN IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ',
  '  WHEN 3 THEN " "  ',
  '  ELSE "???" END NO_ENT_ECO, ',
  'IF(cga.Tipo=0, cga.CNPJ, cga.CPF) CNPJ_CPF ',
  'FROM cteit1autxml cga ',
  'LEFT JOIN entidades ent ON ent.Codigo=cga.AddIDCad',
  'LEFT JOIN enticontat eco ON eco.Controle=cga.AddIDCad',
  'WHERE cga.FatID=' + Geral.FF0(FFatID),
  'AND cga.FatNum=' + Geral.FF0(FFatNum),
  'AND cga.Empresa=' + Geral.FF0(FEmpresa),
  '']);
  //
  if Controle <> 0 then
    QrCTeIt1AutXml.Locate('Controle', Controle, []);
end;

procedure TFmCTeIt1AutXml_0000.RGTipoClick(Sender: TObject);
begin
  PnCNPJ.Visible := RGTipo.ItemIndex = 0;
  PnCPF.Visible  := RGTipo.ItemIndex = 1;
end;

procedure TFmCTeIt1AutXml_0000.SbEntidadeClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(FDestRem, fmcadEntidade2, fmcadEntidade2);
  //
  Screen.Cursor := crHourGlass;
  try
    if not AtualizaListaContatos() then
      Geral.MB_Aviso('Falha ao atualizar lista de contatos!');
  finally
    Screen.Cursor := crDefault;
  end;
  //
  ReopenCTeIt1AutXml(0);
end;

end.
