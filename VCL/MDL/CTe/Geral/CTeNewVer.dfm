object FmCTeNewVer: TFmCTeNewVer
  Left = 339
  Top = 185
  Caption = 'CTe-LAYOU-003 :: Migra'#231#227'o de Vers'#227'o de Layout de NF-e'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 475
        Height = 32
        Caption = 'Migra'#231#227'o de Vers'#227'o de Layout de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 475
        Height = 32
        Caption = 'Migra'#231#227'o de Vers'#227'o de Layout de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 475
        Height = 32
        Caption = 'Migra'#231#227'o de Vers'#227'o de Layout de NF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 455
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 455
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 812
        Height = 455
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Grupos'
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 804
            Height = 427
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object DBGrid1: TDBGrid
              Left = 0
              Top = 0
              Width = 804
              Height = 427
              Align = alClient
              DataSource = DsNFeLayC
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDblClick = DBGrid1DblClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Grupo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 731
                  Visible = True
                end>
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Itens'
          ImageIndex = 1
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 804
            Height = 427
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 804
              Height = 50
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label1: TLabel
                Left = 8
                Top = 8
                Width = 72
                Height = 13
                Caption = 'Vers'#227'o Origem:'
              end
              object Label2: TLabel
                Left = 92
                Top = 8
                Width = 73
                Height = 13
                Caption = 'Vers'#227'o destino:'
              end
              object Label3: TLabel
                Left = 476
                Top = 4
                Width = 58
                Height = 13
                Caption = 'Filtrar grupo:'
              end
              object EdVerOrig: TdmkEdit
                Left = 8
                Top = 24
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '2,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 2.000000000000000000
                ValWarn = False
              end
              object EdVerDest: TdmkEdit
                Left = 92
                Top = 24
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '3,10'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 3.100000000000000000
                ValWarn = False
              end
              object BtCopiar: TBitBtn
                Tag = 14
                Left = 348
                Top = 4
                Width = 120
                Height = 40
                Caption = '&Copiar'
                NumGlyphs = 2
                TabOrder = 2
                OnClick = BtCopiarClick
              end
              object CkNaoSobrepor: TdmkCheckBox
                Left = 184
                Top = 24
                Width = 157
                Height = 17
                Caption = 'N'#227'o sobrepor existentes.'
                Checked = True
                State = cbChecked
                TabOrder = 3
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
              object BtReopen: TBitBtn
                Tag = 14
                Left = 544
                Top = 4
                Width = 120
                Height = 40
                Caption = '&Reabrir'
                NumGlyphs = 2
                TabOrder = 4
                OnClick = BtReopenClick
              end
              object EdPsqGrupo: TdmkEdit
                Left = 476
                Top = 20
                Width = 61
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'A'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'A'
                ValWarn = False
              end
            end
            object dmkDBGridZTO1: TdmkDBGridZTO
              Left = 0
              Top = 50
              Width = 804
              Height = 377
              Align = alClient
              DataSource = DsDest
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 1
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Grupo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SubGrupo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ID'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Campo'
                  Width = 69
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Width = 538
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Elemento'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pai'
                  Width = 53
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Tipo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'OcorMin'
                  Width = 46
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'OcorMax'
                  Width = 51
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TamMin'
                  Width = 46
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TamMax'
                  Width = 50
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TamVar'
                  Width = 51
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DeciCasas'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Observacao'
                  Width = 23
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'LeftZeros'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'InfoVazio'
                  Width = 53
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'FormatStr'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Versao'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CodigoN'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CodigoX'
                  Width = 50
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AlterWeb'
                  Width = 49
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Width = 27
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 503
    Width = 812
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 22
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object PB: TProgressBar
      Left = 2
      Top = 37
      Width = 808
      Height = 17
      Align = alBottom
      TabOrder = 1
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 11
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Editar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrOrigI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfelayi'
      'WHERE Versao=2.0')
    Left = 36
    Top = 172
    object QrOrigIGrupo: TWideStringField
      FieldName = 'Grupo'
      Size = 2
    end
    object QrOrigICodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 11
    end
    object QrOrigIID: TWideStringField
      FieldName = 'ID'
      Size = 7
    end
    object QrOrigICampo: TWideStringField
      FieldName = 'Campo'
      Size = 30
    end
    object QrOrigIDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrOrigIElemento: TWideStringField
      FieldName = 'Elemento'
      Size = 10
    end
    object QrOrigIPai: TWideStringField
      FieldName = 'Pai'
      Size = 10
    end
    object QrOrigITipo: TWideStringField
      FieldName = 'Tipo'
      Size = 1
    end
    object QrOrigIOcorMin: TSmallintField
      FieldName = 'OcorMin'
    end
    object QrOrigIOcorMax: TIntegerField
      FieldName = 'OcorMax'
    end
    object QrOrigITamMin: TSmallintField
      FieldName = 'TamMin'
    end
    object QrOrigITamMax: TIntegerField
      FieldName = 'TamMax'
    end
    object QrOrigITamVar: TWideStringField
      FieldName = 'TamVar'
      Size = 30
    end
    object QrOrigIDeciCasas: TSmallintField
      FieldName = 'DeciCasas'
    end
    object QrOrigIObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOrigIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOrigIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOrigILeftZeros: TSmallintField
      FieldName = 'LeftZeros'
    end
    object QrOrigIInfoVazio: TSmallintField
      FieldName = 'InfoVazio'
    end
    object QrOrigIFormatStr: TWideStringField
      FieldName = 'FormatStr'
      Size = 30
    end
    object QrOrigIVersao: TFloatField
      FieldName = 'Versao'
    end
    object QrOrigICodigoN: TFloatField
      FieldName = 'CodigoN'
    end
    object QrOrigICodigoX: TWideStringField
      FieldName = 'CodigoX'
      Size = 10
    end
  end
  object DsDest: TDataSource
    DataSet = TbDest
    Left = 36
    Top = 264
  end
  object TbDest: TmySQLTable
    Database = Dmod.MyDB
    Filter = 'Versao=2.00'
    IndexName = 'UNIQUE1'
    TableName = 'nfelayi'
    Left = 36
    Top = 220
    object TbDestGrupo: TWideStringField
      FieldName = 'Grupo'
      Required = True
      Size = 2
    end
    object TbDestSubGrupo: TWideStringField
      FieldName = 'SubGrupo'
    end
    object TbDestCodigo: TWideStringField
      FieldName = 'Codigo'
      Required = True
      Size = 11
    end
    object TbDestID: TWideStringField
      FieldName = 'ID'
      Required = True
      Size = 7
    end
    object TbDestCampo: TWideStringField
      FieldName = 'Campo'
      Required = True
      Size = 30
    end
    object TbDestDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object TbDestElemento: TWideStringField
      FieldName = 'Elemento'
      Size = 10
    end
    object TbDestPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 10
    end
    object TbDestTipo: TWideStringField
      FieldName = 'Tipo'
      Size = 1
    end
    object TbDestOcorMin: TSmallintField
      FieldName = 'OcorMin'
      Required = True
    end
    object TbDestOcorMax: TIntegerField
      FieldName = 'OcorMax'
      Required = True
    end
    object TbDestTamMin: TSmallintField
      FieldName = 'TamMin'
      Required = True
    end
    object TbDestTamMax: TIntegerField
      FieldName = 'TamMax'
      Required = True
    end
    object TbDestTamVar: TWideStringField
      FieldName = 'TamVar'
      Size = 30
    end
    object TbDestDeciCasas: TSmallintField
      FieldName = 'DeciCasas'
      Required = True
    end
    object TbDestObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object TbDestAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object TbDestAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object TbDestLeftZeros: TSmallintField
      FieldName = 'LeftZeros'
      Required = True
    end
    object TbDestInfoVazio: TSmallintField
      FieldName = 'InfoVazio'
      Required = True
    end
    object TbDestFormatStr: TWideStringField
      FieldName = 'FormatStr'
      Size = 30
    end
    object TbDestVersao: TFloatField
      FieldName = 'Versao'
      Required = True
    end
    object TbDestCodigoN: TFloatField
      FieldName = 'CodigoN'
      Required = True
    end
    object TbDestCodigoX: TWideStringField
      FieldName = 'CodigoX'
      Required = True
      Size = 10
    end
  end
  object DsNFeLayC: TDataSource
    DataSet = TbNFeLayC
    Left = 108
    Top = 224
  end
  object TbNFeLayC: TmySQLTable
    Database = Dmod.MyDB
    TableName = 'nfelayc'
    Left = 108
    Top = 176
    object TbNFeLayCGrupo: TWideStringField
      FieldName = 'Grupo'
      Size = 2
    end
    object TbNFeLayCNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
end
