�
 TFMCTEINUT_0000 0�U  TPF0TFmCTeInut_0000FmCTeInut_0000LeftpTop� Caption4   CTe-INUTI-001 :: Inutilizações de Números de CT-eClientHeight=ClientWidthColor	clBtnFaceConstraints.MinHeightConstraints.MinWidth�Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenter
OnActivateFormActivateOnClose	FormCloseOnCreate
FormCreateOnResize
FormResizePixelsPerInch`
TextHeight TPanelPainelEditaLeft Top`WidthHeight�AlignalClient
BevelOuterbvNoneFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentBackground
ParentFontTabOrderVisible TPanel
PainelEditLeft Top WidthHeight}AlignalTop
BevelOuterbvNoneTabOrder  TLabelLabel7LeftTopWidthHeightAutoSizeCaptionID:  TLabelLabel8Left@TopWidth9HeightAutoSizeCaption   Código: [F4]FocusControlDBEdit1  TLabelLabel9Left� TopWidth3HeightAutoSizeCaption   Descrição:  	TdmkLabel	dmkLabel1LeftTop,Width,HeightAutoSizeCaptionEmpresa:UpdTypeutYesSQLTypestNil  	TdmkLabel	dmkLabel2LeftPTop,Width1HeightAutoSizeCaption
   Série CT:UpdTypeutYesSQLTypestNil  	TdmkLabel	dmkLabel3Left�Top,WidthAHeightAutoSizeCaption   Nº CT inicial:UpdTypeutYesSQLTypestNil  	TdmkLabel	dmkLabel4Left�Top,Width6HeightAutoSizeCaption   Nº CT final:UpdTypeutYesSQLTypestNil  	TdmkLabel	dmkLabel5LeftTopTWidth=HeightAutoSizeCaptionJustificativa:UpdTypeutYesSQLTypestNil  	TdmkLabel	dmkLabel6LeftTop,Width1HeightAutoSizeCaptionMod. CT:UpdTypeutYesSQLTypestNil  TLabelLabel4Left�TopWidth3HeightAutoSizeCaptionData:  TSpeedButtonSpeedButton5Left�TopdWidthHeightCaption...OnClickSpeedButton5Click  TdmkEditEdCodigoLeftTopWidth8Height	AlignmenttaRightJustifyEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder 
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0QryCampoCodigoUpdCampoCodigoUpdTypeutIncObrigatorioPermiteNuloValueVariant ValWarn  TdmkEditEdNomeLeft� TopWidthHeightTabOrder
FormatTypedmktfStringMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortQryCampoNomeUpdCampoNomeUpdTypeutYesObrigatorioPermiteNuloValueVariant    ValWarn  TdmkEditEdCodUsuLeft@TopWidthPHeight	AlignmenttaRightJustifyFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0QryCampoCodUsuUpdCampoCodUsuUpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarn	OnKeyDownEdCodUsuKeyDown  
TdmkEditCBEdFilialLeftTop<Width8Height	AlignmenttaRightJustifyTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin-2147483647ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarnDBLookupComboBoxCBFilialIgnoraDBLookupComboBox  TdmkDBLookupComboBoxCBFilialLeft@Top<Width�HeightKeyFieldFilial	ListField
NOMEFILIAL
ListSourceDModG.DsEmpresasTabOrder	dmkEditCBEdFilialUpdTypeutYesLocF7SQLMasc$#  TdmkEditEdSerieLeftPTop<Width8Height	AlignmenttaRightJustifyTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin0ValMax999ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0QryCamposerieUpdCamposerieUpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarn  TdmkEditEdnCTIniLeft�Top<Width@Height	AlignmenttaRightJustifyTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin0ValMax	999999999ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0QryCamponCTIniUpdCamponCTIniUpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarn  TdmkEditEdnCTFimLeft�Top<Width@Height	AlignmenttaRightJustifyTabOrder	
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0QryCamponCTFimUpdCamponCTFimUpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarn  TdmkDBLookupComboBox	CBCTeJustLeft=TopdWidth�HeightKeyFieldCodigo	ListFieldNome
ListSource	DsCTeJustTabOrder	dmkEditCB	EdCTeJustUpdTypeutYesLocF7SQLMasc$#  
TdmkEditCB	EdCTeJustLeftTopdWidth8Height	AlignmenttaRightJustifyTabOrder

FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin-2147483647ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto0UpdTypeutYesObrigatorioPermiteNuloValueVariant ValWarnDBLookupComboBox	CBCTeJustIgnoraDBLookupComboBox  TdmkEditEdModeloLeftTop<Width8Height	AlignmenttaRightJustifyTabOrder
FormatTypedmktfIntegerMskTypefmtNoneDecimalSize 	LeftZeros NoEnterToTabNoForceUppercaseValMin57ValMax57ForceNextYear
DataFormat
dmkdfShort
HoraFormat
dmkhfShortTexto57QryCampoModeloUpdCampoModeloUpdTypeutYesObrigatorioPermiteNuloValueVariant9ValWarn  TdmkEditDateTimePickerTPDataCLeft�TopWidthtHeightDate x<|��T�@Time x<|��T�@TabOrderReadOnlyDefaultEditMask!99/99/99;1;_AutoApplyEditMask	QryCampoDataCUpdCampoDataCUpdTypeutYes   	TGroupBoxGBRodaPeLeft Top�WidthHeight@AlignalBottomTabOrder TPanelPanel4LeftTopWidthHeight/AlignalClient
BevelOuterbvNoneTabOrder  TPanel
PnSaiDesisLeft�Top Width� Height/AlignalRight
BevelOuterbvNoneTabOrder  TBitBtn	BtDesisteTagLeftTopWidthxHeight(CursorcrHandPointCaption&DesisteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	NumGlyphs
ParentFontParentShowHintShowHint	TabOrder OnClickBtDesisteClick   TBitBtn
BtConfirmaTagLeftTopWidthxHeight(CursorcrHandPointCaption	&ConfirmaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	NumGlyphs
ParentFontParentShowHintShowHint	TabOrderOnClickBtConfirmaClick     TPanelPainelDadosLeft Top`WidthHeight�AlignalClient
BevelOuterbvNoneFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style ParentBackground
ParentFontTabOrder  TPanel
PainelDataLeft Top WidthHeight� AlignalTop
BevelOuterbvNoneEnabledParentBackgroundTabOrder  TLabelLabel1LeftTopWidthHeightCaptionID:  TLabelLabel2Left� TopWidth3HeightCaption   Descrição:  TLabelLabel3Left@TopWidth$HeightCaption   Código:FocusControlDBEdit1  	TdmkLabel	dmkLabel7LeftTop,Width,HeightAutoSizeCaptionEmpresa:UpdTypeutYesSQLTypestNil  	TdmkLabel	dmkLabel9LeftTop,Width1HeightAutoSizeCaptionMod. CT:UpdTypeutYesSQLTypestNil  	TdmkLabel
dmkLabel10LeftPTop,Width1HeightAutoSizeCaption
   Série CT:UpdTypeutYesSQLTypestNil  	TdmkLabel
dmkLabel11Left�Top,Width6HeightAutoSizeCaption   Nº CT final:UpdTypeutYesSQLTypestNil  	TdmkLabel
dmkLabel12Left�Top,WidthAHeightAutoSizeCaption   Nº CT inicial:UpdTypeutYesSQLTypestNil  TLabelLabel5LeftTopTWidthrHeightCaptionJustificativa cadastrada:  TLabelLabel6Left�TopWidth3HeightAutoSizeCaptionData:  TDBEditDBEdit1Left@TopWidthPHeight	DataFieldCodUsu
DataSource	DsCTeInutFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBEditDBEdit2LeftTop<Width8Height	DataFieldEmpresa
DataSource	DsCTeInutTabOrder  TDBEditDBEdit3Left@Top<Width�Height	DataField
NO_Empresa
DataSource	DsCTeInutTabOrder  TDBEditDBEdit4LeftTop<Width8Height	DataFieldmodelo
DataSource	DsCTeInutTabOrder  TDBEditDBEdit5LeftPTop<Width8Height	DataFieldSerie
DataSource	DsCTeInutTabOrder  TDBEditDBEdit6Left�Top<Width@Height	DataFieldnCTIni
DataSource	DsCTeInutTabOrder  TDBEditDBEdit7Left�Top<Width@Height	DataFieldnCTFim
DataSource	DsCTeInutTabOrder  TDBEditDBEdit8Left@TopdWidth�Height	DataField	NO_Justif
DataSource	DsCTeInutTabOrder  TDBEditDBEdit9LeftTopdWidth8Height	DataFieldJustif
DataSource	DsCTeInutTabOrder  TDBEditDBEdit10Left�TopWidthtHeight	DataFieldDataC
DataSource	DsCTeInutTabOrder	  TDBEditDBEdNomeLeft� TopWidthHeight	DataFieldNome
DataSource	DsCTeInutTabOrder
  TDBEdit
DBEdCodigoLeftTopWidth8Height	DataFieldCodigo
DataSource	DsCTeInutTabOrder   	TGroupBox	GroupBox1Left Top� WidthHeight� AlignalTopCaption Resposta do servidor: Color	clBtnFaceParentBackgroundParentColorTabOrder TLabelLabel10LeftTopWidth$HeightCaption   Versão:FocusControlDBEdit11  TLabelLabel11Left0TopWidthHeightCaptionID:FocusControlDBEdit12  TLabelLabel12LeftxTopWidth/HeightCaption	Ambiente:FocusControlDBEdit13  TLabelLabel13Left TopWidthHeightCaptioncUF:FocusControlDBEdit14  TLabelLabel14Left$TopWidthHeightCaptionAno:FocusControlDBEdit15  TLabelLabel15LeftDTopWidthHeightCaptionCNPJ:FocusControlDBEdit16  TLabelLabel16Left�TopWidth&HeightCaptionModelo:FocusControlDBEdit17  TLabelLabel17Left�TopWidthHeightCaption   Série:FocusControlDBEdit18  TLabelLabel18LeftTop8Width-HeightCaption   Nº CT ini:FocusControlDBEdit20  TLabelLabel19LeftDTop8Width0HeightCaption   Nº CT fim:FocusControlDBEdit21  TLabelLabel20Left� Top8Width!HeightCaptionStatus:FocusControlDBEdit22  TLabelLabel21LeftTopdWidthQHeightCaptionMotivo do status:FocusControlDBEdit23  TLabelLabel22Left� Top8Width� HeightCaptionData / hora do recebimento:FocusControlDBEdit24  TLabelLabel23Left8Top8WidthfHeightCaption   Número do protocolo:FocusControlDBEdit25  TDBEditDBEdit11LeftTop Width)Height	DataFieldversao
DataSource	DsCTeInutTabOrder   TDBEditDBEdit12Left0Top WidthEHeight	DataFieldId
DataSource	DsCTeInutTabOrder  TDBEditDBEdit13LeftxTop WidthHeight	DataFieldtpAmb
DataSource	DsCTeInutTabOrder  TDBEditDBEdit14Left Top Width!Height	DataFieldcUF
DataSource	DsCTeInutTabOrder  TDBEditDBEdit15Left$Top WidthHeight	DataFieldano
DataSource	DsCTeInutTabOrder  TDBEditDBEdit16LeftDTop WidthyHeight	DataFieldCNPJ
DataSource	DsCTeInutTabOrder  TDBEditDBEdit17Left�Top Width)Height	DataFieldmodelo
DataSource	DsCTeInutTabOrder  TDBEditDBEdit18Left�Top Width$Height	DataFieldSerie
DataSource	DsCTeInutTabOrder  TDBEditDBEdit19Left�Top WidthdHeight	DataFieldNO_Ambiente
DataSource	DsCTeInutTabOrder  TDBEditDBEdit20LeftTopHWidth<Height	DataFieldnCTIni
DataSource	DsCTeInutTabOrder	  TDBEditDBEdit21LeftDTopHWidth<Height	DataFieldnCTFim
DataSource	DsCTeInutTabOrder
  TDBEditDBEdit22Left� TopHWidth$Height	DataFieldcStat
DataSource	DsCTeInutTabOrder  TDBEditDBEdit23LeftToptWidthHeight	DataFieldxMotivo
DataSource	DsCTeInutTabOrder  TDBEditDBEdit24Left� TopHWidth� Height	DataFielddhRecbto
DataSource	DsCTeInutTabOrder  TDBEditDBEdit25Left8TopHWidth�Height	DataFieldnProt
DataSource	DsCTeInutTabOrder   TPageControlPageControl1Left Top?WidthHeight^
ActivePage	TabSheet3AlignalBottomTabOrderOnChangePageControl1Change 	TTabSheet	TabSheet1Caption Mensagens ExplicitLeft ExplicitTop ExplicitWidth ExplicitHeight& TDBGridGradeMsgLeft Top WidthHeightBAlignalClient
DataSourceDsCTeInutMsgTabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style    	TTabSheet	TabSheet2Caption XML/BD (Texto plano)
ImageIndexExplicitLeft ExplicitTop ExplicitWidth ExplicitHeight  TDBMemoDBMemo1Left Top WidthHeightBAlignalClient	DataFieldXML_Inu
DataSource	DsCTeInutTabOrder ExplicitLeft8ExplicitTop,ExplicitWidth� ExplicitHeightY   	TTabSheet	TabSheet3CaptionXML/BD (Navegador)
ImageIndexExplicitLeft ExplicitTop ExplicitWidth ExplicitHeight  TWebBrowser
WBRespostaLeft Top WidthHeightBAlignalClientTabOrder ExplicitWidth�ExplicitHeightTControlData
�   L   Q  �                          L              ��W s5��i +.b       L        �      F�                                                              	TGroupBoxGBCntrlLeft Top�WidthHeight@AlignalBottomTabOrder TPanelPanel5LeftTopWidth� Height/AlignalLeft
BevelOuterbvNoneParentColor	TabOrder TBitBtnSpeedButton4TagLeft� TopWidth(Height(CursorcrHandPoint	NumGlyphsParentShowHintShowHint	TabOrder OnClickSpeedButton4Click  TBitBtnSpeedButton3TagLeftXTopWidth(Height(CursorcrHandPoint	NumGlyphsParentShowHintShowHint	TabOrderOnClickSpeedButton3Click  TBitBtnSpeedButton2TagLeft0TopWidth(Height(CursorcrHandPoint	NumGlyphsParentShowHintShowHint	TabOrderOnClickSpeedButton2Click  TBitBtnSpeedButton1TagLeftTopWidth(Height(CursorcrHandPoint	NumGlyphsParentShowHintShowHint	TabOrderOnClickSpeedButton1Click   TStaticText
LaRegistroLeft� TopWidthHeightAlignalClient
BevelInner	bvLowered	BevelKindbkFlatCaption[N]: 0TabOrder  TPanelPanel3LeftTopWidth	Height/AlignalRight
BevelOuterbvNoneParentColor	TabOrder  TPanelPanel2Left�Top Width� Height/AlignalRight	AlignmenttaRightJustify
BevelOuterbvNoneTabOrder  TBitBtnBtSaidaTagLeftTopWidthxHeight(CursorcrHandPointCaption   &Saída	NumGlyphsParentShowHintShowHint	TabOrder OnClickBtSaidaClick   TBitBtnBtDadosLeftTopWidthxHeight(CursorcrHandPointCaption&Dados	NumGlyphsParentShowHintShowHint	TabOrderOnClickBtDadosClick  TBitBtn
BtSolicitaLeft� TopWidthxHeight(CursorcrHandPointCaption	&Solicita	NumGlyphsParentShowHintShowHint	TabOrderOnClickBtSolicitaClick  TBitBtnBtLeXMLTagLeft� TopWidthxHeight(CursorcrHandPointCaption   &Lê XML	NumGlyphsParentShowHintShowHint	TabOrderOnClickBtLeXMLClick     TPanelPnCabecaLeft Top WidthHeight4AlignalTop
BevelOuterbvNoneTabOrder 	TGroupBoxGB_RLeft�Top Width0Height4AlignalRightTabOrder  	TdmkImageImgTipoLeftTopWidth Height Transparent	SQLTypestNil   	TGroupBoxGB_LLeft Top Width� Height4AlignalLeftTabOrder TBitBtn	SbImprimeTagLeftTopWidth(Height(	NumGlyphsTabOrder OnClickSbImprimeClick  TBitBtnSbNovoTagLeft.TopWidth(Height(	NumGlyphsTabOrderOnClickSbNovoClick  TBitBtnSbNumeroTagLeftXTopWidth(Height(	NumGlyphsTabOrderOnClickSbNumeroClick  TBitBtnSbNomeTagLeft� TopWidth(Height(	NumGlyphsTabOrderOnClickSbNomeClick  TBitBtnSbQueryTag	Left� TopWidth(Height(	NumGlyphsTabOrderOnClickSbQueryClick   	TGroupBoxGB_MLeft� Top WidthHeight4AlignalClientTabOrder TLabel
LaTitulo1ALeftTop	Width�Height Caption#   Inutilizações de Números de CT-eColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclGradientActiveCaptionFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFontVisible  TLabel
LaTitulo1BLeft	TopWidth�Height Caption#   Inutilizações de Números de CT-eColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclSilverFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFont  TLabel
LaTitulo1CLeftTop
Width�Height Caption#   Inutilizações de Números de CT-eColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.Color
clHotLightFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFont    	TGroupBox	GBAvisos1Left Top4WidthHeight,AlignalTopCaption	 Avisos: TabOrder TPanelPanel1LeftTopWidthHeightAlignalClient
BevelOuterbvNoneTabOrder  TLabelLaAviso1LeftTopWidthHeightCaption...Font.CharsetDEFAULT_CHARSET
Font.ColorclSilverFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	  TLabelLaAviso2LeftTopWidthHeightCaption...Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	    TDataSource	DsCTeInutDataSet	QrCTeInutLeftLTop\  TmySQLQuery	QrCTeInutDatabase	Dmod.MyDB
BeforeOpenQrCTeInutBeforeOpen	AfterOpenQrCTeInutAfterOpenAfterScrollQrCTeInutAfterScrollSQL.Strings<SELECT IF(ent.Tipo=0,ent.RazaoSocial, ent.Nome) NO_Empresa, jus.Nome NO_Justif, inu.*FROM cteinut inu1LEFT JOIN entidades ent ON ent.Codigo=inu.Empresa.LEFT JOIN ctejust jus ON jus.Codigo=inu.Justif LeftLTop, TIntegerFieldQrCTeInutCodigo	FieldNameCodigo  TIntegerFieldQrCTeInutCodUsu	FieldNameCodUsu  TStringFieldQrCTeInutNome	FieldNameNomeSized  TIntegerFieldQrCTeInutEmpresa	FieldNameEmpresa  TFloatFieldQrCTeInutversao	FieldNameversaoDisplayFormat0.00  TStringFieldQrCTeInutId	FieldNameIdSize  TSmallintFieldQrCTeInuttpAmb	FieldNametpAmb  TSmallintFieldQrCTeInutcUF	FieldNamecUF  TSmallintFieldQrCTeInutano	FieldNameano  TStringFieldQrCTeInutCNPJ	FieldNameCNPJSize  TSmallintFieldQrCTeInutmodelo	FieldNamemodelo  TIntegerFieldQrCTeInutSerie	FieldNameSerie  TIntegerFieldQrCTeInutnCTIni	FieldNamenCTIni  TIntegerFieldQrCTeInutnCTFim	FieldNamenCTFim  TIntegerFieldQrCTeInutJustif	FieldNameJustif  TStringFieldQrCTeInutxJust	FieldNamexJustSize�   TIntegerFieldQrCTeInutcStat	FieldNamecStat  TStringFieldQrCTeInutxMotivo	FieldNamexMotivoSize�   TIntegerFieldQrCTeInutLk	FieldNameLk  
TDateFieldQrCTeInutDataCad	FieldNameDataCad  
TDateFieldQrCTeInutDataAlt	FieldNameDataAlt  TIntegerFieldQrCTeInutUserCad	FieldNameUserCad  TIntegerFieldQrCTeInutUserAlt	FieldNameUserAlt  TSmallintFieldQrCTeInutAlterWeb	FieldNameAlterWeb  TSmallintFieldQrCTeInutAtivo	FieldNameAtivo  TStringFieldQrCTeInutNO_Empresa	FieldName
NO_EmpresaSized  TStringFieldQrCTeInutNO_Justif	FieldName	NO_JustifSize�   
TDateFieldQrCTeInutDataC	FieldNameDataCDisplayFormat
dd/mm/yyyy  TStringFieldQrCTeInutNO_Ambiente	FieldKindfkCalculated	FieldNameNO_Ambiente
Calculated	  TDateTimeFieldQrCTeInutdhRecbto	FieldNamedhRecbto  TStringFieldQrCTeInutnProt	FieldNamenProtSize  
TMemoFieldQrCTeInutXML_Inu	FieldNameXML_InuBlobTypeftMemoSize   TdmkPermissoesdmkPermissoes1CanIns01BtLeXMLCanUpd01
BtSolicitaLeftHTop��    TmySQLQuery	QrCTeJustDatabase	Dmod.MyDBSQL.Strings	SELECT * FROM ctejustWHERE :P0 & Aplicacao > 0ORDER BY Nome Left�Top,	ParamDataDataType	ftUnknownNameP0	ParamType	ptUnknown   TIntegerFieldQrCTeJustCodigo	FieldNameCodigo  TStringFieldQrCTeJustNome	FieldNameNomeSize�    TDataSource	DsCTeJustDataSet	QrCTeJustLeft�Top\  
TdmkValUsudmkVUFilial	dmkEditCBEdFilialPanelPainelEditaQryCampoEmpresaUpdCampoEmpresaRefCampoCodigoUpdTypeutYesLeftdTop��    
TdmkValUsudmkVUCTeJust	dmkEditCB	EdCTeJustPanelPainelEditaQryCampoJustifUpdCampoJustifRefCampoCodigoUpdTypeutYesLeft� Top��    TmySQLQueryQrCTeInutMsgDatabase	Dmod.MyDBSQL.StringsSELECT * FROM cteinutmsgWHERE Codigo=:P0ORDER BY Controle DESC Left�Top,	ParamDataDataType	ftUnknownNameP0	ParamType	ptUnknown   TIntegerFieldQrCTeInutMsgCodigo	FieldNameCodigoOriginnfeinutmsg.Codigo  TIntegerFieldQrCTeInutMsgControle	FieldNameControleOriginnfeinutmsg.Controle  TFloatFieldQrCTeInutMsgversao	FieldNameversaoOriginnfeinutmsg.versao  TStringFieldQrCTeInutMsgId	FieldNameIdOriginnfeinutmsg.IdSize  TSmallintFieldQrCTeInutMsgtpAmb	FieldNametpAmbOriginnfeinutmsg.tpAmb  TStringFieldQrCTeInutMsgverAplic	FieldNameverAplicOriginnfeinutmsg.verAplicSize  TIntegerFieldQrCTeInutMsgcStat	FieldNamecStatOriginnfeinutmsg.cStat  TStringFieldQrCTeInutMsgxMotivo	FieldNamexMotivoOriginnfeinutmsg.xMotivoSize�   TSmallintFieldQrCTeInutMsgcUF	FieldNamecUFOriginnfeinutmsg.cUF   TDataSourceDsCTeInutMsgDataSetQrCTeInutMsgLeft�Top\  
TPopupMenuPMDadosLeft<Top� 	TMenuItemIncluinovoprocesso1Caption&Inclui novo processoOnClickIncluinovoprocesso1Click  	TMenuItemAlteradadosdoprocessoatual1Caption&Altera dados do processo atualEnabledOnClick Alteradadosdoprocessoatual1Click    