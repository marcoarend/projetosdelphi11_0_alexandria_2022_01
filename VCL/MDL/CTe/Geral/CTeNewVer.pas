unit CTeNewVer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, mySQLDbTables,
  dmkCheckBox, dmkDBGridZTO;

type
  TFmCTeNewVer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrOrigI: TmySQLQuery;
    QrOrigIGrupo: TWideStringField;
    QrOrigICodigo: TWideStringField;
    QrOrigIID: TWideStringField;
    QrOrigICampo: TWideStringField;
    QrOrigIDescricao: TWideStringField;
    QrOrigIElemento: TWideStringField;
    QrOrigIPai: TWideStringField;
    QrOrigITipo: TWideStringField;
    QrOrigIOcorMin: TSmallintField;
    QrOrigIOcorMax: TIntegerField;
    QrOrigITamMin: TSmallintField;
    QrOrigITamMax: TIntegerField;
    QrOrigITamVar: TWideStringField;
    QrOrigIDeciCasas: TSmallintField;
    QrOrigIObservacao: TWideMemoField;
    QrOrigIAlterWeb: TSmallintField;
    QrOrigIAtivo: TSmallintField;
    QrOrigILeftZeros: TSmallintField;
    QrOrigIInfoVazio: TSmallintField;
    QrOrigIFormatStr: TWideStringField;
    QrOrigIVersao: TFloatField;
    QrOrigICodigoN: TFloatField;
    QrOrigICodigoX: TWideStringField;
    DsDest: TDataSource;
    TbDest: TmySQLTable;
    TbDestGrupo: TWideStringField;
    TbDestCodigo: TWideStringField;
    TbDestID: TWideStringField;
    TbDestCampo: TWideStringField;
    TbDestDescricao: TWideStringField;
    TbDestElemento: TWideStringField;
    TbDestPai: TWideStringField;
    TbDestTipo: TWideStringField;
    TbDestOcorMin: TSmallintField;
    TbDestOcorMax: TIntegerField;
    TbDestTamMin: TSmallintField;
    TbDestTamMax: TIntegerField;
    TbDestTamVar: TWideStringField;
    TbDestDeciCasas: TSmallintField;
    TbDestObservacao: TWideMemoField;
    TbDestAlterWeb: TSmallintField;
    TbDestAtivo: TSmallintField;
    TbDestLeftZeros: TSmallintField;
    TbDestInfoVazio: TSmallintField;
    TbDestFormatStr: TWideStringField;
    TbDestVersao: TFloatField;
    TbDestCodigoN: TFloatField;
    TbDestCodigoX: TWideStringField;
    PB: TProgressBar;
    TbDestSubGrupo: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel5: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdVerOrig: TdmkEdit;
    EdVerDest: TdmkEdit;
    BtCopiar: TBitBtn;
    CkNaoSobrepor: TdmkCheckBox;
    BtReopen: TBitBtn;
    EdPsqGrupo: TdmkEdit;
    dmkDBGridZTO1: TdmkDBGridZTO;
    DBGrid1: TDBGrid;
    DsNFeLayC: TDataSource;
    TbNFeLayC: TmySQLTable;
    TbNFeLayCGrupo: TWideStringField;
    TbNFeLayCNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCopiarClick(Sender: TObject);
    procedure BtReopenClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    procedure CopiarAtual(Versao: Double);
    procedure ReopenNFeLayC();
    procedure ReopenNFeLayI();
  public
    { Public declarations }
  end;

  var
  FmCTeNewVer: TFmCTeNewVer;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UmySQLModule, MyDBCheck, NFeLayout_Edit;

{$R *.DFM}

procedure TFmCTeNewVer.BtCopiarClick(Sender: TObject);
var
  VerOrig, VerDest: Double;
begin
  if Geral.MB_Pergunta('Ser�o copiados os registros da tabela ' +
  EdVerOrig.Text + ' para a ' + EdVerDest.Text + '. Deseja continuar?') = ID_YES then
  begin
    if not DBCheck.LiberaPelaSenhaAdmin() then
      Exit;
    //
    VerOrig := EdVerOrig.ValueVariant;
    VerDest := EdVerDest.ValueVariant;

    UnDmkDAC_PF.AbreMySQLQuery0(QrOrigI, Dmod.MyDB, [
    'SELECT * ',
    'FROM nfelayi ',
    'WHERE Versao=' + Geral.FFT_Dot(VerOrig, 2, siPositivo),
    '']);
    //
    if QrOrigI.RecordCount > 0 then
    begin
      PB.Position := 0;
      PB.Max := QrOrigI.RecordCount;
      QrOrigI.First;
      while not QrOrigI.Eof do
      begin
        MyObjects.UpdPB(PB, LaAviso1, LaAviso2);
        CopiarAtual(VerDest);
        //
        QrOrigI.Next;
      end;

    end else Geral.MB_Aviso(
      'N�o existem registros com a vers�o de origem na base de dados!');
  end;
end;

procedure TFmCTeNewVer.BtOKClick(Sender: TObject);
const
  SQLType = stUpd;
var
  Codigo, ID: String;
begin
  if DBCheck.CriaFm(TFmNFeLayout_Edit, FmNFeLayout_Edit, afmoSoAdmin) then
  begin
    with FmNFeLayout_Edit do
    begin
      if SQLType = stUpd then
      begin
        EdDeciCasas.ValueVariant  := TbDestDeciCasas.Value;
        EdOcorMax.ValueVariant    := TbDestOcorMax.Value;
        EdTamMin.ValueVariant     := TbDestTamMin.Value;
        EdTamMax.ValueVariant     := TbDestTamMax.Value;
        EdTamVar.ValueVariant     := TbDestTamVar.Value;
        CkLeftZeros.Checked       := Geral.IntToBool(TbDestLeftZeros.Value);
        CkInfoVazio.Checked       := Geral.IntToBool(TbDestInfoVazio.Value);
        EdGrupo.ValueVariant      := TbDestGrupo.Value;
        EdCampo.ValueVariant      := TbDestCampo.Value;
        EdDescricao.ValueVariant  := TbDestDescricao.Value;
        EdElemento.ValueVariant   := TbDestElemento.Value;
        EdPai.ValueVariant        := TbDestPai.Value;
        EdTipo.ValueVariant       := TbDestTipo.Value;
        EdOcorMin.ValueVariant    := TbDestOcorMin.Value;
        MeObservacao.Text         := TbDestObservacao.Value;
        EdCodigo.ValueVariant     := TbDestCodigo.Value;
        EdID.ValueVariant         := TbDestID.Value;
        EdFormatStr.ValueVariant  := TbDestFormatStr.Value;
        EdVersao.ValueVariant     := TbDestVersao.Value;
        //
        EdSubGrupo.Text           := TbDestSubGrupo.Value;
        EdCodigoN.ValueVariant    := TbDestCodigoN.Value;
        EdCodigoX.Text            := TbDestCodigoX.Value;
        //
        FCodigo                   := TbDestCodigo.Value;
        FID                       := TbDestID.Value;
        FPai                      := TbDestPai.Value;
        FVersao                   := TbDestVersao.Value;
        {
        EdCodigo.Enabled := False;
        EdID.Enabled := False;
        }
      end else begin
        FCodigo                   := '';
        FID                       := '';
        FPai                      := '';
        FVersao                   := 0;
      end;
      ImgTipo.SQLType := SQLType;
      ShowModal;
      Codigo := EdCodigo.ValueVariant;
      ID := EdID.ValueVariant;
      Destroy;
    end;
    TbDest.Close;
    TbDest.Open;
  end;
end;

procedure TFmCTeNewVer.BtReopenClick(Sender: TObject);
begin
  ReopenNFeLayI();
end;

procedure TFmCTeNewVer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCTeNewVer.CopiarAtual(Versao: Double);
var
  Grupo, Codigo, ID, Campo, Descricao, Elemento, Pai, Tipo, TamVar, Observacao,
  FormatStr, CodigoX: String;
  OcorMin, OcorMax, TamMin, TamMax, DeciCasas, LeftZeros, InfoVazio: Integer;
  CodigoN: Double;
begin
  Grupo          := QrOrigIGrupo.Value;
  Codigo         := QrOrigICodigo.Value;
  ID             := QrOrigIID.Value;
  Campo          := QrOrigICampo.Value;
  Descricao      := QrOrigIDescricao.Value;
  Elemento       := QrOrigIElemento.Value;
  Pai            := QrOrigIPai.Value;
  Tipo           := QrOrigITipo.Value;
  OcorMin        := QrOrigIOcorMin.Value;
  OcorMax        := QrOrigIOcorMax.Value;
  TamMin         := QrOrigITamMin.Value;
  TamMax         := QrOrigITamMax.Value;
  TamVar         := QrOrigITamVar.Value;
  DeciCasas      := QrOrigIDeciCasas.Value;
  Observacao     := QrOrigIObservacao.Value;
  LeftZeros      := QrOrigILeftZeros.Value;
  InfoVazio      := QrOrigIInfoVazio.Value;
  FormatStr      := QrOrigIFormatStr.Value;
  //Versao         := QrOrigIVersao.Value;
  CodigoN        := QrOrigICodigoN.Value;
  CodigoX        := QrOrigICodigoX.Value;
  //
  UMyMod.SQLInsUpd_IGNORE(Dmod.QrUpd, stIns, 'nfelayi', False, [
  'Grupo', 'Campo', 'Descricao',
  'Elemento', 'Tipo', 'OcorMin',
  'OcorMax', 'TamMin', 'TamMax',
  'TamVar', 'DeciCasas', 'Observacao',
  'LeftZeros', 'InfoVazio', 'FormatStr',
  'CodigoN', 'CodigoX'], [
  'Codigo', 'ID', 'Pai', 'Versao'], [
  Grupo, Campo, Descricao,
  Elemento, Tipo, OcorMin,
  OcorMax, TamMin, TamMax,
  TamVar, DeciCasas, Observacao,
  LeftZeros, InfoVazio, FormatStr,
  CodigoN, CodigoX], [
  Codigo, ID, Pai, Versao], False);
end;

procedure TFmCTeNewVer.DBGrid1DblClick(Sender: TObject);
begin
  EdPsqGrupo.Text := TbNFeLayCGrupo.Value;
  ReopenNFeLayI();
  PageControl1.ActivePageIndex := 1;
end;

procedure TFmCTeNewVer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCTeNewVer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenNfeLayC();
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmCTeNewVer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCTeNewVer.ReopenNFeLayC();
begin
  TbNFeLayC.Open;
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrNfeLayC, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfelayc ',
  'Order By Grupo, Nome ',
  '']);
*)
end;

procedure TFmCTeNewVer.ReopenNFeLayI();
var
  Filtro: String;
begin
  Filtro := 'Versao=' + Geral.FFT(EdVerDest.ValueVariant, 2, siPositivo);
  if Trim(EdPsqGrupo.Text) <> '' then
    Filtro := Filtro + ' AND Grupo=''' + Trim(EdPsqGrupo.Text) + '''';
  TbDest.Close;
  TbDest.Filter := Filtro;
  TbDest.Filtered := True;
  TbDest.Open;
end;

end.
