unit UnCTe_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral,
  UnDmkEnums, UnMsgInt, mySQLDbTables, DB,(* DbTables,*) DmkCoding, dmkEdit,
  dmkRadioGroup, dmkMemo, CTeGeraXML_0200a, AdvToolBar, dmkCheckGroup,
  CAPICOM_TLB,  MSXML2_TLB,
  InvokeRegistry, Rio, SOAPHTTPClient, SOAPHTTPTrans, JwaWinCrypt,
  WinInet, XMLDoc, xmldom, XMLIntf, msxmldom, ComObj,
  UnMyObjects, UnXXe_PF,
  Buttons, UnMLAGeral, Math,
  IBCustomDataSet, IBQuery, UrlMon;

type
  TUnCTe_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    // Geral
    function  AssinarMSXML(XML: String; Certificado:
              ICertificate2; out XMLAssinado: String): Boolean;
    procedure AvisoNaoImplemVerCTe(Versao: Integer);
    procedure BaixaLayoutCTe(Memo: TMemo);
    function  DesmontaID_Inutilizacao(const Id: String; var cUF,
              Ano, emitCNPJ, Modelo, Serie, nCTIni, nCTFim: String): Boolean;
    procedure EnviaLoteAoFisco(Sincronia: TXXeIndSinc; Lote, Empresa: Integer;
              SohLer: Boolean; Qry: TmySQLQuery);
    function  InsUpdFaturasCTe(Empresa, FatID, FatNum, CtaFaturas, Financeiro,
              CartEmiss, CondicaoPG, FaturaNum, IDDuplicata, NumeroCT,
              FaturaSeq, TipoCart, Represen, Cliente: Integer; vRec,
              JurosMes: Double; TpDuplicata, FaturaSep, TxtFaturas: String;
              DataFat: TDateTime; SerieCTe, NumeroCTe: Integer): Boolean;
    function  Evento_MontaId(tpEvento: Integer; Chave: String; nSeqEvento:
              Integer): String;
    function  Evento_Obtem_DadosEspecificos(const EvePreDef: Integer;
              const EventoCTe: TEventoXXe; var tpEvento: Integer; var versao: Double;
              var descEvento: String): Boolean;
    function  MontaChaveDeAcesso(cUF: Integer; Emissao: TDateTime; CNPJ: String;
              Modelo, Serie, NumeroCT: Integer; var ide_cCT: Integer;
              var cDV: String; var ChaveDeAcesso: String; const cCT_Atual,
              ide_tpEmis: Integer; versao: Double): Boolean;
    procedure MostraFormCTeCTaEdit_0200a(Empresa, FrtFatCab: Integer; QrFrtFatCab:
              TmySQLQuery; SerieCTe, NumeroCTe: Integer; vRec: Double);
    procedure MostraFormCTeInut();
    procedure MostraFormCTeJust();
    procedure MostraFormCTeLayout_0200();
    procedure MostraFormCTeLEnc(Codigo: Integer);
    procedure MostraFormCTePesq(AbrirEmAba: Boolean; InOwner: TWincontrol;
              AdvToolBarPager: TAdvToolBarPager; Cliente: Integer);
    procedure MostraFormCTeEveRLoE(Codigo: Integer);
    procedure MostraFormStepsCTe_Cancelamento(Empresa, Serie, nCT, nSeqEvento,
              Justif: Integer; Modelo, nProtCan, chCTe, XML_Evento: String;
              Qry: TmySQLQuery);
    procedure MostraFormStepsCTe_CartaDeCorrecao(Empresa, Serie, nCT,
              nSeqEvento: Integer; Modelo, nProt, chCTe, XML_Evento: String;
              Qry: TmySQLQuery);
    procedure MostraFormStepsCTe_ConsultaCTe(Empresa, IDCtrl: Integer; ChaveCTe:
              String; MeXML: TMemo);
    procedure MostraFormStepsCTe_EPEC(Empresa, Serie, nCT, nSeqEvento,
              Justif: Integer; Modelo, chCTe, XML_Evento: String;
              Qry: TmySQLQuery);
    procedure MostraFormStepsCTe_StepGenerico();
    procedure MostraFormStepsCTe_Inutilizacao(Empresa, Lote, Ano, Modelo, Serie,
              nCTIni, nCTFim, Justif: Integer);
    procedure MostraFormStepsCTe_StatusServico();
(*
    procedure MostraFormCTeIt1AutXml(FatID, FatNum, Empresa, DestRem: Integer;
              InserePadroes: Boolean);
*)
    function  Obtem_Serie_e_NumCT(const SerieDesfeita, CTeDesfeita,
              (*SerieNormal,*) ParamsCTe, FatID, FatNum, Empresa(*, Filial, MaxSeq*): Integer;
              const Tabela: String;
              var SerieCTe,
              NumeroCTe: Integer): Boolean;
    function  ObtemURLWebServerCTe(UF, Servico, Versao: String; tpAmb: Integer):
              String;

    function  SalvaXML(Arquivo: String; XML: String): Boolean;
    function  Texto_StatusCTe(Status: Integer; VersaoCTe: Double): String;
    function  TipoXML(NoStandAlone: Boolean): String;
    function  VDom(const Grupo, Codigo: Integer; const Campo, Valor, Dominio: String; var
              Msg: String): Boolean;
    function  VersaoCTeEmUso(): Integer;
    function  VRegExp(const Grupo, Codigo: Integer; const Campo, Valor,
              ExpressaoRegular: String; var Msg: String): Boolean;
    function  XML_DistribuiCartaDeCorrecao(const VersaoCCe: String;
              const XML_Evento, XML_Protocolo: String;
              var XML_Distribuicao: String): Boolean;
    function  XML_DistribuiCTe(const Id: String; const Status: Integer;
              const XML_CTe, XML_Aut, XML_Can: String; const Protocolar: Boolean;
              var XML_Distribuicao: String; const VersaoCTe: Double;
              const InfoLocal: String): Boolean;
    function  XML_DistribuiEvento(const Id: String;
              const Status: Integer; const XML_Env, XML_Ret: String;
              const Protocolar: Boolean; var XML_Distribuicao: String;
              const Versao: Double; const InfoLocal: String): Boolean;


  end;

var
  CTe_PF: TUnCTe_PF;
  CO_CTE_CERTIFICADO_DIGITAL_SERIAL_NUMBER: String;


const
  // assinatura
  INTERNET_OPTION_CLIENT_CERT_CONTEXT = 84;
  DSIGNS = 'xmlns:ds="http://www.w3.org/2000/09/xmldsig#"';
 { TODO : Colocar aqui os stat da CCe }
  CTe_AllModelos      = '57';
{
  CTe_CodAutorizaTxt  = '100';
  CTe_CodCanceladTxt  = '101';
  CTe_CodInutilizTxt  = '102';
  CTe_CodDenegadoTxt  = '110,301,302';
}
  // DesmontarChaveCTe
  DEMONTA_CHV_INI_SerCTe = 23;
  DEMONTA_CHV_TAM_SerCTe = 03;
  DEMONTA_CHV_INI_NumCTe = 26;
  DEMONTA_CHV_TAM_NumCTe = 09;

(*
  CTe_CodEventoCCe  = 110110;
  CTe_CodEventoCan  = 110111;
  CTe_CodEventoEPEC = 110113;
  CTe_CodEventoMMo  = 110160;
*)
  // Apenas compatibilidade por enquanto!!!!
  // Na verdade estes codigos sao da N F e
  CTe_CodsEveWbserverUserDef = '210200, 210210, 210220, 210240';


implementation

uses DmkDAC_PF, ModuleGeral, MyDBCheck, Restaura2, Module, UnDmkWeb,
  CTeLayout_0200, CTeSteps_0200a, CTeCTaEdit_0200a, CTeLEnc_0200a,
  CTe_Pesq_0000, CTeEveRLoE, CTeJust, CTeInut_0000,
  CTeEveGeraXMLCCe_0200a, CTeEveGeraXMLCan_0200a, CTeEveGeraXMLEPEC_0200a,
  RegExpr, UnFinanceiro, UMySQLModule, UnDmkProcFunc, ModuleCTe_0000;

var
  //  ACBr
  CertStore     : IStore3;
  CertStoreMem  : IStore3;
  PrivateKey    : IPrivateKey;
  Certs         : ICertificates2;
  Cert          : ICertificate2;
  NumCertCarregado : String;

{ TUnCTe_PF }

function TUnCTe_PF.AssinarMSXML(XML: String; Certificado: ICertificate2;
  out XMLAssinado: String): Boolean;
var
 I, J, PosIni, PosFim : Integer;
 URI           : String ;
 Tipo : Integer;

 xmlHeaderAntes, xmlHeaderDepois: String;
 xmldoc  : IXMLDOMDocument3;
 xmldsig : IXMLDigitalSignature;
 dsigKey   : IXMLDSigKey;
 signedKey : IXMLDSigKey;
begin
  Result := False;
  if Pos('<Signature', XML) <= 0 then
  begin
    I := pos('<infCte',XML) ;
    Tipo := 1;
  end;
  if I = 0  then
  begin
    I := Pos('<infEvento', XML);
    if I > 0 then
      Tipo := 2
    else
    begin
      I := pos('<infInut',XML) ;
      if I > 0 then
        Tipo := 3
      else
      begin
        Geral.MB_Erro('Tag n�o definida em "TUnCTe_PF.AssinarMSXML()" [1]' +
        slineBreak + XML);
        Exit;
      end;
(*
      I := pos('<infCanc',XML) ;
      if I > 0 then
        Tipo := 2
      else
      begin
        I := pos('<infInut',XML) ;
        if I > 0 then
          Tipo := 3
        else
        begin
          I := Pos('<infEvento', XML);
          if I > 0 then
          begin
            I := Pos('<Evento', XML);
            if I > 0 then
              Tipo := 6
            else
              Tipo := 5 // '<evento'
          end else
            Tipo := 4;
        end;
     end;
*)
    end;
  end;

      I := PosEx('Id=',XML,6) ;
      if I = 0 then
         raise Exception.Create('N�o encontrado in�cio do URI: Id=') ;
      I := PosEx('"',XML,I+2) ;
      if I = 0 then
         raise Exception.Create('N�o encontrado in�cio do URI: aspas inicial') ;
      J := PosEx('"',XML,I+1) ;
      if J = 0 then
         raise Exception.Create('N�o encontrado in�cio do URI: aspas final') ;

      URI := copy(XML,I+1,J-I-1) ;

      if Tipo = 1 then
         XML := copy(XML,1,pos('</CTe>',XML)-1)
      else if Tipo = 2 then
         XML := copy(XML,1,pos('</eventoCTe>',XML)-1)
      else if Tipo = 3 then
         XML := copy(XML,1,pos('</inutCTe>',XML)-1)
{
      else if Tipo = 2 then
         XML := copy(XML,1,pos('</cancCTe>',XML)-1)
      else if Tipo = 4 then
         XML := copy(XML,1,pos('</envDPEC>',XML)-1)
      else if Tipo = 5 then
         XML := copy(XML,1,pos('</evento>',XML)-1);
(*
      else if Tipo = 6 then
         XML := copy(XML,1,pos('</Evento>',XML)-1)
*)
      }
      else
      begin
        Geral.MB_Erro('Tag n�o definida em "TUnCTe_PF.AssinarMSXML()" [2]');
        Exit;
      end;

      XML := XML + '<Signature xmlns="http://www.w3.org/2000/09/xmldsig#"><SignedInfo><CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/><SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />';
      XML := XML + '<Reference URI="#'+URI+'">';
      XML := XML + '<Transforms><Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" /><Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" /></Transforms><DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />';
      XML := XML + '<DigestValue></DigestValue></Reference></SignedInfo><SignatureValue></SignatureValue><KeyInfo></KeyInfo></Signature>';

      if Tipo = 1 then
         XML := XML + '</CTe>'
      else if Tipo = 2 then
         XML := XML + '</eventoCTe>'
      else if Tipo = 3 then
         XML := XML + '</inutCTe>'
      {
      else if Tipo = 2 then
         XML := XML + '</cancCTe>'
      else if Tipo = 4 then
         XML := XML + '</envDPEC>'
      else if Tipo = 5 then
         XML := XML + '</evento>';
      (*
      else if Tipo = 6 then
         XML := XML + '</Evento>';
      *)
      }
      else
      begin
        Geral.MB_Erro('Tag n�o definida em "TUnCTe_PF.AssinarMSXML()" [3]');
        Exit;
      end;
   //end;

   // Lendo Header antes de assinar //
   xmlHeaderAntes := '' ;
   I := pos('?>',XML) ;
   if I > 0 then
      xmlHeaderAntes := copy(XML,1,I+1) ;

   xmldoc := CoDOMDocument50.Create;

   xmldoc.async              := False;
   xmldoc.validateOnParse    := False;
   xmldoc.preserveWhiteSpace := True;

   xmldsig := CoMXDigitalSignature50.Create;

   if (not xmldoc.loadXML(XML) ) then
   begin
     Geral.MensagemBox('N�o foi poss�vel carregar o arquivo: ' + #13#10 + XML, 'ERRO', MB_OK+MB_ICONERROR);
     raise Exception.Create('N�o foi poss�vel carregar o arquivo: '+XML);
   end;
   xmldoc.setProperty('SelectionNamespaces', DSIGNS);

   xmldsig.signature := xmldoc.selectSingleNode('.//ds:Signature');
   //xmldsig.signature := xmldoc.selectSingleNode('Signature');

   if (xmldsig.signature = nil) then
      raise Exception.Create('� preciso carregar o template antes de assinar.');

   if NumCertCarregado <> Certificado.SerialNumber then
      CertStoreMem := nil;

   if  CertStoreMem = nil then
    begin
      CertStore := CoStore.Create;
      CertStore.Open(CAPICOM_CURRENT_USER_STORE, 'My', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

      CertStoreMem := CoStore.Create;
      CertStoreMem.Open(CAPICOM_MEMORY_STORE, 'Memoria', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

      Certs := CertStore.Certificates as ICertificates2;
      for i:= 1 to Certs.Count do
      begin
        Cert := IInterface(Certs.Item[i]) as ICertificate2;
        if Cert.SerialNumber = Certificado.SerialNumber then
         begin
           CertStoreMem.Add(Cert);
           NumCertCarregado := Certificado.SerialNumber;
         end;
      end;
   end;

   OleCheck(IDispatch(Certificado.PrivateKey).QueryInterface(IPrivateKey,PrivateKey));
   xmldsig.store := CertStoreMem;

   dsigKey := xmldsig.createKeyFromCSP(PrivateKey.ProviderType, PrivateKey.ProviderName, PrivateKey.ContainerName, 0);
   if (dsigKey = nil) then
      raise Exception.Create('Erro ao criar a chave do CSP. (assinatura digital)');

   signedKey := xmldsig.sign(dsigKey, $00000002);
   if (signedKey <> nil) then
    begin
      XMLAssinado := xmldoc.xml;
      XMLAssinado := StringReplace( XMLAssinado, #10, '', [rfReplaceAll] ) ;
      XMLAssinado := StringReplace( XMLAssinado, #13, '', [rfReplaceAll] ) ;
      PosIni := Pos('<SignatureValue>',XMLAssinado)+length('<SignatureValue>');
      XMLAssinado := copy(XMLAssinado,1,PosIni-1)+StringReplace( copy(XMLAssinado,PosIni,length(XMLAssinado)), ' ', '', [rfReplaceAll] ) ;
      PosIni := Pos('<X509Certificate>',XMLAssinado)-1;
      PosFim := dmkPF.PosLast('<X509Certificate>', XMLAssinado);

      XMLAssinado := copy(XMLAssinado,1,PosIni)+copy(XMLAssinado,PosFim,length(XMLAssinado));
    end
   else
     raise Exception.Create('Ocorreu uma falha ao tentar assinar o XML!');
      //raise Exception.Create('Assinatura Falhou.');
   if xmlHeaderAntes <> '' then
   begin
      I := pos('?>',XMLAssinado) ;
      if I > 0 then
       begin
         xmlHeaderDepois := copy(XMLAssinado,1,I+1) ;
         if xmlHeaderAntes <> xmlHeaderDepois then
            XMLAssinado := StuffString(XMLAssinado,1,length(xmlHeaderDepois),xmlHeaderAntes) ;
       end
      else
         XMLAssinado := xmlHeaderAntes + XMLAssinado ;
   end ;

   dsigKey   := nil;
   signedKey := nil;
   xmldoc    := nil;
   xmldsig   := nil;

   Result := True;
end;

procedure TUnCTe_PF.AvisoNaoImplemVerCTe(Versao: Integer);
begin
  Geral.MB_Aviso('Versao de CTe n�o implementada: ' +
  Geral.FFT_Dot(Versao / 100, 2, siPositivo));
end;

procedure TUnCTe_PF.BaixaLayoutCTe(Memo: TMemo);
var
  Res: Boolean;
  ArqNome: String;
  Versao: Integer;
begin
  Res := DmkWeb.VerificaAtualizacaoVersao2(True, True, CO_WEBID_CTELAY,
           'Layout CTe', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value),
           0, 0, DModG.ObtemAgora(), Memo, dtSQLs, Versao, ArqNome, False);
  if (Res) and (VAR_DOWNLOAD_OK) then
  begin
    //C:\Projetos\Delphi 2007\Aplicativos\Auxiliares\DermaBK\Restaura.pas
    Application.CreateForm(TFmRestaura2, FmRestaura2);
    FmRestaura2.Show;
    FmRestaura2.EdBackFile.Text := CO_DIR_RAIZ_DMK + '\' + ArqNome + '.SQL';
    //FmRestaura2.BeRestore.DatabaseName := DMod.MyDB.DatabaseName;
    FmRestaura2.FDB := DMod.MyDB;
    FmRestaura2.BtDiretoClick(Self);
    FmRestaura2.Close;
  end;
end;

function TUnCTe_PF.DesmontaID_Inutilizacao(const Id: String; var cUF, Ano,
  emitCNPJ, Modelo, Serie, nCTIni, nCTFim: String): Boolean;
var
  K, N, Z: Integer;
begin
  K := Length(Id);
  if K in ([41,42,43]) then
  begin
    Z := 1 + K - 41;
    N := 3;
    cUF      := Copy(Id, 02, N);
    Ano      := Copy(Id, 02, N);
    emitCNPJ := Copy(Id, 14, N);
    Modelo   := Copy(Id, 02, N);
    Serie    := Copy(Id,  Z, N);
    nCTIni   := Copy(Id, 09, N);
    nCTFim   := Copy(Id, 09, N);
    Result := True;
  end else begin
    Result := False;
    Geral.MB_Erro('ID de inutiliza��o com tamanho inv�lido: "' +
    Id + '". Deveria ter 41 carateres e tem ' + IntToStr(K) + '.');
  end;
end;

procedure TUnCTe_PF.EnviaLoteAoFisco(Sincronia: TXXeIndSinc; Lote,
  Empresa: Integer; SohLer: Boolean; Qry: TmySQLQuery);
var
  //ServicoStep: TCTeServicoStep;
  VersaoAcao: String;
begin
  case Sincronia of
    nisAssincrono:
    begin
      //ServicoStep := ctesrvEnvioAssincronoLoteCTe;
      VersaoAcao  := Geral.FFT_Dot(DmCTe_0000.QrOpcoesCTeCTeVerEnvLot.Value, 2, siPositivo);
    end;
    nisSincrono:
    begin
      //ServicoStep := ctesrvEnvioSincronoLoteCTe;
      VersaoAcao  := Geral.FFT_Dot(DmCTe_0000.QrOpcoesCTeCTeVerEnvLot.Value, 2, siPositivo);
    end;
    else
    begin
      Geral.MB_Erro(
        'Forma de sincronismo n�o implementada em "CTe_PF.EnviaLoteAoFisco()"');
      Exit;
    end;
  end;
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  if DBCheck.CriaFm(TFmCTeSteps_0200a, FmCTeSteps_0200a, afmoNegarComAviso) then
  begin
    //FmCTeSteps_0200a.PnLoteEnv.Visible := True;
    FmCTeSteps_0200a.EdVersaoAcao.ValueVariant := DmCTe_0000.QrOpcoesCTeCTeVerEnvLot.Value;
    FmCTeSteps_0200a.CkSoLer.Checked  := SohLer;
    FmCTeSteps_0200a.Show;
    //
    FmCTeSteps_0200a.RGAcao.ItemIndex := Integer(TCTeServicoStep.ctesrvEnvioLoteCTe); // Envia lote ao fisco Assincono ou sincrono
    FmCTeSteps_0200a.RGIndSinc.ItemIndex := Integer(Sincronia);
    FmCTeSteps_0200a.PreparaEnvioDeLoteCTe(Lote, Empresa, Sincronia);
    //FmCTeSteps_0200a.FFormChamou      := 'FmCTeLEnc_0200a';
    FmCTeSteps_0200a.FQry      := Qry;
    //
    //
    //FmCTeSteps_0200a.Destroy;
    //
  end;
end;

function TUnCTe_PF.Evento_MontaId(tpEvento: Integer; Chave: String;
  nSeqEvento: Integer): String;
const
  TamStr = 54;
begin
//ID 110110 3511031029073900013955001000000001105112804108 01
  Result := 'ID' + Geral.FFN(tpEvento, 6) + Chave + Geral.FFn(nSeqEvento, 2);
  if Length(Result) <> TamStr then
    Geral.MensagemBox('Tamanho do Id difere de ' + Geral.FF0(TamStr) +
    '! - CTe_PF.Evento_MontaId', 'ERRO', MB_OK+MB_ICONERROR);
end;

function TUnCTe_PF.Evento_Obtem_DadosEspecificos(const EvePreDef: Integer;
  const EventoCTe: TEventoXXe; var tpEvento: Integer; var versao: Double;
  var descEvento: String): Boolean;
begin
  Result := True;
  tpEvento := nEventoXXe[Integer(EventoCTe)];
  case EventoCTe of
    evexxe110110CCe:
    begin
      descEvento := 'Carta de Correcao';
      versao := Geral.DMV_Dot(verEventosCCe_Evento);
    end;
    evexxe110111Can:
    begin
      descEvento := 'Cancelamento';
      versao := Geral.DMV_Dot(verEventosCan_Evento);
    end;
    evexxe110113EPEC:
    begin
      descEvento := 'EPEC';
      versao := Geral.DMV_Dot(verEventosEPEC_Evento);
    end;
    else begin
      Result := False;
      tpEvento := 0;
      versao := 0;
      descEvento := 'Evento n�o definido em CTe_PF.Evento_Obtem_tpEvento';
      Geral.MensagemBox(descEvento, 'ERRO', MB_OK+MB_ICONERROR);
    end;
  end;
end;

function TUnCTe_PF.InsUpdFaturasCTe((*Filial,*) Empresa, FatID, FatNum,
  CtaFaturas, (*Associada, ASS_CtaFaturas, ASS_Filial,*) Financeiro, CartEmiss,
  CondicaoPG, (*AFP_Sit,*) FaturaNum, IDDuplicata, NumeroCT, FaturaSeq, TipoCart,
  Represen, (*ASS_IDDuplicata, ASS_FaturaSeq,*) Cliente: Integer; (*AFP_Per,*)
  (*FreteVal, Seguro, Desconto, Outros*) vRec, JurosMes: Double; TpDuplicata, FaturaSep, TxtFaturas(*,
  ASS_FaturaSep, ASS_TpDuplicata, ASS_TxtFaturas*): String; DataFat: TDateTime;
  (*QueryPrzT, QuerySumT, QueryPrzX, QuerySumX, QueryCT_X: TmySQLQuery;*)
  SerieCTe, NumeroCTe: Integer): Boolean;
var
  Filial: Integer;
  //
  procedure IncluiLancto(Valor: Double; Data, Vencto: TDateTime; Duplicata,
    Descricao: String; TipoCart, Carteira, Genero, CliInt, Parcela,
    NotaFiscal, Account, SerieCTe: Integer; VerificaCliInt: Boolean);
  var
    TabLctA: String;
  begin
    UFinanceiro.LancamentoDefaultVARS;
    //
    if Financeiro = 1 then
      FLAN_Credito := Valor
    else
    if Financeiro = 2 then
      FLAN_Debito := Valor;
    //
    FLAN_VERIFICA_CLIINT := VerificaCliInt;
    FLAN_Data       := Geral.FDT(Data, 1);
    FLAN_Tipo       := TipoCart;
    FLAN_Documento  := 0;
    FLAN_MoraDia    := JurosMes; //QuerySumT.FieldByName('JurosMes').AsFloat;
    FLAN_Multa      := 0;//
    FLAN_Carteira   := Carteira;
    FLAN_Genero     := Genero;
    FLAN_Cliente    := Cliente;
    FLAN_CliInt     := CliInt;
    //FLAN_Depto      := QrBolacarAApto.Value;
    //FLAN_ForneceI   := QrBolacarAPropriet.Value;
    FLAN_Vencimento := Geral.FDT(Vencto, 1);
    //FLAN_Mez        := IntToStr(MLAGeral.PeriodoToAnoMes(QrPrevPeriodo.Value));
    FLAN_FatID      := (*FThis*)FatID;
    FLAN_FatNum     := FatNum;
    FLAN_FatParcela := Parcela;
    FLAN_Descricao  := Descricao;
    FLAN_Duplicata  := Duplicata;
    FLAN_SerieNF    := Geral.FF0(SerieCTe);
    FLAN_NotaFiscal := NotaFiscal;
    FLAN_Account    := Account;
    TabLctA       := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
    FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', TabLctA, LAN_CTOS, 'Controle');
    //
    if UFinanceiro.InsereLancamento(TabLctA) then
    begin
      // nada
    end;
  end;

var
  //TemAssociada: Boolean;
  TabLctA: String;
  FaturNum, Duplicata, NovaDup(*, CT_Emp_Serie, CT_Ass_Serie*): String;
  V1, V2, F1, F2, P1, P2, T1, T2: Double;
  Parcela(*, CT_Emp_Numer, CT_Ass_Numer*): Integer;
  Qr1, Qr2: TmySQLQuery;
begin
 try
    Result       := False;
    Filial       := DModG.ObtemFilialDeEntidade(Empresa);
    //TemAssociada := (AFP_Sit = 1) and (AFP_Per > 0);
    //
    // Exclui lan�amentos para evitar duplica��o
    if (FatID <> 0) and (FatNum <> 0) then
    begin
      TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
      //
      UFinanceiro.ExcluiLct_FatNum(nil, FatID, FatNum,
        Empresa, 0, 311(*CO_MOTVDEL_311_EXCLUILCTFATURAMENTO*), False, TabLctA);
    end;
    if vRec < 0.01 then
    begin
      Result := True;
      Geral.MB_Aviso('N�o ser� gerado lan�amento financeiro.' + sLineBreak +
        'O valor a receber n�o foi definido!');
      Exit;
    end;
(*
    ReabreQueryCT_X(QueryCT_X, FatID, FatNum, Empresa);
    //
    CT_Emp_Serie := Geral.FF0(SerieCTe);
    CT_Emp_Numer := NumeroCTe;
*)
    //
    if (*(Trim(CT_Emp_Serie) = '') or*) (NumeroCTe = 0) then
    begin
      Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
        'O n�mero do conhecimento de frete n�o foi definido para a empresa ' +
        FormatFloat('000', Filial) + ' !');
      Exit;
    end;
    (*
    if TemAssociada then
    begin
      ReabreQueryCT_X(QueryCT_X, FatID, FatNum, Associada);
      //
      CT_Ass_Serie := QueryCT_X.FieldByName('SerieCTTxt').AsString;
      CT_Ass_Numer := QueryCT_X.FieldByName('NumeroCT').AsInteger;
      //
      if (Trim(CT_Ass_Serie) = '') or (CT_Ass_Numer = 0) then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
          'O n�mero de nota fiscal n�o foi definida para a empresa ' +
          FormatFloat('000', ASS_FILIAL) + ' !' + sLineBreak +
          'Para definir o n�mero de nota fiscal clique no bot�o "Fatura" > "'+
          'Nota Fiscal"');
        Exit;
      end;
      //
      P2 := AFP_Per;
      P1 := 100 - P2;
    end else
    begin
      P1 := 100;
      P2 := 0;
      CT_Ass_Serie := '';
      CT_Ass_Numer := 0;
    end;
    *)
    if CtaFaturas = 0 then
    begin
      Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
        'A conta (do plano de contas) da presta��o do servi�o de frete n�o foi definida para a empresa ID = ' +
        Geral.FF0(Filial) + '!');
      Exit;
    end;
(*
    if (Associada <> 0) and (ASS_CtaFaturas = 0) and TemAssociada then
    begin
      Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
        'A conta de venda de produtos n�o foi definida para a empresa ID = ' +
          Geral.FF0(ASS_Filial) + '!');
      Exit;
    end;
*)
    if Financeiro > 0 then
    begin
      if CartEmiss = 0 then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
          'A carteira n�o foi definida no pedido selecionado!');
        Exit;
      end;
      Qr1 := TmySQLQuery.Create(Dmod);
      try
      Qr2 := TmySQLQuery.Create(Dmod);
      try
        //ReabreQueryPrzT(QueryPrzT, CondicaoPG);
        UnDmkDAC_PF.AbreMySQLQuery0(Qr1, Dmod.MyDB, [
        'SELECT Controle, Dias, Percent1, Percent2 ',
        'FROM pediprzits ',
        'WHERE Codigo=' + Geral.FF0(CondicaoPG),
        'ORDER BY Dias ',
        '']);

        if Qr1.RecordCount = 0 then
        begin
          Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
            'N�o h� parcela(s) definida(s) para a empresa ' +
            Geral.FF0(Filial) + ' na condi��o de pagamento ' +
            'cadastrada no pedido selecionado!');
          Exit;
        end;
        (*
        if TemAssociada then
        begin
          ReabreQuerySumT(QuerySumT, CondicaoPG);
          //
          if (QuerySumT.FieldByName('Percent2').AsFloat <> AFP_Per) then
          begin
            Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
              'Percentual da fatura parcial no pedido n�o confere com percentual ' +
              'nos prazos da condi��o de pagamento!');
            Exit;
          end;
          if QuerySumT.RecordCount = 0 then
          begin
            Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
              'Percentual da fatura parcial no pedido n�o confere com percentual ' +
              'nos prazos da condi��o de pagamento!');
            Exit;
          end;
        end;
        *)
      //
      //ReabreQuerySumX(QuerySumX, FatID, FatNum, Empresa);
      //
      T1 := vRec;
      F1 := T1;
      //
      //ReabreQueryPrzX(QueryPrzX, CondicaoPG, 'Percent1');
      UnDmkDAC_PF.AbreMySQLQuery0(Qr2, Dmod.MyDB, [
      'SELECT Controle, Dias, Percent1 ',
      'FROM pediprzits ',
      'WHERE Codigo=' + Geral.FF0(CondicaoPG),
      'AND Percent1 > 0 ',
      'ORDER BY Dias ',
      '']);
      //
      Qr2.First;
      while not Qr2.Eof do
      begin
        if Qr2.RecordCount = Qr2.RecNo then
          V1 := F1
        else
        begin
          if P1 = 0 then
            V1 := 0
          else
            V1 := (Round(T1 * (Qr2.FieldByName('Percent').AsFloat / P1 * 100))) / 100;
          F1 := F1 - V1;
        end;
        Qr1.Locate('Controle', Qr2.FieldByName('Controle').AsInteger, []);
        //
        Parcela := Qr1.RecNo;
        //
        if FaturaNum = 0 then
          FaturNum := FormatFloat('000000', IDDuplicata)
        else
          FaturNum := FormatFloat('000000', NumeroCT);
        Duplicata := TpDuplicata + FaturNum + FaturaSep +
          dmkPF.ParcelaFatura(Qr2.RecNo, FaturaSeq);
        // 2011-08-21
        // Teste para substituir no futuro (uso no form FmFatDivCms)
        NovaDup := dmkPF.MontaDuplicata(IDDuplicata, NumeroCT, Parcela,
                     FaturaNum, FaturaSeq, TpDuplicata, FaturaSep);
        //
(*
      if NovaDup <> Duplicata then
          Geral.MB_Aviso('Defini��o da duplicata:' + sLineBreak + Duplicata + ' <> ' + NovaDup)
        else
          Geral.MB_Aviso('Defini��o da duplicata:' + sLineBreak + Duplicata + ' = ' + NovaDup);
*)
        // Fim 2011-08-21
        IncluiLancto(V1, DataFat, DataFat + Qr2.FieldByName('Dias').AsInteger,
          Duplicata, TxtFaturas, TipoCart, CartEmiss, CtaFaturas, Empresa, Parcela,
          NumeroCTe, Represen, SerieCTe, True);
        //
        Qr2.Next;
      end;
      (*
      // Faturamento associada
      if TemAssociada then
      begin
        ReabreQuerySumX(QuerySumX, FatID, FatNum, Associada);
        //
        T2 := QuerySumX.FieldByName('Total').AsFloat;
        F2 := T2;
        //
        ReabreQueryPrzX(QueryPrzX, CondicaoPG, 'Percent2');
        //
        QueryPrzX.First;
        while not QueryPrzX.Eof do
        begin
          if QueryPrzX.RecordCount = QueryPrzX.RecNo then
            V2 := F2
          else begin
            if P2 = 0 then V2 := 0 else
              V2 := (Round(T2 * (QueryPrzX.FieldByName('Percent').AsFloat / P2 * 100))) / 100;
            F2 := F2 - V2;
          end;
          Qr1.Locate('Controle', QueryPrzX.FieldByName('Controle').AsInteger, []);
          Parcela := Qr1.RecNo;
          //
          if FaturaNum = 0 then
            FaturNum := FormatFloat('000000', ASS_IDDuplicata)
          else
            FaturNum := FormatFloat('000000', NumeroCT);
          //
          Duplicata := ASS_TpDuplicata + FormatFloat('000000', ASS_IDDuplicata) +
            ASS_FaturaSep + dmkPF.ParcelaFatura(
            QueryPrzX.RecNo, ASS_FaturaSeq);
          IncluiLancto(V2, DataFat, DataFat + QueryPrzX.FieldByName('Dias').AsInteger,
            Duplicata, ASS_TxtFaturas, TipoCart, CartEmis, ASS_CtaFaturas,
            Associada, Parcela, CT_Ass_Numer, Represen, CT_Ass_Serie, False);
          //
          QueryPrzX.Next;
        end;
      end;
      *)
      finally
        Qr2.Free;
      end;
      finally
        Qr1.Free;
      end;
    end;
    Result := True;
  except
    Result := False;
  end;
end;

function TUnCTe_PF.MontaChaveDeAcesso(cUF: Integer; Emissao: TDateTime;
  CNPJ: String; Modelo, Serie, NumeroCT: Integer; var ide_cCT: Integer; var cDV,
  ChaveDeAcesso: String; const cCT_Atual, ide_tpEmis: Integer;
  versao: Double): Boolean;
var
  UF_IBGE, AAMM, ModCTe, Mensagem, SerCTe, NumCTe, AleCTe, tpEmis: String;
begin
  Mensagem := '';
  Result := False;
  UF_IBGE := FormatFloat('00', cUF);
  if (cUF < FMinUF_IBGE) or (cUF > FMaxUF_IBGE) then
    Mensagem := 'C�digo IBGE da UF inv�lido: ' + UF_IBGE;
  if Length(UF_IBGE) <> 2 then
    Mensagem := 'C�digo IBGE da UF inv�lido: ' + UF_IBGE;
  //
  AAMM := FormatDateTime('YYMM', Emissao);
  if Emissao < 2 then
    Mensagem := 'Ano/m�s de emiss�o inv�lido: ' + AAMM;
  //
  if Length(CNPJ) < 14 then
    Mensagem := 'Tamanho inv�lido para o CNPJ!';
  //
  ModCTe := FormatFloat('00', Modelo);
  if Length(ModCTe) <> 2 then
    Mensagem := 'Modelo de CTe inv�lido: ' + ModCTe;
  //
  SerCTe := FormatFloat('000', Serie);
  if Length(SerCTe) <> 3 then
    Mensagem := 'S�rie da CTe inv�lida: ' + SerCTe;
  //
  NumCTe := FormatFloat('000000000', NumeroCT);
  if Length(NumCTe) <> 9 then
    Mensagem := 'N�mero de CTe inv�lido: ' + NumCTe;
  //
  begin
    // vers�o 2.00
    if (cCT_Atual <> 0) and (cCT_Atual < 100000000) then
    begin
      ide_cCT := cCT_Atual;
      Geral.MensagemBox('Foi detectado que o c�digo num�rico j� existe para esta CT-e.'
      + sLineBreak + 'Ser� utilizado o mesmo: ' + FormatFloat('00000000', ide_cCT),
      'Aviso', MB_OK+MB_ICONWARNING);
    end else begin
      //  N�mero aleat�rio
      Randomize;
      ide_cCT := 0;
      while (ide_cCT <= 0) or (ide_cCT > 99999998) do
        ide_cCT := Random(100000000);
    end;
    //
    AleCTe := FormatFloat('00000000', ide_cCT);
    if Length(AleCTe) <> 8 then
      Mensagem := 'C�digo num�rico da chave da CTe inv�lido: ' + AleCTe;
    //
    Geral.MB_Info('Ver de onde veio o **TpEmis**!');
    tpEmis := FormatFloat('0', ide_tpEmis);
    if (ide_tpEmis < 1) or (ide_tpEmis > 5) then
      Mensagem := 'Tipo de emiss�o (#25 ID: B21) inv�lido: ' + tpEmis;
    //
    if ((ide_tpEmis <> 3) and (Serie >= 900))
    or ((ide_tpEmis = 3) and (Serie < 900)) then
      Mensagem := 'Tipo de emiss�o (#25 ID: B21): "' + tpEmis +
      '" � incompat�vel com a s�rie: "' + SerCTe + '"';

    // Nova concatena��o para a chave de acesso na CTe 2.0
    ChaveDeAcesso :=
      UF_IBGE + AAMM + CNPJ + ModCTe + SerCTe + NumCTe + tpEmis + AleCTe;
  end;
  cDV := Geral.Modulo11_2a9_Back(ChaveDeAcesso, 0);
  ChaveDeAcesso := ChaveDeAcesso + cDV;
  if Length(ChaveDeAcesso) <> 44 then
    Mensagem := 'Tamanho da chave de acesso inv�lido. Deveria ser 44 mas � ' +
    IntToStr(Length(ChaveDeAcesso));
  if Mensagem <> '' then
    Geral.MB_Aviso(Mensagem + #13+#10 + 'Function: "MontaChaveDeAcesso"')
  else
    Result := True;
end;

procedure TUnCTe_PF.MostraFormCTeCTaEdit_0200a(Empresa, FrtFatCab: Integer;
  QrFrtFatCab: TmySQLQuery; SerieCTe, NumeroCTe: Integer; vRec: Double);
begin
  Application.CreateForm(TFmCTeCTaEdit_0200a, FmCTeCTaEdit_0200a);
  //
  FmCTeCTaEdit_0200a.EdCodigo.ValueVariant    := FrtFatCab;
  FmCTeCTaEdit_0200a.FQrFrtFatCab             := QrFrtFatCab;
  //
  FmCTeCTaEdit_0200a.EdEmpresa.ValueVariant   := Empresa;
  FmCTeCTaEdit_0200a.EdSerieCTe.ValueVariant  := SerieCTe;
  FmCTeCTaEdit_0200a.EdNumeroCTe.ValueVariant := NumeroCTe;
  FmCTeCTaEdit_0200a.EdvRec.ValueVariant      := vRec;
  //
  FmCTeCTaEdit_0200a.ShowModal;
  FmCTeCTaEdit_0200a.Destroy;
end;

procedure TUnCTe_PF.MostraFormCTeEveRLoE(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCTeEveRLoE, FmCTeEveRLoE, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCTeEveRLoE.LocCod(Codigo, Codigo);
    FmCTeEveRLoE.ShowModal;
    FmCTeEveRLoE.Destroy;
  end;
end;

procedure TUnCTe_PF.MostraFormCTeInut();
begin
  {
  if DmCTe_0000.AppCodeCTe(Geral.IMV(VAR_LIB_EMPRESAS)) = 0 then
  begin
  }
    if DBCheck.CriaFm(TFmCTeInut_0000, FmCTeInut_0000, afmoNegarComAviso) then
    begin
      FmCTeInut_0000.ShowModal;
      FmCTeInut_0000.Destroy;
    end;
  {
  end else
  begin
    if DBCheck.CriaFm(TFmCTeInut, FmCTeInut, afmoNegarComAviso) then
    begin
      FmCTeInut.ShowModal;
      FmCTeInut.Destroy;
    end;
  end;
  }
end;

procedure TUnCTe_PF.MostraFormCTeJust();
begin
  if DBCheck.CriaFm(TFmCTeJust, FmCTeJust, afmoNegarComAviso) then
  begin
    FmCTeJust.ShowModal;
    FmCTeJust.Destroy;
  end;
end;

procedure TUnCTe_PF.MostraFormCTeLayout_0200();
begin
  Application.CreateForm(TFmCTeLayout_0200, FmCTeLayout_0200);
  FmCTeLayout_0200.ShowModal;
  FmCTeLayout_0200.Destroy;
end;

procedure TUnCTe_PF.MostraFormCTeLEnc(Codigo: Integer);
var
  VersaoCTe: Integer;
begin
  VersaoCTe := VersaoCTeEmUso();
  //
  case VersaoCTe of
    200:
    if DBCheck.CriaFm(TFmCTeLEnc_0200a, FmCTeLEnc_0200a, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
        FmCTeLEnc_0200a.LocCod(Codigo, Codigo);
      FmCTeLEnc_0200a.ShowModal;
      FmCTeLEnc_0200a.Destroy;
    end;
    else AvisoNaoImplemVerCTe(VersaoCTe);
  end;
end;

procedure TUnCTe_PF.MostraFormCTePesq(AbrirEmAba: Boolean; InOwner: TWincontrol;
  AdvToolBarPager: TAdvToolBarPager; Cliente: Integer);
var
  Form: TForm;
begin
  if AbrirEmAba then
  begin
    Form := MyObjects.FormTDICria(TFmCTe_Pesq_0000, InOwner, AdvToolBarPager, True, True);
    //
    if Cliente <> 0 then
    begin
      if Form <> nil then
      begin
        TFmCTe_Pesq_0000(Form).EdCliente.ValueVariant := Cliente;
        TFmCTe_Pesq_0000(Form).CBCliente.KeyValue     := Cliente;
      end;
    end;
  end else
  begin
    if DBCheck.CriaFm(TFmCTe_Pesq_0000, FmCTe_Pesq_0000, afmoNegarComAviso) then
    begin
      if Cliente <> 0 then
      begin
        FmCTe_Pesq_0000.EdCliente.ValueVariant := Cliente;
        FmCTe_Pesq_0000.CBCliente.KeyValue     := Cliente;
      end;
      FmCTe_Pesq_0000.ShowModal;
      FmCTe_Pesq_0000.Destroy;
    end;
  end;
end;

procedure TUnCTe_PF.MostraFormStepsCTe_Cancelamento(Empresa, Serie,
  nCT, nSeqEvento, Justif: Integer; Modelo, nProtCan, chCTe, XML_Evento: String;
  Qry: TmySQLQuery);
var
  VersaoCTe: Integer;
begin
  VersaoCTe := VersaoCTeEmUso();
  case VersaoCTe of
    200:
    if DBCheck.CriaFm(TFmCTeSteps_0200a, FmCTeSteps_0200a, afmoNegarComAviso) then
    begin
      //
      FmCTeSteps_0200a.EdVersaoAcao.ValueVariant := DmCTe_0000.QrOpcoesCTeCTeVerCanCTe.Value;
      FmCTeSteps_0200a.Show;
      //
      FmCTeSteps_0200a.RGAcao.ItemIndex := Integer(TCTeServicoStep.ctesrvPedirCancelamentoCTe); // 3 -Pedido de cancelamento
      FmCTeSteps_0200a.PreparaCancelamentoDeCTe(Empresa, Serie, nCT, nSeqEvento,
        Justif, Modelo, nProtCan, chCTe, XML_Evento);
      FmCTeSteps_0200a.FQry := Qry;
      //
      //
      //FmCTeSteps_0200a.Destroy;
      //
    end;
    else AvisoNaoImplemVerCTe(VersaoCTe);
  end;
end;

procedure TUnCTe_PF.MostraFormStepsCTe_CartaDeCorrecao(Empresa, Serie, nCT,
  nSeqEvento: Integer; Modelo, nProt, chCTe, XML_Evento: String;
  Qry: TmySQLQuery);
var
  VersaoCTe: Integer;
begin
  VersaoCTe := VersaoCTeEmUso();
  case VersaoCTe of
    200:
    if DBCheck.CriaFm(TFmCTeSteps_0200a, FmCTeSteps_0200a, afmoNegarComAviso) then
    begin
      //
      FmCTeSteps_0200a.EdVersaoAcao.ValueVariant := DmCTe_0000.QrOpcoesCTeCTeVerLotEve.Value;
      FmCTeSteps_0200a.Show;
      //
      FmCTeSteps_0200a.RGAcao.ItemIndex := Integer(TCTeServicoStep.ctesrvEventoCartaCorrecao); // 6 -Carta de correcao
      FmCTeSteps_0200a.PreparaCartaDeCorrecao(Empresa, Serie, nCT, nSeqEvento,
      Modelo, nProt, ChCte, XML_Evento);
      FmCTeSteps_0200a.FQry := Qry;
      //
      //
      //FmCTeSteps_0200a.Destroy;
      //
    end;
    else AvisoNaoImplemVerCTe(VersaoCTe);
  end;
end;

procedure TUnCTe_PF.MostraFormStepsCTe_ConsultaCTe(Empresa, IDCtrl: Integer;
  ChaveCTe: String; MeXML: TMemo);
var
  VersaoCTe: Integer;
begin
  if MyObjects.FIC(Length(ChaveCTe) <> 44, nil, 'Informe a chave da CTe') then
    Exit;
  VersaoCTe := VersaoCTeEmUso();
  case VersaoCTe of
    200:
    if DBCheck.CriaFm(TFmCTeSteps_0200a, FmCTeSteps_0200a, afmoNegarComAviso) then
    begin
      //
      FmCTeSteps_0200a.EdVersaoAcao.ValueVariant := DmCTe_0000.QrOpcoesCTeCTeVerConCTe.Value; // Consulta CTe
      FmCTeSteps_0200a.Show;
      //
      FmCTeSteps_0200a.RGAcao.ItemIndex := Integer(TCTeServicoStep(ctesrvConsultarCTe)); // Consulta CTe
      //DefineVarsDePreparacao();
      FmCTeSteps_0200a.PreparaConsultaCTe(Empresa, IDCtrl, ChaveCTe);
      //
      //
      //FmCTeSteps_0200a.Destroy;
      //
    end;
  end;
end;

procedure TUnCTe_PF.MostraFormStepsCTe_EPEC(Empresa, Serie, nCT, nSeqEvento,
  Justif: Integer; Modelo, chCTe, XML_Evento: String; Qry: TmySQLQuery);
var
  VersaoCTe: Integer;
begin
  VersaoCTe := VersaoCTeEmUso();
  case VersaoCTe of
    200:
    if DBCheck.CriaFm(TFmCTeSteps_0200a, FmCTeSteps_0200a, afmoNegarComAviso) then
    begin
      //
      FmCTeSteps_0200a.EdVersaoAcao.ValueVariant := DmCTe_0000.QrOpcoesCTeCTeVerEPEC.Value;
      FmCTeSteps_0200a.Show;
      //
      FmCTeSteps_0200a.RGAcao.ItemIndex := Integer(TCTeServicoStep.ctesrvEPEC); // 8 -EPEC
      FmCTeSteps_0200a.PreparaEPEC(Empresa, Serie, nCT, nSeqEvento,
        Justif, Modelo, chCTe, XML_Evento);
      FmCTeSteps_0200a.FQry := Qry;
      //
      //
      //FmCTeSteps_0200a.Destroy;
      //
    end;
    else AvisoNaoImplemVerCTe(VersaoCTe);
  end;
end;

procedure TUnCTe_PF.MostraFormStepsCTe_Inutilizacao(Empresa, Lote, Ano, Modelo,
  Serie, nCTIni, nCTFim, Justif: Integer);
var
  VersaoCTe: Integer;
begin
  VersaoCTe := VersaoCTeEmUso();
  case VersaoCTe of
    200:
    if DBCheck.CriaFm(TFmCTeSteps_0200a, FmCTeSteps_0200a, afmoNegarComAviso) then
    begin
      //
      //FmCTeSteps_0200a.PnLoteEnv.Visible := True;
      FmCTeSteps_0200a.EdVersaoAcao.ValueVariant := DmCTe_0000.QrOpcoesCTeCTeVerInuNum.Value;
      FmCTeSteps_0200a.Show;
      //
      FmCTeSteps_0200a.RGAcao.ItemIndex := Integer(TCTeServicoStep.ctesrvPedirInutilizacaoNumerosCTe); // 4 -Pedido de inutiliza��o
      FmCTeSteps_0200a.PreparaInutilizaNumerosCTe(Empresa, Lote, Ano, Modelo,
        Serie, nCTIni, nCTFim, Justif);
      //
      //
      //FmCTeSteps_0200a.Destroy;
      //
    end;
    else AvisoNaoImplemVerCTe(VersaoCTe);
  end;
end;

procedure TUnCTe_PF.MostraFormStepsCTe_StatusServico();
var
  VersaoCTe: Integer;
begin
  VersaoCTe := VersaoCTeEmUso();
  //
  case VersaoCTe of
    200:
    if DBCheck.CriaFm(TFmCTeSteps_0200a, FmCTeSteps_0200a, afmoNegarComAviso) then
    begin
      //
      //FmCTeSteps_0200a.PnLoteEnv.Visible := True;
      FmCTeSteps_0200a.EdVersaoAcao.ValueVariant := DmCTe_0000.QropcoesCTeCTeVerStaSer.Value;
      FmCTeSteps_0200a.Show;
      //
      FmCTeSteps_0200a.RGAcao.ItemIndex := 0; // Status do servi�o
      FmCTeSteps_0200a.PreparaVerificacaoStatusServico(DmodG.QrFiliLogCodigo.Value);
      //
      //FmCTeSteps_0200a.Destroy;
      //
    end;
    else AvisoNaoImplemVerCTe(VersaoCTe);
  end;
end;

procedure TUnCTe_PF.MostraFormStepsCTe_StepGenerico();
var
  CliInt: Integer;
var
  VersaoCTe: Integer;
begin
  VersaoCTe := VersaoCTeEmUso();
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  //
  case VersaoCTe of
    200:
    if DBCheck.CriaFm(TFmCTeSteps_0200a, FmCTeSteps_0200a, afmoSoMaster) then
    begin
      FmCTeSteps_0200a.PreparaStepGenerico(CliInt);
      //
      //FmCTeSteps_0200a.PnLoteEnv.Visible  := True;
      FmCTeSteps_0200a.RGAcao.Enabled     := True;
      FmCTeSteps_0200a.PnAbrirXML.Visible := True;
      FmCTeSteps_0200a.PnAbrirXML.Enabled := True;
      FmCTeSteps_0200a.CkSoLer.Enabled    := True;
      //FmCTeSteps_0200a.EdchCTe.ReadOnly   := False;
      FmCTeSteps_0200a.PnConfirma.Visible := True;
      FmCTeSteps_0200a.ShowModal;
      FmCTeSteps_0200a.Destroy;
    end;
    else AvisoNaoImplemVerCTe(VersaoCTe);
  end;
end;

function TUnCTe_PF.ObtemURLWebServerCTe(UF, Servico, Versao: String;
  tpAmb: Integer): String;
var
  UF_Str, Servico_Str, tpAmb_Str: String;
begin
(*Sefa Paran� - (PR)
Servi�o	URL
CteRecepcao
CteRetRecepcao
CteCancelamento	https://homologacao.cte.fazenda.pr.gov.br/cte/CteCancelamento?wsdl
CteInutilizacao	https://homologacao.cte.fazenda.pr.gov.br/cte/CteInutilizacao?wsdl
CteConsultaProtocolo	https://homologacao.cte.fazenda.pr.gov.br/cte/CteConsulta?wsdl
CteStatusServico	https://homologacao.cte.fazenda.pr.gov.br/cte/CteStatusServico?wsdl
CteRecepcaoEvento	https://homologacao.cte.fazenda.pr.gov.br/cte/CteRecepcaoEvento?wsdl
*)
  //Result := 'https://homologacao.cte.fazenda.pr.gov.br/cte/CteStatusServico?wsdl';
  Result := '';
  //
  try
    UF_Str      := Trim(LowerCase(UF));
    Servico_Str := Trim(LowerCase(Servico));
    tpAmb_Str   := Geral.FF0(tpAmb);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAllAux, DModG.AllID_DB, [
      'SELECT URL ',
      'FROM CTe_ws ',
      'WHERE LOWER(UF) = "' + UF_Str + '" ',
      'AND LOWER(Servico) = "' + Servico_Str + '" ',
      'AND tpAmb = "' + tpAmb_Str + '" ',
      'AND Versao = "' + Versao + '" ',
      '']);
    if DModG.QrAllAux.RecordCount > 0 then
      Result := DModG.QrAllAux.FieldByName('URL').AsString;
  finally
    DModG.QrAllAux.Close;
  end;
end;

function TUnCTe_PF.Obtem_Serie_e_NumCT(const SerieDesfeita, CTeDesfeita,
  (*SerieNormal,*) ParamsCTe, FatID, FatNum, Empresa(*, Filial, MaxSeq*): Integer;
  const Tabela: String;
  var SerieCTe, NumeroCTe: Integer): Boolean;
var
  SerieNormal, MaxSeqLib, SerieDesfe, CTeDesfe: Integer;
  Qry: TmySQLQuery;
begin
  Result := False;
  DModG.ReopenParamsEmp(Empresa);
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SerieCTe, Sequencial, MaxSeqLib  ',
    'FROM paramscte ',
    'WHERE Controle=' + Geral.FF0(ParamsCTe),
    '']);
    SerieNormal := Qry.FieldByName('SerieCTe').AsInteger;
    MaxSeqLib   := Qry.FieldByName('MaxSeqLib').AsInteger;
    //
  finally
    Qry.Free;
  end;
  //
  //Geral.MB_Info('Desmarcar aqui [1]');
  if (SerieDesfeita = SerieNormal) and (CTeDesfeita <> 0) then
  begin
    Geral.MB_Info('Foi detectado que j� havia sido usado o n�mero ' +
    FormatFloat('000000000', CTeDesfeita) +
    ' para esta CTe. Ser� usado este mesmo n�mero novamente na recria��o!');
    SerieCTe  := SerieDesfeita;
    NumeroCTe := CTeDesfeita;
    Result := True;
  end else
  begin
    NumeroCTe := 0;
    case DmCTe_0000.QropcoesCTeCTetpEmis.Value of
      1:
      begin
        SerieCTe := SerieNormal;
        Result   := DModG.BuscaProximoCodigoInt_Novo('paramscte', 'Sequencial',
        'WHERE Controle=' + dmkPF.FFP(ParamsCTe, 0), 0, 'S�rie: ' +
        FormatFloat('000', SerieCTe) +
        sLineBreak + 'Empresa: ' + dmkPF.FFP(Empresa, 0) + sLineBreak +
        'Verifique a configura��o da CT-e da filal informada!',
        NumeroCTe, MaxSeqLib);
      end;
      3:
      begin
        if Geral.MB_Pergunta(
        'A filial selecionada est� configurada para envio de CT-e em conting�ncia SCAN!'
        + sLineBreak + 'Tem certeza que deseja continuar?') = ID_YES then
        begin
          Result := DModG.BuscaProximoCodigoInt_Novo('paramsemp', 'SCAN_nCT',
          'WHERE Codigo=' + dmkPF.FFP(Empresa, 0), 0, 'S�rie: ' +
          FormatFloat('000', SerieCTe) +
          sLineBreak + 'Empresa: ' + dmkPF.FFP(Empresa, 0) + sLineBreak +
          'Verifique a configura��o da CT-e em SCAN da filal informada!',
          NumeroCTe, MaxSeqLib);
        end;
      end;
    end;
    //
    if NumeroCTe > 0 then
    begin
      SerieDesfe := SerieCTe;
      CTeDesfe    := NumeroCTe;
      if Lowercase(Tabela) = 'frtfatcab' then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'frtfatcab', False, [
        'SerieDesfe', 'CTDesfeita'], [
        'Codigo'], [
        SerieDesfe, CTeDesfe], [
        FatNum], True);
      end else
        Geral.MB_Aviso('Tabela desconhecida: "' + Tabela +
        '" para S�rie e N�mero de CTe desfeito!');
    end;
  end;
end;

function TUnCTe_PF.SalvaXML(Arquivo, XML: String): Boolean;
var
  TxtFile: TextFile;
begin
//  Result := False;
  AssignFile(TxtFile, Arquivo);
  Rewrite(TxtFile);
  Write(TxtFile, XML);
  CloseFile(TxtFile);
  //
  Result := True;
end;

function TUnCTe_PF.Texto_StatusCTe(Status: Integer; VersaoCTe: Double): String;
var
  Versao: Integer;
begin
  Versao := Trunc(VersaoCTe * 100 + 0.5);
  if Versao = 0 then
    Versao := Trunc(CTE_EMI_VERSAO_MAX_USO * 100 + 0.5);
  case Versao of
    200:
    case Status of
      0  : Result := '';
      //
      10 : Result := 'Registros da CT-e inclu�dos na Base da Dados';
      20 : Result := 'XML da CT-e foi gerada';
      30 : Result := 'XML da CT-e est� assinado';
      40 : Result := 'Lote de CTe(s) rejeitado';
      50 : Result := 'CT-e adicionada ao lote';
      60 : Result := 'Lote ao qual a CT-e pertence foi enviado';
      70 : Result := 'Lote ao qual a CT-e pertence em fase de consulta';
      //
      //Tabela de C�digos de Erros e Descri��es de Mensagens de Erros
      //C�DIGO RESULTADO DO PROCESSAMENTO DA SOLICITA��O
      100 : Result := 'Autorizado o uso do CT-e';
      101 : Result := 'Cancelamento de CT-e homologado';
      102 : Result := 'Inutiliza��o de n�mero homologado';
      103 : Result := 'Lote recebido com sucesso';
      104 : Result := 'Lote processado';
      105 : Result := 'Lote em processamento';
      106 : Result := 'Lote n�o localizado';
      107 : Result := 'Servi�o em Opera��o';
      108 : Result := 'Servi�o Paralisado Momentaneamente (curto prazo)';
      109 : Result := 'Servi�o Paralisado sem Previs�o';
      110 : Result := 'Uso Denegado';
      111 : Result := 'Consulta cadastro com uma ocorr�ncia';
      112 : Result := 'Consulta cadastro com mais de uma ocorr�ncia';
      113 : Result := 'Servi�o SVC em opera��o. Desativa��o prevista para a UF em dd/mm/aa, �s hh:mm horas';
      114 : Result := 'SVC-[SP/RS] desabilitada pela SEFAZ de Origem';
      134 : Result := 'Evento registrado e vinculado ao CT-e com alerta para situa��o do documento. [Alerta Situa��o do CT-e: XXXXXXXXXX]';
      135 : Result := 'Evento registrado e vinculado a CT-e';
      136 : Result := 'Evento registrado, mas n�o vinculado a CT-e';
      //C�DIGO MOTIVOS DE N�O ATENDIMENTO DA SOLICITA��O';
      201 : Result := 'Rejei��o: O n�mero m�ximo de numera��o de CT-e a inutilizar ultrapassou o limite';
      202 : Result := 'Rejei��o: Falha no reconhecimento da autoria ou integridade do arquivo digital';
      203 : Result := 'Rejei��o: Emissor n�o habilitado para emiss�o do CT-e';
      204 : Result := 'Rejei��o: Duplicidade de CT-e [nRec:999999999999999]';
      205 : Result := 'Rejei��o: CT-e est� denegado na base de dados da SEFAZ';
      206 : Result := 'Rejei��o: N�mero de CT-e j� est� inutilizado na Base de dados da SEFAZ';
      207 : Result := 'Rejei��o: CNPJ do emitente inv�lido';
      208 : Result := 'Rejei��o: CNPJ do destinat�rio inv�lido';
      209 : Result := 'Rejei��o: IE do emitente inv�lida';
      210 : Result := 'Rejei��o: IE do destinat�rio inv�lida';
      211 : Result := 'Rejei��o: IE do substituto inv�lida';
      212 : Result := 'Rejei��o: Data de emiss�o CT-e posterior a data de recebimento';
      213 : Result := 'Rejei��o: CNPJ-Base do Emitente difere do CNPJ-Base do Certificado Digital';
      214 : Result := 'Rejei��o: Tamanho da mensagem excedeu o limite estabelecido';
      215 : Result := 'Rejei��o: Falha no schema XML';
      216 : Result := 'Rejei��o: Chave de Acesso difere da cadastrada';
      217 : Result := 'Rejei��o: CT-e n�o consta na base de dados da SEFAZ';
      218 : Result := 'Rejei��o: CT-e j� est� cancelado na base de dados da SEFAZ';
      219 : Result := 'Rejei��o: Circula��o do CT-e verificada';
      220 : Result := 'Rejei��o: CT-e autorizado h� mais de 7 dias (168 horas)';
      221 : Result := 'Rejei��o: Confirmado a presta��o do servi�o do CT-e pelo destinat�rio';
      222 : Result := 'Rejei��o: Protocolo de Autoriza��o de Uso difere do cadastrado';
      223 : Result := 'Rejei��o: CNPJ do transmissor do lote difere do CNPJ do transmissor da consulta';
      224 : Result := 'Rejei��o: A faixa inicial � maior que a faixa final';
      225 : Result := 'Rejei��o: Falha no Schema XML do CT-e';
      226 : Result := 'Rejei��o: C�digo da UF do Emitente diverge da UF autorizadora';
      227 : Result := 'Rejei��o: Erro na composi��o do Campo ID';
      228 : Result := 'Rejei��o: Data de Emiss�o muito atrasada';
      229 : Result := 'Rejei��o: IE do emitente n�o informada';
      230 : Result := 'Rejei��o: IE do emitente n�o cadastrada';
      231 : Result := 'Rejei��o: IE do emitente n�o vinculada ao CNPJ';
      232 : Result := 'Rejei��o: IE do destinat�rio n�o informada';
      233 : Result := 'Rejei��o: IE do destinat�rio n�o cadastrada';
      235 : Result := 'Rejei��o: Inscri��o SUFRAMA inv�lida';
      236 : Result := 'Rejei��o: Chave de Acesso com d�gito verificador inv�lido';
      237 : Result := 'Rejei��o: CPF do destinat�rio inv�lido';
      238 : Result := 'Rejei��o: Cabe�alho - Vers�o do arquivo XML superior a Vers�o vigente';
      239 : Result := 'Rejei��o: Cabe�alho - Vers�o do arquivo XML n�o suportada';
      240 : Result := 'Rejei��o: Cancelamento/Inutiliza��o - Irregularidade Fiscal do Emitente';
      241 : Result := 'Rejei��o: Um n�mero da faixa j� foi utilizado';
      242 : Result := 'Rejei��o: Elemento cteCabecMsg inexistente no SOAP Header';
      243 : Result := 'Rejei��o: XML Mal Formado';
      245 : Result := 'Rejei��o: CNPJ Emitente n�o cadastrado';
      246 : Result := 'Rejei��o: CNPJ Destinat�rio n�o cadastrado';
      247 : Result := 'Rejei��o: Sigla da UF do Emitente diverge da UF autorizadora';
      248 : Result := 'Rejei��o: UF do Recibo diverge da UF autorizadora';
      249 : Result := 'Rejei��o: UF da Chave de Acesso diverge da UF autorizadora';
      250 : Result := 'Rejei��o: UF diverge da UF autorizadora';
      251 : Result := 'Rejei��o: UF/Munic�pio destinat�rio n�o pertence a SUFRAMA';
      252 : Result := 'Rejei��o: Ambiente informado diverge do Ambiente de recebimento';
      253 : Result := 'Rejei��o: D�gito Verificador da chave de acesso composta inv�lido';
      254 : Result := 'Rejei��o: CT-e a ser complementado n�o informado para CT-e complementar';
      256 : Result := 'Rejei��o: Um n�mero de CT-e da faixa est� inutilizado na Base de dados da SEFAZ';
      257 : Result := 'Rejei��o: Solicitante n�o habilitado para emiss�o do CT-e';
      258 : Result := 'Rejei��o: CNPJ da consulta inv�lido';
      259 : Result := 'Rejei��o: CNPJ da consulta n�o cadastrado como contribuinte na UF';
      260 : Result := 'Rejei��o: IE da consulta inv�lida';
      261 : Result := 'Rejei��o: IE da consulta n�o cadastrada como contribuinte na UF';
      262 : Result := 'Rejei��o: UF n�o fornece consulta por CPF';
      263 : Result := 'Rejei��o: CPF da consulta inv�lido';
      264 : Result := 'Rejei��o: CPF da consulta n�o cadastrado como contribuinte na UF';
      265 : Result := 'Rejei��o: Sigla da UF da consulta difere da UF do Web Service';
      266 : Result := 'Rejei��o: S�rie utilizada n�o permitida no Web Service';
      267 : Result := 'Rejei��o: CT-e Complementar referencia um CT-e inexistente';
      268 : Result := 'Rejei��o: CT-e Complementar referencia outro CT-e Complementar';
      269 : Result := 'Rejei��o: CNPJ Emitente do CT-e Complementar difere do CNPJ do CT complementado';
      270 : Result := 'Rejei��o: C�digo Munic�pio do Fato Gerador: d�gito inv�lido';
      271 : Result := 'Rejei��o: C�digo Munic�pio do Fato Gerador: difere da UF do emitente';
      272 : Result := 'Rejei��o: C�digo Munic�pio do Emitente: d�gito inv�lido';
      273 : Result := 'Rejei��o: C�digo Munic�pio do Emitente: difere da UF do emitente';
      274 : Result := 'Rejei��o: C�digo Munic�pio do Destinat�rio: d�gito inv�lido';
      275 : Result := 'Rejei��o: C�digo Munic�pio do Destinat�rio: difere da UF do Destinat�rio';
      276 : Result := 'Rejei��o: C�digo Munic�pio do Local de Retirada: d�gito inv�lido';
      277 : Result := 'Rejei��o: C�digo Munic�pio do Local de Retirada: difere da UF do Local de Retirada';
      278 : Result := 'Rejei��o: C�digo Munic�pio do Local de Entrega: d�gito inv�lido';
      279 : Result := 'Rejei��o: C�digo Munic�pio do Local de Entrega: difere da UF do Local de Entrega';
      280 : Result := 'Rejei��o: Certificado Transmissor inv�lido';
      281 : Result := 'Rejei��o: Certificado Transmissor Data Validade';
      282 : Result := 'Rejei��o: Certificado Transmissor sem CNPJ';
      283 : Result := 'Rejei��o: Certificado Transmissor - erro Cadeia de Certifica��o';
      284 : Result := 'Rejei��o: Certificado Transmissor revogado';
      285 : Result := 'Rejei��o: Certificado Transmissor difere ICP-Brasil';
      286 : Result := 'Rejei��o: Certificado Transmissor erro no acesso a LCR';
      289 : Result := 'Rejei��o: C�digo da UF informada diverge da UF solicitada';
      290 : Result := 'Rejei��o: Certificado Assinatura inv�lido';
      291 : Result := 'Rejei��o: Certificado Assinatura Data Validade';
      292 : Result := 'Rejei��o: Certificado Assinatura sem CNPJ';
      293 : Result := 'Rejei��o: Certificado Assinatura - erro Cadeia de Certifica��o';
      294 : Result := 'Rejei��o: Certificado Assinatura revogado';
      295 : Result := 'Rejei��o: Certificado Assinatura difere ICP-Brasil';
      296 : Result := 'Rejei��o: Certificado Assinatura erro no acesso a LCR';
      297 : Result := 'Rejei��o: Assinatura difere do calculado';
      298 : Result := 'Rejei��o: Assinatura difere do padr�o do Projeto';
      299 : Result := 'Rejei��o: XML da �rea de cabe�alho com codifica��o diferente de UTF-8';
      401 : Result := 'Rejei��o: CPF do remetente inv�lido';
      402 : Result := 'Rejei��o: XML da �rea de dados com codifica��o diferente de UTF-8';
      404 : Result := 'Rejei��o: Uso de prefixo de namespace n�o permitido';
      405 : Result := 'Rejei��o: C�digo do pa�s do emitente: d�gito inv�lido';
      406 : Result := 'Rejei��o: C�digo do pa�s do destinat�rio: d�gito inv�lido';
      407 : Result := 'Rejei��o: O CPF s� pode ser informado no campo emitente para o CT-e avulso';
      408 : Result := 'Rejei��o: Lote com CT-e de diferentes UF';
      409 : Result := 'Rejei��o: Campo cUF inexistente no elemento cteCabecMsg do SOAP Header';
      410 : Result := 'Rejei��o: UF informada no campo cUF n�o � atendida pelo WebService';
      411 : Result := 'Rejei��o: Campo versaoDados inexistente no elemento cteCabecMsg do SOAP Header';
      413 : Result := 'Rejei��o: C�digo de Munic�pio de t�rmino da presta��o: d�gito inv�lido';
      414 : Result := 'Rejei��o: C�digo de Munic�pio diverge da UF de t�rmino da presta��o';
      415 : Result := 'Rejei��o: CNPJ do remetente inv�lido';
      416 : Result := 'Rejei��o: CPF do remetente inv�lido';
      417 : Result := 'Rejei��o: C�digo de Munic�pio de localiza��o remetente: d�gito inv�lido';
      418 : Result := 'Rejei��o: C�digo de Munic�pio diverge da UF de localiza��o remetente';
      419 : Result := 'Rejei��o: IE do remetente inv�lida';
      420 : Result := 'Rejei��o: CNPJ remetente n�o cadastrado';
      421 : Result := 'Rejei��o: IE do remetente n�o cadastrada';
      422 : Result := 'Rejei��o: IE do remetente n�o vinculada ao CNPJ';
      423 : Result := 'Rejei��o: C�digo de Munic�pio de localiza��o destinat�rio: d�gito inv�lido';
      424 : Result := 'Rejei��o: C�digo de Munic�pio diverge da UF de localiza��o destinat�rio';
      425 : Result := 'Rejei��o: CNPJ destinat�rio n�o cadastrado';
      426 : Result := 'Rejei��o: IE do destinat�rio n�o cadastrada';
      427 : Result := 'Rejei��o: IE do destinat�rio n�o vinculada ao CNPJ';
      428 : Result := 'Rejei��o: CNPJ do expedidor inv�lido';
      429 : Result := 'Rejei��o: CPF do expedidor inv�lido';
      430 : Result := 'Rejei��o: C�digo de Munic�pio de localiza��o expedidor: d�gito inv�lido';
      431 : Result := 'Rejei��o: C�digo de Munic�pio diverge da UF de localiza��o expedidor';
      432 : Result := 'Rejei��o: IE do expedidor inv�lida';
      433 : Result := 'Rejei��o: CNPJ expedidor n�o cadastrado';
      434 : Result := 'Rejei��o: IE do expedidor n�o cadastrada';
      435 : Result := 'Rejei��o: IE do expedidor n�o vinculada ao CNPJ';
      436 : Result := 'Rejei��o: CNPJ do recebedor inv�lido';
      437 : Result := 'Rejei��o: CPF do recebedor inv�lido';
      438 : Result := 'Rejei��o: C�digo de Munic�pio de localiza��o do recebedor: d�gito inv�lido';
      439 : Result := 'Rejei��o: C�digo de Munic�pio diverge da UF de localiza��o recebedor';
      440 : Result := 'Rejei��o: IE do recebedor inv�lida';
      441 : Result := 'Rejei��o: CNPJ recebedor n�o cadastrado';
      442 : Result := 'Rejei��o: IE do recebedor n�o cadastrada';
      443 : Result := 'Rejei��o: IE do recebedor n�o vinculada ao CNPJ';
      444 : Result := 'Rejei��o: CNPJ do tomador inv�lido';
      445 : Result := 'Rejei��o: CPF do tomador inv�lido';
      446 : Result := 'Rejei��o: C�digo de Munic�pio de localiza��o tomador: d�gito inv�lido';
      447 : Result := 'Rejei��o: C�digo de Munic�pio diverge da UF de localiza��o tomador';
      448 : Result := 'Rejei��o: IE do tomador inv�lida';
      449 : Result := 'Rejei��o: CNPJ tomador n�o cadastrado';
      455 : Result := 'Rejei��o: C�digo de Munic�pio de in�cio da presta��o: d�gito inv�lido';
      456 : Result := 'Rejei��o: C�digo de Munic�pio diverge da UF de in�cio da presta��o';
      457 : Result := 'Rejei��o: O lote cont�m CT-e de mais de um estabelecimento emissor';
      458 : Result := 'Rejei��o: Grupo de CT-e normal n�o informado para CT-e normal';
      459 : Result := 'Rejei��o: Grupo de CT-e complementar n�o informado para CT-e complementar';
      460 : Result := 'Rejei��o: N�o informado os dados do remetente indicado como tomador do servi�o';
      461 : Result := 'Rejei��o: N�o informado os dados do expedidor indicado como tomador do servi�o';
      462 : Result := 'Rejei��o: N�o informado os dados do recebedor indicado como tomador do servi�o';
      463 : Result := 'Rejei��o: N�o informado os dados do destinat�rio indicado como tomador do servi�o';
      469 : Result := 'Rejei��o: Remetente deve ser informado para tipo de servi�o diferente de redespacho intermedi�rio ou Servi�o vinculado a multimodal';
      470 : Result := 'Rejei��o: Destinat�rio deve ser informado para tipo de servi�o diferente de redespacho intermedi�rio ou servi�o vinculado a multimodal';
      471 : Result := 'Rejei��o: Ano de inutiliza��o n�o pode ser superior ao Ano atual';
      472 : Result := 'Rejei��o: Ano de inutiliza��o n�o pode ser inferior a 2008';
      473 : Result := 'Rejei��o: Tipo Autorizador do Recibo diverge do �rg�o Autorizador';
      474 : Result := 'Rejei��o: Expedidor deve ser informado para tipo de servi�o de redespacho intermedi�rio e servi�o vinculado a multimodal';
      475 : Result := 'Rejei��o: Recebedor deve ser informado para tipo de servi�o de redespacho intermedi�rio e servi�o vinculado a multimodal';
      489 : Result := 'Rejei��o: IE do tomador n�o cadastrada';
      490 : Result := 'Rejei��o: IE do tomador n�o vinculada ao CNPJ';
      491 : Result := 'Rejei��o: CT-e referenciado � CT-e complementar';
      492 : Result := 'Rejei��o: C�digo de Munic�pio de envio: d�gito inv�lido';
      493 : Result := 'Rejei��o: C�digo de Munic�pio diverge da UF de envio';
      494 : Result := 'Rejei��o: Processo de emiss�o informado inv�lido';
      495 : Result := 'Rejei��o: Solicitante n�o autorizado para consulta';
      496 : Result := 'Rejei��o: Grupo CT-e de Anula��o n�o informado para o CT-e de Anula��o';
      497 : Result := 'Rejei��o: CT-e objeto da anula��o inexistente';
      498 : Result := 'Rejei��o: CT-e objeto da anula��o deve estar com a situa��o autorizada (n�o pode estar cancelado ou denegado)';
      499 : Result := 'Rejei��o: CT-e de anula��o deve ter tipo de emiss�o = normal';
      500 : Result := 'Rejei��o: CT-e objeto da anula��o deve ter Tipo = 0 (normal) ou 3 (Substitui��o)';
      501 : Result := 'Rejei��o: Data de emiss�o do CT-e de Anula��o deve ocorrer em at� 60 dias';
      502 : Result := 'Rejei��o: CT-e de anula��o deve ter o valor do ICMS e de presta��o iguais ao CT-e original';
      503 : Result := 'Rejei��o: CT-e substituto deve ter tipo de emiss�o = normal';
      505 : Result := 'Rejei��o: Grupo CT-e de Substitui��o n�o informado para o CT-e de Substitui��o';
      510 : Result := 'Rejei��o: CNPJ do emitente do CT-e substituto deve ser igual ao informado no CT-e substitu�do';
      511 : Result := 'Rejei��o: CNPJ/CPF do remetente do CT-e substituto deve ser igual ao informado no CT-e substitu�do';
      512 : Result := 'Rejei��o: CNPJ/CPF do destinat�rio do CT-e substituto deve ser igual ao informado no CT-e substitu�do';
      513 : Result := 'Rejeicao: UF nao atendida pela SVC-[SP/RS]';
      514 : Result := 'Rejei��o: vers�o da mensagem n�o suportada na SVC';
      515 : Result := 'Rejei��o: O tpEmis informado s� � v�lido na conting�ncia SVC';
      516 : Result := 'Rejei��o: O tpEmis informado � incompat�vel com SVC-[SP/RS]';
      517 : Result := 'Rejei��o: CT-e informado em SVC deve ser Normal';
      518 : Result := 'Rejei��o: Servi�o indispon�vel na SVC';
      519 : Result := 'Rejei��o: CFOP inv�lido para opera��o';
      520 : Result := 'Rejei��o: CT-e n�o pode receber mais do que 10 CT-e Complementares';
      521 : Result := 'Rejei��o: Os documentos de transporte anterior devem ser informados para os tipos de servi�o Subcontrata��o, Redespacho e Redespacho Intermedi�rio';
      522 : Result := 'Rejei��o: Nro Item Alterado inv�lido. Preencher com valor num�rico (01 � 99)';
      539 : Result := 'Rejeicao: Duplicidade de CT-e, com diferen�a na Chave de Acesso [chCTe: 99999999999999999999999999999999999999999999][nRec:999999999999999]';
      540 : Result := 'Rejei��o: Grupo de documentos informado inv�lido para remetente que emite NF-e';
      550 : Result := 'Rejei��o: O CNPJ/CPF do expedidor do CT-e substituto deve ser igual ao informado no CT-e substitu�do';
      551 : Result := 'Rejei��o: O CNPJ/CPF do recebedor do CT-e substituto deve ser igual ao informado no CT-e substitu�do';
      552 : Result := 'Rejei��o: O CNPJ/CPF do tomador do CT-e substituto deve ser igual ao informado no CT-e substitu�do';
      553 : Result := 'Rejei��o: A IE do emitente do CT-e substituto deve ser igual ao informado no CT-e substitu�do';
      554 : Result := 'Rejei��o: A IE do remetente do CT-e substituto deve ser igual ao informado no CT-e substitu�do';
      555 : Result := 'Rejei��o: A IE do destinat�rio do CT-e substituto deve ser igual ao informado no CT-e substitu�do';
      556 : Result := 'Rejei��o: A IE do expedidor do CT-e substituto deve ser igual ao informado no CT-e substitu�do';
      557 : Result := 'Rejei��o: A IE do recebedor do CT-e substituto deve ser igual ao informado no CT-e substitu�do';
      558 : Result := 'Rejei��o: A IE do tomador do CT-e substituto deve ser igual ao informado no CT-e substitu�do';
      559 : Result := 'Rejei��o: A UF de in�cio da presta��o deve ser igual ao informado no CT-e substitu�do';
      560 : Result := 'Rejei��o: A UF de fim da presta��o deve ser igual ao informado no CT-e substitu�do';
      561 : Result := 'Rejei��o: O valor da presta��o do servi�o deve ser menor ou igual ao informado no CT-e substitu�do';
      562 : Result := 'Rejei��o: O valor do ICMS do CT-e substituto deve ser menor ou igual ao informado no CT-e substitu�do';
      563 : Result := 'Rejei��o: A substitui��o de um CT-e deve ocorrer no prazo m�ximo de 90 dias contados da data de emiss�o do CT-e objeto de Substitui��o';
      564 : Result := 'Rejei��o: O CT-e de anula��o n�o pode ser cancelado';
      565 : Result := 'Rejei��o: O CT-e s� pode ser anulado pelo emitente';
      566 : Result := 'Rejei��o: CT-e objeto da anula��o n�o pode ter sido anulado anteriormente';
      567 : Result := 'Rejei��o: CT-e objeto da anula��o n�o pode ter sido substitu�do anteriormente';
      568 : Result := 'Rejei��o: CT-e a ser substitu�do inexistente';
      569 : Result := 'Rejei��o: CT-e a ser substitu�do deve estar com a situa��o autorizada (n�o pode estar cancelado ou denegado)';
      570 : Result := 'Rejei��o: CT-e a ser substitu�do n�o pode ter sido substitu�do anteriormente';
      571 : Result := 'Rejei��o: CT-e a ser substitu�do deve ter Tipo = 0 (normal) ou 3 (Substitui��o)';
      572 : Result := 'Rejei��o: CT-e de anula��o informado no grupo �Tomador n�o � contribuinte do ICMS� inexistente';
      573 : Result := 'Rejei��o: CT-e de anula��o informado no grupo �Tomador n�o � contribuinte do ICMS� deve ter Tipo=2(Anula��o)';
      574 : Result := 'Rejei��o: Vedado o cancelamento de CT-e do tipo substituto (tipo=3)';
      575 : Result := 'Rejei��o: Vedado o cancelamento se possuir CT-e de Anula��o associado';
      576 : Result := 'Rejei��o: Vedado o cancelamento se possuir CT-e de Substitui��o associado';
      577 : Result := 'Rejei��o: CT-e a ser substitu�do n�o pode ter sido anulado anteriormente';
      578 : Result := 'Rejei��o: Chave de acesso do CT-e anulado deve ser igual ao substitu�do';
      579 : Result := 'Rejei��o: Vers�o informada para o modal n�o suportada';
      580 : Result := 'Rejei��o: Falha no Schema XML espec�fico para o modal';
      581 : Result := 'Rejei��o: Campo Valor da Carga deve ser informado para o modal';
      582 : Result := 'Rejei��o: Grupo Tr�fego M�tuo deve ser informado';
      583 : Result := 'Rejei��o: Ferrovia emitente deve ser a de origem quando respFat=1';
      584 : Result := 'Rejei��o: Referenciar o CT-e que foi emitido pela ferrovia de origem';
      585 : Result := 'Rejei��o: IE Emitente n�o autorizada a emitir CT-e para o modal informado';
      586 : Result := 'Rejei��o: Data e Justificativa de entrada em conting�ncia n�o devem ser informadas para tipo de emiss�o normal.';
      587 : Result := 'Rejei��o: Data e Justificativa de entrada em conting�ncia devem ser informadas';
      588 : Result := 'Rejei��o: Data de entrada em conting�ncia posterior a data de emiss�o.';
      589 : Result := 'Rejei��o: O lote cont�m CT-e de mais de um modal';
      590 : Result := 'Rejei��o: O lote cont�m CT-e de mais de uma vers�o de modal';
      591 : Result := 'Rejei��o: D�gito Verificador inv�lido na Chave de acesso de NF-e transportada';
      592 : Result := 'Rejei��o: Chave de acesso inv�lida (Ano < 2009 ou Ano maior que Ano corrente)';
      593 : Result := 'Rejei��o: Chave de acesso inv�lida (M�s = 0 ou M�s > 12)';
      594 : Result := 'Rejei��o: Chave de acesso inv�lida (CNPJ zerado ou digito inv�lido)';
      595 : Result := 'Rejei��o: Chave de acesso inv�lida (modelo diferente de 57)';
      596 : Result := 'Rejei��o: Chave de acesso inv�lida (numero CT = 0)';
      598 : Result := 'Rejeicao: Usar somente o namespace padrao do CT-e';
      599 : Result := 'Rejeicao: Nao eh permitida a presenca de caracteres de edicao no inicio/fim da mensagem ou entre as tags da mensagem';
      600 : Result := 'Rejeicao: Chave de Acesso difere da existente em BD';
      601 : Result := 'Rejei��o: Quantidade de documentos informados no remetente excede limite de 2000';
      602 : Result := 'Rejei��o: Chave de acesso de NF-e inv�lida (Ano < 2005 ou Ano maior que Ano corrente)';
      603 : Result := 'Rejei��o: Chave de acesso de NF-e inv�lida (M�s = 0 ou M�s > 12)';
      604 : Result := 'Rejei��o: Chave de acesso de NF-e inv�lida (CNPJ zerado ou digito inv�lido)';
      605 : Result := 'Rejei��o: Chave de acesso de NF-e inv�lida (modelo diferente de 55)';
      606 : Result := 'Rejei��o: Chave de acesso de NF-e inv�lida (numero NF = 0)';
      627 : Result := 'Rejei��o: CNPJ do autor do evento inv�lido';
      628 : Result := 'Rejei��o: Erro Atributo ID do evento n�o corresponde a concatena��o dos campos (�ID� + tpEvento + chCTe + nSeqEvento)';
      629 : Result := 'Rejei��o: O tpEvento informado inv�lido';
      630 : Result := 'Rejei��o: Falha no Schema XML espec�fico para o evento';
      631 : Result := 'Rejei��o: Duplicidade de evento';
      632 : Result := 'Rejei��o: O autor do evento diverge do emissor do CT-e';
      633 : Result := 'Rejei��o: O autor do evento n�o � um �rg�o autorizado a gerar o evento';
      634 : Result := 'Rejei��o: A data do evento n�o pode ser menor que a data de emiss�o do CT-e';
      635 : Result := 'Rejei��o: A data do evento n�o pode ser maior que a data do processamento';
      636 : Result := 'Rejei��o: O numero sequencial do evento � maior que o permitido';
      637 : Result := 'Rejei��o: A data do evento n�o pode ser menor que a data de autoriza��o do CT-e';
      638 : Result := 'Rejei��o: J� existe CT-e autorizado com esta numera��o';
      639 : Result := 'Rejei��o: Existe EPEC emitido h� mais de 7 dias (168h) sem a emiss�o do CT-e no ambiente normal de autoriza��o';
      640 : Result := 'Rejei��o: Tipo de emiss�o do CT-e difere de EPEC com EPEC autorizado na SVC-XX para este documento.';
      641 : Result := 'Rejei��o: O evento pr�vio deste CT-e n�o foi autorizado na SVC ou ainda n�o foi sincronizado. [OBS: Em caso de atraso na sincroniza��o, favor aguardar alguns instantes para nova tentativa de transmiss�o]';
      642 : Result := 'Rejei��o: Os valores de ICMS, Presta��o e Total da Carga do CT-e devem ser iguais aos informados no EPEC.';
      643 : Result := 'Rejei��o: As informa��es do tomador de servi�o do CT-e devem ser iguais as informadas no EPEC';
      644 : Result := 'Rejei��o: A informa��o do modal do CT-e deve ser igual a informada no EPEC';
      645 : Result := 'Rejei��o: A UF de inicio e fim de presta��o do CT-e devem ser iguais as informadas no EPEC.';
      646 : Result := 'Rejei��o: CT-e emitido em ambiente de homologa��o com Raz�o Social do remetente diferente de CT-E EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';
      647 : Result := 'Rejei��o: CT-e emitido em ambiente de homologa��o com Raz�o Social do expedidor diferente de CT-E EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';
      648 : Result := 'Rejei��o: CT-e emitido em ambiente de homologa��o com Raz�o Social do recebedor diferente de CT-E EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';
      649 : Result := 'Rejei��o: CT-e emitido em ambiente de homologa��o com Raz�o Social do destinat�rio diferente de CT-E EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';
      650 : Result := 'Rejei��o: Valor total do servi�o superior ao limite permitido (R$ 9.999.999,99)';
      651 : Result := 'Rejei��o: Referenciar o CT-e Multimodal que foi emitido pelo OTM';
      652 : Result := 'Rejei��o: NF-e n�o pode estar cancelada ou denegada';
      653 : Result := 'Rejei��o: Tipo de evento n�o � permitido em ambiente de autoriza��o Normal';
      654 : Result := 'Rejei��o: Tipo de evento n�o � permitido em ambiente de autoriza��o SVC';
      655 : Result := 'Rejei��o: CT-e complementado deve estar com a situa��o autorizada (n�o pode estar cancelado ou denegado)';
      656 : Result := 'Rejei��o: CT-e complementado n�o pode ter sido anulado';
      657 : Result := 'Rejei��o: CT-e complementado n�o pode ter sido substitu�do';
      658 : Result := 'Rejei��o: CT-e objeto da anula��o n�o pode ter sido complementado';
      659 : Result := 'Rejei��o: CT-e substitu�do n�o pode ter sido complementado';
      660 : Result := 'Rejei��o: Vedado o cancelamento se possuir CT-e Complementar associado';
      661 : Result := 'Rejei��o: NF-e inexistente na base de dados da SEFAZ';
      662 : Result := 'Rejei��o: NF-e com diferen�a de Chave de Acesso';
      663 : Result := 'Rejei��o: CT-e autorizado h� mais de 30 dias';
      664 : Result := 'Rejei��o: Evento n�o permitido para CT-e Substituido/Anulado';
      665 : Result := 'Rejei��o: As informa��es do seguro da carga devem ser preenchidas para o modal rodovi�rio';
      666 : Result := 'Rejei��o: O respons�vel pelo seguro da carga indicado n�o foi relacionado no CT-e';
      667 : Result := 'Rejei��o: CNPJ do Tomador deve ser igual ao CNPJ do Emitente do CT-e Multimodal';
      668 : Result := 'Rejei��o: CPF do funcion�rio do registro de passagem inv�lido';
      669 : Result := 'Rejei��o: Segundo c�digo de barras deve ser informado para CT-e emitido em conting�ncia FS-DA';
      670 : Result := 'Rejei��o: S�rie utilizada n�o permitida no webservice';
      671 : Result := 'Rejei��o: CT-e referenciado no CT-e Complementar com diferen�a de Chave de Acesso [chCTe: 99999999999999999999999999999999999999999999][nRec:999999999999999].';
      672 : Result := 'Rejei��o: CT-e de Anula��o com diferen�a de Chave de Acesso [chCTe: 99999999999999999999999999999999999999999999][nRec:999999999999999].';
      673 : Result := 'Rejei��o: CT-e Substitu�do com diferen�a de Chave de Acesso [chCTe: 99999999999999999999999999999999999999999999][nRec]';
      674 : Result := 'Rejei��o: CT-e Objeto de Anula��o com diferen�a de Chave de Acesso [chCTe: 99999999999999999999999999999999999999999999][nRec]';
      675 : Result := 'Rejei��o: Valor do imposto n�o corresponde a base de calculo X aliquota';
      676 : Result := 'Rejei��o: CFOP informado inv�lido';
      677 : Result := 'Rejei��o: �rg�o de recep��o do evento inv�lido 678 Rejei��o: Consumo Indevido [Descri��o: XXXXXXXXXXXXXXXXXXXXXXXXXXXX]';
      679 : Result := 'Rejei��o: O modal do CT-e deve ser Multimodal para Evento Registros do Multimodal';
      680 : Result := 'Rejei��o: Tipo de Emiss�o diferente de EPEC';
      681 : Result := 'Rejei��o: Informa��o n�o pode ser alterada por carta de corre��o';
      682 : Result := 'Rejei��o: J� existe pedido de inutiliza��o com a mesma faixa de inutiliza��o';
      683 : Result := 'Rejei��o: Chave de acesso de MDF-e inv�lida (Ano < 2012 ou Ano maior que Ano corrente)';
      684 : Result := 'Rejei��o: Chave de acesso de MDF-e inv�lida (M�s = 0 ou M�s > 12)';
      685 : Result := 'Rejei��o: Chave de acesso de MDF-e inv�lida (CNPJ zerado ou digito inv�lido)';
      686 : Result := 'Rejei��o: Chave de acesso de MDF-e inv�lida (modelo diferente de 58)';
      687 : Result := 'Rejei��o: Chave de acesso de MDF-e inv�lida (numero MDF = 0)';
      688 : Result := 'Rejei��o: Grupo de informa��es do veiculo deve ser informado para Carga Lota��o';
      689 : Result := 'Rejei��o: CT-e de anula��o n�o � permitido para CT-e cujo tomador � Contribuinte de ICMS.';
      690 : Result := 'Rejei��o: CT-e Multimodal referenciado inexistente na base de dados da SEFAZ';
      691 : Result := 'Rejei��o: CT-e Multimodal referenciado existe com diferen�a de chave de acesso';
      692 : Result := 'Rejei��o: CT-e Multimodal referenciado n�o pode estar cancelado ou denegado';
      693 : Result := 'Rejei��o: Grupo Documentos Transportados deve ser informado para tipo de servi�o diferente de redespacho intermedi�rio e servi�o vinculado a multimodal';
      694 : Result := 'Rejei��o: Grupo Documentos Transportados n�o pode ser informado para tipo de servi�o redespacho intermedi�rio e servi�o vinculado a multimodal';
      695 : Result := 'Rejei��o: CT-e com emiss�o anterior ao evento pr�vio (EPEC)';
      696 : Result := 'Rejei��o:Existe EPEC aguardando CT-e nessa faixa de numera��o';
      697 : Result := 'Rejei��o: Data de emiss�o do CT-e deve ser igual a data de autoriza��o da EPEC';
      698 : Result := 'Rejei��o: Evento Pr�vio autorizado h� mais de 7 dias (168 horas)';
      699 : Result := 'Rejei��o: CNPJ autorizado para download inv�lido';
      700 : Result := 'Rejei��o: CPF autorizado para download inv�lido';
      998 : Result := 'Rejei��o: CT-e Multimodal e Servi�o Vinculado a Multimodal n�o est�o liberados no ambiente de produ��o. *** Regra provis�ria';
      999 : Result := 'Rejei��o: Erro n�o catalogado (informar a mensagem de erro capturado no tratamento da exce��o)';
      //C�DIGO MOTIVOS DE DENEGA��O DE USO';
      301 : Result := 'Uso Denegado : Irregularidade fiscal do emitente';
      //
      else Result := 'Status n�o definido! Informe a DERMATEK';
    end;
  end;
end;

function TUnCTe_PF.TipoXML(NoStandAlone: Boolean): String;
begin
  Result :=
    '<?xml version="' + sXML_Version + '" encoding="' + sXML_Encoding + '"';
  if NoStandAlone then Result := Result + ' standalone="no"';
  Result := Result +  '?>';
end;

function TUnCTe_PF.VDom(const Grupo, Codigo: Integer; const Campo, Valor,
  Dominio: String; var Msg: String): Boolean;
const
  Dominios: array [0..36] of String = ('CFOP', 'D1', 'D2', 'D3', 'D4', 'D5',
  'D6', 'D7', 'D8', 'D9', 'D10', 'D11', 'D12', 'D13', 'D14', 'D15', 'D16',
  'D17', 'D18', 'D19', 'D20', 'D21', 'D22', 'D23', 'D24', 'D25', 'D26', 'D27',
  'D28', 'D29', 'D30', 'D31', 'D32', 'D33', 'D34', 'D35', 'D36');
  CFOP: array [0..035] of String = (
  '1.206', '2.206', '3.206', '5.206', '5.351', '5.352', '5.353', '5.354',
  '5.355', '5.356', '5.357', '5.359', '5.360', '5.601', '5.602', '5.603',
  '5.605', '5.606', '5.932', '5.949', '6.206', '6.351', '6.352', '6.353',
  '6.354', '6.355', '6.356', '6.357', '6.359', '6.360', '6.603', '6.932',
  '6.949', '7.206', '7.358', '7.949');
  D1: array [0..001] of String = ('1', '2');
  D2: array [0..026] of String = ('11', '12', '13', '14', '15', '16', '17', '21', '22', '23', '24', '25', '26', '27', '28', '29', '31', '32', '33', '35', '41', '42', '43', '50', '51', '52', '53');
  D3: array [0..027] of String = ('11', '12', '13', '14', '15', '16', '17', '21', '22', '23', '24', '25', '26', '27', '28', '29', '31', '32', '33', '35', '41', '42', '43', '50', '51', '52', '53', '90');
  D4: array [0..000] of String = ('57');
  D5: array [0..001] of String = ('01', '04');
  D6: array [0..006] of String = ('1', '2', '3', '4', '5', '6', '7');
  D7: array [0..003] of String = ('1', '2', '3', '4');
  D8: array [0..027] of String = ('AC', 'AL', 'AM', 'AP', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MG', 'MS', 'MT', 'PA', 'PB', 'PE', 'PI', 'PR', 'RJ', 'RN', 'RO', 'RR', 'RS', 'SC', 'SE', 'SP', 'TO', 'EX');
  D9: array [0..026] of String = ('AC', 'AL', 'AM', 'AP', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MG', 'MS', 'MT', 'PA', 'PB', 'PE', 'PI', 'PR', 'RJ', 'RN', 'RO', 'RR', 'RS', 'SC', 'SE', 'SP', 'TO');
  D10: array [0..002] of String = ('0', '1', '2');
  D11: array [0..004] of String = ('1', '4', '5', '7', '8');
  D12: array [0..004] of String = ('0', '1', '2', '3', '4');
  D13: array [0..001] of String = ('0', '1');
  D14: array [0..003] of String = ('0', '1', '2', '3');
  D15: array [0..000] of String = ('4');
  D16: array [0..000] of String = ('0');
  D17: array [0..002] of String = ('1', '2', '3');
  D18: array [0..005] of String = ('00', '01', '02', '03', '04', '05');
  D19: array [0..002] of String = ('00', '10', '99');
  D20: array [0..005] of String = ('0', '1', '2', '3', '4', '5');
  D21: array [0..000] of String = ('00');
  D22: array [0..000] of String = ('20');
  D23: array [0..002] of String = ('40', '41', '51');
  D24: array [0..000] of String = ('60');
  D25: array [0..000] of String = ('90');
  D26: array [0..000] of String = ('1');
  D27: array [0..192] of String = (' 101', '102', '103', '104', '105', '106', '107', '108', '201', '302', '303', '304',
  '305', '401', '402', '403', '404', '405', '406', '407', '408', '409', '410', '411', '412', '413', '414', '415', '416',
  '417', '418', '419', '420', '421', '422', '423', '501', '502', '503', '504', '505', '506', '507', '508', '509',
  '601', '602', '603', '604', '605', '701', '702', '703', '704', '705', '706', '707', '708', '709', '710', '711',
  '712', '713', '716', '717', '718', '719', '720', '721', '722', '801', '802', '901', '902', '903', '1001', '1002',
  '1003', '1004', '1005', '1006', '1007', '1008', '1009', '1010', '1101', '1102', '1103', '1104', '1201', '1202',
  '1203', '1204', '1205', '1206', '1207', '1208', '1209', '1210', '1211', '1212', '1213', '1214', '1215', '1216', '1217',
  '1302', '1303', '1304', '1305', '1401', '1402', '1403', '1404', '1405', '1406', '1407', '1408', '1409', '1410', '1411',
  '1412', '1413', '1501', '1502', '1503', '1504', '1505', '1506', '1507', '1508', '1509', '1510', '1511', '1512', '1513',
  '1514', '1515', '1516', '1517', '1518', '1601', '1701', '1702', '1703', '1704', '1705', '1706', '1708', '1709', '1710',
  '1711', '1712', '1713', '1714', '1715', '1716', '1717', '1718', '1719', '1720', '1721', '1722', '1723', '1724', '1801',
  '1901', '2001', '2002', '2003', '2101', '2201', '2301', '2401', '2501', '2502', '2503', '2504', '2601', '2701', '2801',
  '2901', '3001', '3101', '3201', '3301', '3401', '3501', '3601', '3701', '3801', '3901', '4001');
  D28: array [0..013] of String = ('00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '99');
  D29: array [0..028] of String = ('01', '1B', '02', '2D', '2E', '04', '06', '07', '08', '8B', '09', '10', '11', '13', '14', '15', '16', '17', '18', '20', '21', '22', '23', '24', '25', '26', '27', '28', '55');
  D30: array [0..005] of String = ('01', '02', '03', '04', '05', '06');
  D31: array [0..001] of String = ('P', 'T');
  D32: array [0..006] of String = ('00', '01', '02', '03', '04', '05', '06');
  D33: array [0..009] of String = ('1', '2', '3', '4', '5', '6', '7', '8', '9', '99');
  D34: array [0..003] of String = ('N', 'S', 'L', 'O');
  D35: array [0..003] of String = ('1', '5', '7', '8');
  D36: array [0..004] of String = ('01', '02', '03', '04', '05');
var
  Lista: Integer;
  Letra, Nums: String;
begin
  //Msg := '';
  if Trim(Dominio) = '' then
  begin
    Result := True;
    Exit;
  end else
    Result := False;
  //if AnsiMatchStr(Valor, D1) then
  Lista := AnsiIndexStr(Dominio, Dominios);
  if Lista > -1 then
  begin
(*
  Letra := Dominio[1];
  Nums  := Copy(Trim(Dominio), 2);
  if (Letra = 'D') and (Geral.SoNumero_TT(Nums) = Nums) and (Nums <> '') then
  begin
    Lista := Geral.IMV(Nums);
*)
    case Lista of
       0: Result := AnsiMatchStr(Valor, CFOP);
       1: Result := AnsiMatchStr(Valor, D1);
       2: Result := AnsiMatchStr(Valor, D2);
       3: Result := AnsiMatchStr(Valor, D3);
       4: Result := AnsiMatchStr(Valor, D4);
       5: Result := AnsiMatchStr(Valor, D5);
       6: Result := AnsiMatchStr(Valor, D6);
       7: Result := AnsiMatchStr(Valor, D7);
       8: Result := AnsiMatchStr(Valor, D8);
       9: Result := AnsiMatchStr(Valor, D9);
      10: Result := AnsiMatchStr(Valor, D10);
      11: Result := AnsiMatchStr(Valor, D11);
      12: Result := AnsiMatchStr(Valor, D12);
      13: Result := AnsiMatchStr(Valor, D13);
      14: Result := AnsiMatchStr(Valor, D14);
      15: Result := AnsiMatchStr(Valor, D15);
      16: Result := AnsiMatchStr(Valor, D16);
      17: Result := AnsiMatchStr(Valor, D17);
      18: Result := AnsiMatchStr(Valor, D18);
      19: Result := AnsiMatchStr(Valor, D19);
      20: Result := AnsiMatchStr(Valor, D20);
      21: Result := AnsiMatchStr(Valor, D21);
      22: Result := AnsiMatchStr(Valor, D22);
      23: Result := AnsiMatchStr(Valor, D23);
      24: Result := AnsiMatchStr(Valor, D24);
      25: Result := AnsiMatchStr(Valor, D25);
      26: Result := AnsiMatchStr(Valor, D26);
      27: Result := AnsiMatchStr(Valor, D27);
      28: Result := AnsiMatchStr(Valor, D28);
      29: Result := AnsiMatchStr(Valor, D29);
      30: Result := AnsiMatchStr(Valor, D30);
      31: Result := AnsiMatchStr(Valor, D31);
      32: Result := AnsiMatchStr(Valor, D32);
      33: Result := AnsiMatchStr(Valor, D33);
      34: Result := AnsiMatchStr(Valor, D34);
      35: Result := AnsiMatchStr(Valor, D35);
      36: Result := AnsiMatchStr(Valor, D36);
      //
      else Geral.MB_Aviso('Dominio CT-e "' + Dominio + '" inv�lido! [1]');
    end;
  end else
    Geral.MB_Aviso('Dominio CT-e "' + Dominio + '" inv�lido! [2]');
  if not Result then
    Msg := Msg + 'CT-e item #' + Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo) +
    ' campo "' + Campo + '": ' + 'Valor inv�lido: "' + Valor + '" no Dom�nio: '
    + Dominio + sLineBreak;
  //
end;

function TUnCTe_PF.VersaoCTeEmUso(): Integer;
var
  Empresa: Integer;
begin
  Empresa := Geral.IMV(VAR_LIB_EMPRESAS);
  DModG.ReopenParamsEmp(Empresa);
  Result := Trunc((DmCTe_0000.QropcoesCTeCTeversao.Value *100) + 0.5);
end;

function TUnCTe_PF.VRegExp(const Grupo, Codigo: Integer; const Campo, Valor,
  ExpressaoRegular: String; var Msg: String): Boolean;
const
  RegExps: array [0..64] of string = ('ER0',
  'ER1', 'ER2', 'ER3', 'ER4', 'ER5', 'ER6', 'ER7', 'ER8', 'ER9', 'ER10',
  'ER11', 'ER12', 'ER13', 'ER14', 'ER15', 'ER16', 'ER17', 'ER18', 'ER19',
  'ER20', 'ER21', 'ER22', 'ER23', 'ER24', 'ER25', 'ER26', 'ER27', 'ER28',
  'ER29', 'ER30', 'ER31', 'ER32', 'ER33', 'ER34', 'ER35', 'ER36', 'ER37',
  'ER38', 'ER39', 'ER40', 'ER41', 'ER42', 'ER43', 'ER44', 'ER45', 'ER46',
  'ER47', 'ER48', 'ER49', 'ER50', 'ER51', 'ER52', 'ER53', 'ER54', 'ER55',
  'ER56', 'ER57', 'ER58', 'ER59', 'ER60', 'ER61', 'ER62', 'ER63', 'ER64'
  );
const
  ER0 = '';
  ER1 = '[0-9]{2}';
  ER2 = '[0-9]{7}';
  ER3 = '[0-9]{44}';
  ER4 = '[0-9]{14}';
  ER5 = '[0-9]{6,14}';
  ER6 = '[0-9]{3,14}';
  ER7 = '[0-9]{0}|[0-9]{14}';
  ER8 = '[0-9]{11}';
  ER9 = '[0-9]{3,11}';
  ER10 = '(((20(([02468][048])|([13579][26]))-02-29))|(20[0-9][0-9])-((((0[1-9])|(1[0-2]))-((0[1-9])|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))';
  ER11 = '0|0\.[0-9]{2}|[1-9]{1}[0-9]{0,2}(\.[0-9]{2})?';
  ER12 = '0|0\.[0-9]{3}|[1-9]{1}[0-9]{0,2}(\.[0-9]{3})?';
  ER13 = '0\.[0-9]{1}[1-9]{1}|0\.[1-9]{1}[0-9]{1}|[1-9]{1}[0-9]{0,2}(\.[0-9]{2})?';
  ER14 = '0|0\.[0-9]{3}|[1-9]{1}[0-9]{0,7}(\.[0-9]{3})?';
  ER15 = '0\.[1-9]{1}[0-9]{2}|0\.[0-9]{2}[1-9]{1}|0\.[0-9]{1}[1-9]{1}[0-9]{1}|[1-9]{1}[0-9]{0,7}(\.[0-9]{3})?';
  ER16 = '0|0\.[0-9]{4}|[1-9]{1}[0-9]{0,7}(\.[0-9]{4})?';
  ER17 = '0\.[1-9]{1}[0-9]{3}|0\.[0-9]{3}[1-9]{1}|0\.[0-9]{2}[1-9]{1}[0-9]{1}|0\.[0-9]{1}[1-9]{1}[0-9]{2}|[1-9]{1}[0-9]{0,7}(\.[0-9]{4})?';
  ER18 = '0\.[1-9]{1}[0-9]{5}|0\.[0-9]{1}[1-9]{1}[0-9]{4}|0\.[0-9]{2}[1-9]{1}[0-9]{3}|0\.[0-9]{3}[1-9]{1}[0-9]{2}|0\.[0-9]{4}[1-9]{1}[0-9]{1}|0\.[0-9]{5}[1-9]{1}|[1-9]{1}[0-9]{0,8}(\.[0-9]{6})?';
  ER19 = '0|0\.[0-9]{4}|[1-9]{1}[0-9]{0,10}(\.[0-9]{4})?';
  ER20 = '0\.[1-9]{1}[0-9]{3}|0\.[0-9]{3}[1-9]{1}|0\.[0-9]{2}[1-9]{1}[0-9]{1}|0\.[0-9]{1}[1-9]{1}[0-9]{2}|[1-9]{1}[0-9]{0,10}(\.[0-9]{4})?';
  ER21 = '0|0\.[0-9]{3}|[1-9]{1}[0-9]{0,11}(\.[0-9]{3})?';
  ER22 = '0\.[1-9]{1}[0-9]{2}|0\.[0-9]{2}[1-9]{1}|0\.[0-9]{1}[1-9]{1}[0-9]{1}|[1-9]{1}[0-9]{0,11}(\.[0-9]{3})?';
  ER23 = '0|0\.[0-9]{4}|[1-9]{1}[0-9]{0,11}(\.[0-9]{4})?';
  ER24 = '0\.[1-9]{1}[0-9]{3}|0\.[0-9]{3}[1-9]{1}|0\.[0-9]{2}[1-9]{1}[0-9]{1}|0\.[0-9]{1}[1-9]{1}[0-9]{2}|[1-9]{1}[0-9]{0,11}(\.[0-9]{4})?';
  ER25 = '0|0\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\.[0-9]{2})?';
  ER26 = '0\.[0-9]{1}[1-9]{1}|0\.[1-9]{1}[0-9]{1}|[1-9]{1}[0-9]{0,12}(\.[0-9]{2})?';
  ER27 = '[0-9]{2,14}';
  ER28 = '[0-9]{0,14}|ISENTO';
  ER29 = '[0-9]{1,4}';
  ER30 = '[1-9]{1}[0-9]{0,8}';
  ER31 = '[0-9]{15}';
  ER32 = '0|[1-9]{1}[0-9]{0,2}';
  ER33 = '[0-9]{3}';
  ER34 = '[!-�]{1}[ -�]{0,}[!-�]{1}|[!-�]{1}';
  ER35 = '[0-9]\.[0-9]{6}|[1-8][0-9]\.[0-9]{6}|90\.[0-9]{6}|-[0-9]\.[0-9]{6}|-[1-8][0-9]\.[0-9]{6}|-90\.[0-9]{6}';
  ER36 = '[0-9]\.[0-9]{6}|[1-9][0-9]\.[0-9]{6}|1[0-7][0-9]\.[0-9]{6}|180\.[0-9]{6}|-[0-9]\.[0-9]{6}|-[1-9][0-9]\.[0-9]{6}|-1[0-7][0-9]\.[0-9]{6}|-180\.[0-9]{6}';
  ER37 = '[0-9]{8}';
  ER38 = '(((20(([02468][048])|([13579][26]))-02-29))|(20[0-9][0-9])-((((0[1-9])|(1[0-2]))-((0[1-9])|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))T(20|21|22|23|[0-1]\d):[0-5]\d:[0-5]\d';
  ER39 = '[0-9]{1}';
  ER40 = '[0-9]{8,9}';
  ER41 = '[1-9]{1}[0-9]{1,8}';
  ER42 = '[0-9]{1,20}';
  ER43 = '2\.(0[0-9]|[1-9][0-9])';
  ER44 = '[0-9]{4}|ND';
  ER45 = '[A-Z0-9]+';
  ER46 = '[0-9]{1,6}';
  ER47 = 'CTe[0-9]{44}';
  ER48 = '[123567][0-9]([0-9][1-9]|[1-9][0-9])';
  ER49 = '[^@]+@[^\.]+\..+';
  ER50 = '[0-9]{1,15}';
  ER51 = '(([0-1][0-9])|([2][0-3])):([0-5][0-9]):([0-5][0-9])';
  ER52 = '2\.00';
  ER53 = '[0-9]{8}|ISENTO';
  ER54 = '[A-Z]{3}(([1-9]\d{3})|(0[1-9]\d{2})|(00[1-9]\d)|(000[1-9]))';
  ER55 = '[0-9]{12}';
  ER56 = '[1-9]{1}[0-9]{0,5}';
  ER57 = '0|[1-9]{1}[0-9]{0,5}';
  ER58 = '[0-9]{9}';
  ER59 = 'M / G / E';
  ER60 = '[1-9]{1}[0-9]{0,9}';
  ER61 = '[0-9]{7,12}';
  ER62 = '1\.04';
  ER63 = '[1-9]{1}[0-9]{0,3}|ND';
  ER64 = '[0-9]{7,10}';
var
  Lista: Integer;
  Letra, Nums: String;
  Data: TDateTime;
begin
  try
  if Trim(ExpressaoRegular) = '' then
  begin
    Result := True;
    Exit;
  end else
    Result := False;
  //if AnsiMatchStr(Valor, D1) then
  Lista := AnsiIndexStr(ExpressaoRegular, RegExps);
  if Lista > -1 then
  begin
    case Lista of
      //Bool: ExecRegExpr('\d{3}-(\d{2}-\d{2}|\d{4})', edTextWithPhone.Text);
       0: Result := ExecRegExpr(ER0, Valor);
       1: Result := ExecRegExpr(ER1, Valor);
       2: Result := ExecRegExpr(ER2, Valor);
       3: Result := ExecRegExpr(ER3, Valor);
       4: Result := ExecRegExpr(ER4, Valor);
       5: Result := ExecRegExpr(ER5, Valor);
       6: Result := ExecRegExpr(ER6, Valor);
       7: Result := ExecRegExpr(ER7, Valor);
       8: Result := ExecRegExpr(ER8, Valor);
       9: Result := ExecRegExpr(ER9, Valor);
      //10: Result := ExecRegExpr(ER10, Valor);
      10:
      begin
        Data := Geral.ValidaData_YYYY_MM_DD(Valor);
        Result := Data > 2;
      end;
      11: Result := ExecRegExpr(ER11, Valor);
      12: Result := ExecRegExpr(ER12, Valor);
      13: Result := ExecRegExpr(ER13, Valor);
      14: Result := ExecRegExpr(ER14, Valor);
      15: Result := ExecRegExpr(ER15, Valor);
      16: Result := ExecRegExpr(ER16, Valor);
      17: Result := ExecRegExpr(ER17, Valor);
      18: Result := ExecRegExpr(ER18, Valor);
      19: Result := ExecRegExpr(ER19, Valor);
      20: Result := ExecRegExpr(ER20, Valor);
      21: Result := ExecRegExpr(ER21, Valor);
      22: Result := ExecRegExpr(ER22, Valor);
      23: Result := ExecRegExpr(ER23, Valor);
      24: Result := ExecRegExpr(ER24, Valor);
      25: Result := ExecRegExpr(ER25, Valor);
      26: Result := ExecRegExpr(ER26, Valor);
      27: Result := ExecRegExpr(ER27, Valor);
      28: Result := ExecRegExpr(ER28, Valor);
      29: Result := ExecRegExpr(ER29, Valor);
      30: Result := ExecRegExpr(ER30, Valor);
      31: Result := ExecRegExpr(ER31, Valor);
      32: Result := ExecRegExpr(ER32, Valor);
      33: Result := ExecRegExpr(ER33, Valor);
      34: Result := ExecRegExpr(ER34, Valor);
      35: Result := ExecRegExpr(ER35, Valor);
      36: Result := ExecRegExpr(ER36, Valor);
      37: Result := ExecRegExpr(ER37, Valor);
      38: Result := ExecRegExpr(ER38, Valor);
      39: Result := ExecRegExpr(ER39, Valor);
      40: Result := ExecRegExpr(ER40, Valor);
      41: Result := ExecRegExpr(ER41, Valor);
      42: Result := ExecRegExpr(ER42, Valor);
      43: Result := ExecRegExpr(ER43, Valor);
      44: Result := ExecRegExpr(ER44, Valor);
      45: Result := ExecRegExpr(ER45, Valor);
      46: Result := ExecRegExpr(ER46, Valor);
      47: Result := ExecRegExpr(ER47, Valor);
      48: Result := ExecRegExpr(ER48, Valor);
      49: Result := ExecRegExpr(ER49, Valor);
      50: Result := ExecRegExpr(ER50, Valor);
      51: Result := ExecRegExpr(ER51, Valor);
      52: Result := ExecRegExpr(ER52, Valor);
      53: Result := ExecRegExpr(ER53, Valor);
      54: Result := ExecRegExpr(ER54, Valor);
      55: Result := ExecRegExpr(ER55, Valor);
      56: Result := ExecRegExpr(ER56, Valor);
      57: Result := ExecRegExpr(ER57, Valor);
      58: Result := ExecRegExpr(ER58, Valor);
      59: Result := ExecRegExpr(ER59, Valor);
      60: Result := ExecRegExpr(ER60, Valor);
      61: Result := ExecRegExpr(ER61, Valor);
      62: Result := ExecRegExpr(ER62, Valor);
      63: Result := ExecRegExpr(ER63, Valor);
      64: Result := ExecRegExpr(ER64, Valor);
      (*65: Result := ExecRegExpr(ER65, Valor);
      66: Result := ExecRegExpr(ER66, Valor);
      67: Result := ExecRegExpr(ER67, Valor);
      68: Result := ExecRegExpr(ER68, Valor);
      69: Result := ExecRegExpr(ER69, Valor);*)
      //
      else Geral.MB_Aviso('Express�o regular "' + ExpressaoRegular + '" inv�lida [1]!');
    end;
  end else
    Geral.MB_Aviso('Express�o regular "' + ExpressaoRegular + '" inv�lida [2]!');
  if not Result then
    Msg := Msg + 'CT-e item #' + Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo) +
    ' campo "' + Campo + '": Valor inv�lido: "' + Valor +
    '" na Express�o Regular: ' + ExpressaoRegular + sLineBreak;
  except
    on E: Exception do
    begin
      Geral.MB_Erro(E.Message + sLineBreak +
      'Express�o regular: ' + ExpressaoRegular + sLineBreak +
      'Grupo:' + Geral.FF0(Grupo) + sLineBreak +
      'Codigo:' + Geral.FF0(Codigo) + sLineBreak +
      'Grupo:' + Geral.FF0(Grupo) + sLineBreak +
      'Campo:' + Campo + sLineBreak +
      'Valor:' + Valor + sLineBreak +
      '');
    end else
    begin
      Geral.MB_Erro('Express�o regular: ' + ExpressaoRegular + sLineBreak +
      'Grupo:' + Geral.FF0(Grupo) + sLineBreak +
      'Codigo:' + Geral.FF0(Codigo) + sLineBreak +
      'Grupo:' + Geral.FF0(Grupo) + sLineBreak +
      'Campo:' + Campo + sLineBreak +
      'Valor:' + Valor + sLineBreak +
      '');
      raise;
    end;
  end;
end;

function TUnCTe_PF.XML_DistribuiCartaDeCorrecao(const VersaoCCe, XML_Evento,
  XML_Protocolo: String; var XML_Distribuicao: String): Boolean;
var
  TagRaiz: String;
begin
  TagRaiz := 'procEventoCTe';
  XML_Distribuicao := TipoXML(False) +
    '<' + TagRaiz + ' xmlns="http://www.portalfiscal.inf.br/cte" versao="' +
    VersaoCCe + '">' + XML_Evento + XML_Protocolo + '</' + TagRaiz + '>';
  Result := True;
end;


function TUnCTe_PF.XML_DistribuiCTe(const Id: String; const Status: Integer;
  const XML_CTe, XML_Aut, XML_Can: String; const Protocolar: Boolean;
  var XML_Distribuicao: String; const VersaoCTe: Double;
  const InfoLocal: String): Boolean;
  //
  procedure MostraMensagem(Msg, Id: String; Mostra: Boolean = True);
  var
    Mensagem: String;
  begin
    Mensagem := Msg + ' ' + ID + XXe_PF.ChaveDeAcessoDesmontada(ID, VersaoCTe);
    Geral.MB_Aviso(Mensagem);
  end;
var
  P: Integer;
  CTeXML, ProtXML, TagRaiz: String;
begin
  XML_Distribuicao := '';
  TagRaiz := 'cteProc';
  Result := False;
  if Trim(XML_CTe) = '' then
  begin
    MostraMensagem('CTe sem XML ' + InfoLocal + ':', Id);
    Exit;
  end else begin
    //P := pos('<infCte', XML_CTe);
    //P := pos('<CTe xmlns="http://www.portalfiscal.inf.br/cte"><infCte Id="', XML_CTe);
    P := pos('<infCte versao=', XML_CTe);
    if P = 0 then
    begin
      MostraMensagem('CTe sem XML v�lido ' + InfoLocal + ':', Id);
      Exit;
    end;
    CTeXML := Copy(XML_CTe, P);
    if Protocolar then
    begin
      if XXe_PF.XXeEstahAutorizada(Status) then
      begin
        TagRaiz := 'cteProc';
        if Trim(XML_Aut) = '' then
        begin
          MostraMensagem('CTe autorizada sem XML de autoriza��o ' + InfoLocal + ':', Id);
          Exit;
        end;
        ProtXML := XML_Aut;
      end else
      if XXe_PF.XXeEstahCancelada(Status) then
      begin
        TagRaiz := 'procCancCTe';
        if Trim(XML_Can) = '' then
        begin
          if not DmCTe_0000.ReopenLocEve(Id, CO_COD_evexxe110111Can) then
          begin
            MostraMensagem('CTe cancelada sem XML de cancelamento ' + InfoLocal + ':', Id, False);
            Exit;
          end;
        end else
          ProtXML := XML_Can;
      end else
      begin
        TagRaiz := '??CTe??';
        ProtXML := '';
        MostraMensagem('CTe sem XML de autoriza��o ou cancelamento:', Id);
        //Exit;
      end;
    end else ProtXML := '';
    //
    XML_Distribuicao := TipoXML(False) +
      '<' + TagRaiz + ' xmlns="http://www.portalfiscal.inf.br/cte" versao="' +
      MLAGeral.MudaPontuacao(FormatFloat('0.00', VersaoCTe), '.', '.') + '">' +
      '<CTe xmlns="http://www.portalfiscal.inf.br/cte">' +
      CTeXML + ProtXML + '</' + TagRaiz + '>';
    Result := True;
  end;
end;

function TUnCTe_PF.XML_DistribuiEvento(const Id: String; const Status: Integer;
  const XML_Env, XML_Ret: String; const Protocolar: Boolean;
  var XML_Distribuicao: String; const Versao: Double;
  const InfoLocal: String): Boolean;
  //
  procedure MostraMensagem(Msg, Id: String);
  var
    Mensagem: String;
  begin
    Mensagem := Msg + ' ' + ID + XXe_PF.ChaveDeAcessoDesmontada(ID, 2.00);
    Geral.MB_Aviso(Mensagem);
  end;
var
  TagRaiz: String;
begin
  XML_Distribuicao := '';
  TagRaiz := 'procEventoCTe';
  Result := False;
  if (Trim(XML_Env) = '')
  or (Trim(XML_Ret) = '') then
  begin
    MostraMensagem('Evento sem XML ' + InfoLocal + ':', Id);
    Exit;
  end else
  begin
    XML_Distribuicao := TipoXML(False) +
      '<' + TagRaiz + ' xmlns="http://www.portalfiscal.inf.br/cte" versao="' +
      MLAGeral.MudaPontuacao(FormatFloat('0.00', Versao), '.', '.') + '">' +
      XML_Env + XML_Ret + '</' + TagRaiz + '>';
    Result := True;
  end;
end;

(* Parei aqui
TRegExpr(comp): ParseReg Too Many () (pos 92)
Express�o regular: ER10
Grupo:0
Codigo:406
Grupo:0
Campo:dEmi
Valor:2015-12-05
*)

end.
