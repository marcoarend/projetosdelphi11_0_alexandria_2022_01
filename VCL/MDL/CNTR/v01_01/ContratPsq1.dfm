object FmContratPsq1: TFmContratPsq1
  Left = 339
  Top = 185
  Caption = 'CAD-CONTR-004 :: Pesquisa de Contrato'
  ClientHeight = 404
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 262
        Height = 32
        Caption = 'Pesquisa de Contrato'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 262
        Height = 32
        Caption = 'Pesquisa de Contrato'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 262
        Height = 32
        Caption = 'Pesquisa de Contrato'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 242
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 242
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 242
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 110
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 0
            Width = 55
            Height = 13
            Caption = 'Contratada:'
          end
          object Label4: TLabel
            Left = 8
            Top = 40
            Width = 58
            Height = 13
            Caption = 'Contratante:'
          end
          object EdContratada: TdmkEditCB
            Left = 8
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Contratada'
            UpdCampo = 'Contratada'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdContratadaChange
            OnExit = EdContratadaExit
            DBLookupComboBox = CBContratada
            IgnoraDBLookupComboBox = False
          end
          object CBContratada: TdmkDBLookupComboBox
            Left = 64
            Top = 16
            Width = 560
            Height = 21
            KeyField = 'Codigo'
            ListField = 'CONTRATADA'
            ListSource = DsContratada
            TabOrder = 1
            dmkEditCB = EdContratada
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdContratante: TdmkEditCB
            Left = 8
            Top = 56
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Contratante'
            UpdCampo = 'Contratante'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdContratanteChange
            OnExit = EdContratanteExit
            DBLookupComboBox = CBContratante
            IgnoraDBLookupComboBox = False
          end
          object CBContratante: TdmkDBLookupComboBox
            Left = 63
            Top = 56
            Width = 560
            Height = 21
            KeyField = 'Codigo'
            ListField = 'CONTRATANTE'
            ListSource = DsContratante
            TabOrder = 3
            dmkEditCB = EdContratante
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CkInativos: TCheckBox
            Left = 12
            Top = 88
            Width = 185
            Height = 17
            Caption = 'N'#227'o mostar contratos rescindidos.'
            Checked = True
            State = cbChecked
            TabOrder = 4
            OnClick = CkInativosClick
          end
          object CGStatus: TdmkCheckGroup
            Left = 652
            Top = 0
            Width = 128
            Height = 110
            Align = alRight
            Caption = 'Status:'
            ItemIndex = 0
            Items.Strings = (
              'Cadastrando'
              'N'#227'o aprovado'
              'Aprovado')
            TabOrder = 5
            OnClick = CGStatusClick
            QryCampo = 'Status'
            UpdCampo = 'Status'
            UpdType = utYes
            Value = 1
            OldValor = 0
          end
        end
        object DBGrid1: TDBGrid
          Left = 2
          Top = 125
          Width = 780
          Height = 115
          Align = alClient
          DataSource = DsContratos
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Contrato'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Versao'
              Title.Caption = 'Vers'#227'o'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o do contrato'
              Width = 438
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DInstal'
              Title.Caption = 'In'#237'cio'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DVencimento'
              Title.Caption = 'Renovar'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaCntrFim'
              Title.Caption = 'Rescis'#227'o'
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 290
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 334
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrContratada: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial,  Nome) CONTRATADA'
      'FROM Entidades'
      'ORDER BY CONTRATADA')
    Left = 198
    Top = 95
    object QrContratadaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContratadaCONTRATADA: TWideStringField
      FieldName = 'CONTRATADA'
      Size = 100
    end
  end
  object DsContratada: TDataSource
    DataSet = QrContratada
    Left = 227
    Top = 95
  end
  object QrContratante: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial,  Nome) CONTRATANTE'
      'FROM Entidades'
      'ORDER BY CONTRATANTE')
    Left = 255
    Top = 95
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContratanteCONTRATANTE: TWideStringField
      FieldName = 'CONTRATANTE'
      Size = 100
    end
  end
  object DsContratante: TDataSource
    DataSet = QrContratante
    Left = 283
    Top = 95
  end
  object QrContratos: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrContratosAfterOpen
    BeforeClose = QrContratosBeforeClose
    SQL.Strings = (
      'SELECT Codigo, Nome, DInstal, DtaCntrFim, DVencimento'
      'FROM contratos'
      'WHERE Ativo=1'
      'AND DInstal <= SYSDATE()'
      'AND DInstal > 1'
      'AND DtaCntrFim >= SYSDATE()'
      'AND Contratada=:P0'
      'AND Contratante=:P1'
      'ORDER BY Nome')
    Left = 188
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrContratosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContratosNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrContratosVersao: TFloatField
      FieldName = 'Versao'
      DisplayFormat = '0.00'
    end
    object QrContratosDInstal: TDateField
      FieldName = 'DInstal'
      OnGetText = QrContratosDInstalGetText
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosDtaCntrFim: TDateField
      FieldName = 'DtaCntrFim'
      OnGetText = QrContratosDtaCntrFimGetText
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosDVencimento: TDateField
      FieldName = 'DVencimento'
      OnGetText = QrContratosDVencimentoGetText
      DisplayFormat = 'dd/mm/yyyy'
    end
  end
  object DsContratos: TDataSource
    DataSet = QrContratos
    Left = 216
    Top = 192
  end
end
