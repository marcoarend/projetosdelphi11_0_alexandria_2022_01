unit ContratPsq2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, UnDmkProcFunc, dmkRadioGroup, UnDmkEnums, dmkCheckBox, AdvObj,
  BaseGrid, AdvGrid, DBAdvGrid, dmkDBGridZTO, dmkCompoStore;

type
  TFmContratPsq2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    QrContratada: TmySQLQuery;
    QrContratadaCodigo: TIntegerField;
    QrContratadaCONTRATADA: TWideStringField;
    DsContratada: TDataSource;
    QrContratante: TmySQLQuery;
    IntegerField2: TIntegerField;
    QrContratanteCONTRATANTE: TWideStringField;
    DsContratante: TDataSource;
    DBGContratos: TdmkDBGridZTO;
    QrContratos: TmySQLQuery;
    QrContratosCodigo: TIntegerField;
    QrContratosNome: TWideStringField;
    QrContratosDInstal: TDateField;
    QrContratosDVencimento: TDateField;
    DsContratos: TDataSource;
    QrContratosDtaCntrFim: TDateField;
    QrContratosVersao: TFloatField;
    CGStatus: TdmkCheckGroup;
    QrContratosContratada: TIntegerField;
    QrContratosContratante: TIntegerField;
    QrContratosDContrato: TDateField;
    QrContratosStatus: TSmallintField;
    QrContratosValorMes: TFloatField;
    QrContratosTexto: TWideMemoField;
    QrContratosWinDocArq: TWideStringField;
    QrContratosFormaUso: TSmallintField;
    QrContratosDtaPropIni: TDateField;
    QrContratosDtaPropFim: TDateField;
    QrContratosDtaImprime: TDateTimeField;
    QrContratosDtaAssina: TDateField;
    QrContratosTratamento: TWideStringField;
    QrContratosArtigoDef: TSmallintField;
    QrContratosPerDefImpl: TSmallintField;
    QrContratosPerQtdImpl: TSmallintField;
    QrContratosPerDefTrei: TSmallintField;
    QrContratosPerQtdTrei: TSmallintField;
    QrContratosPerDefMoni: TSmallintField;
    QrContratosPerQtdMoni: TSmallintField;
    QrContratosPerDefIniI: TSmallintField;
    QrContratosPerQtdIniI: TSmallintField;
    QrContratosRepreLegal: TIntegerField;
    QrContratosTestemun01: TIntegerField;
    QrContratosTestemun02: TIntegerField;
    QrContratosPercMulta: TFloatField;
    QrContratosPercJuroM: TFloatField;
    QrContratosTpPagDocu: TWideStringField;
    QrContratosddMesVcto: TSmallintField;
    QrContratosLugarNome: TWideStringField;
    QrContratosLugArtDef: TSmallintField;
    QrContratosLk: TIntegerField;
    QrContratosDataCad: TDateField;
    QrContratosDataAlt: TDateField;
    QrContratosUserCad: TIntegerField;
    QrContratosUserAlt: TIntegerField;
    QrContratosAlterWeb: TSmallintField;
    QrContratosAtivo: TSmallintField;
    QrContratosEscopoSP: TWideStringField;
    QrContratosDtaPrxRenw: TDateField;
    QrContratosNCONTRATADA: TWideStringField;
    QrContratosNCONTRATANTE: TWideStringField;
    QrContratosNO_REP_LEGAL: TWideStringField;
    QrContratosNO_TESTEM_01: TWideStringField;
    QrContratosNO_TESTEM_02: TWideStringField;
    Panel6: TPanel;
    GroupBox9: TGroupBox;
    GroupBox10: TGroupBox;
    GroupBox11: TGroupBox;
    DBGDesServico: TdmkDBGridZTO;
    QrBugsS: TmySQLQuery;
    QrBugsSo2Gru: TIntegerField;
    QrBugsSc2Gru: TIntegerField;
    QrBugsSn2Gru: TWideStringField;
    QrBugsSa2Gru: TSmallintField;
    QrBugsSo1Cab: TIntegerField;
    QrBugsSc1Cab: TIntegerField;
    QrBugsSn1Cab: TWideStringField;
    QrBugsSa1Cab: TSmallintField;
    QrBugsSAtivo: TSmallintField;
    QrBugsSNO_Ativo: TWideStringField;
    DsBugsS: TDataSource;
    AGABugsS: TDBAdvGrid;
    QrBugsN: TmySQLQuery;
    QrBugsNo2Gru: TIntegerField;
    QrBugsNc2Gru: TIntegerField;
    QrBugsNn2Gru: TWideStringField;
    QrBugsNa2Gru: TSmallintField;
    QrBugsNo1Cab: TIntegerField;
    QrBugsNc1Cab: TIntegerField;
    QrBugsNn1Cab: TWideStringField;
    QrBugsNa1Cab: TSmallintField;
    QrBugsNAtivo: TSmallintField;
    QrBugsNNO_Ativo: TWideStringField;
    DsBugsN: TDataSource;
    AGABugsN: TDBAdvGrid;
    QrPsq1: TmySQLQuery;
    QrPsq1o2Gru: TIntegerField;
    QrPsq1c2Gru: TIntegerField;
    QrPsq1n2Gru: TWideStringField;
    QrPsq1a2Gru: TSmallintField;
    QrPsq1o1Cab: TIntegerField;
    QrPsq1c1Cab: TIntegerField;
    QrPsq1n1Cab: TWideStringField;
    QrPsq1a1Cab: TSmallintField;
    QrPsq1Ativo: TSmallintField;
    Splitter1: TSplitter;
    QrDesServicos: TmySQLQuery;
    DsDesServicos: TDataSource;
    QrDesServicosCodigo: TIntegerField;
    QrDesServicosNome: TWideStringField;
    BtReabre: TBitBtn;
    Panel7: TPanel;
    Panel8: TPanel;
    Label3: TLabel;
    EdContratada: TdmkEditCB;
    CBContratada: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdContratante: TdmkEditCB;
    CBContratante: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdNome: TdmkEdit;
    CkValorMes: TdmkCheckBox;
    EdValorMesMin: TdmkEdit;
    EdValorMesMax: TdmkEdit;
    Panel9: TPanel;
    Panel10: TPanel;
    CkInativos: TCheckBox;
    RGBugsS: TRadioGroup;
    RGBugsN: TRadioGroup;
    RGDesServico: TRadioGroup;
    QrPsqP: TmySQLQuery;
    QrPsqPCodigo: TIntegerField;
    CSTabSheetChamou: TdmkCompoStore;
    QrContratosNO_STATUS: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdContratadaChange(Sender: TObject);
    procedure EdContratanteChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGContratosDblClick(Sender: TObject);
    procedure QrContratosBeforeClose(DataSet: TDataSet);
    procedure QrContratosAfterOpen(DataSet: TDataSet);
    procedure CkInativosClick(Sender: TObject);
    procedure CGStatusClick(Sender: TObject);
    procedure QrContratosDtaCntrFimGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure QrContratosDVencimentoGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure QrContratosDInstalGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure AGABugsSClick(Sender: TObject);
    procedure AGABugsSClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure AGABugsNClick(Sender: TObject);
    procedure AGABugsNClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure QrBugsSAfterOpen(DataSet: TDataSet);
    procedure QrBugsNAfterOpen(DataSet: TDataSet);
    procedure BtReabreClick(Sender: TObject);
    procedure EdValorMesMinExit(Sender: TObject);
    procedure CkValorMesClick(Sender: TObject);
    procedure EdValorMesMinChange(Sender: TObject);
    procedure EdValorMesMaxChange(Sender: TObject);
    procedure RGBugsSClick(Sender: TObject);
    procedure RGBugsNClick(Sender: TObject);
    procedure RGDesServicoClick(Sender: TObject);
    procedure DBGDesServicoCellClick(Column: TColumn);
    procedure EdNomeChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FServicos, FFiltroBugsS, FFiltroBugsN: String;
    FCriando: Boolean;
    //
    procedure AGABugsXClickCell(Sender: TObject; ARow, ACol: Integer; Tabela:
              String; Qry: TmySQLQuery);
    procedure FechaContratos();
    procedure ReopenContratos();
    procedure SelecionaItem();
    function  CampoDeNivel(Nivel: Integer; Letra: String): String;
    function  SQLPragasSN(Alias, Tabela: String; QryBug: TmySQLQuery): String;
    function  SQLDesservicos(): String;
  public
    { Public declarations }
    FContrato: Integer;
  end;

  var
  FmContratPsq2: TFmContratPsq2;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, CreateBugs, ModuleGeral,
UCreate, MyGlyfs, Principal, Contratos, UnContrat_Tabs;

{$R *.DFM}

const
  FColsToMerge: array[0..1] of Integer = (3,5);
  MaxNivel = 2;

procedure TFmContratPsq2.AGABugsNClick(Sender: TObject);
begin
  MyObjects.UpdMergeDBAvGrid(AGABugsN, FColsToMerge);
end;

procedure TFmContratPsq2.AGABugsNClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  FechaContratos();
  AGABugsXClickCell(Sender, ARow, ACol, FFiltroBugsN, QrBugsN);
end;

procedure TFmContratPsq2.AGABugsSClick(Sender: TObject);
begin
  MyObjects.UpdMergeDBAvGrid(AGABugsS, FColsToMerge);
end;

procedure TFmContratPsq2.AGABugsSClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  FechaContratos();
  AGABugsXClickCell(Sender, ARow, ACol, FFiltroBugsS, QrBugsS);
end;

procedure TFmContratPsq2.AGABugsXClickCell(Sender: TObject; ARow,
  ACol: Integer; Tabela: String; Qry: TmySQLQuery);
var
  cN1, Ativo, Nivel, I: Integer;
  Campo, Codigo, AtivS, FldAtiv, SQL(*, _AND_, xFld, nFld*): String;
begin
  FldAtiv := TDBAdvGrid(Sender).Columns[ACol].FieldName;
  if Copy(FldAtiv, 1, 1) = 'a' then
  begin
    Nivel := Geral.IMV(Copy(FldAtiv, 2, 1));
    if Nivel = 0 then
      Exit;
    cN1 := Geral.IMV(TDBAdvGrid(Sender).Cells[1, ARow]);
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq1, DModG.MyPID_DB, [
    'SELECT * FROM ' + Tabela,
    'WHERE c1Cab=' + Geral.FF0(cN1),
    '']);
    Ativo := QrPsq1.FieldByName(FldAtiv).AsInteger;
    Campo := CampoDeNivel(Nivel, 'c');
    Codigo := Geral.FF0(QrPsq1.FieldByName(Campo).AsInteger);
    if Ativo = 1 then Ativo := 0 else Ativo := 1;
    AtivS := Geral.FF0(Ativo);
    //
    SQL := '';
    for I := 1 to Nivel do
      SQL := SQL + ' ' + CampoDeNivel(I, 'a') + '=' + AtivS + ', ';
    SQL := SQL + 'Ativo=' + AtivS;
    //
    UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
    'UPDATE ' + Tabela,
    ' SET ',
    SQL,
    'WHERE ' + Campo + '=' + Codigo,
    '']);
    //
    UMyMod.AbreQuery(Qry, DModG.MyPID_DB);
    Qry.Locate('c1Cab', cN1, []);
  end;
end;

procedure TFmContratPsq2.BtOKClick(Sender: TObject);
begin
  SelecionaItem();
end;

procedure TFmContratPsq2.BtReabreClick(Sender: TObject);
begin
  ReopenContratos();
end;

procedure TFmContratPsq2.BtSaidaClick(Sender: TObject);
begin
  if TFmContratPsq2(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(TFmContratPsq2(Self), TTabSheet(CSTabSheetChamou.Component));
end;

function TFmContratPsq2.CampoDeNivel(Nivel: Integer; Letra: String): String;
begin
  case Nivel of
    1: Result := 'Cab';
    2: Result := 'Gru';
    else Result := '???';
  end;
  //
  Result := Letra + Geral.FF0(Nivel) + Result;
end;

procedure TFmContratPsq2.CGStatusClick(Sender: TObject);
begin
  FechaContratos();
end;

procedure TFmContratPsq2.CkInativosClick(Sender: TObject);
begin
  FechaContratos();
end;

procedure TFmContratPsq2.CkValorMesClick(Sender: TObject);
begin
  FechaContratos();
end;

procedure TFmContratPsq2.DBGDesServicoCellClick(Column: TColumn);
begin
  FechaContratos();
end;

procedure TFmContratPsq2.DBGContratosDblClick(Sender: TObject);
begin
  SelecionaItem();
end;

procedure TFmContratPsq2.EdContratadaChange(Sender: TObject);
begin
  FechaContratos();
end;

procedure TFmContratPsq2.EdContratanteChange(Sender: TObject);
begin
  FechaContratos();
end;

procedure TFmContratPsq2.EdNomeChange(Sender: TObject);
begin
  FechaContratos();
end;

procedure TFmContratPsq2.EdValorMesMaxChange(Sender: TObject);
begin
  FechaContratos();
end;

procedure TFmContratPsq2.EdValorMesMinChange(Sender: TObject);
begin
  FechaContratos();
end;

procedure TFmContratPsq2.EdValorMesMinExit(Sender: TObject);
begin
  if EdValorMesMax.ValueVariant < EdValorMesMin.ValueVariant then
    EdValorMesMax.ValueVariant := EdValorMesMin.ValueVariant;
end;

procedure TFmContratPsq2.FechaContratos();
begin
  QrContratos.Close;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmContratPsq2.FormActivate(Sender: TObject);
begin
  if TFmContratPsq2(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
    if FCriando then
    begin
      FCriando := False;
      //FechaContratos();
    end;
  end;
end;

procedure TFmContratPsq2.FormCreate(Sender: TObject);
  function GeraRegistrosPragas(Tabela: String): String;
  begin
    UnCreateBugs.RecriaTempTableNovo(
      ntrtt_OS_Alv_, DmodG.QrUpdPID1, False, 1, Tabela);
    UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
    'DELETE FROM ' + Tabela + ' ; ',
    'INSERT INTO ' + Tabela + ' ',
    'SELECT 1 o2Gru, gru.Codigo c2Gru, gru.Nome n2Gru, 0 a2Gru, ',
    '1 o1Cab, cab.Codigo c1Cab, cab.Nome n1Cab, 0 a1Cab, 0 Ativo ',
    'FROM ' + TMeuDB + '.praga_z cab ',
    'LEFT JOIN ' + TMeuDB + '.praga_a gru ON gru.Codigo=cab.NivSup ',
    '']);
    Result := Tabela;
  end;
begin
  FCriando := True;
  ImgTipo.SQLType := stLok;
  FContrato := 0;
  UMyMod.AbreQuery(QrContratada, Dmod.MyDB);
  UMyMod.AbreQuery(QrContratante, Dmod.MyDB);
  CGStatus.SetMaxValue();
  //
  FFiltroBugsS := GeraRegistrosPragas('_filtrobugss');
  UMyMod.AbreQuery(QrBugsS, DModG.MyPID_DB);
  //
  FFiltroBugsN := GeraRegistrosPragas('_filtrobugsn');
  UMyMod.AbreQuery(QrBugsN, DModG.MyPID_DB);
  //
{
  FServicos := UCriar.RecriaTempTableNovo(ntrttSelCods, DModG.QrUpdPID1, False,
  1, '_ContrServi');
  UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
  'DELETE FROM ' + FServicos + '; ',
  'INSERT INTO ' + FServicos,
  'SELECT Codigo Nivel1, 0 Nivel2, ',
  '0 Nivel3, 0 Nivel4, 0 Nivel5, Nome, 0 Ativo ',
  'FROM ' + TMeuDB + '.desservico',
  '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrDesServicos, DModG.MyPID_DB, [
  'SELECT * FROM ' + FServicos,
  '']);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrDesServicos, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM desservico ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmContratPsq2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
  //
  MyObjects.UpdMergeDBAvGrid(AGABugsN, FColsToMerge);
  MyObjects.UpdMergeDBAvGrid(AGABugsS, FColsToMerge);
end;

procedure TFmContratPsq2.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
  if FCriando then
  begin
    FCriando := False;
    //FechaContratos();
  end;
end;

procedure TFmContratPsq2.QrBugsNAfterOpen(DataSet: TDataSet);
begin
  MyObjects.UpdMergeDBAvGrid(AGABugsN, FColsToMerge);
end;

procedure TFmContratPsq2.QrBugsSAfterOpen(DataSet: TDataSet);
begin
  MyObjects.UpdMergeDBAvGrid(AGABugsS, FColsToMerge);
end;

procedure TFmContratPsq2.QrContratosAfterOpen(DataSet: TDataSet);
begin
  BtOK.Enabled := QrContratos.RecordCount > 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Geral.FF0(
    QrContratos.RecordCount) + ' registros foram localizados!');
end;

procedure TFmContratPsq2.QrContratosBeforeClose(DataSet: TDataSet);
begin
  BtOK.Enabled := False;
end;

procedure TFmContratPsq2.QrContratosDInstalGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := Geral.FDT(QrContratosDInstal.Value, 2);
end;

procedure TFmContratPsq2.QrContratosDtaCntrFimGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := Geral.FDT(QrContratosDtaCntrFim.Value, 2);
end;

procedure TFmContratPsq2.QrContratosDVencimentoGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := Geral.FDT(QrContratosDVencimento.Value, 2);
end;

procedure TFmContratPsq2.ReopenContratos();
var
  Contratada, Contratante: Integer;
  SQL_Contratada, SQL_Contratante, SQL_Inativos, SQL_Status, SQL_DesServicos,
  SQL_PragasS, SQL_PragasN, SQL_Nome, SQL_ValorMes, SIT_TXT: String;
begin
  if FCriando then
    Exit;
  //
  Contratada  := EdContratada.ValueVariant;
  if Contratada = 0 then
    SQL_Contratada := ''
  else
    SQL_Contratada := 'AND Contratada=' + Geral.FF0(Contratada);
  Contratante := EdContratante.ValueVariant;
  if Contratante = 0 then
    SQL_Contratante := ''
  else
    SQL_Contratante := 'AND Contratante=' + Geral.FF0(Contratante);
  //
  if CkInativos.Checked then
    SQL_Inativos := Geral.ATS([
    'AND DInstal <= SYSDATE() ',
    'AND DInstal > 1 ',
    'AND (DtaCntrFim >= SYSDATE() ',
    '     OR DtaCntrFim < "1900-01-01") '
    ])
  else
    SQL_Inativos := '';
  //
(* Altera��o: Aumento de 3 para 5 status
  case CGStatus.Value of
    1: SQL_Status := 'AND Status=0';
    2: SQL_Status := 'AND Status=1';
    3: SQL_Status := 'AND Status IN (1,0)';
    4: SQL_Status := 'AND Status=2';
    5: SQL_Status := 'AND Status IN (2,0)';
    6: SQL_Status := 'AND Status IN (2,1)';
    //0,7:
    else SQL_Status := '';
  end;
*)
  SQL_Status := '';
  if Geral.IntInConjunto(1, CGStatus.Value) then
    SQL_Status := SQL_Status + ', 0';
  if Geral.IntInConjunto(2, CGStatus.Value) then
    SQL_Status := SQL_Status + ', 1';
  if Geral.IntInConjunto(4, CGStatus.Value) then
    SQL_Status := SQL_Status + ', 2';
  if Geral.IntInConjunto(8, CGStatus.Value) then
    SQL_Status := SQL_Status + ', 3';
  if Geral.IntInConjunto(16, CGStatus.Value) then
    SQL_Status := SQL_Status + ', 4';
  //
  if SQL_Status <> '' then
    SQL_Status := 'AND Status IN (' + Copy(SQL_Status, 3) + ')';
  // Fim aumento de 3 para 5 status
  //
  case RGBugsS.ItemIndex of
    0: SQL_PragasS := '';
    1: SQL_PragasS := SQLPragasSN('cbs', FFiltroBugsS, QrBugsS);
    else SQL_PragasS := 'AND cbs.Praga_S=???';
  end;
  //
  case RGBugsN.ItemIndex of
    0: SQL_PragasN := '';
    1: SQL_PragasN := SQLPragasSN('cbn', FFiltroBugsN, QrBugsN);
    else SQL_PragasN := 'AND cbn.Praga_N=???';
  end;
  //
  case RGDesServico.ItemIndex of
    0: SQL_DesServicos := '';
    1: SQL_DesServicos := SQLDesservicos();
    else SQL_DesServicos := 'AND csv.Servico=???';
  end;
  //
  SQL_Nome := Trim(EdNome.Text);
  if SQL_Nome <> '' then
    SQL_Nome := 'AND con.Nome LIKE "%' + EdNome.Text + '%"';
  //
  if CkValorMes.Checked then
    SQL_ValorMes := 'AND con.ValorMes BETWEEN ' +
    Geral.FFT_Dot(EdValorMesMin.ValueVariant, 2, siPositivo) +
    ' AND ' +
    Geral.FFT_Dot(EdValorMesMax.ValueVariant, 2, siPositivo)
  else
    SQL_ValorMes := '';
  //
  SIT_TXT := dmkPF.ArrayToTexto('con.Status', 'NO_STATUS', pvPos, True,
    sListaStatusContratos);
  UnDmkDAC_PF.AbreMySQLQuery0(QrContratos, Dmod.MyDB, [
  'SELECT DISTINCT con.*, ',
  SIT_TXT,
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NCONTRATADA, ',
  'IF(enb.Tipo=0, enb.RazaoSocial, enb.Nome) NCONTRATANTE, ',
  'IF(rep.Tipo=0, rep.RazaoSocial, rep.Nome) NO_REP_LEGAL, ',
  'IF(ts1.Tipo=0, ts1.RazaoSocial, ts1.Nome) NO_TESTEM_01, ',
  'IF(ts2.Tipo=0, ts2.RazaoSocial, ts2.Nome) NO_TESTEM_02 ',
  'FROM contratos con ',
  'LEFT JOIN contrbugss cbs ON cbs.Codigo = con.Codigo ',
  'LEFT JOIN contrbugsn cbn ON cbn.Codigo = con.Codigo ',
  'LEFT JOIN contrservi csv ON csv.Codigo = con.Codigo ',
  'LEFT JOIN entidades ent ON ent.Codigo = con.Contratada ',
  'LEFT JOIN entidades enb ON enb.Codigo = con.Contratante ',
  'LEFT JOIN entidades rep ON rep.Codigo = con.RepreLegal ',
  'LEFT JOIN entidades ts1 ON ts1.Codigo = con.Testemun01 ',
  'LEFT JOIN entidades ts2 ON ts2.Codigo = con.Testemun02 ',
(*
  'SELECT Codigo, Nome, DInstal, DtaCntrFim,  ',
  'DVencimento, Versao ',
  'FROM contratos ',
*)
  'WHERE con.Ativo > -1 ',
  SQL_Contratada,
  SQL_Contratante,
  SQL_Inativos,
  SQL_Status,
  SQL_PragasS,
  SQL_PragasN,
  SQL_DesServicos,
  SQL_Nome,
  SQL_ValorMes,
  'ORDER BY Nome ',
  '']);
end;

procedure TFmContratPsq2.RGBugsNClick(Sender: TObject);
begin
  FechaContratos();
end;

procedure TFmContratPsq2.RGBugsSClick(Sender: TObject);
begin
  FechaContratos();
end;

procedure TFmContratPsq2.RGDesServicoClick(Sender: TObject);
begin
  FechaContratos();
end;

procedure TFmContratPsq2.SelecionaItem();
var
  Form: TForm;
  Contrato: Integer;
begin
  if TFmContratPsq2(Self).Owner is TApplication then
  begin
    FContrato := QrContratosCodigo.Value;
    Close;
  end else
  begin
    Contrato := QrContratosCodigo.Value;
    Form := MyObjects.FormTDICria(TFmContratos, FmPrincipal.PageControl1,
      FmPrincipal.AdvToolBarPager1);
    TFmContratos(Form).LocCod(Contrato, Contrato);
  end;
end;

function TFmContratPsq2.SQLDesservicos(): String;
var
  Codigo, I: Integer;
  Codigos: String;
begin
  Result := '';
  Codigo := QrDesServicosCodigo.Value;
  QrDesServicos.DisableControls;
  try
    if DBGDesServico.SelectedRows.Count > 0 then
    begin
      with DBGDesServico.DataSource.DataSet do
      for I:= 0 to DBGDesServico.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGDesServico.SelectedRows.Items[i]));
        Codigos := Codigos + ', ' + Geral.FF0(QrDesServicosCodigo.Value);
      end;
    end else
      Codigos := '';
    //
    Codigos := Copy(Codigos, 3);
    if Codigos <> '' then
      Result := 'AND csv.Servico IN (' + Codigos + ')';
  finally
    QrDesServicos.EnableControls;
  end;
end;

function TFmContratPsq2.SQLPragasSN(Alias, Tabela: String; QryBug: TmySQLQuery): String;
var
  SQLA, SQLZ: String;
  Niveis: Integer;
begin
  Result := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqP, DModG.MyPID_DB, [
  'SELECT DISTINCT c2Gru Codigo',
  'FROM ' + Tabela,
  'WHERE a2Gru=1 ',
  '']);
  SQLA := '';
  QrPsqP.First;
  while not QrPsqP.Eof do
  begin
    SQLA := SQLA + ', ' + Geral.FF0(QrPsqPCodigo.Value);
    QrPsqP.Next;
  end;
  SQLA := Copy(SQLA, 3);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqP, DModG.MyPID_DB, [
  'SELECT DISTINCT c1Cab Codigo',
  'FROM ' + Tabela,
(*
  'WHERE a2Gru=0 ',
  'AND a1Cab=1 ',
*)
  'WHERE a1Cab=1 ',
  '']);
  SQLZ := '';
  QrPsqP.First;
  while not QrPsqP.Eof do
  begin
    SQLZ := SQLZ + ', ' + Geral.FF0(QrPsqPCodigo.Value);
    QrPsqP.Next;
  end;
  SQLZ := Copy(SQLZ, 3);
  //
  Niveis := 0;
  if SQLA <> '' then
  begin
    Niveis := Niveis + 1;
    SQLA := Alias + '.Praga_A IN (' +SQLA + ')' + slineBreak;
  end;
  if SQLZ <> '' then
  begin
    Niveis := Niveis + 2;
    SQLZ := Alias + '.Praga_Z IN (' +SQLZ + ')' + sLineBreak;
  end;
  if Niveis > 0 then
  begin
    Result := 'AND ( ' + sLineBreak;
    case Niveis of
      1: Result :=  Result + SQLA;
      2: Result :=  Result + SQLZ;
      3: Result :=  Result + SQLA + '  OR ' + sLineBreak + SQLZ;
    end;
    Result := Result + ') ';
  end;
end;

end.
