unit ContratTag;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmContratTag = class(TForm)
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel5: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label7: TLabel;
    EdTag: TdmkEdit;
    Panel3: TPanel;
    CkContinuar: TCheckBox;
    MeText: TMemo;
    Label1: TLabel;
    EdNome: TdmkEdit;
    Label2: TLabel;
    DBEdCodUso: TDBEdit;
    Label4: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenQrITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmContratTag: TFmContratTag;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck;

{$R *.DFM}

procedure TFmContratTag.BtOKClick(Sender: TObject);
var
  Nome, Tag, Text: String;
  Codigo, Controle: Integer;
begin
  Codigo         := FQrCab.FieldByName('Codigo').AsInteger;
  Controle       := EdControle.ValueVariant;
  Nome           := EdNome.Text;
  Tag            := EdTag.Text;
  Text           := MeText.Text;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Informe um comentário!') then
    Exit;
  if MyObjects.FIC(Trim(Tag) = '', EdTag, 'Informe uma tag!') then
    Exit;
  Controle := UMyMod.BPGS1I32(
    'contrattag', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'contrattag', False, [
  'Codigo', 'Nome',
  'Tag', 'Text'], [
  'Controle'], [
  Codigo, Nome,
  Tag, Text], [
  Controle], True) then
  begin
    ReopenQrITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdNome.ValueVariant      := '';
      EdTag.ValueVariant       := '';
      EdTag.SetFocus;
    end else Close;
  end;
end;

procedure TFmContratTag.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContratTag.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdCodUso.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmContratTag.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmContratTag.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContratTag.ReopenQrITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    FQrIts.Open;
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
