unit UnContratUnit;

interface

uses
  Windows, SysUtils, Classes, DB, mySQLDbTables, DmkGeral, DmkDAC_PF, (*UnAppListas,*)
  UnInternalConsts, UnDmkProcFunc, frxClass, frxDBSet, Menus,
  dmkDBLookupComboBox, dmkEditCB, UnDmkEnums;

type
  TUnContratUnit = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function  PesquisaNumeroDoContrato(const Empresa, Contratante: Integer;
              var Contrato: Integer): Boolean;
    function  ContratosAVencer(Hoje: TDateTime; MostraForm: Boolean): Integer;
    function  ConfiguraStatus(): TStringList;
    function  ValidaDadosRescisao(ResciIdx: Integer; ResciDta: TDateTime;
              var Msg: String): Boolean;
    procedure MostraFormContratos(Contrato: Integer);
    procedure MostraCtrStatus(Codigo, Status: Integer; DtaCntrFim: TDate);
  end;

var
  ContratUnit: TUnContratUnit;
const
  CO_CONTRATO_STATUS_MAX = 4;
  CO_CONTRATO_STATUS_Str: array[0..CO_CONTRATO_STATUS_MAX] of string =
  (
    'Cadastrando',
    'N�o aprovado',
    'Aprovado',
    'Renovado',
    'Rescindido'
  );

  CO_CONTRATO_STATUS_Int: array[0..CO_CONTRATO_STATUS_MAX] of Integer =
  (
    0, 1, 2, 3, 4
  );


implementation

uses MyDBCheck, ContratPsq1, ContratRnw, Contratos, CtrStatus;

{ TUnContratUnit }

function TUnContratUnit.ConfiguraStatus: TStringList;
var
  Lista: TStringList;
begin
  Lista := TStringList.Create;
  try
    Lista.Add(CO_CONTRATO_STATUS_Str[0]);
    Lista.Add(CO_CONTRATO_STATUS_Str[1]);
    Lista.Add(CO_CONTRATO_STATUS_Str[2]);
    Lista.Add(CO_CONTRATO_STATUS_Str[3]);
    Lista.Add(CO_CONTRATO_STATUS_Str[4]);
  finally
    Result := Lista;
  end;
end;

procedure TUnContratUnit.MostraCtrStatus(Codigo, Status: Integer; DtaCntrFim: TDate);
begin
  if DBCheck.CriaFm(TFmCtrStatus, FmCtrStatus, afmoNegarComAviso) then
  begin
    FmCtrStatus.FCodigo     := Codigo;
    FmCtrStatus.FStatus     := Status;
    FmCtrStatus.FDtaCntrFim := DtaCntrFim;
    FmCtrStatus.ShowModal;
    FmCtrStatus.Destroy;
  end;
end;

function TUnContratUnit.ContratosAVencer(Hoje: TDateTime; MostraForm: Boolean): Integer;
begin
  if DBCheck.CriaFm(TFmContratRnw, FmContratRnw, afmoNegarComAviso) then
  begin
    FmContratRnw.FHoje := Hoje;
    FmContratRnw.ReopenContratos(Hoje);
    Result := FmContratRnw.QrContratos.RecordCount;
    if MostraForm then
    begin
      if Result > 0 then
        FmContratRnw.ShowModal
      else
        Geral.MB_Info('N�o h� contratos a serem renovados!');
    end;
    FmContratRnw.Destroy;
  end;
end;

procedure TUnContratUnit.MostraFormContratos(Contrato: Integer);
begin
  if DBCheck.CriaFm(TFmContratos, FmContratos, afmoNegarComAviso) then
  begin
    if Contrato <> 0 then
      FmContratos.LocCod(Contrato, Contrato);
    FmContratos.ShowModal;
    FmContratos.Destroy;
  end;
end;

function TUnContratUnit.PesquisaNumeroDoContrato(const Empresa,
  Contratante: Integer; var Contrato: Integer): Boolean;
var
  Entidade: Integer;
begin
  Contrato := 0;
  Result   := False;
  //
  if DBCheck.CriaFm(TFmContratPsq1, FmContratPsq1, afmoNegarComAviso) then
  begin
    FmContratPsq1.EdContratada.ValueVariant  := Empresa;
    FmContratPsq1.CBContratada.KeyValue      := Empresa;
    FmContratPsq1.EdContratante.ValueVariant := Contratante;
    FmContratPsq1.CBContratante.KeyValue     := Contratante;
    FmContratPsq1.ShowModal;
    if FmContratPsq1.FContrato <> 0 then
    begin
      //EdNumContrat.ValueVariant := FmContratPsq1.FContrato;
      Contrato := FmContratPsq1.FContrato;
      Result := True;
    end;
    FmContratPsq1.Destroy;
  end;
end;

function TUnContratUnit.ValidaDadosRescisao(ResciIdx: Integer;
  ResciDta: TDateTime; var Msg: String): Boolean;
var
  Conflito: Boolean;
begin
  Conflito := (
    (ResciIdx = CO_CONTRATO_STATUS_Int[4]) and (ResciDta < 2) //Rescindido
    ) or (
    (ResciIdx <> CO_CONTRATO_STATUS_Int[4]) and (ResciDta > 2) //Rescindido
    );
  if Conflito = True then
    Msg := 'Rescis�o precisa de informa��o na data e no status!'
  else
    Msg := '';
  //
  Result := Conflito;
end;

end.
