unit ContrNivTg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, AdvObj, BaseGrid, AdvGrid, DBAdvGrid, UnDmkEnums;

type
  TFmContrNivTg = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    Timer1: TTimer;
    Qr_Niveis_: TmySQLQuery;
    Ds_Niveis_: TDataSource;
    AdvGridDep: TDBAdvGrid;
    QrPsq1: TmySQLQuery;
    QrOD: TmySQLQuery;
    Qr_Niveis_oNiv5: TIntegerField;
    Qr_Niveis_cNiv5: TIntegerField;
    Qr_Niveis_nNiv5: TWideStringField;
    Qr_Niveis_aNiv5: TSmallintField;
    Qr_Niveis_oNiv4: TIntegerField;
    Qr_Niveis_cNiv4: TIntegerField;
    Qr_Niveis_nNiv4: TWideStringField;
    Qr_Niveis_aNiv4: TSmallintField;
    Qr_Niveis_oNiv3: TIntegerField;
    Qr_Niveis_cNiv3: TIntegerField;
    Qr_Niveis_nNiv3: TWideStringField;
    Qr_Niveis_aNiv3: TSmallintField;
    Qr_Niveis_oNiv2: TIntegerField;
    Qr_Niveis_cNiv2: TIntegerField;
    Qr_Niveis_nNiv2: TWideStringField;
    Qr_Niveis_aNiv2: TSmallintField;
    Qr_Niveis_oNiv1: TIntegerField;
    Qr_Niveis_cNiv1: TIntegerField;
    Qr_Niveis_nNiv1: TWideStringField;
    Qr_Niveis_aNiv1: TSmallintField;
    Qr_Niveis_Tabela: TSmallintField;
    Qr_Niveis_Ativo: TSmallintField;
    QrPsq1oNiv5: TIntegerField;
    QrPsq1cNiv5: TIntegerField;
    QrPsq1nNiv5: TWideStringField;
    QrPsq1aNiv5: TSmallintField;
    QrPsq1oNiv4: TIntegerField;
    QrPsq1cNiv4: TIntegerField;
    QrPsq1nNiv4: TWideStringField;
    QrPsq1aNiv4: TSmallintField;
    QrPsq1oNiv3: TIntegerField;
    QrPsq1cNiv3: TIntegerField;
    QrPsq1nNiv3: TWideStringField;
    QrPsq1aNiv3: TSmallintField;
    QrPsq1oNiv2: TIntegerField;
    QrPsq1cNiv2: TIntegerField;
    QrPsq1nNiv2: TWideStringField;
    QrPsq1aNiv2: TSmallintField;
    QrPsq1oNiv1: TIntegerField;
    QrPsq1cNiv1: TIntegerField;
    QrPsq1nNiv1: TWideStringField;
    QrPsq1aNiv1: TSmallintField;
    QrPsq1Tabela: TSmallintField;
    QrPsq1Ativo: TSmallintField;
    QrODoNiv5: TIntegerField;
    QrODcNiv5: TIntegerField;
    QrODnNiv5: TWideStringField;
    QrODaNiv5: TSmallintField;
    QrODoNiv4: TIntegerField;
    QrODcNiv4: TIntegerField;
    QrODnNiv4: TWideStringField;
    QrODaNiv4: TSmallintField;
    QrODoNiv3: TIntegerField;
    QrODcNiv3: TIntegerField;
    QrODnNiv3: TWideStringField;
    QrODaNiv3: TSmallintField;
    QrODoNiv2: TIntegerField;
    QrODcNiv2: TIntegerField;
    QrODnNiv2: TWideStringField;
    QrODaNiv2: TSmallintField;
    QrODoNiv1: TIntegerField;
    QrODcNiv1: TIntegerField;
    QrODnNiv1: TWideStringField;
    QrODaNiv1: TSmallintField;
    QrODTabela: TSmallintField;
    QrODAtivo: TSmallintField;
    QrSel: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AdvGridDepClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure Qr_Niveis_AfterOpen(DataSet: TDataSet);
    procedure AdvGridDepClick(Sender: TObject);
  private
    { Private declarations }
    FCriou: Boolean;
    FNiveis: String;
    //
    procedure Reopen_Niveis_(Campos: String; Tabela, Codigo: Integer);
  public
    { Public declarations }
    FFmChamou: TForm;
    //
    procedure PreparaTabela(Contrato: Integer);
    //
  end;

  var
  FmContrNivTg: TFmContrNivTg;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModuleGeral, CreateGeral,
Contratos;

{$R *.DFM}

const
  FColsToMerge: array[0..3] of Integer = (4,6,8,10);


procedure TFmContrNivTg.AdvGridDepClick(Sender: TObject);
begin
  MyObjects.UpdMergeDBAvGrid(AdvGridDep, FColsToMerge);
end;

procedure TFmContrNivTg.AdvGridDepClickCell(Sender: TObject; ARow, ACol: Integer);
  function CampoDeNivel(Nivel: Integer; Letra: String): String;
  begin
    Result := Letra + 'niv' + Geral.FF0(Nivel);
  end;
const
  MaxNivel = 5;
var
  cN1, Ativo, Nivel, I, Tabela: Integer;
  Campo, Codigo, AtivS, FldAtiv, SQL, _AND_, xFld, nFld: String;
begin
  FldAtiv := TDBAdvGrid(AdvGridDep).Columns[ACol].FieldName;
  if Copy(FldAtiv, 1, 1) = 'a' then
  begin
    Nivel := Geral.IMV(Copy(FldAtiv, 5, 1));
    if Nivel = 0 then
      Exit;
    cN1 := Geral.IMV(TDBAdvGrid(Sender).Cells[2, ARow]);
    Tabela := Geral.IMV(TDBAdvGrid(Sender).Cells[1, ARow]);
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq1, DModG.MyPID_DB, [
    'SELECT * FROM ' + FNiveis,
    'WHERE cNiv1=' + Geral.FF0(cN1),
    'AND Tabela=' + Geral.FF0(Tabela),
    '']);
    Ativo := QrPsq1.FieldByName(FldAtiv).AsInteger;
    Campo := CampoDeNivel(Nivel, 'c');
    Codigo := Geral.FF0(QrPsq1.FieldByName(Campo).AsInteger);
    if Ativo = 1 then Ativo := 0 else Ativo := 1;
    AtivS := Geral.FF0(Ativo);
    //
    SQL := '';
    for I := 1 to Nivel do
      SQL := SQL + ' ' + CampoDeNivel(I, 'a') + '=' + AtivS + ', ';
    SQL := SQL + 'Ativo=' + AtivS;
    //
    _AND_ := '';
    for I := Nivel to MaxNivel do
    begin
      xFld := CampoDeNivel(I, 'c');
      nFld := Geral.FF0(QrPsq1.FieldByName(xFld).AsInteger);
      _AND_ := _AND_ + 'AND ' + xFld + '=' + nFld + sLineBreak;
    end;
    //
    UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
    'UPDATE ' + FNiveis,
    ' SET ',
    SQL,
    'WHERE Tabela=' + Geral.FF0(Tabela),
    //'AND ' + Campo + '=' + Codigo,
    _AND_ +
    '']);
    //
    Reopen_Niveis_('Tabela;cNiv1', Tabela, cN1);
  end;
end;

procedure TFmContrNivTg.BtOKClick(Sender: TObject);
var
  Codigo, Controle(*, Tabela*): Integer;
  CodStr: String;
  CtrTagNiv1: Integer;
begin
  Controle := 0;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo selecionados');
    //
    CodStr := Geral.FF0(TFmContratos(FFmChamou).QrContratosCodigo.Value);
    //
    UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
    'INSERT INTO livre2 (Tabela, Campo, Codigo, Vezes, AlterWeb, Ativo) ',
    'SELECT "contrnivtg" Tabela, "Controle" Campo, ',
    'Controle Codigo, 1 Vezes, 0 AlterWeb, 0 Ativo ',
    'FROM contrnivtg ',
    'WHERE Codigo=' + CodStr,
    ';',
    'DELETE FROM contrnivtg ',
    'WHERE Codigo=' + CodStr,
    '']);
    //
    QrOD.Close;
    UnDmkDAC_PF.AbreMySQLQuery0(QrOD, DModG.MyPID_DB, [
    'SELECT * FROM ' + FNiveis,
    'WHERE Ativo=1 ',
    'ORDER BY Tabela, ',
    'oNiv5, nNiv5, cNiv5, ',
    'oNiv4, nNiv4, cNiv4, ',
    'oNiv3, nNiv3, cNiv3, ',
    'oNiv2, nNiv2, cNiv2, ',
    'oNiv1, nNiv1, cNiv1 ',
    '']);
    Codigo         := TFmContratos(FFmChamou).QrContratosCodigo.Value;
    QrOD.First;
    while not QrOD.Eof do
    begin
      CtrTagNiv1     := QrODcNiv1.Value;
      //
      Controle := UMyMod.BPGS1I32_Reaproveita('contrnivtg', 'Controle', '', '', tsDef, stIns, 0);
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'contrnivtg', False, [
      'Codigo', 'CtrTagNiv1'], [
      'Controle'], [
      Codigo, CtrTagNiv1], [
      Controle], True);
      //
      QrOD.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    TFmContratos(FFmChamou).ReopenContrNivTg(Controle);
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmContrNivTg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContrNivTg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := True;
end;

procedure TFmContrNivTg.FormCreate(Sender: TObject);
begin
  FCriou := False;
end;

procedure TFmContrNivTg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
  //
  MyObjects.UpdMergeDBAvGrid(AdvGridDep, FColsToMerge);
end;

procedure TFmContrNivTg.PreparaTabela(Contrato: Integer);
const
  Tabela = '1';
  aNiv1 = 1;
  Ativo = 1;
var
  Qry: TmySQLQuery;
  cNiv1: Integer;
begin
  Timer1.Enabled := False;
  if not FCriou then
  begin
    Screen.Cursor := crHourGlass;
    try
      FCriou := True;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabela temporária!');
      FNiveis :=
        UnCreateGeral.RecriaTempTableNovo(ntrtt_Niveis, DmodG.QrUpdPID1, False);
      //
      UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
      'DELETE FROM ' + FNiveis + ' ; ',
      'INSERT INTO ' + FNiveis + ' ',
      'SELECT ',
      '1 o5Niv, 0 c5Niv, "" n5Niv, 0 a5Niv, ',
      '1 o4Niv, 0 c4Niv, "" n4Niv, 0 a4Niv, ',
      'ctn3.Ordem o3Niv, ctn3.Codigo c3Niv, ctn3.Nome n3Niv, 0 a3Niv, ',
      'ctn2.Ordem o2Niv, ctn2.Codigo c2Niv, ctn2.Nome n2Niv, 0 a2Niv, ',
      'ctn1.Ordem o1Niv, ctn1.Codigo c1Niv, ctn1.Nome n1Niv, 0 a1Niv, ',
      Tabela + ' Tabela, 0 Ativo ',
      'FROM  ' + TMeuDB + '.ctrtagniv3 ctn3 ',
      'LEFT JOIN  ' + TMeuDB + '.ctrtagniv2 ctn2 ON ctn2.NivSup=ctn3.Codigo ',
      'LEFT JOIN  ' + TMeuDB + '.ctrtagniv1 ctn1 ON ctn1.NivSup=ctn2.Codigo; ',
      '']);
      //
      Qry := TMySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT CtrTagNiv1  ',
        'FROM contrnivtg ',
        'WHERE Codigo=' + Geral.FF0(Contrato),
        '']);
        Qry.First;
        while not Qry.Eof do
        begin
          cNiv1 := Qry.FieldByName('CtrTagNiv1').Value;
          //
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_niveis_', False, [
          (*'oNiv5', 'cNiv5', 'nNiv5',
          'aNiv5', 'oNiv4', 'cNiv4',
          'nNiv4', 'aNiv4', 'oNiv3',
          'cNiv3', 'nNiv3', 'aNiv3',
          'oNiv2', 'cNiv2', 'nNiv2',
          'aNiv2', 'oNiv1', 'cNiv1',
          'nNiv1',*)
          'aNiv1', 'Tabela', 'Ativo'], [
          'cNiv1'], [
          (*oNiv5, cNiv5, nNiv5,
          aNiv5, oNiv4, cNiv4,
          nNiv4, aNiv4, oNiv3,
          cNiv3, nNiv3, aNiv3,
          oNiv2, cNiv2, nNiv2,
          aNiv2, oNiv1, cNiv1,
          nNiv1,*)
          aNiv1, Tabela, Ativo], [
          cNiv1], False);
          //
          Qry.Next;
        end;
      finally
        Qry.Free;
      end;
      //
      Reopen_Niveis_('', 0, 0);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      Screen.Cursor := crDefault;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmContrNivTg.Qr_Niveis_AfterOpen(DataSet: TDataSet);
begin
  MyObjects.UpdMergeDBAvGrid(AdvGridDep, FColsToMerge);
end;

procedure TFmContrNivTg.Reopen_Niveis_(Campos: String; Tabela, Codigo: Integer);
begin
  Qr_Niveis_.Close;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr_Niveis_, DModG.MyPID_DB, [
  'SELECT od.*, ELT(od.Ativo + 1, "N", "S") NO_Ativo ',
  'FROM ' + FNiveis + ' od ',
  'ORDER BY od.Tabela, ',
  'od.oNiv5, od.nNiv5, od.cNiv5, ',
  'od.oNiv4, od.nNiv4, od.cNiv4, ',
  'od.oNiv2, od.nNiv2, od.cNiv2, ',
  'od.oNiv2, od.nNiv2, od.cNiv2, ',
  'od.oNiv1, od.nNiv1, od.cNiv1 ',
  '']);
  if Trim(Campos) <> '' then
  begin
    Qr_Niveis_.Locate(Campos, VarArrayOf([Tabela, Codigo]), [])
  end;
end;

end.
