object FmContratRnw: TFmContratRnw
  Left = 339
  Top = 185
  Caption = 'ACT-RENEW-002 :: Contratos a Renovar'
  ClientHeight = 629
  ClientWidth = 802
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 802
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 862
    object GB_R: TGroupBox
      Left = 754
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 814
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 706
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 766
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 252
        Height = 32
        Caption = 'Contratos a Renovar'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 252
        Height = 32
        Caption = 'Contratos a Renovar'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 252
        Height = 32
        Caption = 'Contratos a Renovar'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 802
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 862
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 802
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 862
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 802
        Height = 467
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 862
        object LaTotal: TLabel
          Left = 2
          Top = 452
          Width = 3
          Height = 13
          Align = alBottom
        end
        object DBGrid1: TDBGrid
          Left = 2
          Top = 15
          Width = 798
          Height = 437
          Align = alClient
          DataSource = DsContratos
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Contratante'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CONTRATANTE'
              Title.Caption = 'Nome contratante'
              Width = 280
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaPrxRenw'
              Title.Caption = 'Pr'#243'x. Renov.'
              Width = 70
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 802
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 862
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 798
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 858
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 802
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitWidth = 862
    object PnSaiDesis: TPanel
      Left = 656
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 716
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 654
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 714
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrContratos: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrContratosBeforeClose
    AfterScroll = QrContratosAfterScroll
    Left = 388
    Top = 152
    object QrContratosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContratosNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrContratosNO_CONTRATANTE: TWideStringField
      FieldName = 'NO_CONTRATANTE'
      Size = 100
    end
    object QrContratosDtaPrxRenw: TDateField
      FieldName = 'DtaPrxRenw'
    end
    object QrContratosContratante: TIntegerField
      FieldName = 'Contratante'
    end
  end
  object DsContratos: TDataSource
    DataSet = QrContratos
    Left = 388
    Top = 196
  end
end
