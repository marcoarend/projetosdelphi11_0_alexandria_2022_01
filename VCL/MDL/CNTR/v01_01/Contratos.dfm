object FmContratos: TFmContratos
  Left = 368
  Top = 194
  Caption = 'CAD-CONTR-001 :: Contratos'
  ClientHeight = 815
  ClientWidth = 1241
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnEdita: TPanel
    Left = 0
    Top = 118
    Width = 1241
    Height = 697
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 619
      Width = 1241
      Height = 78
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 20
        Width = 147
        Height = 50
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 1074
        Top = 18
        Width = 165
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 147
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1241
      Height = 434
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Panel7: TPanel
        Left = 2
        Top = 18
        Width = 1237
        Height = 414
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label7: TLabel
          Left = 15
          Top = 0
          Width = 16
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'ID:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label3: TLabel
          Left = 222
          Top = 0
          Width = 69
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Contratada:'
        end
        object SpeedButton6: TSpeedButton
          Left = 662
          Top = 20
          Width = 26
          Height = 26
          Hint = 'Inclui item de carteira'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SpeedButton6Click
        end
        object Label4: TLabel
          Left = 689
          Top = 0
          Width = 71
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Contratante:'
        end
        object SbContratante: TSpeedButton
          Left = 1199
          Top = 20
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SbContratanteClick
        end
        object Label10: TLabel
          Left = 15
          Top = 49
          Width = 106
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'T'#237'tulo do contrato:'
        end
        object Label8: TLabel
          Left = 15
          Top = 98
          Width = 83
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Data contrato:'
        end
        object Label6: TLabel
          Left = 782
          Top = 98
          Width = 87
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '1'#186' vencimento:'
        end
        object Label5: TLabel
          Left = 655
          Top = 98
          Width = 88
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'In'#237'cio vig'#234'ncia:'
        end
        object Label11: TLabel
          Left = 679
          Top = 49
          Width = 360
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Arquivo base do contrato em Windows Word ou Fast Report:'
        end
        object SbWinDocArq: TSpeedButton
          Left = 1201
          Top = 69
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SbWinDocArqClick
        end
        object Label14: TLabel
          Left = 90
          Top = 0
          Width = 117
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Vers'#227'o do contrato:'
        end
        object Label17: TLabel
          Left = 143
          Top = 98
          Width = 91
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'In'#237'cio proposta:'
        end
        object Label22: TLabel
          Left = 270
          Top = 98
          Width = 95
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Limite proposta:'
        end
        object Label23: TLabel
          Left = 398
          Top = 98
          Width = 99
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Data impress'#227'o:'
        end
        object Label24: TLabel
          Left = 526
          Top = 98
          Width = 97
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Data assinatura:'
        end
        object Label25: TLabel
          Left = 1038
          Top = 98
          Width = 87
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Data rescis'#227'o:'
        end
        object Label13: TLabel
          Left = 489
          Top = 49
          Width = 160
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Tratamento (Sr., Empresa):'
        end
        object Label28: TLabel
          Left = 15
          Top = 212
          Width = 213
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Representante legal do contratante:'
        end
        object SbRepreLegal: TSpeedButton
          Left = 396
          Top = 231
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SbRepreLegalClick
        end
        object Label29: TLabel
          Left = 423
          Top = 212
          Width = 88
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Testemunha 1:'
        end
        object SbTestemun01: TSpeedButton
          Left = 800
          Top = 231
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SbTestemun01Click
        end
        object Label30: TLabel
          Left = 827
          Top = 212
          Width = 88
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Testemunha 2:'
        end
        object SbTestemun02: TSpeedButton
          Left = 1199
          Top = 231
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SbTestemun02Click
        end
        object Label31: TLabel
          Left = 108
          Top = 315
          Width = 50
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% Multa:'
        end
        object Label9: TLabel
          Left = 15
          Top = 315
          Width = 82
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Valor mensal:'
        end
        object Label32: TLabel
          Left = 202
          Top = 315
          Width = 79
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '% Mora/m'#234's:'
        end
        object Label33: TLabel
          Left = 297
          Top = 315
          Width = 120
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Tipo de documento:'
        end
        object Label34: TLabel
          Left = 464
          Top = 315
          Width = 52
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Dia vcto:'
        end
        object Label55: TLabel
          Left = 527
          Top = 315
          Width = 307
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Tipo de estabelecimento (resid'#234'ncia, ind'#250'stria, etc):'
        end
        object Label57: TLabel
          Left = 15
          Top = 364
          Width = 194
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Escopo dos servi'#231'os prestados:'
        end
        object Label59: TLabel
          Left = 910
          Top = 98
          Width = 120
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Pr'#243'xima renova'#231#227'o:'
        end
        object EdCodigo: TdmkEdit
          Left = 15
          Top = 20
          Width = 69
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdContratada: TdmkEditCB
          Left = 222
          Top = 20
          Width = 68
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Contratada'
          UpdCampo = 'Contratada'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBContratada
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBContratada: TdmkDBLookupComboBox
          Left = 290
          Top = 20
          Width = 370
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'NO_ENT'
          ListSource = DsContratada
          TabOrder = 3
          dmkEditCB = EdContratada
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdContratante: TdmkEditCB
          Left = 689
          Top = 20
          Width = 69
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Contratante'
          UpdCampo = 'Contratante'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBContratante
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBContratante: TdmkDBLookupComboBox
          Left = 758
          Top = 20
          Width = 440
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'NO_ENT'
          ListSource = DsContratante
          TabOrder = 5
          dmkEditCB = EdContratante
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNome: TdmkEdit
          Left = 15
          Top = 69
          Width = 469
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object TPDataContrato: TdmkEditDateTimePicker
          Left = 15
          Top = 118
          Width = 123
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 39844.982392210650000000
          Time = 39844.982392210650000000
          TabOrder = 9
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DContrato'
          UpdCampo = 'DContrato'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object TPDInstal: TdmkEditDateTimePicker
          Left = 654
          Top = 118
          Width = 123
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 39844.982392210650000000
          Time = 39844.982392210650000000
          TabOrder = 14
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DInstal'
          UpdCampo = 'DInstal'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object TPDVencimento: TdmkEditDateTimePicker
          Left = 782
          Top = 118
          Width = 123
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 39844.982392210650000000
          Time = 39844.982392210650000000
          TabOrder = 15
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DVencimento'
          UpdCampo = 'DVencimento'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object dmkRGStatus: TdmkRadioGroup
          Left = 271
          Top = 263
          Width = 538
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Status:'
          Columns = 5
          Items.Strings = (
            'Cadastrando'
            'N'#227'o aprovado'
            'Aprovado'
            'Renovado'
            'Rescindido')
          TabOrder = 29
          QryCampo = 'Status'
          UpdCampo = 'Status'
          UpdType = utYes
          OldValor = 0
        end
        object dmkCkAtivo: TdmkCheckBox
          Left = 1019
          Top = 332
          Width = 55
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ativo'
          TabOrder = 36
          Visible = False
          QryCampo = 'Ativo'
          UpdCampo = 'Ativo'
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object CkContinuar: TCheckBox
          Left = 1087
          Top = 332
          Width = 138
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Continuar inserindo.'
          TabOrder = 37
        end
        object EdWinDocArq: TdmkEdit
          Left = 679
          Top = 69
          Width = 519
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 8
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'WinDocArq'
          UpdCampo = 'WinDocArq'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object RGFormaUso: TdmkRadioGroup
          Left = 817
          Top = 261
          Width = 405
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Forma de Uso: '
          Columns = 4
          Items.Strings = (
            'Nenhum'
            'Texto BD'
            'MS Word'
            'Fast Report')
          TabOrder = 30
          QryCampo = 'FormaUso'
          UpdCampo = 'FormaUso'
          UpdType = utYes
          OldValor = 0
        end
        object EdVersao: TdmkEdit
          Left = 89
          Top = 20
          Width = 128
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'Versao'
          UpdCampo = 'Versao'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object TPDtaPropIni: TdmkEditDateTimePicker
          Left = 142
          Top = 118
          Width = 123
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 39844.982392210650000000
          Time = 39844.982392210650000000
          TabOrder = 10
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtaPropIni'
          UpdCampo = 'DtaPropIni'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object TPDtaPropFim: TdmkEditDateTimePicker
          Left = 270
          Top = 118
          Width = 123
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 39844.982392210650000000
          Time = 39844.982392210650000000
          TabOrder = 11
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtaPropFim'
          UpdCampo = 'DtaPropFim'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object TPDtaImprime: TdmkEditDateTimePicker
          Left = 398
          Top = 118
          Width = 123
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 39844.982392210650000000
          Time = 39844.982392210650000000
          TabOrder = 12
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtaImprime'
          UpdCampo = 'DtaImprime'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object TPDtaAssina: TdmkEditDateTimePicker
          Left = 526
          Top = 118
          Width = 123
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 39844.982392210650000000
          Time = 39844.982392210650000000
          TabOrder = 13
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtaAssina'
          UpdCampo = 'DtaAssina'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object TPDtaCntrFim: TdmkEditDateTimePicker
          Left = 1038
          Top = 118
          Width = 123
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 39844.982392210650000000
          Time = 39844.982392210650000000
          TabOrder = 17
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtaCntrFim'
          UpdCampo = 'DtaCntrFim'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdTratamento: TdmkEdit
          Left = 487
          Top = 69
          Width = 189
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Tratamento'
          UpdCampo = 'Tratamento'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object RGArtigoDef: TdmkRadioGroup
          Left = 15
          Top = 261
          Width = 106
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'G'#234'nero cliente:'
          Columns = 2
          Items.Strings = (
            'M'
            'F')
          TabOrder = 28
          QryCampo = 'ArtigoDef'
          UpdCampo = 'ArtigoDef'
          UpdType = utYes
          OldValor = 0
        end
        object GroupBox1: TGroupBox
          Left = 15
          Top = 148
          Width = 298
          Height = 60
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Prazo in'#237'cio implementa'#231#227'o:'
          TabOrder = 18
          object Label26: TLabel
            Left = 235
            Top = 10
            Width = 29
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Qtd*:'
          end
          object RGPerDefIniI: TdmkRadioGroup
            Left = 2
            Top = 18
            Width = 228
            Height = 40
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            Caption = ' Grandeza: '
            Columns = 4
            Items.Strings = (
              'Dia'
              'Sem'
              'M'#234's'
              'Ano')
            TabOrder = 0
            QryCampo = 'PerDefIniI'
            UpdCampo = 'PerDefIniI'
            UpdType = utYes
            OldValor = 0
          end
          object EdPerQtdIniI: TdmkEdit
            Left = 236
            Top = 30
            Width = 50
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PerQtdIniI'
            UpdCampo = 'PerQtdIniI'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object EdRepreLegal: TdmkEditCB
          Left = 15
          Top = 231
          Width = 69
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 22
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'RepreLegal'
          UpdCampo = 'RepreLegal'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBRepreLegal
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBRepreLegal: TdmkDBLookupComboBox
          Left = 84
          Top = 231
          Width = 311
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'NO_ENT'
          ListSource = DsRepreLegal
          TabOrder = 23
          dmkEditCB = EdRepreLegal
          QryCampo = 'RepreLegal'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdTestemun01: TdmkEditCB
          Left = 423
          Top = 231
          Width = 69
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 24
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Testemun01'
          UpdCampo = 'Testemun01'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBTestemun01
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBTestemun01: TdmkDBLookupComboBox
          Left = 492
          Top = 231
          Width = 306
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'NO_ENT'
          ListSource = DsTestemun01
          TabOrder = 25
          dmkEditCB = EdTestemun01
          QryCampo = 'Testemun01'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdTestemun02: TdmkEditCB
          Left = 827
          Top = 231
          Width = 69
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 26
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Testemun02'
          UpdCampo = 'Testemun02'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBTestemun02
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBTestemun02: TdmkDBLookupComboBox
          Left = 896
          Top = 231
          Width = 297
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'NO_ENT'
          ListSource = DsTestemun02
          TabOrder = 27
          dmkEditCB = EdTestemun02
          QryCampo = 'Testemun02'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdPercMulta: TdmkEdit
          Left = 107
          Top = 335
          Width = 89
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 32
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'PercMulta'
          UpdCampo = 'PercMulta'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdValorAluguel: TdmkEdit
          Left = 15
          Top = 335
          Width = 88
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 31
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValorMes'
          UpdCampo = 'ValorMes'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdPercJuroM: TdmkEdit
          Left = 201
          Top = 335
          Width = 88
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 33
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'PercJuroM'
          UpdCampo = 'PercJuroM'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdTpPagDocu: TdmkEdit
          Left = 295
          Top = 335
          Width = 164
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 34
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'TpPagDocu'
          UpdCampo = 'TpPagDocu'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdddMesVcto: TdmkEdit
          Left = 463
          Top = 335
          Width = 60
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 35
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '1'
          ValMax = '31'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          QryCampo = 'ddMesVcto'
          UpdCampo = 'ddMesVcto'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
        end
        object GroupBox2: TGroupBox
          Left = 318
          Top = 148
          Width = 297
          Height = 60
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Tempo de Implem. / desenvolvim.:'
          TabOrder = 19
          object Label35: TLabel
            Left = 235
            Top = 10
            Width = 29
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Qtd*:'
          end
          object RGPerDefImpl: TdmkRadioGroup
            Left = 2
            Top = 18
            Width = 228
            Height = 40
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            Caption = ' Grandeza: '
            Columns = 4
            Items.Strings = (
              'Dia'
              'Sem'
              'M'#234's'
              'Ano')
            TabOrder = 0
            QryCampo = 'PerDefImpl'
            UpdCampo = 'PerDefImpl'
            UpdType = utYes
            OldValor = 0
          end
          object EdPerQtdImpl: TdmkEdit
            Left = 236
            Top = 30
            Width = 50
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PerQtdImpl'
            UpdCampo = 'PerQtdImpl'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object GroupBox3: TGroupBox
          Left = 622
          Top = 148
          Width = 297
          Height = 60
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Tempo de treinamento:'
          TabOrder = 20
          object Label36: TLabel
            Left = 235
            Top = 10
            Width = 29
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Qtd*:'
          end
          object RGPerDefTrei: TdmkRadioGroup
            Left = 2
            Top = 18
            Width = 228
            Height = 40
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            Caption = ' Grandeza: '
            Columns = 4
            Items.Strings = (
              'Dia'
              'Sem'
              'M'#234's'
              'Ano')
            TabOrder = 0
            QryCampo = 'PerDefTrei'
            UpdCampo = 'PerDefTrei'
            UpdType = utYes
            OldValor = 0
          end
          object EdPerQtdTrei: TdmkEdit
            Left = 236
            Top = 30
            Width = 50
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PerQtdTrei'
            UpdCampo = 'PerQtdTrei'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object GroupBox4: TGroupBox
          Left = 926
          Top = 148
          Width = 297
          Height = 60
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Tempo de monitoramento:'
          TabOrder = 21
          object Label37: TLabel
            Left = 235
            Top = 10
            Width = 29
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Qtd*:'
          end
          object RGPerDefMoni: TdmkRadioGroup
            Left = 2
            Top = 18
            Width = 228
            Height = 40
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            Caption = ' Grandeza: '
            Columns = 4
            Items.Strings = (
              'Dia'
              'Sem'
              'M'#234's'
              'Ano')
            TabOrder = 0
            QryCampo = 'PerDefMoni'
            UpdCampo = 'PerDefMoni'
            UpdType = utYes
            OldValor = 0
          end
          object EdPerQtdMoni: TdmkEdit
            Left = 236
            Top = 30
            Width = 50
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PerQtdMoni'
            UpdCampo = 'PerQtdMoni'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object RGLugArtDef: TdmkRadioGroup
          Left = 123
          Top = 261
          Width = 144
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'G'#234'nero estabelecim.:'
          Columns = 2
          Items.Strings = (
            'M'
            'F')
          TabOrder = 38
          QryCampo = 'LugArtDef'
          UpdCampo = 'LugArtDef'
          UpdType = utYes
          OldValor = 0
        end
        object EdLugarNome: TdmkEdit
          Left = 527
          Top = 335
          Width = 483
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 39
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'LugarNome'
          UpdCampo = 'LugarNome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEscopoSP: TdmkEdit
          Left = 15
          Top = 384
          Width = 1206
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 40
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'EscopoSP'
          UpdCampo = 'EscopoSP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object TPDtaPrxRenw: TdmkEditDateTimePicker
          Left = 910
          Top = 118
          Width = 123
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Date = 39844.982392210650000000
          Time = 39844.982392210650000000
          TabOrder = 16
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtaPrxRenw'
          UpdCampo = 'DtaPrxRenw'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 118
    Width = 1241
    Height = 697
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 618
      Width = 1241
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 35
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 476
        Top = 18
        Width = 763
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtContrato: TBitBtn
          Tag = 359
          Left = 5
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Contrato'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtContratoClick
        end
        object Panel2: TPanel
          Left = 629
          Top = 0
          Width = 134
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtTexto: TBitBtn
          Tag = 121
          Left = 118
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Texto'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtTextoClick
        end
        object BtTags: TBitBtn
          Tag = 494
          Left = 231
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ta&gs'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtTagsClick
        end
        object BtLocais: TBitBtn
          Tag = 280
          Left = 345
          Top = 5
          Width = 110
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Locais'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtLocaisClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 1241
      Height = 582
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GBDados: TGroupBox
          Left = 0
          Top = 0
          Width = 1233
          Height = 587
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          object Panel6: TPanel
            Left = 2
            Top = 18
            Width = 1229
            Height = 416
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label1: TLabel
              Left = 15
              Top = 0
              Width = 16
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'ID:'
              Color = clBtnFace
              ParentColor = False
            end
            object Label2: TLabel
              Left = 90
              Top = 0
              Width = 117
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Vers'#227'o do contrato:'
            end
            object Label12: TLabel
              Left = 222
              Top = 0
              Width = 69
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Contratada:'
            end
            object Label15: TLabel
              Left = 689
              Top = 0
              Width = 71
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Contratante:'
            end
            object Label16: TLabel
              Left = 15
              Top = 49
              Width = 106
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'T'#237'tulo do contrato:'
            end
            object Label18: TLabel
              Left = 489
              Top = 49
              Width = 160
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Tratamento (Sr., Empresa):'
            end
            object Label19: TLabel
              Left = 655
              Top = 49
              Width = 269
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Arquivo base do contrato em Windows Word:'
            end
            object Label20: TLabel
              Left = 15
              Top = 98
              Width = 83
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Data contrato:'
            end
            object Label21: TLabel
              Left = 143
              Top = 98
              Width = 91
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'In'#237'cio proposta:'
            end
            object Label27: TLabel
              Left = 270
              Top = 98
              Width = 95
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Limite proposta:'
            end
            object Label38: TLabel
              Left = 398
              Top = 98
              Width = 99
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Data impress'#227'o:'
            end
            object Label39: TLabel
              Left = 526
              Top = 98
              Width = 97
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Data assinatura:'
            end
            object Label40: TLabel
              Left = 655
              Top = 98
              Width = 88
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'In'#237'cio vig'#234'ncia:'
            end
            object Label41: TLabel
              Left = 782
              Top = 98
              Width = 87
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '1'#186' vencimento:'
            end
            object Label42: TLabel
              Left = 914
              Top = 98
              Width = 120
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Pr'#243'xima renova'#231#227'o:'
            end
            object Label43: TLabel
              Left = 15
              Top = 212
              Width = 125
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Representante legal:'
            end
            object Label44: TLabel
              Left = 423
              Top = 212
              Width = 88
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Testemunha 1:'
            end
            object Label45: TLabel
              Left = 827
              Top = 212
              Width = 88
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Testemunha 2:'
            end
            object Label46: TLabel
              Left = 15
              Top = 315
              Width = 82
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Valor mensal:'
            end
            object Label51: TLabel
              Left = 108
              Top = 315
              Width = 50
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '% Multa:'
            end
            object Label52: TLabel
              Left = 202
              Top = 315
              Width = 79
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '% Mora/m'#234's:'
            end
            object Label53: TLabel
              Left = 297
              Top = 315
              Width = 120
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Tipo de documento:'
            end
            object Label54: TLabel
              Left = 464
              Top = 315
              Width = 52
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Dia vcto:'
            end
            object Label56: TLabel
              Left = 527
              Top = 315
              Width = 307
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Tipo de estabelecimento (resid'#234'ncia, ind'#250'stria, etc):'
            end
            object Label58: TLabel
              Left = 15
              Top = 364
              Width = 194
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Escopo dos servi'#231'os prestados:'
            end
            object Label60: TLabel
              Left = 1042
              Top = 98
              Width = 87
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Data rescis'#227'o:'
            end
            object dmkDBEdit1: TdmkDBEdit
              Left = 15
              Top = 20
              Width = 69
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Codigo'
              DataSource = DsContratos
              TabOrder = 0
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit3: TdmkDBEdit
              Left = 290
              Top = 20
              Width = 396
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'NCONTRATADA'
              DataSource = DsContratos
              TabOrder = 1
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object CkAtivo: TDBCheckBox
              Left = 1161
              Top = 335
              Width = 56
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Ativo.'
              DataField = 'Ativo'
              DataSource = DsContratos
              TabOrder = 2
              ValueChecked = '1'
              ValueUnchecked = '0'
              Visible = False
            end
            object dmkDBEdit7: TdmkDBEdit
              Left = 15
              Top = 118
              Width = 123
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'DContrato_TXT'
              DataSource = DsContratos
              TabOrder = 3
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit6: TdmkDBEdit
              Left = 143
              Top = 118
              Width = 123
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'DtaPropIni_TXT'
              DataSource = DsContratos
              TabOrder = 4
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit8: TdmkDBEdit
              Left = 270
              Top = 118
              Width = 123
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'DtaPropFim_TXT'
              DataSource = DsContratos
              TabOrder = 5
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit9: TdmkDBEdit
              Left = 15
              Top = 335
              Width = 87
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'ValorMes'
              DataSource = DsContratos
              TabOrder = 6
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit4: TdmkDBEdit
              Left = 758
              Top = 20
              Width = 464
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'NCONTRATANTE'
              DataSource = DsContratos
              TabOrder = 7
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit2: TdmkDBEdit
              Left = 15
              Top = 69
              Width = 474
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Nome'
              DataSource = DsContratos
              TabOrder = 8
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object DBEdit1: TDBEdit
              Left = 655
              Top = 69
              Width = 567
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'WinDocArq'
              DataSource = DsContratos
              TabOrder = 9
            end
            object DBRadioGroup1: TDBRadioGroup
              Left = 817
              Top = 261
              Width = 405
              Height = 49
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Forma de Uso: '
              Columns = 4
              DataField = 'FormaUso'
              DataSource = DsContratos
              Items.Strings = (
                'Nenhum'
                'Texto BD'
                'MS Word'
                'Fast Report')
              TabOrder = 10
              Values.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7'
                '8'
                '9')
            end
            object DBEdit2: TDBEdit
              Left = 89
              Top = 20
              Width = 128
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Versao'
              DataSource = DsContratos
              TabOrder = 11
            end
            object DBEdit3: TDBEdit
              Left = 492
              Top = 69
              Width = 159
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Tratamento'
              DataSource = DsContratos
              TabOrder = 12
            end
            object GroupBox5: TGroupBox
              Left = 15
              Top = 148
              Width = 298
              Height = 60
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Prazo in'#237'cio implementa'#231#227'o:'
              TabOrder = 13
              object Label47: TLabel
                Left = 235
                Top = 10
                Width = 29
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Qtd*:'
              end
              object dmkRadioGroup1: TDBRadioGroup
                Left = 2
                Top = 18
                Width = 228
                Height = 40
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' Grandeza: '
                Columns = 4
                DataField = 'PerDefIniI'
                DataSource = DsContratos
                Items.Strings = (
                  'Dia'
                  'Sem'
                  'M'#234's'
                  'Ano')
                TabOrder = 0
                Values.Strings = (
                  '0'
                  '1'
                  '2'
                  '3'
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9')
              end
              object dmkDBEdit21: TdmkDBEdit
                Left = 235
                Top = 28
                Width = 53
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'PerQtdIniI'
                DataSource = DsContratos
                TabOrder = 1
                UpdType = utYes
                Alignment = taLeftJustify
              end
            end
            object GroupBox6: TGroupBox
              Left = 315
              Top = 148
              Width = 298
              Height = 60
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Tempo de Implem. / desenvolvim.:'
              TabOrder = 14
              object Label48: TLabel
                Left = 235
                Top = 10
                Width = 29
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Qtd*:'
              end
              object dmkRadioGroup2: TDBRadioGroup
                Left = 2
                Top = 18
                Width = 228
                Height = 40
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' Grandeza: '
                Columns = 4
                DataField = 'PerDefImpl'
                DataSource = DsContratos
                Items.Strings = (
                  'Dia'
                  'Sem'
                  'M'#234's'
                  'Ano')
                TabOrder = 0
                Values.Strings = (
                  '0'
                  '1'
                  '2'
                  '3'
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9')
              end
              object dmkDBEdit22: TdmkDBEdit
                Left = 235
                Top = 28
                Width = 53
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'PerQtdImpl'
                DataSource = DsContratos
                TabOrder = 1
                UpdType = utYes
                Alignment = taLeftJustify
              end
            end
            object GroupBox7: TGroupBox
              Left = 620
              Top = 148
              Width = 298
              Height = 60
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Tempo de treinamento:'
              TabOrder = 15
              object Label49: TLabel
                Left = 235
                Top = 10
                Width = 29
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Qtd*:'
              end
              object dmkRadioGroup3: TDBRadioGroup
                Left = 2
                Top = 18
                Width = 228
                Height = 40
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' Grandeza: '
                Columns = 4
                DataField = 'PerDefTrei'
                DataSource = DsContratos
                Items.Strings = (
                  'Dia'
                  'Sem'
                  'M'#234's'
                  'Ano')
                TabOrder = 0
                Values.Strings = (
                  '0'
                  '1'
                  '2'
                  '3'
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9')
              end
              object dmkDBEdit23: TdmkDBEdit
                Left = 235
                Top = 28
                Width = 53
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'PerQtdTrei'
                DataSource = DsContratos
                TabOrder = 1
                UpdType = utYes
                Alignment = taLeftJustify
              end
            end
            object GroupBox8: TGroupBox
              Left = 926
              Top = 148
              Width = 297
              Height = 60
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Tempo de monitoramento:'
              TabOrder = 16
              object Label50: TLabel
                Left = 235
                Top = 10
                Width = 29
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Qtd*:'
              end
              object dmkRadioGroup4: TDBRadioGroup
                Left = 2
                Top = 18
                Width = 228
                Height = 40
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' Grandeza: '
                Columns = 4
                DataField = 'PerDefMoni'
                DataSource = DsContratos
                Items.Strings = (
                  'Dia'
                  'Sem'
                  'M'#234's'
                  'Ano')
                TabOrder = 0
                Values.Strings = (
                  '0'
                  '1'
                  '2'
                  '3'
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9')
              end
              object dmkDBEdit24: TdmkDBEdit
                Left = 235
                Top = 28
                Width = 53
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DataField = 'PerQtdMoni'
                DataSource = DsContratos
                TabOrder = 1
                UpdType = utYes
                Alignment = taLeftJustify
              end
            end
            object dmkDBEdit5: TdmkDBEdit
              Left = 398
              Top = 118
              Width = 123
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'DtaImprime_TXT'
              DataSource = DsContratos
              TabOrder = 17
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit10: TdmkDBEdit
              Left = 526
              Top = 118
              Width = 123
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'DtaAssina_TXT'
              DataSource = DsContratos
              TabOrder = 18
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit11: TdmkDBEdit
              Left = 655
              Top = 118
              Width = 123
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'DInstal_TXT'
              DataSource = DsContratos
              TabOrder = 19
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit12: TdmkDBEdit
              Left = 782
              Top = 118
              Width = 123
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'DVencimento_TXT'
              DataSource = DsContratos
              TabOrder = 20
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit13: TdmkDBEdit
              Left = 914
              Top = 118
              Width = 124
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'DtaPrxRenw_TXT'
              DataSource = DsContratos
              TabOrder = 21
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object DBRadioGroup2: TDBRadioGroup
              Left = 15
              Top = 261
              Width = 106
              Height = 49
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Genero: '
              Columns = 2
              DataField = 'ArtigoDef'
              DataSource = DsContratos
              Items.Strings = (
                'M'
                'F')
              TabOrder = 23
              Values.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7'
                '8'
                '9')
            end
            object DBRGStatus: TDBRadioGroup
              Left = 271
              Top = 261
              Width = 538
              Height = 49
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Status ( altera'#231#227'o manual!): '
              Columns = 5
              DataField = 'Status'
              DataSource = DsContratos
              Items.Strings = (
                'Cadastrando'
                'N'#227'o aprovado'
                'Aprovado'
                'Renovado'
                'Rescindido')
              TabOrder = 24
              Values.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7'
                '8'
                '9')
            end
            object dmkDBEdit14: TdmkDBEdit
              Left = 106
              Top = 335
              Width = 87
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'PercMulta'
              DataSource = DsContratos
              TabOrder = 25
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit15: TdmkDBEdit
              Left = 199
              Top = 335
              Width = 88
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'PercJuroM'
              DataSource = DsContratos
              TabOrder = 26
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit16: TdmkDBEdit
              Left = 298
              Top = 335
              Width = 161
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'TpPagDocu'
              DataSource = DsContratos
              TabOrder = 27
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit17: TdmkDBEdit
              Left = 465
              Top = 335
              Width = 53
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'ddMesVcto'
              DataSource = DsContratos
              TabOrder = 28
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit18: TdmkDBEdit
              Left = 84
              Top = 231
              Width = 334
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'NO_REP_LEGAL'
              DataSource = DsContratos
              TabOrder = 29
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit19: TdmkDBEdit
              Left = 492
              Top = 231
              Width = 330
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'NO_TESTEM_01'
              DataSource = DsContratos
              TabOrder = 30
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object dmkDBEdit20: TdmkDBEdit
              Left = 896
              Top = 231
              Width = 327
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'NO_TESTEM_02'
              DataSource = DsContratos
              TabOrder = 31
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object DBRadioGroup4: TDBRadioGroup
              Left = 123
              Top = 261
              Width = 144
              Height = 49
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'G'#234'nero estabelecim.:'
              Columns = 2
              DataField = 'LugArtDef'
              DataSource = DsContratos
              Items.Strings = (
                'M'
                'F')
              TabOrder = 32
              Values.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7'
                '8'
                '9')
            end
            object DBEdit4: TDBEdit
              Left = 527
              Top = 335
              Width = 626
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'LugarNome'
              DataSource = DsContratos
              TabOrder = 33
            end
            object DBEdit5: TDBEdit
              Left = 15
              Top = 384
              Width = 1206
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'EscopoSP'
              DataSource = DsContratos
              TabOrder = 34
            end
            object dmkDBEdit25: TdmkDBEdit
              Left = 1042
              Top = 118
              Width = 124
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'DtaCntrFim_TXT'
              DataSource = DsContratos
              TabOrder = 22
              UpdType = utYes
              Alignment = taLeftJustify
            end
            object DBEdit7: TDBEdit
              Left = 689
              Top = 20
              Width = 69
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Contratante'
              DataSource = DsContratos
              TabOrder = 35
            end
            object DBEdit6: TDBEdit
              Left = 222
              Top = 20
              Width = 68
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Contratada'
              DataSource = DsContratos
              TabOrder = 36
            end
            object DBEdit8: TDBEdit
              Left = 15
              Top = 231
              Width = 69
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'RepreLegal'
              DataSource = DsContratos
              TabOrder = 37
            end
            object DBEdit9: TDBEdit
              Left = 423
              Top = 231
              Width = 69
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Testemun01'
              DataSource = DsContratos
              TabOrder = 38
            end
            object DBEdit10: TDBEdit
              Left = 827
              Top = 231
              Width = 69
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Testemun02'
              DataSource = DsContratos
              TabOrder = 39
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Texto em BD'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object RECarta: TRichEdit
          Left = 0
          Top = 59
          Width = 1233
          Height = 492
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Lines.Strings = (
            'RichEdit1')
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          Zoom = 100
        end
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 1233
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          ParentBackground = False
          TabOrder = 1
          object BtLocaliza: TBitBtn
            Tag = 277
            Left = 5
            Top = 5
            Width = 49
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtLocalizaClick
          end
        end
      end
      object TabSheet7: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tags e locais'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl2: TPageControl
          Left = 0
          Top = 0
          Width = 1233
          Height = 551
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet5
          Align = alClient
          TabOrder = 0
          object TabSheet5: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Tags de cadastros (pragas e servi'#231'os) '
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GroupBox9: TGroupBox
              Left = 0
              Top = 0
              Width = 369
              Height = 520
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              Caption = 'Pragas inclusas: '
              TabOrder = 0
              object DBGOSCabAlv: TDBGrid
                Left = 2
                Top = 18
                Width = 365
                Height = 500
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsContrBugsS
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NO_NIVEL'
                    Title.Caption = 'N'
                    Width = 18
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRAGA'
                    Title.Caption = 'Praga'
                    Width = 240
                    Visible = True
                  end>
              end
            end
            object GroupBox10: TGroupBox
              Left = 369
              Top = 0
              Width = 369
              Height = 520
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              Caption = ' Pragas exclusas:  '
              TabOrder = 1
              object DBGrid2: TDBGrid
                Left = 2
                Top = 18
                Width = 365
                Height = 500
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsContrBugsN
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NO_NIVEL'
                    Title.Caption = 'N'
                    Width = 18
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PRAGA'
                    Title.Caption = 'Praga'
                    Width = 240
                    Visible = True
                  end>
              end
            end
            object GroupBox11: TGroupBox
              Left = 738
              Top = 0
              Width = 487
              Height = 520
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Caption = ' Servi'#231'os oferecidos:  '
              TabOrder = 2
              object DBGrid3: TDBGrid
                Left = 2
                Top = 18
                Width = 483
                Height = 500
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsContrServi
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NO_SERVICO'
                    Title.Caption = 'Servi'#231'o'
                    Width = 343
                    Visible = True
                  end>
              end
            end
          end
          object TabSheet4: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Tags em N'#237'veis '
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
          end
          object TabSheet3: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tags de Grupos e Tags Personalizadas'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid1: TDBGrid
              Left = 0
              Top = 0
              Width = 599
              Height = 520
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              DataSource = DsContratTag
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Tag'
                  Width = 128
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Coment'#225'rio'
                  Width = 258
                  Visible = True
                end>
            end
            object DBMemo1: TDBMemo
              Left = 599
              Top = 0
              Width = 626
              Height = 520
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataField = 'Text'
              DataSource = DsContratTag
              TabOrder = 1
            end
          end
          object TabSheet6: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Locais Contemplados'
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid4: TDBGrid
              Left = 0
              Top = 0
              Width = 1225
              Height = 520
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsContratLoc
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Cliente'
                  Title.Caption = 'Entidade'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_ENT'
                  Title.Caption = 'Nome da Entidade'
                  Width = 356
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_LUGAR'
                  Title.Caption = 'Lugar ou tipo de objeto'
                  Width = 305
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_LOCAL'
                  Title.Caption = 'Local ou descri'#231#227'o do objeto'
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 266
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 266
      Top = 0
      Width = 916
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 139
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Contratos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 139
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Contratos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 139
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Contratos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 64
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrContratos: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrContratosBeforeOpen
    AfterOpen = QrContratosAfterOpen
    BeforeClose = QrContratosBeforeClose
    AfterScroll = QrContratosAfterScroll
    OnCalcFields = QrContratosCalcFields
    SQL.Strings = (
      'SELECT con.*,'
      
        'IF(DContrato < "1900-01-01", "", DATE_FORMAT(DContrato, "%d/%m/%' +
        'Y")) DContrato_TXT,'
      
        'IF(DInstal< "1900-01-01", "", DATE_FORMAT(DInstal, "%d/%m/%Y")) ' +
        'DInstal_TXT,'
      
        'IF(DVencimento< "1900-01-01", "", DATE_FORMAT(DVencimento, "%d/%' +
        'm/%Y")) DVencimento_TXT,'
      
        'IF(DtaPropIni< "1900-01-01", "", DATE_FORMAT(DtaPropIni, "%d/%m/' +
        '%Y")) DtaPropIni_TXT,'
      
        'IF(DtaPropFim< "1900-01-01", "", DATE_FORMAT(DtaPropFim, "%d/%m/' +
        '%Y")) DtaPropFim_TXT,'
      
        'IF(DtaCntrFim< "1900-01-01", "", DATE_FORMAT(DtaCntrFim, "%d/%m/' +
        '%Y")) DtaCntrFim_TXT,'
      
        'IF(DtaAssina< "1900-01-01", "", DATE_FORMAT(DtaAssina, "%d/%m/%Y' +
        '")) DtaAssina_TXT,'
      
        'IF(DtaPrxRenw< "1900-01-01", "", DATE_FORMAT(DtaPrxRenw, "%d/%m/' +
        '%Y")) DtaPrxRenw_TXT,'
      
        'IF(DtaImprime < "1900-01-01", "", DATE_FORMAT(DtaImprime, "%d/%m' +
        '/%Y %h:%m:%s")) DtaImprime_TXT,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NCONTRATADA,'
      'IF(enb.Tipo=0, enb.RazaoSocial, enb.Nome) NCONTRATANTE,'
      'IF(rep.Tipo=0, rep.RazaoSocial, rep.Nome) NO_REP_LEGAL,'
      'IF(ts1.Tipo=0, ts1.RazaoSocial, ts1.Nome) NO_TESTEM_01,'
      'IF(ts2.Tipo=0, ts2.RazaoSocial, ts2.Nome) NO_TESTEM_02'
      'FROM contratos con'
      'LEFT JOIN entidades ent ON ent.Codigo = con.Contratada'
      'LEFT JOIN entidades enb ON enb.Codigo = con.Contratante'
      'LEFT JOIN entidades rep ON rep.Codigo = con.RepreLegal'
      'LEFT JOIN entidades ts1 ON ts1.Codigo = con.Testemun01'
      'LEFT JOIN entidades ts2 ON ts2.Codigo = con.Testemun02'
      'WHERE con.Codigo <> 0'
      'AND con.Ativo = 1'
      '')
    Left = 416
    Top = 56
    object QrContratosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contratos.Codigo'
    end
    object QrContratosContratada: TIntegerField
      FieldName = 'Contratada'
      Origin = 'contratos.Contratada'
    end
    object QrContratosContratante: TIntegerField
      FieldName = 'Contratante'
      Origin = 'contratos.Contratante'
    end
    object QrContratosDContrato: TDateField
      FieldName = 'DContrato'
      Origin = 'contratos.DContrato'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosDInstal: TDateField
      FieldName = 'DInstal'
      Origin = 'contratos.DInstal'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosDVencimento: TDateField
      FieldName = 'DVencimento'
      Origin = 'contratos.DVencimento'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosStatus: TSmallintField
      FieldName = 'Status'
      Origin = 'contratos.Status'
    end
    object QrContratosAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'contratos.Ativo'
    end
    object QrContratosValorMes: TFloatField
      FieldName = 'ValorMes'
      Origin = 'contratos.ValorMes'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrContratosNCONTRATADA: TWideStringField
      FieldName = 'NCONTRATADA'
      Size = 100
    end
    object QrContratosSTATUSSTR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'STATUSSTR'
      Size = 50
      Calculated = True
    end
    object QrContratosNCONTRATANTE: TWideStringField
      FieldName = 'NCONTRATANTE'
      Size = 100
    end
    object QrContratosWinDocArq: TWideStringField
      FieldName = 'WinDocArq'
      Origin = 'contratos.WinDocArq'
      Size = 255
    end
    object QrContratosFormaUso: TSmallintField
      FieldName = 'FormaUso'
      Origin = 'contratos.FormaUso'
    end
    object QrContratosNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contratos.Nome'
      Size = 255
    end
    object QrContratosDtaPropIni: TDateField
      FieldName = 'DtaPropIni'
      Origin = 'contratos.DtaPropIni'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosDtaPropFim: TDateField
      FieldName = 'DtaPropFim'
      Origin = 'contratos.DtaPropFim'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosDtaPrxRenw: TDateField
      FieldName = 'DtaPrxRenw'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosDtaCntrFim: TDateField
      FieldName = 'DtaCntrFim'
      Origin = 'contratos.DtaCntrFim'
      OnGetText = QrContratosDtaCntrFimGetText
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosDtaImprime: TDateTimeField
      FieldName = 'DtaImprime'
      Origin = 'contratos.DtaImprime'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosDtaAssina: TDateField
      FieldName = 'DtaAssina'
      Origin = 'contratos.DtaAssina'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosTratamento: TWideStringField
      FieldName = 'Tratamento'
      Origin = 'contratos.Tratamento'
      Size = 30
    end
    object QrContratosArtigoDef: TSmallintField
      FieldName = 'ArtigoDef'
      Origin = 'contratos.ArtigoDef'
    end
    object QrContratosPerDefImpl: TSmallintField
      FieldName = 'PerDefImpl'
      Origin = 'contratos.PerDefImpl'
    end
    object QrContratosPerQtdImpl: TSmallintField
      FieldName = 'PerQtdImpl'
      Origin = 'contratos.PerQtdImpl'
    end
    object QrContratosPerDefTrei: TSmallintField
      FieldName = 'PerDefTrei'
      Origin = 'contratos.PerDefTrei'
    end
    object QrContratosPerQtdTrei: TSmallintField
      FieldName = 'PerQtdTrei'
      Origin = 'contratos.PerQtdTrei'
    end
    object QrContratosPerDefMoni: TSmallintField
      FieldName = 'PerDefMoni'
      Origin = 'contratos.PerDefMoni'
    end
    object QrContratosPerQtdMoni: TSmallintField
      FieldName = 'PerQtdMoni'
      Origin = 'contratos.PerQtdMoni'
    end
    object QrContratosPerDefIniI: TSmallintField
      FieldName = 'PerDefIniI'
      Origin = 'contratos.PerDefIniI'
    end
    object QrContratosPerQtdIniI: TSmallintField
      FieldName = 'PerQtdIniI'
      Origin = 'contratos.PerQtdIniI'
    end
    object QrContratosRepreLegal: TIntegerField
      FieldName = 'RepreLegal'
      Origin = 'contratos.RepreLegal'
    end
    object QrContratosTestemun01: TIntegerField
      FieldName = 'Testemun01'
      Origin = 'contratos.Testemun01'
    end
    object QrContratosTestemun02: TIntegerField
      FieldName = 'Testemun02'
      Origin = 'contratos.Testemun02'
    end
    object QrContratosPercMulta: TFloatField
      FieldName = 'PercMulta'
      Origin = 'contratos.PercMulta'
      DisplayFormat = '0.00'
    end
    object QrContratosPercJuroM: TFloatField
      FieldName = 'PercJuroM'
      Origin = 'contratos.PercJuroM'
      DisplayFormat = '0.00'
    end
    object QrContratosTpPagDocu: TWideStringField
      FieldName = 'TpPagDocu'
      Origin = 'contratos.TpPagDocu'
      Size = 50
    end
    object QrContratosddMesVcto: TSmallintField
      FieldName = 'ddMesVcto'
      Origin = 'contratos.ddMesVcto'
    end
    object QrContratosTexto: TWideMemoField
      FieldName = 'Texto'
      Origin = 'contratos.Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrContratosNO_REP_LEGAL: TWideStringField
      FieldName = 'NO_REP_LEGAL'
      Size = 100
    end
    object QrContratosNO_TESTEM_01: TWideStringField
      FieldName = 'NO_TESTEM_01'
      Size = 100
    end
    object QrContratosNO_TESTEM_02: TWideStringField
      FieldName = 'NO_TESTEM_02'
      Size = 100
    end
    object QrContratosLugarNome: TWideStringField
      FieldName = 'LugarNome'
      Size = 100
    end
    object QrContratosLugArtDef: TSmallintField
      FieldName = 'LugArtDef'
    end
    object QrContratosVersao: TFloatField
      FieldName = 'Versao'
    end
    object QrContratosEscopoSP: TWideStringField
      FieldName = 'EscopoSP'
      Size = 255
    end
    object QrContratosDContrato_TXT: TWideStringField
      FieldName = 'DContrato_TXT'
      Size = 10
    end
    object QrContratosDInstal_TXT: TWideStringField
      FieldName = 'DInstal_TXT'
      Size = 10
    end
    object QrContratosDVencimento_TXT: TWideStringField
      FieldName = 'DVencimento_TXT'
      Size = 10
    end
    object QrContratosDtaPropFim_TXT: TWideStringField
      FieldName = 'DtaPropFim_TXT'
      Size = 10
    end
    object QrContratosDtaPropIni_TXT: TWideStringField
      FieldName = 'DtaPropIni_TXT'
      Size = 10
    end
    object QrContratosDtaCntrFim_TXT: TWideStringField
      FieldName = 'DtaCntrFim_TXT'
      Size = 10
    end
    object QrContratosDtaAssina_TXT: TWideStringField
      FieldName = 'DtaAssina_TXT'
      Size = 10
    end
    object QrContratosDtaPrxRenw_TXT: TWideStringField
      FieldName = 'DtaPrxRenw_TXT'
      Size = 10
    end
    object QrContratosDtaImprime_TXT: TWideStringField
      FieldName = 'DtaImprime_TXT'
    end
  end
  object DsContratos: TDataSource
    DataSet = QrContratos
    Left = 444
    Top = 56
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtContrato
    Left = 472
    Top = 56
  end
  object QrEndContratada: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEndContratadaCalcFields
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Tipo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CONTRATADA,'
      'IF(ent.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,'
      'IF(ent.Tipo=0, ent.ERua, ent.PRua) NOMERUA,'
      'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 NUMERO,'
      'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) COMPL,'
      'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BAIRRO,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      'IF(ent.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,'
      'IF(ent.Tipo=0, ent.IE, ent.RG) IE_RG,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TEL,'
      'IF(ent.Tipo=0, ent.EFax, ent.PFax) FAX'
      'FROM entidades ent'
      'LEFT JOIN UFs ufe ON ufe.Codigo=ent.EUF'
      'LEFT JOIN UFs ufp ON ufp.Codigo=ent.PUF'
      'LEFT JOIN ListaLograd lle ON lle.Codigo=ent.ELograd'
      'LEFT JOIN ListaLograd llp ON llp.Codigo=ent.PLograd'
      'WHERE ent.Codigo=:P0'
      'ORDER BY CONTRATADA')
    Left = 177
    Top = 403
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEndContratadaTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrEndContratadaCONTRATADA: TWideStringField
      FieldName = 'CONTRATADA'
      Size = 100
    end
    object QrEndContratadaNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEndContratadaNOMERUA: TWideStringField
      FieldName = 'NOMERUA'
      Size = 30
    end
    object QrEndContratadaCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEndContratadaBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEndContratadaCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEndContratadaNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEndContratadaCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEndContratadaIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEndContratadaCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEndContratadaTEL: TWideStringField
      FieldName = 'TEL'
    end
    object QrEndContratadaFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEndContratadaTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Calculated = True
    end
    object QrEndContratadaFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Calculated = True
    end
    object QrEndContratadaCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Calculated = True
    end
    object QrEndContratadaNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
  end
  object QrEndContratante: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEndContratanteCalcFields
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Tipo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CONTRATANTE,'
      'IF(ent.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD,'
      'IF(ent.Tipo=0, ent.ERua, ent.PRua) NOMERUA,'
      'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 NUMERO,'
      'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) COMPL,'
      'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BAIRRO,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      'IF(ent.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF,'
      'IF(ent.Tipo=0, ent.IE, ent.RG) IE_RG,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TEL,'
      'IF(ent.Tipo=0, ent.EFax, ent.PFax) FAX'
      'FROM entidades ent'
      'LEFT JOIN UFs ufe ON ufe.Codigo=ent.EUF'
      'LEFT JOIN UFs ufp ON ufp.Codigo=ent.PUF'
      'LEFT JOIN ListaLograd lle ON lle.Codigo=ent.ELograd'
      'LEFT JOIN ListaLograd llp ON llp.Codigo=ent.PLograd'
      'WHERE ent.Codigo=:P0'
      'ORDER BY CONTRATANTE')
    Left = 233
    Top = 403
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEndContratanteCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEndContratanteTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEndContratanteNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEndContratanteNOMERUA: TWideStringField
      FieldName = 'NOMERUA'
      Size = 30
    end
    object QrEndContratanteCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEndContratanteBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEndContratanteCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEndContratanteNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEndContratanteCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEndContratanteIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEndContratanteTEL: TWideStringField
      FieldName = 'TEL'
    end
    object QrEndContratanteFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEndContratanteTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Calculated = True
    end
    object QrEndContratanteCONTRATANTE: TWideStringField
      FieldName = 'CONTRATANTE'
      Size = 100
    end
    object QrEndContratanteFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Calculated = True
    end
    object QrEndContratanteCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Calculated = True
    end
    object QrEndContratanteNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
  end
  object DsEndContratada: TDataSource
    DataSet = QrEndContratada
    Left = 205
    Top = 403
  end
  object DsEndContratante: TDataSource
    DataSet = QrEndContratante
    Left = 261
    Top = 403
  end
  object QrControle: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT con.Cidade, con.UFPadrao, ufe.Nome NOMEUFPADRAO'
      'FROM controle con'
      'LEFT JOIN UFs ufe ON ufe.Codigo=con.UFPadrao')
    Left = 289
    Top = 403
    object QrControleCidade: TWideStringField
      FieldName = 'Cidade'
      Required = True
      Size = 100
    end
    object QrControleUFPadrao: TIntegerField
      FieldName = 'UFPadrao'
      Required = True
    end
    object QrControleNOMEUFPADRAO: TWideStringField
      FieldName = 'NOMEUFPADRAO'
      Required = True
      Size = 2
    end
  end
  object DsContratante: TDataSource
    DataSet = QrContratante
    Left = 263
    Top = 431
  end
  object QrContratante: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM Entidades'
      'ORDER BY NO_ENT')
    Left = 235
    Top = 431
    object QrContratanteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContratanteNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object QrContratada: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM Entidades'
      'ORDER BY NO_ENT')
    Left = 178
    Top = 431
    object QrContratadaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContratadaNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsContratada: TDataSource
    DataSet = QrContratada
    Left = 207
    Top = 431
  end
  object frxDsContratos: TfrxDBDataset
    UserName = 'frxDsContratos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Contratada=Contratada'
      'Contratante=Contratante'
      'DContrato=DContrato'
      'DInstal=DInstal'
      'DVencimento=DVencimento'
      'Status=Status'
      'Ativo=Ativo'
      'ValorMes=ValorMes'
      'NCONTRATADA=NCONTRATADA'
      'STATUSSTR=STATUSSTR'
      'NCONTRATANTE=NCONTRATANTE'
      'WinDocArq=WinDocArq'
      'FormaUso=FormaUso'
      'Nome=Nome'
      'DtaPropIni=DtaPropIni'
      'DtaPropFim=DtaPropFim'
      'DtaPrxRenw=DtaPrxRenw'
      'DtaCntrFim=DtaCntrFim'
      'DtaImprime=DtaImprime'
      'DtaAssina=DtaAssina'
      'Tratamento=Tratamento'
      'ArtigoDef=ArtigoDef'
      'PerDefImpl=PerDefImpl'
      'PerQtdImpl=PerQtdImpl'
      'PerDefTrei=PerDefTrei'
      'PerQtdTrei=PerQtdTrei'
      'PerDefMoni=PerDefMoni'
      'PerQtdMoni=PerQtdMoni'
      'PerDefIniI=PerDefIniI'
      'PerQtdIniI=PerQtdIniI'
      'RepreLegal=RepreLegal'
      'Testemun01=Testemun01'
      'Testemun02=Testemun02'
      'PercMulta=PercMulta'
      'PercJuroM=PercJuroM'
      'TpPagDocu=TpPagDocu'
      'ddMesVcto=ddMesVcto'
      'Texto=Texto'
      'NO_REP_LEGAL=NO_REP_LEGAL'
      'NO_TESTEM_01=NO_TESTEM_01'
      'NO_TESTEM_02=NO_TESTEM_02'
      'LugarNome=LugarNome'
      'LugArtDef=LugArtDef'
      'Versao=Versao'
      'EscopoSP=EscopoSP')
    DataSet = QrContratos
    BCDToCurrency = False
    Left = 513
    Top = 11
  end
  object frxDsEndContratada: TfrxDBDataset
    UserName = 'frxDsEndContratada'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Tipo=Tipo'
      'CONTRATADA=CONTRATADA'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMERUA=NOMERUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'NOMEUF=NOMEUF'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'Codigo=Codigo'
      'TEL=TEL'
      'FAX=FAX'
      'TEL_TXT=TEL_TXT'
      'FAX_TXT=FAX_TXT'
      'CNPJ_TXT=CNPJ_TXT'
      'NUMERO=NUMERO')
    DataSet = QrEndContratada
    BCDToCurrency = False
    Left = 541
    Top = 11
  end
  object frxDsEndContratante: TfrxDBDataset
    UserName = 'frxDsEndContratante'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Tipo=Tipo'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMERUA=NOMERUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'NOMEUF=NOMEUF'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'TEL=TEL'
      'FAX=FAX'
      'TEL_TXT=TEL_TXT'
      'CONTRATANTE=CONTRATANTE'
      'FAX_TXT=FAX_TXT'
      'CNPJ_TXT=CNPJ_TXT'
      'NUMERO=NUMERO')
    DataSet = QrEndContratante
    BCDToCurrency = False
    Left = 569
    Top = 11
  end
  object QrTestemun01: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM Entidades'
      'ORDER BY NO_ENT')
    Left = 235
    Top = 459
    object QrTestemun01Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTestemun01NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object QrRepreLegal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM Entidades'
      'ORDER BY NO_ENT')
    Left = 178
    Top = 459
    object QrRepreLegalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRepreLegalNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsRepreLegal: TDataSource
    DataSet = QrRepreLegal
    Left = 207
    Top = 459
  end
  object DsTestemun01: TDataSource
    DataSet = QrTestemun01
    Left = 263
    Top = 459
  end
  object QrTestemun02: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM Entidades'
      'ORDER BY NO_ENT')
    Left = 178
    Top = 487
    object QrTestemun02Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTestemun02NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsTestemun02: TDataSource
    DataSet = QrTestemun02
    Left = 207
    Top = 487
  end
  object PMTags: TPopupMenu
    Left = 588
    Top = 580
    object Listadospadres1: TMenuItem
      Caption = 'Lista dos padr'#245'es'
      OnClick = Listadospadres1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object agsPersonalizadas1: TMenuItem
      Caption = 'Tags Personalizadas'
      object Listadesugestes1: TMenuItem
        Caption = 'Lista de personalizados (sugest'#245'es)'
        Enabled = False
        OnClick = Listadesugestes1Click
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object Incluirpersonalizado1: TMenuItem
        Caption = 'Incluir personalizado'
        OnClick = Incluirpersonalizado1Click
      end
      object Alterarpersonalizado1: TMenuItem
        Caption = 'Alterar personalizado'
        OnClick = Alterarpersonalizado1Click
      end
      object Excluirpersonalizado1: TMenuItem
        Caption = 'Excluir personalizado'
        OnClick = Excluirpersonalizado1Click
      end
    end
    object Gruposdetags1: TMenuItem
      Caption = 'Grupos de tags'
      object CadastrodeGruposdeTags1: TMenuItem
        Caption = 'Cadastro de Grupos de Tags'
        OnClick = CadastrodeGruposdeTags1Click
      end
      object SeleodeTagsdeGrupo1: TMenuItem
        Caption = 'Sele'#231#227'o de Tags de Grupo'
        OnClick = SeleodeTagsdeGrupo1Click
      end
    end
    object a1: TMenuItem
      Caption = 'Tags de n'#237'veis'
      object CadastrodeTagsdeNveis1: TMenuItem
        Caption = 'Cadastro de Tags de N'#237'veis'
        OnClick = CadastrodeTagsdeNveis1Click
      end
      object InclusodeTagsdeNveis1: TMenuItem
        Caption = 'Inclus'#227'o de Tags de N'#237'veis'
        OnClick = InclusodeTagsdeNveis1Click
      end
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Pragasinclusas1: TMenuItem
      Caption = 'Pragas inclusas'
      OnClick = Pragasinclusas1Click
    end
    object Pragasexclusas1: TMenuItem
      Caption = 'Pragas exclusas'
      OnClick = Pragasexclusas1Click
    end
    object Servioscontemplados1: TMenuItem
      Caption = 'Servi'#231'os oferecidos'
      OnClick = Servioscontemplados1Click
    end
  end
  object DsContratTag: TDataSource
    DataSet = QrContratTag
    Left = 264
    Top = 488
  end
  object QrContratTag: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM contrattag'
      'WHERE Codigo=:P0')
    Left = 236
    Top = 488
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContratTagCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContratTagControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrContratTagNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrContratTagTag: TWideStringField
      FieldName = 'Tag'
      Size = 50
    end
    object QrContratTagText: TWideMemoField
      FieldName = 'Text'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrCDI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM contrdfits'
      'WHERE Controle=8')
    Left = 780
    Top = 16
    object QrCDICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCDIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCDINome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCDITag: TWideStringField
      FieldName = 'Tag'
      Size = 50
    end
    object QrCDIText: TWideMemoField
      FieldName = 'Text'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCDILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCDIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCDIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCDIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCDIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCDIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCDIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object PMWinDocArq: TPopupMenu
    Left = 884
    Top = 16
    object Localizar1: TMenuItem
      Caption = '&Localizar'
      OnClick = Localizar1Click
    end
    object Editar1: TMenuItem
      Caption = '&Editar'
      OnClick = Editar1Click
    end
  end
  object QrOpcoesGerl: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ContrtIPv4, ContrtSDir '
      'FROM opcoesgerl'
      'WHERE Codigo=1')
    Left = 856
    Top = 16
    object QrOpcoesGerlContrtIPv4: TWideStringField
      FieldName = 'ContrtIPv4'
      Size = 100
    end
    object QrOpcoesGerlContrtSDir: TWideStringField
      FieldName = 'ContrtSDir'
      Size = 255
    end
  end
  object QrContrNivTg: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrContrNivTgAfterOpen
    SQL.Strings = (
      'SELECT'
      'ni3.Nome nNiv3,'
      'ni2.Nome nNiv2,'
      'ni1.Nome nNiv1,'
      'cnt.*'
      'FROM contrnivtg cnt'
      'LEFT JOIN ctrtagniv1 ni1 ON ni1.Codigo=cnt.CtrTagNiv1'
      'LEFT JOIN ctrtagniv2 ni2 ON ni2.Codigo=ni1.NivSup'
      'LEFT JOIN ctrtagniv3 ni3 ON ni3.Codigo=ni2.NivSup')
    Left = 251
    Top = 56
    object QrContrNivTgnNiv3: TWideStringField
      FieldName = 'nNiv3'
      Size = 60
    end
    object QrContrNivTgnNiv2: TWideStringField
      FieldName = 'nNiv2'
      Size = 60
    end
    object QrContrNivTgnNiv1: TWideStringField
      FieldName = 'nNiv1'
      Size = 60
    end
    object QrContrNivTgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContrNivTgControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrContrNivTgCtrTagNiv1: TIntegerField
      FieldName = 'CtrTagNiv1'
    end
  end
  object DsContrNivTg: TDataSource
    DataSet = QrContrNivTg
    Left = 280
    Top = 56
  end
  object QrContrBugsS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT oca.*, IF(oca.Praga_Z <> 0, '#39'E'#39', '#39'G'#39') NO_NIVEL, '
      'IF(oca.Praga_Z <> 0, pcz.Nome, pca.Nome) NO_PRAGA,'
      
        'IF(oca.Praga_Z <> 0, CONCAT(" (", pcz.Especie , ") "), "") NO_ES' +
        'PECIE'
      'FROM contrbugss oca'
      'LEFT JOIN praga_a pca ON pca.Codigo=oca.Praga_A'
      'LEFT JOIN praga_z pcz ON pcz.Codigo=oca.Praga_Z'
      'WHERE oca.Codigo=:P0')
    Left = 712
    Top = 480
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContrBugsSCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContrBugsSControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrContrBugsSPraga_A: TIntegerField
      FieldName = 'Praga_A'
    end
    object QrContrBugsSPraga_Z: TIntegerField
      FieldName = 'Praga_Z'
    end
    object QrContrBugsSNO_NIVEL: TWideStringField
      FieldName = 'NO_NIVEL'
      Required = True
      Size = 1
    end
    object QrContrBugsSNO_PRAGA: TWideStringField
      FieldName = 'NO_PRAGA'
      Size = 60
    end
    object QrContrBugsSNO_ESPECIE: TWideStringField
      FieldName = 'NO_ESPECIE'
      Size = 64
    end
  end
  object DsContrBugsS: TDataSource
    DataSet = QrContrBugsS
    Left = 740
    Top = 480
  end
  object PMContrato: TPopupMenu
    OnPopup = PMContratoPopup
    Left = 392
    Top = 580
    object Inclui1: TMenuItem
      Caption = '&Inclui novo contrato'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera contrato atual'
      Enabled = False
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui contrato atual'
      Enabled = False
      OnClick = Exclui1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Duplicacontratoatual1: TMenuItem
      Caption = '&Duplica contrato atual'
      Enabled = False
      OnClick = Duplicacontratoatual1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Alterarstatusdocontratoatual1: TMenuItem
      Caption = 'Alterar &status do contrato atual'
      OnClick = Alterarstatusdocontratoatual1Click
    end
  end
  object QrContrBugsN: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT oca.*, IF(oca.Praga_Z <> 0, '#39'E'#39', '#39'G'#39') NO_NIVEL, '
      'IF(oca.Praga_Z <> 0, pcz.Nome, pca.Nome) NO_PRAGA,'
      
        'IF(oca.Praga_Z <> 0, CONCAT(" (", pcz.Especie , ") "), "") NO_ES' +
        'PECIE'
      'FROM contrbugss oca'
      'LEFT JOIN praga_a pca ON pca.Codigo=oca.Praga_A'
      'LEFT JOIN praga_z pcz ON pcz.Codigo=oca.Praga_Z'
      'WHERE oca.Codigo=:P0')
    Left = 712
    Top = 508
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContrBugsNCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContrBugsNControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrContrBugsNPraga_A: TIntegerField
      FieldName = 'Praga_A'
    end
    object QrContrBugsNPraga_Z: TIntegerField
      FieldName = 'Praga_Z'
    end
    object QrContrBugsNNO_NIVEL: TWideStringField
      FieldName = 'NO_NIVEL'
      Required = True
      Size = 1
    end
    object QrContrBugsNNO_PRAGA: TWideStringField
      FieldName = 'NO_PRAGA'
      Size = 60
    end
    object QrContrBugsNNO_ESPECIE: TWideStringField
      FieldName = 'NO_ESPECIE'
      Size = 64
    end
  end
  object DsContrBugsN: TDataSource
    DataSet = QrContrBugsN
    Left = 740
    Top = 508
  end
  object QrContrServi: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ds.Nome NO_SERVICO, cs.* '
      'FROM contrservi cs'
      'LEFT JOIN desservico ds ON ds.Codigo=cs.Servico'
      'WHERE cs.Codigo=2')
    Left = 856
    Top = 468
    object QrContrServiNO_SERVICO: TWideStringField
      FieldName = 'NO_SERVICO'
      Size = 60
    end
    object QrContrServiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContrServiControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrContrServiServico: TIntegerField
      FieldName = 'Servico'
    end
  end
  object DsContrServi: TDataSource
    DataSet = QrContrServi
    Left = 884
    Top = 468
  end
  object QrContratLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'stc.Cliente, loc.Controle, loc.Tabela, loc.Cadastro, '
      'ELT(loc.Tabela, stc.Nome, ELT(mac.Tipo, '
      '"M'#243'vel", "Autom'#243'vel", "Animal", "Outro", "???")) NO_LUGAR, '
      'ELT(loc.Tabela, sic.SCompl2, mac.Nome, "? ? ?") NO_LOCAL '
      'FROM contratloc loc'
      'LEFT JOIN siapimacad sic ON sic.Codigo=loc.Cadastro'
      'LEFT JOIN siaptercad stc ON stc.Codigo=sic.SiapImaTer'
      'LEFT JOIN movamovcad mac ON mac.Codigo=loc.Cadastro'
      'LEFT JOIN entidades ent ON ent.Codigo=stc.Cliente'
      'WHERE loc.Codigo>0')
    Left = 236
    Top = 516
    object QrContratLocControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrContratLocTabela: TSmallintField
      FieldName = 'Tabela'
    end
    object QrContratLocCadastro: TIntegerField
      FieldName = 'Cadastro'
    end
    object QrContratLocNO_LUGAR: TWideStringField
      FieldName = 'NO_LUGAR'
      Size = 100
    end
    object QrContratLocNO_LOCAL: TWideStringField
      FieldName = 'NO_LOCAL'
      Size = 100
    end
    object QrContratLocNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrContratLocCliente: TFloatField
      FieldName = 'Cliente'
    end
  end
  object DsContratLoc: TDataSource
    DataSet = QrContratLoc
    Left = 264
    Top = 516
  end
  object PMQuery: TPopupMenu
    Left = 192
    Top = 20
    object peloNomedocontrato1: TMenuItem
      Caption = 'pelo &Nome do contrato'
      OnClick = peloNomedocontrato1Click
    end
    object pelosenvolvidos1: TMenuItem
      Caption = 'pelos &Envolvidos'
      OnClick = pelosenvolvidos1Click
    end
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 4
    Top = 12
  end
  object PMLocais: TPopupMenu
    Left = 692
    Top = 568
    object Prprios1: TMenuItem
      Caption = 'Pr'#243'prios'
      OnClick = Prprios1Click
    end
    object DeSubClientes1: TMenuItem
      Caption = 'De Sub-Clientes'
      OnClick = DeSubClientes1Click
    end
  end
  object FindDialog1: TFindDialog
    OnFind = FindDialog1Find
    Left = 632
    Top = 81
  end
end
