unit ContrDfIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, ComCtrls, dmkRichEdit, dmkMemo, UnDmkEnums;

type
  TFmContrDfIts = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Label2: TLabel;
    EdTag: TdmkEdit;
    CkContinuar: TCheckBox;
    MeText: TdmkMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenContrDfIts(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmContrDfIts: TFmContrDfIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck;

{$R *.DFM}

procedure TFmContrDfIts.BtOKClick(Sender: TObject);
var
  Codigo, Controle: Integer;
  Nome, Tag, Ini, Fim, Text: String;
begin
  Nome := EdNome.Text;
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  Tag := EdTag.Text;
  if MyObjects.FIC(Length(Tag) < 3, EdTag, 'Informe uma tag v�lida!') then
    Exit;
  Ini := Tag[1];
  Fim := Tag[Length(Tag)];
  if MyObjects.FIC(Ini <> '[', EdTag, 'Tag deve come�ar com "["') then
    Exit;
  if MyObjects.FIC(Fim <> ']', EdTag, 'Tag deve terminar com "]"') then
    Exit;
  Codigo         := FQrCab.FieldByName('Codigo').AsInteger;
  //Text           := MyObjects.ObtemTextoRichEdit(Self, REText);
  Text           := MeText.Text;
  Controle := EdControle.ValueVariant;
  Controle := UMyMod.BPGS1I32('contrdfits', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'contrdfits', False, [
  'Codigo', 'Nome', 'Tag',
  'Text'], [
  'Controle'], [
  Codigo, Nome, Tag,
  Text], [
  Controle], True) then
  begin
    ReopenContrDfIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdTag.ValueVariant       := '';
      MeText.Text              := '';
      EdNome.ValueVariant      := '';
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmContrDfIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContrDfIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmContrDfIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmContrDfIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContrDfIts.ReopenContrDfIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    FQrIts.Open;
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
