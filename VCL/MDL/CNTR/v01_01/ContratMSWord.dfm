object FmContratMSWord: TFmContratMSWord
  Left = 339
  Top = 185
  Caption = 'CAD-CONTR-002 :: Impress'#227'o de Contrato Personalizado'
  ClientHeight = 382
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 455
        Height = 32
        Caption = 'Impress'#227'o de Contrato Personalizado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 455
        Height = 32
        Caption = 'Impress'#227'o de Contrato Personalizado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 455
        Height = 32
        Caption = 'Impress'#227'o de Contrato Personalizado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 220
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 220
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 213
        Align = alTop
        Caption = ' Texto base em Microsof Word: '
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 45
          Width = 780
          Height = 122
          Align = alTop
          BevelOuter = bvNone
          Enabled = False
          TabOrder = 0
          object Label1: TLabel
            Left = 12
            Top = 0
            Width = 42
            Height = 13
            Caption = 'Servidor:'
          end
          object Label2: TLabel
            Left = 184
            Top = 0
            Width = 30
            Height = 13
            Caption = 'Pasta:'
          end
          object Label3: TLabel
            Left = 520
            Top = 0
            Width = 65
            Height = 13
            Caption = 'Arquivo base:'
          end
          object Label4: TLabel
            Left = 12
            Top = 40
            Width = 55
            Height = 13
            Caption = 'Contratada:'
          end
          object Label5: TLabel
            Left = 392
            Top = 40
            Width = 58
            Height = 13
            Caption = 'Contratante:'
          end
          object Label6: TLabel
            Left = 12
            Top = 80
            Width = 170
            Height = 13
            Caption = 'Representante legal do contratante:'
          end
          object Label7: TLabel
            Left = 268
            Top = 80
            Width = 71
            Height = 13
            Caption = 'Testemunha 1:'
          end
          object Label8: TLabel
            Left = 520
            Top = 80
            Width = 71
            Height = 13
            Caption = 'Testemunha 2:'
          end
          object EdHost: TdmkEdit
            Left = 12
            Top = 16
            Width = 169
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSDir: TdmkEdit
            Left = 184
            Top = 16
            Width = 333
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdArquivo: TdmkEdit
            Left = 520
            Top = 16
            Width = 249
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdContratada: TdmkEdit
            Left = 12
            Top = 56
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNO_CONTRATADA: TdmkEdit
            Left = 68
            Top = 56
            Width = 320
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdContratante: TdmkEdit
            Left = 392
            Top = 56
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNO_CONTRATANTE: TdmkEdit
            Left = 448
            Top = 56
            Width = 320
            Height = 21
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdRepreLegal: TdmkEdit
            Left = 12
            Top = 96
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNO_REP_LEGAL: TdmkEdit
            Left = 68
            Top = 96
            Width = 196
            Height = 21
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdNO_TESTEM_01: TdmkEdit
            Left = 324
            Top = 96
            Width = 192
            Height = 21
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdTestemun01: TdmkEdit
            Left = 268
            Top = 96
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNO_TESTEM_02: TdmkEdit
            Left = 576
            Top = 96
            Width = 192
            Height = 21
            TabOrder = 11
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdTestemun02: TdmkEdit
            Left = 520
            Top = 96
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 12
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 30
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label9: TLabel
            Left = 8
            Top = 8
            Width = 57
            Height = 13
            Caption = 'ID Contrato:'
          end
          object Label10: TLabel
            Left = 160
            Top = 8
            Width = 88
            Height = 13
            Caption = 'Arquivo resultante:'
          end
          object EdCodigo: TdmkEdit
            Left = 72
            Top = 4
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdArqResul: TdmkEdit
            Left = 252
            Top = 5
            Width = 517
            Height = 21
            TabStop = False
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object RGFormaUso: TdmkRadioGroup
          Left = 15
          Top = 167
          Width = 754
          Height = 40
          Caption = ' Forma de Uso: '
          Columns = 4
          Enabled = False
          Items.Strings = (
            'Nenhum'
            'Texto em BD'
            'Arquivo MSWord'
            'Fast Report')
          TabOrder = 2
          QryCampo = 'FormaUso'
          UpdCampo = 'FormaUso'
          UpdType = utYes
          OldValor = 0
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 268
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 312
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 0
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrContratTag: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Text'
      'FROM contrattag'
      'WHERE Codigo=3'
      'AND Tag="[PRAG_ALV]"'
      '')
    Left = 588
    Top = 12
    object QrContratTagText: TWideMemoField
      FieldName = 'Text'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrOpcoesGerl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ContrtIPv4, ContrtSDir '
      'FROM opcoesgerl'
      'WHERE Codigo=1')
    Left = 616
    Top = 12
    object QrOpcoesGerlContrtIPv4: TWideStringField
      FieldName = 'ContrtIPv4'
      Size = 100
    end
    object QrOpcoesGerlContrtSDir: TWideStringField
      FieldName = 'ContrtSDir'
      Size = 255
    end
  end
  object frx_CAD_CONTR_001_03_A: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41187.667138680540000000
    ReportOptions.LastChange = 41187.667138680540000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frx_CAD_CONTR_001_03_AGetValue
    OnUserFunction = frx_CAD_CONTR_001_03_AUserFunction
    Left = 560
    Top = 12
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
    end
  end
  object QrCtrTagNiv3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ni3.Codigo, ni3.Conteudo'
      'FROM contrnivtg cnt'
      'LEFT JOIN ctrtagniv1 ni1 ON ni1.Codigo=cnt.CtrTagNiv1'
      'LEFT JOIN ctrtagniv2 ni2 ON ni2.Codigo=ni1.NivSup'
      'LEFT JOIN ctrtagniv3 ni3 ON ni3.Codigo=ni2.NivSup'
      'WHERE cnt.Codigo> 0'
      'AND ni3.Tag = "[ACOES_A_SEREM_DESENVOLVIDAS]"')
    Left = 644
    Top = 12
    object QrCtrTagNiv3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtrTagNiv3Conteudo: TWideMemoField
      FieldName = 'Conteudo'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrCtrTagNiv2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ni2.Ordem, ni2.Codigo, ni2.Conteudo'
      'FROM ctrtagniv2 ni2'
      'WHERE ni2.Codigo> 0'
      'ORDER BY ni2.Ordem, ni2.Codigo')
    Left = 672
    Top = 12
    object QrCtrTagNiv2Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrCtrTagNiv2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtrTagNiv2Conteudo: TWideMemoField
      FieldName = 'Conteudo'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrCtrTagNiv1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ni1.Ordem, ni1.Codigo, ni1.Conteudo '
      'FROM ctrtagniv1 ni1'
      'LEFT JOIN contrnivtg cnt ON ni1.Codigo=cnt.CtrTagNiv1 '
      'WHERE ni1.NivSup>0'
      'AND cnt.Codigo=4'
      'ORDER BY ni1.Ordem, ni1.Codigo ')
    Left = 700
    Top = 12
    object QrCtrTagNiv1Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrCtrTagNiv1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtrTagNiv1Conteudo: TWideMemoField
      FieldName = 'Conteudo'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrCTN3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ni3.Codigo cNiv3, ni3.Tag tNiv3'
      '/*, '
      'ni2.Codigo cNiv2, ni2.Tag tNiv2, '
      'ni1.Codigo cNiv1, ni1.Tag tNiv1'
      '*/'
      'FROM contrnivtg cnt '
      'LEFT JOIN ctrtagniv1 ni1 ON ni1.Codigo=cnt.CtrTagNiv1 '
      'LEFT JOIN ctrtagniv2 ni2 ON ni2.Codigo=ni1.NivSup '
      'LEFT JOIN ctrtagniv3 ni3 ON ni3.Codigo=ni2.NivSup '
      'WHERE cnt.Codigo=14')
    Left = 644
    Top = 60
    object QrCTN3cNiv3: TIntegerField
      FieldName = 'cNiv3'
    end
    object QrCTN3tNiv3: TWideStringField
      FieldName = 'tNiv3'
      Size = 30
    end
  end
  object frx_CAD_CONTR_001_03_B: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PreviewOptions.ZoomMode = zmPageWidth
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39094.852886284700000000
    ReportOptions.LastChange = 40015.783609479200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frx_CAD_CONTR_001_03_AGetValue
    Left = 532
    Top = 12
    Datasets = <
      item
        DataSet = FmContratos.frxDsContratos
        DataSetName = 'frxDsContratos'
      end
      item
        DataSet = FmContratos.frxDsEndContratada
        DataSetName = 'frxDsEndContratada'
      end
      item
        DataSet = FmContratos.frxDsEndContratante
        DataSetName = 'frxDsEndContratante'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object MasterData1: TfrxMasterData
        Height = 3.779530000000000000
        Top = 98.267780000000000000
        Width = 793.701300000000000000
        RowCount = 1
      end
      object DetailData1: TfrxDetailData
        Height = 18.897637800000000000
        Top = 124.724490000000000000
        Width = 793.701300000000000000
        RowCount = 1
        Stretched = True
        object frxDsContratosTitulo: TfrxMemoView
          Left = 0.377953000000000000
          Width = 793.700787400000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'Nome'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsContratos."Nome"]')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        Height = 18.897650000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
      end
      object DetailData3: TfrxDetailData
        Height = 126.992208000000000000
        Top = 166.299320000000000000
        Width = 793.701300000000000000
        RowCount = 1
        object Memo1: TfrxMemoView
          Left = 41.952783000000000000
          Width = 272.126160000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Contratada')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 37.795300000000000000
          Top = 21.385838000000010000
          Width = 351.496062990000000000
          Height = 101.291404000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 37.795275590000000000
          Top = 22.677165350000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Contratada:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 86.929133860000000000
          Top = 22.677165350000000000
          Width = 302.362209610000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."CONTRATADA"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 37.795275590000000000
          Top = 41.574803150000010000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 86.929133859999990000
          Top = 41.574829999999990000
          Width = 302.362209610000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CAENDERECO]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 37.795275590000000000
          Top = 60.472440940000010000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 86.929133858267700000
          Top = 60.472440940000010000
          Width = 113.763853000000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."BAIRRO"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 200.314960629921000000
          Top = 60.472440940000010000
          Width = 30.236220470000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade:')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 230.551181102362000000
          Top = 60.472440940000010000
          Width = 120.944886770000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."CIDADE"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 351.496062992126000000
          Top = 60.692950000000000000
          Width = 18.897637800000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'UF:')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 370.393700787402000000
          Top = 60.692950000000000000
          Width = 18.897637800000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."NOMEUF"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 37.795275590000000000
          Top = 79.370078740000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CALACNPJCFP]:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 86.929133859999990000
          Top = 79.370078740000000000
          Width = 124.724409450000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 211.653543310000000000
          Top = 79.370078740000000000
          Width = 45.354330710000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CALAIERG]:')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 257.007874020000000000
          Top = 79.370078740000000000
          Width = 132.283469450000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CAIERG]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 37.795275590000000000
          Top = 98.267716540000010000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 86.929133858267700000
          Top = 98.267716540000010000
          Width = 124.724409450000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."TEL_TXT"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 211.653543307087000000
          Top = 98.267716540000010000
          Width = 45.354330710000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Fax:')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 257.007874020000000000
          Top = 98.267716540000010000
          Width = 132.283469450000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratada."FAX_TXT"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 408.945146000000000000
          Width = 272.126160000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Contratante')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 404.409448818898000000
          Top = 21.385838000000010000
          Width = 351.496062990000000000
          Height = 101.291404000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 404.409448820000000000
          Top = 22.677165350000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Contratante:')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 453.543307090000000000
          Top = 22.677165350000000000
          Width = 302.362204724409000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."CONTRATANTE"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 404.409448820000000000
          Top = 41.574803149606310000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 453.543307090000000000
          Top = 41.574829999999990000
          Width = 302.362204724409000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CEENDERECO]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 404.409448820000000000
          Top = 60.472440944881890000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 453.543307090000000000
          Top = 60.472440944881890000
          Width = 113.763853000000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."BAIRRO"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 568.063359000000000000
          Top = 60.472440944881890000
          Width = 30.236220470000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade:')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 598.299599000000000000
          Top = 60.472440944881890000
          Width = 113.385826770000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."CIDADE"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 711.685499000000000000
          Top = 60.472440944881890000
          Width = 18.897637800000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'UF:')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 730.583149000000000000
          Top = 60.472440944881890000
          Width = 18.897637800000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."NOMEUF"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 404.409448820000000000
          Top = 79.370078740000000000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CELACNPJCFP]:')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 453.543307090000000000
          Top = 79.370078740157500000
          Width = 124.724409450000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 578.267716535433000000
          Top = 79.370078740000000000
          Width = 45.354330710000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CELAIERG]:')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 623.622047240000000000
          Top = 79.370078740000000000
          Width = 132.283464566929000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CEIERG]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 404.409448820000000000
          Top = 98.267716535433100000
          Width = 49.133858270000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 453.543307090000000000
          Top = 98.267716535433100000
          Width = 124.724409450000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."TEL_TXT"]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 578.267716535433000000
          Top = 98.267716540000010000
          Width = 45.354330710000000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Fax:')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 623.622047240000000000
          Top = 98.267716540000010000
          Width = 132.283464566929000000
          Height = 18.897637800000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsEndContratante."FAX_TXT"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 52.913420000000000000
        Top = 544.252320000000100000
        Width = 793.701300000000000000
        object Line1: TfrxLineView
          Left = 37.039394000000000000
          Top = 8.937012999999980000
          Width = 718.110700000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
        object Memo41: TfrxMemoView
          Left = 592.630304000000000000
          Top = 12.094496000000050000
          Width = 162.519790000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        Height = 11.338590000000000000
        Top = 317.480520000000000000
        Width = 793.701300000000000000
        DataSet = FmContratos.frxDsContratos
        DataSetName = 'frxDsContratos'
        RowCount = 0
      end
      object DetailData2: TfrxDetailData
        Height = 18.897637800000000000
        Top = 351.496290000000000000
        Width = 793.701300000000000000
        AllowSplit = True
        DataSet = FmContratos.frxDsContratos
        DataSetName = 'frxDsContratos'
        RowCount = 0
        Stretched = True
        object Rich1: TfrxRichView
          Left = 37.795300000000000000
          Top = 0.377952999999990800
          Width = 718.110700000000000000
          Height = 18.897637800000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataField = 'Texto'
          DataSet = FmContratos.frxDsContratos
          DataSetName = 'frxDsContratos'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C204D532053616E732053657269663B7D7D0D0A7B5C2A5C67656E
            657261746F7220526963686564323020362E322E393230307D5C766965776B69
            6E64345C756331200D0A5C706172645C66305C667331365C7061720D0A7D0D0A
            00}
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 89.196908000000000000
        Top = 430.866420000000000000
        Width = 793.701300000000000000
        object Memo150: TfrxMemoView
          Left = 37.795300000000000000
          Top = 41.574830000000020000
          Width = 302.362400000000000000
          Height = 45.354330708661400000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEndContratada."CONTRATADA"]'
            'Contratada')
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Left = 377.953000000000000000
          Top = 41.574830000000020000
          Width = 302.362400000000000000
          Height = 45.354360000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEndContratante."CONTRATANTE"]'
            'Contratante')
          ParentFont = False
        end
      end
    end
  end
  object frxRichObject1: TfrxRichObject
    Left = 503
    Top = 12
  end
end
