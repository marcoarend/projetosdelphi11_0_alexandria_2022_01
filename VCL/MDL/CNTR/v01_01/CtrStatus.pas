unit CtrStatus;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkRadioGroup,
  dmkEditDateTimePicker;

type
  TFmCtrStatus = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkRGStatus: TdmkRadioGroup;
    LaDtaCntrFim: TLabel;
    TPDtaCntrFim: TdmkEditDateTimePicker;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dmkRGStatusClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(Status: Integer; DtaCntrFim: TDate);
  public
    { Public declarations }
    FCodigo, FStatus: Integer;
    FDtaCntrFim: TDate
  end;

  var
  FmCtrStatus: TFmCtrStatus;

implementation

uses UnMyObjects, Module, UMySQLModule, UnContratUnit;

{$R *.DFM}

procedure TFmCtrStatus.BtOKClick(Sender: TObject);
var
  Status: Integer;
  DtaCntrFim, Msg: String;
  Conflito: Boolean;
begin
  Status   := dmkRGStatus.ItemIndex;
  Conflito := ContratUnit.ValidaDadosRescisao(dmkRGStatus.ItemIndex,
                TPDtaCntrFim.Date, Msg);
  //
  if MyObjects.FIC(FCodigo = 0, nil, 'C�digo n�o definido!') then
    Exit;
  if MyObjects.FIC(Status < 0, dmkRGStatus, 'Defina o status!') then
    Exit;
  if MyObjects.FIC(Conflito, nil, Msg) then
    Exit;
  if TPDtaCntrFim.Visible = True then
    DtaCntrFim := Geral.FDT(TPDtaCntrFim.Date, 1)
  else
    DtaCntrFim := '0000-00-00';
  //
  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'contratos', False,
    ['Status', 'DtaCntrFim'], ['Codigo'],
    [Status, DtaCntrFim], [FCodigo], True)
  then
    Geral.MB_Erro('Falha ao atualizar dados!')
  else
    Close;
end;

procedure TFmCtrStatus.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCtrStatus.dmkRGStatusClick(Sender: TObject);
begin
  MostraEdicao(dmkRGStatus.ItemIndex, TPDtaCntrFim.Date);
end;

procedure TFmCtrStatus.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCtrStatus.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  dmkRGStatus.Items.Clear;
  dmkRGStatus.Items.AddStrings(ContratUnit.ConfiguraStatus());
end;

procedure TFmCtrStatus.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCtrStatus.FormShow(Sender: TObject);
begin
  MostraEdicao(FStatus, FDtaCntrFim);
end;

procedure TFmCtrStatus.MostraEdicao(Status: Integer; DtaCntrFim: TDate);
var
  Visi: Boolean;
begin
  dmkRGStatus.ItemIndex := Status;
  TPDtaCntrFim.Date     := DtaCntrFim;
  //
  if CO_CONTRATO_STATUS_Int[4] = Status then //Rescindido
    Visi := True
  else
    Visi := False;
  //
  LaDtaCntrFim.Visible := Visi;
  TPDtaCntrFim.Visible := Visi;
end;

end.
