unit ContratMSWord;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, UnDmkProcFunc, dmkRadioGroup, frxClass, UnDmkEnums, frxRich;
const
  FMaxItsPg = 1023;
type
  TFmContratMSWord = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    EdHost: TdmkEdit;
    EdSDir: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdArquivo: TdmkEdit;
    Label4: TLabel;
    EdContratada: TdmkEdit;
    EdNO_CONTRATADA: TdmkEdit;
    EdContratante: TdmkEdit;
    EdNO_CONTRATANTE: TdmkEdit;
    Label5: TLabel;
    Label6: TLabel;
    EdRepreLegal: TdmkEdit;
    EdNO_REP_LEGAL: TdmkEdit;
    EdNO_TESTEM_01: TdmkEdit;
    Label7: TLabel;
    EdTestemun01: TdmkEdit;
    EdNO_TESTEM_02: TdmkEdit;
    Label8: TLabel;
    EdTestemun02: TdmkEdit;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Label10: TLabel;
    EdArqResul: TdmkEdit;
    RGFormaUso: TdmkRadioGroup;
    QrContratTag: TmySQLQuery;
    QrContratTagText: TWideMemoField;
    QrOpcoesGerl: TmySQLQuery;
    QrOpcoesGerlContrtIPv4: TWideStringField;
    QrOpcoesGerlContrtSDir: TWideStringField;
    frx_CAD_CONTR_001_03_A: TfrxReport;
    QrCtrTagNiv3: TmySQLQuery;
    QrCtrTagNiv3Codigo: TIntegerField;
    QrCtrTagNiv3Conteudo: TWideMemoField;
    QrCtrTagNiv2: TmySQLQuery;
    QrCtrTagNiv2Ordem: TIntegerField;
    QrCtrTagNiv2Codigo: TIntegerField;
    QrCtrTagNiv2Conteudo: TWideMemoField;
    QrCtrTagNiv1: TmySQLQuery;
    QrCtrTagNiv1Ordem: TIntegerField;
    QrCtrTagNiv1Codigo: TIntegerField;
    QrCtrTagNiv1Conteudo: TWideMemoField;
    QrCTN3: TmySQLQuery;
    QrCTN3cNiv3: TIntegerField;
    QrCTN3tNiv3: TWideStringField;
    frx_CAD_CONTR_001_03_B: TfrxReport;
    frxRichObject1: TfrxRichObject;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frx_CAD_CONTR_001_03_AGetValue(const VarName: string;
      var Value: Variant);
    function frx_CAD_CONTR_001_03_AUserFunction(const MethodName: string;
      var Params: Variant): Variant;
  private
    { Private declarations }
    // F_CIDA_CLI, F_CIDA_REP, F_NOME_REP, F_ENDE_REP, F_CPFJ_REP,
    F_CPFJ_EMP,
    F_NOME_EMP, F_NOME_CLI, F_CPFJ_CLI,
    F_ENDE_EMP, F_ENDE_CLI, F_CIDA_EMP,
    F_NOM_RESP, F_CID_RESP, F_CPF_RESP, F_RGN_RESP, F_RGO_RESP, F_RGD_RESP,
    F_TempoImpl, F_TempoMoni, F_TempoTrei, F_PrazoImpl, F_VAL_MENS, F_PER_MULT,
    F_PER_MORA, F_LOGR_PRP, F_LOGR_PPE, F_TIPO_DOC,
    F_NOM_TES1, F_CPF_TES1, F_EN1_TES1, F_EN2_TES1, F_EN3_TES1,
    F_NOM_TES2, F_CPF_TES2, F_EN1_TES2, F_EN2_TES2, F_EN3_TES2,
    F_ESCOPOSP: String;
    F_ArtDefCli, F_ArtDefLug: Integer;
    FItsPgs: array [0..FMaxItsPg] of Integer;
    
    procedure DefineVariaveisEscopo();
    //
    function SetaPaginaDeItem(Pagina, Item: Extended): Boolean;

    {procedure InsertLines(LineNum : Integer);
    procedure CreateMailMergeDataFile;
    procedure FillRow(Doc : Variant; Row : Integer;
                 Text1,Text2,Text3,Text4 : String);
    procedure ZZZ_ExemploDidaticoMicrosoft();
    }
    function ObtemPragas(Qry: TmySQLQuery): String;
    function ObtemServicos(Qry: TmySQLQuery): String;
    function ObtemLocais(Qry: TmySQLQuery): String;
    function LocalizaTagNiv(const VarName: String; var Value: String): Boolean;
  public
    { Public declarations }
    wrdApp, wrdDoc: Variant;
    FQrContratos: TmySQLQuery;
    FQrPragaS, FQrPragaN, FQrServicos, FQrLocais: TmySQLQuery;
    FFmChamou: TForm;
  end;

  var
  FmContratMSWord: TFmContratMSWord;

implementation

uses UnMyObjects, Module, ComObj, UnAppListas, ModuleGeral, DmkDAC_PF,
UMySQLModule, Contratos;

const wdAlignParagraphLeft = 0;
const wdAlignParagraphCenter = 1;
const wdAlignParagraphRight = 2;
const wdAlignParagraphJustify = 3;
const wdAdjustNone = 0;
const wdGray25 = 16;
const wdGoToLine = 3;
const wdGoToLast = -1;
const wdSendToNewDocument = 0;

{$R *.DFM}

procedure TFmContratMSWord.BtOKClick(Sender: TObject);
var
  Host, SDir, RDir, NoArq: String;
  Empresa: Integer;
  DirSorc, DirDest: String;
  //
  //P: Integer;
  //1>Word, Contrato, Doc: Variant;
  MSWord: OleVariant;
  ArqResul, Extensao1, Extensao2, Txt_aviso: String;
  I, Extras: Integer;
  //
  SERVICOS, PRAG_SIM, PRAG_NAO, LOCAIS_S, ESCOPOSP, Value: String;
  arrVar, arrVal: TMyStrDynArr;
begin
  case RGFormaUso.ItemIndex of
    1:
    begin
      TFmContratos(FFmChamou).ReopenEndContratada(TFmContratos(FFmChamou).QrContratosContratada.Value);
      TFmContratos(FFmChamou).ReopenEndContratante(TFmContratos(FFmChamou).QrContratosContratante.Value);
      //
      DefineVariaveisEscopo();
      //
      MyObjects.frxDefineDataSets(frx_CAD_CONTR_001_03_B, [
        TFmContratos(FFmChamou).frxDsContratos,
        TFmContratos(FFmChamou).frxDsEndContratada,
        TFmContratos(FFmChamou).frxDsEndContratante
        ]);
      //
      MyObjects.frxMostra(frx_CAD_CONTR_001_03_B, 'Impress�o de contrato');
    end;
    2,3:
    begin
      Extras := 0;
      for I := 0 to FMaxItsPg do
        FItsPgs[I] := 0;
      //
      frx_CAD_CONTR_001_03_A.AddFunction(
                'function SetaPaginaDeItem(Pagina, Item: Extended): Boolean');
      Host := EdHost.Text;
      SDir := EdSDir.Text;
      RDir := '';
      Empresa := EdContratada.ValueVariant;
      NoArq := EdArquivo.Text;
      Extensao1 := ExtractFileExt(NoArq);
      case RGFormaUso.Itemindex of
        2: Extensao2 := '.doc';
        3: Extensao2 := '.fr3';
      end;
      if Lowercase(Extensao2) <> Lowercase(Extensao1) then
      begin
        if Geral.MB_Pergunta('Extens�o de arquivo inv�lida: "' + Extensao1 +
        '". Extens�o deve ser "' + Extensao2 +
        '". Deseja continuar assim mesmo?') <> ID_YES then
          Exit;
      end;
      //
      if MyObjects.TentaDefinirDiretorio(Host, SDir, Empresa, RDir, LaAviso1, LaAviso2, True, nil, DirDest) then
      if MyObjects.TentaDefinirDiretorio(Host, SDir, 0, RDir, LaAviso1, LaAviso2, False, nil, DirSorc) then
      begin
        //Estacionei aqui!
        NoArq := dmkPF.CaminhoArquivo(DirSorc, NoArq, '');
        case RGFormaUso.Itemindex of
          2:
          begin
{$IFDEF UsaImpContratoMSWord}
            Cursor := crHourGlass;
            try
              MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo: ' + NoArq);
              //NoArq := dmkPF.CaminhoArquivo(DirSorc, NoArq, '');
              if not FileExists(NoArq) then
              begin
                Txt_aviso := 'Arquivo n�o localizado: ' + NoArq;
                Geral.MB_Aviso(Txt_aviso);
                MyObjects.Informa2(LaAviso1, LaAviso2, False, Txt_aviso);
                Exit;
              end;

              //1>Word := CreateOleObject('Word.Application');
              //1>Contrato := Word.Documents;
              //1>Doc := Contrato.Open(NoArq);
              MSWord := CreateOleObject('Word.Application');
              MSWord.Application.Documents.Open(NoArq);
              //
              try
                SERVICOS := '';
                PRAG_SIM := '';
                PRAG_NAO := '';
                LOCAIS_S := '';
                ESCOPOSP := TFmContratos(FFmChamou).QrContratosEscopoSP.Value;
                if FQrServicos <> nil then
                  SERVICOS := ObtemServicos(FQrServicos);
                if FQrPragaS <> nil then
                  PRAG_SIM := ObtemPragas(FQrPragaS);
                if FQrPragaN <> nil then
                  PRAG_NAO := ObtemPragas(FQrPragaN);
                if FQrLocais <> nil then
                  LOCAIS_S := ObtemLocais(FQrLocais);
                //
                dmkPF.AddTxtToArr2(arrVar, arrVal,'[SERVICOS]', SERVICOS);
                dmkPF.AddTxtToArr2(arrVar, arrVal,'[ESCOPOSP]', ESCOPOSP);
                dmkPF.AddTxtToArr2(arrVar, arrVal,'[PRAG_SIM]', PRAG_SIM);
                dmkPF.AddTxtToArr2(arrVar, arrVal,'[PRAG_NAO]', PRAG_NAO);
                dmkPF.AddTxtToArr2(arrVar, arrVal,'[LOCAIS_S]', LOCAIS_S);

                UnDmkDAC_PF.AbreMySQLQuery0(QrCTN3, Dmod.MyDB, [
                'SELECT DISTINCT ni3.Codigo cNiv3, ni3.Tag tNiv3 ',
                'FROM contrnivtg cnt ',
                'LEFT JOIN ctrtagniv1 ni1 ON ni1.Codigo=cnt.CtrTagNiv1 ',
                'LEFT JOIN ctrtagniv2 ni2 ON ni2.Codigo=ni1.NivSup ',
                'LEFT JOIN ctrtagniv3 ni3 ON ni3.Codigo=ni2.NivSup ',
                'WHERE cnt.Codigo=' + Geral.FF0(EdCodigo.ValueVariant),
                '']);
                QrCTN3.First;
                while not QrCTN3.Eof do
                begin
                  LocalizaTagNiv(QrCTN3tNiv3.Value, Value);
                  dmkPF.AddTxtToArr2(arrVar, arrVal, QrCTN3tNiv3.Value, Value);
                  Extras := Extras + 1;
                  //
                  QrCTN3.Next;
                end;
                //1>UnAppListas.ReplaceVarsContratoMSWord_A(Doc, FQrContratos,
    (*            AppListas.ReplaceVarsContratoMSWord_B(MSWord, FQrContratos,
                LaAviso1, LaAviso2, [
                '[SERVICOS]', '[ESCOPOSP]', '[PRAG_SIM]', '[PRAG_NAO]', '[LOCAIS_S]'],
                [SERVICOS, ESCOPOSP, PRAG_SIM, PRAG_NAO, LOCAIS_S]);
    *)
                AppListas.ReplaceVarsContratoMSWord_B(MSWord, FQrContratos,
                LaAviso1, LaAviso2, arrVar, arrVal, Extras);
              except
                Txt_aviso := 'O documento n�o foi apropriadamente carregado!';
                Geral.MB_ERRO(Txt_aviso);
                MyObjects.Informa2(LaAviso1, LaAviso2, False, Txt_aviso);
                Exit;
              end;
              ArqResul := 'C' + Geral.FFN(EdCodigo.ValueVariant, 9) +
              FormatDateTime('_YYYYDDMM_HHNNSS', Now());
              NoArq := dmkPF.CaminhoArquivo(DirDest, ArqResul, '');
              EdArqResul.Text := ArqResul;
              //Doc.ActiveDocument.SaveAs(FileName := NoArq);
              //1>Doc.SaveAs(FileName := NoArq);
              //1>Word.Visible := True;
              MSWord.ActiveDocument.SaveAs(FileName := NoArq);
              MSWord.Visible := True;
              //
              MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
            finally
              Cursor := crDefault;
            end;
{$ELSE}
            Geral.MB_Aviso('Este aplicativo n�o utiliza impress�o de contrato em MS Word!');
{$ENDIF}
          end;
          3:
          begin
            Cursor := crHourGlass;
            try
              MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Definindo vari�veis: ' + NoArq);
              DefineVariaveisEscopo();

              MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando relat�rio customizado: ' + NoArq);
              frx_CAD_CONTR_001_03_A.LoadFromFile(NoArq);

              MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando relat�rio!');
              MyObjects.frxMostra(frx_CAD_CONTR_001_03_A, 'Impress�o de contrato');
            finally
              Cursor := crDefault;
            end;
          end;
        end;
      end;
    end;
    else
      Geral.MB_Aviso('Forma de uso n�o implemetada!');
  end;
end;

procedure TFmContratMSWord.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

{
procedure TFmContratMSWord.CreateMailMergeDataFile;
var
  wrdDataDoc : Variant;
  iCount : Integer;
begin
  // Create a data source at C:\DataDoc.doc containing the field data
  wrdDoc.MailMerge.CreateDataSource('C:\DataDoc.doc',,,'FirstName, LastName,' +
       ' Address, CityStateZip');
  // Open the file to insert data
  wrdDataDoc := wrdApp.Documents.Open('C:\DataDoc.doc');
  for iCount := 1 to 2 do
    wrdDataDoc.Tables.Item(1).Rows.Add;
  // Fill in the data
  FillRow(wrdDataDoc, 2, 'Steve', 'DeBroux',
        '4567 Main Street', 'Buffalo, NY  98052');
  FillRow(wrdDataDoc, 3, 'Jan', 'Miksovsky',
        '1234 5th Street', 'Charlotte, NC  98765');
  FillRow(wrdDataDoc, 4, 'Brian', 'Valentine',
        '12348 78th Street  Apt. 214', 'Lubbock, TX  25874');
  // Save and close the file
  wrdDataDoc.Save;
  wrdDataDoc.Close(False);
end;
}

procedure TFmContratMSWord.DefineVariaveisEscopo();
var
  K, N: Integer;
begin
  DmodG.ReopenEndereco(TFmContratos(FFmChamou).QrContratosContratante.Value);
  F_NOME_CLI := DmodG.QrEnderecoNOME_ENT.Value;
  F_CPFJ_CLI := DmodG.QrEnderecoCNPJ_TXT.Value;
  F_ENDE_CLI := DmodG.QrEnderecoE_ALL.Value;
  F_LOGR_PRP := DmodG.QrEnderecoTRATO.Value;
  if Length(Geral.SoNumero_TT(F_CPFJ_CLI)) >= 14 then
    F_TIPO_DOC := 'CNPJ'
  else
  if Length(Geral.SoNumero_TT(F_CPFJ_CLI)) >= 11 then
    F_TIPO_DOC := 'CPF'
  else
    F_TIPO_DOC := '';
  //
  DmodG.ReopenEndereco(TFmContratos(FFmChamou).QrContratosContratada.Value);
  F_NOME_EMP := DmodG.QrEnderecoNOME_ENT.Value;
  F_CPFJ_EMP := DmodG.QrEnderecoCNPJ_TXT.Value;
  F_ENDE_EMP := DmodG.QrEnderecoE_ALL.Value;
  F_CIDA_EMP := DmodG.QrEnderecoCIDADE.Value;
  F_LOGR_PPE := DmodG.QrEnderecoTRATO.Value;
  //

  DmodG.ReopenEndereco(TFmContratos(FFmChamou).QrContratosRepreLegal.Value);
  F_NOM_RESP := DmodG.QrEnderecoNOME_ENT.Value;
  F_CID_RESP := DmodG.QrEnderecoCIDADE.Value;
  F_CPF_RESP := DmodG.QrEnderecoCNPJ_TXT.Value;
  F_RGN_RESP := DmodG.QrEnderecoRG.Value;
  F_RGO_RESP := Trim(DmodG.QrEnderecoSSP.Value);
  if F_RGO_RESP <> '' then
    F_RGO_RESP := ' ' + F_RGO_RESP;
  F_RGD_RESP := Geral.FDT(DmodG.QrEnderecoDataRG.Value, 2);
  if F_RGD_RESP <> '' then
    F_RGD_RESP := ' emitido em ' + F_RGD_RESP;
  //

  //
  //DmodG.ReopenEndereco(ValI('Testemun01'));
  DModG.ObtemEnderecoEtiqueta3Linhas(
    TFmContratos(FFmChamou).QrContratosTestemun01.Value, F_EN1_TES1, F_EN2_TES1, F_EN3_TES1);
  F_NOM_TES1 := DmodG.QrEnderecoNOME_ENT.Value;
  F_CPF_TES1 := DmodG.QrEnderecoCNPJ_TXT.Value;

  DModG.ObtemEnderecoEtiqueta3Linhas(
    TFmContratos(FFmChamou).QrContratosTestemun02.Value, F_EN1_TES2, F_EN2_TES2, F_EN3_TES2);
  F_NOM_TES2 := DmodG.QrEnderecoNOME_ENT.Value;
  F_CPF_TES2 := DmodG.QrEnderecoCNPJ_TXT.Value;

  //

  F_ArtDefCli :=  TFmContratos(FFmChamou).QrContratosArtigoDef.Value;
  F_ArtDefLug :=  TFmContratos(FFmChamou).QrContratosLugArtDef.Value;
  //
  K := TFmContratos(FFmChamou).QrContratosPerDefImpl.Value;
  N := TFmContratos(FFmChamou).QrContratosPerQtdImpl.Value;
  F_TempoImpl := Geral.FF0(N) + ' ' + dmkPF.TxtGrandezaPeriodo(K, N > 1);
  //
  K := TFmContratos(FFmChamou).QrContratosPerDefMoni.Value;
  N := TFmContratos(FFmChamou).QrContratosPerQtdMoni.Value;
  F_TempoMoni := Geral.FF0(N) + ' ' + dmkPF.TxtGrandezaPeriodo(K, N > 1);
  //
  K := TFmContratos(FFmChamou).QrContratosPerDefTrei.Value;
  N := TFmContratos(FFmChamou).QrContratosPerQtdTrei.Value;
  F_TempoTrei := Geral.FF0(N) + ' ' + dmkPF.TxtGrandezaPeriodo(K, N > 1);
  //
  K := TFmContratos(FFmChamou).QrContratosPerDefIniI.Value;
  N := TFmContratos(FFmChamou).QrContratosPerQtdIniI.Value;
  F_PrazoImpl := Geral.FF0(N) + ' ' + dmkPF.TxtGrandezaPeriodo(K, N > 1);
  //
  F_VAL_MENS := Geral.FFT(TFmContratos(FFmChamou).QrContratosValorMes.Value, 2, siPositivo);
  F_VAL_MENS := F_VAL_MENS + ' (' + dmkPF.ExtensoMoney(F_VAL_MENS) + ')';
  //
  F_PER_MULT := Geral.FFT(TFmContratos(FFmChamou).QrContratosPercMulta.Value, 2, siPositivo);
  F_PER_MULT := F_PER_MULT + '% (' + dmkPF.ExtensoFloat(F_PER_MULT) + ')';
  F_PER_MORA := Geral.FFT(TFmContratos(FFmChamou).QrContratosPercJuroM.Value, 2, siPositivo);
  F_PER_MORA := F_PER_MORA + '% (' + dmkPF.ExtensoFloat(F_PER_MORA) + ')';
  //
  F_ESCOPOSP := TFmContratos(FFmChamou).QrContratosEscopoSP.Value;
end;

{
procedure TFmContratMSWord.FillRow(Doc: Variant; Row: Integer; Text1, Text2, Text3,
  Text4: String);
begin
  (* ERRO!
  Doc.Tables.Item(1).Cell(Row,1).Range.InsertAfter(Text1);
  Doc.Tables.Item(1).Cell(Row,2).Range.InsertAfter(Text2);
  Doc.Tables.Item(1).Cell(Row,3).Range.InsertAfter(Text3);
  Doc.Tables.Item(1).Cell(Row,4).Range.InsertAfter(Text4);
  *)
end;
}

procedure TFmContratMSWord.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContratMSWord.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UMyMod.AbreQuery(QrOpcoesGerl, Dmod.MyDB);
  EdHost.Text := QrOpcoesGerlContrtIPv4.Value;
  EdSDir.Text := QrOpcoesGerlContrtSDir.Value;
end;

procedure TFmContratMSWord.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContratMSWord.frx_CAD_CONTR_001_03_AGetValue(const VarName: string;
  var Value: Variant);
var
  I, CATipo, CETipo: Integer;
  Achou: Boolean;
  x: String;
begin
  Achou := False;
  if Copy(VarName, 1, Length(CO_IniTagNiv)-1) = Copy(CO_IniTagNiv, 2) then
  begin
    Achou := LocalizaTagNiv(VarName, x);
    if Achou then
      Value := x;
  end;
  if Achou then
    Exit;
  //
  CATipo := TFmContratos(FFmChamou).QrEndContratadaTipo.Value;
  CETipo := TFmContratos(FFmChamou).QrEndContratanteTipo.Value;
  //
  if VarName = 'VARF_CAENDERECO' then
    Value := TFmContratos(FFmChamou).QrEndContratadaNOMELOGRAD.Value + ' ' +
     TFmContratos(FFmChamou).QrEndContratadaNOMERUA.Value + ', N� ' +
     Geral.FFT(TFmContratos(FFmChamou).QrEndContratadaNUMERO.Value, 0, siPositivo) +
     ' ' + TFmContratos(FFmChamou).QrEndContratadaCOMPL.Value;
  if VarName = 'VARF_CAIERG' then
  begin
    case CATipo of
    0:
      if TFmContratos(FFmChamou).QrEndContratadaIE_RG.Value = '' then
        Value := 'Isento'
      else
        Value := TFmContratos(FFmChamou).QrEndContratadaIE_RG.Value;
    1:
      if TFmContratos(FFmChamou).QrEndContratadaIE_RG.Value = '' then
        Value := ''
      else
        Value := TFmContratos(FFmChamou).QrEndContratadaIE_RG.Value;
    end;
  end;
  if VarName = 'VARF_CALACNPJCFP' then
  begin
    if CATipo = 0 then
      Value := 'CNPJ'
    else
      Value := 'CPF';
  end;
  if VarName = 'VARF_CALAIERG' then
  begin
    if CATipo = 0 then
      Value := 'IE'
    else
      Value := 'RG';
  end;
  //
  if VarName = 'VARF_CEENDERECO' then
    Value := TFmContratos(FFmChamou).QrEndContratanteNOMELOGRAD.Value + ' ' +
     TFmContratos(FFmChamou).QrEndContratanteNOMERUA.Value + ', N� ' +
     FormatFloat('0', TFmContratos(FFmChamou).QrEndContratanteNUMERO.Value) + ' ' +
     TFmContratos(FFmChamou).QrEndContratanteCOMPL.Value;
  if VarName = 'VARF_CEIERG' then
  begin
    case CETipo of
    0:
      if TFmContratos(FFmChamou).QrEndContratanteIE_RG.Value = '' then
        Value := 'Isento'
      else
        Value := TFmContratos(FFmChamou).QrEndContratanteIE_RG.Value;
    1:
      if TFmContratos(FFmChamou).QrEndContratanteIE_RG.Value = '' then
        Value := ''
      else
        Value := TFmContratos(FFmChamou).QrEndContratanteIE_RG.Value;
    end;
  end;
  if VarName = 'VARF_CELACNPJCFP' then
  begin
    if CETipo = 0 then
      Value := 'CNPJ'
    else
      Value := 'CPF';
  end;
  if VarName = 'VARF_CELAIERG' then
  begin
    if CETipo = 0 then
      Value := 'IE'
    else
      Value := 'RG';
  end;
  if VarName = 'NUM_CNTR' then
    Value := Geral.FFN(TFmContratos(FFmChamou).QrContratosCodigo.Value, 6) + ' v. ' +
    Geral.FFT(TFmContratos(FFmChamou).QrContratosVersao.Value, 2, siPositivo)
  else
  if VarName = 'NOME_CLI' then
    Value := TFmContratos(FFmChamou).QrContratosNCONTRATANTE.Value
  else
  if VarName = 'NOME_EMP' then
    Value := F_NOME_EMP
  else
  if VarName = 'DATA_CON' then
    Value := Geral.FDT(TFmContratos(FFmChamou).QrContratosDContrato.Value, 3)
  else
  if VarName = 'DATA_COE' then
    Value := Geral.FDT(TFmContratos(FFmChamou).QrContratosDContrato.Value, 16) +
               ' de ' + dmkPF.VerificaMes(Geral.IMV(Geral.FDT(
               TFmContratos(FFmChamou).QrContratosDContrato.Value, 22)), False) +
               ' de ' + Geral.FDT(
               TFmContratos(FFmChamou).QrContratosDContrato.Value, 25)
  else
  if VarName = 'DATA_VAL' then
    Value := Geral.FDT(TFmContratos(FFmChamou).QrContratosDtaPropFim.Value, 3)
  else
  if VarName = 'ESTB_ART' then
    Value := dmkPF.FormaDeTratamentoBR(F_ArtDefLug, trcPara)
  else
  if VarName = 'EST_DESC' then
    Value := TFmContratos(FFmChamou).QrContratosLugarNome.Value
  else
  if VarName = 'ART_O_A_' then
    Value := dmkPF.FormaDeTratamentoBR(F_ArtDefCli, trcOA)
  else
  if VarName = 'EST_O_A_' then
    Value := dmkPF.FormaDeTratamentoBR(F_ArtDefLug, trcOA)
  else
  if VarName = 'TEM_IMPL' then
    Value := F_TempoImpl
  else
  if VarName = 'TEM_MONI' then
    Value := F_TempoMoni
  else
  if VarName = 'TEM_TREI' then
    Value := F_TempoTrei
  else
  if VarName = 'PRZ_IMPL' then
    Value := F_PrazoImpl
  else
  if VarName = 'VAL_MENS' then
    Value := F_VAL_MENS
  else
  if VarName = 'DOC_PAGA' then
    Value := TFmContratos(FFmChamou).QrContratosTpPagDocu.Value
  else
  if VarName = 'LOC_ASIN' then
    Value := F_CIDA_EMP
  else
  if VarName = 'DAT_ASIN' then
    Value := Geral.FDT(TFmContratos(FFmChamou).QrContratosDtaAssina.Value, 3)
  else
  if VarName = 'DAT_ASEX' then
    Value := Geral.FDT(TFmContratos(FFmChamou).QrContratosDtaAssina.Value, 16) +
               ' de ' + dmkPF.VerificaMes(Geral.IMV(Geral.FDT(
               TFmContratos(FFmChamou).QrContratosDtaAssina.Value, 22)), False) +
               ' de ' + Geral.FDT(
               TFmContratos(FFmChamou).QrContratosDtaAssina.Value, 25)
  else
  if VarName = 'ENDR_CLI' then
    Value := F_ENDE_CLI
  else
  if VarName = 'ENDR_EMP' then
    Value := F_ENDE_EMP
  else
  if VarName = 'TIPO_DOC' then
    Value := F_TIPO_DOC
  else
  if VarName = 'DOCU_CLI' then
    Value := F_CPFJ_CLI
  else
  if VarName = 'DOCU_EMP' then
    Value := F_CPFJ_EMP
  else
  if VarName = 'LOGR_PRP' then
    Value := F_LOGR_PRP
  else
  if VarName = 'LOGR_PPE' then
    Value := F_LOGR_PPE
  else
  if VarName = 'NOM_RESP' then
    Value := F_NOM_RESP
  else
  if VarName = 'CID_RESP' then
    Value := F_CID_RESP
  else
  if VarName = 'CPF_RESP' then
    Value := F_CPF_RESP
  else
  if VarName = 'RGN_RESP' then
    Value := F_RGN_RESP
  else
  if VarName = 'RGO_RESP' then
    Value := F_RGO_RESP
  else
  if VarName = 'RGD_RESP' then
    Value := F_RGD_RESP
  else
  if VarName = 'DATA_INT' then
    Value := Geral.FDT(TFmContratos(FFmChamou).QrContratosDInstal.Value, 3)
  else
  if VarName = 'DIA_PAGT' then
    Value := Geral.FF0(TFmContratos(FFmChamou).QrContratosddMesVcto.Value)
  else
  if VarName = 'PER_MULT' then
    Value := F_PER_MULT
  else
  if VarName = 'PER_MORA' then
    Value := F_PER_MORA
  else
  if VarName = 'DAT_ASIN' then
    Value := Geral.FDT(TFmContratos(FFmChamou).QrContratosDtaAssina.Value, 3)
  else
  if VarName = 'NOM_TES1' then
    Value := F_NOM_TES1
  else
  if VarName = 'CPF_TES1' then
    Value := F_CPF_TES1
  else
  if VarName = 'EN1_TES1' then
    Value := F_EN1_TES1
  else
  if VarName = 'EN2_TES1' then
    Value := F_EN2_TES1
  else
  if VarName = 'EN3_TES1' then
    Value := F_EN3_TES1
  else
  if VarName = 'NOM_TES2' then
    Value := F_NOM_TES2
  else
  if VarName = 'CPF_TES2' then
    Value := F_CPF_TES2
  else
  if VarName = 'EN1_TES2' then
    Value := F_EN1_TES2
  else
  if VarName = 'EN2_TES2' then
    Value := F_EN2_TES2
  else
  if VarName = 'EN3_TES2' then
    Value := F_EN3_TES2
  else
  if VarName = 'ESCOPOSP' then
    Value := F_ESCOPOSP
  else
  if Uppercase(Copy(VarName, 1, 4)) = 'PAG_' then
  begin
    I := Geral.IMV(Copy(VarName, 5));
    Value := Geral.FFN(FItsPgs[I], 2);
  end
  else
  if VarName = 'PRAG_SIM' then
    Value := ObtemPragas(FQrPragaS)
  else
  if VarName = 'PRAG_NAO' then
    Value := ObtemPragas(FQrPragaN)
  else
  if VarName = 'SERVICOS' then
    Value := ObtemServicos(FQrServicos)
  else
  if VarName = 'LOCAIS_S' then
    Value := ObtemLocais(FQrLocais)
  else
  if VarName = 'DATA_VEN' then
    Value := Geral.FDT(TFmContratos(FFmChamou).QrContratosDVencimento.Value, 3)
  else
  if VarName = 'DATA_REC' then
    Value := Geral.FDT(TFmContratos(FFmChamou).QrContratosDtaCntrFim.Value, 3)
  else
  if VarName = 'DATA_PRR' then
    Value := Geral.FDT(TFmContratos(FFmChamou).QrContratosDtaPrxRenw.Value, 3);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrContratTag, Dmod.MyDB, [
    'SELECT Text ',
    'FROM contrattag ',
    'WHERE Codigo=' + Geral.FF0(TFmContratos(FFmChamou).QrContratosCodigo.Value),
    'AND Tag="[' + VarName + ']" ',
    '']);
    if QrContratTag.RecordCount > 0 then
      Value := QrContratTagText.Value;
  end;
end;

function TFmContratMSWord.frx_CAD_CONTR_001_03_AUserFunction(
  const MethodName: string; var Params: Variant): Variant;
begin
  if MethodName = Uppercase('SetaPaginaDeItem') then
    Result := SetaPaginaDeItem(Params[0], Params[1]);
end;

function TFmContratMSWord.LocalizaTagNiv(const VarName: String;
  var Value: String): Boolean;
var
  SQL_VarName: String;
begin
 Value := '';
  Result := False;
  if VarName[1]= '[' then
    SQL_VarName := 'AND ni3.Tag = "' + VarName + '"'
  else
    SQL_VarName := 'AND ni3.Tag = "[' + VarName + ']"';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtrTagNiv3, Dmod.MyDB, [
  'SELECT DISTINCT ni3.Codigo, ni3.Conteudo ',
  'FROM contrnivtg cnt ',
  'LEFT JOIN ctrtagniv1 ni1 ON ni1.Codigo=cnt.CtrTagNiv1 ',
  'LEFT JOIN ctrtagniv2 ni2 ON ni2.Codigo=ni1.NivSup ',
  'LEFT JOIN ctrtagniv3 ni3 ON ni3.Codigo=ni2.NivSup ',
  'WHERE cnt.Codigo=' + Geral.FF0(EdCodigo.ValueVariant),
  SQL_VarName,
  '']);
  //
  if QrCtrTagNiv3.RecordCount > 0 then
  begin
    Result := True;
    Value := '';
    QrCtrTagNiv3.First;
    while not QrCtrTagNiv3.Eof do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrCtrTagNiv2, Dmod.MyDB, [
      'SELECT ni2.Ordem, ni2.Codigo, ni2.Conteudo ',
      'FROM ctrtagniv2 ni2 ',
      'WHERE ni2.NivSup=' + Geral.FF0(QrCtrTagNiv3Codigo.Value),
      'ORDER BY ni2.Ordem, ni2.Codigo ',
      '']);
      if (QrCtrTagNiv3Conteudo.Value <> '')
        and (QrCtrTagNiv2.RecordCount > 0) then
        Value := Value + QrCtrTagNiv3Conteudo.Value + sLineBreak;
      while not QrCtrTagNiv2.Eof do
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrCtrTagNiv1, Dmod.MyDB, [
        'SELECT ni1.Ordem, ni1.Codigo, ni1.Conteudo ',
        'FROM ctrtagniv1 ni1',
        'LEFT JOIN contrnivtg cnt ON ni1.Codigo=cnt.CtrTagNiv1 ',
        'WHERE ni1.NivSup=' + Geral.FF0(QrCtrTagNiv2Codigo.Value),
        'AND cnt.Codigo=' + Geral.FF0(EdCodigo.ValueVariant),
        'ORDER BY ni1.Ordem, ni1.Codigo ',
        '']);
        if (QrCtrTagNiv2Conteudo.Value <> '')
        and (QrCtrTagNiv1.RecordCount > 0) then
          Value := Value + QrCtrTagNiv2Conteudo.Value + sLineBreak;
        while not QrCtrTagNiv1.Eof do
        begin
          if QrCtrTagNiv1Conteudo.Value <> '' then
            Value := Value + QrCtrTagNiv1Conteudo.Value + sLineBreak;
          //
          QrCtrTagNiv1.Next;
        end;
        //
        QrCtrTagNiv2.Next;
      end;
      //
      QrCtrTagNiv3.Next;
    end;
  end;
end;

function TFmContratMSWord.ObtemLocais(Qry: TmySQLQuery): String;
var
  Lugar, Local, LastLugar: String;
begin
  LastLugar := '';
  Result := '';
  Qry.First;
  while not Qry.Eof do
  begin
    Result := Trim(Result);
    //
    Lugar := Qry.FieldByName('NO_LUGAR').AsString;
    Local := Qry.FieldByName('NO_LOCAL').AsString;
    //
    if LastLugar <> Lugar then
    begin
      if Result <> '' then
        Result := Result + '. ';
      if Lugar <> '' then
        Result := Result + sLineBreak + Lugar + ': ';
      LastLugar := Lugar;
      if Local <> '' then
        Result := Result + Local;
    end else
    begin
      if Local <> '' then
        Result := Result + ', ' + Local;
    end;
    Qry.Next;
  end;
(*
  if Length(Result) > 2 then
    Result := Copy(Result, 3);
*)
end;

function TFmContratMSWord.ObtemPragas(Qry: TmySQLQuery): String;
begin
  Result := '';
  Qry.First;
  while not Qry.Eof do
  begin
    if Qry.FieldByName('NO_PRAGA').AsString <> '' then
      Result := Result + ', ' + Qry.FieldByName('NO_PRAGA').AsString +
      //' [' + Qry.FieldByName('NO_NIVEL').AsString + ']';
      Qry.FieldByName('NO_ESPECIE').AsString;
    Qry.Next;
  end;
  if Length(Result) > 2 then
    Result := Copy(Result, 3);
end;

function TFmContratMSWord.ObtemServicos(Qry: TmySQLQuery): String;
begin
  Result := '';
  Qry.First;
  while not Qry.Eof do
  begin
    if Qry.FieldByName('NO_SERVICO').AsString <> '' then
      Result := Result + ', ' + Qry.FieldByName('NO_SERVICO').AsString;
    Qry.Next;
  end;
  if Length(Result) > 2 then
    Result := Copy(Result, 3);
end;

function TFmContratMSWord.SetaPaginaDeItem(Pagina, Item: Extended): Boolean;
var
  I: Integer;
begin
  I := Trunc(Item);
  if Item <= FMaxItsPg then
    FItsPgs[I] := Trunc(Pagina);
  Result := True;  
end;

{
procedure TFmContratMSWord.InsertLines(LineNum: Integer);
var
  iCount : Integer;
begin
  for iCount := 1 to LineNum do
     wrdApp.Selection.TypeParagraph;
end;
}

{
procedure TFmTesteWord.ZZZ_ExemploDidaticoMicrosoft();
var
  StrToAdd : String;
  wrdSelection, wrdMailMerge, wrdMergeFields : Variant;
begin
  // Create an instance of Word and make it visible
  wrdApp := CreateOleObject('Word.Application');
  wrdApp.Visible := True;
  // Create a new document
  wrdDoc := wrdApp.Documents.Add();
  wrdDoc.Select;

  wrdSelection := wrdApp.Selection;
  wrdMailMerge := wrdDoc.MailMerge;

  // Create MailMerge data file
  CreateMailMergeDataFile;


  // Create a string and insert it into the document
  StrToAdd := 'State University' + Chr(13) +
              'Electrical Engineering Department';
  wrdSelection.ParagraphFormat.Alignment := wdAlignParagraphCenter;
  wrdSelection.TypeText(StrToAdd);

  InsertLines(4);

  // Insert Merge Data
  wrdSelection.ParagraphFormat.Alignment := wdAlignParagraphLeft;
  wrdMergeFields := wrdMailMerge.Fields;

  wrdMergeFields.Add(wrdSelection.Range,'FirstName');
  wrdSelection.TypeText(' ');
  wrdMergeFields.Add(wrdSelection.Range,'LastName');
  wrdSelection.TypeParagraph;
  wrdMergeFields.Add(wrdSelection.Range,'Address');
  wrdSelection.TypeParagraph;
  wrdMergeFields.Add(wrdSelection.Range,'CityStateZip');

  InsertLines(2);

  // Right justify the line and insert a date field with
  // the current date
  wrdSelection.ParagraphFormat.Alignment := wdAlignParagraphRight;
  wrdSelection.InsertDateTime('dddd, MMMM dd, yyyy',False);

  InsertLines(2);

  // Justify the rest of the document
  wrdSelection.ParagraphFormat.Alignment := wdAlignParagraphJustify;

  wrdSelection.TypeText('Dear ');
  wrdMergeFields.Add(wrdSelection.Range,'FirstName');

  wrdSelection.TypeText(',');
  InsertLines(2);

  // Create a string and insert it into the document
  StrToAdd := 'Thank you for your recent request for next ' +
      'semester''s class schedule for the Electrical ' +
      'Engineering Department.  Enclosed with this ' +
      'letter is a booklet containing all the classes ' +
      'offered next semester at State University.  ' +
      'Several new classes will be offered in the ' +
      'Electrical Engineering Department next semester.  ' +
      'These classes are listed below.';
  wrdSelection.TypeText(StrToAdd);

  InsertLines(2);

  // Insert a new table with 9 rows and 4 columns
  wrdDoc.Tables.Add(wrdSelection.Range,9,4);
  wrdDoc.Tables.Item(1).Columns.Item(1).SetWidth(51,wdAdjustNone);
  wrdDoc.Tables.Item(1).Columns.Item(2).SetWidth(170,wdAdjustNone);
  wrdDoc.Tables.Item(1).Columns.Item(3).SetWidth(100,wdAdjustNone);
  wrdDoc.Tables.Item(1).Columns.Item(4).SetWidth(111,wdAdjustNone);
  // Set the shading on the first row to light gray

  wrdDoc.Tables.Item(1).Rows.Item(1).Cells
      .Shading.BackgroundPatternColorIndex := wdGray25;
  // BOLD the first row
  wrdDoc.Tables.Item(1).Rows.Item(1).Range.Bold := True;
  // Center the text in Cell (1,1)
  wrdDoc.Tables.Item(1).Cell(1,1).Range.Paragraphs.Alignment :=
        wdAlignParagraphCenter;

  // Fill each row of the table with data
  FillRow(wrdDoc, 1, 'Class Number', 'Class Name', 'Class Time',
     'Instructor');
  FillRow(wrdDoc, 2, 'EE220', 'Introduction to Electronics II',
     '1:00-2:00 M,W,F', 'Dr. Jensen');
  FillRow(wrdDoc, 3, 'EE230', 'Electromagnetic Field Theory I',
     '10:00-11:30 T,T', 'Dr. Crump');
  FillRow(wrdDoc, 4, 'EE300', 'Feedback Control Systems',
     '9:00-10:00 M,W,F', 'Dr. Murdy');
  FillRow(wrdDoc, 5, 'EE325', 'Advanced Digital Design',
     '9:00-10:30 T,T', 'Dr. Alley');
  FillRow(wrdDoc, 6, 'EE350', 'Advanced Communication Systems',
     '9:00-10:30 T,T', 'Dr. Taylor');
  FillRow(wrdDoc, 7, 'EE400', 'Advanced Microwave Theory',
     '1:00-2:30 T,T', 'Dr. Lee');
  FillRow(wrdDoc, 8, 'EE450', 'Plasma Theory',
     '1:00-2:00 M,W,F', 'Dr. Davis');
  FillRow(wrdDoc, 9, 'EE500', 'Principles of VLSI Design',
     '3:00-4:00 M,W,F', 'Dr. Ellison');

  // Go to the end of the document

  wrdApp.Selection.GoTo(wdGotoLine,wdGoToLast);
  InsertLines(2);

  // Create a string and insert it into the document
  StrToAdd := 'For additional information regarding the ' +
             'Department of Electrical Engineering, ' +
             'you can visit our website at ';
  wrdSelection.TypeText(StrToAdd);
  // Insert a hyperlink to the web page
  wrdSelection.Hyperlinks.Add(wrdSelection.Range,'http://www.ee.stateu.tld');
  // Create a string and insert it into the document
  StrToAdd := '.  Thank you for your interest in the classes ' +
             'offered in the Department of Electrical ' +
             'Engineering.  If you have any other questions, ' +
             'please feel free to give us a call at ' +
             '555-1212.' + Chr(13) + Chr(13) +
             'Sincerely,' + Chr(13) + Chr(13) +
             'Kathryn M. Hinsch' + Chr(13) +
             'Department of Electrical Engineering' + Chr(13);
  wrdSelection.TypeText(StrToAdd);

  // Perform mail merge
  wrdMailMerge.Destination := wdSendToNewDocument;
  wrdMailMerge.Execute(False);

  // Close the original form document
  wrdDoc.Saved := True;
  wrdDoc.Close(False);

  // Notify the user we are done.
  ShowMessage('Mail Merge Complete.');

  // Clean up temp file
  DeleteFile('C:\DataDoc.doc');
end;
}

end.
