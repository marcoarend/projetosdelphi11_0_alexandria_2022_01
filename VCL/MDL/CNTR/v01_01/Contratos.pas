unit Contratos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UnDmkProcFunc,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  ComCtrls, frxRich, frxClass, frxDBSet, dmkCheckBox, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, Variants, dmkMemo, frxGradient, Menus, Grids,
  DBGrids, (*AdvObj,* BaseGrid, AdvGrid,*) dmkCompoStore, UnDmkEnums,
  UnProjGroup_Consts;

type

  TFmContratos = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtContrato: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrContratos: TmySQLQuery;
    DsContratos: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    BtTexto: TBitBtn;
    QrContratosCodigo: TIntegerField;
    QrContratosContratada: TIntegerField;
    QrContratosContratante: TIntegerField;
    QrContratosDContrato: TDateField;
    QrContratosDInstal: TDateField;
    QrContratosDVencimento: TDateField;
    QrContratosStatus: TSmallintField;
    QrContratosAtivo: TSmallintField;
    QrContratosValorMes: TFloatField;
    QrContratosNCONTRATADA: TWideStringField;
    QrContratosNCONTRATANTE: TWideStringField;
    QrContratosSTATUSSTR: TWideStringField;
    QrEndContratada: TmySQLQuery;
    QrEndContratadaTipo: TSmallintField;
    QrEndContratadaCONTRATADA: TWideStringField;
    QrEndContratadaNOMELOGRAD: TWideStringField;
    QrEndContratadaNOMERUA: TWideStringField;
    QrEndContratadaCOMPL: TWideStringField;
    QrEndContratadaBAIRRO: TWideStringField;
    QrEndContratadaCIDADE: TWideStringField;
    QrEndContratadaNOMEUF: TWideStringField;
    QrEndContratadaCNPJ_CPF: TWideStringField;
    QrEndContratadaIE_RG: TWideStringField;
    QrEndContratadaCodigo: TIntegerField;
    QrEndContratadaTEL: TWideStringField;
    QrEndContratadaFAX: TWideStringField;
    QrEndContratadaTEL_TXT: TWideStringField;
    QrEndContratadaFAX_TXT: TWideStringField;
    QrEndContratadaCNPJ_TXT: TWideStringField;
    QrEndContratante: TmySQLQuery;
    QrEndContratanteCodigo: TIntegerField;
    QrEndContratanteTipo: TSmallintField;
    QrEndContratanteNOMELOGRAD: TWideStringField;
    QrEndContratanteNOMERUA: TWideStringField;
    QrEndContratanteCOMPL: TWideStringField;
    QrEndContratanteBAIRRO: TWideStringField;
    QrEndContratanteCIDADE: TWideStringField;
    QrEndContratanteNOMEUF: TWideStringField;
    QrEndContratanteCNPJ_CPF: TWideStringField;
    QrEndContratanteIE_RG: TWideStringField;
    QrEndContratanteTEL: TWideStringField;
    QrEndContratanteFAX: TWideStringField;
    QrEndContratanteTEL_TXT: TWideStringField;
    QrEndContratanteCONTRATANTE: TWideStringField;
    QrEndContratanteFAX_TXT: TWideStringField;
    QrEndContratanteCNPJ_TXT: TWideStringField;
    DsEndContratada: TDataSource;
    DsEndContratante: TDataSource;
    QrControle: TmySQLQuery;
    QrControleCidade: TWideStringField;
    QrControleUFPadrao: TIntegerField;
    QrControleNOMEUFPADRAO: TWideStringField;
    DsContratante: TDataSource;
    QrContratante: TmySQLQuery;
    QrContratada: TmySQLQuery;
    DsContratada: TDataSource;
    frxDsContratos: TfrxDBDataset;
    frxDsEndContratada: TfrxDBDataset;
    frxDsEndContratante: TfrxDBDataset;
    Panel7: TPanel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label3: TLabel;
    EdContratada: TdmkEditCB;
    CBContratada: TdmkDBLookupComboBox;
    SpeedButton6: TSpeedButton;
    Label4: TLabel;
    EdContratante: TdmkEditCB;
    CBContratante: TdmkDBLookupComboBox;
    SbContratante: TSpeedButton;
    Label10: TLabel;
    EdNome: TdmkEdit;
    Label8: TLabel;
    TPDataContrato: TdmkEditDateTimePicker;
    TPDInstal: TdmkEditDateTimePicker;
    Label6: TLabel;
    TPDVencimento: TdmkEditDateTimePicker;
    dmkRGStatus: TdmkRadioGroup;
    dmkCkAtivo: TdmkCheckBox;
    CkContinuar: TCheckBox;
    Label5: TLabel;
    Label11: TLabel;
    EdWinDocArq: TdmkEdit;
    SbWinDocArq: TSpeedButton;
    RGFormaUso: TdmkRadioGroup;
    QrContratosWinDocArq: TWideStringField;
    QrContratosFormaUso: TSmallintField;
    BtTags: TBitBtn;
    Label14: TLabel;
    EdVersao: TdmkEdit;
    QrContratosNome: TWideStringField;
    QrContratosDtaPropIni: TDateField;
    QrContratosDtaPropFim: TDateField;
    QrContratosDtaCntrFim: TDateField;
    QrContratosDtaImprime: TDateTimeField;
    QrContratosDtaAssina: TDateField;
    QrContratosTratamento: TWideStringField;
    QrContratosArtigoDef: TSmallintField;
    QrContratosPerDefImpl: TSmallintField;
    QrContratosPerQtdImpl: TSmallintField;
    QrContratosPerDefTrei: TSmallintField;
    QrContratosPerQtdTrei: TSmallintField;
    QrContratosPerDefMoni: TSmallintField;
    QrContratosPerQtdMoni: TSmallintField;
    QrContratosPerDefIniI: TSmallintField;
    QrContratosPerQtdIniI: TSmallintField;
    QrContratosRepreLegal: TIntegerField;
    QrContratosTestemun01: TIntegerField;
    QrContratosTestemun02: TIntegerField;
    QrContratosPercMulta: TFloatField;
    QrContratosPercJuroM: TFloatField;
    QrContratosTpPagDocu: TWideStringField;
    QrContratosddMesVcto: TSmallintField;
    TPDtaPropIni: TdmkEditDateTimePicker;
    Label17: TLabel;
    Label22: TLabel;
    TPDtaPropFim: TdmkEditDateTimePicker;
    Label23: TLabel;
    TPDtaImprime: TdmkEditDateTimePicker;
    Label24: TLabel;
    TPDtaAssina: TdmkEditDateTimePicker;
    TPDtaCntrFim: TdmkEditDateTimePicker;
    Label25: TLabel;
    EdTratamento: TdmkEdit;
    Label13: TLabel;
    RGArtigoDef: TdmkRadioGroup;
    GroupBox1: TGroupBox;
    RGPerDefIniI: TdmkRadioGroup;
    EdPerQtdIniI: TdmkEdit;
    Label26: TLabel;
    Label28: TLabel;
    EdRepreLegal: TdmkEditCB;
    CBRepreLegal: TdmkDBLookupComboBox;
    SbRepreLegal: TSpeedButton;
    Label29: TLabel;
    EdTestemun01: TdmkEditCB;
    CBTestemun01: TdmkDBLookupComboBox;
    SbTestemun01: TSpeedButton;
    EdTestemun02: TdmkEditCB;
    Label30: TLabel;
    CBTestemun02: TdmkDBLookupComboBox;
    SbTestemun02: TSpeedButton;
    QrTestemun01: TmySQLQuery;
    QrRepreLegal: TmySQLQuery;
    DsRepreLegal: TDataSource;
    DsTestemun01: TDataSource;
    QrTestemun02: TmySQLQuery;
    DsTestemun02: TDataSource;
    Label31: TLabel;
    EdPercMulta: TdmkEdit;
    EdValorAluguel: TdmkEdit;
    Label9: TLabel;
    EdPercJuroM: TdmkEdit;
    Label32: TLabel;
    Label33: TLabel;
    EdTpPagDocu: TdmkEdit;
    Label34: TLabel;
    EdddMesVcto: TdmkEdit;
    QrContratosTexto: TWideMemoField;
    GroupBox2: TGroupBox;
    Label35: TLabel;
    RGPerDefImpl: TdmkRadioGroup;
    EdPerQtdImpl: TdmkEdit;
    GroupBox3: TGroupBox;
    Label36: TLabel;
    RGPerDefTrei: TdmkRadioGroup;
    EdPerQtdTrei: TdmkEdit;
    GroupBox4: TGroupBox;
    Label37: TLabel;
    RGPerDefMoni: TdmkRadioGroup;
    EdPerQtdMoni: TdmkEdit;
    QrContratosNO_REP_LEGAL: TWideStringField;
    QrContratosNO_TESTEM_01: TWideStringField;
    QrContratosNO_TESTEM_02: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GBDados: TGroupBox;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label27: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit3: TdmkDBEdit;
    CkAtivo: TDBCheckBox;
    dmkDBEdit7: TdmkDBEdit;
    dmkDBEdit6: TdmkDBEdit;
    dmkDBEdit8: TdmkDBEdit;
    dmkDBEdit9: TdmkDBEdit;
    dmkDBEdit4: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    GroupBox5: TGroupBox;
    Label47: TLabel;
    dmkRadioGroup1: TDBRadioGroup;
    dmkDBEdit21: TdmkDBEdit;
    GroupBox6: TGroupBox;
    Label48: TLabel;
    dmkRadioGroup2: TDBRadioGroup;
    dmkDBEdit22: TdmkDBEdit;
    GroupBox7: TGroupBox;
    Label49: TLabel;
    dmkRadioGroup3: TDBRadioGroup;
    dmkDBEdit23: TdmkDBEdit;
    GroupBox8: TGroupBox;
    Label50: TLabel;
    dmkRadioGroup4: TDBRadioGroup;
    dmkDBEdit24: TdmkDBEdit;
    dmkDBEdit5: TdmkDBEdit;
    dmkDBEdit10: TdmkDBEdit;
    dmkDBEdit11: TdmkDBEdit;
    dmkDBEdit12: TdmkDBEdit;
    dmkDBEdit13: TdmkDBEdit;
    DBRadioGroup2: TDBRadioGroup;
    DBRGStatus: TDBRadioGroup;
    dmkDBEdit14: TdmkDBEdit;
    dmkDBEdit15: TdmkDBEdit;
    dmkDBEdit16: TdmkDBEdit;
    dmkDBEdit17: TdmkDBEdit;
    dmkDBEdit18: TdmkDBEdit;
    dmkDBEdit19: TdmkDBEdit;
    dmkDBEdit20: TdmkDBEdit;
    RGLugArtDef: TdmkRadioGroup;
    Label55: TLabel;
    EdLugarNome: TdmkEdit;
    QrContratosLugarNome: TWideStringField;
    QrContratosLugArtDef: TSmallintField;
    Label56: TLabel;
    DBRadioGroup4: TDBRadioGroup;
    DBEdit4: TDBEdit;
    QrEndContratadaNUMERO: TFloatField;
    PMTags: TPopupMenu;
    Listadospadres1: TMenuItem;
    Incluirpersonalizado1: TMenuItem;
    Alterarpersonalizado1: TMenuItem;
    Excluirpersonalizado1: TMenuItem;
    DsContratTag: TDataSource;
    QrContratTag: TmySQLQuery;
    QrContratTagCodigo: TIntegerField;
    QrContratTagControle: TIntegerField;
    QrContratTagNome: TWideStringField;
    QrContratTagTag: TWideStringField;
    QrContratTagText: TWideMemoField;
    QrContratosVersao: TFloatField;
    QrEndContratanteNUMERO: TFloatField;
    Listadesugestes1: TMenuItem;
    CadastrodeGruposdeTags1: TMenuItem;
    SeleodeTagsdeGrupo1: TMenuItem;
    QrCDI: TmySQLQuery;
    QrCDICodigo: TIntegerField;
    QrCDIControle: TIntegerField;
    QrCDINome: TWideStringField;
    QrCDITag: TWideStringField;
    QrCDIText: TWideMemoField;
    QrCDILk: TIntegerField;
    QrCDIDataCad: TDateField;
    QrCDIDataAlt: TDateField;
    QrCDIUserCad: TIntegerField;
    QrCDIUserAlt: TIntegerField;
    QrCDIAlterWeb: TSmallintField;
    QrCDIAtivo: TSmallintField;
    PMWinDocArq: TPopupMenu;
    Localizar1: TMenuItem;
    Editar1: TMenuItem;
    QrOpcoesGerl: TmySQLQuery;
    QrOpcoesGerlContrtIPv4: TWideStringField;
    QrOpcoesGerlContrtSDir: TWideStringField;
    N3: TMenuItem;
    CadastrodeTagsdeNveis1: TMenuItem;
    InclusodeTagsdeNveis1: TMenuItem;
    QrContrNivTg: TmySQLQuery;
    DsContrNivTg: TDataSource;
    QrContrNivTgnNiv3: TWideStringField;
    QrContrNivTgnNiv2: TWideStringField;
    QrContrNivTgnNiv1: TWideStringField;
    QrContrNivTgCodigo: TIntegerField;
    QrContrNivTgControle: TIntegerField;
    QrContrNivTgCtrTagNiv1: TIntegerField;
    QrContrBugsS: TmySQLQuery;
    DsContrBugsS: TDataSource;
    PMContrato: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    QrContrBugsSCodigo: TIntegerField;
    QrContrBugsSControle: TIntegerField;
    QrContrBugsSPraga_A: TIntegerField;
    QrContrBugsSPraga_Z: TIntegerField;
    QrContrBugsSNO_NIVEL: TWideStringField;
    QrContrBugsSNO_PRAGA: TWideStringField;
    QrContrBugsN: TmySQLQuery;
    DsContrBugsN: TDataSource;
    QrContrBugsNCodigo: TIntegerField;
    QrContrBugsNControle: TIntegerField;
    QrContrBugsNPraga_A: TIntegerField;
    QrContrBugsNPraga_Z: TIntegerField;
    QrContrBugsNNO_NIVEL: TWideStringField;
    QrContrBugsNNO_PRAGA: TWideStringField;
    agsPersonalizadas1: TMenuItem;
    N4: TMenuItem;
    Gruposdetags1: TMenuItem;
    a1: TMenuItem;
    N1: TMenuItem;
    Pragasinclusas1: TMenuItem;
    Pragasexclusas1: TMenuItem;
    Servioscontemplados1: TMenuItem;
    QrContrServi: TmySQLQuery;
    DsContrServi: TDataSource;
    QrContrServiNO_SERVICO: TWideStringField;
    QrContrServiCodigo: TIntegerField;
    QrContrServiControle: TIntegerField;
    QrContrServiServico: TIntegerField;
    Label57: TLabel;
    EdEscopoSP: TdmkEdit;
    QrContratosEscopoSP: TWideStringField;
    Label58: TLabel;
    DBEdit5: TDBEdit;
    QrContratLoc: TmySQLQuery;
    DsContratLoc: TDataSource;
    BtLocais: TBitBtn;
    QrContratLocControle: TIntegerField;
    QrContratLocTabela: TSmallintField;
    QrContratLocCadastro: TIntegerField;
    QrContratLocNO_LUGAR: TWideStringField;
    QrContratLocNO_LOCAL: TWideStringField;
    QrRepreLegalCodigo: TIntegerField;
    QrRepreLegalNO_ENT: TWideStringField;
    QrTestemun02Codigo: TIntegerField;
    QrTestemun02NO_ENT: TWideStringField;
    QrContratanteCodigo: TIntegerField;
    QrContratanteNO_ENT: TWideStringField;
    QrTestemun01Codigo: TIntegerField;
    QrTestemun01NO_ENT: TWideStringField;
    QrContratadaCodigo: TIntegerField;
    QrContratadaNO_ENT: TWideStringField;
    QrContrBugsNNO_ESPECIE: TWideStringField;
    QrContrBugsSNO_ESPECIE: TWideStringField;
    TPDtaPrxRenw: TdmkEditDateTimePicker;
    Label59: TLabel;
    QrContratosDtaPrxRenw: TDateField;
    Label60: TLabel;
    dmkDBEdit25: TdmkDBEdit;
    PMQuery: TPopupMenu;
    peloNomedocontrato1: TMenuItem;
    pelosenvolvidos1: TMenuItem;
    CSTabSheetChamou: TdmkCompoStore;
    PMLocais: TPopupMenu;
    Prprios1: TMenuItem;
    DeSubClientes1: TMenuItem;
    QrContratLocNO_ENT: TWideStringField;
    DBEdit7: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    N2: TMenuItem;
    Duplicacontratoatual1: TMenuItem;
    RECarta: TRichEdit;
    TabSheet7: TTabSheet;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    GroupBox9: TGroupBox;
    DBGOSCabAlv: TDBGrid;
    GroupBox10: TGroupBox;
    DBGrid2: TDBGrid;
    GroupBox11: TGroupBox;
    DBGrid3: TDBGrid;
    TabSheet4: TTabSheet;
    TabSheet3: TTabSheet;
    DBGrid1: TDBGrid;
    DBMemo1: TDBMemo;
    TabSheet6: TTabSheet;
    DBGrid4: TDBGrid;
    QrContratLocCliente: TFloatField;
    QrContratosDContrato_TXT: TWideStringField;
    QrContratosDInstal_TXT: TWideStringField;
    QrContratosDVencimento_TXT: TWideStringField;
    QrContratosDtaPropFim_TXT: TWideStringField;
    QrContratosDtaPropIni_TXT: TWideStringField;
    QrContratosDtaCntrFim_TXT: TWideStringField;
    QrContratosDtaAssina_TXT: TWideStringField;
    QrContratosDtaPrxRenw_TXT: TWideStringField;
    QrContratosDtaImprime_TXT: TWideStringField;
    Panel8: TPanel;
    FindDialog1: TFindDialog;
    BtLocaliza: TBitBtn;
    N5: TMenuItem;
    Alterarstatusdocontratoatual1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrContratosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrContratosBeforeOpen(DataSet: TDataSet);
    procedure BtContratoClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrContratosCalcFields(DataSet: TDataSet);
    procedure QrEndContratadaCalcFields(DataSet: TDataSet);
    procedure QrEndContratanteCalcFields(DataSet: TDataSet);
    procedure BtTextoClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SbContratanteClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbWinDocArqClick(Sender: TObject);
    procedure BtTagsClick(Sender: TObject);
    procedure Listadospadres1Click(Sender: TObject);
    procedure Incluirpersonalizado1Click(Sender: TObject);
    procedure Alterarpersonalizado1Click(Sender: TObject);
    procedure QrContratosBeforeClose(DataSet: TDataSet);
    procedure QrContratosAfterScroll(DataSet: TDataSet);
    procedure Listadesugestes1Click(Sender: TObject);
    procedure CadastrodeTagspersonalizados1Click(Sender: TObject);
    procedure CadastrodeGruposdeTags1Click(Sender: TObject);
    procedure SeleodeTagsdeGrupo1Click(Sender: TObject);
    procedure Excluirpersonalizado1Click(Sender: TObject);
    procedure Localizar1Click(Sender: TObject);
    procedure Editar1Click(Sender: TObject);
    procedure CadastrodeTagsdeNveis1Click(Sender: TObject);
    procedure InclusodeTagsdeNveis1Click(Sender: TObject);
    procedure QrContrNivTgAfterOpen(DataSet: TDataSet);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure PMContratoPopup(Sender: TObject);
    procedure Pragasinclusas1Click(Sender: TObject);
    procedure Pragasexclusas1Click(Sender: TObject);
    procedure Servioscontemplados1Click(Sender: TObject);
    procedure BtLocaisClick(Sender: TObject);
    procedure QrContratosDtaCntrFimGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure peloNomedocontrato1Click(Sender: TObject);
    procedure pelosenvolvidos1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Prprios1Click(Sender: TObject);
    procedure DeSubClientes1Click(Sender: TObject);
    procedure Duplicacontratoatual1Click(Sender: TObject);
    procedure SbRepreLegalClick(Sender: TObject);
    procedure SbTestemun01Click(Sender: TObject);
    procedure SbTestemun02Click(Sender: TObject);
    procedure BtLocalizaClick(Sender: TObject);
    procedure FindDialog1Find(Sender: TObject);
    procedure Alterarstatusdocontratoatual1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
    //
    procedure MostraFormContratTag(SQLType: TSQLType);
    procedure ReopenContratLoc(Controle: Integer);
    procedure ReopenContratTag(Controle: Integer);
    procedure MostraOSCabAlv(Tabela: String; Query: TmySQLQuery);
    procedure MostrarFormContratLoc(SubCliente: Boolean; Entidade: Integer);
    procedure ReabreQuerys();
    function  SelecionaEntidade(): Integer;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenEndContratada(Entidade: Integer);
    procedure ReopenEndContratante(Entidade: Integer);
    {$IfDef CO_DMKID_APP_0024}
    procedure ReopenContrBugsN(Controle: Integer);
    procedure ReopenContrBugsS(Controle: Integer);
    procedure ReopenContrServi(Controle: Integer);
    procedure ReopenContrNivTg(Controle: Integer);
    {$EndIf}
  end;

var
  FmContratos: TFmContratos;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Textos, ModuleGeral, UnAppListas, MyDBCheck,
  ContrDfCab,
  ContratMSWord, ContratTag, DmkDAC_PF, CfgCadLista, SelCod, UCreate, MyGlyfs,
  {$IfDef CO_DMKID_APP_0024} OSCabAlv, ContrNivTg, ContratLoc, {$EndIf}
  DesignerFR3, frxRichEdit, frxRichEditor, CtrTagNivCab, MyListas,
  UnContratUnit, Principal;

{$R *.DFM}

const
  FColsToMerge: array[0..2] of Integer = (1,2,3);


/////////////////////////////////////////////////////////////////////////////////////
procedure TFmContratos.Listadesugestes1Click(Sender: TObject);
var
  Lista: String;
begin
  // B u g s t r o l
  if CO_DMKID_APP = 24 then
  begin
    Lista := Geral.ATS([
    '[PRAG_ALV] : Pragas alvo',
    '[ESCOPOSP] : Limpeza de Caixa d`�gua',
    '[SERVICOS] : Servi�os oferecidos',
    '[REL_MONI] : Relat�rio de monitoramento'
    ]);
  end else
    Lista := 'Aplicativo sem sugest�es implementadas!';
  Geral.MB_Info(Lista);
end;

procedure TFmContratos.Listadospadres1Click(Sender: TObject);
var
  Lista: String;
begin
  Lista := Geral.ATS(sListaContratos);
  Geral.MB_Info(Lista);
end;

procedure TFmContratos.Localizar1Click(Sender: TObject);
begin
  if MyObjects.DefineArquivo1(Self, EdWinDocArq) then
    EdWinDocArq.Text := ExtractFileName(EdWinDocArq.Text);
end;

procedure TFmContratos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmContratos.MostraEdicao(Mostra: Boolean; SQLType: TSQLType;
  Codigo: Integer);
begin
  if Mostra then
  begin
    PnDados.Visible := False;
    PnEdita.Visible := True;
    if SQLType = stIns then
    begin
      EdContratada.ValueVariant    := 0;
      CBContratada.KeyValue        := NULL;
      EdContratante.ValueVariant   := 0;
      CBContratante.KeyValue       := NULL;
      TPDataContrato.Date          := Date;
      TPDInstal.Date               := Date;
      TPDVencimento.Date           := Date;
      EdValorAluguel.ValueVariant  := 0;
      EdNome.ValueVariant          := '';
      dmkRGStatus.ItemIndex        := -1;
      dmkCkAtivo.Checked           := true;
    end else begin
      EdCodigo.ValueVariant        := QrContratosCodigo.Value;
      EdContratada.ValueVariant    := QrContratosContratada.Value;
      CBContratada.KeyValue        := QrContratosContratada.Value;
      EdContratante.ValueVariant   := QrContratosContratante.Value;
      CBContratante.KeyValue       := QrContratosContratante.Value;
      TPDataContrato.Date          := QrContratosDContrato.Value;
      TPDInstal.Date               := QrContratosDContrato.Value;
      TPDVencimento.Date           := QrContratosDVencimento.Value;
      EdValorAluguel.ValueVariant  := QrContratosValorMes.Value;
      EdNome.ValueVariant          := QrContratosNome.Value;
      dmkRGStatus.ItemIndex        := QrContratosStatus.Value;
      dmkCkAtivo.Checked           := dmkPF.ITB(QrContratosAtivo.Value);
    end;
    EdContratada.SetFocus;
  end else begin
    PnDados.Visible := True;
    PnEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmContratos.MostraFormContratTag(SQLType: TSQLType);
{
var
  Conta: Integer;
}
begin
  //Conta := 0;
  if DBCheck.CriaFm(TFmContratTag, FmContratTag, afmoNegarComAviso) then
  begin
    FmContratTag.ImgTipo.SQLType := SQLType;
    FmContratTag.FQrCab := QrContratos;
    FmContratTag.FDsCab := DsContratos;
    FmContratTag.FQrIts := QrContratTag;
    //
    if SQLType = stUpd then
    begin
      FmContratTag.EdControle.ValueVariant := QrContratTagControle.Value;
      FmContratTag.EdNome.Text := QrContratTagNome.Value;
      FmContratTag.EdTag.Text := QrContratTagTag.Value;
      FmContratTag.MeText.Text := QrContratTagText.Value;
    end;
    FmContratTag.ShowModal;
    FmContratTag.Destroy;
  end;
end;

procedure TFmContratos.MostraOSCabAlv(Tabela: String; Query: TmySQLQuery);
begin
  {$IfDef CO_DMKID_APP_0024}
  if DBCheck.CriaFm(TFmOSCabAlv, FmOSCabAlv, afmoNegarComAviso) then
  begin
    FmOSCabAlv.FTabela := Tabela;
    FmOSCabAlv.ImgTipo.SQLType := stIns;
    FmOSCabAlv.FQrSelect := Query;
    FmOSCabAlv.FQrInsere := Dmod.QrUpd;
    FmOSCabAlv.FCodigo := QrContratosCodigo.Value;
    //
{
    FmOSCabAlv.QrOSSrv.Close;
    FmOSCabAlv.QrOSSrv.SQL.Text := Query.SQL.Text;
    FmOSCabAlv.QrOSSrv.Params := Query.Params;
    UMyMod.AbreQuery(FmOSCabAlv.QrOSSrv, Dmod.MyDB);
}
    //
    FmOSCabAlv.ShowModal;
    FmOSCabAlv.Destroy;
  end;
  {$EndIf}
end;

procedure TFmContratos.peloNomedocontrato1Click(Sender: TObject);
begin
  LocCod(QrContratosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'contratos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmContratos.pelosenvolvidos1Click(Sender: TObject);
var
  Contrato: Integer;
begin
  if ContratUnit.PesquisaNumeroDoContrato(VAR_LIB_EMPRESA_SEL, 0, Contrato) then
    LocCod(Contrato, Contrato);
end;

procedure TFmContratos.PMContratoPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := Geral.IntToBool_0(QrContratosCodigo.Value);
  //
  Altera1.Enabled                       := Habilita;
  Duplicacontratoatual1.Enabled         := Habilita;
  Alterarstatusdocontratoatual1.Enabled := Habilita;
end;

procedure TFmContratos.Pragasexclusas1Click(Sender: TObject);
begin
  {$IfDef CO_DMKID_APP_0024}
  MostraOSCabAlv('ContrBugsN', QrContrBugsN);
  {$EndIf}
end;

procedure TFmContratos.Pragasinclusas1Click(Sender: TObject);
begin
  {$IfDef CO_DMKID_APP_0024}
  MostraOSCabAlv('ContrBugsS', QrContrBugsS);
  {$EndIf}
end;

procedure TFmContratos.Prprios1Click(Sender: TObject);
begin
  //MostrarFormContratLoc(False, QrContratosContratante.Value);
end;

procedure TFmContratos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrContratosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmContratos.DefParams;
begin
  VAR_GOTOTABELA := 'contratos';
  VAR_GOTOMYSQLTABLE := QrContratos;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  {Mostrar inativos
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := 'Ativo = 1';
  }
  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT con.*,');
  VAR_SQLx.Add('IF(DContrato < "1900-01-01", "", DATE_FORMAT(DContrato, "%d/%m/%Y")) DContrato_TXT,');
  VAR_SQLx.Add('IF(DInstal < "1900-01-01", "", DATE_FORMAT(DInstal, "%d/%m/%Y")) DInstal_TXT,');
  VAR_SQLx.Add('IF(DVencimento < "1900-01-01", "", DATE_FORMAT(DVencimento, "%d/%m/%Y")) DVencimento_TXT,');
  VAR_SQLx.Add('IF(DtaPropIni < "1900-01-01", "", DATE_FORMAT(DtaPropIni, "%d/%m/%Y")) DtaPropIni_TXT,');
  VAR_SQLx.Add('IF(DtaPropFim < "1900-01-01", "", DATE_FORMAT(DtaPropFim, "%d/%m/%Y")) DtaPropFim_TXT,');
  VAR_SQLx.Add('IF(DtaCntrFim < "1900-01-01", "", DATE_FORMAT(DtaCntrFim, "%d/%m/%Y")) DtaCntrFim_TXT,');
  VAR_SQLx.Add('IF(DtaAssina < "1900-01-01", "", DATE_FORMAT(DtaAssina, "%d/%m/%Y")) DtaAssina_TXT,');
  VAR_SQLx.Add('IF(DtaPrxRenw < "1900-01-01", "", DATE_FORMAT(DtaPrxRenw, "%d/%m/%Y")) DtaPrxRenw_TXT,');
  VAR_SQLx.Add('IF(DtaImprime < "1900-01-01", "", DATE_FORMAT(DtaImprime, "%d/%m/%Y %h:%m:%s")) DtaImprime_TXT,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NCONTRATADA,');
  VAR_SQLx.Add('IF(enb.Tipo=0, enb.RazaoSocial, enb.Nome) NCONTRATANTE,');
  VAR_SQLx.Add('IF(rep.Tipo=0, rep.RazaoSocial, rep.Nome) NO_REP_LEGAL,');
  VAR_SQLx.Add('IF(ts1.Tipo=0, ts1.RazaoSocial, ts1.Nome) NO_TESTEM_01,');
  VAR_SQLx.Add('IF(ts2.Tipo=0, ts2.RazaoSocial, ts2.Nome) NO_TESTEM_02');
  VAR_SQLx.Add('FROM contratos con');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo = con.Contratada');
  VAR_SQLx.Add('LEFT JOIN entidades enb ON enb.Codigo = con.Contratante');
  VAR_SQLx.Add('LEFT JOIN entidades rep ON rep.Codigo = con.RepreLegal');
  VAR_SQLx.Add('LEFT JOIN entidades ts1 ON ts1.Codigo = con.Testemun01');
  VAR_SQLx.Add('LEFT JOIN entidades ts2 ON ts2.Codigo = con.Testemun02');
  VAR_SQLx.Add('WHERE con.Codigo <> 0');
  //
  VAR_SQL1.Add('AND con.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND con.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND con.Nome Like :P0');
  //
end;

procedure TFmContratos.DeSubClientes1Click(Sender: TObject);
{
const
  Aviso  = '...';
  Titulo = 'Sele��o de Sub-Cliente';
  Prompt = 'Informe o sub-cliente:';
  Campo  = 'Descricao';
var
  Entidade: Integer;
}
begin
{
  Entidade := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
  'SELECT ent.Codigo,  ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Descricao ',
  'FROM cunsits cug ',
  'LEFT JOIN entidades ent ON ent.Codigo=cug.CunsSub ',
  'WHERE cug.Cunsgru=' + Geral.FF0(QrContratosContratante.Value),
  'ORDER BY Descricao ',
  ' ',
  ''], Dmod.MyDB, True);
  if Entidade <> Null then
    MostrarFormContratLoc(True, Entidade);
}
end;

procedure TFmContratos.Duplicacontratoatual1Click(Sender: TObject);
var
  MesmoContratante: Byte;
  Contratante, Codigo, Controle: Integer;
begin
  Screen.Cursor := crHourGlass;
  // Evitar interfer�ncia do usu�rio nas grids (tabelas) durante o processo de duplica��o
  PnDados.Enabled := False;
  try
    //Result := False;
    MesmoContratante := MyObjects.SelRadioGroup('Duplica��o de Contrato',
    'O contratante ser� o mesmo deste contrato?',
    ['N�o, ser� outro contratante', 'Sim, ser� o mesmo contratante'], -1);
    case MesmoContratante of
      0: Contratante := SelecionaEntidade();
      1: Contratante := QrContratosContratante.Value;
      else Contratante := 0;
    end;
    if Contratante = 0 then
    begin
      Geral.MB_Aviso('Duplica��o abortada. Contratante n�o definido!');
      Exit;
    end;
    Codigo := UMyMod.BuscaEmLivreY_Def('Contratos', 'Codigo', stIns, 0);
    if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'contratos', TMeuDB,
    ['Codigo'], [QrContratosCodigo.Value],
    ['Codigo', 'Contratante'], [Codigo, Contratante],
    '', True, LaAviso1, LaAviso2) then
    begin
  {$IfDef CO_DMKID_APP_0024}
      PageControl2.ActivePageIndex := 0;
      // ContrBugsS
      QrContrBugsS.First;
      while not QrContrBugsS.Eof do
      begin
        Controle := UMyMod.BPGS1I32_Reaproveita('ContrBugsS', 'Controle', '', '', tsDef, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'ContrBugsS', TMeuDB,
        ['Controle'], [QrContrBugsSControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrContrBugsS.Next;
      end;
      //
      // ContrBugsN
      QrContrBugsN.First;
      while not QrContrBugsN.Eof do
      begin
        Controle := UMyMod.BPGS1I32_Reaproveita('ContrBugsN', 'Controle', '', '', tsDef, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'ContrBugsN', TMeuDB,
        ['Controle'], [QrContrBugsNControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrContrBugsN.Next;
      end;
      //
      // ContrServi
      QrContrServi.First;
      while not QrContrServi.Eof do
      begin
        Controle := UMyMod.BPGS1I32_Reaproveita('ContrServi', 'Controle', '', '', tsDef, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'ContrServi', TMeuDB,
        ['Controle'], [QrContrServiControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrContrServi.Next;
      end;
      //
      // ContrNivTg
      PageControl2.ActivePageIndex := 1;
      QrContrNivTg.First;
      while not QrContrNivTg.Eof do
      begin
        Controle := UMyMod.BPGS1I32_Reaproveita('ContrNivTg', 'Controle', '', '', tsDef, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'ContrNivTg', TMeuDB,
        ['Controle'], [QrContrNivTgControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrContrNivTg.Next;
      end;
      //
      // ContratTag
      PageControl2.ActivePageIndex := 2;
      QrContratTag.First;
      while not QrContratTag.Eof do
      begin
        Controle := UMyMod.BPGS1I32_Reaproveita('ContratTag', 'Controle', '', '', tsDef, stIns, 0);
        //
        UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'ContratTag', TMeuDB,
        ['Controle'], [QrContratTagControle.Value],
        ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
        //
        QrContratTag.Next;
      end;
      //
      PageControl2.ActivePageIndex := 3;
      if MesmoContratante = 1 then
      begin
        // ContratLoc
        QrContratLoc.First;
        while not QrContratLoc.Eof do
        begin
          Controle := UMyMod.BPGS1I32_Reaproveita('ContratLoc', 'Controle', '', '', tsDef, stIns, 0);
          //
          UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'ContratLoc', TMeuDB,
          ['Controle'], [QrContratLocControle.Value],
          ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
          //
          QrContratLoc.Next;
        end;
      end else
      begin
        LocCod(Codigo, Codigo);
        MostrarFormContratLoc(False, QrContratosContratante.Value);
      end;
      //
  {$EndIf}
      //Result := True;
      LocCod(Codigo, Codigo);
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    PnDados.Enabled := True;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmContratos.Editar1Click(Sender: TObject);
var
  Host, SDir, RDir, NoArq: String;
  Empresa: Integer;
  DirSorc, DirDest: String;
  //
  //P: Integer;
var
  //Word, Contrato, Doc: Variant;
  //ArqResul, Extensao2,
  Extensao1: String;
  //I: Integer;
begin
  UMyMod.AbreQuery(QrOpcoesGerl, Dmod.MyDB);
  Host := QrOpcoesGerlContrtIPv4.Value;
  SDir := QrOpcoesGerlContrtSDir.Value;
  RDir := '';
  Empresa := EdContratada.ValueVariant;
  NoArq := EdWinDocArq.Text;
  Extensao1 := ExtractFileExt(NoArq);
{
  case RGFormaUso.Itemindex of
    2: Extensao2 := '.doc';
    3: Extensao2 := '.fr3';
  end;
  if Lowercase(Extensao2) <> Lowercase(Extensao1) then
  begin
    if Geral.MB_Pergunta('Extens�o de arquivo inv�lida: "' + Extensao1 +
    '". Extens�o deve ser "' + Extensao2 + '". Deseja continuar assim mesmo?') <> ID_YES then
      Exit;
  end;
}
  //
  if MyObjects.TentaDefinirDiretorio(Host, SDir, Empresa, RDir, LaAviso1, LaAviso2, True, nil, DirDest) then
  if MyObjects.TentaDefinirDiretorio(Host, SDir, 0, RDir, LaAviso1, LaAviso2, False, nil, DirSorc) then
  begin
    NoArq := dmkPF.CaminhoArquivo(DirSorc, NoArq, '');
    if Lowercase(Extensao1) = '.fr3' then
    begin
      if DBCheck.CriaFm(TFmDesignerFR3, FmDesignerFR3, afmoNegarComAviso) then
      begin
        FmDesignerFR3.FArqIni := NoArq;
        //FmDesignerFR3.AbreArquivo(NoArq);
        FmDesignerFR3.ShowModal;
        FmDesignerFR3.Destroy;
      end;
    end else
      Geral.AbreArquivo(NoArq);
  end;
{
    case RGFormaUso.Itemindex of
      2:
      begin
        Cursor := crHourGlass;
        try
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo: ' + NoArq);
          NoArq := dmkPF.CaminhoArquivo(DirSorc, NoArq, '');
          Word := CreateOleObject('Word.Application');
          //Word.Visible := True;
          Contrato := Word.Documents;
          Doc := Contrato.Open(NoArq);
          //Doc.Content.Find.Execute(FindText := '[NUM_CNTR]', ReplaceWith := '000.001.234');
          AppListas.ReplaceVarsContratoMSWord(Doc, FQrContratos, LaAviso1, LaAviso2);
          ArqResul := 'C' + Geral.FFN(EdCodigo.ValueVariant, 9) +
          FormatDateTime('_YYYYDDMM_HHNNSS', Now());
          NoArq := dmkPF.CaminhoArquivo(DirDest, ArqResul, '');
          EdArqResul.Text := ArqResul;
          //Doc.ActiveDocument.SaveAs(FileName := NoArq);
          Doc.SaveAs(FileName := NoArq);
          Word.Visible := True;
          //
          MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
        finally
          Cursor := crDefault;
        end;
      end;
      3:
      begin
        Cursor := crHourGlass;
        try
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Definindo vari�veis: ' + NoArq);
          DefineVariaveisEscopo();

          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando relat�rio customizado: ' + NoArq);
          frx_CAD_CONTR_001_03_A.LoadFromFile(NoArq);

          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando relat�rio!');
          MyObjects.frxMostra(frx_CAD_CONTR_001_03_A, 'Impress�o de contrato');
        finally
          Cursor := crDefault;
        end;
      end;
    end;
  end;
}
end;

(*
procedure TFmContratos.este21Click(Sender: TObject);
{
var
  FRichEdit: TrxRichEdit;
}
var
  Txt: String;
begin
  Txt := MyObjects.GetfrxRTF(frxRichView);
  Memo1.Text := Txt;
{
  FRichEdit := TrxRichEdit.Create(nil);
  FRichEdit.Parent := frxParentForm;
  SendMessage(frxParentForm.Handle, WM_CREATEHANDLE, Integer(FRichEdit), 0);
  FRichEdit.AutoURLDetect := False;
  // make rich transparent
  SetWindowLong(FRichEdit.Handle, GWL_EXSTYLE,
    GetWindowLong(FRichEdit.Handle, GWL_EXSTYLE) or WS_EX_TRANSPARENT);
end;
*)

(*
procedure TFmContratos.esteRich1Click(Sender: TObject);
  function frxParentForm: TForm;
  begin
    if FParentForm = nil then
    begin
      FParentForm := TParentForm.CreateNew(nil);
      if not ModuleIsLib or ModuleIsPackage then // Access denied AV inside multithreaded (COM) environment
        FParentForm.HandleNeeded;
    end;
    Result := FParentForm;
  end;
begin
  frxRichView := TfrxRichView.Create(Panel8);
  //frxRichView.CreateUniqueName;
  frxRichView.Visible := True;
  frxRichView.SetBounds(0, 0, 100, 100);
  //Memo.Font.Name   := 'Univers Light Condensed';
  frxRichView.Font.Name   := 'Courier New';
  frxRichView.Font.Size   := 10;
  frxRichView.Frame.Typ := [ftTop] + [ftBottom] + [ftLeft] + [ftRight];
{
  if DBCheck.CriaFm(TfrxRichEditorForm, frxRichEditorForm, afmoNegarComAviso) then
  begin
    FmContrDfCab.ShowModal;
    FmContrDfCab.Destroy;
  end;
}


  with TfrxRichEditorForm.Create(FmContratos) do
  begin
    //RichView := TfrxRichView(Component);
    RichView := frxRichView;
    //Result := ShowModal = mrOk;
    try
      Show;
    except
      ;
    end;
    //Free;
  end;
end;
*)

procedure TFmContratos.Exclui1Click(Sender: TObject);
begin
//
end;

procedure TFmContratos.Excluirpersonalizado1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'Contrattag', 'Controle', QrContratTagControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrContratTag,
      QrContratTagControle, QrContratTagControle.Value);
    ReopenContratTag(Controle);
  end;
end;

procedure TFmContratos.CadastrodeGruposdeTags1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContrDfCab, FmContrDfCab, afmoNegarComAviso) then
  begin
    FmContrDfCab.ShowModal;
    FmContrDfCab.Destroy;
  end;
end;

procedure TFmContratos.CadastrodeTagsdeNveis1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCtrTagNivCab, FmCtrTagNivCab, afmoNegarComAviso) then
  begin
    FmCtrTagNivCab.ShowModal;
    FmCtrTagNivCab.Destroy;
  end;
end;

procedure TFmContratos.CadastrodeTagspersonalizados1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'contrattdf', 60, ncGerlSeq1,
  'Cadastro de Tags de Contratos',
  [], False, Null, [], [], False);
end;

procedure TFmContratos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmContratos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmContratos.ReabreQuerys;
begin
  try
    Screen.Cursor := crHourGlass;
    //
    UMyMod.AbreQuery(QrContratada, Dmod.MyDB);
    UMyMod.AbreQuery(QrContratante, Dmod.MyDB);
    UMyMod.AbreQuery(QrRepreLegal, Dmod.MyDB);
    UMyMod.AbreQuery(QrTestemun01, Dmod.MyDB);
    UMyMod.AbreQuery(QrTestemun02, Dmod.MyDB);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmContratos.ReopenContratLoc(Controle: Integer);
var
  Campos, Tabelas: String;
  Tipos: String;
begin
  // B u g s t r o l
  if CO_DMKID_APP = 24 then
  begin
    Tipos :=
      dmkPF.ArrayToTexto('mac.Tipo', ''(*'NO_TIPO'*), pvNo, True, sListaTiposMoveis);
    Campos := Geral.ATS([
    'stc.Cliente + 0.000 Cliente, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
    'ELT(loc.Tabela, stc.Nome, ' + Tipos + ') NO_LUGAR, ',
    'ELT(loc.Tabela, sic.SCompl2, mac.Nome, "? ? ?") NO_LOCAL ',
    '']);
    Tabelas := Geral.ATS([
    'LEFT JOIN siapimacad sic ON sic.Codigo=loc.Cadastro ',
    'LEFT JOIN siaptercad stc ON stc.Codigo=sic.SiapImaTer ',
    'LEFT JOIN movamovcad mac ON mac.Codigo=loc.Cadastro ',
    'LEFT JOIN entidades ent ON ent.Codigo=stc.Cliente ',
    '']);
  end else
  begin
    Campos := '0.000 Cliente, "" NO_ENT, "" NO_LUGAR, "" NO_LOCAL ';
    Tabelas := '';
    Tipos := '';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrContratLoc, Dmod.MyDB, [
  'SELECT loc.Controle, loc.Tabela, loc.Cadastro, ',
  Campos,
  'FROM contratloc loc ',
  Tabelas,
  'WHERE loc.Codigo=' + Geral.FF0(QrContratosCodigo.Value),
  'ORDER BY NO_LUGAR, NO_LOCAL ', // Para a avari�vel LOCAIS_S
  '']);
end;

procedure TFmContratos.ReopenContratTag(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrContratTag, Dmod.MyDB, [
  'SELECT * ',
  'FROM contrattag ',
  'WHERE Codigo=' + Geral.FF0(QrContratosCodigo.Value),
  '']);
end;

procedure TFmContratos.ReopenEndContratada(Entidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEndContratada, Dmod.MyDB, [
    'SELECT ent.Codigo, ent.Tipo, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CONTRATADA, ',
    'IF(ent.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD, ',
    'IF(ent.Tipo=0, ent.ERua, ent.PRua) NOMERUA, ',
    'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 NUMERO, ',
    'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) COMPL, ',
    'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BAIRRO, ',
    'mun.Nome CIDADE, ',
    'IF(ent.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF, ',
    'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, ',
    'IF(ent.Tipo=0, ent.IE, ent.RG) IE_RG, ',
    'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TEL, ',
    'IF(ent.Tipo=0, ent.EFax, ent.PFax) FAX ',
    'FROM entidades ent ',
    'LEFT JOIN UFs ufe ON ufe.Codigo=ent.EUF ',
    'LEFT JOIN UFs ufp ON ufp.Codigo=ent.PUF ',
    'LEFT JOIN ListaLograd lle ON lle.Codigo=ent.ELograd ',
    'LEFT JOIN ListaLograd llp ON llp.Codigo=ent.PLograd ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici) ',
    'WHERE ent.Codigo=' + Geral.FF0(Entidade),
    'ORDER BY CONTRATADA ',
    '']);
end;

procedure TFmContratos.ReopenEndContratante(Entidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEndContratante, Dmod.MyDB, [
    'SELECT ent.Codigo, ent.Tipo, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CONTRATANTE, ',
    'IF(ent.Tipo=0, lle.Nome, llp.Nome) NOMELOGRAD, ',
    'IF(ent.Tipo=0, ent.ERua, ent.PRua) NOMERUA, ',
    'IF(ent.Tipo=0, ent.ENumero, ent.PNumero) + 0.000 NUMERO, ',
    'IF(ent.Tipo=0, ent.ECompl, ent.PCompl) COMPL, ',
    'IF(ent.Tipo=0, ent.EBairro, ent.PBairro) BAIRRO, ',
    'mun.Nome CIDADE, ',
    'IF(ent.Tipo=0, ufe.Nome, ufp.Nome) NOMEUF, ',
    'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, ',
    'IF(ent.Tipo=0, ent.IE, ent.RG) IE_RG, ',
    'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) TEL, ',
    'IF(ent.Tipo=0, ent.EFax, ent.PFax) FAX ',
    'FROM entidades ent ',
    'LEFT JOIN UFs ufe ON ufe.Codigo=ent.EUF ',
    'LEFT JOIN UFs ufp ON ufp.Codigo=ent.PUF ',
    'LEFT JOIN ListaLograd lle ON lle.Codigo=ent.ELograd ',
    'LEFT JOIN ListaLograd llp ON llp.Codigo=ent.PLograd ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici) ',
    'WHERE ent.Codigo=' + Geral.FF0(Entidade),
    'ORDER BY CONTRATANTE ',
    '']);
end;

{$IfDef CO_DMKID_APP_0024}
procedure TFmContratos.ReopenContrBugsN(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrContrBugsN, Dmod.MyDB, [
  'SELECT oca.*, IF(oca.Praga_Z <> 0, "E", "G") NO_NIVEL, ',
  'IF(oca.Praga_Z <> 0, pcz.Nome, pca.Nome) NO_PRAGA, ',
  'IF(oca.Praga_Z <> 0, CONCAT(" (", pcz.Especie , ") "), "") NO_ESPECIE ',
  'FROM contrbugsn oca ',
  'LEFT JOIN praga_a pca ON pca.Codigo=oca.Praga_A ',
  'LEFT JOIN praga_z pcz ON pcz.Codigo=oca.Praga_Z ',
  'WHERE oca.Codigo=' + Geral.FF0(QrContratosCodigo.Value),
  '']);
end;
{$EndIf}

{$IfDef CO_DMKID_APP_0024}
procedure TFmContratos.ReopenContrBugsS(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrContrBugsS, Dmod.MyDB, [
  'SELECT oca.*, IF(oca.Praga_Z <> 0, "E", "G") NO_NIVEL, ',
  'IF(oca.Praga_Z <> 0, pcz.Nome, pca.Nome) NO_PRAGA, ',
  'IF(oca.Praga_Z <> 0, CONCAT(" (", pcz.Especie , ") "), "") NO_ESPECIE ',
  'FROM contrbugss oca ',
  'LEFT JOIN praga_a pca ON pca.Codigo=oca.Praga_A ',
  'LEFT JOIN praga_z pcz ON pcz.Codigo=oca.Praga_Z ',
  'WHERE oca.Codigo=' + Geral.FF0(QrContratosCodigo.Value),
  '']);
end;
{$EndIf}

{$IfDef CO_DMKID_APP_0024}
procedure TFmContratos.ReopenContrServi(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrContrServi, Dmod.MyDB, [
  'SELECT ds.Nome NO_SERVICO, cs.* ',
  'FROM contrservi cs ',
  'LEFT JOIN desservico ds ON ds.Codigo=cs.Servico ',
  'WHERE cs.Codigo=' + Geral.FF0(QrContratosCodigo.Value),
  '']);
end;
{$EndIf}

{$IfDef CO_DMKID_APP_0024}
procedure TFmContratos.ReopenContrNivTg(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrContrNivTg, Dmod.MyDB, [
    'SELECT ',
    'ni3.Nome nNiv3, ',
    'ni2.Nome nNiv2, ',
    'ni1.Nome nNiv1, ',
    'cnt.* ',
    'FROM contrnivtg cnt ',
    'LEFT JOIN ctrtagniv1 ni1 ON ni1.Codigo=cnt.CtrTagNiv1 ',
    'LEFT JOIN ctrtagniv2 ni2 ON ni2.Codigo=ni1.NivSup ',
    'LEFT JOIN ctrtagniv3 ni3 ON ni3.Codigo=ni2.NivSup ',
    'WHERE cnt.Codigo=' + Geral.FF0(QrContratosCodigo.Value),
    '']);
end;
{$EndIf}

procedure TFmContratos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmContratos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmContratos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmContratos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmContratos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmContratos.SpeedButton6Click(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(EdContratada.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  ReabreQuerys();
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdContratada, CBContratada, QrContratada, VAR_CADASTRO);
    CBContratada.SetFocus;
  end;
end;

procedure TFmContratos.SbContratanteClick(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(EdContratante.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  ReabreQuerys();
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdContratante, CBContratante, QrContratante, VAR_CADASTRO);
    CBContratante.SetFocus;
  end;
end;

procedure TFmContratos.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContratos.BtTagsClick(Sender: TObject);
begin
  {$IfDef CO_DMKID_APP_0024}
    PageControl1.ActivePageIndex := 2;
  {$Else}
    PageControl1.ActivePageIndex := 0;
  {$EndIf}
  //
  MyObjects.MostraPopUpDeBotao(PMTags, BtTags);
end;

procedure TFmContratos.BtTextoClick(Sender: TObject);
var
  Codigo: Integer;
  TxtCarta: AnsiString;
begin
  PageControl1.ActivePageIndex := 1;
  //
  if (QrContratos.State <> dsInactive) and (QrContratos.RecordCount > 0) then
  begin
    Application.CreateForm(TFmTextos, FmTextos);
    FmTextos.FAtributesSize   := 10;
    FmTextos.FAtributesName   := 'Arial';
    FmTextos.FSaveSit         := 3;
    FmTextos.FRichEdit        := RECarta;
    FmTextos.LBItensMD.Sorted := False;
    MyObjects.ListaItensListBoxDeLista(FmTextos.LBItensMD, sListaContratos);
    FmTextos.LBItensMD.Sorted := True;
    MyObjects.TextoEntreRichEdits(RECarta, FmTextos.Editor);
    FmTextos.ShowModal;
    FmTextos.Destroy;
    //
    Codigo   := QrContratosCodigo.Value;
    TxtCarta := MyObjects.ObtemTextoRichEdit(Self, RECarta);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'contratos', False,
      ['Texto'], ['Codigo'], [TxtCarta], [Codigo], False)
    then
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmContratos.Altera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrContratos, [PnDados],
  [PnEdita], EdContratada, ImgTipo, 'contratos');
  //
  EdVersao.ValueVariant := EdVersao.ValueVariant + 0.01;
  dmkCkAtivo.Checked := Geral.IntToBool(QrContratosAtivo.Value);
  CkContinuar.Visible := False;
end;

procedure TFmContratos.Alterarpersonalizado1Click(Sender: TObject);
begin
  MostraFormContratTag(stUpd);
end;

procedure TFmContratos.Alterarstatusdocontratoatual1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrContratosCodigo.Value;
  //
  ContratUnit.MostraCtrStatus(Codigo, QrContratosStatus.Value, QrContratosDtaCntrFim.Value);
  LocCod(Codigo, Codigo);
end;

procedure TFmContratos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrContratosCodigo.Value;
  //
  if TFmContratos(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(TFmContratos(Self), TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmContratos.BtConfirmaClick(Sender: TObject);
var
  Codigo, Contratada, Contratante, Status: Integer;
  VAluguel: Double;
  Nome, Msg: String;
  Conflito: Boolean;
begin
  Contratada  := EdContratada.ValueVariant;
  Contratante := EdContratante.ValueVariant;
  Status      := dmkRGStatus.ItemIndex;
  VAluguel    := EdValorAluguel.ValueVariant;
  Nome        := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Contratada = 0, EdContratada, 'Defina o nome da contratado!') then
    Exit;
  if EdContratante.Enabled then
    if MyObjects.FIC(Contratante = 0, EdContratante, 'Defina o nome da contratante!') then
      Exit;
  if MyObjects.FIC(Status = -1, dmkRGStatus, 'Defina o status!') then
    Exit;
  if MyObjects.FIC(VAluguel = 0, EdValorAluguel, 'Defina o valor da mensalidade!') then
    Exit;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina o t�tulo do contrato!') then
    Exit;
  //
  Conflito := ContratUnit.ValidaDadosRescisao(dmkRGStatus.ItemIndex,
                TPDtaCntrFim.Date, Msg);
  //
  if MyObjects.FIC(Conflito, nil, Msg) then
    Exit;
  Codigo := UMyMod.BuscaEmLivreY_Def('Contratos', 'Codigo', ImgTipo.SQLType,
    QrContratosCodigo.Value);
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, TFmContratos(Self), PnEdita,
    'contratos', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if CkContinuar.Checked then
    begin
      Geral.MB_Info('Contrato cadastrado com sucesso!');
      ImgTipo.SQLType := stIns;
      MostraEdicao(True, stIns, 0);
    end else
      MostraEdicao(False, stLok, 0);
  end;
end;

procedure TFmContratos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'contratos', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'contratos', 'Codigo');
  MostraEdicao(False, stLok, 0);
end;

procedure TFmContratos.BtLocaisClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  //
  //MyObjects.MostraPopUpDeBotao(PMLocais, BtLocais);
  MostrarFormContratLoc(False, QrContratosContratante.Value);
end;

procedure TFmContratos.BtLocalizaClick(Sender: TObject);
begin
  FindDialog1.Execute;
end;

procedure TFmContratos.MostrarFormContratLoc(SubCliente: Boolean; Entidade: Integer);
const
  SQLType = stIns;
begin
  //MyObjects.MostraPopUpDeBotao(PMLocais, BtLocais);
  PageControl2.ActivePageIndex := 3;
{$IfDef CO_DMKID_APP_0024}
  if DBCheck.CriaFm(TFmContratLoc, FmContratLoc, afmoNegarComAviso) then
  begin
    FmContratLoc.ImgTipo.SQLType := SQLType;
    FmContratLoc.FQrContratLoc := QrContratLoc;
    FmContratLoc.FCodigo := QrContratosCodigo.Value;
{    FmContratLoc.FControle := DmModOS.QrOSSrvControle.Value;
    FmContratLoc.FConta := DmModOS.QrOSFrmCabConta.Value;
    FmContratLoc.FSiapTerCad := QrOSCabSiapTerCad.Value;
}
    FmContratLoc.FEntidade := Entidade;
    //
    FmContratLoc.QrContratos.Close;
    FmContratLoc.QrContratos.SQL.Text := QrContratos.SQL.Text;
    FmContratLoc.QrContratos.Params := QrContratos.Params;
    UMyMod.AbreQuery(FmContratLoc.QrContratos, Dmod.MyDB);
    //
{
    FmContratLoc.QrOSFrmCab.Close;
    FmContratLoc.QrOSFrmCab.SQL.Text := DmModOS.QrOSFrmCab.SQL.Text;
    FmContratLoc.QrOSFrmCab.Params := DmModOS.QrOSFrmCab.Params;
    UMyMod.AbreQuery(FmContratLoc.QrOSFrmCab, Dmod.MyDB);
    FmContratLoc.QrOSFrmCab.Locate('Conta', DmModOS.QrOSFrmCabConta.Value, []);
}    //
    if SQLType = stUpd then
    begin
      // N�o tem!
    end;
    FmContratLoc.ShowModal;
    FmContratLoc.Destroy;
    //
    ReopenContratLoc(0);
  end;
{$EndIf}
end;

procedure TFmContratos.BtContratoClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMContrato, BtContrato);
end;

procedure TFmContratos.FormCreate(Sender: TObject);
var
  Enab: Boolean;
begin
  ImgTipo.SQLType              := stLok;
  PageControl1.ActivePageIndex := 0;
  PageControl1.Align           := alClient;
  GBDados.Align                := alClient;
  PageControl2.ActivePageIndex := 0;
  CriaOForm;
  //
  QrContratos.Database   := Dmod.MyDB;
  QrControle.Database    := Dmod.MyDB;
  QrContratada.Database  := Dmod.MyDB;
  QrContratante.Database := Dmod.MyDB;
  //
  UMyMod.AbreQuery(QrContratos, Dmod.MyDB);
  UMyMod.AbreQuery(QrContratada, Dmod.MyDB);
  UMyMod.AbreQuery(QrContratante, Dmod.MyDB);
  UMyMod.AbreQuery(QrRepreLegal, Dmod.MyDB);
  UMyMod.AbreQuery(QrTestemun01, Dmod.MyDB);
  UMyMod.AbreQuery(QrTestemun02, Dmod.MyDB);
  UMyMod.AbreQuery(QrControle, Dmod.MyDB);
  //
  TPDataContrato.Date   := Date;
  TPDInstal.Date        := Date;
  TPDVencimento.Date    := Date;
  TPDtaPropIni.Date     := Date;
  TPDtaPropFim.Date     := Date;
  TPDtaAssina.Date      := Date;
  TPDtaImprime.Date     := Date;
  TPDtaCntrFim.Date     := Date;
  TPDtaPrxRenw.Date     := 0;
  //
  {$IfDef CO_DMKID_APP_0024}
    Enab := True;
  {$Else}
    Enab := False;
  {$EndIf}
  a1.Visible                   := Enab;
  Pragasinclusas1.Visible      := Enab;
  Pragasexclusas1.Visible      := Enab;
  Servioscontemplados1.Visible := Enab;
  BtLocais.Visible             := Enab;
  TabSheet7.TabVisible         := Enab;
  N3.Visible                   := Enab;
  N1.Visible                   := Enab;
  Gruposdetags1.Visible        := Enab;
  agsPersonalizadas1.Visible   := Enab;
  //
  DBRGStatus.Items.Clear;
  DBRGStatus.Items.AddStrings(ContratUnit.ConfiguraStatus());
  //
  dmkRGStatus.Items.Clear;
  dmkRGStatus.Items.AddStrings(ContratUnit.ConfiguraStatus());
end;

procedure TFmContratos.SbNumeroClick(Sender: TObject);
begin
  DefParams; //<- evitar erro em form tabbed ou paneled! Colocar no in�cio da procedure!!!
  LaRegistro.Caption := GOTOy.Codigo(QrContratosCodigo.Value, LaRegistro.Caption);
end;

procedure TFmContratos.SbImprimeClick(Sender: TObject);
var
  Contratada, Contratante: Integer;
begin
  case QrContratosFormaUso.Value of
    1,2,3:
    begin
      if DBCheck.CriaFm(TFmContratMSWord, FmContratMSWord, afmoNegarComAviso) then
      begin
        FmContratMSWord.FFmChamou := Self;
        FmContratMSWord.EdCodigo.ValueVariant := QrContratosCodigo.Value;

        FmContratMSWord.EdContratada.ValueVariant := QrContratosContratada.Value;
        FmContratMSWord.EdNO_CONTRATADA.Text := QrContratosNCONTRATADA.Value;

        FmContratMSWord.EdContratante.ValueVariant := QrContratosContratante.Value;
        FmContratMSWord.EdNO_CONTRATANTE.Text := QrContratosNCONTRATANTE.Value;

        FmContratMSWord.EdRepreLegal.ValueVariant := QrContratosRepreLegal.Value;
        FmContratMSWord.EdNO_REP_LEGAL.Text := QrContratosNO_REP_LEGAL.Value;

        FmContratMSWord.EdTestemun01.ValueVariant := QrContratosTestemun01.Value;
        FmContratMSWord.EdNO_TESTEM_01.Text := QrContratosNO_TESTEM_01.Value;

        FmContratMSWord.EdTestemun02.ValueVariant := QrContratosTestemun02.Value;
        FmContratMSWord.EdNO_TESTEM_02.Text := QrContratosNO_TESTEM_02.Value;

        FmContratMSWord.EdArquivo.ValueVariant := QrContratosWinDocArq.Value;
        FmContratMSWord.RGFormaUso.ItemIndex := QrContratosFormaUso.Value;
        FmContratMSWord.FQrContratos := QrContratos;
        //
        FmContratMSWord.FQrPragaS    := QrContrBugsS;
        FmContratMSWord.FQrPragaN    := QrContrBugsN;
        FmContratMSWord.FQrServicos  := QrContrServi;
        FmContratMSWord.FQrLocais    := QrContratLoc;
        //
        FmContratMSWord.ShowModal;
        FmContratMSWord.Destroy;
      end;
    end;
    else
      Geral.MB_Erro('Forma de uso n�o implemetada!');
  end;
end;

procedure TFmContratos.SbNomeClick(Sender: TObject);
begin
  DefParams; //<- evitar erro em form tabbed ou paneled! Colocar no in�cio da procedure!!!
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmContratos.SbNovoClick(Sender: TObject);
begin
  //N�o tem codusu
  //LaRegistro.Caption := GOTOy.CodUsu(QrContratosCodigo.Value, LaRegistro.Caption);
end;

procedure TFmContratos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmContratos.QrContratosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtTexto.Enabled  := Geral.IntToBool_0(QrContratosCodigo.Value);
end;

procedure TFmContratos.QrContratosAfterScroll(DataSet: TDataSet);
begin
  ReopenContratTag(0);
  ReopenContratLoc(0);
  {$IfDef CO_DMKID_APP_0024}
  ReopenContrNivTg(0);
  ReopenContrBugsS(0);
  ReopenContrBugsN(0);
  ReopenContrServi(0);
  {$EndIf}
  MyObjects.DefineTextoRichEdit(RECarta, QrContratosTexto.Value);
end;

procedure TFmContratos.FindDialog1Find(Sender: TObject);
begin
  MyObjects.LocalizaTextoEmRich(RECarta, FindDialog1);
end;

procedure TFmContratos.FormActivate(Sender: TObject);
begin
  if TFmContratos(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmContratos.SbQueryClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMQuery, SbQuery);
end;

procedure TFmContratos.SbRepreLegalClick(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(EdRepreLegal.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  ReabreQuerys();
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdRepreLegal, CBRepreLegal, QrRepreLegal, VAR_CADASTRO);
    CBRepreLegal.SetFocus;
  end;
end;

procedure TFmContratos.SbTestemun01Click(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(EdTestemun01.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  ReabreQuerys();
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdTestemun01, CBTestemun01, QrTestemun01, VAR_CADASTRO);
    CBTestemun01.SetFocus;
  end;
end;

procedure TFmContratos.SbTestemun02Click(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(EdTestemun02.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  ReabreQuerys();
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdTestemun02, CBTestemun02, QrTestemun02, VAR_CADASTRO);
    CBTestemun02.SetFocus;
  end;
end;

procedure TFmContratos.SbWinDocArqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMWinDocArq, SbWinDocArq);
end;

function TFmContratos.SelecionaEntidade(): Integer;
const
  Aviso   = '...';
  Titulo  = 'Contratante';
  Prompt  = 'Informe o contratante:';
  Campo   = 'Descricao';
var
  Entidade: Variant;
begin
  Entidade := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
  'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) ' + Campo,
  'FROM entidades ',
  'ORDER BY ' + Campo,
  ''], Dmod.MyDB, False);
  if Entidade <> Null then
    Result := Entidade
  else
    Result := 0;
end;

procedure TFmContratos.SeleodeTagsdeGrupo1Click(Sender: TObject);
  procedure InsereItemAtual();
  var
    Nome, Tag, Text: String;
    Codigo, Controle: Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCDI, Dmod.MyDB, [
    'SELECT * ',
    'FROM contrdfits ',
    'WHERE Controle=' + Geral.FF0(DModG.QrSelCodsNivel2.Value),
    '']);
    Codigo         := QrContratosCodigo.Value;
    Nome           := QrCDINome.Value;
    Tag            := QrCDITag.Value;
    Text           := QrCDIText.Value;
    Controle       := UMyMod.BPGS1I32('contrattag', 'Controle', '', '', tsPos, stIns, 0);
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'contrattag', False, [
    'Codigo', 'Nome', 'Tag',
    'Text'], [
    'Controle'], [
    Codigo, Nome, Tag,
    Text], [
    Controle], True);
  end;
const
  Aviso  = '...';
  Titulo = 'Sele��o de Tags';
  Prompt1 = 'Informe o grupo de tags:';
  Prompt2 = 'Selecione as tags desejadas:';
  Campo  = 'Descricao';
var
  Res: Variant;
  Codigo(*, Nivel1, Controle, G1PriAtiI*): Integer;
begin
  //Res = Quando n�o seleciona nenhum fica Nulo <> Integer
  Res := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt1, nil, nil, Campo, 0, [
  'SELECT Codigo, Nome ' + Campo,
  'FROM contrdfcab ',
  'ORDER BY ' + Campo,
  ''], Dmod.MyDB, False);
  //
  if Res <> Null then
    Codigo := Res
  else
    Codigo := 0;
  //
  if Codigo <> 0 then
  begin
    if DBCheck.EscolheCodigosMultiplos_0(
    Aviso, Caption, Prompt2, nil, 'Ativo', 'Nivel2', 'Nome', [
    'DELETE FROM _selcods_; ',
    'INSERT INTO _selcods_ ',
    'SELECT Codigo Nivel1, Controle Nivel2, ',
    '0 Nivel3, 0 Nivel4, 0 Nivel5, Nome, 1 Ativo ',
    'FROM ' + TMeuDB + '.contrdfits ',
    'WHERE Codigo=' + Geral.FF0(Codigo) + ';',
    ''],[
    'SELECT * FROM _selcods_; ',
    ''], Dmod.QrUpd) then
    begin
      DModG.QrSelCods.First;
      while not DModG.QrSelCods.Eof do
      begin
        if DModG.QrSelCodsAtivo.Value = 1 then
          InsereItemAtual();
        DModG.QrSelCods.Next;
      end;
    end;
    //
    ReopenContratTag(0);
  end;
end;

procedure TFmContratos.Servioscontemplados1Click(Sender: TObject);
{$IfDef CO_DMKID_APP_0024}
const
  Aviso  = '...';
  Titulo = 'Sele��o de Servi�os';
  Prompt = 'Seleciones os servi�os desejados:';
  Campo  = 'Descricao';
  //
  DestTab = 'contrservi';
  DestMaster = 'Codigo';
  DestDetail = 'Controle';
  DestSelec1 = 'Servico';
  SorcField  = 'Servico';
  ExcluiAnteriores = True;
var
  ValrMaster: Integer;
{$EndIf}
begin
  {$IfDef CO_DMKID_APP_0024}
  ValrMaster := QrContratosCodigo.Value;
  if DBCheck.EscolheCodigosMultiplos_A(
  Aviso, Caption, Prompt, nil, 'Ativo', 'Nivel1', 'Nome', [
  'DELETE FROM _selcods_; ',
  'INSERT INTO _selcods_ ',
  'SELECT Codigo Nivel1, 0 Nivel2, ',
  '0 Nivel3, 0 Nivel4, 0 Nivel5, Nome, 0 Ativo ',
  'FROM ' + TMeuDB + '.desservico',
  (*'WHERE Codigo=' + Geral.FF0(Codigo) +*) ';',
  ''],[
  'SELECT * FROM _selcods_; ',
  ''], DestTab, [DestMaster], DestDetail, DestSelec1, [ValrMaster],
  QrContrServi, SorcField, ExcluiAnteriores, Dmod.QrUpd) then
  begin
{
    DModG.QrSelCods.First;
    while not DModG.QrSelCods.Eof do
    begin
      if DModG.QrSelCodsAtivo.Value = 1 then
        InsereItemAtual();
      DModG.QrSelCods.Next;
    end;
}
  end;
  //
  //ReopenContratServi(0);
  {$EndIf}
end;

procedure TFmContratos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
  //MyObjects.UpdMergeDBAvGrid(AdvGridDep, FColsToMerge);
end;

procedure TFmContratos.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmContratos.Inclui1Click(Sender: TObject);
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrContratos, [PnDados],
  [PnEdita], EdContratada, ImgTipo, 'contratos');
  //
  EdContratada.ValueVariant := DmodG.QrFiliLogCodigo.Value;
  CBContratada.KeyValue     := DmodG.QrFiliLogCodigo.Value;
  dmkCkAtivo.Checked        := True;
  CkContinuar.Visible       := True;
end;

procedure TFmContratos.Incluirpersonalizado1Click(Sender: TObject);
begin
  MostraFormContratTag(stIns);
end;

procedure TFmContratos.InclusodeTagsdeNveis1Click(Sender: TObject);
begin
{$IfDef CO_DMKID_APP_0024}
  if DBCheck.CriaFm(TFmContrNivTg, FmContrNivTg, afmoNegarComAviso) then
  begin
    FmContrNivTg.ImgTipo.SQLType := stIns;
    FmContrNivTg.FFmChamou := Self;
    //FmContrNivTg.FContrato := QrContratosCodigo.Value;
    FmContrNivTg.PreparaTabela(QrContratosCodigo.Value);
    FmContrNivTg.ShowModal;
    FmContrNivTg.Destroy;
  end;
{$EndIf}
end;

procedure TFmContratos.QrContratosBeforeClose(DataSet: TDataSet);
begin
  QrContratLoc.Close;
  QrContratTag.Close;
  QrContrBugsS.Close;
  QrContrBugsN.Close;
  QrContrServi.Close;
  //
  RECarta.Text := '';
end;

procedure TFmContratos.QrContratosBeforeOpen(DataSet: TDataSet);
begin
  QrContratosCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmContratos.QrContratosCalcFields(DataSet: TDataSet);
begin
  case QrContratosStatus.Value of
    0: QrContratosSTATUSSTR.Value := 'Cadastrando';
    1: QrContratosSTATUSSTR.Value := 'N�o aprovado';
    2: QrContratosSTATUSSTR.Value := 'Aprovado';
  end;
end;

procedure TFmContratos.QrContratosDtaCntrFimGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := Geral.FDT(QrContratosDtaCntrFim.Value, 2);
end;

procedure TFmContratos.QrContrNivTgAfterOpen(DataSet: TDataSet);
begin
  //MyObjects.UpdMergeDBAvGrid(AdvGridDep, FColsToMerge);
end;

procedure TFmContratos.QrEndContratadaCalcFields(DataSet: TDataSet);
begin
  QrEndContratadaTEL_TXT.Value  := Geral.FormataTelefone_TT(QrEndContratadaTEL.Value);
  QrEndContratadaFAX_TXT.Value  := Geral.FormataTelefone_TT(QrEndContratadaFAX.Value);
  QrEndContratadaCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEndContratadaCNPJ_CPF.Value);
end;

procedure TFmContratos.QrEndContratanteCalcFields(DataSet: TDataSet);
begin
  QrEndContratanteTEL_TXT.Value := Geral.FormataTelefone_TT(QrEndContratanteTEL.Value);
  QrEndContratanteFAX_TXT.Value := Geral.FormataTelefone_TT(QrEndContratanteFAX.Value);
  QrEndContratanteCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEndContratanteCNPJ_CPF.Value);
end;

end.

(* 2013-06-23
colocar renovacoes (aditamentos) de valores / monitoramentos
*)
