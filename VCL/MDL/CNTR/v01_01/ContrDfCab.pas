unit ContrDfCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, ComCtrls, UnDmkEnums;

type
  TFmContrDfCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrContrDfCab: TmySQLQuery;
    DsContrDfCab: TDataSource;
    QrContrDfIts: TmySQLQuery;
    DsContrDfIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrContrDfCabCodigo: TIntegerField;
    QrContrDfCabNome: TWideStringField;
    QrContrDfCabLk: TIntegerField;
    QrContrDfCabDataCad: TDateField;
    QrContrDfCabDataAlt: TDateField;
    QrContrDfCabUserCad: TIntegerField;
    QrContrDfCabUserAlt: TIntegerField;
    QrContrDfCabAlterWeb: TSmallintField;
    QrContrDfCabAtivo: TSmallintField;
    QrContrDfItsCodigo: TIntegerField;
    QrContrDfItsControle: TIntegerField;
    QrContrDfItsNome: TWideStringField;
    QrContrDfItsTag: TWideStringField;
    QrContrDfItsText: TWideMemoField;
    DBMeText: TDBMemo;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrContrDfCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrContrDfCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrContrDfCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraContrDfIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenContrDfIts(Controle: Integer);

  end;

var
  FmContrDfCab: TFmContrDfCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, ContrDfIts, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmContrDfCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmContrDfCab.MostraContrDfIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmContrDfIts, FmContrDfIts, afmoNegarComAviso) then
  begin
    FmContrDfIts.ImgTipo.SQLType := SQLType;
    FmContrDfIts.FDsCab := DsContrDfCab;
    FmContrDfIts.FQrCab := QrContrDfCab;
    FmContrDfIts.FQrIts := QrContrDfIts;
    if SQLType = stIns then
      //
    else
    begin
      FmContrDfIts.EdControle.ValueVariant := QrContrDfItsControle.Value;
      FmContrDfIts.EdNome.Text := QrContrDfItsNome.Value;
      FmContrDfIts.EdTag.Text  := QrContrDfItsTag.Value;
      FmContrDfIts.MeText.Text := QrContrDfItsText.Value;
    end;
    FmContrDfIts.ShowModal;
    FmContrDfIts.Destroy;
  end;
end;

procedure TFmContrDfCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrContrDfCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrContrDfCab, QrContrDfIts);
end;

procedure TFmContrDfCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrContrDfCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrContrDfIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrContrDfIts);
end;

procedure TFmContrDfCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrContrDfCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmContrDfCab.DefParams;
begin
  VAR_GOTOTABELA := 'contrdfcab';
  VAR_GOTOMYSQLTABLE := QrContrDfCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM contrdfcab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmContrDfCab.ItsAltera1Click(Sender: TObject);
begin
  MostraContrDfIts(stUpd);
end;

procedure TFmContrDfCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmContrDfCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmContrDfCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmContrDfCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'ContrDfIts', 'Controle', QrContrDfItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrContrDfIts,
      QrContrDfItsControle, QrContrDfItsControle.Value);
    ReopenContrDfIts(Controle);
  end;
end;

procedure TFmContrDfCab.ReopenContrDfIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrContrDfIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM contrdfits ',
  'WHERE Codigo=' + Geral.FF0(QrContrDfCabCodigo.Value),
  '']);
  //
  QrContrDfIts.Locate('Controle', Controle, []);
end;


procedure TFmContrDfCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmContrDfCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmContrDfCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmContrDfCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmContrDfCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmContrDfCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContrDfCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrContrDfCabCodigo.Value;
  Close;
end;

procedure TFmContrDfCab.ItsInclui1Click(Sender: TObject);
begin
  MostraContrDfIts(stIns);
end;

procedure TFmContrDfCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrContrDfCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'contrdfcab');
end;

procedure TFmContrDfCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := QrContrDfCabCodigo.Value;
  Codigo := UMyMod.BPGS1I32('contrdfcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'contrdfcab',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmContrDfCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'contrdfcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'contrdfcab', 'Codigo');
end;

procedure TFmContrDfCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmContrDfCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmContrDfCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DBMeText.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmContrDfCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrContrDfCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmContrDfCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmContrDfCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrContrDfCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmContrDfCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmContrDfCab.QrContrDfCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmContrDfCab.QrContrDfCabAfterScroll(DataSet: TDataSet);
begin
  ReopenContrDfIts(0);
end;

procedure TFmContrDfCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrContrDfCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmContrDfCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrContrDfCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'contrdfcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmContrDfCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContrDfCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrContrDfCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'contrdfcab');
end;

procedure TFmContrDfCab.QrContrDfCabBeforeOpen(DataSet: TDataSet);
begin
  QrContrDfCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

