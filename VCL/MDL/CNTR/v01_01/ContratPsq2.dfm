object FmContratPsq2: TFmContratPsq2
  Left = 339
  Top = 185
  Caption = 'CAD-CONTR-004 :: Pesquisa de Contrato'
  ClientHeight = 631
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 261
        Height = 32
        Caption = 'Pesquisa de Contrato'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 261
        Height = 32
        Caption = 'Pesquisa de Contrato'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 261
        Height = 32
        Caption = 'Pesquisa de Contrato'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 469
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 469
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 469
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 2
          Top = 401
          Width = 1004
          Height = 5
          Cursor = crVSplit
          Align = alTop
          ExplicitTop = 281
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 126
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object CGStatus: TdmkCheckGroup
            Left = 876
            Top = 0
            Width = 128
            Height = 126
            Align = alLeft
            Caption = 'Status:'
            ItemIndex = 0
            Items.Strings = (
              'Cadastrando'
              'N'#227'o aprovado'
              'Aprovado'
              'Renovado'
              'Rescindido')
            TabOrder = 0
            OnClick = CGStatusClick
            QryCampo = 'Status'
            UpdCampo = 'Status'
            UpdType = utYes
            Value = 1
            OldValor = 0
          end
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 876
            Height = 126
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object Panel8: TPanel
              Left = 0
              Top = 0
              Width = 876
              Height = 80
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label3: TLabel
                Left = 8
                Top = 0
                Width = 55
                Height = 13
                Caption = 'Contratada:'
              end
              object Label4: TLabel
                Left = 8
                Top = 40
                Width = 58
                Height = 13
                Caption = 'Contratante:'
              end
              object Label10: TLabel
                Left = 628
                Top = 0
                Width = 108
                Height = 13
                Caption = 'Descri'#231#227'o do contrato:'
              end
              object EdContratada: TdmkEditCB
                Left = 8
                Top = 16
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Contratada'
                UpdCampo = 'Contratada'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdContratadaChange
                DBLookupComboBox = CBContratada
                IgnoraDBLookupComboBox = False
              end
              object CBContratada: TdmkDBLookupComboBox
                Left = 64
                Top = 16
                Width = 560
                Height = 21
                KeyField = 'Codigo'
                ListField = 'CONTRATADA'
                ListSource = DsContratada
                TabOrder = 1
                dmkEditCB = EdContratada
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdContratante: TdmkEditCB
                Left = 8
                Top = 56
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Contratante'
                UpdCampo = 'Contratante'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdContratanteChange
                DBLookupComboBox = CBContratante
                IgnoraDBLookupComboBox = False
              end
              object CBContratante: TdmkDBLookupComboBox
                Left = 63
                Top = 56
                Width = 560
                Height = 21
                KeyField = 'Codigo'
                ListField = 'CONTRATANTE'
                ListSource = DsContratante
                TabOrder = 3
                dmkEditCB = EdContratante
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdNome: TdmkEdit
                Left = 627
                Top = 16
                Width = 206
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'Nome'
                UpdCampo = 'Nome'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdNomeChange
              end
              object CkValorMes: TdmkCheckBox
                Left = 630
                Top = 39
                Width = 209
                Height = 17
                Caption = 'Valor m'#237'nimo e m'#225'ximo de mensalidade.'
                TabOrder = 5
                OnClick = CkValorMesClick
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
              object EdValorMesMin: TdmkEdit
                Left = 628
                Top = 57
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'ValorMes'
                UpdCampo = 'ValorMes'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdValorMesMinChange
                OnExit = EdValorMesMinExit
              end
              object EdValorMesMax: TdmkEdit
                Left = 731
                Top = 57
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 7
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'ValorMes'
                UpdCampo = 'ValorMes'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdValorMesMaxChange
              end
            end
            object Panel9: TPanel
              Left = 0
              Top = 80
              Width = 876
              Height = 46
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object Panel10: TPanel
                Left = 0
                Top = 0
                Width = 196
                Height = 46
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 0
                object CkInativos: TCheckBox
                  Left = 8
                  Top = 4
                  Width = 185
                  Height = 17
                  Caption = 'N'#227'o mostar contratos rescindidos.'
                  Checked = True
                  State = cbChecked
                  TabOrder = 0
                  OnClick = CkInativosClick
                end
              end
              object RGBugsS: TRadioGroup
                Left = 196
                Top = 0
                Width = 221
                Height = 46
                Align = alLeft
                Caption = 'Forma pesquisa pragas inclusas:'
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o pesquisar'
                  'Est'#225' contido')
                TabOrder = 1
                OnClick = RGBugsSClick
              end
              object RGBugsN: TRadioGroup
                Left = 417
                Top = 0
                Width = 221
                Height = 46
                Align = alLeft
                Caption = 'Forma pesquisa pragas exclusas:'
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o pesquisar'
                  'Est'#225' contido')
                TabOrder = 2
                OnClick = RGBugsNClick
              end
              object RGDesServico: TRadioGroup
                Left = 638
                Top = 0
                Width = 238
                Height = 46
                Align = alClient
                Caption = 'Forma pesquisa servi'#231'os oferecidos:'
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o pesquisar'
                  'Est'#225' contido')
                TabOrder = 3
                OnClick = RGDesServicoClick
              end
            end
          end
        end
        object DBGContratos: TdmkDBGridZTO
          Left = 2
          Top = 406
          Width = 1004
          Height = 61
          Align = alClient
          DataSource = DsContratos
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = DBGContratosDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Contrato'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Versao'
              Title.Caption = 'Vers'#227'o'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o do contrato'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_STATUS'
              Title.Caption = 'Status'
              Width = 102
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DInstal'
              Title.Caption = 'In'#237'cio'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DVencimento'
              Title.Caption = 'Renovar'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaCntrFim'
              Title.Caption = 'Rescis'#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaPrxRenw'
              Title.Caption = 'Renova'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorMes'
              Title.Caption = 'Mensalidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Contratante'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NCONTRATANTE'
              Title.Caption = 'Nome contratante'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'WinDocArq'
              Title.Caption = 'Arq. win.doc'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DContrato'
              Title.Caption = 'Dta. Contrato'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ddMesVcto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaAssina'
              Title.Caption = 'Dta. Assina'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaImprime'
              Title.Caption = 'Dta. Imprime'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaPropIni'
              Title.Caption = 'Dta. prop. ini'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaPropFim'
              Title.Caption = 'Dta. prop. fim'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DVencimento'
              Title.Caption = 'Dta. Vencto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ArtigoDef'
              Title.Caption = 'Artigo def.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EscopoSP'
              Title.Caption = 'Escopo SP'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FormaUso'
              Title.Caption = 'Forma uso'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LugarNome'
              Title.Caption = 'Lugar nome'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LugArtDef'
              Title.Caption = 'Lug art. def.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RepreLegal'
              Title.Caption = 'Repr. Legal'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_REP_LEGAL'
              Title.Caption = 'Nome Repr. Legal'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Testemun01'
              Title.Caption = 'Testem. 1'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TESTEM_01'
              Title.Caption = 'Nome Testem. 1'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Testemun02'
              Title.Caption = 'Testem. 2'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TESTEM_02'
              Title.Caption = 'Nome Testem. 1'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PercJuroM'
              Title.Caption = '% Juro m.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PercMulta'
              Title.Caption = '% Multa'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PerDefImpl'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PerDefIniI'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PerDefMoni'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PerDefTrei'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PerQtdImpl'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PerQtdIniI'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PerQtdMoni'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PerQtdTrei'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Status'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TpPagDocu'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tratamento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Contratada'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NCONTRATADA'
              Title.Caption = 'Nome contratada'
              Visible = True
            end>
        end
        object Panel6: TPanel
          Left = 2
          Top = 141
          Width = 1004
          Height = 260
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object GroupBox9: TGroupBox
            Left = 0
            Top = 0
            Width = 417
            Height = 260
            Align = alLeft
            Caption = 'Pragas inclusas: '
            TabOrder = 0
            object AGABugsS: TDBAdvGrid
              Left = 2
              Top = 15
              Width = 413
              Height = 243
              Cursor = crDefault
              Align = alClient
              ColCount = 7
              DefaultRowHeight = 18
              DrawingStyle = gdsClassic
              RowCount = 5
              FixedRows = 1
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -9
              Font.Name = 'Tahoma'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
              ParentFont = False
              ScrollBars = ssBoth
              TabOrder = 0
              OnClick = AGABugsSClick
              GridLineColor = 15527152
              GridFixedLineColor = 13947601
              HoverRowCells = [hcNormal, hcSelected]
              OnClickCell = AGABugsSClickCell
              DragDropSettings.OleDropTarget = True
              DragDropSettings.OleDropSource = True
              DragDropSettings.OleEntireRows = True
              ActiveCellFont.Charset = DEFAULT_CHARSET
              ActiveCellFont.Color = clWindowText
              ActiveCellFont.Height = -11
              ActiveCellFont.Name = 'Tahoma'
              ActiveCellFont.Style = [fsBold]
              ActiveCellColor = 16575452
              ActiveCellColorTo = 16571329
              AutoThemeAdapt = True
              AutoFilterUpdate = False
              ControlLook.FixedGradientMirrorFrom = 16049884
              ControlLook.FixedGradientMirrorTo = 16247261
              ControlLook.FixedGradientHoverFrom = 16710648
              ControlLook.FixedGradientHoverTo = 16446189
              ControlLook.FixedGradientHoverMirrorFrom = 16049367
              ControlLook.FixedGradientHoverMirrorTo = 15258305
              ControlLook.FixedGradientDownFrom = 15853789
              ControlLook.FixedGradientDownTo = 15852760
              ControlLook.FixedGradientDownMirrorFrom = 15522767
              ControlLook.FixedGradientDownMirrorTo = 15588559
              ControlLook.FixedGradientDownBorder = 14007466
              ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
              ControlLook.DropDownHeader.Font.Color = clWindowText
              ControlLook.DropDownHeader.Font.Height = -11
              ControlLook.DropDownHeader.Font.Name = 'Tahoma'
              ControlLook.DropDownHeader.Font.Style = []
              ControlLook.DropDownHeader.Visible = True
              ControlLook.DropDownHeader.Buttons = <>
              ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
              ControlLook.DropDownFooter.Font.Color = clWindowText
              ControlLook.DropDownFooter.Font.Height = -11
              ControlLook.DropDownFooter.Font.Name = 'Tahoma'
              ControlLook.DropDownFooter.Font.Style = []
              ControlLook.DropDownFooter.Visible = True
              ControlLook.DropDownFooter.Buttons = <>
              EnhRowColMove = False
              Filter = <>
              FilterDropDown.Font.Charset = DEFAULT_CHARSET
              FilterDropDown.Font.Color = clWindowText
              FilterDropDown.Font.Height = -11
              FilterDropDown.Font.Name = 'Tahoma'
              FilterDropDown.Font.Style = []
              FilterDropDown.TextChecked = 'Checked'
              FilterDropDown.TextUnChecked = 'Unchecked'
              FilterDropDownClear = '(All)'
              FilterEdit.TypeNames.Strings = (
                'Starts with'
                'Ends with'
                'Contains'
                'Not contains'
                'Equal'
                'Not equal'
                'Clear')
              FixedColWidth = 20
              FixedRowHeight = 18
              FixedFont.Charset = DEFAULT_CHARSET
              FixedFont.Color = clWindowText
              FixedFont.Height = -11
              FixedFont.Name = 'Tahoma'
              FixedFont.Style = [fsBold]
              FloatFormat = '%.2f'
              Look = glWin7
              PrintSettings.DateFormat = 'dd/mm/yyyy'
              PrintSettings.Font.Charset = DEFAULT_CHARSET
              PrintSettings.Font.Color = clWindowText
              PrintSettings.Font.Height = -11
              PrintSettings.Font.Name = 'Tahoma'
              PrintSettings.Font.Style = []
              PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
              PrintSettings.FixedFont.Color = clWindowText
              PrintSettings.FixedFont.Height = -11
              PrintSettings.FixedFont.Name = 'Tahoma'
              PrintSettings.FixedFont.Style = []
              PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
              PrintSettings.HeaderFont.Color = clWindowText
              PrintSettings.HeaderFont.Height = -11
              PrintSettings.HeaderFont.Name = 'Tahoma'
              PrintSettings.HeaderFont.Style = []
              PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
              PrintSettings.FooterFont.Color = clWindowText
              PrintSettings.FooterFont.Height = -11
              PrintSettings.FooterFont.Name = 'Tahoma'
              PrintSettings.FooterFont.Style = []
              PrintSettings.PageNumSep = '/'
              SearchFooter.Color = 16645370
              SearchFooter.ColorTo = 16247261
              SearchFooter.FindNextCaption = 'Find &next'
              SearchFooter.FindPrevCaption = 'Find &previous'
              SearchFooter.Font.Charset = DEFAULT_CHARSET
              SearchFooter.Font.Color = clWindowText
              SearchFooter.Font.Height = -11
              SearchFooter.Font.Name = 'Tahoma'
              SearchFooter.Font.Style = []
              SearchFooter.HighLightCaption = 'Highlight'
              SearchFooter.HintClose = 'Close'
              SearchFooter.HintFindNext = 'Find next occurrence'
              SearchFooter.HintFindPrev = 'Find previous occurrence'
              SearchFooter.HintHighlight = 'Highlight occurrences'
              SearchFooter.MatchCaseCaption = 'Match case'
              SortSettings.DefaultFormat = ssAutomatic
              SortSettings.HeaderColor = 16579058
              SortSettings.HeaderColorTo = 16579058
              SortSettings.HeaderMirrorColor = 16380385
              SortSettings.HeaderMirrorColorTo = 16182488
              Version = '2.3.6.2'
              AutoCreateColumns = False
              AutoRemoveColumns = False
              Columns = <
                item
                  Alignment = taRightJustify
                  Borders = []
                  BorderPen.Color = clSilver
                  CheckFalse = 'N'
                  CheckTrue = 'Y'
                  Color = clWindow
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  HeaderFont.Charset = DEFAULT_CHARSET
                  HeaderFont.Color = clWindowText
                  HeaderFont.Height = -11
                  HeaderFont.Name = 'Tahoma'
                  HeaderFont.Style = []
                  PrintBorders = [cbTop, cbLeft, cbRight, cbBottom]
                  PrintFont.Charset = DEFAULT_CHARSET
                  PrintFont.Color = clWindowText
                  PrintFont.Height = -11
                  PrintFont.Name = 'Tahoma'
                  PrintFont.Style = []
                  Width = 20
                end
                item
                  Alignment = taRightJustify
                  Borders = []
                  BorderPen.Color = clSilver
                  CheckFalse = 'N'
                  CheckTrue = 'Y'
                  Color = clWindow
                  FieldName = 'c1Cab'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Header = 'C'#243'digo'
                  HeaderFont.Charset = DEFAULT_CHARSET
                  HeaderFont.Color = clWindowText
                  HeaderFont.Height = -11
                  HeaderFont.Name = 'Tahoma'
                  HeaderFont.Style = []
                  PrintBorders = [cbTop, cbLeft, cbRight, cbBottom]
                  PrintFont.Charset = DEFAULT_CHARSET
                  PrintFont.Color = clWindowText
                  PrintFont.Height = -11
                  PrintFont.Name = 'Tahoma'
                  PrintFont.Style = []
                  Width = 45
                end
                item
                  Borders = []
                  BorderPen.Color = clSilver
                  CheckBoxField = True
                  CheckFalse = '0'
                  CheckTrue = '1'
                  Color = clWindow
                  FieldName = 'a2Gru'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Header = 'A2'
                  HeaderFont.Charset = DEFAULT_CHARSET
                  HeaderFont.Color = clWindowText
                  HeaderFont.Height = -11
                  HeaderFont.Name = 'Tahoma'
                  HeaderFont.Style = []
                  PrintBorders = [cbTop, cbLeft, cbRight, cbBottom]
                  PrintFont.Charset = DEFAULT_CHARSET
                  PrintFont.Color = clWindowText
                  PrintFont.Height = -11
                  PrintFont.Name = 'Tahoma'
                  PrintFont.Style = []
                  Width = 19
                end
                item
                  Borders = []
                  BorderPen.Color = clSilver
                  CheckFalse = 'N'
                  CheckTrue = 'Y'
                  Color = clWindow
                  FieldName = 'n2Gru'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Header = 'Grupo'
                  HeaderFont.Charset = DEFAULT_CHARSET
                  HeaderFont.Color = clWindowText
                  HeaderFont.Height = -11
                  HeaderFont.Name = 'Tahoma'
                  HeaderFont.Style = []
                  PrintBorders = [cbTop, cbLeft, cbRight, cbBottom]
                  PrintFont.Charset = DEFAULT_CHARSET
                  PrintFont.Color = clWindowText
                  PrintFont.Height = -11
                  PrintFont.Name = 'Tahoma'
                  PrintFont.Style = []
                  Width = 81
                end
                item
                  Borders = []
                  BorderPen.Color = clSilver
                  CheckBoxField = True
                  CheckFalse = '0'
                  CheckTrue = '1'
                  Color = clWindow
                  FieldName = 'a1Cab'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Header = 'A1'
                  HeaderFont.Charset = DEFAULT_CHARSET
                  HeaderFont.Color = clWindowText
                  HeaderFont.Height = -11
                  HeaderFont.Name = 'Tahoma'
                  HeaderFont.Style = []
                  PrintBorders = [cbTop, cbLeft, cbRight, cbBottom]
                  PrintFont.Charset = DEFAULT_CHARSET
                  PrintFont.Color = clWindowText
                  PrintFont.Height = -11
                  PrintFont.Name = 'Tahoma'
                  PrintFont.Style = []
                  Width = 19
                end
                item
                  AutoMinSize = 100
                  AutoMaxSize = 500
                  Borders = []
                  BorderPen.Color = clSilver
                  CheckFalse = 'N'
                  CheckTrue = 'Y'
                  Color = clWindow
                  FieldName = 'n1Cab'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Header = 'Descri'#231#227'o'
                  HeaderFont.Charset = DEFAULT_CHARSET
                  HeaderFont.Color = clWindowText
                  HeaderFont.Height = -11
                  HeaderFont.Name = 'Tahoma'
                  HeaderFont.Style = []
                  MinSize = 100
                  MaxSize = 500
                  PrintBorders = [cbTop, cbLeft, cbRight, cbBottom]
                  PrintFont.Charset = DEFAULT_CHARSET
                  PrintFont.Color = clWindowText
                  PrintFont.Height = -11
                  PrintFont.Name = 'Tahoma'
                  PrintFont.Style = []
                  Width = 184
                end
                item
                  Borders = []
                  BorderPen.Color = clSilver
                  CheckFalse = 'N'
                  CheckTrue = 'Y'
                  Color = clWindow
                  FieldName = 'NO_Ativo'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -9
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Header = 'S'
                  HeaderFont.Charset = DEFAULT_CHARSET
                  HeaderFont.Color = clWindowText
                  HeaderFont.Height = -9
                  HeaderFont.Name = 'Tahoma'
                  HeaderFont.Style = []
                  PrintBorders = [cbTop, cbLeft, cbRight, cbBottom]
                  PrintFont.Charset = DEFAULT_CHARSET
                  PrintFont.Color = clWindowText
                  PrintFont.Height = -9
                  PrintFont.Name = 'Tahoma'
                  PrintFont.Style = []
                  Width = 19
                end>
              DataSource = DsBugsS
              InvalidPicture.Data = {
                055449636F6E0000010001002020200000000000A81000001600000028000000
                2000000040000000010020000000000000100000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000006A6A6B256A6A6B606A6A6B946A6A6BC06A6A6BE1
                6A6A6BF86A6A6BF86A6A6BE16A6A6BC06A6A6B946A6A6B606A6A6B2500000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000006A6A6B407575769E787879F19F9F9FF6C0C0C0FDDADADAFFEDEDEEFF
                FBFBFBFFFBFBFBFFEDEDEEFFDADADAFFC0C0C0FD9F9F9FF6787879F17575769E
                6A6A6B4000000000000000000000000000000000000000000000000000000000
                000000000000000000000000000000000000000000000000000000006A6A6B22
                7C7C7C98888889F0BDBDBDFCE9E9EBFED9D9E9FEB5B5DDFE8B8BCDFE595AB7FF
                3739A8FF2B2CA4FF4A49B1FF7171C1FFA1A2D7FFD3D3E8FFEAEAEBFEBEBEBFFC
                888889F07C7C7C986A6A6B220000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000006A6A6B43838383D8
                B7B7B8FAECECEFFEC0C0DFFF7977C4FF2221A0FF12129BFF1010A4FF0C0CA8FF
                0A0AACFF0A0AB4FF0A0AB9FF0D0DBEFF0F0FB1FF1111A6FF5656B8FFAEADDCFF
                ECECEFFEB7B7B8FA838383D86A6A6B4300000000000000000000000000000000
                00000000000000000000000000000000000000006A6A6B4E878788EAD3D3D3FE
                CACAE8FF4443B0FF171799FF11119CFF0C0C98FF0B0B9BFF0B0BA0FF0A0AA6FF
                0909ACFF0909B2FF0808BAFF0707BFFF0B09C8FF0D0DCEFF1111CCFF1010AFFF
                4A49B2FFCFCFEBFFD3D3D3FE878788EA6A6A6B4E000000000000000000000000
                000000000000000000000000000000006A6A6B43878788EAE1E1E1FFA8A8DAFF
                2323A0FF15159CFF0D0D92FF0C0C95FF0C0C99FF0B0B9EFF0B0BA0FF0A0AA6FF
                0909ACFF0909B2FF0808B8FF0808BCFF0808C3FF0C0CC9FF0C0CD0FF0D0DD6FF
                1313CFFF2222A9FFAFAFDEFFE1E1E1FF878788EA6A6A6B430000000000000000
                0000000000000000000000006A6A6B22838383D8D3D3D3FEA8A8D9FF2020A4FF
                13139BFF0C0C92FF0C0C95FF0C0C97FF0C0C99FF0B0B9EFF0B0BA0FF0A0AA4FF
                0A0AA9FF0909B0FF0808B4FF0808BBFF0707C0FF0A0AC6FF0909CCFF0C0CD3FF
                0D0DD8FF1313D3FF1A1AA8FFAEADDEFFD4D4D4FE838383D86A6A6B2200000000
                0000000000000000000000007C7C7C98B7B7B8FACACAE8FF2524A3FF13139FFF
                0C0C97FF0C0C95FF0C0C95FF0C0C91FF0C0C95FF0B0B9EFF0B0BA0FF0A0AA4FF
                0A0AA8FF0909ADFF0909B2FF0808B8FF0808BCFF0707C0FF0808BCFF0707C5FF
                0C0CD3FF0D0DD7FF1212D1FF2020A7FFCDCDEBFFB8B8B9FA7C7C7C9800000000
                00000000000000006A6A6B40888889F0ECECEFFE4545B1FF1616A4FF0B0B9BFF
                0C0C99FF0C0C96FF3333A2FFB9B9D0FF393A9BFF0C0C95FF0B0BA1FF0A0AA4FF
                0A0AA7FF0A0AABFF0909B0FF0808B4FF0808B7FF2F2FC2FFAEAEE2FF4B4BBFFF
                0707BEFF0B0BD1FF0C0CD3FF1413CCFF4848B1FFECECEFFE888889F06A6A6B40
                00000000000000007575769EBFBFBFFD9B9BD5FF1C1CA6FF0C0CA1FF0B0B9FFF
                0B0B9AFF3535A7FFB5B5BEFFE6E6DFFFEDEDEFFF3C3C9CFF0C0C97FF0A0AA4FF
                0A0AA6FF0A0AA9FF0909ADFF0909B0FF2626B5FFCECEDEFFFFFFFBFFEEEEF1FF
                4848BAFF0808BCFF0A0ACDFF0B0BCEFF1111ABFFBEC0E0FFBFC0BFFD7575769E
                000000006A6A6B25787879F1E3E3E5FE4646B2FF1414A8FF0A0AA4FF0B0BA0FF
                2121A9FFBDBDCAFFD0D0C8FFC5C5C5FFE3E3E1FFEDEDEFFF3E3E9EFF0C0C98FF
                0A0AA6FF0A0AA8FF0A0AA9FF2B2BB0FFC0C0CDFFEAEAE2FFEBEBEBFFFEFEF8FF
                EDEDEEFF2828BDFF0707C4FF0809C7FF0F0FC4FF8788CBFFEBEBECFE79797AF1
                6A6A6B256A6A6B609D9E9DF6D6D7E4FF3A3AB3FF1212ADFF0A0AA8FF0A0AA4FF
                1313AAFFABABCFFFD6D6CBFFCACACAFFC6C6C6FFE4E4E0FFEEEEEFFF3F3FA0FF
                0C0C99FF0A0AA6FF2828ABFFB2B2BFFFD8D8CEFFD6D6D8FFE0E0E0FFF6F5EDFF
                D1D1EDFF1E1CC0FF0707BEFF0707BFFF0707C0FF2120AAFFD3D5E9FE9FA0A0F6
                6A6A6B606A6A6B94BDBDBDFBBABBDCFF3A39B7FF2F2FB8FF0909ADFF0A0AA9FF
                0A0AA6FF1515ACFFADADCFFFD6D6CBFFCBCBCAFFC6C6C6FFE4E4E1FFEEEEEFFF
                3838A1FF2222A2FFACABB8FFC8C8C0FFC7C7C8FFCDCDCDFFE1E1D9FFC8CAE1FF
                2424BCFF0808B4FF0808B9FF0808BAFF0808BBFF0F0EABFFA1A2D5FEC0C0C0FC
                6A6A6B946A6A6BC0D9D8D7FE9999D1FF3838BBFF3636BCFF2C2CB7FF0909ADFF
                0A0AA9FF0A0AA4FF1C1CAFFFB1B1CFFFD6D6CBFFCCCCCBFFC7C7C7FFE4E4E1FF
                ECECEEFFACACB7FFC2C2BCFFBEBEBFFFC0C0C0FFCFCFC6FFC1C1D5FF2727B8FF
                0909ACFF0909B2FF0909B2FF0909B4FF0808B4FF0E0EB5FF6E6EBFFFD9D9D9FE
                6A6A6BC06A6A6BE1EBEAEBFF7D7CC7FF3838BFFF3434BEFF3536BEFF2A2AB8FF
                0909B0FF0909ACFF0A0AA8FF1C1CB1FFB2B2D0FFD7D7CCFFCBCBCBFFC7C7C8FF
                C8C8C3FFC6C6C3FFBFBFC1FFBDBDBDFFC5C5BCFFB8B8CEFF2929B5FF0A0AA8FF
                0909ACFF0909ADFF0909AFFF0909AFFF0909AFFF0C0CB0FF4747AFFFECECEDFF
                6A6A6BE16A6A6BF8F9F9F9FF6666C1FF3838C4FF3535C2FF3434C0FF3535BEFF
                3030BCFF1313B4FF0909ADFF0A0AA8FF1E1EB3FFAAAAD0FFD3D3CDFFCCCCCCFF
                C8C8C8FFC3C3C3FFC2C2C1FFC4C4BFFFB2B2CBFF2B2BB4FF0A0AA4FF0A0AA8FF
                0A0AA8FF0A0AA9FF0A0AA9FF0A0AA9FF0A0AA9FF0B0BA9FF3131A6FFFAFAFAFF
                6A6A6BF86A6A6BF8FBFBFBFF5959BEFF3B3BCAFF3A3AC8FF3737C4FF3535C2FF
                3636C0FF3636BEFF2323B8FF0909B1FF0A0AA7FF4949BEFFD6D6D4FFD3D3D1FF
                CDCDCDFFC8C8C8FFC4C4C3FFEDEDEDFF5F5FB3FF0C0C98FF0A0AA7FF0A0AA6FF
                0A0AA6FF0A0AA6FF0A0AA4FF0A0AA6FF0A0AA4FF0B0BA4FF2D2DA6FFFBFBFBFF
                6A6A6BF86A6A6BE1EDEDEEFF7F80CBFF4041CCFF3C3CCAFF3A3AC8FF383AC8FF
                3838C4FF3636C2FF3939C0FF2123B7FF4A4AC2FFCBCBDEFFE0E0DCFFD6D6D6FF
                D2D2D3FFCDCDCEFFC9C9C9FFE2E2E1FFF1F1F2FF4242A3FF0C0C99FF0A0AA4FF
                0A0AA4FF0A0AA4FF0B0BA3FF0B0BA3FF0B0BA1FF0E0EA1FF4443B0FFEDEDEEFF
                6A6A6BE16A6A6BC0DADADAFF9C9BD5FE4949CDFF3E3DD0FF3C3DCEFF3C3CCAFF
                3A3AC8FF3B39C7FF2828BDFF5C5CCCFFE5E5EDFFF4F4EDFFE5E5E6FFDEDEDEFF
                DCDCD9FFD9D9D3FFCDCDCDFFC8C8C8FFE5E5E1FFF1F1F3FF3F3FA0FF0C0C99FF
                0A0AA4FF0B0BA1FF0B0BA0FF0B0BA0FF0B0B9FFF1313A2FF6B6BC0FFDADADAFF
                6A6A6BC06A6A6B94C0C0C0FDBDBAE1FE5655CFFF4141D4FF3F3FD2FF3F3FCEFF
                3D3DCCFF2C2AC3FF5E5ED3FFEBEBF6FFFFFFFAFFF1F1F1FFEDEDEEFFF0F0E9FF
                D2D2E6FFBDBDD6FFDADAD3FFCFCFCFFFC9C9CAFFE5E5E2FFF1F1F3FF3A3AA0FF
                0C0C98FF0B0BA3FF0B0B9FFF0B0B9EFF0B0B9EFF1C1CA4FF9C9CD3FFC1C1C1FD
                6A6A6B946A6A6B609F9F9FF6DAD9EAFF6B6BCFFF4444D7FF4143D6FF4242D3FF
                3434CDFF6464DBFFEFEFFFFFFFFFFFFFFCFCFCFFF6F6F6FFFCFCF4FFE2E1F0FF
                5050CCFF4040C1FFC3C3DBFFE1E1D8FFD4D4D5FFCFCFCFFFE8E8E5FFF2F2F4FF
                4040A2FF0C0C99FF0F0FA2FF0F0FA0FF0F0F9DFF302FA9FFD1D1E8FEA0A0A0F6
                6A6A6B606A6A6B25787879F1E9E9EBFEA7A7DAFF6060DBFF4547DBFF3C3CD6FF
                5857DEFFF2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E8F8FF5B5BD4FF
                2828BDFF2A2BBDFF4949C5FFC3C3DBFFE4E4DAFFD5D5D5FFCECED0FFE8E8E5FF
                F4F4F4FF4949AFFF2121A6FF2A2AA6FF2C2BA9FF5557B8FFEAEAECFE787879F1
                6A6A6B25000000007575769EBEBEBEFDC9CAE6FF7A79DBFF4C4CDFFF4141DBFF
                5757E0FFEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E7FFFF5B5BD7FF2E2EC6FF
                3E3EC9FF3A3AC5FF2C2EC1FF4A49C8FFC2C2DDFFE3E3DAFFD5D5D4FFDADAD3FF
                CACBD9FF4747BBFF2525ADFF2C2BACFF3332AEFFA5A4D8FFBFBFBFFD7575769E
                00000000000000006A6A6B40888889F0ECECEFFE9696D6FF7B7BE3FF4D4BE0FF
                4141DBFF5F5FE6FFE7E7FFFFFFFFFFFFE9E9FFFF5A5ADCFF3333CAFF4242CFFF
                4040CBFF3D3DC9FF3D3EC8FF3030C2FF4848C9FFC0C0DDFFECEEDEFFD0D0E0FF
                5554C7FF2828B3FF3232B4FF3434B1FF5453B7FFECECEFFE888889F06A6A6B40
                0000000000000000000000007C7C7C98B7B7B8FAD0D0ECFF8F8FDBFF6868E3FF
                4E4EE2FF3E40DBFF6565E9FFB2B2F7FF6565E4FF393BD2FF4646D7FF4343D4FF
                4343D1FF4242CFFF4040CBFF3F3FCAFF3333C4FF4E4ECBFF9E9EE2FF5C5BCFFF
                292ABAFF3636BCFF3938B8FF3F3EB1FFCBCBE9FFB7B7B8FA7C7C7C9800000000
                0000000000000000000000006A6A6B22838383D8D3D3D3FEB5B5E2FF9E9EE4FF
                6766E2FF4E50E6FF4646E0FF3D3DDAFF4444DCFF4B4BDCFF4848DBFF4847D9FF
                4646D5FF4443D3FF4343D1FF4242CFFF4143CDFF3A3AC8FF312FC5FF3535C3FF
                3C3CC3FF3D3DBEFF403FB5FFACACDCFFD3D3D3FE838383D86A6A6B2200000000
                000000000000000000000000000000006A6A6B43878788EAE1E1E1FFB5B5E2FF
                A7A6E4FF7877E5FF5151E5FF4F4FE4FF4E4EE2FF4D4DE0FF4C4CDEFF4B4BDCFF
                4949DBFF4848D7FF4747D5FF4545D3FF4545D1FF4343CFFF4242CCFF3F3FCBFF
                4343C2FF4645B6FFADADDCFFE1E1E1FF878788EA6A6A6B430000000000000000
                00000000000000000000000000000000000000006A6A6B4E878788EAD3D3D3FE
                D0D0ECFFAAA9DFFFA2A2ECFF6565E3FF5151E6FF4F4FE4FF4F4DE4FF4D4DE0FF
                4D4DDFFF4D4DDCFF4C49DBFF4A4AD8FF4749D6FF4747D4FF4949CBFF4B4BC3FF
                8E8ED0FFCDCCE8FFD3D3D3FE878788EA6A6A6B4E000000000000000000000000
                0000000000000000000000000000000000000000000000006A6A6B43838383D8
                B7B7B8FAECECEFFEC3C2E5FFADAEE1FF9E9DE8FF6F6FE0FF5C5CE1FF5452E2FF
                5051E1FF4F4FDFFF4F4FDBFF5150D6FF5151CFFF5F5FC8FFA1A1D3FEC7C8E0FE
                E4E4E7FEB7B7B8FA838383D86A6A6B4300000000000000000000000000000000
                000000000000000000000000000000000000000000000000000000006A6A6B22
                7C7C7C98888889F0BFBFBFFDEBEBECFED8D9EBFEBDBDE4FEA8A7DCFF9695D7FF
                8886D4FF7F7DCEFF8C8BD2FFA1A2D9FFC0BEE1FED9D9EAFEEAEAECFEBFBFBFFD
                888889F07C7C7C986A6A6B220000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000006A6A6B407575769E787879F19F9F9FF6C0C0C0FDDADADAFFEDEDEEFF
                FBFBFBFFFBFBFBFFEDEDEEFFDADADAFFC0C0C0FD9F9F9FF6787879F17575769E
                6A6A6B4000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000006A6A6B256A6A6B606A6A6B946A6A6BC06A6A6BE1
                6A6A6BF86A6A6BF86A6A6BE16A6A6BC06A6A6B946A6A6B606A6A6B2500000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000FFC003FFFF0000FFFC00003FF800001FF000000FE0000007C0000003
                C000000380000001800000010000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000080000001
                80000001C0000003C0000003E0000007F000000FF800001FFC00003FFF0000FF
                FFC003FF}
              ShowUnicode = False
              ColWidths = (
                20
                45
                19
                81
                19
                184
                19)
            end
          end
          object GroupBox10: TGroupBox
            Left = 417
            Top = 0
            Width = 404
            Height = 260
            Align = alLeft
            Caption = ' Pragas exclusas:  '
            TabOrder = 1
            object AGABugsN: TDBAdvGrid
              Left = 2
              Top = 15
              Width = 400
              Height = 243
              Cursor = crDefault
              Align = alClient
              ColCount = 7
              DefaultRowHeight = 18
              DrawingStyle = gdsClassic
              RowCount = 5
              FixedRows = 1
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -9
              Font.Name = 'Tahoma'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
              ParentFont = False
              ScrollBars = ssBoth
              TabOrder = 0
              OnClick = AGABugsNClick
              GridLineColor = 15527152
              GridFixedLineColor = 13947601
              HoverRowCells = [hcNormal, hcSelected]
              OnClickCell = AGABugsNClickCell
              DragDropSettings.OleDropTarget = True
              DragDropSettings.OleDropSource = True
              DragDropSettings.OleEntireRows = True
              ActiveCellFont.Charset = DEFAULT_CHARSET
              ActiveCellFont.Color = clWindowText
              ActiveCellFont.Height = -11
              ActiveCellFont.Name = 'Tahoma'
              ActiveCellFont.Style = [fsBold]
              ActiveCellColor = 16575452
              ActiveCellColorTo = 16571329
              AutoThemeAdapt = True
              AutoFilterUpdate = False
              ControlLook.FixedGradientMirrorFrom = 16049884
              ControlLook.FixedGradientMirrorTo = 16247261
              ControlLook.FixedGradientHoverFrom = 16710648
              ControlLook.FixedGradientHoverTo = 16446189
              ControlLook.FixedGradientHoverMirrorFrom = 16049367
              ControlLook.FixedGradientHoverMirrorTo = 15258305
              ControlLook.FixedGradientDownFrom = 15853789
              ControlLook.FixedGradientDownTo = 15852760
              ControlLook.FixedGradientDownMirrorFrom = 15522767
              ControlLook.FixedGradientDownMirrorTo = 15588559
              ControlLook.FixedGradientDownBorder = 14007466
              ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
              ControlLook.DropDownHeader.Font.Color = clWindowText
              ControlLook.DropDownHeader.Font.Height = -11
              ControlLook.DropDownHeader.Font.Name = 'Tahoma'
              ControlLook.DropDownHeader.Font.Style = []
              ControlLook.DropDownHeader.Visible = True
              ControlLook.DropDownHeader.Buttons = <>
              ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
              ControlLook.DropDownFooter.Font.Color = clWindowText
              ControlLook.DropDownFooter.Font.Height = -11
              ControlLook.DropDownFooter.Font.Name = 'Tahoma'
              ControlLook.DropDownFooter.Font.Style = []
              ControlLook.DropDownFooter.Visible = True
              ControlLook.DropDownFooter.Buttons = <>
              EnhRowColMove = False
              Filter = <>
              FilterDropDown.Font.Charset = DEFAULT_CHARSET
              FilterDropDown.Font.Color = clWindowText
              FilterDropDown.Font.Height = -11
              FilterDropDown.Font.Name = 'Tahoma'
              FilterDropDown.Font.Style = []
              FilterDropDown.TextChecked = 'Checked'
              FilterDropDown.TextUnChecked = 'Unchecked'
              FilterDropDownClear = '(All)'
              FilterEdit.TypeNames.Strings = (
                'Starts with'
                'Ends with'
                'Contains'
                'Not contains'
                'Equal'
                'Not equal'
                'Clear')
              FixedColWidth = 20
              FixedRowHeight = 18
              FixedFont.Charset = DEFAULT_CHARSET
              FixedFont.Color = clWindowText
              FixedFont.Height = -11
              FixedFont.Name = 'Tahoma'
              FixedFont.Style = [fsBold]
              FloatFormat = '%.2f'
              Look = glWin7
              PrintSettings.DateFormat = 'dd/mm/yyyy'
              PrintSettings.Font.Charset = DEFAULT_CHARSET
              PrintSettings.Font.Color = clWindowText
              PrintSettings.Font.Height = -11
              PrintSettings.Font.Name = 'Tahoma'
              PrintSettings.Font.Style = []
              PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
              PrintSettings.FixedFont.Color = clWindowText
              PrintSettings.FixedFont.Height = -11
              PrintSettings.FixedFont.Name = 'Tahoma'
              PrintSettings.FixedFont.Style = []
              PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
              PrintSettings.HeaderFont.Color = clWindowText
              PrintSettings.HeaderFont.Height = -11
              PrintSettings.HeaderFont.Name = 'Tahoma'
              PrintSettings.HeaderFont.Style = []
              PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
              PrintSettings.FooterFont.Color = clWindowText
              PrintSettings.FooterFont.Height = -11
              PrintSettings.FooterFont.Name = 'Tahoma'
              PrintSettings.FooterFont.Style = []
              PrintSettings.PageNumSep = '/'
              SearchFooter.Color = 16645370
              SearchFooter.ColorTo = 16247261
              SearchFooter.FindNextCaption = 'Find &next'
              SearchFooter.FindPrevCaption = 'Find &previous'
              SearchFooter.Font.Charset = DEFAULT_CHARSET
              SearchFooter.Font.Color = clWindowText
              SearchFooter.Font.Height = -11
              SearchFooter.Font.Name = 'Tahoma'
              SearchFooter.Font.Style = []
              SearchFooter.HighLightCaption = 'Highlight'
              SearchFooter.HintClose = 'Close'
              SearchFooter.HintFindNext = 'Find next occurrence'
              SearchFooter.HintFindPrev = 'Find previous occurrence'
              SearchFooter.HintHighlight = 'Highlight occurrences'
              SearchFooter.MatchCaseCaption = 'Match case'
              SortSettings.DefaultFormat = ssAutomatic
              SortSettings.HeaderColor = 16579058
              SortSettings.HeaderColorTo = 16579058
              SortSettings.HeaderMirrorColor = 16380385
              SortSettings.HeaderMirrorColorTo = 16182488
              Version = '2.3.6.2'
              AutoCreateColumns = False
              AutoRemoveColumns = False
              Columns = <
                item
                  Alignment = taRightJustify
                  Borders = []
                  BorderPen.Color = clSilver
                  CheckFalse = 'N'
                  CheckTrue = 'Y'
                  Color = clWindow
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  HeaderFont.Charset = DEFAULT_CHARSET
                  HeaderFont.Color = clWindowText
                  HeaderFont.Height = -11
                  HeaderFont.Name = 'Tahoma'
                  HeaderFont.Style = []
                  PrintBorders = [cbTop, cbLeft, cbRight, cbBottom]
                  PrintFont.Charset = DEFAULT_CHARSET
                  PrintFont.Color = clWindowText
                  PrintFont.Height = -11
                  PrintFont.Name = 'Tahoma'
                  PrintFont.Style = []
                  Width = 20
                end
                item
                  Alignment = taRightJustify
                  Borders = []
                  BorderPen.Color = clSilver
                  CheckFalse = 'N'
                  CheckTrue = 'Y'
                  Color = clWindow
                  FieldName = 'c1Cab'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Header = 'C'#243'digo'
                  HeaderFont.Charset = DEFAULT_CHARSET
                  HeaderFont.Color = clWindowText
                  HeaderFont.Height = -11
                  HeaderFont.Name = 'Tahoma'
                  HeaderFont.Style = []
                  PrintBorders = [cbTop, cbLeft, cbRight, cbBottom]
                  PrintFont.Charset = DEFAULT_CHARSET
                  PrintFont.Color = clWindowText
                  PrintFont.Height = -11
                  PrintFont.Name = 'Tahoma'
                  PrintFont.Style = []
                  Width = 45
                end
                item
                  Borders = []
                  BorderPen.Color = clSilver
                  CheckBoxField = True
                  CheckFalse = '0'
                  CheckTrue = '1'
                  Color = clWindow
                  FieldName = 'a2Gru'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Header = 'A2'
                  HeaderFont.Charset = DEFAULT_CHARSET
                  HeaderFont.Color = clWindowText
                  HeaderFont.Height = -11
                  HeaderFont.Name = 'Tahoma'
                  HeaderFont.Style = []
                  PrintBorders = [cbTop, cbLeft, cbRight, cbBottom]
                  PrintFont.Charset = DEFAULT_CHARSET
                  PrintFont.Color = clWindowText
                  PrintFont.Height = -11
                  PrintFont.Name = 'Tahoma'
                  PrintFont.Style = []
                  Width = 19
                end
                item
                  Borders = []
                  BorderPen.Color = clSilver
                  CheckFalse = 'N'
                  CheckTrue = 'Y'
                  Color = clWindow
                  FieldName = 'n2Gru'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Header = 'Grupo'
                  HeaderFont.Charset = DEFAULT_CHARSET
                  HeaderFont.Color = clWindowText
                  HeaderFont.Height = -11
                  HeaderFont.Name = 'Tahoma'
                  HeaderFont.Style = []
                  PrintBorders = [cbTop, cbLeft, cbRight, cbBottom]
                  PrintFont.Charset = DEFAULT_CHARSET
                  PrintFont.Color = clWindowText
                  PrintFont.Height = -11
                  PrintFont.Name = 'Tahoma'
                  PrintFont.Style = []
                  Width = 81
                end
                item
                  Borders = []
                  BorderPen.Color = clSilver
                  CheckBoxField = True
                  CheckFalse = '0'
                  CheckTrue = '1'
                  Color = clWindow
                  FieldName = 'a1Cab'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Header = 'A1'
                  HeaderFont.Charset = DEFAULT_CHARSET
                  HeaderFont.Color = clWindowText
                  HeaderFont.Height = -11
                  HeaderFont.Name = 'Tahoma'
                  HeaderFont.Style = []
                  PrintBorders = [cbTop, cbLeft, cbRight, cbBottom]
                  PrintFont.Charset = DEFAULT_CHARSET
                  PrintFont.Color = clWindowText
                  PrintFont.Height = -11
                  PrintFont.Name = 'Tahoma'
                  PrintFont.Style = []
                  Width = 19
                end
                item
                  AutoMinSize = 100
                  AutoMaxSize = 500
                  Borders = []
                  BorderPen.Color = clSilver
                  CheckFalse = 'N'
                  CheckTrue = 'Y'
                  Color = clWindow
                  FieldName = 'n1Cab'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Header = 'Descri'#231#227'o'
                  HeaderFont.Charset = DEFAULT_CHARSET
                  HeaderFont.Color = clWindowText
                  HeaderFont.Height = -11
                  HeaderFont.Name = 'Tahoma'
                  HeaderFont.Style = []
                  MinSize = 100
                  MaxSize = 500
                  PrintBorders = [cbTop, cbLeft, cbRight, cbBottom]
                  PrintFont.Charset = DEFAULT_CHARSET
                  PrintFont.Color = clWindowText
                  PrintFont.Height = -11
                  PrintFont.Name = 'Tahoma'
                  PrintFont.Style = []
                  Width = 170
                end
                item
                  Borders = []
                  BorderPen.Color = clSilver
                  CheckFalse = 'N'
                  CheckTrue = 'Y'
                  Color = clWindow
                  FieldName = 'NO_Ativo'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -9
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Header = 'S'
                  HeaderFont.Charset = DEFAULT_CHARSET
                  HeaderFont.Color = clWindowText
                  HeaderFont.Height = -9
                  HeaderFont.Name = 'Tahoma'
                  HeaderFont.Style = []
                  PrintBorders = [cbTop, cbLeft, cbRight, cbBottom]
                  PrintFont.Charset = DEFAULT_CHARSET
                  PrintFont.Color = clWindowText
                  PrintFont.Height = -9
                  PrintFont.Name = 'Tahoma'
                  PrintFont.Style = []
                  Width = 19
                end>
              DataSource = DsBugsN
              InvalidPicture.Data = {
                055449636F6E0000010001002020200000000000A81000001600000028000000
                2000000040000000010020000000000000100000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000006A6A6B256A6A6B606A6A6B946A6A6BC06A6A6BE1
                6A6A6BF86A6A6BF86A6A6BE16A6A6BC06A6A6B946A6A6B606A6A6B2500000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000006A6A6B407575769E787879F19F9F9FF6C0C0C0FDDADADAFFEDEDEEFF
                FBFBFBFFFBFBFBFFEDEDEEFFDADADAFFC0C0C0FD9F9F9FF6787879F17575769E
                6A6A6B4000000000000000000000000000000000000000000000000000000000
                000000000000000000000000000000000000000000000000000000006A6A6B22
                7C7C7C98888889F0BDBDBDFCE9E9EBFED9D9E9FEB5B5DDFE8B8BCDFE595AB7FF
                3739A8FF2B2CA4FF4A49B1FF7171C1FFA1A2D7FFD3D3E8FFEAEAEBFEBEBEBFFC
                888889F07C7C7C986A6A6B220000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000006A6A6B43838383D8
                B7B7B8FAECECEFFEC0C0DFFF7977C4FF2221A0FF12129BFF1010A4FF0C0CA8FF
                0A0AACFF0A0AB4FF0A0AB9FF0D0DBEFF0F0FB1FF1111A6FF5656B8FFAEADDCFF
                ECECEFFEB7B7B8FA838383D86A6A6B4300000000000000000000000000000000
                00000000000000000000000000000000000000006A6A6B4E878788EAD3D3D3FE
                CACAE8FF4443B0FF171799FF11119CFF0C0C98FF0B0B9BFF0B0BA0FF0A0AA6FF
                0909ACFF0909B2FF0808BAFF0707BFFF0B09C8FF0D0DCEFF1111CCFF1010AFFF
                4A49B2FFCFCFEBFFD3D3D3FE878788EA6A6A6B4E000000000000000000000000
                000000000000000000000000000000006A6A6B43878788EAE1E1E1FFA8A8DAFF
                2323A0FF15159CFF0D0D92FF0C0C95FF0C0C99FF0B0B9EFF0B0BA0FF0A0AA6FF
                0909ACFF0909B2FF0808B8FF0808BCFF0808C3FF0C0CC9FF0C0CD0FF0D0DD6FF
                1313CFFF2222A9FFAFAFDEFFE1E1E1FF878788EA6A6A6B430000000000000000
                0000000000000000000000006A6A6B22838383D8D3D3D3FEA8A8D9FF2020A4FF
                13139BFF0C0C92FF0C0C95FF0C0C97FF0C0C99FF0B0B9EFF0B0BA0FF0A0AA4FF
                0A0AA9FF0909B0FF0808B4FF0808BBFF0707C0FF0A0AC6FF0909CCFF0C0CD3FF
                0D0DD8FF1313D3FF1A1AA8FFAEADDEFFD4D4D4FE838383D86A6A6B2200000000
                0000000000000000000000007C7C7C98B7B7B8FACACAE8FF2524A3FF13139FFF
                0C0C97FF0C0C95FF0C0C95FF0C0C91FF0C0C95FF0B0B9EFF0B0BA0FF0A0AA4FF
                0A0AA8FF0909ADFF0909B2FF0808B8FF0808BCFF0707C0FF0808BCFF0707C5FF
                0C0CD3FF0D0DD7FF1212D1FF2020A7FFCDCDEBFFB8B8B9FA7C7C7C9800000000
                00000000000000006A6A6B40888889F0ECECEFFE4545B1FF1616A4FF0B0B9BFF
                0C0C99FF0C0C96FF3333A2FFB9B9D0FF393A9BFF0C0C95FF0B0BA1FF0A0AA4FF
                0A0AA7FF0A0AABFF0909B0FF0808B4FF0808B7FF2F2FC2FFAEAEE2FF4B4BBFFF
                0707BEFF0B0BD1FF0C0CD3FF1413CCFF4848B1FFECECEFFE888889F06A6A6B40
                00000000000000007575769EBFBFBFFD9B9BD5FF1C1CA6FF0C0CA1FF0B0B9FFF
                0B0B9AFF3535A7FFB5B5BEFFE6E6DFFFEDEDEFFF3C3C9CFF0C0C97FF0A0AA4FF
                0A0AA6FF0A0AA9FF0909ADFF0909B0FF2626B5FFCECEDEFFFFFFFBFFEEEEF1FF
                4848BAFF0808BCFF0A0ACDFF0B0BCEFF1111ABFFBEC0E0FFBFC0BFFD7575769E
                000000006A6A6B25787879F1E3E3E5FE4646B2FF1414A8FF0A0AA4FF0B0BA0FF
                2121A9FFBDBDCAFFD0D0C8FFC5C5C5FFE3E3E1FFEDEDEFFF3E3E9EFF0C0C98FF
                0A0AA6FF0A0AA8FF0A0AA9FF2B2BB0FFC0C0CDFFEAEAE2FFEBEBEBFFFEFEF8FF
                EDEDEEFF2828BDFF0707C4FF0809C7FF0F0FC4FF8788CBFFEBEBECFE79797AF1
                6A6A6B256A6A6B609D9E9DF6D6D7E4FF3A3AB3FF1212ADFF0A0AA8FF0A0AA4FF
                1313AAFFABABCFFFD6D6CBFFCACACAFFC6C6C6FFE4E4E0FFEEEEEFFF3F3FA0FF
                0C0C99FF0A0AA6FF2828ABFFB2B2BFFFD8D8CEFFD6D6D8FFE0E0E0FFF6F5EDFF
                D1D1EDFF1E1CC0FF0707BEFF0707BFFF0707C0FF2120AAFFD3D5E9FE9FA0A0F6
                6A6A6B606A6A6B94BDBDBDFBBABBDCFF3A39B7FF2F2FB8FF0909ADFF0A0AA9FF
                0A0AA6FF1515ACFFADADCFFFD6D6CBFFCBCBCAFFC6C6C6FFE4E4E1FFEEEEEFFF
                3838A1FF2222A2FFACABB8FFC8C8C0FFC7C7C8FFCDCDCDFFE1E1D9FFC8CAE1FF
                2424BCFF0808B4FF0808B9FF0808BAFF0808BBFF0F0EABFFA1A2D5FEC0C0C0FC
                6A6A6B946A6A6BC0D9D8D7FE9999D1FF3838BBFF3636BCFF2C2CB7FF0909ADFF
                0A0AA9FF0A0AA4FF1C1CAFFFB1B1CFFFD6D6CBFFCCCCCBFFC7C7C7FFE4E4E1FF
                ECECEEFFACACB7FFC2C2BCFFBEBEBFFFC0C0C0FFCFCFC6FFC1C1D5FF2727B8FF
                0909ACFF0909B2FF0909B2FF0909B4FF0808B4FF0E0EB5FF6E6EBFFFD9D9D9FE
                6A6A6BC06A6A6BE1EBEAEBFF7D7CC7FF3838BFFF3434BEFF3536BEFF2A2AB8FF
                0909B0FF0909ACFF0A0AA8FF1C1CB1FFB2B2D0FFD7D7CCFFCBCBCBFFC7C7C8FF
                C8C8C3FFC6C6C3FFBFBFC1FFBDBDBDFFC5C5BCFFB8B8CEFF2929B5FF0A0AA8FF
                0909ACFF0909ADFF0909AFFF0909AFFF0909AFFF0C0CB0FF4747AFFFECECEDFF
                6A6A6BE16A6A6BF8F9F9F9FF6666C1FF3838C4FF3535C2FF3434C0FF3535BEFF
                3030BCFF1313B4FF0909ADFF0A0AA8FF1E1EB3FFAAAAD0FFD3D3CDFFCCCCCCFF
                C8C8C8FFC3C3C3FFC2C2C1FFC4C4BFFFB2B2CBFF2B2BB4FF0A0AA4FF0A0AA8FF
                0A0AA8FF0A0AA9FF0A0AA9FF0A0AA9FF0A0AA9FF0B0BA9FF3131A6FFFAFAFAFF
                6A6A6BF86A6A6BF8FBFBFBFF5959BEFF3B3BCAFF3A3AC8FF3737C4FF3535C2FF
                3636C0FF3636BEFF2323B8FF0909B1FF0A0AA7FF4949BEFFD6D6D4FFD3D3D1FF
                CDCDCDFFC8C8C8FFC4C4C3FFEDEDEDFF5F5FB3FF0C0C98FF0A0AA7FF0A0AA6FF
                0A0AA6FF0A0AA6FF0A0AA4FF0A0AA6FF0A0AA4FF0B0BA4FF2D2DA6FFFBFBFBFF
                6A6A6BF86A6A6BE1EDEDEEFF7F80CBFF4041CCFF3C3CCAFF3A3AC8FF383AC8FF
                3838C4FF3636C2FF3939C0FF2123B7FF4A4AC2FFCBCBDEFFE0E0DCFFD6D6D6FF
                D2D2D3FFCDCDCEFFC9C9C9FFE2E2E1FFF1F1F2FF4242A3FF0C0C99FF0A0AA4FF
                0A0AA4FF0A0AA4FF0B0BA3FF0B0BA3FF0B0BA1FF0E0EA1FF4443B0FFEDEDEEFF
                6A6A6BE16A6A6BC0DADADAFF9C9BD5FE4949CDFF3E3DD0FF3C3DCEFF3C3CCAFF
                3A3AC8FF3B39C7FF2828BDFF5C5CCCFFE5E5EDFFF4F4EDFFE5E5E6FFDEDEDEFF
                DCDCD9FFD9D9D3FFCDCDCDFFC8C8C8FFE5E5E1FFF1F1F3FF3F3FA0FF0C0C99FF
                0A0AA4FF0B0BA1FF0B0BA0FF0B0BA0FF0B0B9FFF1313A2FF6B6BC0FFDADADAFF
                6A6A6BC06A6A6B94C0C0C0FDBDBAE1FE5655CFFF4141D4FF3F3FD2FF3F3FCEFF
                3D3DCCFF2C2AC3FF5E5ED3FFEBEBF6FFFFFFFAFFF1F1F1FFEDEDEEFFF0F0E9FF
                D2D2E6FFBDBDD6FFDADAD3FFCFCFCFFFC9C9CAFFE5E5E2FFF1F1F3FF3A3AA0FF
                0C0C98FF0B0BA3FF0B0B9FFF0B0B9EFF0B0B9EFF1C1CA4FF9C9CD3FFC1C1C1FD
                6A6A6B946A6A6B609F9F9FF6DAD9EAFF6B6BCFFF4444D7FF4143D6FF4242D3FF
                3434CDFF6464DBFFEFEFFFFFFFFFFFFFFCFCFCFFF6F6F6FFFCFCF4FFE2E1F0FF
                5050CCFF4040C1FFC3C3DBFFE1E1D8FFD4D4D5FFCFCFCFFFE8E8E5FFF2F2F4FF
                4040A2FF0C0C99FF0F0FA2FF0F0FA0FF0F0F9DFF302FA9FFD1D1E8FEA0A0A0F6
                6A6A6B606A6A6B25787879F1E9E9EBFEA7A7DAFF6060DBFF4547DBFF3C3CD6FF
                5857DEFFF2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E8F8FF5B5BD4FF
                2828BDFF2A2BBDFF4949C5FFC3C3DBFFE4E4DAFFD5D5D5FFCECED0FFE8E8E5FF
                F4F4F4FF4949AFFF2121A6FF2A2AA6FF2C2BA9FF5557B8FFEAEAECFE787879F1
                6A6A6B25000000007575769EBEBEBEFDC9CAE6FF7A79DBFF4C4CDFFF4141DBFF
                5757E0FFEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E7FFFF5B5BD7FF2E2EC6FF
                3E3EC9FF3A3AC5FF2C2EC1FF4A49C8FFC2C2DDFFE3E3DAFFD5D5D4FFDADAD3FF
                CACBD9FF4747BBFF2525ADFF2C2BACFF3332AEFFA5A4D8FFBFBFBFFD7575769E
                00000000000000006A6A6B40888889F0ECECEFFE9696D6FF7B7BE3FF4D4BE0FF
                4141DBFF5F5FE6FFE7E7FFFFFFFFFFFFE9E9FFFF5A5ADCFF3333CAFF4242CFFF
                4040CBFF3D3DC9FF3D3EC8FF3030C2FF4848C9FFC0C0DDFFECEEDEFFD0D0E0FF
                5554C7FF2828B3FF3232B4FF3434B1FF5453B7FFECECEFFE888889F06A6A6B40
                0000000000000000000000007C7C7C98B7B7B8FAD0D0ECFF8F8FDBFF6868E3FF
                4E4EE2FF3E40DBFF6565E9FFB2B2F7FF6565E4FF393BD2FF4646D7FF4343D4FF
                4343D1FF4242CFFF4040CBFF3F3FCAFF3333C4FF4E4ECBFF9E9EE2FF5C5BCFFF
                292ABAFF3636BCFF3938B8FF3F3EB1FFCBCBE9FFB7B7B8FA7C7C7C9800000000
                0000000000000000000000006A6A6B22838383D8D3D3D3FEB5B5E2FF9E9EE4FF
                6766E2FF4E50E6FF4646E0FF3D3DDAFF4444DCFF4B4BDCFF4848DBFF4847D9FF
                4646D5FF4443D3FF4343D1FF4242CFFF4143CDFF3A3AC8FF312FC5FF3535C3FF
                3C3CC3FF3D3DBEFF403FB5FFACACDCFFD3D3D3FE838383D86A6A6B2200000000
                000000000000000000000000000000006A6A6B43878788EAE1E1E1FFB5B5E2FF
                A7A6E4FF7877E5FF5151E5FF4F4FE4FF4E4EE2FF4D4DE0FF4C4CDEFF4B4BDCFF
                4949DBFF4848D7FF4747D5FF4545D3FF4545D1FF4343CFFF4242CCFF3F3FCBFF
                4343C2FF4645B6FFADADDCFFE1E1E1FF878788EA6A6A6B430000000000000000
                00000000000000000000000000000000000000006A6A6B4E878788EAD3D3D3FE
                D0D0ECFFAAA9DFFFA2A2ECFF6565E3FF5151E6FF4F4FE4FF4F4DE4FF4D4DE0FF
                4D4DDFFF4D4DDCFF4C49DBFF4A4AD8FF4749D6FF4747D4FF4949CBFF4B4BC3FF
                8E8ED0FFCDCCE8FFD3D3D3FE878788EA6A6A6B4E000000000000000000000000
                0000000000000000000000000000000000000000000000006A6A6B43838383D8
                B7B7B8FAECECEFFEC3C2E5FFADAEE1FF9E9DE8FF6F6FE0FF5C5CE1FF5452E2FF
                5051E1FF4F4FDFFF4F4FDBFF5150D6FF5151CFFF5F5FC8FFA1A1D3FEC7C8E0FE
                E4E4E7FEB7B7B8FA838383D86A6A6B4300000000000000000000000000000000
                000000000000000000000000000000000000000000000000000000006A6A6B22
                7C7C7C98888889F0BFBFBFFDEBEBECFED8D9EBFEBDBDE4FEA8A7DCFF9695D7FF
                8886D4FF7F7DCEFF8C8BD2FFA1A2D9FFC0BEE1FED9D9EAFEEAEAECFEBFBFBFFD
                888889F07C7C7C986A6A6B220000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000006A6A6B407575769E787879F19F9F9FF6C0C0C0FDDADADAFFEDEDEEFF
                FBFBFBFFFBFBFBFFEDEDEEFFDADADAFFC0C0C0FD9F9F9FF6787879F17575769E
                6A6A6B4000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000006A6A6B256A6A6B606A6A6B946A6A6BC06A6A6BE1
                6A6A6BF86A6A6BF86A6A6BE16A6A6BC06A6A6B946A6A6B606A6A6B2500000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000FFC003FFFF0000FFFC00003FF800001FF000000FE0000007C0000003
                C000000380000001800000010000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000080000001
                80000001C0000003C0000003E0000007F000000FF800001FFC00003FFF0000FF
                FFC003FF}
              ShowUnicode = False
              ColWidths = (
                20
                45
                19
                81
                19
                170
                19)
            end
          end
          object GroupBox11: TGroupBox
            Left = 821
            Top = 0
            Width = 183
            Height = 260
            Align = alClient
            Caption = ' Servi'#231'os oferecidos:  '
            TabOrder = 2
            object DBGDesServico: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 179
              Height = 243
              Align = alClient
              DataSource = DsDesServicos
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              OnCellClick = DBGDesServicoCellClick
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Servi'#231'o'
                  Width = 343
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 517
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 561
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 140
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Seleciona'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 16
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Reabre'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtReabreClick
      end
    end
  end
  object QrContratada: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial,  Nome) CONTRATADA'
      'FROM Entidades'
      'ORDER BY CONTRATADA')
    Left = 198
    Top = 95
    object QrContratadaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContratadaCONTRATADA: TWideStringField
      FieldName = 'CONTRATADA'
      Size = 100
    end
  end
  object DsContratada: TDataSource
    DataSet = QrContratada
    Left = 227
    Top = 95
  end
  object QrContratante: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial,  Nome) CONTRATANTE'
      'FROM Entidades'
      'ORDER BY CONTRATANTE')
    Left = 255
    Top = 95
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContratanteCONTRATANTE: TWideStringField
      FieldName = 'CONTRATANTE'
      Size = 100
    end
  end
  object DsContratante: TDataSource
    DataSet = QrContratante
    Left = 283
    Top = 95
  end
  object QrContratos: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrContratosAfterOpen
    BeforeClose = QrContratosBeforeClose
    SQL.Strings = (
      'SELECT con.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NCONTRATADA,'
      'IF(enb.Tipo=0, enb.RazaoSocial, enb.Nome) NCONTRATANTE,'
      'IF(rep.Tipo=0, rep.RazaoSocial, rep.Nome) NO_REP_LEGAL,'
      'IF(ts1.Tipo=0, ts1.RazaoSocial, ts1.Nome) NO_TESTEM_01,'
      'IF(ts2.Tipo=0, ts2.RazaoSocial, ts2.Nome) NO_TESTEM_02'
      'FROM contratos con'
      'LEFT JOIN entidades ent ON ent.Codigo = con.Contratada'
      'LEFT JOIN entidades enb ON enb.Codigo = con.Contratante'
      'LEFT JOIN entidades rep ON rep.Codigo = con.RepreLegal'
      'LEFT JOIN entidades ts1 ON ts1.Codigo = con.Testemun01'
      'LEFT JOIN entidades ts2 ON ts2.Codigo = con.Testemun02'
      'WHERE con.Ativo=1'
      'AND con.DInstal <= SYSDATE()'
      'AND con.DInstal > 1'
      'AND con.DtaCntrFim >= SYSDATE()'
      'AND con.Contratada=:P0'
      'AND Contratante=:P1'
      'ORDER BY Nome')
    Left = 356
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrContratosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContratosNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrContratosVersao: TFloatField
      FieldName = 'Versao'
      DisplayFormat = '0.00'
    end
    object QrContratosDInstal: TDateField
      FieldName = 'DInstal'
      OnGetText = QrContratosDInstalGetText
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosDtaCntrFim: TDateField
      FieldName = 'DtaCntrFim'
      OnGetText = QrContratosDtaCntrFimGetText
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosDVencimento: TDateField
      FieldName = 'DVencimento'
      OnGetText = QrContratosDVencimentoGetText
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrContratosContratada: TIntegerField
      FieldName = 'Contratada'
    end
    object QrContratosContratante: TIntegerField
      FieldName = 'Contratante'
    end
    object QrContratosDContrato: TDateField
      FieldName = 'DContrato'
    end
    object QrContratosStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrContratosValorMes: TFloatField
      FieldName = 'ValorMes'
    end
    object QrContratosTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrContratosWinDocArq: TWideStringField
      FieldName = 'WinDocArq'
      Size = 255
    end
    object QrContratosFormaUso: TSmallintField
      FieldName = 'FormaUso'
    end
    object QrContratosDtaPropIni: TDateField
      FieldName = 'DtaPropIni'
    end
    object QrContratosDtaPropFim: TDateField
      FieldName = 'DtaPropFim'
    end
    object QrContratosDtaImprime: TDateTimeField
      FieldName = 'DtaImprime'
    end
    object QrContratosDtaAssina: TDateField
      FieldName = 'DtaAssina'
    end
    object QrContratosTratamento: TWideStringField
      FieldName = 'Tratamento'
      Size = 30
    end
    object QrContratosArtigoDef: TSmallintField
      FieldName = 'ArtigoDef'
    end
    object QrContratosPerDefImpl: TSmallintField
      FieldName = 'PerDefImpl'
    end
    object QrContratosPerQtdImpl: TSmallintField
      FieldName = 'PerQtdImpl'
    end
    object QrContratosPerDefTrei: TSmallintField
      FieldName = 'PerDefTrei'
    end
    object QrContratosPerQtdTrei: TSmallintField
      FieldName = 'PerQtdTrei'
    end
    object QrContratosPerDefMoni: TSmallintField
      FieldName = 'PerDefMoni'
    end
    object QrContratosPerQtdMoni: TSmallintField
      FieldName = 'PerQtdMoni'
    end
    object QrContratosPerDefIniI: TSmallintField
      FieldName = 'PerDefIniI'
    end
    object QrContratosPerQtdIniI: TSmallintField
      FieldName = 'PerQtdIniI'
    end
    object QrContratosRepreLegal: TIntegerField
      FieldName = 'RepreLegal'
    end
    object QrContratosTestemun01: TIntegerField
      FieldName = 'Testemun01'
    end
    object QrContratosTestemun02: TIntegerField
      FieldName = 'Testemun02'
    end
    object QrContratosPercMulta: TFloatField
      FieldName = 'PercMulta'
    end
    object QrContratosPercJuroM: TFloatField
      FieldName = 'PercJuroM'
    end
    object QrContratosTpPagDocu: TWideStringField
      FieldName = 'TpPagDocu'
      Size = 50
    end
    object QrContratosddMesVcto: TSmallintField
      FieldName = 'ddMesVcto'
    end
    object QrContratosLugarNome: TWideStringField
      FieldName = 'LugarNome'
      Size = 100
    end
    object QrContratosLugArtDef: TSmallintField
      FieldName = 'LugArtDef'
    end
    object QrContratosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContratosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContratosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContratosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrContratosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrContratosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrContratosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrContratosEscopoSP: TWideStringField
      FieldName = 'EscopoSP'
      Size = 255
    end
    object QrContratosDtaPrxRenw: TDateField
      FieldName = 'DtaPrxRenw'
    end
    object QrContratosNCONTRATADA: TWideStringField
      FieldName = 'NCONTRATADA'
      Size = 100
    end
    object QrContratosNCONTRATANTE: TWideStringField
      FieldName = 'NCONTRATANTE'
      Size = 100
    end
    object QrContratosNO_REP_LEGAL: TWideStringField
      FieldName = 'NO_REP_LEGAL'
      Size = 100
    end
    object QrContratosNO_TESTEM_01: TWideStringField
      FieldName = 'NO_TESTEM_01'
      Size = 100
    end
    object QrContratosNO_TESTEM_02: TWideStringField
      FieldName = 'NO_TESTEM_02'
      Size = 100
    end
    object QrContratosNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 30
    end
  end
  object DsContratos: TDataSource
    DataSet = QrContratos
    Left = 384
    Top = 96
  end
  object QrBugsS: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrBugsSAfterOpen
    SQL.Strings = (
      'SELECT oa.*, ELT(oa.Ativo + 1, "N", "S") NO_Ativo '
      'FROM _filtrobugss oa '
      'ORDER BY oa.o2Gru, oa.n2Gru, '
      'oa.c2Gru, oa.o1Cab, oa.n1Cab, oa.c1Cab ')
    Left = 52
    Top = 208
    object QrBugsSo2Gru: TIntegerField
      FieldName = 'o2Gru'
    end
    object QrBugsSc2Gru: TIntegerField
      FieldName = 'c2Gru'
    end
    object QrBugsSn2Gru: TWideStringField
      FieldName = 'n2Gru'
      Size = 60
    end
    object QrBugsSa2Gru: TSmallintField
      FieldName = 'a2Gru'
    end
    object QrBugsSo1Cab: TIntegerField
      FieldName = 'o1Cab'
    end
    object QrBugsSc1Cab: TIntegerField
      FieldName = 'c1Cab'
    end
    object QrBugsSn1Cab: TWideStringField
      FieldName = 'n1Cab'
      Size = 60
    end
    object QrBugsSa1Cab: TSmallintField
      FieldName = 'a1Cab'
    end
    object QrBugsSAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrBugsSNO_Ativo: TWideStringField
      FieldName = 'NO_Ativo'
      Size = 1
    end
  end
  object DsBugsS: TDataSource
    DataSet = QrBugsS
    Left = 52
    Top = 256
  end
  object QrBugsN: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrBugsNAfterOpen
    SQL.Strings = (
      'SELECT oa.*, ELT(oa.Ativo + 1, "N", "S") NO_Ativo '
      'FROM _filtrobugsn oa '
      'ORDER BY oa.o2Gru, oa.n2Gru, '
      'oa.c2Gru, oa.o1Cab, oa.n1Cab, oa.c1Cab ')
    Left = 112
    Top = 208
    object QrBugsNo2Gru: TIntegerField
      FieldName = 'o2Gru'
    end
    object QrBugsNc2Gru: TIntegerField
      FieldName = 'c2Gru'
    end
    object QrBugsNn2Gru: TWideStringField
      FieldName = 'n2Gru'
      Size = 60
    end
    object QrBugsNa2Gru: TSmallintField
      FieldName = 'a2Gru'
    end
    object QrBugsNo1Cab: TIntegerField
      FieldName = 'o1Cab'
    end
    object QrBugsNc1Cab: TIntegerField
      FieldName = 'c1Cab'
    end
    object QrBugsNn1Cab: TWideStringField
      FieldName = 'n1Cab'
      Size = 60
    end
    object QrBugsNa1Cab: TSmallintField
      FieldName = 'a1Cab'
    end
    object QrBugsNAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrBugsNNO_Ativo: TWideStringField
      FieldName = 'NO_Ativo'
      Size = 1
    end
  end
  object DsBugsN: TDataSource
    DataSet = QrBugsN
    Left = 112
    Top = 252
  end
  object QrPsq1: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * FROM _rece_desp_'
      'WHERE cN1=189')
    Left = 172
    Top = 208
    object QrPsq1o2Gru: TIntegerField
      FieldName = 'o2Gru'
    end
    object QrPsq1c2Gru: TIntegerField
      FieldName = 'c2Gru'
    end
    object QrPsq1n2Gru: TWideStringField
      FieldName = 'n2Gru'
      Size = 60
    end
    object QrPsq1a2Gru: TSmallintField
      FieldName = 'a2Gru'
    end
    object QrPsq1o1Cab: TIntegerField
      FieldName = 'o1Cab'
    end
    object QrPsq1c1Cab: TIntegerField
      FieldName = 'c1Cab'
    end
    object QrPsq1n1Cab: TWideStringField
      FieldName = 'n1Cab'
      Size = 60
    end
    object QrPsq1a1Cab: TSmallintField
      FieldName = 'a1Cab'
    end
    object QrPsq1Ativo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrDesServicos: TmySQLQuery
    Database = DModG.AllID_DB
    Left = 232
    Top = 208
    object QrDesServicosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDesServicosNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsDesServicos: TDataSource
    DataSet = QrDesServicos
    Left = 232
    Top = 252
  end
  object QrPsqP: TmySQLQuery
    Database = DModG.AllID_DB
    Left = 172
    Top = 256
    object QrPsqPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 4
    Top = 12
  end
end
