unit ContratRnw;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables;

type
  TFmContratRnw = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DBGrid1: TDBGrid;
    QrContratos: TmySQLQuery;
    DsContratos: TDataSource;
    LaTotal: TLabel;
    QrContratosCodigo: TIntegerField;
    QrContratosNome: TWideStringField;
    QrContratosNO_CONTRATANTE: TWideStringField;
    QrContratosDtaPrxRenw: TDateField;
    QrContratosContratante: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrContratosAfterScroll(DataSet: TDataSet);
    procedure QrContratosBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    procedure MostraContratos(Codigo: Integer; AbrirEmAba: Boolean = False);
  public
    { Public declarations }
    FHoje: TDateTime;
    procedure ReopenContratos(Hoje: TDateTime);
  end;

var
  FmContratRnw: TFmContratRnw;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, Contratos, Principal,
  {$IfNDef SNoti} UnitNotificacoesEdit, {$EndIf}
  MyDBCheck;

{$R *.DFM}

procedure TFmContratRnw.BtOKClick(Sender: TObject);
begin
  MostraContratos(QrContratos.FieldByName('Codigo').Value);
  //
  if FHoje <> 0 then
    ReopenContratos(FHoje);
end;

procedure TFmContratRnw.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContratRnw.MostraContratos(Codigo: Integer;
  AbrirEmAba: Boolean = False);
var
  Form: TForm;
begin
  if not AbrirEmAba then
  begin
    if DBCheck.CriaFm(TFmContratos, FmContratos, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
        FmContratos.LocCod(Codigo, 0);
      FmContratos.ShowModal;
      FmContratos.Destroy;
    end;
  end else
  begin
    Form := MyObjects.FormTDICria(TFmContratos, FmPrincipal.PageControl1,
            FmPrincipal.AdvToolBarPagerNovo);
    if Codigo <> 0 then
      TFmContratos(Form).LocCod(Codigo, 0);
  end;
end;

procedure TFmContratRnw.QrContratosAfterScroll(DataSet: TDataSet);
begin
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrContratos.RecordCount)
end;

procedure TFmContratRnw.QrContratosBeforeClose(DataSet: TDataSet);
begin
  LaTotal.Caption := '';
end;

procedure TFmContratRnw.DBGrid1DblClick(Sender: TObject);
begin
  MostraContratos(QrContratos.FieldByName('Codigo').Value);
end;

procedure TFmContratRnw.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContratRnw.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FHoje := 0;
end;

procedure TFmContratRnw.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContratRnw.ReopenContratos(Hoje: TDateTime);
var
  DataFutura: String;
begin
  DataFutura := Geral.FDT(Hoje + 30, 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrContratos, Dmod.MyDB,
    ['SELECT ctr.Codigo, ctr.Nome, ctr.Contratada, ctr.Contratante, ',
    'ctr.Status, ELT(ctr.Status + 1, "Cadastrando", "N�o aprovado", ',
    '"Aprovado", "Renovado", "Rescindido", "?") NO_STATUS, ',
    'ctr.ValorMes, ctr.DtaPrxRenw, ',
    ' IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CONTRATANTE ',
    'FROM contratos ctr ',
    'LEFT JOIN entidades ent ON ent.Codigo=ctr.Contratante ', 'WHERE ( ',
    '    ctr.Status IN (2,3) ', '    OR ', '    ctr.DtaCntrFim < "1900-01-01" ',
    ') ', 'AND ( ', '    ctr.DtaPrxRenw BETWEEN "1900-01-01" ',
    '    AND "' + DataFutura + '" ', ') ', 'ORDER BY ctr.DtaPrxRenw ', '']);
  {$IfNDef SNoti}
  if QrContratos.RecordCount > 0 then
    UnNotificacoesEdit.CriaNotificacaoPublica(Dmod.MyDB, nftCntrAVencer, DmodG.ObtemAgora(True))
  else
    UnNotificacoesEdit.ExcluiNotificacao(Dmod.MyDB, Integer(nftCntrAVencer));
  {$EndIf}
end;

end.
