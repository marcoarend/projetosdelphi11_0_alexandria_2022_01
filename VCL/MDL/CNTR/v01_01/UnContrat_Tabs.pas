unit UnContrat_Tabs;
{ Colocar no MyListas:

Uses UnContrat_Tabs;


//
function TMyListas.CriaListaTabelas(:
      UnUnContrat_Tabs.CarregaListaTabelas(Lista);
//
function TMyListas.CriaListaSQL:
    UnUnContrat_Tabs.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:
    UnUnContrat_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      UnUnContrat_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      UnUnContrat_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    UnUnContrat_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  UnUnContrat_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms, UnInternalConsts, dmkGeral, UnDmkEnums;

type
  TUnContrat_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(TabelaBase: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  Contrat_Tabs: TUnContrat_Tabs;

const
  CO_CONTRAT_STATUS_CONTRATO_COD_CADASTRD = 0;
  CO_CONTRAT_STATUS_CONTRATO_COD_NAOAPROV = 1;
  CO_CONTRAT_STATUS_CONTRATO_COD_APROVADO = 2;
  CO_CONTRAT_STATUS_CONTRATO_COD_RENOVADO = 3;
  CO_CONTRAT_STATUS_CONTRATO_COD_RESCINDI = 4;

  CO_CONTRAT_STATUS_CONTRATO_TXT_CADASTRD = 'Cadastrado';
  CO_CONTRAT_STATUS_CONTRATO_TXT_NAOAPROV = 'N�o aprovado';
  CO_CONTRAT_STATUS_CONTRATO_TXT_APROVADO = 'Aprovado';
  CO_CONTRAT_STATUS_CONTRATO_TXT_RENOVADO = 'Renovado';
  CO_CONTRAT_STATUS_CONTRATO_TXT_RESCINDI = 'Rescindido';

  MaxStatusContratos = 4;
  sListaStatusContratos: array[0..MaxStatusContratos]of String = (
    CO_CONTRAT_STATUS_CONTRATO_TXT_CADASTRD,
    CO_CONTRAT_STATUS_CONTRATO_TXT_NAOAPROV,
    CO_CONTRAT_STATUS_CONTRATO_TXT_APROVADO,
    CO_CONTRAT_STATUS_CONTRATO_TXT_RENOVADO,
    CO_CONTRAT_STATUS_CONTRATO_TXT_RESCINDI);

  //

implementation

uses UMySQLModule;

function TUnContrat_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
{
  if Uppercase(Tabela) = Uppercase('???') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo|SloganFoot');
    FListaSQL.Add('1|"O justo viver� por f�! Rm 1.17"');
  end else
  if Uppercase(Tabela) = Uppercase('??') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Numero|Login');
    FListaSQL.Add('0|""');
  end;
}
end;

function TUnContrat_Tabs.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  try
    MyLinguas.AdTbLst(Lista, False, Lowercase('Contratos'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('ContratLoc'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('ContratTag'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('ContratDds'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('ContrDfCab'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('ContrDfIts'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('ContrNivTg'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('IGPM'), '');
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnContrat_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('') then
    begin
    {
    end else
    if Uppercase(Tabela) = Uppercase('ProtocoOco') then
    begin
      FListaSQL.Add('Codigo|Nome');
      FListaSQL.Add('0|"Lan�amentos"');
    }
    end else
    ;
  except
    raise;
    Result := False;
  end;
end;


function TUnContrat_Tabs.CarregaListaFRIndices(TabelaBase: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  //
  if Uppercase(TabelaBase) = Uppercase('Contratos') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
(*
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Numero';
    FLIndices.Add(FRIndices);
    //
*)
  end else
  if Uppercase(TabelaBase) = Uppercase('ContratLoc') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Tabela';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Cadastro';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('ContratTag') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Tag';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('ContratDds') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('ContrDfCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('ContrDfIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('ContrNivTg') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('IGPM') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'AnoMes';
    FLIndices.Add(FRIndices);
    //
  end;
end;

function TUnContrat_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    if Uppercase(Tabela) = Uppercase('contratos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Contratada';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Contratante';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DContrato';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DInstal';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DVencimento';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaPrxRenw';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Status';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorMes';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'WinDocArq';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FormaUso';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1'; // 0=Nenhum 1=Texto 2=WinDocArq
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaPropIni';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaPropFim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaCntrFim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaImprime';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaAssina';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'Numero';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'Tratamento';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArtigoDef';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; //0=o 1=a
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerDefImpl'; // Implanta��o / Desenvolvimento
      FRCampos.Tipo       := 'tinyint(1)'; // 0=Dias, 1=Semanas, 2=Meses, 3=Anos
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerQtdImpl';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerDefTrei'; // Treinamento
      FRCampos.Tipo       := 'tinyint(1)'; // 0=Dias, 1=Semanas, 2=Meses, 3=Anos
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerQtdTrei';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerDefMoni'; // Monitoramento
      FRCampos.Tipo       := 'tinyint(1)'; // 0=Dias, 1=Semanas, 2=Meses, 3=Anos
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerQtdMoni';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerDefIniI'; // Parazo in�cio implanta��o
      FRCampos.Tipo       := 'tinyint(1)'; // 0=Dias, 1=Semanas, 2=Meses, 3=Anos
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerQtdIniI';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RepreLegal';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Testemun01';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Testemun02';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PercMulta';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PercJuroM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TpPagDocu';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'boleto banc�rio';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ddMesVcto';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'Servicos01';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Servicos02';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'LugarNome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LugArtDef';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; //0=o 1=a
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Versao';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EscopoSP';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ContratLoc') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0-ND 1-Dependencia 2=DependType (usar�???)
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cadastro';  // Bugstrol > SiapImaCad.Codigo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // 2013-09-02
      New(FRCampos);
      FRCampos.Field      := 'SubCliente';  // Bugstrol > Entidades.Codigo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // FIM 2013-09-02
      //
    end else
    if Uppercase(Tabela) = Uppercase('ContratTag') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tag';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Text';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ContratDds') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';   // 0 > Primeiro, 1 > proximos
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dias';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ContrDfCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ContrDfIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tag';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Text';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ContrNivTg') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CtrTagNiv1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('IGPM') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'AnoMes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IGPM_D';
      FRCampos.Tipo       := 'double(20,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnContrat_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      New(FRCampos);
      FRCampos.Field      := 'Contratos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else  
    if Uppercase(Tabela) = Uppercase('OpcoesGerl') then
    begin
      New(FRCampos);
      FRCampos.Field      := 'ContrtIPv4';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '127.0.0.1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ContrtSDir';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '|C:|Dermatek|Textos|Contratos';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnContrat_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  // CAD-CONTR-001 :: Cadastro de Contratos
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONTR-001';
  FRJanelas.Nome      := 'FmContratos';
  FRJanelas.Descricao := 'Cadastro de Contratos';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONTR-002 :: Impress�o de Contrato Personalizado
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONTR-002';
  FRJanelas.Nome      := 'FmContratMSWord';
  FRJanelas.Descricao := 'Impress�o de Contrato Personalizado';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONTR-003 :: Texto Exclusivo de Contrato
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONTR-003';
  FRJanelas.Nome      := 'FmContratTag';
  FRJanelas.Descricao := 'Texto Exclusivo de Contrato';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONTR-004 :: Pesquisa de Contrato
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONTR-004';
  FRJanelas.Nome      := 'FmContratPsq';
  FRJanelas.Descricao := 'Pesquisa de Contrato';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONTR-005 :: Grupos de Tags Padr�es de Contratos
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONTR-005';
  FRJanelas.Nome      := 'FmContrDfCab';
  FRJanelas.Descricao := 'Grupos de Tags Padr�es de Contratos';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONTR-006 :: Itens de Tags Padr�es de Contratos
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONTR-006';
  FRJanelas.Nome      := 'FmContrDfIts';
  FRJanelas.Descricao := 'Itens de Tags Padr�es de Contratos';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONTR-007 :: Locais Contemplados no Contrato
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONTR-007';
  FRJanelas.Nome      := 'FmContratLoc';
  FRJanelas.Descricao := 'Locais Contemplados no Contrato';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONTR-008 :: Intervalos de Monitoramentos
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONTR-008';
  FRJanelas.Nome      := 'FmContratDds';
  FRJanelas.Descricao := 'Intervalos de Monitoramentos';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONTR-009 :: Sele��o de Tags em N�veis para Contrato
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONTR-009';
  FRJanelas.Nome      := 'FmContrNiv';
  FRJanelas.Descricao := 'Sele��o de Tags em N�veis para Contrato';
  FLJanelas.Add(FRJanelas);
  //
  // CAD-CONTR-010 :: Status do Contrato
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-CONTR-010';
  FRJanelas.Nome      := 'FmCtrStatus';
  FRJanelas.Descricao := 'Status do Contrato';
  FLJanelas.Add(FRJanelas);
  //
  // TAB-IGPM1-001 :: IGP-M
  New(FRJanelas);
  FRJanelas.ID        := 'TAB-IGPM1-001';
  FRJanelas.Nome      := 'FmIGPM';
  FRJanelas.Descricao := 'IGP-M';
  FLJanelas.Add(FRJanelas);
  //
  // TAB-IGPM1-002 :: IGP-M - Cadastro
  New(FRJanelas);
  FRJanelas.ID        := 'TAB-IGPM1-002';
  FRJanelas.Nome      := 'FmIGPMCad';
  FRJanelas.Descricao := 'IGP-M - Cadastro';
  FLJanelas.Add(FRJanelas);
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
