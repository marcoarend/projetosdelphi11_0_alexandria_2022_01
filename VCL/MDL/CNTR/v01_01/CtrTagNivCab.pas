unit CtrTagNivCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, dmkMemo, UnDmkEnums;

type
  TFmCtrTagNivCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrCtrTagNiv3: TmySQLQuery;
    DsCtrTagNiv3: TDataSource;
    QrCtrTagNiv2: TmySQLQuery;
    DsCtrTagNiv2: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtNivel3: TBitBtn;
    BtNivel2: TBitBtn;
    QrCtrTagNiv3NivSup: TIntegerField;
    QrCtrTagNiv3Codigo: TIntegerField;
    QrCtrTagNiv3Nome: TWideStringField;
    QrCtrTagNiv3Tag: TWideStringField;
    QrCtrTagNiv3Conteudo: TWideMemoField;
    QrCtrTagNiv3Ordem: TIntegerField;
    QrCtrTagNiv3Lk: TIntegerField;
    QrCtrTagNiv3DataCad: TDateField;
    QrCtrTagNiv3DataAlt: TDateField;
    QrCtrTagNiv3UserCad: TIntegerField;
    QrCtrTagNiv3UserAlt: TIntegerField;
    QrCtrTagNiv3AlterWeb: TSmallintField;
    QrCtrTagNiv3Ativo: TSmallintField;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label4: TLabel;
    EdTag: TdmkEdit;
    MeConteudo: TdmkMemo;
    DBMemo1: TDBMemo;
    QrCtrTagNiv2NivSup: TIntegerField;
    QrCtrTagNiv2Codigo: TIntegerField;
    QrCtrTagNiv2Nome: TWideStringField;
    QrCtrTagNiv2Tag: TWideStringField;
    QrCtrTagNiv2Conteudo: TWideMemoField;
    QrCtrTagNiv2Ordem: TIntegerField;
    QrCtrTagNiv2Lk: TIntegerField;
    QrCtrTagNiv2DataCad: TDateField;
    QrCtrTagNiv2DataAlt: TDateField;
    QrCtrTagNiv2UserCad: TIntegerField;
    QrCtrTagNiv2UserAlt: TIntegerField;
    QrCtrTagNiv2AlterWeb: TSmallintField;
    QrCtrTagNiv2Ativo: TSmallintField;
    QrCtrTagNiv1: TmySQLQuery;
    DsCtrTagNiv1: TDataSource;
    QrCtrTagNiv1NivSup: TIntegerField;
    QrCtrTagNiv1Codigo: TIntegerField;
    QrCtrTagNiv1Nome: TWideStringField;
    QrCtrTagNiv1Tag: TWideStringField;
    QrCtrTagNiv1Conteudo: TWideMemoField;
    QrCtrTagNiv1Ordem: TIntegerField;
    QrCtrTagNiv1Lk: TIntegerField;
    QrCtrTagNiv1DataCad: TDateField;
    QrCtrTagNiv1DataAlt: TDateField;
    QrCtrTagNiv1UserCad: TIntegerField;
    QrCtrTagNiv1UserAlt: TIntegerField;
    QrCtrTagNiv1AlterWeb: TSmallintField;
    QrCtrTagNiv1Ativo: TSmallintField;
    PnNivel1: TPanel;
    Splitter1: TSplitter;
    DBGNiv1: TDBGrid;
    MeNivel1: TDBMemo;
    PnNivel2: TPanel;
    DBGNivel2: TDBGrid;
    MeNivel2: TDBMemo;
    BtNivel1: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCtrTagNiv3AfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCtrTagNiv3BeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrCtrTagNiv3AfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtNivel3Click(Sender: TObject);
    procedure BtNivel2Click(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrCtrTagNiv3BeforeClose(DataSet: TDataSet);
    procedure QrCtrTagNiv2BeforeClose(DataSet: TDataSet);
    procedure QrCtrTagNiv2AfterScroll(DataSet: TDataSet);
    procedure BtNivel1Click(Sender: TObject);
  private
    FNivelSel: Integer;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraCtrTagNivX(SQLType: TSQLType; Nivel: Integer);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenCtrTagNiv2(Codigo: Integer);
    procedure ReopenCtrTagNiv1(Codigo: Integer);

  end;

var
  FmCtrTagNivCab: TFmCtrTagNivCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, CtrTagNivX, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCtrTagNivCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCtrTagNivCab.MostraCtrTagNivX(SQLType: TSQLType; Nivel: Integer);
begin
  if DBCheck.CriaFm(TFmCtrTagNivX, FmCtrTagNivX, afmoNegarComAviso) then
  begin
    FmCtrTagNivX.ImgTipo.SQLType := SQLType;
    FmCtrTagNivX.EdNivelAcima.ValueVariant := Nivel + 1;
    FmCtrTagNivX.EdOrdem.ValueVariant := 100;
    case Nivel of
      1:
      begin
        FmCtrTagNivX.FQrIts := QrCtrTagNiv1;
        //
        FmCtrTagNivX.EdNivSup.ValueVariant  := QrCtrTagNiv2Codigo.Value;
        FmCtrTagNivX.EdSupNome.ValueVariant := QrCtrTagNiv2Nome.Value;
      end;
      2:
      begin
        FmCtrTagNivX.FQrIts := QrCtrTagNiv2;
        //
        FmCtrTagNivX.EdNivSup.ValueVariant  := QrCtrTagNiv3Codigo.Value;
        FmCtrTagNivX.EdSupNome.ValueVariant := QrCtrTagNiv3Nome.Value;
      end;
      { N�o precisa � o CtrTagNivCab!
      3:
      begin
        FmCtrTagNivX.FQrIts := QrCtrTagNiv3;
        //
        FmCtrTagNivX.EdNivSup.ValueVariant  := QrCtrTagNiv4Codigo.Value;
        FmCtrTagNivX.EdSupNome.ValueVariant := QrCtrTagNiv4Nome.Value;
      end;
      }
    end;
    if SQLType = stIns then
      //
    else
    begin
      case Nivel of
        1:
        begin
          FmCtrTagNivX.EdCodigo.ValueVariant := QrCtrTagNiv1Codigo.Value;
          FmCtrTagNivX.EdNome.ValueVariant   := QrCtrTagNiv1Nome.Value;
          FmCtrTagNivX.EdTag.ValueVariant    := QrCtrTagNiv1Tag.Value;
          FmCtrTagNivX.EdOrdem.ValueVariant  := QrCtrTagNiv1Ordem.Value;
          FmCtrTagNivX.MeConteudo.Text       := QrCtrTagNiv1Conteudo.Value;
        end;
        2:
        begin
          FmCtrTagNivX.EdCodigo.ValueVariant := QrCtrTagNiv2Codigo.Value;
          FmCtrTagNivX.EdNome.ValueVariant   := QrCtrTagNiv2Nome.Value;
          FmCtrTagNivX.EdTag.ValueVariant    := QrCtrTagNiv2Tag.Value;
          FmCtrTagNivX.EdOrdem.ValueVariant  := QrCtrTagNiv2Ordem.Value;
          FmCtrTagNivX.MeConteudo.Text       := QrCtrTagNiv2Conteudo.Value;
        end;
      end;
    end;
    FmCtrTagNivX.ShowModal;
    FmCtrTagNivX.Destroy;
  end;
end;

procedure TFmCtrTagNivCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrCtrTagNiv3);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrCtrTagNiv3, QrCtrTagNiv2);
end;

procedure TFmCtrTagNivCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrCtrTagNiv3);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrCtrTagNiv2);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrCtrTagNiv2);
end;

procedure TFmCtrTagNivCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCtrTagNiv3Codigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCtrTagNivCab.DefParams;
begin
  VAR_GOTOTABELA := 'ctrtagniv3';
  VAR_GOTOMYSQLTABLE := QrCtrTagNiv3;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM ctrtagniv3');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCtrTagNivCab.ItsAltera1Click(Sender: TObject);
begin
  MostraCtrTagNivX(stUpd, FNivelSel);
end;

procedure TFmCtrTagNivCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmCtrTagNivCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCtrTagNivCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCtrTagNivCab.ItsExclui1Click(Sender: TObject);
(*
var
  Controle: Integer;
*)
begin
{
  Exclui item de FNivelSel
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'ctrtagniv2', 'Controle', QrCtrTagNiv2Controle.Value) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrCtrTagNiv2,
      QrCtrTagNiv2Controle, QrCtrTagNiv2Controle.Value);
    ReopenCtrTagNiv2(Controle);
  end;
}
end;

procedure TFmCtrTagNivCab.ReopenCtrTagNiv2(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtrTagNiv2, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctrtagniv2 ',
  'WHERE NivSup=' + Geral.FF0(QrCtrTagNiv3Codigo.Value),
  'ORDER BY Ordem, Codigo ',
  '']);
  //
  QrCtrTagNiv2.Locate('Codigo', Codigo, []);
end;

procedure TFmCtrTagNivCab.ReopenCtrTagNiv1(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCtrTagNiv1, Dmod.MyDB, [
  'SELECT * ',
  'FROM ctrtagniv1 ',
  'WHERE NivSup=' + Geral.FF0(QrCtrTagNiv2Codigo.Value),
  'ORDER BY Ordem, Codigo ',
  '']);
  //
  QrCtrTagNiv1.Locate('Codigo', Codigo, []);
end;

procedure TFmCtrTagNivCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCtrTagNivCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCtrTagNivCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCtrTagNivCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCtrTagNivCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCtrTagNivCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCtrTagNivCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCtrTagNiv3Codigo.Value;
  Close;
end;

procedure TFmCtrTagNivCab.ItsInclui1Click(Sender: TObject);
begin
  MostraCtrTagNivX(stIns, FNivelSel);
end;

procedure TFmCtrTagNivCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCtrTagNiv3, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ctrtagniv3');
end;

procedure TFmCtrTagNivCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome, Tag: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Tag := Trim(EdTag.ValueVariant);
  if Tag <> '' then
  begin
    if MyObjects.FIC(pos(CO_IniTagNiv, Tag) <> 1, EdTag,
      'A tag quando informada deve come�ar com ' + CO_IniTagNiv + '!') then
      Exit;
    if MyObjects.FIC(Tag[Length(Tag)] <> ']', EdTag,
      'A tag quando informada deve terminar com "]"!') then
      Exit;
  end;
  //
  Codigo := QrCtrTagNiv3Codigo.Value;
  Codigo := UMyMod.BPGS1I32('ctrtagniv3', 'Codigo', '', '', tsPos,
    ImgTipo.SQLType, Codigo);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'ctrtagniv3',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmCtrTagNivCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ctrtagniv3', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ctrtagniv3', 'Codigo');
end;

procedure TFmCtrTagNivCab.BtNivel1Click(Sender: TObject);
begin
  FNivelSel := 1;
  MyObjects.MostraPopUpDeBotao(PMIts, BtNivel1);
end;

procedure TFmCtrTagNivCab.BtNivel2Click(Sender: TObject);
begin
  FNivelSel := 2;
  MyObjects.MostraPopUpDeBotao(PMIts, BtNivel2);
end;

procedure TFmCtrTagNivCab.BtNivel3Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtNivel3);
end;

procedure TFmCtrTagNivCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //GBEdita.Align := alClient;
  MeConteudo.Align := alClient;
  PnNivel1.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmCtrTagNivCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCtrTagNiv3Codigo.Value, LaRegistro.Caption);
end;

procedure TFmCtrTagNivCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCtrTagNivCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCtrTagNiv3Codigo.Value, LaRegistro.Caption);
end;

procedure TFmCtrTagNivCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCtrTagNivCab.QrCtrTagNiv2AfterScroll(DataSet: TDataSet);
begin
  ReopenCtrTagNiv1(0);
end;

procedure TFmCtrTagNivCab.QrCtrTagNiv2BeforeClose(DataSet: TDataSet);
begin
  QrCtrTagNiv1.Close;
end;

procedure TFmCtrTagNivCab.QrCtrTagNiv3AfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCtrTagNivCab.QrCtrTagNiv3AfterScroll(DataSet: TDataSet);
begin
  ReopenCtrTagNiv2(0);
end;

procedure TFmCtrTagNivCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrCtrTagNiv3Codigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmCtrTagNivCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCtrTagNiv3Codigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ctrtagniv3', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCtrTagNivCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCtrTagNivCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrCtrTagNiv3, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ctrtagniv3');
end;

procedure TFmCtrTagNivCab.QrCtrTagNiv3BeforeClose(DataSet: TDataSet);
begin
  QrCtrTagNiv2.Close;
end;

procedure TFmCtrTagNivCab.QrCtrTagNiv3BeforeOpen(DataSet: TDataSet);
begin
  QrCtrTagNiv3Codigo.DisplayFormat := FFormatFloat;
end;

end.

