unit ContratPsq1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, UnDmkProcFunc, dmkRadioGroup, UnDmkEnums;

type
  TFmContratPsq1 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    QrContratada: TmySQLQuery;
    QrContratadaCodigo: TIntegerField;
    QrContratadaCONTRATADA: TWideStringField;
    DsContratada: TDataSource;
    QrContratante: TmySQLQuery;
    IntegerField2: TIntegerField;
    QrContratanteCONTRATANTE: TWideStringField;
    DsContratante: TDataSource;
    Label3: TLabel;
    EdContratada: TdmkEditCB;
    CBContratada: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdContratante: TdmkEditCB;
    CBContratante: TdmkDBLookupComboBox;
    DBGrid1: TDBGrid;
    QrContratos: TmySQLQuery;
    QrContratosCodigo: TIntegerField;
    QrContratosNome: TWideStringField;
    QrContratosDInstal: TDateField;
    QrContratosDVencimento: TDateField;
    DsContratos: TDataSource;
    CkInativos: TCheckBox;
    QrContratosDtaCntrFim: TDateField;
    QrContratosVersao: TFloatField;
    CGStatus: TdmkCheckGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdContratadaChange(Sender: TObject);
    procedure EdContratadaExit(Sender: TObject);
    procedure EdContratanteChange(Sender: TObject);
    procedure EdContratanteExit(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure QrContratosBeforeClose(DataSet: TDataSet);
    procedure QrContratosAfterOpen(DataSet: TDataSet);
    procedure CkInativosClick(Sender: TObject);
    procedure CGStatusClick(Sender: TObject);
    procedure QrContratosDtaCntrFimGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure QrContratosDVencimentoGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure QrContratosDInstalGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    { Private declarations }
    FCriando: Boolean;
    //
    procedure ReopenContratos();
    procedure SelecionaItem();
  public
    { Public declarations }
    FContrato: Integer;
  end;

  var
  FmContratPsq1: TFmContratPsq1;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmContratPsq1.BtOKClick(Sender: TObject);
begin
  SelecionaItem();
end;

procedure TFmContratPsq1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContratPsq1.CGStatusClick(Sender: TObject);
begin
  ReopenContratos();
end;

procedure TFmContratPsq1.CkInativosClick(Sender: TObject);
begin
  ReopenContratos();
end;

procedure TFmContratPsq1.DBGrid1DblClick(Sender: TObject);
begin
  SelecionaItem();
end;

procedure TFmContratPsq1.EdContratadaChange(Sender: TObject);
begin
  if not EdContratada.Focused then
    ReopenContratos();
end;

procedure TFmContratPsq1.EdContratadaExit(Sender: TObject);
begin
  ReopenContratos();
end;

procedure TFmContratPsq1.EdContratanteChange(Sender: TObject);
begin
  if not EdContratante.Focused then
    ReopenContratos();
end;

procedure TFmContratPsq1.EdContratanteExit(Sender: TObject);
begin
  ReopenContratos();
end;

procedure TFmContratPsq1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if FCriando then
  begin
    FCriando := False;
    ReopenContratos();
  end;
end;

procedure TFmContratPsq1.FormCreate(Sender: TObject);
begin
  FCriando := True;
  ImgTipo.SQLType := stLok;
  FContrato := 0;
  UMyMod.AbreQuery(QrContratada, Dmod.MyDB);
  UMyMod.AbreQuery(QrContratante, Dmod.MyDB);
  CGStatus.SetMaxValue();
end;

procedure TFmContratPsq1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContratPsq1.QrContratosAfterOpen(DataSet: TDataSet);
begin
  BtOK.Enabled := QrContratos.RecordCount > 0;
end;

procedure TFmContratPsq1.QrContratosBeforeClose(DataSet: TDataSet);
begin
  BtOK.Enabled := False;
end;

procedure TFmContratPsq1.QrContratosDInstalGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := Geral.FDT(QrContratosDInstal.Value, 2);
end;

procedure TFmContratPsq1.QrContratosDtaCntrFimGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := Geral.FDT(QrContratosDtaCntrFim.Value, 2);
end;

procedure TFmContratPsq1.QrContratosDVencimentoGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := Geral.FDT(QrContratosDVencimento.Value, 2);
end;

procedure TFmContratPsq1.ReopenContratos();
var
  Inativos, Status: String;
begin
  if FCriando then
    Exit;
  //
  if CkInativos.Checked then
    Inativos := Geral.ATS([
    'AND DInstal <= SYSDATE() ',
    'AND DInstal > 1 ',
    'AND (DtaCntrFim >= SYSDATE() ',
    '     OR DtaCntrFim < "1900-01-01") '
    ])
  else
    Inativos := '';
  //
  case CGStatus.Value of
    1: Status := 'AND Status=0';
    2: Status := 'AND Status=1';
    3: Status := 'AND Status IN (1,0)';
    4: Status := 'AND Status=2';
    5: Status := 'AND Status IN (2,0)';
    6: Status := 'AND Status IN (2,1)';
    //0,7:
    else Status := '';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrContratos, Dmod.MyDB, [
  'SELECT Codigo, Nome, DInstal, DtaCntrFim,  ',
  'DVencimento, Versao ',
  'FROM contratos ',
  'WHERE Ativo=1 ',
  Inativos,
  Status,
  'AND Contratada=' + Geral.FF0(EdContratada.ValueVariant),
  'AND Contratante=' + Geral.FF0(EdContratante.ValueVariant),
  'ORDER BY Nome ',
  '']);
end;

procedure TFmContratPsq1.SelecionaItem();
begin
  FContrato := QrContratosCodigo.Value;
  Close;
end;

end.
