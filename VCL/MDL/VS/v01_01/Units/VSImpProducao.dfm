object FmVSImpProducao: TFmVSImpProducao
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-232 :: Impress'#245'es de Produ'#231#227'o'
  ClientHeight = 639
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 302
        Height = 32
        Caption = 'Impress'#245'es de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 302
        Height = 32
        Caption = 'Impress'#245'es de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 302
        Height = 32
        Caption = 'Impress'#245'es de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 477
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 477
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 101
        Width = 812
        Height = 192
        Align = alTop
        TabOrder = 1
        object CGEstagios: TdmkCheckGroup
          Left = 12
          Top = 4
          Width = 633
          Height = 93
          Caption = ' Est'#225'gios:  (deixe vazio para TODOS)'
          Columns = 3
          Items.Strings = (
            '1024 - Entrada In Natura'
            '1072 - Mat'#233'ria-prima Em Conserva'#231#227'o'
            '1088 - Mat'#233'ria-prima Conservada'
            '2048 - Em Caleiro'
            '3072 - Artigo Caleado'
            '4096 - Em Curtimento'
            '5120 - Couro Curtido'
            '6144 - Couro Classificado'
            '7168 - Couro Reclassificado'
            '8192 - Couro Curtido em Opera'#231#227'o'
            '9216 - Couro em Recurtimento')
          TabOrder = 0
          UpdType = utYes
          Value = 0
          OldValor = 0
        end
        object GroupBox2: TGroupBox
          Left = 12
          Top = 100
          Width = 241
          Height = 89
          Caption = ' Per'#237'odo: '
          TabOrder = 1
          object Panel5: TPanel
            Left = 2
            Top = 15
            Width = 237
            Height = 72
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitWidth = 249
            ExplicitHeight = 89
            object CkDataIni: TCheckBox
              Left = 4
              Top = 8
              Width = 97
              Height = 17
              Caption = 'Data inicial:'
              Checked = True
              State = cbChecked
              TabOrder = 0
            end
            object TPDataIni: TdmkEditDateTimePicker
              Left = 4
              Top = 28
              Width = 112
              Height = 21
              Date = 44086.000000000000000000
              Time = 0.776208518516796200
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object CkDataFim: TCheckBox
              Left = 120
              Top = 8
              Width = 97
              Height = 17
              Caption = 'Data final:'
              Checked = True
              State = cbChecked
              TabOrder = 2
            end
            object TPDataFim: TdmkEditDateTimePicker
              Left = 120
              Top = 28
              Width = 112
              Height = 21
              Date = 44086.000000000000000000
              Time = 0.776208518516796200
              TabOrder = 3
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
          end
        end
        object RGFormaQtdes: TRadioGroup
          Left = 648
          Top = 4
          Width = 157
          Height = 93
          Caption = ' Forma das quantidades: '
          ItemIndex = 0
          Items.Strings = (
            'Somas de quantidades'
            'Quantidades individuais')
          TabOrder = 2
        end
        object RGOrdem1: TRadioGroup
          Left = 256
          Top = 100
          Width = 97
          Height = 89
          Caption = ' Ordem 1: '
          ItemIndex = 0
          Items.Strings = (
            'Data'
            'Est'#225'gio'
            'Artigo'
            'Fornecedor')
          TabOrder = 3
        end
        object RGOrdem2: TRadioGroup
          Left = 356
          Top = 100
          Width = 97
          Height = 89
          Caption = ' Ordem 2: '
          ItemIndex = 1
          Items.Strings = (
            'Data'
            'Est'#225'gio'
            'Artigo'
            'Fornecedor')
          TabOrder = 4
        end
        object RGOrdem3: TRadioGroup
          Left = 456
          Top = 100
          Width = 97
          Height = 89
          Caption = ' Ordem 3: '
          ItemIndex = 2
          Items.Strings = (
            'Data'
            'Est'#225'gio'
            'Artigo'
            'Fornecedor')
          TabOrder = 5
        end
        object RGAgrupa: TRadioGroup
          Left = 556
          Top = 100
          Width = 89
          Height = 89
          Caption = 'Agrupamentos: '
          Columns = 2
          ItemIndex = 1
          Items.Strings = (
            '0'
            '1'
            '2'
            '3')
          TabOrder = 6
        end
        object Ck1Cor: TCheckBox
          Left = 684
          Top = 132
          Width = 97
          Height = 17
          Caption = 'Colore.'
          Checked = True
          State = cbChecked
          TabOrder = 7
        end
      end
      object DBGSum: TdmkDBGridZTO
        Left = 0
        Top = 293
        Width = 812
        Height = 184
        Align = alClient
        DataSource = DsSum
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 2
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        OnDblClick = DBGSumDblClick
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 812
        Height = 101
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label7: TLabel
          Left = 8
          Top = 8
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label20: TLabel
          Left = 8
          Top = 32
          Width = 74
          Height = 13
          Caption = 'Dono do couro:'
        end
        object Label2: TLabel
          Left = 8
          Top = 56
          Width = 57
          Height = 13
          Caption = 'Fornecedor:'
        end
        object Label1: TLabel
          Left = 8
          Top = 80
          Width = 27
          Height = 13
          Caption = 'Arigo:'
        end
        object EdEmpresa: TdmkEditCB
          Left = 104
          Top = 4
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 160
          Top = 4
          Width = 533
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdClientMO: TdmkEditCB
          Left = 104
          Top = 28
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ClienteMO'
          UpdCampo = 'ClienteMO'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBClientMO
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBClientMO: TdmkDBLookupComboBox
          Left = 160
          Top = 28
          Width = 533
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsClienteMO
          TabOrder = 3
          dmkEditCB = EdClientMO
          QryCampo = 'ClienteMO'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdFornece: TdmkEditCB
          Left = 104
          Top = 52
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ClienteMO'
          UpdCampo = 'ClienteMO'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBFornece
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBFornece: TdmkDBLookupComboBox
          Left = 160
          Top = 52
          Width = 533
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsFornecedor
          TabOrder = 5
          dmkEditCB = EdFornece
          QryCampo = 'ClienteMO'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdGraGruX: TdmkEditCB
          Left = 104
          Top = 76
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBGraGruX
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBGraGruX: TdmkDBLookupComboBox
          Left = 160
          Top = 76
          Width = 533
          Height = 21
          KeyField = 'GraGruX'
          ListField = 'NO_PRD_TAM_COR'
          ListSource = DsGraGruX
          TabOrder = 7
          dmkEditCB = EdGraGruX
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 525
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 569
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 28
    Top = 7
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cou.GraGruX, CONCAT(gg1.Nome,  '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),  '
      
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nom' +
        'e)))  '
      'NO_PRD_TAM_COR '
      'FROM gragruxcou cou  '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=cou.GraGruX  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'WHERE ggx.Controle > 0 '
      'ORDER BY NO_PRD_TAM_COR ')
    Left = 368
    Top = 68
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 368
    Top = 116
  end
  object QrSum: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  '
      'CAST(DataHora AS Date) Data,  '
      '  /*CONCAT( '
      '  LPAD(YEAR(vmi.DataHora), 4, "0"),"-", '
      '  LPAD(MONTH(vmi.DataHora), 2, "0"),"-", '
      '  LPAD(DAY(vmi.DataHora), 2, "0")) Data,  */'
      'DataHora,'
      'CASE  '
      '  WHEN (MovimID=1 AND MovimNiv=0) '
      '  THEN 1024  '
      '  WHEN (MovimID=26 AND MovimNiv=30) '
      '  THEN 2048 '
      '  WHEN (MovimID=29 AND MovimNiv=31 AND Iuvpei=6) '
      '  THEN 3072 '
      '  WHEN (MovimID=27 AND MovimNiv=35) '
      '  THEN 4096  '
      '  WHEN (MovimID=6 AND MovimNiv=13) '
      '  THEN 5120 '
      '  WHEN (MovimID=7 AND MovimNiv=2) '
      '  THEN 6144  '
      '  WHEN (MovimID=8 AND MovimNiv=2) '
      '  THEN 7168 '
      '  WHEN (MovimID=11 AND MovimNiv=8) '
      '  THEN 8192 '
      '  WHEN (MovimID=19 AND MovimNiv=21) '
      '  THEN 9216 '
      '  ELSE 9999 '
      '  END GRUPO_COD, '
      'CASE  '
      '  WHEN (MovimID=1 AND MovimNiv=0) '
      '  THEN "Entrada In Natura"  '
      '  WHEN (MovimID=26 AND MovimNiv=30) '
      '  THEN "Em Caleiro" '
      '  WHEN (MovimID=29 AND MovimNiv=31 AND Iuvpei=6) '
      '  THEN "Artigo Caleado" '
      '  WHEN (MovimID=27 AND MovimNiv=35) '
      '  THEN "Em Curtimento" '
      '  WHEN (MovimID=6 AND MovimNiv=13) '
      '  THEN "Couro Curtido" '
      '  WHEN (MovimID=7 AND MovimNiv=2) '
      '  THEN "Couro Classificado"  '
      '  WHEN (MovimID=8 AND MovimNiv=2) '
      '  THEN "Couro Reclassificado" '
      '  WHEN (MovimID=11 AND MovimNiv=8) '
      '  THEN "Couro Curtido em Opera'#231#227'o" '
      '  WHEN (MovimID=19 AND MovimNiv=21) '
      '  THEN "Couro em Recurtimento" '
      '  ELSE "???????" '
      '  END GRUPO_TXT, '
      'vmi.GraGruX, '
      ''
      'CONCAT(gg1.Nome,  '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),  '
      
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nom' +
        'e)))  '
      'NO_PRD_TAM_COR, '
      ''
      'vmi.MovimID,  '
      'vmi.MovimNiv, vmi.Iuvpei, '
      'SUM(vmi.Pecas) Pecas,  '
      'SUM(vmi.PesoKg) PesoKg,  '
      'SUM(vmi.AreaM2) AreaM2 '
      'FROM vsmovits vmi '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'WHERE vmi.Pecas > 0  '
      'AND vmi.DataHora BETWEEN "2019-01-01" AND "2019-09-11 23:59:59" '
      'AND ( '
      '  (MovimID=1 AND MovimNiv=0) '
      '  OR  '
      '  (MovimID=26 AND MovimNiv=30) '
      '  OR  '
      '  (MovimID=29 AND MovimNiv=31 AND Iuvpei=6) '
      '  OR  '
      '  (MovimID=27 AND MovimNiv=35) '
      '  OR  '
      '  (MovimID=6 AND MovimNiv=13) '
      '  OR  '
      '  (MovimID=7 AND MovimNiv=2) '
      '  OR  '
      '  (MovimID=8 AND MovimNiv=2) '
      '  OR  '
      '  (MovimID=19 AND MovimNiv=21) '
      '  OR  '
      '  (MovimID=11 AND MovimNiv=8) '
      '  )  '
      'GROUP BY Data, GRUPO_COD, GraGruX '
      'ORDER BY Data, GRUPO_COD, GraGruX ')
    Left = 480
    Top = 140
    object QrSumIMEI: TIntegerField
      FieldName = 'IMEI'
    end
    object QrSumDATA: TDateField
      FieldName = 'DATA'
    end
    object QrSumDataHora: TDateTimeField
      DisplayLabel = 'Data / Hora'
      FieldName = 'DataHora'
      Required = True
    end
    object QrSumGRUPO_COD: TFloatField
      DisplayLabel = 'Est'#225'gio'
      FieldName = 'GRUPO_COD'
    end
    object QrSumGRUPO_TXT: TWideStringField
      DisplayLabel = 'Descri'#231#227'o do est'#225'gio'
      FieldName = 'GRUPO_TXT'
      Required = True
      Size = 25
    end
    object QrSumGraGruX: TIntegerField
      DisplayLabel = 'Reduzido'
      FieldName = 'GraGruX'
      Required = True
    end
    object QrSumNO_PRD_TAM_COR: TWideStringField
      DisplayLabel = 'Artigo'
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrSumMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrSumMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrSumIuvpei: TIntegerField
      FieldName = 'Iuvpei'
      Required = True
    end
    object QrSumPecas: TFloatField
      DisplayLabel = 'Pe'#231'as'
      FieldName = 'Pecas'
    end
    object QrSumPesoKg: TFloatField
      DisplayLabel = 'Peso kg'
      FieldName = 'PesoKg'
    end
    object QrSumAreaM2: TFloatField
      DisplayLabel = #193'rea m'#178
      FieldName = 'AreaM2'
    end
    object QrSumNO_Fornece: TWideStringField
      FieldName = 'NO_Fornece'
      Size = 255
    end
    object QrSumTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
  end
  object DsSum: TDataSource
    DataSet = QrSum
    Left = 480
    Top = 192
  end
  object frxDsSum: TfrxDBDataset
    UserName = 'frxDsSum'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IMEI=IMEI'
      'DATA=DATA'
      'DataHora=DataHora'
      'GRUPO_COD=GRUPO_COD'
      'GRUPO_TXT=GRUPO_TXT'
      'GraGruX=GraGruX'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'MovimID=MovimID'
      'MovimNiv=MovimNiv'
      'Iuvpei=Iuvpei'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'NO_Fornece=NO_Fornece'
      'Terceiro=Terceiro')
    DataSet = QrSum
    BCDToCurrency = False
    DataSetOptions = []
    Left = 480
    Top = 244
  end
  object QrClienteMO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 436
    Top = 68
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClienteMO: TDataSource
    DataSet = QrClienteMO
    Left = 436
    Top = 116
  end
  object frxWET_CURTI_232_00_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 42013.497466747700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  GH001.Visible := <VARF_AGRUP1>;'
      '  GF001.Visible := <VARF_AGRUP1>;'
      '  GH001.Condition := <VARF_GRUPO1>;'
      '  Me_GH1.Memo.Text := <VARF_HEADR1>;            '
      '  MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;'
      '    '
      '  GH002.Visible := <VARF_AGRUP2>;'
      '  GF002.Visible := <VARF_AGRUP2>;'
      '  GH002.Condition := <VARF_GRUPO2>;'
      '  Me_GH2.Memo.Text := <VARF_HEADR2>;            '
      '  MeGrupo2Foot.Memo.Text := <VARF_FOOTR2>;'
      '    '
      '  GH003.Visible := <VARF_AGRUP3>;'
      '  GF003.Visible := <VARF_AGRUP3>;'
      '  GH003.Condition := <VARF_GRUPO3>;'
      '  Me_GH3.Memo.Text := <VARF_HEADR3>;            '
      '  MeGrupo3Foot.Memo.Text := <VARF_FOOTR3>;'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_232_00_AGetValue
    Left = 136
    Top = 388
    Datasets = <
      item
        DataSet = frxDsSum
        DataSetName = 'frxDsSum'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 132.283540240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 668.976810000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NOMEDONO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.314960630000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 472.441250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Relat'#243'rio de Produ'#231#227'o :: [VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 578.268090000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 117.165430000000000000
          Width = 188.976377950000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 41.574830000000000000
          Width = 672.756340000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 608.503963860000000000
          Top = 117.165430000000000000
          Width = 71.811045590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472399450000000000
          Top = 117.165430000000000000
          Width = 68.031510710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543265590000000000
          Top = 117.165430000000000000
          Width = 86.929160710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Top = 117.165430000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / Hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 117.165430000000000000
          Width = 41.574790940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Top = 117.165430000000000000
          Width = 162.519716770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Est'#225'gio')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 385.512060000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Cliente interno: [VARF_CLI_INT]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 64.252010000000000000
          Width = 336.378170000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 83.149660000000000000
          Width = 676.535870000000000000
          Height = 34.015770000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Est'#225'gios: [VARF_ESTAGIOS]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 585.827150000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 540.472790000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 453.543404720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE ITENS: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504298270000000000
          Top = 3.779530000000000000
          Width = 68.031496062992100000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472733860000000000
          Top = 3.779530000000000000
          Width = 68.031496062992100000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 3.779530000000000000
          Width = 86.929133858267700000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 676.535870000000000000
          Top = 3.779530000000000000
          Width = 3.779334720000000000
          Height = 15.118110236220500000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH002: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 253.228510000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSum."GRUPO_COD"'
        object Me_GH2: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          Memo.UTF8W = (
            '[<frxDsSum."GRUPO_COD"><frxDsSum."GRUPO_TXT">]')
          ParentFont = False
        end
      end
      object GH003: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSum."GraGruX"'
        object MeGH_02_F01: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object Me_GH3: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.755851810000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11854022
          Memo.UTF8W = (
            '[frxDsSum."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 12.472440940000000000
        Top = 328.819110000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSum
        DataSetName = 'frxDsSum'
        RowCount = 0
        object MeMD_F01: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 12.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object MeMD_F02: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 12.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11854022
          ParentFont = False
        end
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Left = 287.244280000000000000
          Width = 166.299212600000000000
          Height = 12.472440940000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsSum."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Width = 22.677165350000000000
          Height = 12.472440940000000000
          DataField = 'GraGruX'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSum."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Width = 68.031496060000000000
          Height = 12.472440940000000000
          DataField = 'Pecas'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSum."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Width = 68.031496060000000000
          Height = 12.472440940000000000
          DataField = 'AreaM2'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSum."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543265590000000000
          Width = 86.929160710000000000
          Height = 12.472440940000000000
          DataField = 'PesoKg'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSum."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Width = 60.472440940000000000
          Height = 12.472440940000000000
          DataField = 'DataHora'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsSum."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 136.063031180000000000
          Height = 12.472440940000000000
          DataField = 'GRUPO_TXT'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsSum."GRUPO_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Width = 26.456685590000000000
          Height = 12.472440940000000000
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSum."GRUPO_COD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 37.795260940000000000
          Height = 12.472440940000000000
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'N/D')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeVMI: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 37.795260940000000000
          Height = 12.472440940000000000
          DataField = 'IMEI'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSum."IMEI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF003: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.566929130000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
        object MeFT_03_F01: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 24.566929130000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object MeFT_03_F02: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11854022
          ParentFont = False
        end
        object MeGrupo3Foot: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000000000
          Width = 381.732334720000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 608.503963860000000000
          Top = 3.779530000000000000
          Width = 68.031496062992100000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472399450000000000
          Top = 3.779530000000000000
          Width = 68.031496062992100000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543265590000000000
          Top = 3.779530000000000000
          Width = 86.929133858267700000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Top = 3.779530000000000000
          Width = 64.251968500000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Itens: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
      object GF002: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787401570000000000
        Top = 411.968770000000000000
        Width = 680.315400000000000000
        object MeFT_02: TfrxMemoView
          AllowVectorExport = True
          Width = 680.314911810000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object MeGrupo2Foot: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 385.511864720000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Top = 3.779530000000000000
          Width = 64.251968500000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Itens: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504298270000000000
          Top = 3.779530000000000000
          Width = 68.031496062992100000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472733860000000000
          Top = 3.779530000000000000
          Width = 68.031496062992100000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 3.779530000000000000
          Width = 86.929133858267700000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH001: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 211.653680000000000000
        Width = 680.315400000000000000
        Condition = '<frxDsSum."GRUPO_COD">'
        object Me_GH1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSum."Data"]')
          ParentFont = False
        end
      end
      object GF001: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.165359210000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        object MeGrupo1Foot: TfrxMemoView
          AllowVectorExport = True
          Width = 453.543404720000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504298270000000000
          Width = 68.031496060000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472733860000000000
          Width = 68.031496060000000000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 86.929133860000000000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 676.535870000000000000
          Width = 3.779334720000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE,'
      'CONCAT(IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome),'
      '  " - ", IF(ent.Tipo=0, ent.CNPJ, ent.CPF)) NOME_E_DOC_ENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 536
    Top = 80
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrFornecedorNOME_E_DOC_ENTIDADE: TWideStringField
      FieldName = 'NOME_E_DOC_ENTIDADE'
      Size = 121
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 536
    Top = 124
  end
  object frxWET_CURTI_232_00_B: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 42013.497466747700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  GH001.Visible := <VARF_AGRUP1>;'
      '  GF001.Visible := <VARF_AGRUP1>;'
      '  GH001.Condition := <VARF_GRUPO1>;'
      '  Me_GH1.Memo.Text := <VARF_HEADR1>;            '
      '  MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;'
      '    '
      '  GH002.Visible := <VARF_AGRUP2>;'
      '  GF002.Visible := <VARF_AGRUP2>;'
      '  GH002.Condition := <VARF_GRUPO2>;'
      '  Me_GH2.Memo.Text := <VARF_HEADR2>;            '
      '  MeGrupo2Foot.Memo.Text := <VARF_FOOTR2>;'
      '    '
      '  GH003.Visible := <VARF_AGRUP3>;'
      '  GF003.Visible := <VARF_AGRUP3>;'
      '  GH003.Condition := <VARF_GRUPO3>;'
      '  Me_GH3.Memo.Text := <VARF_HEADR3>;            '
      '  MeGrupo3Foot.Memo.Text := <VARF_FOOTR3>;'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_232_00_AGetValue
    Left = 136
    Top = 440
    Datasets = <
      item
        DataSet = frxDsSum
        DataSetName = 'frxDsSum'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 132.283540240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 668.976810000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NOMEDONO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.314960630000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 472.441250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Relat'#243'rio de Produ'#231#227'o :: [VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 578.268090000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Top = 117.165430000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 41.574830000000000000
          Width = 672.756340000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 608.503963860000000000
          Top = 117.165430000000000000
          Width = 71.811045590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472399450000000000
          Top = 117.165430000000000000
          Width = 68.031510710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543265590000000000
          Top = 117.165430000000000000
          Width = 86.929160710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Top = 117.165430000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / Hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 117.165430000000000000
          Width = 41.574790940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Top = 117.165430000000000000
          Width = 102.047236770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Est'#225'gio')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 385.512060000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Cliente interno: [VARF_CLI_INT]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 64.252010000000000000
          Width = 336.378170000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 83.149660000000000000
          Width = 676.535870000000000000
          Height = 34.015770000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Est'#225'gios: [VARF_ESTAGIOS]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 117.165430000000000000
          Width = 113.385812130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fornecedor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 585.827150000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 540.472790000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 453.543404720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE ITENS: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504298270000000000
          Top = 3.779530000000000000
          Width = 68.031496062992100000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472733860000000000
          Top = 3.779530000000000000
          Width = 68.031496062992100000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 3.779530000000000000
          Width = 86.929133858267700000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 676.535870000000000000
          Top = 3.779530000000000000
          Width = 3.779334720000000000
          Height = 15.118110236220500000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH002: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 253.228510000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSum."GRUPO_COD"'
        object Me_GH2: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          Memo.UTF8W = (
            '[<frxDsSum."GRUPO_COD"><frxDsSum."GRUPO_TXT">]')
          ParentFont = False
        end
      end
      object GH003: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsSum."GraGruX"'
        object MeGH_02_F01: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object Me_GH3: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.755851810000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11854022
          Memo.UTF8W = (
            '[frxDsSum."NO_PRD_TAM_COR"]')
          ParentFont = False
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 12.472440940000000000
        Top = 328.819110000000000000
        Width = 680.315400000000000000
        DataSet = frxDsSum
        DataSetName = 'frxDsSum'
        RowCount = 0
        object MeMD_F01: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 12.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object MeMD_F02: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 12.472440940000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11854022
          ParentFont = False
        end
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 113.385826770000000000
          Height = 12.472440940000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsSum."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Width = 22.677165350000000000
          Height = 12.472440940000000000
          DataField = 'GraGruX'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSum."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504330000000000000
          Width = 68.031496060000000000
          Height = 12.472440940000000000
          DataField = 'Pecas'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSum."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Width = 68.031496060000000000
          Height = 12.472440940000000000
          DataField = 'AreaM2'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSum."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543265590000000000
          Width = 86.929160710000000000
          Height = 12.472440940000000000
          DataField = 'PesoKg'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSum."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Width = 60.472440940000000000
          Height = 12.472440940000000000
          DataField = 'DataHora'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsSum."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 75.590551181102360000
          Height = 12.472440940000000000
          DataField = 'GRUPO_TXT'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsSum."GRUPO_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Width = 26.456685590000000000
          Height = 12.472440940000000000
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsSum."GRUPO_COD"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 37.795260940000000000
          Height = 12.472440940000000000
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'N/D')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeVMI: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 37.795260940000000000
          Height = 12.472440940000000000
          DataField = 'IMEI'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSum."IMEI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Width = 113.385826770000000000
          Height = 12.472440940000000000
          DataField = 'NO_Fornece'
          DataSet = frxDsSum
          DataSetName = 'frxDsSum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          Memo.UTF8W = (
            '[frxDsSum."NO_Fornece"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF003: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.566929130000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
        object MeFT_03_F01: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 24.566929130000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object MeFT_03_F02: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Width = 672.756340000000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 11854022
          ParentFont = False
        end
        object MeGrupo3Foot: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000000000
          Width = 381.732334720000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 608.503963860000000000
          Top = 3.779530000000000000
          Width = 68.031496062992100000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472399450000000000
          Top = 3.779530000000000000
          Width = 68.031496062992100000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543265590000000000
          Top = 3.779530000000000000
          Width = 86.929133858267700000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Top = 3.779530000000000000
          Width = 64.251968500000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Itens: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
      object GF002: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787401570000000000
        Top = 411.968770000000000000
        Width = 680.315400000000000000
        object MeFT_02: TfrxMemoView
          AllowVectorExport = True
          Width = 680.314911810000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = 15123099
          ParentFont = False
        end
        object MeGrupo2Foot: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 385.511864720000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Total de ???')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 389.291590000000000000
          Top = 3.779530000000000000
          Width = 64.251968500000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            'Itens: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504298270000000000
          Top = 3.779530000000000000
          Width = 68.031496062992100000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472733860000000000
          Top = 3.779530000000000000
          Width = 68.031496062992100000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 3.779530000000000000
          Width = 86.929133858267700000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH001: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 211.653680000000000000
        Width = 680.315400000000000000
        Condition = '<frxDsSum."GRUPO_COD">'
        object Me_GH1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsSum."Data"]')
          ParentFont = False
        end
      end
      object GF001: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 21.165359210000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        object MeGrupo1Foot: TfrxMemoView
          AllowVectorExport = True
          Width = 453.543404720000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: [COUNT(MD002)]  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 608.504298270000000000
          Width = 68.031496060000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472733860000000000
          Width = 68.031496060000000000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 86.929133860000000000
          Height = 13.606299210000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSum."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 676.535870000000000000
          Width = 3.779334720000000000
          Height = 13.606299210000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
end
