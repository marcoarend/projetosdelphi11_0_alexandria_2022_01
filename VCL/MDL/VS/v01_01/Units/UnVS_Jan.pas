unit UnVS_Jan;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Buttons, ComCtrls, CommCtrl, Consts,
  Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums, dmkEditCB,
  dmkEdit, dmkDBLookupComboBox, mySQLDbTables, Data.Db, DBGrids, AppListas,
  dmkDBGridZTO, UnDmkProcFunc, UnProjGroup_Vars, BlueDermConsts, TypInfo,
  System.Math, UnProjGroup_Consts, UnEntities, DBCtrls, Grids, Mask, UnVS_Tabs,
  dmkEditDateTimePicker, UnGrl_Consts, UnGrl_Geral, UnVS_CRC_PF,
  UnAppEnums;

type
  TUnVS_Jan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ImprimeVSImpMapaDefei(SerieFch, Ficha: Integer);
    //
    procedure MostraFormVSGerArtDatas(Codigo, MovimCod: Integer; DtHrAberto,
              DtHrLibCla, DtHrCfgCla, DtHrFimCla: TDateTime; MovimID:
              TEstqMovimID);
    procedure MostraFormVSGerRclDatas(Codigo, MovimCod: Integer; DtHrAberto,
              DtHrLibCla, DtHrCfgCla, DtHrFimCla: TDateTime; MovimID:
              TEstqMovimID);
    procedure MostraFormVSLoadCRCCab1(Codigo, Controle: Integer);
    procedure MostraFormVSLoadCRCCab2(Codigo, Controle: Integer);
    procedure MostraFormVSLoadCRCPalletA(Pallet: Integer);
    procedure MostraFormGraGruXCou(var GraGruX: Integer);
    procedure MostraFormGraGruYPrecos(GraGruY: Integer);
    procedure MostraFormVSInnNFsCab();
    procedure MostraFormArtGeComodty(Codigo: Integer);
    procedure MostraFormVSCorrigeCusReclas();
    procedure MostraFormVSTabMPAG();
    procedure MostraFormVSImpEstqReduz();
    procedure MostraFormVSMarcaAjusta();
    procedure MostraFormVSLocalizaMarcas();
    procedure MostraFormVSSifDipoaImp(Pallets: String);
    procedure MostraFormVSImpEmProcBH();

  end;

var
  VS_Jan: TUnVS_Jan;

implementation

uses DmkDAC_PF, Module, MyDBCheck, ModuleGeral, CfgCadLista, UMySQLModule,
  UnMyObjects, GetValor, GraGruXCou,
  VSLoadCRCCab1, VSLoadCRCCab2, VSLoadCRCPalletA, VSImpMapaDefei,
  VSGerArtDatas, VSGerRclDatas, GraGruYPrecos, ArtGeComodty,
  VSCorrigeCusReclas, VSTabMPAG, VSImpEstqReduz, VSMarcaAjusta,
  VSLocalizaMarcas, VSSifDipoaImp, VSImpEmProcBH;

{ TUnVS_Jan }

procedure TUnVS_Jan.ImprimeVSImpMapaDefei(SerieFch, Ficha: Integer);
begin
  if DBCheck.CriaFm(TFmVSImpMapaDefei, FmVSImpMapaDefei, afmoNegarComAviso) then
  begin
    //FmVSImpMapaDefei.ShowModal;
    FmVSImpMapaDefei.ImprimeMapaDeDefeitos(SerieFch, Ficha);
    FmVSImpMapaDefei.Destroy;
  end;
end;

procedure TUnVS_Jan.MostraFormArtGeComodty(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmArtGeComodty, FmArtGeComodty, afmoNegarComAviso) then
  begin
    FmArtGeComodty.ShowModal;
    if Codigo > 0 then
      FmArtGeComodty.LocCod(Codigo, Codigo);
    FmArtGeComodty.Destroy;
  end;
end;

procedure TUnVS_Jan.MostraFormGraGruXCou(var GraGruX: Integer);
var
  Val: Variant;
begin
  if GraGruX = 0 then
  begin
    if not MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, GraGruX,
    0, 0, '', '', True, 'Reduzido', 'Informe o Reduzido: ', 0, Val) then
      Exit;
    GraGruX := Val;
  end;
  //
  if GraGruX <> 0 then
  begin
    if DBCheck.CriaFm(TFmGraGruXCou, FmGraGruXCou, afmoSoAdmin) then
    begin
      FmGraGruXCou.ImgTipo.SQLType := stUpd;
      FmGraGruXCou.ReopengraGruX(GraGruX);
      FmGraGruXCou.ShowModal;
      FmGraGruXCou.Destroy;
    end;
  end;
end;

procedure TUnVS_Jan.MostraFormGraGruYPrecos(GraGruY: Integer);
begin
  if DBCheck.CriaFm(TFmGraGruYPrecos, FmGraGruYPrecos, afmoNegarComAviso) then
  begin
    if GraGruY <> 0 then
    begin
      FmGraGruYPrecos.EdGraGruY.ValueVariant := GraGruY;
      FmGraGruYPrecos.CBGraGruY.KeyValue     := GraGruY;
    end;
    FmGraGruYPrecos.ShowModal;
    FmGraGruYPrecos.Destroy;
  end;
end;

procedure TUnVS_Jan.MostraFormVSCorrigeCusReclas();
begin
  if DBCheck.CriaFm(TFmVSCorrigeCusReclas, FmVSCorrigeCusReclas, afmoNegarComAviso) then
  begin
    FmVSCorrigeCusReclas.ShowModal;
    FmVSCorrigeCusReclas.Destroy;
  end;
end;

procedure TUnVS_Jan.MostraFormVSGerArtDatas(Codigo, MovimCod: Integer;
  DtHrAberto, DtHrLibCla, DtHrCfgCla, DtHrFimCla: TDateTime; MovimID:
  TEstqMovimID);
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  //Codigo := QrVSGerArtCodigo.Value;
  if DBCheck.CriaFm(TFmVSGerArtDatas, FmVSGerArtDatas, afmoNegarComAviso) then
  begin
    FmVSGerArtDatas.FCodigo := Codigo;
    FmVSGerArtDatas.TPDtHrAberto.Date         := DtHrAberto;
    FmVSGerArtDatas.EdDtHrAberto.ValueVariant := DtHrAberto;
    FmVSGerArtDatas.TPDtHrLibCla.Date         := DtHrLibCla;
    FmVSGerArtDatas.EdDtHrLibCla.ValueVariant := DtHrLibCla;
    FmVSGerArtDatas.FDtHrCfgCla               := DtHrCfgCla;
    FmVSGerArtDatas.TPDtHrCfgCla.Date         := DtHrCfgCla;
    FmVSGerArtDatas.EdDtHrCfgCla.ValueVariant := DtHrCfgCla;
    FmVSGerArtDatas.FDtHrFimCla               := DtHrFimCla;
    FmVSGerArtDatas.TPDtHrFimCla.Date         := DtHrFimCla;
    FmVSGerArtDatas.EdDtHrFimCla.ValueVariant := DtHrFimCla;
    //
    FmVSGerArtDatas.FMovimCod                 := MovimCod;
    FmVSGerArtDatas.FMovimID                  := Integer(MovimID); //emidIndsXX);
    //
    FmVSGerArtDatas.ShowModal;
    FmVSGerArtDatas.Destroy;
    //
    //LocCod(Codigo, Codigo);
  end;
end;

procedure TUnVS_Jan.MostraFormVSGerRclDatas(Codigo, MovimCod: Integer;
  DtHrAberto, DtHrLibCla, DtHrCfgCla, DtHrFimCla: TDateTime;
  MovimID: TEstqMovimID);
begin
  if not DBCheck.LiberaPelaSenhaBoss() then
    Exit;
  //Codigo := QrVSGerRclCodigo.Value;
  if DBCheck.CriaFm(TFmVSGerRclDatas, FmVSGerRclDatas, afmoNegarComAviso) then
  begin
    FmVSGerRclDatas.FCodigo := Codigo;
    FmVSGerRclDatas.TPDtHrAberto_.Date         := DtHrAberto;
    FmVSGerRclDatas.EdDtHrAberto_.ValueVariant := DtHrAberto;
    FmVSGerRclDatas.TPDtHrLibCla.Date         := DtHrLibCla;
    FmVSGerRclDatas.EdDtHrLibCla.ValueVariant := DtHrLibCla;
    FmVSGerRclDatas.FDtHrCfgCla               := DtHrCfgCla;
    FmVSGerRclDatas.TPDtHrCfgCla.Date         := DtHrCfgCla;
    FmVSGerRclDatas.EdDtHrCfgCla.ValueVariant := DtHrCfgCla;
    FmVSGerRclDatas.FDtHrFimCla               := DtHrFimCla;
    FmVSGerRclDatas.TPDtHrFimCla.Date         := DtHrFimCla;
    FmVSGerRclDatas.EdDtHrFimCla.ValueVariant := DtHrFimCla;
    //
    FmVSGerRclDatas.FMovimCod                 := MovimCod;
    FmVSGerRclDatas.FMovimID                  := Integer(MovimID); //emidIndsXX);
    //
    FmVSGerRclDatas.ShowModal;
    FmVSGerRclDatas.Destroy;
    //
    //LocCod(Codigo, Codigo);
  end;
end;

procedure TUnVS_Jan.MostraFormVSImpEmProcBH();
begin
  if DBCheck.CriaFm(TFmVSImpEmProcBH, FmVSImpEmProcBH, afmoNegarComAviso) then
  begin
    FmVSImpEmProcBH.ShowModal;
    FmVSImpEmProcBH.Destroy;
  end;
end;

procedure TUnVS_Jan.MostraFormVSImpEstqReduz();
begin
  if DBCheck.CriaFm(TFmVSImpEstqReduz, FmVSImpEstqReduz, afmoNegarComAviso) then
  begin
    FmVSImpEstqReduz.ShowModal;
    FmVSImpEstqReduz.Destroy;
  end;
end;

procedure TUnVS_Jan.MostraFormVSInnNFsCab();
begin

end;

procedure TUnVS_Jan.MostraFormVSLoadCRCCab1(Codigo, Controle: Integer);
const
  sProcName = 'UnVS_Jan.MostraFormVSLoadCRCCab1()';
begin
  if (Trim(TMeuERP_CDR_DB) <> '')
  and (Trim(Lowercase(TMeuERP_CDR_DB)) <> 'clarecocdr_ nomeemp') then
  begin
    try
      DModG.ConectaMySyncDB(TMeuERP_CDR_DB);
    except
      Geral.MB_Aviso(
      'N�o foi poss�vel conectar no BD de sincronismo do ClaReCo!' + sLineBreak +
      'Database: ' + TMeuERP_CDR_DB);
      Exit;
    end;
    if DBCheck.CriaFm(TFmVSLoadCRCCab1, FmVSLoadCRCCab1, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
      begin
        FmVSLoadCRCCab1.LocCod(Codigo, Codigo);
        // Somente se for o controle de baixa vai localizar!
        if FmVSLoadCRCCab1.QrVSLoadCRCCab.State <> dsInactive then
          FmVSLoadCRCCab1.QrVSLoadCRCCab.Locate('Controle', Controle, []);
        //
      end;
      FmVSLoadCRCCab1.ShowModal;
      FmVSLoadCRCCab1.Destroy;
    end;
  end else
    Geral.MB_Aviso(
    'Nome do BD de sincronismo com o ClaReCo n�o foi definido nas op��es do ERP!');
end;

procedure TUnVS_Jan.MostraFormVSLoadCRCCab2(Codigo, Controle: Integer);
const
  sProcName = 'UnVS_Jan.MostraFormVSLoadCRCCab2()';
begin
  if (Trim(TMeuERP_CDR_DB) <> '')
  and (Trim(Lowercase(TMeuERP_CDR_DB)) <> 'clarecocdr_ nomeemp') then
  begin
    try
      DModG.ConectaMySyncDB(TMeuERP_CDR_DB);
    except
      Geral.MB_Aviso(
      'N�o foi poss�vel conectar no BD de sincronismo do ClaReCo!' + sLineBreak +
      'Database: ' + TMeuERP_CDR_DB);
      Exit;
    end;
    if DBCheck.CriaFm(TFmVSLoadCRCCab2, FmVSLoadCRCCab2, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
      begin
        FmVSLoadCRCCab2.LocCod(Codigo, Codigo);
        // Somente se for o controle de baixa vai localizar!
        if FmVSLoadCRCCab2.QrVSLoadCRCCab.State <> dsInactive then
          FmVSLoadCRCCab2.QrVSLoadCRCCab.Locate('Controle', Controle, []);
        //
      end;
      FmVSLoadCRCCab2.ShowModal;
      FmVSLoadCRCCab2.Destroy;
    end;
  end else
    Geral.MB_Aviso(
    'Nome do BD de sincronismo com o ClaReCo n�o foi definido nas op��es do ERP!');
end;

procedure TUnVS_Jan.MostraFormVSLoadCRCPalletA(Pallet: Integer);
begin
  if DBCheck.CriaFm(TFmVSLoadCRCPalletA, FmVSLoadCRCPalletA, afmoNegarComAviso) then
  begin
    FmVSLoadCRCPalletA.ReopenPallets(Pallet);
    FmVSLoadCRCPalletA.ShowModal;
    FmVSLoadCRCPalletA.Destroy;
  end;
end;

procedure TUnVS_Jan.MostraFormVSLocalizaMarcas();
begin
  if DBCheck.CriaFm(TFmVSLocalizaMarcas, FmVSLocalizaMarcas, afmoNegarComAviso) then
  begin
    FmVSLocalizaMarcas.ShowModal;
    FmVSLocalizaMarcas.Destroy;
  end;
end;

procedure TUnVS_Jan.MostraFormVSMarcaAjusta();
begin
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  //
  if DBCheck.CriaFm(TFmVSMarcaAjusta, FmVSMarcaAjusta, afmoNegarComAviso) then
  begin
    FmVSMarcaAjusta.ShowModal;
    FmVSMarcaAjusta.Destroy;
  end;
end;

procedure TUnVS_Jan.MostraFormVSSifDipoaImp(Pallets: String);
begin
  if DBCheck.CriaFm(TFmVSSifDipoaImp, FmVSSifDipoaImp, afmoNegarComAviso) then
  begin
    if Trim(Pallets) <> EmptyStr then
    begin
      FmVSSifDipoaImp.EdPallets.ValueVariant := Pallets;
      FmVSSifDipoaImp.CkDataIni.Checked := False;
      FmVSSifDipoaImp.CkDataFim.Checked := False;
    end;
    FmVSSifDipoaImp.ShowModal;
    FmVSSifDipoaImp.Destroy;
  end;
end;

procedure TUnVS_Jan.MostraFormVSTabMPAG();
begin
  if DBCheck.CriaFm(TFmVSTabMPAG, FmVSTabMPAG, afmoNegarComAviso) then
  begin
    FmVSTabMPAG.ShowModal;
    FmVSTabMPAG.Destroy;
  end;
end;

end.
