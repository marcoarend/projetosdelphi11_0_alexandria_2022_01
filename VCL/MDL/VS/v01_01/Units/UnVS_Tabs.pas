unit UnVS_Tabs;
// VS - Controle de estoque para Bloco K - SPED
{ Colocar no MyListas:

Uses UnVS_Tabs;


//
function TMyListas.CriaListaTabelas:
      VS_Tabs.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
    VS_Tabs.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:

    VS_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      VS_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      VS_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    VS_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  VS_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms, UnInternalConsts, dmkGeral, UnDmkEnums,
  UnProjGroup_Consts, AppListas, MyListas, UnGrl_Vars, UnAppEnums;

type
  TUnVS_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(TabelaBase: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>;
             CollectDataServerRepository: Boolean = False): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
             FLQeiLnk: TList<TQeiLnk>): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos: TCampos;
             FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  VS_Tabs: TUnVS_Tabs;

implementation

uses UMySQLModule;


function TUnVS_Tabs.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
const
  QeiLnk = True;
  NoCRCTM = TCRCTableManage.crctmIndef;
var
  CDR: Boolean;
begin
  try
    if CO_SIGLA_APP = 'CLRC' then
    begin
      CDR := VAR_IS_CollectDataServerRepository; // or (CO_SIGLA_APP = 'BDER');
//////  C A D A S T R O S   I M P R E S C I N D � V E I S  //////////////////////////////////////////////////////////////

      MyLinguas.AdTbLst(Lista, False, Lowercase('ArtGeComodty'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('CouNiv1'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('CouNiv2'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('DefeitosLoc'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('DefeitosTip'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('GraGruXCou'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('Operacoes'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMixClaCab'), '_lst_sample'); // Mix de classes Classes
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSNatArt'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSNatCad'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSNatInC'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSNatCon'), 'VSNatCad');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSRibCad'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSRibCla'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSSerFch'), '_lst_sample');   //   - S�ries de Fichas
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPalSta'), '_lst_sample');   //   - Status do Pallet
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMrtCad'), '_lst_sample');   //   - Martelos de procedencia
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMovimID'), '');
//////  C A D A S T R O S   D E   C O N F I G U R A � � O   ( A U X I L I A R ) /////////////////////////////////////////
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCOPCab'), ''); // Configura��o de processo, opera��o, etc
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCOPIts'), ''); // WB de origem de Configura��o de processo, opera��o, etc
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPMOCab'), (''));
//////  ? ? ? ? ? ? ? ? ? ///////////////////////////////////////////////////////////////////////////////////////////////
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSEntiMP'), 'EntiMP', False, CDR);
//////  M O V I M E N T O ///////////////////////////////////////////////////////////////////////////////////////////////
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPalletA'), Lowercase('WBPallet'), False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase(CO_TAB_TAB_VMI), Lowercase('WBMovIts'), False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMovCab'), Lowercase('WBMovCab'), False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSInnCab'), Lowercase('WBInnCab'), False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSInnRas'), '', False, CDR);  // Rastreio de In Naturo no Curtido
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSInnNFs'), '', False, CDR);
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSInnNFsCab'), '', False, CDR);
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSInnNFsIts'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMovIDLoc'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSESCCab'), '', False, CDR); // ESC = Entrada sem Cobertura
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSSSCCab'), Lowercase('WBAjsCab'), False, CDR); // SSC = Sa�da sem Cobertura
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSGerArtA'), 'VSGerArt', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSGerRclA'), 'VSGerRcl', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPrePalCab'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaClaCabA'), 'VSPaClaCab', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaClaItsA'), 'VSPaClaIts', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaRclCabA'), 'VSPaRclCab', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaRclItsA'), 'VSPaClaIts', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCacCab'), Lowercase('WBMovCab'), False, CDR); // Cac  > Couro a couro
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCacItsA'), Lowercase('VSCacIts'), False, CDR); // Cac  > Couro a couro
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaMulCabA'), 'VSPaMulCab', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaMulItsA'), 'VSPaMulIts', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaMulCabR'), 'VSPaMulCab', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaMulItsR'), 'VSPaMulIts', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMovDif'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSFchRMPCab'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMulFrnCab'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMulFrnIts'), '', False, CDR);
//////  M O V I M E N T O (Compatibilidade! n�o implementado!) /////////////////
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSOutNFeCab'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSOutNFeIts'), '', False, CDR);
      // 2018-03-25
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPlCCab'), Lowercase('WBInnCab'), False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSOpeCab'), '', False, CDR); // Operacao de Artigo (Classificado) com Gera��o de subprodutos
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPWECab'), 'VSOpeCab', False, CDR); // Processo de WetEnd
      // Fim 2018-03-25
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSOutCab'), Lowercase('WBOutCab'), False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSOutFat'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSTrfLocCab'), '', False, CDR);

//////  A R Q U I V O   M O R T O  /////////////////////////////////////////////////////////////////////////////////////
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMovItb'), Lowercase('WBMovIts'));
      MyLinguas.AdTbLst(Lista, True, Lowercase('VSMovItz'), Lowercase('WBMovIts'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCacItsB'), Lowercase('VSCacIts')); // Cac  > Couro a couro
    end else
    begin
      CDR := False; // VAR_IS_CollectDataServerRepository;
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('ArtGeComodty'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('GraGruXCou'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('CouNiv1'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('CouNiv2'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('DefeitosLoc'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('DefeitosTip'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('EntiMP'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('Operacoes'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSAllGGX'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSAjsCab'), Lowercase('WBAjsCab'));
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSBalCab'), '');  Mudado para SPEDEFDEnce!
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSBalEmp'), '');  Mudado para SPEDEFDEnce!
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSArtCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSArtGGX'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSBxaCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCacCab'), Lowercase('WBMovCab'), False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward); // Cac  > Couro a couro
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCacItsA'), Lowercase('VSCacIts'), False, CDR, TCRCTableManage.crctmCRCUpNotSyncToward); // Cac  > Couro a couro
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCacItsB'), Lowercase('VSCacIts')); // Cac  > Couro a couro
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCfgEqzCb'), ''); // config de equalize Cabecalho
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCfgEqzIt'), ''); // config de equalize Itens
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCGICab'), ''); // CGI > Classes Geradas de IME-I
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCGIIts'), ''); // CGI > Classes Geradas de IME-I
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCPMRSBCb'), ''); // Config. Plano metas rendim. subprodutos
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCPMRSBIt'), ''); // Config. Plano metas rendim. subprodutos

      MyLinguas.AdTbLst(Lista, False, Lowercase('VSDefeiCaC'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('VSDSCCab'), 'WBRclCab'); // Processo de WetEnd
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSDsnCab'), ''); // Dsn > Desnate
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSDsnArt'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSDsnIts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSDsnSub'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSEntiMP'), 'EntiMP', False, CDR, TCRCTableManage.crctmAllUpAndSyncOnlyIns);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSEqzCab'), ''); // Eqz> Equ�ize
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSEqzArt'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSEqzIts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSEqzSub'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSExBCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSExcCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSFchRMPCab'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSFchRslCus'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSFchRslCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSFchRslIts'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('VSGruGGX'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('VSESCCab'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward); // ESC = Entrada sem Cobertura
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMovimID'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMovIDLoc'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);

      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMulFrnCab'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMulFrnIts'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);

      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMulNFeCab'), 'VSMulFrnCab');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMulNFeIts'), '');

      MyLinguas.AdTbLst(Lista, False, Lowercase('VSNatArt'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSNatCad'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSNatInC'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSNatCon'), 'VSNatCad');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSNatPDA'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSProCal'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCouCal'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCouDTA'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSProCur'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCouCur'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSRibCad'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSRibArt'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSRibCla'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSRibOpe'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSSubPrd'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPSPPro'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPSPEnd'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSTrfLocCab'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSWetEnd'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSFinCla'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSRepMer'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSClaWet'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCOPCab'), ''); // Configura��o de processo, opera��o, etc
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCOPIts'), ''); // WB de origem de Configura��o de processo, opera��o, etc
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMOEnvAvu'), (''));
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMOEnvAVMI'), (''));
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMOEnvEnv'), (''));
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMOEnvEVMI'), (''));
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMOEnvRet'), (''));
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMOEnvRVMI'), (''));
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMOEnvGVMI'), (''));
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMovCab'), Lowercase('WBMovCab'), False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase(CO_TAB_TAB_VMI), Lowercase('WBMovIts'), False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward, 'VSMovItZ');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMovItb'), Lowercase('WBMovIts'));
      MyLinguas.AdTbLst(Lista, False, Lowercase(CO_TAB_TAB_CRCI), Lowercase('WBMovIts'));
      MyLinguas.AdTbLst(Lista, True, Lowercase('VSMovItz'), Lowercase('WBMovIts'), False, CDR, TCRCTableManage.crctmCRCUpAndSyncDelSelf);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMovDif'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSHisFch'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSInnCab'), Lowercase('WBInnCab'), False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSInnRas'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSInnNFs'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSInnNFsCab'), '', False, CDR);
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSInnNFsIts'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSInvNFe'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMotivBxa'), '_lst_sample');   //   - Motivos de Baixa Extravio
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMorCab'), '');   //   - Arquivo morto VS
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSMrtCad'), '_lst_sample');   //   - Martelos de procedencia
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSObsNFx'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSOpeCab'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward); // Operacao de Artigo (Classificado) com Gera��o de subprodutos
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSOpeSeq'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSOutCab'), Lowercase('WBOutCab'), False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSOutFat'), '', False, CDR);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSOutNFeCab'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSOutNFeIts'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSOutNFI'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPalletA'), Lowercase('WBPallet'), False, CDR, TCRCTableManage.crctmAllUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPalFrn'), Lowercase('WBPalFrn')); // Vai usar?
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPalArt'), Lowercase('WBPalArt'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSProQui'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPSPCab'), 'VSOpeCab'); // Processo de Subproduto
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPWECab'), 'VSOpeCab', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward); // Processo de WetEnd
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSRRMCab'), 'VSOpeCab'); // Reprocesso/Reparo de  Mercadoria
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSConCab'), 'VSOpeCab'); // Conserva��o de couro verde
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSConJmp'), 'VSOpeCab'); // Couro Conservado????
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCalCab'), 'VSOpeCab'); // Processo de Caleiro
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCalJmp'), 'VSOpeCab'); // Couro Caleirado
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCalPDA'), 'VSOpeCab'); // Pr�-descarne
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCalDTA'), 'VSOpeCab'); // Redescarne / divisao
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCurCab'), 'VSOpeCab'); // Processo de Curtimento
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSCurJmp'), 'VSOpeCab'); // Couro Curtido
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSGerArtA'), 'VSGerArt', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSGerRclA'), 'VSGerRcl', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSGerAPalA'), Lowercase('VSGerAPal'));
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSGerAPalB'), Lowercase('VSGerAPal'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPalSta'), '_lst_sample');   //   - Status do Pallet
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaClaCabA'), 'VSPaClaCab', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaClaItsA'), 'VSPaClaIts', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSRclAPalA'), Lowercase('VSRclAPal'));
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSRclAPalB'), Lowercase('VSRclAPal'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaMulCabA'), 'VSPaMulCab', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaMulItsA'), 'VSPaMulIts', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaMulCabR'), 'VSPaMulCab', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaMulItsR'), 'VSPaMulIts', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSRclCab'), 'WBRclCab');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaRclCabA'), 'VSPaRclCab', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaRclItsA'), 'VSPaClaIts', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPaRclBxaA'), 'VSPaRclBxa');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPlCCab'), Lowercase('WBInnCab'), False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPMOCab'), (''));
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSDvlCab'), Lowercase('WBInnCab'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSRtbCab'), Lowercase('WBInnCab'));
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPedCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPedIts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSPrePalCab'), '', False, CDR, TCRCTableManage.crctmCRCUpAndSyncToward);
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSSerFch'), '_lst_sample');   //   - S�ries de Fichas
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSSubPrdCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSReqDiv'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSReqMov'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSInfInn'), 'VSReqMov');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSInfMov'), 'VSReqMov');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSInfOut'), 'VSReqMov');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSLstPal'), 'VSReqMov');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSInfPal'), 'VSReqMov');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSInfIEC'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSVmcWrn'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSLoadCRCCab'), '');
      //MyLinguas.AdTbLst(Lista, False, Lowercase('VSLoadCRCIts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSLoadCRCTbs'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSLoadCRCWrn'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('VSSifDipoa'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBInnCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBArtCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBAjsCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBMovCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBMovIts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBMPrCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBMPrFrn'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBOutCab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBPallet'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBPalFrn'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBPalArt'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('WBRclCab'), '');
    end;
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TUnVS_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
var
  I: Integer;
begin
{
separador = "|"
}
  Result := True;
(*
  if Uppercase(Tabela) = Uppercase('VSCtrl') then
  begin
    FListaSQL.Add('Codigo');
    FListaSQL.Add('1');
  end;
*)
  if Uppercase(Tabela) = Uppercase('ArtGeComodty') then
  begin
    FListaSQL.Add('Codigo|Nome|Ordem');
    FListaSQL.Add('0|"N/D"|0');
  end else
  if Uppercase(Tabela) = Uppercase('CouNiv1') then
  begin
    FListaSQL.Add('Codigo|Nome|FatorInt');
    FListaSQL.Add('0|"Indefinido"|0.000');
    FListaSQL.Add('1|"Inteiro"|1.000');
    FListaSQL.Add('2|"Meios"|0.500');
    FListaSQL.Add('3|"Cabe�a"|0.3000');
    FListaSQL.Add('4|"Culatra"|0.700');
    FListaSQL.Add('5|"Barrigas"|0.250');
    FListaSQL.Add('6|"Grup�o"|0.400');
    FListaSQL.Add('7|"Drops"|1.000');
    FListaSQL.Add('8|"Recortes"|0.000');
    FListaSQL.Add('9|"Retalhos"|0.000');
    FListaSQL.Add('10|"Carna�a"|0.000');
    FListaSQL.Add('11|"Sebo"|0.000');
    FListaSQL.Add('12|"Res�duos"|0.000');
    FListaSQL.Add('13|"Outros"|0.000');
  end else if Uppercase(Tabela) = Uppercase('Defeitosloc') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"(TODA PELE)"');
  end else if Uppercase(Tabela) = Uppercase('Defeitostip') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"(N�O DEFINIDO)"');
  end else if Uppercase(Tabela) = Uppercase('Operacoes') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"(N�O DEFINIDA)"');
  end else
  if Uppercase(Tabela) = Uppercase('VSPalSta') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"Bloqueado"');
    FListaSQL.Add(Geral.FF0(CO_STAT_VSPALLET_0100_DISPONIVEL) + '|"Dispon�vel"');
    FListaSQL.Add(Geral.FF0(CO_STAT_VSPALLET_0300_INDICADO)   + '|"Indicado"');
    FListaSQL.Add(Geral.FF0(CO_STAT_VSPALLET_0500_RESERVADO)  + '|"Reservado"');
    FListaSQL.Add(Geral.FF0(CO_STAT_VSPALLET_0900_PROMETIDO)  + '|"Prometido"');
  end else
  if Uppercase(Tabela) = Uppercase('VSMovimID') then
  begin
    FListaSQL.Add('Codigo|Nome');
    for I := 0 to MaxEstqMovimID do
      if sEstqMovimID[I] <> '' then
        FListaSQL.Add(Geral.FF0(Integer(TEstqMovimID(I))) + '|"' + sEstqMovimID[I] + '"');
  end else
  if Uppercase(Tabela) = Uppercase('VSMulFrnCab') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('-1|"Saldo inicial"');
  end else
  if Uppercase(Tabela) = Uppercase('VSVmcWrn') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"OK"');
    FListaSQL.Add('16|"Info faltando"');
    FListaSQL.Add('32|"Info parcial"');
    FListaSQL.Add('48|"Info dubia"');
    FListaSQL.Add('64|"Info errada"');
  end;
end;

function TUnVS_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('') then
  begin
  end else if Uppercase(Tabela) = Uppercase('PerfisIts') then
  begin
    //FListaSQL.Add('"Entidades"|"Cadastro de pessoas f�sicas e jur�dicas (clientes| fornecedores| etc.)"');
    //FListaSQL.Add('""|""');
  end;
end;


function TUnVS_Tabs.CarregaListaFRIndices(TabelaBase: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>; CollectDataServerRepository: Boolean): Boolean;
begin
  Result := True;
  //
  if Uppercase(TabelaBase) = Uppercase('DefeitosLoc') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('DefeitosTip') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('GraGruXCou') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('ArtGeComodty') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('CouNiv1') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else if Uppercase(TabelaBase) = Uppercase('CouNiv2') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else if Uppercase(TabelaBase) = Uppercase('EntiMP') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else if Uppercase(TabelaBase) = Uppercase('Operacoes') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSArtCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSArtGGX') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSBalCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Periodo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSBalEmp') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Periodo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSBxaCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSDefeiCaC') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSCacIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSCfgEqzCb') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSCfgEqzIt') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'SubClass';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSCGICab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSCGIIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSCPMRSBCb') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSCPMRSBIt') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSDsnCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSDsnArt') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSDsnIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'VSCacCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSDsnSub') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'SubClass';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSEqzCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSEqzIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Pallet';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSEqzSub') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'SubClass';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSESCCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSExBCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSExcCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
(*
  end else
  if Uppercase(TabelaBase) = Uppercase('VSFchRslCus') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'SerieFch';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Ficha';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
*)
  end else
  if Uppercase(TabelaBase) = Uppercase('VSFchRMPCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'SerieFch';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Ficha';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'MovimID';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSFchRslCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'SerieFch';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Ficha';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSFchRslIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'SerieFch';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Ficha';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSInfIEC') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSInnNFs') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
(*
  end else
  if Uppercase(TabelaBase) = Uppercase('VSInnNFsCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSInnNFsIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
*)
  end else
  if Uppercase(TabelaBase) = Uppercase('VSInvNFe') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSMorCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
(*
  end else
  if Uppercase(TabelaBase) = Uppercase('VSObsNFx') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
*)
  end else
  if Uppercase(TabelaBase) = Uppercase('VSNatCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSNatInC') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSNatPDA') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSProCal') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSCouCal') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSCouDTA') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSProCur') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSCouCur') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  (*
  end else
  if Uppercase(TabelaBase) = Uppercase('VSAllGGX') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  *)
  end else
  if Uppercase(TabelaBase) = Uppercase('VSGruGGX') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSMovimID') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSMovIDLoc') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimID';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'StqCenLoc';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSMulFrnCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSMulFrnIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSMulNFeIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSNatArt') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSRibCad') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSRibArt') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSRibCla') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSRibOpe') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSSubPrd') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSPSPPro') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSPSPEnd') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSTrfLocCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSWetEnd') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSFinCla') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSRepMer') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSClaWet') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSCOPCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSCOPIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSMOEnvAvu') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSMOEnvAVMI') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'VSMOEnvAvu';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'VSMovIts';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSMOEnvEnv') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSMOEnvEVMI') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'VSMOEnvEnv';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'VSMovIts';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSMOEnvGVMI') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'VSMOEnvRet';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'VSMovIts';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSMOEnvRVMI') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'VSMOEnvRet';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'VSMOEnvEnv';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSMOEnvRet') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSMovDif') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSHisFch') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSOpeCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSGerArt') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSGerAPal') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSPaClaCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'VSGerArt';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := CO_FLD_TAB_VMI;
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'VSESCCab';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSPaClaIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'VMI_Baix';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'VMI_Dest';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE2';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'VMI_Dest';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSPaMulCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSPaMulIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSPaRclCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'VSGerRcl';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := CO_FLD_TAB_VMI;
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSPaRclBxa') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'VMI_Baix';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSPrePalCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSGerRcl') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSPMOCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSPedCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSPedIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSRclAPal') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSSubPrdCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSReqDiv') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSReqMov') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSOpeSeq') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSOutFat') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSOutNFeCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'ide_serie';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'ide_nNF';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSOutNFeIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
(*
  end else
  if Uppercase(TabelaBase) = Uppercase('VSOutNFI') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'ItemNFe';
    FLIndices.Add(FRIndices);
    //
*)
  end else
  if Uppercase(TabelaBase) = Uppercase('VSProQui') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'SetorID';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSVmcWrn') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSVmcWrn') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSLoadCRCCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSLoadCRCIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSLoadCRCTbs') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSLoadCRCWrn') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSSifDipoa') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBArtCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBAjsCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBInnCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBMovCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBMovIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('VSInnRas') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBMPrCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBMPrFrn') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'GraGruX';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Fornece';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBPallet') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBPalFrn') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Conta';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBPalArt') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBOutCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(TabelaBase) = Uppercase('WBRclCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'MovimCod';
    FLIndices.Add(FRIndices);
    //
  end;
end;

function TUnVS_Tabs.CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
  FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
  Result := False; // mudar para true
  //
  if Uppercase(TabelaCria) = Uppercase('VSCacCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CodigoID';
    FRQeiLnk.RplyTab       := CO_VSXxxTab;
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := 'MovimID';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncMulTab;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSEntiMP') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpERPSrvrIncOver;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MPVImpOpc';
    FRQeiLnk.RplyTab       := 'MPVImpOpc';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
  end else
  if Uppercase(TabelaCria) = Uppercase('VSEscCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'VSMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSFchRMPCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'SerieFch';
    FRQeiLnk.RplyTab       := 'VSSerFch';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpERPPriNoIncRelatSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Ficha';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpERPPriNoIncNoRelSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimID';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpERPPriNoIncNoRelSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSGerArtA') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'VSMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CacCod';
    FRQeiLnk.RplyTab       := 'VSCacCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSGerRclA') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'VSMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CacCod';
    FRQeiLnk.RplyTab       := 'VSCacCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
(*
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'VSPallet';
    FRQeiLnk.RplyTab       := 'VSPalletA';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
*)
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSInnCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'VSMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSInnNFs') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := 'VSInnCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := 'VSMovIts';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Conta';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
(*
  end else
  if Uppercase(TabelaCria) = Uppercase('VSInnNFsCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := 'VSInnCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := 'VSMovIts';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSInnNFsIts') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := 'VSInnCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := 'VSMovIts';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Conta';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
*)
  end else
  if Uppercase(TabelaCria) = Uppercase('VSMovCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CodigoID';
    FRQeiLnk.RplyTab       := CO_VSXxxTab;
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := 'MovimID';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncMulTab;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSMovDif') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := 'VSMovIts';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpERPPriNoIncRelatSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSMovIDLoc') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSMovIts') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := CO_VSXxxTab;
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := 'MovimID';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncMulTab;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'VSMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimTwn';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'MovimTwn';
    FRQeiLnk.MTbsCol       := 'VSMovimTwn';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncOrfao;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'SrcNivel1';
    FRQeiLnk.RplyTab       := CO_VSXxxTab;
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := 'SrcMovID';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncMulTab;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'SrcNivel2';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'DstNivel1';
    FRQeiLnk.RplyTab       := CO_VSXxxTab;
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := 'DstMovID';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncMulTab;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'DstNivel2';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'GSPInnNiv2';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'GSPArtNiv2';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'JmpNivel1';
    FRQeiLnk.RplyTab       := CO_VSXxxTab;
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := 'JmpMovID';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncMulTab;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'JmpNivel2';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'RmsNivel1';
    FRQeiLnk.RplyTab       := CO_VSXxxTab;
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := 'RmsMovID';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncMulTab;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'RmsNivel2';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'GSPSrcNiv2';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'GSPJmpNiv2';
    FRQeiLnk.RplyTab       := TabelaCria;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovCodPai';
    FRQeiLnk.RplyTab       := 'VSMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSMovItz') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := 'VSMovIts';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRDeleteSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSMulFrnCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovTypeCod';
    FRQeiLnk.RplyTab       := 'TEstqDefMulFldEMxx';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := 'MovimType';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelSncMulTab;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSMulFrnIts') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := 'VSMulFrnCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSOpeCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'VSMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CacCod';
    FRQeiLnk.RplyTab       := 'VSCacCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'EmitGru';
    FRQeiLnk.RplyTab       := 'EmitGru';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSOutCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'VSMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSOutNFeCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'VSMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'OriCod';
    FRQeiLnk.RplyTab       := 'VSOutCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSOutNFeIts') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := 'VSOutNFeIts';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSPaClaCabA') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'VSGerArt';
    FRQeiLnk.RplyTab       := 'VSGerArtA';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'VSMovIts';
    FRQeiLnk.RplyTab       := 'VSMovits';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CacCod';
    FRQeiLnk.RplyTab       := 'VSCacCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if (Uppercase(TabelaCria) = Uppercase('VSPaMulCabA'))
  or (Uppercase(TabelaCria) = Uppercase('VSPaMulCabR')) then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'VSMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CacCod';
    FRQeiLnk.RplyTab       := 'VSCacCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'VMI_Sorc';
    FRQeiLnk.RplyTab       := 'VSMovits';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'VSGerArt';
    FRQeiLnk.RplyTab       := 'VSGerArtA';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if (Uppercase(TabelaCria) = Uppercase('VSPaMulItsA'))
  or (Uppercase(TabelaCria) = Uppercase('VSPaMulItsR')) then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := Geral.Substitui(Lowercase(TabelaCria), 'its', 'cab');
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'VMI_Sorc';
    FRQeiLnk.RplyTab       := 'VSMovits';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'VMI_Dest';
    FRQeiLnk.RplyTab       := 'VSMovits';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'VMI_Baix';
    FRQeiLnk.RplyTab       := 'VSMovits';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSPaRclCabA') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'VSGerRcl';
    FRQeiLnk.RplyTab       := 'VSGerRclA';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'VSMovIts';
    FRQeiLnk.RplyTab       := 'VSMovits';
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CacCod';
    FRQeiLnk.RplyTab       := 'VSCacCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if (Uppercase(TabelaCria) = Uppercase('VSPaClaItsA'))
  or (Uppercase(TabelaCria) = Uppercase('VSPaRclItsA')) then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := Geral.Substitui(Lowercase(TabelaCria), 'its', 'cab');
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Controle';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'VMI_Sorc';
    FRQeiLnk.RplyTab       := CO_TAB_VMI;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'VMI_Baix';
    FRQeiLnk.RplyTab       := CO_TAB_VMI;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'VMI_Dest';
    FRQeiLnk.RplyTab       := CO_TAB_VMI;
    FRQeiLnk.RplyCol       := 'Controle';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSPalletA') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpAllSrvrIncUniq;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'GerRclCab';
    FRQeiLnk.RplyTab       := 'VSGerRclA';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSPlCCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'VSMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSPrePalCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
(*
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'VSPallet';
    FRQeiLnk.RplyTab       := 'VSPalletA';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
*)
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSPWECab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'VSMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'CacCod';
    FRQeiLnk.RplyTab       := 'VSCacCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'EmitGru';
    FRQeiLnk.RplyTab       := 'EmitGru';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end else
  if Uppercase(TabelaCria) = Uppercase('VSTrfLocCab') then
  begin
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'Codigo';
    FRQeiLnk.RplyTab       := '';
    FRQeiLnk.RplyCol       := '';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRIncremSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
    New(FRQeiLnk);
    FRQeiLnk.AskrTab       := TabelaCria;
    FRQeiLnk.AskrCol       := 'MovimCod';
    FRQeiLnk.RplyTab       := 'VSMovCab';
    FRQeiLnk.RplyCol       := 'Codigo';
    FRQeiLnk.MTbsCol       := '';
    FRQeiLnk.Purpose       := TItemTuplePurpose.itpCDRRelatnSync;
    FRQeiLnk.Seq_in_pref   := 1;
    FLQeiLnk.Add(FRQeiLnk);
    //
  end;
end;

function TUnVS_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    //end else
    if Uppercase(Tabela) = Uppercase('DefeitosLoc') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'ND';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('DefeitosTip') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'ND';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Procede';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(5)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Simbolo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CorBorda';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := Geral.FF0(Integer(clGray));
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CorMiolo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := Geral.FF0(Integer(clRed));
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('EntiMP') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPSrvrIncOver;
      FLCampos.Add(FRCampos);
      //
      // Limite percentual de quebra de viagem
      New(FRCampos);
      FRCampos.Field      := 'CMP_LPQV';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '2.5000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPL_LPQV';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AGI_LPQV';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMI_LPQV';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      // Indice de devolu��o de quebra de viagem
      // 0-nada, 1-Tudo, 2-Excedente
      New(FRCampos);
      FRCampos.Field      := 'CMP_IDQV';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPL_IDQV';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AGI_IDQV';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMI_IDQV';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPLSit'; // Como complementa valores (sobre kg ou pe�as, etc)
      FRCampos.Tipo       := 'tinyint(1)'; // 0-Nota fiscal, 1-Entrada curtume
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MaskLetras';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MaskFormat';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PreDescarn';
      FRCampos.Tipo       := 'tinyint(1)';  // 0- Curtume 1-N�o 2-J� vem
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      // CMP - Compra da m�t�ria-prima
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPUnida'; // Unidade referencial
      FRCampos.Tipo       := 'tinyint(1)';  // 0- kg 1-pe�a 2-m2
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPMoeda'; // Moeda da unidade referencial
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPPerio'; // Per�odo de ac�mulo para pagamento
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPPreco'; // Pre�o da unidade referencial
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPFrete'; // Valor do frete por caminh�o
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //

      // CPL - Complemanro do pagamento da mat�ria-prima
      //
      New(FRCampos);
      FRCampos.Field      := 'CPLUnida'; // Unidade referencial
      FRCampos.Tipo       := 'tinyint(1)';  // 0- kg 1-pe�a 2-m2
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPLMoeda'; // Moeda da unidade referencial
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPLPerio'; // Per�odo de ac�mulo para pagamento
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPLPreco'; // Pre�o da unidade referencial
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPLFrete'; // Valor do frete por caminh�o
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //

      // AGI - Pagamento de �gio na compra da mat�ria-prima
      //
      New(FRCampos);
      FRCampos.Field      := 'AGIUnida'; // Unidade referencial
      FRCampos.Tipo       := 'tinyint(1)';  // 0- kg 1-pe�a 2-m2
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AGIMoeda'; // Moeda da unidade referencial
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AGIPerio'; // Per�odo de ac�mulo para pagamento
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPVImpOpc'; // Cadastro Op��es de impress�o de comiss�es
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AGIPreco'; // Pre�o da unidade referencial
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AGIFrete'; // Valor do frete por caminh�o
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //

      // CMI - Comiss�o na compra da mat�ria-prima
      //
      New(FRCampos);
      FRCampos.Field      := 'CMIUnida'; // Unidade referencial
      FRCampos.Tipo       := 'tinyint(1)';  // 0- kg 1-pe�a 2-m2
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMIMoeda'; // Moeda da unidade referencial
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMIPerio'; // Per�odo de ac�mulo para pagamento
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMIPreco'; // Pre�o da unidade referencial
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMIFrete'; // Valor do frete por caminh�o
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Contatos';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LocalEntrg';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Corretor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Comissao';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondPagto';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondComis';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DescAdiant'; // Valor a descontar (parcelamento de adiantamento)
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Abate'; //  0=Matadouro, 1=Frigorifico
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Transporte';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SdoIniPele';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LoteLetras';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LoteFormat';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NotaVS';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SiglaVS';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('GraGruXCou') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CouNiv1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CouNiv2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArtigoImp';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClasseImp';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrevPcPal'; // PPP - Previs�o de pe�as no Pallet
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrevAMPal'; // PMP - Previs�o de area em m2 no Pallet
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrevKgPal'; // PKP - Previs�o de Kg no Pallet
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MediaMinM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MediaMaxM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MediaMinKg';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MediaMaxKg';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Grandeza';
      FRCampos.Tipo       := 'tinyint(4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Bastidao';
      FRCampos.Tipo       := 'tinyint(4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXPronto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatrClase';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '9.990000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BaseValVenda';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BaseCliente';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BaseImpostos';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasePerComis';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasFrteVendM2';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BaseValLiq';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BaseValCusto';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArtGeComodty';
      FRCampos.Tipo       := 'tinyint(4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrecoMin';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrecoMax';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsaSifDipoa';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ArtGeComodty') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(12)'; // 2023-11-28 de 10 para 12
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '999';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VisuRel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('CouNiv1') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'ND';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorInt';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('CouNiv2') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := 'ND';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('Operacoes') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RedefnFrmt'; // TVSRedefnFrmt
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSOpeSeq'; // tabela VSOpeSeq.Codigo ou CO_SIMS_...
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('VSArtCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiRecu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiRefu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiAcab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtMPs';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observa';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FluxProCab'; // da tabela Fluxos por enquanto...
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FluxPcpCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinCulReb';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinCabReb';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinCulSem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinCabSem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('VSArtGGX') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('VSBalCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSBalEmp') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Periodo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSBxaCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtBaixa';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSDefeiCaC') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod'; // VSCacCab.Codigo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacID';  // VSCacCab.MovimID
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo'; // VSPaClaCab.Codigo  ou VSPaRclCab.Codigo !!!
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacIts';  // Primary Key CacIts
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';  // Primary Key
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Marca';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DefeitLocX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DefeitLocY';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DefeitTipo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DefeitQtde';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSCfgEqzCb') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasNotZero';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSCfgEqzIt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubClass';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasNota';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorEqz';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSCGICab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Desnatador';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'DtHrIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSCGIIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_FLD_TAB_VMI;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSCPMRSBCb') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataIni';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataFim';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSCPMRSBIt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DivKgSPpcCou';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PercKgSPKgCou';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSDsnCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Desnatador';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasNotZero';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSDsnArt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasNota';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Interesse';
      FRCampos.Tipo       := 'tinyint(1)';  // 0=Nao, 1=Sim
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSDsnIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSCacCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSDsnSub') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'Mul';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubClass';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasNota';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Interesse';
      FRCampos.Tipo       := 'tinyint(1)';  // 0=Nao, 1=Sim
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSEqzCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Classificador';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasNotZero';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSCfgEqzCb';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSEqzIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasNota';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSEqzSub') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'Mul';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubClass';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BasNota';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Interesse';
      FRCampos.Tipo       := 'tinyint(1)';  // 0=Nao, 1=Sim
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorEqz';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSESCCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClienteMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '-11';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      // B07
      New(FRCampos);
      FRCampos.Field      := 'ide_serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'ide_nNF';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B07
      New(FRCampos);
      FRCampos.Field      := 'emi_serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'emi_nNF';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrCfgCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovIDAsc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSExBCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtBaixa';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MotivBxa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSExcCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtEntrada';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSFchRMPCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPPriNoIncRelatSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPPriNoIncNoRelSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPPriNoIncNoRelSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReInseriu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSFchRslCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmPago';    // 0-N�o info
      FRCampos.Tipo       := 'tinyint(1)'; // 1-Kg
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR           // 2-Peca
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoUni';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoAll';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TribtMP';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TribtMO';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TribtAll';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoLiq';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSFchRslIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoKg';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoMOKg';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusFretKg';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Preco';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ImpostP';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComissP';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FreteM2';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrcMPAG';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoMOTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusFretTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoINTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoTotal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ImpostCred';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ImpostTotV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComissTotV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FretM2Tot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BrutoV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MargemV';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MargemP';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
    end else if Uppercase(Tabela) = Uppercase('VSFchRslCus') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Preco';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MOKg';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FreteKg';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
    end else if Uppercase(Tabela) = Uppercase('VSInfIEC') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSInfFol';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSInfLin';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Data';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fornecedor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSInnNFs') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motorista';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Placa';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      // B07
      New(FRCampos);
      FRCampos.Field      := 'ide_serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'ide_nNF';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B07
      New(FRCampos);
      FRCampos.Field      := 'emi_serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'emi_nNF';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcWrn';  // 0-Ok, 16-Falta info, 32-Info incompleto, 48-Info dubia, 64-Info errada
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcObs';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcSeq';
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcSta';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=Cosiderar, 1-Ignorar
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
(*
    end else if Uppercase(Tabela) = Uppercase('VSInnNFsCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motorista';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Placa';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      // B06
      New(FRCampos);
      FRCampos.Field      := 'COD_MOD';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C100.06
      New(FRCampos);
      FRCampos.Field      := 'COD_SIT';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // Documento Regular!
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // B07
      New(FRCampos);
      FRCampos.Field      := 'SER';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'NUM_DOC';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // A03  e  EFD:C100.09
      New(FRCampos);
      FRCampos.Field      := 'CHV_NFE';  // NFeCabA.ID ou chNFE
      FRCampos.Tipo       := 'varchar(44)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '100'; // Autorizada
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      // B09 ou EFD:C100.10
      New(FRCampos);
      FRCampos.Field      := 'DT_DOC';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // B10 ou EFD:C100.11
      New(FRCampos);
      FRCampos.Field      := 'DT_E_S';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C100.12 ICMSTot_vNF (ou ICMSTot_vBC) ??
      New(FRCampos);
      FRCampos.Field      := 'VL_DOC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.13
      New(FRCampos);
      FRCampos.Field      := 'IND_PGTO';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.14
      New(FRCampos);
      FRCampos.Field      := 'VL_DESC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.15
      New(FRCampos);
      FRCampos.Field      := 'VL_ABAT_NT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.16
      New(FRCampos);
      FRCampos.Field      := 'VL_MERC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.17
      New(FRCampos);
      FRCampos.Field      := 'IND_FRT';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.18
      New(FRCampos);
      FRCampos.Field      := 'VL_FRT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.19
      New(FRCampos);
      FRCampos.Field      := 'VL_SEG';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.20
      New(FRCampos);
      FRCampos.Field      := 'VL_OUT_DA';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.21
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.22
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.23
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.24
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.25
      New(FRCampos);
      FRCampos.Field      := 'VL_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.26
      New(FRCampos);
      FRCampos.Field      := 'VL_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.27
      New(FRCampos);
      FRCampos.Field      := 'VL_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.28
      New(FRCampos);
      FRCampos.Field      := 'VL_PIS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // EFD:C100.29
      New(FRCampos);
      FRCampos.Field      := 'VL_COFINS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'StaLnk';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcWrn';  // 0-Ok, 16-Falta info, 32-Info incompleto, 48-Info dubia, 64-Info errada
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcObs';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcSeq';
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcSta';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=Cosiderar, 1-Ignorar
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSInnNFsIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimTwn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGru1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'prod_vProd';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'prod_vFrete';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'prod_vSeg';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'prod_vOutro';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // EFD:C170.05
      New(FRCampos);
      FRCampos.Field      := 'QTD';
      FRCampos.Tipo       := 'double(15,5)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.06
      New(FRCampos);
      FRCampos.Field      := 'UNID';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.07
      New(FRCampos);
      FRCampos.Field      := 'VL_ITEM';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.08
      New(FRCampos);
      FRCampos.Field      := 'VL_DESC';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.09
      New(FRCampos);
      FRCampos.Field      := 'IND_MOV';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.10
      New(FRCampos);
      FRCampos.Field      := 'CST_ICMS';
      FRCampos.Tipo       := 'mediumint(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.11
      New(FRCampos);
      FRCampos.Field      := 'CFOP';
      FRCampos.Tipo       := 'mediumint(4)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.12
      New(FRCampos);
      FRCampos.Field      := 'COD_NAT';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.13
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.14
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_ICMS';
      FRCampos.Tipo       := 'double(6,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.15
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.16
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.17
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  EFD:C170.18
      New(FRCampos);
      FRCampos.Field      := 'VL_ICMS_ST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.19
      New(FRCampos);
      FRCampos.Field      := 'IND_APUR';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.20
      New(FRCampos);
      FRCampos.Field      := 'CST_IPI';
      FRCampos.Tipo       := 'char(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //  EFD:C170.21
      New(FRCampos);
      FRCampos.Field      := 'COD_ENQ';
      FRCampos.Tipo       := 'char(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.22
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.23
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_IPI';
      FRCampos.Tipo       := 'double(6,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.24
      New(FRCampos);
      FRCampos.Field      := 'VL_IPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.25
      New(FRCampos);
      FRCampos.Field      := 'CST_PIS';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.26
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.27
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_PIS_p';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.29
      New(FRCampos);
      FRCampos.Field      := 'QUANT_BC_PIS';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.29
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_PIS_r';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.30
      New(FRCampos);
      FRCampos.Field      := 'VL_PIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.31
      New(FRCampos);
      FRCampos.Field      := 'CST_COFINS';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.32
      New(FRCampos);
      FRCampos.Field      := 'VL_BC_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.33
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_COFINS_p';
      FRCampos.Tipo       := 'double(6,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.34
      New(FRCampos);
      FRCampos.Field      := 'QUANT_BC_COFINS';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.35
      New(FRCampos);
      FRCampos.Field      := 'ALIQ_COFINS_r';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.36
      New(FRCampos);
      FRCampos.Field      := 'VL_COFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // EFD:C170.37
      New(FRCampos);
      FRCampos.Field      := 'COD_CTA';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
       // EFD:C170.38
      New(FRCampos);
      FRCampos.Field      := 'VL_ABAT_NT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtCorrApo';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'xLote';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ori_IPIpIPI';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ori_IPIvIPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
    end else if Uppercase(Tabela) = Uppercase('VSInvNFe') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Data';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      // B07
      New(FRCampos);
      FRCampos.Field      := 'ide_serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'ide_nNF';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovCodOri';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcWrn';  // 0-Ok, 16-Falta info, 32-Info incompleto, 48-Info dubia, 64-Info errada
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcObs';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcSeq';
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcSta';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=Cosiderar, 1-Ignorar
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSMorCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IMEILimite';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaLimite';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
    end else if Uppercase(Tabela) = Uppercase('VSObsNFx') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      /
      New(FRCampos);
      FRCampos.Field      := '';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorNota';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1.00000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
    end else if Uppercase(Tabela) = Uppercase('VSNatCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorNota';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1.00000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXInProc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSNatInC') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSNatPDA') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //
    end else if Uppercase(Tabela) = Uppercase('VSProCal') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSCouCal') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSCouDTA') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSProCur') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSCouCur') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSGruGGX') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'tinyint(3)'; // 1 Curtido
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXIntegrl';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXLaminad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXDivTrip';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXDivCurt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSMovimID') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSMovIDLoc') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'StqCenLoc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSMulFrnCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimType';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovTypeCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelSncMulTab;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSMulFrnIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Remistura';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodRemix';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'VMIOrig';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itp;
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'Fornece';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'PerPecas';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itp;
      FLCampos.Add(FRCampos);
      //
*)
    end else if Uppercase(Tabela) = Uppercase('VSMulNFeIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Remistura';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodRemix';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeSer';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSNatArt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSNatCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSRibCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSRibCad') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorNota';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1.00000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSRibArt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSRibCad';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSRibCla';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSRibCla') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'SubProd';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // TEstqSubProd
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
    end else if Uppercase(Tabela) = Uppercase('VSRibOpe') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSSubPrd') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSPSPPro') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSPSPEnd') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSTrfLocCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtVenda';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtViagem';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtEntrega';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Transporta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ide_serie';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ide_nNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcWrn';  // 0-Ok, 16-Falta info, 32-Info incompleto, 48-Info dubia, 64-Info errada
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcObs';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcSeq';
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcSta';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=Cosiderar, 1-Ignorar
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSWetEnd') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorNota';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1.00000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSFinCla') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorNota';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1.00000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSRepMer') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorNota';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1.00000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSClaWet') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSRibCla';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSWetEnd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSCOPCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXDst';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TipoArea';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=M2 1=P2
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ForneceMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClienteMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'StqCenLoc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PedItsLib';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Operacoes'; // Operacoes.Codigo > para saber o RedefnFrmt (TVSRedefnFrmt)
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSPMOCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSArtCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('VSCOPIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end
    else if Uppercase(Tabela) = Uppercase('VSHisFch') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_FLD_TAB_VMI;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSMOEnvAvu') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      New(FRCampos);
      //
      FRCampos.Field      := 'NFEMA_FatID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMA_FatNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMA_Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMA_Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMA_nItem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMA_SerNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMA_nNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMA_Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMA_PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMA_AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMA_AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMA_ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMA_FatID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMA_FatNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMA_Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMA_Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMA_nItem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMA_SerCT';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMA_nCT';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMA_Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMA_PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMA_AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMA_AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMA_PesTrKg';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMA_CusTrKg';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMA_ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSMOEnvAVMI') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSMOEnvAvu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSMovIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorFrete';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSMOEnvEnv') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMP_FatID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMP_FatNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMP_Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMP_Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMP_nItem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMP_SerNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMP_nNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMP_Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMP_PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMP_AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMP_AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMP_ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
{
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_MovimNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
{
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_SerNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_nNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
      New(FRCampos);
      FRCampos.Field      := 'CFTMP_FatID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMP_FatNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMP_Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMP_Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMP_nItem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMP_SerCT';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMP_nCT';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMP_Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMP_PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMP_AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMP_AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMP_PesTrKg';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMP_CusTrKg';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTMP_ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMP_SdoPeca';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMP_SdoPeso';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFEMP_SdoArM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSMOEnvEVMI') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSMOEnvEnv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSMovIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorFrete';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSMOEnvGVMI') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSMOEnvRet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSMovIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSMovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
{
      New(FRCampos);
      FRCampos.Field      := 'ValFreteE';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValFreteR';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
      New(FRCampos);
      FRCampos.Field      := 'ValorFrete';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSMOEnvRVMI') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSMOEnvRet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSMOEnvEnv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorFrete';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSMOEnvRet') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_FatID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_FatNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_nItem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_SerNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_nNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_CusMOM2';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_CusMOKG';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFCMO_ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_FatID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_FatNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_nItem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_SerNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_nNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFRMP_ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
(*
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_MovimNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_SerNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVMI_nNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
*)
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTPA_FatID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTPA_FatNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTPA_Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTPA_Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTPA_nItem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTPA_SerCT';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTPA_nCT';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTPA_Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTPA_PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTPA_AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTPA_AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTPA_PesTrKg';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTPA_CusTrKg';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CFTPA_ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'VSMOEnvEnv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSSrc_Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSSrc_Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSSrc_MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSSrc_MovimNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSSrc_MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSSrc_Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
*)
      //
    end else if Uppercase(Tabela) = Uppercase('VSMovDif') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPPriNoIncRelatSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InfPecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InfPesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InfAreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InfAreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'InfValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DifPecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DifPesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DifAreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DifAreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DifValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerQbrViag';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerQbrSal';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoSalKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RstCouPc';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RstCouKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RstCouVl';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RstSalKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RstSalVl';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RstTotVl';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TribDefSel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSOpeCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrAberto';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrLibOpe';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrCfgOpe';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFimOpe';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKgMan';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PecasMan';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaManM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaManP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorTMan';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKgSrc';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PecasSrc';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaSrcM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaSrcP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorTSrc';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKgDst';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FRCampos.Extra      := '';
      //
      FLCampos.Add(FRCampos);
      New(FRCampos);
      FRCampos.Field      := 'PecasDst';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaDstM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaDstP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorTDst';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKgBxa';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PecasBxa';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaBxaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaBxaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorTBxa';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKgSdo';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PecasSdo';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaSdoM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaSdoP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorTSdo';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TipoArea';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=M2 1=P2
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoManMOKg';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoManMOTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorManMP';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorManT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieRem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeRem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LPFMO';  // Lote de Produ��o do Fornecedor de Mao de Obra
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KgCouPQ';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXDst';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXSrc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
(*
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXJmp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
*)
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Operacoes'; // Operacoes.Codigo > para saber o RedefnFrmt (TVSRedefnFrmt)
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSCOPCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmitGru';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcWrn';  // 0-Ok, 16-Falta info, 32-Info incompleto, 48-Info dubia, 64-Info errada
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcObs';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcSeq';
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcSta';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=Cosiderar, 1-Ignorar
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSArtCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmitGrLote';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinCulReb';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinCabReb';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinCulSem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LinCabSem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataPed';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataStart';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataCrust';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataExped';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiRecu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiRefu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Classes';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MPVIts';  // C�digo OS antiga Softcouros
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSGerArt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrAberto';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrLibCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrCfgCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFimCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PecasMan';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaManM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaManP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TipoArea';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=M2 1=P2
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoManMOKg';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoManMOTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorManMP';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorManT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSGerRcl') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSPallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'DtHrAberto';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itp;
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'DtHrLibCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrCfgCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFimCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'PecasMan';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itp;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaManM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itp;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaManP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itp;
      FLCampos.Add(FRCampos);
      //
*)
      New(FRCampos);
      FRCampos.Field      := 'TipoArea';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=M2 1=P2
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSGerAPal') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSPaClaIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSPallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VMI_Sorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VMI_Dest';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Box';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Revisor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Digitador';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSPaClaCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSGerArt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_FLD_TAB_VMI;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal01';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal02';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal03';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal04';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal05';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal06';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal07';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal08';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal09';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal10';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal11';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal12';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal13';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal14';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal15';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFimCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSESCCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSPaClaIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSPallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VMI_Sorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VMI_Baix';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VMI_Dest';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tecla';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QIt_Sorc'; // Quantidade de VMI_Sorc (pode ser mais de um!)
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QIt_Baix'; // Quantidade de VMI_Baix (pode ser mais de um!)
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSPaMulCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VMI_Sorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSGerArt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieRem';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeRem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSPaMulIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VMI_Sorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VMI_Dest';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VMI_Baix';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSPaRclCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSGerRcl';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_FLD_TAB_VMI;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSPallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal01';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal02';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal03';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal04';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal05';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal06';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal07';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal08';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal09';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal10';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal11';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal12';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal13';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal14';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LstPal15';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFimCla';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSPaRclBxa') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'VMI_Baix';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VMI_Sorc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VMI_Dest';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSPrePalCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'ide_serie';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itp;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ide_nNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itp;
      FLCampos.Add(FRCampos);
      //
*)
    end else if Uppercase(Tabela) = Uppercase('VSCacIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'CacCod'; // VSCacCab.Codigo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CacID';  // VSCacCab.MovimID
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo'; // VSPaClaCab.Codigo  ou VSPaRclCab.Codigo !!!
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';  // Primary Key
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClaAPalOri'; // VSCacIts[A][B].Controle de Origem para classificacao
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RclAPalOri'; // VSCacIts[A][B].Controle de Origem para reclassificacao
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RclAPalDst'; // ClaAPalOri ou RclAPalOri destinado na reclassificacao
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSPaClaIts';  // VSPaClaIts.Controle
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSPaRclIts'; // VSPaRclIts.Controle
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSPallet'; // VSPallet.Codigo
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VMI_Sorc'; // VSMovIts.Controle
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VMI_Baix'; // VSMovIts.Controle
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VMI_Dest'; // VSMovIts.Controle
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Box';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Martelo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Revisor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Digitador';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sumido';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FrmaIns';  // Forma de insercao
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '3'; // Manual
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SubClass';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorIntSrc';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatorIntDst';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSMorCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Atualizou';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSPMOCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSPedCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Vendedor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataF';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Desco';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      {New(FRCampos);
      FRCampos.Field      := 'ComissV_Per';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ComissV_Val';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);}
      //
      {New(FRCampos);
      FRCampos.Field      := 'DescoExtra';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);}
      //
      {New(FRCampos);
      FRCampos.Field      := 'Volumes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);}
      //
      {New(FRCampos);
      FRCampos.Field      := 'Obs';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);}
      //
      New(FRCampos);
      FRCampos.Field      := 'Obz';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Transp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CondicaoPg';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PedidCli';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntregaEnti';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
    end else if Uppercase(Tabela) = Uppercase('VSPedIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_FLD_TAB_VMI;
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrecoTipo';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrecoMoeda';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrecoVal';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PercDesco';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PercTrib';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Valor';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descn';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entrega';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pronto';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Status';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 2023-04-28
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fluxo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TipoProd';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoPQ';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiRecu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiRefu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiAcab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoWB';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LibPecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LibPesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LibAreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LibAreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FinPecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FinPesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FinAreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FinAreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VdaPecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VdaPesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VdaAreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VdaAreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NaoFinaliza';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSSubPrdCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSReqDiv') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumIni';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumFim';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Responsa';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSReqMov') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumIni';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumFim';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Responsa';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSOpeSeq') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '???';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSOutFat') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Qtde';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);                                  // 0 N/I
      FRCampos.Field      := 'PrecoTipo';             // 1 Pe�as
      FRCampos.Tipo       := 'tinyint(2)';            // 2 Peso kg
      FRCampos.Null       := 'NO';                    // 3 �rea m�
      FRCampos.Key        := '';                      // 4 �rea ft�
      FRCampos.Default    := '0';                     //
      FRCampos.Extra      := '';                      //
      FLCampos.Add(FRCampos);                         //
      //
      New(FRCampos);
      FRCampos.Field      := 'PrecoMoeda';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrecoVal';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PercDesco';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PercTrib';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MdaTotal';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MdaBruto';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MdaLiqui';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cambio';
      FRCampos.Tipo       := 'double(15,10)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NacTotal';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NacBruto';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NacLiqui';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSOutNFeCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      // B07
      New(FRCampos);
      FRCampos.Field      := 'ide_serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'ide_nNF';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcWrn';  // 0-Ok, 16-Falta info, 32-Info incompleto, 48-Info dubia, 64-Info errada
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcObs';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcSeq';
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcSta';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=Cosiderar, 1-Ignorar
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSOutNFeIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ItemNFe';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TpCalcVal';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorU';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TribDefSel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
(*
    end else if Uppercase(Tabela) = Uppercase('VSOutNFI') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NumrNFe';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ItemNFe';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TpCalcVal';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorU';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TribDefSel';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
    end else if Uppercase(Tabela) = Uppercase('VSProQui') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SetorID';
      FRCampos.Tipo       := 'tinyint(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimTwn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';  // Pecas no setor
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';  // kg no setor
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';  // m2 no setor
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusPQ';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fuloes';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MinDta';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MaxDta';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstqPeca';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstqPeso';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EstqArM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSVmcWrn') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSLoadCRCCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OriServrID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSLoadCRCIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // TVSTabelas.vstab....
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Niveis';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CRCNiv01';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ERPNiv01';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CRCNiv02';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ERPNiv02';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CRCNiv03';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ERPNiv03';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CRCNiv04';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ERPNiv04';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CRCNiv05';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ERPNiv05';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSLoadCRCTbs') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Registros';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSLoadCRCWrn') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Linha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSSifDipoa') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Registro';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DdValid';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '30';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArqLogoFilial';
      FRCampos.Tipo       := 'varchar(511)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArqLogoSIF';
      FRCampos.Tipo       := 'varchar(511)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QualDtFab';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=Indef, 1= Min dta VMI, 2= Max dta VMI, 3= dta Pallet
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('WBArtCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fluxo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiRecu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiRefu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReceiAcab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TxtMPs';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observa';
      FRCampos.Tipo       := 'Text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('WBAjsCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_GRL;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClienteMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '-11';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('WBInnCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtCompra';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtViagem';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtEntrada';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fornecedor';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Transporta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorMP';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoComiss';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusFrtAvMoER';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClienteMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Procednc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Motorista';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Placa';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      // B07
      New(FRCampos);
      FRCampos.Field      := 'ide_serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'ide_nNF';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B07
      New(FRCampos);
      FRCampos.Field      := 'emi_serie';
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      // B08
      New(FRCampos);
      FRCampos.Field      := 'emi_nNF';
      FRCampos.Tipo       := 'int(9)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcWrn';  // 0-Ok, 16-Falta info, 32-Info incompleto, 48-Info dubia, 64-Info errada
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcObs';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcSeq';
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcSta';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=Cosiderar, 1-Ignorar
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtEnceRend';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TpEnceRend';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=N�o encerr, 1=Automatico, 2=Manual
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('WBMovCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodigoID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelSncMulTab;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('WBMovIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelSncMulTab;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimNiv';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimTwn';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelSncOrfao;
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Terceiro';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliVenda';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LnkIDXtr';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LnkNivXtr1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpDeprecado;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LnkNivXtr2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpDeprecado;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := CO_DATA_HORA_VMI;
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pallet';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
{     N�o usar! Colocar no Nome!!!
      New(FRCampos);
      FRCampos.Field      := 'DefPeca';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose....;
      FLCampos.Add(FRCampos);
      //
}
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,4)'; // 2023-05-04
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrcMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrcNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelSncMulTab;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrcNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SrcGGX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SdoVrtPeca';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SdoVrtPeso';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SdoVrtArM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Observ';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieFch';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ficha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Misturou';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClientMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '-11';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoPQ';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FornecMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoMOKg';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoMOM2';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoMOTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorMP';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DstMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DstNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelSncMulTab;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DstNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DstGGX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdGerPeca';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdGerPeso';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdGerArM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdGerArP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdAntPeca';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdAntPeso';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdAntArM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdAntArP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AptoUso';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NotaMPAG';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Marca';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TpCalcAuto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';  // O que calculou automatico   -1: nao se sabe 0: Nada 1: Pecas 2: Peso 4: m2 8: p2
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Zerado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  // 0: Nao 1: Sim
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EmFluxo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '1';  // 0: Sim 1: Nao
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NotFluxo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  // 0:  em todos 1: Nao no In Natura
      FRCampos.Extra      := '';   // 2: Nao no Artigo gerado 4: Nao no artigo classif.
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);      // 8: Nao no Artigo em operacao
      //
      New(FRCampos);
      FRCampos.Field      := 'FatNotaVNC';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FatNotaVRC';
      FRCampos.Tipo       := 'double(15,8)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PedItsLib';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PedItsFin';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PedItsVda';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);         // DEPRECADO !!!!
      FRCampos.Field      := 'GSPInnNiv2'; // In Natura de onde tirou o subproduto
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);         // DEPRECADO !!!!
      FRCampos.Field      := 'GSPArtNiv2'; // Artigo Gerado que saiu do In Natura que gerou o produto
      FRCampos.Tipo       := 'int(11)';    // Permitir zero. Pode ter varios !!!
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ReqMovEstq'; // Requisicao de movimentacao de estoque
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'StqCenLoc'; // Local do centro de estoque
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ItemNFe';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSMorCab';  // Arquivo morto!!! ???
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSMulFrnCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'KgCouPQ';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeSer';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeNum';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSMulNFeCab';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GGXRcl'; // GGX usado no lugar do GraGruX para faturar, etc  EFD Bloco K220!
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelSncMulTab;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'JmpGGX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RmsMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RmsNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelSncMulTab;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RmsNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RmsGGX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GSPSrcMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GSPSrcNiv2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GSPJmpMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GSPJmpNiv2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtCorrApo'; // Data de corre��o de apontamento!
      FRCampos.Tipo       := 'datetime';   // Registros K270 / K275 do SPED Fiscal!
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovCodPai';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IxxMovIX';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IxxFolha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IxxLinha';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Iuvpei';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusFrtAvuls';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusFrtMOEnv';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusFrtMORet';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
(*
      //
      New(FRCampos);
      FRCampos.Field      := 'CusFrtTrnsf';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
*)
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoMOPc';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoComiss';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerceComiss';
      FRCampos.Tipo       := 'double(15,10)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CusKgComiss';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CredValrImposto';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CredPereImposto';
      FRCampos.Tipo       := 'double(15,10)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000000000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      //
      New(FRCampos);
      FRCampos.Field      := 'RpICMS';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RpICMSST';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RpPIS';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RpCOFINS';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RvICMS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RvICMSST';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RvPIS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RvCOFINS';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RpIPI';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RvIPI';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Corrigido';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VmiPai';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MedEClas';   // Mede e classifica junto.
      FRCampos.Tipo       := 'tinyint(1)'; // N�o � informadoa �rea na gera��o
      FRCampos.Null       := 'NO';         // do artigo
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrOrErpPrimtiv;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('VSInnRas') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RasMovID';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RasNivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RasNivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RasGGX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('WBMPrCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BRLMedM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('WBMPrFrn') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fornece';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'M2Medio';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      //
    end else if Uppercase(Tabela) = Uppercase('WBPallet') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpAllSrvrIncUniq;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ClientMO';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Status';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliStat'; // Cliente referente ao status
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX'; // Artigo Classificado
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrEndAdd'; // Fim da adi��o de couros (encerramento) em classifica��o
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GerRclCab'; // �ltima Reclassifica��o atrelada
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFimRcl'; // Fim da adi��o de couros (encerramento) em reclassifica��o
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimIDGer'; // MovimID que gerou o Pallet
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'QtdPrevPc'; // Quantidade previsivel de pe�as
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      //
      New(FRCampos);
      FRCampos.Field      := 'StatPall';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '4'; // Encerrado
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SeqSifDipoa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ObsSifDipoa';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataFab';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('WBPalArt') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('WBPalFrn') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fornece';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'M2Medio';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
    end else if Uppercase(Tabela) = Uppercase('WBOutCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRIncremSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpCDRRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtVenda';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtViagem';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtEntrega';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Transporta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SerieV';  // NF Venda
      FRCampos.Tipo       := 'int(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFV';  // NF Venda
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysOrCalcData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NFeStatus';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '100';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcWrn';  // 0-Ok, 16-Falta info, 32-Info incompleto, 48-Info dubia, 64-Info errada
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpERPRelatnSync;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcObs';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcSeq';
      FRCampos.Tipo       := 'varchar(25)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSVmcSta';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 0=Cosiderar, 1-Ignorar
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpSysRelatnPrDf;
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pedido';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FRCampos.Coments    := '';
      FRCampos.Purpose    := TItemTuplePurpose.itpUsrPrimtivData;
      FLCampos.Add(FRCampos);
      //
    end else if Uppercase(Tabela) = Uppercase('WBRclCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MovimCod';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrIni';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtHrFim';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pecas';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PesoKg';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaM2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AreaP2';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ValorT';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TemIMEIMrt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ide_serie';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ide_nNF';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnVS_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos: TCampos;
  FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    //end else
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      //
      New(FRCampos);
      FRCampos.Field      := 'DefeitosLoc';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DefeitosTip';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSPwdDdClas';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSPwdDdData';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSPwdDdUser';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VSImpRandStr';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NObrigNFeVS';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_PDA';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 210/215
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_EmCal';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '3'; // 29x/30x
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_Caleado';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '3'; // 29x/30x
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_DTA';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 210/215
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_EmCur';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '3'; // 29x/30x
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_Curtido';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '3'; // 29x/30x
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_ArtCur';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '3'; // 29x/30x
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_Operacao';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0'; // 210/215
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_Recurt';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '3'; // 29x/30x
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_SubProd';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '3'; // 29x/30x
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SPED_II_Reparo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '4'; // 260/265
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      (* Foi movido para CMPT_Tabs
      New(FRCampos);
      FRCampos.Field      := 'CMPPVai';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPPVaiIts';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CMPPVem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      *)
      //
      New(FRCampos);
      FRCampos.Field      := 'VSImpMediaPall';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';  // Imprime m�dia no pallet
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TUnVS_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  // CAD-BDERM-004 :: Locais de Defeitos
  New(FRJanelas);
  FRJanelas.ID        := 'CAD-BDERM-004';
  FRJanelas.Nome      := 'FmDefeitosloc';
  FRJanelas.Descricao := 'Locais de Defeitos';
  FLJanelas.Add(FRJanelas);
  //
  // COU-MP_IN-010 :: Tipos de Defeitos
  New(FRJanelas);
  FRJanelas.ID        := 'COU-MP_IN-010';
  FRJanelas.Nome      := 'FmDefeitosTip';
  FRJanelas.Descricao := 'Tipos de Defeitos';
  FLJanelas.Add(FRJanelas);
  //
  // COU-MP_IN-011 :: Locais de Defeitos
  New(FRJanelas);
  FRJanelas.ID        := 'COU-MP_IN-011';
  FRJanelas.Nome      := 'FmDefeitosLoc';
  FRJanelas.Descricao := 'Locais de Defeitos';
  FLJanelas.Add(FRJanelas);
  //
  // FLU-OPERA-001 :: Opera��es de Fluxos
  New(FRJanelas);
  FRJanelas.ID        := 'FLU-OPERA-001';
  FRJanelas.Nome      := 'FmOperacoes';
  FRJanelas.Descricao := 'Opera��es de Fluxos';
  FLJanelas.Add(FRJanelas);
  //
  // SIF-DIPOA-001 :: Op��es SIF/DIPOA
  New(FRJanelas);
  FRJanelas.ID        := 'SIF-DIPOA-001';
  FRJanelas.Nome      := 'FmOpcoesVSSifDipoa';
  FRJanelas.Descricao := 'Op��es SIF/DIPOA';
  FLJanelas.Add(FRJanelas);
  //
  // SIF-DIPOA-002 :: Pesquisa N� de Identifica��o do SIF/DIPOA
  New(FRJanelas);
  FRJanelas.ID        := 'SIF-DIPOA-002';
  FRJanelas.Nome      := 'FmVSSifDipoaPesqBag';
  FRJanelas.Descricao := 'Pesquisa N� de Identifica��o do SIF/DIPOA';
  FLJanelas.Add(FRJanelas);
  //
  // SIF-DIPOA-003 :: Impress�es SIF/DIPOA
  New(FRJanelas);
  FRJanelas.ID        := 'SIF-DIPOA-003';
  FRJanelas.Nome      := 'FmVSSifDipoaImp';
  FRJanelas.Descricao := 'Impress�es SIF/DIPOA';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-001 :: Configura��o de Mat�rias-primas In Natura
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-001';
  FRJanelas.Nome      := 'FmVSNatCad';
  FRJanelas.Descricao := 'Configura��o de Mat�rias-primas In Natura';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-002 :: Configura��o de Artigos de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-002';
  FRJanelas.Nome      := 'FmVSRibCad';
  FRJanelas.Descricao := 'Configura��o de Artigos de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-003 :: Mat�ria-prima In Natura X Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-003';
  FRJanelas.Nome      := 'FmVSNatArt';
  FRJanelas.Descricao := 'Mat�ria-prima In Natura X Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-004 :: Configura��o de Artigos de Ribeira Classificados
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-004';
  FRJanelas.Nome      := 'FmVSRibCla';
  FRJanelas.Descricao := 'Configura��o de Artigos de Ribeira Classificados';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-005 :: Artigo de Ribeira X Artigo de Ribeira Classificado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-005';
  FRJanelas.Nome      := 'FmVSRibArt';
  FRJanelas.Descricao := 'Artigo de Ribeira X Artigo de Ribeira Classificado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-006 :: Entrada de Peles In Natura
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-006';
  FRJanelas.Nome      := 'FmVSInnCab';
  FRJanelas.Descricao := 'Entrada de Peles In Natura';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-007 :: Item de Entrada de Peles In Natura
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-007';
  FRJanelas.Nome      := 'FmVSInnIts';
  FRJanelas.Descricao := 'Item de Entrada de Peles In Natura';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-008 :: Gera��o de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-008';
  FRJanelas.Nome      := 'FmVSGerArtCab';
  FRJanelas.Descricao := 'Gera��o de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-009 :: Item de Gera��o de Artigo de Ribeira da Barraca
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-009';
  FRJanelas.Nome      := 'FmVSGerArtItsBar';
  FRJanelas.Descricao := 'Item de Gera��o de Artigo de Ribeira da Barraca';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-010 :: Encerramento de Gera��o de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-010';
  FRJanelas.Nome      := 'FmVSGerArtEnc';
  FRJanelas.Descricao := 'Libera��o de Artigo de Ribeira para Classificar';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-011 :: Configura��o de Classifica��o de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-011';
  FRJanelas.Nome      := 'FmVSClaArtPrp';
  FRJanelas.Descricao := 'Configura��o de Classifica��o de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-012 :: Classifica��o de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-012';
  FRJanelas.Nome      := 'FmVSClaArtOne';
  FRJanelas.Descricao := 'Classifica��o de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-013 :: Pallet de Artigo de Ribeira Classificado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-013';
  FRJanelas.Nome      := 'FmVSPallet';
  FRJanelas.Descricao := 'Pallet de Artigo de Ribeira Classificado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-014 :: Cria��o de Pallet de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-014';
  FRJanelas.Nome      := 'FmVSPalletAdd';
  FRJanelas.Descricao := 'Cria��o de Pallet de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-015 :: Classifica��o de Artigo de Ribeira Couro a Couro
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-015';
  FRJanelas.Nome      := 'FmVSClassifOne';
  FRJanelas.Descricao := 'Classifica��o de Artigo de Ribeira Couro a Couro';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-016 :: Continua��o de Classifica��o de Artigo de Ribeira (IMEI)
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-016';
  FRJanelas.Nome      := 'FmVSClaArtSelA';
  FRJanelas.Descricao := 'Continua��o de Classifica��o de Artigo de Ribeira (IMEI)';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-017 :: Adi��o de Pallet em Classifica��o de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-017';
  FRJanelas.Nome      := 'FmVSClaArtPalAdd';
  FRJanelas.Descricao := 'Adi��o de Pallet em Classifica��o de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-018 :: Relat�rios de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-018';
  FRJanelas.Nome      := 'FmVSMovImp';
  FRJanelas.Descricao := 'Relat�rios de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-019 :: Sa�da de Wet Blue do Estoque
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-019';
  FRJanelas.Nome      := 'FmVSOutCab';
  FRJanelas.Descricao := 'Sa�da de Wet Blue do Estoque';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-020 :: Item de Venda de Wet Blue Com Rastreio
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-020';
  FRJanelas.Nome      := 'FmVSOutKno';
  FRJanelas.Descricao := 'Item de Venda de Wet Blue Com Rastreio';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-021 :: Item de Venda de Wet Blue Sem Rastreio
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-021';
  FRJanelas.Nome      := 'FmVSOutUnk';
  FRJanelas.Descricao := 'Item de Venda de Wet Blue Sem Rastreio';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-022 :: Item de Baixa For�ada de Couros na Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-022';
  FRJanelas.Nome      := 'FmVSOutIts';
  FRJanelas.Descricao := 'Item de Baixa For�ada de Couros na Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-023 :: Ajuste de Estoque de Couros na Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-023';
  FRJanelas.Nome      := 'FmVSAjsCab';
  FRJanelas.Descricao := 'Ajuste de Estoque de Couros na Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-024 :: Item de Ajuste de Estoque de Couros Na Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-024';
  FRJanelas.Nome      := 'FmVSAjsIts';
  FRJanelas.Descricao := 'Item de Ajuste de Estoque de Couros Na Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-025 :: Impress�o de IME-I
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-025';
  FRJanelas.Nome      := 'FmVSImpIMEI';
  FRJanelas.Descricao := 'Impress�o de IME-I';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-026 :: Impress�o de Pallet VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-026';
  FRJanelas.Nome      := 'FmVSImpPallet';
  FRJanelas.Descricao := 'Impress�o de Pallet de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-027 :: Baixa For�ada de Couro na Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-027';
  FRJanelas.Nome      := 'FmVSBxaCab';
  FRJanelas.Descricao := 'Baixa For�ada de Couro na Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-028 :: Item de Baixa For�ada de Couro na Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-028';
  FRJanelas.Nome      := 'FmVSBxaIts';
  FRJanelas.Descricao := 'Item de Baixa For�ada de Couro na Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-029 :: Reclassifica��o de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-029';
  FRJanelas.Nome      := 'FmVSRclCab';
  FRJanelas.Descricao := 'Reclassifica��o de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-030 :: Reclassifica��o de Artigo de Ribeira Couro a Couro
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-030';
  FRJanelas.Nome      := 'FmVSReclassifOne';
  FRJanelas.Descricao := 'Reclassifica��o de Artigo de Ribeira Couro a Couro';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-031 :: Configura��o de Reclassifica��o de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-031';
  FRJanelas.Nome      := 'FmVSReclaArtPrp';
  FRJanelas.Descricao := 'Configura��o de Reclassifica��o de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-032 :: Continua��o de Reclassifica��o de Artigo de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-032';
  FRJanelas.Nome      := 'FmVSRclArtSel';
  FRJanelas.Descricao := 'Continua��o de Reclassifica��o de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
(*  Nao efetivado!!! Usado o WET-CURTI-044
  // WET-CURTI-033 :: Habilita��o de Pallet para Classifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-033';
  FRJanelas.Nome      := 'FmVSGerRclCab';
  FRJanelas.Descricao := 'Habilita��o de Pallet para Classifica��o';
  FLJanelas.Add(FRJanelas);
*)
  //
  // WET-CURTI-034 :: Impress�o de Packing List de Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-034';
  FRJanelas.Nome      := 'FmVSImpPackList';
  FRJanelas.Descricao := 'Impress�o de Packing List de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-035 :: Item de Venda de Wet Blue por Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-035';
  FRJanelas.Nome      := 'FmVSOutPal';
  FRJanelas.Descricao := 'Item de Venda de Wet Blue por Pallet';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-036 :: Classes Geradas - IMEI
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-036';
  FRJanelas.Nome      := 'FmVSImpClaIMEI';
  FRJanelas.Descricao := 'Classes Geradas - IMEI';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-037 :: Pesquisa de Ficha RMP
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-037';
  FRJanelas.Nome      := 'FmVSRMPPsq';
  FRJanelas.Descricao := 'Pesquisa de Ficha RMP';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-038 :: Artigo de Ribeira em Opera��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-038';
  FRJanelas.Nome      := 'FmVSOpeCab';
  FRJanelas.Descricao := 'Artigo de Ribeira em Opera��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-039 :: Configura��o de Artigos de Ribeira Em Opera��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-039';
  FRJanelas.Nome      := 'FmVSRibOpe';
  FRJanelas.Descricao := 'Configura��o de Artigos de Ribeira Em Opera��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-040 :: Origem de Artigo de Ribeira Em Opera��o (Pallet)
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-040';
  FRJanelas.Nome      := 'FmVSOpeOriPall';
  FRJanelas.Descricao := 'Origem de Artigo de Ribeira Em Opera��o (Pallet)';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-041 :: Destino de Artigo de Ribeira Em Opera��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-041';
  FRJanelas.Nome      := 'FmVSOpeDst';
  FRJanelas.Descricao := 'Destino de Artigo de Ribeira Em Opera��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-042 :: Defini��o de Produto de Artigo VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-042';
  FRJanelas.Nome      := 'FmVSSubProd';
  FRJanelas.Descricao := 'Defini��o de Produto de Artigo VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-043 :: Continua��o de Classifica��o de Artigo de Ribeira (RMP)
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-043';
  FRJanelas.Nome      := 'FmVSClaArtSelB';
  FRJanelas.Descricao := 'Continua��o de Classifica��o de Artigo de Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-044 :: Gerenciamento de Reclassifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-044';
  FRJanelas.Nome      := 'FmVSGerRclCab';  // A e B ???
  FRJanelas.Descricao := 'Gerenciamento de Reclassifica��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-045 :: Gerenciamento de IME-Is
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-045';
  FRJanelas.Nome      := 'FmVSMovIts';  // A e B ???
  FRJanelas.Descricao := 'Gerenciamento de IME-Is';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-046 :: Gerenciamento de Classifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-046';
  FRJanelas.Nome      := 'FmVSGerClaCab';  // A e B ???
  FRJanelas.Descricao := 'Gerenciamento de Classifica��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-047 :: Impress�o de N�mero de Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-047';
  FRJanelas.Nome      := 'FmVSPalNumImp';
  FRJanelas.Descricao := 'Impress�o de N�mero de Pallet';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-048 :: Reclasses Geradas
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-048';
  FRJanelas.Nome      := 'FmVSImpRecla';
  FRJanelas.Descricao := 'Reclasses Geradas';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-049 :: Gerenciamento de Desnate
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-049';
  FRJanelas.Nome      := 'FmVSDsnCab';
  FRJanelas.Descricao := 'Gerenciamento de Desnate';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-050 :: Gerenciamento de Desnate - Artigos
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-050';
  FRJanelas.Nome      := 'FmVSDsnArt';
  FRJanelas.Descricao := 'Gerenciamento de Desnate - Artigos';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-051 :: Gerenciamento de Desnate - IME-Cs
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-051';
  FRJanelas.Nome      := 'FmVSDsnIts';
  FRJanelas.Descricao := 'Gerenciamento de Desnate - IME-Cs';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-052 :: Gerenciamento de Boxes de Classifica��o / Reclassifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-052';
  FRJanelas.Nome      := 'FmVSPaCRIts';
  FRJanelas.Descricao := 'Gerenciamento de Boxes de Classifica��o / Reclassifica��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-053 :: Gerenciamento de Classifica��es / Reclassifica��es
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-053';
  FRJanelas.Nome      := 'FmVSPaCRCab';
  FRJanelas.Descricao := 'Gerenciamento de Classifica��es / Reclassifica��es';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-054 :: Origem de Artigo de Ribeira Em Opera��o (IME-I)
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-054';
  FRJanelas.Nome      := 'FmVSOpeOriIMEI';
  FRJanelas.Descricao := 'Origem de Artigo de Ribeira Em Opera��o (IME-I)';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-055 :: Gerenciamento de Desnate - Sub Classe
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-055';
  FRJanelas.Nome      := 'FmVSDsnSub';
  FRJanelas.Descricao := 'Gerenciamento de Desnate - Sub Classe';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-056 :: Altera��o Manual de IME-I
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-056';
  FRJanelas.Nome      := 'FmVSMovItsAlt';
  FRJanelas.Descricao := 'Altera��o Manual de IME-I';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-057 :: Gerenciamento de Fichas
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-057';
  FRJanelas.Nome      := 'FmVSFchGerCab';
  FRJanelas.Descricao := 'Gerenciamento de Fichas';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-058 :: Resultado de Fichas
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-058';
  FRJanelas.Nome      := 'FmVSFchRslCab';
  FRJanelas.Descricao := 'Resultado de Fichas';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-059 :: Item de Resultado de Ficha
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-059';
  FRJanelas.Nome      := 'FmVSFchRslIts';
  FRJanelas.Descricao := 'Item de Resultado de Ficha';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-060 :: Classifica��o de Artigo de Ribeira - M�ltiplo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-060';
  FRJanelas.Nome      := 'FmVSPaMulCab';
  FRJanelas.Descricao := 'Classifica��o de Artigo de Ribeira - M�ltiplo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-061 :: Item de Classifica��o de Artigo de Ribeira - M�ltiplo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-061';
  FRJanelas.Nome      := 'FmVSPaMulIts';
  FRJanelas.Descricao := 'Item de Classifica��o de Artigo de Ribeira - M�ltiplo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-062 :: Pallet em Classifica��o / Reclassifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-062';
  FRJanelas.Nome      := 'FmVSPalInClaRcl';
  FRJanelas.Descricao := 'Pallet em Classifica��o / Reclassifica��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-063 :: Prepara��o de Pallet para Reclassifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-063';
  FRJanelas.Nome      := 'FmVSReclassPrePal';
  FRJanelas.Descricao := 'Prepara��o de Pallet para Reclassifica��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-064 :: Configura��o de Classifica��o de Artigo de Ribeira (IME-I)
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-064';
  FRJanelas.Nome      := 'FmVSRclArtPrpNew';
  FRJanelas.Descricao := 'Configura��o de Classifica��o de Artigo de Ribeira (IME-I)';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-065 :: Hist�rico de Ficha RMP
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-065';
  FRJanelas.Nome      := 'FmVSHisFchAdd';
  FRJanelas.Descricao := 'Hist�rico de Ficha RMP';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-066 :: Gerenciamento de OCs
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-066';
  FRJanelas.Nome      := 'FmVSOSGerCab';
  FRJanelas.Descricao := 'Gerenciamento de OCs';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-067 :: Gerenciamento de Equ�lize
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-067';
  FRJanelas.Nome      := 'FmVSEqzCab';
  FRJanelas.Descricao := 'Gerenciamento de Equ�lize';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-068 :: Gerenciamento de Equ�lize - Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-068';
  FRJanelas.Nome      := 'FmVSEqzIts';
  FRJanelas.Descricao := 'Gerenciamento de Equ�lize - Pallet';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-069 :: Gerenciamento de Equ�lize - Sub Classe
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-069';
  FRJanelas.Nome      := 'FmVSEqzSub';
  FRJanelas.Descricao := 'Gerenciamento de Equ�lize - Sub Classe';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-070 :: Classes Geradas - Ficha RMP
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-070';
  FRJanelas.Nome      := 'FmVSImpClaFichaRMP';
  FRJanelas.Descricao := 'Classes Geradas - Ficha RMP';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-071 :: Configura��o de equ�lize
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-071';
  FRJanelas.Nome      := 'FmVSCfgEqzCb';
  FRJanelas.Descricao := 'Configura��o de equ�lize';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-072 :: Item de Configura��o de equ�lize
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-072';
  FRJanelas.Nome      := 'FmVSCfgEqzIt';
  FRJanelas.Descricao := 'Item de Configura��o de equ�lize';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-073 :: Troca de Ficha em Classifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-073';
  FRJanelas.Nome      := 'FmVSClassifOneRetFichaRMP';
  FRJanelas.Descricao := 'Troca de Ficha em Classifica��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-074 :: Troca de IMEI em Classifica��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-074';
  FRJanelas.Nome      := 'FmVSClassifOneRetIMEI';
  FRJanelas.Descricao := 'Troca de IMEI em Classifica��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-075 :: Baixas For�adas de IMEIs Completos
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-075';
  FRJanelas.Nome      := 'FmVSBxaItsIMEIs';
  FRJanelas.Descricao := 'Baixas For�adas de IMEIs Completos';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-076 :: Entrada de Artigo Classificado Por Compra
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-076';
  FRJanelas.Nome      := 'FmVSPlCCab';
  FRJanelas.Descricao := 'Entrada de Artigo Classificado Por Compra';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-077 :: Item de Entrada de Artigo Classificado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-077';
  FRJanelas.Nome      := 'FmVSPlCIts';
  FRJanelas.Descricao := 'Item de Entrada de Artigo Classificado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-078 :: Baixa Extra de Couro na Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-078';
  FRJanelas.Nome      := 'FmVSExBCab';
  FRJanelas.Descricao := 'Baixa Extra de Couro na Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-079 :: Item de Baixa Extra de Couro na Ribeira
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-079';
  FRJanelas.Nome      := 'FmVSExBIts';
  FRJanelas.Descricao := 'Item de Baixa Extra de Couro na Ribeira';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-080 :: Baixas Extras de IMEIs Completos
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-080';
  FRJanelas.Nome      := 'FmVSExBItsIMEIs';
  FRJanelas.Descricao := 'Baixas Extras de IMEIs Completos';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-081 :: Impress�o de Fluxo de Couros Inteiros VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-081';
  FRJanelas.Nome      := 'FmVSImpFluxo';
  FRJanelas.Descricao := 'Impress�o de Fluxo de Couros Inteiros VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-082 :: Configura��o de Fornecedore s de MP-VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-082';
  FRJanelas.Nome      := 'FmVSEntiMP';
  FRJanelas.Descricao := 'Configura��o de Fornecedores de MP-VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-083 :: Invent�rio de Couros VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-083';
  FRJanelas.Nome      := 'FmVSBalCab';
  FRJanelas.Descricao := 'Invent�rio de couros VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-084 :: Adi��o de Empresa a Invent�rio de Couros VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-084';
  FRJanelas.Nome      := 'FmVSBalEmp';
  FRJanelas.Descricao := 'Adi��o de Empresa a Invent�rio de Couros VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-085 :: Novo Invent�rio de Couros VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-085';
  FRJanelas.Nome      := 'FmVSBalCabNew';
  FRJanelas.Descricao := 'Novo Invent�rio de Couros VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-086 :: Impress�o de Diferen�as de Couros VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-086';
  FRJanelas.Nome      := 'FmVSImpDif';
  FRJanelas.Descricao := 'Impress�o de Diferen�as de Couros VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-087 :: Classes Geradas de IME-Is VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-087';
  FRJanelas.Nome      := 'FmVSCGICab';
  FRJanelas.Descricao := 'Classes Geradas de IME-Is VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-088 :: Sele��o de IME-Is VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-088';
  FRJanelas.Nome      := 'FmVSCGIIts';
  FRJanelas.Descricao := 'Sele��o de IME-Is VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-089 :: Configura��o de Artigos Semi Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-089';
  FRJanelas.Nome      := 'FmVSWetEnd';
  FRJanelas.Descricao := 'Configura��o de Artigos Semi Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-090 :: Artigo Classificado X Artigos Semi Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-090';
  FRJanelas.Nome      := 'FmVSClaWet';
  FRJanelas.Descricao := 'Artigo Classificado X Artigos Semi Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-091 :: Pedido de Venda
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-091';
  FRJanelas.Nome      := 'FmVSPedCab';
  FRJanelas.Descricao := 'Pedido de Venda';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-092 :: Item de Pedido de Venda
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-092';
  FRJanelas.Nome      := 'FmVSPedIts';
  FRJanelas.Descricao := 'Item de Pedido de Venda';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-093 :: Processo de Semi Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-093';
  FRJanelas.Nome      := 'FmVSPWECab';
  FRJanelas.Descricao := 'Processo de Semi Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-094 :: Origem de Artigo de Semi Acabado em Processo (Pallet)
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-094';
  FRJanelas.Nome      := 'FmVSPWEOriPall';
  FRJanelas.Descricao := 'Origem de Artigo de Semi Acabado em Processo (Pallet)';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-095 :: Origem de Artigo de Semi Acabado em Processo (IME-I)
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-095';
  FRJanelas.Nome      := 'FmVSPWEOriIMEI';
  FRJanelas.Descricao := 'Origem de Artigo de Semi Acabado em Processo (IME-I)';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-096 :: Destino de Artigo de Semi Acabado em Processo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-096';
  FRJanelas.Nome      := 'FmVSPWEDst';
  FRJanelas.Descricao := 'Destino de Artigo de Semi Acabado em Processo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-097 :: Cria��o de Pallet de Artigo de Semi Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-097';
  FRJanelas.Nome      := 'FmVSPalletPWEAdd';
  FRJanelas.Descricao := 'Cria��o de Pallet de Artigo de Semi Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-098 :: Configura��o de Artigo Acabado Classificado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-098';
  FRJanelas.Nome      := 'FmVSFinCla';
  FRJanelas.Descricao := 'Configura��o de Artigo Acabado Classificado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-099 :: Artigo VS sem Tipo de Material Definido
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-099';
  FRJanelas.Nome      := 'FmVSImpMatNoDef';
  FRJanelas.Descricao := 'Artigo VS sem Tipo de Material Definido';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-100 :: Impress�o de Movimento de Compra e Venda
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-100';
  FRJanelas.Nome      := 'FmVSImpMatNoDef';
  FRJanelas.Descricao := 'Impress�o de Movimento de Compra e Venda';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-101 :: Divis�o ao Meio de Couro VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-101';
  FRJanelas.Nome      := 'FmVSDivCouMeio';
  FRJanelas.Descricao := 'Divis�o ao Meio de Couros VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-102 :: Pesquisa Sequ�ncia de Pe�as
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-102';
  FRJanelas.Nome      := 'FmVSPesqSeqPeca';
  FRJanelas.Descricao := 'Pesquisa Sequ�ncia de Pe�as';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-103 :: Altera��o de Quantidades de Artigo Gerado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-103';
  FRJanelas.Nome      := 'FmVSGerArtAltQtd';
  FRJanelas.Descricao := 'Altera��o de Quantidades de Artigo Gerado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-104 :: Altera��o de In Natura de Artigo Gerado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-104';
  FRJanelas.Nome      := 'FmVSGerArtAltInN';
  FRJanelas.Descricao := 'Altera��o de In Natura de Artigo Gerado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-105 :: �rvore de Artigos
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-105';
  FRJanelas.Nome      := 'FmVSArvoreArtigos';
  FRJanelas.Descricao := '�rvore de Artigos';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-106 :: Entrada de Artigo Por Devolu��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-106';
  FRJanelas.Nome      := 'FmVSDvlCab';
  FRJanelas.Descricao := 'Entrada de Artigo Por Devolu��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-107 :: Item de Entrada de Artigo Por Devolu��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-107';
  FRJanelas.Nome      := 'FmVSDvlIts';
  FRJanelas.Descricao := 'Item de Entrada de Artigo Por Devolu��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-108 :: Entrada de Artigo Vendido Para Retrabalho
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-108';
  FRJanelas.Nome      := 'FmVSRtbCab';
  FRJanelas.Descricao := 'Entrada de Artigo Vendido Para Retrabalho';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-109 :: Item de Entrada de Artigo Vendido Para Retrabalho
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-109';
  FRJanelas.Nome      := 'FmVSRtbIts';
  FRJanelas.Descricao := 'Item de Entrada de Artigo Vendido Para Retrabalho';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-110 :: Configura��o de Subproduto In Natura
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-110';
  FRJanelas.Nome      := 'FmVSSubPrd';
  FRJanelas.Descricao := 'Configura��o de Subproduto In Natura';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-111 :: Item de Sub Produto
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-111';
  FRJanelas.Nome      := 'FmVSInnSubPrdIts';
  FRJanelas.Descricao := 'Item de Sub Produto';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-112 :: Item de Venda por Peso
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-112';
  FRJanelas.Nome      := 'FmVSOutPeso';
  FRJanelas.Descricao := 'Item de Venda por Peso';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-113 :: Datas de Gera��o de Artigo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-113';
  FRJanelas.Nome      := 'FmVSGerArtDatas';
  FRJanelas.Descricao := 'Datas de Gera��o de Artigo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-114 :: Relassifica��o de Artigo de Ribeira - M�ltiplo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-114';
  FRJanelas.Nome      := 'FmVSPaMulCabR';
  FRJanelas.Descricao := 'Reclassifica��o de Artigo de Ribeira - M�ltiplo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-115 :: Item de Reclassifica��o de Artigo de Ribeira - M�ltiplo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-115';
  FRJanelas.Nome      := 'FmVSPaMulItsR';
  FRJanelas.Descricao := 'Item de Reclassifica��o de Artigo de Ribeira - M�ltiplo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-116 :: Informe de N�mero
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-116';
  FRJanelas.Nome      := 'FmVSReqMovEstq';
  FRJanelas.Descricao := 'Informe de N�mero';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-117 :: RME - Requisi��o de Movimenta��o de Estoque
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-117';
  FRJanelas.Nome      := 'FmVSReqMov';
  FRJanelas.Descricao := 'RME - Requisi��o de Movimenta��o de Estoque';
  FLJanelas.Add(FRJanelas);
  //
(*
  // WET-CURTI-118 :: Sa�da de Couro - Item de NFe
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-118';
  FRJanelas.Nome      := 'FmVSOutNFI';
  FRJanelas.Descricao := 'Sa�da de Couro - Item de NFe';
  FLJanelas.Add(FRJanelas);
  //
*)
  // WET-CURTI-119 :: Adi��o ao Arquivo Morto VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-119';
  FRJanelas.Nome      := 'FmVSMovItbAdd';
  FRJanelas.Descricao := 'Adi��o ao Arquivo Morto VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-120 :: Impress�o de Hist�rico
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-120';
  FRJanelas.Nome      := 'FmVSImpHistorico';
  FRJanelas.Descricao := 'Impress�o de Hist�rico';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-121 :: Impress�o de Nota MPAG
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-121';
  FRJanelas.Nome      := 'FmVSImpNotaMPAG';
  FRJanelas.Descricao := 'Impress�o de Nota MPAG';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-122 :: Impress�o de Pesquisa por Martelo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-122';
  FRJanelas.Nome      := 'FmVSImpMartelo';
  FRJanelas.Descricao := 'Impress�o de Pesquisa por Martelo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-123 :: Impress�o de Estoque VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-123';
  FRJanelas.Nome      := 'FmVSImpEstoque';
  FRJanelas.Descricao := 'Impress�o de Estoque VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-124 :: Mistura de Fornecedores
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-124';
  FRJanelas.Nome      := 'FmVSMulFrnCab';
  FRJanelas.Descricao := 'Mistura de Fornecedores';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-125 :: Item de Mistura de Fornecedores
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-125';
  FRJanelas.Nome      := 'FmVSMulFrnIts';
  FRJanelas.Descricao := 'Item de Mistura de Fornecedores';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-126 :: Gerenciamento de Pr� Reclasse
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-126';
  FRJanelas.Nome      := 'FmVSPrePalCab';
  FRJanelas.Descricao := 'Gerenciamento de Pr� Reclasse';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-127 :: Corrige IME-Is sem Fornecedor
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-127';
  FRJanelas.Nome      := 'FmVSCorrigeMulFrn';
  FRJanelas.Descricao := 'Corrige IME-Is sem Fornecedor';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-128 :: Corrige IME-Is G�meos Orf�os
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-128';
  FRJanelas.Nome      := 'FmVSCorrigeMovimTwn';
  FRJanelas.Descricao := 'Corrige IME-Is G�meos Orf�os';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-129 :: Estoque VS Em...
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-129';
  FRJanelas.Nome      := 'FmVSImpEstqEm';
  FRJanelas.Descricao := 'Estoque VS Em...';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-130 :: Hist�rico de Pallets
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-130';
  FRJanelas.Nome      := 'FmVSImpHistPall';
  FRJanelas.Descricao := 'Hist�rico de Pallets';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-131 :: Transfer�ncia de Local de Estoque VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-131';
  FRJanelas.Nome      := 'FmVSTrfLocCab';
  FRJanelas.Descricao := 'Transfer�ncia de Local de Estoque VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-132 :: Transfer�ncia de Local de Estoque VS - por IMEI
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-132';
  FRJanelas.Nome      := 'FmVSTrfLocIMEI';
  FRJanelas.Descricao := 'Transfer�ncia de Local de Estoque VS - por IMEI';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-133 :: Transfer�ncia de Local de Estoque VS - por Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-133';
  FRJanelas.Nome      := 'FmVSTrfLocPal';
  FRJanelas.Descricao := 'Transfer�ncia de Local de Estoque VS - por Pallet';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-134 :: S�rie/N�mero de NFe - VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-134';
  FRJanelas.Nome      := 'FmVSOutNfeCab';
  FRJanelas.Descricao := 'S�rie/N�mero de NFe - VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-135 :: Item de NFe - VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-135';
  FRJanelas.Nome      := 'FmVSOutNfeIts';
  FRJanelas.Descricao := 'Item de NFe - VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-136 :: IMEIS sem Refer�ncia de Origem
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-136';
  FRJanelas.Nome      := 'FmVSCorrigeSN2Orfao';
  FRJanelas.Descricao := 'IMEIS sem Refer�ncia de Origem';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-137 :: Adi��o de IME-I em Pesagem
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-137';
  FRJanelas.Nome      := 'FmVSEmitCus';
  FRJanelas.Descricao := 'Adi��o de IME-I em Pesagem';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-138 :: Resultados de V�rias Fichas RMP
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-138';
  FRJanelas.Nome      := 'FmVSImpClaMulRMP';
  FRJanelas.Descricao := 'Resultados de V�rias Fichas RMP';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-139 :: Senha VS do Dia
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-139';
  FRJanelas.Nome      := 'FmVSPwdDd';
  FRJanelas.Descricao := 'Senha VS do Dia';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-140 :: Configura��o de Couro em Caleiro
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-140';
  FRJanelas.Nome      := 'FmVSProCal';
  FRJanelas.Descricao := 'Configura��o de Couro em Caleiro';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-141 :: Configura��o de Couro em Curtimento
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-141';
  FRJanelas.Nome      := 'FmVSProCur';
  FRJanelas.Descricao := 'Configura��o de Couro em Curtimento';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-142 :: Artigo em Processo de Caleiro
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-142';
  FRJanelas.Nome      := 'FmVSCalCab';
  FRJanelas.Descricao := 'Artigo em Processo de Caleiro';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-143 :: Artigo Commodity Gerado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-143';
  FRJanelas.Nome      := 'FmArtGeComodty';
  FRJanelas.Descricao := 'Artigo Commodity Gerado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-144 :: Destino de Artigo em Processo de Caleiro - M�ltiplo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-144';
  FRJanelas.Nome      := 'FmVSCalDstMul';
  FRJanelas.Descricao := 'Destino de Artigo em Processo de Caleiro - M�ltiplo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-145 :: Artigo em Processo de Curtimento
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-145';
  FRJanelas.Nome      := 'FmVSCurCab';
  FRJanelas.Descricao := 'Artigo em Processo de Curtimento';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-146 :: Origem de Artigo em Processo de Curtimento
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-146';
  FRJanelas.Nome      := 'FmVSCurOriIMEI';
  FRJanelas.Descricao := 'Origem de Artigo em Processo de Curtimento';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-147 :: Destino de Artigo em Processo de Curtimento
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-147';
  FRJanelas.Nome      := 'FmVSCurDst';
  FRJanelas.Descricao := 'Destino de Artigo em Processo de Curtimento';
  FLJanelas.Add(FRJanelas);
  //
  {
  // WET-CURTI-148 :: Item de Sub Produto (Origem M�ltipla)
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-148';
  FRJanelas.Nome      := 'FmVSInnSubPrdItsMul';
  FRJanelas.Descricao := 'Item de Sub Produto (Origem M�ltipla)';
  FLJanelas.Add(FRJanelas);
  //
  }
  // WET-CURTI-149 :: Item de Gera��o de Artigo de Ribeira do Curtimento - Uni
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-149';
  FRJanelas.Nome      := 'FmVSGerArtItsCurUni';
  FRJanelas.Descricao := 'Item de Gera��o de Artigo de Ribeira do Curtimento - Uni';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-150 :: RDC - Requisi��o de Divis�o de Couro
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-150';
  FRJanelas.Nome      := 'FmVSReqDiv';
  FRJanelas.Descricao := 'RDC - Requisi��o de Divis�o de Couro';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-151 :: Impress�o de Couros em Processo BH
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-151';
  FRJanelas.Nome      := 'FmVSImpEmProcBH';
  FRJanelas.Descricao := 'Impress�o de Couros em Processo BH';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-152 :: Item de Gera��o de Artigo de Ribeira do Curtimento - Mul
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-152';
  FRJanelas.Nome      := 'FmVSGerArtItsCurMul';
  FRJanelas.Descricao := 'Item de Gera��o de Artigo de Ribeira do Curtimento - Mul';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-153 :: Impress�o de Rendimento de Semi e Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-153';
  FRJanelas.Nome      := 'FmVSImpRendPWE';
  FRJanelas.Descricao := 'Impress�o de Rendimento de Semi e Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-154 :: Grupos de Artigos VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-154';
  FRJanelas.Nome      := 'FmVSGruGGX';
  FRJanelas.Descricao := 'Grupos de Artigos VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-155 :: Configura��o de Opera��es e Processos
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-155';
  FRJanelas.Nome      := 'FmVSCOPCab';
  FRJanelas.Descricao := 'Configura��o de Opera��es e Processos';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-156 :: Itens de Configura��o de Opera��es e Processos
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-156';
  FRJanelas.Nome      := 'FmVSCOPIts';
  FRJanelas.Descricao := 'Itens de Configura��o de Opera��es e Processos';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-157 :: Pre�os de M�o-de-Obra
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-157';
  FRJanelas.Nome      := 'FmVSPMOCab';
  FRJanelas.Descricao := 'Pre�os de M�o-de-Obra';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-158 :: Configura��o de Couro Caleirado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-158';
  FRJanelas.Nome      := 'FmVSCouCal';
  FRJanelas.Descricao := 'Configura��o de Couro Caleirado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-159 :: Configura��o de Couro Curtido
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-159';
  FRJanelas.Nome      := 'FmVSCouCur';
  FRJanelas.Descricao := 'Configura��o de Couro Curtido';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-160 :: IME-Cs
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-160';
  FRJanelas.Nome      := 'FmVSMovCab';
  FRJanelas.Descricao := 'IME-Cs';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-161 :: Desclassifica��o de Mat�ria-prima em Processo de Semi
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-161';
  FRJanelas.Nome      := 'FmVSPWEDescl';
  FRJanelas.Descricao := 'Desclassifica��o de Mat�ria-prima em Processo de Semi';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-162 :: Impress�o de Campara��o de Pallets
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-162';
  FRJanelas.Nome      := 'FmVSComparaCacIts';
  FRJanelas.Descricao := 'Impress�o de Campara��o de Pallets';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-163 :: Confer�ncia de Movimento de Couro
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-163';
  FRJanelas.Nome      := 'FmVSCfgMovEFD';
  FRJanelas.Descricao := 'Confer�ncia de Movimento de Couro';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-164 :: Gera��o de Raspa WB em Processo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-164';
  FRJanelas.Nome      := 'FmVSPWESubPrd';
  FRJanelas.Descricao := 'Gera��o de Raspa WB em Processo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-165 :: Gerenciamento de MO de Semi Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-165';
  FRJanelas.Nome      := 'FmVSMOPWEGer';
  FRJanelas.Descricao := 'Gerenciamento de MO de Semi Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-166 :: Altera��o de IME-I de Sa�da
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-166';
  FRJanelas.Nome      := 'FmVSOutAltVMI';
  FRJanelas.Descricao := 'Altera��o de IME-I de Sa�da';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-167 :: Edi��o de Controle de Cobran�a de MO
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-167';
  FRJanelas.Nome      := 'FmVSMOEnvRet';
  FRJanelas.Descricao := 'Edi��o de Controle de Cobran�a de MO';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-168 :: Relat�rio de Controle de Cobran�a de MO
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-168';
  FRJanelas.Nome      := 'FmVSImpMOEnvRet';
  FRJanelas.Descricao := 'Relat�rio de Controle de Cobran�a de MO';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-169 :: NFe VS Outros ou N�o Autorizada
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-169';
  FRJanelas.Nome      := 'FmVSInvNFe';
  FRJanelas.Descricao := 'NFe VS Outros ou N�o Autorizada';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-170 :: Lista Resultado VS - do Prestador da M.O.
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-170';
  FRJanelas.Nome      := 'FmVSImpResultVS';
  FRJanelas.Descricao := 'Lista Resultado VS - do Prestador da M.O.';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-171 :: Configura��o de Mat�rias-primas PDA
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-171';
  FRJanelas.Nome      := 'FmVSNatPDA';
  FRJanelas.Descricao := 'Configura��o de Mat�rias-primas PDA';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-172 :: Configura��o de Couro DTA
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-172';
  FRJanelas.Nome      := 'FmVSCouDTA';
  FRJanelas.Descricao := 'Configura��o de Couro DTA';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-173 :: Processamento de Subproduto
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-173';
  FRJanelas.Nome      := 'FmVSPSPCab';
  FRJanelas.Descricao := 'Processamento de Subproduto';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-174 :: IME-I Origem de Processamento de Subproduto
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-174';
  FRJanelas.Nome      := 'FmVSPSPOriIMEI';
  FRJanelas.Descricao := 'IME-I Origem de Processamento de Subproduto';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-175 :: Configura��o de Subproduto em Processo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-175';
  FRJanelas.Nome      := 'FmVSPSPPro';
  FRJanelas.Descricao := 'Configura��o de Subproduto em Processo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-176 :: Configura��o de Subproduto Processado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-176';
  FRJanelas.Nome      := 'FmVSPSPEnd';
  FRJanelas.Descricao := 'Configura��o de Subproduto em Processado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-177 :: Subproduto Processado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-177';
  FRJanelas.Nome      := 'FmVSPSPDst';
  FRJanelas.Descricao := 'Subproduto Processado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-178 :: Reproceso / Reparo de Material
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-178';
  FRJanelas.Nome      := 'FmVSRRMDst';
  FRJanelas.Descricao := 'Reproceso / Reparo de Material';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-179 :: Origem de Artigo de Reprocesso / Reparo (Pallet)
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-179';
  FRJanelas.Nome      := 'FmVSRRMOriPall';
  FRJanelas.Descricao := 'Origem de Artigo de Reprocesso / Reparo (Pallet)';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-180 :: Origem de Artigo de Reprocesso / Reparo (IME-I)
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-180';
  FRJanelas.Nome      := 'FmVSRRMOriIMEI';
  FRJanelas.Descricao := 'Origem de Artigo de Reprocesso / Reparo (IME-I)';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-181 :: Destino de Artigo de Reprocesso / Reparo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-181';
  FRJanelas.Nome      := 'FmVSRRMDst';
  FRJanelas.Descricao := 'Destino de Artigo de Reprocesso / Reparo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-182 :: Datas Lan�amento Retroativo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-182';
  FRJanelas.Nome      := 'FmVSDataEFDData';
  FRJanelas.Descricao := 'Datas Lan�amento Retroativo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-183 :: Impress�o de Ordem de Produ��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-183';
  FRJanelas.Nome      := 'FmVSImpOrdem';
  FRJanelas.Descricao := 'Impress�o de Ordem de Produ��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-184 :: Altera��o de IME-I de Origem de Processo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-184';
  FRJanelas.Nome      := 'FmVSPWEAltOriIMEI';
  FRJanelas.Descricao := 'Altera��o de IME-I de Origem de Processo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-185 :: Altera��o de Data e N�mero de NFe
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-185';
  FRJanelas.Nome      := 'FmVSDtHrSerNumNFe';
  FRJanelas.Descricao := 'Altera��o de Data e N�mero de NFe';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-186 :: Configura��o de Artigo Semi / Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-186';
  FRJanelas.Nome      := 'FmVSArtCab';
  FRJanelas.Descricao := 'Configura��o de Artigo Semi / Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-187 :: Atrelamento de Itens de Configura��o de Artigo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-187';
  FRJanelas.Nome      := 'FmVSArtGGX';
  FRJanelas.Descricao := 'Atrelamento de Itens de Configura��o de Artigo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-188 :: Informa��es do movimento (Status)
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-188';
  FRJanelas.Nome      := 'FmVSVmcObs';
  FRJanelas.Descricao := 'Informa��es do movimento (Status)';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-189 :: Inclus�o de Pallet Manualmente
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-189';
  FRJanelas.Nome      := 'FmVSPalletManual';
  FRJanelas.Descricao := 'Inclus�o de Pallet Manualmente';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-190 :: NF-e de Entrada In Natura
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-190';
  FRJanelas.Nome      := 'FmVSInnNFs';
  FRJanelas.Descricao := 'NF-e de Entrada In Natura';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-191 :: Configura��o de Plano de Metas de Rendimento de Subprodutos
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-191';
  FRJanelas.Nome      := 'FmVSCPMRSBCb';
  FRJanelas.Descricao := 'Configura��o de Plano de Metas de Rendimento de Subprodutos';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-192 :: Item de Plano de Metas de Rendimento de Subprodutos
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-192';
  FRJanelas.Nome      := 'FmVSCPMRSBIt';
  FRJanelas.Descricao := 'Item de Plano de Metas de Rendimento de Subprodutos';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-193 :: Encerramento de Rendimento de Lote
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-193';
  FRJanelas.Nome      := 'FmVSCPMRSBER';
  FRJanelas.Descricao := 'Encerramento de Rendimento de Lote';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-194 :: IEC - Informa��o de Entrada de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-194';
  FRJanelas.Nome      := 'FmVSInfInn';
  FRJanelas.Descricao := 'IEC - Informa��o de Entrada de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-195 :: ICR - Informa��o de Classe/Reclasse de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-195';
  FRJanelas.Nome      := 'FmVSInfMov';
  FRJanelas.Descricao := 'ICR - Informa��o de Classe/Reclasse de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-196 :: ISC - Informa��o de Sa�da de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-196';
  FRJanelas.Nome      := 'FmVSInfOut';
  FRJanelas.Descricao := 'ISC - Informa��o de Sa�da de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-197 :: LSP - Lista Sequencial de Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-197';
  FRJanelas.Nome      := 'FmVSLstPal';
  FRJanelas.Descricao := 'LSP - Lista Sequencial de Pallet';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-198 :: IEC - Informa��o de Entrada de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-198';
  FRJanelas.Nome      := 'FmVSInfIEC';
  FRJanelas.Descricao := 'IEC - Informa��o de Entrada de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-199 :: IPM - Informa��o de Pallets Movimentados
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-199';
  FRJanelas.Nome      := 'FmVSInfPal';
  FRJanelas.Descricao := 'IPM - Informa��o de Pallets Movimentados';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-200 :: IXX - Atrelamento de Informa��o Manual
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-200';
  FRJanelas.Nome      := 'FmVSIxx';
  FRJanelas.Descricao := 'Atrelamento de Informa��o Manual';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-201 :: Estoque - Custo Integrado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-201';
  FRJanelas.Nome      := 'FmVSEstqCustoIntegr';
  FRJanelas.Descricao := 'Estoque - Custo Integrado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-202 :: ID Movimento
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-202';
  FRJanelas.Nome      := 'FmVSMovimID';
  FRJanelas.Descricao := 'ID Movimento';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-203 :: Local x ID Movimento
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-203';
  FRJanelas.Nome      := 'FmVSMovIDLoc';
  FRJanelas.Descricao := 'Local x ID Movimento';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-204 :: Fun�oes VS em Janela Oculta
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-204';
  FRJanelas.Nome      := 'FmVSHide';
  FRJanelas.Descricao := 'Fun�oes VS em Janela Oculta';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-205 :: Entrada de Couros sem Cobertura
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-205';
  FRJanelas.Nome      := 'FmVSESCCab';
  FRJanelas.Descricao := 'Entrada de Couros sem Cobertura';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-206 :: Item de Entrada de Couros sem Cobertura
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-206';
  FRJanelas.Nome      := 'FmVSESCIts';
  FRJanelas.Descricao := 'Item de Entrada de Couros sem Cobertura';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-207 :: Configura��o de Classifica��o de Artigo de Ribeira por Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-207';
  FRJanelas.Nome      := 'FmVSClaPalPrpQnz';
  FRJanelas.Descricao := 'Configura��o de Classifica��o de Artigo de Ribeira por Pallet';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-208 :: Importa��o de Dados do ClaReCo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-208';
  FRJanelas.Nome      := 'VSLoadCRCCab';
  FRJanelas.Descricao := 'Importa��o de Dados do ClaReCo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-209 :: Item de Importa��o de Dados do ClaReCo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-209';
  FRJanelas.Nome      := 'VSLoadCRCIts';
  FRJanelas.Descricao := 'Item de Importa��o de Dados do ClaReCo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-210 :: ClaReCo - Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-210';
  FRJanelas.Nome      := 'VSLoadCRCPalletA';
  FRJanelas.Descricao := 'ClaReCo - Pallet';
  FLJanelas.Add(FRJanelas);
  //
{
  Movido para grade! >> PRD-GRUPO-045 ::
  // WET-CURTI-211 :: Configura��o de Reduzidos Orf�os
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-211';
  FRJanelas.Nome      := 'GraGruYIncorpora';
  FRJanelas.Descricao := 'Configura��o de Reduzidos Orf�os';
  FLJanelas.Add(FRJanelas);
  //
}
  // WET-CURTI-212 :: Configura��o de Reduzido de Couro
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-212';
  FRJanelas.Nome      := 'GraGruXCou';
  FRJanelas.Descricao := 'Configura��o de Reduzido de Couro';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-213 :: Datas de Reclassifica��o de Artigo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-213';
  FRJanelas.Nome      := 'FmVSGerRclDatas';
  FRJanelas.Descricao := 'Datas de Reclassifica��o de Artigo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-214 :: Confer�ncia de Baixas x Estoque de Couro In Natura
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-214';
  FRJanelas.Nome      := 'FmVSCfgEstqInNatEFD';
  FRJanelas.Descricao := 'Confer�ncia de Baixas x Estoque de Couro In Natura';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-215 :: Confer�ncia de Baixas x Estoque de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-215';
  FRJanelas.Nome      := 'FmVSCfgEstqOthersEFD';
  FRJanelas.Descricao := 'Confer�ncia de Baixas x Estoque de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-216 :: Confer�ncia de Gera��o x Estoque de Couros
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-216';
  FRJanelas.Nome      := 'FmVSCfgEstqGeradoEFD';
  FRJanelas.Descricao := 'Confer�ncia de Gera��o x Estoque de Couros';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-217 :: Confer�ncia de Processos x Pesagem de PQ
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-217';
  FRJanelas.Nome      := 'FmVSCfgEstqVSxPQ_EFD';
  FRJanelas.Descricao := 'Confer�ncia de Processos x Pesagem de PQ';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-218 :: Edi��o de Controle de Envio para MO
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-218';
  FRJanelas.Nome      := 'FmVSMOEnvEnv';
  FRJanelas.Descricao := 'Edi��o de Controle de Envio para MO';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-219 :: Sele��o de Enviado(s) para MO
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-219';
  FRJanelas.Nome      := 'FmVSMOEnvSel';
  FRJanelas.Descricao := 'Sele��o de Enviado(s) para MO';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-220 :: Edi��o de Item de Envio para MO
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-220';
  FRJanelas.Nome      := 'FmVSMOEnvEVMI';
  FRJanelas.Descricao := 'Edi��o de Item de Envio para MO';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-221 :: Edi��o de Item de Gerado em MO
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-221';
  FRJanelas.Nome      := 'FmVSMOEnvGVMI';
  FRJanelas.Descricao := 'Edi��o de Item de Gerado em MO';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-222 :: Edi��o de Item de Retorno de MO
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-222';
  FRJanelas.Nome      := 'FmVSMOEnvRVMI';
  FRJanelas.Descricao := 'Edi��o de Item de Retorno de MO';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-223 :: Edi��o de Controle de Frete
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-223';
  FRJanelas.Nome      := 'FmVSMOEnvAvu';
  FRJanelas.Descricao := 'Edi��o de Controle de Frete';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-224 :: Edi��o de Item de Frete
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-224';
  FRJanelas.Nome      := 'FmVSMOEnvAVMI';
  FRJanelas.Descricao := 'Edi��o de Item de Frete';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-225 :: Gerenciamento de Frete Simples
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-225';
  FRJanelas.Nome      := 'FmVSMOEnvAvuGer';
  FRJanelas.Descricao := 'Gerenciamento de Frete Simples';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-226 :: Gerenciamento de Frete de Envio para Retorno
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-226';
  FRJanelas.Nome      := 'FmVSMOEnvEnvGer';
  FRJanelas.Descricao := 'Gerenciamento de Frete de Envio para Retorno';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-227 :: Gerenciamento de Frete de Retorno de Envio
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-227';
  FRJanelas.Nome      := 'FmVSMOEnvRetGer';
  FRJanelas.Descricao := 'Gerenciamento de Frete de Retorno de Envio';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-228 :: Gerenciamento de Fretes VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-228';
  FRJanelas.Nome      := 'FmVSMOEnvCTeGer';
  FRJanelas.Descricao := 'Gerenciamento de Fretes VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-229 :: Autoriza��o de Fretes VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-229';
  FRJanelas.Nome      := 'FmVSMOEnvCTeImp';
  FRJanelas.Descricao := 'Autoriza��o de Fretes VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-230 :: Marca��o de Defeito
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-230';
  FRJanelas.Nome      := 'FmVSClassifOneDefei';
  FRJanelas.Descricao := 'Marca��o de Defeito';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-231 :: Impress�o de Mapa de Defeitos
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-231';
  FRJanelas.Nome      := 'FmVSImpMapaDefei';
  FRJanelas.Descricao := 'Impress�o de Mapa de Defeitos';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-232 :: Impress�es de Produ��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-232';
  FRJanelas.Nome      := 'FmVSImpProducao';
  FRJanelas.Descricao := 'Impress�es de Produ��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-233 :: Impress�o de OS de Fluxo de Produ��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-233';
  FRJanelas.Nome      := 'FmVSImpOSFluxo';
  FRJanelas.Descricao := 'Impress�o de OS de Fluxo de Produ��o';
  FLJanelas.Add(FRJanelas);
  //
(*
  // WET-CURTI-234 :: NF-e de Entrada
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-234';
  FRJanelas.Nome      := 'FmVSInnNFsCab';
  FRJanelas.Descricao := 'NF-e de Entrada';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-235 :: Itens de NF-e de Entrada
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-235';
  FRJanelas.Nome      := 'FmVSInnNFsIts';
  FRJanelas.Descricao := 'Itens de NF-e de Entrada';
  FLJanelas.Add(FRJanelas);
*)
  // WET-CURTI-236 :: Configura��o de Mat�ria-prima Conservada
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-236';
  FRJanelas.Nome      := 'FmVSNatCon';
  FRJanelas.Descricao := 'Configura��o de Mat�ria-prima Conservada';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-237 :: Conserva��o de Mat�ria-Prima
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-237';
  FRJanelas.Nome      := 'FmVSConCab';
  FRJanelas.Descricao := 'Conserva��o de Mat�ria-Prima';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-238 :: Origem de Artigo de Conserva��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-238';
  FRJanelas.Nome      := 'FmVSConOriIMEI';
  FRJanelas.Descricao := 'Origem de Artigo de Conserva��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-239 :: Destino de Artigo de Conserva��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-239';
  FRJanelas.Nome      := 'FmVSConDst';
  FRJanelas.Descricao := 'Destino de Artigo de Conserva��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-240 :: Configura��o de Mat�ria-Prima em Conserva��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-240';
  FRJanelas.Nome      := 'FmVSNatInC';
  FRJanelas.Descricao := 'Configura��o de Mat�ria-Prima em Conserva��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-241 :: In Natura Pendente
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-241';
  FRJanelas.Nome      := 'FmVSInnNatPend';
  FRJanelas.Descricao := 'In Natura Pendente';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-242 :: Gera��o de Artigos por Dia - BAR
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-242';
  FRJanelas.Nome      := 'FmVSGerArtDdImpBar';
  FRJanelas.Descricao := 'Gera��o de Artigos por Dia - BAR';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-243 :: Gera��o de Artigos por Dia - CUR
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-243';
  FRJanelas.Nome      := 'FmVSGerArtDdImpCur';
  FRJanelas.Descricao := 'Gera��o de Artigos por Dia - CUR';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-244 :: Gera��o de Artigos por Marca - BAR
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-244';
  FRJanelas.Nome      := 'FmVSGerArtDdMarcaBar';
  FRJanelas.Descricao := 'Gera��o de Artigos por Dia - BAR';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-245 :: Impress�o de Ficha Kardex
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-245';
  FRJanelas.Nome      := 'FmVSImpKardex';
  FRJanelas.Descricao := 'Impress�o de Ficha Kardex';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-246 :: Entrada de Excedente
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-246';
  FRJanelas.Nome      := 'FmVSExcCab';
  FRJanelas.Descricao := 'Entrada de Excedente';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-247 :: Item de Entrada de Excedente
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-247';
  FRJanelas.Nome      := 'FmVSExcItsIMEIs';
  FRJanelas.Descricao := 'Item de Entrada de Excedente';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-248 :: Saldo de Movimenta��o Corretiva
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-248';
  FRJanelas.Nome      := 'FmVSImpSdoCorret';
  FRJanelas.Descricao := 'Saldo de Movimenta��o Corretiva';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-249 :: Reduzidos em Conflito
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-249';
  FRJanelas.Nome      := 'FmVSRedzConfltDstGGX';
  FRJanelas.Descricao := 'Reduzidos em Conflito';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-250 :: IMEIs com Quantidades Positivas e Negativas no Mesmo Registro
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-250';
  FRJanelas.Nome      := 'FmVSQtdPosNegSameReg';
  FRJanelas.Descricao := 'IMEIs com Quantidades Positivas e Negativas no Mesmo Registro';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-251 :: Reduzidos de Pr� Classe/Reclasse em Conflito
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-251';
  FRJanelas.Nome      := 'VSRedzConfltPreClasRecl';
  FRJanelas.Descricao := 'Reduzidos de Pr� Classe/Reclasse em Conflito';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-252 :: Custos Divergentes entre Origem e Destino
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-252';
  FRJanelas.Nome      := 'VSCustosDifSrcDst';
  FRJanelas.Descricao := 'Custos Divergentes entre Origem e Destino';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-253 :: Custos Divergentes de Saldo Virtual
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-253';
  FRJanelas.Nome      := 'VSCustosDifSdoVrt';
  FRJanelas.Descricao := 'Custos Divergentes de Saldo Virtual';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-254 :: Custos de Produ��o
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-254';
  FRJanelas.Nome      := 'VSCustosProdu';
  FRJanelas.Descricao := 'Custos de Produ��o';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-255 :: Reduzidos Conflitantes na Gera��o de Artigo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-255';
  FRJanelas.Nome      := 'VSRedConfltGerA';
  FRJanelas.Descricao := 'Reduzidos Conflitantes na Gera��o de Artigo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-256 :: Reduzidos Conflitantes na Baixa de Artigo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-256';
  FRJanelas.Nome      := 'VSRedConfltBaixa';
  FRJanelas.Descricao := 'Reduzidos Conflitantes na Baixa de Artigo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-257 :: Custos Divergentes de Pr� Reclasse
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-257';
  FRJanelas.Nome      := 'VSCustosDifPreRecl';
  FRJanelas.Descricao := 'Custos Divergentes de Pr� Reclasse';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-258 :: Custos Divergentes de Reclasse
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-258';
  FRJanelas.Nome      := 'VSCustosDifReclasse';
  FRJanelas.Descricao := 'Custos Divergentes de Reclasse';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-259 :: Corre��o de custos em Sequ�ncia de IME-C
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-259';
  FRJanelas.Nome      := 'VSCorreCustosIMECs';
  FRJanelas.Descricao := 'Corre��o de custos em Sequ�ncia de IME-C';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-260 :: Pe�as Positivas/Negativas X Valor Total Negativos/Positivos
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-260';
  FRJanelas.Nome      := 'VSPcPosNegXValTNegPos';
  FRJanelas.Descricao := 'Pe�as Positivas/Negativas X Valor Total Negativos/Positivos';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-261 :: Links de Baixas de Artigos em Processos
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-261';
  FRJanelas.Nome      := 'VSSrcGGXDifProcessos';
  FRJanelas.Descricao := 'Links de Baixas de Artigos em Processos';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-262 :: Configura��o de Artigo em Reprocesso/Reparo
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-262';
  FRJanelas.Nome      := 'FmVSRepMer';
  FRJanelas.Descricao := 'Configura��o de Artigo em Reprocesso/Reparo';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-263 :: Tabela Rendimento In Natura � Curtido
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-263';
  FRJanelas.Nome      := 'FmVSTabMPAG';
  FRJanelas.Descricao := 'Tabela de Rendimento de In Natura � Curtido';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-264 :: IMEIs com Valores Positivos e Negativos no Mesmo Registro
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-264';
  FRJanelas.Nome      := 'FmVSVlrPosNegSameReg';
  FRJanelas.Descricao := 'IMEIs com Valores Positivos e Negativos no Mesmo Registro';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-265 :: Pesquisa OS MPV para VS
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-265';
  FRJanelas.Nome      := 'FmOsMpvVs';
  FRJanelas.Descricao := 'Pesquisa OS MPV para VS';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-266 :: Estoque de Reduzido
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-266';
  FRJanelas.Nome      := 'FmVSImpEstqReduz';
  FRJanelas.Descricao := 'Estoque de Reduzido';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-267 :: Origem de Artigo em Processo de Caleiro - Pe�as
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-267';
  FRJanelas.Nome      := 'FmVSCalOriIMEI_Peca';
  FRJanelas.Descricao := 'Origem de Artigo em Processo de Caleiro - Pe�as';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-268 :: Origem de Artigo em Processo de Caleiro - Peso
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-268';
  FRJanelas.Nome      := 'FmVSCalOriIMEI_Peso';
  FRJanelas.Descricao := 'Origem de Artigo em Processo de Caleiro - Peso';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-269 :: Destino de Artigo em Processo de Caleiro - Item Selecionado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-269';
  FRJanelas.Nome      := 'FmVSCalDstUni';
  FRJanelas.Descricao := 'Destino de Artigo em Processo de Caleiro - Item Selecionado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-270 :: Ajuste de Marcas
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-270';
  FRJanelas.Nome      := 'FmVSMarcaAjusta';
  FRJanelas.Descricao := 'Ajuste de Marcas';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-271 :: Localiza Marcas
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-271';
  FRJanelas.Nome      := 'FmVSLocalizaMarcas';
  FRJanelas.Descricao := 'Localiza Marcas';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-272 :: Reordena Artigos Commodity
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-272';
  FRJanelas.Nome      := 'FmArtGeCommReord';
  FRJanelas.Descricao := 'Reordena Artigos Commodity';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-273 :: Sub Produto Processado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-273';
  FRJanelas.Nome      := 'FmVSCalSubPrdItsMul';
  FRJanelas.Descricao := 'Sub Produto Processado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-CURTI-274 :: Sub Produto Processado (M�ltiplo)
  New(FRJanelas);
  FRJanelas.ID        := 'WET-CURTI-274';
  FRJanelas.Nome      := 'FmVSCalSubPrdItsMul';
  FRJanelas.Descricao := 'Sub Produto Processado (M�ltiplo)';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-001 :: Entrada de Wet Blue no Estoque
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-001';
  FRJanelas.Nome      := 'FmWBInnCab';
  FRJanelas.Descricao := 'Entrada de Wet Blue no Estoque';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-002 :: Item de Entrada de Wet Blue no Estoque
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-002';
  FRJanelas.Nome      := 'FmWBInnIts';
  FRJanelas.Descricao := 'Item de Entrada de Wet Blue no Estoque';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-003 :: Reclassifica��o de Wet Blue
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-003';
  FRJanelas.Nome      := 'FmWBRclIns';
  FRJanelas.Descricao := 'Reclassifica��o de Wet Blue';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-004 :: Relat�rios de Wet Blue
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-004';
  FRJanelas.Nome      := 'FmWBMovImp';
  FRJanelas.Descricao := 'Relat�rios de Wet Blue';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-005 :: Sa�da de Wet Blue no Estoque
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-005';
  FRJanelas.Nome      := 'FmWBOutCab';
  FRJanelas.Descricao := 'Sa�da de Wet Blue no Estoque';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-006 :: Item de Venda de Wet Blue Sem Rastreio
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-006';
  FRJanelas.Nome      := 'FmWBOutUnk';
  FRJanelas.Descricao := 'Item de Venda de Wet Blue';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-007 :: Item de Venda de Wet Blue com Rastreio
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-007';
  FRJanelas.Nome      := 'FmWBOutKno';
  FRJanelas.Descricao := 'Item de Venda de Wet Blue';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-008 :: Configura��o de Artigo Semi / Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-008';
  FRJanelas.Nome      := 'FmWBArtCab';
  FRJanelas.Descricao := 'Configura��o de Artigo Semi / Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-009 :: Item de Baixa For�ada de Wet Blue no Estoque
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-009';
  FRJanelas.Nome      := 'FmWBOutIts';
  FRJanelas.Descricao := 'Item de Baixa For�ada de Wet Blue no Estoque';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-010 :: Ajuste de Estoque de Wet Blue
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-010';
  FRJanelas.Nome      := 'FmWBAjsCab';
  FRJanelas.Descricao := 'Ajuste de Estoque de Wet Blue';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-011 :: Item de Ajuste de Estoque de Wet Blue
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-011';
  FRJanelas.Nome      := 'FmWBAjsIts';
  FRJanelas.Descricao := 'Item de Ajuste de Estoque de Wet Blue';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-012 :: Industrializa��o de Wet Blue
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-012';
  FRJanelas.Nome      := 'FmWBIndsWE';
  FRJanelas.Descricao := 'Industrializa��o de Wet Blue';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-013 :: Configura��o de Mat�ria-prima para Semi / Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-013';
  FRJanelas.Nome      := 'FmWBMPrCab';
  FRJanelas.Descricao := 'Configura��o de Mat�ria-prima para Semi / Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-014 :: Reclassifica��o de Mat�ria-prima para Semi / Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-014';
  FRJanelas.Nome      := 'FmWBRclCab';
  FRJanelas.Descricao := 'Reclassifica��o de Mat�ria-prima para Semi / Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-015 :: Cadastro de Pallet de Mat�ria-prima para Semi / Acabado
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-015';
  FRJanelas.Nome      := 'FmWBPallet';
  FRJanelas.Descricao := 'Cadastro de Pallet de Mat�ria-prima para Semi / Acabado';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-016 :: MP Permitido em Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-016';
  FRJanelas.Nome      := 'FmWBPalArt';
  FRJanelas.Descricao := 'MP Permitido em Pallet';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-017 :: Fornecedor Permitido em MP de Pallet
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-017';
  FRJanelas.Nome      := 'FmWBPalFrn';
  FRJanelas.Descricao := 'Fornecedor Permitido em MP de Pallet';
  FLJanelas.Add(FRJanelas);
  //
  // WET-RECUR-018 :: �rea M�dia por Fornecedor
  New(FRJanelas);
  FRJanelas.ID        := 'WET-RECUR-018';
  FRJanelas.Nome      := 'FmWBMPrFrn';
  FRJanelas.Descricao := '�rea M�dia por Fornecedor';
  FLJanelas.Add(FRJanelas);
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;


end.
