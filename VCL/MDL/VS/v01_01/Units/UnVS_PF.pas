unit UnVS_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Buttons, ComCtrls, CommCtrl, Consts,
  Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums, dmkEditCB,
  dmkEdit, dmkDBLookupComboBox, mySQLDbTables, Data.Db, DBGrids, AppListas,
  dmkDBGridZTO, UnDmkProcFunc, UnProjGroup_Vars, BlueDermConsts, TypInfo,
  System.Math, UnProjGroup_Consts, UnEntities, DBCtrls, Grids, Mask, UnVS_Tabs,
  dmkEditDateTimePicker, UnGrl_Consts, UnGrl_Geral, UnVS_CRC_PF,
  UnAppPF, UnGrade_PF, UnAppEnums, mySQLDirectQuery, UMySQLDB, dmkPageControl;

type
  THackDBGrid = class(TDBGrid);
  TIMECArr = array of array [0..2] of Integer;
  TIMExReg = array [0..31] of array [0..1] of Integer;
  TIMExArr = array of TIMExReg;
  TUnVS_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraFormVSMovImp(AbrirEmAba: Boolean; InOwner: TWincontrol;
              PageControl: TdmkPageControl; PageIndex: Integer);
    //
    procedure AbreGraGruXY(Qry: TmySQLQuery; _AND: String);
    procedure AbreVSSerFch(QrVSSerFch: TmySQLQuery);
    function  AdicionarNovosVS_emid(): Boolean;
    procedure AtualizaDtHrFimOpe_MovimCod(MovimID: TEstqMovimID; MovimCod: Integer);
    procedure AtualizaNotaMPAG(Controle: Integer; NotaMPAG, FatNotaVNC,
              FatNotaVRC: Double);
    procedure AtualizaSaldoIMEI(Controle: Integer; Gera: Boolean);
    procedure AtualizaSaldoOrigemVMI_Dest(VMI_Dest, VMI_Sorc, VMI_Baix: Integer);
    procedure AtualizaSaldoVirtualVSMovIts(Controle: Integer; Gera: Boolean);
    procedure AtualizaSaldoVirtualVSMovIts_Generico(Controle, MovimID, MovimNiv: Integer);
    procedure AtualizaStatPall(Pallet: Integer);
    procedure AtualizaPalletPelosIMEIsDeGeracao(Pallet: Integer;
              QrSumVMI: TmySQLQuery);
    procedure AtualizaTotaisVSOpeCab(MovimCod: Integer);
    function  AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc: Integer;
              QrSumDest, QrSumSorc, QrVMISorc, QrPalSorc: TmySQLQuery): Boolean;
    procedure AtualizaTotaisVSCalCab(MovimCod: Integer);
    procedure AtualizaTotaisVSConCab(MovimCod: Integer);
    procedure AtualizaTotaisVSCurCab(MovimCod: Integer);
    procedure AtualizaTotaisVSPSPCab(MovimCod: Integer);
    procedure AtualizaTotaisVSRRMCab(MovimCod: Integer);
    procedure AtualizaTotaisVSPWECab(MovimCod: Integer);
    procedure AtualizaVSConCabGGxSrc(MovimCod: Integer);
    procedure AtualizaVSCalCabGGxSrc(MovimCod: Integer);
    procedure AtualizaVSCurCabGGxSrc(MovimCod: Integer);
    procedure AtualizaVSPedIts_Lib(VSPedIts, VSMovIts: Integer;
              LibPecas, LibPesoKg, LibAreaM2, LibAreaP2: Double);
    //
    function  CalculaBaseValLiq(BaseValVenda, BaseImpostos, BasePerComis,
              BasFrteVendM2: Double): Double;
    //
    function  CadastraPalletRibCla(Empresa, ClientMO: Integer; EdPallet:
              TdmkEditCB; CBPallet: TdmkDBLookupComboBox; QrVSPallet: TmySQLQuery;
              MovimIDGer: TEstqMovimID; GraGruX: Integer): Integer;
    function  CampoLstPal(Box: Integer): String;
    function  DesfazEncerramentoPallet(Pallet, GraGruX: Integer): Boolean;
    function  EncerraPalletNew(const Pallet: Integer;
              const Pergunta: Boolean): Boolean;
    function  ExcluiVSMovIts_EnviaArquivoExclu(Controle, Motivo: Integer; Query:
              TmySQLQuery; DataBase: TmySQLDatabase; Senha: String): Boolean;
    function  ExcluiVSNaoVMI(Pergunta, Tabela, Campo: String; Inteiro1:
              Integer; DB: TmySQLDatabase): Integer;
    function  FatorNotaMP(GraGruX: Integer): Double;
    function  GetIDNiv(MovimID, MovimNiv: Integer): Integer;
    function  GeraSQLTabMov(var SQL: String; const Tab: TTabToWork; const
              TemIMEIMrt: Integer = 0): Boolean;
    function  GeraSQLVSMovItx_IMEI(SQL_Select, SQL_Flds, SQL_Left, SQL_Wher,
              SQL_Group: String; Tab: TTabToWork; TemIMEIMrt: Integer = 0):
              String;
    procedure ImprimeClassIMEIs(IMEIS, Mortos: array of Integer;
              MostraForm: Boolean);
    procedure ImprimePackListsIMEIs(Empresa: Integer; IMEIS: array of Integer;
              Vertical: Boolean);
    procedure ImprimePallet_Unico(Empresa, ClientMO, Pallet: Integer; JanTab:
              String; InfoNO_PALLET: Boolean);
    procedure ImprimePallets(Empresa: Integer; Pallets: array of Integer;
              VSMovImp4, VSLstPalBox: String; InfoNO_PALLET: Boolean;
              DestImprFichaPallet: TDestImprFichaPallet;
              ImpEmpresa: Boolean = True; EmptyLabels: Integer = 0);
    //
    procedure MostraFormVS_Do_IMEI(Controle: Integer);
    procedure MostraFormVSMovIts(Controle: Integer);
    procedure MostraFormVSMovItsAlt(Controle: Integer;
              AtualizaSaldoModoGenerico: Boolean;
              GroupBoxes: array of TEstqEditGB);
    procedure MostraFormVSPallet(Codigo: Integer);
    procedure MostraFormVSPallet1(Codigo: Integer);
    procedure MostraFormVSSerFch();
    //
    function  NotaCouroRibeiraApuca(Pecas, Peso, AreaM2, FatorMP, FatorAR: Double):
              Double;
    function  ObtemNomeTabelaVSXxxCab(MovimID: TEstqMovimID): String;
    function  PesquisaPallets(const Empresa, ClientMO, CouNiv2, CouNiv1,
              StqCenCad, StqCenLoc: Integer; const Tabela: String; const GraGruYs: String;
              const GraGruXs, Pallets: array of Integer; const SQL_Especificos:
              String; const MovimID: TEstqMovimID; const MovimCod: Integer;
              var sVSMovImp4: String): Boolean;
    function  RedefineReduzidoOnPallet(MovimID: TEstqMovimID; MovimNiv:
              TEstqMovimNiv): TTipoTrocaGgxVmiPall;
    procedure ReopenQrySaldoIMEI_Baixa(Qry1, Qry2: TmySQLQuery; SrcNivel2: Integer);
    procedure ReopenVSMovXXX(Qry: TmySQLQuery; Campo: String; IMEI, TemIMEIMrt,
              CtrlLoc: Integer);
    function  SQL_TipoEstq_DefinirCodi(CAST: Boolean; NomeFld: String; UsaVirgula:
              Boolean; FldQtdArM2, FldQtdPeso, FldQtdPeca: String;
              Prefacio: String = ''): String;
    function  SQL_NO_GGX(): String;
    function  SQL_NO_FRN(): String;
    function  SQL_NO_CMO(): String;
    function  SQL_NO_SCL(): String;
    function  SQL_LJ_GGX(): String;
    function  SQL_LJ_FRN(): String;
    function  SQL_LJ_CMO(): String;
    function  SQL_LJ_SCL(): String;
    function  SQL_MovIDeNiv_Pos_Inn(): String;
    function  TabCacVS_Tab(Tab: TTabToWork): String;
    function  TabMovVS_Fld_IMEI(Tab: TTabToWork): String;
    function  TabMovVS_Tab(Tab: TTabToWork): String;
    procedure ZeraSaldoIMEI(Controle: Integer);

    function  ObrigaInfoIxx(IxxMovIX: TEstqMovInfo; IxxFolha, IxxLinha:
              Integer): Boolean;
    function  VSFic(GraGruX, Empresa, ClienteMO, Fornecedor, Pallet,
              Ficha: Integer; Pecas, AreaM2, PesoKg, ValorT: Double; EdGraGruX,
              EdPallet, EdFicha, EdPecas, EdAreaM2, EdPesoKg, EdValorT: TWinControl;
              ExigeFornecedor: Boolean; GraGruY: Integer; ExigeAreaouPeca:
              Boolean; EdStqCenLoc: TWinControl): Boolean;
{
    function  InsUpdVSMovIts1(SQLType: TSQLType; Codigo, MovimCod, MovimTwn,
              Empresa, Terceiro: Integer; MovimID: TEstqMovimID;
              MovimNiv: TEstqMovimNiv;
              Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2, AreaP2,
              ValorT: Double; _Data_Hora_: String; SrcMovID: TEstqMovimID;
              SrcNivel1, SrcNivel2: Integer; Observ: String;
              LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,
              Misturou*): Integer; CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
              DstMovID: TEstqMovimID; DstNivel1, DstNivel2: Integer;
              QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
              AptoUso, FornecMO, SerieFch: Integer; NotaMPAG: Double;
              SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto, PedItsLib,
              PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
              GSPSrcNiv2, ReqMovEstq,
              //StqCenLoc, ItemNFe, VSMulFrnCab: Integer; ClientMO: Integer = -11): Boolean;
              StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
              QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2: Double;
              GGXRcl: Integer;
              JmpMovID: TEstqMovimID = emidAjuste; JmpNivel1:
              Integer = 0; JmpNivel2: Integer = 0; JmpGGX: Integer = 0;
              RmsMovID: TEstqMovimID = emidAjuste; RmsNivel1: Integer = 0;
              RmsNivel2: Integer = 0; RmsGGX: Integer = 0; GSPJmpMovID:
              TEstqMovimID = emidAjuste; GSPJmpNiv2: Integer = 0;(*;
              ForcaDtCorrApo: TDateTime = 0;*)
              MovCodPai: Integer = 0;
              IxxMovIX: TEstqMovInfo = TEstqMovInfo.eminfIndef;
              IxxFolha: Integer = 0; IxxLinha: Integer = 0): Boolean;
}
    function  InsUpdVSMovIts2(SQLType: TSQLType; Codigo, MovimCod, MovimTwn,
              Empresa, Terceiro: Integer; MovimID: TEstqMovimID;
              MovimNiv: TEstqMovimNiv;
              Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2, AreaP2,
              ValorT: Double; _Data_Hora_: String; SrcMovID: TEstqMovimID;
              SrcNivel1, SrcNivel2: Integer; Observ: String;
              LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,
              Misturou*): Integer; CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
              DstMovID: TEstqMovimID; DstNivel1, DstNivel2: Integer;
              QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
              AptoUso, FornecMO, SerieFch: Integer; NotaMPAG: Double;
              SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto, PedItsLib,
              PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
              GSPSrcNiv2, ReqMovEstq,
              //StqCenLoc, ItemNFe, VSMulFrnCab: Integer; ClientMO: Integer = -11): Boolean;
              StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
              QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
              PerceComiss, CusKgComiss, CustoComiss, CredPereImposto,
              CredValrImposto, CusFrtAvuls: Double;
              GGXRcl: Integer;
              //
              RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI: Double;
              //
              JmpMovID: TEstqMovimID; JmpNivel1, JmpNivel2, JmpGGX: Integer;
              RmsMovID: TEstqMovimID; RmsNivel1, RmsNivel2, RmsGGX: Integer;
              GSPJmpMovID: TEstqMovimID; GSPJmpNiv2, MovCodPai: Integer;
              IxxMovIX: TEstqMovInfo; IxxFolha, IxxLinha: Integer;
              ExigeClientMO, ExigeFornecMO, ExigeStqLoc: Boolean;
              InsUpdVMIPrcExecID: TInsUpdVMIPrcExecID): Boolean;
    function  InsUpdVSMovIts3(SQLType: TSQLType; Codigo, MovimCod, MovimTwn,
              Empresa, Terceiro: Integer; MovimID: TEstqMovimID;
              MovimNiv: TEstqMovimNiv;
              Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2, AreaP2,
              ValorT: Double; _Data_Hora_: String; SrcMovID: TEstqMovimID;
              SrcNivel1, SrcNivel2: Integer; Observ: String;
              LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,
              Misturou*): Integer; CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
              DstMovID: TEstqMovimID; DstNivel1, DstNivel2: Integer;
              QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
              AptoUso, FornecMO, SerieFch: Integer; NotaMPAG: Double;
              SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto, PedItsLib,
              PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
              GSPSrcNiv2, ReqMovEstq,
              //StqCenLoc, ItemNFe, VSMulFrnCab: Integer; ClientMO: Integer = -11): Boolean;
              StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
              QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
              PerceComiss, CusKgComiss, CustoComiss, CredPereImposto,
              CredValrImposto, CusFrtAvuls: Double;
              GGXRcl: Integer;
              //
              RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI: Double;
              //
              JmpMovID: TEstqMovimID; JmpNivel1, JmpNivel2, JmpGGX: Integer;
              RmsMovID: TEstqMovimID; RmsNivel1, RmsNivel2, RmsGGX: Integer;
              GSPJmpMovID: TEstqMovimID; GSPJmpNiv2, MovCodPai, VmiPai: Integer;
              IxxMovIX: TEstqMovInfo; IxxFolha, IxxLinha: Integer;
              ExigeClientMO, ExigeFornecMO, ExigeStqLoc: Boolean;
              InsUpdVMIPrcExecID: TInsUpdVMIPrcExecID): Boolean;
    procedure AtualizaTotaisVSXxxCab(Tabela: String; MovimCod: Integer);
    function  ConfigContinuarInserindoFolhaLinha(RGModoContinuarInserindo:
              TRadioGroup; EdFolha, EdLinha: TdmkEdit): Boolean;
    function  ObtemProximaFichaRMP(const Empresa: Integer; const EdSerieFch:
              TdmkEditCB; var Ficha: Integer; SerieDefinida: Integer = 0): Boolean;
    function  DefineDatasVMI(const _Data_Hora_: String; var DataHora, DtCorrApo:
              String): Boolean;
    function  HabilitaMenuInsOuAllVSAberto(Qry: TmySQLQuery; Data: TDateTime;
              Button: TWinControl; DefMenu: TPopupMenu;
              MenuItens: array of TMenuItem): Boolean;
    function  HabilitaComposVSAtivo(ID_TTW: Integer; Compos:
              array of TComponent): Boolean;
    function  ExcluiControleVSMovIts(Tabela: TmySQLQuery; Campo: TIntegerField;
              Controle1, CtrlBaix, SrcNivel2: Integer; Gera: Boolean;
              Motivo: Integer; Pergunta: Boolean = True; Reabre: Boolean = True): Boolean;
    procedure InsereVSMovCab(Codigo: Integer; MovimID: TEstqMovimID;
              CodigoID: Integer);
    procedure ObtemNomeCamposNFeVSXxxCab(const MovimID: TEstqMovimID; var
              FldSer, FldNum: String);
    function  DefineSiglaVS_NFe(NFeSer, NFeNum: Integer): String;
    procedure MostraFormVSRclArtPrpNew(SQLType: TSQLType; Pallet, Pallet1,
              Pallet2, Pallet3, Pallet4, Pallet5, Pallet6, Digitador, Revisor:
              Integer; Reclasse: Boolean; StqCenLoc, FornecMO: Integer);
    function  PalletDuplicado(MaxBox: Integer;
              p1, p2, p3, p4, p5, p6: Integer): Boolean;
    procedure InsereVSCacCab(Codigo: Integer; MovimID: TEstqMovimID;
              CodigoID: Integer);
    function  InsAltVSPalRclNewUni(const Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab:
              Integer; const MovimID: TEstqMovimID; const Codigo, MovimCod,
              BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2,
              VSMovIts, Tecla, VSPallet, GragruX: Integer;
              const SrcPallet, StqCenLoc: Integer;
              const IuvpeiInn, IuvpeiBxa: TInsUpdVMIPrcExecID;
              const LaAviso1, LaAviso2: TLabel; var CtrlSorc,
              CtrlDest: Integer): Integer;
    procedure MostraFormVSGeraArt(Codigo, Controle: Integer);
    procedure ReopenVSPallet(QrVSPallet: TmySQLQuery; Empresa, ClientMO, GraGruX:
              Integer; Ordem: String; PallOnEdit: array of Integer);
    //
    function  AreaEstaNaMedia(Pecas, AreaM2, MediaMinM2, MediaMaxM2: Double;
              Pergunta: Boolean): Boolean;
    function  FatorNotaAR(GraGruX: Integer): Double;
    procedure DistribuiCustoIndsVS(MovimNiv: TEstqMovimNiv; MovimCod,
              Codigo, CtrlDst: Integer);
    procedure AtualizaCustosDescendentesGerArtFicha(Empresa, SerieFch, Ficha:
              Integer);
    function  FatoresIncompativeis(FatorSrc, FatorDst: Integer; MeAviso: TMemo):
              Boolean;
    procedure AtualizaValoresDescendArtGerAposInfoAreaTotal(IMEIArtGer: Integer;
              AreaM2, ValorT: Double);
    function  ObtemMovimCodDeMovimIDECodigo(MovimID: TEstqMovimID; Codigo:
              Integer): Integer;
    function  CordaIMEIS(Itens: array of Integer): String;
    function  IMEI_JaEstaNoArray(const IMEI: Integer; const Lista: array of Integer;
              var ItensLoc: Integer): Boolean;
    procedure DefineDataHoraOuDtCorrApoCompos(const DataHora, DtCorrApo: TDateTime;
              TPData: TdmkEditDateTimePicker; EdHora: TdmkEdit); overload;
    procedure DefineDataHoraOuDtCorrApoCompos(const DataHora, DtCorrApo: TDateTime;
              var _DataHora: TDateTime); overload;
    procedure AtualizaValoresDescend_0000_All(Qry: TmySQLQuery; CustoM2: Double);
    procedure AtualizaValoresDescend_ID02_Venda(Controle, MovimCod: Integer;
              AreaM2, CustoM2: Double);
    procedure AtualizaValoresDescend_ID14_ClassArtVSMul(MovimTwn: Integer;
              CustoM2: Double);
    procedure AtualizaValoresDescend_ID19_EmProcWE(Controle, MovimCod: Integer;
              AreaM2, CustoM2: Double);
    procedure AtualizaValoresDescend_ID25_TransfLocal(Controle, MovimCod: Integer;
              AreaM2, CustoM2: Double);
    procedure MostraFormVSReclassifOneNew(Codigo, CacCod, StqCenLoc: Integer;
              MovimID: TEstqMovimID(*; TpAreaRcl: Integer*));
    function  PalletDuplicad3(MaxBox: Integer; Pallets: array of Integer): Boolean;
    function  InsAltVSPalCla(const Empresa, ClientMO, Fornecedor, VSMulFrnCab:
              Integer; const MovimID: TEstqMovimID; const Codigo, MovimCod,
              BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2,
              VSMovIts, Tecla, VSPallet, GragruX, StqCenLoc: Integer; const
              IuvpeiInn, IuvpeiBxa: TInsUpdVMIPrcExecID; const LaAviso1, LaAviso2: TLabel;
              var CtrlSorc, CtrlDest: Integer): Integer;
    procedure ImprimePallet_Varios(Empresa: Integer; Pallets: array of Integer;
              JanTab: String; InfoNO_PALLET: Boolean);
    function  CadastraPalletInfo(Empresa, ClientMO, GraGruX, GraGruY: Integer;
              QrVSPallet: TmySQLQuery; EdPallet: TdmkEditCB; CBPallet:
              TdmkDBLookupComboBox; SBNewPallet: TSpeedButton; EdPecas:
              TdmkEdit): Integer;
    function  FatorNotaCC(GraGruX, MovimID: Integer): Double;
    procedure AtualizaSaldoItmCal(Controle: Integer);
    procedure AtualizaSaldoItmCur(Controle: Integer);
    procedure MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc: TControl; EdStqCenLoc:
              TdmkEditCB; CBStqCenLoc: TdmkDBLookupComboBox; QrStqCenLoc:
              TmySQLQuery; MovimID: TEstqMovimID);
    procedure MostraFormVSMovimID(MovimID: TEstqMovimID);
    (*
    function  LocalizaPorCampo(EstqMovimID: TEstqMovimID; Caption, Campo:
              String): Integer;
    *)
    function  LocalizaPeloIMEC(EstqMovimID: TEstqMovimID): Integer;
    function  LocalizaPeloIMEI(EstqMovimID: TEstqMovimID): Integer;
    function  LocalizaPelaOC(EstqMovimID: TEstqMovimID): Integer;
    function  MostraFormVSRMPPsq(const MovimID: TEstqMovimID; const MovimNivs:
              array of TEstqMovimNiv; const SQLType: TSQLType; var Codigo,
              Controle: Integer; OnlySelIMEI: Boolean): Boolean;
    function  DefineIDs_Str(DBGridZTO: TdmkDBGridZTO; Query:
              TmySQLQuery; Campo: String): String; // Corda Lista
    procedure ImprimeClassIMEI(IMEI, VSGerArt, MulCab_MovimCod: Integer);
    procedure ImprimeIMEI(IMEIs: array of Integer; VSImpImeiKind:
              TXXImpImeiKind; LPFMO, FNFeRem: String; QryCab: TmySQLQuery);
    procedure ReopenVSGerArtDst(Qry: TmySQLQuery; MovimCod, Controle,
              TemIMEIMrt: Integer; EstqMovimNiv: TEstqMovimNiv);
    procedure ReopenVSGerArtSrc(Qry: TmySQLQuery; MovimCod, Controle, TemIMEIMrt:
              Integer; SQL_Limit: String; EstqMovimNiv: TEstqMovimNiv);
    procedure AtualizaSerieNFeVMI(Controle, NFeSer, NFeNum, VSMulNFeCab:
              Integer);
    procedure ReopenVSOpePrcDst(Qry: TmySQLQuery; MovimCod, Controle, TemIMEIMrt:
              Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String = '');
    procedure ReopenVSPWEDesclDst(Qry: TmySQLQuery; Codigo, Controle, Pallet,
              TemIMEIMrt: Integer);
    procedure ReopenVSOpePrcBxa(Qry: TmySQLQuery; MovimCod, MovimTwn, Controle,
              TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
    function  ObtemFatorIntDeGraGruXeVeSeDifere(const GraGruX1, GraGruX2:
              Integer; var FatorInt1, FatorInt2, Fator1para2: Double): Boolean;
    procedure AtualizaVSValorT(Controle: Integer);
    function  AlteraVMI_PesoKg(CampoPeso: String; MovimID, MovimNiv, Controle:
              Integer; Default: Double; PermiteNegativo: TSinal): Boolean;
    function  SenhaVSPwdDdNaoConfere(Digitado: String): Boolean;
    function  DefineSiglaVS_Frn(Fornece: Integer): String;
    procedure ReopenQrySaldoPallet_Visual(Qry: TmySQLQuery; Empresa, Pallet,
              VSMovIts: Integer);
    procedure MostraFormVSDivCouMeio(MovimID: TEstqMovimID;
              Boxes: array of Boolean; LaTipo_Caption: String);
    function  EncerraPalletReclassificacaoNew(const VSPaRclCabCacCod,
              VSPaRclCabCodigo, VSPaRclCabVSPallet, Box_Box, Box_VSPaClaIts,
              Box_VSPallet, Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
              const EncerrandoTodos, Pergunta: Boolean; var ReabreVSPaRclCab:
              Boolean): Boolean;
    function  ZeraEstoquePalletOrigemReclas(Pallet, Codigo, MovimCod, Empresa:
              Integer; BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2, BxaValorT:
              Double; DataEHora: TDateTime; Iuvpei: TInsUpdVMIPrcExecID):
              Boolean;
    function  GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Where, SQL_Group:
              String; Tab: TTabToWork; TemIMEIMrt: Integer = 0): String;
    function  TabMovVS_Fld_Pall(Tab: TTabToWork): String;
    procedure MostraFormVSReclassPrePalCac(SQLType: TSQLType; Pallet1, Pallet2,
              Pallet3, Pallet4, Pallet5, Pallet6, Digitador, Revisor: Integer);
    procedure MostraFormVSRclArtSel();
    procedure MostraFormVSGerRclCab(Codigo, Controle: Integer);
    procedure MostraFormVSPrePalCab(Codigo: Integer);
    procedure LimpaArray15Int(var Arr: TClass15Int);
    procedure MostraFormVSClaArtPrpQnz(Pallets: array of Integer; FormAFechar:
              TForm; IMEI, DV, StqCenLoc: Integer);
    procedure ReopenVSGerArtDst_ToClassPorFicha(QrVSGerArtNew: TmySQLQuery;
              Ficha, Controle: Integer; SoNaoZerados: Boolean);
    procedure ReopenVSGerArtDst_ToClassPorIMEI(QrVSGerArtNew: TmySQLQuery;
              IMEI: Integer);
    procedure MostraFormVSClassifOneRetIMEI_06(Pallet1, Pallet2, Pallet3, Pallet4,
              Pallet5, Pallet6: Integer; Form: TForm);
    procedure MostraFormVSClassifOne(Codigo, CacCod: Integer;
              MovimID: TEstqMovimID; FormPreDef: Integer = 0);
    procedure MostraFormVSClaArtPrpMDz(Pallet1, Pallet2, Pallet3, Pallet4, Pallet5,
              Pallet6: Integer; FormAFechar: TForm; IMEI, DV, StqCenLoc: Integer);
    procedure MostraFormVSClaArtSel_IMEI();
    procedure MostraFormVSClaArtSel_FRMP();
    procedure MostraFormVSGerClaCab(Codigo: Integer);
    procedure MostraFormVSPaMulCabR(Codigo: Integer);
    function  ObtemNomeGraGruYDestdeOrig(GraGruY: Integer): Integer;
    function  CalculaValorT(IniPecas, IniAreaM2, IniPesoKg, IniValorT,
              Pecas, AreaM2, PesoKg: Double): Double;  // ObtemValorT()
    function  ExcluiCabecalhoReclasse(MovimID: TEstqMovimID; PreClasse,
              Reclasse: Integer): Boolean;
    function  ValidaCampoNF(Operacao: Integer; EditNF: TdmkEdit; MostraMsg:
              Boolean): Boolean;
    procedure AtualizaSerieNFeVMC(MovimCod, NFeSer, NFeNum, VSMulNFeCab: Integer);
    function  ObtemListaOperacoes(): TStringList;
    procedure MostraFormVSInnCab(Codigo, VSInnIts, VSReclas: Integer);
    function  FichaErro(EdSerieFch: TdmkEdit; Empresa, Controle, Ficha: Integer;
              PermiteDuplicar: Boolean =  False): Boolean;
    function  InsereVSMovDif(Controle: Integer; Pecas, PesoKg, AreaM2, AreaP2,
              ValorT, InfPecas, InfPesoKg, InfAreaM2, InfAreaP2, InfValorT,
              PerQbrViag, PerQbrSal, RstCouPc, RstCouKg, RstCouVl, RstSalKg,
              RstSalVl, RstTotVl: Double; TribDefSel: Integer; PesoSalKg:
              Double): Boolean;
    procedure InsereVSFchRMPCab(SerieFch, Ficha, MovimID, Terceiro: Integer);
    procedure EncerraRendimentoInn(Modo: TdmkModoExec; MovimCod, Serie, Ficha:
              Integer; PB: TProgressBar; LaAviso1, LaAviso2: TLabel; GraGruYIni,
              GraGruYFim, GraGruYPos: Integer);
    procedure ReopenVSItsBxa(Qry: TmySQLQuery; SrcMovID: TEstqMovimID;
              SrcNivel1, SrcNivel2, TemIMEIMrt: Integer; MovimNiv:
              TEstqMovimNiv; LocCtrl: Integer);
    procedure AtualizaDescendentes(IMEI: Integer; Campos: array of String;
              Valores: array of Variant);
    procedure ReopenVSIts_Controle_If(Qry: TmySQLQuery; Controle, TemIMEIMrt:
              Integer);
    procedure EncerraPalletSimples(Pallet, Empresa, ClientMO: Integer;
              QrVSPallet: TmySQLQuery; PallOnEdit: array of Integer);
    procedure ReopenPedItsXXX(QrVSPedIts: TmySQLQuery; InsPedIts,
              UpdPedIts: Integer);
    //procedure ExcluiAtrelamentoMOEnvAvu(QrVSMOEnvAvu: TmySQLQuery);
    //procedure ExcluiAtrelamentoMOEnvEnv(QrVSMOEnvEnv: TmySQLQuery);
    //procedure ExcluiAtrelamentoMOEnvRet(QrVSMOEnvRet, QrVSMOEnvRVMI: TmySQLQuery);






















    procedure MandaVSMovItsVSCacItsArquivoMorto();
    //
    procedure AbreSQLListaNFesEmitidasVS(Query: TmySQLQuery; UsaSerie: Boolean;
              Serie, NFeIni, NFeFim: Integer; (*DataIni, DataFim: TDateTime;
              UsaDataIni, UsaDataFim: Boolean;*) SQLReturn: String);
    function  AdicionarNovosVS_emin(): Boolean;
    procedure AlterarNotFluxo(Controle, NotFluxo: Integer);
    function  AlteraVMI_CliVenda(Controle, Atual: Integer): Boolean;
    function  AlteraVMI_FornecMO(Controle, Atual: Integer): Boolean;
    function  AlteraVMI_AreaM2(MovimID, MovimNiv, Controle: Integer;
              Default: Double; PermiteNegativo: TSinal): Boolean;
    function  AlteraVMI_StqCenLoc(Controle, Atual: Integer): Boolean;
    procedure AtualizaNF_IMEI(MovimCod: Integer; IMEI: Integer);
    function  AtualizaArtigosDePallets(PB1: TProgressBar;
              LaAviso1, LaAviso2: TLabel): Integer;
    procedure AtualizaFornecedorBxa(IMEI: Integer);
    procedure AtualizaFornecedorCon(MovimCod: Integer);
    procedure AtualizaFornecedorCal(MovimCod: Integer);
    procedure AtualizaFornecedorCur(MovimCod: Integer);
    procedure AtualizaFornecedorExB(IMEI: Integer);
    procedure AtualizaFornecedorInd(MovimCod: Integer);
    procedure AtualizaFornecedorOpe(MovimCod: Integer);
    procedure AtualizaFornecedorPSP(MovimCod: Integer);
    procedure AtualizaFornecedorPreRcl(MovimCod: Integer);
    procedure AtualizaFornecedorPWE(MovimCod: Integer);
    procedure AtualizaFornecedorResRcl(IMEI: Integer);
    procedure AtualizaFornecedorRclUni(MovimCod: Integer);
    procedure AtualizaFornecedorRRM(MovimCod: Integer);
    procedure AtualizaIMEIsPalletComGraGruXErrado(PB1: TProgressBar;
              LaAviso1, LaAviso2: TLabel);
    function  AtualizaMovimCodDeVSMovCab(PB1: TProgressBar;
              LaAviso1, LaAviso2: TLabel): Integer;
    procedure AtualizaNotaVSCfgEqzCb(Codigo: Integer);
    function  AtualizaReclassesSemPallet(PB1: TProgressBar;
              LaAviso1, LaAviso2: TLabel): Integer;
    function  AtualizaReclassesComPalletErrado(PB1: TProgressBar;
              LaAviso1, LaAviso2: TLabel): Integer;
    function  AtualizaOrigensReclasseSemGragruX(PB1: TProgressBar;
              LaAviso1, LaAviso2: TLabel): Integer;
    procedure AtualizaOrigensReclasseSemPallet(PB1: TProgressBar;
              LaAviso1, LaAviso2: TLabel);
    procedure AtualizaOrigensReclassePalletErrado(PB1: TProgressBar;
              LaAviso1, LaAviso2: TLabel);
    procedure AtualizaTemIMEIMrt1(Tabela: String; LaAviso1, LaAviso2: TLabel;
              Campo: String = '');
    procedure AtualizaTemIMEIMrt2(Tabela: String; LaAviso1, LaAviso2: TLabel;
              Itens: String; Campo: String = '');
    procedure AtualizaSaldoVMI_SemOrig(VMI_Sorc, VSPallet: Integer);
    procedure AtualizaTotaisVSPedCab(Codigo: Integer);
    procedure AtualizaVSPedIts_Fin(VSPedIts: Integer);
    procedure AtualizaVSPedIts_Vda(VSPedIts: Integer);
    procedure AtualizaDtHrFimOpe_MovimID(MovimID: TEstqMovimID);
    procedure AtualizaJmpGGX(MovimID: TEstqMovimID; Codigo: Integer);
    //
    procedure ArrayDePallets(const Grade: TDBGrid; const QryPallet:
              TIntegerField; var PallArr: TPallArr);
    //
    procedure AvisoID(MovimID: TEstqMovimID; ProcName: String);
    procedure AvisoIDNiv(MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv;
              IMEI: Integer; ProcName: String);
    procedure AvisoNiv(MovimNiv: TEstqMovimNiv; ProcName: String);
    //
    procedure BuscaDadosItemNF(EdItemNFe, EdGraGruX, EdPecas, EdPesoKg,
              EdAreaM2, EdAreaP2: TdmkEdit; CBGraGruX: TDBLookupComboBox;
              const MovimCod: Integer; var Buscou: Boolean; var GraGruX:
              Integer; var Pecas, PesoKg, AreaM2, AreaP2: Double);
    procedure CadastraPalletWetEnd(Empresa, ClientMO: Integer; EdPallet:
              TdmkEditCB; CBPallet: TdmkDBLookupComboBox; QrVSPallet:
              TmySQLQuery; MovimIDGer: TEstqMovimID; GraGruX: Integer);
    function  CalculaValorCouros(PrecoTipo: TTipoCalcCouro; Preco, Pecas,
              PesoKg, AreaM2, AreaP2, ValTot: Double): Double;
    function  DefineQtdePedIts(PrecoTipo: TTipoCalcCouro; Pecas, PesoKg, AreaM2,
              AreaP2: Double): Double;
    function  ClaOuRclAbertos(Pallet, CacCod: Integer): Boolean;
    procedure ConfiguraRGGrandezaVS(RGGrandeza: TRadioGroup; Colunas,
              Default: Integer);
    procedure ConfiguraRGVSBastidao(RGBastidao: TRadioGroup; Habilita: Boolean;
              Default: Integer);
    procedure ConfiguraRGVSUmidade(RGUmidade: TRadioGroup; Habilita: Boolean;
              Default: Integer);
    function  CriaVMIJmpCal(VMICur, GGXJmpSrc, GGXJmpDst, GGXInn: Integer; InnPecas,
              InnAreaM2, InnPesoKg, CalPecas, CalAreaM2, CalPesoKg, CalValorT:
              Double; CalStqCenLoc, MovCodPai, VmiPai: Integer): Boolean;
    function  CriaVMIJmpCa2(VMICur, GGXJmpSrc, GGXJmpDst, GGXInn: Integer;
              InnPecas, InnAreaM2, InnPesoKg, InnValorT,
              CalPecas, CalAreaM2, CalPesoKg, CalValorT: Double;
              CalStqCenLoc, MovCodPai, VmiPai, OriGGX: Integer): Boolean;
    function  CriaVMIJmpCur(VSCurCab, Empresa(*, VMICur, GGXJmpSrc, GGXJmpDst, GGXInn*):
              Integer; DtHrAberto: String(*InnPecas,
              InnAreaM2, InnPesoKg, CurPecas, CurAreaM2, CurPesoKg, CurValorT:
              Double; CurStqCenLoc, MovCodPai: Integer*)): Boolean;
    function  CriaVMIRmsCal(VMICal, GGXRmsSrc, GGXRmsDst, GGXPDA: Integer; PDAPecas,
              PDAAreaM2, PDAPesoKg, CalPecas, CalAreaM2, CalPesoKg, CalValorT:
              Double; CalStqCenLoc: Integer; VSCalCab_Codigo, VSCalCab_MovimCod: Integer;
              // ini 2023-11-20 Baixa de sub produto por peso
              Peso: Double
              // fim 2023-11-20 Baixa de sub produto por peso
              ): Boolean;
    function  CriaVMIRmsCur(VMICur, GGXRmsSrc, GGXRmsDst, GGXDTA: Integer; DTAPecas,
              DTAAreaM2, DTAPesoKg, CurPecas, CurAreaM2, CurPesoKg, CurValorT:
              Double; CurStqCenLoc: Integer): Boolean;
    procedure DefineDataHoraOuDtMinima(const DataHora: TDateTime;
              TPData: TdmkEditDateTimePicker; EdHora: TdmkEdit); overload;
    function  DefineSQLTerceiro(const Obrigatorio: Boolean; const
              EdTerceiro: TdmkEditCB; var SQL: String): Boolean;
    function  DefineSQLPallet(const Obrigatorio: Boolean; const EdPallet:
              TdmkEditCB; var SQL: String): Boolean;
    function  OperacaoEhDivisao(MovimID, MovimCod: Integer): Boolean;
    function  ExcluiCabEIMEI_OpeCab(MovimCod, IMEI: Integer;
              MovimID: TEstqMovimID; MotivDel: TEstqMotivDel): Boolean;
    function  GeraNovoPallet(Empresa, ClientMO, GraGruX: Integer;
              EdGraGruX, EdPallet: TdmkEdit; CBPallet: TdmkDBLookupComboBox;
              SBNewPallet: TSpeedButton; QrVSPallet: TmySQLQuery;
              PallOnEdit: array of Integer): Integer;
    function  ImpedePeloBalanco(Data: TDateTime; PermiteCorrApo, Avisa: Boolean): Boolean;
    procedure ImprimeClassFichaRMP_Uni_Old(SerieFch, Ficha: Integer; NO_Serie: String;
              MostraJanela, PorMartelo: Boolean);
    procedure ImprimeClassFichaRMP_Uni_New(SerieFch, Ficha: Integer; NO_Serie: String;
              MostraJanela, PorMartelo: Boolean);
    procedure ImprimeClassFichaRMP_Mul_Old(SerieFch: Integer; NO_Serie: String;
              Fichas: Array of Integer; MostraJanela: Boolean; PB: TProgressBar;
              LaAviso1, LaAviso2: TLabel);
    procedure ImprimeClassFichaRMP_Mul_New(SerieFch: Integer; NO_Serie: String;
              Fichas: Array of Integer; MostraJanela: Boolean; PB: TProgressBar;
              LaAviso1, LaAviso2: TLabel);
    function  ImprimeEstoqueEm(Entidade, Filial, Ed00Terceiro_ValueVariant,
              RG00_Ordem1_ItemIndex, RG00_Ordem2_ItemIndex,
              RG00_Ordem3_ItemIndex, RG00_Ordem4_ItemIndex,
              RG00_Ordem5_ItemIndex, RG00_Agrupa_ItemIndex: Integer;
              Ck00DescrAgruNoItm_Checked: Boolean; Ed00StqCenCad_ValueVariant,
              RG00ZeroNegat_ItemIndex: Integer; FNO_EMPRESA, CB00StqCenCad_Text,
              CB00Terceiro_Text: String; TPDataRelativa_Date: TDateTime;
              Ck00DataCompra_Checked, Ck00EmProcessoBH_Checked: Boolean;
              DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2: TdmkDBGridZTO;
              Qr00GraGruY, Qr00GraGruX, Qr00CouNiv2: TmySQLQuery;
              DataEm: TDateTime;
              // 2021-01-09 fim
              //GraCusPrc: Integer;
              Relatorio: TRelatorioVSImpEstoque;
              // 2021-01-09 fim
              Ed00NFeIni_ValueVariant,
              Ed00NFeFim_ValueVariant: Integer; Ck00Serie_Checked: Boolean;
              Ed00Serie_ValueVariant, MovimCod, ClientMO: Integer;
              LaAviso1, LaAviso2: TLabel;
              MostraFrx: Boolean): Boolean;
    procedure ImprimeHistPall(Empresa_Cod: Integer; Empresa_Txt: String;
              Pallets: array of Integer);
    procedure ImprimeOXsAbertas(RG18Ordem1_ItemIndex, RG18Ordem2_ItemIndex,
              RG18Ordem3_ItemIndex, RG18Agrupa_ItemIndex: Integer;
              MovimIDs: array of TEstqMovimID(*MovimID: TEstqMovimID*);
              Ck18Cor_Checked, Ck18Especificos_Checked: Boolean;
              Ed18Especificos_Text: String; Ed18Fornecedor_ValueVariant: Integer);
    procedure ImprimeReclassDesnate(Codigo: Integer; ImpDesnate: TImpDesnate);
    procedure ImprimeReclassEqualize(Codigo: Integer);
    procedure ImprimeIMEIsPallets(Empresa_Cod: Integer; Empresa_Txt: String;
              Pallets: array of Integer; Ordenacao: Integer(*; InfoNO_PALLET:
              Boolean*));
    procedure ImprimeNotaMPAG(RG14Periodo_ItemIndex, RG14FontePsq_ItemIndex,
              Ed14Terceiro_ValueVariant, Ed14SerieFch_ValueVariant,
              Ed14Ficha_ValueVariant, RG14Ordem1_ItemIndex,
              RG14Ordem2_ItemIndex, RG14Ordem3_ItemIndex,
              Ed14GraGruX_ValueVariant, RG14Agrupa_ItemIndex: Integer;
              TP14DataIni_Date, TP14DataFim_Date: TDateTime;
              Ck14DataIni_Checked, Ck14DataFim_Checked, Ck14SoMPAGNo0_Checked,
              Ck14Cor_Checked: Boolean; EdEmpresa: TdmkEditCB;
              CB14SerieFch: TdmkDBLookupComboBox; CBEmpresa_Text,
              CB14Terceiro_Text, Ed14Marca_Text, CB14GraGruX_Text: String);
    procedure ImprimeOrdem(MovimID: TEstqMovimID; Empresa, Codigo, MovimCod:
              Integer);
    procedure ImprimePesquisaMartelos(EdEmpresa_ValueVariant,
              Ed05VSCacCod_ValueVariant, Ed05VSPallet_ValueVariant,
              Ed05VMI_Sorc_ValueVariant, Ed05VMI_Dest_ValueVariant,
              Ed05Martelo_ValueVariant, Ed05Ficha_ValueVariant,
              Ed05GraGruX_ValueVariant, Ed05Terceiro_ValueVariant: Integer;
              CBEmpresa_Text, Ed05Marca_Text, CB05GraGruX_Text,
              CB05Terceiro_Text, CB05SerieFch_Text: String;
              TP05DataIni_Date, TP05DataFim_Date: TDateTime;
              Ck05DataIni_Checked, Ck05DataFim_Checked, Ck05Revisores_Checked:
              Boolean; RG05OperClass_ItemIndex, RG05Ordem_ItemIndex,
              RG05AscDesc_ItemIndex: Integer; Ck05MostraPontos_Checked,
              Ck05Morto_Checked: Boolean);
    procedure ImprimePesquisaMartelos2(Entidade: Integer;
              CBEmpresa_Text: String);
    //
    procedure ImprimePrevia(Empresa: Integer; Pallets: array of Integer);
    procedure ImprimePackListsPallets(Empresa: Integer; Pallets: array of Integer;
              Vertical: Boolean; VSMovImp4, VSLstPalBox: String; Grandeza:
              TGrandezaArea);
    procedure ImprimeFichaEstoque(Empresa: Integer; NO_Emp, Responsa: String);
    //procedure ImprimeVSEmProcBH();
    procedure ImprimeRendimentoIMEIS(IMEIs: array of Integer; UsaNfeSers,
              UsaNFeNums: array of Boolean; NFeSers, NFeNums: array of Integer);
    procedure ImprimeRendimentoSemi(Empresa_Cod, Fornece_Cod, OPIni, OPFim,
              Bastidoes, Agrupa, Ordem1, Ordem2, Ordem3, Ordem4: Integer;
              Empresa_Txt, Fornece_Txt: String; DataIni, DataFim: TDateTime;
              UsaDtIni, UsaDtFim, UsaOPIni, UsaOPFim: Boolean; DBG21CouNiv2:
              TdmkDBGridZTO; Qr21CouNiv2: TmySQLQuery);
    procedure ImprimeComparaCacIts(PalletA, PalletB: Integer);
    procedure InsereVSFchRslIts(SQLType: TSQLType;
              SerieFch, Ficha, GraGruX: Integer;
              CustoKg, CustoMOKg, CusFretKg, Preco, ImpostP, ComissP, FreteM2,
              Pecas, AreaM2, PesoKg, ImpostCred: Double);
    procedure InsereVSMovImpExtra();
    //
    function  IMEC_JaEstaNoArray(const IMEC, MovimID, IMEI_Pai: Integer; const Lista:
              TIMECArr; var ItensLoc: Integer): Boolean;
    function  IMEx_JaEstaNoArray(IMEx: TIMExReg; Lista: TIMExArr): Boolean;
    function  IMEx_InsereNoArray(IMEx: TIMExReg; Lista: TIMExArr): Boolean;
    function  InverteDatas(const MovimXX: Integer; const Data, Agora: TDateTime;
              var DataHora, DtCorrApo: String): Boolean;
    procedure MostraFormVSAjsCab(Codigo, Controle: Integer);
    procedure MostraFormVSArtCab(Codigo, GraGruX: Integer);
    procedure MostraFormVSArvoreArtigos(Controle: Integer);
    procedure MostraFormVSBxaCab(Codigo, Controle: Integer);
    procedure MostraFormVSCalCab(Codigo: Integer);
    procedure MostraFormVSConCab(Codigo: Integer);
    procedure MostraFormVSCOPCab(Codigo: Integer);
    procedure MostraFormVSCurCab(Codigo: Integer);

    procedure MostraFormVSCfgEqzCb(Codigo: Integer);
    procedure MostraFormVSCfgMovEFD(ImporExpor, AnoMes, Empresa, PeriApu: Integer);
    procedure MostraFormVSCGICab(Codigo: Integer);
    procedure MostraFormVSCPMRSBCb();
    procedure MostraFormVSEmRibDTA(Codigo, Controle: Integer);
    procedure MostraFormVSEmRibPDA(Codigo, Controle: Integer);
    procedure MostraFormVSEntiMP(Codigo: Integer);
    procedure MostraFormVSDsnCab(Codigo: Integer);
    procedure MostraFormVSDvlCab(Codigo: Integer);
    procedure MostraFormVSEmitCus(Pesagem, Empresa, TipoCouro, Formula: Integer;
              BxaEstqVS: TBxaEstqVS; DataEmis: TDateTime);
    procedure MostraFormVSEqzCab(Codigo: Integer);
    procedure MostraFormVSExBCab(Codigo, Controle: Integer);
    procedure MostraFormVSExcCab(Codigo, Controle: Integer);
    procedure MostraFormVSPaInClaRcl(Pallet, CacCod: Integer);
    procedure MostraFormVSFchGerCab(SerieFch, Ficha: Integer);
    procedure MostraFormVSFchRslCab(SerieFch, Ficha: Integer);
    procedure MostraFormVSOCGerCab(MovimCod: Integer);
    procedure MostraFormVSInvNFe(Codigo: Integer);
    procedure MostraFormVSMOPWEGer();
    procedure MostraFormVSMOEnvAvu(SQLType: TSQLType; QrVMI, QrVSMOEnvAvu,
              QrVSMOEnvAVMI: TmySQLQuery; Sinal: TSinal; Terceiro, CabSerieRem,
              CabNFeRem: Integer);
    procedure MostraFormVSMOEnvEnv(SQLType: TSQLType; QrVMI, QrVSMOEnvEnv,
              QrVSMOEnvEVMI: TmySQLQuery; Sinal: TSinal; CabSerieRem, CabNFeRem:
              Integer);
    procedure MostraFormVSMOEnvSel(const EnvEmpresa, EnvSerieRem, EnvNFeRem,
              EnvFornecMO, MovimCod: Integer; const TabVMIQtdEnvRVMI: String);
    procedure MostraFormVSMOEnvRet(SQLType: TSQLType; QrVMINew, QrVMIDst,
              QrVMIBxa, QrVSMOEnvRet, QrVSMOEnvGVMI, QrVSMOEnvRVMI: TmySQLQuery;
              MovimCod_Env, CabSerieRem, CabNFeRem: Integer; Sinal: TSinal;
              GerArX2: Boolean);
    procedure MostraFormVSMotivBxa((*Codigo: Integer*));
    procedure MostraFormVSMovCab(MovimCod: Integer);
    procedure MostroFormVSMovimCod(MovimCod: Integer);
    procedure MostraFormVSMovItbAdd();
    procedure MostraFormVSMrtCad();
    procedure MostraFormVSNatCad(GraGruX: Integer; Edita: Boolean;
              QryMul: TmySQLQuery);
    procedure MostraFormVSNatInC(GraGruX: Integer; Edita: Boolean);
    procedure MostraFormVSNatCon(GraGruX: Integer; Edita: Boolean;
              QryMul: TmySQLQuery);
    procedure MostraFormVSNatPDA(GraGruX: Integer; Edita: Boolean);
    procedure MostraFormVSProCal(GraGruX: Integer; Edita: Boolean);
    procedure MostraFormVSCouCal(GraGruX: Integer; Edita: Boolean);
    procedure MostraFormVSCouDTA(GraGruX: Integer; Edita: Boolean);
    procedure MostraFormVSProCur(GraGruX: Integer; Edita: Boolean);
    procedure MostraFormVSCouCur(GraGruX: Integer; Edita: Boolean);
    procedure MostraFormVSOpeCab(Codigo: Integer);
    procedure MostraFormVSPaCRIts();
    procedure MostraFormVSPedCab(Codigo: Integer);
    procedure MostraFormVSPesqSeqPeca();
    procedure MostraFormVSPSPCab(Codigo: Integer);
    procedure MostraFormVSPMOCab(Codigo: Integer);
    procedure MostraFormVSPwdDd(AutoRegera: Boolean);
    procedure MostraFormVSPWECab(Codigo: Integer);
    procedure MostraFormVSRRMCab(Codigo: Integer);
    procedure MostraFormVSReqDiv(Codigo: Integer);
    procedure MostraFormVSReqMov(Codigo: Integer);
    procedure MostraFormVSInfInn(Codigo: Integer);
    procedure MostraFormVSInfMov(Codigo: Integer);
    procedure MostraFormVSInfOut(Codigo: Integer);
    procedure MostraFormVSInfPal(Codigo: Integer);

    procedure MostraFormVSLstPal(Codigo: Integer);
    procedure MostraFormVSRibCad(GraGruX: Integer; Edita: Boolean);
    procedure MostraFormVSRibCla(GraGruX: Integer; Edita: Boolean;
              QryMul: TmySQLQuery);
    procedure MostraFormVSRibOpe(GraGruX: Integer; Edita: Boolean);
    procedure MostraFormVSPalSta();
    procedure MostraFormVSPaMulCabA(Codigo: Integer);
    procedure MostraFormVSPlCCab(Codigo, Controle: Integer);
    procedure MostraFormVSRclCab(Codigo, VSRclIts, VSReclas: Integer);
    function  MostraFormVSRMPPsq2(var SerieFch, Ficha: Integer): Boolean;
    procedure MostraFormVSRtbCab(Codigo: Integer);
    procedure MostraFormVSSubPrd(GraGruX: Integer; Edita: Boolean);
    procedure MostraFormVSPSPPro(GraGruX: Integer; Edita: Boolean);
    procedure MostraFormVSPSPEnd(GraGruX: Integer; Edita: Boolean;
              QryMul: TmySQLQuery);
    procedure MostraFormVSSubPrdCad(MovimID, Codigo, Controle: Integer);
    procedure MostraFormVSInnSubPrdIts_Uni(SQLType: TSQLType; DataHora: TDateTime;
              QrCab, QrIts: TmySQLQuery; DsCab: TDataSource; GSPSrcMovID: TEstqMovimID;
              GSPSrcNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro,
              VSMulFrnCab, Ficha, SerieFch, Controle: Integer;
              (*OriGGX, OriSerieFch, OriFicha: Integer;*) OriMarca: String);
    procedure MostraFormVSInnSubPrdIts_Mul(SQLType: TSQLType; DataHora: TDateTime;
              QrCab, QrIts: TmySQLQuery; DsCab: TDataSource; GSPSrcMovID:
              TEstqMovimID; GSPSrcNiv2: Integer; GSPJmpMovID: TEstqMovimID;
              GSPJmpNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro,
              VSMulFrnCab, Ficha, SerieFch, Controle, MulOriMovimCod,
              TemIMEIMrt: Integer; UsaGSPJmp: Boolean; Marca: String);
    procedure MostraFormVSCalSubPrdIts_Uni(SQLType: TSQLType; DataHora: TDateTime;
              QrCab, QrIts: TmySQLQuery; DsCab: TDataSource; GSPSrcMovID: TEstqMovimID;
              GSPSrcNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro,
              VSMulFrnCab, Ficha, SerieFch, Controle: Integer;
              (*OriGGX, OriSerieFch, OriFicha: Integer;*) OriMarca: String;
              CouNiv2EmProc: Integer);
    procedure MostraFormVSCalSubPrdIts_Mul(SQLType: TSQLType; DataHora: TDateTime;
              QrCab, QrIts: TmySQLQuery; DsCab: TDataSource; GSPSrcMovID:
              TEstqMovimID; GSPSrcNiv2: Integer; GSPJmpMovID: TEstqMovimID;
              GSPJmpNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro,
              VSMulFrnCab, Ficha, SerieFch, Controle, MulOriMovimCod,
              TemIMEIMrt: Integer; UsaGSPJmp: Boolean; Marca: String;
              CouNiv2EmProc: Integer);
    procedure MostraFormVSTrfLocCab(Codigo: Integer);
    procedure MostraFormVSWetEnd(GraGruX: Integer; Edita: Boolean;
              QryMul: TmySQLQuery);
    procedure MostraFormVSFinCla(GraGruX: Integer; Edita: Boolean;
              QryMul: TmySQLQuery);
    procedure MostraFormVSRepMer(GraGruX: Integer; Edita: Boolean;
              QryMul: TmySQLQuery);
    procedure MostraFormVSImpClaMulRMP();
    procedure MostraFormVS_XXX(MovimID, Codigo, Controle: Integer);
    procedure MostraFormVSGruGGX(SQLType: TSQLType; Codigo, Bastidao, GGXAtu:
              Integer);
    procedure MostraFormVSImpResultVS();
    procedure MostraFormOperacoes(Codigo: Integer);
    procedure MostraFormVSImpMOEnvRet();
    procedure MostraFormVSImpMOEnvRet2();
    procedure MostraFormVSImpProducao();
    procedure MostraFormVSEstqCustoIntegr(Empresa: Integer);
    procedure MostraFormVSMOEnvAvuGer(Codigo: Integer);
    procedure MostraFormVSMOEnvEnvGer(Codigo: Integer);
    procedure MostraFormVSMOEnvRetGer(Codigo: Integer);
    procedure MostraFormVSMOEnvCTeGer(Empresa, Terceiro, SerCTe, nCTe: Integer);
    procedure MostraFormVSMOEnvCTeImp(Empresa, Terceiro, SerCTe, nCTe: Integer);
    procedure MostraFormVSInnNatPend();
    procedure MostraFormVSGerArtDdImpAll();
    procedure MostraFormVSGerArtDdImpBar();
    procedure MostraFormVSGerArtDdImpCur();
    procedure MostraFormVSImpKardex4();
    procedure MostraFormVSImpKardex5();
    procedure MostraFormVSImpSdoCorret();
    procedure MostraFormVSGerArtMarcaImpBar();
    procedure MostraFormVSCustosProdu();
    procedure MostraFormOpcoesVSSifDipoa();




    (*
    procedure MostraFormGraGruY(GraGruY, GraGruX: Integer; NewNome: String);
    procedure MostraFormGraGruY1(GraGruY, GraGruX: Integer; NewNome: String);*)
    //
    function  MovimIDCorreto(MovimID: TEstqMovimID): Boolean;
    //
    procedure MostraRelatorioCourosSemParteMaterial();
(*
    procedure MostraRelatorioVSImpCompraVenda(TP12DataIni_Date,
              TP12DataFim_Date: TDateTime; Ck12DataIni_Checked,
              Ck12DataFim_Checked: Boolean; CBEmpresa_Text: String;
              EdEmpresa_ValueVariant: Variant);
*)
    procedure MostraRelatorioVSImpCompraVenda2();
    procedure MostraRelatorioVSImpHistorico(GraGruX, Pallet, Terceiro: Integer;
              TP01DataIni_Date, TP01DataFim_Date,
              TPDataRelativa_Date: TDateTime; Ck01DataIni_Checked,
              Ck01DataFim_Checked: Boolean; CBEmpresa_Text: String;
              EdEmpresa: TdmkEditCB;
              //CB01GraGruX_Text, CB01Pallet_Text, CB01Terceiro_Text: String;
              PB1: TProgressBar; LaAviso1, LaAviso2: TLabel);
    procedure MostraRelatorioVSImpFluxo(TP13DataIni_Date, TP13DataFim_Date:
              TDateTime; Ck13DataIni_Checked, Ck13DataFim_Checked,
              Ck13DataIni_Visible, Ck13DataIni_Enabled: Boolean;
              Ed13_Pallet_ValueVariant, Ed13_IMEI_ValueVariant,
              Ed13GraGruX_ValueVariant, EdEmpresa_ValueVariant,
              Ed13_MaxInteiros_ValueVariant: Variant;
              RG13TipoMov_ItemIndex, RG13_AnaliSinte_ItemIndex,
              RG13_GragruY_ItemIndex: Integer; FRG13_AnaliSinte: TRadioGroup;
              FPB1: TProgressBar; FLaAviso1, FLaAviso2: TLabel;
              FBtImprime: TBitBtn; CBEmpresa_Text: String;
              Ed13SerieFch_ValueVariant, Ed13Ficha_ValueVariant: Integer);
    procedure MostraRelatoroFluxoFichaRMP(SerieFch, Ficha: Integer);
    procedure MostraRelatoroFluxoIMEI(Controle: Integer);
    procedure MostraRelatoroFluxoPallet(Pallet, Empresa: Integer;
              NO_EMP: String);
    //
    function  NotaCouroRibeiraApuca_Area(Pecas, Peso, FatorMP, FatorAR,
              NotaMPAG: Double): Double;
    //
    function  ObtemControleIMEI(const SQLType: TSQLType; var Controle: Integer;
              const Senha: String = ''): Boolean;
    procedure ObtemCodENomeEmpresaDeIMEI(const Controle: Integer; var Empresa:
              Integer; var Nome: String);
    function  ObtemControleDeVSPedItsPeloVSMovIts(VSMovIts: Integer): Integer;
    function  ObtemControleMovimTwin(MovimCod, MovimTwn: Integer; MovimID:
              TEstqMovimID; MovimNiv: TEstqMovimNiv): Integer;
    function  ObtemDataBalanco(Empresa: Integer): TDateTime;
    function  ObtemDataHoraSerNumNFe(const DtHrAtual: TDateTime; const
              SerNFeAtual, NumNFeAtual: Integer; var DtHrNovo: TDateTime; var
              SerNFeNovo, NumNFeNovo: Integer): Boolean;
    function  ObtemFatorInteiro(const GraGruX: Integer; var FatorIntRes: Integer;
              const Avisa: Boolean; const FatorIntCompara: Integer; MeAviso:
              TMemo): Boolean;
    function  ObtemGraGruYDeIMEI(IMEI: Integer): Integer;
    function  ObtemInsumoDeGraGruX(GraGruX: Integer): Integer;
    function  ObtemVSOpeSeqDeMovimCod(MovimCod: Integer): Integer;
    procedure ObtemIMEIdeSerieEFichaRMP(const SerieFch, Ficha: Integer;
              var IMEI: Integer);
    function  ObtemMovimIDDeGraGruY(GraGruY: Integer): TEstqMovimID;
    function  ObtemMovimIDBxaDeMovimID(MovimID: TEstqMovimID): TEstqMovimID;
    function  ObtemMovimNivCliForLocDeMovimID(MovimID: TEstqMovimID): TEstqMovimNiv;
    function  ObtemMovimNivInnDeMovimID(MovimID: TEstqMovimID): TEstqMovimNiv;
    function  ObtemMovimNivBxaDeMovimID(MovimID: TEstqMovimID): TEstqMovimNiv;
    function  ObtemMovimNivEFDDeMovimID(MovimID: TEstqMovimID): TEstqMovimNiv;
    function  ObtemMovimNivSrcDeMovimID(MovimID: TEstqMovimID): TEstqMovimNiv;
    function  ObtemMovimNivDstDeMovimID(MovimID: TEstqMovimID): TEstqMovimNiv;
    function  ObtemNomeDtAberturaVSXxxCab(MovimID: TEstqMovimID): String;
    function  ObtemCodigoDeMovimCod(MovimCod: Integer): Integer;
    function  ObtemMovimSeqDeMovimIDEMovimNiv(Level, MovimID, MovimNiv, MovimCod,
              IMEI, SeqAtual: Integer): Integer;
    function  ObtemSaldoFuturoIMEI(const VMIOri: Integer; const VoltaPecas,
              VoltaPesoKg, VoltaAreaM2: Double; var FuturoPecas, FuturoPesoKg,
              FuturoAreaM2, FuturoValorT: Double): Boolean;
    function  ObtemOrigemDesclasse(const IMEI: Integer; var MovimID, Codigo,
              Controle: Integer): Boolean;
    function  ObtemJmpGGXdeMovCodPai_Fast(MovCodPai: Integer; ProcName: String): Integer;
    function  ObtemJmpGGXdeMovCodPai_Slow(MovCodPai: Integer): Integer;
    //
    //
    procedure PesquisaDoubleVS(Campo: String; Valor: Double; TemIMEIMrt: Integer);
    //
    procedure PesquisaReclasseDePreClasse(MovimID, Codigo, Controle: Integer);
    procedure PreencheDadosDeVSCOPCab(VSCOPCab: Integer;
              EdEmpresa: TdmkEditCB; CBEmpresa: TDmkDBLookupComboBox;
              RGTipoArea: TRadioGroup;
              EdGraGruX: TdmkEditCB; CBGraGruX: TDmkDBLookupComboBox;
              EdGGXDst: TdmkEditCB; CBGGXDst: TDmkDBLookupComboBox;
              EdForneceMO: TdmkEditCB; CBForneceMO: TDmkDBLookupComboBox;
              EdStqCenLoc: TdmkEditCB; CBStqCenLoc: TDmkDBLookupComboBox;
              EdOperacoes: TdmkEditCB; CBOperacoes: TDmkDBLookupComboBox;
              EdCliente: TdmkEditCB; CBCliente: TDmkDBLookupComboBox;
              EdPedItsLib: TdmkEditCB; CBPedItsLib: TDmkDBLookupComboBox;
              EdClienteMO: TdmkEditCB; CBClienteMO: TDmkDBLookupComboBox;
              EdCustoMO, EdVSArtCab: TdmkEdit; CBVSArtCab: TDmkDBLookupComboBox;
              EdLinCulReb, EdLinCabReb, EdLinCulSem, EdLinCabSem: TdmkEdit;
              EdReceiRecu, EdReceiRefu: TDmkEditCB; CBReceiRecu, CBReceiRefu:
              TDmkDBLookupComboBox);

    procedure ReIncluiCouNivs(GraGruX, CouNiv1, CouNiv2, PrevPcPal, Grandeza,
              Bastidao: Integer; PrevAMPal, PrevKgPal: Double;
              ArtigoImp, ClasseImp: String; MediaMinM2, MediaMaxM2: Double;
              MediaMinKg: Double = 0; MediaMaxKg: Double = 0;
              GGXPronto: Integer = 0; FatrClase: Double = 999.000000;
              BaseValCusto: Double = 0; BaseValVenda: Double = 0; BaseCliente: String = '';
              BaseImpostos: Double = 0; BasePerComis: Double = 0;
              BasFrteVendM2: Double = 0; BaseValLiq: Double = 0;
              ArtGeComodty: Integer = 0; PrecoMin: Double = 0.0000; PrecoMax:
              Double = 0.0000);
    procedure ReopenGraGruX_A(Qry: TmySQLQuery; NomeParcial: String);
    procedure ReopenGraGruXCou(QrGraGruXCou: TmySQLQuery; GraGruX: Integer);
    procedure ReopenDesnate(Codigo, GraGruX: Integer; QrVSCacIts, QrVSCacSum:
              TmySQLQUery);
    procedure ReopenNotasEqualize(Codigo: Integer; QrNotaAll, QrNotaCrr,
              QrNotas: TmySQLQuery);
    procedure ReopenPallet_A(Qry: TmySQLQuery);
    procedure ReopenVSCOPCab(Query: TmySQLQuery; MovimID: TEstqMovimID);
    procedure ReopenVSListaPallet2(Qry: TmySQLQuery; VSMovImp4, VSLstPalBox:
              String; Empresa, GraGruX, Pallet, Terceiro: Integer; GraGruYs,
              OrderBy: String);
(*
    procedure ReopenVSGerArtBxa(Qry: TmySQLQuery; MovimCod, Controle: Integer;
              SQL_Limit: String = '');
*)
    procedure ReopenVSMovIts_Pallet1(QrVSMovIts, QrPallets: TmySQLQuery);
    (*procedure ReopenVSOpePrcAtu(Qry: TmySQLQuery; MovimCod, Controle, TemIMEIMrt:
              Integer; MovimNiv: TEstqMovimNiv);*)
    (*procedure ReopenVSPrcPrcDst(Qry: TmySQLQuery; SrcNivel1, MovimCod, Controle,
              TemIMEIMrt: Integer; SrcMovID: TEstqMovimID;
              DstMovimNiv: TEstqMovimNiv; SQL_Limit: String = '');*)
    (*procedure ReopenVSOpePrcOriIMEI(Qry: TmySQLQuery; MovimCod, Controle, TemIMEIMrt:
              Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String = '');*)
    procedure ReopenVSOpePrcOriIMEI_Sum(Qry: TmySQLQuery; MovimCod, TemIMEIMrt:
              Integer; MovimNiv: TEstqMovimNiv);
    procedure ReopenVSEmitCusIMEI(Qry: TmySQLQuery; Pesagem, TemIMEIMrt:
              Integer; SQL_Limit: String = '');
    procedure ReopenVSEmitCusIMEI_Sum(Qry: TmySQLQuery; Pesagem, TemIMEIMrt:
              Integer);
(*
    procedure ReopenVSOpePrcForcados(Qry: TmySQLQuery; Controle, SrcNivel1,
              SrcNivel2, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID);
*)
(*
    procedure ReopenVSOpeOriIMEI(Qry: TmySQLQuery; MovimCod, Controle: Integer;
              SQL_Limit: String = '');
    procedure ReopenVSOpeOriPallet(Qry: TmySQLQuery; MovimCod, Pallet: Integer;
              SQL_Limit: String = '');
*)
    procedure ReopenVSPalletEntradasValidas(Qry: TmySQLQuery; Pallet: Integer);
(*
    procedure ReopenVSPWEAtu(Qry: TmySQLQuery; MovimCod, Controle, TemIMEIMrt:
              Integer);
*)
(*
    procedure ReopenVSPWEDst(Qry: TmySQLQuery; MovimCod, Controle, TemIMEIMrt:
              Integer; SQL_Limit: String = '');
*)
(*
    procedure ReopenVSPWEOriIMEI(Qry: TmySQLQuery; MovimCod, Controle: Integer;
              SQL_Limit: String = '');
    procedure ReopenVSPWEOriPallet(Qry: TmySQLQuery; MovimCod, Pallet: Integer;
              SQL_Limit: String = '');
*)
    function  SQL_MovIDeNiv_Pos_All(): String;
    function  TextoDeEstqMovimID(EstqMovimID: TEstqMovimID): String;

    //
{
    procedure ReopenVSPallet(Qry: TmySQLQuery; EdPallet: TdmkEditCB;
              CBPallet: TdmkDBLookupComboBox; Empresa(*, Fornece, GraGruX*):
              Integer);
}
{
    function  EncerraPalletOld(VSPallet, CacCod: Integer; EncerrandoTodos: Boolean;
              FromBox: Variant; MovimIDGer: TEstqMovimID; Pergunta, Encerra,
              FromRcl: Boolean): Boolean;
}
    procedure ReopenVSMovDif(Qry: TmySQLQuery; Controle: Integer);
    procedure ReopenVSSubPrdIts(Qry: TmySQLQuery; GSPInnNiv2, TemIMEIMrt,
              Controle: Integer);
    procedure ReopenVSXxxOris(QrVSXxxCab, QrVSXxxOriIMEI, QrVSXxxOriPallet:
              TmySQLQuery; Controle, Pallet: Integer; MovimNiv: TEstqMovimNiv);
    procedure ReopenVSXxxExtra(Qry: TmySQLQuery; Codigo, (*Controle,*) TemIMEIMrt:
              Integer; MovimID, DstMovID: TEstqMovimID; MovimNiv: TEstqMovimNiv);
    function  VerificaBalanco(MesesAntes: Integer): Integer;
    function  VerificaGraGruXCouSemNivel1(): Integer;
    procedure VerificaGSP(Campo: String; MovimID: TEstqMovimID);
    function  VerificaFornecMO(PB: TProgressBar; LaAviso1, LaAviso2: TLabel): Boolean;
    procedure InfoRegIntInVMI(Campo: String; VMI, Inteiro: Integer; Qry:
              TmySQLQuery);
    procedure InfoReqMovEstq(VMI, ReqMovEstq: Integer; Qry: TmySQLQuery);
    procedure InfoPalletVMI(VMI, Pallet: Integer; Qry: TmySQLQuery);
    procedure InfoStqCenLoc(VMI, StqCenLoc: Integer; Qry: TmySQLQuery);
    procedure DefinirAscendenciasDeIMEIsDeMovimCod(IMEIAtu, MovimCod: Integer;
              MovimNivSrc, MovimNivDst: TEstqMovimNiv);
    function  GeraSQL_Pall(SQL_Flds, SQL_Left, SQL_Where, SQL_Group:
              String; Tab: TTabToWork; TemIMEIMrt: Integer = 0): String;
    function  HabilitaMenuItensVSAberto(Qry: TmySQLQuery; Data: TDateTime;
              MenuItens: array of TMenuItem): Boolean;
    procedure VerificaAtzCaleados(PB: TProgressBar; LaAviso1, LaAviso2: TLabel);
    procedure VerificaAtzCurtidos(PB: TProgressBar; LaAviso1, LaAviso2: TLabel);
    procedure VerificaAtzCalDTA(PB: TProgressBar; LaAviso1, LaAviso2: TLabel);
    procedure VerificaAtzCalPDA(PB: TProgressBar; LaAviso1, LaAviso2: TLabel);
    procedure VerificaCadastroVSEntiMPIncompleto();
    function  VerificaCalCurDstGGX(LaAviso1, LaAviso2: TLabel;
              PB1: TProgressBar): Boolean;
    function  VerificaCliForEmpty(): Boolean;
    function  VerificaStqCenLocEmpty(): Boolean;
    function  VerificaDatasDeEncerradasAposAbertura(): Boolean;
    function  VerificaOpeSemOperacao(): Boolean;
    procedure VerificaInconsistencias(PB1: TProgressBar;
              LaAviso1, LaAviso2: TLabel);
    procedure CorrigeArtigoOpeEmDiante(IMEI, GGXAtu: Integer);
    procedure ImportaDadosNFeCab(QuemEmit: Integer; EdEmit: TdmkEditCB;
              CBEmit: TdmkDBLookupComboBox; EdDest: TdmkEditCB;
              CBDest: TdmkDBLookupComboBox; TPEmi: TdmkEditDateTimePicker;
              EdEmi: TDmkEdit; TPSaiEnt: TdmkEditDateTimePicker;
              EdSaiEnt: TDmkEdit; EdSerie, EdnNF: TDmkEdit);
    function  SelecionaStatusVSVmcWrn(Query: TmySQLQuery): Boolean;
    function  ObtemTipoEstqIMEI(IMEI: Integer; FldArM2, FldPesoKg, FldPecas,
              FldGGX, FldCtrl: String): Integer;
    function  ObtemTipoEstqIMEI_Fast(IMEI: Integer; FldArM2, FldPesoKg, FldPecas,
              FldGGX, FldCtrl: String): Integer;
    procedure ReopenVSPaMulIts(QrVSPaMulIts: TmySQLQuery; TemIMEIMrt, MovimCod,
              Controle: Integer);
    function  InformaIxx(DBGIMEI: TDBGrid; QrIMEI: TmySQLQuery; FldIMEI:
              String): Boolean;
    function  VerSeCriaCadNaTabela(GraGruX: Integer; Tabela: String): Boolean;
    function  RegistrosComProblema(Empresa, GraGruX: Integer; LaAviso1, LaAviso2: TLabel): Boolean;

  end;

var
  VS_PF: TUnVS_PF;

implementation

uses DmkDAC_PF, Module, MyDBCheck, ModuleGeral, CfgCadLista, UMySQLModule,
  MyListas, UnGOTOy, UnMyObjects, OpcoesBlueDerm,
  CreateVS, ModVS, ModVS_CRC, VSClassifOneNew,
  VSClassifOneQnz, VSGerArtCab, VSInnCab, VSOutCab, (*VSOutIts,*) VSPallet, VSMovImp,
  VSNatcad, VSRibCad, VSRibCla, VSClaArtPrpMDz, VSClaArtPrpQnz, VSPalletAdd,
  VSAjsCab, VSImpIMEI, VSImpPallet, VSBxaCab, VSRclCab, VSProCal, VSProCur,
  VSRclArtSel, VSImpPackList, VSImpClaIMEI, VSRMPPsq, VSOpeCab, VSRibOpe,
  VSClaArtSelA, VSClaArtSelB, VSMod, VSGerRclCab, VSGerClaCab, VSMovIts,
  VSImpPrevia, VSImpRecla, VSDsnCab, VSPaCRIts, VSMovItsAlt, VSFchGerCab,
  VSFchRslCab, VSPalInClaRcl, VSReclassPrePal, VSRclArtPrpNew,
  (*VSReclassifOneOld,*) VSReclassifOneNew, VSOcGerCab, VSEqzCab, VSPlCCab,
  VSImpClaFichaRMP, VSImpClaFichaRMP2, VSCfgEqzCb, (*VSClassifOneRetFichaRMP,*) VSClassifOneRetIMEI,
  // Temporarios
  GraTabAppSel, GraGru1CorTam, GraGruAtiCorTam, ModProd,
  // Fim temporarios
  {$IfNDef semNFe_v0000}
  NFe_PF,
  {$EndIf}
  UnVS_EFD_ICMS_IPI,
  VSExBCab, GraGruY, VSEntiMP, VSImpClaIMEIs, VSCGICab, VSWetEnd,
  VSFinCla, VSRepMer, VSPedCab, VSPWECab, VSPalletPWEAdd, VSImpMatNoDef, VSImpFluxo,
  VSImpCompraVenda2, VSDivCouMeio, VSPesqSeqPeca, VSArvoreArtigos, VSDvlCab,
  VSRtbCab, VSSubPrd, VSPaMulCab, VSPaMulCabR, VSReqMovEstq, VSReqMov, VSReqDiv,
  VSMovItbAdd, VSMovItbAdd3, VSImpHistorico2, VSImpNotaMPAG, VSImpMartelo,
  GraGruYIncompleto, VSImpEstoque, VSPrePalCab, VSImpEstqEm, VSImpHistPall,
  VSTrfLocCab, VSOutNfeCab, VSOutNfeIts, VSCorrigeMulFrn, VSCorrigeMovimTwn,
  VSCorrigeSN2Orfao, EntiIncompletos, VSEmitCus, VSImpClaMulRMP,
  VSClassifOneNw2, VSClassifOneNw3, VSReclassifOneNw3, VSPwdDd,
  VSCalCab, VSCurCab, VSOutPeso, VSImpRequis, VSImpEmProcBH,
  VSImpRendPWE, VSGruGGX, Operacoes, VSCOPCab, VSPMOCab, VSCouCal, VSCouCur,
  VSComparaCacIts, GetValor, VSCfgMovEFD, SelOnStringGrid, VSImpResultVS,
  UnMyVCLRef, VSMOPWEGer, VSMOEnvEnv, VSMOEnvRet, VSImpMOEnvRet, VSImpMOEnvRet2, VSInvNFe,
  VSNatPDA, VSCouDTA, VSPSPCab, VSPSPPro, VSPSPEnd, VSRRMCab, VSCPMRSBER,
  XXDataEFDData, VSImpOrdem, VSDtHrSerNumNFe, VSArtCab, VSVmcObs, VSCPMRSBCb,
  VSMovCab, VSInfInn, VSInfMov, VSInfOut, VSLstPal, VSInfPal, VSPalletManual,
  VSIxx, VSEstqCustoIntegr, VSMovimID, VSHide, GraGruYIncorpora, VSMOEnvSel,
  VSMOEnvAvu, VSMOEnvAvuGer, VSMOEnvEnvGer, VSMOEnvCTeGer, VSMOEnvCTeImp,
  VSImpProducao, VSNatCon, VSConCab, VSNatInC,
  VSInnNatPend, VSGerArtDdImpBar, VSGerArtDdImpCur, VSGerArtDdImpAll,
  VSImpKardex5, VSImpKardex4, VSExcCab, VSGerArtMarcaImpBar,
  VSimpSdoCorret, VSCustosProdu, OpcoesVSSifDipoa,
  VSInnSubPrdIts, VSCalSubPrdIts, VSInnSubPrdItsMul, VSCalSubPrdItsMul;

{ TUnVS_PF }


(*
procedure TUnVS_PF.MostraFormVSClassifOneRetFichaRMP(Pallet1, Pallet2, Pallet3, Pallet4,
Pallet5, Pallet6: Integer; Form: TForm);
begin
  if DBCheck.CriaFm(TFmVSClassifOneRetFichaRMP, FmVSClassifOneRetFichaRMP, afmoNegarComAviso) then
  begin
    FmVSClassifOneRetFichaRMP.FForm := Form;
    FmVSClassifOneRetFichaRMP.FPallet1 := Pallet1;
    FmVSClassifOneRetFichaRMP.FPallet2 := Pallet2;
    FmVSClassifOneRetFichaRMP.FPallet3 := Pallet3;
    FmVSClassifOneRetFichaRMP.FPallet4 := Pallet4;
    FmVSClassifOneRetFichaRMP.FPallet5 := Pallet5;
    FmVSClassifOneRetFichaRMP.FPallet6 := Pallet6;
    //
    FmVSClassifOneRetFichaRMP.ShowModal;
    //
    FmVSClassifOneRetFichaRMP.Destroy;
  end;
end;
*)

procedure TUnVS_PF.MostraFormVSCouCal(GraGruX: Integer; Edita: Boolean);
begin
  if DBCheck.CriaFm(TFmVSCouCal, FmVSCouCal, afmoNegarComAviso) then
  begin
    if GraGruX <> 0 then
    begin
      FmVSCouCal.LocCod(GraGruX, GraGruX);
      if FmVSCouCal.QrVSCouCalGraGruX.Value = GraGruX then
        FmVSCouCal.AlteraMP1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSCouCal.FSeq := 1;
    FmVSCouCal.ShowModal;
    FmVSCouCal.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSCouCur(GraGruX: Integer; Edita: Boolean);
begin
  if DBCheck.CriaFm(TFmVSCouCur, FmVSCouCur, afmoNegarComAviso) then
  begin
    if GraGruX <> 0 then
    begin
      FmVSCouCur.LocCod(GraGruX, GraGruX);
      if FmVSCouCur.QrVSCouCurGraGruX.Value = GraGruX then
        FmVSCouCur.AlteraMP1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSCouCur.FSeq := 1;
    FmVSCouCur.ShowModal;
    FmVSCouCur.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSCouDTA(GraGruX: Integer; Edita: Boolean);
begin
  if DBCheck.CriaFm(TFmVSCouDTA, FmVSCouDTA, afmoNegarComAviso) then
  begin
    if GraGruX <> 0 then
    begin
      FmVSCouDTA.LocCod(GraGruX, GraGruX);
      if FmVSCouDTA.QrVSCouDTAGraGruX.Value = GraGruX then
        FmVSCouDTA.AlteraArtigoClassificadoAtual1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSCouDTA.FSeq := 1;
    FmVSCouDTA.ShowModal;
    FmVSCouDTA.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSCPMRSBCb();
begin
  if DBCheck.CriaFm(TFmVSCPMRSBCb, FmVSCPMRSBCb, afmoNegarComAviso) then
  begin
{
    if Codigo <> 0 then
    begin
      FmVSCPMRSBCb.LocCod(Codigo, Codigo);
    end;
    //
}
    FmVSCPMRSBCb.ShowModal;
    FmVSCPMRSBCb.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSCurCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSCurCab, FmVSCurCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSCurCab.LocCod(Codigo, Codigo);
    end;
    //
    FmVSCurCab.ShowModal;
    FmVSCurCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSCustosProdu;
begin
  if DBCheck.CriaFm(TFmVSCustosProdu, FmVSCustosProdu, afmoNegarComAviso) then
  begin
    FmVSCustosProdu.ShowModal;
    FmVSCustosProdu.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSDsnCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSDsnCab, FmVSDsnCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSDsnCab.LocCod(Codigo, Codigo);
    FmVSDsnCab.ShowModal;
    FmVSDsnCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSDvlCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSDvlCab, FmVSDvlCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSDvlCab.LocCod(Codigo, Codigo);
      //FmVSDvlCab.ReopenVSPaClaCab();
    end;
    //
    FmVSDvlCab.ShowModal;
    FmVSDvlCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSEmitCus(Pesagem, Empresa, TipoCouro, Formula:
  Integer; BxaEstqVS: TBxaEstqVS; DataEmis: TDateTime);
begin
  if DBCheck.CriaFm(TFmVSEmitCus, FmVSEmitCus, afmoNegarComAviso) then
  begin
    FmVSEmitCus.ImgTipo.SQLType        := stIns;
    //
    FmVSEmitCus.EdCodigo.ValueVariant  := Pesagem;
    FmVSEmitCus.EdEmpresa.ValueVariant := Empresa;
    FmVSEmitCus.RGTipoCouro.ItemIndex  := TipoCouro;
    FmVSEmitCus.FFormula               := Formula;
    FmVSEmitCus.FBxaEstqVS             := BxaEstqVS;
    FmVSEmitCus.FDataEmis              := DataEmis;
    //
    FmVSEmitCus.ShowModal;
    FmVSEmitCus.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSEmRibDTA(Codigo, Controle: Integer);
const
  sProcName = 'VS_PF.MostraFormVSEmRibDTA()';
var
  Qry: TmySQLQuery;
  MovimID: TEstqMovimID;
  CurCodigo, CurControle: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT RmsMovID, RmsNivel1, RmsNivel2 ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE Controle=' + Geral.FF0(Controle),
    '']);
    //
    if (Qry.RecordCount > 0) and (Controle <> 0) then
    begin
      MovimID  := TEstqMovimID(Qry.FieldByName('RmsMovID').AsInteger);
      CurCodigo   := Qry.FieldByName('RmsNivel1').AsInteger;
      CurControle := Qry.FieldByName('RmsNivel2').AsInteger;
      if MovimID = TEstqMovimID.emidEmProcCur then
      begin
        MostraFormVSCurCab(CurCodigo);
      end else
        Geral.MB_Aviso('IME-I ' + Geral.FF0(Controle) + ' n�o � Couro em Ribeira DTA em ' + sProcName);
    end else
      Geral.MB_Aviso('IME-I ' + Geral.FF0(Controle) + ' n�o localizado em ' + sProcName);
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_PF.MostraFormVSEmRibPDA(Codigo, Controle: Integer);
const
  sProcName = 'VS_PF.MostraFormVSEmRibPDA()';
var
  Qry: TmySQLQuery;
  MovimID: TEstqMovimID;
  CalCodigo, CalControle: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT RmsMovID, RmsNivel1, RmsNivel2 ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE Controle=' + Geral.FF0(Controle),
    '']);
    //
    if (Qry.RecordCount > 0) and (Controle <> 0) then
    begin
      MovimID  := TEstqMovimID(Qry.FieldByName('RmsMovID').AsInteger);
      CalCodigo   := Qry.FieldByName('RmsNivel1').AsInteger;
      CalControle := Qry.FieldByName('RmsNivel2').AsInteger;
      if MovimID = TEstqMovimID.emidEmProcCal then
      begin
        MostraFormVSCalCab(CalCodigo);
      end else
        Geral.MB_Aviso('IME-I ' + Geral.FF0(Controle) + ' n�o � Couro em Ribeira PDA em ' + sProcName);
    end else
      Geral.MB_Aviso('IME-I ' + Geral.FF0(Controle) + ' n�o localizado em ' + sProcName);
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_PF.MostraFormVSEntiMP(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSEntiMP, FmVSEntiMP, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSEntiMP.ReopenVSEntiMP(Codigo);
    FmVSEntiMP.ShowModal;
    FmVSEntiMP.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSEqzCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSEqzCab, FmVSEqzCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSEqzCab.LocCod(Codigo, Codigo);
    FmVSEqzCab.ShowModal;
    FmVSEqzCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSEstqCustoIntegr(Empresa: Integer);
var
  Filial: Integer;
begin
  if DBCheck.CriaFm(TFmVSEstqCustoIntegr, FmVSEstqCustoIntegr, afmoNegarComAviso) then
  begin
    if Empresa <> 0 then
    begin
      Filial := DModG.ObtemFilialDeEntidade(Empresa);
      FmVSEstqCustoIntegr.EdEmpresa.ValueVariant := Filial;
      FmVSEstqCustoIntegr.CbEmpresa.KeyValue     := Filial;
    end;
    FmVSEstqCustoIntegr.ShowModal;
    FmVSEstqCustoIntegr.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSExBCab(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmVSExBCab, FmVSExBCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSExBCab.LocCod(Codigo, Codigo);
    end;
    FmVSExBCab.ShowModal;
    FmVSExBCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSExcCab(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmVSExcCab, FmVSExcCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSExcCab.LocCod(Codigo, Codigo);
    end;
    FmVSExcCab.ShowModal;
    FmVSExcCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSFchGerCab(SerieFch, Ficha: Integer);
begin
  if DBCheck.CriaFm(TFmVSFchGerCab, FmVSFchGerCab, afmoNegarComAviso) then
  begin
    //
    if Ficha <> 0 then
      FmVSFchGerCab.ReopenVSFchGerCab(SerieFch, Ficha);
    FmVSFchGerCab.ShowModal;
    FmVSFchGerCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSFchRslCab(SerieFch, Ficha: Integer);
begin
  if DBCheck.CriaFm(TFmVSFchRslCab, FmVSFchRslCab, afmoNegarComAviso) then
  begin
    if Ficha <> 0 then
      FmVSFchRslCab.ReopenLote(SerieFch, Ficha);
    FmVSFchRslCab.ShowModal;
    //
    FmVSFchRslCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSFinCla(GraGruX: Integer; Edita: Boolean;
  QryMul: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmVSFinCla, FmVSFinCla, afmoNegarComAviso) then
  begin
    //FmVSFinCla.PnNavi.Enabled := False;
    //FmVSFinCla.GB_L.Enabled := False;
    FmVSFinCla.FQryMul := QryMul;
    if GraGruX <> 0 then
    begin
      FmVSFinCla.LocCod(GraGruX, GraGruX);
      if FmVSFinCla.QrVSFinClaGraGruX.Value = GraGruX then
        FmVSFinCla.AlteraArtigoClassificadoAtual1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSFinCla.FSeq := 1;
    FmVSFinCla.ShowModal;
    FmVSFinCla.Destroy;
  end;
end;

procedure TUnVS_PF.AbreSQLListaNFesEmitidasVS(Query: TmySQLQuery; UsaSerie:
  Boolean; Serie, NFeIni, NFeFim: Integer; (*DataIni, DataFim: TDateTime;
  UsaDataIni, UsaDataFim: Boolean;*) SQLReturn: String);
  function SQL_NFes(FldSer, FldNum: String): String;
  var
    Ser, Ini, Fim: Integer;
  begin
    Ser := Serie; //Ed20Serie.ValueVariant;
    Ini := NFeIni; //Ed20NFeIni.ValueVariant;
    Fim := NFeFim; //Ed20NFeFim.ValueVariant;
    Result :='';
    if UsaSerie then //Ck20Serie.Checked then
      Result := Result + 'AND ' + FldSer + '=' + Geral.FF0(Ser);
    if (Ini <> 0) or (Fim > Ini) then
    begin
      if Ini = Fim then
        Result := Result + 'AND ' + FldNum + ' = ' + Geral.FF0(ini)
      else
        Result := Result +
        'AND ' + FldNum + ' BETWEEN ' + Geral.FF0(ini) + ' AND ' + Geral.FF0(Fim);
    end;
  end;
//var
  //SQL_Periodo,
  //SQL_NFesA, ATT_MovimID: String;
begin
{
  SQL_Periodo := dmkPF.SQL_Periodo('WHERE Data',
    //TP20DataIni.Date, TP20DataFim.Date, Ck20DataIni.Checked, Ck20DataFim.Checked);
    DataIni, DataFim, UsaDataIni, UsaDataFim);
  //
  ATT_MovimID := dmkPF.ArrayToTexto('MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
}
  //UnDmkDAC_PF.AbreMySQLQuery0(QrNFes, DModG.MyPID_DB, [
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS ' + CO_NFes_XX + ';',
  'CREATE TABLE ' + CO_NFes_XX + '',
  '',
  'SELECT "vsoutcab" Tabela, "NFV" Campo, ',
  'VSVmcWrn, VSVmcObs, VSVmcSeq, VSVmcSta,  "" TxtSaldo, ',
  'cab.NFeStatus cStat, cab.SerieV Serie, cab.NFV NFe, cab.MovimCod, 2.000 MovimID, ',
  'cab.DtVenda Data, cab.Cliente Terceiro, -cab.Pecas Pecas, ',
  '-cab.PesoKg PesoKg, -cab.AreaM2 AreaM2, -ValorT ValorT',
  'FROM ' + TMeuDB + '.vsoutcab cab',
  'WHERE cab.NFV > 0',
  SQL_NFes('SerieV', 'NFV'),
  '',
  'UNION ',
  '',
  'SELECT "vsoutnfecab" Tabela, "ide_nNF" Campo, ',
  'nfe.VSVmcWrn, nfe.VSVmcObs, nfe.VSVmcSeq, nfe.VSVmcSta,  "" TxtSaldo, ',
  'nfe.NFeStatus cStat, nfe.ide_serie Serie, nfe.ide_nNF NFe, cab.MovimCod, ',
  '2.000 MovimID, cab.DtVenda Data, ',
  'cab.Cliente Terceiro, -cab.Pecas Pecas, ',
  '-cab.PesoKg PesoKg, -cab.AreaM2 AreaM2, -ValorT ValorT',
  'FROM ' + TMeuDB + '.vsoutnfecab nfe ',
  'LEFT JOIN ' + TMeuDB + '.vsoutcab cab ON cab.Codigo=nfe.OriCod',
  'WHERE nfe.ide_nNF <> 0 ',
  'AND nfe.ide_nNF <> cab.NFV',
  'AND cab.MovimCod=nfe.MovimCod',
  SQL_NFes('nfe.ide_serie', 'nfe.ide_nNF'),
  '',
  'UNION ',
  '',
  'SELECT "vstrfloccab" Tabela, "ide_nNF" Campo, ',
  'VSVmcWrn, VSVmcObs, VSVmcSeq, VSVmcSta,  "" TxtSaldo, ',
  'NFeStatus cStat, ide_serie Serie, ide_nNF NFe, MovimCod, 25.000 MovimID, ',
  'DtVenda Data, Cliente Terceiro, ',
  'Pecas, PesoKg, AreaM2, ValorT',
  'FROM ' + TMeuDB + '.vstrfloccab ',
  'WHERE ide_nNF > 0',
  SQL_NFes('ide_serie', 'ide_nNF'),
  '',
  'UNION',
  '',
  'SELECT "vsoutnfecab" Tabela, "ide_nNF" Campo, ',
  'nfe.VSVmcWrn, nfe.VSVmcObs, nfe.VSVmcSeq, nfe.VSVmcSta,  "" TxtSaldo, ',
  'nfe.NFeStatus cStat, nfe.ide_serie Serie, nfe.ide_nNF NFe, cab.MovimCod, ',
  '25.000 MovimID, cab.DtVenda Data, ',
  'cab.Cliente Terceiro, cab.Pecas Pecas, ',
  'cab.PesoKg PesoKg, cab.AreaM2 AreaM2, ValorT ValorT',
  'FROM ' + TMeuDB + '.vsoutnfecab nfe ',
  'LEFT JOIN ' + TMeuDB + '.vstrfloccab cab ON cab.Codigo=nfe.OriCod',
  'WHERE nfe.ide_nNF <> 0 ',
  'AND cab.MovimCod=nfe.MovimCod',
  SQL_NFes('nfe.ide_serie', 'nfe.ide_nNF'),
  '',
  'UNION ',
  '',
  'SELECT "vsoutnfecab" Tabela, "ide_nNF" Campo, ',
  'nfe.VSVmcWrn, nfe.VSVmcObs, nfe.VSVmcSeq, nfe.VSVmcSta,  "" TxtSaldo, ',
  'nfe.NFeStatus cStat, nfe.ide_serie Serie, nfe.ide_nNF NFe, cab.MovimCod, ',
  '6.000 MovimID, cab.DtHrAberto Data, ',
  'vmi.FornecMO Terceiro, Pecas, PesoKg, AreaM2, ValorT',
  'FROM ' + TMeuDB + '.vsoutnfecab nfe ',
  'LEFT JOIN ' + TMeuDB + '.vsgerarta cab ON cab.Codigo=nfe.OriCod',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  '  ON vmi.MovimCod=cab.MovimCod ',
  'WHERE vmi.MovimNiv=13 AND vmi.MovimID=6',
  'AND cab.MovimCod=nfe.MovimCod',
  SQL_NFes('nfe.ide_serie', 'nfe.ide_nNF'),
  '',
  'UNION ',
  '',
  'SELECT "vsopecab" Tabela, "NFeRem" Campo, ',
  'VSVmcWrn, VSVmcObs, VSVmcSeq, VSVmcSta,  IF(PecasSrc<>0, ',
  '  IF(AreaSdoM2 > 0, CONCAT(AreaSdoM2, " m�"), ""), ',
  '  IF(PesoKgSdo >0, CONCAT(PesoKgSdo, " kg"), "")) TxtSaldo, ',
  'cab.NFeStatus cStat, cab.SerieRem Serie, cab.NFeRem NFe, cab.MovimCod, vmi.MovimID, ',
  'cab.DtHrAberto Data, vmi.FornecMO Terceiro, ',
  '-cab.PecasSrc Pecas, -cab.PesoKgSrc PesoKg, ',
  '-cab.AreaSrcM2 AreaM2, -ValorTSrc ValorT',
  'FROM ' + TMeuDB + '.vsopecab cab',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  '  ON vmi.MovimCod=cab.MovimCod ',
  'WHERE vmi.MovimNiv=8 AND vmi.MovimID=11',
  'AND cab.NFeRem >0',
  SQL_NFes('SerieRem', 'NFeRem'),
  ' ',
  'UNION ',
  ' ',
  'SELECT "vspwecab" Tabela, "NFeRem" Campo, ',
  'VSVmcWrn, VSVmcObs, VSVmcSeq, VSVmcSta,  IF(PecasSrc<>0, ',
  '  IF(AreaSdoM2 > 0, CONCAT(AreaSdoM2, " m�"), ""), ',
  '  IF(PesoKgSdo >0, CONCAT(PesoKgSdo, " kg"), "")) TxtSaldo, ',
  'cab.NFeStatus cStat, cab.SerieRem Serie, cab.NFeRem NFe, cab.MovimCod, vmi.MovimID, ',
  'cab.DtHrAberto Data, vmi.FornecMO Terceiro, ',
  '-cab.PecasSrc Pecas, -cab.PesoKgSrc PesoKg, ',
  '-cab.AreaSrcM2 AreaM2, -ValorTSrc ValorT ',
  'FROM ' + TMeuDB + '.vspwecab cab',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  '  ON vmi.MovimCod=cab.MovimCod ',
  'WHERE vmi.MovimNiv=21 AND vmi.MovimID=19',
  'AND cab.NFeRem >0 ',
  SQL_NFes('SerieRem', 'NFeRem'),
  ' ',
  'UNION ',
  ' ',
  'SELECT "vspspcab" Tabela, "NFeRem" Campo, ',
  'VSVmcWrn, VSVmcObs, VSVmcSeq, VSVmcSta,  IF(PecasSrc<>0, ',
  '  IF(AreaSdoM2 > 0, CONCAT(AreaSdoM2, " m�"), ""), ',
  '  IF(PesoKgSdo >0, CONCAT(PesoKgSdo, " kg"), "")) TxtSaldo, ',
  'cab.NFeStatus cStat, cab.SerieRem Serie, cab.NFeRem NFe, cab.MovimCod, vmi.MovimID, ',
  'cab.DtHrAberto Data, vmi.FornecMO Terceiro, ',
  '-cab.PecasSrc Pecas, -cab.PesoKgSrc PesoKg, ',
  '-cab.AreaSrcM2 AreaM2, -ValorTSrc ValorT ',
  'FROM ' + TMeuDB + '.vspspcab cab',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  '  ON vmi.MovimCod=cab.MovimCod ',
  'WHERE vmi.MovimNiv=50 AND vmi.MovimID=32',
  'AND cab.NFeRem >0 ',
  SQL_NFes('SerieRem', 'NFeRem'),
  ' ',
  'UNION ',
  ' ',
  'SELECT "vsrrmcab" Tabela, "NFeRem" Campo, ',
  'VSVmcWrn, VSVmcObs, VSVmcSeq, VSVmcSta,  IF(PecasSrc<>0, ',
  '  IF(AreaSdoM2 > 0, CONCAT(AreaSdoM2, " m�"), ""), ',
  '  IF(PesoKgSdo >0, CONCAT(PesoKgSdo, " kg"), "")) TxtSaldo, ',
  'cab.NFeStatus cStat, cab.SerieRem Serie, cab.NFeRem NFe, cab.MovimCod, vmi.MovimID, ',
  'cab.DtHrAberto Data, vmi.FornecMO Terceiro, ',
  '-cab.PecasSrc Pecas, -cab.PesoKgSrc PesoKg, ',
  '-cab.AreaSrcM2 AreaM2, -ValorTSrc ValorT ',
  'FROM ' + TMeuDB + '.vsrrmcab cab',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  '  ON vmi.MovimCod=cab.MovimCod ',
  'WHERE vmi.MovimNiv=55 AND vmi.MovimID=33',
  'AND cab.NFeRem >0 ',
  SQL_NFes('SerieRem', 'NFeRem'),
  ' ',
  'UNION ',
  ' ',
  'SELECT "vsinncab" Tabela, "ide_nNF" Campo, ',
  'VSVmcWrn, VSVmcObs, VSVmcSeq, VSVmcSta,  "" TxtSaldo, ',
  'NFeStatus cStat, ide_serie Serie, ide_nNF NFe, MovimCod, 1.000 MovimID, ',
  'DtCompra Data, Fornecedor Terceiro, ',
  'Pecas, PesoKg, AreaM2, ValorT',
  'FROM ' + TMeuDB + '.vsinncab ',
  'WHERE ide_nNF > 0',
  SQL_NFes('ide_serie', 'ide_nNF'),
  ' ',
  'UNION ',
  ' ',
  'SELECT "vsinnnfs" Tabela, "ide_nNF" Campo, ',
  'nnn.VSVmcWrn, nnn.VSVmcObs, nnn.VSVmcSeq, nnn.VSVmcSta,  ',
  '"" TxtSaldo, nnn.NFeStatus cStat, ',
  'nnn.ide_serie Serie, nnn.ide_nNF NFe, ',
  'cab.MovimCod, 1.000 MovimID, ',
  'cab.DtCompra Data, cab.Fornecedor Terceiro, ',
  'nnn.Pecas, nnn.PesoKg, nnn.AreaM2, nnn.ValorT',
  'FROM ' + TMeuDB + '.vsinnnfs nnn',
  'LEFT JOIN ' + TMeuDB + '.vsinncab cab ON cab.Codigo=nnn.Codigo',
  'WHERE nnn.ide_nNF > 0',
  SQL_NFes('ide_serie', 'ide_nNF'),
  ' ',
  'UNION ',
  ' ',
  'SELECT "vsplccab" Tabela, "ide_nNF" Campo,  ',
  'VSVmcWrn, VSVmcObs, VSVmcSeq, VSVmcSta,  "" TxtSaldo, ',
  'NFeStatus cStat, ide_serie Serie, ide_nNF NFe, MovimCod, 16.000 MovimID, ',
  'DtCompra Data, Fornecedor Terceiro, ',
  'Pecas, PesoKg, AreaM2, ValorT',
  'FROM ' + TMeuDB + '.vsplccab ',
  'WHERE ide_nNF > 0',
  SQL_NFes('ide_serie', 'ide_nNF'),
  ' ',
  'UNION ',
  ' ',
  'SELECT "vsdvlcab" Tabela, "ide_nNF" Campo,  ',
  'VSVmcWrn, VSVmcObs, VSVmcSeq, VSVmcSta,  "" TxtSaldo, ',
  'NFeStatus cStat, ide_serie Serie, ide_nNF NFe, MovimCod, 21.000 MovimID, ',
  'DtCompra Data, Fornecedor Terceiro, ',
  'Pecas, PesoKg, AreaM2, ValorT',
  'FROM ' + TMeuDB + '.vsdvlcab ',
  'WHERE ide_nNF > 0',
  SQL_NFes('ide_serie', 'ide_nNF'),
  ' ',
  'UNION ',
  ' ',
  'SELECT "vsrtbcab", "ide_nNF" Campo,  ',
  'VSVmcWrn, VSVmcObs, VSVmcSeq, VSVmcSta,  "" TxtSaldo, ',
  'NFeStatus cStat, ide_serie Serie, ide_nNF NFe, MovimCod, 22.000 MovimID, ',
  'DtCompra Data, Fornecedor Terceiro, ',
  'Pecas, PesoKg, AreaM2, ValorT',
  'FROM ' + TMeuDB + '.vsrtbcab ',
  'WHERE ide_nNF > 0',
  SQL_NFes('ide_serie', 'ide_nNF'),
  '',
  '',
  'UNION ',
  ' ',
  'SELECT "vsinvnfe", "ide_nNF" Campo,   ',
  'VSVmcWrn, VSVmcObs, VSVmcSeq, VSVmcSta,  "" TxtSaldo, ',
  'NFeStatus cStat, ide_serie Serie, ide_nNF NFe, MovimCod,  ',
  'MovimID + 0.000 MovimID, Data, Terceiro,  ',
  '0.000 Pecas, 0.000 PesoKg, 0.00 AreaM2, 0.00 ValorT ',
  'FROM ' + TMeuDB + '.vsinvnfe  ',
  'WHERE ide_nNF > 0 ',
  ' ',
  'ORDER BY NFe DESC  ',
  ' ',
  '; ',
  '',
(*
  'SELECT nfm.Nome NO_cStat, nfe.*, ',
  ATT_MovimID,
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
  'FROM ' + CO_NFes_XX + ' nfe',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=nfe.Terceiro',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.nfemsg nfm ON nfm.Codigo=nfe.cStat ',
  SQL_Periodo,
  'ORDER BY NFe DESC, Data DESC',
*)
  SQLReturn,
  ' ',
  '']);
  //
  //Geral.MB_Teste(Query);
end;

function TUnVS_PF.AdicionarNovosVS_emin(): Boolean;
const
////////////////////////////////////////////////////////////////////////////////
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  Procedure criada somente para marcar lugares para implementar codigo
///  quando criar mais TEstqMovimNiv.emin....
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
////////////////////////////////////////////////////////////////////////////////
  sEstqMovimNiv: array[0..MaxEstqMovimNiv] of string = (
    '00', // 00
    '01', // 01
    '02', // 02
    '03', // 03
    '04', // 04
    '05', // 05
    '06', // 06
    '07', // 07
    '08', // 08
    '09', // 09
    '10', // 10
    '11', // 11
    '12', // 12
    '13', // 13
    '14', // 14
    '15', // 15
    '16', // 16
    '17', // 17
    '18', // 18
    '19', // 19
    '20', // 20
    '21', // 21
    '22', // 22
    '23', // 23
    '24', // 24
    '25', // 25
    '26', // 26
    '27', // 27
    '28', // 28
    '29', // 29
    '30', // 30
    '31', // 31
    '32', // 32
    '33', // 33
    '34', // 34
    '35', // 35
    '36', // 36
    '37', // 37
    '38', // 38
    '39', // 39
    '40', // 40
    '41', // 41
    '42', // 42
    '43', // 43
    '44', // 44
    '45', // 45
    '46', // 46
    '47', // 47
    '48', // 48
    '49', // 49
    '50', // 50
    '51', // 51
    '52', // 52
    '53', // 53
    '54', // 54
    '55', // 55
    '56', // 56
    '57', // 57
    '58', // 58
    '59', // 59
    '60', // 60
    '61', // 61
    '62', // 62
    '63', // 63
    '64', // 64
    '65', // 65
    '66', // 66
    '67', // 67
    '68', // 68
    '69' // 69
  );
////////////////////////////////////////////////////////////////////////////////
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  Procedure criada somente para marcar lugares para implementar codigo
///  quando criar mais TEstqMovimNiv.emin....
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
////////////////////////////////////////////////////////////////////////////////
begin
  Result := True;
end;

{
procedure TUnVS_PF.AlteraDataImei(Controle: Integer; DataHora: String);
begin
  quando desmarcar ciudar com DtCorrApo SPED EFD!!!
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  CO_DATA_HORA_VMI?], [
  'Controle'], [DataHora], [
  Controle], True);
end;
}

procedure TUnVS_PF.AlterarNotFluxo(Controle, NotFluxo: Integer);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  'NotFluxo'], [
  'Controle'], [NotFluxo], [
  Controle], True);
end;


procedure TUnVS_PF.ArrayDePallets(const Grade: TDBGrid; const QryPallet:
  TIntegerField; var PallArr: TPallArr);
var
  I, N, Empresa: Integer;
begin
  N := 0;
  with Grade.DataSource.DataSet do
  for I := 0 to Grade.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(Grade.SelectedRows.Items[I]));
    GotoBookmark(Grade.SelectedRows.Items[I]);
    N := N + 1;
    SetLength(PallArr, N);
    PallArr[N - 1] := QryPallet.Value;
  end;
end;

function TUnVS_PF.AtualizaArtigosDePallets(PB1: TProgressBar;
  LaAviso1, LaAviso2: TLabel): Integer;
var
  Qry: TmySQLQuery;
  GraGruX, Controle, Itens: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Verificando necessidade de alterar artigos');
  Screen.Cursor := crHourGlass;
  try
  Result := 0;
  Qry := TmySQLQuery.Create(DMod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
(*
    'DROP TABLE IF EXISTS _TESTE_PAL_GGX1_; ',
    'CREATE TABLE _TESTE_PAL_GGX1_ ',
    'SELECT Controle, Pallet, GraGruX  ',
    'FROM ' + TMeuDB + '.v s m o v i t s ',
    'WHERE Pallet <> 0  ',
    'AND Pecas > 0; ',
    ' ',
    'DROP TABLE IF EXISTS _TESTE_PAL_GGX2_; ',
    'CREATE TABLE _TESTE_PAL_GGX2_ ',
    'SELECT vm2.Controle, vm2.Pallet, vm2.GraGruX  ',
    'FROM ' + TMeuDB + '.v s m o v i t s vm2 ',
    'LEFT JOIN _TESTE_PAL_GGX1_ vm1 ON vm2.SrcNivel2=vm1.Controle  ',
    '; ',
    ' ',
    'SELECT 1 SrcDst, vm1.Controle, pal.GraGruX ',
    'FROM ' + TMeuDB + '.vspalleta pal ',
    'LEFT JOIN _TESTE_PAL_GGX1_ vm1 ON pal.Codigo=vm1.Pallet ',
    'WHERE vm1.GraGruX <> pal.GraGruX ',
    ' ',
    'UNION ',
    ' ',
    'SELECT 2 SrcDst, vm2.Controle, pal.GraGruX ',
    'FROM ' + TMeuDB + '.vspalleta pal ',
    'LEFT JOIN _TESTE_PAL_GGX2_ vm2 ON pal.Codigo=vm2.Pallet ',
    'WHERE vm2.GraGruX <> pal.GraGruX ',
*)
    'DROP TABLE IF EXISTS _TESTE_PAL_GGX1_;  ',
    'CREATE TABLE _TESTE_PAL_GGX1_  ',
    'SELECT Controle, Pallet, GraGruX,  ',
    'MovimID, MovimNiv   ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
    'WHERE Pallet <> 0   ',
    'AND Pecas > 0;  ',
    '  ',
    'DROP TABLE IF EXISTS _TESTE_PAL_GGX2_;  ',
    'CREATE TABLE _TESTE_PAL_GGX2_  ',
    'SELECT vm2.Controle, vm2.Pallet, vm2.GraGruX, ',
    'vm2.MovimID, vm2.MovimNiv   ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vm2  ',
    'LEFT JOIN _TESTE_PAL_GGX1_ vm1 ON vm2.SrcNivel2=vm1.Controle   ',
    // 2015-07-14
    'WHERE vm2.Pallet <> 0 ',
    'AND vm1.MovimNiv<>' + Geral.FF0(Integer(eminSorcClass)),
    'AND vm2.MovimID<>' + Geral.FF0(Integer(eminSorcClass)),
    // 2015-07-14 Fim
    ';  ',
    '  ',
    'SELECT 1 SrcDst, vm1.Controle, vm1.Pallet, ',
    'vm1.MovimID, vm1.MovimNiv, pal.GraGruX,  ',
    'vm1.GraGruX GGX_ERR, CONCAT(gg1p.Nome,  ',
    'IF(gtip.PrintTam=0, "", CONCAT(" ", gtip.Nome)),  ',
    'IF(gccp.PrintCor=0,"", CONCAT(" ", gccp.Nome)))  ',
    'NO_PRD_TAM_COR_P,  ',
    'CONCAT(gg1i.Nome,  ',
    'IF(gtii.PrintTam=0, "", CONCAT(" ", gtii.Nome)),  ',
    'IF(gcci.PrintCor=0,"", CONCAT(" ", gcci.Nome)))  ',
    'NO_PRD_TAM_COR_I  ',
    'FROM ' + TMeuDB + '.vspalleta pal  ',
    'LEFT JOIN _TESTE_PAL_GGX1_ vm1 ON pal.Codigo=vm1.Pallet  ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggxp ON ggxp.Controle=pal.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggcp ON ggcp.Controle=ggxp.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gccp ON gccp.Codigo=ggcp.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gtip ON gtip.Controle=ggxp.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1p ON gg1p.Nivel1=ggxp.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.unidmed   unmp ON unmp.Codigo=gg1p.UnidMed ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggxi ON ggxi.Controle=vm1.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggci ON ggci.Controle=ggxi.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcci ON gcci.Codigo=ggci.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gtii ON gtii.Controle=ggxi.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1i ON gg1i.Nivel1=ggxi.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.unidmed   unmi ON unmi.Codigo=gg1i.UnidMed ',
    ' ',
    'WHERE vm1.GraGruX <> pal.GraGruX  ',
    '  ',
    'UNION  ',
    '  ',
    'SELECT 2 SrcDst, vm2.Controle, vm2.Pallet, ',
    'vm2.MovimID, vm2.MovimNiv,  pal.GraGruX, ',
    'vm2.GraGruX GGX_ERR, CONCAT(gg1p.Nome,  ',
    'IF(gtip.PrintTam=0, "", CONCAT(" ", gtip.Nome)),  ',
    'IF(gccp.PrintCor=0,"", CONCAT(" ", gccp.Nome)))  ',
    'NO_PRD_TAM_COR_,  ',
    'CONCAT(gg1i.Nome,  ',
    'IF(gtii.PrintTam=0, "", CONCAT(" ", gtii.Nome)),  ',
    'IF(gcci.PrintCor=0,"", CONCAT(" ", gcci.Nome)))  ',
    'NO_PRD_TAM_COR_I   ',
    ' ',
    'FROM ' + TMeuDB + '.vspalleta pal  ',
    'LEFT JOIN _TESTE_PAL_GGX2_ vm2 ON pal.Codigo=vm2.Pallet ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggxp ON ggxp.Controle=pal.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggcp ON ggcp.Controle=ggxp.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gccp ON gccp.Codigo=ggcp.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gtip ON gtip.Controle=ggxp.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1p ON gg1p.Nivel1=ggxp.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.unidmed   unmp ON unmp.Codigo=gg1p.UnidMed ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggxi ON ggxi.Controle=vm2.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggci ON ggci.Controle=ggxi.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcci ON gcci.Codigo=ggci.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gtii ON gtii.Controle=ggxi.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1i ON gg1i.Nivel1=ggxi.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.unidmed   unmi ON unmi.Codigo=gg1i.UnidMed ',
    ' ',
    'WHERE vm2.GraGruX <> pal.GraGruX  ',
    ';  ',
    '']);
    //Geral.MB_Teste(Qry);
    PB1.Position := 0;
    PB1.Max := Qry.RecordCount;
    Qry.First;
    while not Qry.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      GraGruX   := Qry.FieldByName('GraGruX').AsInteger;
      Controle := Qry.FieldByName('Controle').AsInteger;
      //
      if GraGruX <> 0 then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
        'GraGruX'], ['Controle'], [GraGruX], [Controle], False) then
           Itens := Itens + 1;
      end;
      //
      Qry.Next;
    end;

////////////////////////////////////////////////////////////////////////////////

    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _TESTE_PAL_GGX1_; ',
    'CREATE TABLE _TESTE_PAL_GGX1_ ',
    'SELECT Controle, GraGruX  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
    'WHERE Pallet = 0  ',
    'AND Pecas > 0; ',
    ' ',
    'DROP TABLE IF EXISTS _TESTE_PAL_GGX2_; ',
    'CREATE TABLE _TESTE_PAL_GGX2_ ',
    'SELECT vm2.Controle, vm2.SrcNivel2, vm2.GraGruX  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vm2 ',
    'WHERE vm2.SrcNivel2 <> 0',
    '; ',
    ' ',
    'SELECT vm2.Controle, vm1.GraGruX ',
    'FROM _TESTE_PAL_GGX2_ vm2 ',
    'LEFT JOIN _TESTE_PAL_GGX1_ vm1 ON vm2.SrcNivel2=vm1.Controle',
    'WHERE vm2.GraGruX <> vm1.GraGruX ',
    '; ',
    '']);
    //Geral.MB_Teste(Qry);
    PB1.Position := 0;
    PB1.Max := Qry.RecordCount;
    Qry.First;
    while not Qry.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      GraGruX   := Qry.FieldByName('GraGruX').AsInteger;
      Controle := Qry.FieldByName('Controle').AsInteger;
      //
      if GraGruX <> 0 then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
        'GraGruX'], ['Controle'], [GraGruX], [Controle], False) then
           Itens := Itens + 1;
      end;
      //
      Qry.Next;
    end;

////////////////////////////////////////////////////////////////////////////////

    Result := Itens;
    if Itens > 0 then
    Geral.MB_Info(Geral.FF0(Itens) + ' IME-Is de ' + Geral.FF0(
    Itens) + ' tiveram seus reduzidos corrigidos pelo pallet! [4]');
  finally
    Qry.Free;
  end;
  finally
    Screen.Cursor := crDefault;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TUnVS_PF.AtualizaDtHrFimOpe_MovimID(MovimID: TEstqMovimID);
var
  Tabela: String;
  Qry1, Qry2: TmySQLQuery;
  MovimCod: Integer;
begin
  // Precisa Fazer MovimID 6 ????
  if MovimID = emidIndsXX then
    Exit;
  //////////////////////////////////////////////////////////////////////////////
  Tabela := ObtemNomeTabelaVSXxxCab(MovimID);
  //
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    Qry2 := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
      'SELECT Codigo, MovimCod  ',
      'FROM ' + Tabela,
      'WHERE IF(PecasMan<>0, PecasMan, -PecasSrc) <> 0 ',
      'AND PecasSdo <= 0 ',
      'AND DtHrFimOpe < "1900-01-01" ',
      '']);
      Qry1.First;
      while not Qry1.Eof do
      begin
        MovimCod := Qry1.FieldByName('MovimCod').AsInteger;
        //
        AtualizaDtHrFimOpe_MovimCod(MovimID, MovimCod);
        //
        Qry1.Next;
      end;
    finally
      Qry2.Free;
    end;
  finally
    Qry1.Free;
  end;
end;

procedure TUnVS_PF.AtualizaFornecedorBxa(IMEI: Integer);
begin
  DmModVS.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfSrcNiv2, IMEI,
  [emidForcado], [], []);
end;

procedure TUnVS_PF.AtualizaFornecedorCal(MovimCod: Integer);
begin
  DmModVS.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfMovCod, MovimCod,
  [emidEmProcCal], [eminSorcCal], [eminEmCalInn, eminDestCal, eminEmCalBxa]);
end;

procedure TUnVS_PF.AtualizaFornecedorCon(MovimCod: Integer);
begin
  DmModVS.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfMovCod, MovimCod,
  [emidEmProcCon], [eminSorcCon], [eminEmConInn, eminDestCon, eminEmConBxa]);
end;

procedure TUnVS_PF.AtualizaFornecedorCur(MovimCod: Integer);
begin
  DmModVS.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfMovCod, MovimCod,
  [emidEmProcCur], [eminSorcCur], [eminEmCurInn, eminDestCur, eminEmCurBxa]);
end;

procedure TUnVS_PF.AtualizaFornecedorExB(IMEI: Integer);
begin
  DmModVS.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfSrcNiv2, IMEI,
  [emidExtraBxa], [], []);
end;

procedure TUnVS_PF.AtualizaFornecedorInd(MovimCod: Integer);
begin
  DmModVS.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfMovCod, MovimCod,
  [emidIndsXX], [eminSorcCurtiXX], [eminDestCurtiXX, eminBaixCurtiXX]);
end;

procedure TUnVS_PF.AtualizaFornecedorPreRcl(MovimCod: Integer);
begin
  DmModVS.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfMovCod, MovimCod,
  [emidPreReclasse], [eminSorcPreReclas], [eminDestPreReclas]);
end;

procedure TUnVS_PF.AtualizaFornecedorPSP(MovimCod: Integer);
begin
  DmModVS.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfMovCod, MovimCod,
  [emidEmProcSP], [eminSorcPSP], [eminEmPSPInn, eminDestPSP, eminEmPSPBxa]);
end;

procedure TUnVS_PF.AtualizaFornecedorRclUni(MovimCod: Integer);
begin
  DmModVS.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfMovCod, MovimCod,
  [emidReclasXXUni], [eminSorcClass, eminSorcReclass], [eminDestClass, eminDestReclass]);
end;

procedure TUnVS_PF.AtualizaFornecedorResRcl(IMEI: Integer);
begin
  DmModVS.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfSrcNiv2, IMEI,
  [emidResiduoReclas], [], []);
end;

procedure TUnVS_PF.AtualizaFornecedorRRM(MovimCod: Integer);
begin
  DmModVS.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfMovCod, MovimCod,
  [emidEmProcSP], [eminSorcRRM], [eminEmRRMInn, eminDestRRM, eminEmRRMBxa]);
end;

procedure TUnVS_PF.AtualizaIMEIsPalletComGraGruXErrado(PB1: TProgressBar;
  LaAviso1, LaAviso2: TLabel);
var
  Qry: TmySQLQuery;
  Itens, GraGruX, Controle: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  Screen.Cursor := crHourGlass;
  try
    Itens := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT pal.GraGruX, vmi.Controle, vmi.GraGruX, vmi.MovimID, vmi.MovimNiv  ',
    'FROM vspalleta pal ',
    'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Pallet=pal.Codigo ',
    'WHERE pal.GraGruX<>vmi.GraGruX ',
    'AND Pecas > 0',
    'ORDER BY vmi.MovimID, vmi.MovimNiv, vmi.Controle ',
    '']);
    PB1.Position := 0;
    PB1.Max := Qry.RecordCount;
    Qry.First;
    while not Qry.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      GraGruX   := Qry.FieldByName('GraGruX').AsInteger;
      Controle := Qry.FieldByName('Controle').AsInteger;
      //
      if GraGruX <> 0 then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
        'GraGruX'], ['Controle'], [GraGruX], [Controle], False) then
           Itens := Itens + 1;
      end;
      //
      Qry.Next;
    end;
    Geral.MB_Info(Geral.FF0(Itens) + ' IME-Is de ' + Geral.FF0(
    Qry.RecordCount) + ' tiveram seus reduzidos corrigidos!');
  finally
    Screen.Cursor := crDefault;
    Qry.Free;
  end;
end;

procedure TUnVS_PF.AtualizaJmpGGX(MovimID: TEstqMovimID; Codigo: Integer);
const
  sProcName = 'VS_PF.AtualizaJmpGGX()';
var
  Qry: TmySQLQuery;
  MovimCod: Integer;
begin
  EXIT;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UndmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM vsmovcab ',
    'WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
    'AND CodigoID=' + Geral.FF0(Codigo),
    '']);
    MovimCod := Qry.FieldByName('Codigo').AsInteger;
    if MovimCod <> 0 then
    begin
      case MovimID of
        TEstqMovimID.emidEmProcCal: AtualizaVSCalCabGGxSrc(MovimCod);
        TEstqMovimID.emidEmProcCur: AtualizaVSCurCabGGxSrc(MovimCod);
        else Geral.MB_Erro('MovimID nao implementado em ' + sProcName);
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnVS_PF.AtualizaMovimCodDeVSMovCab(PB1: TProgressBar; LaAviso1,
  LaAviso2: TLabel): Integer;
var
  Qry: TmySQLQuery;
var
  Itens, Codigo, MovimCod: Integer;
  MovimID: TEstqMovimID;
begin
  Qry := TmySQLQuery.Create(Dmod);
  Screen.Cursor := crHourGlass;
  try
    Itens := 0;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT vmi.Codigo, vmi.MovimCod, vmi.MovimID ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
    'WHERE MovimID=15 ',  // Tem o problema do codigo 3 e 4 no cialeather
    'AND vmi.MovimCod NOT IN ( ',
    '  SELECT Codigo ',
    '  FROM vsmovcab ',
    ') ',
    '']);
    PB1.Position := 0;
    PB1.Max := Qry.RecordCount;
    Qry.First;
    while not Qry.Eof do
    begin
      Itens := Itens + 1;
      //
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      //
      Codigo         := Qry.FieldByName('Codigo').AsInteger;
      MovimID        := TEstqMovimID(Qry.FieldByName('MovimID').AsInteger);
      MovimCod       := Qry.FieldByName('MovimCod').AsInteger;
      //
      InsereVSMovCab(MovimCod, MovimID, Codigo);
      //
      Qry.Next;
    end;
    if Qry.RecordCount > 0 then
      Geral.MB_Info(Geral.FF0(Itens) + ' IME-Cs de ' + Geral.FF0(
      Qry.RecordCount) + ' tiveram seus registros inseridos!');
  finally
    Screen.Cursor := crDefault;
    Qry.Free;
  end;
  Result := Itens;
end;

procedure TUnVS_PF.AtualizaNotaVSCfgEqzCb(Codigo: Integer);
var
  Qry: TmySQLQuery;
  BasNotZero: Double;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(FatorEqz*BasNota) / SUM(FatorEqz) BaseNotaCab',
    'FROM vscfgeqzit ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    BasNotZero := Qry.FieldByName('BaseNotaCab').AsFloat;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscfgeqzcb', False, [
    'BasNotZero'], ['Codigo'], [BasNotZero], [Codigo], True);
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_PF.AtualizaOrigensReclassePalletErrado(PB1: TProgressBar;
  LaAviso1, LaAviso2: TLabel);
var
  Qry: TmySQLQuery;
  Itens, Pallet, Controle: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  Screen.Cursor := crHourGlass;
  try
    Itens := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vm2.Pallet, vm1.Controle ',
    'FROM ' + CO_SEL_TAB_VMI + ' vm1 ',
    'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vm2 ON vm1.SrcNivel2=vm2.Controle ',
    'AND vm2.Pallet <> vm1.Pallet ',
    'WHERE NOT (vm2.Controle IS NULL) ',
    'AND vm2.Pallet <> 0 ',
    'AND vm1.MovimNiv=6 ',
    'AND vm1.Pecas<0 ',
    'ORDER BY vm1.Controle ',
    '']);
    PB1.Position := 0;
    PB1.Max := Qry.RecordCount;
    Qry.First;
    while not Qry.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      Pallet   := Qry.FieldByName('Pallet').AsInteger;
      Controle := Qry.FieldByName('Controle').AsInteger;
      //
      if Pallet <> 0 then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
        'Pallet'], ['Controle'], [Pallet], [Controle], False) then
           Itens := Itens + 1;
      end;
      //
      Qry.Next;
    end;
    Geral.MB_Info(Geral.FF0(Itens) + ' IME-Is de ' + Geral.FF0(
    Qry.RecordCount) + ' tiveram seus pallets corrigidos!');
  finally
    Screen.Cursor := crDefault;
    Qry.Free;
  end;
end;

function TUnVS_PF.AtualizaOrigensReclasseSemGragruX(PB1: TProgressBar;
  LaAviso1, LaAviso2: TLabel): Integer;
var
  Qry: TmySQLQuery;
  Itens, GraGruX, Controle: Integer;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  Screen.Cursor := crHourGlass;
  try
    Itens := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.Codigo, vmi.MovimCod, vmi.Controle,  ',
    'vmi.MovimID, vmi.MovimNiv, vmi.SrcNivel2,  ',
    'vmi.DstNivel2, grc.GraGruX ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN vsgerrcla grc ON grc.MovimCod=vmi.MovimCod ',
    'WHERE vmi.MovimID=8  ',
    'AND vmi.MovimNiv=1  ',
    'AND vmi.GraGruX=0  ',
    '']);
    PB1.Position := 0;
    PB1.Max := Qry.RecordCount;
    Qry.First;
    while not Qry.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      GraGruX  := Qry.FieldByName('GraGruX').AsInteger;
      Controle := Qry.FieldByName('Controle').AsInteger;
      //
      if GraGruX <> 0 then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
        'GraGruX'], ['Controle'], [GraGruX], [Controle], False) then
           Itens := Itens + 1;
      end;
      //
      Qry.Next;
    end;
    Result := Qry.RecordCount;
    if Result > 0 then
      Geral.MB_Info(Geral.FF0(Itens) + ' IME-Is de ' + Geral.FF0(
      Qry.RecordCount) + ' tiveram seus reduzidos informados! [2]');
  finally
    Screen.Cursor := crDefault;
    Qry.Free;
  end;
end;

procedure TUnVS_PF.AtualizaOrigensReclasseSemPallet(PB1: TProgressBar;
LaAviso1, LaAviso2: TLabel);
var
  Qry: TmySQLQuery;
  Itens, Pallet, Controle: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  Screen.Cursor := crHourGlass;
  try
    Itens := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.Codigo, vmi.MovimCod, vmi.Controle, ',
    'vmi.MovimID, vmi.MovimNiv, vmi.SrcNivel2, ',
    'vmi.DstNivel2, grc.VSPallet  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'LEFT JOIN vsgerrcla grc ON grc.MovimCod=vmi.MovimCod',
    'WHERE vmi.MovimID=8 ',
    'AND vmi.MovimNiv=1 ',
    'AND vmi.Pallet=0 ',
    'ORDER BY SrcNivel2, MovimCod ',
    '']);
    PB1.Position := 0;
    PB1.Max := Qry.RecordCount;
    Qry.First;
    while not Qry.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      Pallet   := Qry.FieldByName('VSPallet').AsInteger;
      Controle := Qry.FieldByName('Controle').AsInteger;
      //
      if Pallet <> 0 then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
        'Pallet'], ['Controle'], [Pallet], [Controle], False) then
           Itens := Itens + 1;
      end;
      //
      Qry.Next;
    end;
    Geral.MB_Info(Geral.FF0(Itens) + ' IME-Is de ' + Geral.FF0(
    Qry.RecordCount) + ' tiveram seus pallets informados!');
  finally
    Screen.Cursor := crDefault;
    Qry.Free;
  end;
end;

function TUnVS_PF.AtualizaReclassesComPalletErrado(PB1: TProgressBar; LaAviso1,
  LaAviso2: TLabel): Integer;
var
  Qry: TmySQLQuery;
  Itens, Pallet, Controle: Integer;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  Screen.Cursor := crHourGlass;
  try
    Itens := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
(*
    'DROP TABLE IF EXISTS _TESTE_1_;',
    'CREATE TABLE _TESTE_1_',
    'SELECT Controle, Pallet',
    'FROM ' + TMeuDB + '.v s m o v i t s',
    'WHERE Pallet <> 0',
    'AND Pecas > 0;',
    '',
    'DROP TABLE IF EXISTS _TESTE_2_;',
    'CREATE TABLE _TESTE_2_',
    'SELECT Controle, Pallet, ',
    'SrcNivel2, DstNivel2, MovimID',
    'FROM ' + TMeuDB + '.v s m o v i t s',
    'WHERE SrcNivel2 <> 0',
    'AND Pecas < 0;',
    '',
    'SELECT t2.Controle, t1.Pallet',
    'FROM _TESTE_2_ t2',
    'LEFT JOIN _TESTE_1_ t1 ON t2.SrcNivel2 = t1.Controle',
    'WHERE t1.Pallet > 0',
    'AND t1.Pallet <> t2.Pallet',
    'ORDER BY t2.Controle',
*)
(*
    'DROP TABLE IF EXISTS _TESTE_1_;',
    'CREATE TABLE _TESTE_1_',
    'SELECT Controle, Pallet',
    'FROM ' + TMeuDB + '.v s m o v i t s',
    'WHERE Pallet <> 0',
    'AND Pecas > 0;',
    '',
    'DROP TABLE IF EXISTS _TESTE_2_;',
    'CREATE TABLE _TESTE_2_',
    'SELECT Controle, Pallet, ',
    'SrcNivel2, DstNivel2, MovimID',
    'FROM ' + TMeuDB + '.v s m o v i t s',
    'WHERE SrcNivel2 <> 0',
    'AND Pecas < 0;',
    '',
    'SELECT t2.*, t1.Pallet',
    'FROM _TESTE_2_ t2',
    'LEFT JOIN _TESTE_1_ t1 ON t2.SrcNivel2 = t1.Controle',
    'WHERE t1.Pallet > 0',
    'AND t1.Pallet <> t2.Pallet',
    'AND t2.SrcNivel2=t2.DstNivel2',
    'ORDER BY t1.Pallet',
*)
    'DROP TABLE IF EXISTS _TESTE_1_;',
    'CREATE TABLE _TESTE_1_',
    'SELECT Controle, Pallet, GraGruX',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '',
    'WHERE Pallet <> 0',
    'AND Pecas > 0;',
    '',
    'DROP TABLE IF EXISTS _TESTE_2_;',
    'CREATE TABLE _TESTE_2_',
    'SELECT Controle, Pallet, ',
    'SrcNivel2, DstNivel2, MovimID, GraGruX',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '',
    'WHERE SrcNivel2 <> 0',
    'AND Pecas < 0;',
    '',
    'SELECT t2.Controle, t1.Pallet',
    'FROM _TESTE_2_ t2',
    'LEFT JOIN _TESTE_1_ t1 ON t2.SrcNivel2 = t1.Controle',
    'WHERE t1.Pallet > 0',
    'AND t1.Pallet <> t2.Pallet',
    'ORDER BY t1.Pallet',
    '']);
    PB1.Position := 0;
    PB1.Max := Qry.RecordCount;
    Qry.First;
    while not Qry.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      Pallet   := Qry.FieldByName('Pallet').AsInteger;
      Controle := Qry.FieldByName('Controle').AsInteger;
      //
      if Pallet <> 0 then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
        'Pallet'], ['Controle'], [Pallet], [Controle], False) then
           Itens := Itens + 1;
      end;
      //
      Qry.Next;
    end;
    Result := Qry.RecordCount;
    if Result > 0 then
      Geral.MB_Info(Geral.FF0(Itens) + ' IME-Is de ' + Geral.FF0(
      Qry.RecordCount) + ' tiveram seus pallets corrigidos! [3]');
  finally
    Screen.Cursor := crDefault;
    Qry.Free;
  end;
end;

function TUnVS_PF.AtualizaReclassesSemPallet(PB1: TProgressBar; LaAviso1,
  LaAviso2: TLabel): Integer;
var
  Qry: TmySQLQuery;
  Itens, Pallet, Controle: Integer;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  Screen.Cursor := crHourGlass;
  try
    Itens := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.Controle, pra.VSPallet ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN vsparclitsa pri ON pri.VMI_Baix=vmi.Controle ',
    'LEFT JOIN vsparclcaba pra ON pra.Codigo=pri.Codigo ',
    'WHERE vmi.Pallet=0 ',
    'AND vmi.MovimID=8 ',
    'AND vmi.MovimNiv=1 ',
    'ORDER BY vmi.Controle ',
    '']);
    PB1.Position := 0;
    PB1.Max := Qry.RecordCount;
    Qry.First;
    while not Qry.Eof do
    begin
      MyObjects.UpdPB(PB1, LaAviso1, LaAviso2);
      Pallet   := Qry.FieldByName('VSPallet').AsInteger;
      Controle := Qry.FieldByName('Controle').AsInteger;
      //
      if Pallet <> 0 then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
        'Pallet'], ['Controle'], [Pallet], [Controle], False) then
           Itens := Itens + 1;
      end;
      //
      Qry.Next;
    end;
    Result := Qry.RecordCount;
    if Result > 0 then
      Geral.MB_Info(Geral.FF0(Itens) + ' IME-Is de ' + Geral.FF0(
      Qry.RecordCount) + ' tiveram seus pallets informados [1]!');
  finally
    Screen.Cursor := crDefault;
    Qry.Free;
  end;
end;

procedure TUnVS_PF.AtualizaSaldoVMI_SemOrig(VMI_Sorc, VSPallet: Integer);
var
  Qry: TmySQLQuery;
  Controle: Integer;
  Pecas, PesoKg, AreaM2,
  SdoVrtPeso, SdoVrtPeca, SdoVrtArM2: Double;
begin
  Controle := VMI_Sorc;
  if Controle = 0 then
    Exit;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2 ',
    'FROM vscacitsa ',
    'WHERE VSPallet=' + Geral.FF0(VSPallet),
    'AND VMI_Sorc=0 ',
    '']);
    //
    Pecas      := Qry.FieldByName('Pecas').AsFloat;
    PesoKg     := 0;
    AreaM2     := Qry.FieldByName('AreaM2').AsFloat;
    SdoVrtPeca := -Pecas;
    SdoVrtPeso := -PesoKg;
    SdoVrtArM2 := -AreaM2;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'Pecas', 'PesoKg', 'AreaM2',
    'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2'
    ], ['Controle'], [
    Pecas, PesoKg, AreaM2,
    SdoVrtPeca, SdoVrtPeso, SdoVrtArM2
    ], [Controle], True);
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_PF.AtualizaTemIMEIMrt1(Tabela: String; LaAviso1, LaAviso2:
  TLabel; Campo: String);
var
  FldIdx: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Atualizando cabe�alho: ' + Tabela);
  //
  if Trim(Campo) <> '' then
    FldIdx := Campo
  else
    FldIdx := 'MovimCod';
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE ' + Tabela,
  'SET TemIMEIMrt=1 ',
  'WHERE ' + FldIdx + ' IN ( ',
  '  SELECT DISTINCT MovimCod ',
  '  FROM vsmovitb ',
  ')',
  '']);
end;

procedure TUnVS_PF.AtualizaTemIMEIMrt2(Tabela: String; LaAviso1,
  LaAviso2: TLabel; Itens, Campo: String);
var
  FldIdx: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Atualizando cabe�alho: ' + Tabela);
  //
  if Trim(Campo) <> '' then
    FldIdx := Campo
  else
    FldIdx := 'MovimCod';
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE ' + Tabela,
  'SET TemIMEIMrt=1 ',
  'WHERE ' + FldIdx + ' IN (' + Itens + ')',
  '']);
end;

procedure TUnVS_PF.AtualizaTotaisVSPedCab(Codigo: Integer);
var
  Qry: TmySQLQuery;
  Pecas, PesoKg, AreaM2, AreaP2, Valor, Desco: Double;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    //'SUM(Valor) Valor, SUM(Desco) Desco ',
    'SUM(Valor) Valor, SUM(Descn) Desco ',
    'FROM vspedits',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    //
    Pecas          := Qry.FieldByName('Pecas').AsFloat;
    PesoKg         := Qry.FieldByName('PesoKg').AsFloat;
    AreaM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaP2         := Qry.FieldByName('AreaP2').AsFloat;
    Valor          := Qry.FieldByName('Valor').AsFloat;
    Desco          := Qry.FieldByName('Desco').AsFloat;

    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspedcab', False, [
    'Pecas', 'PesoKg',
    'AreaM2', 'AreaP2',
    'Valor', 'Desco'], [
    'Codigo'], [
    Pecas, PesoKg,
    AreaM2, AreaP2,
    Valor, Desco], [
    Codigo], True);
    //
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_PF.AtualizaVSPedIts_Vda(VSPedIts: Integer);
var
  Qry: TmySQLQuery;
  Controle, Status, NaoFinaliza: Integer;
  VdaPecas, VdaPesoKg, VdaAreaM2, VdaAreaP2: Double;
begin
  if VSPedIts = 0 then
    Exit;
    //
  Qry := TmySQLQuery.Create(Dmod);
  Screen.Cursor := crHourGlass;
  try
    Controle := VSPedIts;
    Status := Integer(TStausPedVda.spvLiberado);
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT NaoFinaliza ',
    'FROM vspedits ',
    'WHERE Controle=' + Geral.FF0(VSPedIts),
    '']);
    NaoFinaliza := Qry.Fields[0].AsInteger;
    if NaoFinaliza = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
      'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2 ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE PedItsVda=' + Geral.FF0(VSPedIts),
      '']);
      VdaPecas  := -Qry.FieldByName('Pecas').AsFloat;
      VdaPesoKg := -Qry.FieldByName('PesoKg').AsFloat;
      VdaAreaM2 := -Qry.FieldByName('AreaM2').AsFloat;
      VdaAreaP2 := -Qry.FieldByName('AreaP2').AsFloat;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspedits', False, [
      'VdaPecas', 'VdaPesoKg', 'VdaAreaM2', 'VdaAreaP2'
      ], ['Controle'], [
      VdaPecas, VdaPesoKg, VdaAreaM2, VdaAreaP2
      ], [Controle], True) then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Pecas, PesoKg, AreaM2, AreaP2 ',
        'FROM vspedits ',
        'WHERE Controle=' + Geral.FF0(VSPedIts),
        '']);
        if (Qry.FieldByName('AreaM2').AsFloat >= 0.01) and (VdaAreaM2 >= Qry.FieldByName('AreaM2').AsFloat) then
          Status := Integer(TStausPedVda.spvFinalizado)
        else
        if (Qry.FieldByName('PesoKg').AsFloat >= 0.01) and (VdaPesoKg >= Qry.FieldByName('PesoKg').AsFloat) then
          Status := Integer(TStausPedVda.spvFinalizado)
        else
        if (Qry.FieldByName('Pecas').AsFloat >= 0.01) and (VdaPecas >= Qry.FieldByName('Pecas').AsFloat) then
          Status := Integer(TStausPedVda.spvFinalizado);
        //
      end;
    end;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspedits', False, [
    'Status'], ['Controle'], [Status], [Controle], True);
  finally
    Screen.Cursor := crDefault;
    Qry.Free;
  end;
end;

procedure TUnVS_PF.AvisoID(MovimID: TEstqMovimID; ProcName: String);
begin
  Geral.MB_Erro(
  ' n�o implementado em "' + ProcName + '"');
end;

procedure TUnVS_PF.AvisoIDNiv(MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv;
  IMEI: Integer; ProcName: String);
begin
  Geral.MB_Erro('TEstqMovimID: ' + Geral.FF0(Integer(MovimID)) +
  ' combinado com o TEstqMovimNiv: ' + Geral.FF0(Integer(MovimNiv)) +
  ' n�o implementado em "' + ProcName + '"' + sLineBreak +
  'IMEI: ' + Geral.FF0(IMEI));
end;

procedure TUnVS_PF.AvisoNiv(MovimNiv: TEstqMovimNiv; ProcName: String);
begin
  Geral.MB_Erro('TEstqMovimNiv: ' + Geral.FF0(Integer(MovimNiv)) +
  ' n�o implementado em "' + ProcName + '"');
end;

procedure TUnVS_PF.BuscaDadosItemNF(EdItemNFe, EdGraGruX, EdPecas, EdPesoKg,
  EdAreaM2, EdAreaP2: TdmkEdit; CBGraGruX: TDBLookupComboBox; const MovimCod:
  Integer; var Buscou: Boolean; var GraGruX: Integer; var Pecas, PesoKg, AreaM2,
  AreaP2: Double);
var
  Qry: TmySQLQuery;
  ItemNFe: Integer;
  Item: String;
begin
  ItemNFe        := EdItemNFe.ValueVariant;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    ' SELECT vmi.ItemNFe, vmi.GraGruX,  ',
    '-SUM(vmi.Pecas) Pecas, -SUM(vmi.PesoKg) PesoKg,  ',
    '-SUM(vmi.AreaM2) AreaM2, -SUM(vmi.AreaP2) AreaP2   ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND Pecas + PesoKg + AreaM2 < 0 ',
    'GROUP BY vmi.ItemNFe  ',
    'ORDER BY vmi.ItemNFe ',
    '']);
    //Geral.MB_Teste(Qry);
    if Qry.RecordCount > 1 then
    begin
      Item := '1';
      if InputQuery('Item de NFe', 'Informe o item:', Item) then
        if Qry.Locate('ItemNFe', Item, []) then
          EdItemNFe.Text := Item;
    end else
      EdItemNFe.ValueVariant := 1;
    //
    Buscou  := True;
    GraGruX := Qry.FieldByName('GraGruX').AsInteger;
    Pecas   := dmkPF.PositFlu(Qry.FieldByName('Pecas').AsFloat);
    PesoKg  := dmkPF.PositFlu(Qry.FieldByName('PesoKg').AsFloat);
    AreaM2  := dmkPF.PositFlu(Qry.FieldByName('AreaM2').AsFloat);
    AreaP2  := dmkPF.PositFlu(Qry.FieldByName('AreaP2').AsFloat);
    //
    //if (Qry.RecordCount = 1) and (FGraGruX <> 0) then
    begin
      EdGraGruX.ValueVariant := GraGruX;
      CBGraGruX.KeyValue     := GraGruX;
    end;
    //
    EdPecas.ValueVariant      := Pecas;
    EdPesoKg.ValueVariant     := PesoKg;
    EdAreaP2.ValueVariant     := AreaP2; // Fazer antes para evitar erro na area m2.
    EdAreaM2.ValueVariant     := AreaM2;
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_PF.CadastraPalletWetEnd(Empresa, ClientMO: Integer; EdPallet: TdmkEditCB;
  CBPallet: TdmkDBLookupComboBox; QrVSPallet: TmySQLQuery;
  MovimIDGer: TEstqMovimID; GraGruX: Integer);
var
  Habilita: Boolean;
begin
  if DBCheck.CriaFm(TFmVSPalletPWEAdd, FmVSPalletPWEAdd, afmoNegarComAviso) then
  begin
    FmVSPalletPWEAdd.ImgTipo.SQLType := stIns;
    //
    FmVSPalletPWEAdd.EdEmpresa.ValueVariant := DModG.ObtemFilialDeEntidade(Empresa);
    //Habilita := FmVSPalletPWEAdd.EdEmpresa.ValueVariant = 0;
    FmVSPalletPWEAdd.EdClientMO.ValueVariant := ClientMO;
    FmVSPalletPWEAdd.CBClientMO.KeyValue     := ClientMO;
    Habilita := True;
    FmVSPalletPWEAdd.EdEmpresa.Enabled := Habilita;
    FmVSPalletPWEAdd.CBEmpresa.Enabled := Habilita;
    FmVSPalletPWEAdd.EdClientMO.Enabled := Habilita;
    FmVSPalletPWEAdd.CBClientMO.Enabled := Habilita;
    FmVSPalletPWEAdd.EdStatus.ValueVariant := CO_STAT_VSPALLET_0100_DISPONIVEL;
    FmVSPalletPWEAdd.CBStatus.KeyValue     := CO_STAT_VSPALLET_0100_DISPONIVEL;
    FmVSPalletPWEAdd.FMovimIDGer := MovimIDGer;
    if GraGruX <> 0 then
    begin
      FmVSPalletPWEAdd.EdGraGruX.ValueVariant := GraGruX;
      FmVSPalletPWEAdd.CBGraGruX.KeyValue     := GraGruX;
    end;
    //
    FmVSPalletPWEAdd.ShowModal;
    if FmVSPalletPWEAdd.FPallet <> 0 then
    begin
      if QrVSPallet.State <> dsInactive then
      begin
        UnDmkDAC_PF.AbreQuery(QrVSPallet, Dmod.MyDB);
        if QrVSPallet.Locate('Codigo', FmVSPalletPWEAdd.FPallet, []) then
        begin
          EdPallet.ValueVariant := FmVSPalletPWEAdd.FPallet;
          CBPallet.KeyValue := FmVSPalletPWEAdd.FPallet;
        end;
      end;
    end;
    FmVSPalletPWEAdd.Destroy;
    UnDmkDAC_PF.AbreQuery(QrVSPallet, Dmod.MyDB);
  end;
end;

function TUnVS_PF.CalculaBaseValLiq(BaseValVenda, BaseImpostos, BasePerComis,
  BasFrteVendM2: Double): Double;
begin
  Result := BaseValVenda - (BaseValVenda * (BaseImpostos + BasePerComis) / 100) - BasFrteVendM2;
end;

function TUnVS_PF.CalculaValorCouros(PrecoTipo: TTipoCalcCouro; Preco, Pecas,
  PesoKg, AreaM2, AreaP2, ValTot: Double): Double;
begin
  case PrecoTipo of
    ptcNaoInfo: Result := 0;
    ptcPecas  : Result := Preco * Pecas;
    ptcPesoKg : Result := Preco * PesoKg;
    ptcAreaM2 : Result := Preco * AreaM2;
    ptcAreaP2 : Result := Preco * AreaP2;
    ptcTotal  : Result := ValTot;
    else
    begin
      Geral.MB_Erro('"TipoCalcCouro" n�o implementado!');
      Result := 0;
    end;
  end;
end;

function TUnVS_PF.ClaOuRclAbertos(Pallet, CacCod: Integer): Boolean;
var
  Qry: TmySQLQuery;
  Texto, Esse, SQL_OC: String;
begin
  Result := False;
  if CacCod <> 0 then
    SQL_OC :=   'AND CacCod <> ' + Geral.FF0(CacCod)
  else
    SQL_OC :=   '';
  //
  Qry := TmySQLQuery.Create(Dmod.MyDB);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT 1 ClaRcl, CacCod, VSMovIts, 0.000 VSPallet  ',
    'FROM vspaclacaba ',
    'WHERE DtHrFimCla<"1900-01-01"  ',
    SQL_OC,
    'AND (' + Geral.FF0(Pallet) + ' IN (' ,
    '             LstPal01, LstPal02, LstPal03, ',
    '             LstPal04, LstPal05, LstPal06, ',
    '             LstPal07, LstPal08, LstPal09, ',
    '             LstPal10, LstPal11, LstPal12, ',
    '             LstPal13, LstPal14, LstPal15) ',
    '     ) ',
    ' ',
    'UNION ',
    ' ',
    'SELECT 2 ClaRcl, CacCod, VSMovIts, VSPallet  ',
    'FROM vsparclcaba ',
    'WHERE DtHrFimCla<"1900-01-01"  ',
    SQL_OC,
    'AND (' + Geral.FF0(Pallet) + ' IN (',
    '             LstPal01, LstPal02, LstPal03, ',
    '             LstPal04, LstPal05, LstPal06, ',
    '             LstPal07, LstPal08, LstPal09, ',
    '             LstPal10, LstPal11, LstPal12, ',
    '             LstPal13, LstPal14, LstPal15) ',
    '     ) ',
    '']);
    Result := Qry.RecordCount > 0;
    if Result then
    begin
      if Qry.RecordCount > 1 then
        Esse := 's'
      else
        Esse := '';
      //
      if Geral.MB_Pergunta('O encerramento do pallet ' + Geral.FF0(Pallet) +
      ' foi abortado!' + sLineBreak +
      'Motivo: Estar presente em ' + Geral.FF0(Qry.RecordCount) +
      ' OC' + Esse + '!' + sLineBreak +
      'Deseja visualizar a lista a' + Esse + ' OC' + Esse + '?') = ID_YES then
      begin
        MostraFormVSPaInClaRcl(Pallet, CacCod);
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_PF.ConfiguraRGGrandezaVS(RGGrandeza: TRadioGroup; Colunas,
  Default: Integer);
begin
  RGGrandeza.Items.Clear;
  //(*0*)RGGrandeza.Items.Add('Pe�a');
  (*0*)RGGrandeza.Items.Add('Default');
  (*1*)RGGrandeza.Items.Add('�rea (m�)');
  (*2*)RGGrandeza.Items.Add('Peso (kg)');
  {
  (*3*)RGGrandeza.Items.Add('Volume ( m�)');
  (*4*)RGGrandeza.Items.Add('Linear (m)');
  (*5*)RGGrandeza.Items.Add('? ? ? (outros)');
  (*6*)RGGrandeza.Items.Add('�rea (ft�)');
  (*7*)RGGrandeza.Items.Add('Peso (t)');
  }
  RGGrandeza.Columns := 3;
  RGGrandeza.ItemIndex := Default;
end;

procedure TUnVS_PF.ConfiguraRGVSBastidao(RGBastidao: TRadioGroup;
  Habilita: Boolean; Default: Integer);
const
  Colunas = 8;
begin
  MyObjects.ConfiguraRadioGroup(RGBastidao, sVSBastidao, Colunas, Default);
  RGBastidao.Enabled := Habilita;
end;

procedure TUnVS_PF.ConfiguraRGVSUmidade(RGUmidade: TRadioGroup;
  Habilita: Boolean; Default: Integer);
const
  Colunas = 7;
begin
  MyObjects.ConfiguraRadioGroup(RGUmidade, sVSUmidade, Colunas, Default);
  RGUmidade.Enabled := Habilita;
end;

function TUnVS_PF.CriaVMIJmpCa2(VMICur, GGXJmpSrc, GGXJmpDst, GGXInn: Integer;
  InnPecas, InnAreaM2, InnPesoKg, InnValorT, CalPecas, CalAreaM2, CalPesoKg,
  CalValorT: Double; CalStqCenLoc, MovCodPai, VmiPai, OriGGX: Integer): Boolean;
const
  sProcName = 'VS_PF.CriaVMIJmpCa2()';
  //
  Nome = '';
  TipoArea = -1;
  NFeRem = -1;
  SerieRem = 0;
  LPFMO = '';
var
  Qry: TmySQLQuery;
  //
  SQLType: TSQLType;
  Codigo, MovimCod, MovimTwn, Empresa, Terceiro: Integer; MovimID: TEstqMovimID;
  SrcMovNiv, MovimNiv: TEstqMovimNiv;
  Pallet, GraGruX: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataHora, DtHrAberto: String;
  OriMovimID, SrcMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2: Integer;
  Observ: String;
  LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,Misturou*): Integer;
  CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
  DstMovID, GSPSrcMovID: TEstqMovimID;
  DstNivel1, DstNivel2: Integer;
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
  AptoUso, FornecMO, SerieFch: Integer; NotaMPAG: Double;
  SrcGGX, DstGGX: Integer;
  Marca: String;
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2: Double;
  GGXRcl: Integer;
  JmpMovID: TEstqMovimID;
  JmpNivel1, JmpNivel2, JmpGGX: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM ' + CO_SEL_TAB_VMI,
    'WHERE Controle=' + Geral.FF0(VMICur),
    '']);
    SrcMovNiv  := TEstqMovimNiv(Qry.FieldByName('MovimNiv').AsInteger);
    OriMovimID := TEstqMovimID(Qry.FieldByName('SrcMovID').AsInteger);
    // ver se pegou registro certo!
    if (SrcMovNiv <> eminSorcCur) or (OriMovimID <> emidEmProcCal) then
    begin
      Geral.MB_Erro('Registro inv�lido para ' + sProcName + sLineBreak +
      'MovimID destino: ' + Geral.FF0(Qry.FieldByName('SrcMovID').AsInteger) +
      sLineBreak +
      'MovimNiv origem: ' + Geral.FF0(Qry.FieldByName('MovimNiv').AsInteger) +
      sLineBreak + 'Avise a Dermatek!');
      Exit;
    end;
    SQLType      := stIns;
    MovimID      := TEstqMovimID.emidCaleado;
    MovimNiv     := TEstqMovimNiv.eminDestCal;
    //
    Codigo       := Qry.FieldByName('SrcNivel1').AsInteger;
    //MovimCod   := Abaixo
    //Controle   := Abaixo
    //MovimTwn   := Abaixo
    Empresa      := Qry.FieldByName('Empresa').AsInteger;
    Terceiro     := Qry.FieldByName('Terceiro').AsInteger;
    Pallet       := 0;
    GraGruX      := GGXJmpDst;
    //Pecas      := Abaixo
    //PesoKg     := Abaixo
    AreaM2       := 0;
    AreaP2       := 0;
    //ValorT     := Abaixo
    DataHora     := Geral.FDT(Qry.FieldByName(CO_DATA_HORA_VMI).AsDateTime, 1);
    DtHrAberto   := DataHora;
    SrcMovID     := TEstqMovimID(0);
    SrcNivel1    := 0;
    SrcNivel2    := 0;
    Observ       := '';
    LnkNivXtr1   := 0;
    LnkNivXtr2   := 0;
    CliVenda     := 0;
    Ficha        := Qry.FieldByName('Ficha').AsInteger;
    CustoMOPc    := 0;
    CustoMOKg    := 0;
    CustoMOM2    := 0;
    CustoMOTot   := 0;
    ValorMP      := ValorT;
    DstMovID     := TEstqMovimID(0);
    DstNivel1    := 0;
    DstNivel2    := 0;
    //QtdGerPeca := Abaixo
    //QtdGerPeso := Abaixo
    QtdGerArM2   := 0;
    QtdGerArP2   := 0;
    AptoUso      := 0;
    FornecMO     := Qry.FieldByName('FornecMO').AsInteger;
    SerieFch     := Qry.FieldByName('SerieFch').AsInteger;
    NotaMPAG     := 0;
    SrcGGX       := 0;
    DstGGX       := 0;
    Marca        := Qry.FieldByName('Marca').AsString;
    TpCalcAuto   := -1;
    PedItsLib    := 0;
    PedItsFin    := 0;
    PedItsVda    := 0;
    GSPSrcMovID  := TEstqMovimID(0);
    GSPSrcNiv2   := 0;
    ReqMovEstq   := 0;
    StqCenLoc    := CalStqCenLoc;
    ItemNFe      := 0;
    VSMulFrnCab  := Qry.FieldByName('VSMulFrnCab').AsInteger;
    ClientMO     := Qry.FieldByName('ClientMO').AsInteger;
    QtdAntPeca   := 0;
    QtdAntPeso   := 0;
    QtdAntArM2   := 0;
    QtdAntArP2   := 0;
    GGXRcl       := 0;
    JmpMovID     := TEstqMovimID(Qry.FieldByName('MovimID').AsInteger);
    JmpNivel1    := Qry.FieldByName('Codigo').AsInteger;
    JmpNivel2    := Qry.FieldByName('Controle').AsInteger;
    JmpGGX       := GGXJmpSrc; //Qry.FieldByName('GraGruX').AsInteger;
    //

    if (InnPecas = 0)
    or (CalPecas = 0)
    or (InnPesoKg = 0)
    or (CalPesoKg = 0)
    or (InnPecas < CalPecas) then
    begin
      Geral.MB_Erro('C�lculo de peso inv�lido em ' + sProcName + sLineBreak +
      'Avise a Dermatek!');
      Exit;
    end;
    Pecas      := CalPecas;
    PesoKg     := CalPecas * (InnPesoKg / InnPecas);
    QtdGerPeca := CalPecas;
    QtdGerPeso := CalPesoKg;
    //ValorT     := CalValorT;
    ValorT     := CalPecas * (InnValorT / InnPecas);
    //
    Codigo := UMyMod.BPGS1I32('vscaljmp', 'Codigo', '', '', tsPos, SQLType, Codigo);
    MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
    MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, SQLType, MovimTwn);
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
    //
    //
    if SQLType = stIns then
      InsereVSMovCab(MovimCod, emidCaleado, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vscaljmp', False, [
    'MovimCod', 'Empresa',
    'DtHrAberto', 'DtHrFimOpe',
    'Nome', 'TipoArea', 'GraGruX',
    'NFeRem', 'SerieRem', 'LPFMO',
    'GGXDst'], [
    'Codigo'], [
    MovimCod, Empresa,
    DtHrAberto, DtHrAberto,
    Nome, TipoArea, (*GraGruX*)GGXInn,
    NFeRem, SerieRem, LPFMO,
    GGXInn], [
    Codigo], True) then
    begin
      if InsUpdVSMovIts3(SQLType, Codigo, MovimCod, MovimTwn,
      Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
      AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
      Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
      CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
      QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
      NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
      PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
      ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
      QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
      CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
      JmpMovID, JmpNivel1, JmpNivel2, JmpGGX,
      (*RmsMovID*)TEstqMovimID.emidAjuste, (*RmsNivel1*)0,
      (*RmsNivel2*)0, (*RmsGGX*)0, (*GSPJmpMovID*)TEstqMovimID.emidAjuste,
      (*GSPJmpNiv2*)0, MovCodPai,  CO_0_VmiPai,
    (*
      CO_0_JmpMovID,
      CO_0_JmpNivel1,
      CO_0_JmpNivel2,
      CO_0_JmpGGX,
      CO_0_RmsMovID,
      CO_0_RmsNivel1,
      CO_0_RmsNivel2,
      CO_0_RmsGGX,
      CO_0_GSPJmpMovID,
      CO_0_GSPJmpNiv2,
      CO_0_MovCodPai,
    *)
      CO_0_IxxMovIX,
      CO_0_IxxFolha,
      CO_0_IxxLinha,
      CO_TRUE_ExigeClientMO,
      CO_TRUE_ExigeFornecMO,
      CO_TRUE_ExigeStqLoc,
      iuvpei012(*Gera��o autom�tica de couro caleado no curtimento*)) then
      begin
        MovimNiv   := TEstqMovimNiv.eminEmCalBxa;
        //GraGruX    := OriGGX; // 2023-05-15
        Pecas      := -Pecas;
        AreaM2     := -AreaM2;
        AreaP2     := -AreaP2;
        PesoKg     := -PesoKg;
        SrcMovID   := TEstqMovimID.emidCaleado;
        SrcNivel1  := Codigo;
        SrcNivel2  := Controle;
        //JmpMovID   := TEstqMovimID(0);
        //JmpNivel1  := 0;
        //JmpNivel2  := 0;
        Controle   := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
        //
        if InsUpdVSMovIts3(SQLType, Codigo, MovimCod, MovimTwn,
        Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
        //AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
        AreaP2, -ABS(ValorT), DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
        Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
        CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
        QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
        NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
        PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
        ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
        QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
        CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
        JmpMovID, JmpNivel1, JmpNivel2, JmpGGX,
        (*RmsMovID*)TEstqMovimID.emidAjuste, (*RmsNivel1*)0,
        (*RmsNivel2*)0, (*RmsGGX*)0, (*GSPJmpMovID*)TEstqMovimID.emidAjuste,
        (*GSPJmpNiv2*)0, MovCodPai, VmiPai,
  (*
        CO_0_JmpMovID,
        CO_0_JmpNivel1,
        CO_0_JmpNivel2,
        CO_0_JmpGGX,
        CO_0_RmsMovID,
        CO_0_RmsNivel1,
        CO_0_RmsNivel2,
        CO_0_RmsGGX,
        CO_0_GSPJmpMovID,
        CO_0_GSPJmpNiv2,
        CO_0_MovCodPai,
  *)
        CO_0_IxxMovIX,
        CO_0_IxxFolha,
        CO_0_IxxLinha,
        CO_TRUE_ExigeClientMO,
        CO_TRUE_ExigeFornecMO,
        CO_TRUE_ExigeStqLoc,
        iuvpei013(*Baixa de Couro PDA na gera��o autom�tica de couro caleado no curtimento*)) then
        begin
          //  Atualiza VMI de curtimento!
          JmpMovID  := MovimID;
          JmpNivel1 := Codigo;
          JmpNivel2 := SrcNivel2;
          JmpGGX    := GGXJmpDst;
          Controle  := Qry.FieldByName('Controle').AsInteger;
          Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, '' + CO_UPD_TAB_VMI + '', False, [
          'JmpMovID', 'JmpNivel1', 'JmpNivel2', 'JmpGGX'], [
          'Controle'], [
          JmpMovID, JmpNivel1, JmpNivel2, JmpGGX], [
          Controle], True);
        end;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnVS_PF.CriaVMIJmpCal(VMICur, GGXJmpSrc, GGXJmpDst, GGXInn: Integer;
  InnPecas,
  InnAreaM2, InnPesoKg, CalPecas, CalAreaM2, CalPesoKg, CalValorT: Double;
  CalStqCenLoc, MovCodPai, VmiPai: Integer): Boolean;
const
  sProcName = 'VS_PF.CriaVMIJmpCal()';
  //
  Nome = '';
  TipoArea = -1;
  NFeRem = -1;
  SerieRem = 0;
  LPFMO = '';
var
  Qry: TmySQLQuery;
  //
  SQLType: TSQLType;
  Codigo, MovimCod, MovimTwn, Empresa, Terceiro: Integer; MovimID: TEstqMovimID;
  SrcMovNiv, MovimNiv: TEstqMovimNiv;
  Pallet, GraGruX: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataHora, DtHrAberto: String;
  OriMovimID, SrcMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2: Integer;
  Observ: String;
  LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,Misturou*): Integer;
  CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
  DstMovID, GSPSrcMovID: TEstqMovimID;
  DstNivel1, DstNivel2: Integer;
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
  AptoUso, FornecMO, SerieFch: Integer; NotaMPAG: Double;
  SrcGGX, DstGGX: Integer;
  Marca: String;
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2: Double;
  GGXRcl: Integer;
  JmpMovID: TEstqMovimID;
  JmpNivel1, JmpNivel2, JmpGGX: Integer;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM ' + CO_SEL_TAB_VMI,
    'WHERE Controle=' + Geral.FF0(VMICur),
    '']);
    SrcMovNiv  := TEstqMovimNiv(Qry.FieldByName('MovimNiv').AsInteger);
    OriMovimID := TEstqMovimID(Qry.FieldByName('SrcMovID').AsInteger);
    // ver se pegou registro certo!
    if (SrcMovNiv <> eminSorcCur) or (OriMovimID <> emidEmProcCal) then
    begin
      Geral.MB_Erro('Registro inv�lido para ' + sProcName + sLineBreak +
      'MovimID destino: ' + Geral.FF0(Qry.FieldByName('SrcMovID').AsInteger) +
      sLineBreak +
      'MovimNiv origem: ' + Geral.FF0(Qry.FieldByName('MovimNiv').AsInteger) +
      sLineBreak + 'Avise a Dermatek!');
      Exit;
    end;
    SQLType      := stIns;
    MovimID      := TEstqMovimID.emidCaleado;
    MovimNiv     := TEstqMovimNiv.eminDestCal;
    //
    Codigo       := Qry.FieldByName('SrcNivel1').AsInteger;
    //MovimCod   := Abaixo
    //Controle   := Abaixo
    //MovimTwn   := Abaixo
    Empresa      := Qry.FieldByName('Empresa').AsInteger;
    Terceiro     := Qry.FieldByName('Terceiro').AsInteger;
    Pallet       := 0;
    GraGruX      := GGXJmpDst;
    //Pecas      := Abaixo
    //PesoKg     := Abaixo
    AreaM2       := 0;
    AreaP2       := 0;
    //ValorT     := Abaixo
    DataHora     := Geral.FDT(Qry.FieldByName(CO_DATA_HORA_VMI).AsDateTime, 1);
    DtHrAberto   := DataHora;
    SrcMovID     := TEstqMovimID(0);
    SrcNivel1    := 0;
    SrcNivel2    := 0;
    Observ       := '';
    LnkNivXtr1   := 0;
    LnkNivXtr2   := 0;
    CliVenda     := 0;
    Ficha        := Qry.FieldByName('Ficha').AsInteger;
    CustoMOPc    := 0;
    CustoMOKg    := 0;
    CustoMOM2    := 0;
    CustoMOTot   := 0;
    ValorMP      := ValorT;
    DstMovID     := TEstqMovimID(0);
    DstNivel1    := 0;
    DstNivel2    := 0;
    //QtdGerPeca := Abaixo
    //QtdGerPeso := Abaixo
    QtdGerArM2   := 0;
    QtdGerArP2   := 0;
    AptoUso      := 0;
    FornecMO     := Qry.FieldByName('FornecMO').AsInteger;
    SerieFch     := Qry.FieldByName('SerieFch').AsInteger;
    NotaMPAG     := 0;
    SrcGGX       := 0;
    DstGGX       := 0;
    Marca        := Qry.FieldByName('Marca').AsString;
    TpCalcAuto   := -1;
    PedItsLib    := 0;
    PedItsFin    := 0;
    PedItsVda    := 0;
    GSPSrcMovID  := TEstqMovimID(0);
    GSPSrcNiv2   := 0;
    ReqMovEstq   := 0;
    StqCenLoc    := CalStqCenLoc;
    ItemNFe      := 0;
    VSMulFrnCab  := Qry.FieldByName('VSMulFrnCab').AsInteger;
    ClientMO     := Qry.FieldByName('ClientMO').AsInteger;
    QtdAntPeca   := 0;
    QtdAntPeso   := 0;
    QtdAntArM2   := 0;
    QtdAntArP2   := 0;
    GGXRcl       := 0;
    JmpMovID     := TEstqMovimID(Qry.FieldByName('MovimID').AsInteger);
    JmpNivel1    := Qry.FieldByName('Codigo').AsInteger;
    JmpNivel2    := Qry.FieldByName('Controle').AsInteger;
    JmpGGX       := GGXJmpSrc; //Qry.FieldByName('GraGruX').AsInteger;
    //

(**)
    if (InnPecas = 0)
    or (CalPecas = 0)
    or (InnPesoKg = 0)
    or (CalPesoKg = 0)
    or (InnPecas < CalPecas) then
    begin
      Geral.MB_Erro('C�lculo de peso inv�lido em ' + sProcName + sLineBreak +
      'Avise a Dermatek!');
      Exit;
    end;
    Pecas      := CalPecas;
    //PesoKg     := CalPecas / InnPecas * InnPesoKg;
    PesoKg     := CalPecas * (InnPesoKg / InnPecas);
    QtdGerPeca := CalPecas;
    QtdGerPeso := CalPesoKg;
    ValorT     := CalValorT;
(**)
{
    if (InnPecas = 0)
    or (CalPecas = 0)
    or (InnPesoKg = 0)
    or (CalPesoKg = 0)
    or (InnPesoKg < CalPesoKg) then
    begin
      Geral.MB_Erro('C�lculo de peso inv�lido em ' + sProcName + sLineBreak +
      'Avise a Dermatek!');
      Exit;
    end;
    Pecas      := CalPecas;
    PesoKg     := ?;
    QtdGerPeca := CalPecas;
    QtdGerPeso := CalPesoKg;
    ValorT     := CalValorT;
}
    //
    Codigo := UMyMod.BPGS1I32('vscaljmp', 'Codigo', '', '', tsPos, SQLType, Codigo);
    MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
    MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, SQLType, MovimTwn);
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
    //
    //
    if SQLType = stIns then
      InsereVSMovCab(MovimCod, emidCaleado, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vscaljmp', False, [
    'MovimCod', 'Empresa',
    'DtHrAberto', 'DtHrFimOpe',
    'Nome', 'TipoArea', 'GraGruX',
    'NFeRem', 'SerieRem', 'LPFMO',
    'GGXDst'], [
    'Codigo'], [
    MovimCod, Empresa,
    DtHrAberto, DtHrAberto,
    Nome, TipoArea, (*GraGruX*)GGXInn,
    NFeRem, SerieRem, LPFMO,
    GGXInn], [
    Codigo], True) then
    begin
      if InsUpdVSMovIts3(SQLType, Codigo, MovimCod, MovimTwn,
      Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
      AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
      Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
      CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
      QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
      NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
      PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
      ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
      QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
      CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
      JmpMovID, JmpNivel1, JmpNivel2, JmpGGX,
      (*RmsMovID*)TEstqMovimID.emidAjuste, (*RmsNivel1*)0,
      (*RmsNivel2*)0, (*RmsGGX*)0, (*GSPJmpMovID*)TEstqMovimID.emidAjuste,
      (*GSPJmpNiv2*)0, MovCodPai,  CO_0_VmiPai,
    (*
      CO_0_JmpMovID,
      CO_0_JmpNivel1,
      CO_0_JmpNivel2,
      CO_0_JmpGGX,
      CO_0_RmsMovID,
      CO_0_RmsNivel1,
      CO_0_RmsNivel2,
      CO_0_RmsGGX,
      CO_0_GSPJmpMovID,
      CO_0_GSPJmpNiv2,
      CO_0_MovCodPai,
    *)
      CO_0_IxxMovIX,
      CO_0_IxxFolha,
      CO_0_IxxLinha,
      CO_TRUE_ExigeClientMO,
      CO_TRUE_ExigeFornecMO,
      CO_TRUE_ExigeStqLoc,
      iuvpei012(*Gera��o autom�tica de couro caleado no curtimento*)) then
      begin
        MovimNiv   := TEstqMovimNiv.eminEmCalBxa;
        Pecas      := -Pecas;
        AreaM2     := -AreaM2;
        AreaP2     := -AreaP2;
        PesoKg     := -PesoKg;
        SrcMovID   := TEstqMovimID.emidCaleado;
        SrcNivel1  := Codigo;
        SrcNivel2  := Controle;
        //JmpMovID   := TEstqMovimID(0);
        //JmpNivel1  := 0;
        //JmpNivel2  := 0;
        Controle   := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
        //
        if InsUpdVSMovIts3(SQLType, Codigo, MovimCod, MovimTwn,
        Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
        //AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
        AreaP2, -ABS(ValorT), DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
        Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
        CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
        QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
        NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
        PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
        ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
        QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
        CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
        JmpMovID, JmpNivel1, JmpNivel2, JmpGGX,
        (*RmsMovID*)TEstqMovimID.emidAjuste, (*RmsNivel1*)0,
        (*RmsNivel2*)0, (*RmsGGX*)0, (*GSPJmpMovID*)TEstqMovimID.emidAjuste,
        (*GSPJmpNiv2*)0, MovCodPai, VmiPai,
  (*
        CO_0_JmpMovID,
        CO_0_JmpNivel1,
        CO_0_JmpNivel2,
        CO_0_JmpGGX,
        CO_0_RmsMovID,
        CO_0_RmsNivel1,
        CO_0_RmsNivel2,
        CO_0_RmsGGX,
        CO_0_GSPJmpMovID,
        CO_0_GSPJmpNiv2,
        CO_0_MovCodPai,
  *)
        CO_0_IxxMovIX,
        CO_0_IxxFolha,
        CO_0_IxxLinha,
        CO_TRUE_ExigeClientMO,
        CO_TRUE_ExigeFornecMO,
        CO_TRUE_ExigeStqLoc,
        iuvpei013(*Baixa de Couro PDA na gera��o autom�tica de couro caleado no curtimento*)) then
        begin
          //  Atualiza VMI de curtimento!
          JmpMovID  := MovimID;
          JmpNivel1 := Codigo;
          JmpNivel2 := SrcNivel2;
          JmpGGX    := GGXJmpDst;
          Controle  := Qry.FieldByName('Controle').AsInteger;
          Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, '' + CO_UPD_TAB_VMI + '', False, [
          'JmpMovID', 'JmpNivel1', 'JmpNivel2', 'JmpGGX'], [
          'Controle'], [
          JmpMovID, JmpNivel1, JmpNivel2, JmpGGX], [
          Controle], True);
        end;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnVS_PF.CriaVMIJmpCur(VSCurCab, Empresa(*, VMICur, GGXJmpSrc, GGXJmpDst, GGXInn*):
              Integer; DtHrAberto: String(*InnPecas,
              InnAreaM2, InnPesoKg, CurPecas, CurAreaM2, CurPesoKg, CurValorT:
              Double; CurStqCenLoc, MovCodPai: Integer*)): Boolean;
const
  sProcName = 'VS_PF.CriaVMIJmpCur()';
  //
  Nome = '';
  TipoArea = -1;
  NFeRem = -1;
  SerieRem = 0;
  LPFMO = '';
var
{
copiado do CriaVMIJmpCal()!!!
  Qry: TmySQLQuery;
  //
}
  SQLType: TSQLType;
  Codigo, MovimCod(*, MovimTwn, Empresa, Terceiro*): Integer;
{
copiado do CriaVMIJmpCal()!!!
  MovimID: TEstqMovimID;
  SrcMovNiv, MovimNiv: TEstqMovimNiv;
  Pallet, GraGruX: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataHora, DtHrAberto: String;
  OriMovimID, SrcMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2: Integer;
  Observ: String;
  LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,Misturou*): Integer;
  CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
  DstMovID, GSPSrcMovID: TEstqMovimID;
  DstNivel1, DstNivel2: Integer;
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
  AptoUso, FornecMO, SerieFch: Integer; NotaMPAG: Double;
  SrcGGX, DstGGX: Integer;
  Marca: String;
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2: Double;
  GGXRcl: Integer;
  JmpMovID: TEstqMovimID;
  JmpNivel1, JmpNivel2, JmpGGX: Integer;
}
begin
  SQLType := stIns;
{
copiado do CriaVMIJmpCal()!!!
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM ' + CO_SEL_TAB_VMI,
    'WHERE Controle=' + Geral.FF0(VMICur),
    '']);
    SrcMovNiv  := TEstqMovimNiv(Qry.FieldByName('MovimNiv').AsInteger);
    OriMovimID := TEstqMovimID(Qry.FieldByName('SrcMovID').AsInteger);
    // ver se pegou registro certo!
    if (SrcMovNiv <> eminSorcCur) or (OriMovimID <> emidEmProcCur) then
    begin
      Geral.MB_Erro('Registro inv�lido para ' + sProcName + sLineBreak +
      'MovimID destino: ' + Geral.FF0(Qry.FieldByName('SrcMovID').AsInteger) +
      sLineBreak +
      'MovimNiv origem: ' + Geral.FF0(Qry.FieldByName('MovimNiv').AsInteger) +
      sLineBreak + 'Avise a Dermatek!');
      Exit;
    end;
    MovimID      := TEstqMovimID.emidCurtido;
    MovimNiv     := TEstqMovimNiv.eminDestCur;
    //
    Codigo       := Qry.FieldByName('SrcNivel1').AsInteger;
    //MovimCod   := Abaixo
    //Controle   := Abaixo
    //MovimTwn   := Abaixo
    Empresa      := Qry.FieldByName('Empresa').AsInteger;
    Terceiro     := Qry.FieldByName('Terceiro').AsInteger;
    Pallet       := 0;
    GraGruX      := GGXJmpDst;
    //Pecas      := Abaixo
    //PesoKg     := Abaixo
    AreaM2       := 0;
    AreaP2       := 0;
    //ValorT     := Abaixo
    DataHora     := Geral.FDT(Qry.FieldByName(CO_DATA_HORA_VMI).AsDateTime, 1);
    DtHrAberto   := DataHora;
    SrcMovID     := TEstqMovimID(0);
    SrcNivel1    := 0;
    SrcNivel2    := 0;
    Observ       := '';
    LnkNivXtr1   := 0;
    LnkNivXtr2   := 0;
    CliVenda     := 0;
    Ficha        := Qry.FieldByName('Ficha').AsInteger;
    CustoMOKg    := 0;
    CustoMOM2    := 0;
    CustoMOTot   := 0;
    ValorMP      := ValorT;
    DstMovID     := TEstqMovimID(0);
    DstNivel1    := 0;
    DstNivel2    := 0;
    //QtdGerPeca := Abaixo
    //QtdGerPeso := Abaixo
    QtdGerArM2   := 0;
    QtdGerArP2   := 0;
    AptoUso      := 0;
    FornecMO     := Qry.FieldByName('FornecMO').AsInteger;
    SerieFch     := Qry.FieldByName('SerieFch').AsInteger;
    NotaMPAG     := 0;
    SrcGGX       := 0;
    DstGGX       := 0;
    Marca        := Qry.FieldByName('Marca').AsString;
    TpCalcAuto   := -1;
    PedItsLib    := 0;
    PedItsFin    := 0;
    PedItsVda    := 0;
    GSPSrcMovID  := TEstqMovimID(0);
    GSPSrcNiv2   := 0;
    ReqMovEstq   := 0;
    StqCenLoc    := CurStqCenLoc;
    ItemNFe      := 0;
    VSMulFrnCab  := Qry.FieldByName('VSMulFrnCab').AsInteger;
    ClientMO     := Qry.FieldByName('ClientMO').AsInteger;
    QtdAntPeca   := 0;
    QtdAntPeso   := 0;
    QtdAntArM2   := 0;
    QtdAntArP2   := 0;
    GGXRcl       := 0;
    JmpMovID     := TEstqMovimID(Qry.FieldByName('MovimID').AsInteger);
    JmpNivel1    := Qry.FieldByName('Codigo').AsInteger;
    JmpNivel2    := Qry.FieldByName('Controle').AsInteger;
    JmpGGX       := GGXJmpSrc; //Qry.FieldByName('GraGruX').AsInteger;
    //

    if (InnPecas = 0)
    or (CurPecas = 0)
    or (InnPesoKg = 0)
    or (CurPesoKg = 0)
    or (InnPecas < CurPecas) then
    begin
      Geral.MB_Erro('C�lculo de peso inv�lido em ' + sProcName + sLineBreak +
      'Avise a Dermatek!');
      Exit;
    end;
    Pecas      := CurPecas;
    PesoKg     := CurPecas * (InnPesoKg / InnPecas);
    QtdGerPeca := CurPecas;
    QtdGerPeso := CurPesoKg;
    ValorT     := CurValorT;
   //
}
    Codigo   := VSCurCab; // UMyMod.BPGS1I32('vscurjmp', 'Codigo', '', '', tsPos, SQLType, Codigo);
    MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
{
copiado do CriaVMIJmpCal()!!!
    MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, SQLType, MovimTwn);
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
}
    //
    //
    if SQLType = stIns then
      InsereVSMovCab(MovimCod, emidCurtido, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vscurjmp', False, [
    'MovimCod', 'Empresa',
    'DtHrAberto', (*'DtHrFimOpe',*)
    'Nome', 'TipoArea', (*'GraGruX',*)
    'NFeRem', 'SerieRem', 'LPFMO'(*,
    'GGXDst'*)], [
    'Codigo'], [
    MovimCod, Empresa,
    DtHrAberto, (*DtHrAberto,*)
    Nome, TipoArea, (*GraGruX,*)
    NFeRem, SerieRem, LPFMO(*,
    GGXInn*)], [
    Codigo], True) then
    begin
{
copiado do CriaVMIJmpCal()!!!
      if InsUpdVSMovIts_(SQLType, Codigo, MovimCod, MovimTwn,
      Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
      AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
      Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
      CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
      QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
      NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
      PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
      ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
      QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, CO_0_GGXRcl,
      JmpMovID, JmpNivel1, JmpNivel2, JmpGGX,
      (*RmsMovID*)TEstqMovimID.emidAjuste, (*RmsNivel1*)0,
      (*RmsNivel2*)0, (*RmsGGX*)0, (*GSPJmpMovID*)TEstqMovimID.emidAjuste,
      (*GSPJmpNiv2*)0, MovCodPai) then
      begin
        MovimNiv   := TEstqMovimNiv.eminEmCurBxa;
        Pecas      := -Pecas;
        AreaM2     := -AreaM2;
        AreaP2     := -AreaP2;
        PesoKg     := -PesoKg;
        SrcMovID   := TEstqMovimID.emidCurtido;
        SrcNivel1  := Codigo;
        SrcNivel2  := Controle;
        //JmpMovID   := TEstqMovimID(0);
        //JmpNivel1  := 0;
        //JmpNivel2  := 0;
        Controle   := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
        //
        if InsUpdVSMovIts_(SQLType, Codigo, MovimCod, MovimTwn,
        Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
        AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
        Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
        CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
        QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
        NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
        PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
        ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
        QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, CO_0_GGXRcl,
        JmpMovID, JmpNivel1, JmpNivel2, JmpGGX,
      (*RmsMovID*)TEstqMovimID.emidAjuste, (*RmsNivel1*)0,
      (*RmsNivel2*)0, (*RmsGGX*)0, (*GSPJmpMovID*)TEstqMovimID.emidAjuste,
      (*GSPJmpNiv2*)0, MovCodPai) then
        begin
          //  Atualiza VMI de curtiemnto!
          JmpMovID  := MovimID;
          JmpNivel1 := Codigo;
          JmpNivel2 := SrcNivel2;
          JmpGGX    := GGXJmpDst;
          Controle  := Qry.FieldByName('Controle').AsInteger;
          Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, '' + CO_UPD_TAB_VMI + '', False, [
          'JmpMovID', 'JmpNivel1', 'JmpNivel2', 'JmpGGX'], [
          'Controle'], [
          JmpMovID, JmpNivel1, JmpNivel2, JmpGGX], [
          Controle], True);
        end;
      end;
}
    end;
{
  finally
    Qry.Free;
  end;
}
end;

function TUnVS_PF.CriaVMIRmsCal(VMICal, GGXRmsSrc, GGXRmsDst, GGXPDA: Integer;
  PDAPecas, PDAAreaM2, PDAPesoKg, CalPecas, CalAreaM2, CalPesoKg, CalValorT:
  Double; CalStqCenLoc: Integer; VSCalCab_Codigo, VSCalCab_MovimCod: Integer;
    // ini 2023-11-20 Baixa de sub produto por peso
    Peso: Double
    // fim 2023-11-20 Baixa de sub produto por peso
  ): Boolean;
const
  sProcName = 'VS_PF.CriaVMIRmsCal()';
  //
  Nome = '';
  TipoArea = -1;
  NFeRem = -1;
  SerieRem = 0;
  LPFMO = '';
var
  Qry: TmySQLQuery;
  //
  SQLType: TSQLType;
  Codigo, MovimCod, MovimTwn, Empresa, Terceiro: Integer; MovimID: TEstqMovimID;
  SrcMovNiv, MovimNiv: TEstqMovimNiv;
  Pallet, GraGruX: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataHora, DtHrAberto: String;
  OriMovimID, SrcMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2: Integer;
  Observ: String;
  LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,Misturou*): Integer;
  CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
  DstMovID: TEstqMovimID;
  DstNivel1, DstNivel2: Integer;
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
  AptoUso, FornecMO, SerieFch: Integer; NotaMPAG: Double;
  SrcGGX, DstGGX: Integer;
  Marca: String;
  GSPSrcMovID: TEstqMovimID;
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2: Double;
  GGXRcl: Integer;
  JmpMovID, RmsMovID: TEstqMovimID;
  JmpNivel1, JmpNivel2, JmpGGX, RmsNivel1, RmsNivel2, RmsGGX: Integer;
  sMotivo, Mensagem: String;
  OK: Boolean;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM ' + CO_SEL_TAB_VMI,
    'WHERE Controle=' + Geral.FF0(VMICal),
    '']);
    SrcMovNiv  := TEstqMovimNiv(Qry.FieldByName('MovimNiv').AsInteger);
    OriMovimID := TEstqMovimID(Qry.FieldByName('SrcMovID').AsInteger);
    // ver se pegou registro certo!
    // ini 2023-05-17
    //if (SrcMovNiv <> eminSorcCal) or (OriMovimID <> emidCompra) then
    OK := (SrcMovNiv = eminSorcCal)
    and (
      (OriMovimID = emidCompra) or
      (OriMovimID = emidConservado) or
      (OriMovimID = emidGeraSubProd)
    );
    if OK = False then
    begin
      Geral.MB_Erro('Registro inv�lido para ' + sProcName + sLineBreak +
      'MovimID destino: ' + Geral.FF0(Qry.FieldByName('SrcMovID').AsInteger) +
      sLineBreak +
      'MovimNiv origem: ' + Geral.FF0(Qry.FieldByName('MovimNiv').AsInteger) +
      sLineBreak + 'Avise a Dermatek!');
      //Exit;
    // fim 2023-05-17
    end;
    SQLType      := stIns;
    MovimID      := TEstqMovimID.emidEmRibPDA;
    MovimNiv     := TEstqMovimNiv.eminDestPDA;
    //
    Codigo       := Qry.FieldByName('SrcNivel1').AsInteger;
    //MovimCod   := Abaixo
    //Controle   := Abaixo
    //MovimTwn   := Abaixo
    Empresa      := Qry.FieldByName('Empresa').AsInteger;
    Terceiro     := Qry.FieldByName('Terceiro').AsInteger;
    Pallet       := 0;
    GraGruX      := GGXRmsDst;
    //Pecas      := Abaixo
    //PesoKg     := Abaixo
    AreaM2       := 0;
    AreaP2       := 0;
    //ValorT     := Abaixo
    DataHora     := Geral.FDT(Qry.FieldByName(CO_DATA_HORA_VMI).AsDateTime, 1);
    DtHrAberto   := DataHora;
    SrcMovID     := TEstqMovimID(0);
    SrcNivel1    := 0;
    SrcNivel2    := 0;
    Observ       := '';
    LnkNivXtr1   := 0;
    LnkNivXtr2   := 0;
    CliVenda     := 0;
    Ficha        := Qry.FieldByName('Ficha').AsInteger;
    CustoMOKg    := 0;
    CustoMOM2    := 0;
    CustoMOTot   := 0;
    ValorMP      := ValorT;
    DstMovID     := TEstqMovimID(0);
    DstNivel1    := 0;
    DstNivel2    := 0;
    //QtdGerPeca := Abaixo
    //QtdGerPeso := Abaixo
    QtdGerArM2   := 0;
    QtdGerArP2   := 0;
    AptoUso      := 0;
    FornecMO     := Qry.FieldByName('FornecMO').AsInteger;
    SerieFch     := Qry.FieldByName('SerieFch').AsInteger;
    NotaMPAG     := 0;
    SrcGGX       := 0;
    DstGGX       := 0;
    Marca        := Qry.FieldByName('Marca').AsString;
    TpCalcAuto   := -1;
    PedItsLib    := 0;
    PedItsFin    := 0;
    PedItsVda    := 0;
    GSPSrcMovID  := TEstqMovimID(0);
    GSPSrcNiv2   := 0;
    ReqMovEstq   := 0;
    StqCenLoc    := CalStqCenLoc;
    ItemNFe      := 0;
    VSMulFrnCab  := Qry.FieldByName('VSMulFrnCab').AsInteger;
    ClientMO     := Qry.FieldByName('ClientMO').AsInteger;
    QtdAntPeca   := 0;
    QtdAntPeso   := 0;
    QtdAntArM2   := 0;
    QtdAntArP2   := 0;
    GGXRcl       := 0;
    JmpMovID     := TEstqMovimID.emidAjuste;
    JmpNivel1    := 0;
    JmpNivel2    := 0;
    JmpGGX       := 0;
    RmsMovID     := TEstqMovimID(Qry.FieldByName('MovimID').AsInteger);
    RmsNivel1    := Qry.FieldByName('Codigo').AsInteger;
    RmsNivel2    := Qry.FieldByName('Controle').AsInteger;
    RmsGGX       := GGXRmsSrc; //Qry.FieldByName('GraGruX').AsInteger;
    //
(**)
    if OriMovimID = emidGeraSubProd then
    begin
      if (PDAPecas > 0)
      or (CalPecas > 0)
      or (PDAPesoKg = 0)
      or (CalPesoKg = 0)
      or (PDAPesoKg < CalPesoKg) then
      begin
        sMotivo := '';
        if (PDAPecas = 0) then sMotivo := 'PDAPecas > 0 ';
        if (CalPecas = 0) then sMotivo := 'CalPecas > 0 ';
        if (PDAPesoKg = 0) then sMotivo := 'PDAPesoKg < 0 ';
        if (CalPesoKg = 0) then sMotivo := 'CalPesoKg < 0 ';
        if (PDAPecas < CalPecas) then sMotivo := 'PDAPesoKg < CalPesoKg';
        Mensagem :=
        'C�lculo de peso inv�lido em ' + sProcName + sLineBreak +
        'IMEI ' + Geral.FF0(VMICal) + sLineBreak +
        'C�digo Gerenciamento Caleiro: ' + Geral.FF0(VSCalCab_Codigo) + sLineBreak +
        'IMEC Gerenciamento Caleiro: ' + Geral.FF0(VSCalCab_MovimCod) + sLineBreak +
        'IMEI RmsNivel2: ' + Geral.FF0(RmsNivel2) + sLineBreak + sLineBreak +
        'PDAPecas  =  '  + FloatToStr(PDAPecas ) + sLineBreak +
        'CalPecas  =  '  + FloatToStr(CalPecas ) + sLineBreak +
        'PDAPesoKg  =  ' + FloatToStr(PDAPesoKg) + sLineBreak +
        'CalPesoKg  =  ' + FloatToStr(CalPesoKg) + sLineBreak +
        sLineBreak +
        'Motivo: ' + sMotivo + sLineBreak;
        //
        //
        if (PDAPecas = 0)
        and (CalPecas = 0)
        and (PDAPesoKg > 0)
        and (CalPesoKg > 0)
        and (PDAPesoKg < CalPesoKg) then
        begin
          if Geral.MB_Pergunta(Mensagem + sLineBreak +
          'Deseja continuar assim mesmo? ') <> ID_YES then
            Exit;
        end else
        begin
          Geral.MB_Erro(Mensagem + sLineBreak + 'Avise a Dermatek!');
          Exit;
        end;
      end;
      QtdGerPeca := CalPecas;
      QtdGerPeso := -Peso; //CalPecas * (PDAPesoKg / PDAPecas);
      Pecas      := CalPecas;
      PesoKg     := CalPesoKg;
      //
      ValorT     := CalValorT;
    end else
    begin
      if (PDAPecas = 0)
      or (CalPecas = 0)
      or (PDAPesoKg = 0)
      or (CalPesoKg = 0)
      or (PDAPecas < CalPecas) then
      begin
        sMotivo := '';
        if (PDAPecas = 0) then sMotivo := 'PDAPecas < 0 ';
        if (CalPecas = 0) then sMotivo := 'CalPecas < 0 ';
        if (PDAPesoKg = 0) then sMotivo := 'PDAPesoKg < 0 ';
        if (CalPesoKg = 0) then sMotivo := 'CalPesoKg < 0 ';
        if (PDAPecas < CalPecas) then sMotivo := 'PDAPecas < CalPecas';
        Mensagem :=
        'C�lculo de peso inv�lido em ' + sProcName + sLineBreak +
        'IMEI ' + Geral.FF0(VMICal) + sLineBreak +
        'C�digo Gerenciamento Caleiro: ' + Geral.FF0(VSCalCab_Codigo) + sLineBreak +
        'IMEC Gerenciamento Caleiro: ' + Geral.FF0(VSCalCab_MovimCod) + sLineBreak +
        'IMEI RmsNivel2: ' + Geral.FF0(RmsNivel2) + sLineBreak + sLineBreak +
        'PDAPecas  =  '  + FloatToStr(PDAPecas ) + sLineBreak +
        'CalPecas  =  '  + FloatToStr(CalPecas ) + sLineBreak +
        'PDAPesoKg  =  ' + FloatToStr(PDAPesoKg) + sLineBreak +
        'CalPesoKg  =  ' + FloatToStr(CalPesoKg) + sLineBreak +
        sLineBreak +
        'Motivo: ' + sMotivo + sLineBreak;
        //
        //
        if (PDAPecas > 0)
        and (CalPecas > 0)
        and (PDAPesoKg > 0)
        and (CalPesoKg > 0)
        and (PDAPecas < CalPecas) then
        begin
          if Geral.MB_Pergunta(Mensagem + sLineBreak +
          'Deseja continuar assim mesmo? ') <> ID_YES then
            Exit;
        end else
        begin
          Geral.MB_Erro(Mensagem + sLineBreak + 'Avise a Dermatek!');
          Exit;
        end;
      end;
      QtdGerPeca := CalPecas;
      QtdGerPeso := CalPecas * (PDAPesoKg / PDAPecas);
      Pecas      := CalPecas;
      PesoKg     := CalPesoKg;
      //
      ValorT     := CalValorT;
    end;
    //
    Codigo := UMyMod.BPGS1I32('vscalpda', 'Codigo', '', '', tsPos, SQLType, Codigo);
    MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
    MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, SQLType, MovimTwn);
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
    //
    //
    if SQLType = stIns then
      InsereVSMovCab(MovimCod, emidEMRibDTA, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vscalpda', False, [
    'MovimCod', 'Empresa',
    'DtHrAberto', 'DtHrFimOpe',
    'Nome', 'TipoArea', 'GraGruX',
    'NFeRem', 'SerieRem', 'LPFMO',
    'GGXDst', 'PesoKgSrc', 'PecasSrc'
    ], [
    'Codigo'], [
    MovimCod, Empresa,
    DtHrAberto, DtHrAberto,
    Nome, TipoArea, (*GraGruX*)GGXPDA,
    NFeRem, SerieRem, LPFMO,
    GGXPDA, PDAPesoKg, PDAPecas
    ], [
    Codigo], True) then
    begin
      if InsUpdVSMovIts3(SQLType, Codigo, MovimCod, MovimTwn,
      Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
      AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
      Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
      CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
      QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
      NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
      PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
      ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
      QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
      CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
      JmpMovID, JmpNivel1, JmpNivel2, JmpGGX,
      RmsMovID, RmsNivel1, RmsNivel2, RmsGGX,
(*
      CO_0_JmpMovID,
      CO_0_JmpNivel1,
      CO_0_JmpNivel2,
      CO_0_JmpGGX,
      CO_0_RmsMovID,
      CO_0_RmsNivel1,
      CO_0_RmsNivel2,
      CO_0_RmsGGX,
*)
      CO_0_GSPJmpMovID,
      CO_0_GSPJmpNiv2,
      CO_0_MovCodPai,
      CO_0_VmiPai,
      CO_0_IxxMovIX,
      CO_0_IxxFolha,
      CO_0_IxxLinha,
      CO_TRUE_ExigeClientMO,
      CO_TRUE_ExigeFornecMO,
      CO_TRUE_ExigeStqLoc,
      iuvpei004(*Couro PDA origem no processo de caleiro*)) then
      begin
        MovimNiv   := TEstqMovimNiv.eminEmPDABxa;
        Pecas      := -Pecas;
        AreaM2     := -AreaM2;
        AreaP2     := -AreaP2;
        PesoKg     := -PesoKg;
        ValorT     := -ValorT; //2023-05-17
        SrcMovID   := TEstqMovimID.emidEmRibPDA;
        SrcNivel1  := Codigo;
        SrcNivel2  := Controle;
        Controle   := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
        //
        if InsUpdVSMovIts3(SQLType, Codigo, MovimCod, MovimTwn,
        Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
        AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
        Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
        CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
        QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
        NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
        PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
        ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
        QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
        CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
        JmpMovID, JmpNivel1, JmpNivel2, JmpGGX,
        RmsMovID, RmsNivel1, RmsNivel2, RmsGGX,
(*
        CO_0_JmpMovID,
        CO_0_JmpNivel1,
        CO_0_JmpNivel2,
        CO_0_JmpGGX,
        CO_0_RmsMovID,
        CO_0_RmsNivel1,
        CO_0_RmsNivel2,
        CO_0_RmsGGX,
*)
        CO_0_GSPJmpMovID,
        CO_0_GSPJmpNiv2,
        CO_0_MovCodPai,
        CO_0_VmiPai,
        CO_0_IxxMovIX,
        CO_0_IxxFolha,
        CO_0_IxxLinha,
        CO_TRUE_ExigeClientMO,
        CO_TRUE_ExigeFornecMO,
        CO_TRUE_ExigeStqLoc,
        iuvpei005(*Baixa da MP atrav�s do PDA no processo de caleiro*)) then
        begin
          //  Atualiza VMI de caleiro!
          RmsMovID  := MovimID;
          RmsNivel1 := Codigo;
          RmsNivel2 := SrcNivel2;
          RmsGGX    := GGXRmsDst;
          Controle  := Qry.FieldByName('Controle').AsInteger;
          Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, '' + CO_UPD_TAB_VMI + '', False, [
          'RmsMovID', 'RmsNivel1', 'RmsNivel2', 'RmsGGX'], [
          'Controle'], [
          RmsMovID, RmsNivel1, RmsNivel2, RmsGGX], [
          Controle], True);
        end;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnVS_PF.CriaVMIRmsCur(VMICur, GGXRmsSrc, GGXRmsDst, GGXDTA: Integer;
  DTAPecas, DTAAreaM2, DTAPesoKg, CurPecas, CurAreaM2, CurPesoKg,
  CurValorT: Double; CurStqCenLoc: Integer): Boolean;
const
  sProcName = 'VS_PF.CriaVMIRmsCur()';
  //
  Nome = '';
  TipoArea = -1;
  NFeRem = -1;
  SerieRem = 0;
  LPFMO = '';
var
  Qry: TmySQLQuery;
  //
  SQLType: TSQLType;
  Codigo, MovimCod, MovimTwn, Empresa, Terceiro: Integer; MovimID: TEstqMovimID;
  SrcMovNiv, MovimNiv: TEstqMovimNiv;
  Pallet, GraGruX: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataHora, DtHrAberto: String;
  OriMovimID, SrcMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2: Integer;
  Observ: String;
  LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,Misturou*): Integer;
  CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
  DstMovID: TEstqMovimID;
  DstNivel1, DstNivel2: Integer;
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
  AptoUso, FornecMO, SerieFch: Integer; NotaMPAG: Double;
  SrcGGX, DstGGX: Integer;
  Marca: String;
  GSPSrcMovID: TEstqMovimID;
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2: Double;
  GGXRcl: Integer;
  JmpMovID, RmsMovID: TEstqMovimID;
  JmpNivel1, JmpNivel2, JmpGGX, RmsNivel1, RmsNivel2, RmsGGX: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM ' + CO_SEL_TAB_VMI,
    'WHERE Controle=' + Geral.FF0(VMICur),
    '']);
    SrcMovNiv  := TEstqMovimNiv(Qry.FieldByName('MovimNiv').AsInteger);
    OriMovimID := TEstqMovimID(Qry.FieldByName('SrcMovID').AsInteger);
    // ver se pegou registro certo!
    if (SrcMovNiv <> eminSorcCur) or (OriMovimID <> emidEmProcCal) then
    begin
      Geral.MB_Erro('Registro inv�lido para ' + sProcName + sLineBreak +
      'MovimID destino: ' + Geral.FF0(Qry.FieldByName('SrcMovID').AsInteger) +
      sLineBreak +
      'MovimNiv origem: ' + Geral.FF0(Qry.FieldByName('MovimNiv').AsInteger) +
      sLineBreak + 'Avise a Dermatek!');
      Exit;
    end;
    SQLType      := stIns;
    MovimID      := TEstqMovimID.emidEmRibDTA;
    MovimNiv     := TEstqMovimNiv.eminDestDTA;
    //
    Codigo       := Qry.FieldByName('SrcNivel1').AsInteger;
    //MovimCod   := Abaixo
    //Controle   := Abaixo
    //MovimTwn   := Abaixo
    Empresa      := Qry.FieldByName('Empresa').AsInteger;
    Terceiro     := Qry.FieldByName('Terceiro').AsInteger;
    Pallet       := 0;
    GraGruX      := GGXRmsDst;
    //Pecas      := Abaixo
    //PesoKg     := Abaixo
    AreaM2       := 0;
    AreaP2       := 0;
    //ValorT     := Abaixo
    DataHora     := Geral.FDT(Qry.FieldByName('DataHora').AsDateTime, 1);
    DtHrAberto   := DataHora;
    SrcMovID     := TEstqMovimID(0);
    SrcNivel1    := 0;
    SrcNivel2    := 0;
    Observ       := '';
    LnkNivXtr1   := 0;
    LnkNivXtr2   := 0;
    CliVenda     := 0;
    Ficha        := Qry.FieldByName('Ficha').AsInteger;
    CustoMOKg    := 0;
    CustoMOM2    := 0;
    CustoMOTot   := 0;
    ValorMP      := ValorT;
    DstMovID     := TEstqMovimID(0);
    DstNivel1    := 0;
    DstNivel2    := 0;
    //QtdGerPeca := Abaixo
    //QtdGerPeso := Abaixo
    QtdGerArM2   := 0;
    QtdGerArP2   := 0;
    AptoUso      := 0;
    FornecMO     := Qry.FieldByName('FornecMO').AsInteger;
    SerieFch     := Qry.FieldByName('SerieFch').AsInteger;
    NotaMPAG     := 0;
    SrcGGX       := 0;
    DstGGX       := 0;
    Marca        := Qry.FieldByName('Marca').AsString;
    TpCalcAuto   := -1;
    PedItsLib    := 0;
    PedItsFin    := 0;
    PedItsVda    := 0;
    GSPSrcMovID  := TEstqMovimID(0);
    GSPSrcNiv2   := 0;
    ReqMovEstq   := 0;
    StqCenLoc    := CurStqCenLoc;
    ItemNFe      := 0;
    VSMulFrnCab  := Qry.FieldByName('VSMulFrnCab').AsInteger;
    ClientMO     := Qry.FieldByName('ClientMO').AsInteger;
    QtdAntPeca   := 0;
    QtdAntPeso   := 0;
    QtdAntArM2   := 0;
    QtdAntArP2   := 0;
    GGXRcl       := 0;
    JmpMovID     := TEstqMovimID.emidAjuste;
    JmpNivel1    := 0;
    JmpNivel2    := 0;
    JmpGGX       := 0;
    RmsMovID     := TEstqMovimID(Qry.FieldByName('MovimID').AsInteger);
    RmsNivel1    := Qry.FieldByName('Codigo').AsInteger;
    RmsNivel2    := Qry.FieldByName('Controle').AsInteger;
    RmsGGX       := GGXRmsSrc; //Qry.FieldByName('GraGruX').AsInteger;
    //
(**)
    if (DTAPecas = 0)
    or (CurPecas = 0)
    or (DTAPesoKg = 0)
    or (CurPesoKg = 0)
    or (DTAPecas < CurPecas) then
    begin
      Geral.MB_Erro('C�lculo de peso inv�lido em ' + sProcName + sLineBreak +
      'Avise a Dermatek!');
      Exit;
    end;
    Pecas      := CurPecas;
    //PesoKg     := CurPecas / DTAPecas * DTAPesoKg;
    PesoKg     := CurPecas * (DTAPesoKg / DTAPecas);
    QtdGerPeca := CurPecas;
    QtdGerPeso := CurPesoKg;
    ValorT     := CurValorT;
(**)
{
    if (InnPecas = 0)
    or (CurPecas = 0)
    or (InnPesoKg = 0)
    or (CurPesoKg = 0)
    or (InnPesoKg < CurPesoKg) then
    begin
      Geral.MB_Erro('C�lculo de peso inv�lido em ' + sProcName + sLineBreak +
      'Avise a Dermatek!');
      Exit;
    end;
    Pecas      := CurPecas;
    PesoKg     := ?;
    QtdGerPeca := CurPecas;
    QtdGerPeso := CurPesoKg;
    ValorT     := CurValorT;
}
    //
    Codigo := UMyMod.BPGS1I32('vscaldta', 'Codigo', '', '', tsPos, SQLType, Codigo);
    MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
    MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, SQLType, MovimTwn);
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
    //
    //
    if SQLType = stIns then
      InsereVSMovCab(MovimCod, emidEmRibDTA, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vscaldta', False, [
    'MovimCod', 'Empresa',
    'DtHrAberto', 'DtHrFimOpe',
    'Nome', 'TipoArea', 'GraGruX',
    'NFeRem', 'SerieRem', 'LPFMO',
    'GGXDst', 'PesoKgSrc', 'PecasSrc'
    ], [
    'Codigo'], [
    MovimCod, Empresa,
    DtHrAberto, DtHrAberto,
    Nome, TipoArea, (*GraGruX*)GGXDTA,
    NFeRem, SerieRem, LPFMO,
    GGXDTA, DTAPesoKg, DTAPecas], [
    Codigo], True) then
    begin
      if InsUpdVSMovIts3(SQLType, Codigo, MovimCod, MovimTwn,
      Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
      AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
      Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
      CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
      QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
      NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
      PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
      ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
      QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
      CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
      JmpMovID, JmpNivel1, JmpNivel2, JmpGGX,
      RmsMovID, RmsNivel1, RmsNivel2, RmsGGX,
(*
      CO_0_JmpMovID,
      CO_0_JmpNivel1,
      CO_0_JmpNivel2,
      CO_0_JmpGGX,
      CO_0_RmsMovID,
      CO_0_RmsNivel1,
      CO_0_RmsNivel2,
      CO_0_RmsGGX,
*)
      CO_0_GSPJmpMovID,
      CO_0_GSPJmpNiv2,
      CO_0_MovCodPai,
      CO_0_VmiPai,
      CO_0_IxxMovIX,
      CO_0_IxxFolha,
      CO_0_IxxLinha,
      CO_TRUE_ExigeClientMO,
      CO_TRUE_ExigeFornecMO,
      CO_TRUE_ExigeStqLoc,
      iuvpei014(*Gera��o de Couro DTA no processo de curtimento*)) then
      begin
        MovimNiv   := TEstqMovimNiv.eminEmDTABxa;

        //GraGruX      := GGXRmsSrc;
        //RmsGGX       := GGXRmsDst;

        Pecas      := -Pecas;
        AreaM2     := -AreaM2;
        AreaP2     := -AreaP2;
        PesoKg     := -PesoKg;
        SrcMovID   := TEstqMovimID.emidEmRibDTA;
        SrcNivel1  := Codigo;
        SrcNivel2  := Controle;
        //JmpMovID   := TEstqMovimID(0);
        //JmpNivel1  := 0;
        //JmpNivel2  := 0;
        Controle   := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
        //
        if InsUpdVSMovIts3(SQLType, Codigo, MovimCod, MovimTwn,
        Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
        AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
        Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
        CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2,
        QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
        NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
        PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
        ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
        QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
        CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
        JmpMovID, JmpNivel1, JmpNivel2, JmpGGX,
        RmsMovID, RmsNivel1, RmsNivel2, RmsGGX,
(*
        CO_0_JmpMovID,
        CO_0_JmpNivel1,
        CO_0_JmpNivel2,
        CO_0_JmpGGX,
        CO_0_RmsMovID,
        CO_0_RmsNivel1,
        CO_0_RmsNivel2,
        CO_0_RmsGGX,
*)
        CO_0_GSPJmpMovID,
        CO_0_GSPJmpNiv2,
        CO_0_MovCodPai,
        CO_0_VmiPai,
        CO_0_IxxMovIX,
        CO_0_IxxFolha,
        CO_0_IxxLinha,
        CO_TRUE_ExigeClientMO,
        CO_TRUE_ExigeFornecMO,
        CO_TRUE_ExigeStqLoc,
        iuvpei015(*'Baixa de caleado na gera��o do DTA no processo de curtimento'*)) then
        begin
          //  Atualiza VMI de curtimento!
          RmsMovID  := MovimID;
          RmsNivel1 := Codigo;
          RmsNivel2 := SrcNivel2;
          RmsGGX    := GGXRmsDst;
          Controle  := Qry.FieldByName('Controle').AsInteger;
          Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, '' + CO_UPD_TAB_VMI + '', False, [
          'RmsMovID', 'RmsNivel1', 'RmsNivel2', 'RmsGGX'], [
          'Controle'], [
          RmsMovID, RmsNivel1, RmsNivel2, RmsGGX], [
          Controle], True);
        end;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_PF.DefineDataHoraOuDtMinima(const DataHora: TDateTime;
  TPData: TdmkEditDateTimePicker; EdHora: TdmkEdit);
begin
  if DataHora >= TPData.MinDate then
  begin
    TPData.Date := DataHora;
    EdHora.ValueVariant := DataHora;
  end else
  begin
    TPData.Date := TPData.MinDate;
    EdHora.ValueVariant := DataHora;
  end;
end;

function TUnVS_PF.DefineSQLPallet(const Obrigatorio: Boolean;
  const EdPallet: TdmkEditCB; var SQL: String): Boolean;
var
  Pallet: Integer;
begin
  Result := False;
  SQL := '';
  Pallet := EdPallet.ValueVariant;
  if MyObjects.FIC((Pallet = 0) and Obrigatorio, EdPallet,
  'Informe o Pallet!') then
    Exit;
  if Pallet <> 0 then
    SQL := 'AND vmi.Pallet=' + Geral.FF0(Pallet);
  Result := True;
end;

function TUnVS_PF.DefineSQLTerceiro(const Obrigatorio: Boolean;
  const EdTerceiro: TdmkEditCB; var SQL: String): Boolean;
var
  Terceiro: Integer;
begin
  Result := False;
  SQL := '';
  Terceiro := EdTerceiro.ValueVariant;
  if MyObjects.FIC((Terceiro = 0) and Obrigatorio, EdTerceiro,
  'Informe o terceiro!') then
    Exit;
  if Terceiro <> 0 then
    SQL := 'AND vmi.Terceiro=' + Geral.FF0(Terceiro);
  Result := True;
end;

{
function TUnVS_PF.EncerraPalletOld(VSPallet, CacCod: Integer; EncerrandoTodos: Boolean;
  FromBox: Variant; MovimIDGer: TEstqMovimID; Pergunta, Encerra, FromRcl:
  Boolean): Boolean;
  procedure EncerraPalletRcl(VSPallet: Integer);
  const
    EncerrandoTodos = False;
    Pergunta = False;
  var
    VSPaClaIts, Box, VMI_Sorc, VMI_Baix, VMI_Dest, CacCod, Codigo: Integer;
    ReabreVSPaRclCab: Boolean;
  begin
    Box        := -1; // para nao remover
    VSPaClaIts := 0;
    //VSPallet   := QrVSPalletCodigo.Value;
    VMI_Sorc   := 0;
    VMI_Baix   := 0;
    VMI_Dest   := 0;
    //
    CacCod     := 0;
    Codigo     := 0;
    ReabreVSPaRclCab := False;
    //if QrVSPalletDtHrEndAdd.Value < 2 then
    //begin
      (*Result := *)EncerraPalletReclassificacaoOld(CacCod,
      Codigo, VSPallet, Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
      EncerrandoTodos, Pergunta, ReabreVSPaRclCab);
    //end else
      //Geral.MB_Aviso('Pallet j� encerrado!');
  end;
const
  AptoUso = 1;
var
  //VSPaClaIts,
  Pallet, Controle, VMI_Sorc, VMI_Baix, VMI_Dest, Box, Terceiro,
  SerieFch, Ficha, Codigo: Integer;
  DtHrFim: String;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT, CustoM2: Double;
  Continua: Boolean;
  //
  QrVMIsDePal, QrSumDest, QrSumSorc, QrVMISorc, QrVMIDest, QrPalSorc,
  QrPaClaIts, QrSemOrig, QrRcl: TmySQLQuery;
  VMIs, VMI, Marca: String;
  NaoEncerra, EncerraReclass: Boolean;
  MovimID: TEstqMovimID;
begin
  if ClaOuRclAbertos(VSPallet, CacCod) then
    Exit;
  NaoEncerra := False;
  EncerraReclass := False;
  //Fechar todos vmi, nao apenas o ultimo!
  //
  Result := False;
  //
  if FromBox = null then
    Box := 0
  else
    Box := Integer(FromBox);
  //
(*  if not ObtemQryesBox(Box, QrVSPallet, QrItens, QrSum, QrSumPal) then
    Exit;
  if not ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Dest) then
    Exit;
*)

  if EncerrandoTodos or (not Pergunta) then
    Continua := True
  else
  begin
    if FromBox = null then
      Continua := Geral.MB_Pergunta('Deseja realmente encerrar o pallet ' +
      Geral.FF0(VSPallet) +  ' ?') = ID_YES
    else
      Continua := Geral.MB_Pergunta('Deseja realmente encerrar o pallet ' +
      Geral.FF0(VSPallet) + ' do Box ' + Geral.FF0(Box) + ' ?') = ID_YES;
  end;
  if not Continua then
    Exit;
  //
  if Encerra and (FromRcl = False) then
  begin
    QrRcl := TmySQLQuery.Create(Dmod);
    try
      // Ver se esta em alguma reclassificacao!
      UnDmkDAC_PF.AbreMySQLQuery0(QrRcl, Dmod.MyDB, [
      'SELECT * ',
      'FROM vscacitsa cia ',
      'LEFT JOIN vsparclitsa pci ON cia.Codigo=pci.Codigo ',
      'WHERE cia.VSPallet=' + Geral.FF0(VSPallet),
      'AND pci.VSPallet=' + Geral.FF0(VSPallet),
      'AND cia.CacID=8 ',
      'AND pci.DtHrFim < "1900-01-01" ',
      'ORDER BY cia.VMI_Dest ',
      '']);
      if QrRcl.RecordCount > 0 then
      begin
        //Exit;
        //Geral.MB_Aviso(
        //'Pallet n�o ser� encerrado pois est� sendo montando em reclassifica��o!');
        //NaoEncerra := True;
        //Exit;
        //
        EncerraReclass := True;
        Geral.MB_Aviso(
        'Pallet ser� encerrado tamb�m em reclassifica��o!');
      end;
    finally
      QrRcl.Free;
    end;
  end;

  QrVMIsDePal := TmySQLQuery.Create(Dmod);
  try
  QrSumDest := TmySQLQuery.Create(Dmod);
  try
  QrSumSorc := TmySQLQuery.Create(Dmod);
  try
  QrVMISorc := TmySQLQuery.Create(Dmod);
  try
  QrPalSorc := TmySQLQuery.Create(Dmod);
  try
  QrPaClaIts := TmySQLQuery.Create(Dmod);
  try
  QrVMIDest := TmySQLQuery.Create(Dmod);
  try
  //
  DtHrFim  := Geral.FDT(DmodG.ObtemAgora(), 109);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMIsDePal, Dmod.MyDB, [
  'SELECT DISTINCT VMI_Dest ',
  'FROM vscacitsa ',
  'WHERE VSPallet=' + Geral.FF0(VSPallet),
  // Parei Aqui 2015-03-12
  'AND CacID=7 ',
  'ORDER BY VMI_Dest ',
  '']);
  //Geral.MB_Teste(QrVMIsDePal);
  QrVMIsDePal.First;
  while not QrVMIsDePal.Eof do
  begin














    VMI_Dest := QrVMIsDePal.FieldByName('VMI_Dest').AsInteger;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrSumDest, Dmod.MyDB, [
    'SELECT VMI_Baix, SUM(Pecas) Pecas, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2 ',
    'FROM vscacitsa ',
    'WHERE VMI_Dest=' + Geral.FF0(VMI_Dest),
    '']);
    if QrSumDest.RecordCount > 1 then
    begin
      Result := False;
      Geral.MB_Erro('ERRO! Avise a DERMATEK' + sLineBreak +
      '"TUnVS_PF.EncerraPallet()" > Mais de um VMI_Baix por VMI_Dest!');
      Exit;
    end;
    VMI_Baix := QrSumDest.FieldByName('VMI_Baix').AsInteger;
    Pecas    := QrSumDest.FieldByName('Pecas').AsFloat;
    PesoKg   := 0;
    AreaM2   := QrSumDest.FieldByName('AreaM2').AsFloat;
    AreaP2   := QrSumDest.FieldByName('AreaP2').AsFloat;

    UnDmkDAC_PF.AbreMySQLQuery0(QrSumSorc, Dmod.MyDB, [
    'SELECT VMI_Sorc, SUM(AreaM2) AreaM2 ',
    'FROM vscacitsa ',
    'WHERE VSPallet=' + Geral.FF0(VSPallet),
    'AND VMI_Dest=' + Geral.FF0(VMI_Dest),
    'GROUP BY VMI_Sorc ',
    '']);
    //fazer query com sum do VMI_Sorc e usar no lugar do QrVSGerArtNew !
    ValorT := 0;
    VMIs   := '';
    QrSumSorc.First;
    while not QrSumSorc.Eof do
    begin
      VMI := Geral.FF0(QrSumSorc.FieldByName('VMI_Sorc').AsInteger);
      if VMIs <> '' then
        VMIs := VMIs + ', ';
      VMIS := VMIs + VMI;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrVMISorc, Dmod.MyDB, [
      'SELECT IF(AreaM2=0, 0, ValorT / AreaM2) CustoM2 ',
      'FROM v s m o v i t s ',
      'WHERE Controle=' + VMI,
      '']);
      //
      CustoM2 := QrVMISorc.FieldByName('CustoM2').AsFloat;
      ValorT := ValorT + (AreaM2  * CustoM2);
      //
      QrSumSorc.Next;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPalSorc, Dmod.MyDB, [
    'SELECT ',
    'IF(COUNT(DISTINCT vmi.SerieFch, vmi.Ficha) <> 1, "0", "1") Series_E_Fichas, ',
    'IF(COUNT(DISTINCT vmi.SerieFch) <> 1, 0.000, vmi.SerieFch) SerieFch, ',
    'IF(COUNT(DISTINCT vmi.Ficha) <> 1, 0.000, vmi.Ficha) Ficha, ',
    'IF(COUNT(DISTINCT vmi.Terceiro) <> 1, 0.000, vmi.Terceiro) Terceiro, ',
    'IF(COUNT(DISTINCT vmi.Marca) <> 1, "", vmi.Marca) Marca ',
    'FROM v s m o v i t s vmi ',
    'WHERE vmi.Controle IN (' + VMIs + ')',
    '']);
    Terceiro := Trunc(QrPalSorc.FieldByName('Terceiro').AsFloat);
    Marca    := QrPalSorc.FieldByName('Marca').AsString;
    if QrPalSorc.FieldByName('Series_E_Fichas').AsString = '1' then
    begin
      SerieFch := Trunc(QrPalSorc.FieldByName('SerieFch').AsFloat);
      Ficha    := Trunc(QrPalSorc.FieldByName('Ficha').AsFloat);
    end else
    begin
      SerieFch := 0;
      Ficha    := 0;
    end;
    //Pallet   := 0;
    //
    Controle := VMI_Baix;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'v s m o v i t s', False, [
    'Terceiro', (*'Pallet',*)
    'Pecas', 'PesoKg',
    'AreaM2', 'AreaP2', 'ValorT',
    (*'Observ',*) 'SerieFch', 'Ficha', (*'Misturou',*)
    'AptoUso', 'Marca'], [
    'Controle'], [
    Terceiro, (*Pallet,*)
    -Pecas, -PesoKg,
    -AreaM2, -AreaP2, -ValorT,
    (*Observ,*) SerieFch, Ficha, (*Misturou,*)
    AptoUso, Marca], [
    Controle], True) then
    begin
      Controle := VMI_Dest;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'v s m o v i t s', False, [
      'Terceiro', (*'Pallet',*)
      'Pecas', 'PesoKg',
      'AreaM2', 'AreaP2', 'ValorT',
      (*'Observ',*) 'SerieFch', 'Ficha', (*'Misturou',*)
      'AptoUso', 'Marca'], [
      'Controle'], [
      Terceiro, (*Pallet,*)
      Pecas, PesoKg,
      AreaM2, AreaP2, ValorT,
      (*Observ,*) SerieFch, Ficha, (*Misturou,*)
      AptoUso, Marca], [
      Controle], True) then
      begin
        AtualizaSaldoIMEI(VMI_Dest, False);
        //
        QrSumSorc.First;
        while not QrSumSorc.Eof do
        begin
          VMI_Sorc := QrSumSorc.FieldByName('VMI_Sorc').AsInteger;
          //
          if VMI_Sorc <> 0 then
            AtualizaSaldoIMEI(VMI_Sorc, False)
          else
          begin
            QrSemOrig := TmySQLQuery.Create(Dmod);
            try
              UnDmkDAC_PF.AbreMySQLQuery0(QrSemOrig, Dmod.MyDB, [
              'SELECT Controle ',
              'FROM v s m o v i t s ',
              'WHERE MovimID=' + Geral.FF0(Integer(emidSemOrigem)),
              'AND Pallet=' + Geral.FF0(VSPallet),
              '']);
              if QrSemOrig.RecordCount = 0 then
              begin
                // D� erro na reclassificacao! Realmente precisa para ela?
                if TEstqMovimID(MovimIDGer) <> emidReclasVS then
                  InsereVMI_SemOrig(VSPallet, VMI_Sorc);
              end else
                VMI_Sorc := QrSemOrig.FieldByName('Controle').AsInteger;
              //
              AtualizaSaldoVMI_SemOrig(VMI_Sorc, VSPallet);
            finally
              QrSemOrig.Free;
            end;
          end;
          //
          QrSumSorc.Next;
        end;
      end;
    end;
    //
    // 2015-03-30 Saber certo o MovimID!!
    UnDmkDAC_PF.AbreMySQLQuery0(QrVMIDest, Dmod.MyDB, [
    'SELECT MovimID ',
    'FROM v s m o v i t s ',
    'WHERE Controle=' + Geral.FF0(VMI_Dest),
    '']);
    MovimID := TEstqMovimID(QrVMIDest.FieldByName('MovimID').AsInteger);
    // FIM 2015-03-30
    case MovimID of
    //if MovimIDGer = emidClassArtXXUni then
      emidClassArtXXUni:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrPaClaIts, Dmod.MyDB, [
        'SELECT Controle ',
        'FROM vspaclaitsa ',
        'WHERE VMI_Dest=' + Geral.FF0(VMI_Dest),
        '']);
        //
        Controle := QrPaClaIts.FieldByName('Controle').AsInteger;
        //
        if Controle <> 0 then
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspaclaitsa', False, [
          'DtHrFim'], ['Controle'], [DtHrFim], [Controle], True) then ;
      end;
      //
      //if MovimIDGer = emidReclasVS then
      emidReclasVS:
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrPaClaIts, Dmod.MyDB, [
        'SELECT Controle ',
        'FROM vsparclitsa ',
        'WHERE VMI_Dest=' + Geral.FF0(VMI_Dest),
        '']);
        //
        Controle := QrPaClaIts.FieldByName('Controle').AsInteger;
        //
        if Controle <> 0 then
        begin
          if QrSumSorc.RecordCount <> 1 then
            VMI_Sorc := 0
          else
            VMI_Sorc := QrSumSorc.FieldByName('VMI_Sorc').AsInteger;
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclitsa', False, [
          'DtHrFim', 'VMI_Sorc'], [
          'Controle'], [
          DtHrFim, VMI_Sorc], [
          Controle], True) then ;
        end;
      end;
      emidClassArtXXMul:
      begin
        // Nada!!!!???
      end;
      else
      begin
        Geral.MB_Erro('"MovimID" n�o implementado no encerramento de Pallet!');
      end;
    end;
    //












    QrVMIsDePal.Next;
  end;
  //
  Codigo := VSPallet;
  //

  if EncerraReclass then
    EncerraPalletRcl(VSPallet);
  if Encerra then
  begin
    if NaoEncerra = False then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'VSPalleta', False, [
      'DtHrEndAdd'], ['Codigo'], [DtHrFim], [Codigo], True) then
      begin
        //RemovePallet(Box, not EncerrandoTodos);
        Result := True;
      end;
    end else
      Result := True;
  end else
    Result := True;
  //

  finally
    QrVMIDest.Free;
  end;
  finally
    QrPaClaIts.Free;
  end;
  finally
    QrPalSorc.Free;
  end;
  finally
    QrVMISorc.Free;
  end;
  finally
    QrSumSorc.Free;
  end;
  finally
    QrSumDest.Free;
  end;
  finally
    QrVMIsDePal.Free;
  end;
end;
}

(*
function TUnVS_PF.EncerraPalletReclassificacaoOld(const VSPaRclCabCacCod,
  VSPaRclCabCodigo, VSPaRclCabVSPallet, Box_Box, Box_VSPaClaIts, Box_VSPallet,
  Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
  const EncerrandoTodos, Pergunta: Boolean; var ReabreVSPaRclCab: Boolean): Boolean;
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    if DBCheck.CriaFm(TDfVSMod, DfVSMod, afmoSemVerificar) then
    begin
      //DfVSMod.ShowModal;
      Result := DfVSMod.EncerraPalletReclassificacaoOld(VSPaRclCabCacCod,
      VSPaRclCabCodigo, VSPaRclCabVSPallet, Box_Box, Box_VSPaClaIts,
      Box_VSPallet, Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest, EncerrandoTodos,
      Pergunta);
      //
      ReabreVSPaRclCab := DfVSMod.FReabreVSPaRclCab;
      //
      DfVSMod.Destroy;
    end;
  finally
    Screen.Cursor := MyCursor;
  end;
end;
*)

function TUnVS_PF.GeraSQL_Pall(SQL_Flds, SQL_Left, SQL_Where, SQL_Group: String;
  Tab: TTabToWork; TemIMEIMrt: Integer): String;
begin
  if GeraSQLTabMov(Result, Tab, TemIMEIMrt) then
    Result := Geral.ATS([
    'SELECT ' + TabMovVS_Fld_Pall(tab),
    SQL_NO_GGX(),
    'vsp.Nome NO_Pallet, ',
    (*'CAST(IF(COUNT(DISTINCT vmi.SerieFch, vmi.Ficha) <> 1, "0", "1") AS CHAR) Series_E_Fichas, ',
    'CAST(IF(COUNT(DISTINCT vmi.SerieFch) <> 1, 0, vmi.SerieFch) AS SIGNED) SerieFch, ',
    'CAST(IF(COUNT(DISTINCT vmi.Ficha) <> 1, 0, vmi.Ficha) AS SIGNED) Ficha, ',
    'CAST(IF(COUNT(DISTINCT vmi.Terceiro) <> 1, 0, vmi.Terceiro) AS SIGNED) Terceiro, ',
    'CAST(IF(COUNT(DISTINCT vmi.Marca) <> 1, "", vmi.Marca) AS CHAR) Marca, ',
    'CAST(IF(COUNT(DISTINCT(vmi.Terceiro))<>1, "", ',
    'IF((vmi.Terceiro=0) OR (vmi.Terceiro IS NULL), "",   ',
    'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome))) AS CHAR) NO_FORNECE,  ',
    'CAST(IF(COUNT(DISTINCT(vmi.SerieFch))<>1, "", ',
    'IF((vmi.SerieFch=0) OR (vmi.SerieFch IS NULL), "",   ',
    'vsf.Nome)) AS CHAR) NO_SerieFch,  ',*)
//  Codigo                         int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.Codigo AS SIGNED) Codigo, ',
//  Controle                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.Controle AS SIGNED) Controle, ',
//  MovimCod                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.MovimCod AS SIGNED) MovimCod, ',
//MovimNiv                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.MovimNiv AS SIGNED) MovimNiv, ',
//MovimTwn                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.MovimTwn AS SIGNED) MovimTwn, ',
//Empresa                        int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.Empresa AS SIGNED) Empresa, ',
//Terceiro                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.Terceiro AS SIGNED) Terceiro, ',
//CliVenda                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.CliVenda AS SIGNED) CliVenda, ',
//MovimID                        int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.MovimID AS SIGNED) MovimID, ',
//LnkIDXtr                       int(11)      NOT NULL  DEFAULT "0"
//LnkNivXtr1                     int(11)      NOT NULL  DEFAULT "0"
//LnkNivXtr2                     int(11)      NOT NULL  DEFAULT "0"
//DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00"
//    'CAST(vmi.DataHora AS DATETIME) DataHora, ',
//Pallet                         int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.Pallet AS SIGNED) Pallet, ',
//GraGruX                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.GraGruX AS SIGNED) GraGruX, ',
//Pecas                          double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(SUM(vmi.Pecas) AS DECIMAL (15,3)) Pecas, ',
//PesoKg                         double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(SUM(vmi.PesoKg) AS DECIMAL (15,3)) PesoKg, ',
//AreaM2                         double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(SUM(vmi.AreaM2) AS DECIMAL (15,2)) AreaM2, ',
//AreaP2                         double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(SUM(vmi.AreaP2) AS DECIMAL (15,2)) AreaP2, ',
//ValorT                         double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(SUM(vmi.ValorT) AS DECIMAL (15,2)) ValorT, ',
//SrcMovID                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.SrcMovID AS SIGNED) SrcMovID, ',
//SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.SrcNivel1 AS SIGNED) SrcNivel1, ',
//SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.SrcNivel2 AS SIGNED) SrcNivel2, ',
//SrcGGX                         int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.SrcGGX AS SIGNED) SrcGGX, ',
//SdoVrtPeca                     double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(SUM(IF(vmi.SdoVrtPeca > 0, vmi.SdoVrtPeca, 0)) AS DECIMAL (15,3)) SdoVrtPeca, ',
//SdoVrtPeso                     double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(SUM(IF(vmi.SdoVrtPeca > 0, vmi.SdoVrtPeso, 0)) AS DECIMAL (15,3)) SdoVrtPeso, ',
//SdoVrtArM2                     double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(SUM(IF(vmi.SdoVrtPeca > 0, vmi.SdoVrtArM2, 0)) AS DECIMAL (15,2)) SdoVrtArM2 ',
//Observ                         varchar(255) NOT NULL
//    'CAST(vmi.Observ AS CHAR) Observ, ',
//SerieFch                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.SerieFch AS SIGNED) SerieFch, ',
//Ficha                          int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.Ficha AS SIGNED) Ficha, ',
//Misturou                       tinyint(1)   NOT NULL  DEFAULT "0"
//    'CAST(vmi.Misturou AS UNSIGNED) Misturou, ',
//FornecMO                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.FornecMO AS SIGNED) FornecMO, ',
//CustoMOKg                      double(15,6) NOT NULL  DEFAULT "0.000000"
//    'CAST(vmi.CustoMOKg AS DECIMAL (15,6)) CustoMOKg, ',
//CustoMOTot                     double(15,2) NOT NULL  DEFAULT "0.00"
//    'CAST(vmi.CustoMOTot AS DECIMAL (15,2)) CustoMOTot, ',
//ValorMP                        double(15,4) NOT NULL  DEFAULT "0.0000"
//    'CAST(vmi.ValorMP AS DECIMAL (15,4)) ValorMP, ',
//DstMovID                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.DstMovID AS SIGNED) DstMovID, ',
//DstNivel1                      int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.DstNivel1 AS SIGNED) DstNivel1, ',
//DstNivel2                      int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.DstNivel2 AS SIGNED) DstNivel2, ',
//DstGGX                         int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.DstGGX AS SIGNED) DstGGX, ',
//QtdGerPeca                     double(15,3) NOT NULL  DEFAULT "0.000"
//    'CAST(vmi.QtdGerPeca AS DECIMAL (15,3)) QtdGerPeca, ',
//QtdGerPeso                     double(15,3) NOT NULL  DEFAULT "0.000"
//    'CAST(vmi.QtdGerPeso AS DECIMAL (15,3)) QtdGerPeso, ',
//QtdGerArM2                     double(15,2) NOT NULL  DEFAULT "0.00"
//    'CAST(vmi.QtdGerArM2 AS DECIMAL (15,2)) QtdGerArM2, ',
//QtdGerArP2                     double(15,2) NOT NULL  DEFAULT "0.00"
//    'CAST(vmi.QtdGerArP2 AS DECIMAL (15,2)) QtdGerArP2, ',
//QtdAntPeca                     double(15,3) NOT NULL  DEFAULT "0.000"
//    'CAST(vmi.QtdAntPeca AS DECIMAL (15,3)) QtdAntPeca, ',
//QtdAntPeso                     double(15,3) NOT NULL  DEFAULT "0.000"
//    'CAST(vmi.QtdAntPeso AS DECIMAL (15,3)) QtdAntPeso, ',
//QtdAntArM2                     double(15,2) NOT NULL  DEFAULT "0.00"
//    'CAST(vmi.QtdAntArM2 AS DECIMAL (15,2)) QtdAntArM2, ',
//QtdAntArP2                     double(15,2) NOT NULL  DEFAULT "0.00"
//    'CAST(vmi.QtdAntArP2 AS DECIMAL (15,2)) QtdAntArP2, ',
//AptoUso                        tinyint(1)   NOT NULL  DEFAULT "1"
//NotaMPAG                       double(15,8) NOT NULL  DEFAULT "0.00000000"
//    'CAST(vmi.NotaMPAG AS DECIMAL (15,8)) NotaMPAG, ',
//Marca                          varchar(20)
//    'CAST(vmi.Marca AS CHAR) Marca, ',
//TpCalcAuto                     int(11)      NOT NULL  DEFAULT "-1"
//Zerado                         tinyint(1)   NOT NULL  DEFAULT "0"
//EmFluxo                        tinyint(1)   NOT NULL  DEFAULT "1"
//NotFluxo                       int(11)      NOT NULL  DEFAULT "0"
//FatNotaVNC                     double(15,8) NOT NULL  DEFAULT "0.00000000"
//FatNotaVRC                     double(15,8) NOT NULL  DEFAULT "0.00000000"
//PedItsLib                      int(11)      NOT NULL  DEFAULT "0"
//PedItsFin                      int(11)      NOT NULL  DEFAULT "0"
//PedItsVda                      int(11)      NOT NULL  DEFAULT "0"
//Lk                             int(11)                DEFAULT "0"
//DataCad                        date
//DataAlt                        date
//UserCad                        int(11)                DEFAULT "0"
//UserAlt                        int(11)                DEFAULT "0"
//AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"
//Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"
//CustoMOM2                      double(15,6) NOT NULL  DEFAULT "0.000000"
//    'CAST(vmi.CustoMOM2 AS DECIMAL (15,6)) CustoMOM2, ',
//ReqMovEstq                     int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.ReqMovEstq AS SIGNED) ReqMovEstq, ',
//StqCenLoc                      int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.StqCenLoc AS SIGNED) StqCenLoc, ',
//ItemNFe                        int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.ItemNFe AS SIGNED) ItemNFe, ',
//VSMorCab                       int(11)      NOT NULL  DEFAULT "0"
    //'vmi.*,
    SQL_Flds,
    'FROM ' + TabMovVS_Tab(tab) + ' vmi ',
    SQL_LJ_GGX(),
    'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
(*
    'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
    'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
*)
    SQL_Left,
    SQL_Where,
    SQL_Group,
    Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
    '']);
end;

function TUnVS_PF.HabilitaMenuItensVSAberto(Qry: TmySQLQuery; Data: TDateTime;
  MenuItens: array of TMenuItem): Boolean;
var
  Habilita: Boolean;
  I: Integer;
begin
  if (Qry <> nil) and (Qry.State <> dsInactive) and (Qry.RecordCount = 0) then
    Habilita := True
  else if (Qry <> nil) and (Qry.State = dsInactive) then
    Habilita := True
  else
    Habilita := Geral.Periodo2000(Data) > (VAR_VS_PERIODO_BAL - 1);
  for I := Low(MenuItens) to High(MenuItens) do
    TMenuItem(MenuItens[I]).Enabled := Habilita;

end;

function TUnVS_PF.IMEC_JaEstaNoArray(const IMEC, MovimID, IMEI_Pai: Integer;
  const Lista: TIMECArr; var ItensLoc: Integer): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to Length(Lista) -1 do
  begin
    if Lista[I][0] = IMEC then
    begin
      if Lista[I][1] = MovimID then
      begin
        if Lista[I][2] = IMEI_Pai then
        begin
          Result := True;
          ItensLoc := ItensLoc + 1;
          Exit;
        end;
      end;
    end;
  end;
end;

function TUnVS_PF.IMEx_InsereNoArray(IMEx: TIMExReg; Lista: TIMExArr): Boolean;
var
  I, j, K: Integer;
begin
  Result := False;
  SetLength(Lista, Length(Lista) + 1);
  for I := 0 to Length(Lista) -1 do
    for J := 0 to 31 do
      for K := 0 to 1 do
        Lista[I][J][K] := IMEx[J][K];
end;

function TUnVS_PF.IMEx_JaEstaNoArray(IMEx: TIMExReg; Lista: TIMExArr): Boolean;
var
  I, j, K, Certos: Integer;
begin
  Result := False;
  for I := 0 to Length(Lista) -1 do
  begin
    Certos := 0;
    for J := 0 to 31 do
      for K := 0 to 1 do
        if Lista[I][J][K] = IMEx[J][K] then
          Certos := Certos + 1;
    if Certos = 64 then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

function TUnVS_PF.ImpedePeloBalanco(Data: TDateTime; PermiteCorrApo, Avisa: Boolean): Boolean;
var
  Dia: TDateTime;
  BalVal, Meses: Integer;
begin
  if Dmod.QrControleCanAltBalVS.Value = 1 then
    Result := False
  else begin
    Result := True;
    if PermiteCorrApo then
      Meses := 2
    else
      Meses := 0;
    //
    BalVal := VerificaBalanco(Meses);
    Dia := StrToDate(dmkPF.PrimeiroDiaAposPeriodo(BalVal, dtSystem3));
    if Data >= Dia then
      Result := False
    else
      if Avisa then
      Geral.MB_Aviso('Data inv�lida!.' + sLineBreak +
      'J� existe balan�o com data posterior.' + sLineBreak +
      'Solicite ao seu superior o desbloqueio em: ' + sLineBreak +
      'Op��es espec�ficas do aplicagtivo,' + sLineBreak +
      'na aba "' + CO_PODE_ALTERAR_ESTOQUE_PRODUTO_Aba + '" marque a op��o:' + sLineBreak+
      '"' + CO_PODE_ALTERAR_ESTOQUE_PRODUTO_Chk + '".');
  end;
end;

procedure TUnVS_PF.ImportaDadosNFeCab(QuemEmit: Integer; EdEmit: TdmkEditCB;
  CBEmit: TdmkDBLookupComboBox; EdDest: TdmkEditCB; CBDest: TdmkDBLookupComboBox;
  TPEmi: TdmkEditDateTimePicker; EdEmi: TDmkEdit; TPSaiEnt: TdmkEditDateTimePicker;
  EdSaiEnt: TDmkEdit; EdSerie, EdnNF: TDmkEdit);
var
  Continua: Boolean;
  Qry: TmySQLQuery;
  IDCtrl: Integer;
begin
  {$IfNDef semNFe_v0000}
  Continua := Grl_Geral.LiberaModulo(CO_DMKID_APP, Grl_Geral.ObtemSiglaModulo(mdlappNFe),
                DModG.QrMasterHabilModulos.Value);
  {$Else}
  Continua := False;
  {$EndIf}
  if Continua then
  begin
    VAR_CADASTRO := 0;
    //
    UnNFe_PF.MostraFormNFePesq(False, nil, nil, 0, QuemEmit);
    //
    if (VAR_CADASTRO <> 0) then
    begin
      IDCtrl := VAR_CADASTRO;
      Qry    := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT ide_serie, ide_nNF, CodInfoEmit, ',
          'CodInfoDest, ide_dEmi, ide_dSaiEnt ',
          'FROM nfecaba ',
          'WHERE IDCtrl=' + Geral.FF0(IDCtrl),
          '']);
        if Qry.RecordCount > 0 then
        begin
          if EdEmit <> nil then
            EdEmit.ValueVariant := Qry.FieldByName('CodInfoEmit').AsInteger;
          if CBEmit <> nil then
            CBEmit.KeyValue := Qry.FieldByName('CodInfoEmit').AsInteger;
          if EdDest <> nil then
            EdDest.ValueVariant := Qry.FieldByName('CodInfoDest').AsInteger;
          if CBDest <> nil then
            CBDest.KeyValue := Qry.FieldByName('CodInfoDest').AsInteger;
          if TPEmi <> nil then
            TPEmi.Date := Qry.FieldByName('ide_dEmi').AsDateTime;
          if EdEmi <> nil then
            EdEmi.ValueVariant := Qry.FieldByName('ide_dEmi').AsDateTime;
          if TPSaiEnt <> nil then
            TPSaiEnt.Date := Qry.FieldByName('ide_dSaiEnt').AsDateTime;
          if EdSaiEnt <> nil then
            EdSaiEnt.ValueVariant := Qry.FieldByName('ide_dSaiEnt').AsDateTime;
          if EdSerie <> nil then
            EdSerie.ValueVariant := Qry.FieldByName('ide_serie').AsInteger;
          if EdnNF <> nil then
            EdnNF.ValueVariant := Qry.FieldByName('ide_nNF').AsInteger;
        end;
      finally
        Qry.Free;
      end;
    end;
  end else
    Geral.MB_Aviso('Voc� n�o est� habilitado para utilizar o m�dulo "' +
      Grl_Geral.ObtemSiglaModulo(mdlappNFe) + '"!' + sLineBreak +
      'Solicite sua libera��o junto a Dermatek!');
end;

procedure TUnVS_PF.ImprimeClassFichaRMP_Mul_Old(SerieFch: Integer; NO_Serie: String;
  Fichas: array of Integer; MostraJanela: Boolean; PB: TProgressBar; LaAviso1,
  LaAviso2: TLabel);
begin
  if DBCheck.CriaFm(TFmVSImpClaFichaRMP, FmVSImpClaFichaRMP, afmoLiberado) then
  begin
    FmVSImpClaFichaRMP.FFichas    := MyObjects.CordaDeArrayInt(Fichas);
    FmVSImpClaFichaRMP.FSerieFch := SerieFch;
    FmVSImpClaFichaRMP.FNO_Serie := NO_Serie;
    //
    if MostraJanela then
    begin
      FmVSImpClaFichaRMP.ImprimeClassFichaRMP_Mul(False, PB, LaAviso1, LaAviso2);
      FmVSImpClaFichaRMP.ShowModal;
    end else
      FmVSImpClaFichaRMP.ImprimeClassFichaRMP_Mul(True, PB, LaAviso1, LaAviso2);
    FmVSImpClaFichaRMP.Destroy;
  end;
end;

procedure TUnVS_PF.ImprimeClassFichaRMP_Mul_New(SerieFch: Integer; NO_Serie: String;
  Fichas: array of Integer; MostraJanela: Boolean; PB: TProgressBar; LaAviso1,
  LaAviso2: TLabel);
begin
  if DBCheck.CriaFm(TFmVSImpClaFichaRMP2, FmVSImpClaFichaRMP2, afmoLiberado) then
  begin
    FmVSImpClaFichaRMP2.FFichas    := MyObjects.CordaDeArrayInt(Fichas);
    FmVSImpClaFichaRMP2.FSerieFch := SerieFch;
    FmVSImpClaFichaRMP2.FNO_Serie := NO_Serie;
    //
    if MostraJanela then
    begin
      FmVSImpClaFichaRMP2.ImprimeClassFichaRMP_Mul(False, PB, LaAviso1, LaAviso2);
      FmVSImpClaFichaRMP2.ShowModal;
    end else
      FmVSImpClaFichaRMP2.ImprimeClassFichaRMP_Mul(True, PB, LaAviso1, LaAviso2);
    FmVSImpClaFichaRMP2.Destroy;
  end;
end;

procedure TUnVS_PF.ImprimeClassFichaRMP_Uni_Old(SerieFch, Ficha: Integer; NO_Serie:
  String; MostraJanela, PorMartelo: Boolean);
begin
  if DBCheck.CriaFm(TFmVSImpClaFichaRMP, FmVSImpClaFichaRMP, afmoLiberado) then
  begin
    FmVSImpClaFichaRMP.FFicha      := Ficha;
    FmVSImpClaFichaRMP.FSerieFch   := SerieFch;
    FmVSImpClaFichaRMP.FNO_Serie   := NO_Serie;
    FmVSImpClaFichaRMP.FPorMartelo := PorMartelo;
    //
    if MostraJanela then
    begin
      FmVSImpClaFichaRMP.ImprimeClassFichaRMP_Uni(False);
      FmVSImpClaFichaRMP.ShowModal;
    end else
      FmVSImpClaFichaRMP.ImprimeClassFichaRMP_Uni(True);
    FmVSImpClaFichaRMP.Destroy;
  end;
end;

procedure TUnVS_PF.ImprimeClassFichaRMP_Uni_New(SerieFch, Ficha: Integer; NO_Serie:
  String; MostraJanela, PorMartelo: Boolean);
begin
  if DBCheck.CriaFm(TFmVSImpClaFichaRMP2, FmVSImpClaFichaRMP2, afmoLiberado) then
  begin
    FmVSImpClaFichaRMP2.FFicha      := Ficha;
    FmVSImpClaFichaRMP2.FSerieFch   := SerieFch;
    FmVSImpClaFichaRMP2.FNO_Serie   := NO_Serie;
    FmVSImpClaFichaRMP2.FPorMartelo := PorMartelo;
    //
    if MostraJanela then
    begin
      FmVSImpClaFichaRMP2.ImprimeClassFichaRMP_Uni(False);
      FmVSImpClaFichaRMP2.ShowModal;
    end else
      FmVSImpClaFichaRMP2.ImprimeClassFichaRMP_Uni(True);
    FmVSImpClaFichaRMP2.Destroy;
  end;
end;

procedure TUnVS_PF.ImprimeComparaCacIts(PalletA, PalletB: Integer);
begin
  //Result := False;
  if DBCheck.CriaFm(TFmVSComparaCacIts, FmVSComparaCacIts, afmoLiberado) then
  begin
    FmVSComparaCacIts.EdPalletA.ValueVariant     := PalletA;
    FmVSComparaCacIts.EdPalletB.ValueVariant     := PalletB;
    //
    FmVSComparaCacIts.ShowModal;
    //Result := FmVSComparaCacIts.ImprimeEstoqueEm();
    FmVSComparaCacIts.Destroy;
  end;
end;

function TUnVS_PF.ImprimeEstoqueEm(Entidade, Filial, Ed00Terceiro_ValueVariant,
  RG00_Ordem1_ItemIndex, RG00_Ordem2_ItemIndex, RG00_Ordem3_ItemIndex,
  RG00_Ordem4_ItemIndex, RG00_Ordem5_ItemIndex, RG00_Agrupa_ItemIndex: Integer;
  Ck00DescrAgruNoItm_Checked: Boolean; Ed00StqCenCad_ValueVariant,
  RG00ZeroNegat_ItemIndex: Integer; FNO_EMPRESA, CB00StqCenCad_Text,
  CB00Terceiro_Text: String; TPDataRelativa_Date: TDateTime;
  Ck00DataCompra_Checked, Ck00EmProcessoBH_Checked: Boolean;
  DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2: TdmkDBGridZTO;
  Qr00GraGruY, Qr00GraGruX, Qr00CouNiv2: TmySQLQuery; DataEm: TDateTime;
  // 2021-01-09 fim
  //GraCusPrc: Integer;
  Relatorio: TRelatorioVSImpEstoque;
  // 2021-01-09 fim
  Ed00NFeIni_ValueVariant, Ed00NFeFim_ValueVariant: Integer;
  Ck00Serie_Checked: Boolean; Ed00Serie_ValueVariant, MovimCod, ClientMO: Integer;
  LaAviso1, LaAviso2: TLabel; MostraFrx: Boolean): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmVSImpEstqEm, FmVSImpEstqEm, afmoLiberado) then
  begin
    FmVSImpEstqEm.FEntidade                  := Entidade;
    FmVSImpEstqEm.FFilial                    := Filial;
    FmVSImpEstqEm.Ed00Terceiro_ValueVariant  := Ed00Terceiro_ValueVariant;
    FmVSImpEstqEm.RG00_Ordem1_ItemIndex      := RG00_Ordem1_ItemIndex;
    FmVSImpEstqEm.RG00_Ordem2_ItemIndex      := RG00_Ordem2_ItemIndex;
    FmVSImpEstqEm.RG00_Ordem3_ItemIndex      := RG00_Ordem3_ItemIndex;
    FmVSImpEstqEm.RG00_Ordem4_ItemIndex      := RG00_Ordem4_ItemIndex;
    FmVSImpEstqEm.RG00_Ordem5_ItemIndex      := RG00_Ordem5_ItemIndex;
    FmVSImpEstqEm.RG00_Agrupa_ItemIndex      := RG00_Agrupa_ItemIndex;
    FmVSImpEstqEm.Ck00DescrAgruNoItm_Checked := Ck00DescrAgruNoItm_Checked;
    FmVSImpEstqEm.Ed00StqCenCad_ValueVariant := Ed00StqCenCad_ValueVariant;
    FmVSImpEstqEm.RG00ZeroNegat_ItemIndex    := RG00ZeroNegat_ItemIndex;
    FmVSImpEstqEm.FNO_EMPRESA                := FNO_EMPRESA;
    FmVSImpEstqEm.CB00StqCenCad_Text         := CB00StqCenCad_Text;
    FmVSImpEstqEm.CB00Terceiro_Text          := CB00Terceiro_Text;
    FmVSImpEstqEm.TPDataRelativa_Date        := TPDataRelativa_Date;
    FmVSImpEstqEm.Ck00DataCompra_Checked     := Ck00DataCompra_Checked;
    FmVSImpEstqEm.Ck00EmProcessoBH_Checked   := Ck00EmProcessoBH_Checked;
    FmVSImpEstqEm.DBG00GraGruY               := DBG00GraGruY;
    FmVSImpEstqEm.DBG00GraGruX               := DBG00GraGruX;
    FmVSImpEstqEm.DBG00CouNiv2               := DBG00CouNiv2;
    FmVSImpEstqEm.Qr00GraGruY                := Qr00GraGruY;
    FmVSImpEstqEm.Qr00GraGruX                := Qr00GraGruX;
    FmVSImpEstqEm.Qr00CouNiv2                := Qr00CouNiv2;
    FmVSImpEstqEm.FDataEm                    := DataEm;
    // 2021-01-09 fim
    //FmVSImpEstqEm.FGraCusPrc                 := GraCusPrc;
    FmVSImpEstqEm.FRelatorio                 := Relatorio;
    // 2021-01-09 fim
    FmVSImpEstqEm.FEd00NFeIni_ValueVariant   := Ed00NFeIni_ValueVariant;
    FmVSImpEstqEm.FEd00NFeFim_ValueVariant   := Ed00NFeFim_ValueVariant;
    FmVSImpEstqEm.FCk00Serie_Checked         := Ck00Serie_Checked;
    FmVSImpEstqEm.FEd00Serie_ValueVariant    := Ed00Serie_ValueVariant;
    FmVSImpEstqEm.FLaAviso1                  := LaAviso1;
    FmVSImpEstqEm.FLaAviso2                  := LaAviso2;
    FmVSImpEstqEm.FMovimCod                  := MovimCod;
    FmVSImpEstqEm.FClientMO                  := ClientMO;
    FmVSImpEstqEm.FMostraFrx                 := MostraFrx;
    //
    //FmVSImpEstqEm.ShowModal;
    Result := FmVSImpEstqEm.ImprimeEstoqueEm();
    FmVSImpEstqEm.Destroy;
  end;
end;

procedure TUnVS_PF.ImprimeFichaEstoque(Empresa: Integer; NO_Emp, Responsa: String);
const
  Reduzido = False;
begin
  Application.CreateForm(TFmVSImpRequis, FmVSImpRequis);
(*
  FmVSImpRequis.FEmpresa_Cod   := QrVSReqDivEmpresa.Value;
  FmVSImpRequis.FEmpresa_Txt   := QrVSReqDivNO_EMP.Value;
  FmVSImpRequis.FResponsa      := QrVSReqDivResponsa.Value;
*)
  FmVSImpRequis.FEmpresa_Cod   := Empresa;
  FmVSImpRequis.FEmpresa_Txt   := NO_EMP;
  FmVSImpRequis.FResponsa      := Responsa;
  //
  FmVSImpRequis.FQuantidade    :=  0;
  FmVSImpRequis.FNumInicial    :=  0;
  FmVSImpRequis.FNumVias       :=  0;
  FmVSImpRequis.FReduzido      :=  Reduzido;
  //
  FmVSImpRequis.ImprimeFichaEstoque();
  //
  FmVSImpRequis.Destroy;
end;

procedure TUnVS_PF.ImprimeHistPall(Empresa_Cod: Integer; Empresa_Txt: String;
  Pallets: array of Integer);
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmVSImpHistPall, FmVSImpHistPall, afmoLiberado) then
  begin
    FmVSImpHistPall.FEmpresa_Cod := Empresa_Cod;
    FmVSImpHistPall.FEmpresa_Txt := Empresa_Txt;
    SetLength(FmVSImpHistPall.FPallets, Length(Pallets));
    for I := Low(Pallets) to High(Pallets) do
      FmVSImpHistPall.FPallets[I] := Pallets[I];
    //FmVSImpHistPall.ShowModal;
    FmVSImpHistPall.ImprimeHistPall();
    FmVSImpHistPall.Destroy;
  end;
end;

procedure TUnVS_PF.ImprimeIMEIsPallets(Empresa_Cod: Integer; Empresa_Txt: String;
  Pallets: array of Integer; Ordenacao: Integer(*; InfoNO_PALLET: Boolean*));
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmVSImpPallet, FmVSImpPallet, afmoLiberado) then
  begin
    //FmVSImpPallet.ShowModal;
    SetLength(FmVSImpPallet.FPallets, Length(Pallets));
    for I := Low(Pallets) to High(Pallets) do
      FmVSImpPallet.FPallets[I] := Pallets[I];
    FmVSImpPallet.FEmpresa_Cod := Empresa_Cod;
    FmVSImpPallet.FEmpresa_Txt := Empresa_Txt;
    //FmVSImpPallet.FInfoNO_PALLET := InfoNO_PALLET;
    FmVSImpPallet.ImprimeIMEIsPallets(Ordenacao);
    FmVSImpPallet.Destroy;
  end;
end;

procedure TUnVS_PF.ImprimeNotaMPAG(RG14Periodo_ItemIndex,
  RG14FontePsq_ItemIndex, Ed14Terceiro_ValueVariant, Ed14SerieFch_ValueVariant,
  Ed14Ficha_ValueVariant, RG14Ordem1_ItemIndex, RG14Ordem2_ItemIndex,
  RG14Ordem3_ItemIndex, Ed14GraGruX_ValueVariant, RG14Agrupa_ItemIndex: Integer;
  TP14DataIni_Date, TP14DataFim_Date: TDateTime; Ck14DataIni_Checked,
  Ck14DataFim_Checked, Ck14SoMPAGNo0_Checked, Ck14Cor_Checked: Boolean;
  EdEmpresa: TdmkEditCB; CB14SerieFch: TdmkDBLookupComboBox; CBEmpresa_Text,
  CB14Terceiro_Text, Ed14Marca_Text, CB14GraGruX_Text: String);
begin
  if DBCheck.CriaFm(TFmVSImpNotaMPAG, FmVSImpNotaMPAG, afmoLiberado) then
  begin
    FmVSImpNotaMPAG.RG14FontePsq_ItemIndex    := RG14FontePsq_ItemIndex;
    FmVSImpNotaMPAG.Ed14Terceiro_ValueVariant := Ed14Terceiro_ValueVariant;
    FmVSImpNotaMPAG.Ed14SerieFch_ValueVariant := Ed14SerieFch_ValueVariant;
    FmVSImpNotaMPAG.Ed14Ficha_ValueVariant    := Ed14Ficha_ValueVariant;
    FmVSImpNotaMPAG.RG14Ordem1_ItemIndex      := RG14Ordem1_ItemIndex;
    FmVSImpNotaMPAG.RG14Ordem2_ItemIndex      := RG14Ordem2_ItemIndex;
    FmVSImpNotaMPAG.RG14Ordem3_ItemIndex      := RG14Ordem3_ItemIndex;
    FmVSImpNotaMPAG.Ed14GraGruX_ValueVariant  := Ed14GraGruX_ValueVariant;
    FmVSImpNotaMPAG.RG14Agrupa_ItemIndex      := RG14Agrupa_ItemIndex;
    FmVSImpNotaMPAG.TP14DataIni_Date          := TP14DataIni_Date;
    FmVSImpNotaMPAG.TP14DataFim_Date          := TP14DataFim_Date;
    FmVSImpNotaMPAG.Ck14DataIni_Checked       := Ck14DataIni_Checked;
    FmVSImpNotaMPAG.Ck14DataFim_Checked       := Ck14DataFim_Checked;
    FmVSImpNotaMPAG.Ck14SoMPAGNo0_Checked     := Ck14SoMPAGNo0_Checked;
    FmVSImpNotaMPAG.Ck14Cor_Checked           := Ck14Cor_Checked;
    FmVSImpNotaMPAG.EdEmpresa                 := EdEmpresa;
    FmVSImpNotaMPAG.CB14SerieFch              := CB14SerieFch;
    FmVSImpNotaMPAG.CBEmpresa_Text            := CBEmpresa_Text;
    FmVSImpNotaMPAG.CB14Terceiro_Text         := CB14Terceiro_Text;
    FmVSImpNotaMPAG.Ed14Marca_Text            := Ed14Marca_Text;
    FmVSImpNotaMPAG.CB14GraGruX_Text          := CB14GraGruX_Text;
    //FmVSImpNotaMPAG.ShowModal;
    FmVSImpNotaMPAG.ImprimeNotaMPAG();
    FmVSImpNotaMPAG.Destroy;
  end;
end;

procedure TUnVS_PF.ImprimePackListsPallets(Empresa: Integer;
  Pallets: array of Integer; Vertical: Boolean; VSMovImp4, VSLstPalBox: String;
   Grandeza: TGrandezaArea);
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmVSImpPackList, FmVSImpPackList, afmoLiberado) then
  begin
    //FmVSImpPackList.ShowModal;
    SetLength(FmVSImpPackList.FPallets, Length(Pallets));
    for I := Low(Pallets) to High(Pallets) do
      FmVSImpPackList.FPallets[I] := Pallets[I];
    FmVSImpPackList.FEmpresa := Empresa;
    FmVSImpPackList.FGrandeza := Grandeza;
    FmVSImpPackList.ImprimePallets(Vertical, VSMovImp4, VSLstPalBox);
    FmVSImpPackList.Destroy;
  end;
end;

procedure TUnVS_PF.ImprimePesquisaMartelos(EdEmpresa_ValueVariant,
  Ed05VSCacCod_ValueVariant, Ed05VSPallet_ValueVariant,
  Ed05VMI_Sorc_ValueVariant, Ed05VMI_Dest_ValueVariant,
  Ed05Martelo_ValueVariant, Ed05Ficha_ValueVariant, Ed05GraGruX_ValueVariant,
  Ed05Terceiro_ValueVariant: Integer; CBEmpresa_Text, Ed05Marca_Text,
  CB05GraGruX_Text, CB05Terceiro_Text, CB05SerieFch_Text: String;
  TP05DataIni_Date, TP05DataFim_Date: TDateTime; Ck05DataIni_Checked,
  Ck05DataFim_Checked, Ck05Revisores_Checked: Boolean;
  RG05OperClass_ItemIndex, RG05Ordem_ItemIndex, RG05AscDesc_ItemIndex: Integer;
  Ck05MostraPontos_Checked, Ck05Morto_Checked: Boolean);
begin
  if DBCheck.CriaFm(TFmVSImpMartelo, FmVSImpMartelo, afmoLiberado) then
  begin
    //FmVSImpMartelo.ShowModal;
    FmVSImpMartelo.Ed05VSCacCod_ValueVariant := Ed05VSCacCod_ValueVariant;
    FmVSImpMartelo.Ed05VSPallet_ValueVariant := Ed05VSPallet_ValueVariant;
    FmVSImpMartelo.Ed05VMI_Sorc_ValueVariant := Ed05VMI_Sorc_ValueVariant;
    FmVSImpMartelo.Ed05VMI_Dest_ValueVariant := Ed05VMI_Dest_ValueVariant;
    FmVSImpMartelo.Ed05Martelo_ValueVariant  := Ed05Martelo_ValueVariant;
    FmVSImpMartelo.Ed05Ficha_ValueVariant    := Ed05Ficha_ValueVariant;
    FmVSImpMartelo.Ed05GraGruX_ValueVariant  := Ed05GraGruX_ValueVariant;
    FmVSImpMartelo.Ed05Terceiro_ValueVariant := Ed05Terceiro_ValueVariant;
    FmVSImpMartelo.CBEmpresa_Text            := CBEmpresa_Text;
    FmVSImpMartelo.Ed05Marca_Text            := Ed05Marca_Text;
    FmVSImpMartelo.CB05GraGruX_Text          := CB05GraGruX_Text;
    FmVSImpMartelo.CB05Terceiro_Text         := CB05Terceiro_Text;
    FmVSImpMartelo.CB05SerieFch_Text         := CB05SerieFch_Text;
    FmVSImpMartelo.TP05DataIni_Date          := TP05DataIni_Date;
    FmVSImpMartelo.TP05DataFim_Date          := TP05DataFim_Date;
    FmVSImpMartelo.Ck05DataIni_Checked       := Ck05DataIni_Checked;
    FmVSImpMartelo.Ck05DataFim_Checked       := Ck05DataFim_Checked;
    //FmVSImpMartelo.Ck05Revisores_Checked     := Ck05Revisores_Checked;
    FmVSImpMartelo.RG05OperClass_ItemIndex   := RG05OperClass_ItemIndex;
    FmVSImpMartelo.RG05Ordem_ItemIndex       := RG05Ordem_ItemIndex;
    FmVSImpMartelo.RG05AscDesc_ItemIndex     := RG05AscDesc_ItemIndex;
    //FmVSImpMartelo.Ck05MostraPontos_Checked  := Ck05MostraPontos_Checked;
    FmVSImpMartelo.Ck05Morto_Checked         := Ck05Morto_Checked;
    //
    FmVSImpMartelo.ImprimePesquisaMartelos();
    FmVSImpMartelo.Destroy;
  end;
end;

procedure TUnVS_PF.ImprimePesquisaMartelos2(Entidade: Integer;
CBEmpresa_Text: String);
begin
  if DBCheck.CriaFm(TFmVSImpMartelo, FmVSImpMartelo, afmoLiberado) then
  begin
    FmVSImpMartelo.FEntidade                 := Entidade;
    FmVSImpMartelo.CBEmpresa_Text            := CBEmpresa_Text;
    //
    FmVSImpMartelo.ShowModal;
(*
    //FmVSImpMartelo.ShowModal;
    FmVSImpMartelo.Ed05VSCacCod_ValueVariant := Ed05VSCacCod_ValueVariant;
    FmVSImpMartelo.Ed05VSPallet_ValueVariant := Ed05VSPallet_ValueVariant;
    FmVSImpMartelo.Ed05VMI_Sorc_ValueVariant := Ed05VMI_Sorc_ValueVariant;
    FmVSImpMartelo.Ed05VMI_Dest_ValueVariant := Ed05VMI_Dest_ValueVariant;
    FmVSImpMartelo.Ed05Martelo_ValueVariant  := Ed05Martelo_ValueVariant;
    FmVSImpMartelo.Ed05Ficha_ValueVariant    := Ed05Ficha_ValueVariant;
    FmVSImpMartelo.Ed05GraGruX_ValueVariant  := Ed05GraGruX_ValueVariant;
    FmVSImpMartelo.Ed05Terceiro_ValueVariant := Ed05Terceiro_ValueVariant;
    FmVSImpMartelo.CBEmpresa_Text            := CBEmpresa_Text;
    FmVSImpMartelo.Ed05Marca_Text            := Ed05Marca_Text;
    FmVSImpMartelo.CB05GraGruX_Text          := CB05GraGruX_Text;
    FmVSImpMartelo.CB05Terceiro_Text         := CB05Terceiro_Text;
    FmVSImpMartelo.CB05SerieFch_Text         := CB05SerieFch_Text;
    FmVSImpMartelo.TP05DataIni_Date          := TP05DataIni_Date;
    FmVSImpMartelo.TP05DataFim_Date          := TP05DataFim_Date;
    FmVSImpMartelo.Ck05DataIni_Checked       := Ck05DataIni_Checked;
    FmVSImpMartelo.Ck05DataFim_Checked       := Ck05DataFim_Checked;
    FmVSImpMartelo.Ck05Revisores_Checked     := Ck05Revisores_Checked;
    FmVSImpMartelo.RG05OperClass_ItemIndex   := RG05OperClass_ItemIndex;
    FmVSImpMartelo.RG05Ordem_ItemIndex       := RG05Ordem_ItemIndex;
    FmVSImpMartelo.RG05AscDesc_ItemIndex     := RG05AscDesc_ItemIndex;
    FmVSImpMartelo.Ck05MostraPontos_Checked  := Ck05MostraPontos_Checked;
    FmVSImpMartelo.Ck05Morto_Checked         := Ck05Morto_Checked;
    //
    FmVSImpMartelo.ImprimePesquisaMartelos();
*)
    FmVSImpMartelo.Destroy;
  end;
end;

procedure TUnVS_PF.ImprimePrevia(Empresa: Integer; Pallets: array of Integer);
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmVSImpPrevia, FmVSImpPrevia, afmoLiberado) then
  begin
    //FmVSImpPrevia.ShowModal;
    SetLength(FmVSImpPrevia.FPallets, Length(Pallets));
    for I := Low(Pallets) to High(Pallets) do
      FmVSImpPrevia.FPallets[I] := Pallets[I];
    FmVSImpPrevia.FEmpresa := Empresa;
    FmVSImpPrevia.ImprimePallet();
    FmVSImpPrevia.Destroy;
  end;
end;

procedure TUnVS_PF.ImprimeReclassDesnate(Codigo: Integer; ImpDesnate: TImpDesnate);
const
  GraGruX = 0;
begin
  if DBCheck.CriaFm(TFmVSImpRecla, FmVSImpRecla, afmoLiberado) then
  begin
    FmVSImpRecla.FOC := Codigo;
    //FmVSImpRecla.ShowModal;
    case ImpDesnate of
      impdsnDenate: FmVSImpRecla.ImprimeReclassDesnate(Codigo);
      impdsnDenSubUni:
      begin
        //FmVSImpRecla.ImprimeReclassDesnSubUni(Codigo);
        ReopenDesnate(Codigo, GraGruX, FmVSImpRecla.QrVSCacIts, FmVSImpRecla.QrVSCacSum);
        FmVSImpRecla.ShowModal;
      end;
      impdsnDenSubAll: FmVSImpRecla.ImprimeReclassDesnSubAll(Codigo);
      else Geral.MB_Erro('Impress�o de desnate n�o implementada!');
    end;

    FmVSImpRecla.Destroy;
  end;
end;

procedure TUnVS_PF.ImprimeReclassEqualize(Codigo: Integer);
begin
(*
SELECT cia.SubClass, SUM(cia.Pecas) Pecas,
SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2,
vez.BasNota, SUM(cia.AreaM2) * vez.BasNota NotaM2
FROM vscacitsa cia
LEFT JOIN vseqzits vdi ON vdi.Pallet=cia.VSPallet
LEFT JOIN vseqzcab vdc ON vdc.Codigo=vdi.Codigo
LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass
WHERE vdc.Codigo=1
GROUP BY cia.SubClass
*)
end;

procedure TUnVS_PF.ImprimeRendimentoIMEIS(IMEIs: array of Integer; UsaNfeSers,
  UsaNFeNums: array of Boolean; NFeSers, NFeNums: array of Integer);
begin
  Application.CreateForm(TFmVSImpRendPWE, FmVSImpRendPWE);
  FmVSImpRendPWE.ImprimeRendIMEIs(IMEIs, UsaNfeSers, UsaNFeNums, NFeSers, NFeNums);
  FmVSImpRendPWE.Destroy;
end;

procedure TUnVS_PF.ImprimeRendimentoSemi(Empresa_Cod, Fornece_Cod, OPIni, OPFim,
  Bastidoes, Agrupa, Ordem1, Ordem2, Ordem3, Ordem4: Integer; Empresa_Txt,
  Fornece_Txt: String; DataIni, DataFim: TDateTime; UsaDtIni, UsaDtFim,
  UsaOPIni, UsaOPFim: Boolean; DBG21CouNiv2: TdmkDBGridZTO; Qr21CouNiv2:
  TmySQLQuery);
var
  Qry: TmySQLQuery;
  Txt: String;
begin
  Geral.MB_Info('Reativar?');
(*
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.Codigo, cou.Bastidao, gg1.Nome, vmi.*  ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN gragrux ggx    ON ggx.Controle=vmi.Gragrux ',
    'LEFT JOIN gragru1 gg1    ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragruxcou cou ON cou.GraGruX=vmi.GraGruX  ',
    'LEFT JOIN vsopecab cab   ON cab.MovimCod=vmi.MovimCod  ',
    'LEFT JOIN operacoes ope  ON ope.Codigo=cab.Operacoes ',
    'WHERE vmi.MovimID=' + Geral.FF0(Integer(emidEmOperacao)),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestOper)),
    'AND cou.Bastidao=' + Geral.FF0(Integer(vsbstdDivTripa)),
    'AND ope.RedefnFrmt=' + Geral.FF0(Integer(vsrdfnfrmtDivRacha)),
    '']);
    if Qry.RecordCount > 0 then
    begin
      Txt := '';
      while not Qry.Eof do
      begin
        Txt := Txt + Geral.FF0(Qry.FieldByName('Codigo').AsInteger) + ', ' ;
        //
        Qry.Next;
      end;
      Geral.MB_Aviso(
      'As oper��es de divis�o abaixo podem estar com couro destino incorreto!' +
      'Couro de destino n�o podem ser divididos em tripa!' + sLinebreak + Txt);
    end;
  finally
    Qry.Free;
  end;
  //
  Application.CreateForm(TFmVSImpRendPWE, FmVSImpRendPWE);
  FmVSImpRendPWE.FEmpresa_Cod   := Empresa_Cod;
  FmVSImpRendPWE.FEmpresa_Txt   := Empresa_Txt;
  FmVSImpRendPWE.FFornece_Cod   := Fornece_Cod;
  FmVSImpRendPWE.FFornece_Txt   := Fornece_Txt;
  FmVSImpRendPWE.FDataIni       := DataIni;
  FmVSImpRendPWE.FDataFim       := DataFim;
  FmVSImpRendPWE.FUsaDtIni      := UsaDtIni;
  FmVSImpRendPWE.FUsaDtFim      := UsaDtFim;
  FmVSImpRendPWE.FOPIni         := OPIni;
  FmVSImpRendPWE.FOPIni         := OPFim;
  FmVSImpRendPWE.FUsaOPIni      := UsaOPIni;
  FmVSImpRendPWE.FUsaOPIni      := UsaOPFim;
  FmVSImpRendPWE.FAgrupa        := Agrupa;
  FmVSImpRendPWE.FOrdem1        := Ordem1;
  FmVSImpRendPWE.FOrdem2        := Ordem2;
  FmVSImpRendPWE.FOrdem3        := Ordem3;
  FmVSImpRendPWE.FOrdem4        := Ordem4;
  FmVSImpRendPWE.FBastidoes     := Bastidoes;
  FmVSImpRendPWE.FDBG21CouNiv2  := DBG21CouNiv2;
  FmVSImpRendPWE.FQr21CouNiv2   := Qr21CouNiv2;
  //
  FmVSImpRendPWE.ReopenIndef(True);
  if FmVSImpRendPWE.QrIndef.RecordCount > 0 then
  begin
    FmVSImpRendPWE.ShowModal;
  end else
  begin
    FmVSImpRendPWE.ImprimeRendimentoSemi();
  end;
  //
  FmVSImpRendPWE.Destroy;
*)
end;

{
procedure TUnVS_PF.ImprimeVSEmProcBH();
begin
  if DBCheck.CriaFm(TFmVSImpEmProcBH, FmVSImpEmProcBH, afmoLiberado) then
  begin
    //FmVSImpEmProcBHs.ShowModal;
    FmVSImpEmProcBH.ImprimeVSEmProcBH();
    FmVSImpEmProcBH.Destroy;
  end;
end;
}
{
function TUnVS_PF.InsAltVSPalRclOld(const Empresa, Fornecedor: Integer;
  const MovimID: TEstqMovimID; const Codigo, MovimCod, BxaGraGruX, BxaMovimNiv,
  BxaSrcNivel1, BxaSrcNivel2, VSMovIts, Tecla, VSPallet, GragruX: Integer;
  const LaAviso1, LaAviso2: TLabel; var CtrlSorc, CtrlDest: Integer): Integer;
  //
  function CriaVSMovIts(): Integer;
  const
    QtdGerPeca = 0;
    QtdGerPeso = 0;
    LnkNivXtr1 = 0;
    LnkNivXtr2 = 0;
    CliVenda   = 0;
    AreaM2     = 0;
    AreaP2     = 0;
    CustoMOKg  = 0;
    CustoMOTot = 0;
    ValorT     = 0;
    //Misturou   = 0;
    AptoUso    = 0;
    FornecMO   = 0;
    Pecas      = 0;
    PesoKg     = 0;
    Observ     = ''; // Como fazer!!!
    SerieFch   = 0;  // Fazer depois!!!
    Ficha      = 0;  // Fazer depois!!!
    Marca      = ''; // Fazer depois!!!
    ValorMP    = 0;  // Fazer depois!!!
    QtdGerArM2 = 0;  // Fazer depois!!!
    QtdGerArP2 = 0;  // Fazer depois!!!
    NotaMPAG   = 0;
    //
    ExigeFornecedor = True;
    //
    TpCalcAuto = -1;
    //
    EdPallet = nil;
    EdValorT = nil;
    EdAreaM2 = nil;
  var
    Controle, SrcNivel1, SrcNivel2, SrcGGX, DstNivel1, DstNivel2, MovimTwn,
    DstGGX, Pallet: Integer;
    MovimNiv: TEstqMovimNiv;
    DataHora: String;
    SrcMovID, DstMovID: TEstqMovimID;
  begin
    Result := 0;
    DataHora := Geral.FDT(DModG.ObtemAgora(), 109);
    //
    // Classe a reclassificar
    MovimTwn := UMyMod.BPGS1I32('v s m o v i t s', 'MovimTwn', '', '', tsPos, stIns, MovimTwn);
    Controle := 0;
(*   Desabilitado em 2014-10-12  - Pode ser mais de um Registro!
    Controle := UMyMod.BPGS1I32('v s m o v i t s', 'Controle', '', '', tsPos, stIns, Controle);
    //SrcMovimNiv := TEstqMovimNiv(BxaMovimNiv);
    SrcNivel1    := 0; //BxaSrcNivel1;
    SrcNivel2    := 0; //BxaSrcNivel2;
    SrcGGX       := 0; //BxaGraGruX;
    //
    DstMovID     := MovimID;
    DstNivel1    := Codigo;
*)
    DstNivel2    := UMyMod.BPGS1I32('v s m o v i t s', 'Controle', '', '', tsPos, stIns, 0);
(*
    DstGGX       := GraGruX;
    //
    MovimNiv := eminSorcReclass;
    //
    /
    if InsUpdVSMovIts_(stIns, Codigo, MovimCod, MovimTwn, Empresa,
    Fornecedor, MovimID, MovimNiv, Pallet, BxaGraGruX, Pecas, PesoKg, AreaM2,
    AreaP2, ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ,
    LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha,
    //Misturou,
    CustoMOKg,
    CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso,
    QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
    NotaMPAG, SrcGGX, DstGGX, Marca) then
*)
    begin
      // Nova classe
      SrcMovID  := MovimID;
      SrcNivel1 := Codigo;
      SrcNivel2 := Controle;
      SrcGGX    := GraGruX;
      //
      Controle  := DstNivel2;
      //
      DstMovID     := TEstqMovimID(0);
      DstNivel1    := 0;
      DstNivel2    := 0;
      DstGGX       := 0;
      Pallet       := VSPallet;
      //
      MovimNiv     := eminDestReclass;
      //
      if InsUpdVSMovIts_(stIns, Codigo, MovimCod, MovimTwn, Empresa,
      Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
      AreaP2, ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ,
      LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg,
      CustoMOTot, ValorMP, SrcMovID, SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso,
      QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
      NotaMPAG, SrcGGX, SrcGGX, Marca, TpCalcAuto) then
      begin
        CtrlSorc := SrcNivel2;
        CtrlDest := Controle;
      end;
    end;
  end;
  //
var
  Controle, VMI_Sorc, VMI_Baix, VMI_Dest, MovimIDGer: Integer;
  DtHrIni: String;
  Qry: TmySQLQuery;
begin
  Result := VSPallet;
  if VSPallet > 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle, VSPallet ',
      'FROM vsparclitsa ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'AND Tecla=' + Geral.FF0(Tecla),
      'AND DtHrFim < "1900-01-01" ',
      'ORDER BY DtHrFim DESC, Controle DESC ',
      '']);
      if Qry.FieldByName('VSPallet').AsInteger <> VSPallet then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando box ' +
          Geral.FF0(Tecla));
        DtHrIni := Geral.FDT(DModG.ObtemAgora(), 109);
        //
        CriaVSMovIts();
        VMI_Sorc := VSMovIts;
        VMI_Dest := CtrlDest;
        VMI_Baix := CtrlSorc;
        //
        Controle := UMyMod.BPGS1I32('vsparclitsa', 'Controle', '', '', tsPos, stIns, 0);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsparclitsa', False, [
        'Codigo', 'VSPallet', 'Tecla',
        'DtHrIni',
        'VMI_Sorc', 'VMI_Baix', 'VMI_Dest'], [
        'Controle'], [
        Codigo, VSPallet, Tecla,
        DtHrIni, VMI_Sorc, VMI_Baix, VMI_Dest], [
        Controle], True) then
        begin
          Result := VSPallet;
          //
          MovimIDGer := Integer(emidReclasVS);
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspalleta', False, [
          'MovimIDGer'], [
          'Codigo'], [
          MovimIDGer], [
          Codigo], True);
        end;
      end;
    finally
      Qry.Free;
    end;
  end;
end;
}

{
procedure TUnVS_PF.InsereVMI_SemOrig(const VSPallet: Integer; var VMI: Integer);
const
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  CustoMOKg  = 0;
  CustoMOTot = 0;
  ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  FornecMO   = 0;
  NotaMPAG   = 0;
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  Terceiro   = 0;
  SrcMovID   = 0;
  Pecas      = 0;
  PesoKg     = 0;
  AreaM2     = 0;
  AreaP2     = 0;
  ValorT     = 0;
  Observ     = '';
  SerieFch   = 0;
  Ficha      = 0;
  Marca      = '';
  //
  TpCalcAuto = -1;
  //
  EdFicha  = nil;
  EdPallet = nil;
  ExigeFornecedor = False;
var
  DataHora: String;
  Pallet, Codigo, Controle, MovimCod: Integer;
  //Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  //
  Qry: TmySQLQuery;
  Empresa, GraGruX, GraGruY: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT pal.Empresa, pal.GraGruX, ggx.GraGruY ',
    'FROM vspalleta pal ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=pal.GraGruX ',
    'WHERE pal.Codigo=' + Geral.FF0(VSPAllet),
    '']);
    Empresa := Qry.FieldByName('Empresa').AsInteger;
    GraGruX := Qry.FieldByName('GraGruX').AsInteger;
    GraGruY := Qry.FieldByName('GraGruY').AsInteger;
  finally
    Qry.Free;
  end;
  //
  Codigo         := VSPallet;
  DataHora       := Geral.FDT(DmodG.ObtemAgora(), 109);
  MovimID        := emidSemOrigem;
  MovimNiv       := eminSemNiv;
  Pallet         := VSPallet;
  //
  //Misturou       := QrVSMovItsMisturou.Value;
  //
(*
  if VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas,
  AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas,
  EdAreaM2, EdPesoKg, EdValorT, ExigeFornecedor, GraGruY) then
    Exit;
*)
  //
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, stIns, 0);
  Controle := UMyMod.BPGS1I32('v s m o v i t s', 'Controle', '', '', tsPos, stIns, 0);
  //
  if InsUpdVSMovIts_(stIns, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
  CustoMOKg, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca,
  QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto) then
  begin
    VMI := Controle;
  end;
end;
}


procedure TUnVS_PF.InsereVSFchRslIts(SQLType: TSQLType;
  SerieFch, Ficha, GraGruX: Integer;
  CustoKg, CustoMOKg, CusFretKg, Preco, ImpostP, ComissP, FreteM2, Pecas,
  AreaM2, PesoKg, ImpostCred: Double);
var
  ImpostTotV, ComissTotV, BrutoV, MargemV, MargemP, KgMedio, CustoMOTot,
  CusFretTot, CustoINTot, CustoTotal, FretM2Tot: Double;
  SrcMPAG: Double;
begin
  CustoMOTot     := Geral.RoundC(CustoMOKg * PesoKg, 2);
  CusFretTot     := Geral.RoundC(CusFretKg * PesoKg, 2);
  CustoINTot     := Geral.RoundC(CustoKg * PesoKg, 2);
  //CustoTotal     := CustoINTot + CustoMOTot + CusFretTot;
  //
  BrutoV         := Geral.RoundC(Preco * AreaM2, 2);
  ImpostTotV     := Geral.RoundC(BrutoV * ImpostP / 100, 2);
  ComissTotV     := Geral.RoundC(BrutoV * ComissP / 100, 2);
  FretM2Tot      := Geral.RoundC(AreaM2 * FreteM2, 2);
  //
  //
  CustoTotal     := CustoINTot + CustoMOTot + CusFretTot + ImpostTotV + ComissTotV + FretM2Tot;
  MargemV        := BrutoV + ImpostCred - CustoTotal;
  //
  if BrutoV = 0 then
    MargemP      := 0
  else
    MargemP      := Geral.RoundC((MargemV / CustoTotal) * 100, 4);
  //
  // Fazer?
  SrcMPAG := 0;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsfchrslits', False, [
  'Pecas', 'AreaM2', 'PesoKg',
  'CustoKg', 'CustoMOKg', 'CusFretKg',
  'Preco', 'ImpostP', 'ComissP',
  'FreteM2', 'SrcMPAG', 'CustoMOTot',
  'CusFretTot', 'ImpostTotV', 'ComissTotV',
  'FretM2Tot', 'BrutoV', 'MargemV',
  'MargemP', 'CustoINTot', 'CustoTotal',
  'ImpostCred'], [
  'SerieFch', 'Ficha', 'GraGruX'], [
  Pecas, AreaM2, PesoKg,
  CustoKg, CustoMOKg, CusFretKg,
  Preco, ImpostP, ComissP,
  FreteM2, SrcMPAG, CustoMOTot,
  CusFretTot, ImpostTotV, ComissTotV,
  FretM2Tot, BrutoV, MargemV,
  MargemP, CustoINTot, CustoTotal,
  ImpostCred], [
  SerieFch, Ficha, GraGruX], True);
end;

{
procedure TUnVS_PF.InsereVSMovImpEmClassificacao(Empresa, GraGruX, CouNiv2,
  CouNiv1: Integer; TabVSMovImpX, SQL_GraGruY: String);
var
  SQL_Empresa, SQL_GraGruX, SQL_CouNiv2, SQL_CouNiv1, SQL_IMEI, ATT_MovimID,
  ATT_MovimNiv: String;
begin


////////////////////////////////////////////////////////////////////////////////
///  Nao precisa mais!!!!
////////////////////////////////////////////////////////////////////////////////

  EXIT;


  //if CkStatusReal.Checked then
  begin
    if Empresa <> 0 then
      SQL_Empresa := 'AND vsp.Empresa=' + Geral.FF0(Empresa)
    else
      SQL_Empresa := '';
    //
    if GraGruX <> 0 then
      //SQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX)
      SQL_GraGruX := 'AND vsp.GraGruX=' + Geral.FF0(GraGruX)
    else
      SQL_GraGruX := '';
    //
    if CouNiv2 <> 0 then
      SQL_CouNiv2 := 'AND xco.CouNiv2=' + Geral.FF0(CouNiv2)
    else
      SQL_CouNiv2 := '';
    //
    if CouNiv1 <> 0 then
      SQL_CouNiv1 := 'AND xco.CouNiv1=' + Geral.FF0(CouNiv1)
    else
      SQL_CouNiv1 := '';
    //
    SQL_IMEI := Geral.ATS([
    '0 Codigo, 0 IMEC, 0 IMEI, ',
    '0 MovimID, 0 MovimNiv, "" NO_MovimID, "" NO_MovimNiv, ']);
    //
////////////////////////////////////////////////////////////////////////////////
///  Descontar couros classificados j� reclassificados em reclassifica��o
///
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO  ' + TabVSMovImpX,
    'SELECT vsp.Empresa, vsp.GraGruX, 0 Pecas, 0 PesoKg,',
    '0 AreaM2, 0 AreaP2, 0 ValorT, 0 SdoVrtPeca, ',
    '0 SdoVrtPeso, 0 SdoVrtArM2, -SUM(cia.Pecas) LmbVrtPeca, ',
    '0 LmbVrtPeso, -SUM(cia.AreaM2) LmbVrtArM2,',
    'ggx.GraGru1, CONCAT(gg1.Nome,',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
    'NO_PRD_TAM_COR, rca.VSPallet Pallet, vsp.Nome NO_Pallet,',
    '0 Terceiro,',
    'IF(vsp.CliStat IS NULL, 0, vsp.CliStat),',
    'IF(vsp.Status IS NULL, 0, vsp.Status),',
    '"" NO_FORNECE,',
    'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,',
    'vps.Nome NO_STATUS,',
    'vsp.DtHrEndAdd DataHora, 0 OrdGGX,',
    'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
    //
    '2 PalStat, ',
    'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
    'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
    'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
    '0 SdoInteiros, ',
    '-SUM(cia.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) LmbInteiros, ',
    SQL_IMEI,
    '0 SerieFch, "" NO_SerieFch, 0 Ficha, ',
    '1 Ativo  ',
    'FROM ' + TMeuDB + '.vscacitsa cia',
    'LEFT JOIN ' + TMeuDB + '.vsparclcaba rca ON rca.Codigo=cia.Codigo',
    'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=rca.VSPallet',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsp.GraGruX',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle',
    'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    '', // Sem terceiro???
    'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vsp.CliStat',
    'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vsp.Empresa',
    'LEFT JOIN ' + TMeuDB + '.vspalsta  vps ON vps.Codigo=vsp.Status',
    'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
    'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
    //'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
////////////  2015-03-05 ///////////////////////////////////////////////////////
    //'WHERE vsp.DtHrEndAdd < "1900-01-01" ',
    //'AND cia.VSPaRclIts IN (',
    'WHERE cia.VSPaRclIts IN (',
    '     SELECT Controle ',
    '     FROM ' + TMeuDB + '.vsparclitsa pri ',
    '     WHERE DtHrFim < "1900-01-01"',
    ')',
(*
    'WHERE cia.VMI_Dest =0 ',
    'AND  cia.VSPaRclIts IN (',
    '     SELECT Controle ',
    '     FROM ' + TMeuDB + '.vsparclitsa pri ',
    '     WHERE DtHrFim < "1900-01-01"',
    ')',
*)
//////////// FIM 2015-03-05 ////////////////////////////////////////////////////
    SQL_Empresa,
    SQL_GraGruX,
    SQL_GraGruY,
    SQL_CouNiv2,
    SQL_CouNiv1,
    'GROUP BY rca.VSPallet, ggx.Controle',
    '']);
    //Geral.MB_Aviso(DModG.QrUpdPID1.SQL.Text);
    //
////////////////////////////////////////////////////////////////////////////////
///  Somar Couros classificados em reclassifica��o
///
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO  ' + TabVSMovImpX,
    'SELECT vsp.Empresa, vsp.GraGruX, 0 Pecas, 0 PesoKg,',
    '0 AreaM2, 0 AreaP2, 0 ValorT, 0 SdoVrtPeca, ',
    '0 SdoVrtPeso, 0 SdoVrtArM2, SUM(cia.Pecas) LmbVrtPeca, ',
    '0 LmbVrtPeso, SUM(cia.AreaM2) LmbVrtArM2,',
    'ggx.GraGru1, CONCAT(gg1.Nome,',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
    'NO_PRD_TAM_COR, cia.VSPallet Pallet, vsp.Nome NO_Pallet,',
    '0 Terceiro,',
    'IF(vsp.CliStat IS NULL, 0, vsp.CliStat),',
    'IF(vsp.Status IS NULL, 0, vsp.Status),',
    '"" NO_FORNECE,',
    'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,',
    'vps.Nome NO_STATUS,',
    'vsp.DtHrEndAdd DataHora, 0 OrdGGX,',
    'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
    //
    '1 PalStat, ',
    'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
    'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
    'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
    '0 SdoInteiros, ',
    'SUM(cia.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) LmbInteiros, ',
    SQL_IMEI,
    '0 SerieFch, "" NO_SerieFch, 0 Ficha, ',
    '1 Ativo  ',
    'FROM ' + TMeuDB + '.vscacitsa cia',
    'LEFT JOIN ' + TMeuDB + '.vsparclcaba rca ON rca.Codigo=cia.Codigo',
    'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=cia.VSPallet',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsp.GraGruX',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle',
    'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    '', // Sem terceiro???
    'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vsp.CliStat',
    'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vsp.Empresa',
    'LEFT JOIN ' + TMeuDB + '.vspalsta   vps ON vps.Codigo=vsp.Status',
    'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
    'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
    //'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
////////////  2015-03-05 ///////////////////////////////////////////////////////
    'WHERE vsp.DtHrEndAdd < "1900-01-01" ',
    'AND cia.VSPaRclIts IN (',
    '     SELECT Controle ',
    '     FROM ' + TMeuDB + '.vsparclitsa pri ',
    '     WHERE DtHrFim < "1900-01-01"',
    ')',
(*
    'WHERE cia.VMI_Dest =0 ',
    'AND  cia.VSPaRclIts IN (',
    '     SELECT Controle ',
    '     FROM ' + TMeuDB + '.vsparclitsa pri ',
    '     WHERE DtHrFim < "1900-01-01"',
    ')',
*)
//////////// FIM 2015-03-05 ////////////////////////////////////////////////////
    SQL_Empresa,
    SQL_GraGruX,
    SQL_GraGruY,
    SQL_CouNiv2,
    SQL_CouNiv1,
    // 2015-03-06 Erro no estoque por pallet!!!
    //'GROUP BY rca.VSPallet, ggx.Controle',
    'GROUP BY cia.VSPallet, ggx.Controle',
    // FIM 2015-03-06 Erro no estoque por pallet!!!
    '']);
    //Geral.MB_Aviso(DModG.QrUpdPID1.SQL.Text);
    //
////////////////////////////////////////////////////////////////////////////////
///  Somar Pallets de Couros classificados em classifica��o
///
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO  ' + TabVSMovImpX,
    'SELECT vsp.Empresa, vsp.GraGruX, 0 Pecas, 0 PesoKg, ',
    '0 AreaM2, 0 AreaP2, 0 ValorT, 0 SdoVrtPeca, ',
    '0 SdoVrtPeso, 0 SdoVrtArM2, SUM(cia.Pecas) LmbVrtPeca, ',
    '0 LmbVrtPeso, SUM(cia.AreaM2) LmbVrtArM2, ',
    'ggx.GraGru1, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, cia.VSPallet Pallet, vsp.Nome NO_Pallet, ',
    '0 Terceiro, ',
    'IF(vsp.CliStat IS NULL, 0, vsp.CliStat), ',
    'IF(vsp.Status IS NULL, 0, vsp.Status), ',
    '"" NO_FORNECE, ',
    'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "", ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat, ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
    'vps.Nome NO_STATUS, ',
    'vsp.DtHrEndAdd DataHora, 0 OrdGGX, ',
    'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
    '1 PalStat, ',
    'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
    'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
    'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
    '0 SdoInteiros, ',
    'SUM(cia.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) LmbInteiros, ',
    SQL_IMEI,
    '0 SerieFch, "" NO_SerieFch, 0 Ficha, ',
    '1 Ativo ',
    'FROM ' + TMeuDB + '.vscacitsa cia ',
    'LEFT JOIN ' + TMeuDB + '.vspaclacaba rca ON rca.Codigo=cia.Codigo ',
    'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=cia.VSPallet ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsp.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vsp.CliStat ',
    'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vsp.Empresa ',
    'LEFT JOIN ' + TMeuDB + '.vspalsta   vps ON vps.Codigo=vsp.Status ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
    //'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
    'WHERE vsp.DtHrEndAdd < "1900-01-01" ',
    'AND ( ',
    '(cia.VSPaClaIts <> 0) AND ',
    '(cia.VSPaClaIts IN ( ',
    '     SELECT DISTINCT Controle ',
    '     FROM ' + TMeuDB + '.vspaclaitsa ',
    '     WHERE DtHrFim < "1900-01-01") ',
    ')) ',
    SQL_Empresa,
    SQL_GraGruX,
    SQL_GraGruY,
    SQL_CouNiv2,
    SQL_CouNiv1,
    'GROUP BY cia.VSPallet, ggx.Controle',
    '']);
    //
    //Geral.MB_Aviso(DModG.QrUpdPID1.SQL.Text);

////////////////////////////////////////////////////////////////////////////////
///  Descontar Artigos gerados j� classificados em classifica��o
///
    ATT_MovimID := dmkPF.ArrayToTexto('vsp.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
    //
    ATT_MovimNiv := dmkPF.ArrayToTexto('vsp.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO  ' + TabVSMovImpX,
    'SELECT vsp.Empresa, vsp.GraGruX, 0 Pecas, 0 PesoKg, ',
    '0 AreaM2, 0 AreaP2, 0 ValorT, 0 SdoVrtPeca, ',
    '0 SdoVrtPeso, 0 SdoVrtArM2, -SUM(cia.Pecas) LmbVrtPeca, ',
    '0 LmbVrtPeso, -SUM(cia.AreaM2) LmbVrtArM2, ',
    'ggx.GraGru1, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, 0 Pallet, "" NO_Pallet, ',
    'vsp.Terceiro, ',
    'IF(vsp.CliVenda IS NULL, 0, vsp.CliVenda), ',
    '0 Status, ',
    'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
    'IF((vsp.CliVenda=0) OR (vsp.CliVenda IS NULL), "", ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat, ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
    '"" NO_STATUS, ',
    '"1899-12-30" DataHora, 0 OrdGGX, ',
    'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
    '8 PalStat, ',
    'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
    'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
    'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
    '0 SdoInteiros, ',
    '-SUM(cia.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) LmbInteiros, ',
    'vsp.Codigo, vsp.MovimCod IMEC, vsp.Controle IMEI,  ',
    'vsp.MovimID, vsp.MovimNiv, ',
    ATT_MovimID,
    ATT_MovimNiv,
    'vsp.SerieFch, vsf.Nome NO_SerieFch, vsp.Ficha, ',
    '1 Ativo ',
    'FROM ' + TMeuDB + '.vscacitsa cia ',
    'LEFT JOIN ' + TMeuDB + '.vspaclacaba rca ON rca.Codigo=cia.Codigo ',
    'LEFT JOIN ' + TMeuDB + '.v s m o v i t s   vsp ON vsp.Controle=rca.VSMovIts ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsp.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vsp.CliVenda ',
    'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vsp.Empresa ',
    'LEFT JOIN ' + TMeuDB + '.entidades  frn ON frn.Codigo=vsp.Terceiro ',
    '/*LEFT JOIN ' + TMeuDB + '.vspalsta   vps ON vps.Codigo=vsp.Status*/ ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vsp.SerieFch  ',
    //'WHERE vsp.DtHrEndAdd < "1900-01-01" ',
    //'AND cia.VMI_Sorc IN ',
    'WHERE cia.VMI_Sorc IN ',
    '( ',
    'SELECT VSMovIts ',
    'FROM ' + TMeuDB + '.vspaclacaba ',
    'WHERE DtHrFimCla < "1900-01-01" ',
    ') ',
    SQL_Empresa,
    SQL_GraGruX,
    SQL_GraGruY,
    SQL_CouNiv2,
    SQL_CouNiv1,
    'GROUP BY cia.VMI_Sorc, ggx.Controle ',
    '']);
    //Geral.MB_Aviso(DModG.QrUpdPID1.SQL.Text);
////////////////////////////////////////////////////////////////////////////////
///  Somar itens Artigos gerados n�o classificados em classifica��o
///
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO  ' + TabVSMovImpX,
    'SELECT vsp.Empresa, vsp.GraGruX, 0 Pecas, 0 PesoKg, ',
    '0 AreaM2, 0 AreaP2, 0 ValorT, 0 SdoVrtPeca, ',
    '0 SdoVrtPeso, 0 SdoVrtArM2, SUM(cia.Pecas) LmbVrtPeca, ',
    '0 LmbVrtPeso, SUM(cia.AreaM2) LmbVrtArM2, ',
    'ggx.GraGru1, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, 0 Pallet, "" NO_Pallet, ',
    'vsp.Terceiro, ',
    'IF(vsp.CliVenda IS NULL, 0, vsp.CliVenda), ',
    '0 Status, ',
    'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
    'IF((vsp.CliVenda=0) OR (vsp.CliVenda IS NULL), "", ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat, ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
    '"" NO_STATUS, ',
    '"1899-12-30" DataHora, 0 OrdGGX, ',
    'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
    '8 PalStat, ',
    'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
    'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
    'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
    '0 SdoInteiros, ',
    'SUM(cia.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) LmbInteiros, ',
    'vsp.Codigo, vsp.MovimCod IMEC, vsp.Controle IMEI,  ',
    'vsp.MovimID, vsp.MovimNiv, ',
    ATT_MovimID,
    ATT_MovimNiv,
    'vsp.SerieFch, vsf.Nome NO_SerieFch, vsp.Ficha, ',
    '1 Ativo ',
    'FROM ' + TMeuDB + '.vscacitsa cia ',
    'LEFT JOIN ' + TMeuDB + '.vspaclaitsa rci ON rci.Controle=cia.VSPaClaIts  ',
    'LEFT JOIN ' + TMeuDB + '.v s m o v i t s   vsp ON vsp.Controle=rci.VMI_Sorc ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsp.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vsp.CliVenda ',
    'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vsp.Empresa ',
    'LEFT JOIN ' + TMeuDB + '.entidades  frn ON frn.Codigo=vsp.Terceiro ',
    '/*LEFT JOIN ' + TMeuDB + '.vspalsta   vps ON vps.Codigo=vsp.Status*/ ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
    'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vsp.SerieFch  ',
    'WHERE cia.VSPaClaIts IN  ',
    '(  ',
    'SELECT pci2.Controle ',
    'FROM ' + TMeuDB + '.vspaclaitsa pci2 ',
    'LEFT JOIN ' + TMeuDB + '.vspaclacaba pca2 ON pca2.Codigo=pci2.Codigo ',
    'WHERE pca2.DtHrFimCla < "1900-01-01" ',
    'AND pci2.DtHrFim < "1900-01-01" ',
    ')  ',
    SQL_Empresa,
    SQL_GraGruX,
    SQL_GraGruY,
    SQL_CouNiv2,
    SQL_CouNiv1,
    'GROUP BY cia.VMI_Sorc, ggx.Controle',
    '']);
    //Geral.MB_Aviso(DModG.QrUpdPID1.SQL.Text);
  end;
end;
}

procedure TUnVS_PF.InsereVSMovImpExtra;
(*
var
  NO_PRD_TAM_COR, NO_PALLET, NO_FORNECE, NO_CLISTAT, NO_EMPRESA, NO_STATUS, DataHora, NO_GGY: String;
  Empresa, GraGruX, GraGru1, Pallet, Terceiro, CliStat, Status, OrdGGX, OrdGGY, GraGruY: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT, SdoVrtPeca, SdoVrtPeso, SdoVrtArM2, LmbVrtPeca, LmbVrtPeso, LmbVrtArM2: Double;
*)
begin
(*
  //Empresa        := ;
  //GraGruX        := ;
  Pecas          := 0;
  PesoKg         := 0;
  AreaM2         := 0;
  AreaP2         := 0;
  ValorT         := 0;
  SdoVrtPeca     := 0;
  SdoVrtPeso     := 0;
  SdoVrtArM2     := 0;
  //LmbVrtPeca     := ;
  //LmbVrtPeso     := ;
  //LmbVrtArM2     := ;
  //GraGru1        := ;
  //NO_PRD_TAM_COR := ;
  //Pallet         := ;
  //NO_PALLET      := ;
  Terceiro       := ;
  CliStat        := ;
  Status         := ;
  NO_FORNECE    := ;
  NO_CLISTAT     := ;
  NO_EMPRESA     := ;
  NO_STATUS      := ;
  DataHora       := ;
  OrdGGX         := ;
  OrdGGY         := ;
  GraGruY        := ;
  NO_GGY         := ;

  //
  Result := UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, Tabela, False, [
  'Empresa', 'GraGruX', 'Pecas',
  'PesoKg', 'AreaM2', 'AreaP2',
  'ValorT', 'SdoVrtPeca', 'SdoVrtPeso',
  'SdoVrtArM2', 'LmbVrtPeca', 'LmbVrtPeso',
  'LmbVrtArM2', 'GraGru1', 'NO_PRD_TAM_COR',
  'Pallet', 'NO_PALLET', 'Terceiro',
  'CliStat', 'Status', 'NO_FORNECE',
  'NO_CLISTAT', 'NO_EMPRESA', 'NO_STATUS',
  CO_DATA_HORA_GRL, 'OrdGGX', 'OrdGGY',
  'GraGruY', 'NO_GGY'], [
  ], [
  Empresa, GraGruX, Pecas,
  PesoKg, AreaM2, AreaP2,
  ValorT, SdoVrtPeca, SdoVrtPeso,
  SdoVrtArM2, LmbVrtPeca, LmbVrtPeso,
  LmbVrtArM2, GraGru1, NO_PRD_TAM_COR,
  Pallet, NO_PALLET, Terceiro,
  CliStat, Status, NO_FORNECE,
  NO_CLISTAT, NO_EMPRESA, NO_STATUS,
  DataHora, OrdGGX, OrdGGY,
  GraGruY, NO_GGY], [
  ], UserDataAlterweb?, IGNORE?
*)
end;

function TUnVS_PF.InverteDatas(const MovimXX: Integer; const Data, Agora:
  TDateTime; var DataHora, DtCorrApo: String): Boolean;
var
  DataIni, DataFim: TDateTime;
begin
  Result  := False;
  DataIni := Geral.PeriodoToDate(MovimXX + 1, 1, True, detJustSum);
  DataFim := Geral.UltimoDiaDoMes(DataIni);
  if DBCheck.CriaFm(TFmXXDataEFDData, FmXXDataEFDData, afmoNegarComAviso) then
  begin
    FmXXDataEFDData.TPDataFisico.MinDate := Dataini;
    FmXXDataEFDData.TPDataFisico.MaxDate := DataFim;
    if Data < Dataini then
      FmXXDataEFDData.TPDataFisico.Date    := DataIni
    else
    if Data > DataFim then
      FmXXDataEFDData.TPDataFisico.Date    := DataFim
    else
      FmXXDataEFDData.TPDataFisico.Date    := Agora;
    FmXXDataEFDData.EdHoraFisico.ValueVariant := Agora;
    //
    FmXXDataEFDData.TPDataSPEDEFD.Date         := Data;
    FmXXDataEFDData.EdHoraSPEDEFD.ValueVariant := Data;
    //
    FmXXDataEFDData.ShowModal;
    //
    DataHora  := FmXXDataEFDData.FDataFisico;
    DtCorrApo := FmXXDataEFDData.FDataSPEDEFD;
    Result    := True;
    //
    FmXXDataEFDData.Destroy;
  end;
end;

{
function TUnVS_PF.InsUpdVSMovIts_Old(SQLType: TSQLType; Codigo, MovimCod,
  MovimTwn, Empresa, Terceiro: Integer; MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv; Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2,
  AreaP2, ValorT: Double; DataHora: String; SrcMovID: TEstqMovimID; SrcNivel1,
  SrcNivel2: Integer; Observ: String; LnkNivXtr1, LnkNivXtr2, CliVenda,
  Controle, Ficha: Integer; CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
  DstMovID: TEstqMovimID; DstNivel1, DstNivel2: Integer; QtdGerPeca, QtdGerPeso,
  QtdGerArM2, QtdGerArP2: Double; AptoUso, FornecMO, SerieFch: Integer;
  NotaMPAG: Double; SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2, ReqMovEstq,
  StqCenLoc: Integer): Boolean;
begin
  Result := InsUpdVSMovIts_(SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, Controle, Ficha, CustoMOKg, CustoMOM2, CustoMOTot,
  ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
  QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc);
end;
}

procedure TUnVS_PF.MandaVSMovItsVSCacItsArquivoMorto();
begin
(*
SQL para saber os IMEIs que nao devem ir por terem estoque ou serem muito novos:
================================================================================
SELECT * FROM v s m o v i t s
WHERE SdoVrtPeca > 0
OR DataHora >= "2015-05-01"
ORDER BY DataHora
///// Resultados: 18.078 registros

SQL para saber os IMEIS que fazem parte dos pallets com estoque ou recentes
================================================================================
SELECT *
FROM v s m o v i t s WHERE Pallet IN
(
  SELECT Pallet
  FROM v s m o v i t s
  WHERE (
    SdoVrtPeca > 0
    OR
    DataHora >= "2015-05-01"
  )
  AND Pallet <> 0
)
///// Resultados: 14.302 registro

As duas SQL acima juntas
================================================================================
SELECT *
FROM v s m o v i t s
WHERE
  (SdoVrtPeca > 0)
OR
  (DataHora >= "2015-05-01")
OR
  (Pallet IN
    (
      SELECT Pallet
      FROM v s m o v i t s
      WHERE (
        SdoVrtPeca > 0
        OR
        DataHora >= "2015-05-01"
      )
      AND Pallet <> 0
    )
  )
///// Resultados: 18.851 registros

SQL Inverso da SQL acima - mais segura para nao excluir errado
================================================================================
SELECT *
FROM v s m o v i t s
WHERE
  (SdoVrtPeca <= 0)
AND
  (DataHora < "2015-05-01")
AND
  (NOT Pallet IN
    (
      SELECT Pallet
      FROM v s m o v i t s
      WHERE (
        SdoVrtPeca > 0
        OR
        DataHora >= "2015-05-01"
      )
      AND Pallet <> 0
    )
  )
///// Resultados: 4.897 registros
///// Resultados: 23.748 - 18.851 = 4.897 registros




VSCacItsA
////  207.299 registros

*)

end;

(*
procedure TUnVS_PF.MostraFormGraGruY(GraGruY, GragruX: Integer; NewNome: String);
begin
  if DBCheck.CriaFm(TFmGraGruY, FmGraGruY, afmoNegarComAviso) then
  begin
    if GraGruY <> 0 then
      FmGraGruY.LocCod(GraGruY, GraGruY);
    FmGraGruY.FGraGruX := GraGruX;
    FmGraGruY.FNewNome := NewNome;
    FmGraGruY.ShowModal;
    FmGraGruY.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormGraGruY1(GraGruY, GraGruX: Integer;
  NewNome: String);
begin
  if DBCheck.CriaFm(TFmGraGruY, FmGraGruY1, afmoNegarComAviso) then
  begin
    if GraGruY <> 0 then
      FmGraGruY1.LocCod(GraGruY, GraGruY);
    FmGraGruY1.FGraGruX := GraGruX;
    FmGraGruY1.FNewNome := NewNome;
    FmGraGruY1.ShowModal;
    FmGraGruY1.Destroy;
  end;
end;
*)

procedure TUnVS_PF.MostraFormOpcoesVSSifDipoa();
begin
  if DBCheck.CriaFm(TFmOpcoesVSSifDipoa, FmOpcoesVSSifDipoa, afmoNegarComAviso) then
  begin
    FmOpcoesVSSifDipoa.ShowModal;
    FmOpcoesVSSifDipoa.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormOperacoes(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOperacoes, FmOperacoes, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmOperacoes.LocCod(Codigo, Codigo);
    FmOperacoes.ShowModal;
    FmOperacoes.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSAjsCab(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmVSAjsCab, FmVSAjsCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSAjsCab.LocCod(Codigo, Codigo);
      FmVSAjsCab.QrVSAjsIts.Locate('Controle', Controle, []);
    end;
    FmVSAjsCab.ShowModal;
    FmVSAjsCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSArtCab(Codigo, GraGruX: Integer);
begin
  if DBCheck.CriaFm(TFmVSArtCab, FmVSArtCab, afmoNegarComAviso) then
  begin
    FmVSArtCab.ShowModal;
    if Codigo <> 0 then
    begin
      FmVSArtCab.LocCod(Codigo, Codigo);
      FmVSArtCab.QrVSArtGGX.Locate('GraGruX', GraGruX, []);
    end else
    if GraGruX <> 0 then
    begin
      Geral.MB_Erro('Localiza��o por reduzido n�o implementado!');
    end;
    FmVSArtCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSArvoreArtigos(Controle: Integer);
begin
  if DBCheck.CriaFm(TFmVSArvoreArtigos, FmVSArvoreArtigos, afmoNegarComAviso) then
  begin
    //FmVSArvoreArtigos.MontaArvore(Controle);
    FmVSArvoreArtigos.EdIMEI.ValueVariant := Controle;
    FmVSArvoreArtigos.ShowModal;
    FmVSArvoreArtigos.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSBxaCab(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmVSBxaCab, FmVSBxaCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSBxaCab.LocCod(Codigo, Codigo);
      FmVSBxaCab.QrVSBxaIts.Locate('Controle', Controle, []);
    end;
    FmVSBxaCab.ShowModal;
    FmVSBxaCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSCalCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSCalCab, FmVSCalCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSCalCab.LocCod(Codigo, Codigo);
    end;
    //
    FmVSCalCab.ShowModal;
    FmVSCalCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSConCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSConCab, FmVSConCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSConCab.LocCod(Codigo, Codigo);
    end;
    //
    FmVSConCab.ShowModal;
    FmVSConCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSCfgEqzCb(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSCfgEqzCb, FmVSCfgEqzCb, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSCfgEqzCb.LocCod(Codigo, Codigo);
    FmVSCfgEqzCb.ShowModal;
    FmVSCfgEqzCb.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSCfgMovEFD(ImporExpor, AnoMes, Empresa, PeriApu: Integer);
begin
  if DBCheck.CriaFm(TFmVSCfgMovEFD, FmVSCfgMovEFD, afmoNegarComAviso) then
  begin
    FmVSCfgMovEFD.FImporExpor := ImporExpor;
    FmVSCfgMovEFD.FAnoMes     := AnoMes;
    FmVSCfgMovEFD.FEmpresa    := Empresa;
    FmVSCfgMovEFD.FPeriApu    := PeriApu;
    //
    FmVSCfgMovEFD.PreencheAnoMesPeriodo(AnoMes);
    FmVSCfgMovEFD.ShowModal;
    FmVSCfgMovEFD.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSCGICab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSCGICab, FmVSCGICab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSCGICab.LocCod(Codigo, Codigo);
    FmVSCGICab.ShowModal;
    FmVSCGICab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSImpKardex4();
begin
  if DBCheck.CriaFm(TFmVSImpKardex4, FmVSImpKardex4, afmoNegarComAviso) then
  begin
    FmVSImpKardex4.ShowModal;
    FmVSImpKardex4.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSImpKardex5();
begin
  if DBCheck.CriaFm(TFmVSImpKardex5, FmVSImpKardex5, afmoNegarComAviso) then
  begin
    FmVSImpKardex5.ShowModal;
    FmVSImpKardex5.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSImpClaMulRMP();
begin
  if DBCheck.CriaFm(TFmVSImpClaMulRMP, FmVSImpClaMulRMP, afmoNegarComAviso) then
  begin
    FmVSImpClaMulRMP.ShowModal;
    FmVSImpClaMulRMP.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSImpMOEnvRet();
begin
  if DBCheck.CriaFm(TFmVSImpMOEnvRet, FmVSImpMOEnvRet, afmoNegarComAviso) then
  begin
    FmVSImpMOEnvRet.ShowModal;
    FmVSImpMOEnvRet.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSImpMOEnvRet2();
begin
  if DBCheck.CriaFm(TFmVSImpMOEnvRet2, FmVSImpMOEnvRet2, afmoNegarComAviso) then
  begin
    FmVSImpMOEnvRet2.ShowModal;
    FmVSImpMOEnvRet2.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSImpProducao();
begin
  if DBCheck.CriaFm(TFmVSImpProducao, FmVSImpProducao, afmoNegarComAviso) then
  begin
    FmVSImpProducao.ShowModal;
    FmVSImpProducao.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSImpResultVS();
begin
  if DBCheck.CriaFm(TFmVSImpResultVS, FmVSImpResultVS, afmoNegarComAviso) then
  begin
    FmVSImpResultVS.ShowModal;
    FmVSImpResultVS.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSImpSdoCorret();
begin
  if DBCheck.CriaFm(TFmVSimpSdoCorret, FmVSimpSdoCorret, afmoNegarComAviso) then
  begin
    FmVSimpSdoCorret.ShowModal;
    FmVSimpSdoCorret.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSInfInn(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSInfInn, FmVSInfInn, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSInfInn.LocCod(Codigo, Codigo);
    FmVSInfInn.ShowModal;
    FmVSInfInn.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSInfMov(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSInfMov, FmVSInfMov, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSInfMov.LocCod(Codigo, Codigo);
    FmVSInfMov.ShowModal;
    FmVSInfMov.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSInfOut(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSInfOut, FmVSInfOut, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSInfOut.LocCod(Codigo, Codigo);
    FmVSInfOut.ShowModal;
    FmVSInfOut.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSInfPal(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSInfPal, FmVSInfPal, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSInfPal.LocCod(Codigo, Codigo);
    FmVSInfPal.ShowModal;
    FmVSInfPal.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSInvNFe(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSInvNFe, FmVSInvNFe, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      //FmVSInvNFe.QrVSInvNFe.Locate('Codigo', Codigo, []);
      FmVSInvNFe.LocCod(Codigo, Codigo);
    FmVSInvNFe.ShowModal;
    FmVSInvNFe.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSLstPal(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSLstPal, FmVSLstPal, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSLstPal.LocCod(Codigo, Codigo);
    FmVSLstPal.ShowModal;
    FmVSLstPal.Destroy;
  end;
end;

function TUnVS_PF.MostraFormVSRMPPsq2(var SerieFch, Ficha: Integer): Boolean;
begin
  SerieFch := 0;
  Ficha    := 0;
  //
  if DBCheck.CriaFm(TFmVSRMPPsq, FmVSRMPPsq, afmoNegarComAviso) then
  begin
    FmVSRMPPsq.FMovimID  := emidAjuste;
    FmVSRMPPsq.FCodigo   := 0;
    FmVSRMPPsq.FControle := 0;
    //FmVSRMPPsq.ReopenMovimNiv(MovimID);
    //
    FmVSRMPPsq.ShowModal;
    //
    SerieFch := FmVSRMPPsq.FSerieFch;
    Ficha    := FmVSRMPPsq.FFicha;
    //
    FmVSRMPPsq.Destroy;
  end;
  if (SerieFch <> 0) or (Ficha <> 0) then
    Result := True
  else
    Result := False;
end;

procedure TUnVS_PF.MostraFormVSRRMCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSRRMCab, FmVSRRMCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSRRMCab.LocCod(Codigo, Codigo);
    FmVSRRMCab.ShowModal;
    FmVSRRMCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSRtbCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSRtbCab, FmVSRtbCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSRtbCab.LocCod(Codigo, Codigo);
      //FmVSRtbCab.ReopenVSPaClaCab();
    end;
    //
    FmVSRtbCab.ShowModal;
    FmVSRtbCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSMOEnvAvu(SQLType: TSQLType; QrVMI, QrVSMOEnvAvu,
  QrVSMOEnvAVMI: TmySQLQuery; Sinal: TSinal; Terceiro, CabSerieRem, CabNFeRem:
  Integer);
var
  Qry: TmySQLQuery;
var
  Pecas, PesoKg, AreaM2, AreaP2, ValorT, CusTrKg: Double;
  VMI, IDItem: Integer;
  Corda, TabVMIQtdEnvAvu, SQL_Qtd: String;
  //QrMyIMEI: TmySQLQuery;
begin
  if QrVMI = nil then
  begin
   // Fazer sql aqui!
   // QrMyIMEI :=

  end else // chamado de alguma janela VS (com IME-C de origem!)
  begin
    if (QrVMI.State = dsInactive) or (QrVMI.RecordCount = 0) then
    begin
      Geral.MB_Aviso('A��o abortada! N�o h� IME-I passivel de atrelamento!');
      Exit;
    end else
      //QrMyIMEI := QrVMI;
      ;
  end;
  //
  TabVMIQtdEnvAvu := UnCreateVS.RecriaTempTableNovo(ntrttVMIQtdEnvEVMI, DModG.QrUpdPID1, False);
  if QrVMI <> nil then
  begin
    VMI := QrVMI.FieldByName('Controle').AsInteger;
    Corda := MyObjects.CordaDeQuery(QrVMI, 'Controle', EmptyStr);
    QrVMI.Locate('Controle', VMI, []);
  end else
    Corda := '-999999999';
  //   Insere itens VMI Qtd!
  if Sinal = siNegativo then
    SQL_Qtd := '-vmi.Pecas, -vmi.PesoKg, -vmi.AreaM2, -vmi.AreaP2, -vmi.ValorT, '
  else
    SQL_Qtd := 'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, vmi.AreaP2, vmi.ValorT, ';
  //
  DModG.MyPID_DB.Execute(Geral.ATS([
  'INSERT INTO ' + TabVMIQtdEnvAvu,
  'SELECT vmi.Controle, vmi.MovimCod, vmi.Pallet, vmi.GraGruX, ',
  SQL_Qtd,
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, 0 IDItem, 1 Ativo',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE vmi.Controle IN (' + Corda + ')',
  '']));
  //
  if DBCheck.CriaFm(TFmVSMOEnvAvu, FmVSMOEnvAvu, afmoNegarComAviso) then
  begin
    FmVSMOEnvAvu.ImgTipo.SQLType := SQLType;
    FmVSMOEnvAvu.FTabVMIQtdEnvAvu := TabVMIQtdEnvAvu;
    FmVSMOEnvAvu.ReopenVMI(VMI);
    //
    if SQLType = stIns then
    begin
      if QrVSMOEnvAvu.RecordCount = 0 then
      begin
        CusTrKg  := 0;
      end else
      begin
        CusTrKg  := USQLDB.v_f(QrVSMOEnvAvu, 'CFTMA_CusTrKg');
      end;
      //
      if QrVMI <> nil then
      begin
        FmVSMOEnvAvu.EdVSVMI_MovimCod.ValueVariant := USQLDB.v_i(QrVMI, 'MovimCod'); //EnvMovimCod;
        FmVSMOEnvAvu.EdCFTMA_Empresa.ValueVariant  := USQLDB.v_i(QrVMI, 'Empresa'); //EnvEmpresa; //CFTMA_Empresa;
      end else
      begin
        FmVSMOEnvAvu.EdVSVMI_MovimCod.ValueVariant := 0;
        FmVSMOEnvAvu.EdCFTMA_Empresa.ValueVariant  := VAR_LIB_EMPRESA_SEL;
      end;
      //
      FmVSMOEnvAvu.EdNFEMA_FatID.ValueVariant    := -1;
      FmVSMOEnvAvu.EdNFEMA_FatNum.ValueVariant   := -1;
      FmVSMOEnvAvu.EdNFEMA_Terceiro.ValueVariant := Terceiro;
      FmVSMOEnvAvu.CBNFEMA_Terceiro.KeyValue     := Terceiro;
      FmVSMOEnvAvu.EdNFEMA_SerNF.ValueVariant    := CabSerieRem;
      FmVSMOEnvAvu.EdNFEMA_nNF.ValueVariant      := CabNFeRem;
      FmVSMOEnvAvu.EdNFEMA_nItem.ValueVariant    := -1;
      FmVSMOEnvAvu.EdNFEMA_Empresa.ValueVariant  := USQLDB.v_i(QrVMI, 'Empresa'); //EnvEmpresa; //AtuFornecMO;
      //
      FmVSMOEnvAvu.EdCFTMA_FatID.ValueVariant    := -1; //CFTMA_FatID;
      FmVSMOEnvAvu.EdCFTMA_FatNum.ValueVariant   := -1; //CFTMA_FatNum;
      FmVSMOEnvAvu.EdCFTMA_Terceiro.ValueVariant := 0;//CFTMA_Terceiro;
      FmVSMOEnvAvu.EdCFTMA_nItem.ValueVariant    := -1;//CFTMA_nItem;
      FmVSMOEnvAvu.EdCFTMA_SerCT.ValueVariant    := -1; //CFTMA_SerCT;
      FmVSMOEnvAvu.EdCFTMA_nCT.ValueVariant      := 0; //CFTMA_nCT;
      FmVSMOEnvAvu.EdCFTMA_CusTrKg.ValueVariant  := CusTrKg; //CusTrKg; //CFTMA_CusTrKg;
      //
    end else
    begin
      DModG.MyPID_DB.Execute(Geral.ATS([
      'UPDATE ' + TabVMIQtdEnvAvu,
      //'SET Pecas=0, PesoKg=0, AreaM2=0, AreaP2=0, Ativo=0 ',
      'SET Ativo=0 ',
      '']));
      QrVSMOEnvAVMI.First;
      while not QrVSMOEnvAVMI.Eof do
      begin
        Pecas    := USQLDB.v_f(QrVSMOEnvAVMI, 'Pecas');
        PesoKg   := USQLDB.v_f(QrVSMOEnvAVMI, 'PesoKg');
        AreaM2   := USQLDB.v_f(QrVSMOEnvAVMI, 'AreaM2');
        AreaP2   := USQLDB.v_f(QrVSMOEnvAVMI, 'AreaP2');
        VMI      := USQLDB.v_i(QrVSMOEnvAVMI, 'VSMovIts');
        IDItem   := USQLDB.v_i(QrVSMOEnvAVMI, 'Codigo');
        //
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, TabVMIQtdEnvAvu, False, [
        'Ativo', 'Pecas', 'PesoKg',
        'AreaM2', 'AreaP2', 'IDItem'], [
        'Controle'], [
        1, Pecas, PesoKg,
        AreaM2, AreaP2, IDItem], [
        VMI], False);
        //
        QrVSMOEnvAVMI.Next;
      end;
      FmVSMOEnvAvu.ReopenVMI(0);
      //
      Qry := TmySQLQuery.Create(Dmod);
      try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * FROM vsmoenvavu ',
      'WHERE Codigo=' + Geral.FF0(USQLDB.v_i(QrVSMOEnvAvu, 'Codigo')), //Codigo),
      '']);
      FmVSMOEnvAvu.EdCodigo.ValueVariant         := USQLDB.v_i(Qry, 'Codigo');
      //
      FmVSMOEnvAvu.EdVSVMI_MovimCod.ValueVariant := USQLDB.v_i(Qry, 'VSVMI_MovimCod');
      //
      FmVSMOEnvAvu.MeNome.Text                   := USQLDB.v_x(Qry, 'Nome');
      FmVSMOEnvAvu.TPData.Date                   := USQLDB.v_t(Qry, 'DataHora');
      FmVSMOEnvAvu.EdHora.ValueVariant           := USQLDB.v_t(Qry, 'DataHora');
      //
      //
      FmVSMOEnvAvu.EdNFEMA_FatID.ValueVariant    := USQLDB.v_i(Qry, 'NFEMA_FatID');
      FmVSMOEnvAvu.EdNFEMA_FatNum.ValueVariant   := USQLDB.v_i(Qry, 'NFEMA_FatNum');
      FmVSMOEnvAvu.EdNFEMA_Empresa.ValueVariant  := USQLDB.v_i(Qry, 'NFEMA_Empresa');
      FmVSMOEnvAvu.EdNFEMA_Terceiro.ValueVariant := USQLDB.v_i(Qry, 'NFEMA_Terceiro');
      FmVSMOEnvAvu.CBNFEMA_Terceiro.KeyValue     := USQLDB.v_i(Qry, 'NFEMA_Terceiro');
      FmVSMOEnvAvu.EdNFEMA_nItem.ValueVariant    := USQLDB.v_i(Qry, 'NFEMA_nItem');
      FmVSMOEnvAvu.EdNFEMA_SerNF.ValueVariant    := USQLDB.v_i(Qry, 'NFEMA_SerNF');
      FmVSMOEnvAvu.EdNFEMA_nNF.ValueVariant      := USQLDB.v_i(Qry, 'NFEMA_nNF');
      FmVSMOEnvAvu.EdNFEMA_Pecas.ValueVariant    := USQLDB.v_f(Qry, 'NFEMA_Pecas');
      FmVSMOEnvAvu.EdNFEMA_PesoKg.ValueVariant   := USQLDB.v_f(Qry, 'NFEMA_PesoKg');
      FmVSMOEnvAvu.EdNFEMA_AreaM2.ValueVariant   := USQLDB.v_f(Qry, 'NFEMA_AreaM2');
      FmVSMOEnvAvu.EdNFEMA_AreaP2.ValueVariant   := USQLDB.v_f(Qry, 'NFEMA_AreaP2');
      //
      FmVSMOEnvAvu.EdCFTMA_FatID.ValueVariant    := USQLDB.v_i(Qry, 'CFTMA_FatID');
      FmVSMOEnvAvu.EdCFTMA_FatNum.ValueVariant   := USQLDB.v_i(Qry, 'CFTMA_FatNum');
      FmVSMOEnvAvu.EdCFTMA_Empresa.ValueVariant  := USQLDB.v_i(Qry, 'CFTMA_Empresa');
      FmVSMOEnvAvu.EdCFTMA_Terceiro.ValueVariant := USQLDB.v_i(Qry, 'CFTMA_Terceiro');
      FmVSMOEnvAvu.CBCFTMA_Terceiro.KeyValue     := USQLDB.v_i(Qry, 'CFTMA_Terceiro');
      FmVSMOEnvAvu.EdCFTMA_nItem.ValueVariant    := USQLDB.v_i(Qry, 'CFTMA_nItem');
      FmVSMOEnvAvu.EdCFTMA_SerCT.ValueVariant    := USQLDB.v_i(Qry, 'CFTMA_SerCT');
      FmVSMOEnvAvu.EdCFTMA_nCT.ValueVariant      := USQLDB.v_i(Qry, 'CFTMA_nCT');
      FmVSMOEnvAvu.EdCFTMA_Pecas.ValueVariant    := USQLDB.v_f(Qry, 'CFTMA_Pecas');
      FmVSMOEnvAvu.EdCFTMA_PesoKg.ValueVariant   := USQLDB.v_f(Qry, 'CFTMA_PesoKg');
      FmVSMOEnvAvu.EdCFTMA_AreaM2.ValueVariant   := USQLDB.v_f(Qry, 'CFTMA_AreaM2');
      FmVSMOEnvAvu.EdCFTMA_AreaP2.ValueVariant   := USQLDB.v_f(Qry, 'CFTMA_AreaP2');
      FmVSMOEnvAvu.EdCFTMA_PesTrKg.ValueVariant  := USQLDB.v_f(Qry, 'CFTMA_PesTrKg');
      FmVSMOEnvAvu.EdCFTMA_CusTrKg.ValueVariant  := USQLDB.v_f(Qry, 'CFTMA_CusTrKg');
      //
      FmVSMOEnvAvu.EdCFTMA_ValorT.ValueVariant   := USQLDB.v_f(Qry, 'CFTMA_ValorT');
      //
      finally
        Qry.Free
      end;
    end;
    FmVSMOEnvAvu.ShowModal;
    VAR_VSMOEnvAvu := FmVSMOEnvAvu.FCodigo;
    //
    if QrVSMOEnvAvu.State <> dsInactive then
    begin
      UnDmkDAC_PF.AbreQuery(QrVSMOEnvAvu, Dmod.MyDB);
      QrVSMOEnvAvu.Locate('Codigo', FmVSMOEnvAvu.FCodigo, []);
    end;
    FmVSMOEnvAvu.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSMOEnvAvuGer(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSMOEnvAvuGer, FmVSMOEnvAvuGer, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSMOEnvAvuGer.LocCod(Codigo, Codigo);
    FmVSMOEnvAvuGer.ShowModal;
    //
    FmVSMOEnvAvuGer.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSMOEnvCTeGer(Empresa, Terceiro, SerCTe,
  nCTe: Integer);
begin
  if DBCheck.CriaFm(TFmVSMOEnvCTeGer, FmVSMOEnvCTeGer, afmoNegarComAviso) then
  begin
    FmVSMOEnvCTeGer.ShowModal;
    //
    FmVSMOEnvCTeGer.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSMOEnvCTeImp(Empresa, Terceiro, SerCTe,
  nCTe: Integer);
begin
  if DBCheck.CriaFm(TFmVSMOEnvCTeImp, FmVSMOEnvCTeImp, afmoNegarComAviso) then
  begin
    FmVSMOEnvCTeImp.FEmpresa  := Empresa;
    FmVSMOEnvCTeImp.FTerceiro := Terceiro;
    FmVSMOEnvCTeImp.FSerCTe   := SerCTe;
    FmVSMOEnvCTeImp.FnCTe     := nCTe;
    FmVSMOEnvCTeImp.PesquisaCTe();
    //
    FmVSMOEnvCTeImp.ShowModal;
    //
    FmVSMOEnvCTeImp.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSMOEnvEnv(SQLType: TSQLType; QrVMI, QrVSMOEnvEnv,
  QrVSMOEnvEVMI: TmySQLQuery; Sinal: TSinal; CabSerieRem, CabNFeRem: Integer);
var
  Qry: TmySQLQuery;
var
  Pecas, PesoKg, AreaM2, AreaP2, ValorT, CusTrKg: Double;
  VMI, IDItem: Integer;
  Corda, TabVMIQtdEnvEnv, SQL_Qtd: String;
begin
  if (QrVMI.State = dsInactive) or (QrVMI.RecordCount = 0) then
  begin
    Geral.MB_Aviso('A��o abortada! N�o h� IME-I passivel de atrelamento!');
    Exit;
  end;
  TabVMIQtdEnvEnv := UnCreateVS.RecriaTempTableNovo(ntrttVMIQtdEnvEVMI, DModG.QrUpdPID1, False);
  VMI := QrVMI.FieldByName('Controle').AsInteger;
  Corda := MyObjects.CordaDeQuery(QrVMI, 'Controle', EmptyStr);
  QrVMI.Locate('Controle', VMI, []);
  //   Insere itens VMI Qtd!
  if Sinal = siNegativo then
    //SQL_Qtd := '-vmi.Pecas, -vmi.PesoKg, -vmi.AreaM2, -vmi.AreaP2, -vmi.ValorT, '
    SQL_Qtd := '-vmi.Pecas, -vmi.PesoKg, -vmi.AreaM2, -vmi.AreaP2, -vmi.ValorMP, '
  else
    //SQL_Qtd := 'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, vmi.AreaP2, vmi.ValorT, ';
    SQL_Qtd := 'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, vmi.AreaP2, vmi.ValorMP, ';
  //
  DModG.MyPID_DB.Execute(Geral.ATS([
  'INSERT INTO ' + TabVMIQtdEnvEnv,
  'SELECT vmi.Controle, vmi.MovimCod, vmi.Pallet, vmi.GraGruX, ',
  SQL_Qtd,
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, 0 IDItem, 1 Ativo',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE vmi.Controle IN (' + Corda + ')',
  '']));
  //
  if DBCheck.CriaFm(TFmVSMOEnvEnv, FmVSMOEnvEnv, afmoNegarComAviso) then
  begin
    FmVSMOEnvEnv.ImgTipo.SQLType := SQLType;
    FmVSMOEnvEnv.FTabVMIQtdEnvEnv := TabVMIQtdEnvEnv;
    FmVSMOEnvEnv.ReopenVMI(VMI);
    //
    if SQLType = stIns then
    begin
      //MyObjects.SelecionarLinhasNoDBGrid(TDBGrid(FmVSMOEnvEnv.DBGIMEIs), True);
      //FmVSMOEnvEnv.EdCodigo.ValueVariant         := ;
      //
      FmVSMOEnvEnv.EdNFEMP_FatID.ValueVariant    := -1;
      FmVSMOEnvEnv.EdNFEMP_FatNum.ValueVariant   := -1;
      FmVSMOEnvEnv.EdNFEMP_Empresa.ValueVariant  := USQLDB.v_i(QrVMI, 'Empresa'); //EnvEmpresa; //AtuFornecMO;
      FmVSMOEnvEnv.EdNFEMP_Terceiro.ValueVariant := USQLDB.v_i(QrVMI, 'FornecMO'); //AtuFornecMO;
      //FmVSMOEnvEnv.EdNFEMP_nItem.ValueVariant    := ;
      FmVSMOEnvEnv.EdNFEMP_SerNF.ValueVariant    := CabSerieRem;
      FmVSMOEnvEnv.EdNFEMP_nNF.ValueVariant      := CabNFeRem;
      //
      (*
      Pecas    := USQLDB.v_f(QrVMI, 'Pecas');
      PesoKg   := USQLDB.v_f(QrVMI, 'PesoKg');
      AreaM2   := USQLDB.v_f(QrVMI, 'AreaM2');
      AreaP2   := USQLDB.v_f(QrVMI, 'AreaP2');
      ValorT   := USQLDB.v_f(QrVMI, 'ValorT');
      *)
      if QrVSMOEnvEnv.RecordCount = 0 then
      begin
        CusTrKg  := 0;
      end else
      begin
        CusTrKg  := USQLDB.v_f(QrVSMOEnvEnv, 'CFTMP_CusTrKg');
(*
        //
        QrVSMOEnvEnv.First;
        while not QrVSMOEnvEnv.Eof do
        begin
          Pecas  := Pecas  - USQLDB.v_f(QrVSMOEnvEnv, 'NFEMP_Pecas');
          PesoKg := PesoKg - USQLDB.v_f(QrVSMOEnvEnv, 'NFEMP_PesoKg');
          AreaM2 := AreaM2 - USQLDB.v_f(QrVSMOEnvEnv, 'NFEMP_AreaM2');
          AreaP2 := AreaP2 - USQLDB.v_f(QrVSMOEnvEnv, 'NFEMP_AreaP2');
          ValorT := ValorT - USQLDB.v_f(QrVSMOEnvEnv, 'NFEMP_ValorT');
          //
          QrVSMOEnvEnv.Next;
        end;
*)
      end;
      //
(*
      FmVSMOEnvEnv.EdNFEMP_Pecas.ValueVariant    := Pecas; //EnvPecas;
      FmVSMOEnvEnv.EdNFEMP_PesoKg.ValueVariant   := PesoKg; //EnvPesoKg;
      FmVSMOEnvEnv.EdNFEMP_AreaM2.ValueVariant   := AreaM2; //EnvAreaM2;
      FmVSMOEnvEnv.EdNFEMP_AreaP2.ValueVariant   := AreaP2; //EnvAreaP2;
      FmVSMOEnvEnv.EdNFEMP_ValorT.ValueVariant   := ValorT; //EnvValorT;
*)
      (*
      FmVSMOEnvEnv.EdVSVMI_Controle.ValueVariant := USQLDB.v_i(QrVMI, 'Controle'); //EnvControle;
      FmVSMOEnvEnv.EdVSVMI_Codigo.ValueVariant   := USQLDB.v_i(QrVMI, 'Codigo'); //EnvCodigo;
      FmVSMOEnvEnv.EdVSVMI_MovimID.ValueVariant  := USQLDB.v_i(QrVMI, 'MovimID'); //EnvMovimID;
      FmVSMOEnvEnv.EdVSVMI_MovimNiv.ValueVariant := USQLDB.v_i(QrVMI, 'MovimNiv'); //EnvMovimNiv;
      *)
      FmVSMOEnvEnv.EdVSVMI_MovimCod.ValueVariant := USQLDB.v_i(QrVMI, 'MovimCod'); //EnvMovimCod;
      (*
      FmVSMOEnvEnv.EdVSVMI_Empresa.ValueVariant  := USQLDB.v_i(QrVMI, 'Empresa'); //EnvEmpresa;
      FmVSMOEnvEnv.EdVSVMI_SerNF.ValueVariant    := CabSerieRem;
      FmVSMOEnvEnv.EdVSVMI_nNF.ValueVariant      := CabNFeRem;
      *)
      //
      FmVSMOEnvEnv.EdCFTMP_FatID.ValueVariant    := -1; //CFTMP_FatID;
      FmVSMOEnvEnv.EdCFTMP_FatNum.ValueVariant   := -1; //CFTMP_FatNum;
      FmVSMOEnvEnv.EdCFTMP_Empresa.ValueVariant  := USQLDB.v_i(QrVMI, 'Empresa'); //EnvEmpresa; //CFTMP_Empresa;
      FmVSMOEnvEnv.EdCFTMP_Terceiro.ValueVariant := 0;//CFTMP_Terceiro;
      FmVSMOEnvEnv.EdCFTMP_nItem.ValueVariant    := -1;//CFTMP_nItem;
      FmVSMOEnvEnv.EdCFTMP_SerCT.ValueVariant    := -1; //CFTMP_SerCT;
      FmVSMOEnvEnv.EdCFTMP_nCT.ValueVariant      := 0; //CFTMP_nCT;
(*
      FmVSMOEnvEnv.EdCFTMP_Pecas.ValueVariant    := Pecas;  //EnvPecas; //CFTMP_Pecas;
      FmVSMOEnvEnv.EdCFTMP_PesoKg.ValueVariant   := PesoKg; //EnvPesoKg; //CFTMP_PesoKg;
      FmVSMOEnvEnv.EdCFTMP_AreaM2.ValueVariant   := AreaM2; //EnvAreaM2; //CFTMP_AreaM2;
      FmVSMOEnvEnv.EdCFTMP_AreaP2.ValueVariant   := AreaP2; //EnvAreaP2; //CFTMP_AreaP2;
      FmVSMOEnvEnv.EdCFTMP_ValorT.ValueVariant   := 0; //CFTMP_ValorT;
      FmVSMOEnvEnv.EdCFTMP_PesTrKg.ValueVariant  := PesoKg; //PesTrKg; //CFTMP_PesTrKg;
*)
      FmVSMOEnvEnv.EdCFTMP_CusTrKg.ValueVariant  := CusTrKg; //CusTrKg; //CFTMP_CusTrKg;
      //
    end else
    begin
      DModG.MyPID_DB.Execute(Geral.ATS([
      'UPDATE ' + TabVMIQtdEnvEnv,
      //'SET Pecas=0, PesoKg=0, AreaM2=0, AreaP2=0, Ativo=0 ',
      'SET Ativo=0 ',
      '']));
      QrVSMOEnvEVMI.First;
      while not QrVSMOEnvEVMI.Eof do
      begin
        Pecas    := USQLDB.v_f(QrVSMOEnvEVMI, 'Pecas');
        PesoKg   := USQLDB.v_f(QrVSMOEnvEVMI, 'PesoKg');
        AreaM2   := USQLDB.v_f(QrVSMOEnvEVMI, 'AreaM2');
        AreaP2   := USQLDB.v_f(QrVSMOEnvEVMI, 'AreaP2');
        VMI      := USQLDB.v_i(QrVSMOEnvEVMI, 'VSMovIts');
        IDItem   := USQLDB.v_i(QrVSMOEnvEVMI, 'Codigo');
        //
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, TabVMIQtdEnvEnv, False, [
        'Ativo', 'Pecas', 'PesoKg',
        'AreaM2', 'AreaP2', 'IDItem'], [
        'Controle'], [
        1, Pecas, PesoKg,
        AreaM2, AreaP2, IDItem], [
        VMI], False);
        //
        QrVSMOEnvEVMI.Next;
      end;
      FmVSMOEnvEnv.ReopenVMI(0);
      //
      Qry := TmySQLQuery.Create(Dmod);
      try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * FROM vsmoenvenv ',
      'WHERE Codigo=' + Geral.FF0(USQLDB.v_i(QrVSMOEnvEnv, 'Codigo')), //Codigo),
      '']);
      FmVSMOEnvEnv.EdCodigo.ValueVariant         := USQLDB.v_i(Qry, 'Codigo');
      //
      (*
      FmVSMOEnvEnv.EdVSVMI_Controle.ValueVariant := USQLDB.v_i(Qry, 'VSVMI_Controle');
      FmVSMOEnvEnv.EdVSVMI_Codigo.ValueVariant   := USQLDB.v_i(Qry, 'VSVMI_Codigo');
      FmVSMOEnvEnv.EdVSVMI_MovimID.ValueVariant  := USQLDB.v_i(Qry, 'VSVMI_MovimID');
      FmVSMOEnvEnv.EdVSVMI_MovimNiv.ValueVariant := USQLDB.v_i(Qry, 'VSVMI_MovimNiv');
      *)
      FmVSMOEnvEnv.EdVSVMI_MovimCod.ValueVariant := USQLDB.v_i(Qry, 'VSVMI_MovimCod');
      (*
      FmVSMOEnvEnv.EdVSVMI_Empresa.ValueVariant  := USQLDB.v_i(Qry, 'VSVMI_Empresa');
      FmVSMOEnvEnv.EdVSVMI_SerNF.ValueVariant    := USQLDB.v_i(Qry, 'VSVMI_SerNF');
      FmVSMOEnvEnv.EdVSVMI_nNF.ValueVariant      := USQLDB.v_i(Qry, 'VSVMI_nNF');
      *)
      //
      FmVSMOEnvEnv.MeNome.Text                   := USQLDB.v_x(Qry, 'Nome');
      FmVSMOEnvEnv.TPData.Date                   := USQLDB.v_t(Qry, 'DataHora');
      FmVSMOEnvEnv.EdHora.ValueVariant           := USQLDB.v_t(Qry, 'DataHora');
      //
      FmVSMOEnvEnv.EdNFEMP_FatID.ValueVariant    := USQLDB.v_i(Qry, 'NFEMP_FatID');
      FmVSMOEnvEnv.EdNFEMP_FatNum.ValueVariant   := USQLDB.v_i(Qry, 'NFEMP_FatNum');
      FmVSMOEnvEnv.EdNFEMP_Empresa.ValueVariant  := USQLDB.v_i(Qry, 'NFEMP_Empresa');
      FmVSMOEnvEnv.EdNFEMP_Terceiro.ValueVariant := USQLDB.v_i(Qry, 'NFEMP_Terceiro');
      FmVSMOEnvEnv.CBNFEMP_Terceiro.KeyValue     := USQLDB.v_i(Qry, 'NFEMP_Terceiro');
      FmVSMOEnvEnv.EdNFEMP_nItem.ValueVariant    := USQLDB.v_i(Qry, 'NFEMP_nItem');
      FmVSMOEnvEnv.EdNFEMP_SerNF.ValueVariant    := USQLDB.v_i(Qry, 'NFEMP_SerNF');
      FmVSMOEnvEnv.EdNFEMP_nNF.ValueVariant      := USQLDB.v_i(Qry, 'NFEMP_nNF');
      FmVSMOEnvEnv.EdNFEMP_Pecas.ValueVariant    := USQLDB.v_f(Qry, 'NFEMP_Pecas');
      FmVSMOEnvEnv.EdNFEMP_PesoKg.ValueVariant   := USQLDB.v_f(Qry, 'NFEMP_PesoKg');
      FmVSMOEnvEnv.EdNFEMP_AreaM2.ValueVariant   := USQLDB.v_f(Qry, 'NFEMP_AreaM2');
      FmVSMOEnvEnv.EdNFEMP_AreaP2.ValueVariant   := USQLDB.v_f(Qry, 'NFEMP_AreaP2');
      //
      FmVSMOEnvEnv.EdCFTMP_FatID.ValueVariant    := USQLDB.v_i(Qry, 'CFTMP_FatID');
      FmVSMOEnvEnv.EdCFTMP_FatNum.ValueVariant   := USQLDB.v_i(Qry, 'CFTMP_FatNum');
      FmVSMOEnvEnv.EdCFTMP_Empresa.ValueVariant  := USQLDB.v_i(Qry, 'CFTMP_Empresa');
      FmVSMOEnvEnv.EdCFTMP_Terceiro.ValueVariant := USQLDB.v_i(Qry, 'CFTMP_Terceiro');
      FmVSMOEnvEnv.CBCFTMP_Terceiro.KeyValue     := USQLDB.v_i(Qry, 'CFTMP_Terceiro');
      FmVSMOEnvEnv.EdCFTMP_nItem.ValueVariant    := USQLDB.v_i(Qry, 'CFTMP_nItem');
      FmVSMOEnvEnv.EdCFTMP_SerCT.ValueVariant    := USQLDB.v_i(Qry, 'CFTMP_SerCT');
      FmVSMOEnvEnv.EdCFTMP_nCT.ValueVariant      := USQLDB.v_i(Qry, 'CFTMP_nCT');
      FmVSMOEnvEnv.EdCFTMP_Pecas.ValueVariant    := USQLDB.v_f(Qry, 'CFTMP_Pecas');
      FmVSMOEnvEnv.EdCFTMP_PesoKg.ValueVariant   := USQLDB.v_f(Qry, 'CFTMP_PesoKg');
      FmVSMOEnvEnv.EdCFTMP_AreaM2.ValueVariant   := USQLDB.v_f(Qry, 'CFTMP_AreaM2');
      FmVSMOEnvEnv.EdCFTMP_AreaP2.ValueVariant   := USQLDB.v_f(Qry, 'CFTMP_AreaP2');
      FmVSMOEnvEnv.EdCFTMP_PesTrKg.ValueVariant  := USQLDB.v_f(Qry, 'CFTMP_PesTrKg');
      FmVSMOEnvEnv.EdCFTMP_CusTrKg.ValueVariant  := USQLDB.v_f(Qry, 'CFTMP_CusTrKg');
      //
      FmVSMOEnvEnv.EdNFEMP_ValorT.ValueVariant   := USQLDB.v_f(Qry, 'NFEMP_ValorT');
      FmVSMOEnvEnv.EdCFTMP_ValorT.ValueVariant   := USQLDB.v_f(Qry, 'CFTMP_ValorT');
      //
      finally
        Qry.Free
      end;
    end;
    FmVSMOEnvEnv.ShowModal;
    if QrVSMOEnvEnv.State <> dsInactive then
    begin
      UnDmkDAC_PF.AbreQuery(QrVSMOEnvEnv, Dmod.MyDB);
      QrVSMOEnvEnv.Locate('Codigo', FmVSMOEnvEnv.FCodigo, []);
    end;
    FmVSMOEnvEnv.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSMOEnvEnvGer(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSMOEnvEnvGer, FmVSMOEnvEnvGer, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSMOEnvEnvGer.LocCod(Codigo, Codigo);
    FmVSMOEnvEnvGer.ShowModal;
    //
    FmVSMOEnvEnvGer.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSMOEnvRetGer(Codigo: Integer);
begin
  Geral.MB_Erro('Falta implemenatr: TUnVS_PF.MostraFormVSMOEnvEnvRet()');
end;

procedure TUnVS_PF.MostraFormVSMOEnvSel(const EnvEmpresa, EnvSerieRem, EnvNFeRem,
  EnvFornecMO, MovimCod: Integer; const TabVMIQtdEnvRVMI: String);
begin
  if DBCheck.CriaFm(TFmVSMOEnvSel, FmVSMOEnvSel, afmoNegarComAviso) then
  begin
    FmVSMOEnvSel.FTabVMIQtdEnvRVMI             := TabVMIQtdEnvRVMI;
    //
    FmVSMOEnvSel.EdNFEMP_Empresa.ValueVariant  := EnvEmpresa;
    FmVSMOEnvSel.EdNFEMP_SerNF.ValueVariant    := EnvSerieRem;
    FmVSMOEnvSel.EdNFEMP_nNF.ValueVariant      := EnvNFeRem;
    FmVSMOEnvSel.EdNFEMP_Terceiro.ValueVariant := EnvFornecMO;
    FmVSMOEnvSel.EdVSVMI_MovimCod.ValueVariant := MovimCod;
    FmVSMOEnvSel.Pesquisa();
    FmVSMOEnvSel.ShowModal;
    //
    FmVSMOEnvSel.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSMOEnvRet(SQLType: TSQLType; QrVMINew, QrVMIDst,
  QrVMIBxa, QrVSMOEnvRet, QrVSMOEnvGVMI, QrVSMOEnvRVMI: TmySQLQuery;
  MovimCod_Env, CabSerieRem, CabNFeRem: Integer; Sinal: TSinal;
  GerArX2: Boolean);
var
  TabVMIQtdEnvGVMI, TabVMIQtdEnvRVMI: String;
  //
  procedure InsereEnvRVMI_Atual();
  var
    VSMOEnvEnv, NFeSer, NFeNum, IDItem: Integer;
    Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
    SQLType: TSQLType;
  begin
    SQLType        := stIns;
    VSMOEnvEnv     := QrVSMOEnvRVMI.FieldByName('VSMOEnvEnv').AsInteger;
    NFeSer         := QrVSMOEnvRVMI.FieldByName('NFEMP_SerNF').AsInteger;
    NFeNum         := QrVSMOEnvRVMI.FieldByName('NFEMP_nNF').AsInteger;
    Pecas          := QrVSMOEnvRVMI.FieldByName('Pecas').AsFloat;
    PesoKg         := QrVSMOEnvRVMI.FieldByName('PesoKg').AsFloat;
    AreaM2         := QrVSMOEnvRVMI.FieldByName('AreaM2').AsFloat;
    AreaP2         := QrVSMOEnvRVMI.FieldByName('AreaP2').AsFloat;
    ValorT         := QrVSMOEnvRVMI.FieldByName('ValorT').AsFloat;
    IDItem         := QrVSMOEnvRVMI.FieldByName('Codigo').AsInteger;
    //
    try
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, SQLType, TabVMIQtdEnvRVMI, False, [
      'NFeSer', 'NFeNum', 'Pecas',
      'PesoKg', 'AreaM2', 'AreaP2',
      'ValorT', 'IDItem'], [
      'VSMOEnvEnv'], [
      NFeSer, NFeNum, Pecas,
      PesoKg, AreaM2, AreaP2,
      ValorT, IDItem], [
      VSMOEnvEnv], False);
    finally
      // continua?
    end;
  end;
var
  Qry: TmySQLQuery;
var
  //VSMOEnvEnv, VSVMI_MovimCod,
  //VSSrc_Controle, VSSrc_Codigo, VSSrc_MovimID, VSSrc_MovimNiv, VSSrc_Empresa,
  Pecas, PesoKg, AreaM2, AreaP2, ValorT, CusTrKg: Double;
  VSSrc_MovimCod, VMI, IDItem: Integer;
  Corda, SQL_Qtd: String;
begin
  if (QrVMIDst.State = dsInactive) or (QrVMIDst.RecordCount = 0) then
  begin
    Geral.MB_Aviso(
    'A��o abortada! N�o h� IME-I de produto industrializado pass�vel de atrelamento!');
    Exit;
  end;
  TabVMIQtdEnvGVMI := UnCreateVS.RecriaTempTableNovo(ntrttVMIQtdEnvGVMI, DModG.QrUpdPID1, False);
  TabVMIQtdEnvRVMI := UnCreateVS.RecriaTempTableNovo(ntrttVMIQtdEnvRVMI, DModG.QrUpdPID1, False);
  VMI := QrVMIDst.FieldByName('Controle').AsInteger;
  Corda := MyObjects.CordaDeQuery(QrVMIDst, 'Controle', EmptyStr);
  QrVMIDst.Locate('Controle', VMI, []);
  //   Insere itens VMI Qtd!
  if GerArX2 then
  begin
    if Sinal = siNegativo then
      SQL_Qtd := '-vmi.Pecas, 0 PesoKg, -vmi.QtdGerArM2, -vmi.QtdGerArP2, -vmi.ValorT, '
    else
      SQL_Qtd := 'vmi.Pecas, 0 PesoKg, vmi.QtdGerArM2, vmi.QtdGerArP2, vmi.ValorT, ';
  end else
  begin
    if Sinal = siNegativo then
      SQL_Qtd := '-vmi.Pecas, -vmi.PesoKg, -vmi.AreaM2, -vmi.AreaP2, -vmi.ValorT, '
    else
      SQL_Qtd := 'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, vmi.AreaP2, vmi.ValorT, ';
  end;
  //
  DModG.MyPID_DB.Execute(Geral.ATS([
  'INSERT INTO ' + TabVMIQtdEnvGVMI,
  'SELECT vmi.Controle, vmi.MovimCod, vmi.GraGruX, ',
  SQL_Qtd,
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, 0 IDItem, 1 Ativo',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE vmi.Controle IN (' + Corda + ')',
  '']));
  //
  //VSVMI_MovimCod := 0;
  if SQLType = stIns then
  begin
    // Retorno
    MostraFormVSMOEnvSel(USQLDB.v_i(QrVMIDst, 'Empresa'), -1(*EnvSerieRem*),
    0(*EnvNFeRem*), 0(*EnvFornecMO*), MovimCod_Env, TabVMIQtdEnvRVMI);
    //
    // Gerados
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM vsmoenvgvmi',
      'WHERE VSMovIts IN (' + Corda + ')',
      '']);
      //
      while not Qry.Eof do
      begin
        DModG.MyPID_DB.Execute(Geral.ATS([
        'UPDATE ' + TabVMIQtdEnvGVMI,
        'SET Pecas=Pecas-' + Geral.FFT_Dot(Qry.FieldByName('Pecas').AsFloat, 6, siNegativo) +
        ', PesoKg=PesoKg-' + Geral.FFT_Dot(Qry.FieldByName('PesoKg').AsFloat, 6, siNegativo) +
        ', AreaM2=AreaM2-' + Geral.FFT_Dot(Qry.FieldByName('AreaM2').AsFloat, 6, siNegativo) +
        ', AreaP2=AreaP2-' + Geral.FFT_Dot(Qry.FieldByName('AreaP2').AsFloat, 6, siNegativo) +
        'WHERE Controle=' + Geral.FF0(Qry.FieldByName('VSMovIts').AsInteger) +
        '']));
        //
        Qry.Next;
      end;
      //
    finally
      Qry.Free;
    end;
  end else
  begin
    // Retorno
    QrVSMOEnvRVMI.First;
    while not QrVSMOEnvRVMI.Eof do
    begin
      InsereEnvRVMI_Atual();
      //
      QrVSMOEnvRVMI.Next;
    end;
    //
    // Gerados abaixo!
  end;
  //
  if DBCheck.CriaFm(TFmVSMOEnvRet, FmVSMOEnvRet, afmoNegarComAviso) then
  begin
    FmVSMOEnvRet.ImgTipo.SQLType := SQLType;
    FmVSMOEnvRet.FTabVMIQtdEnvGVMI := TabVMIQtdEnvGVMI;
    FmVSMOEnvRet.FTabVMIQtdEnvRVMI := TabVMIQtdEnvRVMI;
    //
    if SQLType = stIns then
    begin
      //FmVSMOEnvRet.EdCodigo.ValueVariant         := ;
      //FmVSMOEnvRet.EdNFCMO_FatID.ValueVariant    := ;
      //FmVSMOEnvRet.EdNFCMO_FatNum.ValueVariant   := ;
      FmVSMOEnvRet.EdNFCMO_Empresa.ValueVariant  := USQLDB.v_i(QrVMIDst, 'Empresa'); //DstEmpresa; //AtuFornecMO;
      FmVSMOEnvRet.EdNFCMO_Terceiro.ValueVariant := USQLDB.v_i(QrVMIDst, 'FornecMO'); //AtuFornecMO;
      //FmVSMOEnvRet.EdNFCMO_nItem.ValueVariant    := ;
      //FmVSMOEnvRet.EdNFCMO_SerNF.ValueVariant    := ;
      //FmVSMOEnvRet.EdNFCMO_nNF.ValueVariant      := ;
      FmVSMOEnvRet.EdNFCMO_Pecas.ValueVariant    := DmkPF.VP(USQLDB.v_f(QrVMIBxa, 'Pecas')); //DstPecas;
      FmVSMOEnvRet.EdNFCMO_PesoKg.ValueVariant   := DmkPF.VP(USQLDB.v_f(QrVMIBxa, 'PesoKg')); //DstPesoKg;
      FmVSMOEnvRet.EdNFCMO_AreaM2.ValueVariant   := DmkPF.VP(USQLDB.v_f(QrVMIBxa, 'AreaM2')); //DstAreaM2;
      FmVSMOEnvRet.EdNFCMO_AreaP2.ValueVariant   := DmkPF.VP(USQLDB.v_f(QrVMIBxa, 'AreaP2')); //DstAreaP2;
      FmVSMOEnvRet.EdNFCMO_CusMOKG.ValueVariant  := USQLDB.v_i(QrVMINew, 'CustoMOKG');
      FmVSMOEnvRet.EdNFCMO_CusMOM2.ValueVariant  := USQLDB.v_i(QrVMINew, 'CustoMOM2');
      //FmVSMOEnvRet.EdNFCMO_ValorT.ValueVariant   := DstValorT;
      //FmVSMOEnvRet.EdNFRMP_FatID.ValueVariant    := ;
      //FmVSMOEnvRet.EdNFRMP_FatNum.ValueVariant   := ;
      FmVSMOEnvRet.EdNFRMP_Empresa.ValueVariant  := USQLDB.v_i(QrVMIDst, 'Empresa'); //DstEmpresa; //AtuFornecMO;
      FmVSMOEnvRet.EdNFRMP_Terceiro.ValueVariant := USQLDB.v_i(QrVMIDst, 'FornecMO'); //AtuFornecMO;
      //FmVSMOEnvRet.EdNFRMP_nItem.ValueVariant    := ;
      //FmVSMOEnvRet.EdNFRMP_SerNF.ValueVariant    := ;
      //FmVSMOEnvRet.EdNFRMP_nNF.ValueVariant      := ;
      FmVSMOEnvRet.EdNFRMP_Pecas.ValueVariant    := DmkPF.VP(USQLDB.v_f(QrVMIDst, 'Pecas'));  //BxaPecas;
      FmVSMOEnvRet.EdNFRMP_PesoKg.ValueVariant   := DmkPF.VP(USQLDB.v_f(QrVMIDst, 'PesoKg')); //BxaPesoKg;
      FmVSMOEnvRet.EdNFRMP_AreaM2.ValueVariant   := DmkPF.VP(USQLDB.v_f(QrVMIDst, 'AreaM2')); //BxaAreaM2;
      FmVSMOEnvRet.EdNFRMP_AreaP2.ValueVariant   := DmkPF.VP(USQLDB.v_f(QrVMIDst, 'AreaP2')); //BxaAreaP2;
      FmVSMOEnvRet.EdNFRMP_ValorT.ValueVariant   := DmkPF.VP(USQLDB.v_f(QrVMIDst, 'ValorT')); // BxaValorT;
(*
      FmVSMOEnvRet.EdVSVMI_Controle.ValueVariant := USQLDB.v_i(QrVMIDst, 'Controle'); //DstControle;
      FmVSMOEnvRet.EdVSVMI_Codigo.ValueVariant   := USQLDB.v_i(QrVMIDst, 'Codigo'); //stCodigo;
      FmVSMOEnvRet.EdVSVMI_MovimID.ValueVariant  := USQLDB.v_i(QrVMIDst, 'MovimID'); //DstMovimID;
      FmVSMOEnvRet.EdVSVMI_MovimNiv.ValueVariant := USQLDB.v_i(QrVMIDst, 'MovimNiv'); //DstMovimNiv;
      FmVSMOEnvRet.EdVSVMI_MovimCod.ValueVariant := USQLDB.v_i(QrVMIDst, 'MovimCod'); //DstMovimCod;
      FmVSMOEnvRet.EdVSVMI_Empresa.ValueVariant  := USQLDB.v_i(QrVMIDst, 'Empresa'); //DstEmpresa;
      FmVSMOEnvRet.EdVSVMI_SerNF.ValueVariant    := CabSerieRem;
      FmVSMOEnvRet.EdVSVMI_nNF.ValueVariant      := CabNFeRem;
      //
*)
      FmVSMOEnvRet.EdCFTPA_Empresa.ValueVariant  := USQLDB.v_i(QrVMIDst, 'Empresa');
      FmVSMOEnvRet.EdCFTPA_Pecas.ValueVariant    := DmkPF.VP(USQLDB.v_f(QrVMIDst, 'Pecas'));  //BxaPecas;
      FmVSMOEnvRet.EdCFTPA_PesoKg.ValueVariant   := DmkPF.VP(USQLDB.v_f(QrVMIDst, 'PesoKg')); //BxaPesoKg;
      FmVSMOEnvRet.EdCFTPA_AreaM2.ValueVariant   := DmkPF.VP(USQLDB.v_f(QrVMIDst, 'AreaM2')); //BxaAreaM2;
      FmVSMOEnvRet.EdCFTPA_AreaP2.ValueVariant   := DmkPF.VP(USQLDB.v_f(QrVMIDst, 'AreaP2')); //BxaAreaP2;
      //FmVSMOEnvRet.EdCFTPA_PesTrKg.ValueVariant   := ????
      //FmVSMOEnvRet.EdCFTPA_CusTrKg.ValueVariant   := ????
      //FmVSMOEnvRet.EdCFTPA_ValorT.ValueVariant   := ???;
      //
      (*
      FmVSMOEnvRet.EdVSMOEnvEnv.ValueVariant     := VSMOEnvEnv;
      FmVSMOEnvRet.EdVSSrc_Controle.ValueVariant := VSSrc_Controle;
      FmVSMOEnvRet.EdVSSrc_Codigo.ValueVariant   := VSSrc_Codigo;
      FmVSMOEnvRet.EdVSSrc_MovimID.ValueVariant  := VSSrc_MovimID;
      FmVSMOEnvRet.EdVSSrc_MovimNiv.ValueVariant := VSSrc_MovimNiv;
      FmVSMOEnvRet.EdVSSrc_MovimCod.ValueVariant := VSSrc_MovimCod;
      FmVSMOEnvRet.EdVSSrc_Empresa.ValueVariant  := VSSrc_Empresa;
      *)
      //
    end else
    begin
      DModG.MyPID_DB.Execute(Geral.ATS([
      'UPDATE ' + TabVMIQtdEnvGVMI,
      //'SET Pecas=0, PesoKg=0, AreaM2=0, AreaP2=0, Ativo=0 ',
      'SET Ativo=0 ',
      '']));
      QrVSMOEnvGVMI.First;
      while not QrVSMOEnvGVMI.Eof do
      begin
        Pecas    := USQLDB.v_f(QrVSMOEnvGVMI, 'Pecas');
        PesoKg   := USQLDB.v_f(QrVSMOEnvGVMI, 'PesoKg');
        AreaM2   := USQLDB.v_f(QrVSMOEnvGVMI, 'AreaM2');
        AreaP2   := USQLDB.v_f(QrVSMOEnvGVMI, 'AreaP2');
        VMI      := USQLDB.v_i(QrVSMOEnvGVMI, 'VSMovIts');
        IDItem   := USQLDB.v_i(QrVSMOEnvGVMI, 'Codigo');
        //
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, TabVMIQtdEnvGVMI, False, [
        'Ativo', 'Pecas', 'PesoKg',
        'AreaM2', 'AreaP2', 'IDItem'], [
        'Controle'], [
        1, Pecas, PesoKg,
        AreaM2, AreaP2, IDItem], [
        VMI], False);
        //
        QrVSMOEnvGVMI.Next;
      end;
      FmVSMOEnvRet.ReopenVSMOEnvGVMI(0);
      //
      Qry := TmySQLQuery.Create(Dmod);
      try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * FROM vsmoenvret ',
      'WHERE Codigo=' + Geral.FF0(USQLDB.v_i(QrVSMOEnvRet, 'Codigo')),
      '']);
      //
      FmVSMOEnvRet.EdCodigo.ValueVariant         := USQLDB.v_i(Qry, 'Codigo');
      //
(*
      FmVSMOEnvRet.EdVSVMI_Controle.ValueVariant := USQLDB.v_i(Qry, 'VSVMI_Controle');
      FmVSMOEnvRet.EdVSVMI_Codigo.ValueVariant   := USQLDB.v_i(Qry, 'VSVMI_Codigo');
      FmVSMOEnvRet.EdVSVMI_MovimID.ValueVariant  := USQLDB.v_i(Qry, 'VSVMI_MovimID');
      FmVSMOEnvRet.EdVSVMI_MovimNiv.ValueVariant := USQLDB.v_i(Qry, 'VSVMI_MovimNiv');
      FmVSMOEnvRet.EdVSVMI_MovimCod.ValueVariant := USQLDB.v_i(Qry, 'VSVMI_MovimCod');
      FmVSMOEnvRet.EdVSVMI_Empresa.ValueVariant  := USQLDB.v_i(Qry, 'VSVMI_Empresa');
      FmVSMOEnvRet.EdVSVMI_SerNF.ValueVariant    := USQLDB.v_i(Qry, 'VSVMI_SerNF');
      FmVSMOEnvRet.EdVSVMI_nNF.ValueVariant      := USQLDB.v_i(Qry, 'VSVMI_nNF');
      //
      FmVSMOEnvRet.EdVSMOEnvEnv.ValueVariant     := USQLDB.v_i(Qry, 'VSMOEnvEnv');
      FmVSMOEnvRet.EdVSSrc_Controle.ValueVariant := USQLDB.v_i(Qry, 'VSSrc_Controle');
      FmVSMOEnvRet.EdVSSrc_Codigo.ValueVariant   := USQLDB.v_i(Qry, 'VSSrc_Codigo');
      FmVSMOEnvRet.EdVSSrc_MovimID.ValueVariant  := USQLDB.v_i(Qry, 'VSSrc_MovimID');
      FmVSMOEnvRet.EdVSSrc_MovimNiv.ValueVariant := USQLDB.v_i(Qry, 'VSSrc_MovimNiv');
      FmVSMOEnvRet.EdVSSrc_MovimCod.ValueVariant := USQLDB.v_i(Qry, 'VSSrc_MovimCod');
      FmVSMOEnvRet.EdVSSrc_Empresa.ValueVariant  := USQLDB.v_i(Qry, 'VSSrc_Empresa');
      //
*)
      //
      FmVSMOEnvRet.MeNome.Text                   := USQLDB.v_x(Qry, 'Nome');
      FmVSMOEnvRet.TPData.Date                   := USQLDB.v_t(Qry, 'DataHora');
      FmVSMOEnvRet.EdHora.ValueVariant           := USQLDB.v_t(Qry, 'DataHora');
      //
      FmVSMOEnvRet.EdNFRMP_FatID.ValueVariant    := USQLDB.v_i(Qry, 'NFRMP_FatID');
      FmVSMOEnvRet.EdNFRMP_FatNum.ValueVariant   := USQLDB.v_i(Qry, 'NFRMP_FatNum');
      FmVSMOEnvRet.EdNFRMP_Empresa.ValueVariant  := USQLDB.v_i(Qry, 'NFRMP_Empresa');
      FmVSMOEnvRet.EdNFRMP_Terceiro.ValueVariant := USQLDB.v_i(Qry, 'NFRMP_Terceiro');
      FmVSMOEnvRet.CBNFRMP_Terceiro.KeyValue     := USQLDB.v_i(Qry, 'NFRMP_Terceiro');
      FmVSMOEnvRet.EdNFRMP_nItem.ValueVariant    := USQLDB.v_i(Qry, 'NFRMP_nItem');
      FmVSMOEnvRet.EdNFRMP_SerNF.ValueVariant    := USQLDB.v_i(Qry, 'NFRMP_SerNF');
      FmVSMOEnvRet.EdNFRMP_nNF.ValueVariant      := USQLDB.v_i(Qry, 'NFRMP_nNF');
      FmVSMOEnvRet.EdNFRMP_Pecas.ValueVariant    := USQLDB.v_f(Qry, 'NFRMP_Pecas');
      FmVSMOEnvRet.EdNFRMP_PesoKg.ValueVariant   := USQLDB.v_f(Qry, 'NFRMP_PesoKg');
      FmVSMOEnvRet.EdNFRMP_AreaM2.ValueVariant   := USQLDB.v_f(Qry, 'NFRMP_AreaM2');
      FmVSMOEnvRet.EdNFRMP_AreaP2.ValueVariant   := USQLDB.v_f(Qry, 'NFRMP_AreaP2');
      //
      FmVSMOEnvRet.EdNFCMO_FatID.ValueVariant    := USQLDB.v_i(Qry, 'NFCMO_FatID');
      FmVSMOEnvRet.EdNFCMO_FatNum.ValueVariant   := USQLDB.v_i(Qry, 'NFCMO_FatNum');
      FmVSMOEnvRet.EdNFCMO_Empresa.ValueVariant  := USQLDB.v_i(Qry, 'NFCMO_Empresa');
      FmVSMOEnvRet.EdNFCMO_Terceiro.ValueVariant := USQLDB.v_i(Qry, 'NFCMO_Terceiro');
      FmVSMOEnvRet.CBNFCMO_Terceiro.KeyValue     := USQLDB.v_i(Qry, 'NFCMO_Terceiro');
      FmVSMOEnvRet.EdNFCMO_nItem.ValueVariant    := USQLDB.v_i(Qry, 'NFCMO_nItem');
      FmVSMOEnvRet.EdNFCMO_SerNF.ValueVariant    := USQLDB.v_i(Qry, 'NFCMO_SerNF');
      FmVSMOEnvRet.EdNFCMO_nNF.ValueVariant      := USQLDB.v_i(Qry, 'NFCMO_nNF');
      FmVSMOEnvRet.EdNFCMO_Pecas.ValueVariant    := USQLDB.v_f(Qry, 'NFCMO_Pecas');
      FmVSMOEnvRet.EdNFCMO_PesoKg.ValueVariant   := USQLDB.v_f(Qry, 'NFCMO_PesoKg');
      FmVSMOEnvRet.EdNFCMO_AreaM2.ValueVariant   := USQLDB.v_f(Qry, 'NFCMO_AreaM2');
      FmVSMOEnvRet.EdNFCMO_AreaP2.ValueVariant   := USQLDB.v_f(Qry, 'NFCMO_AreaP2');
      FmVSMOEnvRet.EdNFCMO_CusMOKG.ValueVariant  := USQLDB.v_f(Qry, 'NFCMO_CusMOKG');
      FmVSMOEnvRet.EdNFCMO_CusMOM2.ValueVariant  := USQLDB.v_f(Qry, 'NFCMO_CusMOM2');
      //
      FmVSMOEnvRet.EdCFTPA_FatID.ValueVariant    := USQLDB.v_i(Qry, 'CFTPA_FatID');
      FmVSMOEnvRet.EdCFTPA_FatNum.ValueVariant   := USQLDB.v_i(Qry, 'CFTPA_FatNum');
      FmVSMOEnvRet.EdCFTPA_Empresa.ValueVariant  := USQLDB.v_i(Qry, 'CFTPA_Empresa');
      FmVSMOEnvRet.EdCFTPA_Terceiro.ValueVariant := USQLDB.v_i(Qry, 'CFTPA_Terceiro');
      FmVSMOEnvRet.CBCFTPA_Terceiro.KeyValue     := USQLDB.v_i(Qry, 'CFTPA_Terceiro');
      FmVSMOEnvRet.EdCFTPA_nItem.ValueVariant    := USQLDB.v_i(Qry, 'CFTPA_nItem');
      FmVSMOEnvRet.EdCFTPA_Pecas.ValueVariant    := USQLDB.v_i(Qry, 'CFTPA_Pecas');
      FmVSMOEnvRet.EdCFTPA_PesoKg.ValueVariant   := USQLDB.v_i(Qry, 'CFTPA_PesoKg');
      FmVSMOEnvRet.EdCFTPA_AreaM2.ValueVariant   := USQLDB.v_i(Qry, 'CFTPA_AreaM2');
      FmVSMOEnvRet.EdCFTPA_AreaP2.ValueVariant   := USQLDB.v_i(Qry, 'CFTPA_AreaP2');
      FmVSMOEnvRet.EdCFTPA_PesTrKg.ValueVariant  := USQLDB.v_i(Qry, 'CFTPA_PesTrKg');
      FmVSMOEnvRet.EdCFTPA_CusTrKg.ValueVariant  := USQLDB.v_i(Qry, 'CFTPA_CusTrKg');
      FmVSMOEnvRet.EdCFTPA_SerCT.ValueVariant    := USQLDB.v_i(Qry, 'CFTPA_SerCT');
      FmVSMOEnvRet.EdCFTPA_nCT.ValueVariant      := USQLDB.v_i(Qry, 'CFTPA_nCT');
      //
      FmVSMOEnvRet.EdNFRMP_ValorT.ValueVariant   := USQLDB.v_f(Qry, 'NFRMP_ValorT');
      FmVSMOEnvRet.EdNFCMO_ValorT.ValueVariant   := USQLDB.v_f(Qry, 'NFCMO_ValorT');
      FmVSMOEnvRet.EdCFTPA_ValorT.ValueVariant   := USQLDB.v_i(Qry, 'CFTPA_ValorT');
      //
      finally
        Qry.Free
      end;
    end;
    FmVSMOEnvRet.ReopenVSMOEnvGVMI(0);
    FmVSMOEnvRet.ReopenVSMOEnvRVMI(0);
    //
    FmVSMOEnvRet.IncrementaListaEnvEnv();
    //
    FmVSMOEnvRet.ShowModal;
    if QrVSMOEnvRet.State <> dsInactive then
    begin
      UnDmkDAC_PF.AbreQuery(QrVSMOEnvRet, Dmod.MyDB);
      QrVSMOEnvRet.Locate('Codigo', FmVSMOEnvRet.FCodigo, []);
    end;
    FmVSMOEnvRet.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSMOPWEGer();
begin
  if DBCheck.CriaFm(TFmVSMOPWEGer, FmVSMOPWEGer, afmoNegarComAviso) then
  begin
    FmVSMOPWEGer.ShowModal;
    FmVSMOPWEGer.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSMotivBxa((*Codigo: Integer*));
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'VSMotivBxa', 60, ncGerlSeq1,
  'Motivo de Baixa Extra',
  [], False, Null, [], [], False);
end;

procedure TUnVS_PF.MostraFormVSMovCab(MovimCod: Integer);
begin
  if DBCheck.CriaFm(TFmVSMovCab, FmVSMovCab, afmoNegarComAviso) then
  begin
    if MovimCod <> 0 then
      FmVSMovCab.LocCod(MovimCod, MovimCod);
    FmVSMovCab.ShowModal;
    FmVSMovCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSMovItbAdd();
var
  Janela: Integer;
begin
(*
  Janela := MyObjects.SelRadioGroup('Modo de Envio ao Arquivo Morto',
  'Selecione o modo:', ['Nenhum', 'Modo atual (demorado)',
    'Modo novo (em desenvolvimento)'], 2);
  //
  case Janela of
    1:
    begin
      if DBCheck.CriaFm(TFmVSMovItbAdd, FmVSMovItbAdd, afmoNegarComAviso) then
      begin
        FmVSMovItbAdd.ShowModal;
        FmVSMovItbAdd.Destroy;
      end;
    end;
    2:
    begin
*)
      if DBCheck.CriaFm(TFmVSMovItbAdd3, FmVSMovItbAdd3, afmoNegarComAviso) then
      begin
        FmVSMovItbAdd3.ShowModal;
        FmVSMovItbAdd3.Destroy;
      end;
(*
    end;
  end;
*)
end;

procedure TUnVS_PF.MostraFormVSMrtCad;
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'VSMrtCad', 60, ncGerlSeq1,
  'Martelos de Proced�ncias',
  [], False, Null, [], [], False);
end;

procedure TUnVS_PF.MostraFormVSNatCad(GraGruX: Integer; Edita: Boolean;
  QryMul: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmVSNatCad, FmVSNatCad, afmoNegarComAviso) then
  begin
    FmVSNatCad.FQryMul := QryMul;
    if GraGruX <> 0 then
    begin
      FmVSNatCad.LocCod(GraGruX, GraGruX);
      if FmVSNatCad.QrVSNatCadGraGruX.Value = GraGruX then
        FmVSNatCad.AlteraMP1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSNatCad.FSeq := 1;
    FmVSNatCad.ShowModal;
    FmVSNatCad.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSNatCon(GraGruX: Integer; Edita: Boolean;
  QryMul: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmVSNatCon, FmVSNatCon, afmoNegarComAviso) then
  begin
    FmVSNatCon.FQryMul := QryMul;
    if GraGruX <> 0 then
    begin
      FmVSNatCon.LocCod(GraGruX, GraGruX);
      if FmVSNatCon.QrVSNatConGraGruX.Value = GraGruX then
        FmVSNatCon.AlteraMP1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSNatCon.FSeq := 1;
    FmVSNatCon.ShowModal;
    FmVSNatCon.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSNatInC(GraGruX: Integer; Edita: Boolean);
begin
  if DBCheck.CriaFm(TFmVSNatInC, FmVSNatInC, afmoNegarComAviso) then
  begin
    if GraGruX <> 0 then
    begin
      FmVSNatInC.LocCod(GraGruX, GraGruX);
      if FmVSNatInC.QrVSNatInCGraGruX.Value = GraGruX then
        FmVSNatInC.AlteraMP1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSNatInC.FSeq := 1;
    FmVSNatInC.ShowModal;
    FmVSNatInC.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSNatPDA(GraGruX: Integer; Edita: Boolean);
begin
  if DBCheck.CriaFm(TFmVSNatPDA, FmVSNatPDA, afmoNegarComAviso) then
  begin
    if GraGruX <> 0 then
    begin
      FmVSNatPDA.LocCod(GraGruX, GraGruX);
      if FmVSNatPDA.QrVSNatPDAGraGruX.Value = GraGruX then
        FmVSNatPDA.AlteraArtigoClassificadoAtual1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSNatPDA.FSeq := 1;
    FmVSNatPDA.ShowModal;
    FmVSNatPDA.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSOCGerCab(MovimCod: Integer);
begin
  if DBCheck.CriaFm(TFmVSOCGerCab, FmVSOCGerCab, afmoNegarComAviso) then
  begin
    (*if MovimCod <> 0 then
    begin
    end;
    *)
    FmVSOCGerCab.ShowModal;
    FmVSOCGerCab.Destroy;
  end;
end;


{
procedure TUnVS_PF.MostraFormVSOutIts(SQLType: TSQLType; Controle: Integer;
  QrCab, QrIts: TmySQLQuery);
var
  Qry: TmySQLQuery;
  Empresa: Integer;
begin
  if DBCheck.CriaFm(TFmVSOutIts, FmVSOutIts, afmoNegarComAviso) then
  begin
    FmVSOutIts.ImgTipo.SQLType := SQLType;
    FmVSOutIts.FQrCab := QrCab;
    //FmVSOutIts.FDsCab := DsCab;
    FmVSOutIts.FQrIts := QrIts;
    if SQLType = stIns then
    begin
      FmVSOutIts.FAntSrcNivel2 := 0;
    end else
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM ' + CO_SEL_TAB_VMI,
        'WHERE Controle=' + Geral.FF0(Controle),
        '']);
        if Qry.RecordCount = 0 then
        begin
          FmVSOutIts.Destroy;
          Geral.MB_Erro('ID Item ' + Geral.FF0(Controle) + ' n�o encontrado na tabela ativa!');
          Exit;
        end;
        if TEstqMovimID(Qry.FieldByName('MovimID').AsInteger) <> emidForcado then
        begin
          FmVSOutIts.Destroy;
          Geral.MB_Erro('ID Item ' + Geral.FF0(Controle) + ' n�o � de baixa for�ada!');
          Exit;
        end;
        //
        //Codigo         :=
        FmVSOutIts.EdCodigo.ValueVariant := Qry.FieldByName('Codigo').AsInteger;
        //Controle       :=
        FmVSOutIts.EdControle.ValueVariant := Qry.FieldByName('Controle').AsInteger;
        //MovimCod       :=
        FmVSOutIts.EdMovimCod.ValueVariant := Qry.FieldByName('MovimCod').AsInteger;
        (*Empresa*)
        Empresa := DModG.ObtemFilialDeEntidade(Qry.FieldByName('Empresa').AsInteger);
        FmVSOutIts.EdEmpresa.ValueVariant := Empresa;
        FmVSOutIts.CBEmpresa.KeyValue     := Empresa;
        //Fornecedor     :=
        //FmVSOutIts.EdFornecedor.ValueVariant := Qry.FieldByName('Terceiro').AsInteger;
        //FmVSOutIts.CBFornecedor.KeyValue     := Qry.FieldByName('Terceiro').AsInteger;
        //DataHora       := Geral.FDT(
        FmVSOutIts.TPDataHora.Date := Qry.FieldByName(CO_DATA_HORA_VMI).AsDateTime;
        FmVSOutIts.EdDataHora.ValueVariant  := Qry.FieldByName(CO_DATA_HORA_VMI).AsDateTime;
        //MovimID        := emidForcado;
        //MovimNiv       := eminSemNiv;
        //Pallet         :=
        FmVSOutIts.EdPsqPallet.ValueVariant   := Qry.FieldByName('Pallet').AsInteger;
        FmVSOutIts.CBPsqPallet.KeyValue       := Qry.FieldByName('Pallet').AsInteger;
        // Ficha
        FmVSOutIts.EdPsqSerieFch.ValueVariant := Qry.FieldByName('SerieFch').AsInteger;
        FmVSOutIts.CBPsqSerieFch.KeyValue     := Qry.FieldByName('SerieFch').AsInteger;
        FmVSOutIts.EdPsqFicha.ValueVariant    := Qry.FieldByName('Ficha').AsInteger;
        FmVSOutIts.EdPsqMarca.ValueVariant    := Qry.FieldByName('Marca').AsInteger;
        //GraGruX        :=
        FmVSOutIts.EdGragruX.ValueVariant     := Qry.FieldByName('GragruX').AsInteger;
        FmVSOutIts.CBGragruX.KeyValue         := Qry.FieldByName('GragruX').AsInteger;
        //Pecas          := -
        FmVSOutIts.EdPecas.ValueVariant       := -Qry.FieldByName('Pecas').AsFloat;
        //PesoKg         := -
        FmVSOutIts.EdPesoKg.ValueVariant      := -Qry.FieldByName('PesoKg').AsFloat;
        //AreaM2         := -
        FmVSOutIts.EdAreaM2.ValueVariant      := -Qry.FieldByName('AreaM2').AsFloat;
        //AreaP2         := -
        FmVSOutIts.EdAreaP2.ValueVariant      := -Qry.FieldByName('AreaP2').AsFloat;
        //ValorT         := -
        FmVSOutIts.EdValorT.ValueVariant      := -Qry.FieldByName('ValorT').AsFloat;
        //Observ         :=
        FmVSOutIts.EdObserv.Text              := Qry.FieldByName('Observ').AsString;
        //
        FmVSOutIts.EdSrcMovID.ValueVariant    := Qry.FieldByName('SrcMovID').AsInteger;
        FmVSOutIts.EdSrcNivel1.ValueVariant   := Qry.FieldByName('SrcNivel1').AsInteger;
        FmVSOutIts.EdSrcNivel2.ValueVariant   := Qry.FieldByName('SrcNivel2').AsInteger;
        //
        FmVSOutIts.FAntSrcNivel2              := Qry.FieldByName('SrcNivel2').AsInteger;
      finally
        Qry.Free;
      end;
    end;
    FmVSOutIts.ShowModal;
    FmVSOutIts.Destroy;
  end;
end;
}

procedure TUnVS_PF.MostraFormVSPaCRIts();
begin
  if DBCheck.CriaFm(TFmVSPaCRIts, FmVSPaCRIts, afmoNegarComAviso) then
  begin
    //if Codigo <> 0 then
      //FmVSPaCRIts.LocCod(Codigo, Codigo);
    FmVSPaCRIts.ShowModal;
    FmVSPaCRIts.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSPaInClaRcl(Pallet, CacCod: Integer);
begin
  if DBCheck.CriaFm(TFmVSPaInClaRcl, FmVSPaInClaRcl, afmoNegarComAviso) then
  begin
    FmVSPaInClaRcl.FPallet := Pallet;
    FmVSPaInClaRcl.FCacCod := CacCod;
    FmVSPaInClaRcl.ReopenPalClaRcl();
    FmVSPaInClaRcl.ShowModal;
    //
    FmVSPaInClaRcl.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSPalSta();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'VSPalSta', 60, ncGerlSeq1,
  'Status de Pallet',
  [], True, 2000, [], [], False);
end;

procedure TUnVS_PF.MostraFormVSPaMulCabA(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSPaMulCab, FmVSPaMulCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSPaMulCab.LocCod(Codigo, Codigo);
    FmVSPaMulCab.ShowModal;
    FmVSPaMulCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSPedCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSPedCab, FmVSPedCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSPedCab.LocCod(Codigo, Codigo);
    FmVSPedCab.ShowModal;
    FmVSPedCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSPesqSeqPeca;
begin
  if DBCheck.CriaFm(TFmVSPesqSeqPeca, FmVSPesqSeqPeca, afmoNegarComAviso) then
  begin
    FmVSPesqSeqPeca.ShowModal;
    FmVSPesqSeqPeca.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSProCal(GraGruX: Integer; Edita: Boolean);
begin
  if DBCheck.CriaFm(TFmVSProCal, FmVSProCal, afmoNegarComAviso) then
  begin
    if GraGruX <> 0 then
    begin
      FmVSProCal.LocCod(GraGruX, GraGruX);
      if FmVSProCal.QrVSProCalGraGruX.Value = GraGruX then
        FmVSProCal.AlteraMP1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSProCal.FSeq := 1;
    FmVSProCal.ShowModal;
    FmVSProCal.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSProCur(GraGruX: Integer; Edita: Boolean);
begin
  if DBCheck.CriaFm(TFmVSProCur, FmVSProCur, afmoNegarComAviso) then
  begin
    if GraGruX <> 0 then
    begin
      FmVSProCur.LocCod(GraGruX, GraGruX);
      if FmVSProCur.QrVSProCurGraGruX.Value = GraGruX then
        FmVSProCur.AlteraMP1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSProCur.FSeq := 1;
    FmVSProCur.ShowModal;
    FmVSProCur.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSPSPCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSPSPCab, FmVSPSPCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSPSPCab.LocCod(Codigo, Codigo);
    end;
    FmVSPSPCab.ShowModal;
    FmVSPSPCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSPSPEnd(GraGruX: Integer; Edita: Boolean; QryMul:
  TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmVSPSPEnd, FmVSPSPEnd, afmoNegarComAviso) then
  begin
    FmVSPSPEnd.FQryMul := QryMul;
    if GraGruX <> 0 then
    begin
      FmVSPSPEnd.LocCod(GraGruX, GraGruX);
      if FmVSPSPEnd.QrVSPSPEndGraGruX.Value = GraGruX then
        FmVSPSPEnd.AlteraArtigoClassificadoAtual1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSPSPEnd.FSeq := 1;
    FmVSPSPEnd.ShowModal;
    FmVSPSPEnd.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSPSPPro(GraGruX: Integer; Edita: Boolean);
begin
  if DBCheck.CriaFm(TFmVSPSPPro, FmVSPSPPro, afmoNegarComAviso) then
  begin
    if GraGruX <> 0 then
    begin
      FmVSPSPPro.LocCod(GraGruX, GraGruX);
      if FmVSPSPPro.QrVSPSPProGraGruX.Value = GraGruX then
        FmVSPSPPro.AlteraArtigoClassificadoAtual1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSPSPPro.FSeq := 1;
    FmVSPSPPro.ShowModal;
    FmVSPSPPro.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSPwdDd(AutoRegera: Boolean);
begin
  if DBCheck.CriaFm(TFmVSPwdDd, FmVSPwdDd, afmoNegarComAviso) then
  begin
    if (FmVSPwdDd.QrControleVSPwdDdUser.Value = VAR_USUARIO) or (VAR_USUARIO < 0) then
    begin
      if AutoRegera then
      begin
        if (Trunc(FmVSPwdDd.QrControleVSPwdDdData.Value) < Trunc(DModG.ObtemAgora())) then
        begin
          FmVSPwdDd.RegeraSenha();
          FmVSPwdDd.AlteraDados();
          FmVSPwdDd.ShowModal;
        end;
      end else
        FmVSPwdDd.ShowModal;
      FmVSPwdDd.Destroy;
    end else
      Geral.MB_Aviso('Acesso restrito pelo login!');
  end;
end;

{
procedure TUnVS_PF.MostraFormVSRclArtPrpOld(SQLType: TSQLType);
var
  Codigo, CacCod(*, TpAreaRcl*): Integer;
  MovimID: TEstqMovimID;
begin
  if DBCheck.CriaFm(TFmVSRclArtPrpOld, FmVSRclArtPrpOld, afmoNegarComAviso) then
  begin
    FmVSRclArtPrpOld.ImgTipo.SQLType := SQLType;
    //
    FmVSRclArtPrpOld.ShowModal;
    Codigo    := FmVSRclArtPrpOld.FCodigo;
    CacCod    := FmVSRclArtPrpOld.FCacCod;
    MovimID   := FmVSRclArtPrpOld.FMovimID;
    //TpAreaRcl := FmVSRclArtPrpOld.RGTpAreaRcl.ItemIndex;
    FmVSRclArtPrpOld.Destroy;
    //
    if Codigo <> 0 then
      MostraFormVSReclassifOneOld(Codigo, CacCod, MovimID(*, TpAreaRcl*));
  end;
end;
}

procedure TUnVS_PF.MostraFormVSRclCab(Codigo, VSRclIts, VSReclas: Integer);
begin
  if DBCheck.CriaFm(TFmVSRclCab, FmVSRclCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSRclCab.LocCod(Codigo, Codigo);
      FmVSRclCab.QrVSRclIts.Locate('Controle', VSRclIts, []);
      if not FmVSRclCab.QrReclasif.Locate('Controle', VSreclas, []) then
        // tenta o item par
        FmVSRclCab.QrReclasif.Locate('Controle', VSreclas + 1, []);
    end;
    FmVSRclCab.ShowModal;
    FmVSRclCab.Destroy;
  end;
end;

{
procedure TUnVS_PF.MostraFormVSReclassifOneOld(Codigo, CacCod: Integer;
  MovimID: TEstqMovimID);
begin
  if DBCheck.CriaFm(TFmVSReclassifOneOld, FmVSReclassifOneOld, afmoNegarComAviso) then
  begin
    FmVSReclassifOneOld.FCodigo := Codigo;
    FmVSReclassifOneOld.FCacCod := CacCod;
    FmVSReclassifOneOld.FMovimID := MovimID;
    FmVSReclassifOneOld.ReopenVSPaRclCab();
    //
    FmVSReclassifOneOld.ShowModal;
    FmVSReclassifOneOld.Destroy;
  end;
end;
}

{
procedure TUnVS_PF.MostraFormVSReclassPrePal(SQLType: TSQLType; Pallet1, Pallet2,
              Pallet3, Pallet4, Pallet5, Pallet6, Digitador, Revisor: Integer);
var
  Pallet: Integer;
  Reclasse: Boolean;
begin
  if DBCheck.CriaFm(TFmVSReclassPrePal, FmVSReclassPrePal, afmoNegarComAviso) then
  begin
    FmVSReclassPrePal.ImgTipo.SQLType := SQLType;
    //
(*
    FmVSReclassPrePal.FPallet1     := Pallet1;
    FmVSReclassPrePal.FPallet2     := Pallet2;
    FmVSReclassPrePal.FPallet3     := Pallet3;
    FmVSReclassPrePal.FPallet4     := Pallet4;
    FmVSReclassPrePal.FPallet5     := Pallet5;
    FmVSReclassPrePal.FPallet6     := Pallet6;
    FmVSReclassPrePal.FDigitador   := Digitador;
    FmVSReclassPrePal.FRevisor     := Revisor;
    //
*)
    FmVSReclassPrePal.ShowModal;
    Pallet := FmVSReclassPrePal.FPallet;
    Reclasse := FmVSReclassPrePal.CkReclasse.Checked;
    FmVSReclassPrePal.Destroy;
    if Pallet <> 0 then
      MostraFormVSRclArtPrpNew(stIns, Pallet, Pallet1, Pallet2,
              Pallet3, Pallet4, Pallet5, Pallet6, Digitador, Revisor, Reclasse);
  end;
end;
}

procedure TUnVS_PF.MostraFormVSReqDiv(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSReqDiv, FmVSReqDiv, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSReqDiv.LocCod(Codigo, Codigo);
    FmVSReqDiv.ShowModal;
    FmVSReqDiv.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSReqMov(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSReqMov, FmVSReqMov, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSReqMov.LocCod(Codigo, Codigo);
    FmVSReqMov.ShowModal;
    FmVSReqMov.Destroy;
  end;
end;

(*
procedure TUnVS_PF.MostraFormVSReclassPrePalMul(SQLType: TSQLType; Pallet1,
  Pallet2, Pallet3, Pallet4, Pallet5, Pallet6, Digitador, Revisor: Integer);
begin
// copiar do MostraFormVSReclassPrePalCac ???
end;
*)

procedure TUnVS_PF.MostraFormVSRibCad(GraGruX: Integer; Edita: Boolean);
begin
  if DBCheck.CriaFm(TFmVSRibCad, FmVSRibCad, afmoNegarComAviso) then
  begin
    if GraGruX <> 0 then
    begin
      FmVSRibCad.LocCod(GraGruX, GraGruX);
      if FmVSRibCad.QrVSRibCadGraGruX.Value = GraGruX then
        FmVSRibCad.AlteraMP1Click(Self)
      else
        //Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
      begin
        VerSeCriaCadNaTabela(GraGruX, 'VSRibCad');
        FmVSRibCad.LocCod(GraGruX, GraGruX);
      end;
    end;
    if Edita then
      FmVSRibCad.FSeq := 1;
    FmVSRibCad.ShowModal;
    FmVSRibCad.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSRibCla(GraGruX: Integer; Edita: Boolean;
QryMul: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmVSRibCla, FmVSRibCla, afmoNegarComAviso) then
  begin
    FmVSRibCla.FQryMul := QryMul;
    if GraGruX <> 0 then
    begin
      FmVSRibCla.LocCod(GraGruX, GraGruX);
      if FmVSRibCla.QrVSRibClaGraGruX.Value = GraGruX then
        FmVSRibCla.AlteraArtigoClassificadoAtual1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSRibCla.FSeq := 1;
    FmVSRibCla.ShowModal;
    FmVSRibCla.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSRibOpe(GraGruX: Integer; Edita: Boolean);
begin
  if DBCheck.CriaFm(TFmVSRibOpe, FmVSRibOpe, afmoNegarComAviso) then
  begin
    if GraGruX <> 0 then
    begin
      FmVSRibOpe.LocCod(GraGruX, GraGruX);
      if FmVSRibOpe.QrVSRibOpeGraGruX.Value = GraGruX then
        FmVSRibOpe.AlteraArtigoClassificadoAtual1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSRibOpe.FSeq := 1;
    FmVSRibOpe.ShowModal;
    FmVSRibOpe.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSSubPrd(GraGruX: Integer; Edita: Boolean);
begin
  if DBCheck.CriaFm(TFmVSSubPrd, FmVSSubPrd, afmoNegarComAviso) then
  begin
    if GraGruX <> 0 then
    begin
      FmVSSubPrd.LocCod(GraGruX, GraGruX);
      if FmVSSubPrd.QrVSSubPrdGraGruX.Value = GraGruX then
        FmVSSubPrd.AlteraArtigoClassificadoAtual1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSSubPrd.FSeq := 1;
    FmVSSubPrd.ShowModal;
    FmVSSubPrd.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSSubPrdCad(MovimID, Codigo, Controle: Integer);
  procedure Mensagem();
  begin
    Geral.MB_Info('O tipo de movimento "' +
    sEstqMovimIDLong[Integer(TEstqMovimID.emidGeraSubProd)] +
    '" n�o possui janela geranci�vel de movimento!' + sLineBreak +
    'MovimID: ' + Geral.FF0(MovimID) + sLineBreak + 'C�digo: ' +
    Geral.FF0(Codigo) + sLineBreak + 'Controle: ' + Geral.FF0(Controle));
  end;
var
  Qry: TmySQLQuery;
  _MovimID, _Codigo, _Controle: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT GSPSrcNiv2 ',
    'FROM vsmovits ',
    'WHERE Controle=' + Geral.FF0(Controle),
    '']);
    //
    if Qry.Fields[0].AsInteger <> 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT MovimID, Codigo, Controle ',
      'FROM vsmovits ',
      'WHERE Controle=' + Geral.FF0(Qry.Fields[0].AsInteger),
      '']);
      //
      _MovimID  := Qry.FieldByName('MovimID').AsInteger;
      _Codigo   := Qry.FieldByName('Codigo').AsInteger;
      _Controle := Qry.FieldByName('Controle').AsInteger;
      //
      if _Controle <> 0 then
        VS_CRC_PF.MostraFormVS_XXX(_MovimID, _Codigo, _Controle)
      else
        Mensagem();
      //
    end else
      Mensagem();
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_PF.MostraFormVSInnSubPrdIts_Mul(SQLType: TSQLType;
  DataHora: TDateTime; QrCab, QrIts: TmySQLQuery; DsCab: TDataSource;
  GSPSrcMovID: TEstqMovimID; GSPSrcNiv2: Integer; GSPJmpMovID: TEstqMovimID;
  GSPJmpNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro, VSMulFrnCab, Ficha,
  SerieFch, Controle, MulOriMovimCod, TemIMEIMrt: Integer; UsaGSPJmp: Boolean;
  Marca: String);
const
  SQL_Limit = '';
var
  Qry: TmySQLQuery;
begin
  if SQLType = stIns then
  begin
    if DBCheck.CriaFm(TFmVSInnSubPrdItsMul, FmVSInnSubPrdItsMul, afmoNegarComAviso) then
    begin
      FmVSInnSubPrdItsMul.ImgTipo.SQLType := SQLType;
      //
      FmVSInnSubPrdItsMul.FQrCab      := QrCab;
      FmVSInnSubPrdItsMul.FQrIts      := QrIts;
      FmVSInnSubPrdItsMul.FDsCab      := DsCab;
      FmVSInnSubPrdItsMul.EdControle.ValueVariant   := Controle;
      FmVSInnSubPrdItsMul.FGSPSrcMovID := GSPSrcMovID;
      FmVSInnSubPrdItsMul.FGSPSrcNiv2  := GSPSrcNiv2;
      FmVSInnSubPrdItsMul.FUsaGSPJmp   := UsaGSPJmp;
      FmVSInnSubPrdItsMul.FGSPJmpMovID := GSPJmpMovID;
      FmVSInnSubPrdItsMul.FGSPJmpNiv2  := GSPJmpNiv2;
      FmVSInnSubPrdItsMul.FCodigo      := Codigo;
      FmVSInnSubPrdItsMul.FMovimCod    := MovimCod;
      FmVSInnSubPrdItsMul.FEmpresa     := Empresa;
      //FmVSInnSubPrdItsMul.FClientMO    := ClientMO;
      FmVSInnSubPrdItsMul.FTerceiro    := Terceiro;
      FmVSInnSubPrdItsMul.FVSMulFrnCab := VSMulFrnCab;
      //FmVSInnSubPrdItsMul.FDataHora    := DataHora;
      FmVSInnSubPrdItsMul.FFicha       := Ficha;
      FmVSInnSubPrdItsMul.FSerieFch    := SerieFch;
      //
      //FmVSInnSubPrdItsMul.EdCodigo.ValueVariant     := Codigo;
      //FmVSInnSubPrdItsMul.EdMovimCod.ValueVariant   := MovimCod;
      //FmVSInnSubPrdItsMul.EdEmpresa.ValueVariant    := Empresa;
      //
      FmVSInnSubPrdItsMul.EdGraGruX.ValueVariant    := 0;
      FmVSInnSubPrdItsMul.CBGraGruX.KeyValue        := 0;
      FmVSInnSubPrdItsMul.EdPecas.ValueVariant      := 0;
      FmVSInnSubPrdItsMul.EdAreaM2.ValueVariant     := 0;
      FmVSInnSubPrdItsMul.EdAreaP2.ValueVariant     := 0;
      FmVSInnSubPrdItsMul.EdPesoKg.ValueVariant     := 0;
      FmVSInnSubPrdItsMul.EdValorMP.ValueVariant    := 0;
      FmVSInnSubPrdItsMul.EdCustoMOKg.ValueVariant  := 0;
      FmVSInnSubPrdItsMul.EdCustoMOTot.ValueVariant := 0;
      FmVSInnSubPrdItsMul.EdFornecMO.ValueVariant   := 0;
      FmVSInnSubPrdItsMul.CBFornecMO.KeyValue       := Null;
      FmVSInnSubPrdItsMul.EdClientMO.ValueVariant   := 0;
      FmVSInnSubPrdItsMul.CBClientMO.KeyValue       := Null;
      FmVSInnSubPrdItsMul.EdValorT.ValueVariant     := 0;
      FmVSInnSubPrdItsMul.EdObserv.ValueVariant     := '';
      FmVSInnSubPrdItsMul.TPData.Date               := DataHora;
      FmVSInnSubPrdItsMul.EdHora.ValueVariant       := DataHora;
      //FmVSInnSubPrdItsMul.EdMarca.ValueVariant      := Marca;
      //
      VS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI(FmVSInnSubPrdItsMul.QrVSCalOriIMEI, MulOriMovimCod,
        Controle, TemIMEIMrt, eminSorcCal, SQL_Limit);
      //
      ReopenVSOpePrcOriIMEI_Sum(FmVSInnSubPrdItsMul.QrSum, MulOriMovimCod,
        TemIMEIMrt, eminSorcCal);
      //
      FmVSInnSubPrdItsMul.ShowModal;
      FmVSInnSubPrdItsMul.Destroy;
    end;
  end else
    Geral.MB_Erro('"InsUpd" n�o implementado!');
end;

procedure TUnVS_PF.MostraFormVSInnSubPrdIts_Uni(SQLType: TSQLType; DataHora: TDateTime;
  QrCab, QrIts: TmySQLQuery; DsCab: TDataSource; GSPSrcMovID: TEstqMovimID;
  GSPSrcNiv2, Codigo,
  MovimCod, Empresa, ClientMO, Terceiro, VSMulFrnCab, Ficha, SerieFch,
  Controle(*, OriGGX, OriSerieFch, OriFicha*): Integer; OriMarca: String);
var
  Qry: TmySQLQuery;
begin
  if DBCheck.CriaFm(TFmVSInnSubPrdIts, FmVSInnSubPrdIts, afmoNegarComAviso) then
  begin
    FmVSInnSubPrdIts.ImgTipo.SQLType := SQLType;
    //
    FmVSInnSubPrdIts.FQrCab      := QrCab;
    FmVSInnSubPrdIts.FQrIts      := QrIts;
    FmVSInnSubPrdIts.FDsCab      := DsCab;
    FmVSInnSubPrdIts.EdControle.ValueVariant   := Controle;
    //
    (*
    FmVSInnSubPrdIts.FOriGGX      := OriGGX;
    FmVSInnSubPrdIts.FOriSerieFch := OriSerieFch;
    FmVSInnSubPrdIts.FOriFicha    := OriFicha;
    *)
    //
    if SQLType = stUpd then
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM ' + CO_SEL_TAB_VMI,
        'WHERE Controle=' + Geral.FF0(Controle),
        '']);
        if Qry.RecordCount = 0 then
        begin
          FmVSInnSubPrdIts.Destroy;
          Geral.MB_Erro('ID Item ' + Geral.FF0(Controle) + ' n�o encontrado na tabela ativa!');
          Exit;
        end;
        FmVSInnSubPrdIts.FGSPSrcMovID := TEstqMovimID(Qry.FieldByName('GSPSrcMovID').AsInteger);
        FmVSInnSubPrdIts.FGSPSrcNiv2  := Qry.FieldByName('GSPSrcNiv2').AsInteger;
        FmVSInnSubPrdIts.FGSPJmpMovID := TEstqMovimID(Qry.FieldByName('GSPJmpMovID').AsInteger);
        FmVSInnSubPrdIts.FGSPJmpNiv2  := Qry.FieldByName('GSPJmpNiv2').AsInteger;
        FmVSInnSubPrdIts.FCodigo      := Qry.FieldByName('Codigo').AsInteger;
        FmVSInnSubPrdIts.FMovimCod    := Qry.FieldByName('MovimCod').AsInteger;
        FmVSInnSubPrdIts.FEmpresa     := Qry.FieldByName('Empresa').AsInteger;
        //FmVSInnSubPrdIts.FClientMO    := Qry.FieldByName('ClientMO').AsInteger;
        FmVSInnSubPrdIts.FTerceiro    := Qry.FieldByName('Terceiro').AsInteger;
        FmVSInnSubPrdIts.FVSMulFrnCab := Qry.FieldByName('VSMulFrnCab').AsInteger;
        //FmVSInnSubPrdIts.FDataHora    := Qry.FieldByName(CO_DATA_HORA_VMI).AsDateTime;
        FmVSInnSubPrdIts.FFicha       := Qry.FieldByName('Ficha').AsInteger;
        FmVSInnSubPrdIts.FSerieFch    := Qry.FieldByName('SerieFch').AsInteger;
        //
        FmVSInnSubPrdIts.EdCodigo.ValueVariant     := Qry.FieldByName('Codigo').AsInteger;
        FmVSInnSubPrdIts.EdMovimCod.ValueVariant   := Qry.FieldByName('MovimCod').AsInteger;
        FmVSInnSubPrdIts.EdEmpresa.ValueVariant    := Qry.FieldByName('Empresa').AsInteger;
        FmVSInnSubPrdIts.EdGSPSrcMovID.ValueVariant := Qry.FieldByName('GSPSrcMovID').AsInteger;
        FmVSInnSubPrdIts.EdGSPSrcNiv2.ValueVariant := Qry.FieldByName('GSPSrcNiv2').AsInteger;
        FmVSInnSubPrdIts.EdGSPJmpMovID.ValueVariant := Qry.FieldByName('GSPJmpMovID').AsInteger;
        FmVSInnSubPrdIts.EdGSPJmpNiv2.ValueVariant := Qry.FieldByName('GSPJmpNiv2').AsInteger;
        //
        FmVSInnSubPrdIts.EdGraGruX.ValueVariant    := Qry.FieldByName('GraGruX').AsInteger;
        FmVSInnSubPrdIts.CBGraGruX.KeyValue        := Qry.FieldByName('GraGruX').AsInteger;
        FmVSInnSubPrdIts.EdPecas.ValueVariant      := Qry.FieldByName('Pecas').AsFloat;
        FmVSInnSubPrdIts.EdAreaM2.ValueVariant     := Qry.FieldByName('AreaM2').AsFloat;
        FmVSInnSubPrdIts.EdAreaP2.ValueVariant     := Qry.FieldByName('AreaP2').AsFloat;
        FmVSInnSubPrdIts.EdPesoKg.ValueVariant     := Qry.FieldByName('PesoKg').AsFloat;
        FmVSInnSubPrdIts.EdValorMP.ValueVariant    := Qry.FieldByName('ValorMP').AsFloat;
        FmVSInnSubPrdIts.EdCustoMOKg.ValueVariant  := Qry.FieldByName('CustoMOKg').AsFloat;
        FmVSInnSubPrdIts.EdCustoMOTot.ValueVariant := Qry.FieldByName('CustoMOTot').AsFloat;
        FmVSInnSubPrdIts.EdFornecMO.ValueVariant   := Qry.FieldByName('FornecMO').AsInteger;
        FmVSInnSubPrdIts.CBFornecMO.KeyValue       := Qry.FieldByName('FornecMO').AsInteger;
        FmVSInnSubPrdIts.EdClientMO.ValueVariant   := Qry.FieldByName('ClientMO').AsInteger;
        FmVSInnSubPrdIts.CBClientMO.KeyValue       := Qry.FieldByName('ClientMO').AsInteger;
        FmVSInnSubPrdIts.EdValorT.ValueVariant     := Qry.FieldByName('ValorT').AsFloat;
        FmVSInnSubPrdIts.EdMarca.ValueVariant      := Qry.FieldByName('Marca').AsString;
        FmVSInnSubPrdIts.EdObserv.ValueVariant     := Qry.FieldByName('Observ').AsString;
        FmVSInnSubPrdIts.EdStqCenLoc.ValueVariant  := Qry.FieldByName('StqCenLoc').AsString;
        FmVSInnSubPrdIts.CBStqCenLoc.KeyValue      := Qry.FieldByName('StqCenLoc').AsString;
        FmVSInnSubPrdIts.EdReqMovEstq.ValueVariant := Qry.FieldByName('ReqMovEstq').AsString;
        FmVSInnSubPrdIts.TPData.Date               := Qry.FieldByName(CO_DATA_HORA_VMI).AsDateTime;
        FmVSInnSubPrdIts.EdHora.ValueVariant       := Qry.FieldByName(CO_DATA_HORA_VMI).AsDateTime;
      finally
        Qry.Free;
      end;
    end else
    begin
      FmVSInnSubPrdIts.FGSPSrcMovID  := GSPSrcMovID;
      FmVSInnSubPrdIts.FGSPSrcNiv2  := GSPSrcNiv2;
      FmVSInnSubPrdIts.FCodigo      := Codigo;
      FmVSInnSubPrdIts.FMovimCod    := MovimCod;
      FmVSInnSubPrdIts.FEmpresa     := Empresa;
      //FmVSInnSubPrdIts.FClientMO    := ClientMO;
      FmVSInnSubPrdIts.FTerceiro    := Terceiro;
      FmVSInnSubPrdIts.FVSMulFrnCab := VSMulFrnCab;
      //FmVSInnSubPrdIts.FDataHora    := DataHora;
      FmVSInnSubPrdIts.FFicha       := Ficha;
      FmVSInnSubPrdIts.FSerieFch    := SerieFch;
      //FmVSInnSubPrdIts.FOriMarca    := OriMarca;
      //
      FmVSInnSubPrdIts.EdCodigo.ValueVariant     := Codigo;
      FmVSInnSubPrdIts.EdMovimCod.ValueVariant   := MovimCod;
      FmVSInnSubPrdIts.EdEmpresa.ValueVariant    := Empresa;
      FmVSInnSubPrdIts.EdGSPSrcMovID.ValueVariant := GSPSrcMovID;
      FmVSInnSubPrdIts.EdGSPSrcNiv2.ValueVariant := GSPSrcNiv2;
      //
      FmVSInnSubPrdIts.EdGraGruX.ValueVariant    := 0;
      FmVSInnSubPrdIts.CBGraGruX.KeyValue        := 0;
      FmVSInnSubPrdIts.EdPecas.ValueVariant      := 0;
      FmVSInnSubPrdIts.EdAreaM2.ValueVariant     := 0;
      FmVSInnSubPrdIts.EdAreaP2.ValueVariant     := 0;
      FmVSInnSubPrdIts.EdPesoKg.ValueVariant     := 0;
      FmVSInnSubPrdIts.EdValorMP.ValueVariant    := 0;
      FmVSInnSubPrdIts.EdCustoMOKg.ValueVariant  := 0;
      FmVSInnSubPrdIts.EdCustoMOTot.ValueVariant := 0;
      FmVSInnSubPrdIts.EdFornecMO.ValueVariant   := 0;
      FmVSInnSubPrdIts.CBFornecMO.KeyValue       := Null;
      FmVSInnSubPrdIts.EdClientMO.ValueVariant   := 0;
      FmVSInnSubPrdIts.CBClientMO.KeyValue       := Null;
      FmVSInnSubPrdIts.EdValorT.ValueVariant     := 0;
      FmVSInnSubPrdIts.EdMarca.ValueVariant      := OriMarca; // 2023-12-03
      FmVSInnSubPrdIts.EdObserv.ValueVariant     := '';
      FmVSInnSubPrdIts.TPData.Date               := DataHora;
      FmVSInnSubPrdIts.EdHora.ValueVariant       := DataHora;
    end;
    //
    FmVSInnSubPrdIts.ShowModal;
    FmVSInnSubPrdIts.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSCalSubPrdIts_Mul(SQLType: TSQLType;
  DataHora: TDateTime; QrCab, QrIts: TmySQLQuery; DsCab: TDataSource;
  GSPSrcMovID: TEstqMovimID; GSPSrcNiv2: Integer; GSPJmpMovID: TEstqMovimID;
  GSPJmpNiv2, Codigo, MovimCod, Empresa, ClientMO, Terceiro, VSMulFrnCab, Ficha,
  SerieFch, Controle, MulOriMovimCod, TemIMEIMrt: Integer; UsaGSPJmp: Boolean;
  Marca: String; CouNiv2EmProc: Integer);
const
  SQL_Limit = '';
var
  Qry: TmySQLQuery;
begin
  if SQLType = stIns then
  begin
    if DBCheck.CriaFm(TFmVSCalSubPrdItsMul, FmVSCalSubPrdItsMul, afmoNegarComAviso) then
    begin
      FmVSCalSubPrdItsMul.ImgTipo.SQLType := SQLType;
      //
      FmVSCalSubPrdItsMul.FQrCab      := QrCab;
      FmVSCalSubPrdItsMul.FQrIts      := QrIts;
      FmVSCalSubPrdItsMul.FDsCab      := DsCab;
      FmVSCalSubPrdItsMul.FCouNiv2EmProc  := CouNiv2EmProc;
      FmVSCalSubPrdItsMul.EdControle.ValueVariant   := Controle;
      FmVSCalSubPrdItsMul.FGSPSrcMovID := GSPSrcMovID;
      FmVSCalSubPrdItsMul.FGSPSrcNiv2  := GSPSrcNiv2;
      FmVSCalSubPrdItsMul.FUsaGSPJmp   := UsaGSPJmp;
      FmVSCalSubPrdItsMul.FGSPJmpMovID := GSPJmpMovID;
      FmVSCalSubPrdItsMul.FGSPJmpNiv2  := GSPJmpNiv2;
      FmVSCalSubPrdItsMul.FCodigo      := Codigo;
      FmVSCalSubPrdItsMul.FMovimCod    := MovimCod;
      FmVSCalSubPrdItsMul.FEmpresa     := Empresa;
      //FmVSCalSubPrdItsMul.FClientMO    := ClientMO;
      FmVSCalSubPrdItsMul.FTerceiro    := Terceiro;
      FmVSCalSubPrdItsMul.FVSMulFrnCab := VSMulFrnCab;
      //FmVSCalSubPrdItsMul.FDataHora    := DataHora;
      FmVSCalSubPrdItsMul.FFicha       := Ficha;
      FmVSCalSubPrdItsMul.FSerieFch    := SerieFch;
      //
      //FmVSCalSubPrdItsMul.EdCodigo.ValueVariant     := Codigo;
      //FmVSCalSubPrdItsMul.EdMovimCod.ValueVariant   := MovimCod;
      //FmVSCalSubPrdItsMul.EdEmpresa.ValueVariant    := Empresa;
      //
      FmVSCalSubPrdItsMul.EdGraGruX.ValueVariant    := 0;
      FmVSCalSubPrdItsMul.CBGraGruX.KeyValue        := 0;
      FmVSCalSubPrdItsMul.EdPecas.ValueVariant      := 0;
      FmVSCalSubPrdItsMul.EdAreaM2.ValueVariant     := 0;
      FmVSCalSubPrdItsMul.EdAreaP2.ValueVariant     := 0;
      FmVSCalSubPrdItsMul.EdPesoKg.ValueVariant     := 0;
      FmVSCalSubPrdItsMul.EdValorMP.ValueVariant    := 0;
      FmVSCalSubPrdItsMul.EdCustoMOKg.ValueVariant  := 0;
      FmVSCalSubPrdItsMul.EdCustoMOTot.ValueVariant := 0;
      FmVSCalSubPrdItsMul.EdFornecMO.ValueVariant   := 0;
      FmVSCalSubPrdItsMul.CBFornecMO.KeyValue       := Null;
      FmVSCalSubPrdItsMul.EdClientMO.ValueVariant   := 0;
      FmVSCalSubPrdItsMul.CBClientMO.KeyValue       := Null;
      FmVSCalSubPrdItsMul.EdValorT.ValueVariant     := 0;
      FmVSCalSubPrdItsMul.EdObserv.ValueVariant     := '';
      FmVSCalSubPrdItsMul.TPData.Date               := DataHora;
      FmVSCalSubPrdItsMul.EdHora.ValueVariant       := DataHora;
      //FmVSCalSubPrdItsMul.EdMarca.ValueVariant      := Marca;
      //
      VS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI(FmVSCalSubPrdItsMul.QrVSCalOriIMEI, MulOriMovimCod,
        Controle, TemIMEIMrt, eminSorcCal, SQL_Limit);
      //
      ReopenVSOpePrcOriIMEI_Sum(FmVSCalSubPrdItsMul.QrSum, MulOriMovimCod,
        TemIMEIMrt, eminSorcCal);
      //
      FmVSCalSubPrdItsMul.ShowModal;
      FmVSCalSubPrdItsMul.Destroy;
    end;
  end else
    Geral.MB_Erro('"InsUpd" n�o implementado!');
end;

procedure TUnVS_PF.MostraFormVSCalSubPrdIts_Uni(SQLType: TSQLType; DataHora: TDateTime;
  QrCab, QrIts: TmySQLQuery; DsCab: TDataSource; GSPSrcMovID: TEstqMovimID;
  GSPSrcNiv2, Codigo,
  MovimCod, Empresa, ClientMO, Terceiro, VSMulFrnCab, Ficha, SerieFch,
  Controle(*, OriGGX, OriSerieFch, OriFicha*): Integer; OriMarca: String;
  CouNiv2EmProc: Integer);
var
  Qry: TmySQLQuery;
begin
  if DBCheck.CriaFm(TFmVSCalSubPrdIts, FmVSCalSubPrdIts, afmoNegarComAviso) then
  begin
    FmVSCalSubPrdIts.ImgTipo.SQLType := SQLType;
    //
    FmVSCalSubPrdIts.FQrCab      := QrCab;
    FmVSCalSubPrdIts.FQrIts      := QrIts;
    FmVSCalSubPrdIts.FDsCab      := DsCab;
    FmVSCalSubPrdIts.FCouNiv2EmProc  := CouNiv2EmProc;
    FmVSCalSubPrdIts.EdControle.ValueVariant   := Controle;
    //
    (*
    FmVSCalSubPrdIts.FOriGGX      := OriGGX;
    FmVSCalSubPrdIts.FOriSerieFch := OriSerieFch;
    FmVSCalSubPrdIts.FOriFicha    := OriFicha;
    *)
    //
    if SQLType = stUpd then
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM ' + CO_SEL_TAB_VMI,
        'WHERE Controle=' + Geral.FF0(Controle),
        '']);
        if Qry.RecordCount = 0 then
        begin
          FmVSCalSubPrdIts.Destroy;
          Geral.MB_Erro('ID Item ' + Geral.FF0(Controle) + ' n�o encontrado na tabela ativa!');
          Exit;
        end;
        FmVSCalSubPrdIts.FGSPSrcMovID := TEstqMovimID(Qry.FieldByName('GSPSrcMovID').AsInteger);
        FmVSCalSubPrdIts.FGSPSrcNiv2  := Qry.FieldByName('GSPSrcNiv2').AsInteger;
        FmVSCalSubPrdIts.FGSPJmpMovID := TEstqMovimID(Qry.FieldByName('GSPJmpMovID').AsInteger);
        FmVSCalSubPrdIts.FGSPJmpNiv2  := Qry.FieldByName('GSPJmpNiv2').AsInteger;
        FmVSCalSubPrdIts.FCodigo      := Qry.FieldByName('Codigo').AsInteger;
        FmVSCalSubPrdIts.FMovimCod    := Qry.FieldByName('MovimCod').AsInteger;
        FmVSCalSubPrdIts.FEmpresa     := Qry.FieldByName('Empresa').AsInteger;
        //FmVSCalSubPrdIts.FClientMO    := Qry.FieldByName('ClientMO').AsInteger;
        FmVSCalSubPrdIts.FTerceiro    := Qry.FieldByName('Terceiro').AsInteger;
        FmVSCalSubPrdIts.FVSMulFrnCab := Qry.FieldByName('VSMulFrnCab').AsInteger;
        //FmVSCalSubPrdIts.FDataHora    := Qry.FieldByName(CO_DATA_HORA_VMI).AsDateTime;
        FmVSCalSubPrdIts.FFicha       := Qry.FieldByName('Ficha').AsInteger;
        FmVSCalSubPrdIts.FSerieFch    := Qry.FieldByName('SerieFch').AsInteger;
        //
        FmVSCalSubPrdIts.EdCodigo.ValueVariant     := Qry.FieldByName('Codigo').AsInteger;
        FmVSCalSubPrdIts.EdMovimCod.ValueVariant   := Qry.FieldByName('MovimCod').AsInteger;
        FmVSCalSubPrdIts.EdEmpresa.ValueVariant    := Qry.FieldByName('Empresa').AsInteger;
        FmVSCalSubPrdIts.EdGSPSrcMovID.ValueVariant := Qry.FieldByName('GSPSrcMovID').AsInteger;
        FmVSCalSubPrdIts.EdGSPSrcNiv2.ValueVariant := Qry.FieldByName('GSPSrcNiv2').AsInteger;
        FmVSCalSubPrdIts.EdGSPJmpMovID.ValueVariant := Qry.FieldByName('GSPJmpMovID').AsInteger;
        FmVSCalSubPrdIts.EdGSPJmpNiv2.ValueVariant := Qry.FieldByName('GSPJmpNiv2').AsInteger;
        //
        FmVSCalSubPrdIts.EdGraGruX.ValueVariant    := Qry.FieldByName('GraGruX').AsInteger;
        FmVSCalSubPrdIts.CBGraGruX.KeyValue        := Qry.FieldByName('GraGruX').AsInteger;
        FmVSCalSubPrdIts.EdPecas.ValueVariant      := Qry.FieldByName('Pecas').AsFloat;
        FmVSCalSubPrdIts.EdAreaM2.ValueVariant     := Qry.FieldByName('AreaM2').AsFloat;
        FmVSCalSubPrdIts.EdAreaP2.ValueVariant     := Qry.FieldByName('AreaP2').AsFloat;
        FmVSCalSubPrdIts.EdPesoKg.ValueVariant     := Qry.FieldByName('PesoKg').AsFloat;
        FmVSCalSubPrdIts.EdValorMP.ValueVariant    := Qry.FieldByName('ValorMP').AsFloat;
        FmVSCalSubPrdIts.EdCustoMOKg.ValueVariant  := Qry.FieldByName('CustoMOKg').AsFloat;
        FmVSCalSubPrdIts.EdCustoMOTot.ValueVariant := Qry.FieldByName('CustoMOTot').AsFloat;
        FmVSCalSubPrdIts.EdFornecMO.ValueVariant   := Qry.FieldByName('FornecMO').AsInteger;
        FmVSCalSubPrdIts.CBFornecMO.KeyValue       := Qry.FieldByName('FornecMO').AsInteger;
        FmVSCalSubPrdIts.EdClientMO.ValueVariant   := Qry.FieldByName('ClientMO').AsInteger;
        FmVSCalSubPrdIts.CBClientMO.KeyValue       := Qry.FieldByName('ClientMO').AsInteger;
        FmVSCalSubPrdIts.EdValorT.ValueVariant     := Qry.FieldByName('ValorT').AsFloat;
        FmVSCalSubPrdIts.EdMarca.ValueVariant      := Qry.FieldByName('Marca').AsString;
        FmVSCalSubPrdIts.EdObserv.ValueVariant     := Qry.FieldByName('Observ').AsString;
        FmVSCalSubPrdIts.EdStqCenLoc.ValueVariant  := Qry.FieldByName('StqCenLoc').AsString;
        FmVSCalSubPrdIts.CBStqCenLoc.KeyValue      := Qry.FieldByName('StqCenLoc').AsString;
        FmVSCalSubPrdIts.EdReqMovEstq.ValueVariant := Qry.FieldByName('ReqMovEstq').AsString;
        FmVSCalSubPrdIts.TPData.Date               := Qry.FieldByName(CO_DATA_HORA_VMI).AsDateTime;
        FmVSCalSubPrdIts.EdHora.ValueVariant       := Qry.FieldByName(CO_DATA_HORA_VMI).AsDateTime;
      finally
        Qry.Free;
      end;
    end else
    begin
      FmVSCalSubPrdIts.FGSPSrcMovID  := GSPSrcMovID;
      FmVSCalSubPrdIts.FGSPSrcNiv2  := GSPSrcNiv2;
      FmVSCalSubPrdIts.FCodigo      := Codigo;
      FmVSCalSubPrdIts.FMovimCod    := MovimCod;
      FmVSCalSubPrdIts.FEmpresa     := Empresa;
      //FmVSCalSubPrdIts.FClientMO    := ClientMO;
      FmVSCalSubPrdIts.FTerceiro    := Terceiro;
      FmVSCalSubPrdIts.FVSMulFrnCab := VSMulFrnCab;
      //FmVSCalSubPrdIts.FDataHora    := DataHora;
      FmVSCalSubPrdIts.FFicha       := Ficha;
      FmVSCalSubPrdIts.FSerieFch    := SerieFch;
      //FmVSCalSubPrdIts.FOriMarca    := OriMarca;
      //
      FmVSCalSubPrdIts.EdCodigo.ValueVariant     := Codigo;
      FmVSCalSubPrdIts.EdMovimCod.ValueVariant   := MovimCod;
      FmVSCalSubPrdIts.EdEmpresa.ValueVariant    := Empresa;
      FmVSCalSubPrdIts.EdGSPSrcMovID.ValueVariant := GSPSrcMovID;
      FmVSCalSubPrdIts.EdGSPSrcNiv2.ValueVariant := GSPSrcNiv2;
      //
      FmVSCalSubPrdIts.EdGraGruX.ValueVariant    := 0;
      FmVSCalSubPrdIts.CBGraGruX.KeyValue        := 0;
      FmVSCalSubPrdIts.EdPecas.ValueVariant      := 0;
      FmVSCalSubPrdIts.EdAreaM2.ValueVariant     := 0;
      FmVSCalSubPrdIts.EdAreaP2.ValueVariant     := 0;
      FmVSCalSubPrdIts.EdPesoKg.ValueVariant     := 0;
      FmVSCalSubPrdIts.EdValorMP.ValueVariant    := 0;
      FmVSCalSubPrdIts.EdCustoMOKg.ValueVariant  := 0;
      FmVSCalSubPrdIts.EdCustoMOTot.ValueVariant := 0;
      FmVSCalSubPrdIts.EdFornecMO.ValueVariant   := 0;
      FmVSCalSubPrdIts.CBFornecMO.KeyValue       := Null;
      FmVSCalSubPrdIts.EdClientMO.ValueVariant   := 0;
      FmVSCalSubPrdIts.CBClientMO.KeyValue       := Null;
      FmVSCalSubPrdIts.EdValorT.ValueVariant     := 0;
      FmVSCalSubPrdIts.EdMarca.ValueVariant      := OriMarca; // 2023-12-03
      FmVSCalSubPrdIts.EdObserv.ValueVariant     := '';
      FmVSCalSubPrdIts.TPData.Date               := DataHora;
      FmVSCalSubPrdIts.EdHora.ValueVariant       := DataHora;
    end;
    //
    FmVSCalSubPrdIts.ShowModal;
    FmVSCalSubPrdIts.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSPMOCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSPMOCab, FmVSPMOCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSPMOCab.LocCod(Codigo, Codigo);
    end;
    FmVSPMOCab.ShowModal;
    FmVSPMOCab.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSWetEnd(GraGruX: Integer; Edita: Boolean;
QryMul: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmVSWetEnd, FmVSWetEnd, afmoNegarComAviso) then
  begin
    //FmVSWetEnd.PnNavi.Enabled := False;
    //FmVSWetEnd.GB_L.Enabled := False;
    FmVSWetEnd.FQryMul := QryMul;
    if GraGruX <> 0 then
    begin
      FmVSWetEnd.LocCod(GraGruX, GraGruX);
      if FmVSWetEnd.QrVSWetEndGraGruX.Value = GraGruX then
        FmVSWetEnd.AlteraArtigoClassificadoAtual1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSWetEnd.FSeq := 1;
    FmVSWetEnd.ShowModal;
    FmVSWetEnd.Destroy;
  end;
end;

procedure TUnVS_PF.MostraRelatorioCourosSemParteMaterial();
begin
  if DBCheck.CriaFm(TFmVSImpMatNoDef, FmVSImpMatNoDef, afmoNegarComAviso) then
  begin
    FmVSImpMatNoDef.ImprimeRelatorio();
    //FmVSImpMatNoDef.ShowModal;
    FmVSImpMatNoDef.Destroy;
  end;
end;

(*
procedure TUnVS_PF.MostraRelatorioVSImpCompraVenda(TP12DataIni_Date,
  TP12DataFim_Date: TDateTime; Ck12DataIni_Checked, Ck12DataFim_Checked:
  Boolean; CBEmpresa_Text: String; EdEmpresa_ValueVariant: Variant);
begin
  if DBCheck.CriaFm(TFmVSImpCompraVenda, FmVSImpCompraVenda, afmoNegarComAviso) then
  begin
    FmVSImpCompraVenda.TP12DataIni_Date       := TP12DataIni_Date;
    FmVSImpCompraVenda.TP12DataFim_Date       := TP12DataFim_Date;
    FmVSImpCompraVenda.Ck12DataIni_Checked    := Ck12DataIni_Checked;
    FmVSImpCompraVenda.Ck12DataFim_Checked    := Ck12DataFim_Checked;
    FmVSImpCompraVenda.CBEmpresa_Text         := CBEmpresa_Text;
    FmVSImpCompraVenda.EdEmpresa_ValueVariant := EdEmpresa_ValueVariant;

    FmVSImpCompraVenda.ImprimeMovimento();
    //FmVSImpCompraVenda.ShowModal;
    FmVSImpCompraVenda.Destroy;
  end;
end;
*)

procedure TUnVS_PF.MostraRelatorioVSImpCompraVenda2();
begin
  if DBCheck.CriaFm(TFmVSImpCompraVenda2, FmVSImpCompraVenda2, afmoNegarComAviso) then
  begin
    FmVSImpCompraVenda2.ShowModal;
    FmVSImpCompraVenda2.Destroy;
  end;
end;

procedure TUnVS_PF.MostraRelatorioVSImpFluxo(TP13DataIni_Date,
  TP13DataFim_Date: TDateTime; Ck13DataIni_Checked, Ck13DataFim_Checked,
  Ck13DataIni_Visible, Ck13DataIni_Enabled: Boolean; Ed13_Pallet_ValueVariant,
  Ed13_IMEI_ValueVariant, Ed13GraGruX_ValueVariant, EdEmpresa_ValueVariant,
  Ed13_MaxInteiros_ValueVariant: Variant; RG13TipoMov_ItemIndex,
  RG13_AnaliSinte_ItemIndex, RG13_GragruY_ItemIndex: Integer;
  FRG13_AnaliSinte: TRadioGroup; FPB1: TProgressBar; FLaAviso1,
  FLaAviso2: TLabel; FBtImprime: TBitBtn; CBEmpresa_Text: String;
  Ed13SerieFch_ValueVariant, Ed13Ficha_ValueVariant: Integer);
begin
  if DBCheck.CriaFm(TFmVSImpFluxo, FmVSImpFluxo, afmoNegarComAviso) then
  begin
    FmVSImpFluxo.TP13DataIni_Date              := TP13DataIni_Date;
    FmVSImpFluxo.TP13DataFim_Date              := TP13DataFim_Date;
    FmVSImpFluxo.Ck13DataIni_Checked           := Ck13DataIni_Checked;
    FmVSImpFluxo.Ck13DataFim_Checked           := Ck13DataFim_Checked;
    FmVSImpFluxo.Ck13DataIni_Visible           := Ck13DataIni_Visible;
    FmVSImpFluxo.Ck13DataIni_Enabled           := Ck13DataIni_Enabled;
    FmVSImpFluxo.Ed13_Pallet_ValueVariant      := Ed13_Pallet_ValueVariant;
    FmVSImpFluxo.Ed13_IMEI_ValueVariant        := Ed13_IMEI_ValueVariant;
    FmVSImpFluxo.Ed13GraGruX_ValueVariant      := Ed13GraGruX_ValueVariant;
    FmVSImpFluxo.EdEmpresa_ValueVariant        := EdEmpresa_ValueVariant;
    FmVSImpFluxo.Ed13_MaxInteiros.ValueVariant := Ed13_MaxInteiros_ValueVariant;
    FmVSImpFluxo.RG13TipoMov_ItemIndex         := RG13TipoMov_ItemIndex;
    FmVSImpFluxo.RG13_AnaliSinte_ItemIndex     := RG13_AnaliSinte_ItemIndex;
    FmVSImpFluxo.RG13_GragruY_ItemIndex        := RG13_GragruY_ItemIndex;
    FmVSImpFluxo.FRG13_AnaliSinte              := FRG13_AnaliSinte;
    FmVSImpFluxo.FPB1                          := FPB1;
    FmVSImpFluxo.FLaAviso1                     := FLaAviso1;
    FmVSImpFluxo.FLaAviso2                     := FLaAviso2;
    FmVSImpFluxo.FBtImprime                    := FBtImprime;
    FmVSImpFluxo.CBEmpresa_Text                := CBEmpresa_Text;
    FmVSImpFluxo.Ed13SerieFch_ValueVariant     := Ed13SerieFch_ValueVariant;
    FmVSImpFluxo.Ed13Ficha_ValueVariant        := Ed13Ficha_ValueVariant;
    //
    FmVSImpFluxo.PesquisaFluxo();
    //FmVSImpFluxo.ShowModal;
    FmVSImpFluxo.Destroy;
  end;
end;

procedure TUnVS_PF.MostraRelatorioVSImpHistorico(GraGruX, Pallet,
  Terceiro: Integer; TP01DataIni_Date,
  TP01DataFim_Date, TPDataRelativa_Date: TDateTime; Ck01DataIni_Checked,
  Ck01DataFim_Checked: Boolean; CBEmpresa_Text: String;
  EdEmpresa: TdmkEditCB;
  //CB01GraGruX_Text, CB01Pallet_Text, CB01Terceiro_Text: String;
  PB1: TProgressBar; LaAviso1, LaAviso2: TLabel);
begin
  if DBCheck.CriaFm(TFmVSImpHistorico2, FmVSImpHistorico2, afmoNegarComAviso) then
  begin
    //FmVSImpHistorico2.Ed01GraGruX                   := Ed01GraGruX;
    //FmVSImpHistorico2.Ed01Pallet                    := Ed01Pallet;
    //FmVSImpHistorico2.Ed01Terceiro                  := Ed01Terceiro;
    FmVSImpHistorico2.TP01DataIni.Date              := TP01DataIni_Date;
    FmVSImpHistorico2.TP01DataFim.Date              := TP01DataFim_Date;
    FmVSImpHistorico2.Ck01DataIni.Checked           := Ck01DataIni_Checked;
    FmVSImpHistorico2.Ck01DataFim.Checked           := Ck01DataFim_Checked;
    FmVSImpHistorico2.EdEmpresa                     := EdEmpresa;
    FmVSImpHistorico2.CBEmpresa_Text                := CBEmpresa_Text;
    //
    FmVSImpHistorico2.TPDataRelativa_Date           := TPDataRelativa_Date;
    FmVSImpHistorico2.Ed01GraGruX.ValueVariant      := GraGruX;
    FmVSImpHistorico2.CB01GraGruX.KeyValue          := GraGruX;
    FmVSImpHistorico2.Ed01Pallet.ValueVariant       := Pallet;
    FmVSImpHistorico2.CB01Pallet.KeyValue           := Pallet;
    FmVSImpHistorico2.Ed01Terceiro.ValueVariant     := Terceiro;
    FmVSImpHistorico2.CB01Terceiro.KeyValue         := Terceiro;
    FmVSImpHistorico2.PB1                           := PB1;
    FmVSImpHistorico2.LaAviso1                      := LaAviso1;
    FmVSImpHistorico2.LaAviso2                      := LaAviso2;
    //
    //FmVSImpHistorico2.ImprimeHistorico();
    FmVSImpHistorico2.ShowModal;
    FmVSImpHistorico2.Destroy;
  end;
end;

procedure TUnVS_PF.MostraRelatoroFluxoFichaRMP(SerieFch, Ficha: Integer);
const
  TP13DataIni_Date = 0;
  TP13DataFim_Date = 0;
  Ck13DataIni_Checked = False;
  Ck13DataFim_Checked = False;
  Ck13DataIni_Visible = False;
  Ck13DataIni_Enabled = False;
  Ed13_Pallet_ValueVariant = 0;
  Ed13GraGruX_ValueVariant = 0;
  Ed13_MaxInteiros_ValueVariant = 999999999;
  RG13TipoMov_ItemIndex = 0;
  RG13_AnaliSinte_ItemIndex = 1;
  RG13_GragruY_ItemIndex = 1;
  RG13_AnaliSinte = nil;
  PB1 = nil;
  LaAviso1 = nil;
  LaAviso2 = nil;
  BtImprime = nil;
  Ed13_IMEI_ValueVariant = 0;
var
  EdEmpresa_ValueVariant, Ed13SerieFch_ValueVariant, Ed13Ficha_ValueVariant,
  Controle: Integer;
  CBEmpresa_Text: String;
begin
  ObtemIMEIdeSerieEFichaRMP(SerieFch, Ficha, Controle);
  ObtemCodENomeEmpresaDeIMEI(Controle,
    EdEmpresa_ValueVariant, CBEmpresa_Text);
  //
  Ed13SerieFch_ValueVariant := SerieFch;
  Ed13Ficha_ValueVariant    := Ficha;
  //
  MostraRelatorioVSImpFluxo(TP13DataIni_Date, TP13DataFim_Date,
  Ck13DataIni_Checked, Ck13DataFim_Checked, Ck13DataIni_Visible,
  Ck13DataIni_Enabled, Ed13_Pallet_ValueVariant,
  Ed13_IMEI_ValueVariant, Ed13GraGruX_ValueVariant, EdEmpresa_ValueVariant,
  Ed13_MaxInteiros_ValueVariant, RG13TipoMov_ItemIndex,
  RG13_AnaliSinte_ItemIndex, RG13_GragruY_ItemIndex,
  RG13_AnaliSinte, PB1, LaAviso1, LaAviso2, BtImprime, CBEmpresa_Text,
  Ed13SerieFch_ValueVariant, Ed13Ficha_ValueVariant);
end;

procedure TUnVS_PF.MostraRelatoroFluxoIMEI(Controle: Integer);
const
  TP13DataIni_Date = 0;
  TP13DataFim_Date = 0;
  Ck13DataIni_Checked = False;
  Ck13DataFim_Checked = False;
  Ck13DataIni_Visible = False;
  Ck13DataIni_Enabled = False;
  Ed13_Pallet_ValueVariant = 0;
  Ed13GraGruX_ValueVariant = 0;
  Ed13_MaxInteiros_ValueVariant = 999999999;
  RG13TipoMov_ItemIndex = 0;
  RG13_AnaliSinte_ItemIndex = 1;
  RG13_GragruY_ItemIndex = 2;
  RG13_AnaliSinte = nil;
  PB1 = nil;
  LaAviso1 = nil;
  LaAviso2 = nil;
  BtImprime = nil;
  Ed13SerieFch_ValueVariant = 0;
  Ed13Ficha_ValueVariant = 0;
var
  Ed13_IMEI_ValueVariant, EdEmpresa_ValueVariant: Integer;
  CBEmpresa_Text: String;
begin
  ObtemCodENomeEmpresaDeIMEI(Controle,
    EdEmpresa_ValueVariant, CBEmpresa_Text);
  //
  Ed13_IMEI_ValueVariant := Controle;
  //
  MostraRelatorioVSImpFluxo(TP13DataIni_Date, TP13DataFim_Date,
  Ck13DataIni_Checked, Ck13DataFim_Checked, Ck13DataIni_Visible,
  Ck13DataIni_Enabled, Ed13_Pallet_ValueVariant,
  Ed13_IMEI_ValueVariant, Ed13GraGruX_ValueVariant, EdEmpresa_ValueVariant,
  Ed13_MaxInteiros_ValueVariant, RG13TipoMov_ItemIndex,
  RG13_AnaliSinte_ItemIndex, RG13_GragruY_ItemIndex,
  RG13_AnaliSinte, PB1, LaAviso1, LaAviso2, BtImprime, CBEmpresa_Text,
  Ed13SerieFch_ValueVariant, Ed13Ficha_ValueVariant);
end;

procedure TUnVS_PF.MostraRelatoroFluxoPallet(Pallet, Empresa: Integer;
  NO_EMP: String);
const
  TP13DataIni_Date = 0;
  TP13DataFim_Date = 0;
  Ck13DataIni_Checked = False;
  Ck13DataFim_Checked = False;
  Ck13DataIni_Visible = False;
  Ck13DataIni_Enabled = False;
  Ed13_IMEI_ValueVariant = 0;
  Ed13GraGruX_ValueVariant = 0;
  Ed13_MaxInteiros_ValueVariant = 999999999;
  RG13TipoMov_ItemIndex = 0;
  RG13_AnaliSinte_ItemIndex = 1;
  RG13_GragruY_ItemIndex = 3;
  RG13_AnaliSinte = nil;
  PB1 = nil;
  LaAviso1 = nil;
  LaAviso2 = nil;
  BtImprime = nil;
  Ed13SerieFch_ValueVariant = 0;
  Ed13Ficha_ValueVariant = 0;
var
  Ed13_Pallet_ValueVariant, EdEmpresa_ValueVariant: Integer;
  CBEmpresa_Text: String;
begin
  Ed13_Pallet_ValueVariant := Pallet;
  EdEmpresa_ValueVariant   := Empresa;
  CBEmpresa_Text           := NO_EMP;
  //
  MostraRelatorioVSImpFluxo(TP13DataIni_Date, TP13DataFim_Date,
  Ck13DataIni_Checked, Ck13DataFim_Checked, Ck13DataIni_Visible,
  Ck13DataIni_Enabled, Ed13_Pallet_ValueVariant,
  Ed13_IMEI_ValueVariant, Ed13GraGruX_ValueVariant, EdEmpresa_ValueVariant,
  Ed13_MaxInteiros_ValueVariant, RG13TipoMov_ItemIndex,
  RG13_AnaliSinte_ItemIndex, RG13_GragruY_ItemIndex,
  RG13_AnaliSinte, PB1, LaAviso1, LaAviso2, BtImprime, CBEmpresa_Text,
  Ed13SerieFch_ValueVariant, Ed13Ficha_ValueVariant);
end;

procedure TUnVS_PF.MostroFormVSMovimCod(MovimCod: Integer);
const
  Controle = 0;
var
  MovimID, Codigo: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MovimID, CodigoID ',
    'FROM vsmovcab ',
    'WHERE Codigo=' + Geral.FF0(MovimCod),
    '']);
    MovimID := Qry.FieldByName('MovimID').AsInteger;
    Codigo  := Qry.FieldByName('CodigoID').AsInteger;
    if Codigo <> 0 then
    begin
      MostraFormVS_XXX(MovimID, Codigo, Controle);
    end else
      Geral.MB_Erro('MovimCod indefinido!');
  finally
    Qry.Free;
  end;
end;

function TUnVS_PF.MovimIDCorreto(MovimID: TEstqMovimID): Boolean;
var
  MovIDSel: Integer;
begin
  MovIDSel := MyObjects.SelRadioGroup('Movimento de Couro',
    'Confirme o movimento:', sEstqMovimID, 3);
  //Correto := ObtemGraGruY_de_IDTipoCouro(RGTipoCouro.ItemIndex);
  Result := MovIDSel = Integer(MovimID);
  if (not Result) and (MovIDSel >= 0) then
    Geral.MB_Aviso('O movimento � incorreto!');
end;

(*
function TUnVS_PF.NotaCouroRibeira(Pecas, Peso, AreaM2, FatorMP, FatorAR: Double): Double;
const
  NIC = 160; // Nota inicial do couro
  FNA = 0.2; // Fator do c�lculo A
  FNB = 1.0; // Fator do c�lculo B
  FNC = 1.0; // Fator do c�lculo C
var
  PCV: Double; // Peso Corrigido para Couro Verde
  ACC: Double; // �rea do couro corrigida
  AMC: Double; // �rea m�dia couro
  PMA: Double; // Peso m�dio 100 m2
  A, B, C, _C, AB: Double;
begin
  if (Pecas <= 0) or (Peso <=0 ) or (AreaM2 <= 0) or (FatorMP <= 0)
  or (FatorAR <= 0) then
  begin
    Result := 0;
    Exit;
  end;
  ACC := AreaM2 / FatorAR;
  PCV := Peso / Pecas * FatorMP;
  AMC := ACC / Pecas;
  PMA := PCV * 100 / AMC;
  //
  A := PMA / PCV;
  B := PMA / (AMC * 10);
  C := (10 - AMC) * PMA / 200;
  //
  AB := (FNA * A) - (FNB * B);
  _C := (FNC * C) * (FNA + FNB + FNC);
  Result := NIC + (AB - _C);
end;
*)

function TUnVS_PF.NotaCouroRibeiraApuca_Area(Pecas, Peso, FatorMP, FatorAR,
              NotaMPAG: Double): Double;
const
  PesoBase = 23.75; // kg/couro verde
  AreaBase = 3.80; // m2
  StepPeso = 2.5; // A cada tantos kilos...
  StepArea = 0.1; // ... deve terr tantos m2!
var
  PCV: Double; // Peso Corrigido para Couro Verde
  //ACC: Double; // �rea do couro corrigida
  //AMC: Double; // �rea m�dia couro
  //PMA: Double; // Peso m�dio 100 m2
  //A, B, C, _C, AB: Double;
  DPV: Double; // Diferen�a do peso verde em rela��o ao peso base
  DAV: Double; // Diferen�a da �rea verde em rela��o a �rea base
  ACP: Double; // Area 100% para o Peso indicado
begin
  if (Pecas <= 0) or (Peso <=0 ) or (NotaMPAG <= 0) or (FatorMP <= 0)
  or (FatorAR <= 0) then
  begin
    Result := 0;
    Exit;
  end;
  //ACC := AreaM2 / FatorAR;
  //AMC := ACC / Pecas;
  PCV := Peso / Pecas * FatorMP;
  DPV := PCV - PesoBase;
  DAV := DPV / StepPeso * StepArea;
  ACP := AreaBase + DAV;
  if ACP = 0 then
    Result := 0
  else
    //Result := AMC / ACP * 100;
    Result := ACP * FatorAR * NotaMPAG / 100;
end;

procedure TUnVS_PF.ObtemCodENomeEmpresaDeIMEI(const Controle: Integer;
  var Empresa: Integer; var Nome: String);
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(DMod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.Empresa, IF(ent.Tipo=0,  ',
    'ent.RazaoSocial, ent.Nome) NO_EMP ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN entidades ent ON ent.Codigo=vmi.Empresa ',
    'WHERE vmi.Controle=' + Geral.FF0(Controle),
    '']);
    if Qry.RecordCount = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT vmi.Empresa, IF(ent.Tipo=0,  ',
      'ent.RazaoSocial, ent.Nome) NO_EMP ',
      'FROM ' + CO_TAB_VMB + ' vmi ',
      'LEFT JOIN entidades ent ON ent.Codigo=vmi.Empresa ',
      'WHERE vmi.Controle=' + Geral.FF0(Controle),
      '']);
    end;
    Empresa := Qry.FieldByName('Empresa').AsInteger;
    Nome    := Qry.FieldByName('NO_EMP').AsString;
  finally
    Qry.Free;
  end;
end;

function TUnVS_PF.ObtemControleDeVSPedItsPeloVSMovIts(
  VSMovIts: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
   UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
   'SELECT Controle ',
   'FROM vspedits ',
   'WHERE VSMovIts=' + Geral.FF0(VSMovIts),
   '']);
   Result := Qry.FieldByName('Controle').AsInteger;
  finally
   Qry.Free;
  end;
end;

function TUnVS_PF.ObtemControleIMEI(const SQLType: TSQLType; var Controle:
  Integer; const Senha: String): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  if Trim(Senha) <> '' then
  begin
    if Senha = CO_MASTER then
    begin
     Qry := TmySQLQuery.Create(Dmod);
     try
       UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
       'SELECT MIN(Controle) Controle ',
       'FROM ' + CO_TAB_VMB,
       '']);
       if Qry.RecordCount = 0 then
       begin
         UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
         'SELECT MIN(Controle) Controle ',
         'FROM ' + CO_SEL_TAB_VMI,
         '']);
       end;
       Controle := Qry.FieldByName('Controle').AsInteger;
       Controle := Controle - 1;
       if Controle = 0 then
         Controle := -1;
       Result := True;
     finally
       Qry.Free;
     end;
    end else
    begin
      Geral.MB_Aviso('Senha inv�lida!');
      Exit;
    end;
  end else
  begin
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType,
    Controle);
    Result := True;
  end;
end;

function TUnVS_PF.ObtemDataBalanco(Empresa: Integer): TDateTime;
var
  Periodo: Integer;
begin
  Periodo := VerificaBalanco(0) + 1;
  Result := DmkPF.PrimeiroDiaDoPeriodo_Date(Periodo);
end;

function TUnVS_PF.ObtemDataHoraSerNumNFe(const DtHrAtual: TDateTime;
  const SerNFeAtual, NumNFeAtual: Integer; var DtHrNovo: TDateTime;
  var SerNFeNovo, NumNFeNovo: Integer): Boolean;
begin
  Result := False;
  DtHrNovo   := DtHrAtual;
  SerNFeNovo := SerNFeAtual;
  NumNFeNovo := NumNfeAtual;
  if DBCheck.CriaFm(TFmVSDtHrSerNumNFe, FmVSDtHrSerNumNFe, afmoNegarComAviso) then
  begin
    FmVSDtHrSerNumNFe.TPData.Date              := DtHrAtual;
    FmVSDtHrSerNumNFe.EdHora.ValueVariant      := DtHrAtual;
    FmVSDtHrSerNumNFe.EdSerieNFe.ValueVariant  := SerNFeAtual;
    FmVSDtHrSerNumNFe.EdNumeroNFe.ValueVariant := NumNFeAtual;
    //
    FmVSDtHrSerNumNFe.ShowModal;
    if FmVSDtHrSerNumNFe.FConfirmou then
    begin
      DtHrNovo   := Int(FmVSDtHrSerNumNFe.TPData.Date) +
                    FmVSDtHrSerNumNFe.EdHora.ValueVariant;
      SerNFeNovo := FmVSDtHrSerNumNFe.EdSerieNFe.ValueVariant;
      NumNFeNovo := FmVSDtHrSerNumNFe.EdNumeroNFe.ValueVariant;
      //
      Result := True;
    end;
    //
    FmVSDtHrSerNumNFe.Destroy;
  end;
end;

function TUnVS_PF.ObtemGraGruYDeIMEI(IMEI: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT GraGruY ',
  'FROM gragrux  ',
  'WHERE Controle=( ',
  '  SELECT GraGruX ',
  '  FROM ' + CO_SEL_TAB_VMI + ' ',
  '  WHERE Controle=3363 ',
  ') ',
  '']);
  //
  Result := Qry.FieldByName('GraGruY').AsInteger;
  //
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_PF.ObtemIMEIdeSerieEFichaRMP(const SerieFch, Ficha: Integer;
  var IMEI: Integer);
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_TAB_VMB,
    'WHERE SerieFch=' + Geral.FF0(SerieFch),
    'AND Ficha=' + Geral.FF0(Ficha),
    '',
    'UNION',
    '',
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE SerieFch=' + Geral.FF0(SerieFch),
    'AND Ficha=' + Geral.FF0(Ficha),
    '',
    'ORDER BY MovimID, Controle ',
    '']);
    //
    IMEI := Qry.FieldByName('Controle').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnVS_PF.ObtemInsumoDeGraGruX(GraGruX: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT GraGru1 ',
    'FROM gragrux ',
    'WHERE Controle=' + Geral.FF0(GraGruX),
    '']);
    Result := Qry.FieldByName('GraGru1').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnVS_PF.ObtemJmpGGXdeMovCodPai_Fast(MovCodPai: Integer; ProcName: String): Integer;
const
  sProcName = 'ObtemJmpGGXdeMovCodPai_Fast()';
var
  IsOk: Boolean;
begin
  Result := DModG.MyCompressDB.SelectInteger(//aSQL : string; var IsOk : boolean; aFieldName : string):integer;
    ' SELECT GraGruX ' +
    ' FROM ' + CO_SEL_TAB_VMI + ' ' +
    ' WHERE MovimCod=' + Geral.FF0(MovCodPai) +
    ' AND MovimNiv in (30,35)',
    IsOK, 'GraGruX');
  if IsOk = False then
  begin
    Result := 0;
    Geral.MB_Erro('Reduzido nao encontrado para o MovimCod ' +
    Geral.FF0(MovCodPai) + ' em ' + ProcName + '.' + sProcName);
  end;
end;

function TUnVS_PF.ObtemJmpGGXdeMovCodPai_Slow(MovCodPai: Integer): Integer;
const
  sProcName = 'VS_PF.ObtemJmpGGXdeMovCodPai_Slow()';
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT GraGruX ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimCod=' + Geral.FF0(MovCodPai),
    'AND MovimNiv in (30,35) ',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Result := Qry.FieldByName('GraGruX').AsInteger;
    end else
      Geral.MB_Erro('Reduzido nao encontrado para o MovimCod ' +
      Geral.FF0(MovCodPai) + ' em ' + sProcName);
  finally
    Qry.Free;
  end;
end;

function TUnVS_PF.ObtemMovimIDBxaDeMovimID(MovimID: TEstqMovimID): TEstqMovimID;
const
  sProcName = 'UnVS_PF.ObtemMovimIDxaDeMovimID()';
begin
  case MovimID of
    emidEmRibPDA,
    emidEmRibDTA,
    emidEmOperacao,
    emidEmProcWE: Result := MovimID;
    emidFinished: Result := emidEmProcWE;
    emidEmProcCon,
    emidEmProcCal,
    emidEmProcCur,
    emidEmProcSP,
    emidEmReprRM,
    emidConservado,
    emidCaleado(*,
    emidSPCaleirad*): Result := MovimID;
    else
    begin
      Result := MovimID;
      Geral.MB_Erro(
      '"MovimID" ' + Geral.FF0(Integer(MovimID)) +
      ' n�o implementado em ' + sProcName);
    end;
  end;
end;

function TUnVS_PF.ObtemMovimIDDeGraGruY(GraGruY: Integer): TEstqMovimID;
const
  sProcName = 'UnVS_PF.ObtemMovimIDDeGraGruY()';
begin
  case GraGruY of
    CO_GraGruY_4096_VSRibOpe: Result := emidEmOperacao;
    CO_GraGruY_5120_VSWetEnd: Result := emidEmProcWE;
    CO_GraGruY_1072_VSNatInC: Result := emidEmProcCon;
    CO_GraGruY_1365_VSProCal: Result := emidEmProcCal;
    CO_GraGruY_1707_VSProCur: Result := emidEmProcCur;
    CO_GraGruY_7168_VSRepMer: Result := emidEmReprRM;
    //CO_GraGruY_0683_VSPSPPro???: Result := emidEmProcSP;
    //CO_GraGruY_: Result := emidEmReprRM;
    else
    begin
      Result := emidAjuste;
      Geral.MB_Erro(

      '"MovimID" indefinido para GraGruY=' + Geral.FF0(Integer(GraGruY)) +
      ' em ' + sProcName);
    end;
  end;
end;

function TUnVS_PF.ObtemMovimNivBxaDeMovimID(
  MovimID: TEstqMovimID): TEstqMovimNiv;
const
  sProcName = 'UnVS_PF.ObtemMovimNivBxaDeMovimID()';
begin
  case MovimID of
    emidEmRibPDA: Result := eminEmPDABxa;
    emidEmRibDTA: Result := eminEmDTABxa;
    emidEmOperacao: Result := eminEmOperBxa;
    emidEmProcWE: Result := eminEmWEndBxa;
    emidFinished: Result := eminEmWEndBxa;
    emidEmProcCon: Result := eminEmConBxa;
    emidEmProcCal: Result := eminEmCalBxa;
    emidEmProcCur: Result := eminEmCurBxa;
    emidEmProcSP: Result := eminEmPSPBxa;
    emidEmReprRM: Result := eminEmRRMBxa;
    emidConservado: Result := eminEmConBxa;
    emidCaleado: Result := eminEmCalBxa;
    //emidSPCaleirad: Result := eminEmPSPBxa;
    else
    begin
      Result := eminSemNiv;
      Geral.MB_Erro(

      '"MovimNiv" indefinido para MovimID=' + Geral.FF0(Integer(MovimID)) +
      ' em ' + sProcName);
    end;
  end;
end;

function TUnVS_PF.ObtemMovimNivCliForLocDeMovimID(
  MovimID: TEstqMovimID): TEstqMovimNiv;
const
  sProcName = 'UnVS_PF.ObtemMovimNivCliForLocDeMovimID()';
begin
  case MovimID of
    emidEmOperacao: Result := eminEmOperInn;
    emidEmProcWE:   Result := eminEmWEndInn;
    //emidFinished: Result := eminEmWEndInn; ??? Ativar??
    emidEmProcCon:  Result := eminEmConInn;
    emidEmProcCal:  Result := eminEmCalInn;
    emidEmProcCur:  Result := eminEmCurInn;
    emidEmProcSP:   Result := eminEmPSPInn;
    emidEmReprRM:   Result := eminEmRRMInn;
    //
    emidIndsXX:     Result := eminDestCurtiXX;
    emidCaleado:    Result := eminEmCalInn;
    //emidSPCaleirad: Result := eminEmPSPInn;
    else
    begin
      Result := eminSemNiv;
      Geral.MB_Erro(
      '"MovimNiv" indefinido para MovimID=' + Geral.FF0(Integer(MovimID)) +
      ' em ' + sProcName);
    end;
  end;
end;

function TUnVS_PF.ObtemMovimNivEFDDeMovimID(
  MovimID: TEstqMovimID): TEstqMovimNiv;
const
  sProcName = 'UnVS_PF.ObtemMovimNivEFDDeMovimID()';
begin
  case MovimID of
    emidCompra:       Result := eminSemNiv;
    //emidEmOperacao: Result := emin;
    //emidEmProcWE:   Result := emin;
    emidEmProcCal:    Result := eminSorcCal;
    emidEmProcCur:    Result := eminSorcCur;
    else
    begin
      Result := eminSemNiv;
      Geral.MB_Erro(
      '"MovimNiv" indefinido para MovimID=' + Geral.FF0(Integer(MovimID)) +
      ' em ' + sProcName);
    end;
  end;
end;

function TUnVS_PF.ObtemMovimSeqDeMovimIDEMovimNiv(Level, MovimID,
  MovimNiv, MovimCod, IMEI, SeqAtual: Integer): Integer;
var
  GraGruY: Integer;
const
  sProcName = 'VS_PF.ObtemMovimSeqDeMovimIDEMovimNiv';
  //
  procedure MsgErr();
  begin
    Geral.MB_ERRO('"GGY" n�o implementado em ' + sProcName + sLineBreak +
    'MovimID: ' + Geral.FF0(MovimID) + sLineBreak +
    'MovimNiv: ' + Geral.FF0(MovimNiv) + sLineBreak +
    'GraGruY: ' + Geral.FF0(GraGruY) + sLineBreak +
    '');
  end;
  //
  function DefineClasGGY(): Integer;
  begin
    GraGruY := ObtemGraGruYDeIMEI(IMEI);
    case GraGruY of
      //CO_GraGruY_0512_VSSubPrd
      CO_GraGruY_1024_VSNatCad: Result := CO_SMIS_03000_Classificar;
      CO_GraGruY_1072_VSNatInC: Result := CO_SMIS_03000_Classificar;
      CO_GraGruY_1088_VSNatCon: Result := CO_SMIS_03000_Classificar;
      CO_GraGruY_1365_VSProCal: Result := CO_SMIS_13000_Classificar;
      CO_GraGruY_1536_VSCouCal: Result := CO_SMIS_13000_Classificar;
      CO_GraGruY_1707_VSProCur: Result := CO_SMIS_22000_Classificar;
      CO_GraGruY_1877_VSCouCur: Result := CO_SMIS_22000_Classificar;
      CO_GraGruY_2048_VSRibCad: Result := CO_SMIS_22000_Classificar;//CO_SMIS_31000_Reclassificar;
      CO_GraGruY_3072_VSRibCla: Result := CO_SMIS_31000_Reclassificar;
      CO_GraGruY_4096_VSRibOpe: Result := CO_SMIS_31000_Reclassificar;
      CO_GraGruY_5120_VSWetEnd: Result := CO_SMIS_52000_Classificar;
      CO_GraGruY_6144_VSFinCla: Result := CO_SMIS_70000_Reclassificar;
      CO_GraGruY_7168_VSRepMer: Result := CO_SMIS_70000_Reclassificar;
      else  MsgErr();
    end;
  end;
  function DefineReclasGGY(): Integer;
  begin
    GraGruY := ObtemGraGruYDeIMEI(IMEI);
    case GraGruY of
      //CO_GraGruY_0512_VSSubPrd
      CO_GraGruY_1024_VSNatCad: Result := CO_SMIS_04000_Reclassificar;
      CO_GraGruY_1072_VSNatInc: Result := CO_SMIS_04000_Reclassificar;
      CO_GraGruY_1088_VSNatCon: Result := CO_SMIS_04000_Reclassificar;
      CO_GraGruY_1365_VSProCal: Result := CO_SMIS_13000_Classificar;
      CO_GraGruY_1536_VSCouCal: Result := CO_SMIS_13000_Classificar;
      CO_GraGruY_1707_VSProCur: Result := CO_SMIS_22000_Classificar;
      CO_GraGruY_1877_VSCouCur: Result := CO_SMIS_22000_Classificar;
      CO_GraGruY_2048_VSRibCad: Result := CO_SMIS_22000_Classificar;//CO_SMIS_31000_Reclassificar;
      CO_GraGruY_3072_VSRibCla: Result := CO_SMIS_31000_Reclassificar;
      CO_GraGruY_4096_VSRibOpe: Result := CO_SMIS_31000_Reclassificar;
      CO_GraGruY_5120_VSWetEnd: Result := CO_SMIS_55000_Reclassificar;
      CO_GraGruY_6144_VSFinCla: Result := CO_SMIS_70000_Reclassificar;
      CO_GraGruY_7168_VSRepMer: Result := CO_SMIS_70000_Reclassificar;
      else MsgErr();
    end;
  end;
  function DefineSubProdGGY(): Integer;
  begin
    GraGruY := ObtemGraGruYDeIMEI(IMEI);
    case GraGruY of
      //CO_GraGruY_0512_VSSubPrd
      //CO_GraGruY_1024_VSNatCad: Result := CO_SMIS_;
      //CO_GraGruY_1072_VSNatInC: Result := CO_SMIS_;
      //CO_GraGruY_1088_VSNatCon: Result := CO_SMIS_;
      CO_GraGruY_1365_VSProCal:
      begin
        // Parei aqui! ver que tipo de subproduto eh para ver se e divisaoe nao redescarne!
        Result := CO_SMIS_10000_Redescarne;
      end;
      //CO_GraGruY_1536_VSCouCal:
      //CO_GraGruY_1707_VSProCur: Result := CO_TXT_SMIS_
      //CO_GraGruY_1877_VSCouCur: Result := CO_TXT_SMIS_
      //CO_GraGruY_2048_VSRibCad: Result := CO_SMIS_;
      //CO_GraGruY_3072_VSRibCla: Result := CO_SMIS_;
      //CO_GraGruY_4096_VSRibOpe: Result := CO_SMIS_;
      //CO_GraGruY_5120_VSWetEnd: Result := CO_SMIS_;
      //CO_GraGruY_6144_VSFinCla: Result := CO_SMIS_;
      // CO_GraGruY_7168_VSRepMer
      else MsgErr();
    end;
  end;
begin
  AdicionarNovosVS_emid();
  case TEstqMovimID(MovimID) of
    //emidAjuste
    (*01*)emidCompra         : Result := CO_SMIS_02000_Compra;
    (*02*)emidVenda          : Result := CO_SMIS_99000_VENDA;
    //(*03*)emidReclasWE       :
    //(*04*)emidBaixa          :
    //(*05*)emidIndsWE         :
    (*06*)emidIndsXX         : Result := CO_SMIS_20000_Medir;
    (*07*)emidClassArtXXUni  : Result := DefineClasGGY();
    (*08*)emidReclasXXUni    : Result := DefineReclasGGY();
    (*09*)emidForcado        : Result := CO_SMIS_99100_BaixaForcada;
    //(*10*)emidSemOrigem      :
    (*11*)emidEmOperacao     : Result := ObtemVSOpeSeqDeMovimCod(MovimCod);
    //(*12*)emidResiduoReclas  :
    (*13*)emidInventario     : Result := CO_SMIS_01000_AjusteEstoque;
    (*14*)emidClassArtXXMul  : Result := DefineClasGGY();
    (*15*)emidPreReclasse    : Result := DefineReclasGGY();
    (*16*)emidEntradaPlC     : Result := CO_SMIS_02000_Compra;
    (*17*)emidExtraBxa       : Result := CO_SMIS_99200_BaixaExtravio;
    //(*18*)emidSaldoAnterior  :
    (*19*)emidEmProcWE       : Result := CO_SMIS_41000_Recurtir;
    (*20*)emidFinished       : Result := CO_SMIS_80000_CouroAcabado;
    (*21*)emidDevolucao      : Result := CO_SMIS_02500_EntradaPorDevolucao;
    (*22*)emidRetrabalho     : Result := CO_SMIS_02750_EntradaParaRetrabalho;
    //(*23*)emidGeraSubProd    : Result := DefineSubProdGGY();
    (*24*)emidReclasXXMul    : Result := DefineReclasGGY();
    (*25*)emidTransfLoc      :
    begin
      case Level of
        1: Result := SeqAtual + 1;
        else Result := CO_SMIS_89990_Transferencia;
      end;
    end;
    (*26*)emidEmProcCal      : Result := CO_SMIS_08000_Caleiro;
    (*27*)emidEmProcCur      : Result := CO_SMIS_15000_Curtimento;
    (*39*)emidEmProcCon      : Result := CO_SMIS_02900_Conservar;
    //(*39*)emidConservado      : Result := ???;

    else Geral.MB_ERRO('"MovimID" n�o implementado em ' + sProcName);
  end;
end;

function TUnVS_PF.ObtemNomeDtAberturaVSXxxCab(MovimID: TEstqMovimID): String;
const
  sProcName = 'VS_PF.ObtemNomeDtAberturaVSXxxCab()';
begin
  AdicionarNovosVS_emid();
  Result              := '?????';
  case MovimID of
    (*00*)//emidAjuste: Result        := 'vs;
    (*01*)emidCompra: Result          := 'DtEntrada';
    (*02*)emidVenda: Result           := 'DtVenda';
    (*03*)//emidReclasWE: Result      := 'vs;
    (*04*)//emidBaixa: Result         := 'vs;
    (*05*)//emidIndsWE: Result        := 'vs;
    (*06*)emidIndsXX: Result          := 'DtHrAberto';
    (*07*)emidClassArtXXUni: Result   := 'DtHrLibCla';
    (*08*)emidReclasXXUni: Result     := 'DtHrLibCla';
    (*09*)emidForcado: Result         := 'DtBaixa';
    (*10*)//emidSemOrigem: Result     := 'vs;
    (*11*)emidEmOperacao: Result      := 'DtHrAberto';
    (*12*)//emidResiduoReclas: Result := 'vs;
    (*13*)emidInventario: Result      := 'DataHora';
    (*14*)emidClassArtXXMul: Result   := 'DataHora';
    (*15*)emidPreReclasse: Result     := 'DataCad';
    (*16*)emidEntradaPlC: Result      := 'DtEntrada';
    (*17*)emidExtraBxa: Result        := 'DtBaixa';
    (*18*)//emidSaldoAnterior: Result := 'vs;
    (*19*)emidEmProcWE: Result        := 'DtHrAberto';
    (*20*)emidFinished: Result        := 'DtHrAberto';
    (*21*)emidDevolucao: Result       := 'DtEntrada';
    (*22*)emidRetrabalho: Result      := 'DtEntrada';
    (*23*)emidGeraSubProd: Result     := 'DataHora';
    (*24*)emidReclasXXMul: Result     := 'DataHora';
    (*25*)emidTransfLoc: Result       := 'DtVenda';
    (*26*)emidEmProcCal: Result       := 'DtHrAberto';
    (*27*)emidEmProcCur: Result       := 'DtHrAberto';
    (*28*)emidDesclasse: Result       := 'DtHrIni';
    (*29*)emidCaleado: Result         := 'DtHrAberto';
    (*30*)emidEmRibPDA: Result        := 'DtHrAberto';
    (*31*)emidEmRibDTA: Result        := 'DtHrAberto';
    (*32*)emidEmProcSP: Result        := 'DtHrAberto';
    (*33*)emidEmReprRM: Result        := 'DtHrAberto';
    (*34*)emidCurtido: Result         := 'DtHrAberto';
    (*35*)emidMixInsum: Result        := 'DataB';
    (*39*)emidEmProcCon: Result       := 'DtHrAberto';
    (*40*)emidConservado: Result      := 'DtHrAberto';
    (*41*)emidEntraExced: Result      := 'DtEntra';
    //(*42*)emidSPCaleirad: Result      := 'DtHrAberto';
    else
      Geral.MB_ERRO('"MovimID" ' + Geral.FF0(Integer(MovimID)) +
      ' n�o implementado em ' + sProcName);
  end;
end;

function TUnVS_PF.ObtemOrigemDesclasse(const IMEI: Integer; var MovimID, Codigo,
  Controle: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result   := False;
  MovimID  := 0;
  Codigo   := 0;
  Controle := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MovimNiv, DstMovID, DstNivel1, DstNivel2, ',
    'SrcMovID, SrcNivel1, SrcNivel2  ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE Controle=' + Geral.FF0(IMEI),
    '']);
    if Qry.RecordCount > 0 then
    begin
      case TEstqMovimNiv(Qry.FieldByName('MovimNiv').AsInteger) of
        (*1*)eminSorcClass:
        begin
          MovimID  := Qry.FieldByName('SrcMovID').AsInteger;
          Codigo   := Qry.FieldByName('SrcNivel1').AsInteger;
          Controle := Qry.FieldByName('SrcNivel2').AsInteger;
          //
          Result   := True;
        end;
        (*2*)eminDestClass:
        begin
          MovimID  := Qry.FieldByName('DstMovID').AsInteger;
          Codigo   := Qry.FieldByName('DstNivel1').AsInteger;
          Controle := Qry.FieldByName('DstNivel2').AsInteger;
          //
          Result   := True;
        end;
      end;
    end;
  finally
    Qry.Free
  end;
end;

function TUnVS_PF.ObtemTipoEstqIMEI(IMEI: Integer; FldArM2, FldPesoKg, FldPecas,
  FldGGX, FldCtrl: String): Integer;
var
  Qry: TmySQLQuery;
  //
  procedure ReopenQuery();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ',
    SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', False,
    FldArM2, FldPesoKg, FldPecas),
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=' + FldGGX,
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'WHERE ' + FldCtrl + '=' + Geral.FF0(IMEI),
    '']);
    //Geral.MB_Teste(Qry);
  end;
begin
  Result := -1;
  Qry := TmySQLQuery.Create(Dmod);
  try
    ReopenQuery;
    Result := Trunc(Qry.FieldByName('TipoEstq').AsFloat);
  finally
    Qry.Free;
  end;
end;

function TUnVS_PF.ObtemTipoEstqIMEI_Fast(IMEI: Integer; FldArM2, FldPesoKg,
  FldPecas, FldGGX, FldCtrl: String): Integer;
const
  sProcName = 'TUnVS_PF.ObtemTipoEstqIMEI_Fast()';
var
  IsOk: Boolean;
  Tipo_Item: Integer;
begin
  Result := DModG.MyCompressDB.SelectInteger(
  //aSQL : string; var IsOk : boolean; aFieldName : string):integer;
  ' SELECT ' +
    SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', False,
    FldArM2, FldPesoKg, FldPecas) +
  ' FROM ' + CO_SEL_TAB_VMI + ' vmi ' +
  ' LEFT JOIN gragrux    ggx ON ggx.Controle=' + FldGGX +
  ' LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ' +
  ' LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ' +
  ' LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ' +
  ' WHERE ' + FldCtrl + '=' + Geral.FF0(IMEI),
  IsOK, 'TipoEstq');
  if IsOk = False then
  begin
    Result := -1;
    Geral.MB_Erro('TipoEstq nao encontrado para o Reduzido ' +
    Geral.FF0(IMEI) + ' em ' + sProcName);
  end;
end;

function TUnVS_PF.ObtemVSOpeSeqDeMovimCod(MovimCod: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT VSOpeSeq ',
    'FROM operacoes ',
    'WHERE Codigo = (',
    '  SELECT operacoes ',
    '  FROM vsopecab ',
    '  WHERE MovimCod=' + Geral.FF0(MovimCod),
    ')',
    '']);
    Result := Qry.FieldByName('VSOpeSeq').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnVS_PF.ObtemCodigoDeMovimCod(MovimCod: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT CodigoID ',
    'FROM vsmovcab ',
    'WHERE Codigo=' + Geral.FF0(MovimCod),
    '']);
    Result := Qry.FieldByName('CodigoID').AsInteger;
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_PF.PesquisaReclasseDePreClasse(MovimID, Codigo, Controle: Integer);
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
(*
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.myDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
    'AND Codigo=' + Geral.FF0(Codigo),
    '']);
    //Geral.MB_Teste(Qry);
*)
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.myDB, [
    'SELECT Codigo, MovimCod, COUNT(Controle) ITENS ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE SrcNivel2=' + Geral.FF0(Controle),
    'GROUP BY Codigo, MovimCod ',
    'ORDER BY Codigo, MovimCod ',
    '']);
    if Qry.RecordCount > 1 then
      Geral.MB_Info('Foram encontrados ' + Geral.FF0(Qry.recordCount) +
      ' registros! Ser� mostrado o primeiro!');
    MostraFormVSPaMulCabR(Qry.FieldByName('Codigo').AsInteger);
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_PF.ReIncluiCouNivs(GraGruX, CouNiv1, CouNiv2, PrevPcPal,
  Grandeza, Bastidao: Integer; PrevAMPal, PrevKgPal: Double;
  ArtigoImp, ClasseImp: String; MediaMinM2, MediaMaxM2: Double; MediaMinKg:
  Double; MediaMaxKg: Double; GGXPronto: Integer; FatrClase, BaseValCusto, BaseValVenda: Double;
  BaseCliente: String; BaseImpostos, BasePerComis, BasFrteVendM2: Double;
  BaseValLiq: Double; ArtGeComodty: Integer; PrecoMin: Double; PrecoMax: Double);
begin
  UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'gragruxcou', False, [
  'CouNiv1', 'CouNiv2', 'PrevPcPal',
  'PrevAMPal', 'PrevKgPal',
  'ArtigoImp', 'ClasseImp',
  'MediaMinM2', 'MediaMaxM2',
  'MediaMinKg', 'MediaMaxKg',
  'Grandeza', 'Bastidao',
  'GGXPronto', 'FatrClase',
  'BaseValVenda', 'BaseCliente',
  'BaseImpostos', 'BasePerComis',
  'BasFrteVendM2', 'BaseValLiq'], [
  'GraGruX'], [
  'CouNiv1', 'CouNiv2', 'PrevPcPal',
  'PrevAMPal', 'PrevKgPal',
  'ArtigoImp', 'ClasseImp',
  'MediaMinM2', 'MediaMaxM2',
  'MediaMinKg', 'MediaMaxKg',
  'Grandeza', 'Bastidao',
  'GGXPronto', 'FatrClase',
  'BaseValVenda', 'BaseCliente',
  'BaseImpostos', 'BasePerComis',
  'BasFrteVendM2', 'BaseValLiq',
  'BaseValCusto', 'ArtGeComodty',
  'PrecoMin', 'PrecoMax'], [
  CouNiv1, CouNiv2, PrevPcPal,
  PrevAMPal, PrevKgPal,
  ArtigoImp, ClasseImp,
  MediaMinM2, MediaMaxM2,
  MediaMinKg, MediaMaxKg,
  Grandeza, Bastidao,
  GGXPronto, FatrClase,
  BaseValVenda, BaseCliente,
  BaseImpostos, BasePerComis,
  BasFrteVendM2, BaseValLiq], [
  GraGruX], [
  CouNiv1, CouNiv2, PrevPcPal,
  PrevAMPal, PrevKgPal,
  ArtigoImp, ClasseImp,
  MediaMinM2, MediaMaxM2,
  MediaMinKg, MediaMaxKg,
  Grandeza, Bastidao,
  GGXPronto, FatrClase,
  BaseValVenda, BaseCliente,
  BaseImpostos, BasePerComis,
  BasFrteVendM2, BaseValLiq,
  BaseValCusto, ArtGeComodty,
  PrecoMin, PrecoMax], True);
end;

procedure TUnVS_PF.ReopenDesnate(Codigo, GraGruX: Integer; QrVSCacIts, QrVSCacSum:
  TmySQLQuery);
var
  SQL_GraGruX: String;
begin
  if GraGruX = 0 then
    SQL_GraGruX := ''
  else
    SQL_GraGruX := 'AND pla.GragruX=' + Geral.FF0(GraGruX);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacSum, Dmod.MyDB, [
  'SELECT cia.Codigo, cia.CacCod,',
  'SUM(cia.Pecas) Pecas, ',
  'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2 ',
  'FROM vscacitsa cia ',
  'LEFT JOIN vsdsnits vdi ON vdi.VSCacCod=cia.CacCod',
  'LEFT JOIN vsdsncab vdc ON vdc.Codigo=vdi.Codigo',
  'WHERE vdc.Codigo=' + Geral.FF0(Codigo),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacIts, Dmod.MyDB, [
  'SELECT 0.000 AgrupaTudo, pla.GraGruX, SUM(cia.Pecas) Pecas, ',
  'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, ',
  'IF(SUM(cia.Pecas) > 0, SUM(cia.AreaM2) / SUM(cia.Pecas), 0) MediaM2PC, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM vscacitsa cia ',
  'LEFT JOIN vspalleta pla ON pla.Codigo=cia.VSPallet ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=pla.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vsdsnits vdi ON vdi.VSCacCod=cia.CacCod',
  'LEFT JOIN vsdsncab vdc ON vdc.Codigo=vdi.Codigo',
  'WHERE vdc.Codigo=' + Geral.FF0(Codigo),
  SQL_GraGruX,
  'GROUP BY pla.GraGruX ',
  '']);
end;

procedure TUnVS_PF.ReopenGraGruX_A(Qry: TmySQLQuery; NomeParcial: String);
var
  SQL_NomeParcial: String;
begin
  if (Qry.State = dsInactive) or (NomeParcial <> '') then
  begin
    SQL_NomeParcial := '';
    if NomeParcial <> '' then
      SQL_NomeParcial := Geral.ATS([
      'AND ',
      'CONCAT(gg1.Nome, ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
      'LIKE "%' + NomeParcial + '%"']);
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
    'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED ',
    'FROM gragrux ggx',
    //'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.Controle',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed ',
    'WHERE ggx.GragruY <> 0',
    SQL_NomeParcial,
    //'ORDER BY ggy.Ordem, NO_PRD_TAM_COR, ggx.Controle ',
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
    '']);
  end;
end;

procedure TUnVS_PF.ReopenGraGruXCou(QrGraGruXCou: TmySQLQuery;
  GraGruX: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruXCou, Dmod.MyDB, [
  'SELECT cn1.Nome NO_CouNiv1,  ',
  'cn2.Nome NO_CouNiv2, gxc.*  ',
  'FROM gragruxcou gxc ',
  'LEFT JOIN couniv1 cn1 ON cn1.Codigo=gxc.CouNiv1 ',
  'LEFT JOIN couniv2 cn2 ON cn2.Codigo=gxc.CouNiv2 ',
  'WHERE gxc.GraGruX=' + Geral.FF0(GraGruX),
  ' ']);
end;

procedure TUnVS_PF.ReopenNotasEqualize(Codigo: Integer; QrNotaAll, QrNotaCrr,
  QrNotas: TmySQLQuery);
  procedure ReabreNotas(Qry: TmySQLQuery; Tudo: Boolean);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(cia.Pecas) Pecas,  ',
    'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2,  ',
    'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaAll, ',
    'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) NotaBrutaM2, ',
    'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) / vec.BasNotZero * 100 NotaEqzM2 ',
    'FROM vscacitsa cia  ',
    'LEFT JOIN vseqzits vei ON vei.Pallet=cia.VSPallet ',
    'LEFT JOIN vseqzcab vec ON vec.Codigo=vei.Codigo ',
    'LEFT JOIN vseqzsub vez ON vez.Codigo=vec.Codigo ',
    '  AND vez.SubClass=cia.SubClass ',
    'WHERE vec.Codigo=' + Geral.FF0(Codigo),
    Geral.ATS_IF(not Tudo, ['AND vez.BasNota > 0 ']),
    '']);
  end;
begin
  if QrNotaCrr <> nil then
    ReabreNotas(QrNotaCrr, False);
  if QrNotaAll <> nil then
  begin
    ReabreNotas(QrNotaAll, True);
    if QrNotas <> nil then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrNotas, Dmod.MyDB, [
      'SELECT cia.SubClass, SUM(cia.Pecas) Pecas, ',
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, ',
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaM2,',
      'SUM(cia.AreaM2) / ' + Geral.FFN_Dot(QrNotaAll.FieldByName('AreaM2').AsFloat / 100) + ' PercM2 ',
      'FROM vscacitsa cia ',
      'LEFT JOIN vseqzits vdi ON vdi.Pallet=cia.VSPallet',
      'LEFT JOIN vseqzcab vdc ON vdc.Codigo=vdi.Codigo',
      'LEFT JOIN vseqzsub vez ON vez.Codigo=vdc.Codigo ',
      '  AND vez.SubClass=cia.SubClass ',
      'WHERE vdc.Codigo=' + Geral.FF0(Codigo),
      'GROUP BY cia.SubClass',
      '']);
      //Geral.MB_Teste(QrNotas);
    end;
  end;
end;

procedure TUnVS_PF.ReopenPallet_A(Qry: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM vspalleta ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
end;

(*
procedure TUnVS_PF.ReopenVSGerArtBxa(Qry: TmySQLQuery; MovimCod,
  Controle: Integer; SQL_Limit: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
  FROM v s m o v i t s vmi ,
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminBaixCurtiXX)),
  'ORDER BY NO_Pallet, vmi.Controle ',
  SQL_Limit,
  '']);
  //
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;
*)

procedure TUnVS_PF.ReopenVSEmitCusIMEI(Qry: TmySQLQuery; Pesagem, TemIMEIMrt:
  Integer; SQL_Limit: String);
{
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  SQL_Flds := Geral.ATS([
  SQL_NO_GGX(),
  SQL_NO_FRN(),
  SQL_NO_CMO(),
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet, ',
  '1.000 FatorImp ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX(),
  SQL_LJ_FRN(),
  SQL_LJ_CMO(),
  'LEFT JOIN vspalleta   vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch    vsf ON vsf.Codigo=vmi.SerieFch ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.Controle IN ( ',
  '  SELECT VSMovIts ',
  '  FROM emitcus ',
  '  WHERE Codigo=' + Geral.FF0(Pesagem),
  ') ',
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //Geral.MB_Teste(Qry);
  //
  //Qry.Locate('Controle', Controle, []);
  //
}
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, PesagemTxt, Liga: String;
  Qr2: TmySQLQuery;
begin
  Qr2 := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qr2, Dmod.MyDB, [
    '  SELECT VSMovIts ',
    '  FROM emitcus ',
    '  WHERE Codigo=' + Geral.FF0(Pesagem),
    '  AND Codigo<>0',
    '']);
    if Qr2.RecordCount = 0 then
      PesagemTxt := '-999999999'
    else
    begin
      PesagemTxt := '';
      Liga := '';
      Qr2.First;
      while not Qr2.Eof do
      begin
        PesagemTxt := Liga + Geral.FF0(Qr2.FieldByName('VSMovIts').AsInteger);
        Liga := ',';
        //
        Qr2.Next;
      end;
    end;
    SQL_Flds := Geral.ATS([
    SQL_NO_GGX(),
    SQL_NO_FRN(),
    SQL_NO_CMO(),
    'vsf.Nome NO_SerieFch, ',
    'vsp.Nome NO_Pallet, ',
    '1.000 FatorImp ',
    '']);
    SQL_Left := Geral.ATS([
    SQL_LJ_GGX(),
    SQL_LJ_FRN(),
    SQL_LJ_CMO(),
    'LEFT JOIN vspalleta   vsp ON vsp.Codigo=vmi.Pallet ',
    'LEFT JOIN vsserfch    vsf ON vsf.Codigo=vmi.SerieFch ',
    '']);
    SQL_Wher := Geral.ATS([
    'WHERE vmi.Controle IN ( ' + PesagemTxt + ')',
    '']);
    SQL_Group := '';
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    'ORDER BY NO_Pallet, Controle ',
    '']);
    //Geral.MB_Teste(Qry);
    //
    //Qry.Locate('Controle', Controle, []);
    //
  finally
    Qr2.Free;
  end;
end;

procedure TUnVS_PF.ReopenVSEmitCusIMEI_Sum(Qry: TmySQLQuery; Pesagem,
  TemIMEIMrt: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT  SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
  'SUM(AreaM2) AreaM2 ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'WHERE vmi.Controle IN ( ',
  '  SELECT VSMovIts ',
  '  FROM emitcus ',
  '  WHERE Codigo=' + Geral.FF0(Pesagem),
  ') ',
  '']);
end;

procedure TUnVS_PF.ReopenVSListaPallet2(Qry: TmySQLQuery; VSMovImp4, VSLstPalBox: String;
  Empresa, GraGruX, Pallet, Terceiro: Integer; GraGruYs, OrderBy: String);
var
 Ordem, SQL_GraGruY: String;
begin
  if GraGruYs <> '' then
    SQL_GraGruY := 'AND GraGruY IN (' + GraGruYs + ') '
  else
    SQL_GraGruY := '';
  //
  Ordem := Trim(OrderBy);
  if Ordem = '' then
    Ordem := 'ORDER BY Pallet DESC, PalStat ';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
(*
  'SELECT mi4.Empresa, mi4.GraGruX, SUM(mi4.Pecas) Pecas, SUM(mi4.PesoKg) PesoKg,  ',
  'Sum(mi4.AreaM2) AreaM2, SUM(mi4.AreaP2) AreaP2, SUM(mi4.ValorT) ValorT,   ',
  'SUM(mi4.SdoVrtPeca) SdoVrtPeca, SUM(mi4.SdoVrtPeso) SdoVrtPeso,   ',
  'Sum(mi4.SdoVrtArM2) SdoVrtArM2,  ',
  'SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca) PalVrtPeca,   ',
  'SUM(mi4.SdoVrtPeso+mi4.LmbVrtPeso) PalVrtPeso,  ',
  'SUM(mi4.LmbVrtArM2+mi4.SdoVrtArM2) PalVrtArM2,   ',
  'SUM(mi4.LmbVrtPeca) LmbVrtPeca,   ',
  'SUM(mi4.LmbVrtPeso) LmbVrtPeso, SUM(mi4.LmbVrtArM2) LmbVrtArM2,   ',
  'mi4.GraGru1, mi4.NO_PRD_TAM_COR, mi4.Pallet, mi4.NO_PALLET, mi4.Terceiro,  ',
  'mi4.CliStat, mi4.Status, mi4.NO_FORNECE,  mi4.NO_CLISTAT, mi4.NO_EMPRESA,  ',
  'mi4.NO_STATUS, mi4.DataHora, mi4.OrdGGX, mi4.OrdGGY, mi4.GraGruY,  ',
  'mi4.NO_GGY, StatPall PalStat, NO_StatPall NO_PalStat,  ',
  'IF(SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca)  <> 0,  ',
  'SUM(mi4.LmbVrtArM2+mi4.SdoVrtArM2) / SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca), 0)  ',
  'Media,  ',
  'mi4.NO_MovimNiv, mi4.NO_MovimID, mi4.MovimNiv,  ',
  'mi4.MovimID, mi4.IMEC, mi4.Codigo, mi4.IMEI,  ',
  'mi4.SerieFch, mi4.NO_SerieFch, mi4.Ficha,  ',
  'mi4.Ativo  ',
  'FROM  _vsmovimp4_fmvsmovimp mi4 ',
  'WHERE (SdoVrtPeca > 0 OR LmbVrtPeca <> 0)  ',
  'GROUP BY Pallet  ',
  'ORDER BY Pallet DESC, PalStat  ',
*)
  'SELECT mi4.Empresa, mi4.GraGruX, SUM(mi4.Pecas) Pecas, SUM(mi4.PesoKg) PesoKg,  ',
  'Sum(mi4.AreaM2) AreaM2, SUM(mi4.AreaP2) AreaP2, SUM(mi4.ValorT) ValorT,   ',
  'SUM(mi4.SdoVrtPeca) SdoVrtPeca, SUM(mi4.SdoVrtPeso) SdoVrtPeso,   ',
  'Sum(mi4.SdoVrtArM2) SdoVrtArM2,  ',
  'SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca) PalVrtPeca,   ',
  'SUM(mi4.SdoVrtPeso+mi4.LmbVrtPeso) PalVrtPeso,  ',
  'SUM(mi4.LmbVrtArM2+mi4.SdoVrtArM2) PalVrtArM2,   ',
  'SUM(mi4.LmbVrtPeca) LmbVrtPeca,   ',
  'SUM(mi4.LmbVrtPeso) LmbVrtPeso, SUM(mi4.LmbVrtArM2) LmbVrtArM2,   ',
  'mi4.GraGru1, mi4.NO_PRD_TAM_COR, mi4.Pallet, mi4.NO_PALLET, mi4.Terceiro,  ',
  'mi4.CliStat, mi4.Status, mi4.NO_FORNECE,  mi4.NO_CLISTAT, mi4.NO_EMPRESA,  ',
  'mi4.NO_STATUS, mi4.DataHora, mi4.OrdGGX, mi4.OrdGGY, mi4.GraGruY,  ',
  'mi4.NO_GGY, StatPall PalStat, NO_StatPall NO_PalStat,  ',
  'IF(SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca)  <> 0,  ',
  'SUM(mi4.LmbVrtArM2+mi4.SdoVrtArM2) / SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca), 0)  ',
  'Media,  ',
  'CASE ',
  'WHEN SUM(mi4.LmbVrtArM2+mi4.SdoVrtArM2) / SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca) < cou.MediaMinM2 THEN 0.000 ',
  'WHEN SUM(mi4.LmbVrtArM2+mi4.SdoVrtArM2) / SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca) > cou.MediaMaxM2 THEN 2.000 ',
  'ELSE 1.000 END FaixaMediaM2, ',
  'mi4.NO_MovimNiv, mi4.NO_MovimID, mi4.MovimNiv,  ',
  'mi4.MovimID, mi4.IMEC, mi4.Codigo, mi4.IMEI,  ',
  'mi4.SerieFch, mi4.NO_SerieFch, mi4.Ficha,  ',
  'mi4.Ativo  ',
  'FROM  _vsmovimp4_fmvsmovimp mi4 ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou cou ON cou.GraGruX=mi4.GraGruX',
  'WHERE (SdoVrtPeca > 0 OR LmbVrtPeca <> 0)  ',
  'GROUP BY Pallet  ',
  'ORDER BY Pallet DESC, PalStat  ',
  '']);
  //Geral.MB_Teste(Qry);
  //
  Qry.Locate('Empresa;GraGruX;Pallet;Terceiro',
  VarArrayOf([Empresa, GraGruX, Pallet, Terceiro]), []);
end;

procedure TUnVS_PF.ReopenVSMovDif(Qry: TmySQLQuery; Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsmovdif ',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
end;

procedure TUnVS_PF.ReopenVSMovIts_Pallet1(QrVSMovIts, QrPallets: TmySQLQuery);
var
  GraGruX, Empresa, Pallet: Integer;
begin
  ////////////////////////////////////////////////////////////
  //QrEstqR4 >> GROUP BY vmi.Empresa, vmi.Pallet, vmi.GraGruX
  /////////////////////////////////////////////////////////////
  GraGruX    := QrPallets.FieldByName('GraGruX').AsInteger;
  Empresa    := QrPallets.FieldByName('Empresa').AsInteger;
  Pallet     := QrPallets.FieldByName('Pallet').AsInteger;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT pal.Nome NO_Pallet, vmi.*, ggx.GraGruY, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN vspalleta pal ON pal.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades ent ON ent.Codigo=vmi.Terceiro',
  //'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX',
  'LEFT JOIN gragrux ggx ON ggx.Controle=pal.GraGruX',
  'WHERE vmi.Empresa=' + Geral.FF0(Empresa),
  //'AND vmi.GraGruX=' + Geral.FF0(GraGruX),
  'AND pal.GraGruX=' + Geral.FF0(GraGruX),
  'AND ( ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidCompra)),
  '  OR  ',
  '  vmi.SrcMovID<>0 ',
  ') ',
  'AND (vmi.SdoVrtPeca > 0 ',
  // Precisa?
  //'OR vmi.SdoVrtArM2 > 0 ',
  ') ',
  'AND vmi.Pallet=' + Geral.FF0(Pallet),
  'ORDER BY DataHora, Pallet',
  '']);
end;

{
procedure TUnVS_PF.ReopenVSOpePrcAtu(Qry: TmySQLQuery; MovimCod,
  Controle, TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  SQL_Flds := Geral.ATS([
  SQL_NO_GGX(),
  SQL_NO_FRN(),
  SQL_NO_SCL(),
  'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome)NO_FORNEC_MO, ',
  'IF(vmi.Ficha=0, "V�rias", CONCAT(IF(vsf.Nome IS NULL, ',
  '"?", vsf.Nome), " ", vmi.Ficha)) NO_FICHA, ',
  'IF(PesoKg=0, 0, ValorT / PesoKg) CUSTO_KG, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX(),
  SQL_LJ_FRN(),
  SQL_LJ_SCL(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  fmo ON fmo.Codigo=vmi.FornecMO',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet, Controle ',
  '']);
  //Geral.MB_Teste(Qry);
  Qry.Locate('Controle', Controle, []);
end;
}

{
procedure TUnVS_PF.ReopenVSOpePrcForcados(Qry: TmySQLQuery; Controle, SrcNivel1,
  SrcNivel2, TemIMEIMrt: Integer; SrcMovID: TEstqMovimID);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  //TemIMEIMrt: Integer;
begin
  //TemIMEIMrt := QrVSOpeCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  SQL_NO_GGX(),
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
(*
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod), //QrVSOpeCabMovimCod.Value),
  'AND vmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)), //emidEmOperacao)),  // 11
  'AND vmi.SrcNivel1=' + Geral.FF0(SrcNivel1), //QrVSOpeCabCodigo.Value),
*)
  'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)), //emidEmOperacao)),  // 11
  'AND vmi.SrcNivel1=' + Geral.FF0(SrcNivel1), //QrVSOpeCabCodigo.Value),
  'AND vmi.SrcNivel2=' + Geral.FF0(SrcNivel2), //QrVSOpeCabCodigo.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //
  //Geral.MB_Teste(Qry);
  Qry.Locate('Controle', Controle, []);
end;
}

{
procedure TUnReopenVSOpePrcOriIMEI(Qry: TmySQLQuery; MovimCod, Controle,
  TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  //TemIMEIMrt := QrVSOpeCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  SQL_NO_GGX(),
  SQL_NO_FRN(),
  SQL_NO_CMO(),
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet, ',
  '-1.000 FatorImp ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX(),
  SQL_LJ_FRN(),
  SQL_LJ_CMO(),
  'LEFT JOIN vspalleta   vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch    vsf ON vsf.Codigo=vmi.SerieFch ',
  '']);
  SQL_Wher := Geral.ATS([
  //'WHERE vmi.MovimCod=' + Geral.FF0(QrVSOpeCabMovimCod.Value),
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  '']);
  SQL_Group := '';
  //UnDmkDAC_PF.AbreMySQLQuery0(QrVSOpeOriIMEI, Dmod.MyDB, [
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //Geral.MB_Teste(Qry);
  //
  Qry.Locate('Controle', Controle, []);
  //
end;
}

procedure TUnVS_PF.ReopenVSOpePrcOriIMEI_Sum(Qry: TmySQLQuery; MovimCod,
  TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT  SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
  'SUM(AreaM2) AreaM2 ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  '']);
  //Geral.MB_Teste(Qry);
end;

(*
procedure TUnVS_PF.ReopenVSOpeOriIMEI(Qry: TmySQLQuery; MovimCod, Controle: Integer;
  SQL_Limit: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
  'FROM v s m o v i t s vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcOper)),
  'ORDER BY NO_Pallet, vmi.Controle ',
  SQL_Limit,
  '']);
  //Geral.MB_Teste(Qry);
  //
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;

procedure TUnVS_PF.ReopenVSOpeOriPallet(Qry: TmySQLQuery; MovimCod,
  Pallet: Integer; SQL_Limit: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT vmi.Pallet, vmi.GraGruX, vmi.Ficha, ',
  'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, SUM(PesoKg) PesoKg,',
  'vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet',
  'FROM v s m o v i t s vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcOper)),
  'GROUP BY Pallet',
  'ORDER BY NO_Pallet',
  '']);
  //
  if Pallet <> 0 then
    Qry.Locate('Pallet', Pallet, []);
end;
*)

procedure TUnVS_PF.ReopenVSPalletEntradasValidas(Qry: TmySQLQuery;
  Pallet: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2 ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE Pecas > 0 ',
  'AND MovimID <> ' + Geral.FF0(Integer(emidPreReclasse)), // 15
  'AND Pallet=' + Geral.FF0(Pallet),
  '']);
end;

procedure TUnVS_PF.ReopenVSPaMulIts(QrVSPaMulIts: TmySQLQuery; TemIMEIMrt, MovimCod, Controle: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
{
  TemIMEIMrt: Integer;
}
begin
  //VS_PF.ReopenVSOpeOriIMEI(QrVSPWEOriIMEI, QrVSPWECabMovimCod.Value, Controle);
  //TemIMEIMrt := QrVSPaMulCabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  '']);
  SQL_Wher := Geral.ATS([
  //'WHERE vmi.MovimCod=' + Geral.FF0(QrVSPaMulCabMovimCod.Value),
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND MovimNiv=' + Geral.FF0(Integer(eminDestClass)), //2
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaMulIts, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //
  QrVSPaMulIts.Locate('Controle', Controle, []);
end;

{
procedure TUnVS_PF.ReopenVSPrcPrcDst(Qry: TmySQLQuery; SrcNivel1, MovimCod,
  Controle, TemIMEIMrt: Integer; SrcMovID: (*array of *)TEstqMovimID;
  DstMovimNiv: TEstqMovimNiv; SQL_Limit: String = '');
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  ATT_MovimID, ATT_MovimNiv: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  VS_PF.SQL_NO_FRN(),
  VS_PF.SQL_NO_SCL(),
  ATT_MovimID,
  ATT_MovimNiv,
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  VS_PF.SQL_LJ_FRN(),
  VS_PF.SQL_LJ_SCL(),
  'LEFT JOIN vspalleta  vsp  ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf  ON vsf.Codigo=vmi.SerieFch ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE (',
  'vmi.SrcMovID=' + Geral.FF0(Integer(SrcMovID)),
  //'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.SrcNivel1=' + Geral.FF0(SrcNivel1),
  ') or ( ',
  'vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(DstMovimNiv)),
  ') ',
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet, Controle ',
  '']);
  //Geral.MB_Teste(Qry);
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;
}

(*
procedure TUnVS_PF.ReopenVSPrcPrcDst(Qry: TmySQLQuery; SrcNivel1, Controle,
  TemIMEIMrt: Integer; SrcMovID: array of TEstqMovimID; SQL_Limit: String);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  ATT_MovimID, ATT_MovimNiv, SQL_MovID: String;
  I: Integer;
begin
  SQL_MovID := '';
  for I := Low(SrcMovID) to High(SrcMovID) do
  begin
    if SQL_MovID <> '' then
      SQL_MovID := SQL_MovID + ',';
    SQL_MovID := SQL_MovID + Geral.FF0(Integer(SrcMovID));
  end;
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  VS_PF.SQL_NO_FRN(),
  VS_PF.SQL_NO_SCL(),
  ATT_MovimID,
  ATT_MovimNiv,
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  VS_PF.SQL_LJ_FRN(),
  VS_PF.SQL_LJ_SCL(),
  'LEFT JOIN vspalleta  vsp  ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf  ON vsf.Codigo=vmi.SerieFch ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.SrcMovID IN (' + SQL_MovID + ')',
  'AND vmi.SrcNivel1=' + Geral.FF0(SrcNivel1),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  GeraSQLVSMovItx_IMEI(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  GeraSQLVSMovItx_IMEI(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet, Controle ',
  '']);
  //Geral.MB_Teste(Qry);
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;
*)

procedure TUnVS_PF.ReopenVSSubPrdIts(Qry: TmySQLQuery; GSPInnNiv2, TemIMEIMrt,
  Controle: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  SQL_Flds := Geral.ATS([
  VS_PF.SQL_NO_GGX(),
  'vsf.Nome NO_SerieFch, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CAST(pal.SeqSifDipoa AS SIGNED) SeqSifDipoa  ', // 2023-12-31
  '']);
  SQL_Left := Geral.ATS([
  VS_PF.SQL_LJ_GGX(),
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN vspalleta  pal ON pal.Codigo=vmi.Pallet ', // 2023-12-31
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.GSPSrcNiv2=' + Geral.FF0(GSPInnNiv2),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY DataHora, Controle ',
  '']);
  //Geral.MB_Teste(Qry.SQL.Text);
  //
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;

(*
procedure TUnVS_PF.ReopenVSPWEAtu(Qry: TmySQLQuery; MovimCod,
  Controle, TemIMEIMrt: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT vmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,',
  'IF(vmi.Terceiro=0, "V�rios", ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
  ') NO_FORNECE, ',
  'IF(vmi.Ficha=0, "V�rias", CONCAT(IF(vsf.Nome IS NULL, ',
  '"?", vsf.Nome), " ", vmi.Ficha)) NO_FICHA, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ',
  'FROM v s m o v i t s vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminEmWEndInn)),
  'ORDER BY NO_Pallet, vmi.Controle ',
  '']);
  //
  Qry.Locate('Controle', Controle, []);
end;
*)
(*
procedure TUnVS_PF.ReopenVSPWEDst(Qry: TmySQLQuery; MovimCod, Controle, TemIMEIMrt: Integer;
  SQL_Limit: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
  'FROM v s m o v i t s vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestWEnd)),
  'ORDER BY NO_Pallet, vmi.Controle ',
  SQL_Limit,
  '']);
  //
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;
*)

(*
procedure TUnVS_PF.ReopenVSPWEOriIMEI(Qry: TmySQLQuery; MovimCod,
  Controle: Integer; SQL_Limit: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
  FROM v s m o v i t s vmi
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcWEnd)),
  'ORDER BY NO_Pallet, vmi.Controle ',
  SQL_Limit,
  '']);
  //Geral.MB_Teste(Qry);
  //
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;

procedure TUnVS_PF.ReopenVSPWEOriPallet(Qry: TmySQLQuery; MovimCod,
  Pallet: Integer; SQL_Limit: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT vmi.Pallet, vmi.GraGruX, vmi.Ficha, ',
  'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
  'SUM(AreaP2) AreaP2, SUM(PesoKg) PesoKg,',
  'vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet',
  FROM v s m o v i t s vmi
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcWEnd)),
  'GROUP BY Pallet',
  'ORDER BY NO_Pallet',
  '']);
  //
  if Pallet <> 0 then
    Qry.Locate('Pallet', Pallet, []);
end;
*)

function TUnVS_PF.SelecionaStatusVSVmcWrn(Query: TmySQLQuery): Boolean;
(*
const
  Aviso  = '...';
  Titulo = 'Sele��o de Status de Informa��es de Movimento';
  Prompt = 'Informe o Status';
  Campo  = 'Descricao';
var
  Resp: Variant;
begin
  Result := False;
  Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, VSVmcWrn, [
    'SELECT Codigo, Nome Descricao ',
    'FROM vsvmcwrn ',
    'ORDER BY Descricao ',
    ''], Dmod.MyDB, True);
  if Resp <> Null then
  begin
    VSVmcWrn := Resp;
    Result := True;
  end;
*)
var
  MovimCod, NFe: Integer;
  Tabela, Campo: String;
  EstqMovimID: TEstqMovimID;
begin
  if DBCheck.CriaFm(TFmVSVmcObs, FmVSVmcObs, afmoNegarComAviso) then
  begin
    FmVSVmcObs.ImgTipo.SQLType := stUpd;
    //
    FmVSVmcObs.FTabela   := Query.FieldByName('Tabela').AsString;
    FmVSVmcObs.FCampo    := Query.FieldByName('Campo').AsString;
    FmVSVmcObs.FMovimCod := Query.FieldByName('MovimCod').AsInteger;
    FmVSVmcObs.FNFe      := Query.FieldByName('NFe').AsInteger;
    //FmVSVmcObs.FMovimID  := TEstqMovimID(Query.FieldByName('MovimID').AsInteger;
    FmVSVmcObs.FMovimID  := Trunc(Query.FieldByName('MovimID').AsFloat);
    FmVSVmcObs.FQuery    := Query;
    //
    FmVSVmcObs.EdVSVmcWrn.ValueVariant := Query.FieldByName('VSVmcWrn').AsInteger;
    FmVSVmcObs.CBVSVmcWrn.KeyValue     := Query.FieldByName('VSVmcWrn').AsInteger;
    FmVSVmcObs.EdVSVmcSeq.ValueVariant := Query.FieldByName('VSVmcSeq').AsString;
    if Trim(Query.FieldByName('VSVmcObs').AsString) <> '' then
      FmVSVmcObs.EdVSVmcObs.ValueVariant := Query.FieldByName('VSVmcObs').AsString
    else
      FmVSVmcObs.EdVSVmcSeq.Text :=
      Geral.FDT(Query.FieldByName('Data').AsDateTime, 1) + ' p�g ?';
    FmVSVmcObs.CkVSVmcSta.Checked := Query.FieldByName('VSVmcSta').AsInteger = 1;
    //
    FmVSVmcObs.ShowModal;
    FmVSVmcObs.Destroy;
  end;
end;

function TUnVS_PF.SQL_MovIDeNiv_Pos_All(): String;
begin
  AdicionarNovosVS_emid();
  Result := Geral.ATS([
    '   (MovimID =  6 AND MovimNiv = 13)',
    'OR (MovimID =  7 AND MovimNiv =  2)',
    'OR (MovimID =  8 AND MovimNiv =  2)',
    //'OR (MovimID = 11 AND MovimNiv =  8)',   // ???
    'OR (MovimID = 11 AND MovimNiv =  9)',
    'OR (MovimID = 13 AND MovimNiv =  0)',
    'OR (MovimID = 14 AND MovimNiv =  2)',
    'OR (MovimID = 15 AND MovimNiv = 12)',
    'OR (MovimID = 16 AND MovimNiv =  0)',
    //'OR (MovimID = 19 AND MovimNiv = 21)',   // ???
    'OR (MovimID = 20 AND MovimNiv = 22)',
    'OR (MovimID = 21 AND MovimNiv =  0)',
    'OR (MovimID = 22 AND MovimNiv =  0)',
    'OR (MovimID = 23 AND MovimNiv =  0)',
    'OR (MovimID = 24 AND MovimNiv =  2)',
    'OR (MovimID = 25 AND MovimNiv = 28)',
    'OR (MovimID = 26 AND MovimNiv = 30)',
    'OR (MovimID = 27 AND MovimNiv = 35)',
    'OR (MovimID = 28 AND MovimNiv =  2)',
    // 29 >> 2017-11-07
    'OR (MovimID = 29 AND MovimNiv = 31)',
    // 30, 31 ???
    'OR (MovimID = 32 AND MovimNiv = 51)',
    'OR (MovimID = 33 AND MovimNiv = 56)',
    'OR (MovimID = 39 AND MovimNiv = 66)',
    'OR (MovimID = 40 AND MovimNiv = 67)'
  ]);
end;

function TUnVS_PF.TextoDeEstqMovimID(EstqMovimID: TEstqMovimID): String;
begin
  Result := sEstqMovimID[Integer(EstqMovimID)];
end;

procedure TUnVS_PF.VerificaAtzCalDTA(PB: TProgressBar; LaAviso1,
  LaAviso2: TLabel);
var
  Qry1, Qry2: TmySQLQuery;
  Controle, I, GGXJmpDst, SrcNivel2: Integer;
  Corda: String;
begin
  Corda := '';
  Screen.Cursor := crHourGlass;
  GGXJmpDst := 0;
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT cab.GraGruX GGXNew, cab.GGXSrc, vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'LEFT JOIN vscurcab cab ON cab.MovimCod=vmi.MovimCod',
    'WHERE vmi.MovimID=27 ',
    'AND vmi.MovimNiv=34 ',
    'AND vmi.RmsNivel2=0 ',
    'AND ',
    '( ',
    '(vmi.PesoKg <> 0) OR (vmi.Pecas <> 0) ',
    ') ',
    '']);
    I := Qry1.RecordCount;
    if I > 0 then
    begin
      if Geral.MB_Pergunta(
      Qry1.SQL.Text + sLineBreak + sLineBreak +
      '==============================================================' +
      sLineBreak + sLineBreak +
      'Existem ' + Geral.FF0(I) +
      ' pesagens de curtimento sem origem de redescarne / divis�o!'  + sLineBreak +
      'Ser� necess�rio fazer uma atualiza��o!' + sLineBreak +
      'Deseja realiz�-la agora?') <> ID_YES then
        Exit;
    end else
      Exit;
    if PB <> nil then
    begin
      PB.Max := I;
      PB.Position := 0;
    end;
    Qry2 := TmySQLQuery.Create(Dmod);
    try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
    'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
    'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED',
    'FROM gragrux ggx ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed',
    'WHERE ggx.GragruY IN (1621) ' ,
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
    '']);
    if Qry2.RecordCount = 0 then
    begin
      Geral.MB_Aviso('N�o h� couro caleado cadastrado! GraGruY = 1621');
      Exit;
    end;
    if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
    begin
      with FmSelOnStringGrid do
      begin
        Grade.ColCount      := 3;
        Grade.ColWidths[00] := 56;
        Grade.ColWidths[01] := 56;
        Grade.ColWidths[02] := 400;
        Width := 548;
        FColSel := 1;
        Caption := 'CST IPI';
        Grade.Cells[00,00] := 'Seq';
        Grade.Cells[01,00] := 'C�digo';
        Grade.Cells[02,00] := 'Descri��o';
        Grade.RowCount := Qry2.RecordCount + 1;
        while not Qry2.Eof do
        begin
          Grade.Cells[0, Qry2.RecNo] := IntToStr(Qry2.RecNo);
          Grade.Cells[1, Qry2.RecNo] := Geral.FF0(Qry2.FieldByName('Controle').AsInteger);
          Grade.Cells[2, Qry2.RecNo] := Qry2.FieldByName('NO_PRD_TAM_COR').AsString;
          //
          Qry2.Next;
        end;
        ShowModal;
        if FResult <> '' then
          GGXJmpDst := Geral.IMV(FResult);
        Destroy;
      end;
    end;
    if GGXJmpDst = 0 then
    begin
      Geral.MB_Aviso('Atualiza��o abortada! Nenhum reduzido foi selecionado!');
      Exit;
    end;
    Qry1.First;
    while not Qry1.Eof do
    begin
      MyObjects.UpdPB(PB, LaAviso1, LaAviso2);
      Controle  := Qry1.FieldByName('Controle').AsInteger;
      SrcNivel2 := Qry1.FieldByName('SrcNivel2').AsInteger;
      if SrcNivel2 <> 0 then
      begin

        UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
        'SELECT * ',
        'FROM ' + CO_SEL_TAB_VMI + ' ',
        'WHERE Controle=' + Geral.FF0(SrcNivel2),
        '']);
        if Qry2.RecordCount > 0 then
        begin
          //
          if not VS_PF.CriaVMIRmsCur(Controle,
          Qry1.FieldByName('GGXSrc').AsInteger,
          GGXJmpDst,
          Qry1.FieldByName('GGXNew').AsInteger,

          -Qry2.FieldByName('Pecas').AsFloat,
          -Qry2.FieldByName('AreaM2').AsFloat,
          -Qry2.FieldByName('PesoKg').AsFloat,

          -Qry1.FieldByName('Pecas').AsFloat,
          -Qry1.FieldByName('AreaM2').AsFloat,
          -Qry1.FieldByName('PesoKg').AsFloat,
          -Qry1.FieldByName('ValorT').AsFloat,

          Qry2.FieldByName('StqCenLoc').AsInteger) then
          begin
            // ini 2023-11-15
            if Geral.MB_Pergunta('N�o foi poss�vel corrigir o IME-I '+
            Geral.FF0(Controle) + '!' + sLineBreak +
            'Deseja evit�-lo em nova pesquisa?') = ID_YES then
            begin
              UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
              'UPDATE vsmovits SET ',
              'RmsNivel2=-1 ',
              'WHERE Controle=' + Geral.FF0(Controle),
              'AND RmsNivel2=0',
              '']));
            end;
            //Exit;
            // fim 2023-11-15
          end;
          //
        end else
        begin
          if Trim(Corda) <> '' then Corda := Corda + ', ';
          Corda := Corda + Geral.FF0(Qry1.FieldByName('Controle').AsInteger) + '';
        end;
      end;
      Qry1.Next;
    end;
  finally
    Qry2.Free;
  end;
  finally
    Qry1.Free;
    Screen.Cursor := crDefault;
  end;
  if Trim(Corda) <> '' then
    Geral.MB_Aviso('N�o foi poss�vel setar o "RmsNivel2" dos seguintes IME-Is:' +
    sLineBreak + Corda + slineBreak + 'Solicite � DERMATEK a anula��o: ' +
    sLineBreak + '' + CO_UPD_TAB_VMI + ' SET RmsNivel2=-1 WHERE Controle IN (' +
    Corda + ')' + sLineBreak);
end;

procedure TUnVS_PF.VerificaAtzCaleados(PB: TProgressBar;
LaAviso1, LaAviso2: TLabel);
var
  Qry1, Qry2: TmySQLQuery;
  Controle, I, GGXJmpDst, SrcNivel2: Integer;
begin
  Screen.Cursor := crHourGlass;
  GGXJmpDst := 0;
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT cab.GraGruX GGXNew, cab.GGXSrc, vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'LEFT JOIN vscurcab cab ON cab.MovimCod=vmi.MovimCod',
    'WHERE vmi.MovimID=27 ',
    'AND vmi.MovimNiv=34 ',
    'AND vmi.JmpNivel2=0 ',
    'AND ',
    '(',
    '(vmi.PesoKg <> 0) OR (vmi.Pecas <> 0)',
    ')',
    '']);
    I := Qry1.RecordCount;
    if I > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(I) +
      ' pesagens de curtimento sem origem de caleado!'  + sLineBreak +
      'Ser� necess�rio fazer uma atualiza��o!' + sLineBreak +
      'Deseja realiz�-la agora?' + sLineBreak +
      Qry1.SQL.Text) <> ID_YES then
        Exit;
    end else
      Exit;
////////////////////////////////////////////////////////////////////////////////
    //Geral.MB_Info('Em desenvolvimento! Ver MovCodPai!!');
    //Exit;
////////////////////////////////////////////////////////////////////////////////
    if PB <> nil then
    begin
      PB.Max := I;
      PB.Position := 0;
    end;
    Qry2 := TmySQLQuery.Create(Dmod);
    try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
    'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
    'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED',
    'FROM gragrux ggx ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed',
    'WHERE ggx.GragruY IN (1536) ' ,
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
    '']);
    if Qry2.RecordCount = 0 then
    begin
      Geral.MB_Aviso('N�o h� couro caleado cadastrado! GraGruY = 1536');
      Exit;
    end;
    if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
    begin
      with FmSelOnStringGrid do
      begin
        Grade.ColCount      := 3;
        Grade.ColWidths[00] := 56;
        Grade.ColWidths[01] := 56;
        Grade.ColWidths[02] := 400;
        Width := 548;
        FColSel := 1;
        Caption := 'CST IPI';
        Grade.Cells[00,00] := 'Seq';
        Grade.Cells[01,00] := 'C�digo';
        Grade.Cells[02,00] := 'Descri��o';
        Grade.RowCount := Qry2.RecordCount + 1;
        while not Qry2.Eof do
        begin
          Grade.Cells[0, Qry2.RecNo] := IntToStr(Qry2.RecNo);
          Grade.Cells[1, Qry2.RecNo] := Geral.FF0(Qry2.FieldByName('Controle').AsInteger);
          Grade.Cells[2, Qry2.RecNo] := Qry2.FieldByName('NO_PRD_TAM_COR').AsString;
          //
          Qry2.Next;
        end;
        ShowModal;
        if FResult <> '' then
          GGXJmpDst := Geral.IMV(FResult);
        Destroy;
      end;
    end;
    if GGXJmpDst = 0 then
    begin
      Geral.MB_Aviso('Atualiza��o abortada! Nenhum reduzido foi selecionado!');
      Exit;
    end;
    Qry1.First;
    while not Qry1.Eof do
    begin
      MyObjects.UpdPB(PB, LaAviso1, LaAviso2);
      Controle  := Qry1.FieldByName('Controle').AsInteger;
      SrcNivel2 := Qry1.FieldByName('SrcNivel2').AsInteger;
      if SrcNivel2 <> 0 then
      begin

        UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
        'SELECT * ',
        'FROM ' + CO_SEL_TAB_VMI + ' ',
        'WHERE Controle=' + Geral.FF0(SrcNivel2),
        '']);
        if Qry2.RecordCount > 0 then
        begin
          //
          if CriaVMIJmpCal(Controle,
          Qry1.FieldByName('GGXSrc').AsInteger,
          GGXJmpDst,
          Qry1.FieldByName('GGXNew').AsInteger,

          -Qry2.FieldByName('Pecas').AsFloat,
          -Qry2.FieldByName('AreaM2').AsFloat,
          -Qry2.FieldByName('PesoKg').AsFloat,

          -Qry1.FieldByName('Pecas').AsFloat,
          -Qry1.FieldByName('AreaM2').AsFloat,
          -Qry1.FieldByName('PesoKg').AsFloat,
          -Qry1.FieldByName('ValorT').AsFloat,

          Qry2.FieldByName('StqCenLoc').AsInteger,
          0(*MovCodPai da para saber?*),
          0(*VmiPai da para saber?*)) = False then
          begin
            // ini 2023-11-15
            if Geral.MB_Pergunta('N�o foi poss�vel corrigir o IME-I '+
            Geral.FF0(Controle) + '!' + sLineBreak +
            'Deseja evit�-lo em nova pesquisa?') = ID_YES then
            begin
              UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
              'UPDATE vsmovits SET ',
              'JmpNivel2=-1 ',
              'WHERE Controle=' + Geral.FF0(Controle),
              'AND JmpNivel2=0',
              '']));
            end;
          end;
          //
        end;
      end;
      Qry1.Next;
    end;
  finally
    Qry2.Free;
  end;
  finally
    Qry1.Free;
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnVS_PF.VerificaAtzCalPDA(PB: TProgressBar; LaAviso1,
  LaAviso2: TLabel);
var
  Qry1, Qry2: TmySQLQuery;
  Controle, I, GGXJmpDst, SrcNivel2: Integer;
  Corda: String;
begin
  Corda := '';
  Screen.Cursor := crHourGlass;
  GGXJmpDst := 0;
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT cab.Codigo, cab.MovimCod, ',
    'cab.GraGruX GGXNew, cab.GGXSrc, vmi.* ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'LEFT JOIN vscalcab cab ON cab.MovimCod=vmi.MovimCod',
    'WHERE vmi.MovimID=26 ',
    'AND vmi.MovimNiv=29 ',
    'AND vmi.RmsNivel2=0 ',
    'AND ',
    '(',
    '(vmi.PesoKg <> 0) OR (vmi.Pecas <> 0)',
    ')',
    '']);
    I := Qry1.RecordCount;
    if I > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(I) +
      ' pesagens de caleiro sem origem de pr�-descarne!'  + sLineBreak +
      'Ser� necess�rio fazer uma atualiza��o!' + sLineBreak +
      'Deseja realiz�-la agora?' + sLineBreak +
      Qry1.SQL.Text) <> ID_YES then
        Exit;
    end else
      Exit;
    if PB <> nil then
    begin
      PB.Max := I;
      PB.Position := 0;
    end;
    Qry2 := TmySQLQuery.Create(Dmod);
    try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
    'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
    'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED',
    'FROM gragrux ggx ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed',
    'WHERE ggx.GragruY IN (1195) ' ,
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
    '']);
    if Qry2.RecordCount = 0 then
    begin
      Geral.MB_Aviso('N�o h� couro caleado cadastrado! GraGruY = 1195');
      Exit;
    end;
    if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
    begin
      with FmSelOnStringGrid do
      begin
        Grade.ColCount      := 3;
        Grade.ColWidths[00] := 56;
        Grade.ColWidths[01] := 56;
        Grade.ColWidths[02] := 400;
        Width := 548;
        FColSel := 1;
        Caption := 'CST IPI';
        Grade.Cells[00,00] := 'Seq';
        Grade.Cells[01,00] := 'C�digo';
        Grade.Cells[02,00] := 'Descri��o';
        Grade.RowCount := Qry2.RecordCount + 1;
        while not Qry2.Eof do
        begin
          Grade.Cells[0, Qry2.RecNo] := IntToStr(Qry2.RecNo);
          Grade.Cells[1, Qry2.RecNo] := Geral.FF0(Qry2.FieldByName('Controle').AsInteger);
          Grade.Cells[2, Qry2.RecNo] := Qry2.FieldByName('NO_PRD_TAM_COR').AsString;
          //
          Qry2.Next;
        end;
        ShowModal;
        if FResult <> '' then
          GGXJmpDst := Geral.IMV(FResult);
        Destroy;
      end;
    end;
    if GGXJmpDst = 0 then
    begin
      Geral.MB_Aviso('Atualiza��o abortada! Nenhum reduzido foi selecionado!');
      Exit;
    end;
    Qry1.First;
    while not Qry1.Eof do
    begin
      MyObjects.UpdPB(PB, LaAviso1, LaAviso2);
      Controle  := Qry1.FieldByName('Controle').AsInteger;
      SrcNivel2 := Qry1.FieldByName('SrcNivel2').AsInteger;
      if SrcNivel2 <> 0 then
      begin

        UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
        'SELECT * ',
        'FROM ' + CO_SEL_TAB_VMI + ' ',
        'WHERE Controle=' + Geral.FF0(SrcNivel2),
        '']);
        if Qry2.RecordCount > 0 then
        begin
          //
          if not CriaVMIRmsCal(Controle,
          Qry1.FieldByName('GGXSrc').AsInteger,
          GGXJmpDst,
          Qry1.FieldByName('GGXNew').AsInteger,

          Qry2.FieldByName('Pecas').AsFloat,
          Qry2.FieldByName('AreaM2').AsFloat,
          Qry2.FieldByName('PesoKg').AsFloat,

          Qry1.FieldByName('QtdAntPeca').AsFloat,
          Qry1.FieldByName('QtdAntArM2').AsFloat,
          Qry1.FieldByName('QtdAntPeso').AsFloat,
          -Qry1.FieldByName('ValorT').AsFloat,

          Qry2.FieldByName('StqCenLoc').AsInteger,
          // ini 2021-10-24
          Qry1.FieldByName('Codigo').AsInteger,
          Qry1.FieldByName('MovimCod').AsInteger,
          // fim 2021-10-24
          // ini 2023-11-20 Baixa de sub produto por peso
          // est� certo??? Nunca testado!!!
          Qry2.FieldByName('PesoKg').AsFloat
          // fim 2023-11-20 Baixa de sub produto por peso
          ) then
          begin
            // ini 2023-11-15
            if Geral.MB_Pergunta('N�o foi poss�vel corrigir o IME-I '+
            Geral.FF0(Controle) + '!' + sLineBreak +
            'Deseja evit�-lo em nova pesquisa?') = ID_YES then
            begin
              UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
              'UPDATE vsmovits SET ',
              'RmsNivel2=-1 ',
              'WHERE Controle=' + Geral.FF0(Controle),
              'AND RmsNivel2=0',
              '']));
            end;
            //Exit;
            // fim 2023-11-15
          end;
          //
        end else
        begin
          if Trim(Corda) <> '' then Corda := Corda + ', ';
          Corda := Corda + Geral.FF0(Qry1.FieldByName('Controle').AsInteger) + '';
        end;
      end;
      Qry1.Next;
    end;
  finally
    Qry2.Free;
  end;
  finally
    Qry1.Free;
    Screen.Cursor := crDefault;
  end;
  if Trim(Corda) <> '' then
  begin
    if Geral.MB_Pergunta('N�o foi poss�vel setar o "RmsNivel2" dos seguintes IME-Is:' +
    sLineBreak + Corda + slineBreak + 'Deseja sua exclus�o de futura pesquisa? ' +
    sLineBreak) = ID_YES then
    begin
      UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
      'UPDATE ' + CO_UPD_TAB_VMI + ' SET ',
      'RmsNivel2=-1 ',
      'WHERE Controle IN (' + Corda + ')',
      'AND RmsNivel2=0']));
    end;
  end;
end;

procedure TUnVS_PF.VerificaAtzCurtidos(PB: TProgressBar; LaAviso1,
  LaAviso2: TLabel);
const
  sProcName = 'VS_PF.VerificaAtzCurtidos()';
  //
  Nome = '';
  TipoArea = -1;
  NFeRem = -1;
  SerieRem = 0;
  LPFMO = '';
  SQLType = stIns;
var
  Qry1, Qry2: TmySQLQuery;
  //Controle, GGXJmpDst, SrcNivel2:
  I, Codigo, MovimCod, Empresa: Integer;
  DtHrAberto, DtHrFimOpe: String;
begin
  Screen.Cursor := crHourGlass;
  //GGXJmpDst := 0;
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT cur.* ',
    'FROM vscurcab cur ',
    'LEFT JOIN vscurjmp jmp ON jmp.Codigo=cur.Codigo ',
    'WHERE jmp.Codigo IS NULL ',
    '']);
    I := Qry1.RecordCount;
    if I > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(I) +
      ' pesagens de curtimento sem destino de curtido!'  + sLineBreak +
      'Ser� necess�rio fazer uma atualiza��o!' + sLineBreak +
      'Deseja realiz�-la agora?') <> ID_YES then
        Exit;
    end else
      Exit;
    if PB <> nil then
    begin
      PB.Max := I;
      PB.Position := 0;
    end;
{
    Qry2 := TmySQLQuery.Create(Dmod);
    try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
    'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
    'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED',
    'FROM gragrux ggx ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed',
    'WHERE ggx.GragruY IN (1877) ' ,
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
    '']);
    if Qry2.RecordCount = 0 then
    begin
      Geral.MB_Aviso('N�o h� couro curtido cadastrado! GraGruY = 1877');
      Exit;
    end;
    if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
    begin
      with FmSelOnStringGrid do
      begin
        Grade.ColCount      := 3;
        Grade.ColWidths[00] := 56;
        Grade.ColWidths[01] := 56;
        Grade.ColWidths[02] := 400;
        Width := 548;
        FColSel := 1;
        Caption := 'CST IPI';
        Grade.Cells[00,00] := 'Seq';
        Grade.Cells[01,00] := 'C�digo';
        Grade.Cells[02,00] := 'Descri��o';
        Grade.RowCount := Qry2.RecordCount + 1;
        while not Qry2.Eof do
        begin
          Grade.Cells[0, Qry2.RecNo] := IntToStr(Qry2.RecNo);
          Grade.Cells[1, Qry2.RecNo] := Geral.FF0(Qry2.FieldByName('Controle').AsInteger);
          Grade.Cells[2, Qry2.RecNo] := Qry2.FieldByName('NO_PRD_TAM_COR').AsString;
          //
          Qry2.Next;
        end;
        ShowModal;
        if FResult <> '' then
          GGXJmpDst := Geral.IMV(FResult);
        Destroy;
      end;
    end;
    if GGXJmpDst = 0 then
    begin
      Geral.MB_Aviso('Atualiza��o abortada! Nenhum reduzido foi selecionado!');
      Exit;
    end;
    }
    Qry1.First;
    while not Qry1.Eof do
    begin
      MyObjects.UpdPB(PB, LaAviso1, LaAviso2);
      //
      Codigo     := Qry1.FieldByName('Codigo').AsInteger;
      Empresa    := Qry1.FieldByName('Empresa').AsInteger;
      DtHrAberto := Geral.FDT(Qry1.FieldByName('DtHrAberto').AsDateTime, 1);
      DtHrFimOpe := Geral.FDT(Qry1.FieldByName('DtHrFimOpe').AsDateTime, 1);
      //
      MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
      //
      VS_PF.InsereVSMovCab(MovimCod, emidCurtido, Codigo);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vscurjmp', False, [
      'MovimCod', 'Empresa',
      'DtHrAberto', 'DtHrFimOpe',
      'Nome', 'TipoArea', (*'GraGruX',*)
      'NFeRem', 'SerieRem', 'LPFMO'(*,
      'GGXDst'*)], [
      'Codigo'], [
      MovimCod, Empresa,
      DtHrAberto, DtHrFimOpe,
      Nome, TipoArea, (*GraGruX,*)
      NFeRem, SerieRem, LPFMO(*,
      GGXInn*)], [
      Codigo], True);
      Qry1.Next;
    end;
{
  finally
    Qry2.Free;
  end;
}
  finally
    Qry1.Free;
    Screen.Cursor := crDefault;
  end;
end;

function TUnVS_PF.VerificaBalanco(MesesAntes: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(AnoMes) AnoMes ',
    //'FROM vsbalcab ',
    'FROM spedefdicmsipience ',
    'WHERE MovimXX=1 ', // 1=Feito
    '']);
    Result := Geral.AnoMesToPeriodo(Qry.FieldByName('AnoMes').AsInteger) -
      MesesAntes - 1;
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_PF.VerificaCadastroVSEntiMPIncompleto();
const
  Aviso = 'Existem fornecedores de mat�ria prima com cadastro incompleto!';
var
  Qry: TmySQLQuery;
begin
  if Dmod = nil then
    Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ent.Codigo,  ',
    'CONCAT(IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome), " (", ',
    '  IF(vem.Codigo IS NULL, "?????", vem.SiglaVS), ")") NO_Ent',
    'FROM entidades ent  ',
    'LEFT JOIN vsentimp vem ON ent.Codigo=vem.Codigo ',
    'WHERE ent.Fornece1="V" ',
    'AND vem.Codigo IS NULL',
    'ORDER BY Codigo',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Geral.MB_Aviso(Aviso);
      //
      if DBCheck.CriaFm(TFmEntiIncompletos, FmEntiIncompletos, afmoNegarComAviso) then
      begin
        FmEntiIncompletos.ReopenIncompleto(Qry);
        MyObjects.Informa2(FmEntiIncompletos.LaAviso1,
          FmEntiIncompletos.LaAviso2, False, Aviso);
        FmEntiIncompletos.ShowModal;
        FmEntiIncompletos.Destroy;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnVS_PF.VerificaCalCurDstGGX(LaAviso1, LaAviso2: TLabel;
PB1: TProgressBar): Boolean;
var
  Qry1, Qry2: TmySQLQuery;
  MovimNiv, MovimCod, Controle, DstMovID, DstNivel1, DstNivel2,DstGGX: Integer;
  Periodo: String;
  //
  procedure ReabreQry1VMI();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    //'WHERE MovimNiv IN (29,34) ', // eminSorcCal e eminSorcCur
    'WHERE MovimNiv IN (' +
      Geral.FF0(Integer(eminSorcCal)) + ',' +
      Geral.FF0(Integer(eminSorcCur)) + ')',
    'AND DstGGX=0 ',
    'ORDER BY DataHora ',
    '']);
  end;
  procedure ReabreQry1Cal();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT cab.*  ',
    'FROM vscalcab cab ',
    'WHERE cab.GGXDst=0',
    '']);
  end;
  procedure ReabreQry1Cur();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT cab.*  ',
    'FROM vscurcab cab ',
    'WHERE cab.GGXDst=0',
    '']);
  end;
begin
  Result := False;
  //
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    ReabreQry1VMI();
    if Qry1.RecordCount > 0 then
    begin
      Periodo := Geral.FDT(Qry1.FieldByName('DataHora').AsDateTime, 2);
      Qry1.Last;
      Periodo := Periodo + ' at� ' + Geral.FDT(Qry1.FieldByName('DataHora').AsDateTime, 2);
      Qry1.First;
      if Geral.MB_Pergunta(
      'Existem registros de movimenta��o de couro no caleiro e curtimento sem informa��o de reduzido de destino.' +
      sLineBreak + 'Per�odo: ' + Periodo + sLineBreak +
      'Deseja corrigir agora?') <> ID_YES then
        Exit;
      //
      Qry2 := TmySQLQuery.Create(Dmod);
      try
        Qry1.First;
        PB1.Position := 0;
        PB1.Max := Qry1.RecordCount;
        while not Qry1.Eof do
        begin
          MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
          'Corrigindo movimenta��o de couro no caleiro e curtimento');
          //
          Controle := Qry1.FieldByName('Controle').AsInteger;
          MovimCod := Qry1.FieldByName('MovimCod').AsInteger;
          MovimNiv := Qry1.FieldByName('MovimNiv').AsInteger + 1; // 29>30  e 34>35
          UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
          'SELECT GraGruX, MovimID, Codigo, Controle ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE MovimNiv=' + Geral.FF0(MovimNiv), // eminEmCalInn ou eminEmCurInn
          'AND MovimCod=' + Geral.FF0(MovimCod),
          '']);
          if Qry2.RecordCount > 0 then
          begin
            DstGGX    := Qry2.FieldByName('GraGruX').AsInteger;
            DstMovID  := Qry2.FieldByName('MovimID').AsInteger;
            DstNivel1 := Qry2.FieldByName('Codigo').AsInteger;
            DstNivel2 := Qry2.FieldByName('Controle').AsInteger;
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
            'DstMovID', 'DstNivel1', 'DstNivel2', 'DstGGX'], ['Controle'], [
            DstMovID, DstNivel1, DstNivel2, DstGGX], [Controle], True);
          end;
          //
          Qry1.Next;
        end;
      finally
        Qry2.Free;
      end;
      PB1.Position := 0;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end;

    //////////////////////////////////////////////////////////

    ReabreQry1Cal();
    if Qry1.RecordCount > 0 then
    begin
      Periodo := Geral.FDT(Qry1.FieldByName('DtHrAberto').AsDateTime, 2);
      Qry1.Last;
      Periodo := Periodo + ' at� ' + Geral.FDT(Qry1.FieldByName('DtHrAberto').AsDateTime, 2);
      Qry1.First;
      //
      if Geral.MB_Pergunta(
      'Existem cabe�alhos de processo de caleiro sem informa��o de reduzido de destino.' +
      sLineBreak + 'Per�odo: ' + Periodo + sLineBreak +
      sLineBreak + 'Deseja corrigir agora?') <> ID_YES then
        Exit;
      Qry1.First;
      PB1.Position := 0;
      PB1.Max := Qry1.RecordCount;
      while not Qry1.Eof do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
        'Corrigindo cabe�alhos de processo de caleiro');
        //
        MovimCod := Qry1.FieldByName('MovimCod').AsInteger;
        DstGGX   := Qry1.FieldByName('GraGruX').AsInteger;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscalcab', False, [
        'GGXDst'], ['MovimCod'], [DstGGX], [MovimCod], True);
        //
        Qry1.Next;
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end;


    //////////////////////////////////////////////////////////

    ReabreQry1Cur();
    if Qry1.RecordCount > 0 then
    begin
      Periodo := Geral.FDT(Qry1.FieldByName('DtHrAberto').AsDateTime, 2);
      Qry1.Last;
      Periodo := Periodo + ' at� ' + Geral.FDT(Qry1.FieldByName('DtHrAberto').AsDateTime, 2);
      Qry1.First;
      //
      if Geral.MB_Pergunta(
      'Existem cabe�alhos de processo de curtimento sem informa��o de reduzido de destino.' +
      sLineBreak + 'Per�odo: ' + Periodo + sLineBreak +
      sLineBreak + 'Deseja corrigir agora?') <> ID_YES then
        Exit;
      //
      Qry1.First;
      PB1.Position := 0;
      PB1.Max := Qry1.RecordCount;
      while not Qry1.Eof do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
        'Corrigindo cabe�alhos de processo de curtimento');
        //
        MovimCod := Qry1.FieldByName('MovimCod').AsInteger;
        DstGGX   := Qry1.FieldByName('GraGruX').AsInteger;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscurcab', False, [
        'GGXDst'], ['MovimCod'], [DstGGX], [MovimCod], True);
        //
        Qry1.Next;
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end;

    //

    ReabreQry1VMI();
    Result := Result or (Qry1.RecordCount = 0);
    ReabreQry1Cal();
    Result := Result or (Qry1.RecordCount = 0);
    ReabreQry1Cur();
    Result := Result or (Qry1.RecordCount = 0);

  finally
    Qry1.Free;
  end;
end;

function TUnVS_PF.VerificaCliForEmpty(): Boolean;
const
  sProcName = 'TUnVS_PF.VerificaCliForEmpty()';
var
  Qry: TmySQLQuery;
  Index: Integer;
  MovimID, MovimNiv: Integer;
  Campo, Filtr: String;
  //
  procedure DefineMenos11(MovimID, MovimNiv: Integer);
  begin
    if Geral.MB_Pergunta('Existem registros de: ' + sLineBreak +

    'MovimID=' + Geral.FF0(MovimID) + ' -> ' +
    GetEnumName(TypeInfo(TEstqMovimID), MovimID) + sLineBreak +

    'MovimNiv=' + Geral.FF0(MovimNiv) + ' -> ' +
    GetEnumName(TypeInfo(TEstqMovimNiv), MovimNiv) + sLineBreak +
    slineBreak + 'Deseja corrigir agora?') = ID_YES then
    begin
      if not DBCheck.LiberaPelaSenhaAdmin then Exit;
      //
        Screen.Cursor := crHourGlass;
      try
        Dmod.MyDB.Execute(
        'UPDATE ' + CO_UPD_TAB_VMI + ' SET FornecMO=-11 WHERE FornecMO=0 AND MovimID=' +
        Geral.FF0(MovimID) + ' AND MovimNiv=' + Geral.FF0(MovimNiv));
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
  //
  procedure ReopenPsq(Fornecedor, Cliente: Boolean);
  var
    SQLF, SQLC, SQLI: String;
  begin
    SQLF := '';
    SQLC := '';
    SQLI := '';
(*
    if Fornecedor then
    begin
      SQLF := '  FMO_SIM <> 0 AND FMO_NAO <> 0 ';
      if Cliente then
        SQLI := ') OR ( ';
    end;
    if Cliente then
      SQLC := '  CMO_SIM <> 0 AND CMO_NAO <> 0 ';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _VS_CLI_FOR_EMPTY_; ',
    'CREATE TABLE _VS_CLI_FOR_EMPTY_ ',
    'SELECT MovimID, MovimNiv,  ',
    'SUM(IF(FornecMO<>0, 1, 0)) FMO_SIM, ',
    'SUM(IF(FornecMO<>0, 0, 1)) FMO_NAO, ',
    'SUM(IF(ClientMO<>0, 1, 0)) CMO_SIM, ',
    'SUM(IF(ClientMO<>0, 0, 1)) CMO_NAO ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI,
    'WHERE NOT (MovimID IN (' +
      Geral.FF0(Integer(emidGeraSubProd)) + ',' +
      Geral.FF0(Integer(emidTransfLoc)) //+ ',' +
      //Geral.FF0(Integer(emidPreReclasse)) + '' +
      + ')) ',
    'GROUP BY MovimID, MovimNiv ',
    'ORDER BY MovimID, MovimNiv; ',
    'SELECT *  ',
    'FROM _VS_CLI_FOR_EMPTY_ ',
    'WHERE ( ',
    SQLF,
    SQLI,
    SQLC,
    '); ',
    '']);
    //Geral.MB_Teste(Qry);
*)
    if Fornecedor then
    begin
      SQLF := '  FMO_NAO <> 0 ';
      if Cliente then
        SQLI := ') OR ( ';
    end;
    if Cliente then
      SQLC := '  CMO_NAO <> 0 ';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _VS_CLI_FOR_EMPTY_;  ',
    'CREATE TABLE _VS_CLI_FOR_EMPTY_  ',
    'SELECT MovimID, MovimNiv,   ',
    'SUM(IF(FornecMO<>0, 1, 0)) FMO_SIM,  ',
    'SUM(IF(FornecMO<>0, 0, 1)) FMO_NAO,  ',
    'SUM(IF(ClientMO<>0, 1, 0)) CMO_SIM,  ',
    'SUM(IF(ClientMO<>0, 0, 1)) CMO_NAO  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
    'WHERE (',
    // ini 2023-05-24
    //'  (FornecMO = 0 AND (NOT MovimID IN (1,2,9,12,16,17,21,22,23,25)))',
    '  (FornecMO = 0 AND (NOT MovimID IN (1,2,9,12,16,17,21,22,23,25,41)))',
    // fim 2023-05-24
    '  OR',
    '  (ClientMO = 0 AND (NOT MovimID IN (23,25)))',
    ')',
    'GROUP BY MovimID, MovimNiv  ',
    'ORDER BY MovimID, MovimNiv;  ',
    'SELECT *   ',
    'FROM _VS_CLI_FOR_EMPTY_  ',
    'WHERE ( ',
    SQLF,
    SQLI,
    SQLC,
    '); ',
    '']);
    //Geral.MB_Teste(Qry);
  end;
  procedure AtualizaAtual(MovimID, MovimViv: Integer; FldDisplay, FldEntSrc, FldUpdNom,
    SQL_Filtro: String);
  const
    Aviso  = '...';
    Help   = '?????';
    CampoCU  = 'CodUsu';
    CampoCO  = 'Codigo';
    CampoNO  = 'Nome';
    DataSource = nil;
    ListSource = nil;
    Default    = 0;
    FldIdxNom  = 'Controle';
    UserDataAlterweb = True;
  var
    //Codigo, Controle, Conta: Integer;
    //EntiContat: Variant;
    Titulo, Prompt: String;
    SQLSorc, SQLDest, ATT_MovimID, ATT_MovimNiv: String;
  begin
    Titulo := 'Sele��o de ' + FldDisplay + ' de M�o de obra (' +
    sEstqMovimID[MovimID] + ' - ' + sEstqMovimNiv[MovimNiv] + ')';
    Prompt := 'Informe o ' + FldDisplay + ' da M�o de obra:';
    SQLSorc := Geral.ATS([
      'SELECT Codigo ' + CampoCU + ', Codigo ' + CampoCO +
      ', IF(Tipo=0, RazaoSocial, Nome) ' + CampoNO,
      'FROM entidades ',
      'WHERE ' + FldEntSrc + '="V" ',
      'ORDER BY ' + CampoNO,
      '']);
      ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
      sEstqMovimID);
      //
      ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
      sEstqMovimNiv);
      //
      SQLDest := Geral.ATS([
      'SELECT vmi.DataHora, vmi.Codigo IME_C, vmi.Controle IME_I, ',
      'vmi.MovimCod OP,  vmi.Empresa, ',
      'vmi.MovimID, ',
      ATT_MovimID,
      'vmi.MovimNiv, ',
      ATT_MovimNiv,
      'vmi.ClientMO, IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_ClientMO, ',
      'vmi.FornecMO, IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NO_FornecMO, ',
      'CONCAT(gg1.Nome, ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
      'NO_PRD_TAM_COR, ',
      'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
      'vmi.SdoVrtPeca, vmi.SdoVrtPeso, vmi.SdoVrtArM2, ',
      'vmi.Pallet, vmi.GraGruX, vmi.SerieFch, vmi.Ficha, ',
      'vmi.Terceiro, IF(trc.Tipo=0, trc.RazaoSocial, trc.Nome) NO_Terceiro, ',
      'vmi.Controle ',
      'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
      'LEFT JOIN gragrux     ggx ON ggx.Controle=vmi.GraGruX ',
      'LEFT JOIN gragruc     ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad   gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits   gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1     gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN entidades   cli ON cli.Codigo=vmi.ClientMO ',
      'LEFT JOIN entidades   fmo ON fmo.Codigo=vmi.FornecMO ',
      'LEFT JOIN entidades   trc ON trc.Codigo=vmi.Terceiro ',
      'LEFT JOIN vsmulfrncab mfc ON mfc.Codigo=vmi.VSMulFrnCab ',
      'LEFT JOIN gragruxcou  xco ON xco.GraGruX=ggx.Controle ',
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
      'WHERE vmi.MovimID=' + Geral.FF0(MovimID),
      'AND vmi.MovimNiv=' + Geral.FF0(MovimNiv),
      //'AND vmi.FornecMO=0 ',
      SQL_Filtro,
      'ORDER BY DataHora ',
      '']);
    //Controle := QrEntiContatControle.Value;
    UMyMod.UpdMulReg01(Help, Titulo, Prompt, DataSource, ListSource, CampoNO,
    Default, SQLSorc, Dmod.MyDB, SQLDest, CO_UPD_TAB_VMI, FldUpdNom, FldIdxNom,
    UserDataAlterweb);
  end;
  procedure MsgNiv();
  begin
    Geral.MB_Erro(
    'Implementa��o pendente!' + sLineBreak +
    'Em: "VS_PF.VerificaCliForEmpty()"' + sLineBreak +
    'MovimID: ' + Geral.FF0(MovimID) + slineBreak +
    'MovimNiv: ' + Geral.FF0(MovimNiv) + sLineBreak +
    'Campo: ' + Campo);
  end;
  procedure ZeraCampo(Campo: String; MovimID, MovimNiv: Integer);
  const
    Zero = 0;
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    Campo], ['MovimID', 'MovimNiv'], [
    Zero], [MovimID, MovimNiv], True);
  end;
var
  Field, Dsply: String;
begin
  Result := False;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    ReopenPsq(True, True);
    if Qry.RecordCount > 0 then
    begin
      if Geral.MB_Pergunta(
      'Existem registros de movimenta��o de couro sem informa��o de cliente e/ou fornecedor de M.O.'
      + sLineBreak + 'Deseja visualizar e corrigir agora?') <> ID_YES then
        Exit;
      Geral.MB_Info(Qry.SQL.Text);
////////////////////////////////////////////////////////////////////////////////
      //Atualizar fornecedor de MO!
////////////////////////////////////////////////////////////////////////////////
      Campo := 'FornecMO';
      Filtr := 'AND vmi.FornecMO=0 ';
      Field := 'Fornece7';
      Dsply := 'Fornecedor';
      ReopenPsq(True, False);
      while not Qry.Eof do
      begin
        MovimID  := Qry.FieldByName('MovimID').AsInteger;
        MovimNiv := Qry.FieldByName('MovimNiv').AsInteger;
        Index    := GetIDNiv(MovimID, MovimNiv);
        case Index of
          //11.emidEmOperacao * (eminSorcOper=7, eminEmOperInn=8, eminDestOper=9, eminEmOperBxa=10)
          //19.emidEmProcWE * (eminSorcWEnd=20, eminEmWEndInn=21, eminDestWEnd=22, eminEmWEndBxa=23)
          //emidEmProcCal=26 * (eminSorcCal=29, eminEmCalInn=30, eminDestCal=31, eminEmCalBxa=32)
          //emidEmProcCur=27 * (eminSorcCur=34, eminEmCurInn=35, eminDestCur=36, eminEmCurBxa=37)
          06013: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          06014: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          06015: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          07001: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          07002: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          07006: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          08001: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          08002: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          08006: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          11007: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          11008: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          11009: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          11010: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          13000: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          14001: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          14002: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          15011: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          15012: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          19020: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          19021: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          20022: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          21000: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          22000: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          19023: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          24001: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          24002: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
//25027: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr); ??????????
//25028: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr); ??????????
          26029: DefineMenos11(26, 29);
          26030: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          27034: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          27035: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          28001: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          28002: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          29031: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          29032: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          30041: DefineMenos11(30, 41);
          30042: DefineMenos11(30, 42);
          31046: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          31047: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          41000: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);

          else MsgNiv();
        end;
        //
        Qry.Next;
      end;
////////////////////////////////////////////////////////////////////////////////
      //Atualizar cliente de MO!
////////////////////////////////////////////////////////////////////////////////
      ReopenPsq(False, True);
      while not Qry.Eof do
      begin
        MovimID  := Qry.FieldByName('MovimID').AsInteger;
        MovimNiv := Qry.FieldByName('MovimNiv').AsInteger;
        Index    := GetIDNiv(MovimID, MovimNiv);
        Campo := 'ClientMO';
        Filtr := 'AND vmi.ClientMO=0 ';
        Field := 'Cliente2';
        Dsply := 'Cliente';
        case Index of
          01000: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          02000: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          //06.emidIndsXX * (eminDestCurtiXX=13, eminSorcCurtiXX=14, eminBaixCurtiXX=15)
          06015: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          08001: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          08002: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          //11.emidEmOperacao * (eminSorcOper=7, eminEmOperInn=8, eminDestOper=9, eminEmOperBxa=10)
          //11007: ZeraCampo(Campo, MovimID, MovimNiv);
          11007: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          17000: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          //19.emidEmProcWE * (eminSorcWEnd=20, eminEmWEndInn=21, eminDestWEnd=22, eminEmWEndBxa=23)
          22000: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          //emidEmProcCal=26 * (eminSorcCal=29, eminEmCalInn=30, eminDestCal=31, eminEmCalBxa=32)
          //emidEmProcCur=27 * (eminSorcCur=34, eminEmCurInn=35, eminDestCur=36, eminEmCurBxa=37)
          //?: AtualizaAtual(Campo, 'AND vmi.ClientMO=0 ');
          // parei aqui!
          else MsgNiv();

        end;
        //
        Qry.Next;
      end;
    end;
////////////////////////////////////////////////////////////////////////////////
      // Qry.RecordCount > 0 = Ainda tem problemas!!!
////////////////////////////////////////////////////////////////////////////////
    ReopenPsq(True, True);
    Result := Qry.RecordCount = 0;
////////////////////////////////////////////////////////////////////////////////
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimNiv IN (' + CO_CODS_NIV_NEED_MO_FORNEC + ') ',
    'AND FornecMO=0 ',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Geral.MB_Aviso(
      'Existem registros de movimenta��o de couro sem informa��o de fornecedor de M.O.'
      + sLineBreak + 'Avise a DERMATEK!');
      Exit;
      Result := True;
    end;
////////////////////////////////////////////////////////////////////////////////
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimNiv IN (' + CO_CODS_NIV_NEED_MO_CLIENT + ') ',
    'AND ClientMO=0 ',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Geral.MB_Info(
      'Existem registros de movimenta��o de couro sem informa��o de Cliente de M.O.'
      + sLineBreak + 'Avise a DERMATEK!');
        Exit;
      Result := True;
    end;
////////////////////////////////////////////////////////////////////////////////
  finally
    Qry.Free;
  end;
end;

function TUnVS_PF.VerificaDatasDeEncerradasAposAbertura(): Boolean;
var
  Qry: TmySQLQuery;
  Corda, Msg: String;
begin

  Corda := '';
  Msg   := '';
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cab.* ',
    'FROM vscalcab cab ',
    'WHERE DtHrFimOpe >0 ',
    'AND DATE(DtHrFimOpe) < DATE(DtHrAberto) ',
    '']);
    Corda := MyObjects.CordaDeQuery(Qry, 'MovimCod', EmptyStr);
    if Trim(Corda) <> '' then
      Msg := Msg + 'Itens de caleiro (IME-C) com data posterior inv�lida: ' +
      sLineBreak + Corda + sLineBreak +
      'Para modificar v� na janela principal em "Uso e consumo" > Bot�o "Pesagem VS Caleiro" ...' + sLineBreak +
      'Ap�s localizar o IME-C clique no bot�o "Em processo " > item "Altera��o parcial" > sub-item "Data de abertura" e altere a data'
      + sLineBreak + Qry.SQL.Text + sLineBreak;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cab.* ',
    'FROM vscurcab cab ',
    'WHERE DtHrFimOpe >0 ',
    'AND DATE(DtHrFimOpe) < DATE(DtHrAberto) ',
    '']);
    Corda := MyObjects.CordaDeQuery(Qry, 'MovimCod', EmptyStr);
    if Trim(Corda) <> '' then
      Msg := Msg + 'Itens de curtimento (IME-C) com data posterior inv�lida: ' +
      sLineBreak + Corda + sLineBreak +
      'Para modificar v� na janela principal em "Uso e consumo" > Bot�o "Pesagem VS Curtimento" ...' + sLineBreak +
      'Ap�s localizar o IME-C clique no bot�o "Em processo " > item "Altera��o parcial" > sub-item "Data de abertura" e altere a data'
      + sLineBreak + Qry.SQL.Text + sLineBreak;
    //
    Result := Trim(Msg) = '';
    if not Result then
      Geral.MB_Aviso('A��o abortada!' + sLineBreak + Msg);
  finally
    Qry.Free;
  end;
end;

function TUnVS_PF.VerificaFornecMO(PB: TProgressBar; LaAviso1, LaAviso2: TLabel): Boolean;
var
  Qry1, Qry2: TmySQLQuery;
  //
  procedure ReopenIrregulares();
  begin
    //DmkDAC_PF.AbreMySQLQuery0(DmModVS.QrSemFornecMO, Dmod.MyDB, [
    UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
    'SELECT MovimCod, MovimID ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE FornecMO=0 AND  ',
(*
    '( ',
    '  (MovimID=19) ',
    '  /* OR */ ',
    ') ',
*)
    'MovimID IN (11,19,27,32,33) ',
    '']);
  end;
  function AtualizaItem(MovimCod, FornecMO: Integer): Boolean;
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'FornecMO'], ['MovimCod'], [
    FornecMO], [MovimCod], True);
  end;
var
  MovimNiv, FornecMO, MovimCod: Integer;
begin
////////////////////////////////////////////////////////////////////////////////
  EXIT; // Usar  function TUnVS_PF.VerificaCliForEmpty(): Boolean;
////////////////////////////////////////////////////////////////////////////////

  Qry1 := TmySQLQuery.Create(Dmod);
  try
  Qry2 := TmySQLQuery.Create(Dmod);
  try
    Result := False;
    ReopenIrregulares();
    if Qry1.RecordCount > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' +
      Geral.FF0(Qry1.RecordCount) +
      ' registros sem defini��o de fornecedor de m�o de obra que devem ser regularizados antes de continuar!'
      + sLineBreak + 'Deseja regularizar agora?') = ID_YES then
      begin
        PB.Position := 0;
        PB.Max := Qry1.RecordCount;
        Qry1.First;
        while not Qry1.Eof do
        begin
          MyObjects.UpdPB(PB, LaAviso1, LaAviso2);
          MovimNiv := Integer(VS_PF.ObtemMovimNivInnDeMovimID(TEstqMovimID(Qry1.FieldByName('MovimID').AsInteger)));
          MovimCod := Qry1.FieldByName('MovimCod').AsInteger;
          //
          UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
          'SELECT FornecMO  ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE MovimCod=' + Geral.FF0(MovimCod),
          'AND MovimNiv=' + Geral.FF0(MovimNiv),
            '']);
          FornecMO := Qry2.FieldByName('FornecMO').AsInteger;
          if FornecMO <> 0 then
          begin
            if FornecMO = 0 then
              FornecMO := -11;
            AtualizaItem(MovimCod, FornecMO);
          end;
          //
          Qry1.Next;
        end;
      end;
    end;
  except
    Qry2.Free;
  end;
  except
    Qry1.Free;
  end;
  ReopenIrregulares();
  Result := Qry1.RecordCount = 0;
end;

function TUnVS_PF.VerificaGraGruXCouSemNivel1(): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ggx.GraGruY Grupo, cou.GraGruX Reduzido',
    'FROM gragruxcou cou ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=cou.GraGruX',
    'WHERE cou.CouNiv1=0 ',
    '']);
    Result := Qry.RecordCount;
    if Result > 0 then
    begin
      Geral.MB_Aviso('Existem ' + Geral.FF0(Result) +
      ' cadastros de artigos sem defini��o de "Tipo de Material"!');
      VS_PF.MostraRelatorioCourosSemParteMaterial();
    end;
    //
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_PF.VerificaGSP(Campo: String; MovimID: TEstqMovimID);
var
  Qry: TmySQLQuery;
  Itens: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE ' + Campo + '<>0 ',
    'AND ( ',
    '  GSPSrcMovID=0 ',
    '  OR  ',
    '  GSPSrcNiv2=0 ',
    ') ',
    '']);
    Itens := Qry.RecordCount;
    if Itens > 0 then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
      'UPDATE ' + CO_UPD_TAB_VMI + ' ',
      'SET GSPSrcMovID=' + Geral.FF0(Integer(MovimID)),
      ', GSPSrcNiv2=' + Campo,
      'WHERE ' + Campo + '<>0 ',
      'AND GSPSrcNiv2=0 ',
      '']);
      //
      Geral.MB_Info(Geral.FF0(Itens) + ' itens "' + Campo + '" foram atualizados!');
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_PF.VerificaInconsistencias(PB1: TProgressBar; LaAviso1,
  LaAviso2: TLabel);
var
  Problemas: Integer;
  Msg: String;
begin
  Screen.Cursor := crHourGlass;
  try
    Problemas := 0;
    PB1.Position := 0;
    PB1.Max := 6;
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Verificando artigos com cadastro incompleto');
    AppPF.VerificaCadastroXxArtigoIncompleta();
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Verificando fornecedores VS com cadastro incompleto');
    VerificaCadastroVSEntiMPIncompleto();
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Verificando IME-Is sem fornecedor');
    if DBCheck.CriaFm(TFmVSCorrigeMulFrn, FmVSCorrigeMulFrn, afmoNegarComAviso) then
    begin
      if FmVSCorrigeMulFrn.FProblemas > 0 then
      begin
        Problemas := Problemas + FmVSCorrigeMulFrn.FProblemas;
        FmVSCorrigeMulFrn.ShowModal;
      end;
      FmVSCorrigeMulFrn.Destroy;
    end;
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Verificando IME-Is com "MovimTwn" orf�os');
    if DBCheck.CriaFm(TFmVSCorrigeMovimTwn, FmVSCorrigeMovimTwn, afmoNegarComAviso) then
    begin
      if FmVSCorrigeMovimTwn.FProblemas > 0 then
      begin
        Problemas := Problemas + FmVSCorrigeMovimTwn.FProblemas;
        FmVSCorrigeMovimTwn.ShowModal;
      end;
      FmVSCorrigeMovimTwn.Destroy;
    end;
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Verificando IME-Is com "SrcNivel2" orf�os');
    if DBCheck.CriaFm(TFmVSCorrigeSN2Orfao, FmVSCorrigeSN2Orfao, afmoNegarComAviso) then
    begin
      if FmVSCorrigeSN2Orfao.FProblemas > 0 then
      begin
        Problemas := Problemas + FmVSCorrigeSN2Orfao.FProblemas;
        FmVSCorrigeSN2Orfao.ShowModal;
      end;
      FmVSCorrigeSN2Orfao.Destroy;
    end;
    if Problemas > 0 then
      Msg := Geral.FF0(Problemas) + ' problemas encontrados!'
    else
      Msg := 'Nenuum problema encontrado.';
    //
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, False, Msg);

  finally
    Screen.Cursor := crDefault;
  end;
end;

function TUnVS_PF.VerificaStqCenLocEmpty(): Boolean;
const
  sProcName = 'TUnVS_PF.VerificaStqCenLocEmpty()';
var
  Qry: TmySQLQuery;
  Index: Integer;
  MovimID, MovimNiv: Integer;
  Campo, Filtr: String;
  //
  procedure ReopenPsq(Fornecedor, Cliente: Boolean);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _VS_CEN_LOC_EMPTY_;',
    'CREATE TABLE _VS_CEN_LOC_EMPTY_',
    'SELECT MovimID, MovimNiv,',
    'SUM(IF(StqCenLoc<>0, 1, 0)) SCL_SIM,',
    'SUM(IF(StqCenLoc<>0, 0, 1)) SCL_NAO',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI,
    'WHERE NOT (MovimID IN (2,9,12))',
    'GROUP BY MovimID, MovimNiv',
    'ORDER BY MovimID, MovimNiv;',
    'SELECT *',
    'FROM _VS_CEN_LOC_EMPTY_',
    'WHERE SCL_NAO > 0; ',
    (*'WHERE (',
    '  SCL_SIM <> 0 AND SCL_NAO <> 0',
    ');',*)
    '']);
  end;
  procedure AtualizaAtual(MovimID, MovimViv: Integer; FldDisplay, FldEntSrc,
  FldUpdNom, SQL_Filtro: String);
  const
    Aviso  = '...';
    Help   = '?????';
    CampoCU  = 'CodUsu';
    CampoCO  = 'Codigo';
    CampoNO  = 'Nome';
    DataSource = nil;
    ListSource = nil;
    Default    = 0;
    FldIdxNom  = 'Controle';
    UserDataAlterweb = True;
  var
    Titulo, Prompt: String;
    SQLSorc, SQLDest, ATT_MovimID, ATT_MovimNiv: String;
  begin
    Titulo := 'Sele��o de Local de Estoque (' +
    sEstqMovimID[MovimID] + ' - ' + sEstqMovimNiv[MovimNiv] + ')';
    Prompt := 'Informe o Local de Estoque:';
    SQLSorc := Geral.ATS([
    'SELECT scl.CodUsu ' + CampoCU + ', scl.Controle ' + CampoCO +
    ', CONCAT(scl.Nome, " (", scc.Nome, ")") ' + CampoNO,
    'FROM stqcenloc scl ',
    'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
    'ORDER BY ' + CampoNO,
    '']);
    ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
    //
    ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
    //
    SQLDest := Geral.ATS([
    'SELECT vmi.DataHora, vmi.Codigo IME_C, vmi.Controle IME_I, ',
    'vmi.MovimCod OP,  vmi.Empresa, ',
    'vmi.MovimID, ',
    ATT_MovimID,
    'vmi.MovimNiv, ',
    ATT_MovimNiv,
    'vmi.ClientMO, IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_ClientMO, ',
    'vmi.FornecMO, IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NO_FornecMO, ',
    'CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, ',
    'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
    'vmi.SdoVrtPeca, vmi.SdoVrtPeso, vmi.SdoVrtArM2, ',
    'vmi.Pallet, vmi.GraGruX, vmi.SerieFch, vmi.Ficha, ',
    'vmi.Terceiro, IF(trc.Tipo=0, trc.RazaoSocial, trc.Nome) NO_Terceiro, ',
    'vmi.Controle ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
    'LEFT JOIN gragrux     ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragruc     ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad   gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits   gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1     gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN entidades   cli ON cli.Codigo=vmi.ClientMO ',
    'LEFT JOIN entidades   fmo ON fmo.Codigo=vmi.FornecMO ',
    'LEFT JOIN entidades   trc ON trc.Codigo=vmi.Terceiro ',
    'LEFT JOIN vsmulfrncab mfc ON mfc.Codigo=vmi.VSMulFrnCab ',
    'LEFT JOIN gragruxcou  xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
    'WHERE vmi.MovimID=' + Geral.FF0(MovimID),
    'AND vmi.MovimNiv=' + Geral.FF0(MovimNiv),
    //'AND vmi.StqCenCad=0 ',
    SQL_Filtro,
    'ORDER BY DataHora ',
    '']);
    //
    UMyMod.UpdMulReg01(Help, Titulo, Prompt, DataSource, ListSource, CampoNO,
    Default, SQLSorc, Dmod.MyDB, SQLDest, CO_UPD_TAB_VMI, FldUpdNom, FldIdxNom,
    UserDataAlterweb);
  end;
  procedure MsgNiv();
  begin
    Geral.MB_Erro(
    'Implementa��o pendente!' + sLineBreak +
    'Em: "' + sProcName + '"' + sLineBreak +
    'MovimID: ' + Geral.FF0(MovimID) + ' --> ' +
    GetEnumName(TypeInfo(TEstqMovimID), Integer(MovimID)) + slineBreak +
    'MovimNiv: ' + Geral.FF0(MovimNiv) + ' --> ' +
    GetEnumName(TypeInfo(TEstqMovimNiv), Integer(MovimNiv)) + slineBreak +
    'Campo: ' + Campo);
  end;
(*
  procedure ZeraCampo(Campo: String; MovimID, MovimNiv: Integer);
  const
    Zero = 0;
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    Campo], ['MovimID', 'MovimNiv'], [
    Zero], [MovimID, MovimNiv], True);
  end;
*)
var
  Field, Dsply: String;
begin
  Result := False;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    ReopenPsq(True, True);
    if Qry.RecordCount > 0 then
    begin
      if Geral.MB_Pergunta(
      'Existem registros de movimenta��o de couro sem informa��o de Local de estoque.'
      + sLineBreak + 'Deseja visualizar e corrigir agora?') <> ID_YES then
        Exit;
      Geral.MB_Info(Qry.SQL.Text);
      //
      Campo := 'StqCenLoc';
      Filtr := 'AND vmi.StqCenLoc=0 ';
      Field := 'StqCenLoc';
      Dsply := 'Local de Estoque';
      //ReopenPsq(True, False);
      while not Qry.Eof do
      begin
        MovimID  := Qry.FieldByName('MovimID').AsInteger;
        MovimNiv := Qry.FieldByName('MovimNiv').AsInteger;
        Index    := GetIDNiv(MovimID, MovimNiv);
        case Index of
          //11.emidEmOperacao * (eminSorcOper=7, eminEmOperInn=8, eminDestOper=9, eminEmOperBxa=10)
          //19.emidEmProcWE * (eminSorcWEnd=20, eminEmWEndInn=21, eminDestWEnd=22, eminEmWEndBxa=23)
          //emidEmProcCal=26 * (eminSorcCal=29, eminEmCalInn=30, eminDestCal=31, eminEmCalBxa=32)
          //emidEmProcCur=27 * (eminSorcCur=34, eminEmCurInn=35, eminDestCur=36, eminEmCurBxa=37)
          01000: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          06013: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          06014: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          06015: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          07001: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          07002: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          08001: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          08002: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          11007: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          11008: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          11009: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          13000: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          14001: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          14002: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          15011: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          15012: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          16000: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          19020: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          19021: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          20022: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          19023: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          21000: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          22000: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          24001: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          24002: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
(*
//25027: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr); ??????????
//25028: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr); ??????????
*)
          26029: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          26030: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          27034: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          27035: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          28001: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          28002: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          29031: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          29032: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          30041: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          30042: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          31046: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          31047: AtualizaAtual(MovimID, MovimNiv, Dsply, Field, Campo, Filtr);
          else MsgNiv();
        end;
        //
        Qry.Next;
      end;
    end;
    //
    ReopenPsq(True, True);
    Result := Qry.RecordCount = 0;
  finally
    Qry.Free;
  end;
end;

function TUnVS_PF.VerSeCriaCadNaTabela(GraGruX: Integer; Tabela: String): Boolean;
var
  SQLType: TSQLType;
begin
  Result := False;
  if Geral.MB_Pergunta('O cadastro do reduzido ' + Geral.FF0(GraGruX) +
  ' n�o existe na tabela de artigo espec�fico! Deseja cri�-lo agora?') = ID_YES then
  begin
    SQLType := stIns;
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, Tabela, False, [
    ], [
    'GraGruX'], [
    ], [
    GraGruX], True);
  end;
end;

{
procedure TUnVS_PF.ReopenVSPallet(Qry: TmySQLQuery; EdPallet: TdmkEditCB;
  CBPallet: TdmkDBLookupComboBox; Empresa(*, Fornece, GraGruX*): Integer);
var
  Pallet: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT DISTINCT let.Codigo, let.Nome ',
  'FROM vspalleta let',
  //'LEFT JOIN vspalart art ON art.Codigo=let.Codigo',
  //'LEFT JOIN vspalfrn frn ON frn.Controle=art.Controle',
  'WHERE let.Ativo=1 ',
  'AND let.Empresa=' + Geral.FF0(Empresa),
  //'AND art.GraGruX=' + Geral.FF0(GraGruX),
  //'AND frn.Fornece=' + Geral.FF0(Fornece),
  'ORDER BY let.Nome ',
  '']);
  //
  Pallet := EdPallet.ValueVariant;
  if Pallet <> 0 then
  begin
    if not Qry.Locate('Codigo', Pallet, []) then
    begin
      EdPallet.ValueVariant := 0;
      CBPallet.KeyValue     := Null;
    end;
  end;
end;
}


////////////////////////////////////////////////////////////////////////////////
///
///
///
///
////////////////////////////////////////////////////////////////////////////////

(*
 /* =============== */
/* COURO IN NATURA */
/* =============== */

/*Ficha RMP 000*/
UPDATE v s m o v i t s
SET NotFluxo=1
WHERE Controle IN (
3360, 3362,
4938, 4939,
6594,
6595, 6596);

/*Ficha RMP U 6400*/
UPDATE v s m o v i t s
SET NotFluxo=1
WHERE Controle IN (
3350
);
UPDATE v s m o v i t s
SET Zerado=1
WHERE Controle IN (
3350);

/*Ficha RMP 6472*/
UPDATE v s m o v i t s
SET NotFluxo=1
WHERE Controle IN (
4936
);

/*Ficha RMP 6473*/
UPDATE v s m o v i t s
SET NotFluxo=1
WHERE Controle IN (
4937
);

/* =============== */
/* ARTIGO GERADO   */
/* =============== */

/*IMEI 3587*/
UPDATE v s m o v i t s
SET NotFluxo=2
WHERE Controle IN (
3587, 3589, 3591, 3593, 3595, 3597, 3607
);

/*IMEI 3982*/
UPDATE v s m o v i t s
SET NotFluxo=2
WHERE Controle IN (
3982, 3995, 3997, 4001, 4003, 4007
);

/* =============== */
/* ARTIGO CLASSIF   */
/* =============== */

/*Pallet 342*/
UPDATE v s m o v i t s
SET NotFluxo=0
WHERE Controle IN (
3196, 3452, 3535,
6573, 6574, 6575, 6576);

/*Pallet 342*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
3425, 3428, 3452, 3512, 3513);

/*Pallet 343*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6553, 6554);

/*Pallet 344*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6566, 6567, 6568);

/*Pallet 351*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6556, 6557, 3453);

/*Pallet 352*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6561, 6562);

/*Pallet 354*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
3482);

/*Pallet 376*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6642, 6643);

/*Pallet 377*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
3270,3274, 3281, 3301, 3315, 3489);
UPDATE v s m o v i t s
SET Zerado=1
WHERE Controle IN (
3231);

/*Pallet 387*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6644, 6645, 6646, 6648);
UPDATE v s m o v i t s
SET Zerado=1
WHERE Controle IN (
3240);

/*Pallet 388*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6563, 6564, 6565);

/*Pallet 389*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6558, 6559, 6560);

/*Pallet 390*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6569, 6570);

/*Pallet 395*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6571, 6572);

/*Pallet 397*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6383, 6384);

/*Pallet 398*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6548);

/*Pallet 399*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6549);

/*Pallet 402*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
3488);

/*Pallet 403*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6520, 6521, 6522, 6523);

/*Pallet 404*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
3314);

/*Pallet 414*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
3294, 3302, 3316, 3490, 3760);

/*Pallet 416*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
3295, 3303, 3317, 3491, /*3541,*/ 3761);

/*Pallet 417*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
3304, 3306, 3318, 3339, 3492, 3762);

/*Pallet 420*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
3326, 3340, 3493, 3599, 3763);

/*Pallet 422*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
3570, 3179, 3605, 3606);
UPDATE v s m o v i t s
SET Zerado=1
WHERE Controle IN (
3570);

/*Pallet 423*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6608, 6609, 6610, 6611, 6612, 6613);

/*Pallet 425*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6602, 6603, 6604, 6605, 6606, 6607);

/*Pallet 427*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6597, 6598, 6599, 6600, 6601);

/*Pallet 429*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
3609, 3177);
UPDATE v s m o v i t s
SET Zerado=1
WHERE Controle IN (
3609);

/*Pallet 430*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6583, 6584, 6585, 6586, 6587, 6588);

/*Pallet 434*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6589);

/*Pallet 435*/
UPDATE v s m o v i t s
SET GraGruX=4
WHERE Controle=3366;
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6590);

/*Pallet 436*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6591, 6592);
UPDATE v s m o v i t s
SET Zerado=1
WHERE Controle IN (
3356);

/*Pallet 439*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6577, 6578, 6579, 6580, 6581, 6582);

/*Pallet 444*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6469, 6470);

/*Pallet 449*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
3518, 4793, 6459);

/*Pallet 451*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6551, 6552);

/*Pallet 458*/
UPDATE v s m o v i t s
SET NotFluxo=0
WHERE Controle IN (
7812, 7815, 3889);

/*Pallet 459*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6524, 6525, 6526);

/*Pallet 461*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
3653, 3654, 3655, 3656, 3657, 3658, 3659);

/*Pallet 465*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
3650, 3174);
UPDATE v s m o v i t s
SET Zerado=1
WHERE Controle IN (
3650);

/*Pallet 469*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6541, 6542, 6543, 6544, 6545, 6546, 6547);

/*Pallet 476*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6478, 6479);

/*Pallet 484*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6506, 6507, 6508, 6509);

/*Pallet 485*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6535, 6536, 6537, 6538, 6539, 6540);

/*Pallet 490*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
4464, 4481, 4500);

/*Pallet 489*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6530, 6531, 6532, 6533, 6534);

/*Pallet 502*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
4445, 4463, 4498, 4499);

/*Pallet 503*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6527, 6528, 6529);

/*Pallet 504*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
4436, 4444, 4497);

/*Pallet 512*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6504, 6505);

/*Pallet 513*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6512, 6513, 6514);

/*Pallet 517*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6510, 6511);

/*Pallet 525*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
4013);

/*Pallet 527*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
4211, 4212);

/*Pallet 532*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6489, 6490, 6491);

/*Pallet 533*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6471);

/*Pallet 535*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6492, 6493, 6494);

/*Pallet 536*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
5054, 5055);

/*Pallet 537*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6495, 6496, 6497);

/*Pallet 538*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6498, 6499, 6500);

/*Pallet 539*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6501, 6502);

/*Pallet 541*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6515, 6516, 6517);

/*Pallet 542*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6518, 6519);

/*Pallet 544*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6404, 6407);

/*Pallet 546*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
4290, 4291);

/*Pallet 5563*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6408, 6409, 6410);

/*Pallet 5565*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6346);

/*Pallet 572*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
4501, 6476, 6477);

/*Pallet 579*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6414, 6415, 6416);

/*Pallet 583*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6417, 6418);

/*Pallet 584*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
7275, 7276);

/*Pallet 585*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6411, 6412, 6413);

/*Pallet 589*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6419, 6420);

/*Pallet 593*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6421, 6422, 6423);

/*Pallet 595*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
4607, 4614, 4615,
4622, 4623, 4624, 4625, 4626, 4627, 4628, 4629,
4630, 4631, 4632, 4633, 4634, 4635, 4636, 4637,
4638, 4639, 4640, 4641, 4642, 4643, 4644, 4645,
4646, 4647, 4648, 4649, 4650, 4651, 4652, 4653,
4654, 4655, 4656, 4657, 4658, 4659, 4660, 4661,
4662, 4663, 4664, 4665, 4666, 4672, 4673, 4674,
4675, 4676, 4677, 4678, 4679, 4680, 4681, 4682,
4683, 4684, 4685, 4686, 4687, 4688, 4689, 4690,
4691, 4692, 4693, 4694, 4695, 4696, 4697, 4698,
4699, 4700, 4701, 4702, 4703, 4704, 4705, 4706,
4707, 4708, 4709, 4710, 4711, 4712, 4713, 4714,
4715, 4716, 4717, 4718, 4719, 4720, 4721, 4722,
4730, 4731, 4732, 4733, 4734, 4735, 4736, 4737,
4738, 4739, 4740, 4741, 4747, 4748, 4749, 4750,
4751, 4752, 4753, 4754, 4755, 4756, 4757, 4758,
4759, 4760, 4761, 4762, 4763, 4764, 4787, 4788,
4789, 4790, 4791, 4792, 4511, 4556);

/*Pallet 597*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
4514);

/*Pallet 598*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle = 4516;

/*Pallet 600*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6450, 6451, 6452, 6453);

/*Pallet 601*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6443, 6444, 6445, 6446);

/*Pallet 602*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6454, 6455, 6456, 6457, 6458);

/*Pallet 603*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6460, 6461, 6462, 6463);

/*Pallet 604*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6472, 6473, 6474, 6475);

/*Pallet 605*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
8684, 8685);

/*Pallet 606*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6464, 6465, 6466, 6467, 6468);

/*Pallet 611*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
5116, 5117, 5118, 5091, 5092, 5093, 5079, 5087, 4801);

/*Pallet 613*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
4881, 4882);

/*Pallet 614*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6447, 6448, 6449);

/*Pallet 617*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6428, 6429, 5086);

/*Pallet 638*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6430, 6431, 6432);

/*Pallet 640*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6433, 6434);

/*Pallet 645*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6441, 6442);

/*Pallet 646*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6440);

/*Pallet 647*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6345, 6435, 6436, 6437);

/*Pallet 648*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
4982, 5009);

/*Pallet 654*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
8692);

/*Pallet 669*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6387, 6388);

/*Pallet 675*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6385, 6386);

/*Pallet 694*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
5196, 5197);

/*Pallet 695*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
5195);

/*Pallet 696*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6400, 6401, 6402);

/*Pallet 700*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
5867, 5223);

/*Pallet 704*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6394, 6395, 6396, 6397);

/*Pallet 705*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6389, 6390, 6391, 6392, 6393);

/*Pallet 752*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6380, 6381, 6382);

/*Pallet 756*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6376, 6377, 6378, 6379);

/*Pallet 758*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6371, 6372, 6373, 6374, 6375);

/*Pallet 762*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
6367, 6368, 6369, 6370);

/*Pallet 867*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
8703);

/*Pallet 961*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
8256);

/*Pallet 962*/
UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
8683);


/*==================================*/
/*==================================*/

/*ANTIGOS*/

UPDATE v s m o v i t s
SET NotFluxo=0
WHERE Controle < 3196;

UPDATE v s m o v i t s
SET NotFluxo=4
WHERE Controle IN (
3179, 3178, 3182, 3175);

*)

procedure TUnVS_PF.AbreGraGruXY(Qry: TmySQLQuery; _AND: String);
begin
  VS_CRC_PF.AbreGraGruXY(Qry, _AND);
end;

procedure TUnVS_PF.AbreVSSerFch(QrVSSerFch: TmySQLQuery);
begin
  VS_CRC_PF.AbreVSSerFch(QrVSSerFch);
end;

function TUnVS_PF.AdicionarNovosVS_emid: Boolean;
begin
  Result := VS_CRC_PF.AdicionarNovosVS_emid;
end;

procedure TUnVS_PF.AtualizaDtHrFimOpe_MovimCod(MovimID: TEstqMovimID;
  MovimCod: Integer);
begin
  VS_CRC_PF.AtualizaDtHrFimOpe_MovimCod(MovimID, MovimCod);
end;

procedure TUnVS_PF.AtualizaNotaMPAG(Controle: Integer; NotaMPAG, FatNotaVNC,
  FatNotaVRC: Double);
begin
  VS_CRC_PF.AtualizaNotaMPAG(Controle, NotaMPAG, FatNotaVNC, FatNotaVRC);
end;

procedure TUnVS_PF.AtualizaPalletPelosIMEIsDeGeracao(Pallet: Integer;
  QrSumVMI: TmySQLQuery);
begin
  VS_CRC_PF.AtualizaPalletPelosIMEIsDeGeracao(Pallet, QrSumVMI);
end;

procedure TUnVS_PF.AtualizaSaldoIMEI(Controle: Integer; Gera: Boolean);
begin
  VS_CRC_PF.AtualizaSaldoIMEI(Controle, Gera);
end;

procedure TUnVS_PF.AtualizaSaldoOrigemVMI_Dest(VMI_Dest, VMI_Sorc,
  VMI_Baix: Integer);
begin
  VS_CRC_PF.AtualizaSaldoOrigemVMI_Dest(VMI_Dest, VMI_Sorc, VMI_Baix);
end;

procedure TUnVS_PF.AtualizaSaldoVirtualVSMovIts(Controle: Integer;
  Gera: Boolean);
begin
  VS_CRC_PF.AtualizaSaldoVirtualVSMovIts(Controle, Gera);
end;

procedure TUnVS_PF.AtualizaSaldoVirtualVSMovIts_Generico(Controle, MovimID,
  MovimNiv: Integer);
begin
  VS_CRC_PF.AtualizaSaldoVirtualVSMovIts_Generico(Controle, MovimID, MovimNiv);
end;

procedure TUnVS_PF.AtualizaStatPall(Pallet: Integer);
begin
  VS_CRC_PF.AtualizaStatPall(Pallet);
end;

procedure TUnVS_PF.AtualizaTotaisVSCalCab(MovimCod: Integer);
begin
  VS_CRC_PF.AtualizaTotaisVSCalCab(MovimCod);
end;

procedure TUnVS_PF.AtualizaTotaisVSConCab(MovimCod: Integer);
begin
  VS_CRC_PF.AtualizaTotaisVSConCab(MovimCod);
end;

procedure TUnVS_PF.AtualizaTotaisVSCurCab(MovimCod: Integer);
begin
  VS_CRC_PF.AtualizaTotaisVSCurCab(MovimCod);
end;

procedure TUnVS_PF.AtualizaTotaisVSOpeCab(MovimCod: Integer);
begin
  VS_CRC_PF.AtualizaTotaisVSOpeCab(MovimCod);
end;

procedure TUnVS_PF.AtualizaTotaisVSPSPCab(MovimCod: Integer);
begin
  VS_CRC_PF.AtualizaTotaisVSPSPCab(MovimCod);
end;

procedure TUnVS_PF.AtualizaTotaisVSPWECab(MovimCod: Integer);
begin
  VS_CRC_PF.AtualizaTotaisVSPWECab(MovimCod);
end;

procedure TUnVS_PF.AtualizaTotaisVSRRMCab(MovimCod: Integer);
begin
  VS_CRC_PF.AtualizaTotaisVSRRMCab(MovimCod);
end;

function TUnVS_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix,
  VMI_Sorc: Integer; QrSumDest, QrSumSorc, QrVMISorc,
  QrPalSorc: TmySQLQuery): Boolean;
begin
  Result := VS_CRC_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
  QrSumDest, QrSumSorc, QrVMISorc, QrPalSorc);
end;

procedure TUnVS_PF.AtualizaVSCalCabGGxSrc(MovimCod: Integer);
begin
  VS_CRC_PF.AtualizaVSCalCabGGxSrc(MovimCod);
end;

procedure TUnVS_PF.AtualizaVSConCabGGxSrc(MovimCod: Integer);
begin
  VS_CRC_PF.AtualizaVSConCabGGxSrc(MovimCod);
end;

procedure TUnVS_PF.AtualizaVSCurCabGGxSrc(MovimCod: Integer);
begin
  VS_CRC_PF.AtualizaVSCurCabGGxSrc(MovimCod);
end;

procedure TUnVS_PF.AtualizaVSPedIts_Lib(VSPedIts, VSMovIts: Integer; LibPecas,
  LibPesoKg, LibAreaM2, LibAreaP2: Double);
begin
  VS_CRC_PF.AtualizaVSPedIts_Lib(VSPedIts, VSMovIts, LibPecas,
  LibPesoKg, LibAreaM2, LibAreaP2);
end;

function TUnVS_PF.CadastraPalletRibCla(Empresa, ClientMO: Integer;
  EdPallet: TdmkEditCB; CBPallet: TdmkDBLookupComboBox; QrVSPallet: TmySQLQuery;
  MovimIDGer: TEstqMovimID; GraGruX: Integer): Integer;
begin
  Result := VS_CRC_PF.CadastraPalletRibCla(Empresa, ClientMO, EdPallet,
  CBPallet, QrVSPallet, MovimIDGer, GraGruX);
end;

function TUnVS_PF.CampoLstPal(Box: Integer): String;
begin
  Result := VS_CRC_PF.CampoLstPal(Box);
end;

function TUnVS_PF.DesfazEncerramentoPallet(Pallet, GraGruX: Integer): Boolean;
begin
  Result := VS_CRC_PF.DesfazEncerramentoPallet(Pallet, GraGruX);
end;

function TUnVS_PF.EncerraPalletNew(const Pallet: Integer;
  const Pergunta: Boolean): Boolean;
begin
  Result := VS_CRC_PF.EncerraPalletNew(Pallet, Pergunta);
end;

function TUnVS_PF.ExcluiVSMovIts_EnviaArquivoExclu(Controle, Motivo: Integer;
  Query: TmySQLQuery; DataBase: TmySQLDatabase; Senha: String): Boolean;
begin
  Result := VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(Controle, Motivo, Query,
  DataBase, Senha);
end;

function TUnVS_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo: String;
  Inteiro1: Integer; DB: TmySQLDatabase): Integer;
begin
  Result := VS_CRC_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo, Inteiro1, DB);
end;

function TUnVS_PF.FatorNotaMP(GraGruX: Integer): Double;
begin
  Result := VS_CRC_PF.FatorNotaMP(GraGruX);
end;

function TUnVS_PF.GeraSQLTabMov(var SQL: String; const Tab: TTabToWork;
  const TemIMEIMrt: Integer): Boolean;
begin
  Result := VS_CRC_PF.GeraSQLTabMov(SQL, Tab, TemIMEIMrt);
end;

function TUnVS_PF.GeraSQLVSMovItx_IMEI(SQL_Select, SQL_Flds, SQL_Left, SQL_Wher,
  SQL_Group: String; Tab: TTabToWork; TemIMEIMrt: Integer): String;
begin
  Result := VS_CRC_PF.GeraSQLVSMovItx_IMEI(SQL_Select, SQL_Flds, SQL_Left,
  SQL_Wher, SQL_Group, Tab, TemIMEIMrt);
end;

function TUnVS_PF.GetIDNiv(MovimID, MovimNiv: Integer): Integer;
begin
  Result := VS_CRC_PF.GetIDNiv(MovimID, MovimNiv);
end;

procedure TUnVS_PF.ImprimeClassIMEIs(IMEIS, Mortos: array of Integer;
MostraForm: Boolean);
begin
  VS_CRC_PF.ImprimeClassIMEIs(IMEIS, Mortos, MostraForm);
end;

procedure TUnVS_PF.ImprimePackListsIMEIs(Empresa: Integer;
  IMEIS: array of Integer; Vertical: Boolean);
begin
  VS_CRC_PF.ImprimePackListsIMEIs(Empresa, IMEIS, Vertical);
end;

procedure TUnVS_PF.ImprimePallets(Empresa: Integer; Pallets: array of Integer;
  VSMovImp4, VSLstPalBox: String; InfoNO_PALLET: Boolean;
  DestImprFichaPallet: TDestImprFichaPallet; ImpEmpresa: Boolean;
  EmptyLabels: Integer);
begin
  VS_CRC_PF.ImprimePallets(Empresa, Pallets, VSMovImp4, VSLstPalBox,
  InfoNO_PALLET, DestImprFichaPallet, ImpEmpresa, EmptyLabels);
end;

procedure TUnVS_PF.ImprimePallet_Unico(Empresa, ClientMO, Pallet: Integer;
  JanTab: String; InfoNO_PALLET: Boolean);
begin
  VS_CRC_PF.ImprimePallet_Unico(Empresa, ClientMO, Pallet, JanTab, InfoNO_PALLET);
end;

procedure TUnVS_PF.MostraFormVSMovIts(Controle: Integer);
begin
  VS_CRC_PF.MostraFormVSMovIts(Controle);
end;

procedure TUnVS_PF.MostraFormVSMovItsAlt(Controle: Integer;
  AtualizaSaldoModoGenerico: Boolean; GroupBoxes: array of TEstqEditGB);
begin
  VS_CRC_PF.MostraFormVSMovItsAlt(Controle, AtualizaSaldoModoGenerico, GroupBoxes);
end;

procedure TUnVS_PF.MostraFormVSPallet(Codigo: Integer);
begin
  VS_CRC_PF.MostraFormVSPallet(Codigo);
end;

procedure TUnVS_PF.MostraFormVSPallet1(Codigo: Integer);
begin
  VS_CRC_PF.MostraFormVSPallet1(Codigo);
end;

procedure TUnVS_PF.MostraFormVSSerFch;
begin
  VS_CRC_PF.MostraFormVSSerFch;
end;

procedure TUnVS_PF.MostraFormVS_Do_IMEI(Controle: Integer);
begin
  VS_CRC_PF.MostraFormVS_Do_IMEI(Controle);
end;

procedure TUnVS_PF.MostraFormVS_XXX(MovimID, Codigo, Controle: Integer);
begin
  VS_CRC_PF.MostraFormVS_XXX(MovimID, Codigo, Controle);
end;

function TUnVS_PF.NotaCouroRibeiraApuca(Pecas, Peso, AreaM2, FatorMP,
  FatorAR: Double): Double;
begin
  Result := VS_CRC_PF.NotaCouroRibeiraApuca(Pecas, Peso, AreaM2, FatorMP, FatorAR);
end;

function TUnVS_PF.ObtemNomeTabelaVSXxxCab(MovimID: TEstqMovimID): String;
begin
  Result := VS_CRC_PF.ObtemNomeTabelaVSXxxCab(MovimID);
end;

function TUnVS_PF.PesquisaPallets(const Empresa, ClientMO, CouNiv2, CouNiv1,
  StqCenCad, StqCenLoc: Integer; const Tabela, GraGruYs: String; const GraGruXs,
  Pallets: array of Integer; const SQL_Especificos: String;
  const MovimID: TEstqMovimID; const MovimCod: Integer;
  var sVSMovImp4: String): Boolean;
begin
  Result := VS_CRC_PF.PesquisaPallets(Empresa, ClientMO, CouNiv2, CouNiv1,
  StqCenCad, StqCenLoc, Tabela, GraGruYs, GraGruXs, Pallets, SQL_Especificos,
  MovimID, MovimCod, sVSMovImp4);
end;

function TUnVS_PF.RedefineReduzidoOnPallet(MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv): TTipoTrocaGgxVmiPall;
begin
  Result := VS_CRC_PF.RedefineReduzidoOnPallet(MovimID, MovimNiv);
end;

function TUnVS_PF.RegistrosComProblema(Empresa, GraGruX: Integer; LaAviso1, LaAviso2: TLabel): Boolean;
var
  SQL_Empresa, SQL_GraGruX: String;
begin
  //ver se existem itens com saldo de pe�as zero, mas de Peso e area <> 0
  SQL_Empresa := EmptyStr;
  SQL_GraGruX := EmptyStr;
  //
  if Empresa <> 0 then
    SQL_Empresa := 'AND vmi.Empresa=' + Geral.FF0(Empresa);
  if GraGruX <> 0 then
    SQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
(*
  'SELECT vmi.*  ',
  'FROM vsmovits vmi  ',
  'WHERE vmi.Empresa=' + Geral.FF0(Empresa),
  'AND vmi.GraGruX=' + Geral.FF0(GraGruX),
  'AND  ',
  '  (vmi.SdoVrtPeca<0  ',
  '  OR  ',
  '  (  ',
  '   vmi.SdoVrtPeca=0  ',
  '   AND  ',
  '    (  ',
  '     vmi.SdoVrtPeso <> 0  ',
  '     OR  ',
  '     vmi.SdoVrtArM2 <> 0  ',
  '    )  ',
  '  ))  ',
  'ORDER BY vmi.Controle  ',
*)
  'SELECT vmi.*   ',
  'FROM vsmovits vmi   ',
  'WHERE    ',
  '  (vmi.SdoVrtPeca<0   ',
  '  OR   ',
  '  (   ',
  '   vmi.SdoVrtPeca=0   ',
  '   AND   ',
  '    (   ',
  '     vmi.SdoVrtPeso <> 0   ',
  '     OR   ',
  '     vmi.SdoVrtArM2 <> 0   ',
  '    )   ',
  '  ) ',
  ')   ',
  SQL_Empresa,
  SQL_GraGruX,
  ' ',
  'ORDER BY vmi.Controle  ',
  '']);
  //Geral.MB_Teste(Dmod.QrAux.SQL.Text);
  Result := Dmod.QrAux.RecordCount > 0;

  if Result then
    if Geral.MB_Pergunta('Existem ' + Geral.FF0(Dmod.QrAux.RecordCount) +
    ' registros inconsistentes deste artigo impedindo a gera��o do relat�rio!' +
    sLineBreak +
    'Execute a "Entrada de excedente" deles na guia "Ribeira 2".' + sLineBreak +
    'Deseja executar agora?') = ID_YES then
      VS_PF.MostraFormVSExcCab(0, 0);
end;

procedure TUnVS_PF.ReopenQrySaldoIMEI_Baixa(Qry1, Qry2: TmySQLQuery;
  SrcNivel2: Integer);
begin
  VS_CRC_PF.ReopenQrySaldoIMEI_Baixa(Qry1, Qry2, SrcNivel2);
end;

procedure TUnVS_PF.ReopenVSMovXXX(Qry: TmySQLQuery; Campo: String; IMEI,
  TemIMEIMrt, CtrlLoc: Integer);
begin
  VS_CRC_PF.ReopenVSMovXXX(Qry, Campo, IMEI, TemIMEIMrt, CtrlLoc);
end;

function TUnVS_PF.SQL_LJ_CMO: String;
begin
  Result := VS_CRC_PF.SQL_LJ_CMO;
end;

function TUnVS_PF.SQL_LJ_FRN: String;
begin
  Result := VS_CRC_PF.SQL_LJ_FRN;
end;

function TUnVS_PF.SQL_LJ_GGX: String;
begin
  Result := VS_CRC_PF.SQL_LJ_GGX;
end;

function TUnVS_PF.SQL_LJ_SCL: String;
begin
  Result := VS_CRC_PF.SQL_LJ_SCL;
end;

function TUnVS_PF.SQL_MovIDeNiv_Pos_Inn: String;
begin
  Result := VS_CRC_PF.SQL_MovIDeNiv_Pos_Inn;
end;

function TUnVS_PF.SQL_NO_CMO: String;
begin
  Result := VS_CRC_PF.SQL_NO_CMO;
end;

function TUnVS_PF.SQL_NO_FRN: String;
begin
  Result := VS_CRC_PF.SQL_NO_FRN;
end;

function TUnVS_PF.SQL_NO_GGX: String;
begin
  Result := VS_CRC_PF.SQL_NO_GGX;
end;

function TUnVS_PF.SQL_NO_SCL: String;
begin
  Result := VS_CRC_PF.SQL_NO_SCL;
end;

function TUnVS_PF.SQL_TipoEstq_DefinirCodi(CAST: Boolean; NomeFld: String;
  UsaVirgula: Boolean; FldQtdArM2, FldQtdPeso, FldQtdPeca,
  Prefacio: String): String;
begin
  Result := VS_CRC_PF.SQL_TipoEstq_DefinirCodi(CAST, NomeFld, UsaVirgula,
  FldQtdArM2, FldQtdPeso, FldQtdPeca, Prefacio);
end;

function TUnVS_PF.TabCacVS_Tab(Tab: TTabToWork): String;
begin
  Result := VS_CRC_PF.TabCacVS_Tab(Tab);
end;

function TUnVS_PF.TabMovVS_Fld_IMEI(Tab: TTabToWork): String;
begin
  Result := VS_CRC_PF.TabMovVS_Fld_IMEI(Tab);
end;

function TUnVS_PF.TabMovVS_Tab(Tab: TTabToWork): String;
begin
  Result := VS_CRC_PF.TabMovVS_Tab(Tab);
end;

procedure TUnVS_PF.ZeraSaldoIMEI(Controle: Integer);
begin
  VS_CRC_PF.ZeraSaldoIMEI(Controle);
end;

function TUnVS_PF.FichaErro(EdSerieFch: TdmkEdit; Empresa, Controle, Ficha:
Integer; PermiteDuplicar: Boolean): Boolean;
begin
  Result := VS_CRC_PF.FichaErro(EdSerieFch, Empresa, Controle, Ficha,
    PermiteDuplicar);
end;

function TUnVS_PF.ObrigaInfoIxx(IxxMovIX: TEstqMovInfo; IxxFolha, IxxLinha:
  Integer): Boolean;
begin
  Result := VS_CRC_PF.ObrigaInfoIxx(IxxMovIX, IxxFolha, IxxLinha);
end;

function TUnVS_PF.VSFic(GraGruX, Empresa, ClienteMO, Fornecedor, Pallet,
  Ficha: Integer; Pecas, AreaM2, PesoKg, ValorT: Double; EdGraGruX, EdPallet,
  EdFicha, EdPecas, EdAreaM2, EdPesoKg, EdValorT: TWinControl; ExigeFornecedor: Boolean;
  GraGruY: Integer; ExigeAreaouPeca: Boolean; EdStqCenLoc: TWinControl): Boolean;
begin
  Result := VS_CRC_PF.VSFic(GraGruX, Empresa, ClienteMO, Fornecedor, Pallet,
  Ficha, Pecas, AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas,
  EdAreaM2, EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca,
  EdStqCenLoc);
end;

{
function TUnVS_PF.InsUpdVSMovIts1(SQLType: TSQLType; Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro: Integer; MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv;
  Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  _Data_Hora_: String; SrcMovID: TEstqMovimID; SrcNivel1, SrcNivel2: Integer;
  Observ: String; LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,
  Misturou*): Integer; CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double; DstMovID:
  TEstqMovimID; DstNivel1, DstNivel2: Integer; QtdGerPeca, QtdGerPeso,
  QtdGerArM2, QtdGerArP2: Double; AptoUso, FornecMO, SerieFch: Integer;
  NotaMPAG: Double; SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
  GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2: Double; GGXRcl: Integer;
  JmpMovID: TEstqMovimID = emidAjuste;
  JmpNivel1: Integer = 0; JmpNivel2: Integer = 0; JmpGGX: Integer = 0;
  RmsMovID: TEstqMovimID = emidAjuste;
  RmsNivel1: Integer = 0; RmsNivel2: Integer = 0; RmsGGX: Integer = 0;
  GSPJmpMovID: TEstqMovimID = emidAjuste; GSPJmpNiv2: Integer = 0;
  (*ForcaDtCorrApo: TDateTime = 0;*) MovCodPai: Integer = 0;
  IxxMovIX: TEstqMovInfo = TEstqMovInfo.eminfIndef; IxxFolha: Integer = 0;
  IxxLinha: Integer = 0): Boolean;
begin
  Result := VS_CRC_PF.InsUpdVSMovIts1(SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, _Data_Hora_, SrcMovID, SrcNivel1, SrcNivel2, Observ,
  LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*, Misturou*), CustoMOKg,
  CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca,
  QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG,
  SrcGGX, DstGGX, Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda,
  GSPSrcMovID, GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab,
  ClientMO, QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, GGXRcl, JmpMovID,
  JmpNivel1, JmpNivel2, JmpGGX, RmsMovID, RmsNivel1, RmsNivel2, RmsGGX,
  GSPJmpMovID, GSPJmpNiv2, (*ForcaDtCorrApo: TDateTime = 0;*) MovCodPai,
  IxxMovIX, IxxFolha, IxxLinha);
end;
}

function TUnVS_PF.InsUpdVSMovIts2(SQLType: TSQLType; Codigo, MovimCod,
  MovimTwn, Empresa, Terceiro: Integer; MovimID: TEstqMovimID; MovimNiv:
  TEstqMovimNiv; Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2, AreaP2,
  ValorT: Double; _Data_Hora_: String; SrcMovID: TEstqMovimID; SrcNivel1,
  SrcNivel2: Integer; Observ: String; LnkNivXtr1, LnkNivXtr2, CliVenda,
  Controle, Ficha: Integer; CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
  DstMovID: TEstqMovimID; DstNivel1, DstNivel2: Integer; QtdGerPeca, QtdGerPeso,
  QtdGerArM2, QtdGerArP2: Double; AptoUso, FornecMO, SerieFch: Integer;
  NotaMPAG: Double; SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
  GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, PerceComiss, CusKgComiss,
  CustoComiss, CredPereImposto, CredValrImposto, CusFrtAvuls: Double; GGXRcl: Integer;
  RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI: Double;
  JmpMovID: TEstqMovimID; JmpNivel1, JmpNivel2, JmpGGX: Integer;
  RmsMovID: TEstqMovimID; RmsNivel1, RmsNivel2, RmsGGX: Integer;
  GSPJmpMovID: TEstqMovimID; GSPJmpNiv2, MovCodPai: Integer;
  IxxMovIX: TEstqMovInfo; IxxFolha, IxxLinha: Integer;
  ExigeClientMO, ExigeFornecMO, ExigeStqLoc: Boolean;
  InsUpdVMIPrcExecID: TInsUpdVMIPrcExecID): Boolean;
begin
  Result := VS_CRC_PF.InsUpdVSMovIts2(
    SQLType, Codigo, MovimCod, MovimTwn, Empresa, Terceiro,
    MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
    _Data_Hora_, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
    CliVenda, Controle, Ficha, CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP,
    DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
    QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
    TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
    ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO, QtdAntPeca, QtdAntPeso,
    QtdAntArM2, QtdAntArP2, PerceComiss, CusKgComiss, CustoComiss,
    CredPereImposto, CredValrImposto, CusFrtAvuls,
    GGXRcl,
    RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI,
    JmpMovID, JmpNivel1, JmpNivel2, JmpGGX,
    RmsMovID, RmsNivel1, RmsNivel2, RmsGGX, GSPJmpMovID, GSPJmpNiv2, MovCodPai,
    IxxMovIX, IxxFolha, IxxLinha, ExigeClientMO, ExigeFornecMO, ExigeStqLoc,
    InsUpdVMIPrcExecID);
end;

function TUnVS_PF.InsUpdVSMovIts3(SQLType: TSQLType; Codigo, MovimCod, MovimTwn,
              Empresa, Terceiro: Integer; MovimID: TEstqMovimID;
              MovimNiv: TEstqMovimNiv;
              Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2, AreaP2,
              ValorT: Double; _Data_Hora_: String; SrcMovID: TEstqMovimID;
              SrcNivel1, SrcNivel2: Integer; Observ: String;
              LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,
              Misturou*): Integer; CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
              DstMovID: TEstqMovimID; DstNivel1, DstNivel2: Integer;
              QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
              AptoUso, FornecMO, SerieFch: Integer; NotaMPAG: Double;
              SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto, PedItsLib,
              PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
              GSPSrcNiv2, ReqMovEstq,
              //StqCenLoc, ItemNFe, VSMulFrnCab: Integer; ClientMO: Integer = -11): Boolean;
              StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
              QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
              PerceComiss, CusKgComiss, CustoComiss, CredPereImposto,
              CredValrImposto, CusFrtAvuls: Double;
              GGXRcl: Integer;
              //
              RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI: Double;
              //
              JmpMovID: TEstqMovimID; JmpNivel1, JmpNivel2, JmpGGX: Integer;
              RmsMovID: TEstqMovimID; RmsNivel1, RmsNivel2, RmsGGX: Integer;
              GSPJmpMovID: TEstqMovimID; GSPJmpNiv2, MovCodPai, VmiPai: Integer;
              IxxMovIX: TEstqMovInfo; IxxFolha, IxxLinha: Integer;
              ExigeClientMO, ExigeFornecMO, ExigeStqLoc: Boolean;
              InsUpdVMIPrcExecID: TInsUpdVMIPrcExecID): Boolean;
begin
  Result := VS_CRC_PF.InsUpdVSMovIts3(
    SQLType, Codigo, MovimCod, MovimTwn, Empresa, Terceiro,
    MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
    _Data_Hora_, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
    CliVenda, Controle, Ficha, CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP,
    DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
    QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
    TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
    ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO, QtdAntPeca, QtdAntPeso,
    QtdAntArM2, QtdAntArP2, PerceComiss, CusKgComiss, CustoComiss,
    CredPereImposto, CredValrImposto, CusFrtAvuls, GGXRcl,
    RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI,
    JmpMovID, JmpNivel1, JmpNivel2, JmpGGX,
    RmsMovID, RmsNivel1, RmsNivel2, RmsGGX,
    GSPJmpMovID, GSPJmpNiv2, MovCodPai, VmiPai,
    IxxMovIX, IxxFolha, IxxLinha, ExigeClientMO, ExigeFornecMO, ExigeStqLoc,
    InsUpdVMIPrcExecID);
end;


procedure TUnVS_PF.AtualizaTotaisVSXxxCab(Tabela: String; MovimCod: Integer);
var
  Qry: TmySQLQuery;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
begin
  VS_CRC_PF.AtualizaTotaisVSXxxCab(Tabela, MovimCod);
(*
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    //
    'AND (NOT MovimNiv IN (' +
    Geral.FF0(Integer(TEstqMovimNiv.eminSorcClass)) + ',' + // 1 - // Classifica��o (m�ltipla)
    Geral.FF0(Integer(TEstqMovimNiv.eminSorcLocal)) +       // 27  // Transferencia de local
    '))', // Classifica��o (m�ltipla)
    '']);
    //
    Pecas          := Qry.FieldByName('Pecas').AsFloat;
    PesoKg         := Qry.FieldByName('PesoKg').AsFloat;
    AreaM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorT         := Qry.FieldByName('ValorT').AsFloat;

    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, LowerCase(Tabela), False, [
    'Pecas', 'PesoKg',
    'AreaM2', 'AreaP2',
    'ValorT'], [
    'MovimCod'], [
    Pecas, PesoKg,
    AreaM2, AreaP2,
    ValorT], [
    MovimCod], True);
    //
  finally
    Qry.Free;
  end;
*)
end;

function TUnVS_PF.ConfigContinuarInserindoFolhaLinha(
  RGModoContinuarInserindo: TRadioGroup; EdFolha, EdLinha: TdmkEdit): Boolean;
begin
  Result := VS_CRC_PF.ConfigContinuarInserindoFolhaLinha(
  RGModoContinuarInserindo, EdFolha, EdLinha);
(*
  if RGModoContinuarInserindo.ItemIndex = 2 then
  begin
    Result := True;
    MyObjects.IncrementaFolhaLinha(EdFolha, EdLinha, CO_VS_MaxLin);
  end else
    Result := False;
*)
end;

function TUnVS_PF.ObtemProximaFichaRMP(const Empresa: Integer; const EdSerieFch:
  TdmkEditCB; var Ficha: Integer; SerieDefinida: Integer): Boolean;
var
  Qry: TmySQLQuery;
  SerieFch: Integer;
begin
  Result := VS_CRC_PF.ObtemProximaFichaRMP(Empresa, EdSerieFch, Ficha, SerieDefinida);
(*
  Result := False;
  if SerieDefinida <> 0 then
    SerieFch := SerieDefinida
  else
    SerieFch := EdSerieFch.ValueVariant;
  if MyObjects.FIC(SerieFch = 0, EdSerieFch, 'Defina a s�rie primeiro!') then
    Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(Ficha) Ficha ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND SerieFch=' + Geral.FF0(SerieFch),
    '']);
    if Qry.FieldByName('Ficha').AsInteger = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT MAX(Ficha) Ficha ',
      'FROM ' + CO_TAB_VMB,
      'WHERE Empresa=' + Geral.FF0(Empresa),
      'AND SerieFch=' + Geral.FF0(SerieFch),
      '']);
    end;
    //
    Ficha := Qry.FieldByName('Ficha').AsInteger + 1;
    Result := True;
  finally
    Qry.Free;
  end;
*)
end;

function TUnVS_PF.DefineDatasVMI(const _Data_Hora_: String; var DataHora,
  DtCorrApo: String): Boolean;
begin
  Result := VS_CRC_PF.DefineDatasVMI(_Data_Hora_, DataHora, DtCorrApo);
end;

function TUnVS_PF.HabilitaMenuInsOuAllVSAberto(Qry: TmySQLQuery;
  Data: TDateTime; Button: TWinControl; DefMenu: TPopupMenu; MenuItens: array of TMenuItem): Boolean;
begin
  Result := VS_CRC_PF.HabilitaMenuInsOuAllVSAberto(Qry, Data, Button, DefMenu,
  MenuItens);
end;

function TUnVS_PF.HabilitaComposVSAtivo(ID_TTW: Integer;
  Compos: array of TComponent): Boolean;
begin
  Result := VS_CRC_PF.HabilitaComposVSAtivo(ID_TTW, Compos);
end;

function TUnVS_PF.ExcluiControleVSMovIts(Tabela: TmySQLQuery;
  Campo: TIntegerField;
  Controle1, CtrlBaix, SrcNivel2: Integer; Gera: Boolean;
  Motivo: Integer; Pergunta: Boolean = True; Reabre: Boolean = True): Boolean;
begin
  Result := VS_CRC_PF.ExcluiControleVSMovIts(Tabela, Campo, Controle1, CtrlBaix,
  SrcNivel2, Gera, Motivo, Pergunta, Reabre);
end;

procedure TUnVS_PF.InsereVSMovCab(Codigo: Integer; MovimID: TEstqMovimID;
  CodigoID: Integer);
begin
  VS_CRC_PF.InsereVSMovCab(Codigo, MovimID, CodigoID);
end;

procedure TUnVS_PF.ObtemNomeCamposNFeVSXxxCab(const MovimID: TEstqMovimID; var
  FldSer, FldNum: String);
begin
  VS_CRC_PF.ObtemNomeCamposNFeVSXxxCab(MovimID, FldSer, FldNum);
end;

function TUnVS_PF.DefineSiglaVS_NFe(NFeSer, NFeNum: Integer): String;
begin
  Result := VS_CRC_PF.DefineSiglaVS_NFe(NFeSer, NFeNum);
end;

procedure TUnVS_PF.MostraFormVSRclArtPrpNew(SQLType: TSQLType; Pallet, Pallet1,
Pallet2, Pallet3, Pallet4, Pallet5, Pallet6, Digitador, Revisor: Integer;
Reclasse: Boolean; StqCenLoc, FornecMO: Integer);
begin
  VS_CRC_PF.MostraFormVSRclArtPrpNew(SQLType, Pallet, Pallet1, Pallet2, Pallet3,
  Pallet4, Pallet5, Pallet6, Digitador, Revisor, Reclasse, StqCenLoc, FornecMO);
end;

function TUnVS_PF.PalletDuplicado(MaxBox, p1, p2, p3, p4, p5,
  p6: Integer): Boolean;
begin
  Result := VS_CRC_PF.PalletDuplicado(MaxBox, p1, p2, p3, p4, p5, p6);
end;

procedure TUnVS_PF.InsereVSCacCab(Codigo: Integer; MovimID: TEstqMovimID;
  CodigoID: Integer);
begin
  VS_CRC_PF.InsereVSCacCab(Codigo, MovimID, CodigoID);
end;

function TUnVS_PF.InsAltVSPalRclNewUni(const Empresa, fornecMO, ClientMO, Fornecedor,
  VSMulFrnCab: Integer; const MovimID: TEstqMovimID; const Codigo, MovimCod,
  BxaGraGruX, BxaMovimID, BxaMovimNiv,
  BxaSrcNivel1, BxaSrcNivel2, VSMovIts, Tecla, VSPallet, GragruX: Integer;
  const SrcPallet, StqCenLoc: Integer;
  const IuvpeiInn, IuvpeiBxa: TInsUpdVMIPrcExecID;
  const LaAviso1, LaAviso2: TLabel; var CtrlSorc, CtrlDest: Integer): Integer;
begin
  Result := VS_CRC_PF.InsAltVSPalRclNewUni(Empresa, FornecMO, ClientMO, Fornecedor,
  VSMulFrnCab, MovimID, Codigo, MovimCod, BxaGraGruX, BxaMovimID, BxaMovimNiv,
  BxaSrcNivel1, BxaSrcNivel2, VSMovIts, Tecla, VSPallet, GragruX, SrcPallet,
  StqCenLoc, IuvpeiInn, IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc, CtrlDest);
end;

procedure TUnVS_PF.MostraFormVSGeraArt(Codigo, Controle: Integer);
begin
  VS_CRC_PF.MostraFormVSGeraArt(Codigo, Controle);
end;

procedure TUnVS_PF.MostraFormVSGerArtDdImpAll;
begin
  if DBCheck.CriaFm(TFmVSGerArtDdImpAll, FmVSGerArtDdImpAll, afmoNegarComAviso) then
  begin
    FmVSGerArtDdImpAll.ShowModal;
    FmVSGerArtDdImpAll.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSGerArtDdImpBar();
begin
  if DBCheck.CriaFm(TFmVSGerArtDdImpBar, FmVSGerArtDdImpBar, afmoNegarComAviso) then
  begin
    FmVSGerArtDdImpBar.ShowModal;
    FmVSGerArtDdImpBar.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSGerArtDdImpCur();
begin
  if DBCheck.CriaFm(TFmVSGerArtDdImpCur, FmVSGerArtDdImpCur, afmoNegarComAviso) then
  begin
    FmVSGerArtDdImpCur.ShowModal;
    FmVSGerArtDdImpCur.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSGerArtMarcaImpBar();
begin
  if DBCheck.CriaFm(TFmVSGerArtMarcaImpBar, FmVSGerArtMarcaImpBar, afmoNegarComAviso) then
  begin
    FmVSGerArtMarcaImpBar.ShowModal;
    FmVSGerArtMarcaImpBar.Destroy;
  end;
end;

procedure TUnVS_PF.ReopenVSPallet(QrVSPallet: TmySQLQuery; Empresa, ClientMO, GraGruX:
Integer; Ordem: String; PallOnEdit: array of Integer);
begin
  VS_CRC_PF.ReopenVSPallet(QrVSPallet, Empresa, ClientMO, GraGruX, Ordem,
  PallOnEdit);
end;

function TUnVS_PF.AreaEstaNaMedia(Pecas, AreaM2, MediaMinM2,
  MediaMaxM2: Double; Pergunta: Boolean): Boolean;
begin
  Result := VS_CRC_PF.AreaEstaNaMedia(Pecas, AreaM2, MediaMinM2, MediaMaxM2,
  Pergunta);
end;

function TUnVS_PF.FatorNotaAR(GraGruX: Integer): Double;
begin
  Result :=  VS_CRC_PF.FatorNotaAR(GraGruX);
end;

procedure TUnVS_PF.DistribuiCustoIndsVS(MovimNiv: TEstqMovimNiv; MovimCod,
  Codigo, CtrlDst: Integer);
begin
  VS_CRC_PF.DistribuiCustoIndsVS(MovimNiv, MovimCod, Codigo, CtrlDst);
end;

procedure TUnVS_PF.AtualizaCustosDescendentesGerArtFicha(Empresa, SerieFch,
  Ficha: Integer);
begin
  VS_CRC_PF.AtualizaCustosDescendentesGerArtFicha(Empresa, SerieFch, Ficha);
end;

procedure TUnVS_PF.AtualizaValoresDescendArtGerAposInfoAreaTotal(
  IMEIArtGer: Integer; AreaM2, ValorT: Double);
begin
  VS_CRC_PF.AtualizaValoresDescendArtGerAposInfoAreaTotal(
  IMEIArtGer, AreaM2, ValorT);
end;

function TUnVS_PF.ObtemMovimCodDeMovimIDECodigo(MovimID: TEstqMovimID; Codigo:
  Integer): Integer;
begin
  Result := VS_CRC_PF.ObtemMovimCodDeMovimIDECodigo(MovimID, Codigo);
end;

function TUnVS_PF.CordaIMEIS(Itens: array of Integer): String;
begin
  Result := VS_CRC_PF.CordaIMEIS(Itens);
end;

function TUnVS_PF.IMEI_JaEstaNoArray(const IMEI: Integer;
  const Lista: array of Integer; var ItensLoc: Integer): Boolean;
begin
  Result := VS_CRC_PF.IMEI_JaEstaNoArray(IMEI, Lista, ItensLoc);
end;

procedure TUnVS_PF.DefineDataHoraOuDtCorrApoCompos(const DataHora,
  DtCorrApo: TDateTime; TPData: TdmkEditDateTimePicker;
  EdHora: TdmkEdit);
begin
  VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(DataHora, DtCorrApo, TPData, EdHora);
end;

procedure TUnVS_PF.DefineDataHoraOuDtCorrApoCompos(const DataHora,
  DtCorrApo: TDateTime; var _DataHora: TDateTime);
begin
  VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(DataHora, DtCorrApo, _DataHora);
end;

procedure TUnVS_PF.AtualizaSerieNFeVMI(Controle, NFeSer, NFeNum, VSMulNFeCab: Integer);
begin
  VS_CRC_PF.AtualizaSerieNFeVMI(Controle, NFeSer, NFeNum, VSMulNFeCab);
end;

procedure TUnVS_PF.AtualizaValoresDescend_0000_All(Qry: TmySQLQuery;
  CustoM2: Double);
begin
  VS_CRC_PF.AtualizaValoresDescend_0000_All(Qry, CustoM2);
end;

procedure TUnVS_PF.AtualizaValoresDescend_ID02_Venda(Controle,
  MovimCod: Integer; AreaM2, CustoM2: Double);
begin
  VS_CRC_PF.AtualizaValoresDescend_ID02_Venda(Controle, MovimCod, AreaM2,
  CustoM2);
end;

procedure TUnVS_PF.AtualizaValoresDescend_ID14_ClassArtVSMul(MovimTwn: Integer;
  CustoM2: Double);
begin
  VS_CRC_PF.AtualizaValoresDescend_ID14_ClassArtVSMul(MovimTwn, CustoM2);
end;

procedure TUnVS_PF.AtualizaValoresDescend_ID19_EmProcWE(Controle, MovimCod: Integer;
  AreaM2, CustoM2: Double);
begin
  VS_CRC_PF.AtualizaValoresDescend_ID19_EmProcWE(Controle, MovimCod, AreaM2,
  CustoM2);
end;

procedure TUnVS_PF.AtualizaValoresDescend_ID25_TransfLocal(Controle,
  MovimCod: Integer; AreaM2, CustoM2: Double);
begin
  VS_CRC_PF.AtualizaValoresDescend_ID25_TransfLocal(Controle, MovimCod, AreaM2,
  CustoM2);
end;

procedure TUnVS_PF.MostraFormVSReclassifOneNew(Codigo, CacCod, StqCenLoc: Integer;
  MovimID: TEstqMovimID);
begin
  VS_CRC_PF.MostraFormVSReclassifOneNew(Codigo, CacCod, StqCenLoc, MovimID);
end;

function TUnVS_PF.PalletDuplicad3(MaxBox: Integer;
  Pallets: array of Integer): Boolean;
begin
  Result := VS_CRC_PF.PalletDuplicad3(MaxBox, Pallets);
end;

function TUnVS_PF.InsAltVSPalCla(const Empresa, ClientMO, Fornecedor,
  VSMulFrnCab: Integer; const MovimID: TEstqMovimID; const Codigo, MovimCod,
  BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
  Tecla, VSPallet, GragruX, StqCenLoc: Integer; const IuvpeiInn, IuvpeiBxa:
  TInsUpdVMIPrcExecID; const LaAviso1, LaAviso2: TLabel; var CtrlSorc, CtrlDest:
  Integer): Integer;
begin
  Result := VS_CRC_PF.InsAltVSPalCla(Empresa, ClientMO, Fornecedor, VSMulFrnCab,
  MovimID, Codigo, MovimCod, BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1,
  BxaSrcNivel2, VSMovIts, Tecla, VSPallet, GragruX, StqCenLoc, IuvpeiInn,
  IuvpeiBxa, LaAviso1, LaAviso2, CtrlSorc, CtrlDest);
end;

procedure TUnVS_PF.ImprimePallet_Varios(Empresa: Integer; Pallets: array of
  Integer; JanTab: String; InfoNO_PALLET: Boolean);
begin
  VS_CRC_PF.ImprimePallet_Varios(Empresa, Pallets, JanTab, InfoNO_PALLET);
end;


function TUnVS_PF.CadastraPalletInfo(Empresa, ClientMO, GraGruX, GraGruY:
  Integer; QrVSPallet: TmySQLQuery; EdPallet: TdmkEditCB; CBPallet:
  TdmkDBLookupComboBox; SBNewPallet: TSpeedButton; EdPecas: TdmkEdit): Integer;
begin
  Result := VS_CRC_PF.CadastraPalletInfo(Empresa, ClientMO, GraGruX, GraGruY, QrVSPallet,
  EdPallet, CBPallet, SBNewPallet, EdPecas);
end;

function TUnVS_PF.FatorNotaCC(GraGruX, MovimID: Integer): Double;
begin
  Result := VS_CRC_PF.FatorNotaCC(GraGruX, MovimID);
end;

procedure TUnVS_PF.AtualizaSaldoItmCal(Controle: Integer);
begin
  VS_CRC_PF.AtualizaSaldoItmCal(Controle);
end;

procedure TUnVS_PF.AtualizaSaldoItmCur(Controle: Integer);
begin
  VS_CRC_PF.AtualizaSaldoItmCur(Controle);
end;

procedure TUnVS_PF.MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc: TControl;
  EdStqCenLoc: TdmkEditCB; CBStqCenLoc: TdmkDBLookupComboBox; QrStqCenLoc:
  TmySQLQuery; MovimID: TEstqMovimID);
begin
  VS_CRC_PF.MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc, EdStqCenLoc, CBStqCenLoc,
  QrStqCenLoc, MovimID);
end;

procedure TUnVS_PF.MostraFormVSMovimID(MovimID: TEstqMovimID);
begin
   VS_CRC_PF.MostraFormVSMovimID(MovimID);
end;

(*
function TUnVS_PF.LocalizaPorCampo(EstqMovimID: TEstqMovimID; Caption, Campo:
  String): Integer;
begin
  Result := VS_CRC_PF.LocalizaPorCampo(EstqMovimID, Caption, Campo);
end;
*)

function TUnVS_PF.LocalizaPeloIMEC(EstqMovimID: TEstqMovimID): Integer;
begin
  Result := VS_CRC_PF.LocalizaPeloIMEC(EstqMovimID);
end;

function TUnVS_PF.LocalizaPeloIMEI(EstqMovimID: TEstqMovimID): Integer;
begin
  Result := VS_CRC_PF.LocalizaPeloIMEI(EstqMovimID);
end;

function TUnVS_PF.LocalizaPelaOC(EstqMovimID: TEstqMovimID): Integer;
begin
  Result := VS_CRC_PF.LocalizaPelaOC(EstqMovimID);
end;

function TUnVS_PF.MostraFormVSRMPPsq(const MovimID: TEstqMovimID;
  const MovimNivs: array of TEstqMovimNiv; const SQLType: TSQLType; var Codigo,
  Controle: Integer; OnlySelIMEI: Boolean): Boolean;
begin
  Result := VS_CRC_PF.MostraFormVSRMPPsq(MovimID, MovimNivs, SQLType, Codigo,
  Controle, OnlySelIMEI);
end;

function TUnVS_PF.DefineIDs_Str(DBGridZTO: TdmkDBGridZTO; Query:
              TmySQLQuery; Campo: String): String;
begin
  Result := VS_CRC_PF.DefineIDs_Str(DBGridZTO, Query, Campo);
end;

function TUnVS_PF.DefineQtdePedIts(PrecoTipo: TTipoCalcCouro; Pecas, PesoKg,
  AreaM2, AreaP2: Double): Double;
begin
  case PrecoTipo of
    ptcNaoInfo: Result := 0;
    ptcPecas  : Result := Pecas;
    ptcPesoKg : Result := PesoKg;
    ptcAreaM2 : Result := AreaM2;
    ptcAreaP2 : Result := AreaP2;
    ptcTotal  : Result := 0;
    else
    begin
      Geral.MB_Erro('"TipoCalcCouro" n�o implementado!');
      Result := 0;
    end;
  end;
end;

procedure TUnVS_PF.ImprimeClassIMEI(IMEI, VSGerArt, MulCab_MovimCod: Integer);
begin
  VS_CRC_PF.ImprimeClassIMEI(IMEI, VSGerArt, MulCab_MovimCod);
end;

procedure TUnVS_PF.ImprimeIMEI(IMEIs: array of Integer; VSImpImeiKind:
  TXXImpImeiKind; LPFMO, FNFeRem: String; QryCab: TmySQLQuery);
begin
  VS_CRC_PF.ImprimeIMEI(IMEIs, VSImpImeiKind, LPFMO, FNFeRem, QryCab);
end;

procedure TUnVS_PF.ReopenVSGerArtDst(Qry: TmySQLQuery; MovimCod, Controle,
  TemIMEIMrt: Integer; EstqMovimNiv: TEstqMovimNiv);
begin
  VS_CRC_PF.ReopenVSGerArtDst(Qry, MovimCod, Controle, TemIMEIMrt, EstqMovimNiv);
end;

procedure TUnVS_PF.ReopenVSGerArtSrc(Qry: TmySQLQuery; MovimCod, Controle,
  TemIMEIMrt: Integer; SQL_Limit: String; EstqMovimNiv: TEstqMovimNiv);
begin
  VS_CRC_PF.ReopenVSGerArtSrc(Qry, MovimCod, Controle, TemIMEIMrt, SQL_Limit,
  EstqMovimNiv);
end;

function TUnVS_PF.FatoresIncompativeis(FatorSrc, FatorDst: Integer; MeAviso:
  TMemo): Boolean;
begin
  Result := VS_CRC_PF.FatoresIncompativeis(FatorSrc, FatorDst, MeAviso);
end;

procedure TUnVS_PF.MostraFormVSMovImp(AbrirEmAba: Boolean; InOwner: TWincontrol;
  PageControl: TdmkPageControl; PageIndex: Integer);
begin
  VS_CRC_PF.MostraFormVSMovImp(AbrirEmAba, InOwner, PageControl, PageIndex);
end;

procedure TUnVS_PF.PesquisaDoubleVS(Campo: String; Valor: Double; TemIMEIMrt:
  Integer);
begin
  VS_CRC_PF.PesquisaDoubleVS(Campo, Valor, TemIMEIMrt);
end;

procedure TUnVS_PF.ReopenVSOpePrcDst(Qry: TmySQLQuery; MovimCod, Controle,
  TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv;
  SQL_Limit: String);
begin
  VS_CRC_PF.ReopenVSOpePrcDst(Qry, MovimCod, Controle, TemIMEIMrt, MovimNiv,
  SQL_Limit);
end;

procedure TUnVS_PF.ReopenVSPWEDesclDst(Qry: TmySQLQuery; Codigo, Controle,
  Pallet, TemIMEIMrt: Integer);
begin
  VS_CRC_PF.ReopenVSPWEDesclDst(Qry, Codigo, Controle, Pallet, TemIMEIMrt);
end;

procedure TUnVS_PF.ReopenVSOpePrcBxa(Qry: TmySQLQuery; MovimCod, MovimTwn,
  Controle, TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
begin
  VS_CRC_PF.ReopenVSOpePrcBxa(Qry, MovimCod, MovimTwn, Controle, TemIMEIMrt,
  MovimNiv, SQL_Limit);
end;

function TUnVS_PF.ObtemFatorIntDeGraGruXeVeSeDifere(const GraGruX1,
  GraGruX2: Integer; var FatorInt1, FatorInt2, Fator1para2: Double): Boolean;
begin
  Result := VS_CRC_PF.ObtemFatorIntDeGraGruXeVeSeDifere(GraGruX1, GraGruX2,
  FatorInt1, FatorInt2, Fator1para2);
end;

procedure TUnVS_PF.AtualizaVSValorT(Controle: Integer);
begin
  VS_CRC_PF.AtualizaVSValorT(Controle);
end;

function TUnVS_PF.AlteraVMI_PesoKg(CampoPeso: String; MovimID, MovimNiv, Controle: Integer;
  Default: Double; PermiteNegativo: TSinal): Boolean;
begin
  Result := VS_CRC_PF.AlteraVMI_PesoKg(CampoPeso, MovimID, MovimNiv, Controle,
  Default, PermiteNegativo);
end;

function TUnVS_PF.SenhaVSPwdDdNaoConfere(Digitado: String): Boolean;
begin
  Result := VS_CRC_PF.SenhaVSPwdDdNaoConfere(Digitado);
end;

function TUnVS_PF.DefineSiglaVS_Frn(Fornece: Integer): String;
begin
  Result := VS_CRC_PF.DefineSiglaVS_Frn(Fornece);
end;

procedure TUnVS_PF.ReopenQrySaldoPallet_Visual(Qry: TmySQLQuery;
Empresa, Pallet, VSMovIts: Integer);
begin
  VS_CRC_PF.ReopenQrySaldoPallet_Visual(Qry, Empresa, Pallet, VSMovIts);
end;

procedure TUnVS_PF.MostraFormVSDivCouMeio(MovimID: TEstqMovimID;
Boxes: array of Boolean; LaTipo_Caption: String);
begin
  VS_CRC_PF.MostraFormVSDivCouMeio(MovimID, Boxes, LaTipo_Caption);
end;

function TUnVS_PF.EncerraPalletReclassificacaoNew(const VSPaRclCabCacCod,
  VSPaRclCabCodigo, VSPaRclCabVSPallet, Box_Box, Box_VSPaClaIts, Box_VSPallet,
  Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
  const EncerrandoTodos, Pergunta: Boolean; var ReabreVSPaRclCab: Boolean): Boolean;
begin
  Result := VS_CRC_PF.EncerraPalletReclassificacaoNew(VSPaRclCabCacCod,
  VSPaRclCabCodigo, VSPaRclCabVSPallet, Box_Box, Box_VSPaClaIts, Box_VSPallet,
  Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest, EncerrandoTodos, Pergunta,
  ReabreVSPaRclCab);
end;

function TUnVS_PF.ZeraEstoquePalletOrigemReclas(Pallet, Codigo, MovimCod,
  Empresa: Integer; BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2, BxaValorT:
  Double; DataEHora: TDateTime; Iuvpei: TInsUpdVMIPrcExecID): Boolean;
begin
  Result := VS_CRC_PF.ZeraEstoquePalletOrigemReclas(Pallet, Codigo, MovimCod,
  Empresa, BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2, BxaValorT, DataEHora,
  Iuvpei);
end;

function TUnVS_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Where, SQL_Group:
  String; Tab: TTabToWork; TemIMEIMrt: Integer): String;
begin
  Result := VS_CRC_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Where,
  SQL_Group, Tab, TemIMEIMrt);
end;

function TUnVS_PF.TabMovVS_Fld_Pall(Tab: TTabToWork): String;
begin
  Result := VS_CRC_PF.TabMovVS_Fld_Pall(Tab);
end;

procedure TUnVS_PF.MostraFormVSReclassPrePalCac(SQLType: TSQLType; Pallet1,
  Pallet2, Pallet3, Pallet4, Pallet5, Pallet6, Digitador, Revisor: Integer);
begin
  VS_CRC_PF.MostraFormVSReclassPrePalCac(SQLType, Pallet1, Pallet2, Pallet3,
  Pallet4, Pallet5, Pallet6, Digitador, Revisor);
end;

procedure TUnVS_PF.MostraFormVSRepMer(GraGruX: Integer; Edita: Boolean;
  QryMul: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmVSRepMer, FmVSRepMer, afmoNegarComAviso) then
  begin
    //FmVSRepMer.PnNavi.Enabled := False;
    //FmVSRepMer.GB_L.Enabled := False;
    FmVSRepMer.FQryMul := QryMul;
    if GraGruX <> 0 then
    begin
      FmVSRepMer.LocCod(GraGruX, GraGruX);
      if FmVSRepMer.QrVSRepMerGraGruX.Value = GraGruX then
        FmVSRepMer.AlteraArtigoClassificadoAtual1Click(Self)
      else
        Geral.MB_Aviso('Cadastro n�o localizado: ' + Geral.FF0(GraGruX));
    end;
    if Edita then
      FmVSRepMer.FSeq := 1;
    FmVSRepMer.ShowModal;
    FmVSRepMer.Destroy;
  end;
end;

procedure TUnVS_PF.MostraFormVSRclArtSel();
begin
  VS_CRC_PF.MostraFormVSRclArtSel();
end;

procedure TUnVS_PF.MostraFormVSGerRclCab(Codigo, Controle: Integer);
begin
  VS_CRC_PF.MostraFormVSGerRclCab(Codigo, Controle);
end;

procedure TUnVS_PF.MostraFormVSPrePalCab(Codigo: Integer);
begin
  VS_CRC_PF.MostraFormVSPrePalCab(Codigo);
end;

procedure TUnVS_PF.LimpaArray15Int(var Arr: TClass15Int);
begin
  VS_CRC_PF.LimpaArray15Int(Arr);
end;

procedure TUnVS_PF.MostraFormVSClaArtPrpQnz(Pallets: array of Integer;
  FormAFechar: TForm; IMEI, DV, StqCenLoc: Integer);
begin
  VS_CRC_PF.MostraFormVSClaArtPrpQnz(Pallets, FormAFechar, IMEI, DV, StqCenLoc);
end;

procedure TUnVS_PF.ReopenVSGerArtDst_ToClassPorFicha(QrVSGerArtNew: TmySQLQuery;
  Ficha, Controle: Integer; SoNaoZerados: Boolean);
begin
  VS_CRC_PF.ReopenVSGerArtDst_ToClassPorFicha(QrVSGerArtNew, Ficha, Controle,
  SoNaoZerados);
end;

procedure TUnVS_PF.ReopenVSGerArtDst_ToClassPorIMEI(QrVSGerArtNew: TmySQLQuery;
  IMEI: Integer);
begin
  VS_CRC_PF.ReopenVSGerArtDst_ToClassPorIMEI(QrVSGerArtNew, IMEI);
end;

procedure TUnVS_PF.MostraFormVSClassifOneRetIMEI_06(Pallet1, Pallet2, Pallet3,
  Pallet4, Pallet5, Pallet6: Integer; Form: TForm);
begin
  VS_CRC_PF.MostraFormVSClassifOneRetIMEI_06(Pallet1, Pallet2, Pallet3, Pallet4,
  Pallet5, Pallet6, Form, 6);
end;

procedure TUnVS_PF.MostraFormVSClassifOne(Codigo, CacCod: Integer;
  MovimID: TEstqMovimID; FormPreDef: Integer);
begin
  VS_CRC_PF.MostraFormVSClassifOne(Codigo, CacCod, MovimID, FormPreDef);
end;

procedure TUnVS_PF.MostraFormVSClaArtPrpMDz(Pallet1, Pallet2, Pallet3, Pallet4,
  Pallet5, Pallet6: Integer; FormAFechar: TForm; IMEI, DV, StqCenLoc: Integer);
begin
  VS_CRC_PF.MostraFormVSClaArtPrpMDz(Pallet1, Pallet2, Pallet3, Pallet4,
  Pallet5, Pallet6, FormAFechar, IMEI, DV, StqCenLoc);
end;

procedure TUnVS_PF.MostraFormVSClaArtSel_IMEI();
begin
  VS_CRC_PF.MostraFormVSClaArtSel_IMEI();
end;

procedure TUnVS_PF.MostraFormVSClaArtSel_FRMP();
begin
  VS_CRC_PF.MostraFormVSClaArtSel_FRMP();
end;

procedure TUnVS_PF.MostraFormVSGerClaCab(Codigo: Integer);
begin
  VS_CRC_PF.MostraFormVSGerClaCab(Codigo);
end;

procedure TUnVS_PF.MostraFormVSPaMulCabR(Codigo: Integer);
begin
  VS_CRC_PF.MostraFormVSPaMulCabR(Codigo);
end;

function TUnVS_PF.ObtemNomeGraGruYDestdeOrig(GraGruY: Integer): Integer;
begin
  Result := VS_CRC_PF.ObtemNomeGraGruYDestdeOrig(GraGruY);
end;

function TUnVS_PF.CalculaValorT(IniPecas, IniAreaM2, IniPesoKg, IniValorT,
  Pecas, AreaM2, PesoKg: Double): Double;
begin
  Result := VS_CRC_PF.CalculaValorT(IniPecas, IniAreaM2, IniPesoKg, IniValorT,
  Pecas, AreaM2, PesoKg);
end;

{
procedure TUnVS_PF.ExcluiAtrelamentoMOEnvAvu(QrVSMOEnvAvu: TmySQLQuery);
var
  VSMOEnvAvu, MovimCod: Integer;
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o do atrelamento selecionado e seus itens?') = ID_YES then
  begin
    MovimCod := QrVSMOEnvAvu.FieldByName('VSVMI_MovimCod').AsInteger;
    VSMOEnvAvu := QrVSMOEnvAvu.FieldByName('Codigo').AsInteger;
    //
    Dmod.MyDB.Execute('DELETE FROM vsmoenvavmi WHERE VSMOEnvAvu=' + Geral.FF0(VSMOEnvAvu));
    //
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrVSMOEnvAvu, 'vsmoenvavu', ['Codigo'],
    ['Codigo'], True, '');
    DmModVS_CRC.CalculaFreteVMIdeMOEnvAvu(MovimCod);

    UnDmkDAC_PF.AbreQuery(QrVSMOEnvAvu, Dmod.MyDB);
  end;
end;
}

{
procedure TUnVS_PF.ExcluiAtrelamentoMOEnvEnv(QrVSMOEnvEnv: TmySQLQuery);
var
  VSMOEnvEnv, MovimCod: Integer;
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o do atrelamento selecionado e seus itens?') = ID_YES then
  begin
    MovimCod := QrVSMOEnvEnv.FieldByName('VSVMI_MovimCod').AsInteger;
    VSMOEnvEnv := QrVSMOEnvEnv.FieldByName('Codigo').AsInteger;
    //
    Dmod.MyDB.Execute('DELETE FROM vsmoenvevmi WHERE VSMOEnvEnv=' + Geral.FF0(VSMOEnvEnv));
    //
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrVSMOEnvEnv, 'vsmoenvenv', ['Codigo'],
    ['Codigo'], True, '');
    DmModVS_CRC.CalculaFreteVMIdeMOEnvEnv(MovimCod);
    DmModVS_CRC.AtualizaFreteVMIdeMOEnvEnv(QrVMIControle.Value);

    UnDmkDAC_PF.AbreQuery(QrVSMOEnvEnv, Dmod.MyDB);
  end;
end;
}

{
procedure TUnVS_PF.ExcluiAtrelamentoMOEnvRet(QrVSMOEnvRet, QrVSMOEnvRVMI: TmySQLQuery);
var
  VSMOEnvRet(*, MovimCod*): Integer;
  ListaEnvEnv: TMyGrlArrInt;
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o do atrelamento selecionado e seus itens?') = ID_YES then
  begin
    //MovimCod := QrVSMOEnvEnv.FieldByName('VSVMI_MovimCod').AsInteger;
    VSMOEnvRet := QrVSMOEnvRet.FieldByName('Codigo').AsInteger;
    //
    SetLength(ListaEnvEnv, QrVSMOEnvRVMI.RecordCount);
    QrVSMOEnvRVMI.First;
    while not QrVSMOEnvRVMI.Eof do
    begin
      ListaEnvEnv[QrVSMOEnvRVMI.RecNo -1 ] := QrVSMOEnvRVMI.FieldByName('VSMOEnvEnv').AsInteger;
      //
      QrVSMOEnvRVMI.Next;
    end;
    //
    Dmod.MyDB.Execute('DELETE FROM vsmoenvgvmi WHERE VSMOEnvRet=' + Geral.FF0(VSMOEnvRet));
    Dmod.MyDB.Execute('DELETE FROM vsmoenvrvmi WHERE VSMOEnvRet=' + Geral.FF0(VSMOEnvRet));
    //
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrVSMOEnvRet, 'vsmoenvret', ['Codigo'],
    ['Codigo'], True, '');
    //
    DmModVS_CRC.AtualizaSaldoVSMOEnvRet(ListaEnvEnv);
    //
    UnDmkDAC_PF.AbreQuery(QrVSMOEnvRet, Dmod.MyDB);
  end;
end;
}

function TUnVS_PF.ExcluiCabecalhoReclasse(MovimID: TEstqMovimID; PreClasse,
  Reclasse: Integer): Boolean;
begin
  Result := VS_CRC_PF.ExcluiCabecalhoReclasse(MovimID, PreClasse, Reclasse);
end;

function TUnVS_PF.ValidaCampoNF(Operacao: Integer; EditNF: TdmkEdit;
  MostraMsg: Boolean): Boolean;
begin
  Result := VS_CRC_PF.ValidaCampoNF(Operacao, EditNF, MostraMsg);
end;

procedure TUnVS_PF.AtualizaSerieNFeVMC(MovimCod, NFeSer, NFeNum,
  VSMulNFeCab: Integer);
begin
  VS_CRC_PF.AtualizaSerieNFeVMC(MovimCod, NFeSer, NFeNum, VSMulNFeCab);
end;

function TUnVS_PF.ObtemListaOperacoes(): TStringList;
begin
  Result := VS_CRC_PF.ObtemListaOperacoes();
end;

procedure TUnVS_PF.MostraFormVSInnCab(Codigo, VSInnIts, VSReclas: Integer);
begin
  VS_CRC_PF.MostraFormVSInnCab(Codigo, VSInnIts, VSReclas);
end;

procedure TUnVS_PF.MostraFormVSInnNatPend();
begin
  if DBCheck.CriaFm(TFmVSInnNatPend, FmVSInnNatPend, afmoNegarComAviso) then
  begin
    FmVSInnNatPend.ShowModal;
    FmVSInnNatPend.Destroy;
  end;
end;

function TUnVS_PF.InsereVSMovDif(Controle: Integer; Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, InfPecas, InfPesoKg, InfAreaM2, InfAreaP2,
  InfValorT, PerQbrViag, PerQbrSal,
  RstCouPc, RstCouKg, RstCouVl, RstSalKg, RstSalVl, RstTotVl: Double;
  TribDefSel: Integer; PesoSalKg: Double): Boolean;
begin
  Result := VS_CRC_PF.InsereVSMovDif(Controle, Pecas, PesoKg, AreaM2, AreaP2,
  ValorT, InfPecas, InfPesoKg, InfAreaM2, InfAreaP2, InfValorT, PerQbrViag,
  PerQbrSal, RstCouPc, RstCouKg, RstCouVl, RstSalKg, RstSalVl, RstTotVl,
  TribDefSel, PesoSalKg);
end;

procedure TUnVS_PF.InsereVSFchRMPCab(SerieFch, Ficha, MovimID,
  Terceiro: Integer);
begin
  VS_CRC_PF.InsereVSFchRMPCab(SerieFch, Ficha, MovimID, Terceiro);
end;

procedure TUnVS_PF.EncerraRendimentoInn(Modo: TdmkModoExec; MovimCod, Serie,
  Ficha: Integer; PB: TProgressBar; LaAviso1, LaAviso2: TLabel; GraGruYIni,
  GraGruYFim, GraGruYPos: Integer);
begin
  VS_CRC_PF.EncerraRendimentoInn(Modo, MovimCod, Serie, Ficha, PB, LaAviso1,
  LaAviso2, GraGruYIni, GraGruYFim, GraGruYPos);
end;

procedure TUnVS_PF.ReopenVSItsBxa(Qry: TmySQLQuery; SrcMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2, TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; LocCtrl: Integer);
begin
  VS_CRC_PF.ReopenVSItsBxa(Qry, SrcMovID, SrcNivel1, SrcNivel2, TemIMEIMrt,
  MovimNiv, LocCtrl);
end;

procedure TUnVS_PF.AtualizaDescendentes(IMEI: Integer; Campos: array of String;
  Valores: array of Variant);
begin
  VS_CRC_PF.AtualizaDescendentes(IMEI, Campos, Valores);
end;

procedure TUnVS_PF.ReopenVSIts_Controle_If(Qry: TmySQLQuery; Controle,
  TemIMEIMrt: Integer);
begin
  VS_CRC_PF.ReopenVSIts_Controle_If(Qry, Controle, TemIMEIMrt);
end;

procedure TUnVS_PF.MostraFormVSPlCCab(Codigo, Controle: Integer);
begin
  VS_CRC_PF.MostraFormVSPlCCab(Codigo, Controle);
end;

procedure TUnVS_PF.MostraFormVSOpeCab(Codigo: Integer);
begin
  VS_CRC_PF.MostraFormVSOpeCab(Codigo);
end;

procedure TUnVS_PF.AtualizaVSPedIts_Fin(VSPedIts: Integer);
begin
  VS_CRC_PF.AtualizaVSPedIts_Fin(VSPedIts);
end;

procedure TUnVS_PF.EncerraPalletSimples(Pallet, Empresa, ClientMO: Integer;
  QrVSPallet: TmySQLQuery; PallOnEdit: array of Integer);
begin
  VS_CRC_PF.EncerraPalletSimples(Pallet, Empresa, ClientMO, QrVSPallet, PallOnEdit);
end;

function TUnVS_PF.ObtemFatorInteiro(const GraGruX: Integer;
  var FatorIntRes: Integer; const Avisa: Boolean; const
  FatorIntCompara: Integer; MeAviso: TMemo): Boolean;
begin
  Result := VS_CRC_PF.ObtemFatorInteiro(GraGruX, FatorIntRes, Avisa,
  FatorIntCompara, MeAviso);
end;

function TUnVS_PF.GeraNovoPallet(Empresa, ClientMO, GraGruX: Integer;
  EdGraGruX, EdPallet: TdmkEdit; CBPallet: TdmkDBLookupComboBox;
  SBNewPallet: TSpeedButton; QrVSPallet: TmySQLQuery;
  PallOnEdit: array of Integer): Integer;
begin
  Result := VS_CRC_PF.GeraNovoPallet(Empresa, ClientMO, GraGruX, EdGraGruX,
  EdPallet, CBPallet, SBNewPallet, QrVSPallet, PallOnEdit);
end;

procedure TUnVS_PF.ReopenPedItsXXX(QrVSPedIts: TmySQLQuery; InsPedIts,
UpdPedIts: Integer);
begin
  VS_CRC_PF.ReopenPedItsXXX(QrVSPedIts, InsPedIts, UpdPedIts);
end;

procedure TUnVS_PF.AtualizaFornecedorOpe(MovimCod: Integer);
begin
  VS_CRC_PF.AtualizaFornecedorOpe(MovimCod);
end;

procedure TUnVS_PF.ImprimeOXsAbertas(RG18Ordem1_ItemIndex,
RG18Ordem2_ItemIndex, RG18Ordem3_ItemIndex, RG18Agrupa_ItemIndex: Integer;
MovimIDs: array of TEstqMovimID(*MovimID: TEstqMovimID*); Ck18Cor_Checked,
Ck18Especificos_Checked: Boolean; Ed18Especificos_Text: String;
Ed18Fornecedor_ValueVariant: Integer);
begin
  VS_CRC_PF.ImprimeOXsAbertas(RG18Ordem1_ItemIndex, RG18Ordem2_ItemIndex,
  RG18Ordem3_ItemIndex, RG18Agrupa_ItemIndex, MovimIDs, Ck18Cor_Checked,
  Ck18Especificos_Checked, Ed18Especificos_Text, Ed18Fornecedor_ValueVariant);
end;

procedure TUnVS_PF.ImprimeOrdem(MovimID: TEstqMovimID; Empresa, Codigo,
  MovimCod: Integer);
begin
   VS_CRC_PF.ImprimeOrdem(MovimID, Empresa, Codigo, MovimCod);
end;

function TUnVS_PF.VerificaOpeSemOperacao(): Boolean;
begin
  Result := VS_CRC_PF.VerificaOpeSemOperacao();
end;

procedure TUnVS_PF.CorrigeArtigoOpeEmDiante(IMEI, GGXAtu: Integer);
begin
  VS_CRC_PF.CorrigeArtigoOpeEmDiante(IMEI, GGXAtu);
end;

procedure TUnVS_PF.DefinirAscendenciasDeIMEIsDeMovimCod(IMEIAtu, MovimCod:
  Integer; MovimNivSrc, MovimNivDst: TEstqMovimNiv);
begin
  VS_CRC_PF.DefinirAscendenciasDeIMEIsDeMovimCod(IMEIAtu, MovimCod, MovimNivSrc,
  MovimNivDst);
end;

procedure TUnVS_PF.InfoReqMovEstq(VMI, ReqMovEstq: Integer; Qry: TmySQLQuery);
begin
  VS_CRC_PF.InfoReqMovEstq(VMI, ReqMovEstq, Qry);
end;

function TUnVS_PF.InformaIxx(DBGIMEI: TDBGrid; QrIMEI: TmySQLQuery; FldIMEI:
  String): Boolean;
begin
  Result := VS_CRC_PF.InformaIxx(DBGIMEI, QrIMEI, FldIMEI);
end;

function TUnVS_PF.ExcluiCabEIMEI_OpeCab(MovimCod, IMEI: Integer;
  MovimID: TEstqMovimID; MotivDel: TEstqMotivDel): Boolean;
begin
  Result := VS_CRC_PF.ExcluiCabEIMEI_OpeCab(MovimCod, IMEI, MovimID, MotivDel);
end;

function TUnVS_PF.ObtemControleMovimTwin(MovimCod, MovimTwn: Integer; MovimID:
TEstqMovimID; MovimNiv: TEstqMovimNiv): Integer;
begin
  Result := VS_CRC_PF.ObtemControleMovimTwin(MovimCod, MovimTwn, MovimID,
  MovimNiv);
end;

procedure TUnVS_PF.InfoStqCenLoc(VMI, StqCenLoc: Integer; Qry: TmySQLQuery);
begin
  VS_CRC_PF.InfoStqCenLoc(VMI, StqCenLoc, Qry);
end;

procedure TUnVS_PF.InfoPalletVMI(VMI, Pallet: Integer; Qry: TmySQLQuery);
begin
  VS_CRC_PF.InfoPalletVMI(VMI, Pallet, Qry);
end;

procedure TUnVS_PF.ReopenVSCOPCab(Query: TmySQLQuery; MovimID: TEstqMovimID);
begin
  VS_CRC_PF.ReopenVSCOPCab(Query, MovimID);
end;

procedure TUnVS_PF.PreencheDadosDeVSCOPCab(VSCOPCab: Integer;
  EdEmpresa: TdmkEditCB; CBEmpresa: TDmkDBLookupComboBox;
  RGTipoArea: TRadioGroup; EdGraGruX: TdmkEditCB;
  CBGraGruX: TDmkDBLookupComboBox; EdGGXDst: TdmkEditCB;
  CBGGXDst: TDmkDBLookupComboBox; EdForneceMO: TdmkEditCB;
  CBForneceMO: TDmkDBLookupComboBox; EdStqCenLoc: TdmkEditCB;
  CBStqCenLoc: TDmkDBLookupComboBox; EdOperacoes: TdmkEditCB;
  CBOperacoes: TDmkDBLookupComboBox; EdCliente: TdmkEditCB;
  CBCliente: TDmkDBLookupComboBox; EdPedItsLib: TdmkEditCB;
  CBPedItsLib: TDmkDBLookupComboBox; EdClienteMO: TdmkEditCB;
  CBClienteMO: TDmkDBLookupComboBox;
  EdCustoMO, EdVSArtCab: TdmkEdit; CBVSArtCab: TDmkDBLookupComboBox;
  EdLinCulReb, EdLinCabReb, EdLinCulSem, EdLinCabSem: TdmkEdit;
  EdReceiRecu, EdReceiRefu: TDmkEditCB; CBReceiRecu, CBReceiRefu:
  TDmkDBLookupComboBox);
  //
begin
  VS_CRC_PF.PreencheDadosDeVSCOPCab(VSCOPCab, EdEmpresa, CBEmpresa,
  RGTipoArea, EdGraGruX, CBGraGruX, EdGGXDst, CBGGXDst,  EdForneceMO,
  CBForneceMO,  EdStqCenLoc, CBStqCenLoc,  EdOperacoes, CBOperacoes, EdCliente,
  CBCliente, EdPedItsLib, CBPedItsLib, EdClienteMO, CBClienteMO, EdCustoMO,
  EdVSArtCab, CBVSArtCab, EdLinCulReb, EdLinCabReb, EdLinCulSem, EdLinCabSem,
  EdReceiRecu, EdReceiRefu, CBReceiRecu, CBReceiRefu);
end;

function TUnVS_PF.ObtemMovimNivInnDeMovimID(
  MovimID: TEstqMovimID): TEstqMovimNiv;
begin
  Result := VS_CRC_PF.ObtemMovimNivInnDeMovimID(MovimID);
end;

function TUnVS_PF.ObtemMovimNivDstDeMovimID(
  MovimID: TEstqMovimID): TEstqMovimNiv;
begin
  Result := VS_CRC_PF.ObtemMovimNivDstDeMovimID(MovimID);
end;

function TUnVS_PF.ObtemMovimNivSrcDeMovimID(
  MovimID: TEstqMovimID): TEstqMovimNiv;
begin
  Result := VS_CRC_PF.ObtemMovimNivSrcDeMovimID(MovimID);
end;

procedure TUnVS_PF.InfoRegIntInVMI(Campo: String; VMI, Inteiro: Integer;
Qry: TmySQLQuery);
begin
  VS_CRC_PF.InfoRegIntInVMI(Campo, VMI, Inteiro, Qry);
end;

function TUnVS_PF.AlteraVMI_StqCenLoc(Controle, Atual: Integer): Boolean;
begin
  Result := VS_CRC_PF.AlteraVMI_StqCenLoc(Controle, Atual);
end;

procedure TUnVS_PF.MostraFormVSGruGGX(SQLType: TSQLType; Codigo, Bastidao,
  GGXAtu: Integer);
begin
  VS_CRC_PF.MostraFormVSGruGGX(SQLType, Codigo, Bastidao, GGXAtu);
end;

function TUnVS_PF.OperacaoEhDivisao(MovimID, MovimCod: Integer): Boolean;
begin
  Result := VS_CRC_PF.OperacaoEhDivisao(MovimID, MovimCod);
end;

procedure TUnVS_PF.MostraFormVSPWECab(Codigo: Integer);
begin
  VS_CRC_PF.MostraFormVSPWECab(Codigo);
end;

procedure TUnVS_PF.AtualizaNF_IMEI(MovimCod: Integer; IMEI: Integer);
begin
  VS_CRC_PF.AtualizaNF_IMEI(MovimCod, IMEI);
end;

function TUnVS_PF.ObtemSaldoFuturoIMEI(const VMIOri: Integer; const VoltaPecas,
  VoltaPesoKg, VoltaAreaM2: Double; var FuturoPecas, FuturoPesoKg,
  FuturoAreaM2, FuturoValorT: Double): Boolean;
begin
  Result := VS_CRC_PF.ObtemSaldoFuturoIMEI(VMIOri, VoltaPecas, VoltaPesoKg,
  VoltaAreaM2, FuturoPecas, FuturoPesoKg, FuturoAreaM2, FuturoValorT);
end;

procedure TUnVS_PF.AtualizaFornecedorPWE(MovimCod: Integer);
begin
  VS_CRC_PF.AtualizaFornecedorPWE(MovimCod);
end;

function TUnVS_PF.AlteraVMI_AreaM2(MovimID, MovimNiv, Controle: Integer;
  Default: Double; PermiteNegativo: TSinal): Boolean;
begin
  Result := VS_CRC_PF.AlteraVMI_AreaM2(MovimID, MovimNiv, Controle, Default,
  PermiteNegativo);
end;

function TUnVS_PF.AlteraVMI_CliVenda(Controle, Atual: Integer): Boolean;
begin
  Result := VS_CRC_PF.AlteraVMI_CliVenda(Controle, Atual);
end;

procedure TUnVS_PF.ReopenVSXxxExtra(Qry: TmySQLQuery; Codigo, //Controle,
  TemIMEIMrt: Integer; MovimID, DstMovID: TEstqMovimID; MovimNiv: TEstqMovimNiv);
begin
  VS_CRC_PF.ReopenVSXxxExtra(Qry, Codigo, TemIMEIMrt, MovimID, DstMovID, MovimNiv);
end;

procedure TUnVS_PF.ReopenVSXxxOris(QrVSXxxCab, QrVSXxxOriIMEI, QrVSXxxOriPallet:
  TmySQLQuery; Controle, Pallet: Integer; MovimNiv: TEstqMovimNiv);
begin
  VS_CRC_PF.ReopenVSXxxOris(QrVSXxxCab, QrVSXxxOriIMEI, QrVSXxxOriPallet,
  Controle, Pallet, MovimNiv);
end;

function TUnVS_PF.AlteraVMI_FornecMO(Controle, Atual: Integer): Boolean;
begin
  Result := VS_CRC_PF.AlteraVMI_FornecMO(Controle, Atual);
end;

procedure TUnVS_PF.MostraFormVSCOPCab(Codigo: Integer);
begin
  VS_CRC_PF.MostraFormVSCOPCab(Codigo);
end;

procedure TUnVS_PF.MostraFormVSTrfLocCab(Codigo: Integer);
begin
  VS_CRC_PF.MostraFormVSTrfLocCab(Codigo);
end;


end.
