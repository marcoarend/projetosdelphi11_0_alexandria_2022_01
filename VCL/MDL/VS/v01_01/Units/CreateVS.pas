unit CreateVS;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, UnMLAGeral,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables,(* DbTables,*) dmkGeral;

type
  TNomeTabRecriaTempTable = (
    ntrttVSMovImp1, ntrttVSMovImp2, ntrttVSMovImp3, ntrttVSMovImp4,
    (*ntrttVSMovImp5,*) ntrttVSImpIMEI, ntrttVSImpRoma,
    ntrttVSCacIts, ntrttVSMovIts, ntrttVSCacGBY, ntrttVSLstPalBox,
    ntrttVSLstPalGhost, ntrttVSVMIClaSdo, ntrttVSBlendVS,
    ntrttVSVerifIMEI, ntrttVSSeqIts, ntrttVSFluxIncon, ntrttVSPsqSeqCac,
    ntrttVsVmiPsq1, ntrttVsVmiPsq2, ntrttVmiEstqEmVmi, ntrttVmiEstqEmVmiPosit,
    ntrttVSImpRendIMEI, ntrttVSComparaCacIts, ntrttVSMO, ntrttVSCustoFicha,
    ntrttVSUsoPQ_VMI, ntrttVSEstqPQ,
    ntrttVMIQtdEnvEVMI, ntrttVMIQtdEnvGVMI, ntrttVMIQtdEnvRVMI,
    ntrttVSMOEnvCTeGer, ntrttVSMOEnvNFeGer, ntrttVSInCabEIts, ntrttVSGACabEIts,
    ntrttVSGACabEIt2, ntrttVSGerArtFromInDd, ntrttVSGerArtFromInD2,
    ntrttVSKardex2, ntrttVSKardex3, ntrttVSTabMPAG, ntrttVSMarcasDesc);
  TAcaoCreate = (acDrop, acCreate, acFind);
  TCreateVS = class(TObject)

  private
    { Private declarations }
    procedure Cria_ntrttVSMovImp1(Qry: TmySQLQuery);
    procedure Cria_ntrttVSMovImp2(Qry: TmySQLQuery);
    procedure Cria_ntrttVSMovImp3(Qry: TmySQLQuery);
    procedure Cria_ntrttVSMovImp4(Qry: TmySQLQuery);
    procedure Cria_ntrttVSImpIMEI(Qry: TmySQLQuery);
    procedure Cria_ntrttVSImpRoma(Qry: TmySQLQuery);
    procedure Cria_ntrttVSCacIts(Qry: TmySQLQuery);
    procedure Cria_ntrttVSMovIts(Qry: TmySQLQuery);
    procedure Cria_ntrttVSCacGBY(Qry: TmySQLQuery);
    procedure Cria_ntrttVSLstPalBox(Qry: TmySQLQuery);
    procedure Cria_ntrttVSLstPalGhost(Qry: TmySQLQuery);
    procedure Cria_ntrttVSVMIClaSdo(Qry: TmySQLQuery);
    procedure Cria_ntrttVSBlendVS(Qry: TmySQLQuery);
    procedure Cria_ntrttVSVerifIMEI(Qry: TmySQLQuery);
    procedure Cria_ntrttVSSeqIts(Qry: TmySQLQuery);
    procedure Cria_ntrttVSFluxIncon(Qry: TmySQLQuery);
    procedure Cria_ntrttVSPsqSeqCac(Qry: TmySQLQuery);
    procedure Cria_ntrttVsVmiPsq1(Qry: TmySQLQuery);
    procedure Cria_ntrttVsVmiPsq2(Qry: TmySQLQuery);
    procedure Cria_ntrttVmiEstqEmVmi(Qry: TmySQLQuery);
    procedure Cria_ntrttVmiEstqEmVmiPosit(Qry: TmySQLQuery);
    procedure Cria_ntrttVSImpRendIMEI(Qry: TmySQLQuery);
    procedure Cria_ntrttVSComparaCacIts(Qry: TmySQLQuery);
    procedure Cria_ntrttVSMO(Qry: TmySQLQuery);
    procedure Cria_ntrttVSCustoFicha(Qry: TmySQLQuery);
    procedure Cria_ntrttVSUsoPQ_VMI(Qry: TmySQLQuery);
    procedure Cria_ntrttVSEstqPQ(Qry: TmySQLQuery);
    procedure Cria_ntrttVMIQtdEnvEVMI(Qry: TmySQLQuery);
    procedure Cria_ntrttVMIQtdEnvGVMI(Qry: TmySQLQuery);
    procedure Cria_ntrttVMIQtdEnvRVMI(Qry: TmySQLQuery);
    procedure Cria_ntrttVSMOEnvCTeGer(Qry: TmySQLQuery);
    procedure Cria_ntrttVSMOEnvNFeGer(Qry: TmySQLQuery);
    procedure Cria_ntrttVSInCabEIts(Qry: TmySQLQuery);
    procedure Cria_ntrttVSGACabEIts(Qry: TmySQLQuery);
    procedure Cria_ntrttVSGACabEIt2(Qry: TmySQLQuery);
    procedure Cria_ntrttVSGerArtFromInDd(Qry: TmySQLQuery; Repeticoes: Integer);
    procedure Cria_ntrttVSGerArtFromInD2(Qry: TmySQLQuery; Repeticoes: Integer);
    procedure Cria_ntrttVSKardex2(Qry: TmySQLQuery);
    procedure Cria_ntrttVSKardex3(Qry: TmySQLQuery);
    procedure Cria_ntrttVSKardex4(Qry: TmySQLQuery);
    procedure Cria_ntrttVSTabMPAG(Qry: TmySQLQuery);
    procedure Cria_ntrttVSMarcasDesc(Qry: TmySQLQuery);

    //
  public
    { Public declarations }
    function RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
  end;

var
  UnCreateVS: TCreateVS;

implementation

uses UnMyObjects, Module;

procedure TCreateVS.Cria_ntrttVSVerifIMEI(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DataEmis                       datetime                                   ,');
  Qry.SQL.Add('  IMEI                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Status                         int(11)                                    ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSMarcasDesc(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nivel                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSMO(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFCMO_FatID                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFCMO_FatNum                   int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFCMO_Empresa                  int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFCMO_nItem                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFCMO_SerNF                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFCMO_nNF                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFCMO_Pecas                    double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  NFCMO_PesoKg                   double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  NFCMO_AreaM2                   double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  NFCMO_AreaP2                   double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  NFCMO_CusMOM2                  double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  NFCMO_CusMOKG                  double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  NFCMO_ValorT                   double(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  NFRMP_FatID                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFRMP_FatNum                   int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFRMP_Empresa                  int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFRMP_nItem                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFRMP_SerNF                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFRMP_nNF                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFRMP_Pecas                    double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  NFRMP_PesoKg                   double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  NFRMP_AreaM2                   double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  NFRMP_AreaP2                   double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  NFRMP_ValorT                   double(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  VSVMI_Controle                 int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSVMI_Codigo                   int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSVMI_MovimID                  int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSVMI_MovimNiv                 int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSVMI_MovimCod                 int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSVMI_Empresa                  int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSVMI_SerNF                    int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  VSVMI_nNF                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSVMI_Pecas                    double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  VSVMI_PesoKg                   double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  VSVMI_AreaM2                   double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  VSVMI_AreaM2WB                 double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  VSVMI_PesoKgWB                 double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  VSVMI_PercRendim               double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  Grandeza                       tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSVMI_M2Virtual                double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  GraGru1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(255)                               ,');
  Qry.SQL.Add('  SerieNF_VMI                    varchar(255)                               ,');
  Qry.SQL.Add('  SerieNF_CMO                    varchar(255)                               ,');
  Qry.SQL.Add('  SerieNF_RMP                    varchar(255)                               ,');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
//  Qry.SQL.Add('  PRIMARY KEY (Codigo)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSMOEnvCTeGer(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  DataCad                        date                                       ,');
  Qry.SQL.Add('  TipoFrete                      varchar(10)  NOT NULL                      ,');
  Qry.SQL.Add('  Tabela                         varchar(15)  NOT NULL                      ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SerCT                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  nCT                            int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PesoKg                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AreaP2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  PesTrKg                        double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  CusTrKg                        double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  ValorT                         double(15,4) NOT NULL  DEFAULT "0.00"      ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSMOEnvNFeGer(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  DataHora                       datetime                                   ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSVMI_MovimCod                 int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  TipoFrete                      varchar(10)  NOT NULL                      ,');
  Qry.SQL.Add('  Tabela                         varchar(15)  NOT NULL                      ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SerNF                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  nNF                            int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PesoKg                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AreaP2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorT                         double(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SdoPeca                        double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoPeso                        double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoArM2                        double(15,2) NOT NULL  DEFAULT "0.00"      ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSMovImp1(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  PesoKg                         double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  AreaM2                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  AreaP2                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ValorT                         double(19,4) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  SdoVrtPeca                     double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  SdoVrtPeso                     double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  SdoVrtArM2                     double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  LmbVrtPeca                     double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  LmbVrtPeso                     double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  LmbVrtArM2                     double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  GraGru1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(255)                               ,');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PALLET                      varchar(60)                                ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CliStat                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Status                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(255)                               ,');
  Qry.SQL.Add('  NO_CLISTAT                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_EMPRESA                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_STATUS                      varchar(20)                                ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  OrdGGX                         int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  OrdGGY                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruY                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_GGY                         varchar(255)                               ,');
  //
  Qry.SQL.Add('  PalStat                        int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  CouNiv2                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CouNiv1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_CouNiv2                     varchar(60)                                ,');
  Qry.SQL.Add('  NO_CouNiv1                     varchar(60)                                ,');
  Qry.SQL.Add('  FatorInt                       double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  SdoInteiros                    double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  LmbInteiros                    double(20,3) NOT NULL DEFAULT "0.000"      ,');
  //
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IMEC                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IMEI                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_MovimID                     varchar(30)                                ,');
  Qry.SQL.Add('  NO_MovimNiv                    varchar(40)                                ,');
  //
  Qry.SQL.Add('  SerieFch                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_SerieFch                    varchar(60)                                ,');
  Qry.SQL.Add('  Ficha                          int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  //
  Qry.SQL.Add('  StatPall                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_StatPall                    varchar(60)                                ,');
  //
  Qry.SQL.Add('  ReqMovEstq                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  StqCenCad                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_StqCenCad                   varchar(50)                                ,');
  Qry.SQL.Add('  StqCenLoc                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_LOC_CEN                     varchar(120)                               ,');
  Qry.SQL.Add('  Historico                      varchar(255)                               ,');
  Qry.SQL.Add('  VSMulFrnCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MulFornece                     int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  ClientMO                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ID_UNQ                         varchar(60)                                ,');
  Qry.SQL.Add('  Grandeza                       tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  NFeSer                         tinyint(3)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulNFeCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  FornecMO                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_ClientMO                    varchar(100)                               ,');
  Qry.SQL.Add('  NO_FornecMO                    varchar(100)                               ,');
  //
  Qry.SQL.Add('  BaseValVenda                   double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  BaseCliente                    varchar(60)                                ,');
  Qry.SQL.Add('  BaseImpostos                   double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  BasePerComis                   double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  BasFrteVendM2                  double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  BaseValLiq                     double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  BaseValCusto                   double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSMovImp2(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  OrdGrupSeq                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PesoKg                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AreaP2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SrcMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtPeca                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtArM2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Observ                         varchar(255) NOT NULL                      ,');
  //
  Qry.SQL.Add('  ValorT                         double(19,4) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  GraGru1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(160)                               ,');
  Qry.SQL.Add('  NO_PALLET                      varchar(60)                                ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_MovimID                     varchar(20)                                ,');
  //
  Qry.SQL.Add('  AcumPecas                      double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcumPesoKg                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcumAreaM2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcumAreaP2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcumValorT                     double(19,4) NOT NULL DEFAULT "0.00"       ,');
  //
  //
  Qry.SQL.Add('  OrdGGY                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruY                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_GGY                         varchar(255)                               ,');
  //
  Qry.SQL.Add('  NFeSer                         tinyint(3)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulNFeCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  ;;
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSMovImp3(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  PesoKg                         double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  AreaM2                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  AreaP2                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ValorT                         double(19,4) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  SdoVrtPeca                     double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  SdoVrtPeso                     double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  SdoVrtArM2                     double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  GraGru1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(255)                               ,');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PALLET                      varchar(60)                                ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_EMPRESA                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_STATUS                      varchar(20)                                ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  OrdGGX                         int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  OrdGGY                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruY                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_GGY                         varchar(255)                               ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSMovImp4(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  PesoKg                         double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  AreaM2                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  AreaP2                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ValorT                         double(19,4) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  SdoVrtPeca                     double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  SdoVrtPeso                     double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  SdoVrtArM2                     double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  LmbVrtPeca                     double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  LmbVrtPeso                     double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  LmbVrtArM2                     double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  GraGru1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(255)                               ,');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PALLET                      varchar(60)                                ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CliStat                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Status                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_CLISTAT                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_EMPRESA                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_STATUS                      varchar(20)                                ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  OrdGGX                         int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  OrdGGY                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruY                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_GGY                         varchar(255)                               ,');
  //
  Qry.SQL.Add('  PalStat                        int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  CouNiv2                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CouNiv1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_CouNiv2                     varchar(60)                                ,');
  Qry.SQL.Add('  NO_CouNiv1                     varchar(60)                                ,');
  Qry.SQL.Add('  FatorInt                       double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  SdoInteiros                    double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  LmbInteiros                    double(20,3) NOT NULL DEFAULT "0.000"      ,');
  //
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IMEC                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IMEI                           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_MovimID                     varchar(30)                                ,');
  Qry.SQL.Add('  NO_MovimNiv                    varchar(40)                                ,');
  //
  Qry.SQL.Add('  SerieFch                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_SerieFch                    varchar(60)                                ,');
  Qry.SQL.Add('  Ficha                          int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  StatPall                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_StatPall                    varchar(60)                                ,');
  //
  Qry.SQL.Add('  NFeSer                         tinyint(3)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulNFeCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  StqCenCad                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_StqCenCad                   varchar(50)                                ,');
  Qry.SQL.Add('  StqCenLoc                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_StqCenLoc                   varchar(50)                                ,');
  //
(* Quando mudar aqui, alterar....:
  - TUnVS_PF.PesquisaPallets(
*)

  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSMovIts(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimTwn                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliVenda                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  LnkNivXtr1                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  LnkNivXtr2                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PesoKg                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AreaP2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorT                         double(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SrcMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcGGX                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtPeca                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtPeso                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtArM2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Observ                         varchar(255) NOT NULL                      ,');
  Qry.SQL.Add('  SerieFch                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ficha                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Misturou                       tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  FornecMO                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOKg                      double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  CustoMOTot                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorMP                        double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  DstMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstGGX                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  QtdGerPeca                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerPeso                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerArM2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdGerArP2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntPeca                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntPeso                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntArM2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntArP2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AptoUso                        tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  NotaMPAG                       double(15,8) NOT NULL  DEFAULT "0.00000000",');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
(*
  Qry.SQL.Add('  Lk                             int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  DataCad                        date                                       ,');
  Qry.SQL.Add('  DataAlt                        date                                       ,');
  Qry.SQL.Add('  UserCad                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  UserAlt                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"         ,');
*)
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSPsqSeqCac(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  VSPallet                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Itens                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Posicao                        int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (VSPallet, Posicao)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSSeqIts(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimTwn                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliVenda                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  LnkNivXtr1                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  LnkNivXtr2                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  Data                           date         NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PesoKg                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AreaP2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorT                         double(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SrcMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcGGX                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtPeca                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtPeso                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtArM2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Observ                         varchar(255)                               ,');
  Qry.SQL.Add('  SerieFch                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ficha                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Misturou                       tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  FornecMO                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOKg                      double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  CustoMOTot                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorMP                        double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  DstMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstGGX                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  QtdGerPeca                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerPeso                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerArM2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdGerArP2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntPeca                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntPeso                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntArM2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntArP2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AptoUso                        tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  NotaMPAG                       double(15,8) NOT NULL  DEFAULT "0.00000000",');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  Qry.SQL.Add('  TpCalcAuto                     int(11)      NOT NULL  DEFAULT "-1"        ,');
  //
  Qry.SQL.Add('  Inteiros                       double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcumInteir                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Sequencia                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruY                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IMEI_Src                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Operacao                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IMEI_Ord                       int(11)      NOT NULL  DEFAULT "0"         ,');
(*
  Qry.SQL.Add('  Lk                             int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  DataCad                        date                                       ,');
  Qry.SQL.Add('  DataAlt                        date                                       ,');
  Qry.SQL.Add('  UserCad                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  UserAlt                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"         ,');
*)
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle, GraGruY)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

(*
procedure TCreateVS.Cria_ntrttVSTabMPAG(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  PesoKg_A                     double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2_A                     double(15,2)           DEFAULT "0.00"     ,');
  Qry.SQL.Add('  AreaP2_A                     double(15,2)           DEFAULT "0.00"     ,');
  Qry.SQL.Add('  KgM2_A                       double(15,3)           DEFAULT "0.000"     ,');
  //
  Qry.SQL.Add('  PesoKg_B                     double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2_B                     double(15,2)           DEFAULT "0.00"     ,');
  Qry.SQL.Add('  AreaP2_B                     double(15,2)           DEFAULT "0.00"     ,');
  Qry.SQL.Add('  KgM2_B                       double(15,3)           DEFAULT "0.000"     ,');
  //
  Qry.SQL.Add('  PesoKg_C                     double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2_C                     double(15,2)           DEFAULT "0.00"     ,');
  Qry.SQL.Add('  AreaP2_C                     double(15,2)           DEFAULT "0.00"     ,');
  Qry.SQL.Add('  KgM2_C                       double(15,3)           DEFAULT "0.000"     ,');
  //
  Qry.SQL.Add('  PesoKg_D                     double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2_D                     double(15,2)           DEFAULT "0.00"     ,');
  Qry.SQL.Add('  AreaP2_D                     double(15,2)           DEFAULT "0.00"     ,');
  Qry.SQL.Add('  KgM2_D                       double(15,3)           DEFAULT "0.000"     ,');
  //
  Qry.SQL.Add('  PesoKg_E                     double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2_E                     double(15,2)           DEFAULT "0.00"     ,');
  Qry.SQL.Add('  AreaP2_E                     double(15,2)           DEFAULT "0.00"     ,');
  Qry.SQL.Add('  KgM2_E                       double(15,3)           DEFAULT "0.000"     ,');
  //
end;
*)

procedure TCreateVS.Cria_ntrttVSTabMPAG(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Linha                        int(11) NOT NULL      DEFAULT "0"     ,');

  Qry.SQL.Add('  PesoKg_A                     varchar(20)           DEFAULT " "     ,');
  Qry.SQL.Add('  AreaM2_A                     varchar(20)           DEFAULT " "     ,');
  Qry.SQL.Add('  AreaP2_A                     varchar(20)           DEFAULT " "     ,');
  Qry.SQL.Add('  KgM2_A                       varchar(20)           DEFAULT " "     ,');
  //
  Qry.SQL.Add('  PesoKg_B                     varchar(20)           DEFAULT " "     ,');
  Qry.SQL.Add('  AreaM2_B                     varchar(20)           DEFAULT " "     ,');
  Qry.SQL.Add('  AreaP2_B                     varchar(20)           DEFAULT " "     ,');
  Qry.SQL.Add('  KgM2_B                       varchar(20)           DEFAULT " "     ,');
  //
  Qry.SQL.Add('  PesoKg_C                     varchar(20)           DEFAULT " "     ,');
  Qry.SQL.Add('  AreaM2_C                     varchar(20)           DEFAULT " "     ,');
  Qry.SQL.Add('  AreaP2_C                     varchar(20)           DEFAULT " "     ,');
  Qry.SQL.Add('  KgM2_C                       varchar(20)           DEFAULT " "     ,');
  //
  Qry.SQL.Add('  PesoKg_D                     varchar(20)           DEFAULT " "     ,');
  Qry.SQL.Add('  AreaM2_D                     varchar(20)           DEFAULT " "     ,');
  Qry.SQL.Add('  AreaP2_D                     varchar(20)           DEFAULT " "     ,');
  Qry.SQL.Add('  KgM2_D                       varchar(20)           DEFAULT " "     ,');
  //
  Qry.SQL.Add('  PesoKg_E                     varchar(20)           DEFAULT " "     ,');
  Qry.SQL.Add('  AreaM2_E                     varchar(20)           DEFAULT " "     ,');
  Qry.SQL.Add('  AreaP2_E                     varchar(20)           DEFAULT " "     ,');
  Qry.SQL.Add('  KgM2_E                       varchar(20)           DEFAULT " "     ,');
  //
  Qry.SQL.Add('  Ativo                        tinyint(1)  NOT NULL  DEFAULT "1"         ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSUsoPQ_VMI(Qry: TmySQLQuery);
begin
  // estoque: _vsmovimp1_
  Qry.SQL.Add('  Estq_MovID                     int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Estq_MovNiv                    int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Estq_IMEI                      int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Estq_Pecas                     double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Estq_VrtPc                     double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Estq_ClientMO                  int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Estq_FornecMO                  int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Estq_StqCenLoc                 int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Targ_ProcIMEI                  int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Targ_ProcPecas                 double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Targ_OriIMEI                   int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Targ_OriPecas                  double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Targ_Fator                     double(15,12)          DEFAULT "0.000000000000",');
  Qry.SQL.Add('  Proc_ProcIMEI                  int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Proc_ProcPecas                 double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Proc_OriIMEI                   int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Proc_OriPecas                  double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Proc_Fator                     double(15,12)          DEFAULT "0.000000000000",');
  Qry.SQL.Add('  Targ_Perc                      double(15,12)          DEFAULT "0.000000000000",');
  Qry.SQL.Add('  Gerl_Fator                     double(15,12)          DEFAULT "0.000000000000",');
  // Baixa: emit * emitcus * pqx:
  Qry.SQL.Add('  Setor                          tinyint(4)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NOMESETOR                      varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  PercTotCus                     double(15,4)           DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  VSMovIts                       int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  SerieFch                       int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Ficha                          int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  OrigemCodi                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  OrigemCtrl                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Tipo                           mediumint(4) NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliOrig                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  CliDest                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Insumo                         int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  PesoTotal                      double(24,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorTotal                     double(24,4)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  PesoLote                       double(24,5)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorLote                      double(24,4)           DEFAULT "0.0000"     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSVMIClaSdo(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  VMI_Baix                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSPaClaIts                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  BxaPecas                       double(20,3)                               ,');
  Qry.SQL.Add('  BxaAreaM2                      double(20,2)                               ,');
  Qry.SQL.Add('  BxaAreaP2                      double(20,2)                               ,');
  Qry.SQL.Add('  VMI_Pecas                      double(20,3)                               ,');
  Qry.SQL.Add('  Saldo                          double(20,3)                               ,');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVsVmiPsq1(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SerieFch                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ficha                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  //
  Qry.SQL.Add('  CUSTOM2                        double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  AreaM2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorT                         double(15,4) NOT NULL  DEFAULT "0.00"      ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVsVmiPsq2(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSCustoFicha(Qry: TmySQLQuery);
begin
  // estoque: _vsmovimp1_
  Qry.SQL.Add('  Estq_MovID                     int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Estq_MovNiv                    int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Estq_IMEI                      int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Estq_Pecas                     double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Estq_VrtPc                     double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Estq_VrtPerc                   double(24,12)          DEFAULT "0.000000000000",');
  Qry.SQL.Add('  Estq_ProcPerc                  double(24,12)          DEFAULT "0.000000000000",');
  Qry.SQL.Add('  Estq_IMEIPerc                  double(24,12)          DEFAULT "0.000000000000",');
  Qry.SQL.Add('  Targ_IMEI                      int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Targ_Pecas                     double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Targ_ProcPerc                  double(24,12)          DEFAULT "0.000000000000",');
  Qry.SQL.Add('  Proc_IMEI                      int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Proc_Pecas                     double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  Proc_IMEIPerc                  double(24,12)          DEFAULT "0.000000000000",');
  Qry.SQL.Add('  IMEI_IMEI                      int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  IMEI_Pecas                     double(15,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  IMEI_EstqPerc                  double(24,12)          DEFAULT "0.000000000000",');
  // Baixa: emit * emitcus * pqx:
  Qry.SQL.Add('  Setor                          tinyint(4)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NOMESETOR                      varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  PercTotCus                     double(15,4)           DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  VSMovIts                       int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  SerieFch                       int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Ficha                          int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  OrigemCodi                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  OrigemCtrl                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Tipo                           mediumint(4) NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliOrig                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  CliDest                        int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  Insumo                         int(11)                DEFAULT "0"         ,');
  Qry.SQL.Add('  PesoTotal                      double(24,3)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorTotal                     double(24,4)           DEFAULT "0.00"      ,');
  Qry.SQL.Add('  PesoLote                       double(24,5)           DEFAULT "0.000"     ,');
  Qry.SQL.Add('  ValorLote                      double(24,4)           DEFAULT "0.0000"     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSEstqPQ(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  IQ                             int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NOMEFO                         varchar(100)                               ,');
  Qry.SQL.Add('  NOMECI                         varchar(100)                               ,');
  Qry.SQL.Add('  Setor                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NOMESE                         varchar(20)                                ,');
  Qry.SQL.Add('  PQ                             int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NOMEPQ                         varchar(50)                                ,');
  Qry.SQL.Add('  CtrlPqCli                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CI                             int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CodProprio                     varchar(20)                                ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "-11"       ,');
  Qry.SQL.Add('  PclPeso                        double(25,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PclValor                       double(25,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  uVSPeso                        double(25,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  uVsValor                       double(25,6) NOT NULL  DEFAULT "0.000000"  ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVmiEstqEmVmi(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimTwn                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliVenda                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  LnkIDXtr                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  LnkNivXtr1                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  LnkNivXtr2                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PesoKg                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AreaP2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorT                         double(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SrcMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcGGX                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtPeca                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtPeso                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtArM2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Observ                         varchar(255) NOT NULL                      ,');
  Qry.SQL.Add('  SerieFch                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ficha                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Misturou                       tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  FornecMO                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOKg                      double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  CustoMOTot                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorMP                        double(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  DstMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstGGX                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  QtdGerPeca                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerPeso                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerArM2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdGerArP2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntPeca                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntPeso                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntArM2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntArP2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AptoUso                        tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  NotaMPAG                       double(15,8) NOT NULL  DEFAULT "0.00000000",');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  Qry.SQL.Add('  TpCalcAuto                     int(11)      NOT NULL  DEFAULT "-1"        ,');
  Qry.SQL.Add('  Zerado                         tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  EmFluxo                        tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  NotFluxo                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  FatNotaVNC                     double(15,8) NOT NULL  DEFAULT "0.00000000",');
  Qry.SQL.Add('  FatNotaVRC                     double(15,8) NOT NULL  DEFAULT "0.00000000",');
  Qry.SQL.Add('  PedItsLib                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsFin                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsVda                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GSPSrcMovID                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GSPSrcNiv2                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOM2                      double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  ReqMovEstq                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  StqCenLoc                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ItemNFe                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMorCab                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulFrnCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ClientMO                       int(11)      NOT NULL  DEFAULT "0"         ,');
//
(*
  Qry.SQL.Add('  TemEstq                        tinyint(1)                                 ,');
  Qry.SQL.Add('  FatorEm                        int(11)                                    ,');
  Qry.SQL.Add('  VMI_IMEI                       int(11)                                    ,');
*)
//
  Qry.SQL.Add('  NFeSer                         tinyint(3)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulNFeCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVmiEstqEmVmiPosit(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Empresa                        int(11)                                    ,');
  Qry.SQL.Add('  GraGruX                        int(11)                                    ,');
  Qry.SQL.Add('  Pecas                          double(20,3)                               ,');
  Qry.SQL.Add('  PesoKg                         double(20,3)                               ,');
  Qry.SQL.Add('  AreaM2                         double(19,2)                               ,');
  Qry.SQL.Add('  AreaP2                         double(19,2)                               ,');
  Qry.SQL.Add('  ValorT                         double(19,4)                               ,');
  Qry.SQL.Add('  SdoVrtPeca                     double(20,3)                               ,');
  Qry.SQL.Add('  SdoVrtPeso                     double(20,3)                               ,');
  Qry.SQL.Add('  SdoVrtArM2                     double(19,2)                               ,');
  Qry.SQL.Add('  PalVrtPeca                     double(20,3)                               ,');
  Qry.SQL.Add('  PalVrtPeso                     double(20,3)                               ,');
  Qry.SQL.Add('  PalVrtArM2                     double(19,2)                               ,');
  Qry.SQL.Add('  LmbVrtPeca                     double(20,3)                               ,');
  Qry.SQL.Add('  LmbVrtPeso                     double(20,3)                               ,');
  Qry.SQL.Add('  LmbVrtArM2                     double(19,2)                               ,');
  Qry.SQL.Add('  GraGru1                        int(11)                                    ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(255)                               ,');
  Qry.SQL.Add('  Pallet                         int(11)                                    ,');
  Qry.SQL.Add('  NO_PALLET                      varchar(60)                                ,');
  Qry.SQL.Add('  Terceiro                       int(11)                                    ,');
  Qry.SQL.Add('  CliStat                        int(11)                                    ,');
  Qry.SQL.Add('  Status                         int(11)                                    ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(255)                               ,');
  Qry.SQL.Add('  NO_CLISTAT                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_EMPRESA                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_STATUS                      varchar(20)                                ,');
  Qry.SQL.Add('  DataHora                       datetime                                   ,');
  Qry.SQL.Add('  OrdGGX                         int(11)                                    ,');
  Qry.SQL.Add('  OrdGGY                         int(11)                                    ,');
  Qry.SQL.Add('  GraGruY                        int(11)                                    ,');
  Qry.SQL.Add('  NO_GGY                         varchar(255)                               ,');
  Qry.SQL.Add('  PalStat                        int(11)                                    ,');
  Qry.SQL.Add('  NO_PalStat                     varchar(60)                                ,');
  Qry.SQL.Add('  Media                          double(24,7)                               ,');
  Qry.SQL.Add('  NO_MovimNiv                    varchar(40)                                ,');
  Qry.SQL.Add('  NO_MovimID                     varchar(30)                                ,');
  Qry.SQL.Add('  MovimNiv                       int(11)                                    ,');
  Qry.SQL.Add('  MovimID                        int(11)                                    ,');
  Qry.SQL.Add('  IMEC                           int(11)                                    ,');
  Qry.SQL.Add('  Codigo                         int(11)                                    ,');
  Qry.SQL.Add('  IMEI                           int(11)                                    ,');
  Qry.SQL.Add('  SerieFch                       int(11)                                    ,');
  Qry.SQL.Add('  NO_SerieFch                    varchar(60)                                ,');
  Qry.SQL.Add('  Ficha                          int(11)                                    ,');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  Qry.SQL.Add('  Inteiros                       double(20,3)                               ,');
  Qry.SQL.Add('  ReqMovEstq                     int(11)                                    ,');
  Qry.SQL.Add('  StqCenCad                      int(11)                                    ,');
  Qry.SQL.Add('  NO_StqCenCad                   varchar(50)                                ,');
  Qry.SQL.Add('  StqCenLoc                      int(11)                                    ,');
  Qry.SQL.Add('  NO_LOC_CEN                     varchar(120)                               ,');
  Qry.SQL.Add('  Historico                      varchar(255)                               ,');
  Qry.SQL.Add('  VSMulFrnCab                    int(11)                                    ,');
  Qry.SQL.Add('  MulFornece                     int(11)                                    ,');
  Qry.SQL.Add('  NO_MulFornece                  varchar(10)  NOT NULL                      ,');
  //
  Qry.SQL.Add('  CouNiv2                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CouNiv1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_CouNiv2                     varchar(60)                                ,');
  Qry.SQL.Add('  NO_CouNiv1                     varchar(60)                                ,');
  //
  Qry.SQL.Add('  ClientMO                       int(11)                                    ,');
  Qry.SQL.Add('  ID_UNQ                         varchar(60)                                ,');
  //
  Qry.SQL.Add('  Grandeza                       tinyint(1)                                 ,');
  Qry.SQL.Add('  GraGruValU                     double(15,7)                               ,');
  Qry.SQL.Add('  GraGruValT                     double(15,2)                               ,');
  //
  Qry.SQL.Add('  NFeSer                         tinyint(3)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulNFeCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeAgrup                       varchar(60)  NOT NULL  DEFAULT "????"      ,');
  //
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  FornecMO                       int(11)                                    ,');
  Qry.SQL.Add('  NO_ClientMO                    varchar(100)                               ,');
  Qry.SQL.Add('  NO_FornecMO                    varchar(100)                               ,');
  //
  Qry.SQL.Add('  BaseValVenda                   double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  BaseCliente                    varchar(60)                                ,');
  Qry.SQL.Add('  BaseImpostos                   double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  BasePerComis                   double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  BasFrteVendM2                  double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  BaseValLiq                     double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  BaseValCusto                   double(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  //
  Qry.SQL.Add('  Ativo                          int(4)                                     ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVMIQtdEnvEVMI(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(20,3)                               ,');
  Qry.SQL.Add('  PesoKg                         double(20,3)                               ,');
  Qry.SQL.Add('  AreaM2                         double(19,2)                               ,');
  Qry.SQL.Add('  AreaP2                         double(19,2)                               ,');
  Qry.SQL.Add('  ValorT                         double(19,4)                               ,');
  Qry.SQL.Add('  NO_PRDA_TAM_COR                varchar(255)                               ,');
  Qry.SQL.Add('  IDItem                         int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVMIQtdEnvGVMI(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(20,3)                               ,');
  Qry.SQL.Add('  PesoKg                         double(20,3)                               ,');
  Qry.SQL.Add('  AreaM2                         double(19,2)                               ,');
  Qry.SQL.Add('  AreaP2                         double(19,2)                               ,');
  Qry.SQL.Add('  ValorT                         double(19,4)                               ,');
  Qry.SQL.Add('  NO_PRDA_TAM_COR                varchar(255)                               ,');
  Qry.SQL.Add('  IDItem                         int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVMIQtdEnvRVMI(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  VSMOEnvEnv                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeSer                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(20,3)                               ,');
  Qry.SQL.Add('  PesoKg                         double(20,3)                               ,');
  Qry.SQL.Add('  AreaM2                         double(19,2)                               ,');
  Qry.SQL.Add('  AreaP2                         double(19,2)                               ,');
  Qry.SQL.Add('  ValorT                         double(19,4)                               ,');
  Qry.SQL.Add('  IDItem                         int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (VSMOEnvEnv)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSBlendVS(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SerieFch                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ficha                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(20,3)                               ,');
  Qry.SQL.Add('  AreaM2                         double(19,2)                               ,');
  //
  Qry.SQL.Add('  PercPecas                      double(15,2)                               ,');
  Qry.SQL.Add('  PercAreaM2                     double(15,2)                               ,');
  //
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(100) NOT NULL                      ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSCacGBY(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  CacCod                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CacID                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSPallet                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VMI_Sorc                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VMI_Dest                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VMI_Baix                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Box                            int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Revisor                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Digitador                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Martelo                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(20,3)                               ,');
  Qry.SQL.Add('  AreaM2                         double(19,2)                               ,');
  Qry.SQL.Add('  AreaP2                         double(19,2)                               ,');
  Qry.SQL.Add('  Tamanho                        int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  OrigArM2                       double(19,2)                               ,');
  Qry.SQL.Add('  OrigValT                       double(19,2)                               ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSCacIts(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  CacCod                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CacID                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ClaAPalOri                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RclAPalOri                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RclAPalDst                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSPaClaIts                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSPaRclIts                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSPallet                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VMI_Sorc                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VMI_Baix                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VMI_Dest                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Box                            int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AreaP2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Revisor                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Digitador                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  Sumido                         tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  //Qry.SQL.Add('  AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  Martelo                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSKardex2(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  NO_TTW                         varchar(5)   NOT NULL                      ,');
  Qry.SQL.Add('  ID_TTW                         bigint(20) unsigned NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimTwn                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ClientMO                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliVenda                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime                                   ,');
  Qry.SQL.Add('  Pallet                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PesoKg                         decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2                         decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AreaP2                         decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorT                         decimal(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SrcMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Observ                         varchar(255)                               ,');
  Qry.SQL.Add('  SerieFch                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ficha                          bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Misturou                       bigint(20) unsigned NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  FornecMO                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOKg                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  CustoMOTot                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorMP                        decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CustoPQ                        decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  DstMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  QtdGerPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdGerArP2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntArP2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  NotaMPAG                       decimal(15,8) NOT NULL  DEFAULT "0.00000000",');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  Qry.SQL.Add('  PedItsLib                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsFin                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsVda                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOM2                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  ReqMovEstq                     bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  StqCenLoc                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ItemNFe                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulFrnCab                    bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeSer                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulNFeCab                    bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GGXRcl                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DtCorrApo                      datetime                                   ,');
  Qry.SQL.Add('  IxxMovIX                       bigint(20) unsigned NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IxxFolha                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IxxLinha                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CusFrtAvuls                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CusFrtMOEnv                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CusFrtMORet                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CustoMOPc                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(157)                                ,');
  Qry.SQL.Add('  NO_MovimID                     varchar(23)                                 ,');
  //
  Qry.SQL.Add('  CustoMP                        double(20,7)                                ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(100)                                ,');
  Qry.SQL.Add('  NO_SerieFch                    varchar(60)                                 ,');
  Qry.SQL.Add('  NO_Pallet                      varchar(60)                                 ,');
  Qry.SQL.Add('  MediaArM2                      double(20,7)                                ,');
  Qry.SQL.Add('  MediaPeso                      double(19,7)                                ,');
  Qry.SQL.Add('  AcumPeca                       decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcumPeso                       decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcumArM2                       decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcumArP2                       decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcumValorT                     decimal(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcumCusPc                       decimal(15,3) NOT NULL  DEFAULT "0.000"    ,');
  Qry.SQL.Add('  AcumCusKg                       decimal(15,3) NOT NULL  DEFAULT "0.000"    ,');
  Qry.SQL.Add('  AcumCusM2                      decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  AcumCusP2                       decimal(15,4) NOT NULL  DEFAULT "0.0000"    ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSKardex3(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  NO_TTW                         varchar(5)   NOT NULL                      ,');
  Qry.SQL.Add('  ID_TTW                         bigint(20) unsigned NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimTwn                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ClientMO                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliVenda                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime                                   ,');
  Qry.SQL.Add('  Pallet                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PesoKg                         decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2                         decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AreaP2                         decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorT                         decimal(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SrcMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Observ                         varchar(255)                               ,');
  Qry.SQL.Add('  SerieFch                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ficha                          bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Misturou                       bigint(20) unsigned NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  FornecMO                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOKg                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  CustoMOTot                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorMP                        decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CustoPQ                        decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  DstMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  QtdGerPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdGerArP2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntArP2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  NotaMPAG                       decimal(15,8) NOT NULL  DEFAULT "0.00000000",');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  Qry.SQL.Add('  PedItsLib                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsFin                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsVda                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOM2                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  ReqMovEstq                     bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  StqCenLoc                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ItemNFe                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulFrnCab                    bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeSer                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulNFeCab                    bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GGXRcl                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DtCorrApo                      datetime                                   ,');
  Qry.SQL.Add('  IxxMovIX                       bigint(20) unsigned NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IxxFolha                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IxxLinha                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CusFrtAvuls                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CusFrtMOEnv                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CusFrtMORet                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CustoMOPc                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(157)                                ,');
  Qry.SQL.Add('  NO_MovimID                     varchar(23)                                 ,');
  //
  Qry.SQL.Add('  CustoMP                        double(20,7)                                ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(100)                                ,');
  Qry.SQL.Add('  NO_SerieFch                    varchar(60)                                 ,');
  Qry.SQL.Add('  NO_Pallet                      varchar(60)                                 ,');
  Qry.SQL.Add('  MediaArM2                      double(20,7)                                ,');
  Qry.SQL.Add('  MediaPeso                      double(19,7)                                ,');

  Qry.SQL.Add('  AcumPeca                       decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcumPeso                       decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcumArM2                       decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcumArP2                       decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcumValorT                     decimal(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcumCusPc                       decimal(15,3) NOT NULL  DEFAULT "0.000"    ,');
  Qry.SQL.Add('  AcumCusKg                       decimal(15,3) NOT NULL  DEFAULT "0.000"    ,');
  Qry.SQL.Add('  AcumCusM2                      decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  AcumCusP2                       decimal(15,4) NOT NULL  DEFAULT "0.0000"   ,');

  Qry.SQL.Add('  AcImeiPeca                       decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcImeiPeso                       decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcImeiArM2                       decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcImeiArP2                       decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcImeiValorT                     decimal(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcImeiCusPc                       decimal(15,3) NOT NULL  DEFAULT "0.000"    ,');
  Qry.SQL.Add('  AcImeiCusKg                       decimal(15,3) NOT NULL  DEFAULT "0.000"    ,');
  Qry.SQL.Add('  AcImeiCusM2                      decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  AcImeiCusP2                       decimal(15,4) NOT NULL  DEFAULT "0.0000"   ,');

  Qry.SQL.Add('  Ordem                          bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PalOri                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IMEIOri                        bigint(20)   NOT NULL  DEFAULT "0"         ');

  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSKardex4(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  NO_TTW                         varchar(5)   NOT NULL                      ,');
  Qry.SQL.Add('  ID_TTW                         bigint(20) unsigned NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimTwn                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ClientMO                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliVenda                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime                                   ,');
  Qry.SQL.Add('  Pallet                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PesoKg                         decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2                         decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AreaP2                         decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorT                         decimal(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SrcMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Observ                         varchar(255)                               ,');
  Qry.SQL.Add('  SerieFch                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ficha                          bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Misturou                       bigint(20) unsigned NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  FornecMO                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOKg                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  CustoMOTot                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorMP                        decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CustoPQ                        decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  DstMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  QtdGerPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdGerArP2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntArP2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  NotaMPAG                       decimal(15,8) NOT NULL  DEFAULT "0.00000000",');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  Qry.SQL.Add('  PedItsLib                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsFin                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsVda                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOM2                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  ReqMovEstq                     bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  StqCenLoc                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ItemNFe                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulFrnCab                    bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeSer                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulNFeCab                    bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GGXRcl                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DtCorrApo                      datetime                                   ,');
  Qry.SQL.Add('  IxxMovIX                       bigint(20) unsigned NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IxxFolha                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IxxLinha                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CusFrtAvuls                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CusFrtMOEnv                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CusFrtMORet                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CustoMOPc                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(157)                                ,');
  Qry.SQL.Add('  NO_MovimID                     varchar(23)                                 ,');
  //
  Qry.SQL.Add('  CustoMP                        double(20,7)                                ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(100)                                ,');
  Qry.SQL.Add('  NO_SerieFch                    varchar(60)                                 ,');
  Qry.SQL.Add('  NO_Pallet                      varchar(60)                                 ,');
  Qry.SQL.Add('  MediaArM2                      double(20,7)                                ,');
  Qry.SQL.Add('  MediaPeso                      double(19,7)                                ,');

  Qry.SQL.Add('  AcumPeca                       decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcumPeso                       decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcumArM2                       decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcumArP2                       decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcumValorT                     decimal(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcumCusPc                      decimal(15,3) NOT NULL  DEFAULT "0.000"    ,');
  Qry.SQL.Add('  AcumCusKg                      decimal(15,3) NOT NULL  DEFAULT "0.000"    ,');
  Qry.SQL.Add('  AcumCusM2                      decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  AcumCusP2                      decimal(15,4) NOT NULL  DEFAULT "0.0000"   ,');

  Qry.SQL.Add('  AcImeiPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcImeiPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AcImeiArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcImeiArP2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcImeiValorT                   decimal(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AcImeiCusPc                    decimal(15,3) NOT NULL  DEFAULT "0.000"    ,');
  Qry.SQL.Add('  AcImeiCusKg                    decimal(15,3) NOT NULL  DEFAULT "0.000"    ,');
  Qry.SQL.Add('  AcImeiCusM2                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  AcImeiCusP2                    decimal(15,4) NOT NULL  DEFAULT "0.0000"   ,');

  Qry.SQL.Add('  Ordem                          bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PalOri                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IMEIOri                        bigint(20)   NOT NULL  DEFAULT "0"         ');

  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSComparaCacIts(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  AreaM2                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PalletA                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PalletB                        int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (AreaM2)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSFluxIncon(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  GraGruY                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Inteiros                       double(20,3)                               ,');
  Qry.SQL.Add('  LastDtHr                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  //
  Qry.SQL.Add('  SerieFch                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ficha                          int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  IMEI_Src                       int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Operacao                       int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Int_Pos                        double(20,3)                               ,');
  Qry.SQL.Add('  Int_Neg                        double(20,3)                               ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSGACabEIt2(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  NO_TTW                         varchar(5)   NOT NULL                      ,');
  Qry.SQL.Add('  ID_TTW                         tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimTwn                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ClientMO                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliVenda                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime                                   ,');
  Qry.SQL.Add('  Pallet                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PesoKg                         decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2                         decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AreaP2                         decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorT                         decimal(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SrcMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Observ                         varchar(255)                               ,');
  Qry.SQL.Add('  SerieFch                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ficha                          bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Misturou                       bigint(20) unsigned NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  FornecMO                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOKg                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  CustoMOTot                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorMP                        decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CustoPQ                        decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  DstMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  QtdGerPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdGerArP2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntArP2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  NotaMPAG                       decimal(15,8) NOT NULL  DEFAULT "0.00000000",');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  Qry.SQL.Add('  PedItsLib                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsFin                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsVda                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOM2                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  ReqMovEstq                     bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  StqCenLoc                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ItemNFe                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulFrnCab                    bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeSer                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulNFeCab                    bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GGXRcl                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DtCorrApo                      datetime                                   ,');
  Qry.SQL.Add('  IxxMovIX                       bigint(20) unsigned NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IxxFolha                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IxxLinha                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CusFrtAvuls                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CusFrtMOEnv                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CusFrtMORet                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CustoMOPc                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(157)                               ,');
  Qry.SQL.Add('  Mediam2Peca                    double(20,7)                               ,');
  Qry.SQL.Add('  Mediam2Peca_TXT                varchar(59)                                ,');
  Qry.SQL.Add('  RendKgm2                       double(19,7)                               ,');
  Qry.SQL.Add('  RendKgm2_TXT                   varchar(58)                                ,');
  Qry.SQL.Add('  NotaMPAG_TXT                   varchar(53)                                ,');
  Qry.SQL.Add('  Misturou_TXT                   varchar(3)   NOT NULL                      ,');
  Qry.SQL.Add('  SIGLAUNIDMED                   varchar(6)                                 ,');
  Qry.SQL.Add('  NOMEUNIDMED                    varchar(30)                                ,');
  Qry.SQL.Add('  KgMedioCouro                   double(19,7)                               ,');
  Qry.SQL.Add('  NO_SerieFch                    varchar(60)                                ,');
  Qry.SQL.Add('  ArtGeComodty                   int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ArtGeComOrd                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ArtGeComSgl                    varchar(10)  NOT NULL DEFAULT "??"         ,');
  //
  Qry.SQL.Add('  Fuloes                         varchar(20)                                ,');
  // No fim
  Qry.SQL.Add('  DtEntrada_TXT                  varchar(10) NOT NULL DEFAULT "00/00/0000"  ,');
  Qry.SQL.Add('  AnoMesDiaIn                    int(11)      NOT NULL  DEFAULT "0"         ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSGACabEIts(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  NO_TTW                         varchar(5)   NOT NULL                      ,');
  Qry.SQL.Add('  ID_TTW                         tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimTwn                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ClientMO                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliVenda                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime                                   ,');
  Qry.SQL.Add('  Pallet                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PesoKg                         decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2                         decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AreaP2                         decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorT                         decimal(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SrcMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Observ                         varchar(255)                               ,');
  Qry.SQL.Add('  SerieFch                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ficha                          bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Misturou                       bigint(20) unsigned NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  FornecMO                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOKg                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  CustoMOTot                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorMP                        decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CustoPQ                        decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  DstMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  QtdGerPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdGerArP2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntArP2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  NotaMPAG                       decimal(15,8) NOT NULL  DEFAULT "0.00000000",');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  Qry.SQL.Add('  PedItsLib                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsFin                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsVda                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOM2                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  ReqMovEstq                     bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  StqCenLoc                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ItemNFe                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulFrnCab                    bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeSer                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulNFeCab                    bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GGXRcl                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DtCorrApo                      datetime                                   ,');
  Qry.SQL.Add('  IxxMovIX                       bigint(20) unsigned NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IxxFolha                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IxxLinha                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CusFrtAvuls                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CusFrtMOEnv                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CusFrtMORet                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CustoMOPc                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(157)                               ,');
  Qry.SQL.Add('  Mediam2Peca                    double(20,7)                               ,');
  Qry.SQL.Add('  Mediam2Peca_TXT                varchar(59)                                ,');
  Qry.SQL.Add('  RendKgm2                       double(19,7)                               ,');
  Qry.SQL.Add('  RendKgm2_TXT                   varchar(58)                                ,');
  Qry.SQL.Add('  NotaMPAG_TXT                   varchar(53)                                ,');
  Qry.SQL.Add('  Misturou_TXT                   varchar(3)   NOT NULL                      ,');
  Qry.SQL.Add('  SIGLAUNIDMED                   varchar(6)                                 ,');
  Qry.SQL.Add('  NOMEUNIDMED                    varchar(30)                                ,');
  Qry.SQL.Add('  KgMedioCouro                   double(19,7)                               ,');
  Qry.SQL.Add('  NO_SerieFch                    varchar(60)                                ,');
  Qry.SQL.Add('  ArtGeComodty                   int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ArtGeComOrd                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ArtGeComSgl                    varchar(10)  NOT NULL DEFAULT "??"         ,');
  // No fim
  Qry.SQL.Add('  DtEntrada_TXT                  varchar(10) NOT NULL DEFAULT "00/00/0000"  ,');
  Qry.SQL.Add('  AnoMesDiaIn                    int(11)      NOT NULL  DEFAULT "0"         ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSGerArtFromInD2(Qry: TmySQLQuery;
  Repeticoes: Integer);
var
  I: Integer;
  S: String;
begin
  //Qry.SQL.Add('  EstqMovIDDst                   int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GGXInn                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  AnoMesDiaIn                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Data                           date NOT NULL         DEFAULT "0000/00/00" ,');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  Qry.SQL.Add('  InnPecas                       double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  InnPesoKg                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  InnKgPeca                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  InnSdoVrtPeca                  double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  InnSdoVrtPeso                  double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  NFePecas                       double(20,3) NOT NULL DEFAULT "0.000"      ,');

  Qry.SQL.Add('  EmCPecas                       double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  EmCPesoKg                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  EmCPercCo                      double(20,3) NOT NULL DEFAULT "0.000"      ,');

  Qry.SQL.Add('  MpCPecas                       double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  MpCPesoKg                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  MpCPercCo                      double(20,3) NOT NULL DEFAULT "0.000"      ,');

  Qry.SQL.Add('  CalPecas                       double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CalPesoKg                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CalPercCo                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CurPecas                       double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CurPesoKg                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CurPercCo                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  FuloesCal                      varchar(20)                                ,');
  Qry.SQL.Add('  FuloesCur                      varchar(20)                                ,');

  Qry.SQL.Add('  ClaPecas                       double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  ClaAreaM2                      double(20,2) NOT NULL DEFAULT "0.00"       ,');

  for I := 1 to Repeticoes do
  begin
    S := Geral.FF0(I);
    Qry.SQL.Add('  Pecas_' + s + '              double(20,3) NOT NULL DEFAULT "0.000"      ,');
    Qry.SQL.Add('  PesoKg_' + s + '             double(20,3) NOT NULL DEFAULT "0.000"      ,');
    Qry.SQL.Add('  AreaM2_' + s + '             double(20,3) NOT NULL DEFAULT "0.000"      ,');
    Qry.SQL.Add('  PercCo_' + s + '             double(20,3) NOT NULL DEFAULT "0.000"      ,');
    Qry.SQL.Add('  RendKgM2_' + s + '           double(20,3) NOT NULL DEFAULT "0.000"      ,');
    Qry.SQL.Add('  MediaM2_' + s + '            double(20,3) NOT NULL DEFAULT "0.000"      ,');
  end;
  //
  //Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (GGXInn, Marca, AnoMesDiaIn)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSGerArtFromInDd(Qry: TmySQLQuery; Repeticoes: Integer);
var
  I: Integer;
  S: String;
begin
  //Qry.SQL.Add('  EstqMovIDDst                   int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GGXInn                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  AnoMesDiaIn                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Data                           date NOT NULL         DEFAULT "0000/00/00" ,');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  Qry.SQL.Add('  InnPecas                       double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  InnPesoKg                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  InnKgPeca                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  InnSdoVrtPeca                  double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  InnSdoVrtPeso                  double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  NFePecas                       double(20,3) NOT NULL DEFAULT "0.000"      ,');

  Qry.SQL.Add('  EmCPecas                       double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  EmCPesoKg                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  EmCPercCo                      double(20,3) NOT NULL DEFAULT "0.000"      ,');

  Qry.SQL.Add('  MpCPecas                       double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  MpCPesoKg                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  MpCPercCo                      double(20,3) NOT NULL DEFAULT "0.000"      ,');

  Qry.SQL.Add('  CalPecas                       double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CalPesoKg                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CalPercCo                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CurPecas                       double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CurPesoKg                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  CurPercCo                      double(20,3) NOT NULL DEFAULT "0.000"      ,');
  for I := 1 to Repeticoes do
  begin
    S := Geral.FF0(I);
    Qry.SQL.Add('  Pecas_' + s + '              double(20,3) NOT NULL DEFAULT "0.000"      ,');
    Qry.SQL.Add('  PesoKg_' + s + '             double(20,3) NOT NULL DEFAULT "0.000"      ,');
    Qry.SQL.Add('  AreaM2_' + s + '             double(20,3) NOT NULL DEFAULT "0.000"      ,');
    Qry.SQL.Add('  PercCo_' + s + '             double(20,3) NOT NULL DEFAULT "0.000"      ,');
    Qry.SQL.Add('  RendKgM2_' + s + '           double(20,3) NOT NULL DEFAULT "0.000"      ,');
    Qry.SQL.Add('  MediaM2_' + s + '            double(20,3) NOT NULL DEFAULT "0.000"      ,');
  end;
  //
  //Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (GGXInn, Marca, AnoMesDiaIn)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSImpIMEI(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  PesoKg                         double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  AreaM2                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  AreaP2                         double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ValorT                         double(19,4) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  SdoVrtPeca                     double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  SdoVrtPeso                     double(20,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  SdoVrtArM2                     double(19,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  GraGru1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(255)                               ,');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PALLET                      varchar(60)                                ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_EMPRESA                     varchar(100)                               ,');
  Qry.SQL.Add('  Cliente                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_CLIENTE                     varchar(100)                               ,');
  Qry.SQL.Add('  Fornece                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_FORNECE                      varchar(100)                               ,');
  Qry.SQL.Add('  Status                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_STATUS                      varchar(20)                                ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  OrdGGX                         int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  OrdGGY                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruY                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_GGY                         varchar(255)                               ,');
  //
  Qry.SQL.Add('  NO_FICHA                       varchar(60)                                ,');
  //
  Qry.SQL.Add('  ReqMovEstq                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Observ                         varchar(255)                               ,');
  Qry.SQL.Add('  PedItsLib                      int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSImpRendIMEI(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  IMEIIni                        int(1)       NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Seq                            int(1)       NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Level1                         int(1)       NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Level2                         int(1)       NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Level3                         int(1)       NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimTwn                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliVenda                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PesoKg                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AreaP2                         double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorT                         double(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SrcMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcGGX                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtPeca                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtPeso                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtArM2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Observ                         varchar(255) NOT NULL                      ,');
  Qry.SQL.Add('  SerieFch                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ficha                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Misturou                       tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  FornecMO                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstGGX                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  QtdGerPeca                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerPeso                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerArM2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdGerArP2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntPeca                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntPeso                     double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntArM2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntArP2                     double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  NotaMPAG                       double(15,8) NOT NULL  DEFAULT "0.00000000",');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  Qry.SQL.Add('  ReqMovEstq                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  StqCenLoc                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ItemNFe                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulFrnCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ClientMO                       int(11)      NOT NULL  DEFAULT "-11"       ,');
  Qry.SQL.Add('  NFeSer                         tinyint(3)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulNFeCab                    int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PrcMovID                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PrcMovNiv                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PrcNivel1                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PrcNivel2                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PrcPeca                        double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PrcPeso                        double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PrcArM2                        double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  PrcKnd                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PrcRend                        double(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Lvl1Ant                        int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          int(1)       NOT NULL  DEFAULT "0"         ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSImpRoma(Qry: TmySQLQuery);
begin
//
end;

procedure TCreateVS.Cria_ntrttVSInCabEIts(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  NO_TTW                         varchar(5)   NOT NULL                      ,');
  Qry.SQL.Add('  ID_TTW                         tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimCod                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimNiv                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimTwn                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Empresa                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ClientMO                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Terceiro                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CliVenda                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  MovimID                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DataHora                       datetime                                   ,');
  Qry.SQL.Add('  Pallet                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGruX                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Pecas                          decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  PesoKg                         decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  AreaM2                         decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  AreaP2                         decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorT                         decimal(15,4) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  SrcMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SrcGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SdoVrtPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  SdoVrtArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  Observ                         varchar(255)                               ,');
  Qry.SQL.Add('  SerieFch                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ficha                          bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Misturou                       bigint(20) unsigned NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  FornecMO                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOKg                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  CustoMOTot                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  ValorMP                        decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CustoPQ                        decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  DstMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DstGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  QtdGerPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdGerArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdGerArP2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntPeca                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntPeso                     decimal(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  QtdAntArM2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  QtdAntArP2                     decimal(15,2) NOT NULL  DEFAULT "0.00"      ,');
  Qry.SQL.Add('  NotaMPAG                       decimal(15,8) NOT NULL  DEFAULT "0.00000000",');
  Qry.SQL.Add('  Marca                          varchar(20)                                ,');
  Qry.SQL.Add('  PedItsLib                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsFin                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PedItsVda                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CustoMOM2                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  ReqMovEstq                     bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  StqCenLoc                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ItemNFe                        bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulFrnCab                    bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeSer                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NFeNum                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  VSMulNFeCab                    bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsMovID                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsNivel1                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsNivel2                      bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GGXRcl                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RmsGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  JmpGGX                         bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DtCorrApo                      datetime                                   ,');
  Qry.SQL.Add('  IxxMovIX                       bigint(20) unsigned NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IxxFolha                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IxxLinha                       bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CusFrtAvuls                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CusFrtMOEnv                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CusFrtMORet                    decimal(15,4) NOT NULL  DEFAULT "0.0000"    ,');
  Qry.SQL.Add('  CustoMOPc                      decimal(15,6) NOT NULL  DEFAULT "0.000000"  ,');
  Qry.SQL.Add('  NO_PRD_TAM_COR                 varchar(157)                               ,');
  Qry.SQL.Add('  NO_Pallet                      varchar(60)                                ,');
  Qry.SQL.Add('  RendKgm2                       double(20,7)                               ,');
  Qry.SQL.Add('  RendKgm2_TXT                   varchar(58)                                ,');
  Qry.SQL.Add('  NotaMPAG_TXT                   varchar(53)                                ,');
  Qry.SQL.Add('  Misturou_TXT                   varchar(3)   NOT NULL                      ,');
  Qry.SQL.Add('  SIGLAUNIDMED                   varchar(6)                                 ,');
  Qry.SQL.Add('  NOMEUNIDMED                    varchar(30)                                ,');
  Qry.SQL.Add('  m2_CouroTXT                    varchar(59)                                ,');
  Qry.SQL.Add('  KgMedioCouro                   double(19,7)                               ,');
  Qry.SQL.Add('  NO_SerieFch                    varchar(60)                                ,');
  Qry.SQL.Add('  NO_EMPRESA                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_FORNECE                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_TRANSPORTA                  varchar(100)                               ,');
  Qry.SQL.Add('  NO_CLIENTEMO                   varchar(100)                               ,');
  Qry.SQL.Add('  NO_PROCEDNC                    varchar(100)                               ,');
  Qry.SQL.Add('  NO_MOTORISTA                   varchar(100)                               ,');
  Qry.SQL.Add('  DtCompra                       datetime                                   ,');
  Qry.SQL.Add('  DtViagem                       datetime                                   ,');
  Qry.SQL.Add('  DtEntrada                      datetime                                   ,');
  Qry.SQL.Add('  Fornecedor                     int(11)                                    ,');
  Qry.SQL.Add('  Transporta                     int(11)                                    ,');
  Qry.SQL.Add('  CabPecas                       double(15,3)                               ,');
  Qry.SQL.Add('  CabPesoKg                      double(15,3)                               ,');
  Qry.SQL.Add('  CabAreaM2                      double(15,2)                               ,');
  Qry.SQL.Add('  CabAreaP2                      double(15,2)                               ,');
  Qry.SQL.Add('  ide_serie                      int(11)                                    ,');
  Qry.SQL.Add('  ide_nNF                        int(11)                                    ,');
  Qry.SQL.Add('  emi_serie                      int(11)                                    ,');
  Qry.SQL.Add('  emi_nNF                        int(11)                                    ,');
  Qry.SQL.Add('  CabValorT                      double(15,4)                               ,');
  Qry.SQL.Add('  CabValorMP                     double(15,2)                               ,');
  Qry.SQL.Add('  InfPecas                       double(15,3)                               ,');
  // No fim
  Qry.SQL.Add('  DtEntrada_TXT                  varchar(10) NOT NULL DEFAULT "00/00/0000"  ,');
  Qry.SQL.Add('  AnoMesDiaIn                    int(11)      NOT NULL  DEFAULT "0"         ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSLstPalBox(Qry: TmySQLQuery);
begin
////////////////////////////////////////////////////////////////////////////////
  EXIT;
////////////////////////////////////////////////////////////////////////////////
  Qry.SQL.Add('  MontPalt                       int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateVS.Cria_ntrttVSLstPalGhost(Qry: TmySQLQuery);
begin
////////////////////////////////////////////////////////////////////////////////
  EXIT;
////////////////////////////////////////////////////////////////////////////////
  Qry.SQL.Add('  Pallet                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Pecas                          double(20,3) NOT NULL DEFAULT "0.000"      ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;


function TCreateVS.RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      //
      ntrttVSMovImp1:         Nome := Lowercase('_VSMovImp1_');
      ntrttVSMovImp2:         Nome := Lowercase('_VSMovImp2_');
      ntrttVSMovImp3:         Nome := Lowercase('_VSMovImp3_');
      ntrttVSMovImp4:         Nome := Lowercase('_VSMovImp4_');
      //ntrttVSMovImp5:       Nome := Lowercase('_VSMovImp5_');
      ntrttVSImpIMEI:         Nome := Lowercase('_VSImpIMEI_');
      ntrttVSImpRoma:         Nome := Lowercase('_VSImpRoma_');
      ntrttVSCacIts:          Nome := Lowercase('_VSCacIts_');
      ntrttVSMovIts:          Nome := Lowercase('_VSMovIts_');
      ntrttVSCacGBY:          Nome := Lowercase('_VSCacGBY_');
      ntrttVSLstPalBox:       Nome := Lowercase('_VSLstPalBox_');
      ntrttVSLstPalGhost:     Nome := Lowercase('_VSLstPalGhost_');
      ntrttVSVMIClaSdo:       Nome := Lowercase('_VSVMIClaSdo_');
      ntrttVSBlendVS:         Nome := Lowercase('_VSBlendVS_');
      ntrttVSVerifIMEI:       Nome := Lowercase('_VSVerifIMEI_');
      ntrttVSSeqIts:          Nome := Lowercase('_VSSeqIts_');
      ntrttVSFluxIncon:       Nome := Lowercase('_VSFluxIncon_');
      ntrttVSPsqSeqCac:       Nome := Lowercase('_VSPsqSeqCac_');
      ntrttVsVmiPsq1:         Nome := Lowercase('_VsVmiPsq1_');
      ntrttVsVmiPsq2:         Nome := Lowercase('_VsVmiPsq2_');
      ntrttVmiEstqEmVmi:      Nome := Lowercase('_vmi_estq_em_vmi_');
      ntrttVmiEstqEmVmiPosit: Nome := Lowercase('_vmi_estq_em_vmi_posit_');
      ntrttVSImpRendIMEI:     Nome := Lowercase('_vs_imp_rend_imei_');
      ntrttVSComparaCacIts:   Nome := Lowercase('_Compara_CacIts_');
      ntrttVSMO:              Nome := Lowercase('_VS_MO_');
      ntrttVSCustoFicha:      Nome := Lowercase('_VS_Cus_Fich');
      ntrttVSUsoPQ_VMI:       Nome := Lowercase('_VS_Uso_PQ_VMI_');
      ntrttVSEstqPQ:          Nome := Lowercase('_vs_estq_pq_');
      ntrttVMIQtdEnvEVMI:     Nome := Lowercase('_vmi_qtd_env_evmi');
      ntrttVMIQtdEnvGVMI:     Nome := Lowercase('_vmi_qtd_env_gvmi');
      ntrttVMIQtdEnvRVMI:     Nome := Lowercase('_vmi_qtd_env_rvmi');
      ntrttVSMOEnvCTeGer:     Nome := Lowercase('_vs_mo_env_cte_ger');
      ntrttVSMOEnvNFeGer:     Nome := Lowercase('_vs_mo_env_nfe_ger');
      ntrttVSInCabEIts:       Nome := Lowercase('_vs_In_Cab_E_Its');
      ntrttVSGACabEIts:       Nome := Lowercase('_vs_GA_Cab_E_Its');
      ntrttVSGACabEIt2:       Nome := Lowercase('_vs_GA_Cab_E_It2');
      ntrttVSGerArtFromInDd:  Nome := Lowercase('_vs_ger_art_from_in_dd');
      ntrttVSGerArtFromInD2:  Nome := Lowercase('_vs_ger_art_from_in_d2');
      ntrttVSKardex2:         Nome := Lowercase('_vs_imp_kardex_2');
      ntrttVSKardex3:         Nome := Lowercase('_vs_imp_kardex_3');
      ntrttVSTabMPAG:         Nome := Lowercase('_vs_tab_MPAG_');
      ntrttVSMarcasDesc:      Nome := Lowercase('_vs_marcas_desc_');
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MensagemBox(
    'Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)',
    'Erro', MB_OK+MB_ICONERROR);
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    ntrttVSMovImp1:         Cria_ntrttVSMovImp1(Qry);
    ntrttVSMovImp2:         Cria_ntrttVSMovImp2(Qry);
    ntrttVSMovImp3:         Cria_ntrttVSMovImp3(Qry);
    ntrttVSMovImp4:         Cria_ntrttVSMovImp4(Qry);
    //ntrttVSMovImp5:         Cria_ntrttVSMovImp4(Qry);
    ntrttVSImpIMEI:         Cria_ntrttVSImpIMEI(Qry); // C�pia do VSMovImp4
    ntrttVSCacIts:          Cria_ntrttVSCacIts(Qry);
    ntrttVSMovIts:          Cria_ntrttVSMovIts(Qry);
    ntrttVSCacGBY:          Cria_ntrttVSCacGBY(Qry);
    ntrttVSLstPalBox:       Cria_ntrttVSLstPalBox(Qry);
    ntrttVSLstPalGhost:     Cria_ntrttVSLstPalGhost(Qry);
    ntrttVSVMIClaSdo:       Cria_ntrttVSVMIClaSdo(Qry);
    ntrttVSBlendVS:         Cria_ntrttVSBlendVS(Qry);
    ntrttVSVerifIMEI:       Cria_ntrttVSVerifIMEI(Qry);
    ntrttVSSeqIts:          Cria_ntrttVSSeqIts(Qry);
    ntrttVSFluxIncon:       Cria_ntrttVSFluxIncon(Qry);
    ntrttVSPsqSeqCac:       Cria_ntrttVSPsqSeqCac(Qry);
    ntrttVsVmiPsq1:         Cria_ntrttVsVmiPsq1(Qry);
    ntrttVsVmiPsq2:         Cria_ntrttVsVmiPsq2(Qry);
    ntrttVmiEstqEmVmi:      Cria_ntrttVmiEstqEmVmi(Qry);
    ntrttVmiEstqEmVmiPosit: Cria_ntrttVmiEstqEmVmiPosit(Qry);
    ntrttVSImpRendIMEI:     Cria_ntrttVSImpRendIMEI(Qry);
    ntrttVSComparaCacIts:   Cria_ntrttVSComparaCacIts(Qry);
    ntrttVSMO:              Cria_ntrttVSMO(Qry);
    ntrttVSCustoFicha:      Cria_ntrttVSCustoFicha(Qry);
    ntrttVSUsoPQ_VMI:       Cria_ntrttVSUsoPQ_VMI(Qry);
    ntrttVSEstqPQ:          Cria_ntrttVSEstqPQ(Qry);
    ntrttVMIQtdEnvEVMI:     Cria_ntrttVMIQtdEnvEVMI(Qry);
    ntrttVMIQtdEnvGVMI:     Cria_ntrttVMIQtdEnvGVMI(Qry);
    ntrttVMIQtdEnvRVMI:     Cria_ntrttVMIQtdEnvRVMI(Qry);
    ntrttVSMOEnvCTeGer:     Cria_ntrttVSMOEnvCTeGer(Qry);
    ntrttVSMOEnvNFeGer:     Cria_ntrttVSMOEnvNFeGer(Qry);
    ntrttVSInCabEIts:       Cria_ntrttVSInCabEIts(Qry);
    ntrttVSGACabEIts:       Cria_ntrttVSGACabEIts(Qry);
    ntrttVSGACabEIt2:       Cria_ntrttVSGACabEIt2(Qry);
    ntrttVSGerArtFromInDd:  Cria_ntrttVSGerArtFromInDd(Qry, Repeticoes);
    ntrttVSGerArtFromInD2:  Cria_ntrttVSGerArtFromInD2(Qry, Repeticoes);
    ntrttVSKardex2:         Cria_ntrttVSKardex2(Qry);
    ntrttVSKardex3:         Cria_ntrttVSKardex3(Qry);
    ntrttVSTabMPAG:         Cria_ntrttVSTabMPAG(Qry);
    ntrttVSMarcasDesc:      Cria_ntrttVSMarcasDesc(Qry);
    //
    else Geral.MensagemBox('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o!', 'Erro', MB_OK+MB_ICONERROR);
  end;
  Result := TabNo;
end;

end.

