unit VSImpProducao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditDateTimePicker, mySQLDbTables, dmkDBLookupComboBox, dmkEditCB,
  dmkCheckGroup, dmkDBGridZTO, UnProjGroup_Consts, frxClass, frxDBSet;

type
  TFmVSImpProducao = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrGraGruX: TMySQLQuery;
    QrGraGruXGraGruX: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    DsGraGruX: TDataSource;
    CGEstagios: TdmkCheckGroup;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    CkDataIni: TCheckBox;
    TPDataIni: TdmkEditDateTimePicker;
    CkDataFim: TCheckBox;
    TPDataFim: TdmkEditDateTimePicker;
    RGFormaQtdes: TRadioGroup;
    QrSum: TMySQLQuery;
    QrSumDATA: TDateField;
    QrSumDataHora: TDateTimeField;
    QrSumGRUPO_TXT: TWideStringField;
    QrSumGraGruX: TIntegerField;
    QrSumNO_PRD_TAM_COR: TWideStringField;
    QrSumMovimID: TIntegerField;
    QrSumMovimNiv: TIntegerField;
    QrSumIuvpei: TIntegerField;
    QrSumPecas: TFloatField;
    QrSumPesoKg: TFloatField;
    QrSumAreaM2: TFloatField;
    DBGSum: TdmkDBGridZTO;
    DsSum: TDataSource;
    QrSumGRUPO_COD: TFloatField;
    frxDsSum: TfrxDBDataset;
    Panel6: TPanel;
    Label7: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGAgrupa: TRadioGroup;
    Ck1Cor: TCheckBox;
    QrClienteMO: TMySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClienteMO: TDataSource;
    Label20: TLabel;
    EdClientMO: TdmkEditCB;
    CBClientMO: TdmkDBLookupComboBox;
    frxWET_CURTI_232_00_A: TfrxReport;
    QrSumIMEI: TIntegerField;
    Label2: TLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    QrFornecedor: TMySQLQuery;
    QrFornecedorCodigo: TIntegerField;
    QrFornecedorNOMEENTIDADE: TWideStringField;
    QrFornecedorNOME_E_DOC_ENTIDADE: TWideStringField;
    DsFornecedor: TDataSource;
    Label1: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    QrSumNO_Fornece: TWideStringField;
    frxWET_CURTI_232_00_B: TfrxReport;
    QrSumTerceiro: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxWET_CURTI_232_00_AGetValue(const VarName: string;
      var Value: Variant);
    procedure DBGSumDblClick(Sender: TObject);
  private
    { Private declarations }
    function  GeraSQLTab(FormaQtdes: Integer): String;
    procedure RelProducao();
    //procedure RelProducaoSum();
    //procedure RelProducaoIts();
    procedure Imprime();

  public
    { Public declarations }
  end;

  var
  FmVSImpProducao: TFmVSImpProducao;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnDmkProcFunc, ModuleGeral, UnVS_CRC_PF;

{$R *.DFM}

const
  OrdSQL: array[0..3] of String = ('DATA', 'GRUPO_COD', 'vmi.GraGruX', 'vmi.Terceiro');
  OrdFrx: array[0..3] of String = ('DATA', 'GRUPO_COD', 'GraGruX', 'Terceiro');
  Textxo: array[0..3] of String = ('DATA', 'GRUPO_TXT', 'NO_PRD_TAM_COR', 'NO_Fornece');
  Tituls: array[0..3] of String = ('DATA', 'Est�gio', 'Artigo', 'Fornecedor');

procedure TFmVSImpProducao.BtOKClick(Sender: TObject);
begin
{
  case RGFormaQtdes.ItemIndex of
    0: RelProducaoSum();
    1: RelProducaoIts();
    else Geral.MB_Aviso('Informe a forma das quantidades');
  end;
}
  RelProducao();
end;

procedure TFmVSImpProducao.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpProducao.DBGSumDblClick(Sender: TObject);
var
  Campo: String;
  Coluna: Integer;
begin
  Coluna := TStringGrid(DBGSum).Col;
  Campo := Uppercase(TDBGrid(DBGSum).Columns[Coluna -1].FieldName);
  //
  if Campo = Uppercase('IMEI') then
    VS_CRC_PF.MostraFormVS_Do_IMEI(QrSumIMEI.Value);
end;

procedure TFmVSImpProducao.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSImpProducao.FormCreate(Sender: TObject);
begin                                         ImgTipo.SQLType := stLok;
  TPDataIni.Date := Date - 30;
  TPDataFIm.Date := Date;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DModG.SelecionaEmpresaSeUnica(EdEmpresa,CBEmpresa);
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT cou.GraGruX, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR ',
  'FROM gragruxcou cou  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=cou.GraGruX  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE ggx.Controle > 0 ',
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
  UnDmkDAC_PF.AbreQuery(QrCLienteMO, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
end;

procedure TFmVSImpProducao.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSImpProducao.frxWET_CURTI_232_00_AGetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT, Texto: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
  I: Integer;
begin
  if VarName = 'VARF_NOMEDONO' then
    Value := Dmod.QrMasterEm.Value
  else
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_DATA_IMP' then
    Value := Now()
  else
  if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp1(TPDataIni.Date, TPDataFim.Date,
    CkDataIni.Checked, CkDataFim.Checked, '', 'at�', '')
  else
  if VarName = 'VARF_CLI_INT' then
    Value := dmkPF.ParValueCodTxt(
      '', CBClientMO.Text, EdClientMO.ValueVariant, 'TODAS')
  else
  if VarName = 'VARF_ESTAGIOS' then
  begin
    if (CGEstagios.Value = 0) or (CGEstagios.Value = CGEstagios.MaxValue) then
      Value := 'TODOS'
    else
    begin
      Texto := '';
      //
      for I := 0 to CGEstagios.Items.Count -1 do
      begin
        if CGEstagios.Checked[I] then
          Texto := Texto + ' [' + Copy(CGEstagios.Items[I], 8) + ']';
      end;
      Value := Texto;
    end;
  end
  else
  if VarName = 'VARF_TITULO' then
    Value := RGFormaQtdes.Items[RGFormaQtdes.ItemIndex]
  else
end;

function TFmVSImpProducao.GeraSQLTab(FormaQtdes: Integer): String;
var
  GraGruX, ClientMO, Filial, Empresa, I, Estagio, K, Fornece: Integer;
  SQL_Empresa, SQL_FormaQtdes, SQL_Periodo, SQL_GROUP, SQL_GraGruX,
  SQL_ClientMO, SQL_Estagios, _ou_, Texto, SQL_Fornece: String;
  function Parte_SQL(Tabela: String): String;
  begin
    Result := Geral.ATS([
    'SELECT vmi.Controle IMEI, CAST(vmi.DataHora AS Date) DATA,  ',
    (*'  /*CONCAT( ',
    '  LPAD(YEAR(vmi.DataHora), 4, "0"),"-", ',
    '  LPAD(MONTH(vmi.DataHora), 2, "0"),"-", ',
    '  LPAD(DAY(vmi.DataHora), 2, "0")) Data,  */',*)
    'vmi.DataHora, vmi.Terceiro, ',
    'CASE  ',
    '  WHEN (MovimID=1 AND MovimNiv=0) ',
    '  THEN 1024  ',
    '  WHEN (MovimID=39 AND MovimNiv=66) ',
    '  THEN 1072 ',
    '  WHEN (MovimID=40 AND MovimNiv=67) ',
    '  THEN 1088 ',
    '  WHEN (MovimID=26 AND MovimNiv=30) ',
    '  THEN 2048 ',
    '  WHEN (MovimID=29 AND MovimNiv=31 AND Iuvpei=6) ',
    '  THEN 3072 ',
    '  WHEN (MovimID=27 AND MovimNiv=35) ',
    '  THEN 4096  ',
    '  WHEN (MovimID=6 AND MovimNiv=13) ',
    '  THEN 5120 ',
    '  WHEN (MovimID=7 AND MovimNiv=2) ',
    '  THEN 6144  ',
    '  WHEN (MovimID=8 AND MovimNiv=2) ',
    '  THEN 7168 ',
    '  WHEN (MovimID=11 AND MovimNiv=8) ',
    '  THEN 8192 ',
    '  WHEN (MovimID=19 AND MovimNiv=21) ',
    '  THEN 9216 ',
    '  ELSE 9999 ',
    '  END + 0.000 GRUPO_COD, ',
    'CASE  ',
    '  WHEN (MovimID=1 AND MovimNiv=0) ',
    '  THEN "Entrada In Natura"  ',
    '  WHEN (MovimID=39 AND MovimNiv=66) ',
    '  THEN "Mat�ria-prima Em Conserva��o"  ',
    '  WHEN (MovimID=40 AND MovimNiv=67) ',
    '  THEN "Mat�ria-prima Conservada"  ',
    '  WHEN (MovimID=26 AND MovimNiv=30) ',
    '  THEN "Em Caleiro" ',
    '  WHEN (MovimID=29 AND MovimNiv=31 AND Iuvpei=6) ',
    '  THEN "Artigo Caleado" ',
    '  WHEN (MovimID=27 AND MovimNiv=35) ',
    '  THEN "Em Curtimento" ',
    '  WHEN (MovimID=6 AND MovimNiv=13) ',
    '  THEN "Couro Curtido" ',
    '  WHEN (MovimID=7 AND MovimNiv=2) ',
    '  THEN "Couro Classificado"  ',
    '  WHEN (MovimID=8 AND MovimNiv=2) ',
    '  THEN "Couro Reclassificado" ',
    '  WHEN (MovimID=11 AND MovimNiv=8) ',
    '  THEN "Couro Curtido em Opera��o" ',
    '  WHEN (MovimID=19 AND MovimNiv=21) ',
    '  THEN "Couro em Recurtimento" ',
    '  ELSE "???????" ',
    '  END GRUPO_TXT, ',
    'vmi.GraGruX, ',
    '',
    'CONCAT(gg1.Nome,  ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
    'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome)))  ',
    'NO_PRD_TAM_COR, ',
    '',
    'vmi.MovimID,  ',
    'vmi.MovimNiv, vmi.Iuvpei, ',
    'IF(vmi.Terceiro=0, "**Misturado**", IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome)) NO_Fornece, ',
    SQL_FormaQtdes,
    'FROM ' + Tabela + ' vmi ',
    'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
    'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE vmi.Pecas > 0  ',
    SQL_Empresa,
    SQL_Periodo,
    'AND ( ',
      SQL_Estagios,
    '  )  ',
    SQL_ClientMO,
    SQL_Fornece,
    SQL_GraGruX,
    SQL_GROUP,
    '']);
  end;
begin
  SQL_Empresa    := '';
  SQL_FormaQtdes := '';
  SQL_Periodo    := '';
  SQL_GROUP      := '';
  SQL_GraGruX    := '';
  SQL_ClientMO   := '';
  SQL_Estagios   := '';
  SQL_Fornece    := '';
  //
  case RGFormaQtdes.ItemIndex of
    0:
    begin
      SQL_FormaQtdes := Geral.ATS([
        'SUM(vmi.Pecas) Pecas,  ',
        'SUM(vmi.PesoKg) PesoKg,  ',
        'SUM(vmi.AreaM2) AreaM2 ']);
      SQL_GROUP := 'GROUP BY ' +
        OrdSQL[RGOrdem1.ItemIndex] + ', ' +
        OrdSQL[RGOrdem2.ItemIndex] + ', ' +
        OrdSQL[RGOrdem3.ItemIndex] + ' ';
    end;
    1:
    begin
      SQL_FormaQtdes := Geral.ATS([
        'vmi.Pecas,  ',
        'vmi.PesoKg,  ',
        'vmi.AreaM2 ']);
      SQL_GROUP := '';
    end;
    else Geral.MB_Aviso('Informe a forma das quantidades');
  end;
  //
  SQL_Periodo := dmkPF.SQL_Periodo('AND vmi.DataHora ', TPDataIni.Date,
  TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked); //23:59:59"
  //
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX <> 0 then
  SQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX);
  //
  ClientMO := EdClientMO.ValueVariant;
  if ClientMO <> 0 then
  SQL_ClientMO := 'AND vmi.ClientMO=' + Geral.FF0(ClientMO);
  //
  Fornece := EdFornece.ValueVariant;
  if Fornece <> 0 then
  SQL_Fornece := 'AND vmi.Terceiro=' + Geral.FF0(Fornece);
  //
  Filial := EdEmpresa.ValueVariant;
  if Filial <> 0 then
    Empresa := DModG.ObtemEntidadeDeFilial(Filial)
  else
    Empresa := 0;
  if Empresa <> 0 then
    SQL_Empresa := 'AND vmi.Empresa=' + Geral.FF0(Empresa);
  //
  if (CGEstagios.Value > 0) and (CGEstagios.VAlue < CGEstagios.MaxValue) then
  begin
    SQL_Estagios := '';
    _ou_ := '';
    for I := 0 to CGEstagios.Items.Count - 1 do
    begin
      if CGEstagios.Checked[I] then
      begin
        K := pos(' ', CGEstagios.Items[I]);
        Estagio := Geral.IMV(Trim(Copy(CGEstagios.Items[I], 1, K)));
        case Estagio of
        //0512 - Subproduto In Natura
        //0683 - Subproduto em Processo
        //0853 - Subproduto Processado
          1024: Texto := '  (MovimID=1 AND MovimNiv=0) ';
          1072: Texto := '  (MovimID=39 AND MovimNiv=66) '; // Mat�ria-prima Em Conserva��o
          1088: Texto := '  (MovimID=40 AND MovimNiv=67) '; // Mat�ria-prima Conservada
          2048: Texto := '  (MovimID=26 AND MovimNiv=30) ';
          3072: Texto := '  (MovimID=29 AND MovimNiv=31 AND Iuvpei=6) ';
          4096: Texto := '  (MovimID=27 AND MovimNiv=35) ';
          5120: Texto := '  (MovimID=6 AND MovimNiv=13) ';
          6144: Texto := '  (MovimID=7 AND MovimNiv=2) ' ;
          7168: Texto := '  (MovimID=8 AND MovimNiv=2) ';
          8192: Texto := '  (MovimID=19 AND MovimNiv=21) ';
          9216: Texto := '  (MovimID=11 AND MovimNiv=8) ';
          else  Texto := '  (????) ';
        end;
        SQL_Estagios := SQL_Estagios + _ou_ + Texto;
        _ou_ := sLineBreak + '  OR  ' + sLineBreak;
      end;
    end;
  end else
    SQL_Estagios := Geral.ATS([
    '  (MovimID=1 AND MovimNiv=0) ',
    '  OR  ',
    '  (MovimID=39 AND MovimNiv=66) ',
    '  OR  ',
    '  (MovimID=40 AND MovimNiv=67) ',
    '  OR  ',
    '  (MovimID=26 AND MovimNiv=30) ',
    '  OR  ',
    '  (MovimID=29 AND MovimNiv=31 AND Iuvpei=6) ',
    '  OR  ',
    '  (MovimID=27 AND MovimNiv=35) ',
    '  OR  ',
    '  (MovimID=6 AND MovimNiv=13) ',
    '  OR  ',
    '  (MovimID=7 AND MovimNiv=2) ',
    '  OR  ',
    '  (MovimID=8 AND MovimNiv=2) ',
    '  OR  ',
    '  (MovimID=19 AND MovimNiv=21) ',
    '  OR  ',
    '  (MovimID=11 AND MovimNiv=8) ',
    '']);
  //
  Result := Geral.ATS([
    Parte_SQL(CO_TAB_VMI),
    '',
    'UNION',
    '',
    Parte_SQL(CO_TAB_VMB),
    '',
    'ORDER BY ' +
      OrdFrx[RGOrdem1.ItemIndex] + ', ' +
      OrdFrx[RGOrdem2.ItemIndex] + ', ' +
      OrdFrx[RGOrdem3.ItemIndex] + ' ',
  '']);
  //Geral.MB_Info(Result);
end;

procedure TFmVSImpProducao.Imprime;
var
  Campo, TitGru, Texto: String;
  Cor1, Cor2: TColor;
  Memo: TfrxMemoView;
  Report: TFrxReport;
begin
  case RGFormaQtdes.ItemIndex of
    0: Report := frxWET_CURTI_232_00_A;
    1: Report := frxWET_CURTI_232_00_B;
    else
    begin
      Geral.MB_Erro(RGFormaQtdes.Caption + ' n�o implementado!');
      Exit;
    end;
  end;
  MyObjects.frxDefineDataSets(Report, [
    frxDsSum
  ]);
  //
  //if RGFormaQtdes.ItemIndex = 0 then
  begin
    Memo := Report.FindObject('MeVMI') as TfrxMemoView;
    if Memo <> nil then
      Memo.Visible := RGFormaQtdes.ItemIndex = 1;

    //Report.Variables['VARF_MeIMEI']   :=  '<frxDsSum."IMEI">'
    //Report.Variables['VARF_MeIMEI']   := '0';
  end;
  //
  Report.Variables['VARF_AGRUP1']   := RGAgrupa.ItemIndex > 0;
  Report.Variables['VARF_AGRUP2']   := RGAgrupa.ItemIndex > 1;
  Report.Variables['VARF_AGRUP3']   := RGAgrupa.ItemIndex > 2;

  Campo  := OrdFrx[RGOrdem1.ItemIndex];
  TitGru := Tituls[RGOrdem1.ItemIndex];
  Texto  := Textxo[RGOrdem1.ItemIndex];
  Report.Variables['VARF_GRUPO1']   := ''''+'<frxDsSum."'+Campo+'">''';
  Report.Variables['VARF_HEADR1']   := ''''+TitGru+': [frxDsSum."'+Texto+'"]''';
  Report.Variables['VARF_FOOTR1']   := ''''+TitGru+': [frxDsSum."'+Texto+'"]''';
  //
  Campo  := OrdFrx[RGOrdem2.ItemIndex];
  TitGru := Tituls[RGOrdem2.ItemIndex];
  Texto  := Textxo[RGOrdem2.ItemIndex];
  Report.Variables['VARF_GRUPO2']   := ''''+'<frxDsSum."'+Campo+'">''';
  Report.Variables['VARF_HEADR2']   := ''''+TitGru+': [frxDsSum."'+Texto+'"]''';
  Report.Variables['VARF_FOOTR2']   := ''''+TitGru+': [frxDsSum."'+Texto+'"]''';
  //
  Campo  := OrdFrx[RGOrdem3.ItemIndex];
  TitGru := Tituls[RGOrdem3.ItemIndex];
  Texto  := Textxo[RGOrdem3.ItemIndex];
  Report.Variables['VARF_GRUPO3']   := ''''+'<frxDsSum."'+Campo+'">''';
  Report.Variables['VARF_HEADR3']   := ''''+TitGru+': [frxDsSum."'+Texto+'"]''';
  Report.Variables['VARF_FOOTR3']   := ''''+TitGru+': [frxDsSum."'+Texto+'"]''';
  //
  if Ck1Cor.Checked then
  begin
    Cor1 := $00E6C29B;
    Cor2 := $00B4E0C6;
  end else
  begin
    Cor1 := clWhite;
    Cor2 := clWhite;
  end;
  (Report.FindObject('Me_GH2') as TfrxMemoView).Color      := Cor1;
  (Report.FindObject('Me_GH3') as TfrxMemoView).Color      := Cor2;
  (Report.FindObject('MeGH_02_F01') as TfrxMemoView).Color := Cor1;
  (Report.FindObject('MeMD_F01') as TfrxMemoView).Color    := Cor1;
  (Report.FindObject('MeMD_F02') as TfrxMemoView).Color    := Cor2;
  (Report.FindObject('MeFT_03_F01') as TfrxMemoView).Color := Cor1;
  (Report.FindObject('MeFT_03_F02') as TfrxMemoView).Color := Cor2;
  (Report.FindObject('MeFT_02') as TfrxMemoView).Color     := Cor1;
  //
  (Report.FindObject('MeMD_F01') as TfrxMemoView).Visible  := RGAgrupa.ItemIndex > 0;
  (Report.FindObject('MeMD_F02') as TfrxMemoView).Visible  := RGAgrupa.ItemIndex > 1;
  //
  MyObjects.frxMostra(Report, 'Relat�rio de Produ��o', 'RelProducao');
end;

procedure TFmVSImpProducao.RelProducao();
var
  SQL: String;
begin
  DBGSum.DataSource := nil;
  try
    SQL := GeraSQLTab(RGFormaQtdes.ItemIndex);
    UnDMkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [SQL]);
    Imprime();
  finally
    DBGSum.DataSource := DsSum;
    if RGFormaQtdes.ItemIndex = 0 then
      DBGSum.Columns[0].FieldName := '';
  end;
end;

{
procedure TFmVSImpProducao.RelProducaoIts();
var
  SQL: String;
begin
  SQL := GeraSQLTab(RGFormaQtdes.ItemIndex);
  UnDMkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [SQL]);
  Imprime();
end;

procedure TFmVSImpProducao.RelProducaoSum();
var
  SQL: String;
begin
  SQL := GeraSQLTab(RGFormaQtdes.ItemIndex);
  UnDMkDAC_PF.AbreMySQLQuery0(QrSum, Dmod.MyDB, [SQL]);
  Imprime();
end;
}

end.
