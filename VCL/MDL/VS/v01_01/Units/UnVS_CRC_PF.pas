﻿unit UnVS_CRC_PF;

interface

uses
  System.Generics.Collections, UnMyLinguas, MyListas,
  //
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, Buttons, ComCtrls, CommCtrl, Consts,
  Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral, UnDmkEnums, dmkEditCB,
  dmkEdit, dmkDBLookupComboBox, mySQLDbTables, Data.Db, DBGrids, AppListas,
  dmkDBGridZTO, UnDmkProcFunc, UnProjGroup_Vars, BlueDermConsts, TypInfo,
  System.Math, UnProjGroup_Consts, UnEntities, DBCtrls, Grids, Mask, UnVS_Tabs,
  dmkEditDateTimePicker, UnGrl_Consts, UnGrl_Geral, UnGrl_Vars,
  UnAppEnums, dmkPageControl;

type
  THackDBGrid = class(TDBGrid);
  TPallArr = array of Integer;
  TClass15Int = array[0..15] of Integer;
  TUnVS_CRC_PF = class(TObject)
  private
    { Private declarations }
    function  DesenhaSimboloDefeito(Bitmap: TBitmap; const X, Y,
              Simbolo, CorBorda, CorMiolo: Integer): Double;
  public
    { Public declarations }
    procedure DadosDaFaixaDaMediaM2DoCouro(const FaixaMediaM2: Double; var
              CorFundo, CorTexto: TColor; var Texto: String);
    function  EditaIxx(DGDados: TDBGrid; QrVSMovIts: TmySQLQuery): Boolean;
    procedure SetaGGXUnicoEmLista(const GraGruX: Integer; var Lista: TPallArr);
    procedure SetIDTipoCouro(RG: TRadioGroup; Colunas, Default: Integer);
    function  ComparaUnidMed2GGX(GGX1, GGX2: Integer): Boolean;
    procedure EncerraRendimentoInn(Modo: TdmkModoExec; MovimCod, Serie, Ficha:
              Integer; PB: TProgressBar; LaAviso1, LaAviso2: TLabel; GraGruYIni,
              GraGruYFim, GraGruYPos: Integer);
    procedure InsereVSFchRMPCab(SerieFch, Ficha, MovimID, Terceiro: Integer);
    function  InsereVSMovDif(Controle: Integer; Pecas, PesoKg, AreaM2, AreaP2,
              ValorT, InfPecas, InfPesoKg, InfAreaM2, InfAreaP2, InfValorT,
              PerQbrViag, PerQbrSal, RstCouPc, RstCouKg, RstCouVl, RstSalKg,
              RstSalVl, RstTotVl: Double; TribDefSel: Integer; PesoSalKg:
              Double): Boolean;
    procedure DefineDataHoraOuDtCorrApoCompos(const DataHora, DtCorrApo: TDateTime;
              TPData: TdmkEditDateTimePicker; EdHora: TdmkEdit); overload;
    procedure DefineDataHoraOuDtCorrApoCompos(const DataHora, DtCorrApo: TDateTime;
              var _DataHora: TDateTime); overload;
    function  DefineIDs_Str(DBGridZTO: TdmkDBGridZTO; Query:
              TmySQLQuery; Campo: String): String; // Corda Lista
    procedure DefinirAscendenciasDeIMEIsDeMovimCod(IMEIAtu, MovimCod: Integer;
              MovimNivSrc, MovimNivDst: TEstqMovimNiv);
    function  IMEI_JaEstaNoArray(const IMEI: Integer; const Lista: array of Integer;
              var ItensLoc: Integer): Boolean;
    function  CordaIMEIS(Itens: array of Integer): String;
//
    procedure AbreGraGruXY(Qry: TmySQLQuery; _AND: String);
    procedure AbreVSSerFch(QrVSSerFch: TmySQLQuery);
    function  AdicionarNovosVS_emid(): Boolean;
    function  AlteraVMI_AreaM2(MovimID, MovimNiv, Controle: Integer;
              Default: Double; PermiteNegativo: TSinal): Boolean;
    function  AlteraVMI_CliVenda(Controle, Atual: Integer): Boolean;
    function  AlteraVMI_FornecMO(Controle, Atual: Integer): Boolean;
    function  AlteraVMI_PesoKg(CampoPeso: String; MovimID, MovimNiv, Controle:
              Integer; Default: Double; PermiteNegativo: TSinal): Boolean;
    function  AlteraVMI_StqCenLoc(Controle, Atual: Integer): Boolean;
    function  AreaEstaNaMedia(Pecas, AreaM2, MediaMinM2, MediaMaxM2: Double;
              Pergunta: Boolean): Boolean;
    procedure AtualizaCustosDescendentesGerArtFicha(Empresa, SerieFch, Ficha:
              Integer);
    procedure AtualizaCustoPQ(Controle: Integer; CustoPQ: Double);
    procedure AtualizaDescendentes(IMEI: Integer; Campos: array of String;
              Valores: array of Variant);
    procedure AtualizaDtHrFimOpe_MovimCod(MovimID: TEstqMovimID; MovimCod: Integer);
    procedure AtualizaFornecedorOpe(MovimCod: Integer);
    procedure AtualizaFornecedorOut(IMEI: Integer);
    procedure AtualizaFornecedorPreRcl(MovimCod: Integer);
    procedure AtualizaFornecedorPWE(MovimCod: Integer);
    procedure AtualizaNF_IMEI(MovimCod: Integer; IMEI: Integer);
    procedure AtualizaNotaMPAG(Controle: Integer; NotaMPAG, FatNotaVNC,
              FatNotaVRC: Double);
    procedure AtualizaNotaMPAG_e_QtdGerArM2eP2(Controle: Integer; NotaMPAG,
              FatNotaVNC, FatNotaVRC, QtdGerArM2, QtdGerArP2: Double);
    procedure AtualizaNotaMPAG_e_AreaM2eP2(Controle: Integer; NotaMPAG,
              FatNotaVNC, FatNotaVRC, AreaM2, AreaP2: Double);
    procedure AtualizaPalletPelosIMEIsDeGeracao(Pallet: Integer;
              QrSumVMI: TmySQLQuery);
    procedure AtualizaSaldoIMEI(Controle: Integer; Gera: Boolean);
    procedure AtualizaSaldoItmCal(Controle: Integer);
    procedure AtualizaSaldoItmCur(Controle: Integer);
    procedure AtualizaSaldoOrigemVMI_Dest(VMI_Dest, VMI_Sorc, VMI_Baix: Integer);
    procedure AtualizaSaldoVirtualVSMovIts(Controle: Integer; Gera: Boolean);
    procedure AtualizaSaldoVirtualVSMovIts_Generico(Controle, MovimID, MovimNiv: Integer);
    procedure AtualizaSerieNFeVMC(MovimCod, NFeSer, NFeNum, VSMulNFeCab: Integer);
    procedure AtualizaSerieNFeVMI(Controle, NFeSer, NFeNum, VSMulNFeCab: Integer);
    procedure AtualizaStatPall(Pallet: Integer);
    procedure AtualizaTotaisVSOpeCab(MovimCod: Integer);
    procedure AtualizaTotaisVSCalCab(MovimCod: Integer);
    procedure AtualizaTotaisVSConCab(MovimCod: Integer);
    procedure AtualizaTotaisVSCurCab(MovimCod: Integer);
    procedure AtualizaTotaisVSPSPCab(MovimCod: Integer);
    procedure AtualizaTotaisVSRRMCab(MovimCod: Integer);
    procedure AtualizaTotaisVSPWECab(MovimCod: Integer);
    procedure AtualizaTotaisVSXxxCab(Tabela: String; MovimCod: Integer);
    procedure AtualizaValoresDescend_0000_All(Qry: TmySQLQuery; CustoM2: Double);
    procedure AtualizaValoresDescendArtGerAposInfoAreaTotal(IMEIArtGer: Integer;
              AreaM2, ValorT: Double);
    procedure AtualizaValoresDescend_ID02_Venda(Controle, MovimCod: Integer;
              AreaM2, CustoM2: Double);
    procedure AtualizaValoresDescend_ID14_ClassArtVSMul(MovimTwn: Integer;
              CustoM2: Double);
    procedure AtualizaValoresDescend_ID19_EmProcWE(Controle, MovimCod: Integer;
              AreaM2, CustoM2: Double);
    procedure AtualizaValoresDescend_ID25_TransfLocal(Controle, MovimCod: Integer;
              AreaM2, CustoM2: Double);
    function  AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc: Integer;
              QrSumDest, QrSumSorc, QrVMISorc, QrPalSorc: TmySQLQuery): Boolean;
    procedure AtualizaVSConCabGGxSrc(MovimCod: Integer);
    procedure AtualizaVSCalCabGGxSrc(MovimCod: Integer);
    procedure AtualizaVSCurCabGGxSrc(MovimCod: Integer);
    procedure AtualizaVSPedIts_Fin(VSPedIts: Integer);
    procedure AtualizaVSPedIts_Lib(VSPedIts, VSMovIts: Integer;
              LibPecas, LibPesoKg, LibAreaM2, LibAreaP2: Double);
    procedure AtualizaVSValorT(Controle: Integer);
    //
    function  CadastraPalletInfo(Empresa, ClientMO, GraGruX, GraGruY: Integer;
              QrVSPallet: TmySQLQuery; EdPallet: TdmkEditCB; CBPallet:
              TdmkDBLookupComboBox; SBNewPallet: TSpeedButton; EdPecas:
              TdmkEdit): Integer;
    function  CadastraPalletRibCla(Empresa, ClientMO: Integer; EdPallet:
              TdmkEditCB; CBPallet: TdmkDBLookupComboBox; QrVSPallet: TmySQLQuery;
              MovimIDGer: TEstqMovimID; GraGruX: Integer): Integer;
    function  CalculaValorT(IniPecas, IniAreaM2, IniPesoKg, IniValorT,
              Pecas, AreaM2, PesoKg: Double): Double;  // ObtemValorT()
    function  CampoLstPal(Box: Integer): String;
    function  ConfigContinuarInserindoFolhaLinha(RGModoContinuarInserindo:
              TRadioGroup; EdFolha, EdLinha: TdmkEdit): Boolean;
    procedure CorrigeArtigoOpeEmDiante(IMEI, GGXAtu: Integer);
    procedure CorrigeNFes(const Codigo: Integer; const MovimID: TEstqMovimID;
              const MovimNivsOri, MovimNivsDst: array of TEstqMovimNiv;
              const LaAviso1, LaAviso2: TLabel; var Atualizando: Boolean);
    procedure CorrigeMovimID(const Codigo: Integer; const MovimIDDst,
              MovimIDOri: TEstqMovimID; const MovimNivDst, MovimNivOri:
              TEstqMovimNiv; const SQL_Extra: String; const LaAviso1, LaAviso2:
              TLabel; var Atualizando: Boolean);
    procedure CriaBoxEmGridPanel(const Box: Integer; const Form: TForm; const
              GridPanel: TGridPanel; var FPnTxLx, FPnBox, FPnArt1N, FPnArt2N,
              FPnArt3N, FPnNumBx, FPnIDBox, FPnSubXx, FPnBxTot, FPnIMEIImei,
              FPnIMEICtrl, FPnPalletMedia, FPnPalletArea, FPnPalletPecas,
              FPnPalletID, FPnIntMei: array of TPanel; var FLaNumBx,
              FLaIMEIImei, FLaIMEICtrl, FLaPalletMedia, FLaPalletArea,
              FLaPalletPecas, FLaPalletID: array of TLabel;
              var FCkSubClass: array of TCheckBox; var FCkDuplicaArea: array of TCheckBox;
              var FDBEdArtNome, FDBEdArtNoCli, FDBEdArtNoSta, FDBEdIMEIImei,
              FDBEdIMEICtrl, FDBEdPalletID: array of TDBEdit;
              var FGBIMEI, FGBPallet: array of TGroupBox; var
              FEdPalletMedia, FEdPalletArea, FEdPalletPecas: array of TdmkEdit;
              var FSGItens: array of TStringGrid; FPMItens: array of TPopupMenu);
    function  DefineDatasVMI(const _Data_Hora_: String; var DataHora, DtCorrApo:
              String): Boolean;
    function  DefineSiglaVS_Frn(Fornece: Integer): String;
    function  DefineSiglaVS_NFe(NFeSer, NFeNum: Integer): String;
    function  DesfazEncerramentoPallet(Pallet, GraGruX: Integer): Boolean;
    procedure DistribuiCustoIndsVS(MovimNiv: TEstqMovimNiv; MovimCod,
              Codigo, CtrlDst: Integer);
    function  EncerraPalletNew(const Pallet: Integer;
              const Pergunta: Boolean): Boolean;
    function  EncerraPalletReclassificacaoNew(const VSPaRclCabCacCod,
              VSPaRclCabCodigo, VSPaRclCabVSPallet, Box_Box, Box_VSPaClaIts,
              Box_VSPallet, Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
              const EncerrandoTodos, Pergunta: Boolean; var ReabreVSPaRclCab:
              Boolean): Boolean;
    procedure EncerraPalletSimples(Pallet, Empresa, ClientMO: Integer;
              QrVSPallet: TmySQLQuery; PallOnEdit: array of Integer);
    function  ExcluiCabecalhoReclasse(MovimID: TEstqMovimID; PreClasse,
              Reclasse: Integer): Boolean;
    function  ExcluiCabEIMEI_OpeCab(MovimCod, IMEI: Integer;
              MovimID: TEstqMovimID; MotivDel: TEstqMotivDel): Boolean;
    function  ExcluiControleVSMovIts(Tabela: TmySQLQuery; Campo: TIntegerField;
              Controle1, CtrlBaix, SrcNivel2: Integer; Gera: Boolean;
              Motivo: Integer; Pergunta: Boolean = True; Reabre: Boolean = True): Boolean;
    function  ExcluiVSMovIts_EnviaArquivoExclu(Controle, Motivo: Integer; Query:
              TmySQLQuery; DataBase: TmySQLDatabase; Senha: String): Boolean;
    function  ExcluiVSNaoVMI(Pergunta, Tabela, Campo: String; Inteiro1:
              Integer; DB: TmySQLDatabase): Integer;
    function  FatoresIncompativeis(FatorSrc, FatorDst: Integer; MeAviso: TMemo):
              Boolean;
    function  FatorNotaAR(GraGruX: Integer): Double;
    function  FatorNotaCC(GraGruX, MovimID: Integer): Double;
    function  FatorNotaMP(GraGruX: Integer): Double;
    function  FichaErro(EdSerieFch: TdmkEdit; Empresa, Controle, Ficha: Integer;
              PermiteDuplicar: Boolean =  False): Boolean;
    function  GetIDNiv(MovimID, MovimNiv: Integer): Integer;
    function  GeraNovoPallet(Empresa, ClientMO, GraGruX: Integer;
              EdGraGruX, EdPallet: TdmkEdit; CBPallet: TdmkDBLookupComboBox;
              SBNewPallet: TSpeedButton; QrVSPallet: TmySQLQuery;
              PallOnEdit: array of Integer): Integer;
    function  GeraSQLTabMov(var SQL: String; const Tab: TTabToWork; const
              TemIMEIMrt: Integer = 0): Boolean;
    function  GeraSQLVSMovItx_Base(SQL_Select, SQL_Flds, SQL_Left, SQL_Wher,
              SQL_Group: String; Tab: TTabToWork; TemIMEIMrt: Integer = 0):
              String;
    function  GeraSQLVSMovItx_IMEI(SQL_Select, SQL_Flds, SQL_Left, SQL_Wher,
              SQL_Group: String; Tab: TTabToWork; TemIMEIMrt: Integer = 0):
              String;
    function  GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Where, SQL_Group:
              String; Tab: TTabToWork; TemIMEIMrt: Integer = 0): String;
    function  HabilitaComposVSAtivo(ID_TTW: Integer; Compos:
              array of TComponent): Boolean;
    function  HabilitaMenuInsOuAllVSAberto(Qry: TmySQLQuery; Data: TDateTime;
              Button: TWinControl; DefMenu: TPopupMenu;
              MenuItens: array of TMenuItem): Boolean;
    procedure ImprimeClassIMEI(IMEI, VSGerArt, MulCab_MovimCod: Integer);
    procedure ImprimeClassIMEIs(IMEIS, Mortos: array of Integer;
              MostraForm: Boolean);
    procedure ImprimeIMEI(IMEIs: array of Integer; VSImpImeiKind:
              TXXImpImeiKind; LPFMO, FNFeRem: String; QryCab: TmySQLQuery);
    function  ImprimeEstoqueReal(Entidade, Filial, Ed00Terceiro_ValueVariant,
              RG00_Ordem1_ItemIndex, RG00_Ordem2_ItemIndex,
              RG00_Ordem3_ItemIndex, RG00_Ordem4_ItemIndex,
              RG00_Ordem5_ItemIndex, RG00_Agrupa_ItemIndex: Integer;
              Ck00DescrAgruNoItm_Checked: Boolean; Ed00StqCenCad_ValueVariant,
              RG00ZeroNegat_ItemIndex: Integer; FNO_EMPRESA, CB00StqCenCad_Text,
              CB00Terceiro_Text: String; TPDataRelativa_Date: TDateTime;
              Ck00DataCompra_Checked, Ck00EmProcessoBH_Checked: Boolean;
              DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2: TdmkDBGridZTO;
              Qr00GraGruY, Qr00GraGruX, Qr00CouNiv2: TmySQLQuery;
              TableSrc, DataRetroativa: String;
              // 2021-01-09 ini
              //GraCusPrc: Integer;
              Relatorio: TRelatorioVSImpEstoque;
              // 2021-01-09 fim
              DataEstoque: TDateTime;
              Ed00NFeIni_ValueVariant, Ed00NFeFim_ValueVariant: Integer;
              Ck00Serie_Checked: Boolean; Ed00Serie_ValueVariant, MovimCod,
              ClientMO: Integer; MostraFrx: Boolean): Boolean;
    procedure ImprimeOrdem(MovimID: TEstqMovimID; Empresa, Codigo, MovimCod:
              Integer);
    procedure ImprimeOXsAbertas(RG18Ordem1_ItemIndex, RG18Ordem2_ItemIndex,
              RG18Ordem3_ItemIndex, RG18Agrupa_ItemIndex: Integer;
              MovimIDs: array of TEstqMovimID(*MovimID: TEstqMovimID*);
              Ck18Cor_Checked, Ck18Especificos_Checked: Boolean;
              Ed18Especificos_Text: String; Ed18Fornecedor_ValueVariant: Integer);
    procedure ImprimePackListsIMEIs(Empresa: Integer; IMEIS: array of Integer;
              Vertical: Boolean);
    procedure ImprimePallet_Unico(Empresa, ClientMO, Pallet: Integer; JanTab:
              String; InfoNO_PALLET: Boolean);
    procedure ImprimePallet_Varios(Empresa: Integer; Pallets: array of Integer;
              JanTab: String; InfoNO_PALLET: Boolean);
    procedure ImprimePallets(Empresa: Integer; Pallets: array of Integer;
              VSMovImp4, VSLstPalBox: String; InfoNO_PALLET: Boolean;
              DestImprFichaPallet: TDestImprFichaPallet;
              ImpEmpresa: Boolean = True; EmptyLabels: Integer = 0);
    procedure ImprimeReclassOC(OC, CacCod: Integer);
    procedure ImprimePWE_OP_Fluxo(Codigo, Modelo: Integer);
    procedure InfoPalletVMI(VMI, Pallet: Integer; Qry: TmySQLQuery);
    procedure InfoRegIntInVMI(Campo: String; VMI, Inteiro: Integer; Qry:
              TmySQLQuery);
    procedure InfoReqMovEstq(VMI, ReqMovEstq: Integer; Qry: TmySQLQuery);
    function  InformaIxx(DBGIMEI: TDBGrid; QrIMEI: TmySQLQuery; FldIMEI:
              String): Boolean;
    procedure InfoStqCenLoc(VMI, StqCenLoc: Integer; Qry: TmySQLQuery);
    function  InsAltVSPalCla(const Empresa, ClientMO, Fornecedor, VSMulFrnCab:
              Integer; const MovimID: TEstqMovimID; const Codigo, MovimCod,
              BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2,
              VSMovIts, Tecla, VSPallet, GragruX, StqCenLoc: Integer; const
              IuvpeiInn, IuvpeiBxa: TInsUpdVMIPrcExecID; const LaAviso1,
              LaAviso2: TLabel; var CtrlSorc, CtrlDest: Integer): Integer;
    function  InsAltVSPalRclNewUni(const Empresa, FornecMO, ClientMO, Fornecedor, VSMulFrnCab:
              Integer; const MovimID: TEstqMovimID; const Codigo, MovimCod,
              BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2,
              VSMovIts, Tecla, VSPallet, GragruX: Integer;
              const SrcPallet, StqCenLoc: Integer;
              const IuvpeiInn, IuvpeiBxa: TInsUpdVMIPrcExecID;
              const LaAviso1, LaAviso2: TLabel; var CtrlSorc,
              CtrlDest: Integer): Integer;
    procedure InsereVSCacCab(Codigo: Integer; MovimID: TEstqMovimID;
              CodigoID: Integer);
    procedure InsereVSMovCab(Codigo: Integer; MovimID: TEstqMovimID;
              CodigoID: Integer);
{
    function  InsUpdVSMovIts1(SQLType: TSQLType; Codigo, MovimCod, MovimTwn,
              Empresa, Terceiro: Integer; MovimID: TEstqMovimID;
              MovimNiv: TEstqMovimNiv;
              Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2, AreaP2,
              ValorT: Double; _Data_Hora_: String; SrcMovID: TEstqMovimID;
              SrcNivel1, SrcNivel2: Integer; Observ: String;
              LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,
              Misturou*): Integer; CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
              DstMovID: TEstqMovimID; DstNivel1, DstNivel2: Integer;
              QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
              AptoUso, FornecMO, SerieFch: Integer; NotaMPAG: Double;
              SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto, PedItsLib,
              PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
              GSPSrcNiv2, ReqMovEstq,
              //StqCenLoc, ItemNFe, VSMulFrnCab: Integer; ClientMO: Integer = -11): Boolean;
              StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
              QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2: Double;
              GGXRcl: Integer;
              JmpMovID: TEstqMovimID = emidAjuste;
              JmpNivel1: Integer = 0;
              JmpNivel2: Integer = 0;
              JmpGGX: Integer = 0;
              RmsMovID: TEstqMovimID = emidAjuste;
              RmsNivel1: Integer = 0;
              RmsNivel2: Integer = 0;
              RmsGGX: Integer = 0;
              GSPJmpMovID: TEstqMovimID = emidAjuste;
              GSPJmpNiv2: Integer = 0;
              (*ForcaDtCorrApo: TDateTime = 0;*)
              MovCodPai: Integer = 0;
              IxxMovIX: TEstqMovInfo = TEstqMovInfo.eminfIndef;
              IxxFolha: Integer = 0;
              IxxLinha: Integer = 0): Boolean;
}
    function  InsUpdVSMovIts2(SQLType: TSQLType; Codigo, MovimCod, MovimTwn,
              Empresa, Terceiro: Integer; MovimID: TEstqMovimID;
              MovimNiv: TEstqMovimNiv;
              Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2, AreaP2,
              ValorT: Double; _Data_Hora_: String; SrcMovID: TEstqMovimID;
              SrcNivel1, SrcNivel2: Integer; Observ: String;
              LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,
              Misturou*): Integer; CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
              DstMovID: TEstqMovimID; DstNivel1, DstNivel2: Integer;
              QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
              AptoUso, FornecMO, SerieFch: Integer; NotaMPAG: Double;
              SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto, PedItsLib,
              PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
              GSPSrcNiv2, ReqMovEstq,
              //StqCenLoc, ItemNFe, VSMulFrnCab: Integer; ClientMO: Integer = -11): Boolean;
              StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
              QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, PerceComiss,
              CusKgComiss, CustoComiss, CredPereImposto, CredValrImposto,
              CusFrtAvuls: Double;
              //
              GGXRcl: Integer;
              //
              RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI: Double;
              //
              JmpMovID: TEstqMovimID; JmpNivel1, JmpNivel2, JmpGGX: Integer;
              RmsMovID: TEstqMovimID; RmsNivel1, RmsNivel2, RmsGGX: Integer;
              GSPJmpMovID: TEstqMovimID; GSPJmpNiv2, MovCodPai: Integer;
              IxxMovIX: TEstqMovInfo; IxxFolha, IxxLinha: Integer;
              ExigeClientMO, ExigeFornecMO, ExigeStqLoc: Boolean;
              InsUpdVMIPrcExecID: TInsUpdVMIPrcExecID): Boolean;
    function  InsUpdVSMovIts3(SQLType: TSQLType; Codigo, MovimCod, MovimTwn,
              Empresa, Terceiro: Integer; MovimID: TEstqMovimID;
              MovimNiv: TEstqMovimNiv;
              Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2, AreaP2,
              ValorT: Double; _Data_Hora_: String; SrcMovID: TEstqMovimID;
              SrcNivel1, SrcNivel2: Integer; Observ: String;
              LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,
              Misturou*): Integer; CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
              DstMovID: TEstqMovimID; DstNivel1, DstNivel2: Integer;
              QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
              AptoUso, FornecMO, SerieFch: Integer; NotaMPAG: Double;
              SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto, PedItsLib,
              PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
              GSPSrcNiv2, ReqMovEstq,
              StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
              QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, PerceComiss,
              CusKgComiss, CustoComiss, CredPereImposto, CredValrImposto,
              CusFrtAvuls: Double; GGXRcl: Integer;
              RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI: Double;
              JmpMovID: TEstqMovimID; JmpNivel1, JmpNivel2, JmpGGX: Integer;
              RmsMovID: TEstqMovimID; RmsNivel1, RmsNivel2, RmsGGX: Integer;
              GSPJmpMovID: TEstqMovimID; GSPJmpNiv2, MovCodPai, VmiPai: Integer;
              IxxMovIX: TEstqMovInfo; IxxFolha, IxxLinha: Integer;
              ExigeClientMO, ExigeFornecMO, ExigeStqLoc: Boolean;
              InsUpdVMIPrcExecID: TInsUpdVMIPrcExecID): Boolean;
    function  InsUpdVSMovIts4(SQLType: TSQLType; Codigo, MovimCod, MovimTwn,
              Empresa, Terceiro: Integer; MovimID: TEstqMovimID;
              MovimNiv: TEstqMovimNiv;
              Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2, AreaP2,
              ValorT: Double; _Data_Hora_: String; SrcMovID: TEstqMovimID;
              SrcNivel1, SrcNivel2: Integer; Observ: String;
              LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,
              Misturou*): Integer; CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
              DstMovID: TEstqMovimID; DstNivel1, DstNivel2: Integer;
              QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
              AptoUso, FornecMO, SerieFch: Integer; NotaMPAG: Double;
              SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto, PedItsLib,
              PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
              GSPSrcNiv2, ReqMovEstq,
              StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
              QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, PerceComiss,
              CusKgComiss, CustoComiss, CredPereImposto, CredValrImposto,
              CusFrtAvuls: Double; GGXRcl: Integer;
              RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI: Double;
              JmpMovID: TEstqMovimID; JmpNivel1, JmpNivel2, JmpGGX: Integer;
              RmsMovID: TEstqMovimID; RmsNivel1, RmsNivel2, RmsGGX: Integer;
              GSPJmpMovID: TEstqMovimID; GSPJmpNiv2, MovCodPai, VmiPai: Integer;
              IxxMovIX: TEstqMovInfo; IxxFolha, IxxLinha: Integer;
              ExigeClientMO, ExigeFornecMO, ExigeStqLoc: Boolean;
              //
              MedEClas: Integer;
              //
              InsUpdVMIPrcExecID: TInsUpdVMIPrcExecID): Boolean;
    function  InsUpdVS_VMI(SQLType: TSQLType; Codigo, MovimCod, MovimTwn,
              Empresa, Terceiro: Integer; MovimID: TEstqMovimID;
              MovimNiv: TEstqMovimNiv;
              Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2, AreaP2,
              ValorT: Double; _Data_Hora_: String; SrcMovID: TEstqMovimID;
              SrcNivel1, SrcNivel2: Integer; Observ: String;
              LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,
              Misturou*): Integer; CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
              DstMovID: TEstqMovimID; DstNivel1, DstNivel2: Integer;
              QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
              AptoUso, FornecMO, SerieFch: Integer; NotaMPAG: Double;
              SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto, PedItsLib,
              PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
              GSPSrcNiv2, ReqMovEstq,
              //StqCenLoc, ItemNFe, VSMulFrnCab: Integer; ClientMO: Integer = -11): Boolean;
              StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
              QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, PerceComiss,
              CusKgComiss, CustoComiss, CredPereImposto, CredValrImposto,
              CusFrtAvuls: Double;
              //
              GGXRcl: Integer;
              //
              RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI: Double;
              //
              JmpMovID: TEstqMovimID; JmpNivel1, JmpNivel2, JmpGGX: Integer;
              RmsMovID: TEstqMovimID; RmsNivel1, RmsNivel2, RmsGGX: Integer;
              GSPJmpMovID: TEstqMovimID; GSPJmpNiv2, MovCodPai, VmiPai: Integer;
              IxxMovIX: TEstqMovInfo; IxxFolha, IxxLinha: Integer;
              ExigeClientMO, ExigeFornecMO, ExigeStqLoc: Boolean;
              //
              MedEClas: Integer;
              //
              InsUpdVMIPrcExecID: TInsUpdVMIPrcExecID): Boolean;
    procedure LimpaArray15Int(var Arr: TClass15Int);
    function  LocalizaPorCampo(EstqMovimID: TEstqMovimID; Caption, Campo:
              String): Integer;
    function  LocalizaPeloIMEC(EstqMovimID: TEstqMovimID): Integer;
    function  LocalizaPeloIMEI(EstqMovimID: TEstqMovimID): Integer;
    function  LocalizaPelaOC(EstqMovimID: TEstqMovimID): Integer;
    //
    //
    procedure MostraFormStqCenCad(StqCenCad: Integer; StqCenLoc: Integer = 0);
    procedure MostraFormVS_Do_IMEI(Controle: Integer);
    procedure MostraFormVS_Do_IMEC(MovimCod: Integer; Pergunta: String);
    procedure MostraFormVSClaArtPrpMDz(Pallet1, Pallet2, Pallet3, Pallet4, Pallet5,
              Pallet6: Integer; FormAFechar: TForm; IMEI, DV, StqCenLoc: Integer);
    procedure MostraFormVSClaArtPrpQnz(Pallets: array of Integer; FormAFechar:
              TForm; IMEI, DV, StqCenLoc: Integer);
{?:><
    procedure MostraFormVSClaPalPrpQnz(Pallets: array of Integer; FormAFechar:
              TForm; IMEI, DV: Integer);
}
    procedure MostraFormVSClaArtSel_FRMP();
    procedure MostraFormVSClaArtSel_IMEI();
    procedure MostraFormVSClassifOne(Codigo, CacCod: Integer;
              MovimID: TEstqMovimID; FormPreDef: Integer = 0);
    procedure MostraFormVSClassifOneRetIMEI_06(Pallet1, Pallet2, Pallet3, Pallet4,
              Pallet5, Pallet6: Integer; Form: TForm; BoxMax: Integer);
    procedure MostraFormVSClassifOneRetIMEI_15(
              Pallet01, Pallet02, Pallet03, Pallet04, Pallet05,
              Pallet06, Pallet07, Pallet08, Pallet09, Pallet10,
              Pallet11, Pallet12, Pallet13, Pallet14, Pallet15: Integer;
              Form: TForm; BoxMax: Integer);
    procedure MostraFormVSCOPCab(Codigo: Integer);
    procedure MostraFormVSDivCouMeio(MovimID: TEstqMovimID;
              Boxes: array of Boolean; LaTipo_Caption: String);
    procedure MostraFormVSGeraArt(Codigo, Controle: Integer);
    procedure MostraFormVSGerClaCab(Codigo: Integer);
    procedure MostraFormVSGerRclCab(Codigo, Controle: Integer);
    procedure MostraFormVSGruGGX(SQLType: TSQLType; Codigo, Bastidao, GGXAtu:
              Integer);
    procedure MostraFormVSInnCab(Codigo, VSInnIts, VSReclas: Integer);
    procedure MostraFormVSMovimID(MovimID: TEstqMovimID);
    procedure MostraFormVSMovImp(AbrirEmAba: Boolean; InOwner: TWincontrol;
              PageControl: TdmkPageControl; PageIndex: Integer); overload;
    procedure MostraFormVSMovIts(Controle: Integer);
    procedure MostraFormVSMovItsAlt(Controle: Integer;
              AtualizaSaldoModoGenerico: Boolean;
              GroupBoxes: array of TEstqEditGB);
    procedure MostraFormVSOpeCab(Codigo: Integer);
    procedure MostraFormVSNFeOutIts(SQLType: TSQLType; QrVSOutNfeIts,
              QrTribIncIts: TmySQLQuery; Codigo, MovimCod, Empresa: Integer;
              DataHora: TDateTime; SinalOrig: TSinal; DefMulNFe:
              TEstqDefMulFldEMxx; MovTypeCod: Integer; MovimID: TEstqMovimID;
              MovimNivsOri, MovimNivsDst: array of TEstqMovimNiv);
    function  MostraFormVSNFeOutCab(SQLType: TSQLType; QrVSOutNfeCab:
              TmySQLQuery; MovimCod, OriCod, Codigo, ide_serie, ide_nNF:
              Integer): Boolean;
    procedure MostraFormVSOutCab(Codigo, Controle: Integer);
    procedure MostraFormVSOutPeso(SQLType: TSQLType; QrVSOutCab: TmySQLQuery;
              DsVSOutCab: TDataSource; QrVSOutIts: TmySQLQuery; DataHora:
              TDateTime; Quanto: TPartOuTodo; Pedido: Integer);
    procedure MostraFormVSPallet(Codigo: Integer);
    procedure MostraFormVSPallet1(Codigo: Integer);
    procedure MostraFormVSPaMulCabR(Codigo: Integer);
    procedure MostraFormVSPlCCab(Codigo, Controle: Integer);
    procedure MostraFormVSPrePalCab(Codigo: Integer);
    procedure MostraFormVSPWECab(Codigo: Integer);
    procedure MostraFormVSRclArtPrpNew(SQLType: TSQLType; Pallet, Pallet1,
              Pallet2, Pallet3, Pallet4, Pallet5, Pallet6, Digitador, Revisor:
              Integer; Reclasse: Boolean; StqCenLoc, FornecMO: Integer);
    procedure MostraFormVSRclArtSel();
    procedure MostraFormVSReclassPrePalCac(SQLType: TSQLType; Pallet1, Pallet2,
              Pallet3, Pallet4, Pallet5, Pallet6, Digitador, Revisor: Integer);
    procedure MostraFormVSReclassifOneNew(Codigo, CacCod, StqCenLoc: Integer;
              MovimID: TEstqMovimID(*; TpAreaRcl: Integer*));
    function  MostraFormVSRMPPsq(const MovimID: TEstqMovimID; const MovimNivs:
              array of TEstqMovimNiv; const SQLType: TSQLType; var Codigo,
              Controle: Integer; OnlySelIMEI: Boolean): Boolean;
    procedure MostraFormVSMixClaCab();
    procedure MostraFormVSSerFch();
    procedure MostraFormVSTrfLocCab(Codigo: Integer);
    procedure MostraFormVS_XXX(MovimID, Codigo, Controle: Integer);

    //
    procedure MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc: TControl; EdStqCenLoc:
              TdmkEditCB; CBStqCenLoc: TdmkDBLookupComboBox; QrStqCenLoc:
              TmySQLQuery; MovimID: TEstqMovimID);
    function  NotaCouroRibeiraApuca(Pecas, Peso, AreaM2, FatorMP, FatorAR: Double):
              Double;
    function  ObtemControleMovimTwin(MovimCod, MovimTwn: Integer; MovimID:
              TEstqMovimID; MovimNiv: TEstqMovimNiv): Integer;
    function  ObtemFatorIntDeGraGruXeVeSeDifere(const GraGruX1, GraGruX2:
              Integer; var FatorInt1, FatorInt2, Fator1para2: Double): Boolean;
    function  ObtemFatorInteiro(const GraGruX: Integer; var FatorIntRes: Integer;
              const Avisa: Boolean; const FatorIntCompara: Integer; MeAviso:
              TMemo): Boolean;
    function  ObtemGraGruY_de_IDTipoCouro(Index: Integer): Integer;
    function  ObtemIDTipoCouro_de_GraGruY(GraGruY: Integer): Integer;
    function  ObtemMovimIDdeFatID(FatID: Integer): Integer;
    function  ObtemMovimCodDeControle(Controle: Integer): Integer;
    function  ObtemMovimCodDeMovimIDECodigo(MovimID: TEstqMovimID; Codigo:
              Integer): Integer;
    function  ObtemMovimNivDstDeMovimID(MovimID: TEstqMovimID): TEstqMovimNiv;
    function  ObtemMovimNivInnDeMovimID(MovimID: TEstqMovimID): TEstqMovimNiv;
    function  ObtemMovimNivSrcDeMovimID(MovimID: TEstqMovimID): TEstqMovimNiv;
    function  ObtemListaOperacoes(): TStringList;
    procedure ObtemNomeCamposNFeVSXxxCab(const MovimID: TEstqMovimID; var
              FldSer, FldNum: String);
    function  ObtemNomeFrxEstoque(Data: String; RG00_Agrupa_ItemIndex,
              RG00_Ordem1_ItemIndex, RG00_Ordem2_ItemIndex, RG00_Ordem3_ItemIndex,
              RG00_Ordem4_ItemIndex, RG00_Ordem5_ItemIndex: Integer;
              CB00StqCenCad_Text: String): String;
    function  ObtemNomeGraGruYDestdeOrig(GraGruY: Integer): Integer;
    function  ObtemNomeDestinoGraGruY(GraGruY: Integer): String;
    function  ObtemNomeTabelaVSXxxCab(MovimID: TEstqMovimID; Avisa: Boolean = True): String;

    function  ObrigaInfoIxx(IxxMovIX: TEstqMovInfo; IxxFolha, IxxLinha:
              Integer): Boolean;
    function  ObtemProximaFichaRMP(const Empresa: Integer; const EdSerieFch:
              TdmkEditCB; var Ficha: Integer; SerieDefinida: Integer = 0): Boolean;
    function  ObtemSaldoFuturoIMEI(const VMIOri: Integer; const VoltaPecas,
              VoltaPesoKg, VoltaAreaM2: Double; var FuturoPecas, FuturoPesoKg,
              FuturoAreaM2, FuturoValorT: Double): Boolean;
    function  OperacaoEhDivisao(MovimID, MovimCod: Integer): Boolean;
    function  PalletDuplicado(MaxBox: Integer;
              p1, p2, p3, p4, p5, p6: Integer): Boolean;
    function  PalletDuplicad3(MaxBox: Integer; Pallets: array of Integer):
              Boolean;
    procedure PesquisaDoubleVS(Campo: String; Valor: Double; TemIMEIMrt:
              Integer);
    function  PesquisaNovosDadosCDR(SQLTypePsq: TSQLType; LstTabs: String;
              PesqDB: TmySQLDataBase; QrItensPorTab, (*QrTabelas,*) QrRegistros:
              TmySQLQuery; PB1: TProgressBar; LaAviso1, LaAviso2: TLabel;
              EnviarTudo: Boolean; ServerID: Integer): Boolean;
    function  PesquisaNovosDadosClaReCo(SQLTypePsq: TSQLType; LstTabs: String;
              PesqDB: TmySQLDataBase; QrItensPorTab, QrTabelas, QrRegistros:
              TmySQLQuery; PB1: TProgressBar; LaAviso1, LaAviso2: TLabel;
              EnviarTudo: Boolean; ServerID: Integer): Boolean;
    function  PesquisaPallets(const Empresa, ClientMO, CouNiv2, CouNiv1,
              StqCenCad, StqCenLoc: Integer; const Tabela: String; const GraGruYs: String;
              const GraGruXs, Pallets: array of Integer; const SQL_Especificos:
              String; const MovimID: TEstqMovimID; const MovimCod: Integer;
              var sVSMovImp4: String): Boolean;
    procedure PreencheDadosDeVSCOPCab(VSCOPCab: Integer;
              EdEmpresa: TdmkEditCB; CBEmpresa: TDmkDBLookupComboBox;
              RGTipoArea: TRadioGroup;
              EdGraGruX: TdmkEditCB; CBGraGruX: TDmkDBLookupComboBox;
              EdGGXDst: TdmkEditCB; CBGGXDst: TDmkDBLookupComboBox;
              EdForneceMO: TdmkEditCB; CBForneceMO: TDmkDBLookupComboBox;
              EdStqCenLoc: TdmkEditCB; CBStqCenLoc: TDmkDBLookupComboBox;
              EdOperacoes: TdmkEditCB; CBOperacoes: TDmkDBLookupComboBox;
              EdCliente: TdmkEditCB; CBCliente: TDmkDBLookupComboBox;
              EdPedItsLib: TdmkEditCB; CBPedItsLib: TDmkDBLookupComboBox;
              EdClienteMO: TdmkEditCB; CBClienteMO: TDmkDBLookupComboBox;
              EdCustoMO, EdVSArtCab: TdmkEdit; CBVSArtCab: TDmkDBLookupComboBox;
              EdLinCulReb, EdLinCabReb, EdLinCulSem, EdLinCabSem: TdmkEdit;
              EdReceiRecu, EdReceiRefu: TDmkEditCB; CBReceiRecu, CBReceiRefu:
              TDmkDBLookupComboBox);
    function  RedefineReduzidoOnPallet(MovimID: TEstqMovimID; MovimNiv:
              TEstqMovimNiv): (*TTipoSinal*) TTipoTrocaGgxVmiPall;
    procedure ReopenGraGruX_Pallet(QrGraGruX: TmySQLQuery);
    procedure ReopenQrySaldoPallet_Visual(Qry: TmySQLQuery; Empresa, Pallet,
              VSMovIts: Integer);
    procedure ReopenPedItsXXX(QrVSPedIts: TmySQLQuery; InsPedIts,
              UpdPedIts: Integer);
    procedure ReopenVSMovIts_Pallet2(QrVSMovIts, QrPallets: TmySQLQuery);
    procedure ReopenVSListaPallets(Qry: TmySQLQuery; Tabela: String;
              Empresa, GraGruX, Pallet, Terceiro: Integer; OrderBy: String);
    procedure ReopenIMEIsPositivos(Qry: TmySQLQuery; Empresa, GraGruX,
              Pallet, Ficha, IMEI, TipoCouro, StqCenCad, StqCenLoc, ImeiSrc,
              CouNiv1, CouNiv2: Integer; DataLimite: TDateTime);
    procedure ReopenVSIts_Controle_If(Qry: TmySQLQuery; Controle, TemIMEIMrt:
              Integer);
(*
    procedure ReopenVSItsBxa(Qry: TmySQLQuery; SrcMovID: TEstqMovimID;
              SrcNivel1, SrcNivel2, TemIMEIMrt: Integer; MovimNiv:
              TEstqMovimNiv; LocCtrl: Integer);
*)
    procedure ReopenVSItsBxa(Qry: TmySQLQuery; SrcMovID: TEstqMovimID;
              SrcNivel1, SrcNivel2, TemIMEIMrt: Integer; MovimNivs:
              array of TEstqMovimNiv; LocCtrl: Integer);
    procedure ReopenVSItsBxa_Medias(Qry: TmySQLQuery; SrcMovID: TEstqMovimID;
              SrcNivel1, SrcNivel2, TemIMEIMrt: Integer; MovimNivs:
              array of TEstqMovimNiv);
    procedure ReopenVSGerArtDst(Qry: TmySQLQuery; MovimCod, Controle,
              TemIMEIMrt: Integer; EstqMovimNiv: TEstqMovimNiv);
    procedure ReopenVSGerArtSrc(Qry: TmySQLQuery; MovimCod, Controle, TemIMEIMrt:
              Integer; SQL_Limit: String; EstqMovimNiv: TEstqMovimNiv);
    procedure ReopenQrySaldoIMEI_Baixa(Qry1, Qry2: TmySQLQuery; SrcNivel2: Integer);
    procedure ReopenPrestadorMO(Qry: TmySQLQuery; SQL_AND: String);
    procedure ReopenStqCenLoc(Qry: TmySQLQuery; Centro: Variant; Controle:
              Integer; MovimID: TEstqMovimID = TEstqMovimID.emidAjuste);
    procedure ReopenVSCOPCab(Query: TmySQLQuery; MovimID: TEstqMovimID);
    procedure ReopenVSESCPalDst_ToClassPorPallet(QrVSESCPalNew: TmySQLQuery;
              Pallet: Integer);
    procedure ReopenVSGerArtDst_ToClassPorFicha(QrVSGerArtNew: TmySQLQuery;
              Ficha, Controle: Integer; SoNaoZerados: Boolean);
    procedure ReopenVSGerArtDst_ToClassPorIMEI(QrVSGerArtNew: TmySQLQuery;
              IMEI: Integer);
    procedure ReopenVSMovXXX(Qry: TmySQLQuery; Campo: String; IMEI, TemIMEIMrt,
              CtrlLoc: Integer);
    procedure ReopenVSOpePrcDst(Qry: TmySQLQuery; MovimCod, Controle, TemIMEIMrt:
              Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String = '');
    procedure ReopenVSPallet(QrVSPallet: TmySQLQuery; Empresa, ClientMO, GraGruX:
              Integer; Ordem: String; PallOnEdit: array of Integer);
    procedure ReopenVSPWEDesclDst(Qry: TmySQLQuery; Codigo, Controle, Pallet,
              TemIMEIMrt: Integer);
    procedure ReopenVSOpePrcBxa(Qry: TmySQLQuery; MovimCod, MovimTwn, Controle,
              TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
    procedure ReopenVSXxxExtra(Qry: TmySQLQuery; Codigo, (*Controle,*) TemIMEIMrt:
              Integer; MovimID, DstMovID: TEstqMovimID; MovimNiv: TEstqMovimNiv);
    procedure ReopenVSXxxOris(QrVSXxxCab, QrVSXxxOriIMEI, QrVSXxxOriPallet:
              TmySQLQuery; Controle, Pallet: Integer; MovimNiv: TEstqMovimNiv);
    procedure ReopenVSMOEnvAvu(QrVSMOEnvAvu: TmySQLQuery; MovimCod, Codigo:
              Integer);
    procedure ReopenVSMOEnvAVMI(QrVSMOEnvAVmi: TmySQLQuery; VSMOEnvAvu, Codigo:
              Integer);
    procedure ReopenVSMOEnvEnv(QrVSMOEnvEnv: TmySQLQuery; MovimCod, Codigo:
              Integer);
    procedure ReopenVSMOEnvRet(QrVSMOEnvRet: TmySQLQuery; MovimCod, Codigo:
              Integer);
    procedure ReopenVSMOEnvEVMI(QrVSMOEnvEVmi: TmySQLQuery; VSMOEnvEnv, Codigo:
              Integer);
    procedure ReopenVSMOEnvGVmi(QrVSMOEnvGVmi: TmySQLQuery; VSMOEnvRet, Codigo:
              Integer);
    procedure ReopenVSMOEnvRVmi(QrVSMOEnvRVmi: TmySQLQuery; VSMOEnvRet, Codigo:
              Integer);
    procedure ReopenItensAptos_Cal(Qry: TmySQLQuery; Empresa, StqCenCad,
              Terceiro,ClientMO, GraGruX: Integer; ModoBxaEstq: TModoBxaEstq);

    function  SenhaVSPwdDdNaoConfere(Digitado: String): Boolean;
    function  SQL_TipoEstq_DefinirCodi(CAST: Boolean; NomeFld: String; UsaVirgula:
              Boolean; FldQtdArM2, FldQtdPeso, FldQtdPeca: String;
              Prefacio: String = ''): String;
    //function  SQL_AW_Filto(QuaisItensQuer: TQuaisItensQuer; Alias1: String): String;
    function  SQL_NO_GGX(): String;
    function  SQL_NO_GGX_DST(): String;
    function  SQL_NO_FRN(): String;
    function  SQL_NO_CMO(): String;
    function  SQL_NO_SCL(): String;
    function  SQL_LJ_GGX(): String;
    function  SQL_LJ_GGX_DST(Alias: String): String;
    function  SQL_LJ_FRN(): String;
    function  SQL_LJ_CMO(): String;
    function  SQL_LJ_SCL(): String;
    function  SQL_MovIDeNiv_Pos_Inn(): String;
    function  TabCacVS_Tab(Tab: TTabToWork): String;
    function  TabMovVS_Fld_IMEI(Tab: TTabToWork): String;
    function  TabMovVS_Fld_Pall(Tab: TTabToWork): String;
    function  TabMovVS_Tab(Tab: TTabToWork): String;
    function  ValidaCampoNF(Operacao: Integer; EditNF: TdmkEdit; MostraMsg:
              Boolean): Boolean;
    function  VerificaOpeSemOperacao(): Boolean;
    function  VSFic(GraGruX, Empresa, ClienteMO, Fornecedor, Pallet,
              Ficha: Integer; Pecas, AreaM2, PesoKg, ValorT: Double; EdGraGruX,
              EdPallet, EdFicha, EdPecas, EdAreaM2, EdPesoKg, EdValorT: TWinControl;
              ExigeFornecedor: Boolean; GraGruY: Integer; ExigeAreaouPeca:
              Boolean; EdStqCenLoc: TWinControl): Boolean;
    function  ZeraEstoquePalletOrigemReclas(Pallet, Codigo, MovimCod, Empresa:
              Integer; BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2, BxaValorT:
              Double; DataEHora: TDateTime; Iuvpei: TInsUpdVMIPrcExecID): Boolean;
    procedure ZeraSaldoIMEI(Controle: Integer);
    function  CalculoValorOrigemM2(AreaM2, CustoPQ, ValorMP, CusFrtMOEnv:
              Double): Double;
    function  DesenhaSimboloDefeito_Image(var Image: TImage; const X, Y,
              Simbolo, CorBorda, CorMiolo: Integer): Double;
    function  DesenhaSimboloDefeito_Picture(Picture: TPicture; const X, Y,
              Simbolo, CorBorda, CorMiolo: Integer): Double;
    procedure ReopenItensPedido(Qr: TmySQLQuery; Pedido: Integer);
    procedure ExcluiVSOutFat(Controle: Integer);
    procedure AtualizaAreaOrigemSemAreaPreDefinida(SrcNivel2: Integer);
  end;

var
  VS_CRC_PF: TUnVS_CRC_PF;
const
  VsOperaTip_Max = 11;
  VsOperaTip_Str: array[0..VsOperaTip_Max] of string =
  (
  'Saída de Wet Blue do Estoque',
  'Transferência de Local de Estoque VS',
  'Artigo de Ribeira em Operação',
  'Classificação de Artigo de Ribeira',
  'Processo de Semi Acabado',
  'Entrada de Peles In Natura',
  'Processamento de Subproduto',
  'Artigo em Processo de Caleiro',
  'Artigo em Processo de Curtimento',
  'Reproceso / Reparo de Material',
  'Entrada de Artigo Classificado Por Compra',
  'Entrada de Artigo Por Devolução'
  );

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, CfgCadLista,
  UMySQLDB, UMySQLModule, UnGOTOy,
  VSMod, VSImpPallet, CreateVS, VSPallet, GetValor, UnGrade_PF,

  //{$IFNDEF sAllVS}
  VSESCCab,
  //{$IFNDEF sAllVS}
  {$IFDEF sAllVS}
  UnVS_PF, UnEfdIcmsIpi_PF, VSMovImp, VSOutNfeCab, VSOutNfeIts, VSGruGGX,
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$ELSE}
  UnCRC_PF,
  {$ENDIF}
  StqCenCad, VSMovimID,
  ModVS_CRC, VSMovIts, VSImpClaIMEIs, VSImpPackList, VSMovItsAlt,
  VSRclArtPrpNew, VSHide, VSPalletManual, VSPalletAdd, VSGerArtCab,
  VSReclassifOneNw3, VSRMPPsq, VSImpClaIMEI, VSImpIMEI, VSImpRecla,
  VSDivCouMeio, VSReclassPrePal, VSRclArtSel, VSGerRclCab, VSPrePalCab,
  VSClaArtPrpQnz, VSClassifOneNw3, VSClassifOneRetIMEI, VSClaArtPrpMDz,
  VSClassifOneNew, VSClaArtSelA, VSClaArtSelB, {?:><VSClaPalPrpQnz,}
  VSGerClaCab, VSPaMulCabR, VSInnCab, VSCPMRSBER, VSImpEstoque, VSOutCab,
  VSCOPCab, VSOutPeso, VSPlCCab, VSOpeCab, VSImpOrdem, VSReqMovEstq, VSIxx,
  VSPWECab, VSTrfLocCab, VSImpOSFluxo;

{ TUnVS_CRC_PF }

procedure TUnVS_CRC_PF.AbreGraGruXY(Qry: TmySQLQuery; _AND: String);
begin
  Grade_PF.AbreGraGruXY(Qry, _AND);
end;

procedure TUnVS_CRC_PF.AbreVSSerFch(QrVSSerFch: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSSerFch, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM vsserfch ',
  'WHERE Ativo=1 ',
  'ORDER BY Nome ',
  '']);
end;

function TUnVS_CRC_PF.AdicionarNovosVS_emid: Boolean;
const
////////////////////////////////////////////////////////////////////////////////
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  Procedure criada somente para marcar lugares para implementar codigo
///  quando criar mais TEstqMovimID.emid....
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
////////////////////////////////////////////////////////////////////////////////
  sEstqMovimID: array[0..MaxEstqMovimID] of string = (
    '00', // 00
    '01', // 01
    '02', // 02
    '03', // 03
    '04', // 04
    '05', // 05
    '06', // 06
    '07', // 07
    '08', // 08
    '09', // 09
    '10', // 10
    '11', // 11
    '12', // 12
    '13', // 13
    '14', // 14
    '15', // 15
    '16', // 16
    '17', // 17
    '18', // 18
    '19', // 19
    '20', // 20
    '21', // 21
    '22', // 22
    '23', // 23
    '24', // 24
    '25', // 25
    '26', // 26
    '27', // 27
    '28', // 28
    '29', // 29
    '30', // 30
    '31', // 31
    '32', // 32
    '33', // 33
    '34', // 34
    '35', // 35
    '36', // 36
    '37', // 37
    '38', // 38
    '39', // 39
    '40', // 40
    '41' // 41
////////////////////////////////////////////////////////////////////////////////
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  Procedure criada somente para marcar lugares para implementar codigo
///  quando criar mais TEstqMovimID.emid....
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
///  A T E N C A O ! ! ! ! !
////////////////////////////////////////////////////////////////////////////////
  );
begin
  Result := True;
end;

function TUnVS_CRC_PF.AlteraVMI_AreaM2(MovimID, MovimNiv, Controle: Integer;
  Default: Double; PermiteNegativo: TSinal): Boolean;
var
  ResVar: Variant;
  AreaM2, AreaP2: Double;
begin
  AreaM2 := 0;
  Result := False;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  Default, 2, 0, '', '', True, 'Área', 'Informe a área em m²: ',
  0, ResVar) then
  begin
    AreaM2 := Geral.DMV(ResVar);
    if (AreaM2 >= 0) or (PermiteNegativo = siNegativo) then
    begin
      AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'AreaM2', 'AreaP2'], [
      'Controle'], [
      AreaM2, AreaP2], [
      Controle], True);
      if Result then
        AtualizaSaldoVirtualVSMovIts_Generico(Controle, MovimID, MovimNiv);
    end else
      Geral.MB_Aviso('Valor inválido! Não pode ser negativo!');
  end;
end;

function TUnVS_CRC_PF.AlteraVMI_CliVenda(Controle, Atual: Integer): Boolean;
const
  MostraDados = False;
var
  CliVenda: Integer;
begin
  CliVenda := Atual;
  if Entities.SelecionaEntidade(MostraDados, Geral.ATS([
  'SELECT ent.Codigo, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Descricao ',
  'FROM entidades ent ',
  'WHERE ent.Cliente1="V" ',
  'ORDER BY Descricao ']), CliVenda) then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'CliVenda'], [
    'Controle'], [
    CliVenda], [
    Controle], True);
  end;
end;

function TUnVS_CRC_PF.AlteraVMI_FornecMO(Controle, Atual: Integer): Boolean;
const
  MostraDados = False;
var
  FornecMO: Integer;
begin
  FornecMO := Atual;
  if Entities.SelecionaEntidade(MostraDados, Geral.ATS([
  'SELECT ent.Codigo, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Descricao ',
  'FROM entidades ent ',
  'WHERE ent.Fornece7="V" ',
  'ORDER BY Descricao ']), FornecMO) then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'FornecMO'], [
    'Controle'], [
    FornecMO], [
    Controle], True);
  end;
end;

function TUnVS_CRC_PF.AlteraVMI_PesoKg(CampoPeso: String; MovimID, MovimNiv,
  Controle: Integer; Default: Double; PermiteNegativo: TSinal): Boolean;
var
  ResVar: Variant;
  Qtde: Double;
begin
  Qtde := 0;
  Result := False;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  Default, 3, 0, '', '', True, 'Quantidade', 'Informe a quantidade em kg: ',
  0, ResVar) then
  begin
    Qtde := Geral.DMV(ResVar);
    if (Qtde >= 0) or (PermiteNegativo = siNegativo) then
    begin
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      CampoPeso], [
      'Controle'], [
      Qtde], [
      Controle], True);
      if Result then
        VS_CRC_PF.AtualizaSaldoVirtualVSMovIts_Generico(Controle, MovimID, MovimNiv);
    end else
      Geral.MB_Aviso('Valor inválido! Não pode ser negativo!');
  end;
end;

function TUnVS_CRC_PF.AlteraVMI_StqCenLoc(Controle, Atual: Integer): Boolean;
const
  MostraDados = False;
var
  StqCenLoc: Integer;
  //
  function SelecionaStqCenLoc(): Boolean;
  const
    Aviso  = '...';
    Titulo = 'Seleção de Local de estoque';
    Prompt = 'Informe o local: [F7 para pesquisar]';
    Campo  = 'Descricao';
  var
    Codigo: Variant;
  begin
    Result := False;
    Codigo :=
      DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, Atual,
      ['SELECT scl.Controle Codigo, ',
      'CONCAT(scl.Nome, " (", scc.Nome, ")") Descricao ',
      'FROM stqcenloc scl ',
      'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
      'ORDER BY Descricao '], Dmod.MyDB, True);
    if Codigo <> Null then
    begin
      StqCenLoc := Codigo;
      Result    := True;
      //
    end;
  end;
begin
  if SelecionaStqCenLoc() then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'StqCenLoc'], [
    'Controle'], [
    StqCenLoc], [
    Controle], True);
  end;
end;

function TUnVS_CRC_PF.AreaEstaNaMedia(Pecas, AreaM2, MediaMinM2,
  MediaMaxM2: Double; Pergunta: Boolean): Boolean;
var
  Media: Double;
begin
  Result := False;
  if Pecas = 0 then
    Exit;
  Media := AreaM2 / Pecas;
  Result := (Media >= MediaMinM2) and (Media <= MediaMaxM2);
  if not Result then
    Geral.MB_Aviso(
    'Média área = ' + Geral.FFT(Media, 2, siNegativo) + ' m²/peça'
    + sLineBreak +
    'Média mínima: ' + Geral.FFT(MediaMinM2, 2, siNegativo) + ' m²/peça'
    + sLineBreak +
    'Média máxima: ' + Geral.FFT(MediaMaxM2, 2, siNegativo) + ' m²/peça');
  if Pergunta and not Result then
    Result := Geral.MB_Pergunta(
    'A média de área do artigo gerado está fora do esperado!' + sLineBreak +
    'Deseja continuar assim mesmo?') = ID_YES
  else
    Result := True;
end;

procedure TUnVS_CRC_PF.AtualizaAreaOrigemSemAreaPreDefinida(SrcNivel2: Integer);
var
  MedEClas, MovimCod, DstGGX, SrcGGX, Contrl13, Contrl14, Contrl15, MovimID,
  MovimNiv, MovimTwn: Integer;
  TotPecas, TotAreaM2, TotAreaP2, MediaM2, MediaP2, QtdAntPeso,
  QtdGerArM2, QtdGerArP2, Pecas, PesoKg: Double;
  FatorMP, FatorAR, NotaMPAG, FatNotaVNC, FatNotaVRC: Double;
  SumNotaMPAG, SumFatNotaVNC, SumFatNotaVRC: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DmModVS_CRC.Qr06, Dmod.MyDB, [
  'SELECT MovimID, MovimNiv, MedEClas, MovimCod, QtdAntPeso',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE Controle=' + Geral.FF0(SrcNivel2),
  '']);
  MovimID    := DmModVS_CRC.Qr06MovimID.Value;
  MovimNiv   := DmModVS_CRC.Qr06MovimNiv.Value;
  MedEClas   := DmModVS_CRC.Qr06MedEClas.Value;
  MovimCod   := DmModVS_CRC.Qr06MovimCod.Value;
  QtdAntPeso := DmModVS_CRC.Qr06QtdAntPeso.Value;
  //
  if MedEClas = 1 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS_CRC.Qr6Sum, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
    'SUM(AreaP2) AreaP2 ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE SrcNivel2=' + Geral.FF0(SrcNivel2),
    '']);
    //
    TotPecas := -DmModVS_CRC.Qr6SumPecas.Value;
    TotAreaM2 := -DmModVS_CRC.Qr6SumAreaM2.Value;
    TotAreaP2 := -DmModVS_CRC.Qr6SumAreaP2.Value;
    //
    if TotPecas = 0 then
    begin
      MediaM2 := 0;
      MediaP2 := 0;
    end else
    begin
      MediaM2 := TotAreaM2 / TotPecas;
      MediaP2 := TotAreaP2 / TotPecas;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DmModVS_CRC.Qr13, Dmod.MyDB, [
    'SELECT Controle, GraGruX ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=13 ',
    '']);
    DstGGX   := DmModVS_CRC.Qr13GraGruX.Value;
    Contrl13 := DmModVS_CRC.Qr13Controle.Value;  // igual ao SrcNivel2!!
    //
    if Contrl13 <> 0 then
    begin
      SumNotaMPAG   := 0.000000;;
      SumFatNotaVNC := 0.000000;;
      SumFatNotaVRC := 0.000000;;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(DmModVS_CRC.Qr14, Dmod.MyDB, [
      'SELECT Controle, MovimTwn ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=14',
      '']);
      DmModVS_CRC.Qr14.First;
      while not DmModVS_CRC.Qr14.Eof do
      begin
        Contrl14  := DmModVS_CRC.Qr14Controle.Value;
        MovimTwn  := DmModVS_CRC.Qr14MovimTwn.Value;
        //
        //pegar o 15 do Twn e atualiza-lo
        UnDmkDAC_PF.AbreMySQLQuery0(DmModVS_CRC.Qr15, Dmod.MyDB, [
        'SELECT Controle, GraGruX, ',
        'Pecas, PesoKg ', //, QtdGerArM2 ',
        'FROM ' + CO_SEL_TAB_VMI,
        'WHERE MovimID=6 ',
        'AND MovimNiv=15 ',
        'AND MovimTwn=' + Geral.FF0(MovimTwn),
        '']);
        if DmModVS_CRC.Qr15.RecordCount > 0 then
        begin
          Pecas       := -DmModVS_CRC.Qr15Pecas.Value;
          PesoKg      := -DmModVS_CRC.Qr15PesoKg.Value;
          //QtdGerArM2  := DmModVS_CRC.QtdGerArM2.Value;
          QtdGerArM2  := MediaM2 * Pecas;
          QtdGerArP2  := Geral.ConverteArea(QtdGerArM2, ctM2ToP2, cfQuarto);
          SrcGGX      := DmModVS_CRC.Qr15GraGruX.Value;
          //DstGGX      := Qry2.FieldByName('DstGGX.Value;
          FatNotaVNC  := FatorNotaMP(SrcGGX);
          //
          FatNotaVRC  := FatorNotaAR(DstGGX);
          NotaMPAG    := NotaCouroRibeiraApuca(
            Pecas, PesoKg, QtdGerArM2, FatNotaVNC, FatNotaVRC);
          AtualizaNotaMPAG_e_QtdGerArM2eP2(Contrl14, NotaMPAG, FatNotaVNC,
            FatNotaVRC, QtdGerArM2, QtdGerArP2);
          //
          Contrl15     :=  DmModVS_CRC.Qr15Controle.Value;
          AtualizaNotaMPAG_e_QtdGerArM2eP2(Contrl15, NotaMPAG, FatNotaVNC,
            FatNotaVRC, -QtdGerArM2, -QtdGerArP2);
          //
          SumNotaMPAG   := SumNotaMPAG   + (NotaMPAG * QtdGerArM2);
          SumFatNotaVNC := SumFatNotaVNC + (FatNotaVNC * QtdGerArM2);
          SumFatNotaVRC := SumFatNotaVRC + (FatNotaVRC * QtdGerArM2);
        end;
        DmModVS_CRC.Qr14.Next;
      end;
      if TotAreaM2 <> 0 then
      begin
        NotaMPAG   := SumNotaMPAG   / TotAreaM2;
        FatNotaVNC := SumFatNotaVNC / TotAreaM2;
        FatNotaVRC := SumFatNotaVRC / TotAreaM2;
      end else
      begin
        NotaMPAG   := 0.000000;
        FatNotaVNC := 0.000000;
        FatNotaVRC := 0.000000;
      end;
      TotAreaP2 := Geral.ConverteArea(TotAreaM2, ctM2toP2, cfQuarto);
      AtualizaNotaMPAG_e_AreaM2eP2(Contrl13, NotaMPAG, FatNotaVNC,
        FatNotaVRC, TotAreaM2, TotAreaP2);
    end;
  end;
  //
  AtualizaSaldoVirtualVSMovIts_Generico(SrcNivel2, MovimID, MovimNiv);
end;

procedure TUnVS_CRC_PF.AtualizaCustoPQ(Controle: Integer; CustoPQ: Double);
begin
  EXIT;
  /////////////////////////////
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  ' UPDATE vsmovits SET CustoPQ=' + Geral.FFT_Dot(CustoPQ, 6, siNegativo) +
  ' WHERE Controle=' + Geral.FF0(Controle));
end;

procedure TUnVS_CRC_PF.AtualizaCustosDescendentesGerArtFicha(Empresa, SerieFch,
  Ficha: Integer);
var
  MyCursor: TCursor;
  Qry: TmySQLQuery;
begin
  if Ficha = 0 then Exit;
  //
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.myDB, [
      'SELECT * ',
      'FROM ' + CO_SEL_TAB_VMI + '  ',
      'WHERE SerieFch=' + Geral.FF0(SerieFch),
      'AND Ficha=' + Geral.FF0(Ficha),
      'AND MovimID NOT IN (' +
        Geral.FF0(Integer(TEstqMovimID.emidCompra)) + ',' +
        Geral.FF0(Integer(TEstqMovimID.emidIndsXX)) + ')  ',
      '']);
      if Qry.RecordCount > 0 then
      begin
        if DBCheck.CriaFm(TDfVSMod, DfVSMod, afmoSemVerificar) then
        begin
          DfVSMod.Show;
          DfVSMod.AtualizaCustosDescendentesGerArtFicha(Empresa, SerieFch, Ficha, Qry);
          //
          DfVSMod.Destroy;
        end;
      end;
    finally
      Qry.free;
    end;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaDescendentes(IMEI: Integer;
  Campos: array of String; Valores: array of Variant);
var
  Qry: TmySQLQuery;
  Controle, K: Integer;
  Itens: array of Integer;
  //
  function  NaoEstah(Ctrl: Integer): Boolean;
  var
    I: Integer;
  begin
    Result := True;
    for I := Low(Itens) to High(Itens) do
    begin
      if Itens[I] = Ctrl then
      begin
        Result := False;
        Exit;
      end;
    end;
  end;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT Controle ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE SrcNivel2=' + Geral.FF0(IMEI),
    '']);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      Controle := Qry.FieldByName('Controle').AsInteger;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False,
      //UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB,
      Campos, ['Controle'], Valores, [Controle], True);
      //
      Qry.Next;
    end;
    //
    Qry.First;
    while not Qry.Eof do
    begin
      Controle := Qry.FieldByName('Controle').AsInteger;
      if NaoEstah(Controle) then
      begin
        AtualizaDescendentes(Controle, Campos, Valores);
        //
        K := Length(Itens);
        SetLength(Itens, K + 1);
        Itens[K] := Controle;
      end;
      Qry.Next;
    end;
    //
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaDtHrFimOpe_MovimCod(MovimID: TEstqMovimID;
  MovimCod: Integer);
var
  Tabela, DtHrFimOpe: String;
  Qry1, Qry2: TmySQLQuery;
  Codigo: Integer;
  AtzGerado: Boolean;
  DataFimOpe: TDateTime;
  QtdGerMan, QtdGerSrc, QtdGerSdo: Double;
  PecasDst, PesoKgDst, AreaDstM2, AreaDstP2: Double;
  MovimNiv: TEstqMovimNiv;
begin
  AdicionarNovosVS_emid();
  //
  AtzGerado := False;
  Tabela := ObtemNomeTabelaVSXxxCab(MovimID);
  //
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    Qry2 := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
      'SELECT Codigo, DtHrFimOpe, PecasMan, PecasSrc, PecasSdo ',
      'FROM ' + Tabela,
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      //'AND IF(PecasMan<>0, PecasMan, -PecasSrc) <> 0 ',
      //'AND PecasSdo <= 0 ',
      '']);
      Codigo     := Qry1.FieldByName('Codigo').AsInteger;
      DataFimOpe := Qry1.FieldByName('DtHrFimOpe').AsDateTime;
      QtdGerMan  := Qry1.FieldByName('PecasMan').AsInteger;
      QtdGerSrc  := Qry1.FieldByName('PecasSrc').AsInteger;
      QtdGerSdo  := Qry1.FieldByName('PecasSdo').AsInteger;
      //
      // Se for subproduto, quantidades de estoque são em kg e não em peças!
      if ((QtdGerSdo = 0) and (QtdGerMan = 0) and (QtdGerSrc = 0))
      and (MovimID = TEstqMovimID.emidEmProcSP) then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
        'SELECT Codigo, DtHrFimOpe, PesoKgMan, PesoKgSrc, PesoKgSdo ',
        'FROM ' + Tabela,
        'WHERE MovimCod=' + Geral.FF0(MovimCod),
        //'AND IF(PecasMan<>0, PecasMan, -PecasSrc) <> 0 ',
        //'AND PecasSdo <= 0 ',
        '']);
        Codigo     := Qry1.FieldByName('Codigo').AsInteger;
        DataFimOpe := Qry1.FieldByName('DtHrFimOpe').AsDateTime;
        QtdGerMan  := Qry1.FieldByName('PesoKgMan').AsInteger;
        QtdGerSrc  := Qry1.FieldByName('PesoKgSrc').AsInteger;
        QtdGerSdo  := Qry1.FieldByName('PesoKgSdo').AsInteger;
        //
      end;
      // Fim apendice subproduto.
      //
      if (QtdGerSdo <= 0) and ((QtdGerMan <> 0) or (QtdGerSrc <> 0))
      then
      begin
        case MovimID of
          TEstqMovimID.emidEmOperacao,
          TEstqMovimID.emidEmProcWE,
          TEstqMovimID.emidEmProcSP,
          TEstqMovimID.emidEmReprRM,
          TEstqMovimID.emidEmProcCon:
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
            'SELECT MAX(DataHora) DataHora  ',
            'FROM ' + CO_SEL_TAB_VMI + ' ',
            'WHERE MovimCod=' + Geral.FF0(MovimCod),
            '']);
            DtHrFimOpe := Geral.FDT(Qry2.FieldByName(CO_DATA_HORA_VMI).AsDateTime, 109);
          end;
          TEstqMovimID.emidEmProcCur,
          TEstqMovimID.emidEmProcCal:
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
            'SELECT MAX(DataHora) DataHora  ',
            'FROM ' + CO_SEL_TAB_VMI + ' ',
            'WHERE SrcMovID=' + Geral.FF0(Integer(MovimID)),
            'AND SrcNivel1=' + Geral.FF0(Codigo),
            '']);
            DtHrFimOpe := Geral.FDT(Qry2.FieldByName(CO_DATA_HORA_VMI).AsDateTime, 109);
          end;
          else
          begin
            DtHrFimOpe := '0000-00-00';
            Geral.MB_Erro(
              'MovimID não implementado em soma de "AtualizaDtHrFimOpe_MovimCod() (1)"');
          end;
        end;
      end else
        DtHrFimOpe := '0000-00-00';
      //
      PecasDst  := 0;
      PesoKgDst := 0;
      AreaDstM2 := 0;
      AreaDstP2 := 0;
      //
      case MovimID of
        TEstqMovimID.emidEmOperacao,
        TEstqMovimID.emidEmProcWE,
        TEstqMovimID.emidEmProcSP,
        TEstqMovimID.emidEmReprRM:
        begin
          case MovimID of
            TEstqMovimID.emidEmOperacao: MovimNiv := eminDestOper;
            TEstqMovimID.emidEmProcWE:   MovimNiv := eminDestWEnd;
            TEstqMovimID.emidEmProcSP:   MovimNiv := eminDestPSP;
            TEstqMovimID.emidEmReprRM:   MovimNiv := eminDestRRM;
            else MovimNiv := eminSemNiv;
          end;
          UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
          'SELECT  SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
          'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2 ',
          'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
          'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
          'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
          '']);
          Qry2.First;
          while not Qry2.Eof do
          begin
            PecasDst  := PecasDst  + Qry2.FieldByName('Pecas').AsFloat;
            PesoKgDst := PesoKgDst + Qry2.FieldByName('PesoKg').AsFloat;
            AreaDstM2 := AreaDstM2 + Qry2.FieldByName('AreaM2').AsFloat;
            AreaDstP2 := AreaDstP2 + Qry2.FieldByName('AreaP2').AsFloat;
            //
            Qry2.Next;
          end;
          AtzGerado := True;
        end;
        TEstqMovimID.emidEmProcCal,
        TEstqMovimID.emidEmProcCur,
        TEstqMovimID.emidEmProcCon:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
          'SELECT vmi.Controle, vmi.GraGruX, ggx.GraGruY,  ',
          'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
          'vmi.QtdAntPeca, vmi.QtdAntPeso, vmi.QtdAntArM2, ',
          'vmi.QtdGerPeca, vmi.QtdGerPeso, vmi.QtdGerArM2, ',
          //
          SQL_TipoEstq_DefinirCodi(False, 'TipoEstq', True,
          'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
          //
          'vmi.MovimID, vmi.MovimNiv ',
          'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
          'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
          'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
          'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed',
          'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle ',
          'LEFT JOIN couniv1    nv1 ON nv1.Codigo=xco.CouNiv1 ',
          'LEFT JOIN couniv2    nv2 ON nv2.Codigo=xco.CouNiv2 ',
          'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(MovimID)),
          'AND SrcNivel1=' + Geral.FF0(Codigo),
          '']);
          //Geral.MB_SQL(nil, Qry2);
          Qry2.First;
          while not Qry2.Eof do
          begin
            case TEstqMovimID(Qry2.FieldByName('MovimID').AsInteger) of
              emidIndsXX:
              begin
                PecasDst  := PecasDst  - Qry2.FieldByName('Pecas').AsFloat;
                PesoKgDst := PesoKgDst - Qry2.FieldByName('QtdGerPeso').AsFloat;
                AreaDstM2 := AreaDstM2 - Qry2.FieldByName('QtdGerArM2').AsFloat;
                //AreaDstP2 := AreaDstP2 - Qry2.FieldByName('QtdGerArP2').AsFloat;
              end;
              //emidEmProcCal:
              emidCaleado:
              begin
                PecasDst  := PecasDst  + Qry2.FieldByName('Pecas').AsFloat;
                PesoKgDst := PesoKgDst + Qry2.FieldByName('PesoKg').AsFloat;
                AreaDstM2 := AreaDstM2 + 0.000; //Qry2.FieldByName('AreaM2').AsFloat;
              end;
              //emidSPCaleirad:  ??? 2023-12-29
              emidEmProcCur:
              begin
                PecasDst  := PecasDst  + Qry2.FieldByName('Pecas').AsFloat;
                PesoKgDst := PesoKgDst + Qry2.FieldByName('PesoKg').AsFloat;
                AreaDstM2 := AreaDstM2 + Qry2.FieldByName('AreaM2').AsFloat;
              end;
              emidConservado,
              emidExtraBxa,            // 17
              emidForcado:             // 9
              begin
                // nada
              end;
            else Geral.MB_Erro(
              'MovimID não implementado em soma de "AtualizaDtHrFimOpe_MovimCod() (2)"');
            end;
            Qry2.Next;
          end;
          if AreaDstP2 = 0 then
            AreaDstP2 := Geral.ConverteArea(AreaDstM2, ctM2toP2, cfQuarto);
          AtzGerado := True;
        end;
        else
        begin
          Geral.MB_ERRO('MovimID não implementado em "AtualizaDtHrFimOpe_MovimCod()"');
          Exit;
        end;
      end;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
       'PecasDst', 'PesoKgDst', 'AreaDstM2',
       'AreaDstP2', 'DtHrFimOpe'
       ], ['MovimCod'], [
        PecasDst, PesoKgDst, AreaDstM2,
        AreaDstP2, DtHrFimOpe
       ], [MovimCod], True) then
       begin
         if MovimID = TEstqMovimID.emidEmProcCur then
         begin
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscurjmp', False, [
           (*'PecasDst', 'PesoKgDst', 'AreaDstM2',
           'AreaDstP2',*) 'DtHrFimOpe'
           ], ['Codigo'], [
            (*PecasDst, PesoKgDst, AreaDstM2,
            AreaDstP2,*) DtHrFimOpe
           ], [Codigo], True);
         end;
       end;
    finally
      Qry2.Free;
    end;
  finally
    Qry1.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaFornecedorOpe(MovimCod: Integer);
begin
  DmModVS_CRC.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfMovCod, MovimCod,
  [emidEmOperacao], [eminSorcOper], [eminEmOperInn, eminDestOper, eminEmOperBxa]);
end;

procedure TUnVS_CRC_PF.AtualizaFornecedorOut(IMEI: Integer);
begin
  DmModVS_CRC.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfSrcNiv2, IMEI,
  [emidVenda], [], []);
end;

procedure TUnVS_CRC_PF.AtualizaFornecedorPreRcl(MovimCod: Integer);
begin
  DmModVS_CRC.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfMovCod, MovimCod,
  [emidPreReclasse], [eminSorcPreReclas], [eminDestPreReclas]);
end;

procedure TUnVS_CRC_PF.AtualizaFornecedorPWE(MovimCod: Integer);
begin
  DmModVS_CRC.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfMovCod, MovimCod,
  [emidEmProcWE], [eminSorcWEnd], [eminEmWEndInn, eminDestWEnd, eminEmWEndBxa]);
end;

procedure TUnVS_CRC_PF.AtualizaNF_IMEI(MovimCod, IMEI: Integer);
var
  Qry1: TmySQLQuery;
begin
{ Vale a pena?
  Qry1 := TmySQLQuery.Create(Dmod);
  try
    //
  finally
    Qry1.Free;
  end;
}
end;

procedure TUnVS_CRC_PF.AtualizaNotaMPAG(Controle: Integer; NotaMPAG, FatNotaVNC,
  FatNotaVRC: Double);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  'NotaMPAG', 'FatNotaVNC', 'FatNotaVRC'], ['Controle'], [
  NotaMPAG, FatNotaVNC, FatNotaVRC], [Controle], True);
end;

procedure TUnVS_CRC_PF.AtualizaNotaMPAG_e_AreaM2eP2(Controle: Integer; NotaMPAG,
  FatNotaVNC, FatNotaVRC, AreaM2, AreaP2: Double);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  'NotaMPAG', 'FatNotaVNC', 'FatNotaVRC',
  'AreaM2', 'AreaP2'], ['Controle'], [
  NotaMPAG, FatNotaVNC, FatNotaVRC,
  AreaM2, AreaP2], [Controle], True);
end;

procedure TUnVS_CRC_PF.AtualizaNotaMPAG_e_QtdGerArM2eP2(Controle: Integer;
  NotaMPAG, FatNotaVNC, FatNotaVRC, QtdGerArM2, QtdGerArP2: Double);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  'NotaMPAG', 'FatNotaVNC', 'FatNotaVRC',
  'QtdGerArM2', 'QtdGerArP2'], ['Controle'], [
  NotaMPAG, FatNotaVNC, FatNotaVRC,
  QtdGerArM2, QtdGerArP2], [Controle], True);
end;

procedure TUnVS_CRC_PF.AtualizaPalletPelosIMEIsDeGeracao(Pallet: Integer;
  QrSumVMI: TmySQLQuery);
var
  Qry: TmySQLQuery;
  VMI_Dest, VMI_Sorc, (*Pallet,*) Controle: Integer;
  Pecas, AreaM2, AreaP2, SdoVrtPeca, SdoVrtArM2: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    //Pallet := QrVSPalletCodigo.Value;
    //MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando');
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT cia.VMI_Baix, cia.VMI_Dest, cia.VMI_Sorc, ',
      'SUM(cia.Pecas) Pecas',
      'FROM vscacitsa cia',
      //'LEFT JOIN v s m o v i t s vmi ON vmi.Controle=cia.VMI_Baix',
      //'LEFT JOIN v s m o v i t s vmd ON vmd.Controle=cia.VMI_Dest',
      'WHERE cia.VSPallet=' + Geral.FF0(Pallet),
      'GROUP BY cia.VMI_Baix, cia.VMI_Dest, cia.VMI_Sorc',
      '']);
      //
      Qry.First;
      while not Qry.Eof do
      begin
        //MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando item ' +
        //Geral.FF0(Qry.RecNo) + ' de ' + Geral.FF0(Qry.RecordCount));
        //
        VMI_Dest := Qry.FieldByName('VMI_Dest').AsInteger;
        if VMI_Dest <> 0 then
        begin
          //
          UnDmkDAC_PF.AbreMySQLQuery0(QrSumVMI, Dmod.MyDB, [
          'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2 ',
          'FROM vscacitsa ',
          'WHERE VMI_Dest=' + Geral.FF0(VMI_Dest),
          '']);
          Pecas  := QrSumVMI.FieldByName('Pecas').AsFloat;
          AreaM2 := QrSumVMI.FieldByName('AreaM2').AsFloat;
          AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
          SdoVrtPeca := Pecas;
          SdoVrtArM2 := AreaM2;
          Controle := VMI_Dest;
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
          'Pecas', 'AreaM2', 'AreaP2',
          'SdoVrtPeca', 'SdoVrtArM2'], [
          'Controle'], [
          Pecas, AreaM2, AreaP2,
          SdoVrtPeca, SdoVrtArM2], [
          Controle], True) then
          begin
            AtualizaSaldoIMEI(VMI_Dest, False);
            VMI_Sorc := Qry.FieldByName('VMI_Sorc').AsInteger;
            if VMI_Sorc <> 0 then
              AtualizaSaldoIMEI(VMI_Sorc, False);
          end;
        end;
        //
        Qry.Next;
      end;
      //LocCod(Pallet, Pallet);
      //MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    finally
      Qry.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaSaldoIMEI(Controle: Integer; Gera: Boolean);
begin
  VAR_BxaVrtPeca := 0;
  VAR_BxaVrtPeso := 0;
  VAR_BxaVrtArM2 := 0;
  //
  VAR_SdoVrtPeca := 0;
  VAR_SdoVrtPeso := 0;
  VAR_SdoVrtArM2 := 0;
  AtualizaSaldoVirtualVSMovIts(Controle, Gera);
end;

procedure TUnVS_CRC_PF.AtualizaSaldoItmCal(Controle: Integer);
var
  Qry: TmySQLQuery;
var
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
  SQLType: TSQLType;
begin
  Qry := TmySQLQUery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2 ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.SrcNivel2=' + Geral.FF0(Controle),
    '']);
    //
    QtdGerPeca     := -Qry.FieldByName('Pecas').AsFloat;
    QtdGerPeso     := -Qry.FieldByName('PesoKg').AsFloat;
    QtdGerArM2     := -Qry.FieldByName('AreaM2').AsFloat;
    QtdGerArP2     := -Qry.FieldByName('AreaP2').AsFloat;
    //
    SQLType := stUpd;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, CO_UPD_TAB_VMI, False, [
    'QtdGerPeca', 'QtdGerPeso', 'QtdGerArM2',
    'QtdGerArP2'], [
    'Controle'], [
    QtdGerPeca, QtdGerPeso, QtdGerArM2,
    QtdGerArP2], [
    Controle], True) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimCod=' ,
      '( ',
      '  SELECT DISTINCT MovimCod ',
      '  FROM ' + CO_SEL_TAB_VMI + ' ',
      '  WHERE Controle=' + Geral.FF0(Controle),
      ') ',
      'AND MovimNiv=' + Geral.FF0(Integer(eminEmCalInn)),
      '']);
      //
      AtualizaSaldoIMEI(Qry.FieldByName('Controle').AsInteger, True);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaSaldoItmCur(Controle: Integer);
var
  Qry: TmySQLQuery;
var
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
  SQLType: TSQLType;
begin
  Qry := TmySQLQUery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2 ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.SrcNivel2=' + Geral.FF0(Controle),
    '']);
    //
    QtdGerPeca     := -Qry.FieldByName('Pecas').AsFloat;
    QtdGerPeso     := -Qry.FieldByName('PesoKg').AsFloat;
    QtdGerArM2     := -Qry.FieldByName('AreaM2').AsFloat;
    QtdGerArP2     := -Qry.FieldByName('AreaP2').AsFloat;
    //
    SQLType := stUpd;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, CO_UPD_TAB_VMI, False, [
    'QtdGerPeca', 'QtdGerPeso', 'QtdGerArM2',
    'QtdGerArP2'], [
    'Controle'], [
    QtdGerPeca, QtdGerPeso, QtdGerArM2,
    QtdGerArP2], [
    Controle], True) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimCod=' ,
      '( ',
      '  SELECT DISTINCT MovimCod ',
      '  FROM ' + CO_SEL_TAB_VMI + ' ',
      '  WHERE Controle=' + Geral.FF0(Controle),
      ') ',
      'AND MovimNiv=' + Geral.FF0(Integer(eminEmCurInn)),
      '']);
      //
      AtualizaSaldoIMEI(Qry.FieldByName('Controle').AsInteger, True);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaSaldoOrigemVMI_Dest(VMI_Dest, VMI_Sorc,
  VMI_Baix: Integer);
var
  (*VMI_Sorc, Pallet, *)Controle: Integer;
  Pecas, AreaM2, AreaP2, SdoVrtPeca, SdoVrtArM2: Double;
  Qry: TmySQLQUery;
begin
  Qry := TmySQLQUery.Create(Dmod);
  try
    // VMI_Baix
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2 ',
    'FROM vscacitsa ',
    'WHERE VMI_Baix=' + Geral.FF0(VMI_Baix),
    '']);
    Pecas  := -Qry.FieldByName('Pecas').AsFloat;
    AreaM2 := -Qry.FieldByName('AreaM2').AsFloat;
    AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    Controle := VMI_Baix;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'Pecas', 'AreaM2', 'AreaP2'], [
    'Controle'], [
    Pecas, AreaM2, AreaP2], [
    Controle], True);
    //
    // VMI_Dest
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2 ',
    'FROM vscacitsa ',
    'WHERE VMI_Dest=' + Geral.FF0(VMI_Dest),
    '']);
    Pecas  := Qry.FieldByName('Pecas').AsFloat;
    AreaM2 := Qry.FieldByName('AreaM2').AsFloat;
    AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    SdoVrtPeca := Pecas;
    SdoVrtArM2 := AreaM2;
    Controle := VMI_Dest;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'Pecas', 'AreaM2', 'AreaP2',
    'SdoVrtPeca', 'SdoVrtArM2'], [
    'Controle'], [
    Pecas, AreaM2, AreaP2,
    SdoVrtPeca, SdoVrtArM2], [
    Controle], True) then
    begin
      AtualizaSaldoIMEI(VMI_Dest, False);
      //VMI_Sorc := Qry.FieldByName('VMI_Sorc').AsInteger;
      if VMI_Sorc <> 0 then
        AtualizaSaldoIMEI(VMI_Sorc, False);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaSaldoVirtualVSMovIts(Controle: Integer;
  Gera: Boolean);
var
  Qry, Qr2: TmySQLQuery;
  //
var
  InnPecas, InnPesoKg, InnAreaM2, OutPecas, OutPesoKg, OutAreaM2,
  SdoVrtPeso, SdoVrtPeca, SdoVrtArM2,
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
  FatorMP, FatorAR, NotaMPAG: Double;
  MovimID, MovimCod, MovimNiv, Codigo: Integer;
  PcEfetiv, PcClasRcl, FatNotaVNC, FatNotaVRC: Double;
  AvisaErro: Boolean;
begin
  AdicionarNovosVS_emid();
  VAR_BxaVrtPeca := 0;
  VAR_BxaVrtPeso := 0;
  VAR_BxaVrtArM2 := 0;
  //
  VAR_SdoVrtPeca := 0;
  VAR_SdoVrtPeso := 0;
  VAR_SdoVrtArM2 := 0;
  //
  OutPecas       := 0;
  OutPesoKg      := 0;
  OutAreaM2      := 0;
  NotaMPAG       := 0;
  FatorMP        := 1;
  FatorAR        := 1;
  //
  // ver aqui saldo virtual calculando errado para alguns MovimID na baixa forçada!
  // Acho que aqui está o erro de pecas erradas na reclassificação!

  if Controle = 0 then
    Exit;
  //
  Qry := TmySQLQuery.Create(Dmod);
  Qr2 := TmySQLQuery.Create(Dmod);
  try
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.Codigo, vmi.MovimID, vmi.MovimNiv, vmi.MovimCod, ',
    'vmi.GraGruX, vmi.Pecas, vmi.PesoKg, vmi.AreaM2, ',
    'vmi.SdoVrtPeca, vmi.Zerado ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.Controle=' + Geral.FF0(Controle),
    '']);
    //  Craido para resolver problemas de erro antigo do app.
    if Qry.FieldByName('Zerado').AsInteger = 1 then
    begin
      SdoVrtPeca := 0;
      SdoVrtPeso := 0;
      SdoVrtArM2 := 0;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2'], ['Controle'], [
      SdoVrtPeca, SdoVrtPeso, SdoVrtArM2], [Controle], True);
      //
      Exit;
    end;
    //
    InnPecas := Qry.FieldByName('Pecas').AsFloat;
    InnAreaM2 := Qry.FieldByName('AreaM2').AsFloat;
    InnPesoKg := Qry.FieldByName('PesoKg').AsFloat;
    //
    MovimID    := Qry.FieldByName('MovimID').AsInteger;
    MovimNiv   := Qry.FieldByName('MovimNiv').AsInteger;
    MovimCod   := Qry.FieldByName('MovimCod').AsInteger;
    //
    Codigo     := Qry.FieldByName('Codigo').AsInteger;
    //
    SdoVrtPeca := Qry.FieldByName('SdoVrtPeca').AsInteger;
    case TEstqMovimNiv(MovimNiv ) of
      TEstqMovimNiv.eminSorcCurtiXX: AvisaErro := True;
      else AvisaErro := False;
    end;
    if AvisaErro then
    begin
      Geral.MB_Aviso('ERRO! MovimNiv ' + Geral.FF0(MovimNiv) +
      ' não permite ter estoque gerenciado!' + sLineBreak +
      'Avise a DERMATEK!');
      Exit;
    end;
    // Somente se for Entrada positiva!
    if InnPecas + InnPesoKg > 0 then
    begin
      case TEstqMovimID(MovimID) of
        //if MovimID = Integer(TEstqMovimID.emidCompra) then
        (*01*)TEstqMovimID.emidCompra:
        begin
          FatorMP := FatorNotaMP(Qry.FieldByName('GraGruX').AsInteger);
          //
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT SUM(vmi.Pecas) Pecas, ',
          'SUM(vmi.PesoKg) PesoKg, ',
          'SUM(vmi.AreaM2) AreaM2, ',
          'SUM(vmi.QtdGerPeca) QtdGerPeca, ',
          'SUM(vmi.QtdGerPeso) QtdGerPeso, ',
          'SUM(vmi.QtdGerArM2) QtdGerArM2, ',
          'SUM(vmi.QtdGerArP2) QtdGerArP2, ',
          //'SUM(vnc.FatorNota * vmi.PesoKg) / SUM(vmi.PesoKg) FatorAR ',
          'SUM(vnc.FatorNota * vmi.QtdGerArM2) / SUM(vmi.QtdGerArM2) FatorAR ',
          'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
          'LEFT JOIN vsribcad vnc ON vnc.GraGruX=vmi.DstGGX',
          'WHERE vmi.SrcNivel2=' + Geral.FF0(Controle),
          //'AND vmi.Pecas < 0 ',
          'AND (vmi.Pecas < 0 ',
          // Aparas só por peso!
          '  OR (vmi.PesoKg<0 AND vmi.Pecas=0) ',
          // Para couros que sobraram e tem baixa "nao negativa"
          //'OR vmi.MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidExtraBxa)),  //17
          '  OR vmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_VS + ') ',
          ') ',
          '']);
          //Geral.MB_SQL(nil, Qry);
          FatorAR := Qry.FieldByName('FatorAR').AsFloat;
        end;
        //(*01*)emidCompra       : Acima!!!;
        //(*02*)emidVenda        : Atualiza := False;
        //(*03*)emidReclasWE     : Atualiza := False;
        //(*04*)emidBaixa        : Atualiza := False;
        //(*05*)emidIndsWE       : Atualiza := False;
        (*06*)TEstqMovimID.emidIndsXX,
        //(*09*)emidForcado      : Atualiza := False;
        (*10*)emidSemOrigem,
        //(*11*)emidEmOperacao   : Abaixo!! Especifico
        //(*12*)emidResiduoReclas: Atualiza := False;
        (*13*)emidInventario:
        begin
          ReopenQrySaldoIMEI_Baixa(Qry, Qr2, Controle);
        end;
        (*07*)emidClassArtXXUni,
        (*08*)emidReclasXXUni,
        (*14*)emidClassArtXXMul,
        (*15*)emidPreReclasse,
        (*16*)emidEntradaPlC,
        (*20*)emidFinished,
        (*21*)emidDevolucao,
        (*22*)emidRetrabalho,
        (*23*)emidGeraSubProd,
        (*24*)emidReclasXXMul,
        (*25*)emidTransfLoc,
        (*28*)emidDesclasse,
        (*29*)emidCaleado,
        (*30*)emidEmRibPDA,
        (*31*)emidEmRibDTA,
        (*36*)emidInnSemCob,
        //(*37*)emidOutSemCob,
        (*40*)emidConservado(*,
        (*42*)(*emidSPCaleirad*):
        begin
          ReopenQrySaldoIMEI_Baixa(Qry, Qr2, Controle);
(*
          PcEfetiv  := Qry.FieldByName('Pecas').AsFloat;
          if (PcEfetiv < 0.001) and (SdoVrtPeca >= 0.001) then
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(Qr3, Dmod.MyDB, [
            'SELECT SUM(cia.Pecas) Pecas, ',
            'SUM(cia.AreaM2) AreaM ',
            'FROM vscacitsa cia',
            'WHERE cia.VMI_Sorc=' + Geral.FF0(Controle),
            '']);
            Geral.MB_Aviso(FloatToStr(Qr3.FieldByName('Pecas').AsFloat));
          end;
*)
        end;
        (*11*)TEstqMovimID.emidEmOperacao:
        begin
          // Calcula de forma diferente!
          AtualizaTotaisVSOpeCab(MovimCod);
          if (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminDestOper) then
          //or (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminEmOperInn) then
            ReopenQrySaldoIMEI_Baixa(Qry, Qr2, Controle)
          else
            Exit;
        end;
        (*19*)TEstqMovimID.emidEmProcWE:
        begin
          // Calcula de forma diferente!
          AtualizaTotaisVSPWECab(MovimCod);
          if (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminDestWEnd) then
          //or (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminEmOperInn) then
          //or (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminEmOperInn) then
            ReopenQrySaldoIMEI_Baixa(Qry, Qr2, Controle)
          else
            Exit;
        end;
        (*26*)TEstqMovimID.emidEmProcCal:
        begin
          // Calcula de forma diferente!
          AtualizaTotaisVSCalCab(MovimCod);
          if (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminDestCal) then
          //or (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminEmOperInn) then
            ReopenQrySaldoIMEI_Baixa(Qry, Qr2, Controle)
          else
            Exit;
        end;
        (*27*)TEstqMovimID.emidEmProcCur:
        begin
          // Calcula de forma diferente!
          AtualizaTotaisVSCurCab(MovimCod);
          if (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminDestCur) then
          //or (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminEmOperInn) then
            ReopenQrySaldoIMEI_Baixa(Qry, Qr2, Controle)
          else
            Exit;
        end;
        (*32*)TEstqMovimID.emidEmProcSP:
        begin
          // Calcula de forma diferente!
          AtualizaTotaisVSPSPCab(MovimCod);
          if (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminDestPSP) then
          //or (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminEmPSPInn) then
            ReopenQrySaldoIMEI_Baixa(Qry, Qr2, Controle)
          else
            Exit;
        end;
        (*33*)TEstqMovimID.emidEmReprRM:
        begin
          // Calcula de forma diferente!
          AtualizaTotaisVSRRMCab(MovimCod);
          if (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminDestRRM) then
          //or (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminEmRRMInn) then
            ReopenQrySaldoIMEI_Baixa(Qry, Qr2, Controle)
          else
            Exit;
        end;
        (*39*)TEstqMovimID.emidEmProcCon:
        begin
          // Calcula de forma diferente!
          AtualizaTotaisVSConCab(MovimCod);
          if (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminDestCon) then
          //or (TEstqMovimNiv(MovimNiv) = TEstqMovimNiv.eminEmOperInn) then
            ReopenQrySaldoIMEI_Baixa(Qry, Qr2, Controle)
          else
            Exit;
        end;
        else
        begin
          Geral.MB_Aviso('"MovimID" sem atualização de estoque!' + sLineBreak +
          'MovimID = ' + Geral.FF0(MovimID) + sLineBreak +
          'AVISE A DERMATEK COM URGÊNCIA!!!');
          Exit;
        end;
      end;
      OutPecas   := Qry.FieldByName('Pecas').AsFloat;
      OutAreaM2  := Qry.FieldByName('AreaM2').AsFloat;
      OutPesoKg  := Qry.FieldByName('PesoKg').AsFloat;
      QtdGerPeca := -Qry.FieldByName('QtdGerPeca').AsFloat;
      QtdGerPeso := -Qry.FieldByName('QtdGerPeso').AsFloat;
      QtdGerArM2 := -Qry.FieldByName('QtdGerArM2').AsFloat;
      QtdGerArP2 := -Qry.FieldByName('QtdGerArP2').AsFloat;
      //
      if Qr2.State <> dsInactive then
      begin
        OutPecas   := OutPecas   + Qr2.FieldByName('Pecas').AsFloat;
        OutAreaM2  := OutAreaM2  + Qr2.FieldByName('AreaM2').AsFloat;
        OutPesoKg  := OutPesoKg  + Qr2.FieldByName('PesoKg').AsFloat;
        QtdGerPeca := QtdGerPeca - Qr2.FieldByName('QtdGerPeca').AsFloat;
        QtdGerPeso := QtdGerPeso - Qr2.FieldByName('QtdGerPeso').AsFloat;
        QtdGerArM2 := QtdGerArM2 - Qr2.FieldByName('QtdGerArM2').AsFloat;
        QtdGerArP2 := QtdGerArP2 - Qr2.FieldByName('QtdGerArP2').AsFloat;
      end;
      SdoVrtPeca := InnPecas  + OutPecas;
      SdoVrtArM2 := InnAreaM2 + OutAreaM2;
      SdoVrtPeso := InnPesoKg + OutPesoKg;
      //
      if Gera then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
        'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2',
        'QtdGerPeca', 'QtdGerPeso', 'QtdGerArM2', 'QtdGerArP2'
        ], ['Controle'], [
        SdoVrtPeca, SdoVrtPeso, SdoVrtArM2,
        QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2
        ], [Controle], True);
        if MovimID = Integer(TEstqMovimID.emidCompra) then
        begin
          NotaMPAG := 0;
          if SdoVrtPeca >= 0.001 then
            NotaMPAG := 0
          else
            NotaMPAG := NotaCouroRibeiraApuca(
              -OutPecas, -OutPesoKg, QtdGerArM2, FatorMP, FatorAR);
          FatNotaVNC := FatorMP;
          FatNotaVRC := FatorAR;
          //
          AtualizaNotaMPAG(Controle, NotaMPAG, FatNotaVNC, FatNotaVRC);
        end;
      end else
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
        'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2'], ['Controle'], [
        SdoVrtPeca, SdoVrtPeso, SdoVrtArM2], [Controle], True);
    end else
    begin
      SdoVrtPeca := 0;
      SdoVrtPeso := 0;
      SdoVrtArM2 := 0;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2'], ['Controle'], [
      SdoVrtPeca, SdoVrtPeso, SdoVrtArM2], [Controle], True);
    end;
    VAR_BxaVrtPeca := OutPecas;
    VAR_BxaVrtPeso := OutPesoKg;
    VAR_BxaVrtArM2 := OutAreaM2;
    //
    VAR_SdoVrtPeca := SdoVrtPeca;
    VAR_SdoVrtPeso := SdoVrtPeso;
    VAR_SdoVrtArM2 := SdoVrtArM2;
  finally
    Qr2.Free;
  end;
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaSaldoVirtualVSMovIts_Generico(Controle, MovimID,
  MovimNiv: Integer);
const
  Gera = False;
  Sim = 1;
  Nao = 2;
  Zer = 0;
var
  Atualiza: Integer;
begin
  VAR_BxaVrtPeca := 0;
  VAR_BxaVrtPeso := 0;
  VAR_BxaVrtArM2 := 0;
  //
  VAR_SdoVrtPeca := 0;
  VAR_SdoVrtPeso := 0;
  VAR_SdoVrtArM2 := 0;
  //
  Atualiza := Sim;
  //
  AdicionarNovosVS_emid();
  case TEstqMovimID(MovimID) of
    (*00*)emidAjuste        : Atualiza := Nao;
    (*01*)emidCompra        : Atualiza := Sim;
    (*02*)emidVenda         : Atualiza := Nao;
    (*03*)emidReclasWE      : Atualiza := Nao;
    (*04*)emidBaixa         : Atualiza := Nao;
    (*05*)emidIndsWE        : Atualiza := Nao;
    (*06*)emidIndsXX        :
    begin
      case TEstqMovimNiv(MovimNiv) of
        eminDestCurtiXX: Atualiza := Sim;
        eminSorcCurtiXX: Atualiza := Zer;
        eminBaixCurtiXX: Atualiza := Zer;
      end;
    end;
    (*07*)emidClassArtXXUni : Atualiza := Sim;
    (*08*)emidReclasXXUni   : Atualiza := Sim;
    (*09*)emidForcado       : Atualiza := Nao;
    (*10*)emidSemOrigem     : Atualiza := Nao; // deprecado! Sim;
    (*11*)emidEmOperacao    : Atualiza := Sim;
    (*12*)emidResiduoReclas : Atualiza := Nao;
    (*13*)emidInventario    : Atualiza := Sim;
    (*14*)emidClassArtXXMul : Atualiza := Sim;
    (*15*)emidPreReclasse   : Atualiza := Sim;
    (*16*)emidEntradaPlC    : Atualiza := Sim;
    (*17*)emidExtraBxa      : Atualiza := Nao;
    (*18*)emidSaldoAnterior : Atualiza := Nao;
    (*19*)emidEmProcWE      : Atualiza := Sim;
    (*20*)emidFinished      : Atualiza := Sim;
    (*21*)emidDevolucao     : Atualiza := Sim;
    (*22*)emidRetrabalho    : Atualiza := Sim;
    (*23*)emidGeraSubProd   : Atualiza := Sim;
    (*24*)emidReclasXXMul   : Atualiza := Sim;
    (*25*)emidTransfLoc     : Atualiza := Sim;
    (*26*)emidEmProcCal     : Atualiza := Sim;
    (*27*)emidEmProcCur     : Atualiza := Sim;
    (*28*)emidDesclasse     : Atualiza := Sim;
    (*29*)emidCaleado       : Atualiza := Sim;
    (*30*)emidEMRibPDA      : Atualiza := Sim;
    (*31*)emidEmRibDTA      : Atualiza := Sim;
    (*32*)emidEmProcSP      : Atualiza := Sim;
    (*33*)emidEmReprRM      : Atualiza := Sim;
    (*34*)emidCurtido       : Atualiza := Nao; // Não existe lancto fisico
    //35*)emidMixInsum      : Atualiza := Nao;
    (*36*)emidInnSemCob     : Atualiza := Sim;
    (*37*)emidOutSemCob     : Atualiza := Nao;
    (*39*)emidEmProcCon     : Atualiza := Sim;
    (*40*)emidConservado    : Atualiza := Sim;
    (*41*)emidEntraExced    : Atualiza := Nao;
    //(*42*)emidSPCaleirad    : Atualiza := Sim;
    else begin
      Geral.MB_Erro(
      '"MovimID" não definido em "AtualizaSaldoVirtualVSMovIts_Generico()"'
      + 'Avise a DERMATEK!');
      Atualiza := Nao;
    end;
  end;
  case Atualiza of
    Zer: ZeraSaldoIMEI(Controle);
    Sim: AtualizaSaldoIMEI(Controle, Gera);
    Nao: ; // Nada
  end;
end;

procedure TUnVS_CRC_PF.AtualizaSerieNFeVMC(MovimCod, NFeSer, NFeNum,
  VSMulNFeCab: Integer);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  'NFeSer', 'NFeNum', 'VSMulNFeCab'], [
  'MovimCod'], [
  NFeSer, NFeNum, VSMulNFeCab], [
  MovimCod], True);
end;

procedure TUnVS_CRC_PF.AtualizaSerieNFeVMI(Controle, NFeSer, NFeNum,
  VSMulNFeCab: Integer);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  'NFeSer', 'NFeNum', 'VSMulNFeCab'], [
  'Controle'], [
  NFeSer, NFeNum, VSMulNFeCab], [
  Controle], True);
end;

procedure TUnVS_CRC_PF.AtualizaStatPall(Pallet: Integer);
var
  Qry: TmySQLQuery;
  StatPall: Integer;
begin
  StatPall := Integer(TXXStatPall.xxspIndefinido);
  Qry := TmySQLQuery.Create(Dmod);
  try
    //  Desmontando!
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM vsparclcaba ',
    'WHERE VSPallet=' + Geral.FF0(Pallet),
    'AND DtHrFimCla< "1900-01-01" ',
    '']);
     if Qry.RecordCount > 0 then
       StatPall := StatPall + Integer(TXXStatPall.xxspDesmontando);  // 1
       // Montando!
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM vspaclaitsa ',
    'WHERE VSPallet=' + Geral.FF0(Pallet),
    'AND DtHrFim< "1900-01-01" ',
    ' ',
    'UNION ',
    ' ',
    'SELECT Controle ',
    'FROM vsparclitsa ',
    'WHERE VSPallet=' + Geral.FF0(Pallet),
    'AND DtHrFim< "1900-01-01" ',
    ' ']);
     if Qry.RecordCount > 0 then
       StatPall := StatPall + Integer(TXXStatPall.xxspMontando); // 2 ou 3
    //
    //if StatPall = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT DtHrEndAdd ',
      'FROM vspalleta ',
      'WHERE Codigo=' + Geral.FF0(Pallet),
      '']);
      if Qry.FieldByName('DtHrEndAdd').AsDateTime > 2 then
        StatPall := StatPall + Integer(TXXStatPall.xxspEncerrado) // 4 ou 5 ou 6 ou 7
      else if StatPall = 0 then
        StatPall := StatPall + Integer(TXXStatPall.xxspRemovido); // 8 ou ... ver no futuro?
    end;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspalleta', False, [
    'StatPall'], ['Codigo'], [StatPall], [Pallet], True);
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaTotaisVSCalCab(MovimCod: Integer);
var
  Qry: TmySQLQuery;
  PecasSrc, PesoKgSrc, AreaSrcM2, AreaSrcP2, ValorTSrc,
  PecasDst, PesoKgDst, AreaDstM2, AreaDstP2, ValorTDst,
  PecasBxa, PesoKgBxa, AreaBxaM2, AreaBxaP2, ValorTBxa,
  PecasSdo, PesoKgSdo, AreaSdoM2, AreaSdoP2, ValorTSdo,
  ValorSrcMP, ValorSrcPQ: Double;
  Codigo, Controle, PedItsLib, GGXSrc: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(QtdAntPeca) Pecas, SUM(QtdAntPeso) PesoKg, ',
      'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
      'SUM(ValorT) ValorT, SUM(ValorMP) ValorMP, ',
      'SUM(CustoPQ) CustoPQ ',
      'FROM ' + CO_SEL_TAB_VMI + '',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminSorcCal)),  // 29
      '']);
    //Geral.MB_SQL(nil, Qry);
    PecasSrc          := -Qry.FieldByName('Pecas').AsFloat;
    PesoKgSrc         := -Qry.FieldByName('PesoKg').AsFloat;
    AreaSrcM2         := -Qry.FieldByName('AreaM2').AsFloat;
    AreaSrcP2         := -Qry.FieldByName('AreaP2').AsFloat;
    ValorTSrc         := -Qry.FieldByName('ValorT').AsFloat;
    ValorSrcMP        := -Qry.FieldByName('ValorMP').AsFloat;
    ValorSrcPQ        := -Qry.FieldByName('CustoPQ').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminDestCal)),  //31
    '']);
    PecasDst          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgDst         := Qry.FieldByName('PesoKg').AsFloat;
    AreaDstM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaDstP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTDst         := Qry.FieldByName('ValorT').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE ',
    '(',
    '  (',
    '     MovimCod=' + Geral.FF0(MovimCod),
    '     AND MovimNiv=' + Geral.FF0(Integer(eminEmCalBxa)),  //32
    '  )',
    '  OR',
    '  (',
    '    MovimID IN (' + CO_ALL_CODS_BXA_EXTRA_VS + ') ',
    '    AND SrcNivel2 IN',
    '    (',
    '      SELECT Controle',
    '      FROM ' + CO_SEL_TAB_VMI + '',
    '      WHERE MovimCod=' + Geral.FF0(MovimCod),
    '    )',
    '',
    '  )',
    ')',
    '']);
    //Geral.MB_Teste(Qry.SQL.Text);
    PecasBxa          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgBxa         := Qry.FieldByName('PesoKg').AsFloat;
    AreaBxaM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaBxaP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTBxa         := Qry.FieldByName('ValorT').AsFloat;
    //
////////////////////////////////////////////////////////////////////////////////
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, Controle, PedItsLib ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminEmCalInn)),  //30
    '']);
    Codigo    := Qry.FieldByName('Codigo').AsInteger;
    Controle  := Qry.FieldByName('Controle').AsInteger;
    PedItsLib := Qry.FieldByName('PedItsLib').AsInteger;
    //
////////////////////////////////////////////////////////////////////////////////
(*  ini 2023-12-30 - desativado!

    ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    Erro quando baixa um peso de subproduto diferente do peso gerado!!!!!!!!!!
    ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE ',
    '(',
    '  (',
    '     SrcMovID=' + Geral.FF0(Integer(emidEmProcCal)),// 26,// + Geral.FF0(MovimCod),
    '     AND SrcNivel1=' + Geral.FF0(Codigo),
    '     AND NOT (MovimID IN (' + CO_ALL_CODS_BXA_EXTRA_VS + ')) ',
    '  )',
    ')',
    '']);
    //Geral.MB_Teste(Qry.SQL.Text);
    PecasBxa          := PecasBxa  + Qry.FieldByName('Pecas').AsFloat;
    PesoKgBxa         := PesoKgBxa + Qry.FieldByName('PesoKg').AsFloat;
    AreaBxaM2         := AreaBxaM2 + Qry.FieldByName('AreaM2').AsFloat;
    AreaBxaP2         := AreaBxaP2 + Qry.FieldByName('AreaP2').AsFloat;
    ValorTBxa         := ValorTBxa + Qry.FieldByName('ValorT').AsFloat;
*)
    //
    PecasSdo          := - PecasSrc  + PecasBxa;
    PesoKgSdo         := - PesoKgSrc + PesoKgBxa;
    AreaSdoM2         := - AreaSrcM2 + AreaBxaM2;
    AreaSdoP2         := - AreaSrcP2 + AreaBxaP2;
    ValorTSdo         := - ValorTSrc + ValorTBxa;
    //
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscalcab', False, [
      'PecasSrc', 'PesoKgSrc', 'AreaSrcM2', 'AreaSrcP2', 'ValorTSrc',
      'PecasDst', 'PesoKgDst', 'AreaDstM2', 'AreaDstP2', 'ValorTDst',
      'PecasBxa', 'PesoKgBxa', 'AreaBxaM2', 'AreaBxaP2', 'ValorTBxa',
      'PecasSdo', 'PesoKgSdo', 'AreaSdoM2', 'AreaSdoP2', 'ValorTSdo'
      ], [
      'MovimCod'], [
      PecasSrc, PesoKgSrc, AreaSrcM2, AreaSrcP2, ValorTSrc,
      PecasDst, PesoKgDst, AreaDstM2, AreaDstP2, ValorTDst,
      PecasBxa, PesoKgBxa, AreaBxaM2, AreaBxaP2, ValorTBxa,
      PecasSdo, PesoKgSdo, AreaSdoM2, AreaSdoP2, ValorTSdo
      ], [
      MovimCod], True) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
        'Pecas', 'PesoKg', 'AreaM2',
        'AreaP2', 'ValorT',
        'ValorMP', 'CustoPQ',
        'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2'], [
        'Controle'], [
        -PecasSrc, -PesoKgSrc, -AreaSrcM2,
        -AreaSrcP2, (*+*)ValorTSrc,
        ValorSrcMP, ValorSrcPQ,
        PecasSdo, PesoKgSdo, AreaSdoM2], [
        Controle], True) then
    end;
    AtualizaVSCalCabGGxSrc(MovimCod);
    (*
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT GraGruX,',
      'SUM(QtdAntPeca) Pecas, SUM(QtdAntPeso) PesoKg, ',
      'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
      'SUM(ValorT) ValorT, SUM(ValorMP) ValorMP, ',
      'SUM(CustoPQ) CustoPQ ',
      'FROM ' + CO_SEL_TAB_VMI + '',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminSorcCal)),  // 29
      'GROUP BY GraGruX',
      'ORDER BY PesoKg DESC ',
      '']);
    //
    GGXSrc := Qry.FieldByName('GraGruX').AsInteger;
    if GGXSrc <> 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscalcab', False, [
      'GGXSrc'], ['MovimCod'], [GGXSrc], [MovimCod], True);
    end;
    *)
    AtualizaVSPedIts_Lib(PedItsLib, Controle,
      -PecasSrc, -PesoKgSrc, -AreaSrcM2, -AreaSrcP2);
    AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmProcCal, MovimCod);
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaTotaisVSConCab(MovimCod: Integer);
var
  Qry: TmySQLQuery;
  PecasSrc, PesoKgSrc, AreaSrcM2, AreaSrcP2, ValorTSrc,
  PecasDst, PesoKgDst, AreaDstM2, AreaDstP2, ValorTDst,
  PecasBxa, PesoKgBxa, AreaBxaM2, AreaBxaP2, ValorTBxa,
  PecasSdo, PesoKgSdo, AreaSdoM2, AreaSdoP2, ValorTSdo,
  ValorSrcMP, ValorSrcPQ: Double;
  Codigo, Controle, PedItsLib, GGXSrc: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(QtdAntPeca) Pecas, SUM(QtdAntPeso) PesoKg, ',
      'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
      'SUM(ValorT) ValorT, SUM(ValorMP) ValorMP, ',
      'SUM(CustoPQ) CustoPQ ',
      'FROM ' + CO_SEL_TAB_VMI + '',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminSorcCon)),  // 65

      '']);
    //Geral.MB_SQL(nil, Qry);
    PecasSrc          := -Qry.FieldByName('Pecas').AsFloat;
    PesoKgSrc         := -Qry.FieldByName('PesoKg').AsFloat;
    AreaSrcM2         := -Qry.FieldByName('AreaM2').AsFloat;
    AreaSrcP2         := -Qry.FieldByName('AreaP2').AsFloat;
    ValorTSrc         := -Qry.FieldByName('ValorT').AsFloat;
    ValorSrcMP        := -Qry.FieldByName('ValorMP').AsFloat;
    ValorSrcPQ        := -Qry.FieldByName('CustoPQ').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminDestCon)),  //67
    '']);
    PecasDst          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgDst         := Qry.FieldByName('PesoKg').AsFloat;
    AreaDstM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaDstP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTDst         := Qry.FieldByName('ValorT').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE ',
    '(',
    '  (',
    '     MovimCod=' + Geral.FF0(MovimCod),
    '     AND MovimNiv=' + Geral.FF0(Integer(eminEmConBxa)),  //68
    '  )',
    '  OR',
    '  (',
    '    MovimID IN (' + CO_ALL_CODS_BXA_EXTRA_VS + ') ',
    '    AND SrcNivel2 IN',
    '    (',
    '      SELECT Controle',
    '      FROM ' + CO_SEL_TAB_VMI + '',
    '      WHERE MovimCod=' + Geral.FF0(MovimCod),
    '    )',
    '',
    '  )',
    ')',
    '']);
    //Geral.MB_Teste(Qry.SQL.Text);
    PecasBxa          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgBxa         := Qry.FieldByName('PesoKg').AsFloat;
    AreaBxaM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaBxaP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTBxa         := Qry.FieldByName('ValorT').AsFloat;
    //
////////////////////////////////////////////////////////////////////////////////
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, Controle, PedItsLib ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminEmConInn)),  //66
    '']);
    Codigo    := Qry.FieldByName('Codigo').AsInteger;
    Controle  := Qry.FieldByName('Controle').AsInteger;
    PedItsLib := Qry.FieldByName('PedItsLib').AsInteger;
    //
////////////////////////////////////////////////////////////////////////////////
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE ',
    '(',
    '  (',
    '     SrcMovID=' + Geral.FF0(Integer(emidEmProcCon)),// 39,// + Geral.FF0(MovimCod),
    '     AND SrcNivel1=' + Geral.FF0(Codigo),
    '     AND NOT (MovimID IN (' + CO_ALL_CODS_BXA_EXTRA_VS + ', 40)) ', // 40 > evitar duplicidade de baixa normal
    '  )',
    ')',
    '']);
    //Geral.MB_Teste(Qry.SQL.Text);
    PecasBxa          := PecasBxa  + Qry.FieldByName('Pecas').AsFloat;
    PesoKgBxa         := PesoKgBxa + Qry.FieldByName('PesoKg').AsFloat;
    AreaBxaM2         := AreaBxaM2 + Qry.FieldByName('AreaM2').AsFloat;
    AreaBxaP2         := AreaBxaP2 + Qry.FieldByName('AreaP2').AsFloat;
    ValorTBxa         := ValorTBxa + Qry.FieldByName('ValorT').AsFloat;
    //
    PecasSdo          := - PecasSrc  + PecasBxa;
    PesoKgSdo         := - PesoKgSrc + PesoKgBxa;
    AreaSdoM2         := - AreaSrcM2 + AreaBxaM2;
    AreaSdoP2         := - AreaSrcP2 + AreaBxaP2;
    ValorTSdo         := - ValorTSrc + ValorTBxa;
    //
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsconcab', False, [
      'PecasSrc', 'PesoKgSrc', 'AreaSrcM2', 'AreaSrcP2', 'ValorTSrc',
      'PecasDst', 'PesoKgDst', 'AreaDstM2', 'AreaDstP2', 'ValorTDst',
      'PecasBxa', 'PesoKgBxa', 'AreaBxaM2', 'AreaBxaP2', 'ValorTBxa',
      'PecasSdo', 'PesoKgSdo', 'AreaSdoM2', 'AreaSdoP2', 'ValorTSdo'
      ], [
      'MovimCod'], [
      PecasSrc, PesoKgSrc, AreaSrcM2, AreaSrcP2, ValorTSrc,
      PecasDst, PesoKgDst, AreaDstM2, AreaDstP2, ValorTDst,
      PecasBxa, PesoKgBxa, AreaBxaM2, AreaBxaP2, ValorTBxa,
      PecasSdo, PesoKgSdo, AreaSdoM2, AreaSdoP2, ValorTSdo
      ], [
      MovimCod], True) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
        'Pecas', 'PesoKg', 'AreaM2',
        'AreaP2', 'ValorT',
        'ValorMP', 'CustoPQ',
        'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2'], [
        'Controle'], [
        -PecasSrc, -PesoKgSrc, -AreaSrcM2,
        -AreaSrcP2, (*+*)ValorTSrc,
        ValorSrcMP, ValorSrcPQ,
        PecasSdo, PesoKgSdo, AreaSdoM2], [
        Controle], True) then
    end;
    AtualizaVSConCabGGxSrc(MovimCod);
    //
    AtualizaVSPedIts_Lib(PedItsLib, Controle,
      -PecasSrc, -PesoKgSrc, -AreaSrcM2, -AreaSrcP2);
    AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmProcCon, MovimCod);
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaTotaisVSCurCab(MovimCod: Integer);
var
  Qry: TmySQLQuery;
  PecasSrc, PesoKgSrc, AreaSrcM2, AreaSrcP2, ValorTSrc,
  PecasDst, PesoKgDst, AreaDstM2, AreaDstP2, ValorTDst,
  PecasBxa, PesoKgBxa, AreaBxaM2, AreaBxaP2, ValorTBxa,
  PecasSdo, PesoKgSdo, AreaSdoM2, AreaSdoP2, ValorTSdo,
  ValorSrcMP, ValorSrcPQ, KgCouPQ: Double;
  Controle, PedItsLib: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(QtdAntPeca) Pecas, SUM(QtdAntPeso) PesoKg, ',
      'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
      'SUM(ValorT) ValorT, SUM(ValorMP) ValorMP, ',
      'SUM(CustoPQ) CustoPQ, SUM(KgCouPQ) KgCouPQ ',
      'FROM ' + CO_SEL_TAB_VMI + '',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminSorcCur)),  // 34
      '']);
    //Geral.MB_SQL(nil, Qry);
    PecasSrc          := -Qry.FieldByName('Pecas').AsFloat;
    PesoKgSrc         := -Qry.FieldByName('PesoKg').AsFloat;
    AreaSrcM2         := -Qry.FieldByName('AreaM2').AsFloat;
    AreaSrcP2         := -Qry.FieldByName('AreaP2').AsFloat;
    ValorTSrc         := -Qry.FieldByName('ValorT').AsFloat;
    ValorSrcMP        := -Qry.FieldByName('ValorMP').AsFloat;
    ValorSrcPQ        := -Qry.FieldByName('CustoPQ').AsFloat;
    KgCouPQ           := -Qry.FieldByName('KgCouPQ').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
      'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
      'SUM(ValorT) ValorT ',
      'FROM ' + CO_SEL_TAB_VMI + '',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestCur)),  //36
      '']);
    //Geral.MB_SQL(nil, Qry);
    PecasDst          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgDst         := Qry.FieldByName('PesoKg').AsFloat;
    AreaDstM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaDstP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTDst         := Qry.FieldByName('ValorT').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE ',
    '(',
    '  (',
    '     MovimCod=' + Geral.FF0(MovimCod),
    '     AND MovimNiv=' + Geral.FF0(Integer(eminEmCurBxa)),  //37
    '  )',
    '  OR',
    '  (',
    '    MovimID IN (6,' + CO_ALL_CODS_BXA_EXTRA_VS + ') ',
    '    AND SrcNivel2 IN',
    '    (',
    '      SELECT Controle',
    '      FROM ' + CO_SEL_TAB_VMI + '',
    '      WHERE MovimCod=' + Geral.FF0(MovimCod),
    '    )',
    '',
    '  )',
    ')',
    '']);
    //Geral.MB_Teste(Qry.SQL.Text);
    PecasBxa          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgBxa         := Qry.FieldByName('PesoKg').AsFloat;
    AreaBxaM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaBxaP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTBxa         := Qry.FieldByName('ValorT').AsFloat;
    //
    PecasSdo          := - PecasSrc  + PecasBxa;
    PesoKgSdo         := - PesoKgSrc + PesoKgBxa;
    AreaSdoM2         := - AreaSrcM2 + AreaBxaM2;
    AreaSdoP2         := - AreaSrcP2 + AreaBxaP2;
    ValorTSdo         := - ValorTSrc + ValorTBxa;
    //
////////////////////////////////////////////////////////////////////////////////
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle, PedItsLib ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminEmCurInn)),  //35
    '']);
    //Geral.MB_SQL(nil, Qry);
    Controle  := Qry.FieldByName('Controle').AsInteger;
    PedItsLib := Qry.FieldByName('PedItsLib').AsInteger;
    //
////////////////////////////////////////////////////////////////////////////////
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscurcab', False, [
    'PecasSrc', 'PesoKgSrc', 'AreaSrcM2', 'AreaSrcP2', 'ValorTSrc',
    'PecasDst', 'PesoKgDst', 'AreaDstM2', 'AreaDstP2', 'ValorTDst',
    'PecasBxa', 'PesoKgBxa', 'AreaBxaM2', 'AreaBxaP2', 'ValorTBxa',
    'PecasSdo', 'PesoKgSdo', 'AreaSdoM2', 'AreaSdoP2', 'ValorTSdo'
    ], [
    'MovimCod'], [
    PecasSrc, PesoKgSrc, AreaSrcM2, AreaSrcP2, ValorTSrc,
    PecasDst, PesoKgDst, AreaDstM2, AreaDstP2, ValorTDst,
    PecasBxa, PesoKgBxa, AreaBxaM2, AreaBxaP2, ValorTBxa,
    PecasSdo, PesoKgSdo, AreaSdoM2, AreaSdoP2, ValorTSdo
    ], [
    MovimCod], True) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Pecas', 'PesoKg', 'AreaM2',
      'AreaP2', 'ValorT',
      'ValorMP', 'CustoPQ', 'KgCouPQ',
      'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2'], [
      'Controle'], [
      -PecasSrc, -PesoKgSrc, -AreaSrcM2,
      -AreaSrcP2, (*+*)ValorTSrc,
      ValorSrcMP, ValorSrcPQ, KgCouPQ,
      PecasSdo, PesoKgSdo, AreaSdoM2], [
      Controle], True) then
    end;
    AtualizaVSPedIts_Lib(PedItsLib, Controle,
      -PecasSrc, -PesoKgSrc, -AreaSrcM2, -AreaSrcP2);
    //
    AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmProcCur, MovimCod);
    AtualizaVSCurCabGGxSrc(MovimCod);
    //
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaTotaisVSOpeCab(MovimCod: Integer);
var
  Qry: TmySQLQuery;
  PecasSrc, PesoKgSrc, AreaSrcM2, AreaSrcP2, ValorTSrc,
  PecasDst, PesoKgDst, AreaDstM2, AreaDstP2, ValorTDst,
  PecasBxa, PesoKgBxa, AreaBxaM2, AreaBxaP2, ValorTBxa,
  PecasSdo, PesoKgSdo, AreaSdoM2, AreaSdoP2, ValorTSdo,
  CusFrtMOEnv, CusFrtMORet, CustoMOTot, ValorMP, ValorT: Double;
  Controle, PedItsLib: Integer;
begin
  // Parei Aqui!
  // Arrumar o "AtualizaTotaisVSPWECab" e outros
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(CusFrtMOEnv) CusFrtMOEnv, ',
    'SUM(CusFrtMORet) CusFrtMORet, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminSorcOper)),  //7
    '']);
    //Geral.MB_SQL(nil, Qry);
    PecasSrc          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgSrc         := Qry.FieldByName('PesoKg').AsFloat;
    AreaSrcM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaSrcP2         := Qry.FieldByName('AreaP2').AsFloat;
    CusFrtMOEnv       := Qry.FieldByName('CusFrtMOEnv').AsFloat;
    //
    ValorTSrc         := Qry.FieldByName('ValorT').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(CusFrtMOEnv) CusFrtMOEnv, ',
    'SUM(CusFrtMORet) CusFrtMORet, ',
    'SUM(CustoMOTot) CustoMOTot, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminDestOper)),  //9
    '']);
    //Geral.MB_SQL(nil, Qry);
    PecasDst          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgDst         := Qry.FieldByName('PesoKg').AsFloat;
    AreaDstM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaDstP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTDst         := Qry.FieldByName('ValorT').AsFloat;
    CusFrtMORet       := Qry.FieldByName('CusFrtMORet').AsFloat;
    CustoMOTot        := Qry.FieldByName('CustoMOTot').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
(*
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminEmOperBxa)),  //10
    'FROM v s m o v i t s',
*)
    'WHERE ',
    '(',
    '  (',
    '     MovimCod=' + Geral.FF0(MovimCod),
    '     AND MovimNiv=' + Geral.FF0(Integer(eminEmOperBxa)),  //10
    '  )',
    '  OR',
    '  (',
    '    MovimID IN (' + CO_ALL_CODS_BXA_EXTRA_VS + ') ',
    '    AND SrcNivel2 IN',
    '    (',
    '      SELECT Controle',
    '      FROM ' + CO_SEL_TAB_VMI + '',
    '      WHERE MovimCod=' + Geral.FF0(MovimCod),
    '    )',
    '',
    '  )',
    ')',
    '']);
    //Geral.MB_Teste(Qry.SQL.Text);
    PecasBxa          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgBxa         := Qry.FieldByName('PesoKg').AsFloat;
    AreaBxaM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaBxaP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTBxa         := Qry.FieldByName('ValorT').AsFloat;
    //
    PecasSdo          := - PecasSrc  + PecasBxa;
    PesoKgSdo         := - PesoKgSrc + PesoKgBxa;
    AreaSdoM2         := - AreaSrcM2 + AreaBxaM2;
    AreaSdoP2         := - AreaSrcP2 + AreaBxaP2;
    ValorTSdo         := - ValorTSrc + ValorTBxa;
    //
////////////////////////////////////////////////////////////////////////////////
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle, PedItsLib ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminEmOperInn)),  //8
    '']);
    Controle  := Qry.FieldByName('Controle').AsInteger;
    PedItsLib := Qry.FieldByName('PedItsLib').AsInteger;
(*  desmarcado 2015-09-19 Duplicando baixa forcada!!!
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(vmi.Pecas) Pecas, ',
    'SUM(vmi.PesoKg) PesoKg, ',
    'SUM(vmi.AreaM2) AreaM2, ',
    'SUM(vmi.QtdGerPeca) QtdGerPeca, ',
    'SUM(vmi.QtdGerPeso) QtdGerPeso, ',
    'SUM(vmi.QtdGerArM2) QtdGerArM2, ',
    'SUM(vmi.QtdGerArP2) QtdGerArP2, ',
    'SUM(vnc.FatorNota * vmi.PesoKg) / SUM(vmi.PesoKg) FatorAR ',
    'FROM v s m o v i t s vmi ',
    'LEFT JOIN vsribcad vnc ON vnc.GraGruX=vmi.DstGGX',
    'WHERE vmi.SrcNivel2=' + Geral.FF0(Controle),
    'AND vmi.Pecas < 0 ',
    '']);
    PecasSdo  := PecasSdo  + Qry.FieldByName('Pecas').AsFloat;
    AreaSdoM2 := AreaSdoM2 + Qry.FieldByName('AreaM2').AsFloat;
    PesoKgSdo := PesoKgSdo + Qry.FieldByName('PesoKg').AsFloat;
*)
////////////////////////////////////////////////////////////////////////////////
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsopecab', False, [
    'PecasSrc', 'PesoKgSrc', 'AreaSrcM2', 'AreaSrcP2', 'ValorTSrc',
    'PecasDst', 'PesoKgDst', 'AreaDstM2', 'AreaDstP2', 'ValorTDst',
    'PecasBxa', 'PesoKgBxa', 'AreaBxaM2', 'AreaBxaP2', 'ValorTBxa',
    'PecasSdo', 'PesoKgSdo', 'AreaSdoM2', 'AreaSdoP2', 'ValorTSdo'
    ], [
    'MovimCod'], [
    PecasSrc, PesoKgSrc, AreaSrcM2, AreaSrcP2, ValorTSrc,
    PecasDst, PesoKgDst, AreaDstM2, AreaDstP2, ValorTDst,
    PecasBxa, PesoKgBxa, AreaBxaM2, AreaBxaP2, ValorTBxa,
    PecasSdo, PesoKgSdo, AreaSdoM2, AreaSdoP2, ValorTSdo
    ], [
    MovimCod], True) then
    begin
      ValorMP := -ValorTSrc;
      ValorT  := ValorMP + CustoMOTot + CusFrtMOEnv + CusFrtMORet;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Pecas', 'PesoKg', 'AreaM2',
      'AreaP2', 'CusFrtMOEnv', 'CusFrtMORet',
      'ValorMP', 'ValorT',
      'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2'], [
      'Controle'], [
      -PecasSrc, -PesoKgSrc, -AreaSrcM2,
      -AreaSrcP2, CusFrtMOEnv, CusFrtMORet,
      ValorMP, ValorT,
      PecasSdo, PesoKgSdo, AreaSdoM2], [
      Controle], True) then
    end;
    AtualizaVSPedIts_Lib(PedItsLib, Controle,
      -PecasSrc, -PesoKgSrc, -AreaSrcM2, -AreaSrcP2);
    //
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaTotaisVSPSPCab(MovimCod: Integer);
var
  Qry: TmySQLQuery;
  PecasSrc, PesoKgSrc, AreaSrcM2, AreaSrcP2, ValorTSrc,
  PecasDst, PesoKgDst, AreaDstM2, AreaDstP2, ValorTDst,
  PecasBxa, PesoKgBxa, AreaBxaM2, AreaBxaP2, ValorTBxa,
  PecasSdo, PesoKgSdo, AreaSdoM2, AreaSdoP2, ValorTSdo: Double;
  Controle, PedItsLib: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminSorcPSP)),  //49
    '']);
    PecasSrc          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgSrc         := Qry.FieldByName('PesoKg').AsFloat;
    AreaSrcM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaSrcP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTSrc         := Qry.FieldByName('ValorT').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminDestPSP)),  //51
    '']);
    PecasDst          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgDst         := Qry.FieldByName('PesoKg').AsFloat;
    AreaDstM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaDstP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTDst         := Qry.FieldByName('ValorT').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
(*
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminEmPSPBxa)),  //52
    'FROM v s m o v i t s',
*)
    'WHERE ',
    '(',
    '  (',
    '     MovimCod=' + Geral.FF0(MovimCod),
    '     AND MovimNiv=' + Geral.FF0(Integer(eminEmPSPBxa)),  //52
    '  )',
    '  OR',
    '  (',
    '    MovimID IN (' + CO_ALL_CODS_BXA_EXTRA_VS + ') ',
    '    AND SrcNivel2 IN',
    '    (',
    '      SELECT Controle',
    '      FROM ' + CO_SEL_TAB_VMI + '',
    '      WHERE MovimCod=' + Geral.FF0(MovimCod),
    '    )',
    '',
    '  )',
    ')',
    '']);
    //Geral.MB_Teste(Qry.SQL.Text);
    PecasBxa          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgBxa         := Qry.FieldByName('PesoKg').AsFloat;
    AreaBxaM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaBxaP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTBxa         := Qry.FieldByName('ValorT').AsFloat;
    //
    PecasSdo          := - PecasSrc  + PecasBxa;
    PesoKgSdo         := - PesoKgSrc + PesoKgBxa;
    AreaSdoM2         := - AreaSrcM2 + AreaBxaM2;
    AreaSdoP2         := - AreaSrcP2 + AreaBxaP2;
    ValorTSdo         := - ValorTSrc + ValorTBxa;
    //
////////////////////////////////////////////////////////////////////////////////
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle, PedItsLib ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminEmPSPInn)),  //50
    '']);
    Controle  := Qry.FieldByName('Controle').AsInteger;
    PedItsLib := Qry.FieldByName('PedItsLib').AsInteger;
(*  Duplica baixa forcada!!!
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(vmi.Pecas) Pecas, ',
    'SUM(vmi.PesoKg) PesoKg, ',
    'SUM(vmi.AreaM2) AreaM2, ',
    'SUM(vmi.QtdGerPeca) QtdGerPeca, ',
    'SUM(vmi.QtdGerPeso) QtdGerPeso, ',
    'SUM(vmi.QtdGerArM2) QtdGerArM2, ',
    'SUM(vmi.QtdGerArP2) QtdGerArP2, ',
    'SUM(vnc.FatorNota * vmi.PesoKg) / SUM(vmi.PesoKg) FatorAR ',
    'FROM v s m o v i t s vmi ',
    'LEFT JOIN vsribcad vnc ON vnc.GraGruX=vmi.DstGGX',
    'WHERE vmi.SrcNivel2=' + Geral.FF0(Controle),
    'AND vmi.Pecas < 0 ',
    '']);
    PecasSdo  := PecasSdo  + Qry.FieldByName('Pecas').AsFloat;
    AreaSdoM2 := AreaSdoM2 + Qry.FieldByName('AreaM2').AsFloat;
    PesoKgSdo := PesoKgSdo + Qry.FieldByName('PesoKg').AsFloat;
*)
////////////////////////////////////////////////////////////////////////////////
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspspcab', False, [
    'PecasSrc', 'PesoKgSrc', 'AreaSrcM2', 'AreaSrcP2', 'ValorTSrc',
    'PecasDst', 'PesoKgDst', 'AreaDstM2', 'AreaDstP2', 'ValorTDst',
    'PecasBxa', 'PesoKgBxa', 'AreaBxaM2', 'AreaBxaP2', 'ValorTBxa',
    'PecasSdo', 'PesoKgSdo', 'AreaSdoM2', 'AreaSdoP2', 'ValorTSdo'
    ], [
    'MovimCod'], [
    PecasSrc, PesoKgSrc, AreaSrcM2, AreaSrcP2, ValorTSrc,
    PecasDst, PesoKgDst, AreaDstM2, AreaDstP2, ValorTDst,
    PecasBxa, PesoKgBxa, AreaBxaM2, AreaBxaP2, ValorTBxa,
    PecasSdo, PesoKgSdo, AreaSdoM2, AreaSdoP2, ValorTSdo
    ], [
    MovimCod], True) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Pecas', 'PesoKg', 'AreaM2',
      'AreaP2', 'ValorT',
      'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2'], [
      'Controle'], [
      -PecasSrc, -PesoKgSrc, -AreaSrcM2,
      -AreaSrcP2, -ValorTSrc,
      PecasSdo, PesoKgSdo, AreaSdoM2], [
      Controle], True) then
    end;
    AtualizaVSPedIts_Lib(PedItsLib, Controle,
      -PecasSrc, -PesoKgSrc, -AreaSrcM2, -AreaSrcP2);
    //
  finally
    Qry.Free;
  end;
end;

{
procedure TUnVS_CRC_PF.AtualizaTotaisVSPWECab(MovimCod: Integer);
var
  Qry: TmySQLQuery;
  PecasSrc, PesoKgSrc, AreaSrcM2, AreaSrcP2, ValorTSrc,
  PecasDst, PesoKgDst, AreaDstM2, AreaDstP2, ValorTDst,
  PecasBxa, PesoKgBxa, AreaBxaM2, AreaBxaP2, ValorTBxa,
  PecasSdo, PesoKgSdo, AreaSdoM2, AreaSdoP2, ValorTSdo,
  ValorSrcMP, ValorSrcPQ, KgCouPQ, ValorT: Double;
  Controle, PedItsLib: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT, SUM(ValorMP) ValorMP, ',
    'SUM(CustoPQ) CustoPQ, SUM(KgCouPQ) KgCouPQ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminSorcWEnd)),  //7
    '']);
    PecasSrc          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgSrc         := Qry.FieldByName('PesoKg').AsFloat;
    AreaSrcM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaSrcP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTSrc         := Qry.FieldByName('ValorT').AsFloat;
    ValorSrcMP        := Qry.FieldByName('ValorMP').AsFloat;
    ValorSrcPQ        := Qry.FieldByName('CustoPQ').AsFloat;
    KgCouPQ           := Qry.FieldByName('KgCouPQ').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminDestWEnd)),  //9
    '']);
    PecasDst          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgDst         := Qry.FieldByName('PesoKg').AsFloat;
    AreaDstM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaDstP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTDst         := Qry.FieldByName('ValorT').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE ',
    '(',
    '  (',
    '     MovimCod=' + Geral.FF0(MovimCod),
    '     AND MovimNiv=' + Geral.FF0(Integer(eminEmWEndBxa)),  //23
    '  )',
    '  OR',
    '  (',
    '    MovimID IN (' + CO_ALL_CODS_BXA_EXTRA_VS + ') ',
    //'    AND MovimNiv IN (0,1) ',  // SemNiv, Baixa
    '    AND SrcNivel2 IN',
    '    (',
    '      SELECT Controle',
    '      FROM ' + CO_SEL_TAB_VMI + '',
    '      WHERE MovimCod=' + Geral.FF0(MovimCod),
    '    )',
    '',
    '  )',
    ')',
    '']);
    //Geral.MB_SQL(nil, Qry);
    PecasBxa          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgBxa         := Qry.FieldByName('PesoKg').AsFloat;
    AreaBxaM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaBxaP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTBxa         := Qry.FieldByName('ValorT').AsFloat;
    //
    PecasSdo          := - PecasSrc  + PecasBxa;
    PesoKgSdo         := - PesoKgSrc + PesoKgBxa;
    AreaSdoM2         := - AreaSrcM2 + AreaBxaM2;
    AreaSdoP2         := - AreaSrcP2 + AreaBxaP2;
    ValorTSdo         := - ValorTSrc + ValorTBxa;
    //
////////////////////////////////////////////////////////////////////////////////
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle, PedItsLib ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminEmWEndInn)),  //8
    '']);
    Controle  := Qry.FieldByName('Controle').AsInteger;
    PedItsLib := Qry.FieldByName('PedItsLib').AsInteger;
    //
////////////////////////////////////////////////////////////////////////////////
    //
    ValorT := ValorTSrc + ValorSrcPQ;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspwecab', False, [
    'PecasSrc', 'PesoKgSrc', 'AreaSrcM2', 'AreaSrcP2', 'ValorTSrc',
    'PecasDst', 'PesoKgDst', 'AreaDstM2', 'AreaDstP2', 'ValorTDst',
    'PecasBxa', 'PesoKgBxa', 'AreaBxaM2', 'AreaBxaP2', 'ValorTBxa',
    'PecasSdo', 'PesoKgSdo', 'AreaSdoM2', 'AreaSdoP2', 'ValorTSdo'
    ], [
    'MovimCod'], [
    PecasSrc, PesoKgSrc, AreaSrcM2, AreaSrcP2, (*ValorTSrc*)ValorT,
    PecasDst, PesoKgDst, AreaDstM2, AreaDstP2, ValorTDst,
    PecasBxa, PesoKgBxa, AreaBxaM2, AreaBxaP2, ValorTBxa,
    PecasSdo, PesoKgSdo, AreaSdoM2, AreaSdoP2, ValorTSdo
    ], [
    MovimCod], True) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Pecas', 'PesoKg', 'AreaM2',
      'AreaP2', 'ValorT',
      'ValorMP', 'CustoPQ',
      'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2',
      'KgCouPQ'], [
      'Controle'], [
      -PecasSrc, -PesoKgSrc, -AreaSrcM2,
      -AreaSrcP2, -(*ValorTSrc*)ValorT,
      -ValorSrcMP, -ValorSrcPQ,
      PecasSdo, PesoKgSdo, AreaSdoM2,
      KgCouPQ], [
      Controle], True) then
    end;
    AtualizaVSPedIts_Lib(PedItsLib, Controle,
      -PecasSrc, -PesoKgSrc, -AreaSrcM2, -AreaSrcP2);
    //
  finally
    Qry.Free;
  end;
end;
}

procedure TUnVS_CRC_PF.AtualizaTotaisVSPWECab(MovimCod: Integer);
var
  Qry: TmySQLQuery;
  PecasSrc, PesoKgSrc, AreaSrcM2, AreaSrcP2, ValorTSrc,
  PecasDst, PesoKgDst, AreaDstM2, AreaDstP2, ValorTDst,
  PecasBxa, PesoKgBxa, AreaBxaM2, AreaBxaP2, ValorTBxa,
  PecasSdo, PesoKgSdo, AreaSdoM2, AreaSdoP2, ValorTSdo,
  CusFrtMOEnv, CusFrtMORet, CustoMOTot, ValorMP,
  ValorSrcMP, ValorSrcPQ, KgCouPQ, ValorT: Double;
  Controle, PedItsLib: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT, SUM(ValorMP) ValorMP, ',
    'SUM(CusFrtMOEnv) CusFrtMOEnv, ',
    'SUM(CusFrtMORet) CusFrtMORet, ',
    'SUM(CustoPQ) CustoPQ, SUM(KgCouPQ) KgCouPQ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminSorcWEnd)),  //7
    '']);
    //Geral.MB_SQL(nil, Qry);
    PecasSrc          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgSrc         := Qry.FieldByName('PesoKg').AsFloat;
    AreaSrcM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaSrcP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTSrc         := Qry.FieldByName('ValorT').AsFloat;
    ValorSrcMP        := Qry.FieldByName('ValorMP').AsFloat;
    ValorSrcPQ        := Qry.FieldByName('CustoPQ').AsFloat;
    CusFrtMOEnv       := Qry.FieldByName('CusFrtMOEnv').AsFloat;
    KgCouPQ           := Qry.FieldByName('KgCouPQ').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(CusFrtMOEnv) CusFrtMOEnv, ',
    'SUM(CusFrtMORet) CusFrtMORet, ',
    'SUM(CustoMOTot) CustoMOTot, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminDestWEnd)),  //9
    '']);
    PecasDst          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgDst         := Qry.FieldByName('PesoKg').AsFloat;
    AreaDstM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaDstP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTDst         := Qry.FieldByName('ValorT').AsFloat;
    CusFrtMORet       := Qry.FieldByName('CusFrtMORet').AsFloat;
    CustoMOTot        := Qry.FieldByName('CustoMOTot').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE ',
    '(',
    '  (',
    '     MovimCod=' + Geral.FF0(MovimCod),
    '     AND MovimNiv=' + Geral.FF0(Integer(eminEmWEndBxa)),  //23
    '  )',
    '  OR',
    '  (',
    '    MovimID IN (' + CO_ALL_CODS_BXA_EXTRA_VS + ') ',
    //'    AND MovimNiv IN (0,1) ',  // SemNiv, Baixa
    '    AND SrcNivel2 IN',
    '    (',
    '      SELECT Controle',
    '      FROM ' + CO_SEL_TAB_VMI + '',
    '      WHERE MovimCod=' + Geral.FF0(MovimCod),
    '    )',
    '',
    '  )',
    ')',
    '']);
    //Geral.MB_Teste(Qry.SQL.Text);
    PecasBxa          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgBxa         := Qry.FieldByName('PesoKg').AsFloat;
    AreaBxaM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaBxaP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTBxa         := Qry.FieldByName('ValorT').AsFloat;
    //
    PecasSdo          := - PecasSrc  + PecasBxa;
    PesoKgSdo         := - PesoKgSrc + PesoKgBxa;
    AreaSdoM2         := - AreaSrcM2 + AreaBxaM2;
    AreaSdoP2         := - AreaSrcP2 + AreaBxaP2;
    ValorTSdo         := - ValorTSrc + ValorTBxa;
    //
////////////////////////////////////////////////////////////////////////////////
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle, PedItsLib ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminEmWEndInn)),  //8
    '']);
    Controle  := Qry.FieldByName('Controle').AsInteger;
    PedItsLib := Qry.FieldByName('PedItsLib').AsInteger;
    //
////////////////////////////////////////////////////////////////////////////////
    //
    ValorT := ValorTSrc + ValorSrcPQ;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspwecab', False, [
    'PecasSrc', 'PesoKgSrc', 'AreaSrcM2', 'AreaSrcP2', 'ValorTSrc',
    'PecasDst', 'PesoKgDst', 'AreaDstM2', 'AreaDstP2', 'ValorTDst',
    'PecasBxa', 'PesoKgBxa', 'AreaBxaM2', 'AreaBxaP2', 'ValorTBxa',
    'PecasSdo', 'PesoKgSdo', 'AreaSdoM2', 'AreaSdoP2', 'ValorTSdo'
    ], [
    'MovimCod'], [
    PecasSrc, PesoKgSrc, AreaSrcM2, AreaSrcP2, (*ValorTSrc*)ValorT,
    PecasDst, PesoKgDst, AreaDstM2, AreaDstP2, ValorTDst,
    PecasBxa, PesoKgBxa, AreaBxaM2, AreaBxaP2, ValorTBxa,
    PecasSdo, PesoKgSdo, AreaSdoM2, AreaSdoP2, ValorTSdo
    ], [
    MovimCod], True) then
    begin
      ValorMP := -ValorTSrc;
      ValorT  := ValorMP + CustoMOTot + CusFrtMOEnv + CusFrtMORet - ValorSrcPQ;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Pecas', 'PesoKg', 'AreaM2',
      'AreaP2', 'CusFrtMOEnv', 'CusFrtMORet',
      'ValorMP', 'ValorT',
      'ValorMP', 'CustoPQ',
      'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2',
      'KgCouPQ'], [
      'Controle'], [
      -PecasSrc, -PesoKgSrc, -AreaSrcM2,
      -AreaSrcP2, CusFrtMOEnv, CusFrtMORet,
      ValorMP, ValorT,
      -ValorSrcMP, -ValorSrcPQ,
      PecasSdo, PesoKgSdo, AreaSdoM2,
      KgCouPQ], [
      Controle], True) then
      begin
        //
      end;
    end;
    AtualizaVSPedIts_Lib(PedItsLib, Controle,
      -PecasSrc, -PesoKgSrc, -AreaSrcM2, -AreaSrcP2);
    //
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaTotaisVSRRMCab(MovimCod: Integer);
var
  Qry: TmySQLQuery;
  PecasSrc, PesoKgSrc, AreaSrcM2, AreaSrcP2, ValorTSrc, ValorMPSrc,
  PecasDst, PesoKgDst, AreaDstM2, AreaDstP2, ValorTDst,
  PecasBxa, PesoKgBxa, AreaBxaM2, AreaBxaP2, ValorTBxa,
  PecasSdo, PesoKgSdo, AreaSdoM2, AreaSdoP2, ValorTSdo: Double;
  Controle, PedItsLib: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminSorcRRM)),
    '']);
    PecasSrc          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgSrc         := Qry.FieldByName('PesoKg').AsFloat;
    AreaSrcM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaSrcP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTSrc         := Qry.FieldByName('ValorT').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminDestRRM)),
    '']);
    PecasDst          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgDst         := Qry.FieldByName('PesoKg').AsFloat;
    AreaDstM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaDstP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTDst         := Qry.FieldByName('ValorT').AsFloat;
    //
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT ',
    'FROM ' + CO_SEL_TAB_VMI + '',
(*
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminEmRRMBxa)),
    'FROM v s m o v i t s',
*)
    'WHERE ',
    '(',
    '  (',
    '     MovimCod=' + Geral.FF0(MovimCod),
    '     AND MovimNiv=' + Geral.FF0(Integer(eminEmRRMBxa)),
    '  )',
    '  OR',
    '  (',
    '    MovimID IN (' + CO_ALL_CODS_BXA_EXTRA_VS + ') ',
    '    AND SrcNivel2 IN',
    '    (',
    '      SELECT Controle',
    '      FROM ' + CO_SEL_TAB_VMI + '',
    '      WHERE MovimCod=' + Geral.FF0(MovimCod),
    '    )',
    '',
    '  )',
    ')',
    '']);
    //Geral.MB_Teste(Qry.SQL.Text);
    PecasBxa          := Qry.FieldByName('Pecas').AsFloat;
    PesoKgBxa         := Qry.FieldByName('PesoKg').AsFloat;
    AreaBxaM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaBxaP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorTBxa         := Qry.FieldByName('ValorT').AsFloat;
    //
    PecasSdo          := - PecasSrc  + PecasBxa;
    PesoKgSdo         := - PesoKgSrc + PesoKgBxa;
    AreaSdoM2         := - AreaSrcM2 + AreaBxaM2;
    AreaSdoP2         := - AreaSrcP2 + AreaBxaP2;
    ValorTSdo         := - ValorTSrc + ValorTBxa;
    //
////////////////////////////////////////////////////////////////////////////////
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle, PedItsLib ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(eminEmRRMInn)),
    '']);
    Controle  := Qry.FieldByName('Controle').AsInteger;
    PedItsLib := Qry.FieldByName('PedItsLib').AsInteger;
(*  Duplica baixa forcada!!!
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(vmi.Pecas) Pecas, ',
    'SUM(vmi.PesoKg) PesoKg, ',
    'SUM(vmi.AreaM2) AreaM2, ',
    'SUM(vmi.QtdGerPeca) QtdGerPeca, ',
    'SUM(vmi.QtdGerPeso) QtdGerPeso, ',
    'SUM(vmi.QtdGerArM2) QtdGerArM2, ',
    'SUM(vmi.QtdGerArP2) QtdGerArP2, ',
    'SUM(vnc.FatorNota * vmi.PesoKg) / SUM(vmi.PesoKg) FatorAR ',
    'FROM v s m o v i t s vmi ',
    'LEFT JOIN vsribcad vnc ON vnc.GraGruX=vmi.DstGGX',
    'WHERE vmi.SrcNivel2=' + Geral.FF0(Controle),
    'AND vmi.Pecas < 0 ',
    '']);
    PecasSdo  := PecasSdo  + Qry.FieldByName('Pecas').AsFloat;
    AreaSdoM2 := AreaSdoM2 + Qry.FieldByName('AreaM2').AsFloat;
    PesoKgSdo := PesoKgSdo + Qry.FieldByName('PesoKg').AsFloat;
*)
////////////////////////////////////////////////////////////////////////////////
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsrrmcab', False, [
    'PecasSrc', 'PesoKgSrc', 'AreaSrcM2', 'AreaSrcP2', 'ValorTSrc',
    'PecasDst', 'PesoKgDst', 'AreaDstM2', 'AreaDstP2', 'ValorTDst',
    'PecasBxa', 'PesoKgBxa', 'AreaBxaM2', 'AreaBxaP2', 'ValorTBxa',
    'PecasSdo', 'PesoKgSdo', 'AreaSdoM2', 'AreaSdoP2', 'ValorTSdo'
    ], [
    'MovimCod'], [
    PecasSrc, PesoKgSrc, AreaSrcM2, AreaSrcP2, ValorTSrc,
    PecasDst, PesoKgDst, AreaDstM2, AreaDstP2, ValorTDst,
    PecasBxa, PesoKgBxa, AreaBxaM2, AreaBxaP2, ValorTBxa,
    PecasSdo, PesoKgSdo, AreaSdoM2, AreaSdoP2, ValorTSdo
    ], [
    MovimCod], True) then
    begin
      ValorMPSrc := ValorTSrc;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Pecas', 'PesoKg', 'AreaM2',
      'AreaP2', 'ValorMP', 'ValorT',
      'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2'], [
      'Controle'], [
      -PecasSrc, -PesoKgSrc, -AreaSrcM2,
      -AreaSrcP2, -ValorMPSrc, -ValorTSrc,
      PecasSdo, PesoKgSdo, AreaSdoM2], [
      Controle], True) then
    end;
    AtualizaVSPedIts_Lib(PedItsLib, Controle,
      -PecasSrc, -PesoKgSrc, -AreaSrcM2, -AreaSrcP2);
    //
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaValoresDescendArtGerAposInfoAreaTotal(
  IMEIArtGer: Integer; AreaM2, ValorT: Double);
var
  Qr1, Qr2, Qr3: TmySQLQuery;
  MovimID, MovimTwn, SrcNivel2: Integer;
  CustoM2: Double;
  Corda: String;
begin
Corda := '';
  if AreaM2 = 0 then
    Exit;
  //
  CustoM2 := ValorT / AreaM2;
  Qr1 := TmySQLQuery.Create(Dmod);
  try
  Qr2 := TmySQLQuery.Create(Dmod);
  try
  Qr3 := TmySQLQuery.Create(Dmod);
  try
    // Baixas de cada IMEI de classe gerada
    UnDmkDAC_PF.AbreMySQLQuery0(Qr1, Dmod.MyDB, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE SrcNivel2=' + Geral.FF0(IMEIArtGer),
    '']);
    AtualizaValoresDescend_0000_All(Qr1, CustoM2);
  finally
    Qr3.Free;
  end;
  finally
    Qr2.Free;
  end;
  finally
    Qr1.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaValoresDescend_0000_All(Qry: TmySQLQuery;
  CustoM2: Double);
var
  MovimID, MovimTwn, Controle, MovimCod: Integer;
  AreaM2: Double;
begin
  VS_CRC_PF.AdicionarNovosVS_emid();
  Qry.First;
  while not Qry.Eof do
  begin
    MovimID  := Qry.FieldByName('MovimID').AsInteger;
    MovimTwn := Qry.FieldByName('MovimTwn').AsInteger;
    Controle := Qry.FieldByName('Controle').AsInteger;
    MovimCod := Qry.FieldByName('MovimCod').AsInteger;
    AreaM2   := Qry.FieldByName('AreaM2').AsFloat;
    case TEstqMovimID(MovimID) of
      //emidAjuste=0, emidCompra=1,
      (*02*)emidVenda: AtualizaValoresDescend_ID02_Venda(Controle, MovimCod,
                          AreaM2, CustoM2);
      //         emidReclasWE=3, emidBaixa=4, emidIndsWE=5, emidIndsXX=6,
      //         emidClassArtXXUni=7, emidReclasXXUni=8, emidForcado=9,
      //         emidSemOrigem=10, emidEmOperacao=11, emidResiduoReclas=12,
      //         emidInventario=13,
      (*14*)emidClassArtXXMul: AtualizaValoresDescend_ID14_ClassArtVSMul(MovimTwn, CustoM2);
      //emidPreReclasse=15,
      //         emidEntradaPlC=16, emidExtraBxa=17, emidSaldoAnterior=18,

      (*19*)emidEmProcWE: AtualizaValoresDescend_ID19_EmProcWE(Controle, MovimCod,
                          AreaM2, CustoM2);
      //emidFinished=20, emidDevolucao=21,
      //         emidRetrabalho=22, emidGeraSubProd=23, emidReclasXXMul=24,
      //         emidTransfLoc=25
      (*25*)emidTransfLoc: AtualizaValoresDescend_ID25_TransfLocal(Controle,
                           MovimCod, AreaM2, CustoM2);
      else Geral.MB_Aviso('"MovimID" ' + sLineBreak + Geral.FF0(MovimID) +
      ' não implementado em "VS_CRC_PF.AtualizaValoresDescendArtGerAposInfoAreaTotal()"'
      + sLineBreak + Qry.SQL.Text);
    end;
    //
    Qry.Next;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaValoresDescend_ID02_Venda(Controle,
  MovimCod: Integer; AreaM2, CustoM2: Double);
var
  ValorT: Double;
begin
  if Controle = 0 then
    Exit;
  //
  ValorT := AreaM2 * CustoM2;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  'ValorT'], [
  'Controle'], [
  ValorT], [
  Controle], True) then
  begin
    AtualizaTotaisVSXxxCab('vsoutcab', MovimCod);
  end;
end;

procedure TUnVS_CRC_PF.AtualizaValoresDescend_ID14_ClassArtVSMul(
  MovimTwn: Integer; CustoM2: Double);
var
  Qr2, Qr3: TmySQLQuery;
  SrcNivel2: Integer;
  ValorT: Double;
begin
  if MovimTwn = 0 then
    Exit;
  Qr2 := TmySQLQuery.Create(Dmod);
  try
    Qr3 := TmySQLQuery.Create(Dmod);
    try
      // Classe gerada gemea da baixa atual
      UnDmkDAC_PF.AbreMySQLQuery0(Qr2, Dmod.MyDB, [
      'SELECT * ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE MovimTwn=' + Geral.FF0(MovimTwn),
      'AND MovimNiv=2 ',
      '']);
      //testar!!!
      ValorT := Qr2.FieldByName('AreaM2').AsFloat * CustoM2;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'ValorT'], [
      'MovimTwn'], [
      ValorT], [
      MovimTwn], True) then
      begin
        //fazer filhos!!!!!
        SrcNivel2 := Qr2.FieldByName('Controle').AsInteger;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qr3, Dmod.MyDB, [
        'SELECT * ',
        'FROM ' + CO_SEL_TAB_VMI,
        'WHERE SrcNivel2=' + Geral.FF0(SrcNivel2),
        '']);
        AtualizaValoresDescend_0000_All(Qr3, CustoM2);
      end;
    finally
      Qr3.Free;
    end;
  finally
    Qr2.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaValoresDescend_ID19_EmProcWE(Controle,
  MovimCod: Integer; AreaM2, CustoM2: Double);
var
  ValorT, CustoMOM2: Double;
  Qr2, Qr3: TmySQLQuery;
  A_M2, C_M2, CustoMOTot, ValorMP: Double;
  MovimTwn, Ctrl: Integer;
begin

//eminSorcWEnd=20,
//eminEmWEndInn=21, eminDestWEnd=22, eminEmWEndBxa=23,
  if Controle = 0 then
    Exit;
  //
  ValorT := AreaM2 * CustoM2;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  'ValorT'], [
  'Controle'], [
  ValorT], [
  Controle], True) then
  begin
    AtualizaTotaisVSPWECab(MovimCod);
    Qr2 := TmySQLQuery.Create(Dmod);
    try
      Qr3 := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qr2, Dmod.MyDB, [
        'SELECT * ',
        'FROM ' + CO_SEL_TAB_VMI,
        'WHERE MovimCod=' + Geral.FF0(MovimCod),
        'AND MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestWEnd)), //22
        '']);
        //
        Qr2.First;
        while not Qr2.Eof do
        begin
          MovimTwn   := Qr2.FieldByName('MovimTwn').AsInteger;
          // Baixa (Saber custo do Wet Blue de origem!)
          UnDmkDAC_PF.AbreMySQLQuery0(Qr3, Dmod.MyDB, [
          'SELECT * ',
          'FROM ' + CO_SEL_TAB_VMI,
          'WHERE MovimTwn=' + Geral.FF0(MovimTwn),
          'AND MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminEmWEndBxa)), //23
          '']);
          A_M2       := Qr3.FieldByName('AreaM2').AsFloat;
          ValorT     := A_M2 * CustoM2;
          Ctrl       := Qr3.FieldByName('Controle').AsInteger;
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
          'ValorT'], ['Controle'], [
          ValorT], [Ctrl], True);
          //
          // Gerados (Destino)
          ValorMP    := -ValorT;
          CustoMOM2  := Qr2.FieldByName('CustoMOM2').AsFloat;
          A_M2       := Qr2.FieldByName('AreaM2').AsFloat;
          CustoMOTot := A_M2 * CustoMOM2;
          ValorT     := ValorMP + CustoMOTot;
          Ctrl       := Qr2.FieldByName('Controle').AsInteger;
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
          'ValorT', 'ValorMP', 'CustoMOTot'], [
          'Controle'], [
          ValorT, ValorMP, CustoMOTot], [
          Ctrl], True) then
          begin
            if A_M2 <> 0 then
              C_M2 := ValorT / A_M2
            else
              C_M2 := 0;
            UnDmkDAC_PF.AbreMySQLQuery0(Qr3, Dmod.MyDB, [
            'SELECT * ',
            'FROM ' + CO_SEL_TAB_VMI,
            'WHERE SrcNivel2=' + Geral.FF0(Ctrl),
            '']);
            AtualizaValoresDescend_0000_All(Qr3, C_M2);
          end;
          //
          Qr2.Next;
        end;
      finally
        Qr3.Free;
      end;
    finally
      Qr2.Free;
    end;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaValoresDescend_ID25_TransfLocal(Controle,
  MovimCod: Integer; AreaM2, CustoM2: Double);
var
  ValorT: Double;
  Qry: TmySQLQuery;
  Ctrl, Twn: Integer;
begin
  if Controle = 0 then
    Exit;
  //
  ValorT := AreaM2 * CustoM2;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  'ValorT'], [
  'Controle'], [
  ValorT], [
  Controle], True) then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(Controle),
      '']);
      Twn := Qry.FieldByName('MovimTwn').AsInteger;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE MovimTwn=' + Geral.FF0(Twn),
      'AND MovimNiv=' + Geral.FF0(Integer(eminDestLocal)),
      '']);
      //IME-I destino da transferencia (IME-I Gemeo do IME-I de Baixa)
      Ctrl := Qry.FieldByName('Controle').AsInteger;
      ValorT := -ValorT;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'ValorT'], [
      'Controle'], [
      ValorT], [
      Ctrl], True) then
      begin
        AtualizaTotaisVSXxxCab('vstrfloccab', MovimCod);
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM ' + CO_SEL_TAB_VMI,
        'WHERE SrcNivel2=' + Geral.FF0(Ctrl),
        '']);
        AtualizaValoresDescend_0000_All(Qry, CustoM2);
      end;
    finally
      Qry.Free;
    end;
  end;
end;

function TUnVS_CRC_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix,
  VMI_Sorc: Integer; QrSumDest, QrSumSorc, QrVMISorc,
  QrPalSorc: TmySQLQuery): Boolean;
const
  AptoUso = 1;
var
  PecasBxa, PecasDst, PesoKg, AreaM2, AreaP2, ValorT, CustoM2: Double;
  VMI, VMIs, Marca: String;
  Terceiro, SerieFch, Ficha, Controle: Integer;
begin
  Result := False;
  //VMI_Dest := QrVMIsDePal.FieldByName('VMI_Dest').AsInteger;
  //                                                                                                          f
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumDest, Dmod.MyDB, [
(*
  // 2015-05-30
  'SELECT VMI_Baix, SUM(Pecas) Pecas, ',
  'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2 ',
  'FROM vscacitsa ',
  'WHERE VMI_Dest=' + Geral.FF0(VMI_Dest),
*)
  'SELECT VMI_Baix, SUM(Pecas) Pecas, ',
  'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2,',
  'SUM(Pecas / FatorIntSrc * FatorIntDst) PcBxa  ',
  'FROM vscacitsa ',
  'WHERE VMI_Dest=' + Geral.FF0(VMI_Dest),
  'GROUP BY VMI_Sorc ',
  // Fim 2015-05-30
  '']);
// Inicio 2019-01-24
  if QrSumDest.RecordCount > 1 then
  begin
    Geral.MB_Erro('ERRO! Avise a DERMATEK' + sLineBreak +
    '"TUnEncerraPallet()" > Mais de um VMI_Baix por VMI_Dest!' + sLineBreak +
    QrSumDest.SQL.Text);
    Exit;
  end;
  // ini 2023-12-26
  //Erro VMI_Baix. Retorna 0 quando não tem registros para ele no vscacitsa
  //VMI_Baix := QrSumDest.FieldByName('VMI_Baix').AsInteger;
  //VMI_Baix := Usar o VMI_Baix conforme parametro VMI_Baix da declaração da procedure!
  // ini 2023-12-26
  //Pecas    := QrSumDest.FieldByName('Pecas').AsFloat;
  PecasBxa   := QrSumDest.FieldByName('PcBxa').AsFloat;
  PecasDst   := QrSumDest.FieldByName('Pecas').AsFloat;
  PesoKg   := 0;
  AreaM2   := QrSumDest.FieldByName('AreaM2').AsFloat;
  AreaP2   := QrSumDest.FieldByName('AreaP2').AsFloat;
{
  case QrSumDest.RecordCount of
    0:
    begin
      //Geral.MB_Info(
      //'Último couro do pallet de destino!');
      //VMI_Baix := o mesmo que veio?;
      //Pecas      := 0;
      PecasBxa   := 0;
      PecasDst   := 0;
      PesoKg     := 0;
      AreaM2     := 0;
      AreaP2     := 0;
    end;
    1:
    begin
      VMI_Baix := QrSumDest.FieldByName('VMI_Baix').AsInteger;
      //Pecas    := QrSumDest.FieldByName('Pecas').AsFloat;
      PecasBxa   := QrSumDest.FieldByName('PcBxa').AsFloat;
      PecasDst   := QrSumDest.FieldByName('Pecas').AsFloat;
      PesoKg   := 0;
      AreaM2   := QrSumDest.FieldByName('AreaM2').AsFloat;
      AreaP2   := QrSumDest.FieldByName('AreaP2').AsFloat;
    end;
    else
    begin
      Geral.MB_Erro('ERRO! Avise a DERMATEK' + sLineBreak +
      '"TUnEncerraPallet()" > Mais de um VMI_Baix por VMI_Dest!');
      Exit;
    end;
  end;
}
// Fim 2019-01-24

  UnDmkDAC_PF.AbreMySQLQuery0(QrSumSorc, Dmod.MyDB, [
  'SELECT VMI_Sorc, SUM(AreaM2) AreaM2 ',
  'FROM vscacitsa ',
  'WHERE VSPallet=' + Geral.FF0(VSPallet),
  'AND VMI_Dest=' + Geral.FF0(VMI_Dest),
  'GROUP BY VMI_Sorc ',
  '']);
  //fazer query com sum do VMI_Sorc e usar no lugar do QrVSGerArtNew !
  ValorT := 0;
  VMIs   := '';
  QrSumSorc.First;
  while not QrSumSorc.Eof do
  begin
    VMI := Geral.FF0(QrSumSorc.FieldByName('VMI_Sorc').AsInteger);
    if VMIs <> '' then
      VMIs := VMIs + ', ';
    VMIS := VMIs + VMI;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrVMISorc, Dmod.MyDB, [
    'SELECT IF(AreaM2=0, 0, ValorT / AreaM2) CustoM2 ',  // Parei Aqui !!!! Fazer do Saldo?
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE Controle=' + VMI,
    '']);
    //
    CustoM2 := QrVMISorc.FieldByName('CustoM2').AsFloat;
    ValorT := ValorT + (AreaM2  * CustoM2);
    //
    QrSumSorc.Next;
  end;
  //
  // Só se tiver movimento no cacits! Quando não tem fica texto vazio!!!!
  if VMI <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPalSorc, Dmod.MyDB, [
    'SELECT ',
    'IF(COUNT(DISTINCT vmi.SerieFch, vmi.Ficha) <> 1, "0", "1") Series_E_Fichas, ',
    'IF(COUNT(DISTINCT vmi.SerieFch) <> 1, 0.000, vmi.SerieFch) SerieFch, ',
    'IF(COUNT(DISTINCT vmi.Ficha) <> 1, 0.000, vmi.Ficha) Ficha, ',
    'IF(COUNT(DISTINCT vmi.Terceiro) <> 1, 0.000, vmi.Terceiro) Terceiro, ',
    'IF(COUNT(DISTINCT vmi.Marca) <> 1, "", vmi.Marca) Marca ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.Controle IN (' + VMIs + ')',
    '']);
    Terceiro := Trunc(QrPalSorc.FieldByName('Terceiro').AsFloat);
    Marca    := QrPalSorc.FieldByName('Marca').AsString;
    if QrPalSorc.FieldByName('Series_E_Fichas').AsString = '1' then
    begin
      SerieFch := Trunc(QrPalSorc.FieldByName('SerieFch').AsFloat);
      Ficha    := Trunc(QrPalSorc.FieldByName('Ficha').AsFloat);
    end else
    begin
      SerieFch := 0;
      Ficha    := 0;
    end;
  end else
  begin
    Terceiro := 0;
    Marca    := '';
    SerieFch := 0;
    Ficha    := 0;
  end;
  //Pallet   := 0;
  //
  Controle := VMI_Baix;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  'Terceiro', (*'Pallet',*)
  'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT',
  (*'Observ',*) 'SerieFch', 'Ficha', (*'Misturou',*)
  'AptoUso', 'Marca'], [
  'Controle'], [
  Terceiro, (*Pallet,*)
  -PecasBxa, -PesoKg,
  -AreaM2, -AreaP2, -ValorT,
  (*Observ,*) SerieFch, Ficha, (*Misturou,*)
  AptoUso, Marca], [
  Controle], True) then
  begin
    Controle := VMI_Dest;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'Terceiro', (*'Pallet',*)
    'Pecas', 'PesoKg',
    'AreaM2', 'AreaP2', 'ValorT',
    (*'Observ',*) 'SerieFch', 'Ficha', (*'Misturou',*)
    'AptoUso', 'Marca'], [
    'Controle'], [
    Terceiro, (*Pallet,*)
    PecasDst, PesoKg,
    AreaM2, AreaP2, ValorT,
    (*Observ,*) SerieFch, Ficha, (*Misturou,*)
    AptoUso, Marca], [
    Controle], True) then
    begin
      AtualizaSaldoIMEI(VMI_Dest, False);
      //
(*
      QrSumSorc.First;
      while not QrSumSorc.Eof do
      begin
        VMI_Sorc := QrSumSorc.FieldByName('VMI_Sorc').AsInteger;
        //
        if VMI_Sorc <> 0 then
          AtualizaSaldoIMEI(VMI_Sorc, False)
        else
        begin
          QrSemOrig := TmySQLQuery.Create(Dmod);
          try
            UnDmkDAC_PF.AbreMySQLQuery0(QrSemOrig, Dmod.MyDB, [
            'SELECT Controle ',
            'FROM v s m o v i t s ',
            'WHERE MovimID=' + Geral.FF0(Integer(emidSemOrigem)),
            'AND Pallet=' + Geral.FF0(VSPallet),
            '']);
            if QrSemOrig.RecordCount = 0 then
            begin
              // Dá erro na reclassificacao! Realmente precisa para ela?
              if TEstqMovimID(MovimIDGer) <> emidReclasVS then
                InsereVMI_SemOrig(VSPallet, VMI_Sorc);
            end else
              VMI_Sorc := QrSemOrig.FieldByName('Controle').AsInteger;
            //
            AtualizaSaldoVMI_SemOrig(VMI_Sorc, VSPallet);
          finally
            QrSemOrig.Free;
          end;
        end;
        //
        QrSumSorc.Next;
      end;
*)
      AtualizaSaldoIMEI(VMI_Sorc, False);
    end;
  end;
  //
(*
  // 2015-03-30 Saber certo o MovimID!!
  UnDmkDAC_PF.AbreMySQLQuery0(QrVMIDest, Dmod.MyDB, [
  'SELECT MovimID ',
  'FROM v s m o v i t s ',
  'WHERE Controle=' + Geral.FF0(VMI_Dest),
  '']);
  MovimID := TEstqMovimID(QrVMIDest.FieldByName('MovimID').AsInteger);
  // FIM 2015-03-30
  case MovimID of
  //if MovimIDGer = emidClassArtXXUni then
    emidClassArtXXUni:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPaClaIts, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM vspaclaitsa ',
      'WHERE VMI_Dest=' + Geral.FF0(VMI_Dest),
      '']);
      //
      Controle := QrPaClaIts.FieldByName('Controle').AsInteger;
      //
      if Controle <> 0 then
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspaclaitsa', False, [
        'DtHrFim'], ['Controle'], [DtHrFim], [Controle], True) then ;
    end;
    //
    //if MovimIDGer = emidReclasVS then
    emidReclasVS:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrPaClaIts, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM vsparclitsa ',
      'WHERE VMI_Dest=' + Geral.FF0(VMI_Dest),
      '']);
      //
      Controle := QrPaClaIts.FieldByName('Controle').AsInteger;
      //
      if Controle <> 0 then
      begin
        if QrSumSorc.RecordCount <> 1 then
          VMI_Sorc := 0
        else
          VMI_Sorc := QrSumSorc.FieldByName('VMI_Sorc').AsInteger;
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsparclitsa', False, [
        'DtHrFim', 'VMI_Sorc'], [
        'Controle'], [
        DtHrFim, VMI_Sorc], [
        Controle], True) then ;
      end;
    end;
    emidClassArtXXMul:
    begin
      // Nada!!!!???
    end;
    else
    begin
      Geral.MB_Erro('"MovimID" não implementado no encerramento de Pallet!');
    end;
  end;
  //
*)
  Result := True;
end;

procedure TUnVS_CRC_PF.AtualizaVSCalCabGGxSrc(MovimCod: Integer);
var
  GGXSrc: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT GraGruX,',
    'SUM(QtdAntPeca) Pecas, SUM(QtdAntPeso) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT, SUM(ValorMP) ValorMP, ',
    'SUM(CustoPQ) CustoPQ ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminSorcCal)),  // 29
    'GROUP BY GraGruX',
    'ORDER BY PesoKg DESC ',
    '']);
    //
    GGXSrc := Qry.FieldByName('GraGruX').AsInteger;
    if GGXSrc <> 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscalcab', False, [
      'GGXSrc'], ['MovimCod'], [GGXSrc], [MovimCod], True);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaVSConCabGGxSrc(MovimCod: Integer);
var
  GGXSrc: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT GraGruX,',
    'SUM(QtdAntPeca) Pecas, SUM(QtdAntPeso) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT, SUM(ValorMP) ValorMP, ',
    'SUM(CustoPQ) CustoPQ ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminSorcCon)),  // 65
    'GROUP BY GraGruX',
    'ORDER BY PesoKg DESC ',
    '']);
    //
    GGXSrc := Qry.FieldByName('GraGruX').AsInteger;
    if GGXSrc <> 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsconcab', False, [
      'GGXSrc'], ['MovimCod'], [GGXSrc], [MovimCod], True);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaVSCurCabGGxSrc(MovimCod: Integer);
var
  GGXSrc: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cal.GGXDst GraGruX,',
    'SUM(vmi.QtdAntPeca) Pecas, SUM(vmi.QtdAntPeso) PesoKg, ',
    'SUM(vmi.AreaM2) AreaM2, SUM(vmi.AreaP2) AreaP2, ',
    'SUM(vmi.ValorT) ValorT, SUM(vmi.ValorMP) ValorMP, ',
    'SUM(vmi.CustoPQ) CustoPQ ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi',
    'LEFT JOIN vscalcab cal ON cal.Codigo=vmi.SrcNivel1',
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminSorcCur)),  // 34
    'GROUP BY cal.GGXDst',
    'ORDER BY PesoKg DESC ',
    '']);
    //
    GGXSrc := Qry.FieldByName('GraGruX').AsInteger;
    if GGXSrc <> 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscurcab', False, [
      'GGXSrc'], ['MovimCod'], [GGXSrc], [MovimCod], True);
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.AtualizaVSPedIts_Fin(VSPedIts: Integer);
var
  Qry: TmySQLQuery;
  Controle: Integer;
  FinPecas, FinPesoKg, FinAreaM2, FinAreaP2: Double;
begin
{$IfDef sAllVS}
  if VSPedIts = 0 then
    Exit;
  Qry := TmySQLQuery.Create(Dmod);
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2 ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE PedItsFin=' + Geral.FF0(VSPedIts),
    '']);
    FinPecas  := Qry.FieldByName('Pecas').AsFloat;
    FinPesoKg := Qry.FieldByName('PesoKg').AsFloat;
    FinAreaM2 := Qry.FieldByName('AreaM2').AsFloat;
    FinAreaP2 := Qry.FieldByName('AreaP2').AsFloat;
    Controle := VSPedIts;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspedits', False, [
    'FinPecas', 'FinPesoKg', 'FinAreaM2', 'FinAreaP2'
    ], ['Controle'], [
    FinPecas, FinPesoKg, FinAreaM2, FinAreaP2
    ], [Controle], True);
  finally
    Screen.Cursor := crDefault;
    Qry.Free;
  end;
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TUnVS_CRC_PF.AtualizaVSPedIts_Lib(VSPedIts, VSMovIts: Integer;
  LibPecas, LibPesoKg, LibAreaM2, LibAreaP2: Double);
var
  Controle: Integer;
begin
  if VSPedIts = 0 then
    Exit;
  Controle := VSPedIts;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspedits', False, [
  CO_FLD_TAB_VMI, 'LibPecas', 'LibPesoKg',
  'LibAreaM2', 'LibAreaP2'
  ], ['Controle'], [
  VSMovIts, LibPecas, LibPesoKg,
  LibAreaM2, LibAreaP2
  ], [Controle], True);
end;

procedure TUnVS_CRC_PF.AtualizaVSValorT(Controle: Integer);
var
  ValorMP, CustoMOTot, ValorT: Double;
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE ' + CO_UPD_TAB_VMI + ' ',
  'SET CustoPQ=(',
  '  SELECT IF(SUM(CusPQ) IS NULL, 0, SUM(CusPQ))',
  '  FROM vsproqui',
  '  WHERE Controle=' + Geral.FF0(Controle),
  ')',
  'WHERE Controle=' + Geral.FF0(Controle) + ';',
  'UPDATE ' + CO_UPD_TAB_VMI + ' ',
  'SET ValorT = ValorMP + CustoMOTot + CustoPQ',
  'WHERE Controle=' + Geral.FF0(Controle) + ';',
  '']);
end;

function TUnVS_CRC_PF.CadastraPalletInfo(Empresa, ClientMO, GraGruX,
  GraGruY: Integer; QrVSPallet: TmySQLQuery; EdPallet: TdmkEditCB;
  CBPallet: TdmkDBLookupComboBox; SBNewPallet: TSpeedButton;
  EdPecas: TdmkEdit): Integer;
var
  Nome, DtHrEndAdd: String;
  Status, CliStat, QtdPrevPc: Integer;
  PodePallet: Boolean;
  MovimIDGer: TEstqMovimID;
begin
  if not DBCheck.LiberaPelaSenhaBoss() then Exit;
  //
  {
  if (VAR_USUARIO <> -1) and (VAR_TXT_AWServerID = '0') then
  begin
    Geral.MB_Info('Cadastro manual de pallet permitido somente para usuário desenvolvedor!');
    Exit;
  end;
  }
  //if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  Result         := -1;
  //Codigo         := 0;
  Nome           := '';
  //(*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  //Empresa        := FEmpresa;
  //ClientMO       := FClientMO;
  Status         := 0; //EdStatus.ValueVariant;
  CliStat        := 0; //EdCliStat.ValueVariant;
  //GraGruX        := EdGraGruX.ValueVariant;
  DtHrEndAdd     := Geral.FDT(DModG.ObtemAgora(), 109);
  MovimIDGer     := TEstqMovimID.emidAjuste;
  QtdPrevPc      := 0;
  //
  // if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  if MyObjects.FIC(Empresa = 0, nil, 'Defina a Empresa!') then Exit;
  if MyObjects.FIC(GraGruX = 0, nil, 'Defina o Artigo!') then Exit;
  //
  PodePallet :=
    //(QrGraGruXGraGruY.Value = CO_GraGruY_6144_VSFinCla)
    (GraGruY = CO_GraGruY_6144_VSFinCla)
  or
    //(QrGraGruXGraGruY.Value = CO_GraGruY_3072_VSRibCla);
    (GraGruY = CO_GraGruY_3072_VSRibCla)
  or
    (
       (GraGruY = CO_GraGruY_2048_VSRibCad)
     and
       (VAR_TXT_AWServerID <> '0')
     );
  //
  if MyObjects.FIC(not PodePallet, nil,
  'Somente artigo classificado ou acabado permite informação de pallet!') then
    Exit;
  //
  Result := CadastraPalletRibCla(Empresa, ClientMO, EdPallet, CBPallet,
  QrVSPallet, MovimIDGer, GraGruX);
  begin
    AtualizaStatPall(Result);
    //ReopenVSPallet();
    UnDmkDAC_PF.AbreQuery(QrVSPallet, Dmod.MyDB);
    EdPallet.ValueVariant := Result;
    CBPallet.KeyValue := Result;
    //
    if SBNewPallet <> nil then
      SBNewPallet.Enabled := False;
    EdPecas.SetFocus;
  end;
end;

function TUnVS_CRC_PF.CadastraPalletRibCla(Empresa, ClientMO: Integer;
  EdPallet: TdmkEditCB; CBPallet: TdmkDBLookupComboBox; QrVSPallet: TmySQLQuery;
  MovimIDGer: TEstqMovimID; GraGruX: Integer): Integer;
var
  Habilita: Boolean;
  PalletNew: Integer;
begin
  Result := 0;
  if VAR_VSInsPalManu(*Dmod.QrControleVSInsPalManu.Value*) = 1 then
  begin
    if DBCheck.CriaFm(TFmVSPalletManual, FmVSPalletManual, afmoNegarComAviso) then
    begin
      FmVSPalletManual.ImgTipo.SQLType := stIns;
      if (EdPallet <> nil) then
      begin
        PalletNew := EdPallet.ValueVariant + 1;
        if PalletNew > 1 then
        begin
          FmVSPalletManual.EdPallet.ValueVariant := PalletNew;
          FmVSPalletManual.EdPallet.ValueVariant := PalletNew;
        end;
      end;
      FmVSPalletManual.ShowModal;
      if FmVSPalletManual.FPermiteIncluir then
        Result := FmVSPalletManual.FNumNewPallet;
      FmVSPalletManual.Destroy;
      if Result = 0 then
        Exit;
    end;
  end;
  //
  if DBCheck.CriaFm(TFmVSPalletAdd, FmVSPalletAdd, afmoNegarComAviso) then
  begin
    FmVSPalletAdd.ImgTipo.SQLType := stIns;
    FmVSPalletAdd.FNewPallet := Result;
    //
    FmVSPalletAdd.EdCodigo.ValueVariant := Result;
    FmVSPalletAdd.EdEmpresa.ValueVariant := DModG.ObtemFilialDeEntidade(Empresa);
    //Habilita := FmVSPalletAdd.EdEmpresa.ValueVariant = 0;
    FmVSPalletAdd.EdClientMO.ValueVariant := ClientMO;
    FmVSPalletAdd.CBClientMO.KeyValue     := ClientMO;
    Habilita := True;
    FmVSPalletAdd.EdEmpresa.Enabled := Habilita;
    FmVSPalletAdd.CBEmpresa.Enabled := Habilita;
    FmVSPalletAdd.EdClientMO.Enabled := Habilita;
    FmVSPalletAdd.CBClientMO.Enabled := Habilita;
    FmVSPalletAdd.EdStatus.ValueVariant := CO_STAT_VSPALLET_0100_DISPONIVEL;
    FmVSPalletAdd.CBStatus.KeyValue     := CO_STAT_VSPALLET_0100_DISPONIVEL;
    FmVSPalletAdd.FMovimIDGer := MovimIDGer;
    if GraGruX <> 0 then
    begin
      FmVSPalletAdd.EdGraGruX.ValueVariant := GraGruX;
      FmVSPalletAdd.CBGraGruX.KeyValue     := GraGruX;
    end;
    //
    FmVSPalletAdd.ShowModal;
    if FmVSPalletAdd.FPallet <> 0 then
    begin
      if (QrVSPallet <> nil) and (QrVSPallet.State <> dsInactive) then
      begin
        UnDmkDAC_PF.AbreQuery(QrVSPallet, Dmod.MyDB);
        if QrVSPallet.Locate('Codigo', FmVSPalletAdd.FPallet, []) then
        begin
          if EdPallet <> nil then
            EdPallet.ValueVariant := FmVSPalletAdd.FPallet;
          if CBPallet <> nil then
          begin
            CBPallet.KeyValue := FmVSPalletAdd.FPallet;
            try
              CBPallet.SetFocus;
            except
              // nada!
            end;
          end;
        end;
      end;
    end else
    begin
      if (QrVSPallet <> nil) and (QrVSPallet.State <> dsInactive) then
        UnDmkDAC_PF.AbreQuery(QrVSPallet, Dmod.MyDB);
    end;
    Result := FmVSPalletAdd.FPallet;
    FmVSPalletAdd.Destroy;
  end;
end;

function TUnVS_CRC_PF.CalculaValorT(IniPecas, IniAreaM2, IniPesoKg, IniValorT,
  Pecas, AreaM2, PesoKg: Double): Double;
begin
  if (AreaM2 > 0) then
    Result := AreaM2 * (IniValorT / IniPecas)
  else
  if (PesoKg > 0) then
    Result := PesoKg * (IniValorT / IniPecas)
  else
  if (Pecas > 0) then
    Result := Pecas * (IniValorT / IniPecas)
  else
    Result := 0;
end;

function TUnVS_CRC_PF.CalculoValorOrigemM2(AreaM2, CustoPQ, ValorMP,
  CusFrtMOEnv: Double): Double;
begin
  if AreaM2 = 0 then
    Result := 0
  else
    Result := (CustoPQ + ValorMP + CusFrtMOEnv) / AreaM2;
end;

function TUnVS_CRC_PF.CampoLstPal(Box: Integer): String;
begin
  Result := 'LstPal' + Geral.FFN(Box, 2);
end;

function TUnVS_CRC_PF.DesenhaSimboloDefeito(Bitmap: TBitmap; const X, Y,
  Simbolo, CorBorda, CorMiolo: Integer): Double;
const
  sProcName = 'DesenhaSimboloDefeito()';
begin
  //with Image.Picture.Bitmap.Canvas do
  with Bitmap.Canvas do
  begin
    Pen.Color := CorBorda;
    Pen.Style := psSolid;
    Brush.Color := CorMiolo;
    Brush.Style := bsSolid;
    case Simbolo of
      0: (*Nada*);
      1:  // Risco
      begin
        Polygon([
          Point(X+4, Y-5),
          Point(X+5, Y-4),
          Point(X-4, Y+5),
          Point(X-5, Y+4),
          Point(X+4, Y-5)
        ]);
      end;
      2:  // Rasgo
      begin
        Polygon([
          Point(X+0, Y-5),
          Point(X+5, Y+5),
          Point(X+5, Y-5),
          Point(X+0, Y-5)
        ]);
      end;
      3: // Furo (Circulo )
      begin
        Ellipse(X-5, Y-5, X+5, Y+5);
      end;
      4: // Quadrado
      begin
        Polygon([
          Point(X-5, Y-5),
          Point(X+5, Y-5),
          Point(X+5, Y+5),
          Point(X-5, Y+5),
          Point(X-5, Y-5)
        ]);
      end;
      5: // Estrela
      begin
        Polygon([
          Point(X+0, Y-5),
          Point(X+2, Y-1),
          Point(X+6, Y-1),
          Point(X+3, Y+2),
          Point(X+4, Y+5),
          Point(X+0, Y+3),
          Point(X-4, Y+5),
          Point(X+0, Y+3),
          Point(X-4, Y+5),
          Point(X-3, Y+2),
          Point(X-6, Y-1),
          Point(X-2, Y-1),
          Point(X+0, Y-5)
        ]);
      end;
      6: // Cruz
      begin
        Polygon([
          Point(X-1, Y-5),
          Point(X+1, Y-5),
          Point(X+1, Y-1),
          Point(X+5, Y-1),
          Point(X+5, Y+1),
          Point(X+1, Y+1),
          Point(X+1, Y+5),
          Point(X-1, Y+5),
          Point(X-1, Y+1),
          Point(X-5, Y+1),
          Point(X-5, Y-1),
          Point(X-1, Y-1),
          Point(X-1, Y-5)
        ]);
      end;
      7: // Losango
      begin
        Polygon([
          Point(X-0, Y-5),
          Point(X+5, Y+0),
          Point(X+0, Y+5),
          Point(X-5, Y+0),
          Point(X+0, Y-5)
        ]);
      end;
      8: // Triangulo
      begin
        Polygon([
          Point(X+0, Y-5),
          Point(X+5, Y+5),
          Point(X-5, Y+5),
          Point(X+0, Y-5)
        ]);
      end;
      9: // Circulo atravessado
      begin
        Polygon([
          Point(X+4, Y-5),
          Point(X+5, Y-4),
          Point(X+3, Y-2),
          Point(X+3, Y+1),
          Point(X+1, Y+3),
          Point(X-2, Y+3),
          Point(X-4, Y+5),
          Point(X-5, Y+4),
          Point(X-3, Y+2),
          Point(X-3, Y-1),
          Point(X-1, Y-3),
          Point(X+2, Y-3),
          Point(X+4, Y-5)
        ]);
      end;
      10: // Circulo atravessado
      begin
        Polygon([
          Point(X-6, Y-3),
          Point(X+6, Y-3),
          Point(X+6, Y+3),
          Point(X-6, Y+3),
          Point(X-6, Y-3)
        ]);
      end;
      11: // ForRew (Nave Atari)
      begin
        Polygon([
          Point(X-5, Y-5),
          Point(X+0, Y+0),
          Point(X+5, Y-5),
          Point(X+5, Y+5),
          Point(X+0, Y+0),
          Point(X-5, Y+5),
          Point(X-5, Y-5)
        ]);
      end;
      else  Geral.MB_ERRO('Símbolo ID ' + Geral.FF0(Simbolo) + 'indefinido em ' +
      sProcName);
    end;
  end;
  //Image.Invalidate;
end;

function TUnVS_CRC_PF.DesenhaSimboloDefeito_Image(var Image: TImage; const X,
  Y, Simbolo, CorBorda, CorMiolo: Integer): Double;
begin
  DesenhaSimboloDefeito(Image.Picture.Bitmap, X, Y, Simbolo, CorBorda, CorMiolo);
  Image.Invalidate;
end;

function TUnVS_CRC_PF.DesenhaSimboloDefeito_Picture(Picture: TPicture;
  const X, Y, Simbolo, CorBorda, CorMiolo: Integer): Double;
begin
  DesenhaSimboloDefeito(Picture.Bitmap, X, Y, Simbolo, CorBorda, CorMiolo);
  //Picture.Invalidate;
end;

function TUnVS_CRC_PF.DesfazEncerramentoPallet(Pallet,
  GraGruX: Integer): Boolean;
const
  DtHrFim = '0000-00-00';
var
  Codigo: Integer;
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM vspalleta ',
    'WHERE Codigo=' + Geral.FF0(Pallet),
    '']);
    if Qry.RecordCount > 0 then
    begin
      if not MyObjects.FIC(Qry.FieldByName('GraGruX').AsInteger <> GraGruX,
      nil, 'Reduzido não confere: ' + Geral.FF0(GraGruX) + ' > ' +
      Geral.FF0(Qry.FieldByName('GraGruX').AsInteger)) then
      begin
        case TEstqMovimID(Qry.FieldByName('MovimIDGer').AsInteger) of
          (*0*)emidAjuste,
          (*:
          begin
            Codigo := QrVSPalletCodigo.Value;
            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'VSPalleta', False, [
            'DtHrEndAdd'], ['Codigo'], [DtHrFim], [Codigo], True) then
              LocCod(Codigo, Codigo);
          end;*)
          (*7*)emidClassArtXXUni,
          (*14*)emidClassArtXXMul,
          (*13*)emidInventario:
          begin
            Codigo := Pallet;
            //
            Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'VSPalleta', False, [
            'DtHrEndAdd'], ['Codigo'], [DtHrFim], [Codigo], True);
          end;
          (*8*)emidReclasXXUni,
          (*24*)emidReclasXXMul:
          Geral.MB_Aviso(
          'Reclassificação não pode ser reabeta pois já não tem como desfazer o movimento do estoque!');
          else Geral.MB_Aviso(
          'ID de movimento não implementado no desfazimento de encerramento!' +
          sLineBreak + 'Solicite à Dermatek!');
        end;
      end;
    end else
      Geral.MB_Aviso(
      'Pallet não localizado!');
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.DistribuiCustoIndsVS(MovimNiv: TEstqMovimNiv; MovimCod,
  Codigo, CtrlDst: Integer);
var
  Qry: TmySQLQuery;
  CustoMOKg, Pecas, QtdAntPeso, ValorMP, CustoMOTot, ValorT, AreaM2, AreaP2,
  PesoKg, FatorMP, FatorAR, NotaMPAG, FatNotaVNC, FatNotaVRC, CusFrtAvuls,
  CusFrtMOEnv, CusFrtMORet(*, CusFrtTrnsf*), CredPereImposto, Retorno,
  CredValrImposto, CustoPQ: Double;
  MovimID, Controle, Terceiro, SerieFch, Ficha, Misturou, PsqMovNiv: Integer;
  Marca: String;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    Misturou := 0;
    // Custo da mao de obra e FatorAR!
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT vmi.MovimID, vmi.MovimNiv, vmi.Controle, ',
    'vmi.CustoMOKg, vmi.CredPereImposto, vnc.FatorNota ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN vsribcad vnc ON vnc.GraGruX=vmi.GraGruX ',
    'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    'AND vmi.MovimCod=' + Geral.FF0(MovimCod),
    '']);
    FatorAR         := Qry.FieldByName('FatorNota').AsFloat;
    CustoMOKg       := Qry.FieldByName('CustoMOKg').AsFloat;
    CredPereImposto := Qry.FieldByName('CredPereImposto').AsFloat;
    MovimID         := Qry.FieldByName('MovimID').AsInteger;
    PsqMovNiv       := Qry.FieldByName('MovimNiv').AsInteger;
    Controle        := Qry.FieldByName('Controle').AsInteger;
    // 2016-07-14
    //if Controle = 0 then
    if (Controle = 0) or (PsqMovNiv = 14) then
      Controle := CtrlDst;
    // ini 2023-05-05
    if PsqMovNiv = 13 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(CustoPQ) CustoPQ ',
      'FROM vsmovits ',
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimNiv=14 ',
      '']);
      CustoPQ := ABS(Qry.FieldByName('CustoPQ').AsFloat);
    end else
      CustoPQ := 0.0000;
    // fim 2023-05-05
    // Fim 2016-07-14
    if Controle <> 0 then
    begin
      // Itens In Natura que geraram o Artigo de Ribeira!
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SUM(vmi.Pecas) Pecas, SUM(vmi.PesoKg) PesoKg, ',
      'SUM(vmi.ValorMP) ValorMP, ',
      'SUM(vmi.ValorT) ValorT, ',
      'SUM(vmi.QtdGerArM2) QtdGerArM2, ',
      'SUM(vmi.QtdGerArP2) QtdGerArP2,',
      'SUM(vmi.QtdGerPeso) QtdGerPeso,',
      'SUM(vmi.CusFrtMORet) CusFrtMORet,',
      //'SUM(vmi.CusFrtTrnsf) CusFrtTrnsf,',
      //'SUM(vnc.FatorNota * vmi.PesoKg) / SUM(vmi.PesoKg) FatorMP ',
      'SUM(vmi.FatNotaVNC * vmi.PesoKg) / SUM(vmi.PesoKg) FatorMP ',
      'FROM ' + CO_SEL_TAB_VMI + ' vmi',
      //'LEFT JOIN vsnatcad vnc ON vnc.GraGruX=vmi.GraGruX',
      'WHERE vmi.DstMovID=' + Geral.FF0(MovimID),
      'AND vmi.DstNivel1=' + Geral.FF0(Codigo),
      'AND vmi.DstNivel2=' + Geral.FF0(Controle),
      'AND vmi.MovimID<>' + Geral.FF0(Integer(emidClassArtXXMul)),
      '']);
      //Geral.MB_SQL(nil, Qry);
      FatorMP     := Qry.FieldByName('FatorMP').AsFloat;
(*
      Pecas       := - Qry.FieldByName('Pecas').AsFloat;
      QtdAntPeso  := - Qry.FieldByName('PesoKg').AsFloat;
      AreaM2      := - Qry.FieldByName('QtdGerArM2').AsFloat;
      AreaP2      := - Qry.FieldByName('QtdGerArP2').AsFloat;
      ValorMP     := - Qry.FieldByName('ValorMP').AsFloat;
*)
      Pecas       := Qry.FieldByName('Pecas').AsFloat;
      QtdAntPeso  := Qry.FieldByName('PesoKg').AsFloat;
      AreaM2      := Qry.FieldByName('QtdGerArM2').AsFloat;
      AreaP2      := Qry.FieldByName('QtdGerArP2').AsFloat;
      PesoKg      := Qry.FieldByName('QtdGerPeso').AsFloat;
      Retorno     := (CustoMOKg * CredPereImposto / 100);
      CustoMOTot  := QtdAntPeso * (CustoMOKg - Retorno);
      CredValrImposto := QtdAntPeso * Retorno;
      ValorMP     := ABS(Qry.FieldByName('ValorMP').AsFloat);
      //  fretes
      //CusFrtAvuls := Qry.FieldByName('CusFrtAvuls').AsFloat;
      //CusFrtMOEnv := Qry.FieldByName('CusFrtMOEnv').AsFloat;
      CusFrtMORet := Qry.FieldByName('CusFrtMORet').AsFloat;
      //CusFrtTrnsf := Qry.FieldByName('CusFrtTrnsf').AsFloat;
      //
      // ini 2023-05-09
      //if PsqMovNiv = 14 then
        //ValorT      := ValorMP + CustoMOTot + CusFrtMORet + CustoPQ
      //else
      // fim 2023-05-09
      if PsqMovNiv = 13 then
        ValorT      := ABS(Qry.FieldByName('ValorT').AsFloat)
      else
        ValorT      := ValorMP + CustoMOTot + CusFrtMORet;// + CusFrtTrnsf;
        //ValorT      := ValorMP + CustoMOTot + CusFrtMORet + CustoPQ;// 2023-05-09

(*
      ValorT      := Qry.FieldByName('ValorMP').AsFloat;
      ValorMP     := ValorT - CustoMOTot;
*)
      //

      NotaMPAG :=
        NotaCouroRibeiraApuca(Pecas, QtdAntPeso, AreaM2, FatorMP, FatorAR);
      FatNotaVNC := FatorMP;
      FatNotaVRC := FatorAR;
      // Ver se eh apenas um fornecedor
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT DISTINCT Terceiro ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE DstMovID=' + Geral.FF0(MovimID),
      'AND DstNivel1=' + Geral.FF0(Codigo),
      'AND DstNivel2=' + Geral.FF0(Controle),
      '']);
      if Qry.RecordCount = 1 then
        Terceiro := Qry.FieldByName('Terceiro').AsInteger
      else
      begin
        Terceiro := 0;
        Misturou := 1;
      end;
      // Ver se eh apenas uma Ficha
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT DISTINCT SerieFch, Ficha ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE DstMovID=' + Geral.FF0(MovimID),
      'AND DstNivel1=' + Geral.FF0(Codigo),
      'AND DstNivel2=' + Geral.FF0(Controle),
      '']);
      if Qry.RecordCount = 1 then
      begin
        SerieFch := Qry.FieldByName('SerieFch').AsInteger;
        Ficha    := Qry.FieldByName('Ficha').AsInteger;
      end else
      begin
        SerieFch := 0;
        Ficha    := 0;
        Misturou := 1;
      end;
      // Ver se eh apenas uma Marca
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT DISTINCT Marca ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE DstMovID=' + Geral.FF0(MovimID),
      'AND DstNivel1=' + Geral.FF0(Codigo),
      'AND DstNivel2=' + Geral.FF0(Controle),
      '']);
      if Qry.RecordCount = 1 then
        Marca    := Qry.FieldByName('Marca').AsString
      else
        Marca    := '';
      //
      // Atualizar dados gerados do Artigo de Ribeira!
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Pecas', 'ValorT', 'CusFrtMORet',
      'CustoMOTot', 'ValorMP',
      'AreaM2', 'AreaP2', 'PesoKg',
      'QtdAntPeso',
      'Terceiro', 'SerieFch', 'Ficha',
      'NotaMPAG', 'Misturou', 'Marca',
      'FatNotaVNC', 'FatNotaVRC',
      'CredValrImposto'], [
      'Controle'], [
      Pecas, ValorT, CusFrtMORet,
      CustoMOTot, ValorMP,
      AreaM2, AreaP2, PesoKg,
      QtdAntPeso,
      Terceiro, SerieFch, Ficha,
      NotaMPAG, Misturou, Marca,
      FatNotaVNC, FatNotaVRC,
      CredValrImposto], [
      Controle], True) then
      begin
        // ini 2023-05-05
        if PsqMovNiv = 13 then
        begin
          (*
          UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
          ' UPDATE vsmovits SET CustoPQ=' + Geral.FFT_Dot(CustoPQ, 4, siPositivo) +
          ' WHERE Controle=' + Geral.FF0(Controle))
          *)
          AtualizaCustoPQ(Controle, CustoPQ);
        end;
        // fim 2023-05-05
        // Atualizar Saldos do Artigo de Ribeira!
        AtualizaSaldoIMEI(Controle, False);
        if Misturou = 1 then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT DISTINCT SrcNivel2 ',
          'FROM ' + CO_SEL_TAB_VMI + ' ',
          'WHERE DstNivel2=' + Geral.FF0(Controle),//178
          '']);
          while not Qry.Eof do
          begin
            Controle := Qry.FieldByName('SrcNivel2').AsInteger;
            Misturou := 1;
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
            'Misturou'], ['Controle'], [Misturou], [Controle], True) then
            //
            Qry.Next;
          end;
        end;
      end;
    end else
      Geral.MB_Erro('Controle não localizado em "Dmod.DistribuiCustoIndsVS()"');
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.EditaIxx(DGDados: TDBGrid;
  QrVSMovIts: TmySQLQuery): Boolean;
var
  Campo, Texto: String;
  Controle, Inteiro: Integer;
begin
  Result := False;
  Campo := DGDados.Columns[THackDBGrid(DGDados).Col -1].FieldName;
  if (Campo = 'IxxFolha') or (Campo = 'IxxLinha') then
  begin
    Inteiro := QrVSMovIts.FieldByName(Campo).AsInteger;
    Texto := Geral.FF0(Inteiro);
    if InputQuery('Novo valor para "' + Campo + '"', 'Informe o novo valor:',
    Texto) then
    begin
      Inteiro := Geral.IMV(Texto);
      Controle := QrVSMovIts.FieldByName('Controle').AsInteger;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_TAB_VMI, False, [
      Campo], ['Controle'], [Inteiro], [Controle], True) then
      begin
        UnDmkDAC_PF.AbreQuery(QrVSMovIts, Dmod.MyDB);
        QrVSMovIts.Locate('Controle', Controle, []);
        //PCItens.ActivePageIndex := 1;
      end
    end;
  end;
end;

function TUnVS_CRC_PF.EncerraPalletNew(const Pallet: Integer;
  const Pergunta: Boolean): Boolean;
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    if DBCheck.CriaFm(TDfVSMod, DfVSMod, afmoSemVerificar) then
    begin
      //DfVSMod.ShowModal;
      Result := DfVSMod.EncerraPallet(Pallet, Pergunta);
      //
      DfVSMod.Destroy;
    end;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

function TUnVS_CRC_PF.EncerraPalletReclassificacaoNew(const VSPaRclCabCacCod,
  VSPaRclCabCodigo, VSPaRclCabVSPallet, Box_Box, Box_VSPaClaIts, Box_VSPallet,
  Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer; const EncerrandoTodos,
  Pergunta: Boolean; var ReabreVSPaRclCab: Boolean): Boolean;
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    if DBCheck.CriaFm(TDfVSMod, DfVSMod, afmoSemVerificar) then
    begin
      //DfVSMod.ShowModal;
      Result := DfVSMod.EncerraPalletReclassificacaoNew(VSPaRclCabCacCod,
      VSPaRclCabCodigo, VSPaRclCabVSPallet, Box_Box, Box_VSPaClaIts,
      Box_VSPallet, Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest, EncerrandoTodos,
      Pergunta);
      //
      ReabreVSPaRclCab := DfVSMod.FReabreVSPaRclCab;
      //
      DfVSMod.Destroy;
    end;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TUnVS_CRC_PF.EncerraPalletSimples(Pallet, Empresa, ClientMO: Integer;
  QrVSPallet: TmySQLQuery; PallOnEdit: array of Integer);
const
  EncerrandoTodos = False;
  FromRcl  = False;
  Pergunta = True;
  Encerra  = True;
  OC       = 0;
var
  FromBox: Variant;
begin
  FromBox := Null;
  EncerraPalletNew(Pallet, Pergunta);
  ReopenVSPallet(QrVSPallet, Empresa, ClientMO, 0, '', PallOnEdit);
end;

procedure TUnVS_CRC_PF.EncerraRendimentoInn(Modo: TdmkModoExec; MovimCod, Serie,
  Ficha: Integer; PB: TProgressBar; LaAviso1, LaAviso2: TLabel; GraGruYIni,
  GraGruYFim, GraGruYPos: Integer);
const
  sProcName = 'UnVS_CRC_PF.EncerraRendimentoInn()';
begin
  if DBCheck.CriaFm(TFmVSCPMRSBER, FmVSCPMRSBER, afmoNegarComAviso) then
  begin
    FmVSCPMRSBER.FPB         := PB;
    FmVSCPMRSBER.FLaAviso1   := LaAviso1;
    FmVSCPMRSBER.FLaAviso2   := LaAviso2;
    FmVSCPMRSBER.FModoExec   := Modo;
    FmVSCPMRSBER.FMovimCod   := MovimCod;
    FmVSCPMRSBER.FSerie      := Serie;
    FmVSCPMRSBER.FFicha      := Ficha;
    FmVSCPMRSBER.FGraGruYIni := GraGruYIni;
    FmVSCPMRSBER.FGraGruYFim := GraGruYFim;
    FmVSCPMRSBER.FGraGruYPos := GraGruYPos;
    //
    case Modo of
      TdmkModoExec.dmodexAutomatico: FmVSCPMRSBER.Executa();
      TdmkModoExec.dmodexManual: FmVSCPMRSBER.ShowModal;
      else Geral.MB_Erro('ModoExec não implementado em ' + sProcName);
    end;
    FmVSCPMRSBER.Destroy;
  end;
end;

function TUnVS_CRC_PF.ExcluiCabecalhoReclasse(MovimID: TEstqMovimID; PreClasse,
  Reclasse: Integer): Boolean;
const
  sProcName = 'VS_CRC_PF.ExcluiCabecalhoReclasse()';
  Pergunta = '';
var
  Tabela: String;
  Qry: TmySQLQuery;
  Controle, SrcNivel2: Integer;
  MovimNiv: TEstqMovimNiv;
begin
  if MyObjects.FIC(MovimID <> TEstqMovimID.emidReclasXXMul, nil,
  sProcName + ' é válida somente para Reclasse Múltipla!') then
    Exit;
  Result := False;
  Tabela := ObtemNomeTabelaVSXxxCab(MovimID);
  if Geral.MB_Pergunta(
  'Confirma a exclusão da desta classe / reclasse e sua pré-configuração?') =
  ID_YES then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimCod=' + Geral.FF0(Reclasse),
      '']);
      if MyObjects.FIC(Qry.RecordCount > 0, nil,
      'Eclusão de reclasse abortada! Existem itens de destino!') then
        Exit;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM ' + CO_SEL_TAB_VMI + ' ',
      'WHERE MovimCod=' + Geral.FF0(PreClasse),
      '']);
      Qry.First;
      while not Qry.Eof do
      begin
        Controle  := Qry.FieldByName('Controle').AsInteger;
        MovimNiv  := TEstqMovimNiv(Qry.FieldByName('MovimNiv').AsInteger);
        SrcNivel2 := Qry.FieldByName('SrcNivel2').AsInteger;
        //
        if UMyMod.ExcluiRegistroInt1(Pergunta, CO_TAB_VMI, 'Controle',
        Controle, Dmod.MyDB) = ID_YES then
        case MovimNiv of
          (*11*)TEstqMovimNiv.eminSorcPreReclas:
                  AtualizaSaldoVirtualVSMovIts(SrcNivel2, False);
          (*12*)TEstqMovimNiv.eminDestPreReclas: ; // Nada
          else Geral.MB_Erro('"MovimNiv" não implementado em ' + sProcName);
        end;
        //
        Qry.Next;
      end;
      //
      UMyMod.ExcluiRegistroInt1(Pergunta, Tabela, 'MovimCod', Reclasse, Dmod.MyDB);
      //
      Result := True;
    finally
      Qry.Free;
    end;
  end;
end;

function TUnVS_CRC_PF.ExcluiCabEIMEI_OpeCab(MovimCod, IMEI: Integer;
  MovimID: TEstqMovimID; MotivDel: TEstqMotivDel): Boolean;
var
  Qry: TmySQLQuery;
  Pergunta, Tabela, Campo, SQL_XTRA, Aviso: String;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    if IMEI <> 0 then
      SQL_XTRA := 'AND Controle <> ' + Geral.FF0(IMEI)
    else
      SQL_XTRA := '';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    SQL_XTRA,
    '']);
    if Qry.RecordCount > 0 then
    begin
      Aviso :=
        'O item não pode ser excluído pela existência dos IME-Is abaixo:' +
        sLineBreak;
      Qry.First;
      while not Qry.Eof do
      begin
        Aviso := Aviso + Geral.FF0(Qry.FieldByName('Controle').AsInteger) + slineBreak;
        Qry.Next;
      end;
      Geral.MB_Aviso(Aviso);
      Exit;
    end;
    //
    if ExcluiVSMovIts_EnviaArquivoExclu(IMEI, Integer(MotivDel),
    Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      Tabela := ObtemNomeTabelaVSXxxCab(MovimID);
      Campo  := 'MovimCod';
      Pergunta := 'Confirma e exclusão do cabeçalho do movimento atual?';
      Result := ExcluiVSNaoVMI(Pergunta, Tabela, Campo, MovimCod, Dmod.MyDB) =
        ID_YES;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.ExcluiControleVSMovIts(Tabela: TmySQLQuery;
  Campo: TIntegerField; Controle1, CtrlBaix, SrcNivel2: Integer; Gera: Boolean;
  Motivo: Integer; Pergunta, Reabre: Boolean): Boolean;
var
  Continua: Boolean;
  Prox: Integer;
  Qry: TmySQLQuery;
  Texto: String;
begin
  Continua := False;
  Qry      := TmySQLQuery.Create(Dmod);
  try
    if Pergunta = True then
    begin
      if CtrlBaix <> 0 then
        Texto := 'IME-I de baixa a ser excluído: ' + Geral.FF0(CtrlBaix) + sLineBreak
      else
        Texto := '';
      //
      Texto := 'Confirma a exclusão do item selecionado?' + sLineBreak +
        'IME-I a ser excluído: ' + Geral.FF0(Controle1) + sLineBreak +
        Texto +
        'IME-I a receber o estoque de volta: ' + Geral.FF0(SrcNivel2) +
        sLineBreak + '';
      //
      if Geral.MB_Pergunta(Texto) = ID_YES then
        Continua := True;
    end else
      Continua := True;
    //
    if Continua = True then
    begin
      if ExcluiVSMovIts_EnviaArquivoExclu(Controle1, Motivo, Dmod.QrUpd,
      Dmod.MyDB, CO_MASTER) then
      begin
        if CtrlBaix <> 0 then
          ExcluiVSMovIts_EnviaArquivoExclu(CtrlBaix, Motivo, Dmod.QrUpd,
            Dmod.MyDB, CO_MASTER);
        AtualizaSaldoIMEI(SrcNivel2, Gera);
        //
        if (Tabela <> nil) and (Reabre = True) then
        begin
          Prox := GOTOy.LocalizaPriorNextIntQr(Tabela, Campo, Controle1);
          Tabela.Close;
          UnDmkDAC_PF.AbreQuery(Tabela, Dmod.MyDB);
          Tabela.Locate('Controle', Prox, []);
        end;
        Result := True;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(Controle,
  Motivo: Integer; Query: TmySQLQuery; DataBase: TmySQLDatabase;
  Senha: String): Boolean;
const
  TabAtivo = CO_DEL_TAB_VMI;
  TabExclu = 'vsmovitz';
var
  CamposZ, Dta: String;
  QryIMEC: TmySQLQuery;
  SrcNivel2, DstNivel2, QtdReg1: Integer;
begin
  if Senha <> CO_MASTER then
    if not DBCheck.LiberaPelaSenhaAdmin() then
      Exit;
  Dta := Geral.FDT(DModG.ObtemAgora(), 105);
  //
  CamposZ := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, TabExclu, '', QtdReg1);
  CamposZ := Geral.Substitui(CamposZ,
    ', DataDel', ', "' + Dta + '" DataDel');
  CamposZ := Geral.Substitui(CamposZ,
    ', UserDel', ', ' + FormatFloat('0', VAR_USUARIO) + ' UserDel');
  CamposZ := Geral.Substitui(CamposZ,
    ', MotvDel', ', ' + FormatFloat('0', Motivo) + ' MotvDel');
  CamposZ := Geral.Substitui(CamposZ,
    ', AWStatSinc', ', ' + Geral.FF0(Integer(stDel)) + ' AWStatSinc');
  //
  CamposZ := 'INSERT INTO ' + TabExclu + ' SELECT ' + sLineBreak +
  CamposZ + sLineBreak +
  'FROM ' + TabAtivo + sLineBreak +
  'WHERE Controle=' + Geral.FF0(Controle) + ';';
  //
  CamposZ := CamposZ + sLineBreak +
  DELETE_FROM + TabAtivo + sLineBreak +
    'WHERE Controle=' + Geral.FF0(Controle) + ';';
  //
  Query.SQL.Text := CamposZ;
  //
  if Query.Database <> DataBase then
    Query.Database := DataBase;
  //
  UMyMod.ExecutaQuery(Query);
  //
  QryIMEC := TmySQLQuery.Create(DMod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QryIMEC, DataBase, [
    'SELECT * ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE Controle=' + Geral.FF0(Controle),
    '']);
    SrcNivel2 := QryIMEC.FieldByName('SrcNivel2').AsInteger;
    DstNivel2 := QryIMEC.FieldByName('DstNivel2').AsInteger;
    //
    if SrcNivel2 <> 0 then
      AtualizaSaldoIMEI(SrcNivel2, False);
    //
    if DstNivel2 <> 0 then
      AtualizaSaldoIMEI(DstNivel2, False);
  finally
    QryIMEC.Free;
  end;

  Result := True;
end;

function TUnVS_CRC_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo: String;
  Inteiro1: Integer; DB: TmySQLDatabase): Integer;
begin
  if (Lowercase(Tabela) = Lowercase(CO_DEL_TAB_VMI))
  or (Lowercase(Tabela) = Lowercase(CO_TAB_VMB))
  then
  begin
    Result := ID_NO;
    Geral.MB_Erro('AVISE A DERMATEK! Exclusão de VMI a implementar!');
  end else
    Result := UMyMod.ExcluiRegistroInt1(Pergunta, Tabela, Campo, Inteiro1, DB);
end;

procedure TUnVS_CRC_PF.ExcluiVSOutFat(Controle: Integer);
begin
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
    'DELETE FROM vsoutfat WHERE Controle=' + Geral.FF0(Controle));
end;

function TUnVS_CRC_PF.FatoresIncompativeis(FatorSrc, FatorDst: Integer;
  MeAviso: TMemo): Boolean;
  procedure Avisa(Aviso: String);
  begin
    if MeAviso <> nil then
      MeAviso.Text := Aviso
    else
      Geral.MB_Aviso(Aviso);
  end;
begin
  Result := True;
  if ((FatorSrc = 0) and (FatorDst <> 0))
  or ((FatorSrc <> 0) and (FatorDst = 0)) then
  begin
    Geral.MB_Aviso('Seleção abortada!' + sLineBreak +
    'O Artigo não está configurado completamente!' + sLineBreak +
    'Termine sua configuração antes de continuar! (1)');
    Exit;
  end;
  if (FatorSrc = 0) and (FatorDst = 0) then
  begin
    Result := Geral.MB_Pergunta(
    'Fatores de conversão de origem e destino zerados!' + slineBreak +
    'Tabela: "CouNiv1" solicite recriação dos registros obrigatórios à Dermatek! ' + slineBreak +
    'Deseja continuar considerando fator = 1,0?') <> ID_YES;
    Exit;
  end;
  Result := False;
  //
  if FatorSrc <> FatorDst then
    Avisa('CUIDADO!!!' + sLineBreak +
    'Ao informar a quantidade leve em consideração a possibilidade de transformção de inteiros para meios!!!');
end;

function TUnVS_CRC_PF.FatorNotaAR(GraGruX: Integer): Double;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT FatorNota  ',
    'FROM vsribcad  ',
    'WHERE GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //
    Result := Qry.FieldByName('FatorNota').AsFloat;
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.FatorNotaCC(GraGruX, MovimID: Integer): Double;
var
  Qry: TmySQLQuery;
  GGX, MID, InN: Integer;
begin
  Result := 0;
  GGX    := GraGruX;
  MID    := MovimID;
  InN    := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    while (GGX <> 0) and (MID <> Integer(TEstqMovimID.emidCompra)) do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT SrcNivel2, MovimID, GraGruX  ',
      'FROM ' + CO_SEL_TAB_VMI + '  ',
      'WHERE Controle=' + Geral.FF0(GGX),
      '']);
      MID := Qry.FieldByName('MovimID').AsInteger;
      GGX := Qry.FieldByName('SrcNivel2').AsInteger;
      if (TEstqMovimID(MID) = emidCompra) then
        InN := Qry.FieldByName('GraGruX').AsInteger;
    end;
    //
    if Inn <> 0  then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT FatorNota  ',
      'FROM vsnatcad  ',
      'WHERE GraGruX=' + Geral.FF0(Inn),
      '']);
      //
      Result := Qry.FieldByName('FatorNota').AsFloat;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.FatorNotaMP(GraGruX: Integer): Double;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT FatorNota  ',
    'FROM vsnatcad  ',
    'WHERE GraGruX=' + Geral.FF0(GraGruX),
    '']);
    //
    Result := Qry.FieldByName('FatorNota').AsFloat;
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.FichaErro(EdSerieFch: TdmkEdit; Empresa, Controle,
  Ficha: Integer; PermiteDuplicar: Boolean): Boolean;
var
  SerieFch: Integer;
  Qry: TmySQLQuery;
begin
  Result := False;
  if Ficha <> 0 then
  begin
    Result := True;
    SerieFch := EdSerieFch.ValueVariant;
    //
    if MyObjects.FIC(SerieFch = 0, EdSerieFch,
    'Informe a Série da Ficha RMP') then
      Exit;
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE SerieFch=' + Geral.FF0(SerieFch),
      'AND Ficha=' + Geral.FF0(Ficha),
      'AND MovimID=' + Geral.FF0(Integer(emidCompra)),
      'AND Empresa=' + Geral.FF0(Empresa),
      Geral.ATS_IF(Controle <> 0, ['AND Controle<>' + Geral.FF0(Controle)]),
      '',
      'UNION',
      '',
      'SELECT Controle ',
      'FROM ' + CO_TAB_VMB,
      'WHERE SerieFch=' + Geral.FF0(SerieFch),
      'AND Ficha=' + Geral.FF0(Ficha),
      'AND MovimID=' + Geral.FF0(Integer(emidCompra)),
      'AND Empresa=' + Geral.FF0(Empresa),
      Geral.ATS_IF(Controle <> 0, ['AND Controle<>' + Geral.FF0(Controle)]),
      '']);
      if not PermiteDuplicar then
        Result :=
          MyObjects.FIC(Qry.RecordCount > 0, nil, 'Ficha RMP já existe!')
      else
        Result := False;
    finally
      Qry.Free;
    end;
  end;
end;

function TUnVS_CRC_PF.GeraNovoPallet(Empresa, ClientMO, GraGruX: Integer;
  EdGraGruX, EdPallet: TdmkEdit; CBPallet: TdmkDBLookupComboBox;
  SBNewPallet: TSpeedButton; QrVSPallet: TmySQLQuery;
  PallOnEdit: array of Integer): Integer;
var
  Nome, DtHrEndAdd: String;
  Codigo, Status, CliStat, MovimIDGer, QtdPrevPc: Integer;
begin
  Codigo         := 0;
  Nome           := '';
  //(*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  //Empresa        := FEmpresa;
  //ClientMO       := FClientMO;
  Status         := 0; //EdStatus.ValueVariant;
  CliStat        := 0; //EdCliStat.ValueVariant;
  //GraGruX        := EdGraGruX.ValueVariant;
  DtHrEndAdd     := Geral.FDT(0, 109); //DModG.ObtemAgora(), 109);
  MovimIDGer     := 0; // Altera depois?
  QtdPrevPc      := 0;
  //
  // if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  if MyObjects.FIC(Empresa = 0, nil, 'Defina a Empresa!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Defina o Artigo!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('VSPalleta', 'Codigo', '', '', tsPos, stIns, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vspalleta', False, [
  'Nome', 'Empresa', 'Status',
  'CliStat', 'GraGruX', 'DtHrEndAdd',
  'MovimIDGer', 'QtdPrevPc', 'ClientMO'], [
  'Codigo'], [
  Nome, Empresa, Status,
  CliStat, GraGruX, DtHrEndAdd,
  MovimIDGer, QtdPrevPc, ClientMO], [
  Codigo], True) then
  begin
    AtualizaStatPall(Codigo);
    ReopenVSPallet(QrVSPallet, Empresa, ClientMO, 0, '', PallOnEdit);
    EdPallet.ValueVariant := Codigo;
    CBPallet.KeyValue := Codigo;
    //
    SBNewPallet.Enabled := False;
  end;
end;

function TUnVS_CRC_PF.GeraSQLTabMov(var SQL: String; const Tab: TTabToWork;
  const TemIMEIMrt: Integer): Boolean;
begin
  SQL := '';
  Result := ((Tab <> ttwA) and (TemIMEIMrt = 1)) or (Tab = ttwA);
end;

function TUnVS_CRC_PF.GeraSQLVSMovItx_Base(SQL_Select, SQL_Flds, SQL_Left,
  SQL_Wher, SQL_Group: String; Tab: TTabToWork; TemIMEIMrt: Integer): String;
begin
  if GeraSQLTabMov(Result, Tab, TemIMEIMrt) then
    Result := Geral.ATS([
  SQL_Select + TabMovVS_Fld_IMEI(tab),
  SQL_Flds,
  'FROM ' + TMeuDB + '.' + TabMovVS_Tab(tab) + ' vmi ',
  SQL_Left,
  SQL_Wher,
  SQL_Group,
  Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
  '']);
end;

function TUnVS_CRC_PF.GeraSQLVSMovItx_IMEI(SQL_Select, SQL_Flds, SQL_Left,
  SQL_Wher, SQL_Group: String; Tab: TTabToWork; TemIMEIMrt: Integer): String;
(*
var
  SQL_SEL: String;
begin
  if SQL_Select <> then
    SQL_SEL := SQL_Select
  else
    SQL_SEL := 'SELECT ';
  //
*)
begin
  if GeraSQLTabMov(Result, Tab, TemIMEIMrt) then
    Result := Geral.ATS([
  SQL_Select + TabMovVS_Fld_IMEI(tab),
//  Codigo                         int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.Codigo AS SIGNED) Codigo, ',
//  Controle                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.Controle AS SIGNED) Controle, ',
//  MovimCod                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.MovimCod AS SIGNED) MovimCod, ',
//MovimNiv                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.MovimNiv AS SIGNED) MovimNiv, ',
//MovimTwn                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.MovimTwn AS SIGNED) MovimTwn, ',
//Empresa                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.Empresa AS SIGNED) Empresa, ',
//ClientMO                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.ClientMO AS SIGNED) ClientMO, ',
//Terceiro                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.Terceiro AS SIGNED) Terceiro, ',
//CliVenda                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.CliVenda AS SIGNED) CliVenda, ',
//MovimID                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.MovimID AS SIGNED) MovimID, ',
//LnkIDXtr                       int(11)      NOT NULL  DEFAULT "0"
//LnkNivXtr1                     int(11)      NOT NULL  DEFAULT "0"
//LnkNivXtr2                     int(11)      NOT NULL  DEFAULT "0"
//DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00"
    'CAST(vmi.DataHora AS DATETIME) DataHora, ',
//Pallet                         int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.Pallet AS SIGNED) Pallet, ',
//GraGruX                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.GraGruX AS SIGNED) GraGruX, ',
//Pecas                          double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(vmi.Pecas AS DECIMAL (15,3)) Pecas, ',
//PesoKg                         double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(vmi.PesoKg AS DECIMAL (15,3)) PesoKg, ',
//AreaM2                         double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(vmi.AreaM2 AS DECIMAL (15,2)) AreaM2, ',
//AreaP2                         double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(vmi.AreaP2 AS DECIMAL (15,2)) AreaP2, ',
//ValorT                         double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(vmi.ValorT AS DECIMAL (15,2)) ValorT, ',
//SrcMovID                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.SrcMovID AS SIGNED) SrcMovID, ',
//SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.SrcNivel1 AS SIGNED) SrcNivel1, ',
//SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.SrcNivel2 AS SIGNED) SrcNivel2, ',
//SrcGGX                         int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.SrcGGX AS SIGNED) SrcGGX, ',
//SdoVrtPeca                     double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(vmi.SdoVrtPeca AS DECIMAL (15,3)) SdoVrtPeca, ',
//SdoVrtPeso                     double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(vmi.SdoVrtPeso AS DECIMAL (15,3)) SdoVrtPeso, ',
//SdoVrtArM2                     double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(vmi.SdoVrtArM2 AS DECIMAL (15,2)) SdoVrtArM2, ',
//Observ                         varchar(255) NOT NULL
    'CAST(vmi.Observ AS CHAR) Observ, ',
//SerieFch                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.SerieFch AS SIGNED) SerieFch, ',
//Ficha                          int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.Ficha AS SIGNED) Ficha, ',
//Misturou                       tinyint(1)   NOT NULL  DEFAULT "0"
    'CAST(vmi.Misturou AS UNSIGNED) Misturou, ',
//FornecMO                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.FornecMO AS SIGNED) FornecMO, ',
//CustoMOKg                      double(15,6) NOT NULL  DEFAULT "0.000000"
    'CAST(vmi.CustoMOKg AS DECIMAL (15,6)) CustoMOKg, ',
//CustoMOTot                     double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(vmi.CustoMOTot AS DECIMAL (15,2)) CustoMOTot, ',
//ValorMP                        double(15,4) NOT NULL  DEFAULT "0.0000"
    'CAST(vmi.ValorMP AS DECIMAL (15,4)) ValorMP, ',
//CustoPQ                        double(15,4) NOT NULL  DEFAULT "0.0000"
    'CAST(vmi.CustoPQ AS DECIMAL (15,4)) CustoPQ, ',
//DstMovID                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.DstMovID AS SIGNED) DstMovID, ',
//DstNivel1                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.DstNivel1 AS SIGNED) DstNivel1, ',
//DstNivel2                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.DstNivel2 AS SIGNED) DstNivel2, ',
//DstGGX                         int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.DstGGX AS SIGNED) DstGGX, ',
//QtdGerPeca                     double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(vmi.QtdGerPeca AS DECIMAL (15,3)) QtdGerPeca, ',
//QtdGerPeso                     double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(vmi.QtdGerPeso AS DECIMAL (15,3)) QtdGerPeso, ',
//QtdGerArM2                     double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(vmi.QtdGerArM2 AS DECIMAL (15,2)) QtdGerArM2, ',
//QtdGerArP2                     double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(vmi.QtdGerArP2 AS DECIMAL (15,2)) QtdGerArP2, ',
//QtdAntPeca                     double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(vmi.QtdAntPeca AS DECIMAL (15,3)) QtdAntPeca, ',
//QtdAntPeso                     double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(vmi.QtdAntPeso AS DECIMAL (15,3)) QtdAntPeso, ',
//QtdAntArM2                     double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(vmi.QtdAntArM2 AS DECIMAL (15,2)) QtdAntArM2, ',
//QtdAntArP2                     double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(vmi.QtdAntArP2 AS DECIMAL (15,2)) QtdAntArP2, ',
//AptoUso                        tinyint(1)   NOT NULL  DEFAULT "1"
//NotaMPAG                       double(15,8) NOT NULL  DEFAULT "0.00000000"
    'CAST(vmi.NotaMPAG AS DECIMAL (15,8)) NotaMPAG, ',
//Marca                          varchar(20)
    'CAST(vmi.Marca AS CHAR) Marca, ',
//TpCalcAuto                     int(11)      NOT NULL  DEFAULT "-1"
//Zerado                         tinyint(1)   NOT NULL  DEFAULT "0"
//EmFluxo                        tinyint(1)   NOT NULL  DEFAULT "1"
//NotFluxo                       int(11)      NOT NULL  DEFAULT "0"
//FatNotaVNC                     double(15,8) NOT NULL  DEFAULT "0.00000000"
//FatNotaVRC                     double(15,8) NOT NULL  DEFAULT "0.00000000"
//PedItsLib                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.PedItsLib AS SIGNED) PedItsLib, ',
//PedItsFin                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.PedItsFin AS SIGNED) PedItsFin, ',
//PedItsVda                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.PedItsVda AS SIGNED) PedItsVda, ',
//Lk                             int(11)                DEFAULT "0"
//DataCad                        date
//DataAlt                        date
//UserCad                        int(11)                DEFAULT "0"
//UserAlt                        int(11)                DEFAULT "0"
//AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"
//Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"
//CustoMOM2                      double(15,6) NOT NULL  DEFAULT "0.000000"
    'CAST(vmi.CustoMOM2 AS DECIMAL (15,6)) CustoMOM2, ',
//ReqMovEstq                     int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.ReqMovEstq AS SIGNED) ReqMovEstq, ',
//StqCenLoc                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.StqCenLoc AS SIGNED) StqCenLoc, ',
//ItemNFe                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.ItemNFe AS SIGNED) ItemNFe, ',
//VSMorCab                       int(11)      NOT NULL  DEFAULT "0"
//VSMulFrnCab
    'CAST(vmi.VSMulFrnCab AS SIGNED) VSMulFrnCab, ',
//NFeSer
    'CAST(vmi.NFeSer + 0.000 AS SIGNED) NFeSer, ',
//NFeNum
    'CAST(vmi.NFeNum AS SIGNED) NFeNum, ',
//VSMulNFeCab
    'CAST(vmi.VSMulNFeCab AS SIGNED) VSMulNFeCab, ',
//JmpMovID                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.JmpMovID AS SIGNED) JmpMovID, ',
//JmpNivel1                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.JmpNivel1 AS SIGNED) JmpNivel1, ',
//JmpNivel2                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.JmpNivel2 AS SIGNED) JmpNivel2, ',
//RmsMovID                       int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.RmsMovID AS SIGNED) RmsMovID, ',
//RmsNivel1                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.RmsNivel1 AS SIGNED) RmsNivel1, ',
//RmsNivel2                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.RmsNivel2 AS SIGNED) RmsNivel2, ',
//GGXRcl                      int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.GGXRcl AS SIGNED) GGXRcl, ',
//RmsGGX                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.RmsGGX AS SIGNED) RmsGGX, ',
//JmpGGX                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.JmpGGX AS SIGNED) JmpGGX, ',
//DtCorrApo                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00"
    'CAST(vmi.DtCorrApo AS DATETIME) DtCorrApo, ',


//IxxMovIX                        tinyint(1)      NOT NULL  DEFAULT "0"
    'CAST(vmi.IxxMovIX AS UNSIGNED) IxxMovIX, ',
//IxxFolha                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.IxxFolha AS SIGNED) IxxFolha, ',
//IxxLinha                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.IxxLinha AS SIGNED) IxxLinha, ',


//CusFrtAvuls                     double(15,4) NOT NULL  DEFAULT "0.0000"
    'CAST(vmi.CusFrtAvuls AS DECIMAL (15,4)) CusFrtAvuls, ',
//CusFrtMOEnv                     double(15,4) NOT NULL  DEFAULT "0.0000"
    'CAST(vmi.CusFrtMOEnv AS DECIMAL (15,4)) CusFrtMOEnv, ',
//CusFrtMORet                     double(15,4) NOT NULL  DEFAULT "0.0000"
    'CAST(vmi.CusFrtMORet AS DECIMAL (15,4)) CusFrtMORet, ',
//CusFrtTrnsf                     double(15,4) NOT NULL  DEFAULT "0.0000"
    //'CAST(vmi.CusFrtTrnsf AS DECIMAL (15,4)) CusFrtTrnsf, ',


//CustoMOPc                      double(15,6) NOT NULL  DEFAULT "0.000000"
    'CAST(vmi.CustoMOPc AS DECIMAL (15,6)) CustoMOPc, ',

    //'vmi.*,
    SQL_Flds,
    'FROM ' + TMeuDB + '.' + TabMovVS_Tab(tab) + ' vmi ',
    SQL_Left,
    SQL_Wher,
    SQL_Group,
    Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
    '']);
end;

function TUnVS_CRC_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Where,
  SQL_Group: String; Tab: TTabToWork; TemIMEIMrt: Integer): String;
begin
  if GeraSQLTabMov(Result, Tab, TemIMEIMrt) then
    Result := Geral.ATS([
    'SELECT ' + VS_CRC_PF.TabMovVS_Fld_Pall(tab),
    SQL_NO_GGX(),
    'vsp.Nome NO_Pallet, ',
    'CAST(IF(COUNT(DISTINCT vmi.SerieFch, vmi.Ficha) <> 1, "0", "1") AS CHAR) Series_E_Fichas, ',
    'CAST(IF(COUNT(DISTINCT vmi.SerieFch) <> 1, 0, vmi.SerieFch) AS SIGNED) SerieFch, ',
    'CAST(IF(COUNT(DISTINCT vmi.Ficha) <> 1, 0, vmi.Ficha) AS SIGNED) Ficha, ',
    'CAST(IF(COUNT(DISTINCT vmi.Terceiro) <> 1, 0, vmi.Terceiro) AS SIGNED) Terceiro, ',
    'CAST(IF(COUNT(DISTINCT vmi.Marca) <> 1, "", vmi.Marca) AS CHAR) Marca, ',
    'CAST(IF(COUNT(DISTINCT(vmi.Terceiro))<>1, "", ',
    'IF((vmi.Terceiro=0) OR (vmi.Terceiro IS NULL), "",   ',
    'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome))) AS CHAR) NO_FORNECE,  ',
    'CAST(IF(COUNT(DISTINCT(vmi.SerieFch))<>1, "", ',
    'IF((vmi.SerieFch=0) OR (vmi.SerieFch IS NULL), "",   ',
    'vsf.Nome)) AS CHAR) NO_SerieFch,  ',
//  Codigo                         int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.Codigo AS SIGNED) Codigo, ',
//  Controle                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.Controle AS SIGNED) Controle, ',
//  MovimCod                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.MovimCod AS SIGNED) MovimCod, ',
//MovimNiv                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.MovimNiv AS SIGNED) MovimNiv, ',
//MovimTwn                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.MovimTwn AS SIGNED) MovimTwn, ',
//Empresa                        int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.Empresa AS SIGNED) Empresa, ',
//Terceiro                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.Terceiro AS SIGNED) Terceiro, ',
//CliVenda                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.CliVenda AS SIGNED) CliVenda, ',
//MovimID                        int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.MovimID AS SIGNED) MovimID, ',
//LnkIDXtr                       int(11)      NOT NULL  DEFAULT "0"
//LnkNivXtr1                     int(11)      NOT NULL  DEFAULT "0"
//LnkNivXtr2                     int(11)      NOT NULL  DEFAULT "0"
//DataHora                       datetime     NOT NULL  DEFAULT "0000-00-00 00:00:00"
//    'CAST(vmi.DataHora AS DATETIME) DataHora, ',
//Pallet                         int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.Pallet AS SIGNED) Pallet, ',
//GraGruX                        int(11)      NOT NULL  DEFAULT "0"
    'CAST(vmi.GraGruX AS SIGNED) GraGruX, ',
//Pecas                          double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(SUM(vmi.Pecas) AS DECIMAL (15,3)) Pecas, ',
//PesoKg                         double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(SUM(vmi.PesoKg) AS DECIMAL (15,3)) PesoKg, ',
//AreaM2                         double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(SUM(vmi.AreaM2) AS DECIMAL (15,2)) AreaM2, ',
//AreaP2                         double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(SUM(vmi.AreaP2) AS DECIMAL (15,2)) AreaP2, ',
//ValorT                         double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(SUM(vmi.ValorT) AS DECIMAL (15,2)) ValorT, ',
//SrcMovID                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.SrcMovID AS SIGNED) SrcMovID, ',
//SrcNivel1                      int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.SrcNivel1 AS SIGNED) SrcNivel1, ',
//SrcNivel2                      int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.SrcNivel2 AS SIGNED) SrcNivel2, ',
//SrcGGX                         int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.SrcGGX AS SIGNED) SrcGGX, ',
//SdoVrtPeca                     double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(SUM(vmi.SdoVrtPeca) AS DECIMAL (15,3)) SdoVrtPeca, ',
//SdoVrtPeso                     double(15,3) NOT NULL  DEFAULT "0.000"
    'CAST(SUM(vmi.SdoVrtPeso) AS DECIMAL (15,3)) SdoVrtPeso, ',
//SdoVrtArM2                     double(15,2) NOT NULL  DEFAULT "0.00"
    'CAST(SUM(vmi.SdoVrtArM2) AS DECIMAL (15,2)) SdoVrtArM2 ',
//Observ                         varchar(255) NOT NULL
//    'CAST(vmi.Observ AS CHAR) Observ, ',
//SerieFch                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.SerieFch AS SIGNED) SerieFch, ',
//Ficha                          int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.Ficha AS SIGNED) Ficha, ',
//Misturou                       tinyint(1)   NOT NULL  DEFAULT "0"
//    'CAST(vmi.Misturou AS UNSIGNED) Misturou, ',
//FornecMO                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.FornecMO AS SIGNED) FornecMO, ',
//CustoMOKg                      double(15,6) NOT NULL  DEFAULT "0.000000"
//    'CAST(vmi.CustoMOKg AS DECIMAL (15,6)) CustoMOKg, ',
//CustoMOTot                     double(15,2) NOT NULL  DEFAULT "0.00"
//    'CAST(vmi.CustoMOTot AS DECIMAL (15,2)) CustoMOTot, ',
//ValorMP                        double(15,4) NOT NULL  DEFAULT "0.0000"
//    'CAST(vmi.ValorMP AS DECIMAL (15,4)) ValorMP, ',
//DstMovID                       int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.DstMovID AS SIGNED) DstMovID, ',
//DstNivel1                      int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.DstNivel1 AS SIGNED) DstNivel1, ',
//DstNivel2                      int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.DstNivel2 AS SIGNED) DstNivel2, ',
//DstGGX                         int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.DstGGX AS SIGNED) DstGGX, ',
//QtdGerPeca                     double(15,3) NOT NULL  DEFAULT "0.000"
//    'CAST(vmi.QtdGerPeca AS DECIMAL (15,3)) QtdGerPeca, ',
//QtdGerPeso                     double(15,3) NOT NULL  DEFAULT "0.000"
//    'CAST(vmi.QtdGerPeso AS DECIMAL (15,3)) QtdGerPeso, ',
//QtdGerArM2                     double(15,2) NOT NULL  DEFAULT "0.00"
//    'CAST(vmi.QtdGerArM2 AS DECIMAL (15,2)) QtdGerArM2, ',
//QtdGerArP2                     double(15,2) NOT NULL  DEFAULT "0.00"
//    'CAST(vmi.QtdGerArP2 AS DECIMAL (15,2)) QtdGerArP2, ',
//QtdAntPeca                     double(15,3) NOT NULL  DEFAULT "0.000"
//    'CAST(vmi.QtdAntPeca AS DECIMAL (15,3)) QtdAntPeca, ',
//QtdAntPeso                     double(15,3) NOT NULL  DEFAULT "0.000"
//    'CAST(vmi.QtdAntPeso AS DECIMAL (15,3)) QtdAntPeso, ',
//QtdAntArM2                     double(15,2) NOT NULL  DEFAULT "0.00"
//    'CAST(vmi.QtdAntArM2 AS DECIMAL (15,2)) QtdAntArM2, ',
//QtdAntArP2                     double(15,2) NOT NULL  DEFAULT "0.00"
//    'CAST(vmi.QtdAntArP2 AS DECIMAL (15,2)) QtdAntArP2, ',
//AptoUso                        tinyint(1)   NOT NULL  DEFAULT "1"
//NotaMPAG                       double(15,8) NOT NULL  DEFAULT "0.00000000"
//    'CAST(vmi.NotaMPAG AS DECIMAL (15,8)) NotaMPAG, ',
//Marca                          varchar(20)
//    'CAST(vmi.Marca AS CHAR) Marca, ',
//TpCalcAuto                     int(11)      NOT NULL  DEFAULT "-1"
//Zerado                         tinyint(1)   NOT NULL  DEFAULT "0"
//EmFluxo                        tinyint(1)   NOT NULL  DEFAULT "1"
//NotFluxo                       int(11)      NOT NULL  DEFAULT "0"
//FatNotaVNC                     double(15,8) NOT NULL  DEFAULT "0.00000000"
//FatNotaVRC                     double(15,8) NOT NULL  DEFAULT "0.00000000"
//PedItsLib                      int(11)      NOT NULL  DEFAULT "0"
//PedItsFin                      int(11)      NOT NULL  DEFAULT "0"
//PedItsVda                      int(11)      NOT NULL  DEFAULT "0"
//Lk                             int(11)                DEFAULT "0"
//DataCad                        date
//DataAlt                        date
//UserCad                        int(11)                DEFAULT "0"
//UserAlt                        int(11)                DEFAULT "0"
//AlterWeb                       tinyint(1)   NOT NULL  DEFAULT "1"
//Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"
//CustoMOM2                      double(15,6) NOT NULL  DEFAULT "0.000000"
//    'CAST(vmi.CustoMOM2 AS DECIMAL (15,6)) CustoMOM2, ',
//ReqMovEstq                     int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.ReqMovEstq AS SIGNED) ReqMovEstq, ',
//StqCenLoc                      int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.StqCenLoc AS SIGNED) StqCenLoc, ',
//ItemNFe                        int(11)      NOT NULL  DEFAULT "0"
//    'CAST(vmi.ItemNFe AS SIGNED) ItemNFe, ',
//VSMorCab                       int(11)      NOT NULL  DEFAULT "0"
    //'vmi.*,
    SQL_Flds,
    'FROM ' + VS_CRC_PF.TabMovVS_Tab(tab) + ' vmi ',
    SQL_LJ_GGX(),
    'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
    'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
    'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
    SQL_Left,
    SQL_Where,
    SQL_Group,
    Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
    '']);
end;

function TUnVS_CRC_PF.GetIDNiv(MovimID, MovimNiv: Integer): Integer;
begin
  Result := (MovimID * 1000) + MovimNiv;
end;

function TUnVS_CRC_PF.HabilitaComposVSAtivo(ID_TTW: Integer;
  Compos: array of TComponent): Boolean;
var
  Habilita: Boolean;
  I: Integer;
  PropInfo: PPropInfo;
begin
  Habilita := ID_TTW = 0;
  for I := Low(Compos) to High(Compos) do
  begin
    PropInfo := GetPropInfo(Compos[I], 'Enabled');
    if PropInfo <> nil then
    try
      Habilita := Habilita and (GetPropValue(Compos[I], 'Enabled') = True);
      SetPropValue(Compos[I], 'Enabled', Habilita);
    except
      Geral.MB_Erro(
      'Não foi possível definir a propriedade "Enabled" no objeto "' +
      TComponent(Compos[I]).Name + '"');
    end;
  end;
end;

function TUnVS_CRC_PF.HabilitaMenuInsOuAllVSAberto(Qry: TmySQLQuery;
  Data: TDateTime; Button: TWinControl; DefMenu: TPopupMenu;
  MenuItens: array of TMenuItem): Boolean;
  procedure ItensDeMenu(Item: TMenuItem; Dest: TComponent);
  var
    I: Integer;
    Novo: TMenuItem;
  begin
    VAR_SeqMenuItem := VAR_SeqMenuItem + 1;
    Novo := TMenuItem.Create(Button.Owner);
    //Novo.Name := 'Item_Menu_' + Item.Name;
    Novo.Name := 'Item_Menu_' + Item.Name + Geral.FFN(VAR_SeqMenuItem, 9);
    Novo.Caption := Item.Caption;
    Novo.OnClick := Item.OnClick;
    try
      if Dest is TPopupMenu then
        TPopupMenu(Dest).Items.Add(Novo)
      else
        TMenuItem(Dest).Add(Novo);
    except
      Geral.MB_Erro('ERRO!' + sLineBreak + Novo.Caption);
    end;
    // Sub-itens
    for I := 0 to Item.Count - 1 do
    begin
      ItensDeMenu(Item.Items[I], Novo);
    end;
  end;
var
  Habilita: Boolean;
  I: Integer;
  Menu: TPopupMenu;
begin
  if (Qry <> nil) and (Qry.State <> dsInactive) and (Qry.RecordCount = 0) then
    Habilita := True
  else if (Qry <> nil) and (Qry.State = dsInactive) then
    Habilita := True
  else
    Habilita := Geral.Periodo2000(Data) > (VAR_VS_PERIODO_BAL - 1);
  if Habilita then
    MyObjects.MostraPopUpDeBotao(DefMenu, Button)
  else
  begin
    Menu := TPopupMenu.Create(Button.Owner);
    VAR_SeqMenuItem := VAR_SeqMenuItem + 1;
    Menu.Name := 'Menu_' + Geral.FFN(VAR_SeqMenuItem, 9);
    //Menu.Items.Add(MenuItens);
    for I := Low(MenuItens) to High(MenuItens) do
    begin
      ItensDeMenu(MenuItens[I], Menu);
    end;
    MyObjects.MostraPopUpDeBotao(Menu, Button);
  end;
end;

function TUnVS_CRC_PF.IMEI_JaEstaNoArray(const IMEI: Integer;
  const Lista: array of Integer; var ItensLoc: Integer): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to Length(Lista) -1 do
  begin
    if Lista[I] = IMEI then
    begin
      Result := True;
      ItensLoc := ItensLoc + 1;
      Exit;
    end;
  end;
end;

procedure TUnVS_CRC_PF.ImprimeClassIMEI(IMEI, VSGerArt,
  MulCab_MovimCod: Integer);
begin
  if DBCheck.CriaFm(TFmVSImpClaIMEI, FmVSImpClaIMEI, afmoLiberado) then
  begin
    //FmVSImpClaIMEI.ShowModal;
    FmVSImpClaIMEI.FIMEI := IMEI;
    FmVSImpClaIMEI.FVSGerArt := VSGerArt;
    FmVSImpClaIMEI.FVSMulCab_MovimCod := MulCab_MovimCod;
    //
    FmVSImpClaIMEI.ImprimeClassIMEI();
    FmVSImpClaIMEI.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.ImprimeClassIMEIs(IMEIS, Mortos: array of Integer;
MostraForm: Boolean);
var
  I, N: Integer;
begin
  if DBCheck.CriaFm(TFmVSImpClaIMEIs, FmVSImpClaIMEIs, afmoLiberado) then
  begin
    //FmVSImpClaIMEIs.ShowModal;
    N := Length(IMEIs);
    SetLength(FmVSImpClaIMEIs.FIMEIs, N);
    for I := 0 to N -1 do
      FmVSImpClaIMEIs.FIMEIs[I] := IMEIs[I];
    //
    N := Length(Mortos);
    SetLength(FmVSImpClaIMEIs.FMortos, N);
    for I := 0 to N -1 do
      FmVSImpClaIMEIs.FMortos[I] := Mortos[I];
    //
    if MostraForm then
      FmVSImpClaIMEIs.ShowModal
    else
      FmVSImpClaIMEIs.ImprimeClassIMEI();
    FmVSImpClaIMEIs.Destroy;
  end;
end;

function TUnVS_CRC_PF.ImprimeEstoqueReal(Entidade, Filial,
  Ed00Terceiro_ValueVariant, RG00_Ordem1_ItemIndex, RG00_Ordem2_ItemIndex,
  RG00_Ordem3_ItemIndex, RG00_Ordem4_ItemIndex, RG00_Ordem5_ItemIndex,
  RG00_Agrupa_ItemIndex: Integer; Ck00DescrAgruNoItm_Checked: Boolean;
  Ed00StqCenCad_ValueVariant, RG00ZeroNegat_ItemIndex: Integer; FNO_EMPRESA,
  CB00StqCenCad_Text, CB00Terceiro_Text: String; TPDataRelativa_Date: TDateTime;
  Ck00DataCompra_Checked, Ck00EmProcessoBH_Checked: Boolean; DBG00GraGruY,
  DBG00GraGruX, DBG00CouNiv2: TdmkDBGridZTO; Qr00GraGruY, Qr00GraGruX,
  Qr00CouNiv2: TmySQLQuery; TableSrc, DataRetroativa: String;
  // 2021-01-09 ini
  //GraCusPrc: Integer;
  Relatorio: TRelatorioVSImpEstoque;
  // 2021-01-09 fim
  DataEstoque: TDateTime; Ed00NFeIni_ValueVariant,
  Ed00NFeFim_ValueVariant: Integer; Ck00Serie_Checked: Boolean;
  Ed00Serie_ValueVariant, MovimCod, ClientMO: Integer; MostraFrx: Boolean): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmVSImpEstoque, FmVSImpEstoque, afmoLiberado) then
  begin
    //FmVSImpEstoque.ShowModal;
    FmVSImpEstoque.FEntidade                  := Entidade;
    FmVSImpEstoque.FFilial                    := Filial;
    FmVSImpEstoque.Ed00Terceiro_ValueVariant  := Ed00Terceiro_ValueVariant;
    FmVSImpEstoque.RG00_Ordem1_ItemIndex      := RG00_Ordem1_ItemIndex;
    FmVSImpEstoque.RG00_Ordem2_ItemIndex      := RG00_Ordem2_ItemIndex;
    FmVSImpEstoque.RG00_Ordem3_ItemIndex      := RG00_Ordem3_ItemIndex;
    FmVSImpEstoque.RG00_Ordem4_ItemIndex      := RG00_Ordem4_ItemIndex;
    FmVSImpEstoque.RG00_Ordem5_ItemIndex      := RG00_Ordem5_ItemIndex;
    FmVSImpEstoque.RG00_Agrupa_ItemIndex      := RG00_Agrupa_ItemIndex;
    FmVSImpEstoque.Ck00DescrAgruNoItm_Checked := Ck00DescrAgruNoItm_Checked;
    FmVSImpEstoque.Ed00StqCenCad_ValueVariant := Ed00StqCenCad_ValueVariant;
    FmVSImpEstoque.RG00ZeroNegat_ItemIndex    := RG00ZeroNegat_ItemIndex;
    FmVSImpEstoque.FNO_EMPRESA                := FNO_EMPRESA;
    FmVSImpEstoque.CB00StqCenCad_Text         := CB00StqCenCad_Text;
    FmVSImpEstoque.CB00Terceiro_Text          := CB00Terceiro_Text;
    FmVSImpEstoque.TPDataRelativa_Date        := TPDataRelativa_Date;
    FmVSImpEstoque.Ck00DataCompra_Checked     := Ck00DataCompra_Checked;
    FmVSImpEstoque.Ck00EmProcessoBH_Checked   := Ck00EmProcessoBH_Checked;
    FmVSImpEstoque.DBG00GraGruY               := DBG00GraGruY;
    FmVSImpEstoque.DBG00GraGruX               := DBG00GraGruX;
    FmVSImpEstoque.DBG00CouNiv2               := DBG00CouNiv2;
    FmVSImpEstoque.Qr00GraGruY                := Qr00GraGruY;
    FmVSImpEstoque.Qr00GraGruX                := Qr00GraGruX;
    FmVSImpEstoque.Qr00CouNiv2                := Qr00CouNiv2;
    FmVSImpEstoque.FTableSrc                  := TableSrc;
    FmVSImpEstoque.FDataRetroativa            := DataRetroativa;
    FmVSImpEstoque.FDataEstoque               := DataEstoque;
    //FmVSImpEstoque.FGraCusPrc                 := GraCusPrc;
    FmVSImpEstoque.FRelatorio                 := Relatorio;
    FmVSImpEstoque.FEd00NFeIni_ValueVariant   := Ed00NFeIni_ValueVariant;
    FmVSImpEstoque.FEd00NFeFim_ValueVariant   := Ed00NFeFim_ValueVariant;
    FmVSImpEstoque.FCk00Serie_Checked         := Ck00Serie_Checked;
    FmVSImpEstoque.FEd00Serie_ValueVariant    := Ed00Serie_ValueVariant;
    FmVSImpEstoque.FMovimCod                  := MovimCod;
    FmVSImpEstoque.FClientMO                  := ClientMO;
    FmVSImpEstoque.FMostraFrx                 := MostraFrx;
    //
    Result := FmVSImpEstoque.ImprimeEstoque();
    FmVSImpEstoque.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.ImprimeIMEI(IMEIs: array of Integer;
  VSImpImeiKind: TXXImpImeiKind; LPFMO, FNFeRem: String; QryCab: TmySQLQuery);
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmVSImpIMEI, FmVSImpIMEI, afmoLiberado) then
  begin
    //FmVSImpIMEI.ShowModal;
    FmVSImpIMEI.FQryCab := QryCab;
    FmVSImpIMEI.FVSImpImeiKind := VSImpImeiKind;
    FmVSImpIMEI.FLPFMO  := LPFMO;
    FmVSImpIMEI.FNFeRem := FNFeRem;
    SetLength(FmVSImpIMEI.FControles, Length(IMEIs));
    for I := Low(IMEIs) to High(IMEIs) do
      FmVSImpIMEI.FControles[I] := IMEIs[I];
    FmVSImpIMEI.ImprimeIMEI();
    FmVSImpIMEI.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.ImprimeOrdem(MovimID: TEstqMovimID; Empresa, Codigo,
  MovimCod: Integer);
begin
  if DBCheck.CriaFm(TFmVSImpOrdem, FmVSImpOrdem, afmoLiberado) then
  begin
    //FmVSImpOrdem.ShowModal;
    FmVSImpOrdem.FEmpresa     := Empresa;
    FmVSImpOrdem.FCodigo      := Codigo;
    FmVSImpOrdem.FMovimCod    := MovimCod;
    FmVSImpOrdem.FEstqMovimID := MovimID;
    //
    FmVSImpOrdem.ImprimeOrdemPreenchida();//(MovimID, Empresa, Codigo, MovimCod);
    FmVSImpOrdem.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.ImprimeOXsAbertas(RG18Ordem1_ItemIndex,
  RG18Ordem2_ItemIndex, RG18Ordem3_ItemIndex, RG18Agrupa_ItemIndex: Integer;
  MovimIDs: array of TEstqMovimID; Ck18Cor_Checked,
  Ck18Especificos_Checked: Boolean; Ed18Especificos_Text: String;
  Ed18Fornecedor_ValueVariant: Integer);
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmVSImpIMEI, FmVSImpIMEI, afmoLiberado) then
  begin
    //FmVSImpIMEI.ShowModal;
    //FmVSImpIMEI.FLayout := Layout;
    if Length(MovimIDs) = 1 then
      FmVSImpIMEI.ImprimeOPsAbertasUni(RG18Ordem1_ItemIndex, RG18Ordem2_ItemIndex,
        RG18Ordem3_ItemIndex, RG18Agrupa_ItemIndex, MovimIDs[0])
    else
      FmVSImpIMEI.ImprimeOPsAbertasMul(RG18Ordem1_ItemIndex, RG18Ordem2_ItemIndex,
        RG18Ordem3_ItemIndex, RG18Agrupa_ItemIndex, MovimIDs, Ck18Cor_Checked,
        Ck18Especificos_Checked, Ed18Especificos_Text,
        Ed18Fornecedor_ValueVariant);
    FmVSImpIMEI.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.ImprimePackListsIMEIs(Empresa: Integer;
  IMEIS: array of Integer; Vertical: Boolean);
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmVSImpPackList, FmVSImpPackList, afmoLiberado) then
  begin
    //FmVSImpPackList.ShowModal;
    SetLength(FmVSImpPackList.FIMEIs, Length(IMEIs));
    for I := Low(IMEIs) to High(IMEIs) do
      FmVSImpPackList.FIMEIs[I] := IMEIs[I];
    FmVSImpPackList.FEmpresa := Empresa;
    FmVSImpPackList.ImprimeIMEIs(Vertical);
    FmVSImpPackList.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.ImprimePallets(Empresa: Integer;
  Pallets: array of Integer; VSMovImp4, VSLstPalBox: String;
  InfoNO_PALLET: Boolean; DestImprFichaPallet: TDestImprFichaPallet;
  ImpEmpresa: Boolean; EmptyLabels: Integer);
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmVSImpPallet, FmVSImpPallet, afmoLiberado) then
  begin
    //FmVSImpPallet.ShowModal;
    SetLength(FmVSImpPallet.FPallets, Length(Pallets));
    for I := Low(Pallets) to High(Pallets) do
      FmVSImpPallet.FPallets[I] := Pallets[I];
    FmVSImpPallet.FEmpresa_Cod := Empresa;
    FmVSImpPallet.FInfoNO_PALLET := InfoNO_PALLET;
    FmVSImpPallet.FDestImprFichaPallet := DestImprFichaPallet;
    FmVSImpPallet.FImpEmpresa := ImpEmpresa;
    FmVSImpPallet.FVSMovImp4  := VSMovImp4;
    if Length(Pallets) = 0 then
    begin
      FmVSImpPallet.ReopenGraGruX();
      FmVSImpPallet.PreparaPallets(VSMovImp4, VSLstPalBox, EmptyLabels);
      FmVSImpPallet.TbVSMovImp4.DataBase := DModG.MyPID_DB;
      FmVSImpPallet.TbVSMovImp4.Filter := 'Ativo <> 1';
      FmVSImpPallet.TbVSMovImp4.Filtered := True;
      FmVSImpPallet.TbVSMovImp4.TableName := VSMovImp4; //'_vsmovimp4_fmvsmovimp';
      FmVSImpPallet.TbVSMovImp4.Active := True;
      FmVSImpPallet.ShowModal;
    end else
    begin
      FmVSImpPallet.PreparaPallets(VSMovImp4, VSLstPalBox, EmptyLabels);
      FmVSImpPallet.ImprimePallets(VSMovImp4, sqlordDESC);
    end;
    FmVSImpPallet.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.ImprimePallet_Unico(Empresa, ClientMO, Pallet: Integer;
  JanTab: String; InfoNO_PALLET: Boolean);
const
  GraGruX  = 0;
  CouNiv2  = 0;
  CouNiv1  = 0;
  StqCenCad = 0;
  StqCenLoc = 0;
  GraGruYs = '';
  VSLstPalBox = '';
  SQL_Especificos = '';
  MovimID  = 0;
  MovimCod = 0;
var
  Tabela: String;
  //I, GraGruX, Empresa, CouNiv2, CouNiv1: Integer;
  //SQL_Empresa, GraGruYs: String;
begin
  if MyObjects.FIC(Empresa = 0, nil, 'Informe a empresa') then
    Exit;
  (*
  GraGruYs := '';
  if (DBG04GraGruY.SelectedRows.Count > 0) and
  (DBG04GraGruY.SelectedRows.Count < Qr04GraGruY.RecordCount) then
  begin
    for I := 0 to DBG04GraGruY.SelectedRows.Count - 1 do
    begin
      Qr04GraGruY.GotoBookmark(DBG04GraGruY.SelectedRows.Items[I]);
      //
      if GraGruYs <> '' then
        GraGruYs := GraGruYs + ',';
      GraGruYs := GraGruYs + Geral.FF0(Qr04GraGruYCodigo.Value);
    end;
  end;
  GraGruX := Ed04GraGruX.ValueVariant;
  CouNiv2 := Ed04CouNiv2.ValueVariant;
  CouNiv1 := Ed04CouNiv1.ValueVariant;
  *)
  if PesquisaPallets(Empresa, ClientMO, CouNiv2, CouNiv1, StqCenCad, StqcenLoc,
  JanTab, //GraGruYs, [GraGruX], [Pallet], Tabela) then
  GraGruYs, [], [Pallet], SQL_Especificos, TEstqMovimID(MovimID), MovimCod,
  Tabela) then
    ImprimePallets(Empresa, [Pallet], Tabela, VSLstPalBox, InfoNO_PALLET, difpPapelA4);
end;

procedure TUnVS_CRC_PF.ImprimePallet_Varios(Empresa: Integer;
  Pallets: array of Integer; JanTab: String; InfoNO_PALLET: Boolean);
const
  GraGruX  = 0;
  CouNiv2  = 0;
  CouNiv1  = 0;
  ClientMO = 0;
  StqCenCad = 0;
  StqCenLoc = 0;
  GraGruYs = '';
  VSLstPalBox = '';
  SQL_Especificos = '';
  MovimID  = 0;
  MovimCod = 0;
var
  Tabela: String;
begin
  if MyObjects.FIC(Empresa = 0, nil, 'Informe a empresa') then
    Exit;
  if PesquisaPallets(Empresa, ClientMO, CouNiv2, CouNiv1, StqCenCad, StqCenLoc,
  JanTab, GraGruYs, [], Pallets, SQL_Especificos, TEstqMovimID(MovimID),
  MovimCod, Tabela) then
    ImprimePallets(Empresa, Pallets, Tabela, VSLstPalBox, InfoNO_PALLET, difpPapelA4);
end;

procedure TUnVS_CRC_PF.ImprimePWE_OP_Fluxo(Codigo, Modelo: Integer);
begin
  if DBCheck.CriaFm(TFmVSImpOSFluxo, FmVSImpOSFluxo, afmoLiberado) then
  begin
    //FmVSImpOSFluxos.ShowModal;
    FmVSImpOSFluxo.ImprimeOS(Codigo, Modelo);
    FmVSImpOSFluxo.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.ImprimeReclassOC(OC, CacCod: Integer);
begin
  if DBCheck.CriaFm(TFmVSImpRecla, FmVSImpRecla, afmoLiberado) then
  begin
    //FmVSImpReclas.ShowModal;
    FmVSImpRecla.ImprimeReclassOC(OC, CacCod);
    FmVSImpRecla.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormStqCenCad(StqCenCad, StqCenLoc: Integer);
var
  Qry: TmySQLQuery;
  Codigo: Integer;
begin
  Codigo := 0;
  if DBCheck.CriaFm(TFmStqCenCad, FmStqCenCad, afmoNegarComAviso) then
  begin
    if StqCenCad <> 0 then
      Codigo := StqCenCad
    else
    if StqCenloc <> 0 then
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Codigo  ',
        'FROM stqcenloc ',
        'WHERE Controle=' + Geral.FF0(StqCenLoc),
        '']);
        Codigo := Qry.FieldByName('Codigo').AsInteger;
      finally
        Qry.Free;
      end;
    end;
    FmStqCenCad.LocCod(Codigo, Codigo);
    FmStqCenCad.ShowModal;
    if FmStqCenCad.QrStqCenLoc.State <> dsInactive then
      VAR_CAD_STQCENLOC := FmStqCenCad.QrStqCenLocControle.Value
    else
      VAR_CAD_STQCENLOC := 0;
    FmStqCenCad.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSClaArtPrpMDz(Pallet1, Pallet2, Pallet3,
  Pallet4, Pallet5, Pallet6: Integer; FormAFechar: TForm; IMEI, DV,
  StqCenLoc: Integer);
var
  Codigo, CacCod: Integer;
  MovimID: TEstqMovimID;
begin
  if DBCheck.CriaFm(TFmVSClaArtPrpMDz, FmVSClaArtPrpMDz, afmoNegarComAviso) then
  begin
    FmVSClaArtPrpMDz.ImgTipo.SQLType := stIns;
    //
    FmVSClaArtPrpMDz.EdPallet1.ValueVariant := Pallet1;
    FmVSClaArtPrpMDz.CBPallet1.KeyValue     := Pallet1;
    //
    FmVSClaArtPrpMDz.EdPallet2.ValueVariant := Pallet2;
    FmVSClaArtPrpMDz.CBPallet2.KeyValue     := Pallet2;
    //
    FmVSClaArtPrpMDz.EdPallet3.ValueVariant := Pallet3;
    FmVSClaArtPrpMDz.CBPallet3.KeyValue     := Pallet3;
    //
    FmVSClaArtPrpMDz.EdPallet4.ValueVariant := Pallet4;
    FmVSClaArtPrpMDz.CBPallet4.KeyValue     := Pallet4;
    //
    FmVSClaArtPrpMDz.EdPallet5.ValueVariant := Pallet5;
    FmVSClaArtPrpMDz.CBPallet5.KeyValue     := Pallet5;
    //
    FmVSClaArtPrpMDz.EdPallet6.ValueVariant := Pallet6;
    FmVSClaArtPrpMDz.CBPallet6.KeyValue     := Pallet6;
    //
    FmVSClaArtPrpMDz.EdStqCenLoc.ValueVariant := StqCenLoc;
    FmVSClaArtPrpMDz.CBStqCenLoc.KeyValue     := StqCenLoc;
    //
    if IMEI <> 0 then
    begin
      FmVSClaArtPrpMDz.EdDVIMEI.ValueVariant := DV;
      FmVSClaArtPrpMDz.EdIMEI.ValueVariant := IMEI;
      FmVSClaArtPrpMDz.CBIMEI.KeyValue     := IMEI;
      //
      FmVSClaArtPrpMDz.SbIMEIClick(Self);
      FmVSClaArtPrpMDz.BtOKClick(Self);
      //FmVSClaArtPrpMDz.ShowModal;
      Codigo := FmVSClaArtPrpMDz.FCodigo;
      CacCod := FmVSClaArtPrpMDz.FCacCod;
      MovimID := FmVSClaArtPrpMDz.FMovimID;
      //
      FmVSClaArtPrpMDz.Destroy;
      //
      if LowerCase(FormAFechar.Name) = LowerCase(FmVSClassifOneNw3.Name) then
      begin
        TFmVSClassifOneNw3(FormAFechar).FCodigo := Codigo;
        TFmVSClassifOneNw3(FormAFechar).FCacCod := CacCod;
        TFmVSClassifOneNw3(FormAFechar).FMovimID := MovimID;
        TFmVSClassifOneNw3(FormAFechar).ReopenVSPaClaCab();
        TFmVSClassifOneNw3(FormAFechar).ReopenVSPaClaCab();
      end else
      if LowerCase(FormAFechar.Name) = LowerCase(FmVSClassifOneNew.Name) then
      begin
        TFmVSClassifOneNew(FormAFechar).FCodigo := Codigo;
        TFmVSClassifOneNew(FormAFechar).FCacCod := CacCod;
        TFmVSClassifOneNew(FormAFechar).FMovimID := MovimID;
        TFmVSClassifOneNew(FormAFechar).ReopenVSPaClaCab();
        TFmVSClassifOneNew(FormAFechar).ReopenVSPaClaCab();
      end;
    end else
    begin
      FmVSClaArtPrpMDz.ShowModal;
      Codigo := FmVSClaArtPrpMDz.FCodigo;
      CacCod := FmVSClaArtPrpMDz.FCacCod;
      MovimID := FmVSClaArtPrpMDz.FMovimID;
      FmVSClaArtPrpMDz.Destroy;
      //
      if Codigo <> 0 then
      begin
        if FormAFechar <> nil then
        begin
          //FormAFechar.Close;
          if LowerCase(FormAFechar.Name) = LowerCase(FmVSClassifOneNw3.Name) then
          begin
            TFmVSClassifOneNw3(FormAFechar).FCodigo := Codigo;
            TFmVSClassifOneNw3(FormAFechar).FCacCod := CacCod;
            TFmVSClassifOneNw3(FormAFechar).FMovimID := MovimID;
            TFmVSClassifOneNw3(FormAFechar).ReopenVSPaClaCab();
          end else
          if LowerCase(FormAFechar.Name) = LowerCase(FmVSClassifOneNew.Name) then
          begin
            TFmVSClassifOneNew(FormAFechar).FCodigo := Codigo;
            TFmVSClassifOneNew(FormAFechar).FCacCod := CacCod;
            TFmVSClassifOneNew(FormAFechar).FMovimID := MovimID;
            TFmVSClassifOneNew(FormAFechar).ReopenVSPaClaCab();
            //
          end;
        end else
          MostraFormVSClassifOne(Codigo, CacCod, MovimID);
      end;
    end;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSClaArtPrpQnz(Pallets: array of Integer;
  FormAFechar: TForm; IMEI, DV, StqCenLoc: Integer);
var
  I: Integer;
  Codigo, CacCod: Integer;
  MovimID: TEstqMovimID;
begin
  if DBCheck.CriaFm(TFmVSClaArtPrpQnz, FmVSClaArtPrpQnz, afmoNegarComAviso) then
  begin
    FmVSClaArtPrpQnz.ImgTipo.SQLType := stIns;
    //
    FmVSClaArtPrpQnz.EdStqCenLoc.ValueVariant := StqCenLoc;
    FmVSClaArtPrpQnz.CBStqCenLoc.KeyValue     := StqCenLoc;
    //
    for I := 1 to High(Pallets) do
    //for I := 0 to High(Pallets - 1) do
    begin
      FmVSClaArtPrpQnz.FEdPallet[I].ValueVariant := Pallets[I];
      FmVSClaArtPrpQnz.FCBPallet[I].KeyValue     := Pallets[I];
    end;
    //
    if IMEI <> 0 then
    begin
      FmVSClaArtPrpQnz.EdDVIMEI.ValueVariant := DV;
      FmVSClaArtPrpQnz.EdIMEI.ValueVariant := IMEI;
      FmVSClaArtPrpQnz.CBIMEI.KeyValue     := IMEI;
      //
      FmVSClaArtPrpQnz.SbIMEIClick(Self);
      FmVSClaArtPrpQnz.BtOKClick(Self);
      //FmVSClaArtPrpQnz.ShowModal;
      Codigo := FmVSClaArtPrpQnz.FCodigo;
      CacCod := FmVSClaArtPrpQnz.FCacCod;
      MovimID := FmVSClaArtPrpQnz.FMovimID;
      //
      FmVSClaArtPrpQnz.Destroy;
      //
      TFmVSClassifOneNw3(FormAFechar).FCodigo := Codigo;
      TFmVSClassifOneNw3(FormAFechar).FCacCod := CacCod;
      TFmVSClassifOneNw3(FormAFechar).FMovimID := MovimID;
      TFmVSClassifOneNw3(FormAFechar).ReopenVSPaClaCab();
      TFmVSClassifOneNw3(FormAFechar).ReopenVSPaClaCab();
    end else
    begin
      FmVSClaArtPrpQnz.ShowModal;
      Codigo := FmVSClaArtPrpQnz.FCodigo;
      CacCod := FmVSClaArtPrpQnz.FCacCod;
      MovimID := FmVSClaArtPrpQnz.FMovimID;
      FmVSClaArtPrpQnz.Destroy;
      //
      if Codigo <> 0 then
      begin
        if FormAFechar <> nil then
        begin
          //FormAFechar.Close;
          TFmVSClassifOneNw3(FormAFechar).FCodigo := Codigo;
          TFmVSClassifOneNw3(FormAFechar).FCacCod := CacCod;
          TFmVSClassifOneNw3(FormAFechar).FMovimID := MovimID;
          TFmVSClassifOneNw3(FormAFechar).ReopenVSPaClaCab();
          //
        end else
          MostraFormVSClassifOne(Codigo, CacCod, MovimID, 3);
      end;
    end;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSClaArtSel_FRMP;
var
  Codigo, CacCod: Integer;
  MovimID: TEstqMovimID;
begin
  if DBCheck.CriaFm(TFmVSClaArtSelB, FmVSClaArtSelB, afmoNegarComAviso) then
  begin
    FmVSClaArtSelB.ImgTipo.SQLType := stIns;
    FmVSClaArtSelB.ShowModal;
    Codigo := FmVSClaArtSelB.FCodigo;
    CacCod := FmVSClaArtSelB.FCacCod;
    MovimID := FmVSClaArtSelB.FMovimID;
    FmVSClaArtSelB.Destroy;
    //
    if Codigo <> 0 then
      MostraFormVSClassifOne(Codigo, CacCod, MovimID);
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSClaArtSel_IMEI;
var
  Codigo, CacCod: Integer;
  MovimID: TEstqMovimID;
begin
  if DBCheck.CriaFm(TFmVSClaArtSelA, FmVSClaArtSelA, afmoNegarComAviso) then
  begin
    FmVSClaArtSelA.ImgTipo.SQLType := stIns;
    FmVSClaArtSelA.ShowModal;
    Codigo := FmVSClaArtSelA.FCodigo;
    CacCod := FmVSClaArtSelA.FCacCod;
    MovimID := FmVSClaArtSelA.FMovimID;
    FmVSClaArtSelA.Destroy;
    //
    if Codigo <> 0 then
      MostraFormVSClassifOne(Codigo, CacCod, MovimID);
  end;
end;

{?:><
procedure TUnVS_CRC_PF.MostraFormVSClaPalPrpQnz(Pallets: array of Integer;
  FormAFechar: TForm; IMEI, DV: Integer);
var
  I: Integer;
  Codigo, CacCod: Integer;
  MovimID: TEstqMovimID;
begin
  if DBCheck.CriaFm(TFmVSClaPalPrpQnz, FmVSClaPalPrpQnz, afmoNegarComAviso) then
  begin
    FmVSClaPalPrpQnz.ImgTipo.SQLType := stIns;
    //
    for I := 1 to High(Pallets) do
    begin
      FmVSClaPalPrpQnz.FEdPallet[I].ValueVariant := Pallets[I];
      FmVSClaPalPrpQnz.FCBPallet[I].KeyValue     := Pallets[I];
    end;
    //
    if IMEI <> 0 then
    begin
      FmVSClaPalPrpQnz.EdDVIMEI.ValueVariant := DV;
      FmVSClaPalPrpQnz.EdIMEI.ValueVariant := IMEI;
      FmVSClaPalPrpQnz.CBIMEI.KeyValue     := IMEI;
      //
      FmVSClaPalPrpQnz.SbIMEIClick(Self);
      FmVSClaPalPrpQnz.BtOKClick(Self);
      //FmVSClaPalPrpQnz.ShowModal;
      Codigo := FmVSClaPalPrpQnz.FCodigo;
      CacCod := FmVSClaPalPrpQnz.FCacCod;
      MovimID := FmVSClaPalPrpQnz.FMovimID;
      //
      FmVSClaPalPrpQnz.Destroy;
      //
      TFmVSClassifOneNw3(FormAFechar).FCodigo := Codigo;
      TFmVSClassifOneNw3(FormAFechar).FCacCod := CacCod;
      TFmVSClassifOneNw3(FormAFechar).FMovimID := MovimID;
      TFmVSClassifOneNw3(FormAFechar).ReopenVSPaClaCab();
      TFmVSClassifOneNw3(FormAFechar).ReopenVSPaClaCab();
    end else
    begin
      FmVSClaPalPrpQnz.ShowModal;
      Codigo := FmVSClaPalPrpQnz.FCodigo;
      CacCod := FmVSClaPalPrpQnz.FCacCod;
      MovimID := FmVSClaPalPrpQnz.FMovimID;
      FmVSClaPalPrpQnz.Destroy;
      //
      if Codigo <> 0 then
      begin
        if FormAFechar <> nil then
        begin
          //FormAFechar.Close;
          TFmVSClassifOneNw3(FormAFechar).FCodigo := Codigo;
          TFmVSClassifOneNw3(FormAFechar).FCacCod := CacCod;
          TFmVSClassifOneNw3(FormAFechar).FMovimID := MovimID;
          TFmVSClassifOneNw3(FormAFechar).ReopenVSPaClaCab();
          //
        end else
          MostraFormVSClassifOne(Codigo, CacCod, MovimID, 3);
      end;
    end;
  end;
end;
}

procedure TUnVS_CRC_PF.MostraFormVSClassifOne(Codigo, CacCod: Integer;
  MovimID: TEstqMovimID; FormPreDef: Integer);
var
  Form, Max: Integer;
  Ord_Txt: String;
  Qry: TmySQLQuery;
begin
(*
  if FormPreDef <> 0 then
    Form := FormPreDef
  else
  begin
    Form := MyObjects.SelRadioGroup('Seleção de Janela',
    'Selecione a Janela', [
    'Atual',
    'Novo (2)',
    'Novo (3)'], 0);
    Form := Form + 1;
  end;
  case Form of
    1:
    begin
      if DBCheck.CriaFm(TFmVSClassifOneNew, FmVSClassifOneNew, afmoNegarComAviso) then
      begin
        FmVSClassifOneNew.FCodigo := Codigo;
        FmVSClassifOneNew.FCacCod := CacCod;
        FmVSClassifOneNew.FMovimID := MovimID;
        FmVSClassifOneNew.ReopenVSPaClaCab();
        //
        FmVSClassifOneNew.ShowModal;
        FmVSClassifOneNew.Destroy;
      end;
    end;
    2:
    begin
      if DBCheck.CriaFm(TFmVSClassifOneNw2, FmVSClassifOneNw2, afmoNegarComAviso) then
      begin
        FmVSClassifOneNw2.FCodigo := Codigo;
        FmVSClassifOneNw2.FCacCod := CacCod;
        FmVSClassifOneNw2.FMovimID := MovimID;
        FmVSClassifOneNw2.ReopenVSPaClaCab();
        //
        FmVSClassifOneNw2.ShowModal;
        FmVSClassifOneNw2.Destroy;
      end;
    end;
    3:
    begin
*)
      if DBCheck.CriaFm(TFmVSClassifOneNw3, FmVSClassifOneNw3, afmoNegarComAviso) then
      begin
        Qry := TmySQLQuery.Create(Dmod);
        try
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT * ',
          'FROM vspaclacaba ',
          'WHERE Codigo=' + Geral.FF0(Codigo),
          '']);
          try
            if Qry.FieldByName('LstPal15').AsInteger <> 0 then Max := 15 else
            if Qry.FieldByName('LstPal14').AsInteger <> 0 then Max := 14 else
            if Qry.FieldByName('LstPal13').AsInteger <> 0 then Max := 13 else
            if Qry.FieldByName('LstPal12').AsInteger <> 0 then Max := 12 else
            if Qry.FieldByName('LstPal11').AsInteger <> 0 then Max := 11 else
            if Qry.FieldByName('LstPal10').AsInteger <> 0 then Max := 10 else
            if Qry.FieldByName('LstPal09').AsInteger <> 0 then Max := 09 else
            if Qry.FieldByName('LstPal08').AsInteger <> 0 then Max := 08 else
            if Qry.FieldByName('LstPal07').AsInteger <> 0 then Max := 07 else
            Max := 6;
          except
            Max := 15;
          end;
        finally
          Qry.Free;
        end;
        FmVSClassifOneNw3.DefineBoxes(Max);
        FmVSClassifOneNw3.FCodigo := Codigo;
        FmVSClassifOneNw3.FCacCod := CacCod;
        FmVSClassifOneNw3.FMovimID := MovimID;
        FmVSClassifOneNw3.ReopenVSPaClaCab();
        //
        FmVSClassifOneNw3.ShowModal;
        FmVSClassifOneNw3.Destroy;
      end;
(*
    end;
  end;
*)
end;

procedure TUnVS_CRC_PF.MostraFormVSClassifOneRetIMEI_06(Pallet1, Pallet2, Pallet3,
  Pallet4, Pallet5, Pallet6: Integer; Form: TForm; BoxMax: Integer);
begin
  if DBCheck.CriaFm(TFmVSClassifOneRetIMEI, FmVSClassifOneRetIMEI, afmoNegarComAviso) then
  begin
    FmVSClassifOneRetIMEI.FForm := Form;
    FmVSClassifOneRetIMEI.FPallet01 := Pallet1;
    FmVSClassifOneRetIMEI.FPallet02 := Pallet2;
    FmVSClassifOneRetIMEI.FPallet03 := Pallet3;
    FmVSClassifOneRetIMEI.FPallet04 := Pallet4;
    FmVSClassifOneRetIMEI.FPallet05 := Pallet5;
    FmVSClassifOneRetIMEI.FPallet06 := Pallet6;
    //
    FmVSClassifOneRetIMEI.FBoxMax  := BoxMax;
    //
    FmVSClassifOneRetIMEI.ShowModal;
    //
    FmVSClassifOneRetIMEI.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSClassifOneRetIMEI_15(Pallet01, Pallet02,
  Pallet03, Pallet04, Pallet05, Pallet06, Pallet07, Pallet08, Pallet09,
  Pallet10, Pallet11, Pallet12, Pallet13, Pallet14, Pallet15: Integer;
  Form: TForm; BoxMax: Integer);
begin
  if DBCheck.CriaFm(TFmVSClassifOneRetIMEI, FmVSClassifOneRetIMEI, afmoNegarComAviso) then
  begin
    FmVSClassifOneRetIMEI.FForm := Form;
    FmVSClassifOneRetIMEI.FPallet01 := Pallet01;
    FmVSClassifOneRetIMEI.FPallet02 := Pallet02;
    FmVSClassifOneRetIMEI.FPallet03 := Pallet03;
    FmVSClassifOneRetIMEI.FPallet04 := Pallet04;
    FmVSClassifOneRetIMEI.FPallet05 := Pallet05;
    FmVSClassifOneRetIMEI.FPallet06 := Pallet06;
    FmVSClassifOneRetIMEI.FPallet07 := Pallet07;
    FmVSClassifOneRetIMEI.FPallet08 := Pallet08;
    FmVSClassifOneRetIMEI.FPallet09 := Pallet09;
    FmVSClassifOneRetIMEI.FPallet10 := Pallet10;
    FmVSClassifOneRetIMEI.FPallet11 := Pallet11;
    FmVSClassifOneRetIMEI.FPallet12 := Pallet12;
    FmVSClassifOneRetIMEI.FPallet13 := Pallet13;
    FmVSClassifOneRetIMEI.FPallet14 := Pallet14;
    FmVSClassifOneRetIMEI.FPallet15 := Pallet15;
    //
    FmVSClassifOneRetIMEI.FBoxMax  := BoxMax;
    //
    FmVSClassifOneRetIMEI.ShowModal;
    //
    FmVSClassifOneRetIMEI.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSCOPCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSCOPCab, FmVSCOPCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSCOPCab.LocCod(Codigo, Codigo);
    end;
    //
    FmVSCOPCab.ShowModal;
    FmVSCOPCab.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSDivCouMeio(MovimID: TEstqMovimID;
  Boxes: array of Boolean; LaTipo_Caption: String);
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmVSDivCouMeio, FmVSDivCouMeio, afmoNegarComAviso) then
  begin
    FmVSDivCouMeio.EhReclas := True;
    FmVSDivCouMeio.LaTipoArea.Caption := LaTipo_Caption;
(*
    FmVSDivCouMeio.FMovimID := MovimID;
    for I := 0 to VAR_CLA_ART_RIB_MAX_BOX - 1  do
    begin
      if Boxes[I] = True then
        FmVSDivCouMeio.FBoxes[I] := True
      else
        FmVSDivCouMeio.FBoxes[I] := False;
    end;
*)
    FmVSDivCouMeio.ShowModal;
    FmVSDivCouMeio.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSGeraArt(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmVSGerArtCab, FmVSGerArtCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSGerArtCab.LocCod(Codigo, Codigo);
      // Somente se for o controle de baixa vai localizar!
      if FmVSGerArtCab.QrVSGerArt015.State <> dsInactive then
        FmVSGerArtCab.QrVSGerArt015.Locate('Controle', Controle, []);
      //
    end;
    FmVSGerArtCab.ShowModal;
    FmVSGerArtCab.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSGerClaCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSGerClaCab, FmVSGerClaCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSGerClaCab.LocCod(Codigo, Codigo);
      //FmVSGerClaCab.ReopenVSPaClaCab();
    end;
    //
    FmVSGerClaCab.ShowModal;
    FmVSGerClaCab.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSGerRclCab(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmVSGerRclCab, FmVSGerRclCab, afmoNegarComAviso) then
  begin
    if (Codigo <> 0) (*and (Controle <> 0)*) then
      FmVSGerRclCab.LocCod(Codigo, Codigo);
    FmVSGerRclCab.ShowModal;
    FmVSGerRclCab.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSGruGGX(SQLType: TSQLType; Codigo, Bastidao,
  GGXAtu: Integer);
begin
{$IfDef sAllVS}
  if DBCheck.CriaFm(TFmVSGruGGX, FmVSGruGGX, afmoNegarComAviso) then
  begin
    FmVSGruGGX.ImgTipo.SQLType := SQLType;
    if SQLType = stUpd then
    begin
      Geral.MB_Erro('Falta implementar Upd!');
      FmVSGruGGX.EdCodigo.ValueVariant := Codigo;
    end else
    begin
      case TVSBastidao(Bastidao) of
        //vsbstdND=0,
        vsbstdIntegral: FmVSGruGGX.EdGGXIntegrl.ValueVariant := GGXAtu;
        vsbstdLaminado: FmVSGruGGX.EdGGXLaminad.ValueVariant := GGXAtu;
        vsbstdDivTripa: FmVSGruGGX.EdGGXDivTrip.ValueVariant := GGXAtu;
        vsbstdDivCurti: FmVSGruGGX.EdGGXDivCurt.ValueVariant := GGXAtu;
        //vsbstdRebaixad=5,
        //vsbstdDivSemiA=6,
        //vsbstdRebxSemi=7);
      end;
      FmVSGruGGX.ShowModal;
      FmVSGruGGX.Destroy;
    end;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TUnVS_CRC_PF.MostraFormVSInnCab(Codigo, VSInnIts, VSReclas: Integer);
begin
  if DBCheck.CriaFm(TFmVSInnCab, FmVSInnCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSInnCab.LocCod(Codigo, Codigo);
      //
      if (FmVSInnCab.QrVSInnIts.State <> dsInactive) and
        (FmVSInnCab.QrVSInnIts.RecordCount > 0)
      then
        FmVSInnCab.QrVSInnIts.Locate('Controle', VSInnIts, []);
      //
      if (FmVSInnCab.QrVSItsBxa.State <> dsInactive) and
        (FmVSInnCab.QrVSItsBxa.RecordCount > 0) then
      begin
        if not FmVSInnCab.QrVSItsBxa.Locate('Controle', VSReclas, []) then
        begin
          // tenta o item par
          FmVSInnCab.QrVSItsBxa.Locate('Controle', VSReclas + 1, []);
        end;
      end;
    end;
    FmVSInnCab.ShowModal;
    FmVSInnCab.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSMixClaCab;
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'VSMixClaCab', 60, ncGerlSeq1,
  'Cadastro de Mix de Classes',
  [], False, Null, [], [], False);
end;

procedure TUnVS_CRC_PF.MostraFormVSMovimID(MovimID: TEstqMovimID);
var
  MovID: Integer;
begin
  if DBCheck.CriaFm(TFmVSMovimID, FmVSMovimID, afmoNegarComAviso) then
  begin
    MovID := Integer(MovimID);
    if MovID <> 0 then
      FmVSMovimID.LocCod(MovID, MovID);
    FmVSMovimID.ShowModal;
    FmVSMovimID.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSMovImp(AbrirEmAba: Boolean;
  InOwner: TWincontrol; PageControl: TdmkPageControl; PageIndex: Integer);
var
  Form: TForm;
begin
  {$IFDEF sAllVS}
  if AbrirEmAba then
  begin
    Form := MyObjects.FormTDICria(TFmVSMovImp, InOwner, PageControl, True, True);
    //
    if Form <> nil then
    begin
      DModG.SelecionaEmpresaSeUnica(TFmVSMovImp(Form).EdEmpresa,
        TFmVSMovImp(Form).CBEmpresa);
      //
      if PageIndex <> -1 then
      begin
        TFmVSMovImp(Form).PCRelatorio.ActivePageIndex := PageIndex;
        TFmVSMovImp(Form).BtOKClick(TFmVSMovImp(Form).BtOK);
      end;
    end;
  end else
  begin
    if DBCheck.CriaFm(TFmVSMovImp, FmVSMovImp, afmoNegarComAviso) then
    begin
      DModG.SelecionaEmpresaSeUnica(FmVSMovImp.EdEmpresa, FmVSMovImp.CBEmpresa);
      if PageIndex <> -1 then
      begin
        FmVSMovImp.PCRelatorio.ActivePageIndex := PageIndex;
        FmVSMovImp.BtOKClick(FmVSMovImp.BtOK);
      end;
      FmVSMovImp.ShowModal;
      FmVSMovImp.Destroy;
    end;
  end;
  {$ELSE}
    Geral.MB_Info('Este Aplicativo não mostra a janela de impressão!');
  {$ENDIF}
end;

procedure TUnVS_CRC_PF.MostraFormVSMovIts(Controle: Integer);
begin
  if DBCheck.CriaFm(TFmVSMovIts, FmVSMovIts, afmoNegarComAviso) then
  begin
    if Controle <> 0 then
      FmVSMovIts.LocCod(Controle, Controle);
    FmVSMovIts.ShowModal;
    FmVSMovIts.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSMovItsAlt(Controle: Integer;
  AtualizaSaldoModoGenerico: Boolean; GroupBoxes: array of TEstqEditGB);
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmVSMovItsAlt, FmVSMovItsAlt, afmoNegarComAviso) then
  begin
    FmVSMovItsAlt.DefineImeiAEditar(Controle);
    for I := Low(GroupBoxes) to  High(GroupBoxes) do
    begin
      case GroupBoxes[I] of
        (*0*)eegbNone: ;//
        (*1*)eegbQtdOriginal: FmVSMovItsAlt.GBQtdOriginal.Enabled := True;
        (*2*)eegbDadosArtigo: FmVSMovItsAlt.GBDadosArtigo.Enabled := True;
        else Geral.MB_Aviso(
        '"TEstqEditGB" não implementado em "MostraFormVSMovItsAlt()"');
      end;
    end;
    FmVSMovItsAlt.FAtualizaSaldoModoGenerico := AtualizaSaldoModoGenerico;
    FmVSMovItsAlt.ShowModal;
    FmVSMovItsAlt.Destroy;
  end;
end;

function TUnVS_CRC_PF.MostraFormVSNFeOutCab(SQLType: TSQLType;
  QrVSOutNfeCab: TmySQLQuery; MovimCod, OriCod, Codigo, ide_serie,
  ide_nNF: Integer): Boolean;
begin
{$IfDef sAllVS}
  if DBCheck.CriaFm(TFmVSOutNfeCab, FmVSOutNfeCab, afmoNegarComAviso) then
  begin
    FmVSOutNfeCab.ImgTipo.SQLType          := SQLType;
    FmVSOutNfeCab.FQrIts                   := QrVSOutNfeCab;
    FmVSOutNfeCab.EdMovimCod.ValueVariant  := MovimCod;
    FmVSOutNfeCab.EdOriCod.ValueVariant    := OriCod;
    FmVSOutNfeCab.EdCodigo.ValueVariant    := Codigo;
    FmVSOutNfeCab.Edide_serie.ValueVariant := ide_serie;
    FmVSOutNfeCab.Edide_nNF.ValueVariant   := ide_nNF;
    if SQLType = stIns then
    begin
      //
    end else
    begin
      //
    end;
    FmVSOutNfeCab.ShowModal;
    Result := FmVSOutNfeCab.FInseriu;
    FmVSOutNfeCab.Destroy;
  end;
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TUnVS_CRC_PF.MostraFormVSNFeOutIts(SQLType: TSQLType; QrVSOutNfeIts,
  QrTribIncIts: TmySQLQuery; Codigo, MovimCod, Empresa: Integer;
  DataHora: TDateTime; SinalOrig: TSinal; DefMulNFe: TEstqDefMulFldEMxx;
  MovTypeCod: Integer; MovimID: TEstqMovimID; MovimNivsOri,
  MovimNivsDst: array of TEstqMovimNiv);
var
  I: Integer;
begin
{$IfDef sAllVS}
  if DBCheck.CriaFm(TFmVSOutNfeIts, FmVSOutNfeIts, afmoNegarComAviso) then
  begin
    FmVSOutNfeIts.ImgTipo.SQLType         := SQLType;
    FmVSOutNfeIts.FQrIts                  := QrVSOutNfeIts;
    FmVSOutNfeIts.EdCodigo.ValueVariant   := Codigo;
    FmVSOutNfeIts.FQrTribIncIts           := QrTribIncIts;
    FmVSOutNfeIts.FMovimCod               := MovimCod;
    FmVSOutNfeIts.FEmpresa                := Empresa;
    FmVSOutNfeIts.FDataHora               := DataHora;
    FmVSOutNfeIts.FSinalOrig              := SinalOrig;
    FmVSOutNfeIts.FDefMulNFe              := DefMulNFe;
    FmVSOutNfeIts.FMovTypeCod             := MovTypeCod;
    FmVSOutNfeIts.FMovimID                := MovimID;
    //
    SetLength(FmVSOutNfeIts.FMovimNivsOri, Length(MovimNivsOri));
    for I := Low(MovimNivsOri) to High(MovimNivsOri) do
      FmVSOutNfeIts.FMovimNivsOri[I]      := MovimNivsOri[I];
    SetLength(FmVSOutNfeIts.FMovimNivsdst, Length(MovimNivsdst));
    for I := Low(MovimNivsdst) to High(MovimNivsdst) do
      FmVSOutNfeIts.FMovimNivsdst[I]      := MovimNivsdst[I];
    //
    if SQLType = stIns then
    begin
      FmVSOutNfeIts.EdItemNFe.Enabled := True;
      FmVSOutNfeIts.LaItemNFe.Enabled := True;
      //
      FmVSOutNFeIts.BuscaDadosPallets();
(*
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [

      finally
        Qry.Free;
      end;
*)
    end else
    begin
      FmVSOutNFeIts.EdControle.ValueVariant   := QrVSOutNfeIts.FieldByName('Controle').AsInteger;
      FmVSOutNFeIts.EdItemNFe.ValueVariant    := QrVSOutNfeIts.FieldByName('ItemNFe').AsInteger;
      //
      FmVSOutNFeIts.EdGragruX.ValueVariant    := QrVSOutNfeIts.FieldByName('GraGruX').AsInteger;
      FmVSOutNFeIts.CBGragruX.KeyValue        := QrVSOutNfeIts.FieldByName('GraGruX').AsInteger;
      FmVSOutNFeIts.EdPecas.ValueVariant      := QrVSOutNfeIts.FieldByName('Pecas').AsFloat;
      FmVSOutNFeIts.EdAreaP2.ValueVariant     := QrVSOutNfeIts.FieldByName('AreaP2').AsFloat;
      FmVSOutNFeIts.EdAreaM2.ValueVariant     := QrVSOutNfeIts.FieldByName('AreaM2').AsFloat;
      FmVSOutNFeIts.EdPesoKg.ValueVariant     := QrVSOutNfeIts.FieldByName('PesoKg').AsFloat;
      FmVSOutNFeIts.RGTpCalcVal.ItemIndex     := QrVSOutNfeIts.FieldByName('TpCalcVal').AsInteger;
      FmVSOutNFeIts.EdTribDefSel.ValueVariant := QrVSOutNfeIts.FieldByName('TribDefSel').AsInteger;
      FmVSOutNFeIts.CBTribDefSel.KeyValue     := QrVSOutNfeIts.FieldByName('TribDefSel').AsInteger;
      //deixar por último?
      FmVSOutNFeIts.EdValorU.ValueVariant     := QrVSOutNfeIts.FieldByName('ValorU').AsFloat;
      FmVSOutNFeIts.EdValorT.ValueVariant     := QrVSOutNfeIts.FieldByName('ValorT').AsFloat;
    end;
    FmVSOutNfeIts.ShowModal;
    FmVSOutNfeIts.Destroy;
  end;
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TUnVS_CRC_PF.MostraFormVSOpeCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSOpeCab, FmVSOpeCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSOpeCab.LocCod(Codigo, Codigo);
    end;
    FmVSOpeCab.ShowModal;
    FmVSOpeCab.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSOutCab(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmVSOutCab, FmVSOutCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSOutCab.LocCod(Codigo, Codigo);
      FmVSOutCab.QrVSOutIts.Locate('Controle', Controle, []);
    end;
    FmVSOutCab.ShowModal;
    FmVSOutCab.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSOutPeso(SQLType: TSQLType;
  QrVSOutCab: TmySQLQuery; DsVSOutCab: TDataSource; QrVSOutIts: TmySQLQuery;
  DataHora: TDateTime; Quanto: TPartOuTodo; Pedido: Integer);
begin
  if SQLType <> stIns then
  begin
    Geral.MB_Erro('Forma de SQL não implementada!');
    Exit;
  end;
  //Codigo   := QrVSOutCab.FieldByName('Codigo').AsInteger;
  if DBCheck.CriaFm(TFmVSOutPeso, FmVSOutPeso, afmoNegarComAviso) then
  begin
    FmVSOutPeso.FParcial := Quanto = ptParcial;
    FmVSOutPeso.ImgTipo.SQLType := SQLType;
    FmVSOutPeso.FQrCab := QrVSOutCab;
    FmVSOutPeso.FDsCab := DsVSOutCab;
    FmVSOutPeso.FQrIts := QrVSOutIts;
    FmVSOutPeso.FDataHora := DataHora; // QrVSoutCabDtVenda.Value;
    FmVSOutPeso.FPedido  := Pedido; //QrVSOutCabPedido.Value;
    ReopenItensPedido(FmVSOutPeso.QrVSPedIts, Pedido);
    if SQLType = stIns then
    begin
      //
    end else
    begin
      //
    end;
    case Quanto of
      TPartOuTodo.ptParcial:
        FmVSOutPeso.DBGVMI.Options :=
        FmVSOutPeso.DBGVMI.Options - [dgMultiSelect];
      TPartOuTodo.ptTotal:
        FmVSOutPeso.DBGVMI.Options :=
        FmVSOutPeso.DBGVMI.Options + [dgMultiSelect];
    end;
    FmVSOutPeso.ShowModal;
    FmVSOutPeso.Destroy;
    //
    //LocCod(Codigo, Codigo);
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSPallet(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSPallet, FmVSPallet, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSPallet.LocCod(Codigo, Codigo);
    FmVSPallet.ShowModal;
    FmVSPallet.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSPallet1(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSPallet, FmVSPallet1, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSPallet1.LocCod(Codigo, Codigo);
    FmVSPallet1.ShowModal;
    FmVSPallet1.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSPaMulCabR(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSPaMulCabR, FmVSPaMulCabR, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSPaMulCabR.LocCod(Codigo, Codigo);
    FmVSPaMulCabR.ShowModal;
    FmVSPaMulCabR.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSPlCCab(Codigo, Controle: Integer);
begin
  if DBCheck.CriaFm(TFmVSPlCCab, FmVSPlCCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSPlCCab.LocCod(Codigo, Codigo);
      if Controle <> 0 then
        FmVSPlCCab.QrVSPlCIts.Locate('Controle', Controle, []);
    end;
    FmVSPlCCab.ShowModal;
    FmVSPlCCab.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSPrePalCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSPrePalCab, FmVSPrePalCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSPrePalCab.LocCod(Codigo, Codigo);
    FmVSPrePalCab.ShowModal;
    FmVSPrePalCab.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSPWECab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSPWECab, FmVSPWECab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmVSPWECab.LocCod(Codigo, Codigo);
    FmVSPWECab.ShowModal;
    FmVSPWECab.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSRclArtPrpNew(SQLType: TSQLType; Pallet,
  Pallet1, Pallet2, Pallet3, Pallet4, Pallet5, Pallet6, Digitador,
  Revisor: Integer; Reclasse: Boolean; StqCenLoc, FornecMO: Integer);
var
  Codigo, CacCod, NewStqCenLoc(*, TpAreaRcl*): Integer;
  MovimID: TEstqMovimID;
begin
  if DBCheck.CriaFm(TFmVSRclArtPrpNew, FmVSRclArtPrpNew, afmoNegarComAviso) then
  begin
    FmVSRclArtPrpNew.ImgTipo.SQLType    := SQLType;
    FmVSRclArtPrpNew.CkReclasse.Checked := Reclasse;
    //
    FmVSRclArtPrpNew.FFornecMO          := FornecMO;
    //
    if Pallet <> 0 then
    begin
      FmVSRclArtPrpNew.EdPallet.ValueVariant := Pallet;
      FmVSRclArtPrpNew.CBPallet.KeyValue     := Pallet;
      FmVSRclArtPrpNew.SbPalletClick(Self);
      //
      FmVSRclArtPrpNew.EdStqCenLoc.ValueVariant := StqCenLoc;
      FmVSRclArtPrpNew.CBStqCenLoc.KeyValue     := StqCenLoc;
      //
      FmVSRclArtPrpNew.EdPallet1.ValueVariant := Pallet1;
      FmVSRclArtPrpNew.EdPallet2.ValueVariant := Pallet2;
      FmVSRclArtPrpNew.EdPallet3.ValueVariant := Pallet3;
      FmVSRclArtPrpNew.EdPallet4.ValueVariant := Pallet4;
      FmVSRclArtPrpNew.EdPallet5.ValueVariant := Pallet5;
      FmVSRclArtPrpNew.EdPallet6.ValueVariant := Pallet6;
      //
      FmVSRclArtPrpNew.CBPallet1.KeyValue := Pallet1;
      FmVSRclArtPrpNew.CBPallet2.KeyValue := Pallet2;
      FmVSRclArtPrpNew.CBPallet3.KeyValue := Pallet3;
      FmVSRclArtPrpNew.CBPallet4.KeyValue := Pallet4;
      FmVSRclArtPrpNew.CBPallet5.KeyValue := Pallet5;
      FmVSRclArtPrpNew.CBPallet6.KeyValue := Pallet6;
      //
    end;
    //
    FmVSRclArtPrpNew.ShowModal;
    Codigo       := FmVSRclArtPrpNew.FCodigo;
    CacCod       := FmVSRclArtPrpNew.FCacCod;
    NewStqCenLoc := FmVSRclArtPrpNew.FStqCenLoc;
    MovimID      := FmVSRclArtPrpNew.FMovimID;
    //TpAreaRcl := FmVSRclArtPrpNew.RGTpAreaRcl.ItemIndex;
    FmVSRclArtPrpNew.Destroy;
    //
    if Codigo <> 0 then
      MostraFormVSReclassifOneNew(Codigo, CacCod, NewStqCenLoc, MovimID(*, TpAreaRcl*));
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSRclArtSel;
var
  Codigo, CacCod, StqCenLoc, Pallet(*, TpAreaRcl*): Integer;
  MovimID: TEstqMovimID;
  Qry: TmySQLQuery;
  Novo: Boolean;
begin
  if DBCheck.CriaFm(TFmVSRclArtSel, FmVSRclArtSel, afmoNegarComAviso) then
  begin
    FmVSRclArtSel.ImgTipo.SQLType := stIns;
    FmVSRclArtSel.ShowModal;
    Codigo     := FmVSRclArtSel.FCodigo;
    CacCod     := FmVSRclArtSel.FCacCod;
    StqCenLoc  := FmVSRclArtSel.FStqCenLoc;
    StqCenLoc  := FmVSRclArtSel.FStqCenLoc;
    MovimID    := FmVSRclArtSel.FMovimID;
    Pallet     := FmVSRclArtSel.FPallet;
    FmVSRclArtSel.Destroy;
    //
    if Codigo <> 0 then
      MostraFormVSReclassifOneNew(Codigo, CacCod, StqCenLoc, MovimID)
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVSReclassifOneNew(Codigo, CacCod, StqCenLoc: Integer;
  MovimID: TEstqMovimID);
var
  Form, Max: Integer;
  Ord_Txt: String;
  Qry: TmySQLQuery;
begin
(*
  Form := MyObjects.SelRadioGroup('Seleção de Janela',
  'Selecione a Janela', [
  'Atual',
  'Novo (3)'], 0);
  case Form of
    0:
    begin
      if DBCheck.CriaFm(TFmVSReclassifOneNew, FmVSReclassifOneNew, afmoNegarComAviso) then
      begin
        FmVSReclassifOneNew.FCodigo := Codigo;
        FmVSReclassifOneNew.FCacCod := CacCod;
        FmVSReclassifOneNew.FMovimID := MovimID;
        FmVSReclassifOneNew.ReopenVSPaRclCab();
        //
        FmVSReclassifOneNew.ShowModal;
        FmVSReclassifOneNew.Destroy;
      end;
    end;
    1:
    begin
*)
      if DBCheck.CriaFm(TFmVSReclassifOneNw3, FmVSReclassifOneNw3, afmoNegarComAviso) then
      begin
        Qry := TmySQLQuery.Create(Dmod);
        try
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT * ',
          'FROM vsparclcaba ',
          'WHERE Codigo=' + Geral.FF0(Codigo),
          '']);
          try
            if Qry.FieldByName('LstPal15').AsInteger <> 0 then Max := 15 else
            if Qry.FieldByName('LstPal14').AsInteger <> 0 then Max := 14 else
            if Qry.FieldByName('LstPal13').AsInteger <> 0 then Max := 13 else
            if Qry.FieldByName('LstPal12').AsInteger <> 0 then Max := 12 else
            if Qry.FieldByName('LstPal11').AsInteger <> 0 then Max := 11 else
            if Qry.FieldByName('LstPal10').AsInteger <> 0 then Max := 10 else
            if Qry.FieldByName('LstPal09').AsInteger <> 0 then Max := 09 else
            if Qry.FieldByName('LstPal08').AsInteger <> 0 then Max := 08 else
            if Qry.FieldByName('LstPal07').AsInteger <> 0 then Max := 07 else
            Max := 6;
          except
            Max := 15;
          end;
        finally
          Qry.Free;
        end;
        FmVSReclassifOneNw3.DefineBoxes(Max);
        FmVSReclassifOneNw3.FCodigo    := Codigo;
        FmVSReclassifOneNw3.FCacCod    := CacCod;
        FmVSReclassifOneNw3.FStqCenLoc := StqCenLoc;
        FmVSReclassifOneNw3.FMovimID   := MovimID;
        FmVSReclassifOneNw3.ReopenVSPaRclCab();
        //
        FmVSReclassifOneNw3.ShowModal;
        FmVSReclassifOneNw3.Destroy;
      end;
(*
    end;
  end;
*)
end;

procedure TUnVS_CRC_PF.MostraFormVSReclassPrePalCac(SQLType: TSQLType; Pallet1,
  Pallet2, Pallet3, Pallet4, Pallet5, Pallet6, Digitador, Revisor: Integer);
var
  Pallet, StqCenLoc, FornecMO: Integer;
  Reclasse: Boolean;
begin
  if DBCheck.CriaFm(TFmVSReclassPrePal, FmVSReclassPrePal, afmoNegarComAviso) then
  begin
    FmVSReclassPrePal.ImgTipo.SQLType := SQLType;
    //
    FmVSReclassPrePal.ShowModal;
    Pallet    := FmVSReclassPrePal.FPallet;
    StqCenLoc := FmVSReclassPrePal.FStqCenLoc;
    FornecMO  := FmVSReclassPrePal.FFornecMO;
    Reclasse  := FmVSReclassPrePal.CkReclasse.Checked;
    FmVSReclassPrePal.Destroy;
    if Pallet <> 0 then
      MostraFormVSRclArtPrpNew(stIns, Pallet, Pallet1, Pallet2, Pallet3,
      Pallet4, Pallet5, Pallet6, Digitador, Revisor, Reclasse, StqCenLoc,
      FornecMO);
  end;
end;

function TUnVS_CRC_PF.MostraFormVSRMPPsq(const MovimID: TEstqMovimID;
  const MovimNivs: array of TEstqMovimNiv; const SQLType: TSQLType; var Codigo,
  Controle: Integer; OnlySelIMEI: Boolean): Boolean;
var
  I: Integer;
begin
  Codigo   := 0;
  Controle := 0;
  //
  if DBCheck.CriaFm(TFmVSRMPPsq, FmVSRMPPsq, afmoNegarComAviso) then
  begin
    FmVSRMPPsq.ImgTipo.SQLType := SQLType;
    FmVSRMPPsq.FMovimID  := MovimID;
    FmVSRMPPsq.FCodigo   := 0;
    FmVSRMPPsq.FControle := 0;
    FmVSRMPPsq.ReopenMovimNiv(MovimID);
    FmVSRMPPsq.FOnlySelIMEI := OnlySelIMEI;
    if Length(MovimNivs) > 0 then
    begin
      FmVSRMPPsq.QrMovimNiv.First;
      while not FmVSRMPPsq.QrMovimNiv.Eof do
      begin
        for I := Low(MovimNivs) to High(MovimNivs) do
        begin
          if TEstqMovimNiv(FmVSRMPPsq.QrMovimNivMovimNiv.Value) = MovimNivs[I] then
            FmVSRMPPsq.DBGMovimNiv.SelectedRows.CurrentRowSelected := True;
        end;
        FmVSRMPPsq.QrMovimNiv.Next;
      end;
      FmVSRMPPsq.QrMovimNiv.First;
    end;
    //
    FmVSRMPPsq.ShowModal;
    //
    Codigo   := FmVSRMPPsq.FCodigo;
    Controle := FmVSRMPPsq.FControle;
    //
    FmVSRMPPsq.Destroy;
  end;
  if (Codigo <> 0) and (Controle <> 0) then
    Result := True
  else
    Result := False;
end;

procedure TUnVS_CRC_PF.MostraFormVSSerFch();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'VSSerFch', 60, ncGerlSeq1,
  'Séries de Fichas RMP',
  [], False, Null, [], [], False);
end;

procedure TUnVS_CRC_PF.MostraFormVSTrfLocCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmVSTrfLocCab, FmVSTrfLocCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmVSTrfLocCab.LocCod(Codigo, Codigo);
      //FmVSTrfLocCab.QrVSTrfLocIts.Locate('Controle', Controle, []);
    end;
    FmVSTrfLocCab.ShowModal;
    FmVSTrfLocCab.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVS_Do_IMEC(MovimCod: Integer; Pergunta: String);
const
  Controle = 0;
var
  Qry: TmySQLQuery;
  MovimID, Codigo: Integer;
  Continua: Boolean;
begin
  if Trim(Pergunta) <> '' then
    Continua := Geral.MB_Pergunta(Pergunta) = ID_YES
  else
    Continua := True;
  if Continua then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Codigo, MovimID ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      '',
      'UNION',
      '',
      'SELECT Codigo, MovimID ',
      'FROM ' + CO_TAB_VMB,
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      '']);
      if Qry.RecordCount > 0 then
      begin
        Codigo  := Qry.FieldByName('Codigo').AsInteger;
        MovimID := Qry.FieldByName('MovimID').AsInteger;
        //
        MostraFormVS_XXX(MovimID, Codigo, Controle);
      end else
        Geral.MB_Aviso(
        'Não foi possível localizar a janela de movimento do o IME-C: ' +
        Geral.FF0(MovimCod));
    finally
      Qry.Free;
    end;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVS_Do_IMEI(Controle: Integer);
var
  Qry: TmySQLQuery;
  MovimID, Codigo, Iuvpei: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, MovimID, Iuvpei ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE Controle=' + Geral.FF0(Controle),
    '',
    'UNION',
    '',
    'SELECT Codigo, MovimID, Iuvpei ',
    'FROM ' + CO_TAB_VMB,
    'WHERE Controle=' + Geral.FF0(Controle),
    '']);
    if Qry.RecordCount > 0 then
    begin
      Codigo  := Qry.FieldByName('Codigo').AsInteger;
      MovimID := Qry.FieldByName('MovimID').AsInteger;
      Iuvpei  := Qry.FieldByName('Iuvpei').AsInteger;
      //
      MostraFormVS_XXX(MovimID, Codigo, Controle);
    end else
      Geral.MB_Aviso(
      'Não foi possível localizar a janela de movimento do o IME-I: ' +
      Geral.FF0(Controle));
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.MostraFormVS_XXX(MovimID, Codigo, Controle: Integer);
const
  sProcName = 'VS_CRC_PF.MostraFormVS_XXX()';
var
  Janela: Integer;
  Qry: TmySQLQuery;
  //
  procedure MensagemMovimentoSemJanela();
  begin
    Geral.MB_Info('O tipo de movimento "' + sEstqMovimIDLong[MovimID] +
    '" não possui janela geranciável de movimento!' + sLineBreak +
    sProcName);
  end;
  procedure EscolheVSGerXXX();
  var
    CacCod, ID, ThisCod: Integer;
  begin
    ThisCod := 0;
    Janela := MyObjects.SelRadioGroup('Janela',
    'Escolha a janela desejada', [
    'Geração de Artigo',
    'Classificação / Reclassificação',
    'Gerenciamento de Classificação'],
    -1);
    case Janela of
      {$IFDEF sAllVS}
      0: VS_CRC_PF.MostraFormVSGeraArt(Codigo, Controle);
      {$ENDIF}
      1:
      begin
        Qry := TmySQLQuery.Create(Dmod);
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT CacCod  ',
        'FROM vsgerarta',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
        CacCod := Qry.FieldByName('CacCod').AsInteger;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Codigo, 14.000 ID   ',
        'FROM VSPaMulCabA ',
        'WHERE CacCod=' + Geral.FF0(CacCod),
        ' ',
        'UNION ',
        ' ',
        'SELECT Codigo, 28.000 ID ',
        'FROM VSPaMulCabR ',
        'WHERE CacCod=' + Geral.FF0(CacCod),
        ' ',
        'UNION ',
        ' ',
        'SELECT Codigo, 7.000 ID ',
        'FROM VSPaClaCabA ',
        'WHERE CacCod=' + Geral.FF0(CacCod),
        ' ',
        'UNION ',
        ' ',
        'SELECT Codigo, 8.000 ID  ',
        'FROM VSPaRclCabA ',
        'WHERE CacCod=' + Geral.FF0(CacCod),
        ' ',
        '']);
        ThisCod := Qry.FieldByName('Codigo').AsInteger;
        ID := Trunc(Qry.FieldByName('ID').AsFloat);
      {$IFDEF sAllVS}
        if ThisCod > 0 then
        begin
          case ID of
             //7:
             //8:
            14: VS_PF.MostraFormVSPaMulCabA(ThisCod);
            24: VS_CRC_PF.MostraFormVSPaMulCabR(ThisCod);
            else Geral.MB_Erro('ID ' + Geral.FF0(ID) +
                ' não implementado em ' + sProcName + slineBreak +
                'Código: ' + Geral.FF0(ThisCod));
          end;
        end else
      {$ENDIF}
          Geral.MB_Erro('Codigo=0 para ID ' + Geral.FF0(ID) +
          ' em ' + sProcName);
      end;
      {$IFDEF sAllVS}
      2: VS_CRC_PF.MostraFormVSGerClaCab(ThisCod);
      {$ENDIF}
    end;
  end;
  procedure PesquisaEMostraOrigemDesclasse();
  var
    MyMovimID, MyCodigo, MyControle: Integer;
  begin
      {$IFDEF sAllVS}
    if VS_PF.ObtemOrigemDesclasse(Controle, MyMovimID, MyCodigo, MyControle) then
      MostraFormVS_XXX(MyMovimID, MyCodigo, MyControle);
      {$ENDIF}
  end;
  procedure PesquisaEMostraCouroCaleado();
  var
    Codigo, JmpNivel1, JmpMovID, Iuvpei: Integer;
    Qry: TmySQLQuery;
  begin
    Qry := TmySQLQuery.Create(DMod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT JmpNivel1, JmpMovID, Iuvpei, Codigo ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(Controle),
      '']);
      if Qry.RecordCount > 0 then
      begin
        JmpNivel1 := Qry.FieldByName('JmpNivel1').AsInteger;
        JmpMovID  := Qry.FieldByName('JmpMovID').AsInteger;
        Codigo    := Qry.FieldByName('Codigo').AsInteger;
        Iuvpei    := Qry.FieldByName('Iuvpei').AsInteger;
        //
        //testar
        if Iuvpei = Integer(TInsUpdVMIPrcExecID.iuvpei006) then
          VS_PF.MostraFormVSCalCab(Codigo)
        else
          VS_PF.MostraFormVSCurCab(JmpNivel1);
      end else
        Geral.MB_Aviso('IME-I ' + Geral.FF0(Controle) + ' não localizado  em ' +
        sProcName);
    finally
      Qry.Free;
    end;
  end;
  procedure PesquisaEMostraCouroConservado();
  var
    Codigo, JmpNivel1, JmpMovID, Iuvpei: Integer;
    Qry: TmySQLQuery;
  begin
    Qry := TmySQLQuery.Create(DMod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT JmpNivel1, JmpMovID, Iuvpei, Codigo ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(Controle),
      '']);
      if Qry.RecordCount > 0 then
      begin
        JmpNivel1 := Qry.FieldByName('JmpNivel1').AsInteger;
        JmpMovID  := Qry.FieldByName('JmpMovID').AsInteger;
        Codigo    := Qry.FieldByName('Codigo').AsInteger;
        Iuvpei    := Qry.FieldByName('Iuvpei').AsInteger;
        //
        //testar
        if (Iuvpei = Integer(TInsUpdVMIPrcExecID.iuvpei111))
        or (Iuvpei = Integer(TInsUpdVMIPrcExecID.iuvpei113)) then
          VS_PF.MostraFormVSConCab(Codigo)
        else
          //VS_PF.MostraFormVSCurCab(JmpNivel1);
          Geral.MB_Aviso('IME-I ' + Geral.FF0(Controle) + ' não localizado  em ' +
          sProcName + ' [1]');
      end else
        Geral.MB_Aviso('IME-I ' + Geral.FF0(Controle) + ' não localizado  em ' +
        sProcName + ' [2]');
    finally
      Qry.Free;
    end;
  end;
begin
  AdicionarNovosVS_emid();
  case TEstqMovimID(MovimID) of
    //(*0*)emidAjuste:       Deprecado
    {$IFDEF sAllVS}
    (*1*)emidCompra:         VS_CRC_PF.MostraFormVSInnCab(Codigo, Controle, 0);
    (*2*)emidVenda:          VS_CRC_PF.MostraFormVSOutCab(Codigo, Controle);
    //(*3*)emidReclasWE:     Deprecado
    //(*4*)emidBaixa:        Deprecado
    //(*5*)emidIndsWE        Deprecado
    (*6*)emidIndsXX:         EscolheVSGerXXX();
    (*7*)emidClassArtXXUni:  VS_CRC_PF.MostraFormVSGerClaCab(Codigo);
    (*8*)emidReclasXXUni:    VS_CRC_PF.MostraFormVSGerRclCab(Codigo, Controle);
    (*9*)emidForcado:        VS_PF.MostraFormVSBxaCab(Codigo, Controle);
    //(*10*)emidSemOrigem    Deprecado
    (*11*)emidEmOperacao:    VS_PF.MostraFormVSOpeCab(Codigo);
    (*13*)emidInventario:    VS_PF.MostraFormVSAjsCab(Codigo, Controle);
    (*14*)emidClassArtXXMul: VS_PF.MostraFormVSPaMulCabA(Codigo);//MostraFormVSGerClaCab(Codigo);
    (*15*)emidPreReclasse:   VS_PF.PesquisaReclasseDePreClasse(MovimID, Codigo, Controle);
    (*16*)emidEntradaPlC:    VS_CRC_PF.MostraFormVSPlCCab(Codigo, Controle);
    (*17*)emidExtraBxa:      VS_PF.MostraFormVSExBCab(Codigo, Controle);
    (*18*)emidSaldoAnterior: MensagemMovimentoSemJanela();
    (*19*)emidEmProcWE:      VS_CRC_PF.MostraFormVSPWECab(Codigo);
    (*20*)emidFinished:      VS_CRC_PF.MostraFormVSPWECab(Codigo); //MensagemMovimentoSemJanela();
    (*21*)emidDevolucao:     VS_PF.MostraFormVSDvlCab(Codigo);
    (*22*)emidRetrabalho:    VS_PF.MostraFormVSRtbCab(Codigo);
    (*23*)emidGeraSubProd:   VS_PF.MostraFormVSSubPrdCad(MovimID, Codigo, Controle);
    (*24*)emidReclasXXMul:   VS_CRC_PF.MostraFormVSPaMulCabR(Codigo);
    (*25*)emidTransfLoc:     VS_PF.MostraFormVSTrfLocCab(Codigo);
    (*26*)emidEmProcCal:     VS_PF.MostraFormVSCalCab(Codigo);
    (*27*)emidEmProcCur:     VS_PF.MostraFormVSCurCab(Codigo);
    (*28*)emidDesclasse:     PesquisaEMostraOrigemDesclasse();
    (*29*)emidCaleado:       PesquisaEMostraCouroCaleado();
    (*30*)emidEmRibPDA:      VS_PF.MostraFormVSEmRibPDA(Codigo, Controle);
    (*31*)emidEmRibDTA:      VS_PF.MostraFormVSEmRibDTA(Codigo, Controle);
    (*32*)emidEmProcSP:      VS_PF.MostraFormVSPSPCab(Codigo);
    (*33*)emidEmReprRM:      VS_PF.MostraFormVSRRMCab(Codigo);
    (*34*) //  Falta fazer!!! Não existe lancto fisico
    (*35*) //  Falta fazer!!! PQMCab???
    {$ELSE}
    (*36*)emidInnSemCob:     CRC_PF.MostraFormVSESCCab(Codigo);
    //(*37*)emidOutSemCob:      CRC_PF.MostraFormVSSSCCab(Codigo);
    {$ENDIF}
    (*39*)emidEmProcCon:     VS_PF.MostraFormVSConCab(Codigo);
    (*40*)emidConservado:    PesquisaEMostraCouroConservado();
    (*41*)emidEntraExced:     VS_PF.MostraFormVSExcCab(Codigo, Controle);
    //(*42*)emidSPCaleirad:       PesquisaEMostraSubProdCaleirad(); 2023-12-29 Parei Aqui
    else Geral.MB_Erro(
      '"MovimID: ' + Geral.FF0(MovimID) +
      '" não implementado em ' + sProcName);
  end;
end;

procedure TUnVS_CRC_PF.MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc: TControl;
  EdStqCenLoc: TdmkEditCB; CBStqCenLoc: TdmkDBLookupComboBox;
  QrStqCenLoc: TmySQLQuery; MovimID: TEstqMovimID);
begin
  //FmVSHide.FMovimIDSel   := FMovimIDSel;
  //FmVSHide.FStqCenCadSel := FStqCenCadSel;
  //FmVSHide.FStqcenLocSel := FStqcenLocSel;
  FmVSHide.FEdStqCenLoc := EdStqCenLoc;
  FmVSHide.FCBStqCenLoc := CBStqCenLoc;
  FmVSHide.FQrStqCenLoc := QrStqCenLoc;
  FmVSHide.FMovimIDSel  := MovimID;
  MyObjects.MostraPopUpDeBotao(FmVSHide.PMVSMovIDLoc, SbStqCenLoc);
end;

// CalculaNotaMPAG
function TUnVS_CRC_PF.NotaCouroRibeiraApuca(Pecas, Peso, AreaM2, FatorMP,
  FatorAR: Double): Double;
const
  PesoBase = 23.75; // kg/couro verde
  AreaBase = 3.80; // m2
  StepPeso = 2.5; // A cada tantos kilos...
  StepArea = 0.1; // ... deve terr tantos m2!
var
  PCV: Double; // Peso Corrigido para Couro Verde
  ACC: Double; // Área do couro corrigida
  AMC: Double; // Área média couro
  //PMA: Double; // Peso médio 100 m2
  //A, B, C, _C, AB: Double;
  DPV: Double; // Diferença do peso verde em relação ao peso base
  DAV: Double; // Diferença da área verde em relação a área base
  ACP: Double; // Area 100% para o Peso indicado
begin
  if (Pecas <= 0) or (Peso <=0 ) or (AreaM2 <= 0) or (FatorMP <= 0)
  or (FatorAR <= 0) then
  begin
    Result := 0;
    Exit;
  end;
  ACC := AreaM2 / FatorAR;
  AMC := ACC / Pecas;
  PCV := Peso / Pecas * FatorMP;
  DPV := PCV - PesoBase;
  DAV := DPV / StepPeso * StepArea;
  ACP := AreaBase + DAV;
  if ACP = 0 then
    Result := 0
  else
    Result := AMC / ACP * 100;
end;

function TUnVS_CRC_PF.ObrigaInfoIxx(IxxMovIX: TEstqMovInfo; IxxFolha,
  IxxLinha: Integer): Boolean;
begin
  Result := MyObjects.FIC(
  (
    (Integer(IxxMovIX) <> 0)
    or (IxxFolha <> 0)
    or (IxxLinha <> 0)
  ) and
  (
    (Integer(IxxMovIX) = 0)
    or (IxxFolha = 0)
    or (IxxLinha = 0)
  ), nil, 'Informação manual de movimentação incompleta!' + sLineBreak +
   'Informação manual de movimentação: Informe o IEC/ICR/ISC, a Folha e a Linha!');
end;

function TUnVS_CRC_PF.ObtemControleMovimTwin(MovimCod, MovimTwn: Integer;
  MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    'AND MovimTwn=' + Geral.FF0(MovimTwn),
    'AND MovimID=' + Geral.FF0(Integer(MovimID)),
    'AND MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
    '']);
    if Qry.RecordCount = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM ' + CO_TAB_VMB,
      'WHERE MovimCod=' + Geral.FF0(MovimCod),
      'AND MovimTwn=' + Geral.FF0(MovimTwn),
      'AND MovimID=' + Geral.FF0(Integer(MovimID)),
      'AND MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
      '']);
    end;
    //
    Result := Qry.FieldByName('Controle').AsInteger;
    if Result = 0 then
      Geral.MB_Aviso('Controle não localizado para MovimTwn ' + Geral.FF0(MovimTwn));
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.ObtemFatorIntDeGraGruXeVeSeDifere(const GraGruX1,
  GraGruX2: Integer; var FatorInt1, FatorInt2, Fator1para2: Double): Boolean;
var
  Qry: TmySQLQuery;
  //
  function ReabreQry(const GGX: Integer; var FI: Double): Boolean;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cn1.Nome, cn1.FatorInt ',
    'FROM gragruxcou gxc ',
    'LEFT JOIN CouNiv1 cn1 ON cn1.Codigo=gxc.CouNiv1 ',
    'WHERE gxc.GraGruX=' + Geral.FF0(GGX),
    '']);
    FI := Qry.FieldByName('FatorInt').AsFloat;
    Result := Qry.RecordCount > 0;
  end;
begin
  FatorInt1 := 0;
  FatorInt2 := 0;
  Qry := TMySQLQuery.Create(Dmod.MyDB);
  try
    if ReabreQry(GraGruX1, FatorInt1) then ;
    if ReabreQry(GraGruX2, FatorInt2) then ;
    //
    if FatorInt1 <> 0 then
      Fator1Para2 := FatorInt2 / FatorInt1
    else
      Fator1Para2 := 1;
    //
    Result := Fator1Para2 <> 1;
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.ObtemFatorInteiro(const GraGruX: Integer;
  var FatorIntRes: Integer; const Avisa: Boolean;
  const FatorIntCompara: Integer; MeAviso: TMemo): Boolean;
const
  Aviso = 'CUIDADO!!!' + sLineBreak +
  'Ao informar a quantidade leve em consideração a possibilidade de transformção de inteiros para meios!!!';
var
  Qry: TmySQLQuery;
  CouNiv2: Double;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ggc.CouNiv2, ',
    'co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
    'FROM couniv1 co1 ',
    'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
    'WHERE ggc.GraGruX=' + Geral.FF0(GraGruX),
    '']);
    FatorIntRes := Trunc(Qry.FieldByName('FatorInt').AsFloat * 1000);
    CouNiv2     := Qry.FieldByName('FatorInt').AsFloat;
    //
    //if (FatorIntRes = 0) and (FatorIntCompara = 0) and (CouNiv2 <> 1) then
    if  (CouNiv2 <> 1) and (CouNiv2 <> 0.5) then
    begin
      if FatorIntCompara <> 0 then
      begin
        if FatorIntCompara < 100 then
          FatorIntRes := Trunc(FatorIntCompara * 1000)
        else
          FatorIntRes := FatorIntCompara;
      end else
        FatorIntRes := 1000;
    end;
    if FatoresIncompativeis(FatorIntRes, FatorIntCompara, MeAviso) then
      Exit;
    //
    if Avisa then
    begin
      if FatorIntRes <> (FatorIntCompara * 1000) then
      begin
        if MeAviso <> nil then
          MeAviso.Text := Aviso
        else
          Geral.MB_Aviso(Aviso);
      end;
    end;
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.ObtemGraGruY_de_IDTipoCouro(Index: Integer): Integer;
begin
  Result := sCodeGraGruY_VS[Index];
end;

function TUnVS_CRC_PF.ObtemIDTipoCouro_de_GraGruY(GraGruY: Integer): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := Low(sCodeGraGruY_VS) to High(sCodeGraGruY_VS) do
    if sCodeGraGruY_VS[I]= GraGruY then
      Result := I;
end;

function TUnVS_CRC_PF.ObtemListaOperacoes: TStringList;
var
  I: Integer;
  Lista: TStringList;
begin
  Lista := TStringList.Create;
  try
    for I := Low(VsOperaTip_Str) to High(VsOperaTip_Str) do
      Lista.Add(VsOperaTip_Str[I]);
  finally
    Result := Lista;
  end;
end;

function TUnVS_CRC_PF.ObtemMovimCodDeControle(Controle: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MovimCod ',
    'FROM ' + CO_SEL_TAB_VMI + ' ',
    'WHERE Controle=' + Geral.FF0(Controle),
    'AND MovimCod<>0',
    '']);
    Result := Qry.FieldByName('MovimCod').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.ObtemMovimCodDeMovimIDECodigo(MovimID: TEstqMovimID;
  Codigo: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 0;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM vsmovcab ',
    'WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
    'AND CodigoID=' + Geral.FF0(Codigo),
    '']);
    Result := Qry.FieldByName('Codigo').AsInteger;
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.ObtemMovimIDdeFatID(FatID: Integer): Integer;
const
  sProcName = 'TUnVS_CRC_PF.ObtemMovimIDdeFatID()';
begin
  Result := 0;
  //
  case FatID of
    VAR_FATID_1003: Result := Integer(TEstqMovimID.emidCompra);
    VAR_FATID_1041: Result := Integer(TEstqMovimID.emidEntradaPlC);
{
    (*02*)emidVenda         : Atualiza := Nao;
    (*04*)emidBaixa         : Atualiza := Nao;
    (*17*)emidExtraBxa      : Atualiza := Nao;
    (*21*)emidDevolucao     : Atualiza := Sim;
    (*25*)emidTransfLoc     : Atualiza := Sim;
    (*36*)emidInnSemCob     : Atualiza := Sim;
    (*37*)emidOutSemCob     : Atualiza := Nao;
}
    else
      Geral.MB_Info('"FatID" não implementado em ' + sProcName + sLineBreak+
      'SOLICITE À DERMATEK!');
  end;
end;

function TUnVS_CRC_PF.ObtemMovimNivDstDeMovimID(
  MovimID: TEstqMovimID): TEstqMovimNiv;
const
  sProcName = 'UnVS_PF.ObtemMovimNivDstDeMovimID()';
begin
  case MovimID of
    emidEmRibPDA: Result := eminDestPDA;
    emidEmRibDTA: Result := eminDestDTA;
    emidEmOperacao: Result := eminDestOper;
    emidEmProcWE: Result := eminDestWEnd;
    emidEmProcCal: Result := eminDestCal;
    emidEmProcCur: Result := eminDestCur;
    emidEmProcSP: Result := eminDestPSP;
    emidEmReprRM: Result := eminDestRRM;
    else
    begin
      Result := eminSemNiv;
      Geral.MB_Erro(
      '"MovimNiv" indefinido para MovimID=' + Geral.FF0(Integer(MovimID)) +
      ' em ' + sProcName);
    end;
  end;
end;

function TUnVS_CRC_PF.ObtemMovimNivInnDeMovimID(
  MovimID: TEstqMovimID): TEstqMovimNiv;
const
  sProcName = 'TUnVS_CRC_PF.ObtemMovimNivInnDeMovimID()';
begin
  case MovimID of
    emidEmOperacao: Result := eminEmOperInn;
    emidEmProcWE: Result := eminEmWEndInn;
    //emidFinished: Result := eminEmWEndInn; Não!
    emidEmProcCon: Result := eminEmConInn;
    emidEmProcCal: Result := eminEmCalInn;
    emidEmProcCur: Result := eminEmCurInn;
    emidEmProcSP: Result := eminEmPSPInn;
    emidEmReprRM: Result := eminEmRRMInn;
    // ini 2023-05-11
    (* Desabilitado em 2023-05-11
    // está certo????? Pede no TFmSpedEfdIcmsIpiProducaoIsolada_v.....ImportaOpeProcB()
    emidCaleado: Result := eminEmCalBxa;
    *)
    // alterado para: (em 2023-05-11
    emidCaleado: Result := eminEmCalInn;
    //emidSPCaleirad: Result := eminEmProcSPInn;
    // ver como fica se usar o SPED no futuro
    // fim 2023-05-11
    //
    emidConservado: Result := eminEmConInn;
    else
    begin
      Result := eminSemNiv;
      Geral.MB_Erro(
      '"MovimNiv" indefinido para MovimID=' + Geral.FF0(Integer(MovimID)) +
       ' - "' + GetEnumName(TypeInfo(TEstqMovimID), Integer(MovimID)) +
       '" em ' + sProcName);
    end;
  end;
end;

function TUnVS_CRC_PF.ObtemMovimNivSrcDeMovimID(
  MovimID: TEstqMovimID): TEstqMovimNiv;
const
  sProcName = 'UnVS_PF.ObtemMovimNivSrcDeMovimID()';
begin
  case MovimID of
    emidEmRibPDA: Result := eminSorcPDA;
    emidEmRibDTA: Result := eminSorcDTA;
    emidEmOperacao: Result := eminSorcOper;
    emidEmProcWE: Result := eminSorcWEnd;
    emidEmProcCal: Result := eminSorcCal;
    emidEmProcCur: Result := eminSorcCur;
    emidEmProcSP: Result := eminSorcPSP;
    emidEmReprRM: Result := eminSorcRRM;
    else
    begin
      Result := eminSemNiv;
      Geral.MB_Erro(
      '"MovimNiv" indefinido para MovimID=' + Geral.FF0(Integer(MovimID)) +
      ' em ' + sProcName);
    end;
  end;
end;

procedure TUnVS_CRC_PF.ObtemNomeCamposNFeVSXxxCab(const MovimID: TEstqMovimID;
  var FldSer, FldNum: String);
const
  sProcName = 'TUnVS_CRC_PF.ObtemNomeCamposNFeVSXxxCab';
begin
  AdicionarNovosVS_emid();
  FldSer := '?????';
  FldNum := '?????';
  case MovimID of
    (*00*)//emidAjuste: Result := 'vs';
    (*01*)emidCompra:
    begin
       FldSer := 'ide_serie';
       FldNum := 'ide_nNF';
    end;
    (*02*)emidVenda:
    begin
       FldSer := 'SerieV';
       FldNum := 'NFV';
    end;
    (*03*)//emidReclasWE: Result := 'vs';
    (*04*)//emidBaixa: Result := 'vs';
    (*05*)//emidIndsWE: Result := 'vs';
    (*06*)emidIndsXX:
    begin
       FldSer := '**Sem serie**';
       FldNum := '** Sem NFe **';
    end;
    (*07*)//emidClassArtXXUni: Result := 'vs';
    (*08*)//emidReclasXXUni: Result := 'vs';
    (*09*)//emidForcado: Result := 'vs';
    (*10*)//emidSemOrigem: Result := 'vs';
    (*11*)emidEmOperacao:
    begin
       FldSer := 'SerieRem';
       FldNum := 'NFeRem';
    end;
    (*12*)//emidResiduoReclas: Result := 'vs';
    (*13*)//emidInventario: Result := 'vsajscab';
    (*14*)//emidClassArtXXMul: Result := 'vspamulcaba';
    (*15*)emidPreReclasse:
    begin
       FldSer := 'ide_serie';
       FldNum := 'ide_nNF';
    end;
    (*16*)emidEntradaPlC:
    begin
       FldSer := 'ide_serie';
       FldNum := 'ide_nNF';
    end;
    (*17*)//emidExtraBxa: Result :=
    (*18*)//emidSaldoAnterior: Result
    (*19*)emidEmProcWE:
    begin
       FldSer := 'SerieRem';
       FldNum := 'NFeRem';
    end;
    (*20*)//emidFinished: Result := 'vs';
    (*21*)emidDevolucao:
    begin
       FldSer := 'ide_serie';
       FldNum := 'ide_nNF';
    end;
    (*22*)emidRetrabalho:
    begin
       FldSer := 'ide_serie';
       FldNum := 'ide_nNF';
    end;
    (*23*)//emidGeraSubProd: Result := '';
    (*24*)//emidReclasXXMul: Result := 'vspamulcabr';
    (*25*)emidTransfLoc:
    begin
       FldSer := 'ide_serie';
       FldNum := 'ide_nNF';
    end;
    (*26*)emidEmProcCal:
    begin
       FldSer := 'SerieRem';
       FldNum := 'NFeRem';
    end;
    (*27*)emidEmProcCur:
    begin
       FldSer := 'SerieRem';
       FldNum := 'NFeRem';
    end;
    (*28*)emidDesclasse:
    begin
       FldSer := 'ide_serie';
       FldNum := 'ide_nNF';
    end;
    //(*29*)emidCaleado: ; // ?????
    //(*30*)emidEmRibPDA: ; // ?????
    //(*31*)emidEmRibDTA: ; // ?????
    (*32*)emidEmProcSP:
    begin
       FldSer := 'SerieRem';
       FldNum := 'NFeRem';
    end;
    (*33*)emidEmReprRM:
    begin
       FldSer := 'SerieRem';
       FldNum := 'NFeRem';
    end;
    (*36*)emidInnSemCob:
    begin
       FldSer := 'ide_serie';
       FldNum := 'ide_nNF';
    end;
    (*37*)emidOutSemCob:
    begin
       FldSer := 'ide_serie';
       FldNum := 'ide_nNF';
    end;
    (*39*)emidEmProcCon:
    begin
       FldSer := 'SerieRem';
       FldNum := 'NFeRem';
    end;
    //(*40*)emidConservado: ; // ?????
    //(*42*)emidSPCaleirad: ; // ?????
    else
      Geral.MB_ERRO('"MovimID" ' + Geral.FF0(Integer(MovimID)) +
      ' não implementado em ' + sProcName);
  end;
    //(*34*)emidCurtido: ; // ?????
    //(*35*)emidMixInsum: ; // ?????
end;

function TUnVS_CRC_PF.ObtemNomeFrxEstoque(Data: String; RG00_Agrupa_ItemIndex,
  RG00_Ordem1_ItemIndex, RG00_Ordem2_ItemIndex, RG00_Ordem3_ItemIndex,
  RG00_Ordem4_ItemIndex, RG00_Ordem5_ItemIndex: Integer;
  CB00StqCenCad_Text: String): String;
begin
  Result := 'Estoque';
  if Trim(CB00StqCenCad_Text) <> '' then
  Result := Result + '_' + Trim(CB00StqCenCad_Text);
  if Data <> '' then
    Result := Result + '_' + Geral.SoNumero_TT(Data);
  Result := Result + '_' + sMax_VS_IMP_ESTQ_ORD[RG00_Ordem1_ItemIndex];
  if RG00_Agrupa_ItemIndex > 0 then
    Result := Result + '_' + sMax_VS_IMP_ESTQ_ORD[RG00_Ordem2_ItemIndex];
  if RG00_Agrupa_ItemIndex > 1 then
    Result := Result + '_' + sMax_VS_IMP_ESTQ_ORD[RG00_Ordem3_ItemIndex];
  if RG00_Agrupa_ItemIndex > 2 then
    Result := Result + '_' + sMax_VS_IMP_ESTQ_ORD[RG00_Ordem4_ItemIndex];
  if RG00_Agrupa_ItemIndex > 3 then
    Result := Result + '_' + sMax_VS_IMP_ESTQ_ORD[RG00_Ordem5_ItemIndex];
end;

function TUnVS_CRC_PF.ObtemNomeGraGruYDestdeOrig(GraGruY: Integer): Integer;
begin
  case GraGruY of
    //CO_GraGruY_0512_VSSubPrd: Result := ;
    //CO_GraGruY_1024_VSNatCad: Result := ;
    //CO_GraGruY_1072_VSNatInC: Result := ;
    //CO_GraGruY_1088_VSNatCon: Result := ;
    //CO_GraGruY_1365_VSProCal: Result := ;
    //CO_GraGruY_1536_VSCouCal: Result := ;
    //CO_GraGruY_1707_VSProCur: Result := ;
    //CO_GraGruY_1877_VSCouCur: Result := ;
    CO_GraGruY_2048_VSRibCad: Result := CO_GraGruY_3072_VSRibCla;
    CO_GraGruY_3072_VSRibCla: Result := CO_GraGruY_3072_VSRibCla;
    //CO_GraGruY_4096_VSRibOpe: Result := ;
    //CO_GraGruY_5120_VSWetEnd: Result := ;
    CO_GraGruY_6144_VSFinCla: Result := CO_GraGruY_6144_VSFinCla;
    //CO_GraGruY_7168_VSRepMer
    else
    begin
      Result := GraGruY;
      Geral.MB_Erro(
        'GraGruY ' + Geral.FF0(GraGruY) +
        ' não definido em "VS_CRC_PF.ObtemNomeGraGruYDestdeOrig()"');
    end;
  end;
end;

function TUnVS_CRC_PF.ObtemNomeDestinoGraGruY(GraGruY: Integer): String;
begin
  case GraGruY of
    -1: Result := '';
    CO_GraGruY_0512_VSSubPrd: Result := LowerCase('');
    CO_GraGruY_0683_VSPSPPro: Result := LowerCase('');
    CO_GraGruY_0853_VSPSPEnd: Result := LowerCase('');
    CO_GraGruY_1024_VSNatCad: Result := LowerCase('VSRibCad');
    CO_GraGruY_1072_VSNatInC: Result := LowerCase('');
    CO_GraGruY_1088_VSNatCon: Result := LowerCase('VSRibCad'); // Não testado!!!!!!!!!!!
    CO_GraGruY_1195_VSNatPDA: Result := LowerCase('');
    CO_GraGruY_1365_VSProCal: Result := LowerCase('');
    CO_GraGruY_1536_VSCouCal: Result := LowerCase('');
    CO_GraGruY_1621_VSCouDTA: Result := LowerCase('');
    CO_GraGruY_1707_VSProCur: Result := LowerCase('');
    CO_GraGruY_1877_VSCouCur: Result := LowerCase('');
    CO_GraGruY_2048_VSRibCad: Result := LowerCase('');
    CO_GraGruY_3072_VSRibCla: Result := LowerCase('');
    CO_GraGruY_4096_VSRibOpe: Result := LowerCase('');
    CO_GraGruY_5120_VSWetEnd: Result := LowerCase('');
    CO_GraGruY_6144_VSFinCla: Result := LowerCase('');
    CO_GraGruY_7168_VSRepMer: Result := LowerCase('');
    else
    begin
      Result := '';
      Geral.MB_Erro('"GraGruY" não definido em "VS_CRC_PF.ObtemNomeTabelaGraGruY()"');
    end;
  end;
end;

function TUnVS_CRC_PF.ObtemNomeTabelaVSXxxCab(MovimID: TEstqMovimID; Avisa: Boolean): String;
const
  sProcName = 'ObtemNomeTabelaVSXxxCab';
begin
  AdicionarNovosVS_emid();
  Result := CO_FIVE_ASKS;
  case MovimID of
    (*00*)//emidAjuste: Result        := 'vs';
    (*01*)emidCompra: Result          := 'vsinncab';
    (*02*)emidVenda: Result           := 'vsoutcab';
    (*03*)//emidReclasWE: Result      := 'vs';
    (*04*)//emidBaixa: Result         := 'vs';
    (*05*)//emidIndsWE: Result        := 'vs';
    (*06*)emidIndsXX: Result          := 'vsgerarta';
    (*07*)emidClassArtXXUni: Result   := 'vspaclacaba';
    (*08*)emidReclasXXUni: Result     := 'vsgerrcla';
    (*09*)emidForcado: Result         := 'vsbxacab';
    (*10*)//emidSemOrigem: Result     := 'vs';
    (*11*)emidEmOperacao: Result      := 'vsopecab';
    (*12*)emidResiduoReclas: Result   := CO_VS___Cab;
    (*13*)emidInventario: Result      := 'vsajscab';
    (*14*)emidClassArtXXMul: Result   := 'vspamulcaba';
    (*15*)emidPreReclasse: Result     := 'vsprepalcab';
    (*16*)emidEntradaPlC: Result      := 'vsplccab';
    (*17*)emidExtraBxa: Result        := 'vsbxacab';
    (*18*)emidSaldoAnterior: Result   := 'vs';
    (*19*)emidEmProcWE: Result        := 'vspwecab';
    (*20*)emidFinished: Result        := 'vspwecab';
    (*21*)emidDevolucao: Result       := 'vsdvlcab';
    (*22*)emidRetrabalho: Result      := 'vsrtbcab';
    (*23*)emidGeraSubProd: Result     := 'vssubprdcab';
    (*24*)emidReclasXXMul: Result     := 'vspamulcabr';
    (*25*)emidTransfLoc: Result       := 'vstrfloccab';
    (*26*)emidEmProcCal: Result       := 'vscalcab';
    (*27*)emidEmProcCur: Result       := 'vscurcab';
    (*28*)emidDesclasse: Result       := 'vsdsccab';
    (*29*)emidCaleado: Result         := 'vscaljmp';
    (*30*)emidEmRibPDA: Result        := 'vscalpda';
    (*31*)emidEmRibDTA: Result        := 'vscaldta';
    (*32*)emidEmProcSP: Result        := 'vspspcab';
    (*33*)emidEmReprRM: Result        := 'vsrrmcab';
    (*34*)emidCurtido: Result         := 'vscurjmp';
    (*35*)emidMixInsum: Result        := 'pqm';
    (*36*)emidInnSemCob: Result       := 'vsesccab';
    (*37*)emidOutSemCob: Result       := 'vsssccab';
    (*39*)emidEmProcCon: Result       := 'vsconcab';
    (*40*)emidConservado: Result      := 'vsconjmp';
    (*41*)emidEntraExced: Result      := 'vsexccab';
    //(*42*)emidSPCaleirad: Result         := '????';
    else
      if Avisa then
        Geral.MB_ERRO('"MovimID" ' + Geral.FF0(Integer(MovimID)) +
        ' não implementado em ' + sProcName);
  end;
end;

function TUnVS_CRC_PF.PalletDuplicad3(MaxBox: Integer;
  Pallets: array of Integer): Boolean;
  function AtualDuplicou(const Index, pal: Integer; var Res: Boolean): Boolean;
  var
    I: Integer;
  begin
    for I := 1 to MaxBox do
    begin
      if (I <> Index) and (pal <> 0) then
      begin
        Result := pal = Pallets[I];
        if Result then
        begin
          Geral.MB_Aviso('O box ' + Geral.FF0(Index + 1) +
          ' está com o mesmo pallet que o box ' + Geral.FF0(I + 1) + '!' +
          sLineBreak + 'Defina outro pallet!');
          Res := Result;
          Exit;
        end;
      end;
    end;
  end;
var
  I: Integer;
begin
  Result := False;
  //
  for I := 1 to High(Pallets) do
    if AtualDuplicou(I, Pallets[I], Result) then Exit;
end;

procedure TUnVS_CRC_PF.PesquisaDoubleVS(Campo: String; Valor: Double; TemIMEIMrt:
  Integer);
const
  Aviso  = '...';
  Titulo = 'Pesquias no movimento de couro';
  Prompt = 'IME-Is encontrados';
  //DataSource = nil;
var
  ATT_MovimID, ATT_MovimNiv: String;
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  Qry: TmySQLQuery;
  Ds: TDataSource;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
  Ds  := TDataSource.Create(Dmod);
  try
  Ds.Dataset := Qry;
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  SQL_Flds := Geral.ATS([
  SQL_NO_GGX(),
  ATT_MovimID,
  ATT_MovimNiv,
  'CAST(ggx.GraGru1 AS SIGNED) GraGru1, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.' + Campo + '=' + Geral.FFT_Dot(Valor, 15, siNegativo),
  'OR vmi.' + Campo + '=-' + Geral.FFT_Dot(Valor, 15, siNegativo),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  //'ORDER BY Controle ',
  '']);
  if DBCheck.EscolheCodigoUniGrid(Aviso, Titulo, Prompt, Ds, False, False) then
  begin
    //
  end;
  finally
    Ds.Free;
  end;
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.PesquisaNovosDadosCDR(SQLTypePsq: TSQLType;
  LstTabs: String; PesqDB: TmySQLDataBase; QrItensPorTab, (*QrTabelas,*)
  QrRegistros: TmySQLQuery; PB1: TProgressBar; LaAviso1, LaAviso2: TLabel;
  EnviarTudo: Boolean; ServerID: Integer): Boolean;
const
  sProcName = 'UnVS_CRC_PF.PesquisaNovosDadosClaReCo()';
var
  Nome, Msg, SQL_Filtro: String;
  Codigo, Itens, Ativo: Integer;
  SQLTypeInsUpd: TSQLType;
  //
  I, N: Integer;
  FRTabela: TTabelas;
  Tabelas: array of String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando');
    SQLTypeInsUpd := stIns;
    Codigo  := 0;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(QrItensPorTab, DModG.MyPID_DB, [
    DELETE_FROM + ' ' + LstTabs,
    '']);
(*
    UnDmkDAC_PF.AbreMySQLQuery0(QrTabelas, PesqDB, [
    'SHOW TABLES LIKE "VS%" ',
    '']);
*)
  //if FTabelas <> nil then
    //FTabelas.Free;
    FTabelas := TList<TTabelas>.Create;
    try
      MyList.CriaListaTabelas(Dmod.MyDB, FTabelas);
      N := 0;
      SetLength(Tabelas, N);
      //
      for I := 0 to FTabelas.Count -1 do
      begin
        FRTabela := FTabelas[I];
(*
        if FRTabela.QeiLnkOn = True then
        begin
          N := N + 1;
          SetLength(Tabelas, N);
          Tabelas[N - 1] := FRTabela.TabCria;
        end;
*)
        if DBCheck.CRCTableManageToQeiLnkOn(TCRCTableManage(FRTabela.QeiLnkOn)) then
        begin
          N := N + 1;
          SetLength(Tabelas, N);
          Tabelas[N - 1] := FRTabela.TabCria;
        end;
      end;
      //
      PB1.Position := 0;
(*
      PB1.Max := QrTabelas.RecordCount;
      QrTabelas.First;
      while not QrTabelas.Eof do
*)
      PB1.Max := N;
      for I := 0 to N - 1 do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
          'Verificando Tabela ' + Nome);
        Ativo  := 1;
        Codigo := Codigo + 1;
        //Nome   := QrTabelas.Fields[0].AsString;
        Nome := Tabelas[I];
        //if CkEnviarTudo.Checked then
        case SQLTypePsq of
          stUpSinc:
          begin
            if EnviarTudo then
              SQL_Filtro := ''
            else
              SQL_Filtro := Geral.ATS([
              'WHERE AWServerID=' + VAR_TXT_AWServerID,
              'AND AWStatSinc NOT IN (' +
              Geral.FF0(Integer(stDwnSinc)) + ',' +
              Geral.FF0(Integer(stUpSinc)) + ')']); //8 e 9
          end;
          stDwnSinc:
          begin
            if EnviarTudo then
              SQL_Filtro := '**ERRO EnviarTudo** '
            else
              SQL_Filtro := Geral.ATS([
              'WHERE AWServerID = ' + Geral.FF0(ServerID),
              'AND AWStatSinc NOT IN (' +
              Geral.FF0(Integer(stDwnSinc)) + ',' +
              Geral.FF0(Integer(stDwnNoSinc)) + ')']); //8 e 10
          end;
          else
          begin
            SQL_Filtro := '**ERRO SQLType indefinido ** ';
            Geral.MB_Erro('SQLType indefinido em ' + sProcName);
          end;
        end;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrRegistros, PesqDB, [
        'SELECT COUNT(*) ITENS',
        'FROM ' + Nome,
        SQL_Filtro,
        '']);
        //Geral.MB_SQL(nil, QrRegistros);
        Itens  := QrRegistros.FieldByName('ITENS').AsInteger;
        //if
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, SQLTypeInsUpd, LstTabs, False, [
        'Nome', 'Itens', 'Ativo'], ['Codigo'], [
        Nome, Itens, Ativo], [Codigo], False);
        //
        //QrTabelas.Next;
      end;
      UnDmkDAC_PF.AbreMySQLQuery0(QrItensPorTab, DModG.MyPID_DB, [
      'SELECT *',
      'FROM ' + LstTabs,
      'WHERE Itens > 0',
      '']);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
      //
      Result := QrItensPorTab.RecordCount > 0;
    finally
      Screen.Cursor := crDefault;
    end;
  finally
    FTabelas.Free;
  end;
end;

function TUnVS_CRC_PF.PesquisaNovosDadosClaReCo(SQLTypePsq: TSQLType; LstTabs:
  String; PesqDB: TmySQLDataBase; QrItensPorTab, QrTabelas, QrRegistros:
  TmySQLQuery; PB1: TProgressBar; LaAviso1, LaAviso2: TLabel;
  EnviarTudo: Boolean; ServerID: Integer): Boolean;
const
  sProcName = 'UnVS_CRC_PF.PesquisaNovosDadosClaReCo()';
var
  Nome, Msg, SQL_Filtro: String;
  Codigo, Itens, Ativo: Integer;
  SQLTypeInsUpd: TSQLType;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando');
    SQLTypeInsUpd := stIns;
    Codigo  := 0;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(QrItensPorTab, DModG.MyPID_DB, [
    DELETE_FROM + ' ' + LstTabs,
    '']);
    UnDmkDAC_PF.AbreMySQLQuery0(QrTabelas, PesqDB, [
    'SHOW TABLES LIKE "VS%" ',
    '']);
    PB1.Position := 0;
    PB1.Max := QrTabelas.RecordCount;
    QrTabelas.First;
    while not QrTabelas.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
        'Verificando Tabela ' + Nome);
      Ativo  := 1;
      Codigo := Codigo + 1;
      Nome   := QrTabelas.Fields[0].AsString;
      //if CkEnviarTudo.Checked then
      case SQLTypePsq of
        stUpSinc:
        begin
          if EnviarTudo then
            SQL_Filtro := ''
          else
            SQL_Filtro := Geral.ATS([
            'WHERE AWServerID=' + VAR_TXT_AWServerID,
            'AND AWStatSinc NOT IN (' +
            Geral.FF0(Integer(stDwnSinc)) + ',' +
            Geral.FF0(Integer(stUpSinc)) + ')']); //8 e 9
        end;
        stDwnSinc:
        begin
          if EnviarTudo then
            SQL_Filtro := '**ERRO EnviarTudo** '
          else
{
            SQL_Filtro := Geral.ATS([
            'WHERE AWServerID <> ' + VAR_TXT_AWServerID,
            'AND AWStatSinc NOT IN (' +
            Geral.FF0(Integer(stDwnSinc)) + ',' +
            Geral.FF0(Integer(stUpSinc)) + ')']); //stDwnSync e stUpSinc
}
            SQL_Filtro := Geral.ATS([
            'WHERE AWServerID = ' + Geral.FF0(ServerID),
            'AND AWStatSinc NOT IN (' +
            Geral.FF0(Integer(stDwnSinc)) + ',' +
            Geral.FF0(Integer(stDwnNoSinc)) + ')']); //8 e 10
        end;
        else
        begin
          SQL_Filtro := '**ERRO SQLType indefinido ** ';
          Geral.MB_Erro('SQLType indefinido em ' + sProcName);
        end;
      end;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrRegistros, PesqDB, [
      'SELECT COUNT(*) ITENS',
      'FROM ' + Nome,
      SQL_Filtro,
      '']);
      //Geral.MB_SQL(nil, QrRegistros);
      Itens  := QrRegistros.FieldByName('ITENS').AsInteger;
      //if
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, SQLTypeInsUpd, LstTabs, False, [
      'Nome', 'Itens', 'Ativo'], ['Codigo'], [
      Nome, Itens, Ativo], [Codigo], False);
      //
      QrTabelas.Next;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QrItensPorTab, DModG.MyPID_DB, [
    'SELECT *',
    'FROM ' + LstTabs,
    'WHERE Itens > 0',
    '']);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
(*
    if QrItensPorTab.RecordCount > 0 then
      BtOK.Enabled := True
    else
      Geral.MB_Info('Não há dados para serem exportados ao ERP!');
    //
*)
    Result := QrItensPorTab.RecordCount > 0;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TUnVS_CRC_PF.PalletDuplicado(MaxBox, p1, p2, p3, p4, p5,
  p6: Integer): Boolean;
  //
  function AtualDuplicou(const Index, pal: Integer; var Res: Boolean): Boolean;
  var
    I, pN: Integer;
  begin
    for I := 1 to MaxBox do
    begin
      if (I <> Index) and (pal <> 0) then
      begin
        case I of
          1: pN := p1;
          2: pN := p2;
          3: pN := p3;
          4: pN := p4;
          5: pN := p5;
          6: pN := p6;
          else pN := 0;
        end;
        Result := pal = pN;
        if Result then
        begin
          Geral.MB_Aviso('O box ' + Geral.FF0(Index) +
          ' está com o mesmo pallet que o box ' + Geral.FF0(I) + '!' +
          sLineBreak + 'Defina outro pallet!');
          Res := Result;
          Exit;
        end;
      end;
    end;
  end;
begin
  Result := False;
  //
  if AtualDuplicou(1, p1, Result) then Exit;
  if AtualDuplicou(2, p2, Result) then Exit;
  if AtualDuplicou(3, p3, Result) then Exit;
  if AtualDuplicou(4, p4, Result) then Exit;
  if AtualDuplicou(5, p5, Result) then Exit;
  if AtualDuplicou(6, p6, Result) then Exit;
  //
end;

function TUnVS_CRC_PF.PesquisaPallets(const Empresa, ClientMO, CouNiv2, CouNiv1,
  StqCenCad, StqCenLoc: Integer; const Tabela, GraGruYs: String; const GraGruXs,
  Pallets: array of Integer; const SQL_Especificos: String;
  const MovimID: TEstqMovimID; const MovimCod: Integer;
  var sVSMovImp4: String): Boolean;
  //
  function SQL_ListaPallets(): String;
  var
    I, N: Integer;
  begin
    Result := '';
    N := High(Pallets);
    for I := Low(Pallets) to N do
    begin
      Result := Result + sLineBreak + Geral.FF0(Pallets[I]);
      if I < N then
        Result := Result + ',';
    end;
  end;
  function SQL_ListaGraGruXs(): String;
  var
    I, N: Integer;
  begin
    Result := '';
    N := High(GraGruXs);
    for I := Low(GraGruXs) to N do
    begin
      Result := Result + sLineBreak + Geral.FF0(GraGruXs[I]);
      if I < N then
        Result := Result + ',';
    end;
  end;
var
  SQL_IMEI, SQL_Empresa, SQL_GraGruX, SQL_CouNiv1, SQL_CouNiv2, SQL_GraGruY,
  ATT_StatPall, SQL_Pallets, SQL_ClientMO, SQL_StqCenCad, SQL_StqCenLoc,
  SQL_MovimID, SQL_MovimCod: String;
  Qry: TmySQLQuery;
begin
  if (Length(Pallets) > 0 ) and (Trim(SQL_Especificos) <> '') then
    Geral.MB_Aviso('Foram informados pallets checados e específicos!' +
    sLineBreak + 'Serão pesquisados apenas os checados!');
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    sVSMovImp4 :=
      UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp4, DModG.QrUpdPID1,
      False, 1, '_vsmovimp4_' + Tabela);
(*
    sVSMovImp5 :=
      UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp5, DModG.QrUpdPID1,
      False, 1, '_vsmovimp5_' + Tabela);
*)
    if Empresa <> 0 then
      SQL_Empresa := 'AND vmi.Empresa=' + Geral.FF0(Empresa)
    else
      SQL_Empresa := '';
    //
    if ClientMO <> 0 then
      SQL_ClientMO := 'AND vmi.ClientMO=' + Geral.FF0(ClientMO)
    else
      SQL_ClientMO := '';
    //
    if Length(GraGruXs) > 0 then
      //SQL_GraGruX := 'AND vsp.GraGruX=' + Geral.FF0(GraGruX)
      SQL_GraGruX := 'AND vsp.GraGruX IN (' + SQL_ListaGraGruXs() + ') '
    else
      SQL_GraGruX := '';
    //
    if CouNiv2 <> 0 then
      SQL_CouNiv2 := 'AND xco.CouNiv2=' + Geral.FF0(CouNiv2)
    else
      SQL_CouNiv2 := '';
    //
    if CouNiv1 <> 0 then
      SQL_CouNiv1 := 'AND xco.CouNiv1=' + Geral.FF0(CouNiv1)
    else
      SQL_CouNiv1 := '';
    if Trim(GraGruYs) <> '' then
      SQL_GraGruY := 'AND ggy.Codigo IN (' + GraGruYs + ') '
    else
      SQL_GraGruY := '';
    //
    if StqCenCad <> 0 then
      SQL_StqCenCad := 'AND scc.Codigo=' + Geral.FF0(StqCenCad)
    else
      SQL_StqCenCad := '';
    //
    if StqCenLoc <> 0 then
      SQL_StqCenLoc := 'AND vmi.StqCenLoc=' + Geral.FF0(StqCenLoc)
    else
      SQL_StqCenLoc := '';
    //
    //
    if Integer(MovimID) <> 0 then
      SQL_MovimID := 'AND vmi.MovimID=' + Geral.FF0(Integer(MovimID))
    else
      SQL_MovimID := '';
    //
    if MovimCod <> 0 then
      SQL_MovimCod := 'AND vmi.MovimCod=' + Geral.FF0(MovimCod)
    else
      SQL_MovimCod := '';
    //
    SQL_IMEI := Geral.ATS([
    'vmi.Codigo, vmi.MovimCod IMEC, vmi.Controle IMEI, ',
    'vmi.MovimID, vmi.MovimNiv, "" NO_MovimID, "" NO_MovimNiv, ']);
    //
    ATT_StatPall := dmkPF.ArrayToTexto('vsp.StatPall', 'NO_StatPall', pvPos, True,
    sVSStatPall);
    //
    if Length(Pallets) > 0 then
    begin
      SQL_Pallets := 'AND vmi.Pallet IN (' + SQL_ListaPallets() + ') ';
    end else
    begin
      if Trim(SQL_Especificos) <> '' then
        SQL_Pallets := 'AND ' + SQL_Especificos
      else
       SQL_Pallets := '';
    end;
    //fazer ficha e testar
    Result := UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, DModG.MyPID_DB, [
    DELETE_FROM + ' ' + sVSMovImp4 + '; ',
    'INSERT INTO  ' + sVSMovImp4,
    //'SELECT vmi.Empresa, vmi.GraGruX, SUM(vmi.Pecas) Pecas,  ',
    'SELECT vmi.Empresa, vsp.GraGruX, SUM(vmi.Pecas) Pecas,  ',
    'SUM(vmi.PesoKg) PesoKg, SUM(vmi.AreaM2) AreaM2,  ',
    'SUM(AreaP2) AreaP2, SUM(ValorT) ValorT,  ',
    'SUM(vmi.SdoVrtPeca) SdoVrtPeca, SUM(vmi.SdoVrtPeso) SdoVrtPeso, ',
    'SUM(vmi.SdoVrtArM2) SdoVrtArM2, ',
    '0 LmbPeca, 0 LmbVrtPeso, 0 LmbVrtArM2, ',
    'ggx.GraGru1, CONCAT(gg1.Nome,  ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
    'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome)))  ',
    'NO_PRD_TAM_COR, vmi.Pallet, vsp.Nome NO_Pallet,  ',
    'IF(COUNT(DISTINCT(vmi.terceiro))<>1, 0, ',
    'IF(vmi.Terceiro IS NULL, 0, vmi.Terceiro)), ',
    'IF(vsp.CliStat IS NULL, 0, vsp.CliStat), ',
    'IF(vsp.Status IS NULL, 0, vsp.Status), ',
    'IF(COUNT(DISTINCT(vmi.terceiro))<>1, "", ',
    'IF((vmi.Terceiro=0) OR (vmi.Terceiro IS NULL), "",   ',
    'IF(trc.Tipo=0, trc.RazaoSocial, trc.Nome))) NO_FORNECE,  ',
    'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",   ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,  ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,  ',
    'vps.Nome NO_STATUS, ',
    'MAX(vmi.DataHora) DataHora, 0 OrdGGX,  ',
    'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
    // Parei Aqui! 2015-04-11 Ver pallet nao encerrado!
    '0 PalStat, ',
    'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
    'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
    'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
    'SUM(vmi.SdoVrtPeca * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) SdoInteiros, ',
    '0 LmbInteiros, ',
    SQL_IMEI,
    'vmi.SerieFch, vsf.Nome NO_SerieFch, vmi.Ficha, vsp.StatPall, ',
    ATT_StatPall,
    'vmi.NFeSer, vmi.NFeNum, vmi.VSMulNFeCab, ',
    'IF(scc.Codigo IS NULL, 0, scc.Codigo) StqCenCad, scc.Nome NO_StqCenCad, ',
    //'scc.Codigo StqCenCad, scc.Nome NO_StqCenCad, ',
    'vmi.StqCenLoc, scl.Nome NO_StqcenLoc, ',
    '1 Ativo  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi  ',
    'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=vmi.Pallet  ',
    //'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX  ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsp.GraGruX  ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
    'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY  ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI  ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
    'LEFT JOIN ' + TMeuDB + '.entidades  trc ON trc.Codigo=vmi.Terceiro  ',
    'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vsp.CliStat  ',
    'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vmi.Empresa  ',
    'LEFT JOIN ' + TMeuDB + '.vspalsta   vps ON vps.Codigo=vsp.Status  ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
    'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
    'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
    'LEFT JOIN ' + TMeuDB + '.stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN ' + TMeuDB + '.stqcencad  scc ON scc.Codigo=scl.Codigo ',
    'WHERE vmi.Controle <> 0  ',
    'AND vmi.Pallet <> 0 ',
    'AND vmi.SdoVrtPeca > 0 ',
    SQL_Empresa,
    SQL_ClientMO,
    SQL_GraGruX,
    SQL_CouNiv2,
    SQL_CouNiv1,
    SQL_GraGruY,
    SQL_Pallets,
    SQL_StqCenCad,
    SQL_StqCenLoc,
    SQL_MovimID,
    SQL_MovimCod,
    'GROUP BY vmi.Empresa, vmi.Pallet, vsp.GraGruX;  ',
    '']);
    //Geral.MB_SQL(nil, Qry);
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.PreencheDadosDeVSCOPCab(VSCOPCab: Integer;
  EdEmpresa: TdmkEditCB; CBEmpresa: TDmkDBLookupComboBox;
  RGTipoArea: TRadioGroup; EdGraGruX: TdmkEditCB;
  CBGraGruX: TDmkDBLookupComboBox; EdGGXDst: TdmkEditCB;
  CBGGXDst: TDmkDBLookupComboBox; EdForneceMO: TdmkEditCB;
  CBForneceMO: TDmkDBLookupComboBox; EdStqCenLoc: TdmkEditCB;
  CBStqCenLoc: TDmkDBLookupComboBox; EdOperacoes: TdmkEditCB;
  CBOperacoes: TDmkDBLookupComboBox; EdCliente: TdmkEditCB;
  CBCliente: TDmkDBLookupComboBox; EdPedItsLib: TdmkEditCB;
  CBPedItsLib: TDmkDBLookupComboBox; EdClienteMO: TdmkEditCB;
  CBClienteMO: TDmkDBLookupComboBox;
  EdCustoMO, EdVSArtCab: TdmkEdit; CBVSArtCab: TDmkDBLookupComboBox;
  EdLinCulReb, EdLinCabReb, EdLinCulSem, EdLinCabSem: TdmkEdit;
  EdReceiRecu, EdReceiRefu: TDmkEditCB; CBReceiRecu, CBReceiRefu:
  TDmkDBLookupComboBox);
var
  Qry: TmySQLQuery;
  VSPMOCab: Integer;
  //
  procedure PreencheValueVariant(DmkEdit: TdmkEdit; Campo: String);
  var
    Valor: Integer;
  begin
    Valor := Qry.FieldbyName(Campo).AsInteger;
    if (DmkEdit <> nil) and (Valor <> 0) then
      DmkEdit.ValueVariant := Valor;
  end;
  procedure PreencheKeyValue(DmkDBLookupComboBox: TDmkDBLookupComboBox; Campo: String);
  var
    Valor: Integer;
  begin
    Valor := Qry.FieldbyName(Campo).AsInteger;
    if (DmkDBLookupComboBox <> nil) and (Valor <> 0) then
      DmkDBLookupComboBox.KeyValue := Valor;
  end;
var
  VSArtCab: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQUery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM vscopcab ',
    'WHERE Codigo=' + Geral.FF0(VSCOPCab),
    '']);
    VSArtCab := Qry.FieldbyName('VSArtCab').AsInteger;
    //
    DModG.SetaFilialdeEntidade(Qry.FieldbyName('Empresa').AsInteger, EdEmpresa, CBEmpresa);
    RGTipoArea.ItemIndex := Qry.FieldbyName('TipoArea').AsInteger;
    PreencheValueVariant(EdGraGruX  , 'GraGruX');
    PreencheKeyValue(CBGraGruX  , 'GraGruX');
    PreencheValueVariant(EdGGXDst   , 'GGXDst');
    PreencheKeyValue(CBGGXDst   , 'GGXDst');
    PreencheValueVariant(EdForneceMO , 'ForneceMO');
    PreencheKeyValue(CBForneceMO , 'ForneceMO');
    PreencheValueVariant(EdStqCenLoc, 'StqCenLoc');
    PreencheKeyValue(CBStqCenLoc, 'StqCenLoc');
    PreencheValueVariant(EdOperacoes, 'Operacoes');
    PreencheKeyValue(CBOperacoes, 'Operacoes');
    PreencheValueVariant(EdCliente  , 'Cliente');
    PreencheKeyValue(CBCliente  , 'Cliente');
    PreencheValueVariant(EdPedItsLib, 'PedItsLib');
    PreencheKeyValue(CBPedItsLib, 'PedItsLib');
    PreencheValueVariant(EdClienteMO , 'ClienteMO');
    PreencheKeyValue(CBClienteMO , 'ClienteMO');
    PreencheValueVariant(EdVSArtCab, 'VSArtCab');
    PreencheKeyValue(CBVSArtCab, 'VSArtCab');
    if EdCustoMO <> nil then
    begin
      VSPMOCab := Qry.FieldbyName('VSPMOCab').AsInteger;
      if VSPMOCab <> 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQUery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM vspmocab ',
        'WHERE Codigo=' + Geral.FF0(VSPMOCab),
        '']);
        //
        EdCustoMO.ValueVariant := Qry.FieldbyName('Valor').AsFloat;
      end;
    end;
    UnDmkDAC_PF.AbreMySQLQUery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM vsartcab ',
    'WHERE Codigo=' + Geral.FF0(VSArtCab),
    '']);
    //
    PreencheValueVariant(EdLinCulReb, 'LinCulReb');
    PreencheValueVariant(EdLinCabReb, 'LinCabReb');
    PreencheValueVariant(EdLinCulSem, 'LinCulSem');
    PreencheValueVariant(EdLinCabSem, 'LinCabSem');
    PreencheValueVariant(EdReceiRecu, 'ReceiRecu');
    PreencheValueVariant(EdReceiRefu, 'ReceiRefu');
    PreencheKeyValue(CBReceiRecu, 'ReceiRecu');
    PreencheKeyValue(CBReceiRefu, 'ReceiRefu');
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.RedefineReduzidoOnPallet(MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv): (*TTipoSinal*) TTipoTrocaGgxVmiPall;
const
  sProcName = 'RedefineReduzidoOnPallet()';
var
  Index: Integer;
begin
  AdicionarNovosVS_emid();
  //
  Result := ttvpIndef;
  Index  := GetIDNiv(Integer(MovimID), Integer(MovimNiv));
  case Index of
    00000: Result := ttvpIndef;
    01000: Result := ttvpGraGrux; // Reduzido é o foco!
    02000: Result := ttvpGraGrux; // Reduzido é o foco!
    //emidReclasWE=3,
    //emidBaixa=4,
    //emidIndsWE=5,
    //emidIndsXX=6 >> eminDestCurtiXX=13, eminSorcCurtiXX=14, eminBaixCurtiXX=15,
    06013: Result := ttvpNenhum; // Reduzido é o foco! Mas não existe pallet aqui!
    06014: Result := ttvpNenhum;
    06015: Result := ttvpNenhum;

    07001: Result := ttvpNenhum;
    07002: Result := ttvpClaRcl; // Muda em três campos de dois IMVs

    08001: Result := ttvpNenhum;
    08002: Result := ttvpClaRcl; // Muda em três campos de dois IMVs

    09000: Result := ttvpGraGrux; // Reduzido é o foco!

    10000: Result := ttvpGraGrux; // Reduzido é o foco!

    11007: Result := ttvpNenhum;
    11008: Result := ttvpNenhum;
    11009: Result := ttvpGraGrux; // Reduzido é o foco!
    11010: Result := ttvpNenhum;

    12000: Result := ttvpGraGrux; // Reduzido é o foco!

    13000: Result := ttvpGraGrux; // Reduzido é o foco!

    14001: Result := ttvpNenhum;
    14002: Result := ttvpGraGrux; // Reduzido é o foco!

    15011: Result := ttvpGraGrux; // Reduzido é o foco!
    15012: Result := ttvpGraGrux; // Reduzido é o foco!

    16000: Result := ttvpGraGrux; // Reduzido é o foco!

    17000: Result := ttvpGraGrux; // Reduzido é o foco!

    18000: Result := ttvpGraGrux; // Reduzido é o foco!

    19020: Result := ttvpNenhum;
    19021: Result := ttvpNenhum;
    20022: Result := ttvpGraGrux; // Reduzido é o foco!
    19023: Result := ttvpNenhum;

    21000: Result := ttvpGraGrux; // Reduzido é o foco!

    22000: Result := ttvpGraGrux; // Reduzido é o foco!

    23000: Result := ttvpGraGrux; // Tem pallet?

    24001: Result := ttvpNenhum;
    24002: Result := ttvpGraGrux; // Reduzido é o foco!

    25027: Result := ttvpGraGrux; // Reduzido é o foco!;
    25028: Result := ttvpGraGrux; // Reduzido é o foco!;

    26029: Result := ttvpNenhum;
    26030: Result := ttvpNenhum;
    26031: Result := ttvpGraGrux; // Reduzido é o foco! Não tem Pallet!;
    26032: Result := ttvpNenhum;
    26033: Result := ttvpNenhum;

    27034: Result := ttvpNenhum;
    27035: Result := ttvpNenhum;
    27036: Result := ttvpGraGrux; // Reduzido é o foco! Não tem Pallet!;
    27037: Result := ttvpNenhum;
    27038: Result := ttvpNenhum;

    28001: Result := ttvpNenhum;
    28002: Result := ttvpGraGrux;

    29031: Result := ttvpGraGrux; // Reduzido é o foco! Não tem Pallet!;
    29032: Result := ttvpNenhum;

    32049: Result := ttvpNenhum;
    32050: Result := ttvpNenhum;
    32051: Result := ttvpGraGrux; // Reduzido é o foco!
    32052: Result := ttvpNenhum;
    32053: Result := ttvpNenhum;

    33054: Result := ttvpNenhum;
    33055: Result := ttvpNenhum;
    33056: Result := ttvpGraGrux; // Reduzido é o foco!
    33057: Result := ttvpNenhum;
    33058: Result := ttvpNenhum;

    36000: Result := ttvpGraGrux; // Reduzido é o foco!


    41000: Result := ttvpGraGrux; // Reduzido é o foco?

    else Geral.MB_Erro('Index MovimID.Nivel ' + Geral.FF0(Index) +
    ' não implementado em ' + sProcName);
  end;
end;

procedure TUnVS_CRC_PF.ReopenGraGruX_Pallet(QrGraGruX: TmySQLQuery);
begin
  AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY IN (' +
    Geral.FF0(CO_GraGruY_1536_VSCouCal) + ',' +
    Geral.FF0(CO_GraGruY_2048_VSRibCad) + ',' +
    Geral.FF0(CO_GraGruY_3072_VSRibCla) + ',' +
    Geral.FF0(CO_GraGruY_6144_VSFinCla) + ',' +
    Geral.FF0(CO_GraGruY_7168_VSRepMer) +  // ??? 2023-05-19 > não testado
    ')');
end;

procedure TUnVS_CRC_PF.ReopenIMEIsPositivos(Qry: TmySQLQuery; Empresa, GraGruX,
  Pallet, Ficha, IMEI, TipoCouro, StqCenCad, StqCenLoc, ImeiSrc, CouNiv1,
  CouNiv2: Integer; DataLimite: TDateTime);
var
  SQL_Empresa,SQL_GraGruX, SQL_Pallet, SQL_Ficha, SQL_IMEI, SQL_GraGruY,
  SQL_StqCenCad, SQL_StqCenLoc, SQL_ImeiSrc, SQL_DataLimite, SQL_CouNiv1,
  SQL_CouNiv2: String;
begin
  SQL_Empresa := '';
  SQL_Pallet  := '';
  SQL_Ficha   := '';
  SQL_IMEI    := '';
  SQL_GraGruY := '';
  SQL_StqCenCad := '';
  SQL_StqCenLoc := '';
  SQL_ImeiSrc   := '';
  SQL_DataLimite := '';
  SQL_CouNiv1 := '';
  SQL_CouNiv2 := '';
  //
  if Empresa <> 0 then
    SQL_Empresa := 'AND vmi.Empresa=' + Geral.FF0(Empresa);
  if GraGruX <> 0 then
    SQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX);
  if Pallet <> 0 then
    SQL_Pallet := 'AND vmi.Pallet=' + Geral.FF0(Pallet);
  if Ficha <> 0 then
    SQL_Ficha  := 'AND vmi.Ficha=' + Geral.FF0(Ficha);
  if IMEI <> 0 then
    SQL_IMEI  := 'AND vmi.Controle=' + Geral.FF0(IMEI);
  if TipoCouro > -1 then
    SQL_GraGruY := 'AND ggx.GragruY=' + Geral.FF0(VS_CRC_PF.ObtemGraGruY_de_IDTipoCouro(TipoCouro));
  if StqCenCad <> 0 then
    SQL_StqCenCad := 'AND scc.Codigo=' + Geral.FF0(StqCenCad);
  if StqCenLoc <> 0 then
    SQL_StqCenLoc := 'AND vmi.StqCenLoc=' + Geral.FF0(StqCenLoc);
  if ImeiSrc <> 0 then
    SQL_ImeiSrc := Geral.ATS([
    'AND vmi.MovimCod=( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI + ' ',
    '  WHERE Controle=' + Geral.FF0(ImeiSrc),
    ') ']);
  if CouNiv1 <> 0 then
    SQL_CouNiv1 := 'AND xco.CouNiv1=' + Geral.FF0(CouNiv1);
  if CouNiv2 <> 0 then
    SQL_CouNiv2 := 'AND xco.CouNiv2=' + Geral.FF0(CouNiv2);
  if DataLimite > 0 then
///////// Inicio 2019/01/21
    SQL_DataLimite := 'AND vmi.DataHora < + "' + Geral.FDT(DataLimite + 1, 1) + '"';
    //SQL_DataLimite := 'AND vmi.DataHora <= "' + Geral.FDT(DataLimite + 1, 1) + '"';
///////// Fim 2019/01/21
  //
  VS_CRC_PF.AdicionarNovosVS_emid();
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT pal.Nome NO_Pallet, vmi.*,',
  VS_CRC_PF.SQL_NO_GGX(),
  'IF(vmi.SdoVrtPeca=0, 0, vmi.SdoVrtArM2 / vmi.SdoVrtPeca) MediaM2, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  pal ON pal.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'WHERE (vmi.SdoVrtPeca > 0 ',
  '  OR (',
  '  vmi.Pecas=0 AND vmi.SdoVrtPeso > 0 ',
  '  ) ',
  // Precisa?
  //'OR vmi.SdoVrtArM2 > 0 ',
  ') ',
  'AND ( ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidCompra)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidIndsXX)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidClassArtXXUni)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidReclasXXUni)),
////////////////////////////////////////////////////////////////////////////////
  '  OR  ',
  '  (vmi.MovimID=' + Geral.FF0(Integer(emidEmOperacao)),
  '  AND ',
  '  vmi.MovimNiv=' + Geral.FF0(Integer(eminDestOper)) + ')',
////////////////////////////////////////////////////////////////////////////////
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidClassArtXXMul)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidReclasXXMul)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidEmProcWE)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidInventario)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidEntradaPlC)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidFinished)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidDevolucao)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidRetrabalho)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidGeraSubProd)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidTransfLoc)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidEmProcCal)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidEmProcCur)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidDesclasse)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidCaleado)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidEmRibPDA)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidEmRibDTA)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidEmProcSP)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidEmReprRM)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidEmProcCon)),
  '  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidConservado)),
  (*'  OR  ',
  '  vmi.MovimID=' + Geral.FF0(Integer(emidSPCaleirad)),*)
  '  OR  ',
  '  vmi.SrcMovID<>0 ',
  ') ',
  SQL_Empresa,
  SQL_GraGruX,
  SQL_Pallet,
  SQL_Ficha,
  SQL_IMEI,
  SQL_GraGruY,
  SQL_StqCenCad,
  SQL_StqCenLoc,
  SQL_ImeiSrc,
  SQL_CouNiv1,
  SQL_CouNiv2,
  SQL_DataLimite,
  'ORDER BY DataHora, Pallet, Ficha ',
  '']);
  //Geral.MB_SQL(nil, Qry);
end;

procedure TUnVS_CRC_PF.ReopenItensAptos_Cal(Qry: TmySQLQuery; Empresa,
  StqCenCad, Terceiro, ClientMO, GraGruX: Integer; ModoBxaEstq: TModoBxaEstq);
var
  SQL_GraGruX, SQL_ClientMO, SQL_Terceiro, SQL_StqCenCad, SQL_ModoBxaEstq: String;
begin
  if GraGruX <> 0 then
    SQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX)
  else
    SQL_GraGruX := 'AND vmi.GraGruX<>0 ';
  //
  if Terceiro <> 0 then
    SQL_Terceiro := 'AND vmi.Terceiro=' + Geral.FF0(Terceiro)
  else
    SQL_Terceiro := '';
  //
  if StqCenCad <> 0 then
    SQL_StqCenCad := 'AND vmi.StqCenCad=' + Geral.FF0(StqCenCad)
  else
    SQL_StqCenCad := '';
  //
  if ClientMO <> 0 then
    SQL_ClientMO := 'AND vmi.ClientMO=' + Geral.FF0(ClientMO)
  else
    SQL_ClientMO := '';
  //
  case ModoBxaEstq of
    //ModoBxaEstq.mbeIndef
    TModoBxaEstq.mbePeca: SQL_ModoBxaEstq := 'AND vmi.SdoVrtPeca > 0 ';
    TModoBxaEstq.mbePesoKg: SQL_ModoBxaEstq := 'AND (vmi.Pecas=0 AND vmi.SdoVrtPeso > 0) ';
    else SQL_ModoBxaEstq := 'AND **ModoBxaEstq??** > 0 ';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT vmi.Controle, vmi.Empresa, vmi.ClientMO, ',
  'vmi.GraGruX, vmi.PesoKg, vmi.Pecas, vmi.AreaM2, ',
  'vmi.SdoVrtPeca, vmi.SdoVrtPeso, vmi.SdoVrtArM2, ',
  'FLOOR((vmi.SdoVrtArM2 / 0.09290304)) + ',
  'FLOOR(((MOD((vmi.SdoVrtArM2 / 0.09290304), 1)) + ',
  '0.12499) * 4) * 0.25 SdoVrtArP2, ValorT, ',
  'ggx.GraGru1, ggx.GraGruY, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vmi.Pallet, vsp.Nome NO_Pallet, ',
  'vmi.Terceiro, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  '"" NO_STATUS, ',
  '"0000-00-00 00:00:00" DataHora, 0 OrdGGX, ',
  'ggy.Ordem, ggy.Codigo CodiGGY, ggy.Nome, ',
  'vmi.Codigo, vmi.MovimCod IMEC, vmi.Controle IMEI, ',
  'vmi.MovimID, vmi.MovimNiv, "" NO_MovimID, "" NO_MovimNiv, ',
  'vmi.VSMulFrnCab, vmi.SerieFch, vmi.Ficha, vmi.Marca, ',
  'vsf.Nome NO_SerieFch, vnc.GGXInProc,',
  'vmi.ValorT * (vmi.SdoVrtPeso / vmi.PesoKg) SdoVrtValr, ',
  'vmi.SdoVrtPeso / vmi.SdoVrtPeca Kg_Peca, ',
  'vmi.ValorT / vmi.PesoKg Valor_kg, ',
  '1 Ativo ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=IF(vmi.Pallet <> 0, vsp.GraGruX, vmi.GraGruX) ',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro ',
  'LEFT JOIN entidades  emp ON emp.Codigo=vmi.Empresa ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN vsnatcad   vnc ON vnc.GraGruX=vmi.GraGruX ',
  'WHERE ggx.GragruY IN (' +
  //Geral.FF0(CO_GraGruY_1365_VS??????) + ',' +
  Geral.FF0(CO_GraGruY_0512_VSSubPrd) + ',',
  Geral.FF0(CO_GraGruY_1024_VSNatCad) + ',',
  Geral.FF0(CO_GraGruY_1088_VSNatCon) + ')',
  'AND vmi.Controle <> 0 ',
  SQL_GraGruX,
  SQL_ClientMO,
  SQL_Terceiro,
  SQL_StqCenCad,
  //'AND vmi.SdoVrtPeca > 0 ',
  SQL_ModoBxaEstq,
  //'AND vmi.Pallet = 0 ',  1088 usa pallet!!
  'AND vmi.Empresa=' + Geral.FF0(Empresa),
  ' ']);
  //Geral.MB_Teste(Qry.SQL.Text);
end;

procedure TUnVS_CRC_PF.ReopenItensPedido(Qr: TmySQLQuery; Pedido: Integer);
var
  Corda: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT DISTINCT Codigo ',
  'FROM vspedits ',
  'WHERE Codigo=' + Geral.FF0(Pedido),
  'AND Status=' + Geral.FF0(Integer(TStausPedVda.spvLiberado)),
  '']);
  Corda := MyObjects.CordaDeQuery(Dmod.QrAux, 'Codigo', '-999999999');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr, Dmod.MyDB, [
  'SELECT vpi.Controle, vpi.GraGruX, vpi.PrecoTipo, ',
  'vpi.PrecoMoeda, vpi.PrecoVal, vpi.PercDesco, vpi.PercTrib, ',
  'CASE vpi.PrecoTipo',
  '  WHEN 1 THEN vpi.Pecas - vpi.VdaPecas ',
  '  WHEN 2 THEN vpi.PesoKg - vpi.VdaPesoKg',
  '  WHEN 3 THEN vpi.AreaM2 - vpi.VdaAreaM2',
  '  WHEN 4 THEN vpi.AreaP2 - vpi.VdaAreaP2',
  '  ELSE 0.000',
  'END SaldoQtd, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),',
  '" ", vpc.PedidCli, " ", Texto) ',
  'NO_PRD_TAM_COR ',
  'FROM vspedits vpi',
  'LEFT JOIN vspedcab   vpc ON vpc.Codigo=vpi.Codigo',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vpi.GraGruX',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE vpi.Status=' + Geral.FF0(Integer(TStausPedVda.spvLiberado)),
  'AND vpi.Codigo IN (' + Corda + ')',
  'AND vpi.Codigo=' + Geral.FF0(Pedido),
  'ORDER BY NO_PRD_TAM_COR',
  '']);
end;

procedure TUnVS_CRC_PF.ReopenPedItsXXX(QrVSPedIts: TmySQLQuery; InsPedIts,
  UpdPedIts: Integer);
begin
{$IfDef sAllVS}
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPedIts, Dmod.MyDB, [
  'SELECT vpi.Controle, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome)), ',
  '" ", vpc.PedidCli, " ", Texto)  ',
  'NO_PRD_TAM_COR ',
  'FROM vspedits vpi ',
  'LEFT JOIN vspedcab   vpc ON vpc.Codigo=vpi.Codigo ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vpi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE vpi.VSMovIts IN (' +
  Geral.FF0(UpdPedIts) + ',' + Geral.FF0(UpdPedIts) + ') ', // ????
  '']);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TUnVS_CRC_PF.ReopenPrestadorMO(Qry: TmySQLQuery; SQL_AND: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT fmo.Codigo, ',
  'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NOMEENTIDADE ',
  'FROM entidades fmo ',
  'WHERE fmo.Fornece7="V" ',
  SQL_AND,
  'ORDER BY NOMEENTIDADE',
  '']);
end;

procedure TUnVS_CRC_PF.ReopenQrySaldoIMEI_Baixa(Qry1, Qry2: TmySQLQuery;
  SrcNivel2: Integer);
begin
////////////////////////////////////////////////////////////////////////////////
///  C U I D A D O ! ! !
///  C U I D A D O ! ! !
///  Não mexer aqui!
///  Esta procedure abre a query que atualiza o estoque do IMEI !!!!!!!!!!!!!!!!
///  C U I D A D O ! ! !
///  C U I D A D O ! ! !
////////////////////////////////////////////////////////////////////////////////
(*
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT SUM(vmi.Pecas) Pecas, ',
  'SUM(vmi.PesoKg) PesoKg, ',
  'SUM(vmi.AreaM2) AreaM2, ',
  'SUM(vmi.QtdGerPeca) QtdGerPeca, ',
  'SUM(vmi.QtdGerPeso) QtdGerPeso, ',
  'SUM(vmi.QtdGerArM2) QtdGerArM2, ',
  'SUM(vmi.QtdGerArP2) QtdGerArP2 ',
  'FROM v s m o v i t s vmi ',
  'WHERE vmi.SrcNivel2=' + Geral.FF0(SrcNivel2),
  'AND (vmi.Pecas + vmi.PesoKg < 0 ',
  // Para couros que sobraram e tem baixa "nao negativa"
  //'OR vmi.MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidExtraBxa)),  //17
  'OR vmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_VS + ') ',
  ') ',
  '']);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(Qry1, Dmod.MyDB, [
  'SELECT SUM(vmi.Pecas) Pecas, ',
  'SUM(vmi.PesoKg) PesoKg, ',
  'SUM(vmi.AreaM2) AreaM2, ',
  'SUM(vmi.QtdGerPeca) QtdGerPeca, ',
  'SUM(vmi.QtdGerPeso) QtdGerPeso, ',
  'SUM(vmi.QtdGerArM2) QtdGerArM2, ',
  'SUM(vmi.QtdGerArP2) QtdGerArP2 ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'WHERE vmi.SrcNivel2=' + Geral.FF0(SrcNivel2),
  'AND (vmi.Pecas + vmi.PesoKg < 0 ',
  // Para couros que sobraram e tem baixa "nao negativa"
  //'OR vmi.MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidExtraBxa)),  //17
  'OR vmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_VS + ') ',
  ') ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
  'SELECT SUM(vmi.Pecas) Pecas, ',
  'SUM(vmi.PesoKg) PesoKg, ',
  'SUM(vmi.AreaM2) AreaM2, ',
  'SUM(vmi.QtdGerPeca) QtdGerPeca, ',
  'SUM(vmi.QtdGerPeso) QtdGerPeso, ',
  'SUM(vmi.QtdGerArM2) QtdGerArM2, ',
  'SUM(vmi.QtdGerArP2) QtdGerArP2 ',
  'FROM ' + CO_TAB_VMB + ' vmi ',
  'WHERE vmi.SrcNivel2=' + Geral.FF0(SrcNivel2),
  'AND (vmi.Pecas + vmi.PesoKg < 0 ',
  // Para couros que sobraram e tem baixa "nao negativa"
  //'OR vmi.MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidExtraBxa)),  //17
  'OR vmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_VS + ') ',
  ') ',
  '']);
///  C U I D A D O ! ! !
///  C U I D A D O ! ! !
  //Geral.MB_SQL(nil, Qry1);
  //Geral.MB_SQL(nil, Qry2);
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
end;

procedure TUnVS_CRC_PF.ReopenQrySaldoPallet_Visual(Qry: TmySQLQuery; Empresa,
  Pallet, VSMovIts: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT ',
  'SUM(vmi.SdoVrtPeca) SdoVrtPeca, ',
  'SUM(vmi.SdoVrtPeso) SdoVrtPeso, ',
  'SUM(vmi.SdoVrtArM2) SdoVrtArM2',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
  'WHERE vmi.Controle=' + Geral.FF0(VSMovIts),
  'AND vmi.Pallet=' + Geral.FF0(Pallet),
  //'AND vmi.SdoVrtPeca > 0 ',
  'AND vmi.Empresa=' + Geral.FF0(Empresa),
  '']);
  //Geral.MB_Teste(Qry.SQL.Text);
end;

procedure TUnVS_CRC_PF.ReopenStqCenLoc(Qry: TmySQLQuery; Centro: Variant;
  Controle: Integer; MovimID: TEstqMovimID);
  procedure Abre(MovID: TEstqMovimID);
  var
    SQL_StqCenCad, SQL_MovimID_LJ, SQL_MovimID_WH: String;
  begin
    if (Centro <> Null) and (Centro <> Unassigned) then
      SQL_StqCenCad := 'WHERE scl.Codigo=' + Geral.FF0(Centro)
    else
      SQL_StqCenCad := 'WHERE scl.Codigo>-999999999';
    //
    if MovID = TEstqMovimID.emidAjuste then
    begin
      SQL_MovimID_LJ := '';
      SQL_MovimID_WH := '';
    end else
    begin
      SQL_MovimID_LJ := 'LEFT JOIN vsmovidloc mil ON mil.StqCenLoc=scl.Controle';
      SQL_MovimID_WH := 'AND mil.MovimID=' + Geral.FF0(Integer(MovID));
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT scc.EntiSitio, scl.Controle, scl.CodUsu,  ',
      'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ',
      'FROM stqcenloc scl ',
      'LEFT JOIN stqcencad scc ON scc.Codigo=scl.Codigo ',
      SQL_MovimID_LJ,
      SQL_StqCenCad,
      SQL_MovimID_WH,
      VAR_CRC_StqCen_LOC,
      'ORDER BY NO_LOC_CEN ',
      '']);
    if Controle <> 0 then
      Qry.Locate('Controle', Controle, []);
    //Geral.MB_SQL(nil, Qry);
  end;
begin
  if Qry.Params.Count = 0 then
  begin
    Qry.Params.Add;
    Qry.Params[0].Value := Centro;
  end;
  Abre(MovimID);
  if (Qry.RecordCount = 0) and (MovimID <> TEstqMovimID.emidAjuste) then
  begin
    Abre(TEstqMovimID.emidAjuste);
  end;
end;

procedure TUnVS_CRC_PF.ReopenVSCOPCab(Query: TmySQLQuery;
  MovimID: TEstqMovimID);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
  'SELECT * ',
  'FROM vscopcab ',
  'WHERE MovimID=' + Geral.FF0(Integer(MovimID)),
  'ORDER BY Nome ',
  '']);
end;

procedure TUnVS_CRC_PF.ReopenVSESCPalDst_ToClassPorPallet(
  QrVSESCPalNew: TmySQLQuery; Pallet: Integer);
var
  SQL_Pallet: String;
begin
  if Pallet = 0 then
    SQL_Pallet := 'AND vmi.Pallet<>0'
  else
    SQL_Pallet := 'AND vmi.Pallet=' + Geral.FF0(Pallet);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSESCPalNew, Dmod.MyDB, [
  'SELECT vmi.*, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, ',
  'IF(vmi.Terceiro=0, "Vários",  ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) ',
  ') NO_FORNECE,  ',
  'IF(vmi.Ficha=0, "Várias", vmi.Ficha) NO_FICHA,  ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2,  ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2,  ',
  'CONCAT(CONVERT(vmi.Controle, CHAR), " ", gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL,"", CONCAT(" ", gcc.Nome)),  ',
  'IF(vmi.Ficha=0, "", CONCAT(" - Série / Ficha RMP ", vmi.Ficha))) ',
  'IMEI_NO_PRD_TAM_COR_FICHA, ggx.GraGruY ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet  ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro ',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'WHERE vmi.SdoVrtPeca <> 0  ',
  'AND vmi.MovimID IN (36) ',
  SQL_Pallet,
  'AND ggx.GraGruY=2048 ',
  'ORDER BY NO_PRD_TAM_COR, vmi.Codigo   ',
  '']);
end;

procedure TUnVS_CRC_PF.ReopenVSGerArtDst(Qry: TmySQLQuery; MovimCod, Controle,
  TemIMEIMrt: Integer; EstqMovimNiv: TEstqMovimNiv);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'IF(vmi.Terceiro=0, "Vários", ',
  '  IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome)',
  ') NO_FORNECE, ',
  'IF(vmi.Ficha=0, "Várias", CONCAT(IF(vsf.Nome IS NULL, ',
  '"?", vsf.Nome), " ", vmi.Ficha)) NO_FICHA, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, ',
  'vsp.Nome NO_Pallet, med.Grandeza, vmi.CredPereImposto, ',
  'vmi.CustoPQ, vmi.MedEClas ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'LEFT JOIN unidmed    med ON med.Codigo=gg1.UnidMed ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(EstqMovimNiv)),//eminDestCurtiXX)),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet, Controle ',
  '']);
  //Geral.MB_SQL(nil, Qry);
  Qry.Locate('Controle', Controle, []);
end;

procedure TUnVS_CRC_PF.ReopenVSGerArtDst_ToClassPorFicha(
  QrVSGerArtNew: TmySQLQuery; Ficha, Controle: Integer; SoNaoZerados: Boolean);
var
  SQL_Ficha, SQL_Extra: String;
begin
  if Ficha = 0 then
    SQL_Ficha := ''
  else
    SQL_Ficha := 'AND vmi.Ficha=' + Geral.FF0(Ficha);
  //
  if SoNaoZerados then
    SQL_Extra := 'AND vmi.SdoVrtPeca <> 0'
  else
    SQL_Extra := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerArtNew, Dmod.MyDB, [
  'SELECT vmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,',
  'IF(vmi.Terceiro=0, "Vários", ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
  ') NO_FORNECE, ',
  'IF(vmi.Ficha=0, "Várias", vmi.Ficha) NO_FICHA, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2, ',
  'CONCAT(CONVERT(vmi.Controle, CHAR), " ", gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome)), ',
  'IF(vmi.Ficha=0, "", CONCAT(" - Série / Ficha RMP ", vmi.Ficha))) IMEI_NO_PRD_TAM_COR_FICHA ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
{
  'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminDestCurtiXX)),
  SQL_Ficha,
  SQL_Extra,
  // Aqui vê se já foi liberado para configurar e ainda não foi configurado!
  'AND vmi.MovimCod IN ( ',
  'SELECT MovimCod  ',
  '  FROM vsgerarta ',
  '  WHERE DtHrLibCla > "1900-01-01" ',
  '  AND DtHrCfgCla < "1900-01-01" ',
  ') ',
  'ORDER BY NO_PRD_TAM_COR, vmi.Codigo ',
}
  'WHERE vmi.SdoVrtPeca <> 0 ',
  SQL_Ficha,
  SQL_Extra,
  'AND (( ',
  '  vmi.MovimNiv=13 ',
  '  AND vmi.MovimCod IN (  ',
  '    SELECT MovimCod   ',
  '    FROM vsgerarta  ',
  '    WHERE DtHrLibCla > "1900-01-01"  ',
  '    AND DtHrCfgCla < "1900-01-01"  ',
  '    ) ',
  '  ) ',
  ' ',
  'OR ',
  ' ',
  '(vmi.MovimID IN (25) ',
  'AND vmi.Pallet=0 ',
  'AND ggx.GraGruY=2048) ',
  ') ',
  ' ',
  'ORDER BY NO_PRD_TAM_COR, vmi.Codigo  ',  '']);
  if Controle <> 0 then
    QrVSGerArtNew.Locate('Controle', Controle, []);
  //Geral.MB_SQL(nil, QrVSGerArtNew);
end;

procedure TUnVS_CRC_PF.ReopenVSGerArtDst_ToClassPorIMEI(
  QrVSGerArtNew: TmySQLQuery; IMEI: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerArtNew, Dmod.MyDB, [
  'SELECT vmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,',
  'IF(vmi.Terceiro=0, "Vários", ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
  ') NO_FORNECE, ',
  'IF(vmi.Ficha=0, "Várias", vmi.Ficha) NO_FICHA, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2, ',
  'CONCAT(CONVERT(vmi.Controle, CHAR), " ", gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL,"", CONCAT(" ", gcc.Nome)), ',
  'IF(vmi.Ficha=0, "", CONCAT(" - Série / Ficha RMP ", vmi.Ficha))) IMEI_NO_PRD_TAM_COR_FICHA ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro',
  'WHERE vmi.Controle=' + Geral.FF0(IMEI),
  // Aqui vê se já foi liberado para configurar e ainda não foi configurado!
  'AND vmi.MovimCod IN ( ',
  'SELECT MovimCod  ',
  '  FROM vsgerarta ',
  '  WHERE DtHrLibCla > "1900-01-01" ',
  '  AND DtHrCfgCla < "1900-01-01" ',
  ') ',
  '']);
end;

procedure TUnVS_CRC_PF.ReopenVSGerArtSrc(Qry: TmySQLQuery; MovimCod, Controle,
  TemIMEIMrt: Integer; SQL_Limit: String; EstqMovimNiv: TEstqMovimNiv);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet, ',
  'vmi.ValorT/vmi.QtdGerArM2 CustoAreaMP, CustoPQ ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(EstqMovimNiv)),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  SQL_Limit,
  '']);
  //
  //Geral.MB_SQL(nil, Qry);
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;

{
procedure TUnVS_CRC_PF.ReopenVSItsBxa(Qry: TmySQLQuery; SrcMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2, TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv;
  LocCtrl: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet, ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro ',
  '']);
  //'FROM ' + VS_CRC_PF.TabMovVS_Tab(tab) + ' vmi ',
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID(SrcMovID))),
  'AND vmi.SrcNivel1=' + Geral.FF0(SrcNivel1),
  'AND vmi.SrcNivel2=' + Geral.FF0(SrcNivel2),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv(MovimNiv))),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //Geral.MB_SQL(Self, QrVSSubPrdIts);
  //
  if LocCtrl <> 0 then
    Qry.Locate('Controle', LocCtrl, []);
end;
}

procedure TUnVS_CRC_PF.ReopenVSItsBxa(Qry: TmySQLQuery; SrcMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2, TemIMEIMrt: Integer; MovimNivs: array of TEstqMovimNiv;
  LocCtrl: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  I: Integer;
  Nivs, Virgula: String;
begin
  Virgula := '';
  Nivs := '';
  for I := 0 to High(MovimNivs) do
  begin
    Nivs := Nivs + Virgula + Geral.FF0(Integer(MovimNivs[I]));
    Virgula := ',';
  end;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet, ',
  'IF(vmi.PesoKg <> 0.00, vmi.ValorT / vmi.PesoKg, 0.00) CustoKg, ',
  'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro ',
  '']);
  //'FROM ' + VS_CRC_PF.TabMovVS_Tab(tab) + ' vmi ',
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
(* ini 2023-04-10
  'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID(SrcMovID))),
  'AND vmi.SrcNivel1=' + Geral.FF0(SrcNivel1),
  'AND vmi.SrcNivel2=' + Geral.FF0(SrcNivel2),
  'AND vmi.MovimNiv IN (' + Nivs + ') ',
*)
  'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID(SrcMovID))),
  'AND (',
  '  vmi.MovimNiv IN (' + Nivs + ') ',
  '  OR  ',
  '  vmi.MovimID IN (' + CO_ALL_CODS_BXA_EXTRA_SQL_VS + ') ',
  ') ',
  'AND vmi.SrcNivel1=' + Geral.FF0(SrcNivel1),
  'AND vmi.SrcNivel2=' + Geral.FF0(SrcNivel2),
  //  fim 2023-04-10
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //Geral.MB_Teste(Qry.SQL.Text);
  //
  if LocCtrl <> 0 then
    Qry.Locate('Controle', LocCtrl, []);
end;

procedure TUnVS_CRC_PF.ReopenVSItsBxa_Medias(Qry: TmySQLQuery;
  SrcMovID: TEstqMovimID; SrcNivel1, SrcNivel2, TemIMEIMrt: Integer;
  MovimNivs: array of TEstqMovimNiv);
var
//  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  I: Integer;
  Nivs, Virgula: String;
begin
  Virgula := '';
  Nivs := '';
  for I := 0 to High(MovimNivs) do
  begin
    Nivs := Nivs + Virgula + Geral.FF0(Integer(MovimNivs[I]));
    Virgula := ',';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _VMI_MEDIAS_AREA_SEM_GELATINA_;',
  '',
  'CREATE TABLE _VMI_MEDIAS_AREA_SEM_GELATINA_',
  '',
  'SELECT SUM(Pecas) Pecas, ',
  'SUM(QtdGerArM2) QtdGerArM2,  ',
  'SUM(PesoKg) PesoKg',
  ' ',
  'FROM ' + TMeuDB + '.' + CO_TAB_VMI + ' vmi  ',
  'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID(SrcMovID))),
  'AND vmi.SrcNivel1=' + Geral.FF0(SrcNivel1),
  'AND vmi.SrcNivel2=' + Geral.FF0(SrcNivel2),
  'AND vmi.MovimNiv IN (' + Nivs + ') ',
  'AND vmi.QtdGerArM2 <> 0 ',
  'AND vmi.QtdGerPeso = 0 ',
  '',
  'UNION',
  '',
  'SELECT SUM(Pecas) Pecas, ',
  'SUM(QtdGerArM2) QtdGerArM2,  ',
  'SUM(PesoKg) PesoKg',
  ' ',
  'FROM ' + TMeuDB + '.' + CO_TAB_VMB + ' vmi  ',
  'WHERE vmi.SrcMovID=' + Geral.FF0(Integer(TEstqMovimID(SrcMovID))),
  'AND vmi.SrcNivel1=' + Geral.FF0(SrcNivel1),
  'AND vmi.SrcNivel2=' + Geral.FF0(SrcNivel2),
  'AND vmi.MovimNiv IN (' + Nivs + ') ',
  'AND vmi.QtdGerArM2 <> 0 ',
  'AND vmi.QtdGerPeso = 0 ',
  ';',
  '',
  'SELECT SUM(Pecas) Pecas, ',
  'SUM(QtdGerArM2) QtdGerArM2, ',
  'SUM(QtdGerArM2) / SUM(Pecas) M2_PC, ',
  'SUM(PesoKg) PesoKg, ',
  'SUM(PesoKg) / SUM(Pecas) Kg_PC, ',
  'SUM(PesoKg) / SUM(QtdGerArM2) Kg_M2  ',
  'FROM _VMI_MEDIAS_AREA_SEM_GELATINA_',
  ';',
  'DROP TABLE IF EXISTS _VMI_MEDIAS_AREA_SEM_GELATINA_ ',
  '; ',
  '']);
  //
end;

procedure TUnVS_CRC_PF.ReopenVSIts_Controle_If(Qry: TmySQLQuery; Controle,
  TemIMEIMrt: Integer);
var
  Ctrl_TXT: String;
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  if Controle <> 0 then
    Ctrl_TXT := Geral.FF0(Controle)
  else
    Ctrl_TXT := '-999999999';
  //
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'vsp.Nome NO_Pallet, ',
  'IF(vmi.Misturou = 1, "SIM", "NÃO") Misturou_TXT, ',
  'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NO_FORNECMO, ',
  'IF(AreaM2 > 0, ValorT / AreaM2, 0) Custo_m2 ',
  '']);
  //'FROM ' + VS_CRC_PF.TabMovVS_Tab(tab) + ' vmi ',
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  fmo ON fmo.Codigo=vmi.FornecMO ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.Controle=' + Ctrl_TXT,
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY DataHora, Controle ',
  '']);
  //Geral.MB_teste(Qry.SQL.Text);
end;

procedure TUnVS_CRC_PF.ReopenVSListaPallets(Qry: TmySQLQuery; Tabela: String;
  Empresa, GraGruX, Pallet, Terceiro: Integer; OrderBy: String);
var
 Ordem: String;
begin
  Ordem := Trim(OrderBy);
  if Ordem = '' then
    Ordem := 'ORDER BY vmi.Pallet DESC ';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
  'SELECT CONCAT(NO_StqCenLoc, " (", NO_StqCenCad, ")") NO_LOC_CEN, vmi.*,  ',
  'IF(vmi.SdoVrtPeca=0, 0, vmi.SdoVrtArM2 / vmi.SdoVrtPeca) MediaM2 ',
  'FROM  ' + Tabela + ' vmi ',
  'WHERE (vmi.Pecas >= 0.001 OR vmi.Pecas <= -0.001) ',
  'AND vmi.SdoVrtPeca > 0 ',
  //'ORDER BY NO_PALLET, NO_FORNECE, OrdGGX, NO_PRD_TAM_COR ',
  Ordem,
  '']);
  //Geral.MB_SQL(nil, Qry);
  //
  Qry.Locate('Empresa;GraGruX;Pallet;Terceiro',
  VarArrayOf([Empresa, GraGruX, Pallet, Terceiro]), []);
end;

procedure TUnVS_CRC_PF.ReopenVSMOEnvAVMI(QrVSMOEnvAVmi: TmySQLQuery; VSMOEnvAvu,
  Codigo: Integer);
begin
{$IfDef sAllVS}
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMOEnvAVMI, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsmoenvavmi',
  'WHERE VSMOEnvAvu=' + Geral.FF0(VSMOEnvAvu),
  '']);
  if Codigo <> 0 then
    QrVSMOEnvAVmi.Locate('Codigo', Codigo, []);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TUnVS_CRC_PF.ReopenVSMOEnvAvu(QrVSMOEnvAvu: TmySQLQuery; MovimCod,
  Codigo: Integer);
begin
{$IfDef sAllVS}
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMOEnvAvu, Dmod.MyDB, [
  'SELECT cmo.*  ',
  'FROM vsmoenvavu cmo ',
  'WHERE cmo.VSVMI_MovimCod=' + Geral.FF0(MovimCod),
  '']);
  //
  if Codigo <> 0 then
    QrVSMOEnvAvu.Locate('Codigo', Codigo, []);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TUnVS_CRC_PF.ReopenVSMOEnvEnv(QrVSMOEnvEnv: TmySQLQuery; MovimCod,
  Codigo: Integer);
begin
{$IfDef sAllVS}
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMOEnvEnv, Dmod.MyDB, [
  'SELECT cmo.*  ',
  'FROM vsmoenvenv cmo ',
  'WHERE cmo.VSVMI_MovimCod=' + Geral.FF0(MovimCod),
  '']);
  //
  if Codigo <> 0 then
    QrVSMOEnvEnv.Locate('Codigo', Codigo, []);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TUnVS_CRC_PF.ReopenVSMOEnvEVMI(QrVSMOEnvEVmi: TmySQLQuery; VSMOEnvEnv,
  Codigo: Integer);
begin
{$IfDef sAllVS}
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMOEnvEVMI, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsmoenvevmi',
  'WHERE VSMOEnvEnv=' + Geral.FF0(VSMOEnvEnv),
  '']);
  if Codigo <> 0 then
    QrVSMOEnvEVmi.Locate('Codigo', Codigo, []);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TUnVS_CRC_PF.ReopenVSMOEnvGVmi(QrVSMOEnvGVmi: TmySQLQuery; VSMOEnvRet,
  Codigo: Integer);
begin
  {$IfDef sAllVS}
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMOEnvGVmi, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsmoenvgvmi ',
  'WHERE VSMOEnvRet=' + Geral.FF0(VSMOEnvRet),
  '']);
  //
  if Codigo <> 0 then
    QrVSMOEnvGVmi.Locate('Codigo', Codigo, []);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TUnVS_CRC_PF.ReopenVSMOEnvRet(QrVSMOEnvRet: TmySQLQuery; MovimCod,
  Codigo: Integer);
begin
{$IfDef sAllVS}
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMOEnvRet, Dmod.MyDB, [
  'SELECT DISTINCT cmo.*  ',
  'FROM vsmoenvret cmo ',
  'LEFT JOIN vsmoenvgvmi mog ON mog.VSMOEnvRet=cmo.Codigo',
  'WHERE mog.VSMovimCod=' + Geral.FF0(MovimCod),
  '']);
  //Geral.MB_SQL(nil, QrVSMOEnvRet);
  if Codigo <> 0 then
    QrVSMOEnvRet.Locate('Codigo', Codigo, []);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TUnVS_CRC_PF.ReopenVSMOEnvRVmi(QrVSMOEnvRVmi: TmySQLQuery; VSMOEnvRet,
  Codigo: Integer);
begin
  {$IfDef sAllVS}
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMOEnvRVmi, Dmod.MyDB, [
  'SELECT vee.NFEMP_SerNF, vee.NFEMP_nNF, ',
  'mev.* ',
  'FROM vsmoenvrvmi mev',
  'LEFT JOIN vsmoenvenv vee ON vee.Codigo=mev.VSMOEnvEnv',
  'WHERE VSMOEnvRet=' + Geral.FF0(VSMOEnvRet),
  '']);
  //
  if Codigo <> 0 then
    QrVSMOEnvRVmi.Locate('Codigo', Codigo, []);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TUnVS_CRC_PF.ReopenVSMovIts_Pallet2(QrVSMovIts,
  QrPallets: TmySQLQuery);
var
  GraGruX, Empresa, Pallet: Integer;
begin
  ////////////////////////////////////////////////////////////
  //QrEstqR4 >> GROUP BY vmi.Empresa, vmi.Pallet, vmi.GraGruX
  /////////////////////////////////////////////////////////////
  GraGruX    := QrPallets.FieldByName('GraGruX').AsInteger;
  Empresa    := QrPallets.FieldByName('Empresa').AsInteger;
  Pallet     := QrPallets.FieldByName('Pallet').AsInteger;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT pal.Nome NO_Pallet, vmi.*, ggx.GraGruY, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE,  ',
  'IF(vmi.SdoVrtPeca=0, 0, vmi.SdoVrtArM2 / vmi.SdoVrtPeca) MediaM2 ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN vspalleta pal ON pal.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades ent ON ent.Codigo=vmi.Terceiro',
  //'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX',
  'LEFT JOIN gragrux ggx ON ggx.Controle=pal.GraGruX',
  'WHERE vmi.Empresa=' + Geral.FF0(Empresa),
  //'AND vmi.GraGruX=' + Geral.FF0(GraGruX),
  'AND pal.GraGruX=' + Geral.FF0(GraGruX),
  'AND vmi.Pallet=' + Geral.FF0(Pallet),
  'AND SdoVrtPeca> 0 ',
  'ORDER BY DataHora, Pallet, Controle',
  '']);
end;

procedure TUnVS_CRC_PF.ReopenVSMovXXX(Qry: TmySQLQuery; Campo: String; IMEI,
  TemIMEIMrt, CtrlLoc: Integer);
var
  ATT_MovimID, ATT_MovimNiv: String;
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  //ReopenVSOpeOriIMEI(QrVSPWEOriIMEI, QrVSPWECabMovimCod.Value, Controle);
  SQL_Flds := Geral.ATS([
  SQL_NO_GGX(),
  ATT_MovimID,
  ATT_MovimNiv,
  'CAST(ggx.GraGru1 AS SIGNED) GraGru1, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, ',
  'vmi.ValorT/vmi.AreaM2 CustoM2, vmi.CustoPQ ',
  '']);
  SQL_Left := Geral.ATS([
  SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.' + Campo + '=' + Geral.FF0(IMEI),  //QrVSMovItsControle.Value
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  //'ORDER BY Controle ',
  '']);
  //
  if IMEI <> 0 then
    Qry.Locate('Controle', IMEI, []);
  //Geral.MB_Teste(Qry.SQL.Text);
end;

procedure TUnVS_CRC_PF.ReopenVSOpePrcBxa(Qry: TmySQLQuery; MovimCod, MovimTwn,
  Controle, TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  //TemIMEIMrt: Integer;
begin
  //TemIMEIMrt := QrVSPWECabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
  //'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminEmWEndBxa)),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  'AND vmi.MovimTwn=' + Geral.FF0(MovimTwn),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //
  //Geral.MB_SQL(nil, Qry);
end;

procedure TUnVS_CRC_PF.ReopenVSOpePrcDst(Qry: TmySQLQuery; MovimCod, Controle,
  TemIMEIMrt: Integer; MovimNiv: TEstqMovimNiv; SQL_Limit: String);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, SQL_IndsVS: String;
begin
  case MovimNiv of
    eminDestCal, //=31,
    eminDestCur: //=36
    begin
      SQL_IndsVS := Geral.ATS([
      'OR (MovimID=' + Geral.FF0(Integer(emidIndsXX)), // 6
      '    AND SrcNivel2 IN ',
      '    ( ',
      '      SELECT Controle ',
      '      FROM ' + CO_SEL_TAB_VMI + ' ',
      '      WHERE MovimCod=' + Geral.FF0(MovimCod),
      '    ) ',
      ') ',
      '']);
    end;
    else SQL_IndsVS := '';
  end;
  //
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  VS_CRC_PF.SQL_NO_FRN(),
  VS_CRC_PF.SQL_NO_SCL(),
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet, ',
  'IF(vmi.AreaM2 <> 0, vmi.ValorT/vmi.AreaM2, 0.0000) CustoM2, ',
  'IF(vmi.PesoKg <> 0, vmi.ValorT/vmi.PesoKg, 0.0000) CustoKg ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  VS_CRC_PF.SQL_LJ_FRN(),
  VS_CRC_PF.SQL_LJ_SCL(),
  'LEFT JOIN vspalleta  vsp  ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf  ON vsf.Codigo=vmi.SerieFch ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE (vmi.MovimCod=' + Geral.FF0(MovimCod),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  ') ' + SQL_IndsVS,
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet, Controle ',
  '']);
  //
  //Geral.MB_SQL(nil, Qry);
  if Controle <> 0 then
    Qry.Locate('Controle', Controle, []);
end;

procedure TUnVS_CRC_PF.ReopenVSPallet(QrVSPallet: TmySQLQuery; Empresa,
  ClientMO, GraGruX: Integer; Ordem: String; PallOnEdit: array of Integer);
var
  SQL_GraGruX, ORDERBY, SQL_Pallets: String;
begin
  if Length(PallOnEdit) > 0 then
  begin
    SQL_Pallets := 'OR (let.Codigo=' + MyObjects.CordaDeArrayInt(PallOnEdit) + ')';
  end else
    SQL_Pallets := '';
  //
  if GraGruX <> 0 then
    SQL_GraGruX := 'AND let.GraGruX=' + Geral.FF0(GraGruX)
  else
    SQL_GraGruX := '';
  //
  if Ordem = '' then
    ORDERBY := 'ORDER BY Codigo, NO_PRD_TAM_COR '
  else
    ORDERBY := Ordem;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
  'SELECT let.*,  ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,',
  'CONCAT(let.Codigo, " - ", gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL,"", CONCAT(" ", gcc.Nome)), ',
  'IF(let.Nome<>"", CONCAT(" (", let.Nome, ")"), "")',
  ') NO_PRD_TAM_COR, vps.Nome NO_STATUS   ',
  'FROM vspalleta let  ',
  'LEFT JOIN entidades  emp ON emp.Codigo=let.Empresa  ',
  'LEFT JOIN entidades  cli ON cli.Codigo=let.CliStat  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=let.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status',
  'WHERE (let.DtHrEndAdd < "1900-01-01"',
  'AND let.Empresa=' + Geral.FF0(Empresa),
  'AND let.ClientMO=' + Geral.FF0(ClientMO),
  SQL_GraGruX,
  ')',
  SQL_Pallets,
  ORDERBY,
  '']);
  //Geral.MB_SQL(nil, QrVSPallet);
end;

procedure TUnVS_CRC_PF.ReopenVSPWEDesclDst(Qry: TmySQLQuery; Codigo, Controle,
  Pallet, TemIMEIMrt: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  //TemIMEIMrt := QrVSPWECabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(emidDesclasse)),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
  'AND vmi.DstMovID=' + Geral.FF0(Integer(emidFinished)),
  'AND vmi.DstNivel1=' + Geral.FF0(Codigo),//QrVSPWECabCodigo.Value),
  'AND vmi.DstNivel2=' + Geral.FF0(Controle), //QrVSPWEAtuControle.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry(*QrVSPWEDesclDst*), Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
end;

procedure TUnVS_CRC_PF.ReopenVSXxxExtra(Qry: TmySQLQuery; Codigo,
  TemIMEIMrt: Integer; MovimID, DstMovID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
begin
  //TemIMEIMrt := QrVSPWECab.FieldByName('TemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(MovimID)),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),
  'AND vmi.DstMovID=' + Geral.FF0(Integer(DstMovID)), //emidFinished)),
  'AND vmi.DstNivel1=' + Geral.FF0(Codigo),
  //'AND vmi.DstNivel2=' + Geral.FF0(Controle),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
end;

procedure TUnVS_CRC_PF.ReopenVSXxxOris(QrVSXxxCab, QrVSXxxOriIMEI,
  QrVSXxxOriPallet: TmySQLQuery; Controle, Pallet: Integer;
  MovimNiv: TEstqMovimNiv);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSXxxCab.FieldByName('TemIMEIMrt').AsInteger;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSXxxCab.FieldByName('MovimCod').AsInteger),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),  // eminSorc...
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSXxxOriIMEI, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Pallet, Controle ',
  '']);
  //
  QrVSXxxOriIMEI.Locate('Controle', Controle, []);
  //
  //
  SQL_Flds := Geral.ATS([
  '']);
  SQL_Left := Geral.ATS([
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSXxxCab.FieldByName('MovimCod').AsInteger),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv)),  // eminSorc...
  '']);
  SQL_Group := 'GROUP BY Pallet ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSXxxOriPallet, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet ',
  '']);
  QrVSXxxOriPallet.Locate('Pallet', Pallet, []);
end;

function TUnVS_CRC_PF.SenhaVSPwdDdNaoConfere(Digitado: String): Boolean;
var
  Qry: TmySQLQuery;
  VSPwdDdClas: String;
  VSPwdDdData: TDateTime;
  VSPwdDdUser: Integer;
  LiberaSemSenha: Boolean;
begin
  Result := False;
  LiberaSemSenha := VAR_USUARIO < 1;
  if not LiberaSemSenha then
    LiberaSemSenha := Digitado = CO_MASTER;
  //
  if LiberaSemSenha then
    Exit;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT VSPwdDdClas, VSPwdDdData, VSPwdDdUser ',
    'FROM controle ',
    'WHERE Codigo=1 ',
    '']);
    VSPwdDdClas := Qry.FieldByName('VSPwdDdClas').AsString;
    VSPwdDdData := Qry.FieldByName('VSPwdDdData').AsDateTime;
    VSPwdDdUser := Qry.FieldByName('VSPwdDdUser').AsInteger;
    if (VSPwdDdUser <> 0) then
      Result := VSPwdDdClas <> Digitado;
    if Result then
      Geral.MB_Aviso('Senha VS do dia não confere!');
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.SetaGGXUnicoEmLista(const GraGruX: Integer;
  var Lista: TPallArr);
begin
  if GraGruX <> 0 then
  begin
    SetLength(Lista, 1);
    Lista[0] := GraGruX;
  end else
    SetLength(Lista, 0);
end;

procedure TUnVS_CRC_PF.SetIDTipoCouro(RG: TRadioGroup; Colunas,
  Default: Integer);
var
  I: Integer;
begin
  RG.Items.Clear;
  for I := 0 to MaxGraGruY_VS do
    RG.Items.Add(sListaGraGruY_VS[I]);
  //
  RG.Columns := Colunas;
  RG.ItemIndex := Default;
end;

{
function TUnVS_CRC_PF.SQL_AW_Filto(QuaisItensQuer: TQuaisItensQuer; Alias1: String): String;
var
  Filtro: String;
begin
  case QuaisItensQuer of
    //qiqIndef=0,
    (*1*)qiqNenhum:    Como Fazer?
    (*2*)qiqInsAlt:      Filtro := '<>';
    (*3*)qiqNaoAltter:   Filtro := '=';
    (*4*)qiqTodos        Filtro := '';
  Filtro :=
  Result := Geral.ATS([
    'WHERE ' + Alias1 + 'AWServerID' + Filtro + VAR_TXT_AWServerID,
    'AND ' + Alias1 + 'AWStatSinc NOT IN (' +
    Geral.FF0(Integer(stDwnSinc)) + ',' +
    Geral.FF0(Integer(stUpSinc)) + ')']); //stDwnSync e stUpSinc


            SQL_Filtro := ''
          else
            SQL_Filtro := Geral.ATS([
/
            'WHERE AWServerID=' + VAR_TXT_AWServerID,
            'AND AWStatSinc NOT IN (' +
            Geral.FF0(Integer(stDwnSinc)) + ',' +
            Geral.FF0(Integer(stUpSinc)) + ')']); //stDwnSync e stUpSinc
        end;
        stDwnSinc:
        begin
          if EnviarTudo then
            SQL_Filtro := '**ERRO EnviarTudo** '
          else
            SQL_Filtro := Geral.ATS([
/
            'WHERE AWServerID <> 0 ',
            'AND AWStatSinc NOT IN (' +
            Geral.FF0(Integer(stDwnSinc)) + ',' +
            Geral.FF0(Integer(stUpSinc)) + ')']); //stDwnSync e stUpSinc
        end;

end;
}

function TUnVS_CRC_PF.SQL_LJ_CMO: String;
begin
  Result := Geral.ATS([
  'LEFT JOIN entidades   cmo ON cmo.Codigo=vmi.ClientMO ']);
end;

function TUnVS_CRC_PF.SQL_LJ_FRN: String;
begin
  Result := Geral.ATS([
  'LEFT JOIN entidades   frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN vsmulfrncab mfc ON mfc.Codigo=vmi.VSMulFrnCab ']);
end;

function TUnVS_CRC_PF.SQL_LJ_GGX: String;
begin
  Result := Geral.ATS([
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ']);
end;

function TUnVS_CRC_PF.SQL_LJ_GGX_DST(Alias: String): String;
begin
  Result := Geral.ATS([
  'LEFT JOIN gragrux    ggx_dst ON ggx_dst.Controle=' + Alias + '.GGXDst ',
  'LEFT JOIN gragruc    ggc_dst ON ggc_dst.Controle=ggx_dst.GraGruC ',
  'LEFT JOIN gracorcad  gcc_dst ON gcc_dst.Codigo=ggc_dst.GraCorCad ',
  'LEFT JOIN gratamits  gti_dst ON gti_dst.Controle=ggx_dst.GraTamI ',
  'LEFT JOIN gragru1    gg1_dst ON gg1_dst.Nivel1=ggx_dst.GraGru1 ']);
end;

function TUnVS_CRC_PF.SQL_LJ_SCL: String;
begin
  Result := Geral.ATS([
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ']);
end;

function TUnVS_CRC_PF.SQL_MovIDeNiv_Pos_Inn: String;
begin
  AdicionarNovosVS_emid();
  Result := Geral.ATS([
    '   (MovimID =  6 AND MovimNiv = 13)',
    'OR (MovimID =  7 AND MovimNiv =  2)',
    'OR (MovimID =  8 AND MovimNiv =  2)',
    'OR (MovimID = 11 AND MovimNiv =  9)',
    'OR (MovimID = 13 AND MovimNiv =  0)',
    'OR (MovimID = 14 AND MovimNiv =  2)',
    //'OR (MovimID = 15 AND MovimNiv = 12)',
    'OR (MovimID = 16 AND MovimNiv =  0)',
    'OR (MovimID = 20 AND MovimNiv = 22)',
    'OR (MovimID = 21 AND MovimNiv =  0)',
    'OR (MovimID = 22 AND MovimNiv =  0)',
    'OR (MovimID = 23 AND MovimNiv =  0)',
    'OR (MovimID = 24 AND MovimNiv =  2)',
    'OR (MovimID = 25 AND MovimNiv = 28)',
    'OR (MovimID = 26 AND MovimNiv = 30)',
    'OR (MovimID = 27 AND MovimNiv = 35)',
    'OR (MovimID = 28 AND MovimNiv =  2)',
    // 29 >> 2017-11-07
    'OR (MovimID = 29 AND MovimNiv = 31)',
    // 30, 31 ???
    'OR (MovimID = 32 AND MovimNiv = 51)',
    'OR (MovimID = 33 AND MovimNiv = 56)',
    'OR (MovimID = 36 AND MovimNiv =  0)',
    'OR (MovimID = 39 AND MovimNiv = 66)'
  ]);
end;

function TUnVS_CRC_PF.SQL_NO_CMO: String;
begin
  Result := Geral.ATS([
  'IF(vmi.ClientMO <> 0, ',
  '  IF(cmo.Tipo=0, cmo.RazaoSocial, cmo.Nome), ',
  '  "") NO_ClientMO, ']);
end;

function TUnVS_CRC_PF.SQL_NO_FRN: String;
begin
  Result := Geral.ATS([
  'IF(vmi.Terceiro <> 0, ',
  '  IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome), ',
  '  mfc.Nome) NO_FORNECE, ']);
end;

function TUnVS_CRC_PF.SQL_NO_GGX: String;
begin
  Result := Geral.ATS(['CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ']);
end;

function TUnVS_CRC_PF.SQL_NO_GGX_DST(): String;
begin
  Result := Geral.ATS(['CONCAT(gg1_dst.Nome, ',
  'IF(gti_dst.PrintTam=0 OR gti_dst.Codigo IS NULL, "", CONCAT(" ", gti_dst.Nome)), ',
  'IF(gcc_dst.PrintCor=0 OR gcc_dst.Codigo IS NULL,"", CONCAT(" ", gcc_dst.Nome))) ',
  'NO_PRD_TAM_COR_DST, ']);
end;

function TUnVS_CRC_PF.SQL_NO_SCL: String;
begin
  Result := 'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, ';
end;

function TUnVS_CRC_PF.SQL_TipoEstq_DefinirCodi(CAST: Boolean; NomeFld: String;
  UsaVirgula: Boolean; FldQtdArM2, FldQtdPeso, FldQtdPeca,
  Prefacio: String): String;
var
  Cast_Pre, Cast_Pos, Virgula: String;
begin
  if UsaVirgula then
    Virgula := ', '
  else
    Virgula := '';
  //
  if CAST then
  begin
    Cast_Pre := ' CAST(';
    Cast_Pos := ' AS DECIMAL(15,0) ) ';
    //Cast_Pos := ' AS UNSIGNED) ';
  end else
  begin
    Cast_Pre := '';
    Cast_Pos := '';
  end;
  Result := Geral.ATS([
  Cast_Pre + 'IF(' + Prefacio + 'gg1.UnidMed <> 0, ' + Prefacio + 'med.Grandeza, ',
  '  IF(' + Prefacio + 'xco.Grandeza > 0, ' + Prefacio + 'xco.Grandeza, ',
  '  IF((' + Prefacio + 'ggx.GraGruY<2048 OR ',
  '  ' + Prefacio + 'xco.CouNiv2<>1) AND ' + FldQtdPeso + ' <> 0, 2, ',
  '  IF(' + Prefacio + 'xco.CouNiv2=1 AND ' + FldQtdArM2 + ' <> 0, 1, ',
  //  Erro!!! 999
  //'  IF(' + FldQtdPeca + '=0, 5, CAST(0 - 1 AS UNSIGNED) ))))) ' + Cast_Pos + NomeFld + Virgula,
  '  IF(' + FldQtdPeca + '=0, 5, 9 ))))) ' + Cast_Pos + NomeFld + Virgula,
  '']);
end;

function TUnVS_CRC_PF.TabCacVS_Tab(Tab: TTabToWork): String;
begin
  case Tab of
    TTabToWork.ttwA: Result := 'vscacitsa';
    TTabToWork.ttwB: Result := 'vscacitsb';
    else             Result := 'vscacits?'
  end;
end;

function TUnVS_CRC_PF.TabMovVS_Fld_IMEI(Tab: TTabToWork): String;
begin
  case Tab of
    TTabToWork.ttwA: Result := ' "Ativo" NO_TTW, CAST(0 AS UNSIGNED) ID_TTW, ';
    TTabToWork.ttwB: Result := ' "Morto" NO_TTW, CAST(1 AS UNSIGNED) ID_TTW, ';
    TTabToWork.ttwC: Result := ' "Ambos" NO_TTW, CAST(2 AS UNSIGNED) ID_TTW, ';
    else             Result := ' "?????" NO_TTW, CAST(9 AS UNSIGNED) ID_TTW, ';
  end;
end;

function TUnVS_CRC_PF.TabMovVS_Fld_Pall(Tab: TTabToWork): String;
begin
  case Tab of
    TTabToWork.ttwA: Result := ' "Ativo" NO_TTW, CAST(0 AS UNSIGNED) ID_TTW, ';
    TTabToWork.ttwB: Result := ' "Morto" NO_TTW, CAST(1 AS UNSIGNED) ID_TTW, ';
    TTabToWork.ttwC: Result := ' "Ambos" NO_TTW, CAST(2 AS UNSIGNED) ID_TTW, ';
    else             Result := ' "?????" NO_TTW, CAST(9 AS UNSIGNED) ID_TTW, ';
  end;
end;

function TUnVS_CRC_PF.TabMovVS_Tab(Tab: TTabToWork): String;
begin
  case Tab of
    TTabToWork.ttwA: Result := CO_TAB_TAB_VMI;
    TTabToWork.ttwB: Result := CO_TAB_VMB;
    else             Result := 'vsmovit?'
  end;
end;

function TUnVS_CRC_PF.ValidaCampoNF(Operacao: Integer; EditNF: TdmkEdit;
  MostraMsg: Boolean): Boolean;
var
  NObrigNFeVS, Opera: Integer;
begin
  Result := False;
  //
  UnDmkDAC_PF.AbreQuery(Dmod.QrControle, Dmod.MyDB);
  //
  NObrigNFeVS := VARF_NObrigNFeVS; // Dmod.QrControleNObrigNFeVS.Value;
  Opera       := Trunc(Power(2, Operacao));
  //
  if not Geral.IntInConjunto(Opera, NObrigNFeVS) then
  begin
    if EditNF.ValueVariant = 0 then
    begin
      if MostraMsg then
      begin
        Geral.MB_Aviso('Informe a nota fiscal!');
        EditNF.SetFocus;
      end;
    end else
      Result := True;
  end else
    Result := True;
end;

function TUnVS_CRC_PF.VerificaOpeSemOperacao(): Boolean;
var
  Qry: TmySQLQuery;
  CouNiv2: Integer;
  Txt: String;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * ',
    'FROM vsopecab ',
    'WHERE Operacoes < 1 ',
    '']);
    Result := Qry.RecordCount > 0;
    if Result then
    begin
      Txt := '';
      Qry.First;
      while not Qry.Eof do
      begin
        Txt := Txt + Geral.FF0(Qry.FieldByName('Codigo').AsInteger) + ', ';
        //
        Qry.Next;
      end;
      Geral.MB_Aviso('Existem ' + Geral.FF0(Qry.RecordCount) +
        ' operações sem definição do código de cadastro de operação!' +
        sLineBreak + Txt + sLineBreak + sLineBreak +
        'Vá em "Janela Principal" -> Guia "Ribeira 2" -> Botão "Geração Classificado (Operações)"');
    end;
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.VSFic(GraGruX, Empresa, ClienteMO, Fornecedor, Pallet,
  Ficha: Integer; Pecas, AreaM2, PesoKg, ValorT: Double; EdGraGruX, EdPallet,
  EdFicha, EdPecas, EdAreaM2, EdPesoKg, EdValorT: TWinControl;
  ExigeFornecedor: Boolean; GraGruY: Integer; ExigeAreaouPeca: Boolean;
  EdStqCenLoc: TWinControl): Boolean;
var
  Qry: TmySQLQuery;
  CouNiv2: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT CouNiv2 ',
    'FROM gragruxcou ',
    'WHERE GraGruX=' + Geral.FF0(GraGruX),
    '']);
    CouNiv2 := Qry.FieldByName('CouNiv2').AsInteger;
    //
    Result := True;
    if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe uma matéria-prima!') then
      Exit;
    if MyObjects.FIC(Empresa = 0, nil, 'Empresa inválida!') then
      Exit;
    if ExigeFornecedor then
      if MyObjects.FIC(Fornecedor = 0, nil, 'Fornecedor inválido!') then
        Exit;
    if EdPallet <> nil then
      if MyObjects.FIC(Pallet = 0, EdPallet, 'O pallet é obrigatório!') then
        Exit;
    if EdFicha <> nil then
      if MyObjects.FIC(Ficha = 0, EdFicha, 'A Ficha RMP é obrigatória!') then
        Exit;
    if EdPecas <> nil then
    begin
      if CouNiv2 = CO_COU_NIV2_001_COURO then
        if MyObjects.FIC(Pecas = 0, EdPecas, 'A quantidade de peças é obrigatória!') then
          Exit;
    end;
    if EdStqCenLoc <> nil then
      if MyObjects.FIC(TDmkEditCB(EdStqCenLoc).ValueVariant = 0, EdStqCenLoc,
      'O local/centro de estoque é obrigatório!') then
        Exit;
    if GraGruY = CO_GraGruY_4096_VSRibOpe then
    begin
      if MyObjects.FIC((AreaM2 = 0) and (PesoKg = 0) and ExigeAreaouPeca, nil,
      'A área ou o peso é obrigatório!') then
          Exit;
    end else
    begin
      if (EdAreaM2 <> nil) and (GraGruY >= CO_GraGruY_2048_VSRibCad) then
      begin
        if CouNiv2 <> CO_COU_NIV2_002_RASPA then
          if MyObjects.FIC(AreaM2 = 0, EdAreaM2, 'A área é obrigatória!') then
            Exit;
      end;
      if (EdPesoKg <> nil) then
      begin
        if (CouNiv2 = CO_COU_NIV2_002_RASPA)
        or (CouNiv2 = CO_COU_NIV2_003_SUBPR) then
        begin
          if (GraGruY = CO_GraGruY_1024_VSNatCad)
          or (GraGruY = CO_GraGruY_1072_VSNatInC)
          or (GraGruY = CO_GraGruY_1088_VSNatCon)
          or (GraGruY = CO_GraGruY_0512_VSSubPrd)
          or (GraGruY = CO_GraGruY_0683_VSPSPPro)
          or (GraGruY = CO_GraGruY_0853_VSPSPEnd) then
            if MyObjects.FIC(PesoKg = 0, EdPesoKg, 'O peso é obrigatório!') then
              Exit;
        end;
      end;
    end;
    if EdValorT <> nil then
    begin
      if CouNiv2 = CO_COU_NIV2_001_COURO then
      begin
        if (ClienteMO <> 0) and (Empresa <> ClienteMO) then
        begin
          ;
        end else
        begin
          if MyObjects.FIC(ValorT = 0, EdValorT, 'O valor total é obrigatório!') then
            Exit;
        end;
      end;
    end;
    //

    Result := False;
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.ZeraEstoquePalletOrigemReclas(Pallet, Codigo, MovimCod,
  Empresa: Integer; BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2,
  BxaValorT: Double; DataEHora: TDateTime; iuvpei: TInsUpdVMIPrcExecID): Boolean;
  //
  procedure BaixaPecasParciais(InsPecas, InsAreaM2, InsAreaP2: Double);
  const
    LnkNivXtr1 = 0;
    LnkNivXtr2 = 0;
    CliVenda   = 0;
    MovimTwn   = 0;
    DstMovID   = TEstqMovimID(0);
    DstNivel1  = 0;
    DstNivel2  = 0;
    DstGGX     = 0;
    CustoMOKg  = 0;
    CustoMOM2  = 0;
    CustoMOTot = 0;
    ValorMP    = 0;
    QtdGerPeca = 0;
    QtdGerPeso = 0;
    QtdGerArM2 = 0;
    QtdGerArP2 = 0;
    AptoUso    = 0;
    NotaMPAG   = 0;
    //
    TpCalcAuto = -1;
    //
    EdFicha  = nil;
    EdPallet = nil;
    ExigeFornecedor = False;
    //
    PedItsLib   = 0;
    PedItsFin   = 0;
    PedItsVda   = 0;
    //
    GSPSrcMovID  = TEstqMovimID(0);
    GSPSrcNiv2  = 0;
    //
    ReqMovEstq  = 0;
    //
    ItemNFe     = 0;
    //
    QtdAntPeca = 0;
    QtdAntPeso = 0;
    QtdAntArM2 = 0;
    QtdAntArP2 = 0;
  var
    DataHora, Observ, Marca: String;
    Controle, GraGruX, Terceiro, SerieFch, Ficha, SrcNivel1, SrcNivel2,
    GraGruY, SrcGGX, VSMulFrnCab, ClientMO, FornecMO, StqcenLoc: Integer;
    SrcMovID, MovimID: TEstqMovimID;
    MovimNiv: TEstqMovimNiv;
    //
    Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  begin
    SrcMovID       := TEstqMovimID(Dmod.QrVSMovItsMovimID.Value);
    SrcNivel1      := Dmod.QrVSMovItsCodigo.Value;
    SrcNivel2      := Dmod.QrVSMovItsControle.Value;
    SrcGGX         := Dmod.QrVSMovItsGraGruX.Value;
    //
    //Codigo         := ;
    Controle       := 0;
    //MovimCod       := ;
    //Empresa        := ;
    ClientMO       := Dmod.QrVSMovItsClientMO.Value;
    FornecMO       := Dmod.QrVSMovItsFornecMO.Value;
    StqCenLoc      := Dmod.QrVSMovItsStqCenLoc.Value;
    Terceiro       := Dmod.QrVSMovItsTerceiro.Value;
    VSMulFrnCab    := Dmod.QrVSMovItsVSMulFrnCab.Value;
    DataHora       := Geral.FDT(DataEHora, 109);
    MovimID        := emidResiduoReclas;
    MovimNiv       := eminSemNiv;
    //Pallet         := QrVSMovItsPallet.Value;
    GraGruX        := Dmod.QrVSMovItsGraGruX.Value;
    Pecas          := InsPecas;
    PesoKg         := 0;
    AreaM2         := InsAreaM2;
    AreaP2         := InsAreaP2;
    // ini 2023-04-04
    ValorT         := 0;
    if InsAreaM2 <> 0 then
      ValorT := ValorT + (Dmod.QrVSMovItsValorT.Value / Dmod.QrVSMovItsAreaM2.Value * InsAreaM2)
    else
    if PesoKg <> 0 then
      ValorT := ValorT + (Dmod.QrVSMovItsValorT.Value / Dmod.QrVSMovItsPesoKg.Value * PesoKg)
    else
    if InsPecas <> 0 then
      ValorT := ValorT + (Dmod.QrVSMovItsValorT.Value / Dmod.QrVSMovItsPecas.Value * InsPecas);
    // fim 2023-04-04
    //
    Observ         := '';
    //
    SerieFch       := Dmod.QrVSMovItsSerieFch.Value;
    Ficha          := Dmod.QrVSMovItsFicha.Value;
    Marca          := Dmod.QrVSMovItsMarca.Value;
    //Misturou       := QrVSMovItsMisturou.Value;
    //
    GraGruY        := Dmod.QrVSMovItsGraGruY.Value;
    //
  (*
    if VS_CRC_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas,
    AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas,
    EdAreaM2, EdPesoKg, EdValorT, ExigeFornecedor, GraGruY) then
      Exit;
  *)
    //
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, stIns, Controle);
  (*
    if Dmod.InsUpdVSMovIts_(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Terceiro,
    MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
    DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
    CliVenda, Controle) then
  *)
    Result := InsUpdVSMovIts3(stIns, Codigo, MovimCod, MovimTwn,
    Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
    AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
    Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
    CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca,
    QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
    NotaMPAG, SrcGGX, DstGGX, Marca,
    TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
    ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
    QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
    CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
    CO_0_GGXRcl,
    CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
    CO_0_JmpMovID,
    CO_0_JmpNivel1,
    CO_0_JmpNivel2,
    CO_0_JmpGGX,
    CO_0_RmsMovID,
    CO_0_RmsNivel1,
    CO_0_RmsNivel2,
    CO_0_RmsGGX,
    CO_0_GSPJmpMovID,
    CO_0_GSPJmpNiv2,
    CO_0_MovCodPai,
    CO_0_VmiPai,
    CO_0_IxxMovIX,
    CO_0_IxxFolha,
    CO_0_IxxLinha,
    CO_TRUE_ExigeClientMO,
    CO_TRUE_ExigeFornecMO,
    CO_TRUE_ExigeStqLoc,
    iuvpei);
    if Result then
    begin
      //VS_CRC_PF.AtualizaTotaisVSXxxCab(CO_VSXxxTab, MovimCod);  Nao tem! Usa Reclass
      VS_CRC_PF.AtualizaSaldoIMEI(Dmod.QrVSMovItsControle.Value, False);
    end;
  end;
var
  InsPecas, InsAreaM2, InsAreaP2: Double;
  RestoPecas, RestoAreaM2: Double;
begin
////////////////////////////////////////////////////////////////////////////////
  // Cuidado!! Aqui zera apenas quantidades que realmente existirao apos a
  // confirmacao de pallets ainda em montagem na reclassificacao!!
  //
  // Portanto na teoria o pallet ainda tem mais couros do que vai ser baixado aqui!
  //
////////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrVSMovIts, Dmod.MyDB, [
  'SELECT ggx.GraGruY, vmi.*',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi   ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'WHERE vmi.Pallet=' + Geral.FF0(Pallet),
  'AND vmi.SdoVrtPeca > 0 ',
  'ORDER BY vmi.Controle DESC ',
  '']);
  RestoPecas  := BxaPecas;
  RestoAreaM2 := BxaAreaM2;
  //
  Dmod.QrVSMovIts.First;
  //
  while RestoPecas > 0 do
  begin
    if RestoPecas <= Dmod.QrVSMovItsSdoVrtPeca.Value then
    begin
      InsPecas   := RestoPecas;
      InsAreaM2  := RestoAreaM2;
      //
      RestoPecas  := 0;
      RestoAreaM2 := 0;
    end else
    begin
      InsPecas   := Dmod.QrVSMovItsSdoVrtPeca.Value;
      InsAreaM2  := Dmod.QrVSMovItsSdoVrtArM2.Value;
      //
      RestoPecas  := RestoPecas - InsPecas;
      RestoAreaM2 := RestoAreaM2 - InsAreaM2;
    end;
    // Evitar Loop infinito
    if InsPecas <= 0 then
    begin
      Geral.MB_Erro('Peças residuais em estoque a baixar < 0 !!!' + sLineBreak +
      'Avise a DERMATEK!!!!');
      InsPecas   := RestoPecas;
      InsAreaM2  := RestoAreaM2;
      //
      RestoPecas  := 0;
      RestoAreaM2 := 0;
    end;
    InsAreaP2  := Geral.ConverteArea(InsAreaM2, ctM2toP2, cfQuarto);
    //
    BaixaPecasParciais(-InsPecas, -InsAreaM2, -InsAreaP2);
    //
    Dmod.QrVSMovIts.Next;
  end;
end;

procedure TUnVS_CRC_PF.ZeraSaldoIMEI(Controle: Integer);
const
  SdoVrtPeca = 0;
  SdoVrtPeso = 0;
  SdoVrtArM2 = 0;
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  'SdoVrtPeca', 'SdoVrtPeso', 'SdoVrtArM2'], [
  'Controle'], [
  SdoVrtPeca, SdoVrtPeso, SdoVrtArM2], [
  Controle], True);
end;

procedure TUnVS_CRC_PF.InfoPalletVMI(VMI, Pallet: Integer; Qry: TmySQLQuery);
begin
  InfoRegIntInVMI('Pallet', VMI, Pallet, Qry);
end;

procedure TUnVS_CRC_PF.InfoRegIntInVMI(Campo: String; VMI, Inteiro: Integer;
  Qry: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmVSReqMovEstq, FmVSReqMovEstq, afmoLiberado) then
  begin
    FmVSReqMovEstq.ImgTipo.SQLType := stUpd;
    FmVSReqMovEstq.FCampo := Campo;
    FmVSReqMovEstq.EdControle.ValueVariant := VMI;
    FmVSReqMovEstq.EdInteiro.ValueVariant  := Inteiro;
    FmVSReqMovEstq.FQry := Qry;
    //
    FmVSReqMovEstq.ShowModal;
    FmVSReqMovEstq.Destroy;
  end;
end;

procedure TUnVS_CRC_PF.InfoReqMovEstq(VMI, ReqMovEstq: Integer;
  Qry: TmySQLQuery);
begin
  InfoRegIntInVMI('ReqMovEstq', VMI, ReqMovEstq, Qry);
end;

function TUnVS_CRC_PF.InformaIxx(DBGIMEI: TDBGrid; QrIMEI: TmySQLQuery;
  FldIMEI: String): Boolean;
var
  IxxMovIX, IxxFolha, IxxLinha, IMEI: Integer;
////
  procedure ReopenIMEI();
  begin
    if QrIMEI <> nil then
    begin
      UnDmkDAC_PF.AbreQuery(QrIMEI, Dmod.MyDB);
      QrIMEI.Locate('Controle', IMEI, []);
    end;
  end;
  procedure AtualizaAtual();
  begin
    IMEI := QrIMEI.FieldByName(FldIMEI).AsInteger;
    //
    if DBCheck.CriaFm(TFmVSIxx, FmVSIxx, afmoLiberado) then
    begin
      FmVSIxx.ImgTipo.SQLType := stUpd;
      FmVSIxx.FResult := False;
      FmVSIxx.FQrIMEI := QrIMEI;
      FmVSIxx.FIMEI := IMEI;
      FmVSIxx.PreparaEdicao(IxxMovIX, IxxFolha, IxxLinha);
      //
      FmVSIxx.ShowModal;
      Result   := FmVSIxx.FResult;
      IxxMovIX := FmVSIxx.RGIxxMovIX.ItemIndex;
      IxxFolha := FmVSIxx.EdIxxFolha.ValueVariant;
      IxxLinha := FmVSIxx.EdIxxLinha.ValueVariant;
      //
      FmVSIxx.Destroy;
    end;
  end;
  var
    q: TSelType;
    I: Integer;
begin
  IxxMovIX := -1;
  IxxFolha := 0;
  IxxLinha := 0;
  //
  DBCheck.Quais_Selecionou(QrIMEI, TDBGrid(DBGIMEI), q);
  case q of
    istAtual:
    begin
      AtualizaAtual;
      ReopenIMEI();
    end;
    istSelecionados:
    begin
      with DBGIMEI.DataSource.DataSet do
      for I := 0 to DBGIMEI.SelectedRows.Count-1 do
      begin
        //GotoBookmark(pointer(DBGIMEI.SelectedRows.Items[I]));
        GotoBookmark(DBGIMEI.SelectedRows.Items[I]);
        AtualizaAtual();
      end;
      ReopenIMEI();
    end;
    istTodos:
    begin
      QrIMEI.First;
      while not QrIMEI.Eof do
      begin
        AtualizaAtual();
        //
        QrIMEI.Next;
      end;
      ReopenIMEI();
    end;
  end;
end;

procedure TUnVS_CRC_PF.InfoStqCenLoc(VMI, StqCenLoc: Integer; Qry: TmySQLQuery);
begin
  AlteraVMI_StqCenLoc(VMI, StqCenLoc);
  if Qry <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(Qry, Qry.Database);
    Qry.Locate('Controle', VMI, []);
  end;
end;

function TUnVS_CRC_PF.InsAltVSPalCla(const Empresa, ClientMO, Fornecedor,
  VSMulFrnCab: Integer; const MovimID: TEstqMovimID; const Codigo, MovimCod,
  BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
  Tecla, VSPallet, GragruX, StqCenLoc: Integer; const IuvpeiInn, IuvpeiBxa:
  TInsUpdVMIPrcExecID; const LaAviso1, LaAviso2: TLabel; var CtrlSorc,
  CtrlDest: Integer): Integer;
var
  FornecMO: Integer;
  //
  function CriaVSMovIts(): Integer;
  const
    QtdGerPeca = 0;
    QtdGerPeso = 0;
    LnkNivXtr1 = 0;
    LnkNivXtr2 = 0;
    CliVenda   = 0;
    AreaM2     = 0;
    AreaP2     = 0;
    CustoMOKg  = 0;
    CustoMOM2  = 0;
    CustoMOTot = 0;
    ValorT     = 0;
    //Misturou   = 0;
    AptoUso    = 0;
    //FornecMO   = 0;
    Pecas      = 0;
    PesoKg     = 0;
    Observ     = ''; // Como fazer!!!
    SerieFch   = 0;  // Fazer depois!!!
    Ficha      = 0;  // Fazer depois!!!
    Marca      = ''; // Fazer depois!!!
    ValorMP    = 0;  // Fazer depois!!!
    QtdGerArM2 = 0;  // Fazer depois!!!
    QtdGerArP2 = 0;  // Fazer depois!!!
    NotaMPAG   = 0;
    //
    ExigeFornecedor = True;
    //
    TpCalcAuto = -1;
    //
    EdPallet = nil;
    EdValorT = nil;
    EdAreaM2 = nil;
    //
    PedItsLib  = 0;
    PedItsFin  = 0;
    PedItsVda  = 0;
    //
    GSPSrcMovID = TEstqMovimID(0);
    GSPSrcNiv2 = 0;
    //
    ReqMovEstq = 0;
    //StqCenLoc  = 0;
    //
    ItemNFe    = 0;
    //
    QtdAntPeca = 0;
    QtdAntPeso = 0;
    QtdAntArM2 = 0;
    QtdAntArP2 = 0;
  var
    Controle, SrcNivel1, SrcNivel2, SrcGGX, DstNivel1, DstNivel2, MovimTwn,
    DstGGX, Pallet: Integer;
    MovimNiv: TEstqMovimNiv;
    DataHora: String;
    SrcMovID, DstMovID: TEstqMovimID;
  begin
    Result := 0;
    MovimTwn := 0;
    DataHora := Geral.FDT(DModG.ObtemAgora(), 109);
    //
    // Classe a classificar
    MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, stIns, MovimTwn);
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, stIns, 0);
(*  Errado!!!! 2015-03-30
    //SrcMovimNiv := TEstqMovimNiv(BxaMovimNiv);
    SrcNivel1    := 0; //BxaSrcNivel1;
    SrcNivel2    := 0; //BxaSrcNivel2;
    SrcGGX       := 0; //BxaGraGruX;
*)
    SrcMovID     := TEstqMovimID(BxaMovimID);
    SrcNivel1    := BxaSrcNivel1;
    SrcNivel2    := BxaSrcNivel2;
    SrcGGX       := BxaGraGruX;
    //
    Pallet       := 0;
    //
    DstMovID     := MovimID;
    DstNivel1    := Codigo;
    DstNivel2    := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, stIns, 0);
    DstGGX       := GraGruX;
    //
    MovimNiv := eminSorcClass;
    //
    if InsUpdVSMovIts3(stIns, Codigo, MovimCod, MovimTwn, Empresa,
    Fornecedor, MovimID, MovimNiv, Pallet, BxaGraGruX, Pecas, PesoKg, AreaM2,
    AreaP2, ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ,
    LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2,
    CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso,
    QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
    NotaMPAG, SrcGGX, DstGGX, Marca,
    TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
    ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
    QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
    CO_0_GGXRcl,
    CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
    CO_0_JmpMovID,
    CO_0_JmpNivel1,
    CO_0_JmpNivel2,
    CO_0_JmpGGX,
    CO_0_RmsMovID,
    CO_0_RmsNivel1,
    CO_0_RmsNivel2,
    CO_0_RmsGGX,
    CO_0_GSPJmpMovID,
    CO_0_GSPJmpNiv2,
    CO_0_MovCodPai,
    CO_0_VmiPai,
    CO_0_IxxMovIX,
    CO_0_IxxFolha,
    CO_0_IxxLinha,
    CO_TRUE_ExigeClientMO,
    CO_TRUE_ExigeFornecMO,
    CO_TRUE_ExigeStqLoc,
    IuvpeiBxa(*Baixa de artigo de ribeira em classificação x/4*)) then
    begin
      // Nova classe
      SrcMovID  := MovimID;
      SrcNivel1 := Codigo;
      SrcNivel2 := Controle;
      SrcGGX    := GraGruX;
      //
      Pallet    := VSPallet;
      //
      Controle  := DstNivel2;
      //
      DstMovID     := TEstqMovimID(0);
      DstNivel1    := 0;
      DstNivel2    := 0;
      DstGGX       := 0;
      //
      MovimNiv     := eminDestClass;
      //
      if InsUpdVSMovIts3(stIns, Codigo, MovimCod, MovimTwn, Empresa,
      Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
      AreaP2, ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ,
      LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg,
      CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso,
      QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
      NotaMPAG, SrcGGX, SrcGGX, Marca,
      TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
      ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
      QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
      CO_0_GGXRcl,
      CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
      CO_0_JmpMovID,
      CO_0_JmpNivel1,
      CO_0_JmpNivel2,
      CO_0_JmpGGX,
      CO_0_RmsMovID,
      CO_0_RmsNivel1,
      CO_0_RmsNivel2,
      CO_0_RmsGGX,
      CO_0_GSPJmpMovID,
      CO_0_GSPJmpNiv2,
      CO_0_MovCodPai,
      CO_0_VmiPai,
      CO_0_IxxMovIX,
      CO_0_IxxFolha,
      CO_0_IxxLinha,
      CO_TRUE_ExigeClientMO,
      CO_TRUE_ExigeFornecMO,
      CO_TRUE_ExigeStqLoc,
      IuvpeiInn(*Classificação de artigo re ribeira x/4*)) then
      begin
        CtrlSorc := SrcNivel2;
        CtrlDest := Controle;
      end;
    end;
  end;
  //
var
  Controle, VMI_Sorc, VMI_Dest, VMI_Baix, MovimIDGer: Integer;
  DtHrIni: String;
  Qry: TmySQLQuery;
begin
  Result := VSPallet;
  if VSPallet > 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT scc.EntiSitio FornecMO',
      'FROM stqcencad scc ',
      'LEFT JOIN stqcenloc scl ON scl.Codigo=scc.codigo',
      'WHERE scl.Controle=' + Geral.FF0(StqCenLoc),
      '']);
      FornecMO := Qry.FieldByName('FornecMO').AsInteger;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle, VSPallet ',
      'FROM vspaclaitsa ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'AND Tecla=' + Geral.FF0(Tecla),
      'AND DtHrFim < "1900-01-01" ',
      'ORDER BY DtHrFim DESC, Controle DESC ',
      '']);
      if Qry.FieldByName('VSPallet').AsInteger <> VSPallet then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando box ' +
          Geral.FF0(Tecla));
        DtHrIni := Geral.FDT(DModG.ObtemAgora(), 109);
        //
        CriaVSMovIts();
        VMI_Sorc := VSMovIts;
        VMI_Dest := CtrlDest;
        VMI_Baix := CtrlSorc;
        //
        Controle := UMyMod.BPGS1I32('vspaclaitsa', 'Controle', '', '', tsPos, stIns, 0);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vspaclaitsa', False, [
        'Codigo', 'VSPallet', 'Tecla',
        'DtHrIni', 'VMI_Sorc', 'VMI_Baix',
        'VMI_Dest'], [
        'Controle'], [
        Codigo, VSPallet, Tecla,
        DtHrIni, VMI_Sorc, VMI_Baix,
        VMI_Dest], [
        Controle], True) then
        begin
          Result := VSPallet;
          //
          MovimIDGer := Integer(emidClassArtXXUni);
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspalleta', False, [
          'MovimIDGer'], ['Codigo'], [
          MovimIDGer], [Codigo], True) then
            AtualizaStatPall(Codigo);
        end;
      end;
    finally
      Qry.Free;
    end;
  end;
end;

function TUnVS_CRC_PF.InsAltVSPalRclNewUni(const Empresa, FornecMO, ClientMO, Fornecedor,
  VSMulFrnCab: Integer; const MovimID: TEstqMovimID; const Codigo, MovimCod,
  BxaGraGruX, BxaMovimID, BxaMovimNiv, BxaSrcNivel1, BxaSrcNivel2, VSMovIts,
  Tecla, VSPallet, GragruX, SrcPallet, StqCenLoc: Integer; const
  IuvpeiInn, IuvpeiBxa: TInsUpdVMIPrcExecID; const
  LaAviso1, LaAviso2: TLabel; var CtrlSorc, CtrlDest: Integer): Integer;
{
  function CriaVSMovIts(): Integer;
  const
    QtdGerPeca = 0;
    QtdGerPeso = 0;
    LnkNivXtr1 = 0;
    LnkNivXtr2 = 0;
    CliVenda   = 0;
    AreaM2     = 0;
    AreaP2     = 0;
    CustoMOKg  = 0;
    CustoMOTot = 0;
    ValorT     = 0;
    //Misturou   = 0;
    AptoUso    = 0;
    FornecMO   = 0;
    Pecas      = 0;
    PesoKg     = 0;
    Observ     = ''; // Como fazer!!!
    SerieFch   = 0;  // Fazer depois!!!
    Ficha      = 0;  // Fazer depois!!!
    Marca      = ''; // Fazer depois!!!
    ValorMP    = 0;  // Fazer depois!!!
    QtdGerArM2 = 0;  // Fazer depois!!!
    QtdGerArP2 = 0;  // Fazer depois!!!
    NotaMPAG   = 0;
    //
    ExigeFornecedor = True;
    //
    TpCalcAuto = -1;
    //
    EdPallet = nil;
    EdValorT = nil;
    EdAreaM2 = nil;
  var
    Controle, SrcNivel1, SrcNivel2, SrcGGX, DstNivel1, DstNivel2, MovimTwn,
    DstGGX, Pallet: Integer;
    MovimNiv: TEstqMovimNiv;
    DataHora: String;
    SrcMovID, DstMovID: TEstqMovimID;
  begin
    Result := 0;
    DataHora := Geral.FDT(DModG.ObtemAgora(), 109);
    //
    // Classe a reclassificar
    MovimTwn := UMyMod.BPGS1I32('v s m o v i t s', 'MovimTwn', '', '', tsPos, stIns, MovimTwn);
    Controle := 0;
(*   Desabilitado em 2014-10-12  - Pode ser mais de um Registro!
    Controle := UMyMod.BPGS1I32('v s m o v i t s', 'Controle', '', '', tsPos, stIns, Controle);
    //SrcMovimNiv := TEstqMovimNiv(BxaMovimNiv);
    SrcNivel1    := 0; //BxaSrcNivel1;
    SrcNivel2    := 0; //BxaSrcNivel2;
    SrcGGX       := 0; //BxaGraGruX;
    //
    DstMovID     := MovimID;
    DstNivel1    := Codigo;
*)
    DstNivel2    := UMyMod.BPGS1I32('v s m o v i t s', 'Controle', '', '', tsPos, stIns, 0);
(*
    DstGGX       := GraGruX;
    //
    MovimNiv := eminSorcReclass;
    //
    /
    if InsUpdVSMovIts_(stIns, Codigo, MovimCod, MovimTwn, Empresa,
    Fornecedor, MovimID, MovimNiv, Pallet, BxaGraGruX, Pecas, PesoKg, AreaM2,
    AreaP2, ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ,
    LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg,
    CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso,
    QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
    NotaMPAG, SrcGGX, DstGGX, Marca) then
*)
    begin
      // Nova classe
      SrcMovID  := MovimID;
      SrcNivel1 := Codigo;
      SrcNivel2 := Controle;
      SrcGGX    := GraGruX;
      //
      Controle  := DstNivel2;
      //
      DstMovID     := TEstqMovimID(0);
      DstNivel1    := 0;
      DstNivel2    := 0;
      DstGGX       := 0;
      Pallet       := VSPallet;
      //
      MovimNiv     := eminDestReclass;
      //
      if InsUpdVSMovIts_(stIns, Codigo, MovimCod, MovimTwn, Empresa,
      Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
      AreaP2, ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ,
      LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg,
      CustoMOTot, ValorMP, SrcMovID, SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso,
      QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
      NotaMPAG, SrcGGX, SrcGGX, Marca, TpCalcAuto) then
      begin
        CtrlSorc := SrcNivel2;
        CtrlDest := Controle;
      end;
    end;
  end;
  //
}
  function CriaVSMovIts(): Integer;
  const
    QtdGerPeca = 0;
    QtdGerPeso = 0;
    LnkNivXtr1 = 0;
    LnkNivXtr2 = 0;
    CliVenda   = 0;
    AreaM2     = 0;
    AreaP2     = 0;
    CustoMOKg  = 0;
    CustoMOM2  = 0;
    CustoMOTot = 0;
    ValorT     = 0;
    //Misturou   = 0;
    AptoUso    = 0;
    //FornecMO   = 0;
    Pecas      = 0;
    PesoKg     = 0;
    Observ     = ''; // Como fazer!!!
    SerieFch   = 0;  // Fazer depois!!!
    Ficha      = 0;  // Fazer depois!!!
    Marca      = ''; // Fazer depois!!!
    ValorMP    = 0;  // Fazer depois!!!
    QtdGerArM2 = 0;  // Fazer depois!!!
    QtdGerArP2 = 0;  // Fazer depois!!!
    NotaMPAG   = 0;
    //
    ExigeFornecedor = True;
    //
    TpCalcAuto = -1;
    //
    EdPallet = nil;
    EdValorT = nil;
    EdAreaM2 = nil;
    //
    PedItsLib  = 0;
    PedItsFin  = 0;
    PedItsVda  = 0;
    //
    GSPSrcMovID = TEstqMovimID(0);
    GSPSrcNiv2 = 0;
    //
    ReqMovEstq = 0;
    //
    ItemNFe    = 0;
    //
    QtdAntPeca = 0;
    QtdAntPeso = 0;
    QtdAntArM2 = 0;
    QtdAntArP2 = 0;
  var
    Controle, SrcNivel1, SrcNivel2, SrcGGX, DstNivel1, DstNivel2, MovimTwn,
    DstGGX, Pallet: Integer;
    MovimNiv: TEstqMovimNiv;
    DataHora: String;
    SrcMovID, DstMovID: TEstqMovimID;
  begin
    Result := 0;
    MovimTwn := 0;
    DataHora := Geral.FDT(DModG.ObtemAgora(), 109);
    //
    // Baixa (Classe a classificar)
    MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, stIns, MovimTwn);
    Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, stIns, 0);
(*  Errado!!!! 2015-03-30
    //SrcMovimNiv := TEstqMovimNiv(BxaMovimNiv);
    SrcNivel1    := 0; //BxaSrcNivel1;
    SrcNivel2    := 0; //BxaSrcNivel2;
    SrcGGX       := 0; //BxaGraGruX;
*)
    SrcMovID     := TEstqMovimID(BxaMovimID);
    SrcNivel1    := BxaSrcNivel1;
    SrcNivel2    := BxaSrcNivel2;
    SrcGGX       := BxaGraGruX;
    //
    // Erro! 2015-05-04
    //Pallet       := 0;
    Pallet       := SrcPallet;
    // FIM 2015-05-04
    //
    DstMovID     := MovimID;
    DstNivel1    := Codigo;
    DstNivel2    := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, stIns, 0);
    DstGGX       := GraGruX;
    //
    MovimNiv := eminSorcClass;
    //
    if InsUpdVSMovIts3(stIns, Codigo, MovimCod, MovimTwn, Empresa,
    Fornecedor, MovimID, MovimNiv, Pallet, BxaGraGruX, Pecas, PesoKg, AreaM2,
    AreaP2, ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ,
    LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg,
    CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso,
    QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
    NotaMPAG, SrcGGX, DstGGX, Marca,
    TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
    ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
    QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
    CO_0_GGXRcl,
    CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
    CO_0_JmpMovID,
    CO_0_JmpNivel1,
    CO_0_JmpNivel2,
    CO_0_JmpGGX,
    CO_0_RmsMovID,
    CO_0_RmsNivel1,
    CO_0_RmsNivel2,
    CO_0_RmsGGX,
    CO_0_GSPJmpMovID,
    CO_0_GSPJmpNiv2,
    CO_0_MovCodPai,
    CO_0_VmiPai,
    CO_0_IxxMovIX,
    CO_0_IxxFolha,
    CO_0_IxxLinha,
    CO_TRUE_ExigeClientMO,
    CO_TRUE_ExigeFornecMO,
    CO_TRUE_ExigeStqLoc,
    IuvpeiBxa) then
    begin
      // Nova classe (Destino)
      SrcMovID  := MovimID;
      SrcNivel1 := Codigo;
      SrcNivel2 := Controle;
      SrcGGX    := GraGruX;
      //
      Pallet    := VSPallet;
      //
      Controle  := DstNivel2;
      //
      DstMovID     := TEstqMovimID(0);
      DstNivel1    := 0;
      DstNivel2    := 0;
      DstGGX       := 0;
      //
      MovimNiv     := eminDestClass;
      //
      if InsUpdVSMovIts3(stIns, Codigo, MovimCod, MovimTwn, Empresa,
      Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
      AreaP2, ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ,
      LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg,
      CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso,
      QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
      NotaMPAG, SrcGGX, SrcGGX, Marca,
      TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
      ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
      QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
      CO_0_GGXRcl,
      CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
      CO_0_JmpMovID,
      CO_0_JmpNivel1,
      CO_0_JmpNivel2,
      CO_0_JmpGGX,
      CO_0_RmsMovID,
      CO_0_RmsNivel1,
      CO_0_RmsNivel2,
      CO_0_RmsGGX,
      CO_0_GSPJmpMovID,
      CO_0_GSPJmpNiv2,
      CO_0_MovCodPai,
      CO_0_VmiPai,
      CO_0_IxxMovIX,
      CO_0_IxxFolha,
      CO_0_IxxLinha,
      CO_TRUE_ExigeClientMO,
      CO_TRUE_ExigeFornecMO,
      CO_TRUE_ExigeStqLoc,
      IuvpeiInn) then
      begin
        CtrlSorc := SrcNivel2;
        CtrlDest := Controle;
      end;
    end;
  end;
  //
var
  Controle, VMI_Sorc, VMI_Baix, VMI_Dest, MovimIDGer: Integer;
  DtHrIni: String;
  Qry: TmySQLQuery;
begin
  Result := VSPallet;
  if VSPallet > 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle, VSPallet ',
      'FROM vsparclitsa ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'AND Tecla=' + Geral.FF0(Tecla),
      'AND DtHrFim < "1900-01-01" ',
      'ORDER BY DtHrFim DESC, Controle DESC ',
      '']);
      if Qry.FieldByName('VSPallet').AsInteger <> VSPallet then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando box ' +
          Geral.FF0(Tecla));
        DtHrIni := Geral.FDT(DModG.ObtemAgora(), 109);
        //
        CriaVSMovIts();
        VMI_Sorc := VSMovIts;
        VMI_Dest := CtrlDest;
        VMI_Baix := CtrlSorc;
        //
        Controle := UMyMod.BPGS1I32('vsparclitsa', 'Controle', '', '', tsPos, stIns, 0);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsparclitsa', False, [
        'Codigo', 'VSPallet', 'Tecla',
        'DtHrIni',
        'VMI_Sorc', 'VMI_Baix', 'VMI_Dest'], [
        'Controle'], [
        Codigo, VSPallet, Tecla,
        DtHrIni, VMI_Sorc, VMI_Baix, VMI_Dest], [
        Controle], True) then
        begin
          Result := VSPallet;
          //
          MovimIDGer := Integer(emidReclasXXUni);
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspalleta', False, [
          'MovimIDGer'], ['Codigo'], [
          MovimIDGer], [Codigo], True) then
            AtualizaStatPall(Codigo);
        end;
      end;
    finally
      Qry.Free;
    end;
  end;
end;

procedure TUnVS_CRC_PF.InsereVSCacCab(Codigo: Integer; MovimID: TEstqMovimID;
  CodigoID: Integer);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vscaccab', False, [
  'MovimID', 'CodigoID'], [
  'Codigo'], [
  MovimID, CodigoID], [
  Codigo], True);
end;

procedure TUnVS_CRC_PF.InsereVSFchRMPCab(SerieFch, Ficha, MovimID,
  Terceiro: Integer);
const
  ReInseriu = 1;
//var
  //SQL_ReInseriu: String;
begin
  //SQL_ReInseriu := 'ReInseriu = ReInseriu + 1';
  UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'vsfchrmpcab', False, [
  'Terceiro', 'ReInseriu'], [
  'SerieFch', 'Ficha', 'MovimID'], [
  'ReInseriu'], [
  Terceiro, ReInseriu], [
  SerieFch, Ficha, MovimID], [ReInseriu], False);
end;

procedure TUnVS_CRC_PF.InsereVSMovCab(Codigo: Integer; MovimID: TEstqMovimID;
  CodigoID: Integer);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsmovcab', False, [
  'MovimID', 'CodigoID'], [
  'Codigo'], [
  MovimID, CodigoID], [
  Codigo], True);
end;

function TUnVS_CRC_PF.InsereVSMovDif(Controle: Integer; Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, InfPecas, InfPesoKg, InfAreaM2, InfAreaP2, InfValorT,
  PerQbrViag, PerQbrSal, RstCouPc, RstCouKg, RstCouVl, RstSalKg, RstSalVl,
  RstTotVl: Double; TribDefSel: Integer; PesoSalKg: Double): Boolean;
var
  DifPecas, DifPesoKg, DifAreaM2, DifAreaP2, DifValorT: Double;
begin
  DifPecas       := Pecas  - InfPecas;
  DifPesoKg      := PesoKg - InfPesoKg;
  DifAreaM2      := AreaM2 - InfAreaM2;
  DifAreaP2      := AreaP2 - InfAreaP2;
  DifValorT      := ValorT - InfValorT;
  //
  Result := UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'vsmovdif', False, [
  'InfPecas', 'InfPesoKg', 'InfAreaM2',
  'InfAreaP2', 'InfValorT', 'DifPecas',
  'DifPesoKg', 'DifAreaM2', 'DifAreaP2',
  'DifValorT', 'PerQbrViag', 'PerQbrSal',
  'RstCouVl', 'RstSalKg', 'RstSalVl',
  'RstTotVl', 'TribDefSel', 'PesoSalKg',
  'RstCouPc', 'RstCouKg' ], [
  'Controle'], [
  'InfPecas', 'InfPesoKg', 'InfAreaM2',
  'InfAreaP2', 'InfValorT', 'DifPecas',
  'DifPesoKg', 'DifAreaM2', 'DifAreaP2',
  'DifValorT', 'PerQbrViag', 'PerQbrSal',
  'RstCouVl', 'RstSalKg', 'RstSalVl',
  'RstTotVl', 'TribDefSel', 'PesoSalKg',
  'RstCouPc', 'RstCouKg'], [
  InfPecas, InfPesoKg, InfAreaM2,
  InfAreaP2, InfValorT, DifPecas,
  DifPesoKg, DifAreaM2, DifAreaP2,
  DifValorT, PerQbrViag, PerQbrSal,
  RstCouVl, RstSalKg, RstSalVl,
  RstTotVl, TribDefSel, PesoSalKg,
  RstCouPc, RstCouKg], [
  Controle], [
  InfPecas, InfPesoKg, InfAreaM2,
  InfAreaP2, InfValorT, DifPecas,
  DifPesoKg, DifAreaM2, DifAreaP2,
  DifValorT, PerQbrViag, PerQbrSal,
  RstCouVl, RstSalKg, RstSalVl,
  RstTotVl, TribDefSel, PesoSalKg,
  RstCouPc, RstCouKg], True);
end;


{
function TUnVS_CRC_PF.InsUpdVSMovIts1(SQLType: TSQLType; Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro: Integer; MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv;
  Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  _Data_Hora_: String; SrcMovID: TEstqMovimID; SrcNivel1, SrcNivel2: Integer;
  Observ: String; LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,
  Misturou*): Integer; CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double; DstMovID:
  TEstqMovimID; DstNivel1, DstNivel2: Integer; QtdGerPeca, QtdGerPeso,
  QtdGerArM2, QtdGerArP2: Double; AptoUso, FornecMO, SerieFch: Integer;
  NotaMPAG: Double; SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
  GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2: Double; GGXRcl: Integer;
  JmpMovID: TEstqMovimID = emidAjuste;
  JmpNivel1: Integer = 0; JmpNivel2: Integer = 0; JmpGGX: Integer = 0;
  RmsMovID: TEstqMovimID = emidAjuste;
  RmsNivel1: Integer = 0; RmsNivel2: Integer = 0; RmsGGX: Integer = 0;
  GSPJmpMovID: TEstqMovimID = emidAjuste; GSPJmpNiv2: Integer = 0;
  (*ForcaDtCorrApo: TDateTime = 0;*) MovCodPai: Integer = 0;
  IxxMovIX: TEstqMovInfo = TEstqMovInfo.eminfIndef; IxxFolha: Integer = 0;
  IxxLinha: Integer = 0): Boolean;
const
  ExigeClientMO = False;
  ExigeFornecMO = False;
  ExigeStqLoc   = False;
begin
  Result := InsUpdVS_VMI(SQLType, Codigo, MovimCod, MovimTwn, Empresa, Terceiro,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  _Data_Hora_, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle, Ficha, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP,
  DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
  QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO, QtdAntPeca, QtdAntPeso,
  QtdAntArM2, QtdAntArP2, GGXRcl, JmpMovID, JmpNivel1, JmpNivel2, JmpGGX,
  RmsMovID, RmsNivel1, RmsNivel2, RmsGGX, GSPJmpMovID, GSPJmpNiv2, MovCodPai,
  IxxMovIX, IxxFolha, IxxLinha, ExigeClientMO, ExigeFornecMO, ExigeStqLoc,
  TInsUpdVMIPrcExecID.iuvpei000);
end;
}

function TUnVS_CRC_PF.InsUpdVSMovIts2(SQLType: TSQLType; Codigo, MovimCod,
  MovimTwn, Empresa, Terceiro: Integer; MovimID: TEstqMovimID; MovimNiv:
  TEstqMovimNiv; Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2, AreaP2,
  ValorT: Double; _Data_Hora_: String; SrcMovID: TEstqMovimID; SrcNivel1,
  SrcNivel2: Integer; Observ: String; LnkNivXtr1, LnkNivXtr2, CliVenda,
  Controle, Ficha: Integer; CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
  DstMovID: TEstqMovimID; DstNivel1, DstNivel2: Integer; QtdGerPeca, QtdGerPeso,
  QtdGerArM2, QtdGerArP2: Double; AptoUso, FornecMO, SerieFch: Integer;
  NotaMPAG: Double; SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
  GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, PerceComiss, CusKgComiss,
  CustoComiss, CredPereImposto, CredValrImposto, CusFrtAvuls: Double;
  GGXRcl: Integer;
  RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI: Double;
  JmpMovID: TEstqMovimID; JmpNivel1, JmpNivel2, JmpGGX: Integer;
  RmsMovID: TEstqMovimID; RmsNivel1, RmsNivel2, RmsGGX: Integer;
  GSPJmpMovID: TEstqMovimID; GSPJmpNiv2, MovCodPai: Integer;
  IxxMovIX: TEstqMovInfo; IxxFolha, IxxLinha: Integer;
  ExigeClientMO, ExigeFornecMO, ExigeStqLoc: Boolean;
  InsUpdVMIPrcExecID: TInsUpdVMIPrcExecID): Boolean;
const
  VmiPai = 0;
begin
  Result := InsUpdVS_VMI(SQLType, Codigo, MovimCod, MovimTwn, Empresa, Terceiro,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  _Data_Hora_, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle, Ficha, CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP,
  DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
  QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO, QtdAntPeca, QtdAntPeso,
  QtdAntArM2, QtdAntArP2, PerceComiss, CusKgComiss, CustoComiss, CredPereImposto,
  CredValrImposto, CusFrtAvuls, GGXRcl,
  RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI,
  JmpMovID, JmpNivel1, JmpNivel2, JmpGGX,
  RmsMovID, RmsNivel1, RmsNivel2, RmsGGX,
  GSPJmpMovID, GSPJmpNiv2, MovCodPai, VmiPai,
  IxxMovIX, IxxFolha, IxxLinha, ExigeClientMO, ExigeFornecMO, ExigeStqLoc,
  0(*MedEClas*),
  InsUpdVMIPrcExecID);
end;

function TUnVS_CRC_PF.InsUpdVSMovIts3(SQLType: TSQLType; Codigo, MovimCod, MovimTwn,
              Empresa, Terceiro: Integer; MovimID: TEstqMovimID;
              MovimNiv: TEstqMovimNiv;
              Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2, AreaP2,
              ValorT: Double; _Data_Hora_: String; SrcMovID: TEstqMovimID;
              SrcNivel1, SrcNivel2: Integer; Observ: String;
              LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,
              Misturou*): Integer; CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
              DstMovID: TEstqMovimID; DstNivel1, DstNivel2: Integer;
              QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
              AptoUso, FornecMO, SerieFch: Integer; NotaMPAG: Double;
              SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto, PedItsLib,
              PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
              GSPSrcNiv2, ReqMovEstq,
              StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
              QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, PerceComiss,
              CusKgComiss, CustoComiss, CredPereImposto, CredValrImposto,
              CusFrtAvuls: Double; GGXRcl: Integer;
              RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI: Double;
              JmpMovID: TEstqMovimID; JmpNivel1, JmpNivel2, JmpGGX: Integer;
              RmsMovID: TEstqMovimID; RmsNivel1, RmsNivel2, RmsGGX: Integer;
              GSPJmpMovID: TEstqMovimID; GSPJmpNiv2, MovCodPai, VmiPai: Integer;
              IxxMovIX: TEstqMovInfo; IxxFolha, IxxLinha: Integer;
              ExigeClientMO, ExigeFornecMO, ExigeStqLoc: Boolean;
              InsUpdVMIPrcExecID: TInsUpdVMIPrcExecID): Boolean;
begin
  Result := InsUpdVS_VMI(SQLType, Codigo, MovimCod, MovimTwn, Empresa, Terceiro,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  _Data_Hora_, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle, Ficha, CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP,
  DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
  QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO, QtdAntPeca, QtdAntPeso,
  QtdAntArM2, QtdAntArP2, PerceComiss, CusKgComiss, CustoComiss, CredPereImposto,
  CredValrImposto, CusFrtAvuls, GGXRcl,
  RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI,
  JmpMovID, JmpNivel1, JmpNivel2, JmpGGX,
  RmsMovID, RmsNivel1, RmsNivel2, RmsGGX,
  GSPJmpMovID, GSPJmpNiv2, MovCodPai, VmiPai,
  IxxMovIX, IxxFolha, IxxLinha, ExigeClientMO, ExigeFornecMO, ExigeStqLoc,
  0(*MedEClas*), // Precisa informar Area na geração!
  InsUpdVMIPrcExecID);
end;

function TUnVS_CRC_PF.InsUpdVSMovIts4(SQLType: TSQLType; Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro: Integer; MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2, AreaP2,
  ValorT: Double; _Data_Hora_: String; SrcMovID: TEstqMovimID;
  SrcNivel1, SrcNivel2: Integer; Observ: String;
  LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,
  Misturou*): Integer; CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double;
  DstMovID: TEstqMovimID; DstNivel1, DstNivel2: Integer;
  QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2: Double;
  AptoUso, FornecMO, SerieFch: Integer; NotaMPAG: Double;
  SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto, PedItsLib,
  PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
  GSPSrcNiv2, ReqMovEstq,
  StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, PerceComiss,
  CusKgComiss, CustoComiss, CredPereImposto, CredValrImposto,
  CusFrtAvuls: Double; GGXRcl: Integer;
  RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI: Double;
  JmpMovID: TEstqMovimID; JmpNivel1, JmpNivel2, JmpGGX: Integer;
  RmsMovID: TEstqMovimID; RmsNivel1, RmsNivel2, RmsGGX: Integer;
  GSPJmpMovID: TEstqMovimID; GSPJmpNiv2, MovCodPai, VmiPai: Integer;
  IxxMovIX: TEstqMovInfo; IxxFolha, IxxLinha: Integer;
  ExigeClientMO, ExigeFornecMO, ExigeStqLoc: Boolean;
  //
  MedEClas: Integer;
  //
  InsUpdVMIPrcExecID: TInsUpdVMIPrcExecID): Boolean;
begin
  Result := InsUpdVS_VMI(SQLType, Codigo, MovimCod, MovimTwn, Empresa, Terceiro,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  _Data_Hora_, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle, Ficha, CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP,
  DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
  QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO, QtdAntPeca, QtdAntPeso,
  QtdAntArM2, QtdAntArP2, PerceComiss, CusKgComiss, CustoComiss, CredPereImposto,
  CredValrImposto, CusFrtAvuls, GGXRcl,
  RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI,
  JmpMovID, JmpNivel1, JmpNivel2, JmpGGX,
  RmsMovID, RmsNivel1, RmsNivel2, RmsGGX,
  GSPJmpMovID, GSPJmpNiv2, MovCodPai, VmiPai,
  IxxMovIX, IxxFolha, IxxLinha, ExigeClientMO, ExigeFornecMO, ExigeStqLoc,
  MedEClas, InsUpdVMIPrcExecID);
end;

function TUnVS_CRC_PF.InsUpdVS_VMI(SQLType: TSQLType; Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro: Integer; MovimID: TEstqMovimID; MovimNiv: TEstqMovimNiv;
  Pallet, GraGruX: Integer; Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  _Data_Hora_: String; SrcMovID: TEstqMovimID; SrcNivel1, SrcNivel2: Integer;
  Observ: String; LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha(*,
  Misturou*): Integer; CustoMOPc, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP: Double; DstMovID:
  TEstqMovimID; DstNivel1, DstNivel2: Integer; QtdGerPeca, QtdGerPeso,
  QtdGerArM2, QtdGerArP2: Double; AptoUso, FornecMO, SerieFch: Integer;
  NotaMPAG: Double; SrcGGX, DstGGX: Integer; Marca: String; TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda: Integer; GSPSrcMovID: TEstqMovimID;
  GSPSrcNiv2, ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO: Integer;
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2, PerceComiss, CusKgComiss,
  CustoComiss, CredPereImposto, CredValrImposto, CusFrtAvuls: Double;
  GGXRcl: Integer;
  RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI: Double;
(*
  JmpMovID: TEstqMovimID = emidAjuste;
  JmpNivel1: Integer = 0; JmpNivel2: Integer = 0; JmpGGX: Integer = 0;
  RmsMovID: TEstqMovimID = emidAjuste;
  RmsNivel1: Integer = 0; RmsNivel2: Integer = 0; RmsGGX: Integer = 0;
  GSPJmpMovID: TEstqMovimID = emidAjuste; GSPJmpNiv2: Integer = 0;
  MovCodPai: Integer = 0;
  IxxMovIX: TEstqMovInfo = TEstqMovInfo.eminfIndef; IxxFolha: Integer = 0;
  IxxLinha: Integer = 0): Boolean;
  *)
  JmpMovID: TEstqMovimID; JmpNivel1, JmpNivel2, JmpGGX: Integer;
  RmsMovID: TEstqMovimID; RmsNivel1, RmsNivel2, RmsGGX: Integer;
  GSPJmpMovID: TEstqMovimID; GSPJmpNiv2, MovCodPai, VmiPai: Integer;
  IxxMovIX: TEstqMovInfo; IxxFolha, IxxLinha: Integer;
  ExigeClientMO, ExigeFornecMO, ExigeStqLoc: Boolean;
  MedEClas: Integer;
  InsUpdVMIPrcExecID: TInsUpdVMIPrcExecID): Boolean;
var
  Iuvpei: Integer;
  DtCorrApo, DataHora, NO_Iuvpei: String;
begin
  //if VAR_USUARIO = -1 then
  if MovimID <> TEstqMovimID.emidEntraExced then
  begin
    if (StqCenLoc = 0) or (FornecMO = 0) or (ClientMO = 0) then
    begin
      Iuvpei    := Integer(InsUpdVMIPrcExecID);
      NO_Iuvpei := 'Iuvpei = ' +Geral.FF0(Iuvpei) + ' : ' +
        sListaIuvpeiTxt[Iuvpei];
      if ExigeStqLoc and (StqCenLoc = 0) then
        Geral.MB_Erro('StqCenLoc indefinido!' + sLineBreak +
        'MovimID=' + Geral.FF0(Integer(MovimID)) + ' ' +
        GetEnumName(TypeInfo(TEstqMovimID), Integer(MovimID)) + sLineBreak +
        'MovimNiv=' + Geral.FF0(Integer(MovimNiv)) + ' ' +
        GetEnumName(TypeInfo(TEstqMovimNiv), Integer(MovimNiv)) +
        sLineBreak + NO_Iuvpei);
      if ExigeFornecMO and (FornecMO = 0) then
        Geral.MB_Erro('FornecMO indefinido!' + sLineBreak +
        'MovimID=' + Geral.FF0(Integer(MovimID)) + ' ' +
        GetEnumName(TypeInfo(TEstqMovimID), Integer(MovimID)) + sLineBreak +
        'MovimNiv=' + Geral.FF0(Integer(MovimNiv)) + ' ' +
        GetEnumName(TypeInfo(TEstqMovimNiv), Integer(MovimNiv)) +
        sLineBreak + NO_Iuvpei);
      if ExigeClientMO and (ClientMO = 0) then
        Geral.MB_Erro('ClientMO indefinido!' + sLineBreak +
        'MovimID=' + Geral.FF0(Integer(MovimID)) + ' ' +
        GetEnumName(TypeInfo(TEstqMovimID), Integer(MovimID)) + sLineBreak +
        'MovimNiv=' + Geral.FF0(Integer(MovimNiv)) + ' ' +
        GetEnumName(TypeInfo(TEstqMovimNiv), Integer(MovimNiv)) +
        sLineBreak + NO_Iuvpei);
    end;
  end;
  //
  if not DefineDatasVMI(_Data_Hora_, DataHora, DtCorrApo) then
    Exit;
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, CO_INS_TAB_VMI, False, [
  'Codigo', 'MovimCod', 'Empresa',
  'MovimID', 'MovimNiv',
  'Pallet', 'GraGruX',
  'Pecas', 'PesoKg', 'AreaM2',
  'AreaP2', 'ValorT', 'Terceiro',
  'SrcMovID', 'SrcNivel1', 'SrcNivel2', 'SrcGGX',
  CO_DATA_HORA_VMI, 'Observ', 'CliVenda',
  'LnkNivXtr1', 'LnkNivXtr2', 'MovimTwn',
  'Ficha', (*'Misturou',*) 'CustoMOKg',
  'CustoMOPc', 'CustoMOM2', 'CustoMOTot', 'ValorMP',
  'DstMovID', 'DstNivel1', 'DstNivel2', 'DstGGX',
  'QtdGerPeca', 'QtdGerPeso',
  'QtdGerArM2', 'QtdGerArP2',
  'AptoUso', 'FornecMO', 'SerieFch',
  'NotaMPAG', 'Marca', 'TpCalcAuto',
  'PedItsLib', 'PedItsFin', 'PedItsVda',
  'GSPSrcMovID', 'GSPSrcNiv2', 'ReqMovEstq',
  'StqCenLoc', 'ItemNFe', 'VSMulFrnCab',
  'ClientMO', 'QtdAntPeca', 'QtdAntPeso',
  'QtdAntArM2', 'QtdAntArP2',

  'PerceComiss', 'CusKgComiss', 'CustoComiss',
  'CredPereImposto', 'CredValrImposto', 'CusFrtAvuls',

  'GGXRcl',
  'RpICMS', 'RpPIS', 'RpCOFINS', 'RvICMS', 'RvPIS', 'RvCOFINS', 'RpIPI', 'RvIPI',
   //
  'JmpMovID', 'JmpNivel1', 'JmpNivel2',
  'JmpGGX', 'RmsMovID', 'RmsNivel1',
  'RmsNivel2', 'RmsGGX', 'GSPJmpMovID',
  'GSPJmpNiv2', 'DtCorrApo', 'MovCodPai', 'VmiPai',
  'IxxMovIX', 'IxxFolha', 'IxxLinha',
  'MedEClas',
  'Iuvpei'], [
  'Controle'], [
  Codigo, MovimCod, Empresa,
  Integer(MovimID), Integer(MovimNiv),
  Pallet, GraGruX,
  Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, Terceiro,
  Integer(SrcMovID), SrcNivel1, SrcNivel2, SrcGGX,
  DataHora, Observ, CliVenda,
  LnkNivXtr1, LnkNivXtr2, MovimTwn,
  Ficha, (*Misturou,*) CustoMOKg,
  CustoMOPc, CustoMOM2, CustoMOTot, ValorMP,
  DstMovID, DstNivel1, DstNivel2, DstGGX,
  QtdGerPeca, QtdGerPeso,
  QtdGerArM2, QtdGerArP2,
  AptoUso, FornecMO, SerieFch,
  NotaMPAG, Marca, TpCalcAuto,
  PedItsLib, PedItsFin, PedItsVda,
  Integer(GSPSrcMovID), GSPSrcNiv2, ReqMovEstq,
  StqCenLoc, ItemNFe, VSMulFrnCab,
  ClientMO, QtdAntPeca, QtdAntPeso,
  QtdAntArM2, QtdAntArP2,

  PerceComiss, CusKgComiss, CustoComiss,
  CredPereImposto, CredValrImposto, CusFrtAvuls,

  GGXRcl,
  RpICMS, RpPIS, RpCOFINS, RvICMS, RvPIS, RvCOFINS, RpIPI, RvIPI,
  JmpMovID, JmpNivel1, JmpNivel2,
  JmpGGX, RmsMovID, RmsNivel1,
  RmsNivel2, RmsGGX, GSPJmpMovID,
  GSPJmpNiv2, DtCorrApo, MovCodPai, VmiPai,
  Integer(IxxMovIX), IxxFolha, IxxLinha,
  MedEClas,
  Integer(InsUpdVMIPrcExecID)], [
  Controle], True);
end;

procedure TUnVS_CRC_PF.LimpaArray15Int(var Arr: TClass15Int);
var
  I: Integer;
begin
  for I := Low(Arr) to High(Arr) do
    Arr[I] := 0;
end;

function TUnVS_CRC_PF.LocalizaPelaOC(EstqMovimID: TEstqMovimID): Integer;
var
  Numero, Tabela: String;
  Qry: TmySQLQuery;
  Valor: Integer;
begin
  Result := 0;
  Tabela := '?????';
  case EstqMovimID of
    emidIndsXX  : Tabela := 'vsgerarta';
    emidReclasXXUni: Tabela := 'vsgerrcla';
    else
    begin
      Geral.MB_Aviso('Não foi possível localizar a OC solicitada!');
      Exit;
    end;
  end;
  Numero := '0';
  if InputQuery('Localização de Artigo de Ribeira pelo IME-C',
  'IME-C:', Numero) then
  begin
    Valor := Geral.IMV(Numero);
    if Valor <> 0 then
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Codigo ',
        'FROM ' + LowerCase(Tabela),
        'WHERE CacCod=' + Geral.FF0(Valor),
        '']);
        //
        Result := Qry.FieldByName('Codigo').AsInteger;
      finally
        Qry.Free;
      end;
    end;
  end;
end;

function TUnVS_CRC_PF.LocalizaPeloIMEC(EstqMovimID: TEstqMovimID): Integer;
begin
  Result := LocalizaPorCampo(EstqMovimID, 'IME-C', 'MovimCod');
end;

function TUnVS_CRC_PF.LocalizaPeloIMEI(EstqMovimID: TEstqMovimID): Integer;
begin
  Result := LocalizaPorCampo(EstqMovimID, 'IME-I', 'Controle');
end;

function TUnVS_CRC_PF.LocalizaPorCampo(EstqMovimID: TEstqMovimID;
  Caption, Campo: String): Integer;
var
  Qry: TmySQLQuery;
  Valor: Integer;
  Numero: String;
  Avisa: Boolean;
begin
  Result := 0;
  Valor := 0;
  Avisa := False;
  Numero := '0';
  if InputQuery('Localização de Artigo de Ribeira por ' + Caption,
  Caption, Numero) then
  begin
    Valor := Geral.IMV(Numero);
    if Valor <> 0 then
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT DISTINCT Codigo ',
        'FROM ' + CO_SEL_TAB_VMI,
        'WHERE ' + Campo + '=' + Geral.FF0(Valor),
        'AND MovimID=' + Geral.FF0(Integer(EstqMovimID)),
        '',
        'UNION',
        '',
        'SELECT DISTINCT Codigo ',
        'FROM ' + CO_TAB_VMB,
        'WHERE ' + Campo + '=' + Geral.FF0(Valor),
        'AND MovimID=' + Geral.FF0(Integer(EstqMovimID)),
        '']);
        Result := Qry.FieldByName('Codigo').AsInteger;
        Avisa := Result = 0;
      finally
        Qry.Free;
      end;
    end else
      Avisa := True;
  end;
  if Avisa then
    Geral.MB_Aviso('Não foi possivel a obtenção do código!');
end;

procedure TUnVS_CRC_PF.AtualizaTotaisVSXxxCab(Tabela: String; MovimCod: Integer);
var
  Qry: TmySQLQuery;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT, ValorMP, CustoComiss,
  CusFrtAvMoER: Double;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SUM(Pecas) Pecas, SUM(PesoKg) PesoKg, ',
    'SUM(AreaM2) AreaM2, SUM(AreaP2) AreaP2, ',
    'SUM(ValorT) ValorT, ',
    'SUM(ValorMP) ValorMP, ',
    'SUM(CustoComiss) CustoComiss, ',
    'SUM(CusFrtMOEnv + CusFrtMORet + CusFrtAvuls) CusFrtAvMoER ',
    'FROM ' + CO_SEL_TAB_VMI + '',
    'WHERE MovimCod=' + Geral.FF0(MovimCod),
    //
    'AND (NOT MovimNiv IN (' +
    Geral.FF0(Integer(TEstqMovimNiv.eminSorcClass)) + ',' + // 1 - // Classificação (múltipla)
    Geral.FF0(Integer(TEstqMovimNiv.eminSorcLocal)) +       // 27  // Transferencia de local
    '))', // Classificação (múltipla)
    '']);
    //
    Pecas          := Qry.FieldByName('Pecas').AsFloat;
    PesoKg         := Qry.FieldByName('PesoKg').AsFloat;
    AreaM2         := Qry.FieldByName('AreaM2').AsFloat;
    AreaP2         := Qry.FieldByName('AreaP2').AsFloat;
    ValorT         := Qry.FieldByName('ValorT').AsFloat;

    if LowerCase(Tabela) = 'vsinncab' then
    begin
      // Atualização específica para VSInnCAb
      ValorMP      := Qry.FieldByName('ValorMP').AsFloat;
      CustoComiss  := Qry.FieldByName('CustoComiss').AsFloat;
      CusFrtAvMoER := Qry.FieldByName('CusFrtAvMoER').AsFloat;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, LowerCase(Tabela), False, [
      'Pecas', 'PesoKg',
      'AreaM2', 'AreaP2',
      'ValorT',
      //
      'ValorMP', 'CustoComiss', 'CusFrtAvMoER'], [
      'MovimCod'], [
      Pecas, PesoKg,
      AreaM2, AreaP2,
      ValorT,
      //
      ValorMP, CustoComiss, CusFrtAvMoER], [
      MovimCod], True);
    end else
    begin
      // Atualização padrão
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, LowerCase(Tabela), False, [
      'Pecas', 'PesoKg',
      'AreaM2', 'AreaP2',
      'ValorT'], [
      'MovimCod'], [
      Pecas, PesoKg,
      AreaM2, AreaP2,
      ValorT], [
      MovimCod], True);
    end;
    //
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.ComparaUnidMed2GGX(GGX1, GGX2: Integer): Boolean;
var
  Qry: TmySQLQuery;
  UM1, UM2: Integer;
  function AbreQry(GGX: Integer): Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT gg1.UnidMed ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE ggx.Controle=' + Geral.FF0(GGX),
    '']);
    Result := Qry.FieldByName('UnidMed').AsInteger;
  end;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UM1 := AbreQry(GGX1);
    UM2 := AbreQry(GGX2);
    Result := UM1 = UM2;
    //
    if not Result then
      Geral.MB_Aviso('Os reduzidos ' + Geral.FF0(GGX1) + ' e ' +
      Geral.FF0(GGX2) + ' não tem a mesma unidade de medida!');
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.ConfigContinuarInserindoFolhaLinha(
  RGModoContinuarInserindo: TRadioGroup; EdFolha, EdLinha: TdmkEdit): Boolean;
begin
  if RGModoContinuarInserindo.ItemIndex = 2 then
  begin
    Result := True;
    (*
    if EdLinha.ValueVariant > 0 then
    begin
      EdLinha.ValueVariant    := EdLinha.ValueVariant + 1;
      if EdLinha.ValueVariant = 25 then
      begin
        EdFolha.ValueVariant    := EdFolha.ValueVariant + 1;
        EdLinha.ValueVariant    := 1;
      end;
    end;
    *)
    MyObjects.IncrementaFolhaLinha(EdFolha, EdLinha, CO_VS_MaxLin);
  end else
    Result := False;
end;

function TUnVS_CRC_PF.CordaIMEIS(Itens: array of Integer): String;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to Length(Itens) - 1 do
  begin
    if I > 0 then
    begin
      Result := Result + ', ';
      if (I div 20) = (I / 20) then
        Result := Result + sLineBreak;
    end;
    Result := Result + Geral.FF0(Itens[I]);
  end;
end;

procedure TUnVS_CRC_PF.CorrigeArtigoOpeEmDiante(IMEI, GGXAtu: Integer);
  procedure UpdateGraGruX(Controle, GraGrux: Integer);
  begin
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'GraGruX'], [
    'Controle'], [
    GraGruX], [
    Controle], True);
  end;
  procedure UpdateGraGruXEGGXSrc(Controle, GraGrux: Integer);
  begin
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'GraGruX', 'SrcGGX'], [
    'Controle'], [
    GraGruX, GraGruX], [
    Controle], True);
  end;
var
  Qry: TmySQLQuery;
  Bastidao, GGXDivCurt: Integer;
  Continua: Boolean;
  SQL: String;
  //
  function ReabreVSGruGGX(): Integer;
  begin
    GGXDivCurt := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT *  ',
    'FROM vsgruggx ',
    'WHERE GGXDivTrip=' + Geral.FF0(GGXAtu),
    'OR GGXIntegrl=' + Geral.FF0(GGXAtu),
    'OR GGXLaminad=' + Geral.FF0(GGXAtu),
    'AND GGXDivCurt <> 0 ',
    'ORDER BY GGXDivCurt ',
    '']);
    Result := Qry.RecordCount;
    if Result = 1 then
      GGXDivCurt := Qry.FieldByName('GGXDivCurt').AsInteger;
  end;
var
  Sim, Nao, Controle, IDNiv, MovimCod, MovimID, MovimNiv: Integer;
begin
  Continua := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Bastidao ',
    'FROM gragruxcou ',
    'WHERE GraGruX=' + Geral.FF0(GGXAtu),
    '']);
    Bastidao := Qry.FieldByName('Bastidao').AsInteger;
    case TVSBastidao(Bastidao) of
      (*1*)vsbstdIntegral,
      (*2*)vsbstdLaminado,
      (*3*)vsbstdDivTripa: Continua := True;
      (*4*)vsbstdDivCurti:
      begin
        Geral.MB_Aviso('Artigo já é dividido curtido!');
        Exit;
      end;
      else
      //vsbstdND=0,
      //vsbstdRebaixad=5,
      //vsbstdDivSemiA=6,
      //vsbstdRebxSemi=7
      begin
        Geral.MB_Aviso(
        'Artigo a ser alterado deve ser integral, laminado ou dividido tripa!');
        Exit;
      end;
    end;
    if Continua then
    begin
      if ReabreVSGruGGX() = 0 then
        MostraFormVSGruGGX(stIns, 0, Bastidao, GGXAtu);
      if (ReabreVSGruGGX() = 1) then
      begin
        if GGXDivCurt = 0 then
        begin
          Geral.MB_Erro('"GGXDivCurt" indefinido!');
          Exit;
        end;
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT *  ',
        'FROM ' + CO_SEL_TAB_VMI + ' ',
        'WHERE Controle=' + Geral.FF0(IMEI),
        'OR SrcNivel2=' + Geral.FF0(IMEI),
        'OR DstNivel2=' + Geral.FF0(IMEI),
        '']);
        SQL := Qry.SQL.Text;
        ////////////////////
        Nao := 0;
        Sim := 0;
        //Geral.MB_SQL(nil, Qry);
        while not Qry.Eof do
        begin
          Controle := Qry.FieldByName('Controle').AsInteger;
          MovimCod := Qry.FieldByName('MovimCod').AsInteger;
          MovimID  := Qry.FieldByName('MovimID').AsInteger;
          MovimNiv := Qry.FieldByName('MovimNiv').AsInteger;
          //
          IDNiv    := GetIDNiv(MovimID, MovimNiv);
          case IDNiv of
            02000: Sim := Sim + 1;
            11007:
            begin
              if (not OperacaoEhDivisao(MovimID, MovimCod)) then
                Sim := Sim + 1
              else
              begin
                Nao := Nao + 1;
                Geral.MB_Info('O IMEI ' + Geral.FF0(IMEI) +
                ' não será modificado pois é matéria-prima de divisão em outro processo!')
              end;
            end;
            11009: Sim := Sim + 1;
            19020: Sim := Sim + 1;
            else
            begin
              Geral.MB_Erro('"IDNiv" ' + Geral.FF0(IDNiv) + ' não implementado!');
              Nao := Nao + 1;
            end;
          end;
          //
          Qry.Next;
        end;
        ///////////////////////////////
        if (Sim = Qry.RecordCount) and (Nao = 0) then
        begin
          Qry.First;
          while not Qry.Eof do
          begin
            Controle := Qry.FieldByName('Controle').AsInteger;
            MovimCod := Qry.FieldByName('MovimCod').AsInteger;
            MovimID  := Qry.FieldByName('MovimID').AsInteger;
            MovimNiv := Qry.FieldByName('MovimNiv').AsInteger;
            IDNIv := GetIDNiv(MovimID, MovimNiv);
            case IDNiv of
              02000: UpdateGraGruXEGGXSrc(Controle, GGXDivCurt);
              11007:
              begin
                if (not OperacaoEhDivisao(MovimID, MovimCod)) then
                  UpdateGraGruX(Controle, GGXDivCurt)
                else
                  Geral.MB_Info('O IMEI ' + Geral.FF0(IMEI) +
                  ' não será modificado pois é matéria-prima de divisão em outro processo!')
              end;
              11009: UpdateGraGruX(Controle, GGXDivCurt);
              19020: UpdateGraGruXEGGXSrc(Controle, GGXDivCurt);
              else Nao := Nao + 1;
            end;
            //
            Qry.Next;
          end;
        end else
        begin
          Geral.MB_Erro('Alteração não efetuada!');
          Geral.MB_Info(SQL);
        end;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TUnVS_CRC_PF.CorrigeMovimID(const Codigo: Integer; const MovimIDDst,
  MovimIDOri: TEstqMovimID; const MovimNivDst, MovimNivOri: TEstqMovimNiv;
  const SQL_Extra: String; const LaAviso1, LaAviso2: TLabel;
  var Atualizando: Boolean);
var
  Qry: TmySQLQuery;
  Tabela: String;
  MovimCod: Integer;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
  Exit;
  Atualizando := not Atualizando;
  if Atualizando then
  begin
    Tabela := ObtemNomeTabelaVSXxxCab(MovimIDDst);
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM ' + Tabela,
      'WHERE Codigo>=' + Geral.FF0(Codigo),
      'ORDER BY Codigo ',
      '']);
      Qry.First;
      while not Qry.Eof do
      begin
        if not Atualizando then
          Qry.Last
        else
        begin
          MovimCod := Qry.FieldByName('MovimCod').AsInteger;
          //
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando o IME-C ' +
          Geral.FF0(Qry.FieldByName('Codigo').AsInteger));
          //
          UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'UPDATE ' + CO_UPD_TAB_VMI + ' vmi  ',
          'SET MovimID=' + Geral.FF0(Integer(MovimIDDst)),
          ', MovimNiv=' + Geral.FF0(Integer(MovimNivDst)),
          'WHERE MovimCod=' + Geral.FF0(MovimCod),
          'AND MovimID=' + Geral.FF0(Integer(MovimIDOri)),
          'AND MovimNiv=' + Geral.FF0(Integer(MovimNivOri)),
          SQL_Extra,
          '']);
        end;
        Qry.Next;
      end;
    finally
      Qry.Free;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    Atualizando := False;
  end;
end;

procedure TUnVS_CRC_PF.CorrigeNFes(const Codigo: Integer;
  const MovimID: TEstqMovimID; const MovimNivsOri,
  MovimNivsDst: array of TEstqMovimNiv; const LaAviso1, LaAviso2: TLabel;
  var Atualizando: Boolean);
var
  Qry: TmySQLQuery;
  Tabela: String;
  MovimCod: Integer;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
  Exit;
  Atualizando := not Atualizando;
  if Atualizando then
  begin
    Tabela := ObtemNomeTabelaVSXxxCab(MovimID);
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM vsoutcab ',
      'WHERE Codigo>=' + Geral.FF0(Codigo),
      //'ORDER BY Codigo DESC',
      'ORDER BY Codigo ',
      '']);
      Qry.First;
      while not Qry.Eof do
      begin
        if not Atualizando then
          Qry.Last
        else
        begin
          MovimCod := Qry.FieldByName('MovimCod').AsInteger;
          //
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando o IME-C ' +
          Geral.FF0(Qry.FieldByName('Codigo').AsInteger));
          //
          DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
          MovimCod, MovimID, MovimNivsOri, MovimNivsDst);
        end;
        Qry.Next;
      end;
    finally
      Qry.Free;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    Atualizando := False;
  end;
end;

procedure TUnVS_CRC_PF.CriaBoxEmGridPanel(const Box: Integer; const Form: TForm;
  const GridPanel: TGridPanel; var FPnTxLx, FPnBox, FPnArt1N, FPnArt2N,
  FPnArt3N, FPnNumBx, FPnIDBox, FPnSubXx, FPnBxTot, FPnIMEIImei, FPnIMEICtrl,
  FPnPalletMedia, FPnPalletArea, FPnPalletPecas, FPnPalletID,
  FPnIntMei: array of TPanel; var FLaNumBx, FLaIMEIImei, FLaIMEICtrl,
  FLaPalletMedia, FLaPalletArea, FLaPalletPecas, FLaPalletID: array of TLabel;
  var FCkSubClass: array of TCheckBox; var FCkDuplicaArea: array of TCheckBox;
  var FDBEdArtNome, FDBEdArtNoCli,
  FDBEdArtNoSta, FDBEdIMEIImei, FDBEdIMEICtrl, FDBEdPalletID: array of TDBEdit;
  var FGBIMEI, FGBPallet: array of TGroupBox; var FEdPalletMedia, FEdPalletArea,
  FEdPalletPecas: array of TdmkEdit; var FSGItens: array of TStringGrid;
  FPMItens: array of TPopupMenu);
var
  FatorFonte: Double;
  //
  function GetFontHeight(Base: Integer): Integer;
  var
    Tam: Integer;
  begin
    Tam := Trunc(-Base * (3 / GridPanel.ColumnCollection.Count));
    //Result := -Trunc(Tam * (Screen.Width / 1920));
    Result := Trunc(Tam * FatorFonte);
  end;

  function ObtemHeigth(Base: Integer): Integer;
  var
    Tam: Integer;
    H1, H2: Integer;
  begin
    Tam := Trunc(Base * (3 / GridPanel.ColumnCollection.Count));
    H1 := Trunc(Tam * (Screen.Height / 1080));
    //H2 := Trunc(Tam * (Screen.Width * 1920));
    //if H1 < H2 then
      Result := H1
    //else
      //Result := H2;
  end;

  function ObtemWidth(Base: Integer): Integer;
  var
    Tam: Integer;
  begin
    Tam := Trunc(Base * (3 / GridPanel.ColumnCollection.Count));
    Result := Trunc(Tam * (Screen.Width / 1920));
  end;

var
  IdTXT: String;
  Painel: TPanel;
begin
  if (Screen.Width / 1920) > (Screen.Height / 1080) then
    FatorFonte := Screen.Height / 1080
  else
    FatorFonte := Screen.Width / 1920;
  //
  IdTXT := Geral.FFF(Box, 2);
(*
object PnBoxT1L1: TPanel
  Align = alClient
*)
  FPnTxLx[Box] := TPanel.Create(Form);
  FPnTxLx[Box].Parent := GridPanel;
  FPnTxLx[Box].Name := 'PnTxLx' + IdTXT;
  FPnTxLx[Box].Align := alClient;
  //FPnTxLx[Box].BevelOuter := bvNone;
  FPnTxLx[Box].Caption := '';
(*
  object PnBox01: TPanel
    Align = alClient
    BevelOuter = bvNone
*)
  FPnBox[Box] := TPanel.Create(Form);
  FPnBox[Box].Parent := FPnTxLx[Box];
  FPnBox[Box].Name := 'PnBox' + IdTXT;
  FPnBox[Box].Align := alClient;
  FPnBox[Box].BevelOuter := bvNone;
  FPnBox[Box].Caption := '';
(*
    object Panel4: TPanel
      Height = 64
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
*)
    FPnArt1N[Box] := TPanel.Create(Form);
    FPnArt1N[Box].Parent := FPnBox[Box];
    FPnArt1N[Box].Name := 'PnArt1N' + IdTXT;
    //FPnArt1N[Box].Height := ObtemHeigth(72) + 4;
    FPnArt1N[Box].Height := ObtemHeigth(56) + 4;
    FPnArt1N[Box].Align := alTop;
    FPnArt1N[Box].BevelOuter := bvNone;
    FPnArt1N[Box].Caption := '';
(*
      object Panel50: TPanel
        Left = 0
        Top = 0
        Width = 41
        Height = 64
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
*)
      FPnNumBx[Box] := TPanel.Create(Form);
      FPnNumBx[Box].Parent := FPnArt1N[Box];
      FPnNumBx[Box].Name := 'PnNumBx' + IdTXT;
      FPnNumBx[Box].Width := ObtemWidth(41);
      FPnNumBx[Box].Align := alLeft;
      FPnNumBx[Box].BevelOuter := bvNone;
      FPnNumBx[Box].Caption := '';
(*
        object Label79: TLabel
          Align = alTop
          Alignment = taCenter
          Caption = 'Box'
        end
*)
        FLaNumBx[Box] := TLabel.Create(Form);
        FLaNumBx[Box].Parent := FPnNumBx[Box];
        FLaNumBx[Box].Name := 'LaNumBx' + IdTXT;
        //FLaNumBx[Box].Width := ObtemWidth(41);
        FLaNumBx[Box].Align := alTop;
        FLaNumBx[Box].Font.Height := GetFontHeight(-19(*14*));
        FLaNumBx[Box].Caption := ' Box';
        FLaNumBx[Box].Alignment := taCenter;
(*
        object Panel51: TPanel
          Align = alClient
          BevelOuter = bvNone
          Caption = '1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -37
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
      end
*)
        FPnIDBox[Box] := TPanel.Create(Form);
        FPnIDBox[Box].Parent := FPnNumBx[Box];
        FPnIDBox[Box].Name := 'PnIDBox' + IdTXT;
        FPnIDBox[Box].Width := ObtemWidth(41);
        FPnIDBox[Box].Align := alClient;
        FPnIDBox[Box].BevelOuter := bvNone;
        FPnIDBox[Box].Font.Charset := DEFAULT_CHARSET;
        FPnIDBox[Box].Font.Color := clWindowText;
        FPnIDBox[Box].Font.Height := GetFontHeight(-32(*24*));
        FPnIDBox[Box].Font.Name := 'Tahoma';
        FPnIDBox[Box].Font.Style := [fsBold];
        FPnIDBox[Box].ParentFont := False;
        FPnIDBox[Box].Caption := IdTXT;
(*
      object Panel64: TPanel
        Width = 122
        Align = alRight
        BevelOuter = bvNone
*)
      FPnSubXx[Box] := TPanel.Create(Form);
      FPnSubXx[Box].Parent := FPnArt1N[Box];
      FPnSubXx[Box].Name := 'PnSubXx' + IdTXT;
      FPnSubXx[Box].Width := ObtemWidth(122);
      FPnSubXx[Box].Align := alRight;
      FPnSubXx[Box].BevelOuter := bvNone;
      FPnSubXx[Box].Caption := '';
(*
        object CkSubClass1: TCheckBox
          Left = 4
          Top = 0
          Width = 113
          Height = 25
          Caption = 'Sub classe'
          TabOrder = 0
        end
      end
*)
        FCkSubClass[Box] := TCheckBox.Create(Form);
        FCkSubClass[Box].Parent := FPnSubXx[Box];
        FCkSubClass[Box].Name := 'CkSubClass' + IdTXT;
        FCkSubClass[Box].Width := ObtemWidth(118);
        FCkSubClass[Box].Font.Height := GetFontHeight(-19);
        FCkSubClass[Box].Top := ObtemHeigth(4);
        //FCkSubClass[Box].Left := ObtemWidth(4);
        //if FCkSubClass[Box].Left > 4 then
          FCkSubClass[Box].Left := ObtemWidth(4);
        //FCkSubClass[Box].Align := alTop;
        FCkSubClass[Box].Caption := 'Sub classe';

        FCkDuplicaArea[Box] := TCheckBox.Create(Form);
        FCkDuplicaArea[Box].Parent := FPnSubXx[Box];
        FCkDuplicaArea[Box].Name := 'CkDuplicaAreas' + IdTXT;
        FCkDuplicaArea[Box].Width := ObtemWidth(118);
        FCkDuplicaArea[Box].Font.Height := GetFontHeight(-19);
        FCkDuplicaArea[Box].Top := ObtemHeigth(32);
        //FCkDuplicaArea[Box].Left := ObtemWidth(4);
        //if FCkDuplicaArea[Box].Left > 4 then
          FCkDuplicaArea[Box].Left := ObtemWidth(4);
        //FCkDuplicaArea[Box].Align := alBottom;
        FCkDuplicaArea[Box].Caption := 'Duplica área';

(*
      object Panel8: TPanel
        Left = 41
        Top = 0
        Width = 469
        Height = 64
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
*)
      FPnArt2N[Box] := TPanel.Create(Form);
      FPnArt2N[Box].Parent := FPnArt1N[Box];
      FPnArt2N[Box].Name := 'PnArt2N' + IdTXT;
      FPnArt2N[Box].Width := ObtemWidth(122);
      FPnArt2N[Box].Align := alClient;
      FPnArt2N[Box].BevelOuter := bvNone;
      FPnArt2N[Box].Caption := '';
(*
        object DBEdit1: TDBEdit
          Align = alTop
          DataField = 'NO_PRD_TAM_COR'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
*)
        FDBEdArtNome[Box] := TDBEdit.Create(Form);
        FDBEdArtNome[Box].Parent := FPnArt2N[Box];
        FDBEdArtNome[Box].Name := 'EdDBArtNome' + IdTXT;
        FDBEdArtNome[Box].Align := alTop;
        FDBEdArtNome[Box].Font.Charset := DEFAULT_CHARSET;
        FDBEdArtNome[Box].Font.Color := clWindowText;
        FDBEdArtNome[Box].Font.Height := GetFontHeight(-19(*14**));
        FDBEdArtNome[Box].Font.Name := 'Tahoma';
        FDBEdArtNome[Box].Font.Style := [fsBold];
        FDBEdArtNome[Box].ParentFont := False;
        FDBEdArtNome[Box].TabOrder := 0;
        FDBEdArtNome[Box].DataField := 'NO_PRD_TAM_COR';
        FDBEdArtNome[Box].Text := '';
(*
        object Panel11: TPanel
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
*)
      FPnArt3N[Box] := TPanel.Create(Form);
      FPnArt3N[Box].Parent := FPnArt2N[Box];
      FPnArt3N[Box].Name := 'PnArt3N' + IdTXT;
      FPnArt3N[Box].Align := alClient;
      FPnArt3N[Box].BevelOuter := bvNone;
      FPnArt3N[Box].Caption := '';
(*
          object DBEdit8: TDBEdit
            Left = 0
            Top = 0
            Width = 100
            Height = 31
            Align = alLeft
            DataField = 'NO_STATUS'
            TabOrder = 1
            ExplicitHeight = 21
          end
*)
          FDBEdArtNoCli[Box] := TDBEdit.Create(Form);
          FDBEdArtNoCli[Box].Parent := FPnArt3N[Box];
          FDBEdArtNoCli[Box].Name := 'EdDBArtNoCli' + IdTXT;
          FDBEdArtNoCli[Box].Align := alLeft;
          FDBEdArtNoCli[Box].Width := ObtemWidth(100);
          FDBEdArtNoCli[Box].Font.Charset := DEFAULT_CHARSET;
          FDBEdArtNoCli[Box].Font.Color := clWindowText;
          FDBEdArtNoCli[Box].Font.Height := GetFontHeight(-19(*14**));
          FDBEdArtNoCli[Box].Font.Name := 'Tahoma';
          FDBEdArtNoCli[Box].Font.Style := [fsBold];
          FDBEdArtNoCli[Box].ParentFont := False;
          FDBEdArtNoCli[Box].TabOrder := 0;
          FDBEdArtNoCli[Box].DataField := 'NO_STATUS';
          FDBEdArtNoCli[Box].Text := '';
(*
          object DBEdit3: TDBEdit
            Left = 100
            Top = 0
            Width = 369
            Height = 31
            Align = alClient
            DataField = 'NO_CLISTAT'
            TabOrder = 0
            ExplicitHeight = 21
          end
        end
      end
    end
*)
          FDBEdArtNoSta[Box] := TDBEdit.Create(Form);
          FDBEdArtNoSta[Box].Parent := FPnArt3N[Box];
          FDBEdArtNoSta[Box].Name := 'EdDBArtNoSta' + IdTXT;
          FDBEdArtNoSta[Box].Align := alClient;
          FDBEdArtNoSta[Box].Font.Charset := DEFAULT_CHARSET;
          FDBEdArtNoSta[Box].Font.Color := clWindowText;
          FDBEdArtNoSta[Box].Font.Height := GetFontHeight(-19(*14**));
          FDBEdArtNoSta[Box].Font.Name := 'Tahoma';
          FDBEdArtNoSta[Box].Font.Style := [fsBold];
          FDBEdArtNoSta[Box].ParentFont := False;
          FDBEdArtNoSta[Box].TabOrder := 0;
          FDBEdArtNoSta[Box].DataField := 'NO_CLISTAT';
          FDBEdArtNoSta[Box].Text := '';
(*
    object Panel5: TPanel
      Top = 64
      Width = 298
      Height = 252
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
*)
    FPnBxTot[Box] := TPanel.Create(Form);
    FPnBxTot[Box].Parent := FPnBox[Box];
    FPnBxTot[Box].Name := 'PnBxTot' + IdTXT;
    FPnBxTot[Box].Align := alClient;
    FPnBxTot[Box].BevelOuter := bvNone;
    FPnBxTot[Box].Align := alLeft;
    FPnBxTot[Box].Width := ObtemWidth(298);
    FPnBxTot[Box].TabOrder := 0;
    FPnBxTot[Box].Caption := '';
(*
      object GroupBox1: TGroupBox
        Height = 80
        Align = alTop
        Caption = ' Do artigo na classe: '
        TabOrder = 0
*)
      FGBIMEI[Box] := TGroupBox.Create(Form);
      FGBIMEI[Box].Parent := FPnBxTot[Box];
      FGBIMEI[Box].Name := 'GBIMEI' + IdTXT;
      FGBIMEI[Box].Align := alTop;
      //FGBIMEI[Box].Height := ObtemHeigth(80) + 12;
      FGBIMEI[Box].Height := ObtemHeigth(56) + 16;
      FGBIMEI[Box].Caption := ' Do artigo na classe: ';
      FGBIMEI[Box].TabOrder := 0;
(*
        object Panel6: TPanel
          Height = 63
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
*)
        FPnIMEIImei[Box] := TPanel.Create(Form);
        FPnIMEIImei[Box].Parent := FGBIMEI[Box];
        FPnIMEIImei[Box].Name := 'PnIMEIImei' + IdTXT;
        FPnIMEIImei[Box].Align := alTop;
        FPnIMEIImei[Box].BevelOuter := bvNone;
        //FPnIMEIImei[Box].Height := ObtemHeigth(36);
        FPnIMEIImei[Box].Height := ObtemHeigth(28);
        FPnIMEIImei[Box].TabOrder := 0;
        FPnIMEIImei[Box].Caption := '';
(*
          object Label54: TLabel
            Left = 6
            Top = 32
            Width = 30
            Height = 13
            Caption = 'IME-I:'
            FocusControl = DBEdControle01
          end
*)
          FLaIMEIImei[Box] := TLabel.Create(Form);
          FLaIMEIImei[Box].Parent := FPnIMEIImei[Box];
          FLaIMEIImei[Box].Name := 'LaIMEIImei' + IdTXT;
          //FLaIMEIImei[Box].Width := ObtemWidth(41);
          FLaIMEIImei[Box].Align := alLeft;
          //FLaIMEIImei[Box].Top := ObtemHeigth(4);
          //FLaIMEIImei[Box].Font.Height := GetFontHeight(-11(*8*));
          FLaIMEIImei[Box].Font.Height := GetFontHeight(-19(*14*));
          FLaIMEIImei[Box].Caption := ' IME-I:';
          //FLaIMEIImei[Box].Alignment := taCenter;
(*
          object DBEdIMEI01: TDBEdit
            Left = 112
            Top = 28
            Width = 180
            Height = 21
            DataField = 'VMI_Dest'
            TabOrder = 1
          end
        end
      end
*)
          FDBEdIMEIImei[Box] := TDBEdit.Create(Form);
          FDBEdIMEIImei[Box].Parent := FPnIMEIImei[Box];
          FDBEdIMEIImei[Box].Name := 'DBEdIMEIImei' + IdTXT;
          FDBEdIMEIImei[Box].Align := alRight;
          FDBEdIMEIImei[Box].Font.Charset := DEFAULT_CHARSET;
          FDBEdIMEIImei[Box].Font.Color := clWindowText;
          FDBEdIMEIImei[Box].Font.Height := GetFontHeight(-21(*16*));
          FDBEdIMEIImei[Box].Font.Name := 'Tahoma';
          FDBEdIMEIImei[Box].Font.Style := [fsBold];
          FDBEdIMEIImei[Box].ParentFont := False;
          FDBEdIMEIImei[Box].TabOrder := 0;
          FDBEdIMEIImei[Box].DataField := 'VMI_Dest';
          FDBEdIMEIImei[Box].Width := ObtemWidth(120);
          FDBEdIMEIImei[Box].Text := '';
(*
        object Panel?: TPanel
          Height = 63
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
*)
        FPnIMEICtrl[Box] := TPanel.Create(Form);
        FPnIMEICtrl[Box].Parent := FGBIMEI[Box];
        FPnIMEICtrl[Box].Name := 'PnIMEICtrl' + IdTXT;
        FPnIMEICtrl[Box].Align := alTop;
        FPnIMEICtrl[Box].BevelOuter := bvNone;
        //FPnIMEICtrl[Box].Height := ObtemHeigth(36);
        FPnIMEICtrl[Box].Height := ObtemHeigth(28);
        FPnIMEICtrl[Box].TabOrder := 0;
        FPnIMEICtrl[Box].Caption := '';
    ///
(*
          object Label4: TLabel
            Height = 13
            Caption = 'Controle:'
            FocusControl = DBEdControle01
          end
*)
          FLaIMEICtrl[Box] := TLabel.Create(Form);
          FLaIMEICtrl[Box].Parent := FPnIMEICtrl[Box];
          FLaIMEICtrl[Box].Name := 'LaIMEICtrl' + IdTXT;
          //FLaIMEICtrl[Box].Width := ObtemWidth(41);
          FLaIMEICtrl[Box].Align := alLeft;
          //FLaIMEICtrl[Box].Top := ObtemHeigth(4);
          //FLaIMEICtrl[Box].Font.Height := GetFontHeight(-11(*8*));
          FLaIMEICtrl[Box].Font.Height := GetFontHeight(-21(*16*));
          FLaIMEICtrl[Box].Caption := ' Controle:';
          //FLaIMEICtrl[Box].Alignment := taCenter;

(*
          object DBEdControle01: TDBEdit
            Height = 21
            DataField = 'Controle'
            TabOrder = 0
          end
*)
          FDBEdIMEICtrl[Box] := TDBEdit.Create(Form);
          FDBEdIMEICtrl[Box].Parent := FPnIMEICtrl[Box];
          FDBEdIMEICtrl[Box].Name := 'DBEdIMEICtrl' + IdTXT;
          FDBEdIMEICtrl[Box].Align := alRight;
          FDBEdIMEICtrl[Box].Font.Charset := DEFAULT_CHARSET;
          FDBEdIMEICtrl[Box].Font.Color := clWindowText;
          FDBEdIMEICtrl[Box].Font.Height := GetFontHeight(-19(*14*));
          FDBEdIMEICtrl[Box].Font.Name := 'Tahoma';
          FDBEdIMEICtrl[Box].Font.Style := [fsBold];
          FDBEdIMEICtrl[Box].ParentFont := False;
          FDBEdIMEICtrl[Box].TabOrder := 0;
          FDBEdIMEICtrl[Box].DataField := 'Controle';
          FDBEdIMEICtrl[Box].Width := ObtemWidth(120);
          FDBEdIMEICtrl[Box].Text := '';
          //
          //
          //
          //
(*
      object GroupBox2: TGroupBox
        Top = 80
        Height = 172
        Align = alClient
        Caption = ' Do Pallet: '
        TabOrder = 1
*)
      FGBPallet[Box] := TGroupBox.Create(Form);
      FGBPallet[Box].Parent := FPnBxTot[Box];
      FGBPallet[Box].Name := 'GBPallet' + IdTXT;
      FGBPallet[Box].Align := alClient;
      //FGBPallet[Box].Height := ObtemHeigth(172);
      FGBPallet[Box].Caption := ' Do Pallet: ';
      FGBPallet[Box].TabOrder := 1;
(*
        object Panel16: TPanel
          Left = 2
          Top = 15
          Width = 294
          Height = 155
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
*)
        FPnPalletMedia[Box] := TPanel.Create(Form);
        FPnPalletMedia[Box].Parent := FGBPallet[Box];
        FPnPalletMedia[Box].Name := 'PnPalletMedia' + IdTXT;
        FPnPalletMedia[Box].Align := alTop;
        FPnPalletMedia[Box].BevelOuter := bvNone;
        //FPnPalletMedia[Box].Height := ObtemHeigth(36);
        FPnPalletMedia[Box].Height := ObtemHeigth(28);
        FPnPalletMedia[Box].TabOrder := 0;
        FPnPalletMedia[Box].Caption := '';
(*
          object Label73: TLabel
            Height = 13
            Caption = 'm'#178' / Pe'#231'a:'
          end
*)
          FLaPalletMedia[Box] := TLabel.Create(Form);
          FLaPalletMedia[Box].Parent := FPnPalletMedia[Box];
          FLaPalletMedia[Box].Name := 'LaPalletMedia' + IdTXT;
          //FLaPalletMedia[Box].Width := ObtemWidth(41);
          FLaPalletMedia[Box].Align := alLeft;
          //FLaPalletMedia[Box].Top := ObtemHeigth(4);
          //FLaPalletMedia[Box].Font.Height := GetFontHeight(-11(*8*));
          FLaPalletMedia[Box].Font.Height := GetFontHeight(-21(*16*));
          FLaPalletMedia[Box].Caption := ' m² / Peça:';
          //FLaPalletMedia[Box].Alignment := taCenter;
(*
          object EdMedia01: TdmkEdit
            Width = 180
            Height = 27
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
*)
          FEdPalletMedia[Box] := TdmkEdit.Create(Form);
          FEdPalletMedia[Box].Parent := FPnPalletMedia[Box];
          FEdPalletMedia[Box].Name := 'DBEdPalletMedia' + IdTXT;
          FEdPalletMedia[Box].Align := alRight;
          FEdPalletMedia[Box].Alignment := taRightJustify;
          FEdPalletMedia[Box].Font.Charset := DEFAULT_CHARSET;
          FEdPalletMedia[Box].Font.Color := clWindowText;
          FEdPalletMedia[Box].Font.Height := GetFontHeight(-19); //-19(*14*));
          FEdPalletMedia[Box].Font.Name := 'Tahoma';
          FEdPalletMedia[Box].Font.Style := [fsBold];
          FEdPalletMedia[Box].ParentFont := False;
          FEdPalletMedia[Box].ReadOnly := True;
          FEdPalletMedia[Box].TabOrder := 0;
          FEdPalletMedia[Box].Text := '0.00';
          FEdPalletMedia[Box].FormatType := dmktfDouble;
          FEdPalletMedia[Box].MskType := fmtNone;
          FEdPalletMedia[Box].DecimalSize := 2;
          FEdPalletMedia[Box].LeftZeros := 0;
          FEdPalletMedia[Box].NoEnterToTab := False;
          FEdPalletMedia[Box].NoForceUppercase := False;
          FEdPalletMedia[Box].ForceNextYear := False;
          FEdPalletMedia[Box].DataFormat := dmkdfShort;
          FEdPalletMedia[Box].HoraFormat := dmkhfShort;
          FEdPalletMedia[Box].Texto := '0,00';
          FEdPalletMedia[Box].UpdType := utYes;
          FEdPalletMedia[Box].Obrigatorio := False;
          FEdPalletMedia[Box].PermiteNulo := False;
          FEdPalletMedia[Box].ValueVariant := 0;
          FEdPalletMedia[Box].ValWarn := False;
          FEdPalletMedia[Box].Width := ObtemWidth(120);
(*
        object Panel16: TPanel
          Left = 2
          Top = 15
          Width = 294
          Height = 155
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
*)
        FPnPalletArea[Box] := TPanel.Create(Form);
        FPnPalletArea[Box].Parent := FGBPallet[Box];
        FPnPalletArea[Box].Name := 'PnPalletArea' + IdTXT;
        FPnPalletArea[Box].Align := alTop;
        FPnPalletArea[Box].BevelOuter := bvNone;
        //FPnPalletArea[Box].Height := ObtemHeigth(36);
        FPnPalletArea[Box].Height := ObtemHeigth(28);
        FPnPalletArea[Box].TabOrder := 0;
        FPnPalletArea[Box].Caption := '';
(*
          object Label53: TLabel
            Height = 13
            Caption = #193'rea:'
          end
*)
          FLaPalletArea[Box] := TLabel.Create(Form);
          FLaPalletArea[Box].Parent := FPnPalletArea[Box];
          FLaPalletArea[Box].Name := 'LaPalletArea' + IdTXT;
          //FLaPalletArea[Box].Width := ObtemWidth(41);
          FLaPalletArea[Box].Align := alLeft;
          //FLaPalletArea[Box].Top := ObtemHeigth(4);
          //FLaPalletArea[Box].Font.Height := GetFontHeight(-11(*8*));
          FLaPalletArea[Box].Font.Height := GetFontHeight(-21(*16*));
          FLaPalletArea[Box].Caption := ' Área:';
          //FLaPalletArea[Box].Alignment := taCenter;
(*
          object EdArea01: TdmkEdit
            Height = 27
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
*)
          FEdPalletArea[Box] := TdmkEdit.Create(Form);
          FEdPalletArea[Box].Parent := FPnPalletArea[Box];
          FEdPalletArea[Box].Name := 'DBEdPalletArea' + IdTXT;
          FEdPalletArea[Box].Align := alRight;
          FEdPalletArea[Box].Alignment := taRightJustify;
          FEdPalletArea[Box].Font.Charset := DEFAULT_CHARSET;
          FEdPalletArea[Box].Font.Color := clWindowText;
          FEdPalletArea[Box].Font.Height := GetFontHeight(-19(*14*));
          FEdPalletArea[Box].Font.Name := 'Tahoma';
          FEdPalletArea[Box].Font.Style := [fsBold];
          FEdPalletArea[Box].ParentFont := False;
          FEdPalletArea[Box].ReadOnly := True;
          FEdPalletArea[Box].TabOrder := 0;
          FEdPalletArea[Box].Text := '0.00';
          FEdPalletArea[Box].FormatType := dmktfDouble;
          FEdPalletArea[Box].MskType := fmtNone;
          FEdPalletArea[Box].DecimalSize := 2;
          FEdPalletArea[Box].LeftZeros := 0;
          FEdPalletArea[Box].NoEnterToTab := False;
          FEdPalletArea[Box].NoForceUppercase := False;
          FEdPalletArea[Box].ForceNextYear := False;
          FEdPalletArea[Box].DataFormat := dmkdfShort;
          FEdPalletArea[Box].HoraFormat := dmkhfShort;
          FEdPalletArea[Box].Texto := '0,00';
          FEdPalletArea[Box].UpdType := utYes;
          FEdPalletArea[Box].Obrigatorio := False;
          FEdPalletArea[Box].PermiteNulo := False;
          FEdPalletArea[Box].ValueVariant := 0;
          FEdPalletArea[Box].ValWarn := False;
          FEdPalletArea[Box].Width := ObtemWidth(120);
(*
        object Panel16: TPanel
          Left = 2
          Top = 15
          Width = 294
          Height = 155
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
*)
        FPnPalletPecas[Box] := TPanel.Create(Form);
        FPnPalletPecas[Box].Parent := FGBPallet[Box];
        FPnPalletPecas[Box].Name := 'PnPalletPecas' + IdTXT;
        FPnPalletPecas[Box].Align := alTop;
        FPnPalletPecas[Box].BevelOuter := bvNone;
        //FPnPalletPecas[Box].Height := ObtemHeigth(36);
        FPnPalletPecas[Box].Height := ObtemHeigth(28);
        FPnPalletPecas[Box].TabOrder := 0;
        FPnPalletPecas[Box].Caption := '';
(*
          object Label52: TLabel
            Height = 13
            Caption = 'Pe'#231'as:'
          end
*)
          FLaPalletPecas[Box] := TLabel.Create(Form);
          FLaPalletPecas[Box].Parent := FPnPalletPecas[Box];
          FLaPalletPecas[Box].Name := 'LaPalletPecas' + IdTXT;
          //FLaPalletPecas[Box].Width := ObtemWidth(41);
          FLaPalletPecas[Box].Align := alLeft;
          //FLaPalletPecas[Box].Top := ObtemHeigth(4);
          //FLaPalletPecas[Box].Font.Height := GetFontHeight(-11(*8*));
          FLaPalletPecas[Box].Font.Height := GetFontHeight(-21(*16*));
          FLaPalletPecas[Box].Caption := ' Peças:';
          //FLaPalletPecas[Box].Alignment := taCenter;
(*
          object EdPecas01: TdmkEdit
            Height = 27
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
*)
          FEdPalletPecas[Box] := TdmkEdit.Create(Form);
          FEdPalletPecas[Box].Parent := FPnPalletPecas[Box];
          FEdPalletPecas[Box].Name := 'DBEdPalletPecas' + IdTXT;
          FEdPalletPecas[Box].Align := alRight;
          FEdPalletPecas[Box].Alignment := taRightJustify;
          FEdPalletPecas[Box].Font.Charset := DEFAULT_CHARSET;
          FEdPalletPecas[Box].Font.Color := clWindowText;
          FEdPalletPecas[Box].Font.Height := GetFontHeight(-19); //-19(*14*));
          FEdPalletPecas[Box].Font.Name := 'Tahoma';
          FEdPalletPecas[Box].Font.Style := [fsBold];
          FEdPalletPecas[Box].ParentFont := False;
          FEdPalletPecas[Box].ReadOnly := True;
          FEdPalletPecas[Box].TabOrder := 0;
          FEdPalletPecas[Box].Text := '0.00';
          FEdPalletPecas[Box].FormatType := dmktfDouble;
          FEdPalletPecas[Box].MskType := fmtNone;
          FEdPalletPecas[Box].DecimalSize := 1;
          FEdPalletPecas[Box].LeftZeros := 0;
          FEdPalletPecas[Box].NoEnterToTab := False;
          FEdPalletPecas[Box].NoForceUppercase := False;
          FEdPalletPecas[Box].ForceNextYear := False;
          FEdPalletPecas[Box].DataFormat := dmkdfShort;
          FEdPalletPecas[Box].HoraFormat := dmkhfShort;
          FEdPalletPecas[Box].Texto := '0,00';
          FEdPalletPecas[Box].UpdType := utYes;
          FEdPalletPecas[Box].Obrigatorio := False;
          FEdPalletPecas[Box].PermiteNulo := False;
          FEdPalletPecas[Box].ValueVariant := 0;
          FEdPalletPecas[Box].ValWarn := False;
          FEdPalletPecas[Box].Width := ObtemWidth(120);
(*
        object Panel16: TPanel
          Left = 2
          Top = 15
          Width = 294
          Height = 155
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
*)
        FPnPalletID[Box] := TPanel.Create(Form);
        FPnPalletID[Box].Parent := FGBPallet[Box];
        FPnPalletID[Box].Name := 'PnPalletID' + IdTXT;
        FPnPalletID[Box].Align := alTop;
        FPnPalletID[Box].BevelOuter := bvNone;
        //FPnPalletID[Box].Height := ObtemHeigth(36);
        FPnPalletID[Box].Height := ObtemHeigth(28);
        FPnPalletID[Box].TabOrder := 0;
        FPnPalletID[Box].Caption := '';
(*
          object Label55: TLabel
            Left = 4
            Top = 8
            Width = 15
            Height = 13
            Caption = 'ID:'
          end
*)
          FLaPalletID[Box] := TLabel.Create(Form);
          FLaPalletID[Box].Parent := FPnPalletID[Box];
          FLaPalletID[Box].Name := 'LaPalletID' + IdTXT;
          //FLaPalletID[Box].Width := ObtemWidth(41);
          FLaPalletID[Box].Align := alLeft;
          //FLaPalletID[Box].Top := ObtemHeigth(4);
          //FLaPalletID[Box].Font.Height := GetFontHeight(-11(*8*));
          FLaPalletID[Box].Font.Height := GetFontHeight(-21(*16*));
          FLaPalletID[Box].Caption := ' ID:';
          //FLaPalletID[Box].Alignment := taCenter;
(*
          object DBEdPallet01: TDBEdit
            Height = 27
            DataField = 'Codigo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
          end
*)
          FDBEdPalletID[Box] := TDBEdit.Create(Form);
          FDBEdPalletID[Box].Parent := FPnPalletID[Box];
          FDBEdPalletID[Box].Name := 'EdDBPalletID' + IdTXT;
          FDBEdPalletID[Box].Align := alRight;
          FDBEdPalletID[Box].Font.Charset := DEFAULT_CHARSET;
          FDBEdPalletID[Box].Font.Color := clPurple;
          FDBEdPalletID[Box].Font.Height := GetFontHeight(-19(*14*));
          FDBEdPalletID[Box].Font.Name := 'Tahoma';
          FDBEdPalletID[Box].Font.Style := [fsBold];
          FDBEdPalletID[Box].ParentFont := False;
          FDBEdPalletID[Box].TabOrder := 0;
          FDBEdPalletID[Box].DataField := 'Codigo';
          FDBEdPalletID[Box].Width := ObtemWidth(120);
          FDBEdPalletID[Box].Text := '';
(*
        end
      end
    end
*)
(*
    object PnIntMei01: TPanel
      Height = 28
      Align = alBottom
      Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
      UseDockManager = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      Visible = False
    end
*)
    // 2016-02-25
    Painel := TPanel.Create(Form);
    Painel.Parent := FPnBox[Box];
    Painel.Name := 'PnAAAA' + IdTXT;
    Painel.Align := alClient;
    Painel.BevelOuter := bvNone;
    //Painel.Height := ObtemHeigth(36);
    Painel.TabOrder := 0;
    Painel.Caption := '';
    // Fim 2016-02-25

    FPnIntMei[Box] := TPanel.Create(Form);
    //FPnIntMei[Box].Parent := FPnBox[Box];
    FPnIntMei[Box].Parent := Painel;
    FPnIntMei[Box].Name := 'PnIntMei' + IdTXT;
    FPnIntMei[Box].Align := alBottom;
    FPnIntMei[Box].BevelOuter := bvNone;
    //FPnIntMei[Box].Font.Height := GetFontHeight(-19(*14*));
    FPnIntMei[Box].Font.Height := GetFontHeight(-15(*11*));
    //FPnIntMei[Box].Caption := ' ATENÇÃO!!! Couro deve ser cortado!!! ';
    FPnIntMei[Box].Caption := ' ATENÇÃO! CORTAR! ';
    FPnIntMei[Box].UseDockManager := False;
    FPnIntMei[Box].Font.Charset := DEFAULT_CHARSET;
    FPnIntMei[Box].Font.Color := clRed;
    //FPnIntMei[Box].Height := ObtemHeigth(28);
    FPnIntMei[Box].Height := ObtemHeigth(16);
    FPnIntMei[Box].Font.Name := 'Tahoma';
    FPnIntMei[Box].Font.Style := [fsBold];
    FPnIntMei[Box].ParentFont := False;
    FPnIntMei[Box].TabOrder := 2;
    FPnIntMei[Box].Visible := False;
(*
    object SGItens01: TStringGrid
      Align = alClient
      TabOrder = 3
    end
  end
end
*)
    FSGItens[Box] := TStringGrid.Create(Form);
    //FSGItens[Box].Parent := FPnBox[Box];
    FSGItens[Box].Parent := Painel;
    FSGItens[Box].Name := 'SGItens' + IdTXT;
    FSGItens[Box].Align := alClient;
    FSGItens[Box].DefaultRowHeight := ObtemWidth(24);
    FSGItens[Box].Font.Height := GetFontHeight(-19(*14*));
    FSGItens[Box].Font.Charset := DEFAULT_CHARSET;
    //FSGItens[Box].Font.Color := clRed;
    FSGItens[Box].Height := ObtemHeigth(28);
    //FSGItens[Box].Height := ObtemHeigth(24);
    FSGItens[Box].Font.Name := 'Tahoma';
    //FSGItens[Box].Font.Style := [fsBold];
    FSGItens[Box].ParentFont := False;
    FSGItens[Box].TabOrder := 3;
    FSGItens[Box].ColCount := 2;
    FSGItens[Box].FixedCols := 1;
    FSGItens[Box].Cells[0, 0]  := 'Item';
    FSGItens[Box].Cells[1, 0]  := 'Área m²';
    //
    FSGItens[Box].ColWidths[0] := ObtemWidth(46);
    FSGItens[Box].ColWidths[1] := ObtemWidth(78);
    //
    FSGItens[Box].PopupMenu := FPMItens[Box];
end;

function TUnVS_CRC_PF.ObtemProximaFichaRMP(const Empresa: Integer; const EdSerieFch:
  TdmkEditCB; var Ficha: Integer; SerieDefinida: Integer): Boolean;
var
  Qry: TmySQLQuery;
  SerieFch: Integer;
begin
  Result := False;
  if SerieDefinida <> 0 then
    SerieFch := SerieDefinida
  else
    SerieFch := EdSerieFch.ValueVariant;
  if MyObjects.FIC(SerieFch = 0, EdSerieFch, 'Defina a série primeiro!') then
    Exit;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(Ficha) Ficha ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND SerieFch=' + Geral.FF0(SerieFch),
    '']);
    if Qry.FieldByName('Ficha').AsInteger = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT MAX(Ficha) Ficha ',
      'FROM ' + CO_TAB_VMB,
      'WHERE Empresa=' + Geral.FF0(Empresa),
      'AND SerieFch=' + Geral.FF0(SerieFch),
      '']);
    end;
    //
    Ficha := Qry.FieldByName('Ficha').AsInteger + 1;
    Result := True;
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.ObtemSaldoFuturoIMEI(const VMIOri: Integer;
  const VoltaPecas, VoltaPesoKg, VoltaAreaM2: Double; var FuturoPecas,
  FuturoPesoKg, FuturoAreaM2, FuturoValorT: Double): Boolean;
var
  Qry: TmySQLQuery;
  ValorT, Pecas, PesoKg, AreaM2: Double;
begin
  Result       := False;
  FuturoPecas  := 0;
  FuturoPesoKg := 0;
  FuturoAreaM2 := 0;
  ValorT       := 0;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SdoVrtPeca, SdoVrtPeso, SdoVrtArM2, ',
    'Pecas, PesoKg, AreaM2, ValorT ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE Controle=' + Geral.FF0(VMIOri),
    '']);
    //
    ValorT       := Qry.FieldByName('ValorT').AsFloat;
    Pecas        := Qry.FieldByName('Pecas').AsFloat;
    PesoKg       := Qry.FieldByName('PesoKg').AsFloat;
    AreaM2       := Qry.FieldByName('AreaM2').AsFloat;
    //
    FuturoPecas  := VoltaPecas  + Qry.FieldByName('SdoVrtPeca').AsFloat;
    FuturoPesoKg := VoltaPesoKg + Qry.FieldByName('SdoVrtPeso').AsFloat;
    FuturoAreaM2 := VoltaAreaM2 + Qry.FieldByName('SdoVrtArM2').AsFloat;
    FuturoValorT := 0;
    //
    if (FuturoAreaM2 > 0) and (AreaM2 > 0) then
      FuturoValorT := ValorT / AreaM2 * FuturoAreaM2
    else
    if (FuturoPesoKg > 0) and (PesoKg > 0) then
      FuturoValorT := ValorT / PesoKg * FuturoPesoKg
    else
    if (FuturoPecas > 0) and (Pecas > 0) then
      FuturoValorT := ValorT / Pecas * FuturoPecas;
    //
    Result := True;
  except
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.OperacaoEhDivisao(MovimID, MovimCod: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  if TEstqMovimID(MovimID) = TEstqMovimID.emidEmOperacao then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ope.RedefnFrmt ',
      'FROM operacoes ope',
      'LEFT JOIN vsopecab cab ON ope.Codigo=cab.Operacoes',
      'WHERE cab.MovimCod=' + Geral.FF0(MovimCod),
      '']);
      Result := Qry.FieldByName('RedefnFrmt').AsInteger =
        Integer(TVSRedefnFrmt.vsrdfnfrmtDivRacha); // 2
    finally
      Qry.Free;
    end;
  end;
end;

procedure TUnVS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(const DataHora,
  DtCorrApo: TDateTime; TPData: TdmkEditDateTimePicker; EdHora: TdmkEdit);
var
  DtHr: TDateTime;
begin
  if DtCorrApo > 2 then
    DtHr := DtCorrApo
  else
    DtHr := DataHora;
  //
  TPData.Date               := Int(DtHr);
  EdHora.ValueVariant       := DtHr;
end;

procedure TUnVS_CRC_PF.DadosDaFaixaDaMediaM2DoCouro(const FaixaMediaM2: Double;
  var CorFundo, CorTexto: TColor; var Texto: String);
begin
  case Trunc(FaixaMediaM2) of
    0:
    begin
      CorTexto := clRed;
      CorFundo := clLaranja;
      Texto    := 'ABAIXO';
    end;
    1:
    begin
      CorTexto := clGreen;
      CorFundo := clWhite;
      Texto    := 'DENTRO';
    end;
    2:
    begin
      CorTexto := clPurple;
      CorFundo := $00E4E4E4;
      Texto    := 'ACIMA';
    end;
    else begin
      CorTexto := clFuchsia;
      CorFundo := clBlack;
      Texto    := '? ? ?';
    end;
  end;
end;

procedure TUnVS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(const DataHora,
  DtCorrApo: TDateTime; var _DataHora: TDateTime);
begin
  if DtCorrApo > 2 then
    _DataHora := DtCorrApo
  else
    _DataHora := DataHora;
end;

function TUnVS_CRC_PF.DefineDatasVMI(const _Data_Hora_: String; var DataHora,
  DtCorrApo: String): Boolean;
var
  Agora, Data, DtCA: TDateTime;
  (*GerLibEFD*)MovimXX, PeriodoData, PeriodoNow, Ano, Mes: Integer;
begin
  Result    := False;
  DtCorrApo := '0000-00-00 00:00:00';
  DataHora  := _Data_Hora_;
  {$IFDEF sAllVS}
  Data      := Geral.ValidaDataHoraSQL(_Data_Hora_);
  //
  if VS_PF.ImpedePeloBalanco(Data, True, True) then
    Exit;
  //
(*
////////////  Ideia 1  /////////////////////////////////////////////////////////
  if ForcaDtCorrApo > 1 then
    DtCorrApo := Geral.FDT(ForcaDtCorrApo, 1)
  else
  begin
    // Se for do mes corrente ou futuro deixa em branco,
    // caso contrario...
    //Agora := Now(); // Pegar do servidor fica lento em multi insercoes?
    Agora := DModG.ObtemAgora();
    PeriodoData := Geral.Periodo2000(Data);
    PeriodoNow  := Geral.Periodo2000(Agora);
    if PeriodoData < PeriodoNow then
    begin
      //GerLibEFD := EfdIcmsIpi_PF.SPEDEFDEnce_Periodo('GerLibEFD');
      //if PeriodoData < GerLibEFD then
      MovimXX := EfdIcmsIpi_PF.SPEDEFDEnce_Periodo('MovimXX');
      if PeriodoData <= MovimXX then
      begin
        if PeriodoNow - PeriodoData <= 2 then // So pode 2 inventarios pelo sped!
          DtCorrApo := Geral.FDT(Agora, 1)
        else
        begin
          if MovimXX - PeriodoData >= 2 then // So pode 2 inventarios pelo sped!
          begin
            Geral.MB_Aviso('Período de correção de apontamento já encerrado!');
            Exit;
          end;
////////// ultimo dia do proximo periodo aberto ////////////////////////////////
          Geral.AnoEMesDoPeriodo(MovimXX + 2, Ano, Mes);                      //
          DtCA := EncodeDate(Ano, Mes, 1) - 1;                                //
          DtCorrApo := Geral.FDT(DtCA, 1);                                    //
////////////////////////////////////////////////////////////////////////////////
        end;
      end;
    end;
  end;
*)

////////////  Ideia 2  /////////////////////////////////////////////////////////
  //if ForcaDtCorrApo > 1 then
    //DtCorrApo := Geral.FDT(ForcaDtCorrApo, 1)
  //else
  //begin
    // Se for do mes corrente ou futuro deixa em branco,
    // caso contrario...
    //Agora := Now(); // Pegar do servidor fica lento em multi insercoes?
    Agora       := DModG.ObtemAgora();
    PeriodoData := Geral.Periodo2000(Data);
    PeriodoNow  := Geral.Periodo2000(Agora);
    //
    if PeriodoData < PeriodoNow then
    begin
      //GerLibEFD := EfdIcmsIpi_PF.SPEDEFDEnce_Periodo('GerLibEFD');
      //if PeriodoData < GerLibEFD then
      MovimXX := EfdIcmsIpi_PF.SPEDEFDEnce_Periodo('MovimXX');
      //
      if MovimXX < -23900 then
      begin
        if VAR_AVISOS_SPEDEFDEnce_Periodo < 3 then
        begin
          VAR_AVISOS_SPEDEFDEnce_Periodo := VAR_AVISOS_SPEDEFDEnce_Periodo + 1;
          //
          Geral.MB_Aviso('Não há período de encerramento até o momento!' +
            sLineBreak +
            'O(s) lançamento(s) NÃO será(ão) considerado(s) como apontamento(s)!');
        end;
        Result := True;
        Exit;
      end;
      if PeriodoData <= MovimXX then
      begin
        if MovimXX - PeriodoData >= 2 then // So pode 2 inventarios pelo sped!
        begin
          Geral.MB_Aviso('Período de correção de apontamento já encerrado!');
          Exit;
        end;
        if not VS_PF.InverteDatas(MovimXX, Data, Agora, DataHora, DtCorrApo) then
          Exit
        else
          Result := True;
      end else
        Result := True;
    end else
      Result := True;
  //end;
  {$ELSE}
    Result := True;
  {$ENDIF}
end;

function TUnVS_CRC_PF.DefineIDs_Str(DBGridZTO: TdmkDBGridZTO;
  Query: TmySQLQuery; Campo: String): String;
var
  I: Integer;
begin
  Result := '';
  if (DBGridZTO.SelectedRows.Count > 0) and
  (DBGridZTO.SelectedRows.Count < Query.RecordCount) then
  begin
    for I := 0 to DBGridZTO.SelectedRows.Count - 1 do
    begin
      //Query.GotoBookmark(pointer(DBGridZTO.SelectedRows.Items[I]));
      Query.GotoBookmark(DBGridZTO.SelectedRows.Items[I]);
      //
      if Result <> '' then
        Result := Result + ',';
      Result := Result + Geral.FF0(Query.FieldByName(Campo).AsInteger);
    end;
  end;
end;

function TUnVS_CRC_PF.DefineSiglaVS_Frn(Fornece: Integer): String;
var
  Qry: TmySQLQuery;
  SiglaVS: String;
  SQLType: TSQLType;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    SiglaVS := '';
    UnDmkDAC_PF.AbreMySQLQUery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, SiglaVS ',
    'FROM vsentimp ',
    'WHERE Codigo=' + Geral.FF0(Fornece),
    '']);
    if Qry.RecordCount > 0 then
    begin
      SiglaVS := Qry.FieldByName('SiglaVS').AsString;
      if Trim(SiglaVS) <> '' then
      begin
        Result := SiglaVS;
        Exit;
      end else
        SQLType := stUpd;
    end else
    begin
      SQLType := stIns;
    end;
    if Trim(SiglaVS) = '' then
    begin
    UnDmkDAC_PF.AbreMySQLQUery0(Qry, Dmod.MyDB, [
      'SELECT IF(Tipo=0, RazaoSocial, Nome) NO_ENT ',
      'FROM entidades ',
      'WHERE Codigo=' + Geral.FF0(Fornece),
      '']);
      SiglaVS := Qry.FieldByName('NO_ENT').AsString;
      InputQuery('Informe a Sigla do Fornecedor de Matéria Prima',
      'Sigla (máx 10 caracteres):', SiglaVS);
    end;
    //
    if Trim(SiglaVS) = '' then
      SiglaVS := 'F' + Geral.FF0(Fornece);
    //
    SiglaVS := Copy(SiglaVS, 1, 10);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsentimp', False, [
    'SiglaVS'
    ], ['Codigo'], [
    SiglaVS
    ], [Fornece], True) then
      Result := SiglaVS;
  finally
    Qry.Free;
  end;
end;

function TUnVS_CRC_PF.DefineSiglaVS_NFe(NFeSer, NFeNum: Integer): String;
begin
  Result := Geral.FF0(NFeSer) + '_' + Geral.FF0(NFeNum);
end;

procedure TUnVS_CRC_PF.DefinirAscendenciasDeIMEIsDeMovimCod(IMEIAtu,
  MovimCod: Integer; MovimNivSrc, MovimNivDst: TEstqMovimNiv);
var
  Terceiro, Misturou, SerieFch, Ficha, Controle: Integer;
  Marca: String;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    Misturou := 0;
    // Ver se eh apenas um fornecedor
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT vmi.Terceiro ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNivSrc)),
    '']);
    if Qry.RecordCount = 1 then
      Terceiro := Qry.FieldByName('Terceiro').AsInteger
    else
    begin
      Terceiro := 0;
      Misturou := 1;
    end;
    // Ver se eh apenas uma Ficha
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT vmi.SerieFch, vmi.Ficha ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNivSrc)),
    '']);
    if Qry.RecordCount = 1 then
    begin
      SerieFch := Qry.FieldByName('SerieFch').AsInteger;
      Ficha    := Qry.FieldByName('Ficha').AsInteger;
    end else
    begin
      SerieFch := 0;
      Ficha    := 0;
      Misturou := 1;
    end;
    // Ver se eh apenas uma Marca
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT vmi.Marca ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNivSrc)),
    '']);
    if Qry.RecordCount = 1 then
      Marca    := Qry.FieldByName('Marca').AsString
    else
      Marca    := '';
    //
    //
    // Artigo em operacao, processo, etc..
    Controle := IMEIAtu;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, '' + CO_UPD_TAB_VMI + '', False, [
    'Terceiro', 'SerieFch', 'Ficha',
    'Marca'], [
    'Controle'], [
    Terceiro, SerieFch, Ficha,
    Marca], [
    Controle], True);
    // Artigos Destinos
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT vmi.Controle ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.MovimCod=' + Geral.FF0(MovimCod),
    'AND vmi.MovimNiv=' + Geral.FF0(Integer(MovimNivDst)),
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      Controle := Qry.FieldByName('Controle').AsInteger;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, '' + CO_UPD_TAB_VMI + '', False, [
      'Terceiro', 'SerieFch', 'Ficha',
      'Marca'], [
      'Controle'], [
      Terceiro, SerieFch, Ficha,
      Marca], [
      Controle], True);
      //
      Qry.Next;
    end;
      //
  finally
    Qry.Free;
  end;
end;

end.
