unit VSVmcObs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBLookupComboBox, dmkEditCB, mySQLDbTables, dmkCheckBox, UnDmkProcFunc;

type
  TFmVSVmcObs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrVSVmcWrn: TmySQLQuery;
    QrVSVmcWrnCodigo: TIntegerField;
    DsVSVmcWrn: TDataSource;
    LaPrompt: TLabel;
    EdVSVmcWrn: TdmkEditCB;
    CBVSVmcWrn: TdmkDBLookupComboBox;
    QrVSVmcWrnNome: TWideStringField;
    EdVSVmcSeq: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdVSVmcObs: TdmkEdit;
    CkVSVmcSta: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FTabela, FCampo: String;
    FMovimID, FMovimCod, FNFe: Integer;
    //FMovimID: TEstqMovimID;
    FQuery: TmySQLQuery;
  end;

  var
  FmVSVmcObs: TFmVSVmcObs;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmVSVmcObs.BtOKClick(Sender: TObject);
var
  VSVmcWrn, VSVmcSta: Integer;
  VSVmcSeq, VSVmcObs: String;
  //EstqMovimID: TEstqMovimID;
begin
  //EstqMovimID := TEstqMovimID(Trunc(FMovimID));
  VSVmcWrn    := EdVSVmcWrn.ValueVariant;
  VSVmcSeq    := EdVSVmcSeq.Text;
  VSVmcObs    := EdVSVmcObs.Text;
  VSVmcSta    := Geral.BoolToInt(CkVSVmcSta.Checked);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, FTabela, False, [
  'VSVmcWrn', 'VSVmcSeq', 'VSVmcObs', 'VSVmcSta'], [
  'MovimCod', FCampo], [
  VSVmcWrn, VSVmcSeq, VSVmcObs, VSVmcSta], [
  FMovimCod, FNFe], True) then
  begin
    UnDmkDAC_PF.AbreQuery(FQuery, Dmod.MyDB);
    FQuery.Locate('MovimCod', FMovimCod, []);
    //
    Close;
  end;
end;

procedure TFmVSVmcObs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSVmcObs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSVmcObs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrVSVmcWrn, Dmod.MyDB);
end;

procedure TFmVSVmcObs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
