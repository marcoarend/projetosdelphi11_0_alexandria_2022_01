object FmVSVmcObs: TFmVSVmcObs
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-188 :: Informa'#231#245'es do Movimento (Status)'
  ClientHeight = 331
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 431
        Height = 32
        Caption = 'Informa'#231#245'es do Movimento (Status)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 431
        Height = 32
        Caption = 'Informa'#231#245'es do Movimento (Status)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 431
        Height = 32
        Caption = 'Informa'#231#245'es do Movimento (Status)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 169
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 123
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 169
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 123
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 169
        Align = alClient
        TabOrder = 0
        ExplicitHeight = 123
        object LaPrompt: TLabel
          Left = 20
          Top = 16
          Width = 33
          Height = 13
          Caption = 'Status:'
        end
        object Label1: TLabel
          Left = 528
          Top = 16
          Width = 169
          Height = 13
          Caption = 'Ano/m'#234's/dia/p'#225'g/etc. (indexa'#231#227'o):'
        end
        object Label2: TLabel
          Left = 20
          Top = 56
          Width = 109
          Height = 13
          Caption = 'Hist'#243'rico (observa'#231#227'o):'
        end
        object EdVSVmcWrn: TdmkEditCB
          Left = 20
          Top = 32
          Width = 65
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBVSVmcWrn
          IgnoraDBLookupComboBox = False
        end
        object CBVSVmcWrn: TdmkDBLookupComboBox
          Left = 88
          Top = 32
          Width = 437
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsVSVmcWrn
          TabOrder = 1
          dmkEditCB = EdVSVmcWrn
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdVSVmcSeq: TdmkEdit
          Left = 528
          Top = 32
          Width = 233
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdVSVmcObs: TdmkEdit
          Left = 20
          Top = 72
          Width = 741
          Height = 21
          MaxLength = 60
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CkVSVmcSta: TdmkCheckBox
          Left = 24
          Top = 104
          Width = 285
          Height = 17
          Caption = 'Ignorar status em pesquisa espec'#237'fica de pend'#234'ncias.'
          TabOrder = 4
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 217
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 171
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 261
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 215
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 12
    Top = 65535
  end
  object QrVSVmcWrn: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM vsvmcwrn'
      'ORDER BY Nome')
    Left = 184
    Top = 2
    object QrVSVmcWrnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSVmcWrnNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSVmcWrn: TDataSource
    DataSet = QrVSVmcWrn
    Left = 184
    Top = 50
  end
end
