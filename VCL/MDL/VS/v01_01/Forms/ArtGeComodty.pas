unit ArtGeComodty;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums,
  dmkCheckGroup;

type
  TFmArtGeComodty = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrArtGeComodty: TMySQLQuery;
    QrArtGeComodtyCodigo: TIntegerField;
    QrArtGeComodtyNome: TWideStringField;
    DsArtGeComodty: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    EdOrdem: TdmkEdit;
    QrArtGeComodtySigla: TWideStringField;
    QrArtGeComodtyOrdem: TIntegerField;
    EdSigla: TdmkEdit;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label4: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    CGVisuRel: TdmkCheckGroup;
    dmkDBCheckGroup1: TdmkDBCheckGroup;
    QrArtGeComodtyVisuRel: TIntegerField;
    BtReordena: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrArtGeComodtyAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrArtGeComodtyBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure BtReordenaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmArtGeComodty: TFmArtGeComodty;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, ArtGeCommReord;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmArtGeComodty.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmArtGeComodty.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrArtGeComodtyCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmArtGeComodty.DefParams;
begin
  VAR_GOTOTABELA := 'artgecomodty';
  VAR_GOTOMYSQLTABLE := QrArtGeComodty;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM artgecomodty');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmArtGeComodty.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmArtGeComodty.QueryPrincipalAfterOpen;
begin
end;

procedure TFmArtGeComodty.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmArtGeComodty.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmArtGeComodty.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmArtGeComodty.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmArtGeComodty.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmArtGeComodty.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmArtGeComodty.BtAlteraClick(Sender: TObject);
begin
  if (QrArtGeComodty.State <> dsInactive) and (QrArtGeComodty.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrArtGeComodty, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'artgecomodty');
  end;
end;

procedure TFmArtGeComodty.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrArtGeComodtyCodigo.Value;
  Close;
end;

procedure TFmArtGeComodty.BtConfirmaClick(Sender: TObject);
var
  Nome, Sigla: String;
  Codigo, Ordem, VisuRel: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Sigla          := EdSigla.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  VisuRel        := CGVisuRel.Value;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('artgecomodty', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'artgecomodty', False, [
  'Nome', 'Sigla', 'Ordem',
  'VisuRel'], [
  'Codigo'], [
  Nome, Sigla, Ordem,
  VisuRel], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmArtGeComodty.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'artgecomodty', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmArtGeComodty.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrArtGeComodty, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'artgecomodty');
end;

procedure TFmArtGeComodty.BtReordenaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmArtGeCommReord, FmArtGeCommReord, afmoNegarComAviso) then
  begin
    FmArtGeCommReord.ShowModal;
    FmArtGeCommReord.Destroy;
  end;
end;

procedure TFmArtGeComodty.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmArtGeComodty.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrArtGeComodtyCodigo.Value, LaRegistro.Caption);
end;

procedure TFmArtGeComodty.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmArtGeComodty.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrArtGeComodtyCodigo.Value, LaRegistro.Caption);
end;

procedure TFmArtGeComodty.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmArtGeComodty.QrArtGeComodtyAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmArtGeComodty.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmArtGeComodty.SbQueryClick(Sender: TObject);
begin
  LocCod(QrArtGeComodtyCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'artgecomodty', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmArtGeComodty.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmArtGeComodty.QrArtGeComodtyBeforeOpen(DataSet: TDataSet);
begin
  QrArtGeComodtyCodigo.DisplayFormat := FFormatFloat;
end;

end.

