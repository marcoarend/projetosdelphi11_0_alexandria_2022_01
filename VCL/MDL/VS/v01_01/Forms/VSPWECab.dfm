object FmVSPWECab: TFmVSPWECab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-093 :: Processo de Semi Acabado'
  ClientHeight = 758
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 653
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 457
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 461
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 177
      Width = 1008
      Height = 84
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label13: TLabel
        Left = 16
        Top = 4
        Width = 28
        Height = 13
        Caption = 'IME-I:'
      end
      object Label14: TLabel
        Left = 100
        Top = 4
        Width = 95
        Height = 13
        Caption = 'Artigo em opera'#231#227'o:'
      end
      object Label15: TLabel
        Left = 576
        Top = 4
        Width = 45
        Height = 13
        Caption = '$/m'#178' MO:'
      end
      object Label16: TLabel
        Left = 672
        Top = 4
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object Label17: TLabel
        Left = 748
        Top = 4
        Width = 67
        Height = 13
        Caption = 'Peso (origem):'
        Enabled = False
      end
      object Label20: TLabel
        Left = 836
        Top = 4
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        Enabled = False
      end
      object Label21: TLabel
        Left = 916
        Top = 4
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Enabled = False
      end
      object Label24: TLabel
        Left = 132
        Top = 44
        Width = 137
        Height = 13
        Caption = 'Fornecedor da mat'#233'ria prima:'
        FocusControl = DBEdit16
      end
      object Label26: TLabel
        Left = 16
        Top = 44
        Width = 91
        Height = 13
        Caption = 'S'#233'rie / Ficha RMP:'
        FocusControl = DBEdit18
      end
      object Label51: TLabel
        Left = 648
        Top = 44
        Width = 148
        Height = 13
        Caption = 'Local do (e) centro de estoque:'
        FocusControl = DBEdit39
      end
      object Label52: TLabel
        Left = 932
        Top = 44
        Width = 42
        Height = 13
        Caption = 'N'#176' RME:'
        FocusControl = DBEdit41
      end
      object Label60: TLabel
        Left = 532
        Top = 4
        Width = 39
        Height = 13
        Caption = 'Arquivo:'
        FocusControl = DBEdit45
      end
      object Label61: TLabel
        Left = 432
        Top = 45
        Width = 92
        Height = 13
        Caption = 'Fornecedor de MO:'
        FocusControl = DBEdit47
      end
      object DBEdit5: TDBEdit
        Left = 16
        Top = 20
        Width = 81
        Height = 21
        DataField = 'Controle'
        DataSource = DsVSPWEAtu
        TabOrder = 0
      end
      object DBEdit6: TDBEdit
        Left = 100
        Top = 20
        Width = 57
        Height = 21
        DataField = 'GraGruX'
        DataSource = DsVSPWEAtu
        TabOrder = 1
      end
      object DBEdit7: TDBEdit
        Left = 156
        Top = 20
        Width = 373
        Height = 21
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsVSPWEAtu
        TabOrder = 2
      end
      object DBEdit8: TDBEdit
        Left = 576
        Top = 20
        Width = 94
        Height = 21
        DataField = 'CustoMOM2'
        DataSource = DsVSPWEAtu
        TabOrder = 3
      end
      object DBEdit9: TDBEdit
        Left = 672
        Top = 20
        Width = 72
        Height = 21
        DataField = 'Pecas'
        DataSource = DsVSPWEAtu
        TabOrder = 4
      end
      object DBEdit10: TDBEdit
        Left = 748
        Top = 20
        Width = 84
        Height = 21
        DataField = 'QtdAntPeso'
        DataSource = DsVSPWEAtu
        TabOrder = 5
      end
      object DBEdit12: TDBEdit
        Left = 836
        Top = 20
        Width = 76
        Height = 21
        DataField = 'AreaM2'
        DataSource = DsVSPWEAtu
        TabOrder = 6
      end
      object DBEdit13: TDBEdit
        Left = 916
        Top = 20
        Width = 84
        Height = 21
        DataField = 'AreaP2'
        DataSource = DsVSPWEAtu
        TabOrder = 7
      end
      object DBEdit16: TDBEdit
        Left = 132
        Top = 60
        Width = 56
        Height = 21
        DataField = 'Terceiro'
        DataSource = DsVSPWEAtu
        TabOrder = 8
      end
      object DBEdit17: TDBEdit
        Left = 188
        Top = 60
        Width = 240
        Height = 21
        DataField = 'NO_FORNECE'
        DataSource = DsVSPWEAtu
        TabOrder = 9
      end
      object DBEdit18: TDBEdit
        Left = 16
        Top = 60
        Width = 113
        Height = 21
        DataField = 'NO_FICHA'
        DataSource = DsVSPWEAtu
        TabOrder = 10
      end
      object DBEdit39: TDBEdit
        Left = 704
        Top = 60
        Width = 225
        Height = 21
        DataField = 'NO_LOC_CEN'
        DataSource = DsVSPWEAtu
        TabOrder = 11
      end
      object DBEdit40: TDBEdit
        Left = 648
        Top = 60
        Width = 56
        Height = 21
        DataField = 'StqCenLoc'
        DataSource = DsVSPWEAtu
        TabOrder = 12
      end
      object DBEdit41: TDBEdit
        Left = 932
        Top = 60
        Width = 68
        Height = 21
        DataField = 'ReqMovEstq'
        DataSource = DsVSPWEAtu
        TabOrder = 13
      end
      object DBEdit45: TDBEdit
        Left = 532
        Top = 20
        Width = 40
        Height = 21
        DataField = 'NO_TTW'
        DataSource = DsVSPWEAtu
        TabOrder = 14
      end
      object DBEdit46: TDBEdit
        Left = 432
        Top = 60
        Width = 56
        Height = 21
        DataField = 'FornecMO'
        DataSource = DsVSPWEAtu
        TabOrder = 15
      end
      object DBEdit47: TDBEdit
        Left = 488
        Top = 60
        Width = 157
        Height = 21
        DataField = 'NO_FORNEC_MO'
        DataSource = DsVSPWEAtu
        TabOrder = 16
      end
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 177
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label1: TLabel
        Left = 940
        Top = 8
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        FocusControl = DBEdit0
      end
      object Label2: TLabel
        Left = 80
        Top = 8
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit1
      end
      object Label11: TLabel
        Left = 592
        Top = 8
        Width = 107
        Height = 13
        Caption = 'Liberado p/ classificar:'
        FocusControl = DBEdit3
      end
      object Label5: TLabel
        Left = 344
        Top = 48
        Width = 174
        Height = 13
        Caption = 'Observa'#231#227'o sobre a industrializa'#231#227'o:'
        FocusControl = DBEdit4
      end
      object Label22: TLabel
        Left = 440
        Top = 8
        Width = 25
        Height = 13
        Caption = #193'rea:'
        FocusControl = DBEdit14
      end
      object Label23: TLabel
        Left = 476
        Top = 8
        Width = 94
        Height = 13
        Caption = 'Data/hora gera'#231#227'o:'
        FocusControl = DBEdit15
      end
      object Label32: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = dmkDBEdit1
      end
      object Label33: TLabel
        Left = 708
        Top = 8
        Width = 94
        Height = 13
        Caption = 'Configur. p/ classif.:'
        FocusControl = DBEdit25
      end
      object Label34: TLabel
        Left = 824
        Top = 8
        Width = 83
        Height = 13
        Caption = 'Fim classifica'#231#227'o:'
        FocusControl = DBEdit26
      end
      object Label55: TLabel
        Left = 16
        Top = 48
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        FocusControl = DBEdit42
      end
      object Label58: TLabel
        Left = 680
        Top = 48
        Width = 182
        Height = 13
        Caption = 'LPFMO (Lote prod. forn. mao de obra):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label59: TLabel
        Left = 888
        Top = 48
        Width = 116
        Height = 13
        Caption = 'S'#233'rie e N'#176' NFe remessa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label67: TLabel
        Left = 16
        Top = 156
        Width = 110
        Height = 13
        Caption = 'Configura'#231#227'o de artigo:'
      end
      object Label71: TLabel
        Left = 604
        Top = 156
        Width = 88
        Height = 13
        Caption = 'Grupo de emiss'#227'o:'
      end
      object DBEdit0: TdmkDBEdit
        Left = 940
        Top = 24
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'MovimCod'
        DataSource = DsVSPWECab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 7
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 80
        Top = 24
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVSPWECab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 136
        Top = 24
        Width = 301
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsVSPWECab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 592
        Top = 24
        Width = 112
        Height = 21
        DataField = 'NO_DtHrLibOpe'
        DataSource = DsVSPWECab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 344
        Top = 64
        Width = 333
        Height = 21
        DataField = 'Nome'
        DataSource = DsVSPWECab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clFuchsia
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
      end
      object DBEdit14: TDBEdit
        Left = 440
        Top = 24
        Width = 32
        Height = 21
        DataField = 'NO_TIPO'
        DataSource = DsVSPWECab
        TabOrder = 5
      end
      object DBEdit15: TDBEdit
        Left = 476
        Top = 24
        Width = 112
        Height = 21
        DataField = 'DtHrAberto'
        DataSource = DsVSPWECab
        TabOrder = 6
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 16
        Top = 24
        Width = 61
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSPWECab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit25: TDBEdit
        Left = 708
        Top = 24
        Width = 112
        Height = 21
        DataField = 'NO_DtHrCfgOpe'
        DataSource = DsVSPWECab
        TabOrder = 8
      end
      object DBEdit26: TDBEdit
        Left = 824
        Top = 24
        Width = 112
        Height = 21
        DataField = 'NO_DtHrFimOpe'
        DataSource = DsVSPWECab
        TabOrder = 9
      end
      object GroupBox3: TGroupBox
        Left = 16
        Top = 88
        Width = 245
        Height = 61
        Caption = ' Origem:'
        TabOrder = 10
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label36: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label37: TLabel
            Left = 76
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label38: TLabel
            Left = 160
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object DBEdit27: TDBEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PecasINI'
            DataSource = DsVSPWECab
            TabOrder = 0
          end
          object DBEdit28: TDBEdit
            Left = 76
            Top = 20
            Width = 80
            Height = 21
            DataField = 'PesoKgINI'
            DataSource = DsVSPWECab
            TabOrder = 1
          end
          object DBEdit29: TDBEdit
            Left = 160
            Top = 20
            Width = 76
            Height = 21
            DataField = 'AreaINIM2'
            DataSource = DsVSPWECab
            TabOrder = 2
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 264
        Top = 88
        Width = 245
        Height = 61
        Caption = 'Destino:'
        TabOrder = 11
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label40: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label41: TLabel
            Left = 76
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label42: TLabel
            Left = 160
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object DBEdit31: TDBEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PecasDst'
            DataSource = DsVSPWECab
            TabOrder = 0
          end
          object DBEdit32: TDBEdit
            Left = 76
            Top = 20
            Width = 80
            Height = 21
            DataField = 'PesoKgDst'
            DataSource = DsVSPWECab
            TabOrder = 1
          end
          object DBEdit33: TDBEdit
            Left = 160
            Top = 20
            Width = 76
            Height = 21
            DataField = 'AreaDstM2'
            DataSource = DsVSPWECab
            TabOrder = 2
          end
        end
      end
      object GroupBox5: TGroupBox
        Left = 760
        Top = 88
        Width = 245
        Height = 61
        Caption = 'Saldo em opera'#231#227'o:'
        TabOrder = 12
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label44: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label45: TLabel
            Left = 76
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label46: TLabel
            Left = 160
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object DBEdit35: TDBEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PecasSdo'
            DataSource = DsVSPWECab
            TabOrder = 0
          end
          object DBEdit36: TDBEdit
            Left = 76
            Top = 20
            Width = 80
            Height = 21
            DataField = 'PesoKgSdo'
            DataSource = DsVSPWECab
            TabOrder = 1
          end
          object DBEdit37: TDBEdit
            Left = 160
            Top = 20
            Width = 76
            Height = 21
            DataField = 'AreaSdoM2'
            DataSource = DsVSPWECab
            TabOrder = 2
          end
        end
      end
      object GroupBox6: TGroupBox
        Left = 512
        Top = 88
        Width = 245
        Height = 61
        Caption = 'Baixas normais:'
        TabOrder = 13
        object Panel10: TPanel
          Left = 2
          Top = 15
          Width = 241
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label39: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label43: TLabel
            Left = 76
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label47: TLabel
            Left = 160
            Top = 4
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object DBEdit30: TDBEdit
            Left = 4
            Top = 20
            Width = 68
            Height = 21
            DataField = 'PecasBxa'
            DataSource = DsVSPWECab
            TabOrder = 0
          end
          object DBEdit34: TDBEdit
            Left = 76
            Top = 20
            Width = 80
            Height = 21
            DataField = 'PesoKgBxa'
            DataSource = DsVSPWECab
            TabOrder = 1
          end
          object DBEdit38: TDBEdit
            Left = 160
            Top = 20
            Width = 76
            Height = 21
            DataField = 'AreaBxaM2'
            DataSource = DsVSPWECab
            TabOrder = 2
          end
        end
      end
      object DBEdit42: TDBEdit
        Left = 16
        Top = 64
        Width = 56
        Height = 21
        DataField = 'Cliente'
        DataSource = DsVSPWECab
        TabOrder = 14
      end
      object DBEdit43: TDBEdit
        Left = 72
        Top = 64
        Width = 269
        Height = 21
        DataField = 'NO_Cliente'
        DataSource = DsVSPWECab
        TabOrder = 15
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 920
        Top = 64
        Width = 81
        Height = 21
        TabStop = False
        DataField = 'NFeRem'
        DataSource = DsVSPWECab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 16
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit44: TDBEdit
        Left = 680
        Top = 64
        Width = 205
        Height = 21
        DataField = 'LPFMO'
        DataSource = DsVSPWECab
        TabOrder = 17
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 888
        Top = 64
        Width = 29
        Height = 21
        TabStop = False
        DataField = 'SerieRem'
        DataSource = DsVSPWECab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 18
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit49: TDBEdit
        Left = 132
        Top = 152
        Width = 56
        Height = 21
        DataField = 'VSArtCab'
        DataSource = DsVSPWECab
        TabOrder = 19
      end
      object DBEdit50: TDBEdit
        Left = 188
        Top = 152
        Width = 409
        Height = 21
        DataField = 'NO_VSArtCab'
        DataSource = DsVSPWECab
        TabOrder = 20
      end
      object DBEdit53: TDBEdit
        Left = 696
        Top = 152
        Width = 56
        Height = 21
        DataField = 'EmitGru'
        DataSource = DsVSPWECab
        TabOrder = 21
      end
      object DBEdit54: TDBEdit
        Left = 752
        Top = 152
        Width = 245
        Height = 21
        DataField = 'NO_EmitGru'
        DataSource = DsVSPWECab
        TabOrder = 22
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 589
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 202
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 376
        Top = 15
        Width = 630
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 497
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 4
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 296
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Em Processo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCabClick
        end
        object BtOri: TBitBtn
          Tag = 421
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Origem'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtOriClick
        end
        object BtDst: TBitBtn
          Tag = 447
          Left = 376
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Destino'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtDstClick
        end
        object BtPesagem: TBitBtn
          Tag = 194
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pesagem'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtPesagemClick
        end
      end
    end
    object PCPWEOri: TPageControl
      Left = 0
      Top = 321
      Width = 1008
      Height = 136
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 3
      object TabSheet1: TTabSheet
        Caption = 'Palltets'
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 728
          Height = 108
          Align = alClient
          DataSource = DsVSPWEOriPallet
          PopupMenu = PMOri
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_TTW'
              Title.Caption = 'Arquivo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SerieFch'
              Title.Caption = 'S'#233'rie RMP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigos origem da opera'#231#227'o'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea m'#178
              Visible = True
            end>
        end
        object GroupBox7: TGroupBox
          Left = 728
          Top = 0
          Width = 272
          Height = 108
          Align = alRight
          Caption = ' Baixas for'#231'adas: '
          TabOrder = 1
          object DBGrid3: TDBGrid
            Left = 2
            Top = 15
            Width = 268
            Height = 91
            Align = alClient
            DataSource = DsForcados
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso Kg'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data/Hora'
                Visible = True
              end>
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'IME-Is'
        ImageIndex = 1
        object DGDadosOri: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 132
          Align = alClient
          DataSource = DsVSPWEOriIMEI
          PopupMenu = PMOri
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_TTW'
              Title.Caption = 'Arquivo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IxxMovIX'
              Title.Caption = 'IXX'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IxxFolha'
              Title.Caption = 'Folha'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IxxLinha'
              Title.Caption = 'Lin'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ReqMovEstq'
              Title.Caption = 'N'#176' RME'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SerieFch'
              Title.Caption = 'S'#233'rie RMP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigos origem da opera'#231#227'o'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaMPAG'
              Title.Caption = 'Nota MPAG'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = 'm'#178' gerado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaP2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Fornecedor'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Title.Caption = 'Observa'#231#245'es'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end>
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Pesagens de recurtimento e acabamento'
        ImageIndex = 2
        object DBGrid6: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 132
          Align = alClient
          DataSource = DsEmit
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Pesagem'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataEmis'
              Title.Caption = 'Dta emiss'#227'o'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaBaixa_TXT'
              Title.Caption = 'Dta Baixa'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtCorrApo_TXT'
              Title.Caption = 'Dta CorrApo'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Pe'#231'as'
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Peso'
              Title.Caption = 'Peso KG'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Fulao'
              Title.Caption = 'Ful'#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Numero'
              Title.Caption = 'Receita'
              Width = 43
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME'
              Title.Caption = 'Nome da Receita'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESETOR'
              Title.Caption = 'Setor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECI'
              Title.Caption = 'Dono dos produtos'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Custo'
              Title.Caption = 'Custo Total'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Obs'
              Width = 600
              Visible = True
            end>
        end
      end
      object TabSheet6: TTabSheet
        Caption = 'Outras baixas de insumos'
        ImageIndex = 3
        object DBGrid7: TDBGrid
          Left = 0
          Top = 0
          Width = 517
          Height = 94
          Align = alLeft
          DataSource = DsPQO
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataB'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoTotal'
              Title.Caption = 'Custo'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EmitGru'
              Title.Caption = 'Grupo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_EMITGRU'
              Title.Caption = 'Nome Grupo'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESETOR'
              Title.Caption = 'Setor'
              Width = 90
              Visible = True
            end>
        end
        object DBGIts: TDBGrid
          Left = 517
          Top = 0
          Width = 485
          Height = 94
          Align = alClient
          DataSource = DsPQOIts
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'OrigemCtrl'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Insumo'
              Title.Alignment = taCenter
              Title.Caption = 'C'#243'digo'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CliDest'
              Title.Caption = 'Empresa'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPQ'
              Title.Caption = 'Mercadoria'
              Width = 366
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Peso'
              Title.Alignment = taRightJustify
              Title.Caption = 'Quantidade'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Title.Alignment = taRightJustify
              Title.Caption = 'Valor total'
              Width = 92
              Visible = True
            end>
        end
      end
      object TsEnvioMO: TTabSheet
        Caption = ' Dados do envio para M.O. '
        ImageIndex = 4
        object PnEnvioMO: TPanel
          Left = 0
          Top = 0
          Width = 992
          Height = 132
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitWidth = 1000
          ExplicitHeight = 108
          object DBGVSMOEnvEnv: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 529
            Height = 132
            Align = alLeft
            DataSource = DsVSMOEnvEnv
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFEMP_nNF'
                Title.Caption = 'N'#186' NF'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_nCT'
                Title.Caption = 'N'#186' CT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_ValorT'
                Title.Caption = 'Valor frete'
                Width = 72
                Visible = True
              end>
          end
          object DBGVSMOEnvEVmi: TdmkDBGridZTO
            Left = 529
            Top = 0
            Width = 463
            Height = 132
            Align = alClient
            DataSource = DsVSMOEnvEVMI
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VSMovIts'
                Title.Caption = 'IME-I'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorFrete'
                Title.Caption = '$ Frete'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end>
          end
        end
      end
      object TsRetornoMO: TTabSheet
        Caption = ' Retorno da M.O. '
        ImageIndex = 5
        object PnRetornoMO: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 132
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGVSMOEnvRet: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 661
            Height = 132
            Align = alLeft
            DataSource = DsVSMOEnvRet
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_nCT'
                Title.Caption = 'N'#186' CT'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_AreaM2'
                Title.Caption = 'Area m'#178
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_PesoKg'
                Title.Caption = 'Peso kg'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_ValorT'
                Title.Caption = 'Valor frete'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTPA_Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFRMP_SerNF'
                Title.Caption = 'S'#233'r. Ret'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFRMP_nNF'
                Title.Caption = 'NF Retorno'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFCMO_SerNF'
                Title.Caption = 'S'#233'r. M.O.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFCMO_nNF'
                Title.Caption = 'NF M.O.'
                Visible = True
              end>
          end
          object PnItensRetMO: TPanel
            Left = 661
            Top = 0
            Width = 339
            Height = 132
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Splitter2: TSplitter
              Left = 0
              Top = 51
              Width = 339
              Height = 5
              Cursor = crVSplit
              Align = alBottom
              ExplicitLeft = 585
              ExplicitTop = 1
              ExplicitWidth = 208
            end
            object GroupBox8: TGroupBox
              Left = 0
              Top = 0
              Width = 339
              Height = 51
              Align = alClient
              Caption = ' Itens de NFes enviados para M.O.: '
              TabOrder = 0
              object DBGVSMOEnvRVmi: TdmkDBGridZTO
                Left = 2
                Top = 15
                Width = 335
                Height = 34
                TabStop = False
                Align = alClient
                DataSource = DsVSMOEnvRVmi
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NFEMP_SerNF'
                    Title.Caption = 'S'#233'r. NF'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NFEMP_nNF'
                    Title.Caption = 'NF'
                    Width = 65
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaP2'
                    Title.Caption = #193'rea ft'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorT'
                    Title.Caption = 'Valor Total'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorFrete'
                    Title.Caption = 'Valor Frete'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID Item'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VSMOEnvEnv'
                    Title.Caption = 'ID Envio'
                    Visible = True
                  end>
              end
            end
            object GroupBox9: TGroupBox
              Left = 0
              Top = 56
              Width = 339
              Height = 76
              Align = alBottom
              Caption = ' IME-Is de couros prontos retornados: '
              TabOrder = 1
              object DBGVSMOEnvGVmi: TdmkDBGridZTO
                Left = 2
                Top = 15
                Width = 335
                Height = 59
                TabStop = False
                Align = alClient
                DataSource = DsVSMOEnvGVmi
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID Item'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VSMovIts'
                    Title.Caption = 'IME-I'
                    Width = 48
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = #193'rea m'#178
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso kg'
                    Width = 62
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorFrete'
                    Visible = True
                  end>
              end
            end
          end
        end
      end
    end
    object Panel11: TPanel
      Left = 0
      Top = 261
      Width = 1008
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 4
      object Label18: TLabel
        Left = 752
        Top = 20
        Width = 164
        Height = 13
        Caption = 'Observa'#231#227'o sobre o artigo gerado:'
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 745
        Height = 60
        Align = alLeft
        Caption = ' Valores: '
        TabOrder = 0
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 741
          Height = 43
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label25: TLabel
            Left = 12
            Top = 0
            Width = 62
            Height = 13
            Caption = '$/kg M.obra:'
          end
          object Label27: TLabel
            Left = 88
            Top = 0
            Width = 53
            Height = 13
            Caption = 'Total M.O.:'
            FocusControl = DBEdit20
          end
          object Label28: TLabel
            Left = 500
            Top = 0
            Width = 50
            Height = 13
            Caption = 'Valor total:'
            FocusControl = DBEdit21
          end
          object Label29: TLabel
            Left = 172
            Top = 0
            Width = 49
            Height = 13
            Caption = 'Valor M.P.'
            FocusControl = DBEdit22
          end
          object Label30: TLabel
            Left = 588
            Top = 0
            Width = 39
            Height = 13
            Caption = 'R$ / m'#178':'
            FocusControl = DBEdit23
          end
          object Label31: TLabel
            Left = 664
            Top = 0
            Width = 37
            Height = 13
            Caption = 'R$ / ft'#178':'
            FocusControl = DBEdit24
          end
          object Label65: TLabel
            Left = 336
            Top = 0
            Width = 48
            Height = 13
            Caption = 'Custo PQ:'
            FocusControl = DBEdit48
          end
          object Label68: TLabel
            Left = 424
            Top = 0
            Width = 63
            Height = 13
            Caption = 'Frete retorno:'
            FocusControl = DBEdit51
          end
          object Label69: TLabel
            Left = 260
            Top = 0
            Width = 56
            Height = 13
            Caption = 'Frete envio:'
            FocusControl = DBEdit52
          end
          object DBEdit19: TDBEdit
            Left = 12
            Top = 16
            Width = 72
            Height = 21
            DataField = 'CustoMOKg'
            DataSource = DsVSPWEAtu
            TabOrder = 0
          end
          object DBEdit20: TDBEdit
            Left = 88
            Top = 16
            Width = 80
            Height = 21
            DataField = 'CustoMOTot'
            DataSource = DsVSPWEAtu
            TabOrder = 1
          end
          object DBEdit21: TDBEdit
            Left = 500
            Top = 16
            Width = 84
            Height = 21
            DataField = 'ValorT'
            DataSource = DsVSPWEAtu
            TabOrder = 2
          end
          object DBEdit22: TDBEdit
            Left = 172
            Top = 16
            Width = 84
            Height = 21
            DataField = 'ValorMP'
            DataSource = DsVSPWEAtu
            TabOrder = 3
          end
          object DBEdit23: TDBEdit
            Left = 588
            Top = 16
            Width = 72
            Height = 21
            DataField = 'CUSTO_M2'
            DataSource = DsVSPWEAtu
            TabOrder = 4
          end
          object DBEdit24: TDBEdit
            Left = 664
            Top = 16
            Width = 72
            Height = 21
            DataField = 'CUSTO_P2'
            DataSource = DsVSPWEAtu
            TabOrder = 5
          end
          object DBEdit48: TDBEdit
            Left = 336
            Top = 16
            Width = 84
            Height = 21
            DataField = 'CustoPQ'
            DataSource = DsVSPWEAtu
            TabOrder = 6
          end
          object DBEdit51: TDBEdit
            Left = 424
            Top = 16
            Width = 72
            Height = 21
            DataField = 'CusFrtMORet'
            DataSource = DsVSPWEAtu
            TabOrder = 7
          end
          object DBEdit52: TDBEdit
            Left = 260
            Top = 16
            Width = 72
            Height = 21
            DataField = 'CusFrtMOEnv'
            DataSource = DsVSPWEAtu
            TabOrder = 8
          end
        end
      end
      object DBEdit11: TDBEdit
        Left = 752
        Top = 36
        Width = 249
        Height = 21
        DataField = 'Observ'
        DataSource = DsVSPWEAtu
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
    end
    object PCPWEDst: TPageControl
      Left = 0
      Top = 489
      Width = 1008
      Height = 100
      ActivePage = TabSheet3
      Align = alBottom
      TabOrder = 5
      object TabSheet3: TTabSheet
        Caption = ' Destino Semi Acabado'
        object Panel12: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 72
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DGDadosDst: TDBGrid
            Left = 0
            Top = 0
            Width = 593
            Height = 72
            Align = alClient
            DataSource = DsVSPWEDst
            PopupMenu = PMDst
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_TTW'
                Title.Caption = 'Arquivo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ReqMovEstq'
                Title.Caption = 'N'#176' RME'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data / hora'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IME-I'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PALLET'
                Title.Caption = 'Hist'#243'rico'
                Width = 145
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_SerieFch'
                Title.Caption = 'S'#233'rie RMP'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ficha'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Artigos destino da opera'#231#227'o'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Width = 240
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NotaMPAG'
                Title.Caption = 'Nota MPAG'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = 'm'#178' gerado'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorT'
                Title.Caption = 'Valor total'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FORNECE'
                Title.Caption = 'Fornecedor'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Observ'
                Title.Caption = 'Observa'#231#245'es'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorMP'
                Title.Caption = 'Valor MP'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CustoMOTot'
                Title.Caption = 'Custo MO Tot'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CusFrtMORet'
                Title.Caption = 'Frete retorno'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorT'
                Title.Caption = 'Valor T'
                Visible = True
              end>
          end
          object Panel14: TPanel
            Left = 593
            Top = 0
            Width = 407
            Height = 72
            Align = alRight
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object PCPWEDestSub: TPageControl
              Left = 0
              Top = 0
              Width = 407
              Height = 72
              ActivePage = TabSheet9
              Align = alClient
              TabOrder = 0
              object TabSheet8: TTabSheet
                Caption = ' Baixa destino'
                object DBGrid1: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 399
                  Height = 44
                  Align = alClient
                  DataSource = DsVSPWEBxa
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Title.Caption = 'IME-I'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Pallet'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'GraGruX'
                      Title.Caption = 'Reduzido'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Pecas'
                      Title.Caption = 'Pe'#231'as'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'AreaM2'
                      Title.Caption = 'm'#178' gerado'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PesoKg'
                      Title.Caption = 'Peso kg'
                      Width = 60
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DataHora'
                      Title.Caption = 'Data/Hora'
                      Visible = True
                    end>
                end
              end
              object TabSheet9: TTabSheet
                Caption = ' Cobran'#231'a MO'
                ImageIndex = 1
                object dmkDBGridZTO1: TdmkDBGridZTO
                  Left = 0
                  Top = 0
                  Width = 399
                  Height = 44
                  Align = alClient
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  RowColors = <>
                end
              end
            end
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = ' Destino Desclassificado'
        ImageIndex = 1
        object Panel13: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 72
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object DBGrid4: TDBGrid
            Left = 1
            Top = 1
            Width = 641
            Height = 70
            Align = alClient
            DataSource = DsVSPWEDesclDst
            PopupMenu = PMDst
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_TTW'
                Title.Caption = 'Arquivo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ReqMovEstq'
                Title.Caption = 'N'#176' RME'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data / hora'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IME-I'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PALLET'
                Title.Caption = 'Hist'#243'rico'
                Width = 145
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_SerieFch'
                Title.Caption = 'S'#233'rie RMP'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ficha'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRD_TAM_COR'
                Title.Caption = 'Artigos destino da opera'#231#227'o'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Width = 240
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NotaMPAG'
                Title.Caption = 'Nota MPAG'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = 'm'#178' gerado'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_FORNECE'
                Title.Caption = 'Fornecedor'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Observ'
                Title.Caption = 'Observa'#231#245'es'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Visible = True
              end>
          end
          object DBGrid5: TDBGrid
            Left = 642
            Top = 1
            Width = 357
            Height = 70
            Align = alRight
            DataSource = DsVSPWEDesclBxa
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'IME-I'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pallet'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'GraGruX'
                Title.Caption = 'Reduzido'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = 'm'#178' gerado'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Width = 60
                Visible = True
              end>
          end
        end
      end
      object TabSheet7: TTabSheet
        Caption = 'Destino Sub-produto WB'
        ImageIndex = 2
        object DBGrid8: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 72
          Align = alClient
          DataSource = DsVSPWESubPrd
          PopupMenu = PMDst
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_TTW'
              Title.Caption = 'Arquivo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ReqMovEstq'
              Title.Caption = 'N'#176' RME'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data / hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PALLET'
              Title.Caption = 'Hist'#243'rico'
              Width = 145
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SerieFch'
              Title.Caption = 'S'#233'rie RMP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ficha'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigos destino da opera'#231#227'o'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaMPAG'
              Title.Caption = 'Nota MPAG'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = 'm'#178' gerado'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Fornecedor'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Title.Caption = 'Observa'#231#245'es'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end>
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 653
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object Panel15: TPanel
      Left = 0
      Top = 383
      Width = 1008
      Height = 84
      Align = alTop
      TabOrder = 5
      object Label73: TLabel
        Left = 12
        Top = 3
        Width = 104
        Height = 13
        Caption = 'Espessura de rebaixe:'
      end
      object Label74: TLabel
        Left = 60
        Top = 27
        Width = 5
        Height = 13
        Caption = '/'
      end
      object Label75: TLabel
        Left = 128
        Top = 3
        Width = 91
        Height = 13
        Caption = 'Espessura do semi:'
      end
      object Label76: TLabel
        Left = 176
        Top = 27
        Width = 5
        Height = 13
        Caption = '/'
      end
      object Label77: TLabel
        Left = 240
        Top = 4
        Width = 87
        Height = 13
        Caption = 'Data / hora in'#237'cio:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label78: TLabel
        Left = 392
        Top = 4
        Width = 82
        Height = 13
        Caption = 'Data / hora semi:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label79: TLabel
        Left = 548
        Top = 4
        Width = 110
        Height = 13
        Caption = 'Data / hora expedi'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label80: TLabel
        Left = 12
        Top = 44
        Width = 116
        Height = 13
        Caption = 'Receita de recurtimento:'
      end
      object Label81: TLabel
        Left = 508
        Top = 44
        Width = 189
        Height = 13
        Caption = 'Receita de recurtimento - segunda fase:'
      end
      object Label82: TLabel
        Left = 700
        Top = 5
        Width = 39
        Height = 13
        Caption = 'Classes:'
      end
      object EdLinCulReb: TdmkEdit
        Left = 12
        Top = 19
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'LinCulReb'
        UpdCampo = 'LinCulReb'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdLinCabReb: TdmkEdit
        Left = 68
        Top = 19
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'LinCabReb'
        UpdCampo = 'LinCabReb'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdLinCulSem: TdmkEdit
        Left = 128
        Top = 19
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'LinCulSem'
        UpdCampo = 'LinCulSem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdLinCabSem: TdmkEdit
        Left = 184
        Top = 19
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'LinCabSem'
        UpdCampo = 'LinCabSem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDataStart: TdmkEditDateTimePicker
        Left = 240
        Top = 20
        Width = 108
        Height = 21
        Date = 44872.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 4
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdDataStart: TdmkEdit
        Left = 348
        Top = 20
        Width = 40
        Height = 21
        TabOrder = 5
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryName = 'QrVSGerArt'
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDataCrust: TdmkEditDateTimePicker
        Left = 392
        Top = 20
        Width = 108
        Height = 21
        Date = 44872.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 6
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdDataCrust: TdmkEdit
        Left = 500
        Top = 20
        Width = 40
        Height = 21
        TabOrder = 7
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryName = 'QrVSGerArt'
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDataExped: TdmkEditDateTimePicker
        Left = 548
        Top = 20
        Width = 108
        Height = 21
        Date = 44872.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 8
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdDataExped: TdmkEdit
        Left = 656
        Top = 20
        Width = 40
        Height = 21
        TabOrder = 9
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryName = 'QrVSGerArt'
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdReceiRecu: TdmkEditCB
        Left = 12
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ReceiRecu'
        UpdCampo = 'ReceiRecu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBReceiRecu
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBReceiRecu: TdmkDBLookupComboBox
        Left = 68
        Top = 60
        Width = 433
        Height = 21
        Color = clWhite
        KeyField = 'Numero'
        ListField = 'Nome'
        ListSource = DsReceiRecu
        TabOrder = 12
        dmkEditCB = EdReceiRecu
        QryCampo = 'ReceiRecu'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdReceiRefu: TdmkEditCB
        Left = 508
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ReceiRefu'
        UpdCampo = 'ReceiRefu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBReceiRefu
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBReceiRefu: TdmkDBLookupComboBox
        Left = 564
        Top = 60
        Width = 433
        Height = 21
        Color = clWhite
        KeyField = 'Numero'
        ListField = 'Nome'
        ListSource = DsReceiRefu
        TabOrder = 14
        dmkEditCB = EdReceiRefu
        QryCampo = 'ReceiRefu'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdClasses: TdmkEdit
        Left = 700
        Top = 20
        Width = 270
        Height = 21
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Classes'
        UpdCampo = 'Classes'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBEdita1: TGroupBox
      Left = 0
      Top = 45
      Width = 1008
      Height = 126
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label7: TLabel
        Left = 16
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label53: TLabel
        Left = 720
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Data / hora:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 872
        Top = 4
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 16
        Top = 44
        Width = 182
        Height = 13
        Caption = 'Observa'#231#227'o sobre a opera'#231#227'o (IMEC):'
      end
      object Label56: TLabel
        Left = 448
        Top = 44
        Width = 182
        Height = 13
        Caption = 'LPFMO (Lote prod. forn. mao de obra):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label57: TLabel
        Left = 804
        Top = 44
        Width = 116
        Height = 13
        Caption = 'S'#233'rie e N'#176' NFe remessa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label66: TLabel
        Left = 16
        Top = 84
        Width = 232
        Height = 13
        Caption = 'Configura'#231#227'o de artigo semi-acabado / acabado:'
      end
      object Label70: TLabel
        Left = 624
        Top = 84
        Width = 137
        Height = 13
        Caption = 'Contrato (Grupo de emiss'#227'o):'
      end
      object Label72: TLabel
        Left = 644
        Top = 44
        Width = 150
        Height = 13
        Caption = 'Meu lote (do contrato / pedido):'
        Color = clBtnFace
        ParentColor = False
      end
      object SbVSArtCab: TSpeedButton
        Left = 600
        Top = 100
        Width = 21
        Height = 22
        Caption = '...'
        OnClick = SbVSArtCabClick
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 20
        Width = 425
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPDtHrAberto: TdmkEditDateTimePicker
        Left = 720
        Top = 20
        Width = 108
        Height = 21
        Date = 44872.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdDtHrAberto: TdmkEdit
        Left = 828
        Top = 20
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryName = 'QrVSGerArt'
        QryCampo = 'DtHrAberto'
        UpdCampo = 'DtHrAberto'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 872
        Top = 20
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 60
        Width = 429
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryName = 'QrVSGerArt'
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGTipoArea: TdmkRadioGroup
        Left = 940
        Top = 4
        Width = 57
        Height = 77
        Caption = ' '#193'rea:'
        ItemIndex = 0
        Items.Strings = (
          'm'#178
          'ft'#178)
        TabOrder = 11
        QryCampo = 'TipoArea'
        UpdCampo = 'TipoArea'
        UpdType = utYes
        OldValor = 0
      end
      object EdLPFMO: TdmkEdit
        Left = 448
        Top = 60
        Width = 193
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNFeRem: TdmkEdit
        Left = 836
        Top = 60
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdSerieRem: TdmkEdit
        Left = 804
        Top = 60
        Width = 29
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdVSArtCab: TdmkEditCB
        Left = 16
        Top = 100
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'VSArtCab'
        UpdCampo = 'VSArtCab'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBVSArtCab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBVSArtCab: TdmkDBLookupComboBox
        Left = 72
        Top = 100
        Width = 525
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVSArtCab
        TabOrder = 13
        dmkEditCB = EdVSArtCab
        QryCampo = 'VSArtCab'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdEmitGru: TdmkEditCB
        Left = 624
        Top = 100
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmitGru
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBEmitGru: TdmkDBLookupComboBox
        Left = 680
        Top = 100
        Width = 321
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEmitGru
        TabOrder = 15
        dmkEditCB = EdEmitGru
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdEmitGrLote: TdmkEdit
        Left = 644
        Top = 60
        Width = 157
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 590
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 4
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 14
        Top = 18
        Width = 119
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita2: TGroupBox
      Left = 0
      Top = 171
      Width = 1008
      Height = 166
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 2
      object Label3: TLabel
        Left = 16
        Top = 4
        Width = 117
        Height = 13
        Caption = 'Artigo em processo  [F4]:'
      end
      object Label10: TLabel
        Left = 324
        Top = 124
        Width = 56
        Height = 13
        Caption = '$ Total MO:'
        Enabled = False
      end
      object LaPecas: TLabel
        Left = 112
        Top = 124
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object LaPeso: TLabel
        Left = 188
        Top = 124
        Width = 27
        Height = 13
        Caption = 'Peso:'
        Enabled = False
      end
      object Label12: TLabel
        Left = 372
        Top = 84
        Width = 413
        Height = 13
        Caption = 
          'Observa'#231#227'o sobre o artigo em opera'#231#227'o (IMEI - Ser'#225' impresso na O' +
          'rdem de Opera'#231#227'o):'
      end
      object Label19: TLabel
        Left = 264
        Top = 124
        Width = 56
        Height = 13
        Caption = 'ID G'#234'meos:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label35: TLabel
        Left = 16
        Top = 44
        Width = 239
        Height = 13
        Caption = 'Fornecedor da m'#227'o de obra (prestador do servi'#231'o):'
      end
      object Label48: TLabel
        Left = 16
        Top = 124
        Width = 66
        Height = 13
        Caption = '$/m'#178' MO [F4]:'
      end
      object Label49: TLabel
        Left = 372
        Top = 44
        Width = 60
        Height = 13
        Caption = 'Localiza'#231#227'o:'
      end
      object Label50: TLabel
        Left = 836
        Top = 56
        Width = 42
        Height = 13
        Caption = 'N'#176' RME:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label54: TLabel
        Left = 16
        Top = 84
        Width = 56
        Height = 13
        Caption = 'Cliente [F4]:'
      end
      object Label63: TLabel
        Left = 552
        Top = 4
        Width = 105
        Height = 13
        Caption = 'Artigo a ser produzido:'
      end
      object SbGraGruX: TSpeedButton
        Left = 528
        Top = 20
        Width = 21
        Height = 22
        Caption = '...'
        OnClick = SbGraGruXClick
      end
      object SpeedButton5: TSpeedButton
        Left = 980
        Top = 20
        Width = 21
        Height = 22
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object EdGraGruX: TdmkEditCB
        Left = 16
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdGraGruXKeyDown
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 72
        Top = 20
        Width = 456
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 1
        OnKeyDown = CBGraGruXKeyDown
        dmkEditCB = EdGraGruX
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCustoMOTot: TdmkEdit
        Left = 324
        Top = 140
        Width = 94
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 17
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPecas: TdmkEdit
        Left = 112
        Top = 140
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 14
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPesoKg: TdmkEdit
        Left = 188
        Top = 140
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 15
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdObserv: TdmkEdit
        Left = 372
        Top = 100
        Width = 557
        Height = 21
        TabOrder = 12
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdMovimTwn: TdmkEdit
        Left = 264
        Top = 140
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdFornecMO: TdmkEditCB
        Left = 16
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornecMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBFornecMO: TdmkDBLookupComboBox
        Left = 72
        Top = 60
        Width = 297
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsPrestador
        TabOrder = 6
        dmkEditCB = EdFornecMO
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCustoMOM2: TdmkEdit
        Left = 16
        Top = 140
        Width = 94
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnKeyDown = EdCustoMOM2KeyDown
      end
      object EdStqCenLoc: TdmkEditCB
        Left = 372
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenLoc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBStqCenLoc: TdmkDBLookupComboBox
        Left = 428
        Top = 60
        Width = 405
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_LOC_CEN'
        ListSource = DsStqCenLoc
        TabOrder = 8
        dmkEditCB = EdStqCenLoc
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdReqMovEstq: TdmkEdit
        Left = 836
        Top = 72
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCliente: TdmkEditCB
        Left = 16
        Top = 100
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdClienteKeyDown
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 72
        Top = 100
        Width = 297
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsCliente
        TabOrder = 11
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CkCpermitirCrust: TCheckBox
        Left = 668
        Top = 2
        Width = 141
        Height = 17
        Caption = 'Permitir semi e acabado.'
        Checked = True
        State = cbChecked
        TabOrder = 2
        OnClick = CkCpermitirCrustClick
      end
      object EdGGXDst: TdmkEditCB
        Left = 552
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GGXDst'
        UpdCampo = 'GGXDst'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGGXDst
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBGGXDst: TdmkDBLookupComboBox
        Left = 608
        Top = 20
        Width = 372
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGGXDst
        TabOrder = 4
        dmkEditCB = EdGGXDst
        QryCampo = 'GGXDst'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBVSPedIts: TGroupBox
      Left = 0
      Top = 337
      Width = 1008
      Height = 46
      Align = alTop
      TabOrder = 3
      object LaPedItsLib: TLabel
        Left = 16
        Top = 4
        Width = 114
        Height = 13
        Caption = 'Item de pedido atrelado:'
      end
      object Label62: TLabel
        Left = 456
        Top = 4
        Width = 74
        Height = 13
        Caption = 'Dono do couro:'
      end
      object EdPedItsLib: TdmkEditCB
        Left = 16
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPedItsLib
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPedItsLib: TdmkDBLookupComboBox
        Left = 72
        Top = 20
        Width = 381
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsVSPedIts
        TabOrder = 1
        dmkEditCB = EdPedItsLib
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdClientMO: TdmkEditCB
        Left = 456
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClientMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBClientMO: TdmkDBLookupComboBox
        Left = 512
        Top = 20
        Width = 489
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientMO
        TabOrder = 3
        dmkEditCB = EdClientMO
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBConfig: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 45
      Align = alTop
      TabOrder = 0
      object Label6: TLabel
        Left = 16
        Top = 4
        Width = 28
        Height = 13
        Caption = 'IME-I:'
      end
      object Label64: TLabel
        Left = 100
        Top = 4
        Width = 123
        Height = 13
        Caption = 'Configura'#231#227'o de inclus'#227'o:'
      end
      object SbVSCOPCabCad: TSpeedButton
        Left = 956
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbVSCOPCabCadClick
      end
      object SbVSCOPCabCpy: TSpeedButton
        Left = 976
        Top = 20
        Width = 21
        Height = 21
        Caption = '>'
        OnClick = SbVSCOPCabCpyClick
      end
      object EdControle: TdmkEdit
        Left = 16
        Top = 20
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdVSCOPCab: TdmkEditCB
        Left = 100
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBVSCOPCab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBVSCOPCab: TdmkDBLookupComboBox
        Left = 156
        Top = 20
        Width = 797
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVSCOPCab
        TabOrder = 2
        dmkEditCB = EdVSCOPCab
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 334
        Height = 32
        Caption = 'Processo de Semi Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 334
        Height = 32
        Caption = 'Processo de Semi Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 334
        Height = 32
        Caption = 'Processo de Semi Acabado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 19
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrVSPWECab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSPWECabBeforeOpen
    AfterOpen = QrVSPWECabAfterOpen
    BeforeClose = QrVSPWECabBeforeClose
    AfterScroll = QrVSPWECabAfterScroll
    OnCalcFields = QrVSPWECabCalcFields
    SQL.Strings = (
      'SELECT '
      'IF(PecasMan<>0, PecasMan, PecasSrc) PecasINI,'
      'IF(AreaManM2<>0, AreaManM2, AreaSrcM2) AreaINIM2,'
      'IF(AreaManP2<>0, AreaManP2, AreaSrcM2) AreaINIP2,'
      'IF(PesoKgMan<>0, PesoKgMan, PesoKgSrc) PesoKgINI,'
      'voc.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA'
      'FROM vsopecab voc'
      'LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa'
      'WHERE voc.Codigo > 0')
    Left = 120
    Top = 465
    object QrVSPWECabNO_PRD_TAM_COR_DST: TWideStringField
      FieldName = 'NO_PRD_TAM_COR_DST'
      Size = 255
    end
    object QrVSPWECabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'vsopecab.Codigo'
    end
    object QrVSPWECabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Origin = 'vsopecab.MovimCod'
    end
    object QrVSPWECabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'vsopecab.Empresa'
    end
    object QrVSPWECabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPWECabDtHrAberto: TDateTimeField
      FieldName = 'DtHrAberto'
      Origin = 'vsopecab.DtHrAberto'
    end
    object QrVSPWECabDtHrLibOpe: TDateTimeField
      FieldName = 'DtHrLibOpe'
      Origin = 'vsopecab.DtHrLibOpe'
    end
    object QrVSPWECabDtHrCfgOpe: TDateTimeField
      FieldName = 'DtHrCfgOpe'
      Origin = 'vsopecab.DtHrCfgOpe'
    end
    object QrVSPWECabDtHrFimOpe: TDateTimeField
      FieldName = 'DtHrFimOpe'
      Origin = 'vsopecab.DtHrFimOpe'
    end
    object QrVSPWECabNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'vsopecab.Nome'
      Size = 100
    end
    object QrVSPWECabLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'vsopecab.Lk'
    end
    object QrVSPWECabDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'vsopecab.DataCad'
    end
    object QrVSPWECabDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'vsopecab.DataAlt'
    end
    object QrVSPWECabUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'vsopecab.UserCad'
    end
    object QrVSPWECabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'vsopecab.UserAlt'
    end
    object QrVSPWECabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'vsopecab.AlterWeb'
    end
    object QrVSPWECabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'vsopecab.Ativo'
    end
    object QrVSPWECabPecasMan: TFloatField
      FieldName = 'PecasMan'
      Origin = 'vsopecab.PecasMan'
    end
    object QrVSPWECabAreaManM2: TFloatField
      FieldName = 'AreaManM2'
      Origin = 'vsopecab.AreaManM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabAreaManP2: TFloatField
      FieldName = 'AreaManP2'
      Origin = 'vsopecab.AreaManP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabTipoArea: TSmallintField
      FieldName = 'TipoArea'
      Origin = 'vsopecab.TipoArea'
    end
    object QrVSPWECabNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 10
      Calculated = True
    end
    object QrVSPWECabNO_DtHrFimOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrFimOpe'
      Calculated = True
    end
    object QrVSPWECabNO_DtHrLibOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrLibOpe'
      Calculated = True
    end
    object QrVSPWECabNO_DtHrCfgOpe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_DtHrCfgOpe'
      Calculated = True
    end
    object QrVSPWECabCacCod: TIntegerField
      FieldName = 'CacCod'
      Origin = 'vsopecab.CacCod'
    end
    object QrVSPWECabGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'vsopecab.GraGruX'
    end
    object QrVSPWECabCustoManMOKg: TFloatField
      FieldName = 'CustoManMOKg'
      Origin = 'vsopecab.CustoManMOKg'
    end
    object QrVSPWECabCustoManMOTot: TFloatField
      FieldName = 'CustoManMOTot'
      Origin = 'vsopecab.CustoManMOTot'
    end
    object QrVSPWECabValorManMP: TFloatField
      FieldName = 'ValorManMP'
      Origin = 'vsopecab.ValorManMP'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabValorManT: TFloatField
      FieldName = 'ValorManT'
      Origin = 'vsopecab.ValorManT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabPecasSrc: TFloatField
      FieldName = 'PecasSrc'
      Origin = 'vsopecab.PecasSrc'
    end
    object QrVSPWECabAreaSrcM2: TFloatField
      FieldName = 'AreaSrcM2'
      Origin = 'vsopecab.AreaSrcM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabAreaSrcP2: TFloatField
      FieldName = 'AreaSrcP2'
      Origin = 'vsopecab.AreaSrcP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabPecasDst: TFloatField
      FieldName = 'PecasDst'
      Origin = 'vsopecab.PecasDst'
    end
    object QrVSPWECabAreaDstM2: TFloatField
      FieldName = 'AreaDstM2'
      Origin = 'vsopecab.AreaDstM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabAreaDstP2: TFloatField
      FieldName = 'AreaDstP2'
      Origin = 'vsopecab.AreaDstP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabPecasSdo: TFloatField
      FieldName = 'PecasSdo'
      Origin = 'vsopecab.PecasSdo'
    end
    object QrVSPWECabAreaSdoM2: TFloatField
      FieldName = 'AreaSdoM2'
      Origin = 'vsopecab.AreaSdoM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabAreaSdoP2: TFloatField
      FieldName = 'AreaSdoP2'
      Origin = 'vsopecab.AreaSdoP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabPesoKgSrc: TFloatField
      FieldName = 'PesoKgSrc'
      Origin = 'vsopecab.PesoKgSrc'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPWECabPesoKgMan: TFloatField
      FieldName = 'PesoKgMan'
      Origin = 'vsopecab.PesoKgMan'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPWECabPesoKgDst: TFloatField
      FieldName = 'PesoKgDst'
      Origin = 'vsopecab.PesoKgDst'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPWECabPesoKgSdo: TFloatField
      FieldName = 'PesoKgSdo'
      Origin = 'vsopecab.PesoKgSdo'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPWECabValorTMan: TFloatField
      FieldName = 'ValorTMan'
      Origin = 'vsopecab.ValorTMan'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabValorTSrc: TFloatField
      FieldName = 'ValorTSrc'
      Origin = 'vsopecab.ValorTSrc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabValorTSdo: TFloatField
      FieldName = 'ValorTSdo'
      Origin = 'vsopecab.ValorTSdo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabPecasINI: TFloatField
      FieldName = 'PecasINI'
      Required = True
    end
    object QrVSPWECabAreaINIM2: TFloatField
      FieldName = 'AreaINIM2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabAreaINIP2: TFloatField
      FieldName = 'AreaINIP2'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabPesoKgINI: TFloatField
      FieldName = 'PesoKgINI'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPWECabPesoKgBxa: TFloatField
      FieldName = 'PesoKgBxa'
      Origin = 'vsopecab.PesoKgBxa'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSPWECabPecasBxa: TFloatField
      FieldName = 'PecasBxa'
      Origin = 'vsopecab.PecasBxa'
    end
    object QrVSPWECabAreaBxaM2: TFloatField
      FieldName = 'AreaBxaM2'
      Origin = 'vsopecab.AreaBxaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabAreaBxaP2: TFloatField
      FieldName = 'AreaBxaP2'
      Origin = 'vsopecab.AreaBxaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabValorTBxa: TFloatField
      FieldName = 'ValorTBxa'
      Origin = 'vsopecab.ValorTBxa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSPWECabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrVSPWECabNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 100
    end
    object QrVSPWECabNFeRem: TIntegerField
      FieldName = 'NFeRem'
    end
    object QrVSPWECabLPFMO: TWideStringField
      FieldName = 'LPFMO'
      Size = 30
    end
    object QrVSPWECabTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSPWECabGGXDst: TIntegerField
      FieldName = 'GGXDst'
    end
    object QrVSPWECabSerieRem: TSmallintField
      FieldName = 'SerieRem'
    end
    object QrVSPWECabVSCOPCab: TIntegerField
      FieldName = 'VSCOPCab'
    end
    object QrVSPWECabNO_VSCOPCab: TWideStringField
      FieldName = 'NO_VSCOPCab'
      Size = 60
    end
    object QrVSPWECabEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrVSPWECabNO_EmitGru: TWideStringField
      FieldName = 'NO_EmitGru'
      Size = 60
    end
    object QrVSPWECabVSArtCab: TIntegerField
      FieldName = 'VSArtCab'
    end
    object QrVSPWECabNO_VSArtCab: TWideStringField
      FieldName = 'NO_VSArtCab'
      Size = 255
    end
    object QrVSPWECabEmitGrLote: TWideStringField
      FieldName = 'EmitGrLote'
      Size = 60
    end
    object QrVSPWECabValorTDst: TFloatField
      FieldName = 'ValorTDst'
      Required = True
    end
    object QrVSPWECabKgCouPQ: TFloatField
      FieldName = 'KgCouPQ'
      Required = True
    end
    object QrVSPWECabNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
      Required = True
    end
    object QrVSPWECabGGXSrc: TIntegerField
      FieldName = 'GGXSrc'
      Required = True
    end
    object QrVSPWECabOperacoes: TIntegerField
      FieldName = 'Operacoes'
      Required = True
    end
    object QrVSPWECabVSVmcWrn: TSmallintField
      FieldName = 'VSVmcWrn'
      Required = True
    end
    object QrVSPWECabVSVmcObs: TWideStringField
      FieldName = 'VSVmcObs'
      Size = 60
    end
    object QrVSPWECabVSVmcSeq: TWideStringField
      FieldName = 'VSVmcSeq'
      Size = 25
    end
    object QrVSPWECabVSVmcSta: TSmallintField
      FieldName = 'VSVmcSta'
      Required = True
    end
    object QrVSPWECabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrVSPWECabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrVSPWECabLinCulReb: TIntegerField
      FieldName = 'LinCulReb'
      Required = True
    end
    object QrVSPWECabLinCabReb: TIntegerField
      FieldName = 'LinCabReb'
      Required = True
    end
    object QrVSPWECabLinCulSem: TIntegerField
      FieldName = 'LinCulSem'
      Required = True
    end
    object QrVSPWECabLinCabSem: TIntegerField
      FieldName = 'LinCabSem'
      Required = True
    end
    object QrVSPWECabDataPed: TDateTimeField
      FieldName = 'DataPed'
      Required = True
    end
    object QrVSPWECabDataStart: TDateTimeField
      FieldName = 'DataStart'
      Required = True
    end
    object QrVSPWECabDataCrust: TDateTimeField
      FieldName = 'DataCrust'
      Required = True
    end
    object QrVSPWECabDataExped: TDateTimeField
      FieldName = 'DataExped'
      Required = True
    end
    object QrVSPWECabReceiRecu: TIntegerField
      FieldName = 'ReceiRecu'
      Required = True
    end
    object QrVSPWECabReceiRefu: TIntegerField
      FieldName = 'ReceiRefu'
      Required = True
    end
    object QrVSPWECabClasses: TWideStringField
      FieldName = 'Classes'
      Size = 30
    end
  end
  object DsVSPWECab: TDataSource
    DataSet = QrVSPWECab
    Left = 120
    Top = 509
  end
  object QrVSPWEAtu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(wmi.Terceiro=0, "V'#225'rios", '
      '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)'
      ') NO_FORNECE, '
      'IF(wmi.Ficha=0, "V'#225'rias", CONCAT(IF(vsf.Nome IS NULL, '
      '"?", vsf.Nome), " ", wmi.Ficha)) NO_FICHA, '
      'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, '
      'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2 '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  ent ON ent.Codigo=wmi.Terceiro'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch ')
    Left = 204
    Top = 461
    object QrVSPWEAtuCusFrtMORet: TFloatField
      FieldName = 'CusFrtMORet'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuCusFrtMOEnv: TFloatField
      FieldName = 'CusFrtMOEnv'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPWEAtuControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPWEAtuMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPWEAtuMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPWEAtuMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPWEAtuEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPWEAtuTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPWEAtuCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPWEAtuMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPWEAtuDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPWEAtuPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPWEAtuGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPWEAtuPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEAtuPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEAtuAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPWEAtuSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPWEAtuSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPWEAtuSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPWEAtuSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPWEAtuSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEAtuSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPWEAtuSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPWEAtuFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPWEAtuMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPWEAtuFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPWEAtuCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSPWEAtuCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrVSPWEAtuCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPWEAtuDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPWEAtuDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPWEAtuDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPWEAtuQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPWEAtuQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEAtuQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPWEAtuQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEAtuQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPWEAtuNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPWEAtuNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPWEAtuNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPWEAtuID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPWEAtuNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPWEAtuReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSPWEAtuCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSPWEAtuCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSPWEAtuNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrVSPWEAtuMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSPWEAtuPedItsLib: TLargeintField
      FieldName = 'PedItsLib'
    end
    object QrVSPWEAtuStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSPWEAtuNO_FICHA: TWideStringField
      DisplayWidth = 80
      FieldName = 'NO_FICHA'
      Size = 80
    end
    object QrVSPWEAtuNO_FORNEC_MO: TWideStringField
      FieldName = 'NO_FORNEC_MO'
      Size = 100
    end
    object QrVSPWEAtuClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSPWEAtuCustoPQ: TFloatField
      FieldName = 'CustoPQ'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEAtuNO_CLIENT_MO: TWideStringField
      FieldName = 'NO_CLIENT_MO'
      Size = 100
    end
  end
  object DsVSPWEAtu: TDataSource
    DataSet = QrVSPWEAtu
    Left = 200
    Top = 509
  end
  object PMOri: TPopupMenu
    OnPopup = PMOriPopup
    Left = 636
    Top = 516
    object ItsIncluiOri: TMenuItem
      Caption = '&Adiciona artigo de origem'
      Enabled = False
      object porPallet1: TMenuItem
        Caption = 'por &Pallet'
        object Parcial1: TMenuItem
          Caption = '&Parcial'
          OnClick = Parcial1Click
        end
        object otal1: TMenuItem
          Caption = '&Total'
          OnClick = otal1Click
        end
      end
      object porIMEI1: TMenuItem
        Caption = 'por &IME-I'
        object Parcial2: TMenuItem
          Caption = '&Parcial'
          OnClick = Parcial2Click
        end
        object otal2: TMenuItem
          Caption = '&Total'
          OnClick = otal2Click
        end
      end
    end
    object ItsAlteraOri: TMenuItem
      Caption = '&Edita artigo de origem (IME-I)'
      Enabled = False
      OnClick = ItsAlteraOriClick
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object ItsExcluiOriIMEI: TMenuItem
      Caption = '&Remove IMEI de origem'
      Enabled = False
      OnClick = ItsExcluiOriIMEIClick
    end
    object ItsExcluiOriPallet: TMenuItem
      Caption = '&Remove Pallet de origem'
      Enabled = False
      OnClick = ItsExcluiOriPalletClick
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object AtrelamentoNFsdeMO1: TMenuItem
      Caption = 'Atrelamento NFs de MO'
      object IncluiAtrelamento1: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento1Click
      end
      object AlteraAtrelamento1: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento1Click
      end
      object ExcluiAtrelamento1: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento1Click
      end
    end
    object N12: TMenuItem
      Caption = '-'
    end
    object InformaNmerodaRME1: TMenuItem
      Caption = '&Informa N'#250'mero da RME'
      Enabled = False
      OnClick = InformaNmerodaRME1Click
    end
    object InformaPginaelinhadeIEC1: TMenuItem
      Caption = 'Informa P'#225'gina e linha de IEC'
      OnClick = InformaPginaelinhadeIEC1Click
    end
    object IrparacadastrodoPallet1: TMenuItem
      Caption = 'Ir para cadastro do &Pallet'
      OnClick = IrparacadastrodoPallet1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 632
    Top = 468
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object IncluidePedidoitemMPV1: TMenuItem
      Caption = 'Inclui de Pedido (item MPV)'
      OnClick = IncluidePedidoitemMPV1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CabLibera1: TMenuItem
      Caption = '&Libera classifica'#231#227'o'
      OnClick = CabLibera1Click
    end
    object CabReeditar1: TMenuItem
      Caption = '&Volta a editar'
      Enabled = False
      OnClick = CabReeditar1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object AtualizaestoqueEmProcesso1: TMenuItem
      Caption = 'Atualiza estoque "Em &Processo"'
      OnClick = AtualizaestoqueEmProcesso1Click
    end
    object Definiodequantidadesmanualmente1: TMenuItem
      Caption = '&Defini'#231#227'o de quantidades manualmente'
      OnClick = Definiodequantidadesmanualmente1Click
    end
    object CorrigirFornecedor1: TMenuItem
      Caption = 'Corrigir Fornecedor'
      OnClick = CorrigirFornecedor1Click
    end
    object AlteraFornecedorMO1: TMenuItem
      Caption = 'Altera'#231#227'o Parcial'
      object FornecedorMO1: TMenuItem
        Caption = 'Fornecedor M&O'
        OnClick = FornecedorMO1Click
      end
      object Localdoestoque1: TMenuItem
        Caption = '&Local do estoque'
        OnClick = Localdoestoque1Click
      end
      object Cliente1: TMenuItem
        Caption = '&Cliente'
        OnClick = Cliente1Click
      end
    end
    object Reabreitensteste1: TMenuItem
      Caption = 'Reabre itens teste'
      OnClick = Reabreitensteste1Click
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsribcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle'
      '')
    Left = 384
    Top = 36
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 384
    Top = 80
  end
  object QrVSPWEOriIMEI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 288
    Top = 461
    object QrVSPWEOriIMEICodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPWEOriIMEIControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPWEOriIMEIMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPWEOriIMEIMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPWEOriIMEIMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPWEOriIMEIEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPWEOriIMEITerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPWEOriIMEICliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPWEOriIMEIMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPWEOriIMEIDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPWEOriIMEIPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPWEOriIMEIGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPWEOriIMEIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEOriIMEIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEOriIMEIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEOriIMEIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEOriIMEIValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEOriIMEISrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPWEOriIMEISrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPWEOriIMEISrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPWEOriIMEISrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPWEOriIMEISdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPWEOriIMEISdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEOriIMEISdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEOriIMEIObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPWEOriIMEISerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPWEOriIMEIFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPWEOriIMEIMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPWEOriIMEIFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPWEOriIMEICustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEOriIMEICustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEOriIMEICustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEOriIMEIValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEOriIMEIDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPWEOriIMEIDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPWEOriIMEIDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPWEOriIMEIDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPWEOriIMEIQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPWEOriIMEIQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEOriIMEIQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEOriIMEIQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEOriIMEIQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPWEOriIMEIQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEOriIMEIQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEOriIMEIQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEOriIMEINotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPWEOriIMEINO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPWEOriIMEINO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPWEOriIMEINO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPWEOriIMEIID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPWEOriIMEINO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPWEOriIMEINO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSPWEOriIMEIReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSPWEOriIMEIItemNFe: TLargeintField
      FieldName = 'ItemNFe'
    end
    object QrVSPWEOriIMEIGGXRcl: TLargeintField
      FieldName = 'GGXRcl'
    end
    object QrVSPWEOriIMEIDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVSPWEOriIMEIIxxMovIX: TLargeintField
      FieldName = 'IxxMovIX'
    end
    object QrVSPWEOriIMEIIxxFolha: TLargeintField
      FieldName = 'IxxFolha'
    end
    object QrVSPWEOriIMEIIxxLinha: TLargeintField
      FieldName = 'IxxLinha'
    end
  end
  object DsVSPWEOriIMEI: TDataSource
    DataSet = QrVSPWEOriIMEI
    Left = 288
    Top = 509
  end
  object QrPrestador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 320
    Top = 36
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 320
    Top = 80
  end
  object PMNumero: TPopupMenu
    Left = 100
    Top = 65532
    object PeloCdigo1: TMenuItem
      Caption = 'Pela &OP (Ordem de Produ'#231#227'o)'
      OnClick = PeloCdigo1Click
    end
    object PeloIMEC1: TMenuItem
      Caption = 'Pelo I&ME-C'
      OnClick = PeloIMEC1Click
    end
    object PeloIMEI1: TMenuItem
      Caption = 'Pelo IME-&I'
      OnClick = PeloIMEI1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 36
    Top = 12
    object OPFluxo1: TMenuItem
      Caption = 'OP Fluxo A'
      OnClick = OPFluxo1Click
    end
    object OPFluxoB1: TMenuItem
      Caption = 'OP Fluxo B'
      OnClick = OPFluxoB1Click
    end
    object IMEIArtigodeRibeiragerado1: TMenuItem
      Caption = '&IME-I  Ordem de Opera'#231#227'o'
      OnClick = IMEIArtigodeRibeiragerado1Click
    end
    object OPpreenchida1: TMenuItem
      Caption = 'OP preenchida'
      OnClick = OPpreenchida1Click
    end
    object OPpreenchidacomorigemedestino1: TMenuItem
      Caption = 'OP preenchida com origem e destino'
      OnClick = OPpreenchidacomorigemedestino1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object OrdensdeProduoemAberto1: TMenuItem
      Caption = 'Ordens de produ'#231#227'o em &aberto'
      OnClick = OrdensdeProduoemAberto1Click
    end
    object Outrasimpresses1: TMenuItem
      Caption = '&Outras impress'#245'es (outra janela)'
      OnClick = Outrasimpresses1Click
    end
    object CobranadeMO1: TMenuItem
      Caption = 'Cobran'#231'a de MO'
      OnClick = CobranadeMO1Click
    end
  end
  object QrVSPWEDst: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPWEDstBeforeClose
    AfterScroll = QrVSPWEDstAfterScroll
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 468
    Top = 473
    object QrVSPWEDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPWEDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPWEDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPWEDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPWEDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPWEDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPWEDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPWEDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPWEDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPWEDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPWEDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPWEDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPWEDstPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEDstPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEDstAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDstAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPWEDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPWEDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPWEDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPWEDstSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPWEDstSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEDstSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPWEDstSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPWEDstFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPWEDstMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPWEDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPWEDstCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDstCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPWEDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPWEDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPWEDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPWEDstQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPWEDstQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEDstQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDstQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDstQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPWEDstQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEDstQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDstQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDstNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPWEDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPWEDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPWEDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPWEDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPWEDstNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPWEDstNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSPWEDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSPWEDstPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSPWEDstMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSPWEDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSPWEDstDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDstCusFrtMORet: TFloatField
      FieldName = 'CusFrtMORet'
    end
    object QrVSPWEDstCustoMOPc: TFloatField
      FieldName = 'CustoMOPc'
    end
  end
  object DsVSPWEDst: TDataSource
    DataSet = QrVSPWEDst
    Left = 468
    Top = 513
  end
  object PMDst: TPopupMenu
    OnPopup = PMDstPopup
    Left = 632
    Top = 564
    object ItsIncluiDst: TMenuItem
      Caption = '&Adiciona artigo de destino'
      Enabled = False
      OnClick = ItsIncluiDstClick
    end
    object ItsAlteraDst: TMenuItem
      Caption = '&Edita artigo de destino'
      Enabled = False
      OnClick = ItsAlteraDstClick
    end
    object Editaea1: TMenuItem
      Caption = 'Edita '#225'rea de baixa'
      OnClick = Editaea1Click
    end
    object Corrigeartigodebaixa1: TMenuItem
      Caption = 'Corrige artigo de baixa'
      Enabled = False
      OnClick = Corrigeartigodebaixa1Click
    end
    object Corrigetodasbaixas1: TMenuItem
      Caption = 'Corrige todas baixas'
      OnClick = Corrigetodasbaixas1Click
    end
    object ItsExcluiDst: TMenuItem
      Caption = '&Remove artigo de destino'
      Enabled = False
      OnClick = ItsExcluiDstClick
    end
    object InformaNmerodaRME2: TMenuItem
      Caption = '&Informa N'#250'mero da RME'
      Enabled = False
      OnClick = InformaNmerodaRME2Click
    end
    object AtrelamentoNFsdeMO2: TMenuItem
      Caption = 'Atrelamento NFs de MO'
      object IncluiAtrelamento2: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento2Click
      end
      object AlteraAtrelamento2: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento2Click
      end
      object ExcluiAtrelamento2: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento2Click
      end
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object AdicionasubprodutoWBRaspa1: TMenuItem
      Caption = 'Adiciona sub-produto WB (Ra&spa)'
      OnClick = AdicionasubprodutoWBRaspa1Click
    end
    object RemovesubprodutoWBRaspa1: TMenuItem
      Caption = 'Remove sub-produto WB (Raspa)'
      OnClick = RemovesubprodutoWBRaspa1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Adicionadesclassificado1: TMenuItem
      Caption = 'Adiciona desclassificado'
      OnClick = Adicionadesclassificado1Click
    end
    object Removedesclassificado1: TMenuItem
      Caption = '&Remove desclassificado'
      OnClick = Removedesclassificado1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object IrparajaneladoPallet1: TMenuItem
      Caption = 'Ir para janela do &Pallet'
      OnClick = IrparajaneladoPallet1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object MudarodestinoparaoutraOP1: TMenuItem
      Caption = 'Mudar o destino para outra OP'
      OnClick = MudarodestinoparaoutraOP1Click
    end
    object AlteraLocaldeEstque1: TMenuItem
      Caption = 'Altera &Local de Estque'
      OnClick = AlteraLocaldeEstque1Click
    end
  end
  object QrTwn: TMySQLQuery
    Database = Dmod.MyDB
    Left = 840
    Top = 384
    object QrTwnControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrVSPWEBxa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 548
    Top = 465
    object QrVSPWEBxaCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPWEBxaControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPWEBxaMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPWEBxaMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPWEBxaMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPWEBxaEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPWEBxaTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPWEBxaCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPWEBxaMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPWEBxaDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPWEBxaPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPWEBxaGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPWEBxaPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEBxaPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEBxaAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPWEBxaSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPWEBxaSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPWEBxaSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPWEBxaSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPWEBxaSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEBxaSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPWEBxaSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPWEBxaFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPWEBxaMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPWEBxaFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPWEBxaCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPWEBxaDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPWEBxaDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPWEBxaDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPWEBxaQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPWEBxaQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEBxaQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPWEBxaQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEBxaQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEBxaNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPWEBxaNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPWEBxaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPWEBxaNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPWEBxaID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPWEBxaNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPWEBxaNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSPWEBxaReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object DsVSPWEBxa: TDataSource
    DataSet = QrVSPWEBxa
    Left = 544
    Top = 513
  end
  object QrVSPWEOriPallet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.Pallet, wmi.GraGruX, wmi.Ficha, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, '
      'SUM(AreaP2) AreaP2, SUM(PesoKg) PesoKg,'
      'vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet'
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch '
      'WHERE wmi.MovimCod=662'
      'AND wmi.MovimNiv=7'
      'GROUP BY Pallet'
      'ORDER BY NO_Pallet')
    Left = 376
    Top = 461
    object QrVSPWEOriPalletPallet: TLargeintField
      FieldName = 'Pallet'
    end
    object QrVSPWEOriPalletGraGruX: TLargeintField
      FieldName = 'GraGruX'
    end
    object QrVSPWEOriPalletPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSPWEOriPalletAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEOriPalletAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEOriPalletPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEOriPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPWEOriPalletNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrVSPWEOriPalletSerieFch: TLargeintField
      FieldName = 'SerieFch'
    end
    object QrVSPWEOriPalletFicha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrVSPWEOriPalletNO_TTW: TWideStringField
      FieldName = 'NO_TTW'
      Required = True
      Size = 5
    end
    object QrVSPWEOriPalletID_TTW: TLargeintField
      FieldName = 'ID_TTW'
      Required = True
    end
    object QrVSPWEOriPalletSeries_E_Fichas: TWideStringField
      FieldName = 'Series_E_Fichas'
      Size = 1
    end
    object QrVSPWEOriPalletTerceiro: TLargeintField
      FieldName = 'Terceiro'
    end
    object QrVSPWEOriPalletMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSPWEOriPalletNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPWEOriPalletNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSPWEOriPalletValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSPWEOriPalletSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPWEOriPalletSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEOriPalletSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsVSPWEOriPallet: TDataSource
    DataSet = QrVSPWEOriPallet
    Left = 376
    Top = 509
  end
  object QrForcados: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmovits'
      'WHERE MovimID=9 '
      'AND SrcMovID=11'
      'AND SrcNivel1=5'
      '')
    Left = 908
    Top = 420
    object QrForcadosCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrForcadosControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrForcadosMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrForcadosMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrForcadosMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrForcadosEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrForcadosTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrForcadosCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrForcadosMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrForcadosDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrForcadosPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrForcadosGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrForcadosPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrForcadosSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrForcadosSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrForcadosSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrForcadosSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrForcadosSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrForcadosSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrForcadosFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrForcadosMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrForcadosFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrForcadosCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrForcadosDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrForcadosDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrForcadosDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrForcadosQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrForcadosQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrForcadosQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrForcadosQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrForcadosNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrForcadosNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrForcadosNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrForcadosNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrForcadosID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
  end
  object DsForcados: TDataSource
    DataSet = QrForcados
    Left = 908
    Top = 468
  end
  object QrVSPedIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vpi.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),'
      '" ", vpc.PedidCli, " ", Texto) '
      'NO_PRD_TAM_COR'
      'FROM vspedits vpi'
      'LEFT JOIN vspedcab   vpc ON vpc.Codigo=vpi.Codigo'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vpi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE vpi.VSWetEnd=0')
    Left = 708
    Top = 468
    object QrVSPedItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrVSPedItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsVSPedIts: TDataSource
    DataSet = QrVSPedIts
    Left = 708
    Top = 512
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 252
    Top = 80
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 252
    Top = 36
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object QrCliente: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 188
    Top = 36
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrClienteNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 188
    Top = 80
  end
  object PMNovo: TPopupMenu
    Left = 68
    Top = 56
    object CorrigeFornecedoresdestinoapartirdestaoperao1: TMenuItem
      Caption = 'Corrige &Fornecedores destino a partir deste processo'
      OnClick = CorrigeFornecedoresdestinoapartirdestaoperao1Click
    end
    object CorrigeMO1: TMenuItem
      Caption = 'Corrige &MO'
      OnClick = CorrigeMO1Click
    end
  end
  object QrClientMO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 508
    Top = 36
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientMO: TDataSource
    DataSet = QrClientMO
    Left = 508
    Top = 84
  end
  object QrGGXDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,'
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsribcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle'
      '')
    Left = 916
    Top = 104
    object QrGGXDstGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXDstControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXDstSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXDstCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXDstNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXDst: TDataSource
    DataSet = QrGGXDst
    Left = 960
    Top = 148
  end
  object QrVSCOPCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM operacoes'
      'ORDER BY Nome')
    Left = 400
    Top = 188
    object QrVSCOPCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSCOPCabNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSCOPCab: TDataSource
    DataSet = QrVSCOPCab
    Left = 400
    Top = 240
  end
  object QrVSPWEDesclDst: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrVSPWEDesclDstAfterScroll
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 812
    Top = 461
    object QrVSPWEDesclDstCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPWEDesclDstControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPWEDesclDstMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPWEDesclDstMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPWEDesclDstMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPWEDesclDstEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPWEDesclDstTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPWEDesclDstCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPWEDesclDstMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPWEDesclDstDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPWEDesclDstPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPWEDesclDstGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPWEDesclDstPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEDesclDstPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEDesclDstAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclDstAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclDstValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclDstSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPWEDesclDstSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPWEDesclDstSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPWEDesclDstSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPWEDesclDstSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPWEDesclDstSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEDesclDstSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclDstObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPWEDesclDstSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPWEDesclDstFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPWEDesclDstMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPWEDesclDstFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPWEDesclDstCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclDstCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclDstCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclDstValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclDstDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPWEDesclDstDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPWEDesclDstDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPWEDesclDstDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPWEDesclDstQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPWEDesclDstQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEDesclDstQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclDstQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclDstQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPWEDesclDstQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEDesclDstQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclDstQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclDstNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPWEDesclDstNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPWEDesclDstNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPWEDesclDstNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPWEDesclDstID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPWEDesclDstNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPWEDesclDstNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSPWEDesclDstReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSPWEDesclDstPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSPWEDesclDstMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSPWEDesclDstStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSPWEDesclDstDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
  end
  object DsVSPWEDesclDst: TDataSource
    DataSet = QrVSPWEDesclDst
    Left = 812
    Top = 509
  end
  object QrVSPWEDesclBxa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 912
    Top = 517
    object QrVSPWEDesclBxaCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPWEDesclBxaControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPWEDesclBxaMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPWEDesclBxaMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPWEDesclBxaMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPWEDesclBxaEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPWEDesclBxaTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPWEDesclBxaCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPWEDesclBxaMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPWEDesclBxaDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPWEDesclBxaPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPWEDesclBxaGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPWEDesclBxaPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEDesclBxaPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEDesclBxaAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclBxaAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclBxaValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclBxaSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPWEDesclBxaSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPWEDesclBxaSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPWEDesclBxaSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPWEDesclBxaSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPWEDesclBxaSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEDesclBxaSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclBxaObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPWEDesclBxaSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPWEDesclBxaFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPWEDesclBxaMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPWEDesclBxaFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPWEDesclBxaCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclBxaCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclBxaCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclBxaValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclBxaDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPWEDesclBxaDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPWEDesclBxaDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPWEDesclBxaDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPWEDesclBxaQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPWEDesclBxaQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEDesclBxaQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclBxaQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclBxaQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPWEDesclBxaQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWEDesclBxaQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclBxaQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWEDesclBxaNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPWEDesclBxaNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPWEDesclBxaNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPWEDesclBxaNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPWEDesclBxaID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPWEDesclBxaNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPWEDesclBxaNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSPWEDesclBxaReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object DsVSPWEDesclBxa: TDataSource
    DataSet = QrVSPWEDesclBxa
    Left = 908
    Top = 565
  end
  object QrEmit: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM emit')
    Left = 756
    Top = 448
    object QrEmitCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitDataEmis: TDateTimeField
      FieldName = 'DataEmis'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrEmitStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrEmitNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrEmitNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrEmitNOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrEmitTecnico: TWideStringField
      FieldName = 'Tecnico'
    end
    object QrEmitNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrEmitClienteI: TIntegerField
      FieldName = 'ClienteI'
    end
    object QrEmitTipificacao: TSmallintField
      FieldName = 'Tipificacao'
    end
    object QrEmitTempoR: TIntegerField
      FieldName = 'TempoR'
    end
    object QrEmitTempoP: TIntegerField
      FieldName = 'TempoP'
    end
    object QrEmitSetor: TSmallintField
      FieldName = 'Setor'
    end
    object QrEmitTipific: TSmallintField
      FieldName = 'Tipific'
    end
    object QrEmitEspessura: TWideStringField
      FieldName = 'Espessura'
      Size = 15
    end
    object QrEmitDefPeca: TWideStringField
      FieldName = 'DefPeca'
      Size = 15
    end
    object QrEmitPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrEmitCusto: TFloatField
      FieldName = 'Custo'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEmitQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmitAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEmitFulao: TWideStringField
      FieldName = 'Fulao'
      Size = 5
    end
    object QrEmitObs: TWideStringField
      FieldName = 'Obs'
      Size = 255
    end
    object QrEmitSetrEmi: TSmallintField
      FieldName = 'SetrEmi'
    end
    object QrEmitSourcMP: TSmallintField
      FieldName = 'SourcMP'
    end
    object QrEmitCod_Espess: TIntegerField
      FieldName = 'Cod_Espess'
    end
    object QrEmitCodDefPeca: TIntegerField
      FieldName = 'CodDefPeca'
    end
    object QrEmitCustoTo: TFloatField
      FieldName = 'CustoTo'
    end
    object QrEmitCustoKg: TFloatField
      FieldName = 'CustoKg'
    end
    object QrEmitCustoM2: TFloatField
      FieldName = 'CustoM2'
    end
    object QrEmitCusUSM2: TFloatField
      FieldName = 'CusUSM2'
    end
    object QrEmitLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmitDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmitDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmitUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmitUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmitAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEmitAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEmitEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrEmitRetrabalho: TSmallintField
      FieldName = 'Retrabalho'
    end
    object QrEmitSemiCodPeca: TIntegerField
      FieldName = 'SemiCodPeca'
    end
    object QrEmitSemiTxtPeca: TWideStringField
      FieldName = 'SemiTxtPeca'
      Size = 15
    end
    object QrEmitSemiPeso: TFloatField
      FieldName = 'SemiPeso'
    end
    object QrEmitSemiQtde: TFloatField
      FieldName = 'SemiQtde'
    end
    object QrEmitSemiAreaM2: TFloatField
      FieldName = 'SemiAreaM2'
    end
    object QrEmitSemiRendim: TFloatField
      FieldName = 'SemiRendim'
    end
    object QrEmitSemiCodEspe: TIntegerField
      FieldName = 'SemiCodEspe'
    end
    object QrEmitSemiTxtEspe: TWideStringField
      FieldName = 'SemiTxtEspe'
      Size = 15
    end
    object QrEmitBRL_USD: TFloatField
      FieldName = 'BRL_USD'
    end
    object QrEmitBRL_EUR: TFloatField
      FieldName = 'BRL_EUR'
    end
    object QrEmitDtaCambio: TDateField
      FieldName = 'DtaCambio'
    end
    object QrEmitVSMovCod: TIntegerField
      FieldName = 'VSMovCod'
    end
    object QrEmitDtCorrApo_TXT: TWideStringField
      FieldName = 'DtCorrApo_TXT'
      Size = 10
    end
    object QrEmitDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrEmitDtaBaixa_TXT: TWideStringField
      FieldName = 'DtaBaixa_TXT'
      Size = 10
    end
    object QrEmitDtaBaixa: TDateField
      FieldName = 'DtaBaixa'
    end
  end
  object DsEmit: TDataSource
    DataSet = QrEmit
    Left = 756
    Top = 496
  end
  object PMPesagem: TPopupMenu
    OnPopup = PMPesagemPopup
    Left = 572
    Top = 636
    object EmitePesagem2: TMenuItem
      Caption = '&Pesagem por receita'
      OnClick = EmitePesagem2Click
      object Emitepesagem1: TMenuItem
        Caption = '&Emite pesagem'
        object Recurtimento1: TMenuItem
          Caption = '&Recurtimento'
          OnClick = Recurtimento1Click
        end
        object Acabamento1: TMenuItem
          Caption = '&Acabamento'
          OnClick = Acabamento1Click
        end
      end
      object Reimprimereceita1: TMenuItem
        Caption = '&Reimprime receita'
        OnClick = Reimprimereceita1Click
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object ExcluiPesagem1: TMenuItem
        Caption = 'E&xclui Pesagem'
        OnClick = ExcluiPesagem1Click
      end
    end
    object Emiteoutrasbaixas1: TMenuItem
      Caption = '&Outras baixas'
      OnClick = Emiteoutrasbaixas1Click
      object Novogrupo1: TMenuItem
        Caption = 'Nova pesagem'
        OnClick = Novogrupo1Click
      end
      object Novoitem1: TMenuItem
        Caption = 'Novo item'
        OnClick = Novoitem1Click
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object Excluiitem1: TMenuItem
        Caption = 'Exclui item'
        OnClick = Excluiitem1Click
      end
      object Excluigrupo1: TMenuItem
        Caption = 'Exclui toda pesagem'
        OnClick = Excluigrupo1Click
      end
    end
    object Recalculacusto1: TMenuItem
      Caption = 'Recalcula custo'
      OnClick = Recalculacusto1Click
    end
  end
  object QrEmitGru: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM emitgru'
      'WHERE Ativo=1'
      'ORDER BY Nome'
      '')
    Left = 444
    Top = 37
    object QrEmitGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEmitGru: TDataSource
    DataSet = QrEmitGru
    Left = 444
    Top = 81
  end
  object QrPQO: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPQOBeforeClose
    AfterScroll = QrPQOAfterScroll
    SQL.Strings = (
      'SELECT lse.Nome NOMESETOR,'
      'emg.Nome NO_EMITGRU, pqo.*'
      'FROM pqo pqo'
      'LEFT JOIN listasetores lse ON lse.Codigo=pqo.Setor'
      'LEFT JOIN emitgru emg ON emg.Codigo=pqo.EmitGru'
      'WHERE pqo.Codigo > 0'
      '')
    Left = 548
    Top = 221
    object QrPQOCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQOSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrPQOCustoInsumo: TFloatField
      FieldName = 'CustoInsumo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQOCustoTotal: TFloatField
      FieldName = 'CustoTotal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPQODataB: TDateField
      FieldName = 'DataB'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPQOLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPQODataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPQODataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPQOUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPQOUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPQONOMESETOR: TWideStringField
      FieldName = 'NOMESETOR'
    end
    object QrPQONO_EMITGRU: TWideStringField
      FieldName = 'NO_EMITGRU'
      Size = 60
    end
    object QrPQOAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPQOAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPQOEmitGru: TIntegerField
      FieldName = 'EmitGru'
    end
    object QrPQONome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPQO: TDataSource
    DataSet = QrPQO
    Left = 548
    Top = 265
  end
  object QrPQOIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pqx.*, pq_.Nome NOMEPQ, pq_.GrupoQuimico, '
      'pqg.Nome NOMEGRUPO'
      'FROM pqx pqx'
      'LEFT JOIN pqcli pci ON pci.PQ=pqx.Insumo'
      'LEFT JOIN pq    pq_ ON pq_.Codigo=pci.PQ'
      'LEFT JOIN pqg   pqg ON pqg.Codigo=pq_.GrupoQuimico'
      'WHERE pqx.Tipo=190'
      'AND pqx.OrigemCodi=:P0')
    Left = 596
    Top = 225
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPQOItsDataX: TDateField
      FieldName = 'DataX'
    end
    object QrPQOItsTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPQOItsCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrPQOItsCliDest: TIntegerField
      FieldName = 'CliDest'
    end
    object QrPQOItsInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrPQOItsPeso: TFloatField
      FieldName = 'Peso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrPQOItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPQOItsOrigemCodi: TIntegerField
      FieldName = 'OrigemCodi'
    end
    object QrPQOItsOrigemCtrl: TIntegerField
      FieldName = 'OrigemCtrl'
    end
    object QrPQOItsNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrPQOItsGrupoQuimico: TIntegerField
      FieldName = 'GrupoQuimico'
    end
    object QrPQOItsNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 100
    end
  end
  object DsPQOIts: TDataSource
    DataSet = QrPQOIts
    Left = 644
    Top = 269
  end
  object QrVSPWESubPrd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspalleta   wbp ON wbp.Codigo=wmi.Pallet '
      'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro')
    Left = 772
    Top = 573
    object QrVSPWESubPrdCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPWESubPrdControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPWESubPrdMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPWESubPrdMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPWESubPrdMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPWESubPrdEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPWESubPrdTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPWESubPrdCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPWESubPrdMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPWESubPrdDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPWESubPrdPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPWESubPrdGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPWESubPrdPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWESubPrdPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWESubPrdAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWESubPrdAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWESubPrdValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWESubPrdSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPWESubPrdSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPWESubPrdSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPWESubPrdSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPWESubPrdSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPWESubPrdSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWESubPrdSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWESubPrdObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPWESubPrdSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPWESubPrdFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPWESubPrdMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPWESubPrdFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPWESubPrdCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWESubPrdCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWESubPrdCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWESubPrdValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWESubPrdDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPWESubPrdDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPWESubPrdDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPWESubPrdDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPWESubPrdQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPWESubPrdQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWESubPrdQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWESubPrdQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWESubPrdQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPWESubPrdQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPWESubPrdQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWESubPrdQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPWESubPrdNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPWESubPrdNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPWESubPrdNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPWESubPrdNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPWESubPrdID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPWESubPrdNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPWESubPrdNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSPWESubPrdReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSPWESubPrdPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSPWESubPrdMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSPWESubPrdStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSPWESubPrdDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
  end
  object DsVSPWESubPrd: TDataSource
    DataSet = QrVSPWESubPrd
    Left = 772
    Top = 621
  end
  object QrVSMOEnvEnv: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSMOEnvEnvBeforeClose
    AfterScroll = QrVSMOEnvEnvAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvenv cmo'
      'WHERE cmo.VSVMI_Controle=:P0')
    Left = 600
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvEnvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvEnvNFEMP_FatID: TIntegerField
      FieldName = 'NFEMP_FatID'
    end
    object QrVSMOEnvEnvNFEMP_FatNum: TIntegerField
      FieldName = 'NFEMP_FatNum'
    end
    object QrVSMOEnvEnvNFEMP_Empresa: TIntegerField
      FieldName = 'NFEMP_Empresa'
    end
    object QrVSMOEnvEnvNFEMP_Terceiro: TIntegerField
      FieldName = 'NFEMP_Terceiro'
    end
    object QrVSMOEnvEnvNFEMP_nItem: TIntegerField
      FieldName = 'NFEMP_nItem'
    end
    object QrVSMOEnvEnvNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrVSMOEnvEnvNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrVSMOEnvEnvNFEMP_Pecas: TFloatField
      FieldName = 'NFEMP_Pecas'
    end
    object QrVSMOEnvEnvNFEMP_PesoKg: TFloatField
      FieldName = 'NFEMP_PesoKg'
    end
    object QrVSMOEnvEnvNFEMP_AreaM2: TFloatField
      FieldName = 'NFEMP_AreaM2'
    end
    object QrVSMOEnvEnvNFEMP_AreaP2: TFloatField
      FieldName = 'NFEMP_AreaP2'
    end
    object QrVSMOEnvEnvNFEMP_ValorT: TFloatField
      FieldName = 'NFEMP_ValorT'
    end
    object QrVSMOEnvEnvCFTMP_FatID: TIntegerField
      FieldName = 'CFTMP_FatID'
    end
    object QrVSMOEnvEnvCFTMP_FatNum: TIntegerField
      FieldName = 'CFTMP_FatNum'
    end
    object QrVSMOEnvEnvCFTMP_Empresa: TIntegerField
      FieldName = 'CFTMP_Empresa'
    end
    object QrVSMOEnvEnvCFTMP_Terceiro: TIntegerField
      FieldName = 'CFTMP_Terceiro'
    end
    object QrVSMOEnvEnvCFTMP_nItem: TIntegerField
      FieldName = 'CFTMP_nItem'
    end
    object QrVSMOEnvEnvCFTMP_SerCT: TIntegerField
      FieldName = 'CFTMP_SerCT'
    end
    object QrVSMOEnvEnvCFTMP_nCT: TIntegerField
      FieldName = 'CFTMP_nCT'
    end
    object QrVSMOEnvEnvCFTMP_Pecas: TFloatField
      FieldName = 'CFTMP_Pecas'
    end
    object QrVSMOEnvEnvCFTMP_PesoKg: TFloatField
      FieldName = 'CFTMP_PesoKg'
    end
    object QrVSMOEnvEnvCFTMP_AreaM2: TFloatField
      FieldName = 'CFTMP_AreaM2'
    end
    object QrVSMOEnvEnvCFTMP_AreaP2: TFloatField
      FieldName = 'CFTMP_AreaP2'
    end
    object QrVSMOEnvEnvCFTMP_PesTrKg: TFloatField
      FieldName = 'CFTMP_PesTrKg'
    end
    object QrVSMOEnvEnvCFTMP_CusTrKg: TFloatField
      FieldName = 'CFTMP_CusTrKg'
    end
    object QrVSMOEnvEnvCFTMP_ValorT: TFloatField
      FieldName = 'CFTMP_ValorT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvEnvVSVMI_MovimCod: TIntegerField
      FieldName = 'VSVMI_MovimCod'
    end
    object QrVSMOEnvEnvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvEnvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvEnvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvEnvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvEnvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvEnvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvEnvAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvEnvAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvEnvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSMOEnvEnv: TDataSource
    DataSet = QrVSMOEnvEnv
    Left = 600
    Top = 56
  end
  object QrVSMOEnvEVMI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmoenvevmi'
      'WHERE VSMOEnvEnv=:p0')
    Left = 684
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvEVMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvEVMIVSMOEnvEnv: TIntegerField
      FieldName = 'VSMOEnvEnv'
    end
    object QrVSMOEnvEVMIVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSMOEnvEVMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvEVMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvEVMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvEVMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvEVMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvEVMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvEVMIAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvEVMIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvEVMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvEVMIValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvEVMIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0'
    end
    object QrVSMOEnvEVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSMOEnvEVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvEVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsVSMOEnvEVMI: TDataSource
    DataSet = QrVSMOEnvEVMI
    Left = 684
    Top = 56
  end
  object QrVSMOEnvRVmi: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vee.NFEMP_SerNF, vee.NFEMP_nNF, mev.* '
      'FROM vsmoenvrvmi mev'
      'LEFT JOIN vsmoenvenv vee ON vee.Codigo=mev.VSMOEnvEnv')
    Left = 852
    Top = 8
    object QrVSMOEnvRVmiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvRVmiVSMOEnvRet: TIntegerField
      FieldName = 'VSMOEnvRet'
    end
    object QrVSMOEnvRVmiVSMOEnvEnv: TIntegerField
      FieldName = 'VSMOEnvEnv'
    end
    object QrVSMOEnvRVmiPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRVmiPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRVmiAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRVmiAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRVmiValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRVmiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvRVmiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvRVmiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvRVmiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvRVmiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvRVmiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvRVmiAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvRVmiAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvRVmiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvRVmiNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrVSMOEnvRVmiNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrVSMOEnvRVmiValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsVSMOEnvRVmi: TDataSource
    DataSet = QrVSMOEnvRVmi
    Left = 852
    Top = 56
  end
  object QrVSMOEnvGVmi: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM vsmoenvgvmi')
    Left = 940
    Top = 8
    object QrVSMOEnvGVmiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvGVmiVSMOEnvRet: TIntegerField
      FieldName = 'VSMOEnvRet'
    end
    object QrVSMOEnvGVmiVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSMOEnvGVmiPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvGVmiPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvGVmiAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvGVmiAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvGVmiValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvGVmiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvGVmiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvGVmiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvGVmiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvGVmiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvGVmiAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvGVmiAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvGVmiAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvGVmiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvGVmiVSMovimCod: TIntegerField
      FieldName = 'VSMovimCod'
    end
  end
  object DsVSMOEnvGVmi: TDataSource
    DataSet = QrVSMOEnvGVmi
    Left = 940
    Top = 56
  end
  object QrVSMOEnvRet: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSMOEnvRetBeforeClose
    AfterScroll = QrVSMOEnvRetAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvret cmo'
      'WHERE cmo.VSVMI_Controle=:P0')
    Left = 768
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvRetCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvRetNFCMO_FatID: TIntegerField
      FieldName = 'NFCMO_FatID'
    end
    object QrVSMOEnvRetNFCMO_FatNum: TIntegerField
      FieldName = 'NFCMO_FatNum'
    end
    object QrVSMOEnvRetNFCMO_Empresa: TIntegerField
      FieldName = 'NFCMO_Empresa'
    end
    object QrVSMOEnvRetNFCMO_nItem: TIntegerField
      FieldName = 'NFCMO_nItem'
    end
    object QrVSMOEnvRetNFCMO_SerNF: TIntegerField
      FieldName = 'NFCMO_SerNF'
    end
    object QrVSMOEnvRetNFCMO_nNF: TIntegerField
      FieldName = 'NFCMO_nNF'
    end
    object QrVSMOEnvRetNFCMO_Pecas: TFloatField
      FieldName = 'NFCMO_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRetNFCMO_PesoKg: TFloatField
      FieldName = 'NFCMO_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRetNFCMO_AreaM2: TFloatField
      FieldName = 'NFCMO_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_AreaP2: TFloatField
      FieldName = 'NFCMO_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_CusMOM2: TFloatField
      FieldName = 'NFCMO_CusMOM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_CusMOKG: TFloatField
      FieldName = 'NFCMO_CusMOKG'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_ValorT: TFloatField
      FieldName = 'NFCMO_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFCMO_Terceiro: TIntegerField
      FieldName = 'NFCMO_Terceiro'
    end
    object QrVSMOEnvRetNFRMP_FatID: TIntegerField
      FieldName = 'NFRMP_FatID'
    end
    object QrVSMOEnvRetNFRMP_FatNum: TIntegerField
      FieldName = 'NFRMP_FatNum'
    end
    object QrVSMOEnvRetNFRMP_Empresa: TIntegerField
      FieldName = 'NFRMP_Empresa'
    end
    object QrVSMOEnvRetNFRMP_nItem: TIntegerField
      FieldName = 'NFRMP_nItem'
    end
    object QrVSMOEnvRetNFRMP_SerNF: TIntegerField
      FieldName = 'NFRMP_SerNF'
    end
    object QrVSMOEnvRetNFRMP_nNF: TIntegerField
      FieldName = 'NFRMP_nNF'
    end
    object QrVSMOEnvRetNFRMP_Pecas: TFloatField
      FieldName = 'NFRMP_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRetNFRMP_PesoKg: TFloatField
      FieldName = 'NFRMP_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRetNFRMP_AreaM2: TFloatField
      FieldName = 'NFRMP_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFRMP_AreaP2: TFloatField
      FieldName = 'NFRMP_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFRMP_ValorT: TFloatField
      FieldName = 'NFRMP_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetNFRMP_Terceiro: TIntegerField
      FieldName = 'NFRMP_Terceiro'
    end
    object QrVSMOEnvRetCFTPA_FatID: TIntegerField
      FieldName = 'CFTPA_FatID'
    end
    object QrVSMOEnvRetCFTPA_FatNum: TIntegerField
      FieldName = 'CFTPA_FatNum'
    end
    object QrVSMOEnvRetCFTPA_Empresa: TIntegerField
      FieldName = 'CFTPA_Empresa'
    end
    object QrVSMOEnvRetCFTPA_Terceiro: TIntegerField
      FieldName = 'CFTPA_Terceiro'
    end
    object QrVSMOEnvRetCFTPA_nItem: TIntegerField
      FieldName = 'CFTPA_nItem'
    end
    object QrVSMOEnvRetCFTPA_SerCT: TIntegerField
      FieldName = 'CFTPA_SerCT'
    end
    object QrVSMOEnvRetCFTPA_nCT: TIntegerField
      FieldName = 'CFTPA_nCT'
    end
    object QrVSMOEnvRetCFTPA_Pecas: TFloatField
      FieldName = 'CFTPA_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRetCFTPA_PesoKg: TFloatField
      FieldName = 'CFTPA_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRetCFTPA_AreaM2: TFloatField
      FieldName = 'CFTPA_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetCFTPA_AreaP2: TFloatField
      FieldName = 'CFTPA_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetCFTPA_PesTrKg: TFloatField
      FieldName = 'CFTPA_PesTrKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetCFTPA_CusTrKg: TFloatField
      FieldName = 'CFTPA_CusTrKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRetCFTPA_ValorT: TFloatField
      FieldName = 'CFTPA_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsVSMOEnvRet: TDataSource
    DataSet = QrVSMOEnvRet
    Left = 768
    Top = 56
  end
  object QrVSArtCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM vsartcab'
      'ORDER BY Nome')
    Left = 312
    Top = 192
    object QrVSArtCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSArtCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsVSArtCab: TDataSource
    DataSet = QrVSArtCab
    Left = 312
    Top = 236
  end
  object QrReceiRecu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome '
      'FROM formulas'
      'ORDER BY Nome')
    Left = 257
    Top = 630
    object QrReceiRecuNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrReceiRecuNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsReceiRecu: TDataSource
    DataSet = QrReceiRecu
    Left = 257
    Top = 678
  end
  object QrReceirefu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero, Nome '
      'FROM formulas'
      'ORDER BY Nome')
    Left = 321
    Top = 630
    object QrReceirefuNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrReceirefuNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsReceiRefu: TDataSource
    DataSet = QrReceirefu
    Left = 321
    Top = 678
  end
end
