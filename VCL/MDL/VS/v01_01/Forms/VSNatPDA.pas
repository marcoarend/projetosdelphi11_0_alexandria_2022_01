unit VSNatPDA;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkDBLookupComboBox, dmkEditCB, dmkMemo, frxClass,
  frxDBSet, Vcl.Menus, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO;

type
  TFmVSNatPDA = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtArtigo: TBitBtn;
    BtMP: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrVSNatPDA: TmySQLQuery;
    DsVSNatPDA: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrVSNatPDAGraGruX: TIntegerField;
    QrVSNatPDALk: TIntegerField;
    QrVSNatPDADataCad: TDateField;
    QrVSNatPDADataAlt: TDateField;
    QrVSNatPDAUserCad: TIntegerField;
    QrVSNatPDAUserAlt: TIntegerField;
    QrVSNatPDAAlterWeb: TSmallintField;
    QrVSNatPDAAtivo: TSmallintField;
    QrVSNatPDAGraGru1: TIntegerField;
    QrVSNatPDANO_PRD_TAM_COR: TWideStringField;
    QrCad: TmySQLQuery;
    QrCadGraGruX: TIntegerField;
    QrCadLk: TIntegerField;
    QrCadDataCad: TDateField;
    QrCadDataAlt: TDateField;
    QrCadUserCad: TIntegerField;
    QrCadUserAlt: TIntegerField;
    QrCadAlterWeb: TSmallintField;
    QrCadAtivo: TSmallintField;
    QrCadGraGru1: TIntegerField;
    QrCadNO_PRD_TAM_COR: TWideStringField;
    QrCadOrdem: TIntegerField;
    QrCadBRLMedM2: TFloatField;
    PMMP: TPopupMenu;
    PMArtigo: TPopupMenu;
    DBGrid1: TdmkDBGridZTO;
    Incluilinkdeprodutos1: TMenuItem;
    Alteralinkdeprodutos1: TMenuItem;
    Excluilinkdeprodutos1: TMenuItem;
    Incluinovoartigoclassificado1: TMenuItem;
    AlteraArtigoClassificadoAtual1: TMenuItem;
    ExcluiArtigoClassificadoAtual1: TMenuItem;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    QrGraGruXCou: TmySQLQuery;
    QrGraGruXCouNO_CouNiv1: TWideStringField;
    QrGraGruXCouNO_CouNiv2: TWideStringField;
    QrGraGruXCouGraGruX: TIntegerField;
    QrGraGruXCouCouNiv1: TIntegerField;
    QrGraGruXCouCouNiv2: TIntegerField;
    QrGraGruXCouLk: TIntegerField;
    QrGraGruXCouDataCad: TDateField;
    QrGraGruXCouDataAlt: TDateField;
    QrGraGruXCouUserCad: TIntegerField;
    QrGraGruXCouUserAlt: TIntegerField;
    QrGraGruXCouAlterWeb: TSmallintField;
    QrGraGruXCouAtivo: TSmallintField;
    DsGraGruXCou: TDataSource;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    GBCouNiv: TGroupBox;
    Label6: TLabel;
    Label4: TLabel;
    EdCouNiv2: TdmkEditCB;
    CBCouNiv2: TdmkDBLookupComboBox;
    EdCouNiv1: TdmkEditCB;
    CBCouNiv1: TdmkDBLookupComboBox;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    EdArtigoImp: TdmkEdit;
    EdClasseImp: TdmkEdit;
    QrVSNatPDAArtigoImp: TWideStringField;
    QrVSNatPDAClasseImp: TWideStringField;
    QrGraGruXCouArtigoImp: TWideStringField;
    QrGraGruXCouClasseImp: TWideStringField;
    Label13: TLabel;
    EdPrevPcPal: TdmkEdit;
    QrGraGruXCouPrevPcPal: TIntegerField;
    DBEdit3: TDBEdit;
    Label3: TLabel;
    QrVSNatPDAMediaMinM2: TFloatField;
    QrVSNatPDAMediaMaxM2: TFloatField;
    Label15: TLabel;
    Label9: TLabel;
    EdNome: TdmkEdit;
    EdTam: TdmkEdit;
    EdCor: TdmkEdit;
    Label16: TLabel;
    Label21: TLabel;
    EdPrevAMPal: TdmkEdit;
    EdPrevKgPal: TdmkEdit;
    Label22: TLabel;
    QrGraGruXCouPrevAMPal: TFloatField;
    QrGraGruXCouPrevKgPal: TFloatField;
    Label19: TLabel;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label20: TLabel;
    QrGraGruXCouGrandeza: TSmallintField;
    QrVSNatPDAGrandeza: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSNatPDAAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSNatPDABeforeOpen(DataSet: TDataSet);
    procedure BtMPClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure BtArtigoClick(Sender: TObject);
    procedure QrVSNatPDABeforeClose(DataSet: TDataSet);
    procedure QrVSNatPDAAfterScroll(DataSet: TDataSet);
    procedure Incluilinkdeprodutos1Click(Sender: TObject);
    procedure Alteralinkdeprodutos1Click(Sender: TObject);
    procedure Incluinovoartigoclassificado1Click(Sender: TObject);
    procedure AlteraArtigoClassificadoAtual1Click(Sender: TObject);
    procedure ExcluiArtigoClassificadoAtual1Click(Sender: TObject);
    procedure PMMPPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure MostraVSRibArt(SQLType: TSQLType);
    procedure ReopenVSRibArt(Codigo: Integer);

  public
    { Public declarations }
    FSeq: Integer;
    procedure LocCod(Atual, GraGruX: Integer);
  end;

var
  FmVSNatPDA: TFmVSNatPDA;
const
  FFormatFloat = '00000';
  FCo_Codigo = 'GraGruX';
  FCo_Nome = 'CONCAT(gg1.Nome, ' + sLineBreak +
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ' + sLineBreak +
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))';

implementation

uses UnMyObjects, Module, MeuDBUses, DMkDAC_PF, ModuleGeral, MyDBCheck,
  VSRibArt, UnVS_PF, AppListas, UnGrade_PF, UnGrade_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSNatPDA.LocCod(Atual, GraGruX: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, GraGruX);
end;

procedure TFmVSNatPDA.MostraVSRibArt(SQLType: TSQLType);
begin
end;

procedure TFmVSNatPDA.PMMPPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(AlteraArtigoClassificadoAtual1, QrVSNatPDA);
end;

procedure TFmVSNatPDA.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSNatPDAGraGruX.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSNatPDA.DefParams;
begin
  VAR_GOTOTABELA := 'vsnatpda';
  VAR_GOTOMYSQLTABLE := QrVSNatPDA;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := FCo_Codigo;
  VAR_GOTONOME := FCo_Nome;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wmp.*, cou.*, ');
  VAR_SQLx.Add('ggx.GraGru1, CONCAT(gg1.Nome,');
  VAR_SQLx.Add('IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),');
  VAR_SQLx.Add('IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))');
  VAR_SQLx.Add('NO_PRD_TAM_COR');
  VAR_SQLx.Add('FROM vsnatpda wmp');
  VAR_SQLx.Add('LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX');
  VAR_SQLx.Add('LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC');
  VAR_SQLx.Add('LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad');
  VAR_SQLx.Add('LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI');
  VAR_SQLx.Add('LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1');
  VAR_SQLx.Add('LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle');
  VAR_SQLx.Add('');
  //
  VAR_SQLx.Add('WHERE wmp.GraGruX > 0');
  //
  VAR_SQL1.Add('AND wmp.GraGruX=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE ' + FCo_Nome + ' LIKE :P0 ');
  //
end;

procedure TFmVSNatPDA.ExcluiArtigoClassificadoAtual1Click(Sender: TObject);
begin
//
end;

procedure TFmVSNatPDA.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSNatPDA.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSNatPDA.ReopenVSRibArt(Codigo: Integer);
begin
end;

procedure TFmVSNatPDA.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSNatPDA.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSNatPDA.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSNatPDA.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSNatPDA.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSNatPDA.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSNatPDA.AlteraArtigoClassificadoAtual1Click(Sender: TObject);
begin
  Grade_PF.DefineNomeTamCorGraGruX(EdNome, EdTam, EdCor, QrVSNatPDAGraGruX.Value);
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraGruXCou, [PnDados],
  [PnEdita], EdCouNiv2, ImgTipo, 'gragruxcou');
end;

procedure TFmVSNatPDA.Alteralinkdeprodutos1Click(Sender: TObject);
begin
  MostraVSRibArt(stUpd);
end;

procedure TFmVSNatPDA.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSNatPDAGraGruX.Value;
  Close;
end;

procedure TFmVSNatPDA.BtConfirmaClick(Sender: TObject);
const
  MediaMinM2 = 0;
  MediaMaxM2 = 0;
var
  GraGruX, CouNiv1, CouNiv2, PrevPcPal, GraGru1: Integer;
  ArtigoImp, ClasseImp, Nome: String;
  PrevAMPal, PrevKgPal: Double;
begin
  GraGruX        := QrVSNatPDAGraGruX.Value;
  GraGru1        := QrVSNatPDAGraGru1.Value;
  //Ordem          := EdOrdem.ValueVariant;
  CouNiv1        := EdCouNiv1.ValueVariant;
  CouNiv2        := EdCouNiv2.ValueVariant;
  ArtigoImp      := EdArtigoImp.ValueVariant;
  ClasseImp      := EdClasseImp.ValueVariant;
  PrevPcPal      := EdPrevPcPal.ValueVariant;
  PrevAMPal      := EdPrevAMPal.ValueVariant;
  PrevKgPal      := EdPrevKgPal.ValueVariant;
  Nome           := EdNome.Text;
  //
  if MyObjects.FIC(CouNiv2 = 0, EdCouNiv2, 'Informe o "Tipo do material"!') then
    Exit;
  if MyObjects.FIC(CouNiv1 = 0, EdCouNiv1, 'Informe a "Parte do material"!') then
    Exit;
  //
  VS_PF.ReIncluiCouNivs(
    GraGruX, CouNiv1, CouNiv2, PrevPcPal, (*Grandeza*)-1, (*Bastidao*)0, PrevAMPal, PrevKgPal,
    ArtigoImp, ClasseImp, MediaMinM2, MediaMaxM2);
  //
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  //
  Grade_PF.GraGru1_AlteraNome(GraGru1, Nome);
  //
  if FSeq = 0 then
  begin
    LocCod(GraGruX, GraGruX);
  end else
    Close;
end;

procedure TFmVSNatPDA.BtDesisteClick(Sender: TObject);
var
  GraGruX: Integer;
begin
  GraGruX        := QrVSNatPDAGraGruX.Value;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(GraGruX, Dmod.MyDB, 'vsnatpda', FCo_Codigo);
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmVSNatPDA.BtArtigoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMArtigo, BtArtigo);
end;

procedure TFmVSNatPDA.BtMPClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMP, BtMP);
end;

procedure TFmVSNatPDA.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FSeq := 0;
  //GBEdita.Align := alClient;
  DBGrid1.Align := alClient;
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
  CriaOForm;
end;

procedure TFmVSNatPDA.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSNatPDAGraGruX.Value, LaRegistro.Caption);
end;

procedure TFmVSNatPDA.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSNatPDA.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSNatPDAGraGruX.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSNatPDA.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSNatPDA.QrVSNatPDAAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSNatPDA.QrVSNatPDAAfterScroll(DataSet: TDataSet);
begin
  ReopenVSRibArt(0);
  VS_PF.ReopenGraGruXCou(QrGraGruXCou, QrVSNatPDAGraGruX.Value);
end;

procedure TFmVSNatPDA.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSNatPDA.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrVSNatPDAGraGruX.Value,
  CuringaLoc.CriaForm(FCo_Codigo, FCo_Nome, 'vsnatpda', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSNatPDA.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSNatPDA.Incluilinkdeprodutos1Click(Sender: TObject);
begin
  MostraVSRibArt(stIns);
end;

procedure TFmVSNatPDA.Incluinovoartigoclassificado1Click(Sender: TObject);
var
  DefinedSQL: String;
  Qry: TmySQLQuery;
  GraGruX: Integer;
begin
  GraGruX := Grade_Jan.ObtemGraGruXSemY(CO_GraGruY_1195_VSNatPDA);
  if GraGruX <> 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT GraGruX ',
      'FROM vsnatpda ',
      'WHERE GraGruX=' + Geral.FF0(GraGruX),
      '']);
      if Qry.FieldByName('GraGruX').AsInteger = GraGruX then
      begin
        Geral.MB_Aviso('O reduzido ' + Geral.FF0(GraGruX) +
        ' j� estava adicionado � configura��o de mat�ria-prima PDA!');
      end else
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vsnatpda', False, [
        ], [
        'GraGruX'], [
        ], [
        GraGruX], True);
      end;
      //
      LocCod(GraGruX, GraGruX);
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmVSNatPDA.QrVSNatPDABeforeClose(DataSet: TDataSet);
begin
(*
  QrVSRibArt.Close;
*)
  QrGraGruXCou.Close;
end;

procedure TFmVSNatPDA.QrVSNatPDABeforeOpen(DataSet: TDataSet);
begin
  QrVSNatPDAGraGruX.DisplayFormat := FFormatFloat;
end;

end.

