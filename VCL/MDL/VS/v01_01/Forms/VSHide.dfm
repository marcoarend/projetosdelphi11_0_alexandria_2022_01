object FmVSHide: TFmVSHide
  Left = 0
  Top = 0
  Caption = 'WET-CURTI-204 :: Fun'#231#245'es VS em Janela Oculta'
  ClientHeight = 308
  ClientWidth = 578
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PMVSMovIDLoc: TPopupMenu
    Left = 60
    Top = 16
    object CadastraLocalemCentrodeEstoque1: TMenuItem
      Caption = 'Cadastra Local em Centro de Estoque'
      OnClick = CadastraLocalemCentrodeEstoque1Click
    end
    object HabilitaLocalemMovimID1: TMenuItem
      Caption = 'Habilita Local em ID de Movimento'
      OnClick = HabilitaLocalemMovimID1Click
    end
  end
end
