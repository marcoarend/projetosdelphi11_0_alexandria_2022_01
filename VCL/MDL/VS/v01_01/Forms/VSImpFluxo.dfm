object FmVSImpFluxo: TFmVSImpFluxo
  Left = 0
  Top = 0
  Caption = 'WET-CURTI-081 :: Impress'#227'o de Fluxo de Couros Inteiros VS'
  ClientHeight = 645
  ClientWidth = 858
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 858
    Height = 575
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Anal'#237'tico'
    end
    object TabSheet2: TTabSheet
      Caption = 'Sint'#233'tico'
      ImageIndex = 1
      object DBG13Sintetico: TdmkDBGridZTO
        Left = 0
        Top = 25
        Width = 850
        Height = 522
        Align = alClient
        DataSource = Ds13Sintetico
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        PopupMenu = PMSintetico
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'GraGruY'
            Title.Caption = 'Grupo'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_GGY'
            Title.Caption = 'Nome do grupo'
            Width = 164
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_SERFCH'
            Title.Caption = 'S'#233'rie'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ficha'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IMEI_Src'
            Title.Caption = 'IME-I pai'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pallet'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Operacao'
            Title.Caption = 'Opera'#231#227'o'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Inteiros'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LastDtHr'
            Title.Caption = #218'ltimo movimento'
            Visible = True
          end>
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 850
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object Label45: TLabel
          Left = 4
          Top = 4
          Width = 150
          Height = 13
          Caption = 'Quantidade m'#225'xima de interos:'
        end
        object Ed13_MaxInteiros: TdmkEdit
          Left = 160
          Top = 0
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 1
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '9,0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 9.000000000000000000
          ValWarn = False
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Poss'#237'veis erros'
      ImageIndex = 2
      object DBGDsErrMov1: TdmkDBGridZTO
        Left = 0
        Top = 0
        Width = 850
        Height = 547
        Align = alClient
        DataSource = DsErrMov1
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        RowColors = <>
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 575
    Width = 858
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 712
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Fechar'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 710
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtBaixaExtra: TBitBtn
        Tag = 5
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtBaixaExtraClick
      end
      object BtTudo: TBitBtn
        Tag = 127
        Left = 140
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
        OnClick = BtTudoClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 260
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 2
        Visible = False
        OnClick = BtNenhumClick
      end
    end
  end
  object frxWET_CURTI_018_13_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42126.704835439810000000
    ReportOptions.LastChange = 42126.704835439810000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_018_13_AGetValue
    Left = 240
    Top = 136
    Datasets = <
      item
        DataSet = frxDs13VSSeqIts
        DataSetName = 'frxDs13VSSeqIts'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 52.913410240000000000
        Top = 18.897650000000000000
        Width = 1009.134510000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 1009.134510000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 994.016390000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 1009.134510000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 797.480830000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Fluxo de Couros Inteiros')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 903.307670000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 650.079160000000000000
          Top = 37.795300000000000000
          Width = 132.283427950000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mat'#233'ria-prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitCodi: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 37.795300000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 839.055660000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inteiros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaP2: TfrxMemoView
          AllowVectorExport = True
          Left = 895.748610000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Acumulado')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 83.149606299212600000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Data / hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 952.441560000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Top = 37.795300000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Codigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 37.795300000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-C')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 37.795300000000000000
          Width = 83.149574570000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Processo ou opera'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 215.433210000000000000
          Top = 37.795300000000000000
          Width = 132.283354720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#237'vel do processo / opera'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 600.945270000000000000
          Top = 37.795300000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 782.362710000000000000
          Top = 37.795300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Top = 37.795300000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-B')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Top = 37.795300000000000000
          Width = 105.826796060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ficha RMP')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 291.023810000000000000
        Width = 1009.134510000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 725.669760000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 687.874460000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 170.078850000000000000
        Width = 1009.134510000000000000
        DataSet = frxDs13VSSeqIts
        DataSetName = 'frxDs13VSSeqIts'
        RowCount = 0
        object MeValCodi: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Width = 83.149606299212600000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Left = 650.079160000000000000
          Width = 132.283427950000000000
          Height = 15.118110240000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 839.055660000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'Inteiros'
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."Inteiros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 895.748610000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'AcumInteir'
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."AcumInteir"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 952.441560000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.0;-#,###,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."SdoVrtPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'Codigo'
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."Codigo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'MovimCod'
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."MovimCod"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DataField = 'NO_MovimID'
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."NO_MovimID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 234.330860000000000000
          Width = 113.385704720000000000
          Height = 15.118110240000000000
          DataField = 'NO_MovimNiv'
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."NO_MovimNiv"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 18.897454720000000000
          Height = 15.118110240000000000
          DataField = 'MovimID'
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."MovimID"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 215.433210000000000000
          Width = 18.897454720000000000
          Height = 15.118110240000000000
          DataField = 'MovimNiv'
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."MovimNiv"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 600.945270000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'Pallet'
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 782.362710000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'SrcNivel2'
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."SrcNivel2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 347.716760000000000000
          Width = 105.826796060000000000
          Height = 15.118110240000000000
          DataField = 'NO_SERIE_FICHA'
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."NO_SERIE_FICHA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 132.283550000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDs13VSSeqIts."NO_SERIE_FICHA"'
        object Me_GH1: TfrxMemoView
          AllowVectorExport = True
          Width = 1009.134314720000000000
          Height = 15.118110240000000000
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."NO_SERIE_FICHA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object FT_01: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 207.874150000000000000
        Width = 1009.134510000000000000
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 839.055660000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs13VSSeqIts."Inteiros">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FT1: TfrxMemoView
          AllowVectorExport = True
          Width = 782.362514720000000000
          Height = 15.118110240000000000
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13VSSeqIts."NO_SERIE_FICHA"]   ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 782.362710000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs13VSSeqIts."Pecas">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object Qr13VSSeqIts: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT vmi.*, '
      
        ' ELT(vmi.MovimID + 1,"[ND]","Compra","Venda","Reclasse","Baixa",' +
        '"Recurtido","Curtido","Classe Unit.","Reclasse","For'#231'ado","Sem O' +
        'rigem","Em Operacao","Residual","Ajuste","Classe Mult.","Pr'#233' rec' +
        'lasse","Compra de Classificado","Baixa extra") NO_MovimID,'
      
        ' ELT(vmi.MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destin' +
        'o classifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de ' +
        'artigo","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Orig' +
        'em opera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino ' +
        'opera'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa' +
        ' em pr'#233' reclasse","Entrada para reclasse","Origem gera'#231#227'o de art' +
        'igo","Destino gera'#231#227'o de artigo","Baixa gera'#231#227'o de artigo") NO_M' +
        'ovimNiv,'
      'CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, '
      'IF(vsp.CliStat IS NULL, 0, vsp.CliStat) VSP_CliStat, '
      'IF(vsp.Status IS NULL, 0, vsp.Status) VSP_STATUS, '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, '
      'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",   '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, '
      'vps.Nome NO_STATUS, '
      'vsf.Nome NO_SerieFch '
      'FROM _vsseqits_fmvsmovimp vmi '
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet   '
      
        'LEFT JOIN gragrux    ggx ON ggx.Controle=IF(vmi.Pallet <> 0, vsp' +
        '.GraGruX, vmi.GraGruX) '
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  '
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro '
      'LEFT JOIN entidades  emp ON emp.Codigo=vmi.Empresa '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=vsp.Status  '
      'LEFT JOIN couniv2    cn2 ON cn2.Codigo=xco.CouNiv2'
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch '
      '')
    Left = 44
    Top = 48
    object Qr13VSSeqItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr13VSSeqItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr13VSSeqItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object Qr13VSSeqItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object Qr13VSSeqItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object Qr13VSSeqItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr13VSSeqItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object Qr13VSSeqItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object Qr13VSSeqItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object Qr13VSSeqItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object Qr13VSSeqItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object Qr13VSSeqItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object Qr13VSSeqItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object Qr13VSSeqItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object Qr13VSSeqItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr13VSSeqItsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object Qr13VSSeqItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object Qr13VSSeqItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object Qr13VSSeqItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object Qr13VSSeqItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object Qr13VSSeqItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object Qr13VSSeqItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object Qr13VSSeqItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object Qr13VSSeqItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object Qr13VSSeqItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object Qr13VSSeqItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object Qr13VSSeqItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object Qr13VSSeqItsSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object Qr13VSSeqItsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object Qr13VSSeqItsMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object Qr13VSSeqItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object Qr13VSSeqItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object Qr13VSSeqItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object Qr13VSSeqItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object Qr13VSSeqItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object Qr13VSSeqItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object Qr13VSSeqItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object Qr13VSSeqItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object Qr13VSSeqItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object Qr13VSSeqItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object Qr13VSSeqItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object Qr13VSSeqItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object Qr13VSSeqItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object Qr13VSSeqItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object Qr13VSSeqItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object Qr13VSSeqItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object Qr13VSSeqItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object Qr13VSSeqItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object Qr13VSSeqItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object Qr13VSSeqItsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object Qr13VSSeqItsInteiros: TFloatField
      FieldName = 'Inteiros'
    end
    object Qr13VSSeqItsAcumInteir: TFloatField
      FieldName = 'AcumInteir'
    end
    object Qr13VSSeqItsSequencia: TIntegerField
      FieldName = 'Sequencia'
    end
    object Qr13VSSeqItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object Qr13VSSeqItsNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object Qr13VSSeqItsNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 31
    end
    object Qr13VSSeqItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object Qr13VSSeqItsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object Qr13VSSeqItsVSP_CliStat: TLargeintField
      FieldName = 'VSP_CliStat'
    end
    object Qr13VSSeqItsVSP_STATUS: TLargeintField
      FieldName = 'VSP_STATUS'
    end
    object Qr13VSSeqItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object Qr13VSSeqItsNO_CliStat: TWideStringField
      FieldName = 'NO_CliStat'
      Size = 100
    end
    object Qr13VSSeqItsNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object Qr13VSSeqItsNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object Qr13VSSeqItsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object Qr13VSSeqItsNO_SERIE_FICHA: TWideStringField
      FieldName = 'NO_SERIE_FICHA'
      Size = 70
    end
    object Qr13VSSeqItsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object Qr13VSSeqItsIMEI_Src: TIntegerField
      FieldName = 'IMEI_Src'
    end
    object Qr13VSSeqItsIMEI_Ord: TIntegerField
      FieldName = 'IMEI_Ord'
    end
    object Qr13VSSeqItsData: TDateField
      FieldName = 'Data'
    end
    object Qr13VSSeqItsOperacao: TIntegerField
      FieldName = 'Operacao'
    end
  end
  object Ds13VSSeqIts: TDataSource
    DataSet = Qr13VSSeqIts
    Left = 44
    Top = 96
  end
  object frxDs13VSSeqIts: TfrxDBDataset
    UserName = 'frxDs13VSSeqIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'TpCalcAuto=TpCalcAuto'
      'Inteiros=Inteiros'
      'AcumInteir=AcumInteir'
      'Sequencia=Sequencia'
      'Ativo=Ativo'
      'NO_MovimID=NO_MovimID'
      'NO_MovimNiv=NO_MovimNiv'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_Pallet=NO_Pallet'
      'VSP_CliStat=VSP_CliStat'
      'VSP_STATUS=VSP_STATUS'
      'NO_FORNECE=NO_FORNECE'
      'NO_CliStat=NO_CliStat'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_STATUS=NO_STATUS'
      'NO_SerieFch=NO_SerieFch'
      'NO_SERIE_FICHA=NO_SERIE_FICHA'
      'GraGruY=GraGruY'
      'IMEI_Src=IMEI_Src'
      'IMEI_Ord=IMEI_Ord'
      'Data=Data'
      'Operacao=Operacao')
    DataSet = Qr13VSSeqIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 44
    Top = 144
  end
  object Qr13Sintetico: TMySQLQuery
    Database = DModG.RV_CEP_DB
    OnCalcFields = Qr13SinteticoCalcFields
    SQL.Strings = (
      'SELECT vfi.*, vsf.Nome NO_SERFCH, ggy.Nome NO_GGY '
      'FROM _vsfluxincon_fmvsmovimp vfi'
      'LEFT JOIN bluederm.gragruy ggy ON ggy.Codigo=vfi.GraGruY'
      'LEFT JOIN bluederm.vsserfch vsf ON vsf.Codigo=vfi.SerieFch'
      'WHERE vfi.Inteiros <=29'
      'ORDER BY vfi.GraGruY, vfi.SerieFch, '
      'vfi.Ficha, vfi.IMEI_Src, vfi.Pallet')
    Left = 376
    Top = 109
    object Qr13SinteticoGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object Qr13SinteticoInteiros: TFloatField
      FieldName = 'Inteiros'
      DisplayFormat = '#,###,###,##0.0;-#,###,###,##0.0; '
    end
    object Qr13SinteticoSerieFch: TIntegerField
      FieldName = 'SerieFch'
      DisplayFormat = '#,###,###,##0;-#,###,###,##0; '
    end
    object Qr13SinteticoFicha: TIntegerField
      FieldName = 'Ficha'
      DisplayFormat = '#,###,###,##0;-#,###,###,##0; '
    end
    object Qr13SinteticoIMEI_Src: TIntegerField
      FieldName = 'IMEI_Src'
      DisplayFormat = '#,###,###,##0;-#,###,###,##0; '
    end
    object Qr13SinteticoPallet: TIntegerField
      FieldName = 'Pallet'
      DisplayFormat = '#,###,###,##0;-#,###,###,##0; '
    end
    object Qr13SinteticoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object Qr13SinteticoNO_SERFCH: TWideStringField
      FieldName = 'NO_SERFCH'
      Size = 60
    end
    object Qr13SinteticoNO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object Qr13SinteticoLastDtHr: TDateTimeField
      FieldName = 'LastDtHr'
      OnGetText = Qr13SinteticoLastDtHrGetText
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object Qr13SinteticoOperacao: TIntegerField
      FieldName = 'Operacao'
      DisplayFormat = '#,###,###,##0;-#,###,###,##0; '
    end
    object Qr13SinteticoLastDtHr_TXT: TWideStringField
      FieldName = 'LastDtHr_TXT'
      Size = 19
    end
  end
  object Ds13Sintetico: TDataSource
    DataSet = Qr13Sintetico
    Left = 376
    Top = 156
  end
  object frxWET_CURTI_018_13_B: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42126.704835439810000000
    ReportOptions.LastChange = 42126.704835439810000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_018_13_AGetValue
    Left = 240
    Top = 188
    Datasets = <
      item
        DataSet = frxDs13Sintetico
        DataSetName = 'frxDs13Sintetico'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 52.913410240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Sobras / Faltas de Couros em Fluxo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitCodi: TfrxMemoView
          AllowVectorExport = True
          Left = 343.937230000000000000
          Top = 37.795300000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IME-I pai')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Top = 37.795300000000000000
          Width = 60.472443390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inteiros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Top = 37.795300000000000000
          Width = 94.488196300000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #218'ltimo movimento')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 188.976468270000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Grupo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Top = 37.795300000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Ficha')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409710000000000000
          Top = 37.795300000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 37.795300000000000000
          Width = 94.488188976377950000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie RMP')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Top = 37.795300000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Opera'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 427.086890000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 170.078850000000000000
        Width = 680.315400000000000000
        DataSet = frxDs13Sintetico
        DataSetName = 'frxDs13Sintetico'
        RowCount = 0
        object MeValCodi: TfrxMemoView
          AllowVectorExport = True
          Left = 343.937230000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'IMEI_Src'
          DataSet = frxDs13Sintetico
          DataSetName = 'frxDs13Sintetico'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13Sintetico."IMEI_Src"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Width = 94.488196300000000000
          Height = 15.118110240000000000
          DataField = 'LastDtHr_TXT'
          DataSet = frxDs13Sintetico
          DataSetName = 'frxDs13Sintetico'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs13Sintetico."LastDtHr_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Left = 49.133890000000000000
          Width = 139.842487950000000000
          Height = 15.118110240000000000
          DataField = 'NO_GGY'
          DataSet = frxDs13Sintetico
          DataSetName = 'frxDs13Sintetico'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs13Sintetico."NO_GGY"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Width = 60.472443390000000000
          Height = 15.118110240000000000
          DataSet = frxDs13Sintetico
          DataSetName = 'frxDs13Sintetico'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13Sintetico."Inteiros"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'GraGruY'
          DataSet = frxDs13Sintetico
          DataSetName = 'frxDs13Sintetico'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13Sintetico."GraGruY"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 283.464750000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Ficha'
          DataSet = frxDs13Sintetico
          DataSetName = 'frxDs13Sintetico'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13Sintetico."Ficha"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 404.409710000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Pallet'
          DataSet = frxDs13Sintetico
          DataSetName = 'frxDs13Sintetico'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13Sintetico."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 94.488188976377950000
          Height = 15.118110240000000000
          DataField = 'NO_SERFCH'
          DataSet = frxDs13Sintetico
          DataSetName = 'frxDs13Sintetico'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs13Sintetico."NO_SERFCH"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Operacao'
          DataSet = frxDs13Sintetico
          DataSetName = 'frxDs13Sintetico'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13Sintetico."Operacao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH_01: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 132.283550000000000000
        Width = 680.315400000000000000
        Condition = 'frxDs13Sintetico."GraGruY"'
        object Me_GH1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315204720000000000
          Height = 15.118110240000000000
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs13Sintetico."NO_GGY"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object FT_01: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 207.874150000000000000
        Width = 680.315400000000000000
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Width = 60.472443390000000000
          Height = 15.118110240000000000
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs13Sintetico."Inteiros">,MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_FT1: TfrxMemoView
          AllowVectorExport = True
          Width = 525.354474720000000000
          Height = 15.118110240000000000
          DataSet = frxDs13VSSeqIts
          DataSetName = 'frxDs13VSSeqIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs13Sintetico."NO_GGY"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDs13Sintetico: TfrxDBDataset
    UserName = 'frxDs13Sintetico'
    CloseDataSource = False
    FieldAliases.Strings = (
      'GraGruY=GraGruY'
      'Inteiros=Inteiros'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'IMEI_Src=IMEI_Src'
      'Pallet=Pallet'
      'Ativo=Ativo'
      'NO_SERFCH=NO_SERFCH'
      'NO_GGY=NO_GGY'
      'LastDtHr=LastDtHr'
      'Operacao=Operacao'
      'LastDtHr_TXT=LastDtHr_TXT')
    DataSet = Qr13Sintetico
    BCDToCurrency = False
    DataSetOptions = []
    Left = 376
    Top = 204
  end
  object PMSintetico: TPopupMenu
    OnPopup = PMSinteticoPopup
    Left = 404
    Top = 332
    object Irparajaneladegerenciamentodeficha1: TMenuItem
      Caption = 'Ir para janela de gerenciamento de &Ficha'
      OnClick = Irparajaneladegerenciamentodeficha1Click
    end
    object IrparajaneladegerenciamentodePallet1: TMenuItem
      Caption = 'Ir para janela de gerenciamento de &Pallet'
      OnClick = IrparajaneladegerenciamentodePallet1Click
    end
    object IrparajaneladegerenciamentodeIMEI1: TMenuItem
      Caption = 'Ir para janela de gerenciamento de &IME-I'
      OnClick = IrparajaneladegerenciamentodeIMEI1Click
    end
    object IrparajaneladeentradaInNatura1: TMenuItem
      Caption = 'Ir para janela de Entrada In &Natura'
      OnClick = IrparajaneladeentradaInNatura1Click
    end
    object IrparajaneladeGeraodeArtigo1: TMenuItem
      Caption = 'Ir para janela de &Gera'#231#227'o de Artigo'
      OnClick = IrparajaneladeGeraodeArtigo1Click
    end
    object IrparajaneladeOrdemdeOperao1: TMenuItem
      Caption = 'Ir para janela de Gera'#231#227'o de Classificado (Ordem de &Opera'#231#227'o)'
      OnClick = IrparajaneladeOrdemdeOperao1Click
    end
    object IrparajaneladeCassificaoMontagem1: TMenuItem
      Caption = 'Ir para janela de Gerenciamento de &Cassifica'#231#227'o (Montagem)'
      OnClick = IrparajaneladeCassificaoMontagem1Click
    end
    object IrparajenaladereclassificaoMontagem1: TMenuItem
      Caption = 'Ir para janala de Gerenciamento de &Reclassifica'#231#227'o (Montagem)'
      OnClick = IrparajenaladereclassificaoMontagem1Click
    end
    object IrparajanaladeGerenciamentodeReclassificaoDesmontagem1: TMenuItem
      Caption = 
        'Ir para janala de Gerenciamento de Reclassifica'#231#227'o (&Desmontagem' +
        ')'
      OnClick = IrparajanaladeGerenciamentodeReclassificaoDesmontagem1Click
    end
  end
  object QrErrMov1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 200
    Top = 360
  end
  object DsErrMov1: TDataSource
    DataSet = QrErrMov1
    Left = 200
    Top = 408
  end
  object frxDsErrMov1: TfrxDBDataset
    UserName = 'frxDsErrMov1'
    CloseDataSource = False
    DataSet = QrErrMov1
    BCDToCurrency = False
    DataSetOptions = []
    Left = 200
    Top = 456
  end
end
