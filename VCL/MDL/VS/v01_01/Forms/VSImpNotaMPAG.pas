unit VSImpNotaMPAG;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  frxClass, frxDBSet,
  dmkGeral, UnDmkProcFunc, dmkEditCB, AppListas, UnDmkEnums,
  dmkDBLookupComboBox, Data.DB, mySQLDbTables, UnProjGroup_Consts, UnGrl_Consts;

type
  TFmVSImpNotaMPAG = class(TForm)
    frxWET_CURTI_018_14_A: TfrxReport;
    frxDs14VSMovIts: TfrxDBDataset;
    Qr14VsMovIts: TmySQLQuery;
    Qr14VsMovItsCodigo: TLargeintField;
    Qr14VsMovItsControle: TLargeintField;
    Qr14VsMovItsMovimCod: TLargeintField;
    Qr14VsMovItsMovimNiv: TLargeintField;
    Qr14VsMovItsMovimTwn: TLargeintField;
    Qr14VsMovItsEmpresa: TLargeintField;
    Qr14VsMovItsTerceiro: TLargeintField;
    Qr14VsMovItsCliVenda: TLargeintField;
    Qr14VsMovItsMovimID: TLargeintField;
    Qr14VsMovItsDataHora: TDateTimeField;
    Qr14VsMovItsPallet: TLargeintField;
    Qr14VsMovItsGraGruX: TLargeintField;
    Qr14VsMovItsPecas: TFloatField;
    Qr14VsMovItsPesoKg: TFloatField;
    Qr14VsMovItsAreaM2: TFloatField;
    Qr14VsMovItsAreaP2: TFloatField;
    Qr14VsMovItsValorT: TFloatField;
    Qr14VsMovItsSrcMovID: TLargeintField;
    Qr14VsMovItsSrcNivel1: TLargeintField;
    Qr14VsMovItsSrcNivel2: TLargeintField;
    Qr14VsMovItsSrcGGX: TLargeintField;
    Qr14VsMovItsSdoVrtPeca: TFloatField;
    Qr14VsMovItsSdoVrtPeso: TFloatField;
    Qr14VsMovItsSdoVrtArM2: TFloatField;
    Qr14VsMovItsObserv: TWideStringField;
    Qr14VsMovItsSerieFch: TLargeintField;
    Qr14VsMovItsFicha: TLargeintField;
    Qr14VsMovItsMisturou: TLargeintField;
    Qr14VsMovItsFornecMO: TLargeintField;
    Qr14VsMovItsCustoMOKg: TFloatField;
    Qr14VsMovItsCustoMOM2: TFloatField;
    Qr14VsMovItsCustoMOTot: TFloatField;
    Qr14VsMovItsValorMP: TFloatField;
    Qr14VsMovItsDstMovID: TLargeintField;
    Qr14VsMovItsDstNivel1: TLargeintField;
    Qr14VsMovItsDstNivel2: TLargeintField;
    Qr14VsMovItsDstGGX: TLargeintField;
    Qr14VsMovItsQtdGerPeca: TFloatField;
    Qr14VsMovItsQtdGerPeso: TFloatField;
    Qr14VsMovItsQtdGerArM2: TFloatField;
    Qr14VsMovItsQtdGerArP2: TFloatField;
    Qr14VsMovItsQtdAntPeca: TFloatField;
    Qr14VsMovItsQtdAntPeso: TFloatField;
    Qr14VsMovItsQtdAntArM2: TFloatField;
    Qr14VsMovItsQtdAntArP2: TFloatField;
    Qr14VsMovItsNotaMPAG: TFloatField;
    Qr14VsMovItsMarca: TWideStringField;
    Qr14VsMovItsPedItsLib: TLargeintField;
    Qr14VsMovItsPedItsFin: TLargeintField;
    Qr14VsMovItsPedItsVda: TLargeintField;
    Qr14VsMovItsStqCenLoc: TLargeintField;
    Qr14VsMovItsItemNFe: TLargeintField;
    Qr14VsMovItsNO_MovimID: TWideStringField;
    Qr14VsMovItsNO_MovimNiv: TWideStringField;
    Qr14VsMovItsNO_PRD_TAM_COR: TWideStringField;
    Qr14VsMovItsNO_Pallet: TWideStringField;
    Qr14VsMovItsVSP_CliStat: TLargeintField;
    Qr14VsMovItsVSP_STATUS: TLargeintField;
    Qr14VsMovItsNO_FORNECE: TWideStringField;
    Qr14VsMovItsNO_CliStat: TWideStringField;
    Qr14VsMovItsNO_EMPRESA: TWideStringField;
    Qr14VsMovItsNO_STATUS: TWideStringField;
    Qr14VsMovItsNO_SerieFch: TWideStringField;
    Qr14VsMovItsNO_SERIE_FICHA: TWideStringField;
    Qr14VsMovItsAnoSemana: TWideStringField;
    Qr14VsMovItsAnoMes: TWideStringField;
    Qr14VsMovItsData: TWideStringField;
    Qr14VsMovItsAno: TWideStringField;
    Qr14VsMovItsBaseMPAG: TFloatField;
    Qr14VsMovItsMediaM2: TFloatField;
    Qr14VsMovItsInteiros: TFloatField;
    Qr14VsMovItsSumVNC: TFloatField;
    Qr14VsMovItsSumVRC: TFloatField;
    Qr14VsMovItsMediaKg: TFloatField;
    Qr14VsMovItsVNC_Kg: TFloatField;
    procedure frxWET_CURTI_018_14_AGetValue(const VarName: string;
      var Value: Variant);
    function frxWET_CURTI_018_14_AUserFunction(const MethodName: string;
      var Params: Variant): Variant;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function  MyFunc(Inteiros, PesoKg, QtdGerArM2, SumVRC, VNC_Kg: Double): Double;
  public
    { Public declarations }
    RG14Periodo_ItemIndex, RG14FontePsq_ItemIndex, Ed14Terceiro_ValueVariant,
    Ed14SerieFch_ValueVariant, Ed14Ficha_ValueVariant, RG14Ordem1_ItemIndex,
    RG14Ordem2_ItemIndex, RG14Ordem3_ItemIndex, Ed14GraGruX_ValueVariant,
    RG14Agrupa_ItemIndex: Integer;
    TP14DataIni_Date, TP14DataFim_Date: TDateTime;
    Ck14DataIni_Checked, Ck14DataFim_Checked, Ck14SoMPAGNo0_Checked,
    Ck14Cor_Checked: Boolean;
    EdEmpresa: TdmkEditCB;
    CB14SerieFch: TdmkDBLookupComboBox;
    CBEmpresa_Text, CB14Terceiro_Text, Ed14Marca_Text, CB14GraGruX_Text: String;
    //
    procedure ImprimeNotaMPAG();
  end;

var
  FmVSImpNotaMPAG: TFmVSImpNotaMPAG;

implementation

uses
  ModuleGeral, UnMyObjects, DmkDAC_PF, ModVS, Module, unVS_PF;

{$R *.dfm}

{ TFmVSImpNotaMPAG }

procedure TFmVSImpNotaMPAG.FormCreate(Sender: TObject);
begin
  frxWET_CURTI_018_14_A.AddFunction(
    'function MyFunc(Inteiros, PesoKg, QtdGerArM2, SumVRC, VNC_Kg: Double): Double');
  //
  frxDs14VSMovIts.DataSet := Qr14VsMovIts;
end;

procedure TFmVSImpNotaMPAG.frxWET_CURTI_018_14_AGetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt: String;
  Invalido: Boolean;
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa_Text, EdEmpresa.ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName = 'VARF_TERCEIRO_14' then
    Value := dmkPF.ParValueCodTxt(
      '', CB14Terceiro_Text, Ed14Terceiro_ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_FICHA_RMP_14' then
  begin
    if (CB14SerieFch.KeyValue = null) and (Ed14Ficha_ValueVariant = 0) then
      Value := 'TODAS'
    else
    begin
      if CB14SerieFch.KeyValue <> null then
        Value := CB14SerieFch.Text + ' ';
      if Ed14Ficha_ValueVariant <> 0 then
        Value := Value + Geral.FF0(Ed14SerieFch_ValueVariant);
    end;
  end else
  if VarName = 'VARF_GRAGRUX_14' then
    Value := dmkPF.ParValueCodTxt(
      '', CB14GraGruX_Text, Ed14GraGruX_ValueVariant, 'TODAS')
  else
  if VarName = 'VARF_PERIODO_14' then
    Value := dmkPF.PeriodoImp1(TP14DataIni_Date, TP14DataFim_Date,
    Ck14DataIni_Checked, Ck14DataFim_Checked, '', 'at�', '')
  else
  if VarName = 'VARF_AGRUP_14_1' then
    Value := RG14Agrupa_ItemIndex >= 1
  else
  if VarName = 'VARF_AGRUP_14_2' then
    Value := RG14Agrupa_ItemIndex >= 2
  else
end;

function TFmVSImpNotaMPAG.frxWET_CURTI_018_14_AUserFunction(
  const MethodName: string; var Params: Variant): Variant;
(*
var
  Var1, Var2, Var3, Var4, Var5: Variant;
*)
begin
  if MethodName = Uppercase('MyFunc') then
  begin
(*    Var1 := Params[0];
    Var2 := Params[1];
    Var3 := Params[2];
    Var4 := Params[3];
    Var5 := Params[4];
    //
    Result := MyFunc(Var1, Var2, Var3, Var4, Var5);
*)
    Result := MyFunc(
      Params[0],
      Params[1],
      Params[2],
      Params[3],
      Params[4]);
  end;
end;


procedure TFmVSImpNotaMPAG.ImprimeNotaMPAG;
const
  Ordens: array[0..2] of String = ('Terceiro', 'GraGruX', CO_DATA_HORA_VMI);
  Periodos: array[0..3] of String = (CO_DATA_HORA_VMI, 'AnoSemana', 'AnoMes', 'Ano');
var
  Ordem, ATT_MovimID, ATT_MovimNiv,
  SQL_Empresa, SQL_Movim, SQL_GraGruX, SQL_DataHora, SQL_Terceiro, SQL_SerieFch,
  SQL_Ficha, Marca, SQL_Marca, SQL_SoMPAGNo0: String;
  Grupo1, Grupo2: TfrxGroupHeader;
  Futer1, Futer2: TfrxGroupFooter;
  Me_GH1, Me_FT1, Me_GH2, Me_FT2, MeValNome, MeTitNome, MeTitCodi, MeValCodi: TfrxMemoView;
  //Index,
  Empresa, GraGruX, Terceiro, SerieFch, Ficha, Index: Integer;
  Campo, TitGru, SinalGerM2: String;
  //
  Cor1, Cor2: TColor;
  //
  function OrdemItem(Item: Integer): String;
  begin
    if Item = 2 then
      Result := Periodos[RG14Periodo_ItemIndex]
    else
      Result := Ordens[Item];
    Result := Result + ', ';
  end;
  procedure ReopenMovIts();
  const
    TemIMEIMrt = 1;
  var
    SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  begin
    SQL_Flds := Geral.ATS([
    VS_PF.SQL_NO_GGX(),
    ATT_MovimID,
    ATT_MovimNiv,
    'vsp.Nome NO_Pallet, ',
    'IF(vsp.CliStat IS NULL, 0, vsp.CliStat) VSP_CliStat, ',
    'IF(vsp.Status IS NULL, 0, vsp.Status) VSP_STATUS, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, ',
    'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",   ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,  ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
    'vps.Nome NO_STATUS, ',
    'vsf.Nome NO_SerieFch, CONCAT(vsf.Nome, " ", vmi.Ficha) NO_SERIE_FICHA, ',
    'vmi.PesoKg / (vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) MediaKg, ',
    //SinalGerM2 + 'vmi.QtdGerArM2 / (vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) MediaM2, ',
    'vmi.QtdGerArM2 / (vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) MediaM2, ',
    'DATE_FORMAT(vmi.DataHora, "%d/%m/%Y") Data,   ',
    'CONCAT(YEAR(vmi.DataHora), ".", LPAD(WEEK(vmi.DataHora), 2, "0")) AnoSemana, ',
    'DATE_FORMAT(vmi.DataHora, "%Y/%m") AnoMes,   ',
    'DATE_FORMAT(vmi.DataHora, "%Y") Ano, ',
    'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros, ',
    //'vmi.Pecas * vmi.NotaMPAG BaseMPAG  ',
    'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) * vmi.NotaMPAG BaseMPAG, ',
    '(vmi.FatNotaVNC * (vmi.PesoKg / (vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)))) SumVNC, ',
    '(vmi.FatNotaVRC * IF(vmi.QtdGerArM2 < 0, -QtdGerArM2, QtdGerArM2)) SumVRC, ',
    'vmi.FatNotaVNC * vmi.PesoKg VNC_Kg ']);

    SQL_Left := Geral.ATS([
    VS_PF.SQL_LJ_GGX(),
    'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet   ',
    'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
    'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
    'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro ',
    'LEFT JOIN entidades  emp ON emp.Codigo=vmi.Empresa ',
    'LEFT JOIN vspalsta   vps ON vps.Codigo=vsp.Status  ',
    'LEFT JOIN couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
    'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
    'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
    '']);

    SQL_Wher := Geral.ATS([
    SQL_Empresa,
    SQL_Movim,
    SQL_GraGruX,
    SQL_DataHora,
    SQL_Terceiro,
    SQL_SerieFch,
    SQL_Ficha,
    SQL_Marca,
    SQL_SoMPAGNo0,
    '']);

    SQL_Group := '';

    UnDmkDAC_PF.AbreMySQLQuery0(Qr14VsMovIts, Dmod.MyDB, [
    VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    Ordem,
    '']);
    //Geral.MB_SQL(Self, Qr14VsMovIts);
{  2020-02-05 - se precisar saber couros n�o gelatina:
SELECT
SUM(QtdGerArM2) / SUM(Pecas) M2Media,
SUM(PesoKg) / SUM(Pecas) KgMedi,
SUM(Pecas) Pecas,
SUM(QtdGerArM2) QtdGerArM2,
SUM(PesoKg) PesoKg
FROM vsmovits
WHERE SerieFch=1 AND
Ficha=379
AND MovimID=6
AND MovimNiv IN (15, 29)
}

  end;
begin
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  SQL_Empresa := 'WHERE vmi.Empresa=' + Geral.FF0(Empresa);
  //
  case RG14FontePsq_ItemIndex of
    0:
    begin
      SQL_Movim := 'AND vmi.MovimID=1' + SlineBreak + 'AND vmi.MovimNiv=0';
      SinalGerM2 := '';
    end;
    1:
    begin
      SQL_Movim := 'AND vmi.MovimID=6' + SlineBreak + 'AND vmi.MovimNiv=14';
      SinalGerM2 := '-';
    end;
    else
    begin
      Geral.MB_Aviso('Forma de pesquisa n�o definida!');
      Exit;
    end;
  end;
  SQL_DataHora := dmkPF.SQL_Periodo('AND vmi.DataHora ',
      TP14DataIni_Date, TP14DataFim_Date, Ck14DataIni_Checked, Ck14DataFim_Checked);
  Terceiro := Ed14Terceiro_ValueVariant;
  if Terceiro <> 0 then
    SQL_Terceiro := 'AND vmi.Terceiro=' + Geral.FF0(Terceiro)
  else
    SQL_Terceiro := '';
  //
  SerieFch := Ed14SerieFch_ValueVariant;
  if SerieFch <> 0 then
    SQL_SerieFch := 'AND vmi.SerieFch=' + Geral.FF0(SerieFch)
  else
    SQL_SerieFch := '';
  //
  Ficha := Ed14Ficha_ValueVariant;
  if Ficha <> 0 then
    SQL_Ficha := 'AND vmi.Ficha=' + Geral.FF0(Ficha)
  else
    SQL_Ficha := '';
  //
  Marca := Ed14Marca_Text;
  if Trim(Marca) <> '' then
    SQL_Marca := 'AND vmi.Marca="' + Marca + '"'
  else
    SQL_Marca := '';
  //
  if Ed14GraGruX_ValueVariant <> 0 then
    SQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(Ed14GraGruX_ValueVariant)
  else
    SQL_GraGruX := '';
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
  if Ck14SoMPAGNo0_Checked then
    SQL_SoMPAGNo0 := 'AND vmi.NotaMPAG BETWEEN 70 AND 140 '
  else
    SQL_SoMPAGNo0 := 'AND vmi.NotaMPAG > 0';  // Tem itens incompletos!!!

  Ordem := '';
    Ordem := 'ORDER BY ' +
    OrdemItem(RG14Ordem1_ItemIndex) +
    OrdemItem(RG14Ordem2_ItemIndex) +
    OrdemItem(RG14Ordem3_ItemIndex) +
    'DataHora, Controle ';
  //
(*
  UnDmkDAC_PF.AbreMySQLQuery0(DmModVS.Qr14VSMovIts, Dmod.MyDB, [
  'SELECT vmi.*, ',
  ATT_MovimID,
  ATT_MovimNiv,
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, ',
  'IF(vsp.CliStat IS NULL, 0, vsp.CliStat) VSP_CliStat, ',
  'IF(vsp.Status IS NULL, 0, vsp.Status) VSP_STATUS, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, ',
  'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",   ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,  ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  'vps.Nome NO_STATUS, ',
  'vsf.Nome NO_SerieFch, CONCAT(vsf.Nome, " ", vmi.Ficha) NO_SERIE_FICHA, ',
  'vmi.PesoKg / (vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) MediaKg, ',
  //SinalGerM2 + 'vmi.QtdGerArM2 / (vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) MediaM2, ',
  'vmi.QtdGerArM2 / (vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) MediaM2, ',
  'DATE_FORMAT(vmi.DataHora, "%d/%m/%Y") Data,   ',
  'CONCAT(YEAR(vmi.DataHora), ".", LPAD(WEEK(vmi.DataHora), 2, "0")) AnoSemana, ',
  'DATE_FORMAT(vmi.DataHora, "%Y/%m") AnoMes,   ',
  'DATE_FORMAT(vmi.DataHora, "%Y") Ano, ',
  'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros, ',
  //'vmi.Pecas * vmi.NotaMPAG BaseMPAG  ',
  'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) * vmi.NotaMPAG BaseMPAG, ',
  '(vmi.FatNotaVNC * (vmi.PesoKg / (vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)))) SumVNC, ',
  '(vmi.FatNotaVRC * IF(vmi.QtdGerArM2 < 0, -QtdGerArM2, QtdGerArM2)) SumVRC, ',
  'vmi.FatNotaVNC * vmi.PesoKg VNC_Kg ',
  'FROM v s m o v i t s vmi ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet   ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=IF(vmi.Pallet <> 0, vsp.GraGruX, vmi.GraGruX) ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro ',
  'LEFT JOIN entidades  emp ON emp.Codigo=vmi.Empresa ',
  'LEFT JOIN vspalsta   vps ON vps.Codigo=vsp.Status  ',
  'LEFT JOIN couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  SQL_Empresa,
  SQL_Movim,
  SQL_GraGruX,
  SQL_DataHora,
  SQL_Terceiro,
  SQL_SerieFch,
  SQL_Ficha,
  SQL_Marca,
  SQL_SoMPAGNo0,
  Ordem,
  '']);
*)
  ReopenMovIts();
  //Geral.MB_SQL(Self, Qr14VSMovIts);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_018_14_A, [
    DModG.frxDsDono,
    frxDs14VSMovIts
  ]);
  //
  Grupo1 := frxWET_CURTI_018_14_A.FindObject('GH_01') as TfrxGroupHeader;
  Grupo1.Visible := RG14Agrupa_ItemIndex > 0;
  Futer1 := frxWET_CURTI_018_14_A.FindObject('FT_01') as TfrxGroupFooter;
  Futer1.Visible := Grupo1.Visible;
  Me_GH1 := frxWET_CURTI_018_14_A.FindObject('Me_GH1') as TfrxMemoView;
  Me_FT1 := frxWET_CURTI_018_14_A.FindObject('Me_FT1') as TfrxMemoView;
  case RG14Ordem1_ItemIndex of
    0:
    begin
      Grupo1.Condition := 'frxDs14VSMovIts."Terceiro"';
      Me_GH1.Memo.Text := '[frxDs14VSMovIts."Terceiro"] - [frxDs14VSMovIts."NO_FORNECE"]';
      Me_FT1.Memo.Text := '[frxDs14VSMovIts."Terceiro"] - [frxDs14VSMovIts."NO_FORNECE"]: ';
    end;
    1:
    begin
      Grupo1.Condition := 'frxDs14VSMovIts."GraGruX"';
      Me_GH1.Memo.Text := '[frxDs14VSMovIts."GraGruX"] - [frxDs14VSMovIts."NO_PRD_TAM_COR"]';
      Me_FT1.Memo.Text := '[frxDs14VSMovIts."GraGruX"] - [frxDs14VSMovIts."NO_PRD_TAM_COR"]: ';
    end;
    2:
    begin
      case RG14Periodo_ItemIndex of
        0:
        begin
          Grupo1.Condition := 'frxDs14VSMovIts."Data"';
          Me_GH1.Memo.Text := 'Data: [frxDs14VSMovIts."Data"]';
          Me_FT1.Memo.Text := 'Data: [frxDs14VSMovIts."Data"]';
        end;
        1:
        begin
          Grupo1.Condition := 'frxDs14VSMovIts."AnoSemana"';
          Me_GH1.Memo.Text := 'Semana: [frxDs14VSMovIts."AnoSemana"]';
          Me_FT1.Memo.Text := 'Semana: [frxDs14VSMovIts."AnoSemana"]';
        end;
        2:
        begin
          Grupo1.Condition := 'frxDs14VSMovIts."AnoMes"';
          Me_GH1.Memo.Text := 'M�s: [frxDs14VSMovIts."AnoMes"]';
          Me_FT1.Memo.Text := 'M�s: [frxDs14VSMovIts."AnoMes"]';
        end;
        3:
        begin
          Grupo1.Condition := 'frxDs14VSMovIts."Ano"';
          Me_GH1.Memo.Text := 'Ano: [frxDs14VSMovIts."Ano"]';
          Me_FT1.Memo.Text := 'Ano: [frxDs14VSMovIts."Ano"]';
        end;
        else Grupo1.Condition := '***ERRO***';
      end;
    end;
    else Grupo1.Condition := '***ERRO***';
  end;
  //
  Grupo2 := frxWET_CURTI_018_14_A.FindObject('GH_02') as TfrxGroupHeader;
  Grupo2.Visible := RG14Agrupa_ItemIndex > 1;
  Futer2 := frxWET_CURTI_018_14_A.FindObject('FT_02') as TfrxGroupFooter;
  Futer2.Visible := Grupo2.Visible;
  Me_GH2 := frxWET_CURTI_018_14_A.FindObject('Me_GH2') as TfrxMemoView;
  Me_FT2 := frxWET_CURTI_018_14_A.FindObject('Me_FT2') as TfrxMemoView;
  case RG14Ordem2_ItemIndex of
    0:
    begin
      Grupo2.Condition := 'frxDs14VSMovIts."Terceiro"';
      Me_GH2.Memo.Text := '[frxDs14VSMovIts."Terceiro"] - [frxDs14VSMovIts."NO_FORNECE"]';
      Me_FT2.Memo.Text := '[frxDs14VSMovIts."Terceiro"] - [frxDs14VSMovIts."NO_FORNECE"]: ';
    end;
    1:
    begin
      Grupo2.Condition := 'frxDs14VSMovIts."GraGruX"';
      Me_GH2.Memo.Text := '[frxDs14VSMovIts."GraGruX"] - [frxDs14VSMovIts."NO_PRD_TAM_COR"]';
      Me_FT2.Memo.Text := '[frxDs14VSMovIts."GraGruX"] - [frxDs14VSMovIts."NO_PRD_TAM_COR"]: ';
    end;
    2:
    begin
      case RG14Periodo_ItemIndex of
        0:
        begin
          Grupo2.Condition := 'frxDs14VSMovIts."Data"';
          Me_GH2.Memo.Text := 'Data: [frxDs14VSMovIts."Data"]';
          Me_FT2.Memo.Text := 'Data: [frxDs14VSMovIts."Data"]';
        end;
        1:
        begin
          Grupo2.Condition := 'frxDs14VSMovIts."AnoSemana"';
          Me_GH2.Memo.Text := 'Semana: [frxDs14VSMovIts."AnoSemana"]';
          Me_FT2.Memo.Text := 'Semana: [frxDs14VSMovIts."AnoSemana"]';
        end;
        2:
        begin
          Grupo2.Condition := 'frxDs14VSMovIts."AnoMes"';
          Me_GH2.Memo.Text := 'M�s: [frxDs14VSMovIts."AnoMes"]';
          Me_FT2.Memo.Text := 'M�s: [frxDs14VSMovIts."AnoMes"]';
        end;
        3:
        begin
          Grupo2.Condition := 'frxDs14VSMovIts."Ano"';
          Me_GH2.Memo.Text := 'Ano: [frxDs14VSMovIts."Ano"]';
          Me_FT2.Memo.Text := 'Ano: [frxDs14VSMovIts."Ano"]';
        end;
        else Grupo2.Condition := '***ERRO***';
      end;
    end;
    else Grupo2.Condition := '***ERRO***';
  end;
  //
  if Ck14Cor_Checked then
  begin
    Cor1 := $00E6C29B;
    Cor2 := $00B4E0C6;
  end else
  begin
    Cor1 := clWhite;
    Cor2 := clWhite;
  end;
  (frxWET_CURTI_018_14_A.FindObject('Me_GH1') as TfrxMemoView).Color      := Cor1;
  (frxWET_CURTI_018_14_A.FindObject('Me_GH2') as TfrxMemoView).Color      := Cor2;
  (frxWET_CURTI_018_14_A.FindObject('MeGH_02_F01') as TfrxMemoView).Color := Cor1;
  (frxWET_CURTI_018_14_A.FindObject('MeMD_F01') as TfrxMemoView).Color    := Cor1;
  (frxWET_CURTI_018_14_A.FindObject('MeMD_F02') as TfrxMemoView).Color    := Cor2;
  (frxWET_CURTI_018_14_A.FindObject('MeFT_02_F01') as TfrxMemoView).Color := Cor1;
  (frxWET_CURTI_018_14_A.FindObject('MeFT_02_F02') as TfrxMemoView).Color := Cor2;
  (frxWET_CURTI_018_14_A.FindObject('MeFT_01') as TfrxMemoView).Color     := Cor1;
  //
  (frxWET_CURTI_018_14_A.FindObject('MeMD_F01') as TfrxMemoView).Visible  := Grupo1.Visible;
  (frxWET_CURTI_018_14_A.FindObject('MeMD_F02') as TfrxMemoView).Visible  := Grupo2.Visible;
  //
  MyObjects.frxMostra(frxWET_CURTI_018_14_A, 'Nota MPAG');
end;

function TFmVSImpNotaMPAG.MyFunc(Inteiros, PesoKg, QtdGerArM2, SumVRC,
  VNC_Kg: Double): Double;
(*
      SUM(<frxDs14VSMovIts."Inteiros">,MD002,1),
      SUM(<frxDs14VSMovIts."PesoKg">,MD002,1),
      SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1),
      SUM(<frxDs14VSMovIts."SumVRC">,MD002,1),
      SUM(<frxDs14VSMovIts."QtdGerArM2">,MD002,1),
      SUM(<frxDs14VSMovIts."VNC_Kg">,MD002,1)
*)
var
  FatorMP, FatorAR: Double;
begin
  if PesoKg = 0  then
    FatorMP := 0
  else
    FatorMP := VNC_Kg / PesoKg;
  //
  if SumVRC = 0  then
    FatorAR := 0
  else
    FatorAR := SumVRC / QtdGerArM2;
  //
  Result := VS_PF.NotaCouroRibeiraApuca(Inteiros, PesoKg, QtdGerArM2, FatorMP, FatorAR);
end;

end.
