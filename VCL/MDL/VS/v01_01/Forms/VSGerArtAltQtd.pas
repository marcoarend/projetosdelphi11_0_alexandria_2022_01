unit VSGerArtAltQtd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, Vcl.Grids, Vcl.DBGrids,
  dmkEditCalc, AppListas, dmkCheckGroup, Vcl.Menus, UnProjGroup_Consts,
  Vcl.ComCtrls, dmkEditDateTimePicker, UnAppEnums;

type
  TFmVSGerArtAltQtd = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    PnGerar: TPanel;
    GBGerar: TGroupBox;
    EdSrcNivel2: TdmkEdit;
    EdSrcNivel1: TdmkEdit;
    EdSrcMovID: TdmkEdit;
    Label14: TLabel;
    Label1: TLabel;
    Label17: TLabel;
    EdCodigo: TdmkEdit;
    EdMovimCod: TdmkEdit;
    Label2: TLabel;
    EdSrcGGX: TdmkEdit;
    QrNiv1: TmySQLQuery;
    QrNiv1FatorInt: TFloatField;
    GroupBox2: TGroupBox;
    Panel3: TPanel;
    Label6: TLabel;
    EdControleBxa: TdmkEdit;
    LaPecasBxa: TLabel;
    EdPecasBxa: TdmkEdit;
    LaPesoKgBxa: TLabel;
    EdPesoKgBxa: TdmkEdit;
    SbPesoKgBxa: TSpeedButton;
    LaQtdGerArM2Bxa: TLabel;
    EdQtdGerArM2Bxa: TdmkEditCalc;
    LaQtdGerArP2Bxa: TLabel;
    EdQtdGerArP2Bxa: TdmkEditCalc;
    Label9: TLabel;
    EdObservBxa: TdmkEdit;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    LaQtdGerArM2Src: TLabel;
    LaQtdGerArP2Src: TLabel;
    Label12: TLabel;
    EdControleSrc: TdmkEdit;
    EdPecasSrc: TdmkEdit;
    EdPesoKgSrc: TdmkEdit;
    EdQtdGerArM2Src: TdmkEditCalc;
    EdQtdGerArP2Src: TdmkEditCalc;
    EdObservSrc: TdmkEdit;
    Label13: TLabel;
    EdMovimTwn: TdmkEdit;
    QrNiv1MediaMinM2: TFloatField;
    QrNiv1MediaMaxM2: TFloatField;
    PMPesoKgBxa: TPopupMenu;
    Usarpesototaldeorigem1: TMenuItem;
    InformarpesoManualmente1: TMenuItem;
    N1: TMenuItem;
    Voltaraomodoautomtico1: TMenuItem;
    EdSrcValorT: TdmkEdit;
    Label8: TLabel;
    TPData: TdmkEditDateTimePicker;
    Label11: TLabel;
    EdHora: TdmkEdit;
    EdQtdGerPeso: TdmkEdit;
    Label10: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPecasBxaChange(Sender: TObject);
    procedure EdPecasSrcKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbPesoKgBxaClick(Sender: TObject);
    procedure Usarpesototaldeorigem1Click(Sender: TObject);
    procedure InformarpesoManualmente1Click(Sender: TObject);
    procedure Voltaraomodoautomtico1Click(Sender: TObject);
    procedure EdPesoKgBxaChange(Sender: TObject);
  private
    { Private declarations }
    FFatorIntSrc, FFatorIntDst: Integer;
    FMediaMinM2, FMediaMaxM2: Double;
    FInfoManual: Boolean;
    //
    procedure ReopenVSGerArtSrc(Controle: Integer);
    function  ValorTParcial(): Double;
    function  ValorMPParcial(): Double;
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FOrigMovimNiv: TEstqMovimNiv;
    FEmpresa, FOrigMovimCod, FOrigCodigo, FNewGraGruX, FTipoArea:
    Integer;
    //FDataHora: TDateTime;
    //
    FInnGraGruX: Integer;
    FFator, FAntPecas, FAntPesoKg, FAntValorT, FAntValorMP, FCustoMOKg: Double;
    FControleInn: Integer;
    //
    procedure DefineTipoArea();
    procedure LiberaEdicao();
  end;

  var
  FmVSGerArtAltQtd: TFmVSGerArtAltQtd;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
{$IFDEF sAllVS} UnVS_PF, {$ENDIF}
  VSGerArtCab, UnVS_CRC_PF, GetValor, ModVS_CRC;

{$R *.DFM}

procedure TFmVSGerArtAltQtd.BtOKClick(Sender: TObject);
var
  Codigo: Integer;
  FatorMP, FatorAR, NotaMPAG, FatNotaVNC, FatNotaVRC, Pecas, PesoKg, QtdGerArM2,
  QtdGerArP2, QtdGerPeso, ValorMP, ValorT, CustoMOTot, Estq: Double;
  Observ, DataHora: String;
  Controle, IMEIArtGer, MovimCod: Integer;
  Data: TDateTime;
begin
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControleBxa.ValueVariant;
  Pecas          := -EdPecasBxa.ValueVariant;
  PesoKg         := -EdPesoKgBxa.ValueVariant;
  QtdGerArM2     := -EdQtdGerArM2Src.ValueVariant;
  QtdGerArP2     := -EdQtdGerArP2Src.ValueVariant;
  QtdGerPeso     := -EdQtdGerPeso.ValueVariant;
  ValorMP        := -ValorMPParcial();
  ValorT         := -ValorTParcial();
  Observ         := EdObservBxa.Text;
  DataHora       := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  Data           := Geral.ValidaDataHoraSQL(DataHora);
  {$IFDEF sAllVS}
  if VS_PF.ImpedePeloBalanco(Data, False, True) then
    Exit;
  {$ENDIF}
  //
  Estq := QtdGerArM2 + QtdGerPeso;
  if MyObjects.FIC(-Estq < 0.01, EdQtdGerArM2Src, 'Informe a  �rea!') then
    Exit;
  if not VS_CRC_PF.AreaEstaNaMedia(
    EdPecasSrc.ValueVariant, -QtdGerArM2, FMediaMinM2, FMediaMaxM2, True) then
      Exit;
  //
  //
  FatorMP  := VS_CRC_PF.FatorNotaMP(FInnGraGruX);
  FatorAR  := VS_CRC_PF.FatorNotaAR(FNewGraGruX);
  NotaMPAG := VS_CRC_PF.NotaCouroRibeiraApuca(-Pecas, -PesoKg, -QtdGerArM2, FatorMP, FatorAR);
  FatNotaVRC := FatorAR;
  FatNotaVNC := FatorMP;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, CO_UPD_TAB_VMI, False, [
  'Pecas', 'PesoKg', 'QtdGerArM2',
  'QtdGerArP2', 'QtdGerPeso',
  'ValorMP', 'ValorT',
  'Observ', 'NotaMPAG', CO_DATA_HORA_VMI], [
  'Controle'], [
  Pecas, PesoKg, QtdGerArM2,
  QtdGerArP2, QtdGerPeso,
  ValorMP, ValorT,
  Observ, NotaMPAG, DataHora], [
  Controle], True) then
  begin
    VS_CRC_PF.AtualizaNotaMPAG(Controle, NotaMPAG, FatNotaVNC, FatNotaVRC);
    // Geracao Couro Curtido
    Controle       := EdControleSrc.ValueVariant;
    Pecas          := EdPecasSrc.ValueVariant;
    PesoKg         := EdPesoKgBxa.ValueVariant;
    QtdGerArM2     := EdQtdGerArM2Src.ValueVariant;
    QtdGerArP2     := EdQtdGerArP2Src.ValueVariant;
    QtdGerPeso     := EdQtdGerPeso.ValueVariant;
    CustoMOTot     := FCustoMOKg * PesoKg;
    ValorMP        := ValorMPParcial();
    ValorT         := ValorTParcial();
    Observ         := EdObservSrc.Text;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, CO_UPD_TAB_VMI, False, [
    'Pecas', 'PesoKg', 'QtdGerArM2',
    'QtdGerArP2', 'QtdGerPeso',
    'ValorMP', 'ValorT',
    'Observ', 'NotaMPAG', 'CustoMOTot',
    CO_DATA_HORA_VMI], [
    'Controle'], [
    Pecas, PesoKg, QtdGerArM2,
    QtdGerArP2, QtdGerPeso,
    ValorMP, ValorT,
    Observ, NotaMPAG, CustoMOTot,
    DataHora], [
    Controle], True) then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Artualizando nota MPAG');
      VS_CRC_PF.AtualizaNotaMPAG(Controle, NotaMPAG, FatNotaVNC, FatNotaVRC);
      // Nao se aplica!
      //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      // Atualizar dados de baixa do In Natura selecionado
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Artualizando estoque');
      VS_CRC_PF.AtualizaSaldoIMEI(FControleInn(*QrAptosControle.Value*), True);
      //
      //
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Artualizando custos de IME-Is filhos');
      VS_CRC_PF.AtualizaValoresDescendArtGerAposInfoAreaTotal(EdSrcNivel2.ValueVariant,
      EdQtdGerArM2Src.ValueVariant, EdSrcValorT.ValueVariant);
      //
      //
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Artualizando custos do cabe�alho');
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
      VS_CRC_PF.DistribuiCustoIndsVS(FOrigMovimNiv, FOrigMovimCod, FOrigCodigo,
        EdSrcNivel2.ValueVariant);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Artualizando outros dados do cabe�alho');
      // Encerramento e saldos caecalho
      if TEstqMovimID(FQrIts.FieldByName('SrcMovID').AsInteger) = emidEmProcCur then
      begin
        MovimCod := VS_CRC_PF.ObtemMovimCodDeMovimIDECodigo(
        TEstqMovimID(FQrIts.FieldByName('SrcMovID').AsInteger),
        FQrIts.FieldByName('SrcNivel1').AsInteger);
        //
        DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(MovimCod);
      end;
      //
      FmVSGerArtCab.LocCod(Codigo, Codigo);
      ReopenVSGerArtSrc(Controle);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
      //
      Close;
    end;
  end;
end;

procedure TFmVSGerArtAltQtd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSGerArtAltQtd.DefineTipoArea();
begin
  LaQtdGerArM2Src.Enabled := FTipoArea = 0;
  EdQtdGerArM2Src.Enabled := FTipoArea = 0;
  //
  LaQtdGerArP2Src.Enabled := FTipoArea = 1;
  EdQtdGerArP2Src.Enabled := FTipoArea = 1;
end;

procedure TFmVSGerArtAltQtd.EdPecasBxaChange(Sender: TObject);
  procedure CalculaPesoParcial();
  var
    Pecas, PesoKg: Double;
  begin
    Pecas  := EdPecasBxa.ValueVariant;
    PesoKg := 0;
    if FAntPecas > 0 then
      PesoKg := Pecas / FAntPecas * FAntPesoKg;
    //
    EdPesoKgBxa.ValueVariant := PesoKg;
  end;
var
  Pecas: Double;
begin
  Pecas := EdPecasBxa.ValueVariant;
  if not FInfoManual then
    CalculaPesoParcial();
  if (FFatorIntSrc <> 0) and (FFatorIntDst <> 0) then
    EdPecasSrc.ValueVariant := Pecas * FFatorIntSrc / FFatorIntDst;
end;

procedure TFmVSGerArtAltQtd.EdPecasSrcKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Fator: Integer;
begin
  if Key = VK_F5 then
  begin
    Fator := MyObjects.SelRadioGroup('Fator de divis�o',
    'Escolha a forma de divis�o', [
    'Sem reparti��o ao meio',
    'Foi repartido de inteiros para meios'],
    -1);
    case Fator of
      0: Fator := 1;
      1: Fator := 2;
      else Fator := 0;
    end;
    if Fator <> 0 then
      EdPecasSrc.ValueVariant := EdPecasBxa.ValueVariant * Fator;
  end;
end;

procedure TFmVSGerArtAltQtd.EdPesoKgBxaChange(Sender: TObject);
begin
  EdPesoKgSrc.ValueVariant := EdPesoKgBxa.ValueVariant;
end;

procedure TFmVSGerArtAltQtd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSGerArtAltQtd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FInfoManual := False;
end;

procedure TFmVSGerArtAltQtd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSGerArtAltQtd.InformarpesoManualmente1Click(Sender: TObject);
var
  ResVar: Variant;
  PesoKg: Double;
begin
  PesoKg := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger,
  0, 0, 0, '', '', True, 'Peso kg de baixa ', 'Informe o peso em kg: ',
  0, ResVar) then
  begin
    FInfoManual := True;
    PesoKg := Geral.DMV(ResVar);
    EdPesoKgBxa.ValueVariant := PesoKg;
  end;
end;

procedure TFmVSGerArtAltQtd.LiberaEdicao();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, Dmod.MyDB, [
  'SELECT co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
  'FROM couNiv1 co1 ',
  'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
  'WHERE ggc.GraGruX=' + Geral.FF0(EdSrcGGX.ValueVariant),
  '']);
  FFatorIntDst := Trunc(QrNiv1FatorInt.Value * 1000);
  FMediaMinM2  := QrNiv1MediaMinM2.Value;
  FMediaMaxM2  := QrNiv1MediaMaxM2.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNiv1, Dmod.MyDB, [
  'SELECT co1.FatorInt, ggc.MediaMinM2, ggc.MediaMaxM2 ',
  'FROM couNiv1 co1 ',
  'LEFT JOIN gragruxcou ggc ON ggc.CouNiv1=co1.Codigo ',
  'WHERE ggc.GraGruX=' + Geral.FF0(FInnGraGruX),
  '']);
  FFatorIntSrc := Trunc(QrNiv1FatorInt.Value * 1000);
  //
  if (FFatorIntSrc = 0) or (FFatorIntDst = 0) then
  begin
    Geral.MB_Aviso('Sele��o abortada!' + sLineBreak +
    'O Artigo "' + FmVSGerArtCab.QrVSGerArtNewNO_PRD_TAM_COR.Value +
    '" n�o est� configurado completamente!' + sLineBreak +
    'Termine sua configura��o antes de continuar!');
    //
    Close;
    Exit;
  end;
  if FFatorIntSrc <> FFatorIntDst then
    Geral.MB_Aviso('CUIDADO!!!' + sLineBreak +
    'Ao informar a quantidade leve em considera��o a possibilidade de transform��o de inteiros para meios!!!');
end;

procedure TFmVSGerArtAltQtd.ReopenVSGerArtSrc(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmVSGerArtAltQtd.SbPesoKgBxaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesoKgBxa, SbPesoKgBxa);
end;

procedure TFmVSGerArtAltQtd.Usarpesototaldeorigem1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Controle: Integer;
  PesoKg: Double;
  //
  procedure ReopenQry();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT * FROM ' + CO_SEL_TAB_VMI,
    'WHERE Controle=' + Geral.FF0(Controle),
    '']);
  end;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    Controle := EdControleBxa.ValueVariant;
    ReopenQry();
    //
    Controle := Qry.FieldByName('SrcNivel2').AsInteger;
    ReopenQry();
    //
    FInfoManual := True;
    PesoKg := Qry.FieldByName('PesoKg').AsFloat;
    EdPesoKgBxa.ValueVariant := PesoKg;
  finally
    Qry.Free;
  end;
end;

function TFmVSGerArtAltQtd.ValorTParcial(): Double;
begin
  if FAntPesoKg> 0 then
    Result := EdPesoKgBxa.ValueVariant / FAntPesoKg* FAntValorT
  else
    Result := 0;
end;

function TFmVSGerArtAltQtd.ValorMPParcial(): Double;
begin
  if FAntPesoKg> 0 then
    Result := EdPesoKgBxa.ValueVariant / FAntPesoKg* FAntValorMP
  else
    Result := 0;
end;

procedure TFmVSGerArtAltQtd.Voltaraomodoautomtico1Click(Sender: TObject);
begin
  FInfoManual := False;
  EdPecasBxaChange(Self);
end;

end.
