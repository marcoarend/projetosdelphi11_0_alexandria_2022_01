unit VSTrfLocCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet,
  UnProjGroup_Consts, UnGrl_Consts, UnGrl_Geral, UnAppEnums, dmkDBGridZTO,
  UnProjGroup_Vars;

type
  TFmVSTrfLocCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrVSTrfLocCab: TmySQLQuery;
    DsVSTrfLocCab: TDataSource;
    QrIMEIDest: TmySQLQuery;
    DsIMEIDest: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExcluiIMEI1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDtVenda: TdmkEditDateTimePicker;
    EdDtVenda: TdmkEdit;
    Label53: TLabel;
    TPDtViagem: TdmkEditDateTimePicker;
    EdDtViagem: TdmkEdit;
    Label3: TLabel;
    TPDtEntrega: TdmkEditDateTimePicker;
    EdDtEntrega: TdmkEdit;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTIDADE: TWideStringField;
    DsCliente: TDataSource;
    QrTransporta: TmySQLQuery;
    DsTransporta: TDataSource;
    QrTransportaCodigo: TIntegerField;
    QrTransportaNOMEENTIDADE: TWideStringField;
    Label4: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrVSTrfLocCabCodigo: TIntegerField;
    QrVSTrfLocCabMovimCod: TIntegerField;
    QrVSTrfLocCabEmpresa: TIntegerField;
    QrVSTrfLocCabDtVenda: TDateTimeField;
    QrVSTrfLocCabDtViagem: TDateTimeField;
    QrVSTrfLocCabDtEntrega: TDateTimeField;
    QrVSTrfLocCabCliente: TIntegerField;
    QrVSTrfLocCabTransporta: TIntegerField;
    QrVSTrfLocCabPecas: TFloatField;
    QrVSTrfLocCabPesoKg: TFloatField;
    QrVSTrfLocCabAreaM2: TFloatField;
    QrVSTrfLocCabAreaP2: TFloatField;
    QrVSTrfLocCabLk: TIntegerField;
    QrVSTrfLocCabDataCad: TDateField;
    QrVSTrfLocCabDataAlt: TDateField;
    QrVSTrfLocCabUserCad: TIntegerField;
    QrVSTrfLocCabUserAlt: TIntegerField;
    QrVSTrfLocCabAlterWeb: TSmallintField;
    QrVSTrfLocCabAtivo: TSmallintField;
    QrVSTrfLocCabNO_EMPRESA: TWideStringField;
    QrVSTrfLocCabNO_CLIENTE: TWideStringField;
    QrVSTrfLocCabNO_TRANSPORTA: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GBIts: TGroupBox;
    PorIMEI1: TMenuItem;
    PorPallet1: TMenuItem;
    PMImprime: TPopupMenu;
    PackingList1: TMenuItem;
    QrPallets: TmySQLQuery;
    DsPallets: TDataSource;
    PCItens: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DGDados: TDBGrid;
    DBGrid1: TDBGrid;
    QrPalletsArtigoImp: TWideStringField;
    QrPalletsClasseImp: TWideStringField;
    QrPalletsSEQ: TIntegerField;
    frxDsPallets: TfrxDBDataset;
    frxWET_CURTI_019_00_A: TfrxReport;
    QrPalletsMediaM2: TFloatField;
    QrPalletsFaixaMediaM2: TFloatField;
    Porkgsubproduto1: TMenuItem;
    InformaNmerodaRME1: TMenuItem;
    TabSheet3: TTabSheet;
    QrVSOutNFeIts: TmySQLQuery;
    DsVSOutNFeIts: TDataSource;
    DBGrid2: TDBGrid;
    QrVSOutNFeItsNO_PRD_TAM_COR: TWideStringField;
    QrVSOutNFeItsCodigo: TIntegerField;
    QrVSOutNFeItsControle: TIntegerField;
    QrVSOutNFeItsItemNFe: TIntegerField;
    QrVSOutNFeItsGraGruX: TIntegerField;
    QrVSOutNFeItsPecas: TFloatField;
    QrVSOutNFeItsPesoKg: TFloatField;
    QrVSOutNFeItsAreaM2: TFloatField;
    QrVSOutNFeItsAreaP2: TFloatField;
    QrVSOutNFeItsValorT: TFloatField;
    QrVSOutNFeItsTpCalcVal: TSmallintField;
    QrVSOutNFeItsValorU: TFloatField;
    QrVSOutNFeItsTribDefSel: TIntegerField;
    QrVSOutNFeItsLk: TIntegerField;
    QrVSOutNFeItsDataCad: TDateField;
    QrVSOutNFeItsDataAlt: TDateField;
    QrVSOutNFeItsUserCad: TIntegerField;
    QrVSOutNFeItsUserAlt: TIntegerField;
    QrVSOutNFeItsAlterWeb: TSmallintField;
    QrVSOutNFeItsAtivo: TSmallintField;
    BtVSOutNFeCab: TBitBtn;
    PMVSOutNFeIts: TPopupMenu;
    IncluiitemdeNF1: TMenuItem;
    AlteraitemdeNF1: TMenuItem;
    ExcluiitemdeNF1: TMenuItem;
    QrTribIncIts: TmySQLQuery;
    QrTribIncItsFatID: TIntegerField;
    QrTribIncItsFatNum: TIntegerField;
    QrTribIncItsFatParcela: TIntegerField;
    QrTribIncItsEmpresa: TIntegerField;
    QrTribIncItsControle: TIntegerField;
    QrTribIncItsData: TDateField;
    QrTribIncItsHora: TTimeField;
    QrTribIncItsValorFat: TFloatField;
    QrTribIncItsBaseCalc: TFloatField;
    QrTribIncItsValrTrib: TFloatField;
    QrTribIncItsPercent: TFloatField;
    QrTribIncItsLk: TIntegerField;
    QrTribIncItsDataCad: TDateField;
    QrTribIncItsDataAlt: TDateField;
    QrTribIncItsUserCad: TIntegerField;
    QrTribIncItsUserAlt: TIntegerField;
    QrTribIncItsAlterWeb: TSmallintField;
    QrTribIncItsAtivo: TSmallintField;
    QrTribIncItsTributo: TIntegerField;
    QrTribIncItsVUsoCalc: TFloatField;
    QrTribIncItsOperacao: TSmallintField;
    QrTribIncItsNO_Tributo: TWideStringField;
    DsTribIncIts: TDataSource;
    InformaItemdeNFe1: TMenuItem;
    GroupBox3: TGroupBox;
    DBGrid4: TDBGrid;
    BtTribIncIts: TBitBtn;
    PMTribIncIts: TPopupMenu;
    IncluiTributo1: TMenuItem;
    AlteraTributoAtual1: TMenuItem;
    ExcluiTributoAtual1: TMenuItem;
    QrTribIncItsFatorDC: TSmallintField;
    QrPalletsTXT_ARTIGO: TWideStringField;
    QrIMEIDestCodigo: TLargeintField;
    QrIMEIDestControle: TLargeintField;
    QrIMEIDestMovimCod: TLargeintField;
    QrIMEIDestMovimNiv: TLargeintField;
    QrIMEIDestMovimTwn: TLargeintField;
    QrIMEIDestEmpresa: TLargeintField;
    QrIMEIDestTerceiro: TLargeintField;
    QrIMEIDestCliVenda: TLargeintField;
    QrIMEIDestMovimID: TLargeintField;
    QrIMEIDestDataHora: TDateTimeField;
    QrIMEIDestPallet: TLargeintField;
    QrIMEIDestGraGruX: TLargeintField;
    QrIMEIDestPecas: TFloatField;
    QrIMEIDestPesoKg: TFloatField;
    QrIMEIDestAreaM2: TFloatField;
    QrIMEIDestAreaP2: TFloatField;
    QrIMEIDestValorT: TFloatField;
    QrIMEIDestSrcMovID: TLargeintField;
    QrIMEIDestSrcNivel1: TLargeintField;
    QrIMEIDestSrcNivel2: TLargeintField;
    QrIMEIDestSrcGGX: TLargeintField;
    QrIMEIDestSdoVrtPeca: TFloatField;
    QrIMEIDestSdoVrtPeso: TFloatField;
    QrIMEIDestSdoVrtArM2: TFloatField;
    QrIMEIDestObserv: TWideStringField;
    QrIMEIDestSerieFch: TLargeintField;
    QrIMEIDestFicha: TLargeintField;
    QrIMEIDestMisturou: TLargeintField;
    QrIMEIDestFornecMO: TLargeintField;
    QrIMEIDestCustoMOKg: TFloatField;
    QrIMEIDestCustoMOM2: TFloatField;
    QrIMEIDestCustoMOTot: TFloatField;
    QrIMEIDestValorMP: TFloatField;
    QrIMEIDestDstMovID: TLargeintField;
    QrIMEIDestDstNivel1: TLargeintField;
    QrIMEIDestDstNivel2: TLargeintField;
    QrIMEIDestDstGGX: TLargeintField;
    QrIMEIDestQtdGerPeca: TFloatField;
    QrIMEIDestQtdGerPeso: TFloatField;
    QrIMEIDestQtdGerArM2: TFloatField;
    QrIMEIDestQtdGerArP2: TFloatField;
    QrIMEIDestQtdAntPeca: TFloatField;
    QrIMEIDestQtdAntPeso: TFloatField;
    QrIMEIDestQtdAntArM2: TFloatField;
    QrIMEIDestQtdAntArP2: TFloatField;
    QrIMEIDestNotaMPAG: TFloatField;
    QrIMEIDestPedItsFin: TLargeintField;
    QrIMEIDestMarca: TWideStringField;
    QrIMEIDestStqCenLoc: TLargeintField;
    QrIMEIDestNO_PALLET: TWideStringField;
    QrIMEIDestNO_PRD_TAM_COR: TWideStringField;
    QrIMEIDestNO_TTW: TWideStringField;
    QrIMEIDestID_TTW: TLargeintField;
    QrIMEIDestReqMovEstq: TLargeintField;
    QrIMEIDestNO_FORNECE: TWideStringField;
    QrVSTrfLocCabTemIMEIMrt: TIntegerField;
    QrPalletsPallet: TLargeintField;
    QrPalletsGraGruX: TLargeintField;
    QrPalletsPecas: TFloatField;
    QrPalletsAreaM2: TFloatField;
    QrPalletsAreaP2: TFloatField;
    QrPalletsPesoKg: TFloatField;
    QrPalletsNO_PRD_TAM_COR: TWideStringField;
    QrPalletsNO_Pallet: TWideStringField;
    QrPalletsSerieFch: TLargeintField;
    QrPalletsFicha: TLargeintField;
    QrPalletsNO_TTW: TWideStringField;
    QrPalletsID_TTW: TLargeintField;
    N1: TMenuItem;
    CorrigeFornecedores1: TMenuItem;
    QrIMEISorc: TmySQLQuery;
    DsIMEISrc: TDataSource;
    QrIMEISorcCodigo: TLargeintField;
    QrIMEISorcControle: TLargeintField;
    QrIMEISorcMovimCod: TLargeintField;
    QrIMEISorcMovimNiv: TLargeintField;
    QrIMEISorcMovimTwn: TLargeintField;
    QrIMEISorcEmpresa: TLargeintField;
    QrIMEISorcTerceiro: TLargeintField;
    QrIMEISorcCliVenda: TLargeintField;
    QrIMEISorcMovimID: TLargeintField;
    QrIMEISorcDataHora: TDateTimeField;
    QrIMEISorcPallet: TLargeintField;
    QrIMEISorcGraGruX: TLargeintField;
    QrIMEISorcPecas: TFloatField;
    QrIMEISorcPesoKg: TFloatField;
    QrIMEISorcAreaM2: TFloatField;
    QrIMEISorcAreaP2: TFloatField;
    QrIMEISorcValorT: TFloatField;
    QrIMEISorcSrcMovID: TLargeintField;
    QrIMEISorcSrcNivel1: TLargeintField;
    QrIMEISorcSrcNivel2: TLargeintField;
    QrIMEISorcSrcGGX: TLargeintField;
    QrIMEISorcSdoVrtPeca: TFloatField;
    QrIMEISorcSdoVrtPeso: TFloatField;
    QrIMEISorcSdoVrtArM2: TFloatField;
    QrIMEISorcObserv: TWideStringField;
    QrIMEISorcSerieFch: TLargeintField;
    QrIMEISorcFicha: TLargeintField;
    QrIMEISorcMisturou: TLargeintField;
    QrIMEISorcFornecMO: TLargeintField;
    QrIMEISorcCustoMOKg: TFloatField;
    QrIMEISorcCustoMOM2: TFloatField;
    QrIMEISorcCustoMOTot: TFloatField;
    QrIMEISorcValorMP: TFloatField;
    QrIMEISorcDstMovID: TLargeintField;
    QrIMEISorcDstNivel1: TLargeintField;
    QrIMEISorcDstNivel2: TLargeintField;
    QrIMEISorcDstGGX: TLargeintField;
    QrIMEISorcQtdGerPeca: TFloatField;
    QrIMEISorcQtdGerPeso: TFloatField;
    QrIMEISorcQtdGerArM2: TFloatField;
    QrIMEISorcQtdGerArP2: TFloatField;
    QrIMEISorcQtdAntPeca: TFloatField;
    QrIMEISorcQtdAntPeso: TFloatField;
    QrIMEISorcQtdAntArM2: TFloatField;
    QrIMEISorcQtdAntArP2: TFloatField;
    QrIMEISorcNotaMPAG: TFloatField;
    QrIMEISorcPedItsFin: TLargeintField;
    QrIMEISorcMarca: TWideStringField;
    QrIMEISorcStqCenLoc: TLargeintField;
    QrIMEISorcNO_FORNECE: TWideStringField;
    QrIMEISorcNO_PALLET: TWideStringField;
    QrIMEISorcNO_PRD_TAM_COR: TWideStringField;
    QrIMEISorcNO_TTW: TWideStringField;
    QrIMEISorcID_TTW: TLargeintField;
    QrIMEISorcReqMovEstq: TLargeintField;
    DBGrid3: TDBGrid;
    Label9: TLabel;
    QrVSOutNFeCab: TmySQLQuery;
    QrVSOutNFeCabCodigo: TIntegerField;
    QrVSOutNFeCabMovimCod: TIntegerField;
    QrVSOutNFeCabide_serie: TIntegerField;
    QrVSOutNFeCabide_nNF: TIntegerField;
    QrVSOutNFeCabOriCod: TIntegerField;
    QrVSOutNFeCabLk: TIntegerField;
    QrVSOutNFeCabDataCad: TDateField;
    QrVSOutNFeCabDataAlt: TDateField;
    QrVSOutNFeCabUserCad: TIntegerField;
    QrVSOutNFeCabUserAlt: TIntegerField;
    QrVSOutNFeCabAlterWeb: TSmallintField;
    QrVSOutNFeCabAtivo: TSmallintField;
    DsVSOutNFeCab: TDataSource;
    DBGrid5: TDBGrid;
    BtVSOutNFeIts: TBitBtn;
    PMVSOutNFeCab: TPopupMenu;
    IncluiSerieNumeroDeNFe1: TMenuItem;
    AlteraSerieNumeroDeNFe1: TMenuItem;
    ExcluiSerieNumeroDeNFe1: TMenuItem;
    frxDsVSOutNFeCab: TfrxDBDataset;
    frxDsVSOutNFeIts: TfrxDBDataset;
    Edide_serie: TdmkEdit;
    Label28: TLabel;
    Label29: TLabel;
    Edide_nNF: TdmkEdit;
    QrVSTrfLocCabide_serie: TSmallintField;
    QrVSTrfLocCabide_nNF: TIntegerField;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    LaCliente: TLabel;
    QrClienteCNPJ_CPF: TWideStringField;
    AlteraLocaldeestoque1: TMenuItem;
    QrIMEIDestIxxMovIX: TLargeintField;
    QrIMEIDestIxxFolha: TLargeintField;
    QrIMEIDestIxxLinha: TLargeintField;
    InformaIEC1: TMenuItem;
    TsFrCompr: TTabSheet;
    QrVSMOEnvAvu: TmySQLQuery;
    QrVSMOEnvAvuCodigo: TIntegerField;
    QrVSMOEnvAvuCFTMA_FatID: TIntegerField;
    QrVSMOEnvAvuCFTMA_FatNum: TIntegerField;
    QrVSMOEnvAvuCFTMA_Empresa: TIntegerField;
    QrVSMOEnvAvuCFTMA_Terceiro: TIntegerField;
    QrVSMOEnvAvuCFTMA_nItem: TIntegerField;
    QrVSMOEnvAvuCFTMA_SerCT: TIntegerField;
    QrVSMOEnvAvuCFTMA_nCT: TIntegerField;
    QrVSMOEnvAvuCFTMA_Pecas: TFloatField;
    QrVSMOEnvAvuCFTMA_PesoKg: TFloatField;
    QrVSMOEnvAvuCFTMA_AreaM2: TFloatField;
    QrVSMOEnvAvuCFTMA_AreaP2: TFloatField;
    QrVSMOEnvAvuCFTMA_PesTrKg: TFloatField;
    QrVSMOEnvAvuCFTMA_CusTrKg: TFloatField;
    QrVSMOEnvAvuCFTMA_ValorT: TFloatField;
    QrVSMOEnvAvuVSVMI_MovimCod: TIntegerField;
    QrVSMOEnvAvuLk: TIntegerField;
    QrVSMOEnvAvuDataCad: TDateField;
    QrVSMOEnvAvuDataAlt: TDateField;
    QrVSMOEnvAvuUserCad: TIntegerField;
    QrVSMOEnvAvuUserAlt: TIntegerField;
    QrVSMOEnvAvuAlterWeb: TSmallintField;
    QrVSMOEnvAvuAWServerID: TIntegerField;
    QrVSMOEnvAvuAWStatSinc: TSmallintField;
    QrVSMOEnvAvuAtivo: TSmallintField;
    DsVSMOEnvAvu: TDataSource;
    QrVSMOEnvAVMI: TmySQLQuery;
    QrVSMOEnvAVMICodigo: TIntegerField;
    QrVSMOEnvAVMIVSMOEnvAvu: TIntegerField;
    QrVSMOEnvAVMIVSMovIts: TIntegerField;
    QrVSMOEnvAVMILk: TIntegerField;
    QrVSMOEnvAVMIDataCad: TDateField;
    QrVSMOEnvAVMIDataAlt: TDateField;
    QrVSMOEnvAVMIUserCad: TIntegerField;
    QrVSMOEnvAVMIUserAlt: TIntegerField;
    QrVSMOEnvAVMIAlterWeb: TSmallintField;
    QrVSMOEnvAVMIAWServerID: TIntegerField;
    QrVSMOEnvAVMIAWStatSinc: TSmallintField;
    QrVSMOEnvAVMIAtivo: TSmallintField;
    QrVSMOEnvAVMIValorFrete: TFloatField;
    QrVSMOEnvAVMIPecas: TFloatField;
    QrVSMOEnvAVMIPesoKg: TFloatField;
    QrVSMOEnvAVMIAreaM2: TFloatField;
    QrVSMOEnvAVMIAreaP2: TFloatField;
    DsVSMOEnvAVMI: TDataSource;
    PnFrCompr: TPanel;
    DBGVSMOEnvAvu: TdmkDBGridZTO;
    DBGVSMOEnvAVMI: TdmkDBGridZTO;
    AtrelamentoFreteCompra1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    IncluiAtrelamento3: TMenuItem;
    AlteraAtrelamento3: TMenuItem;
    ExcluiAtrelamento3: TMenuItem;
    QrIMEIDestCusFrtAvuls: TFloatField;
    TsEnvioMO: TTabSheet;
    TsRetornoMO: TTabSheet;
    QrVSMOEnvEnv: TmySQLQuery;
    QrVSMOEnvEnvCodigo: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatID: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatNum: TIntegerField;
    QrVSMOEnvEnvNFEMP_Empresa: TIntegerField;
    QrVSMOEnvEnvNFEMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvNFEMP_nItem: TIntegerField;
    QrVSMOEnvEnvNFEMP_SerNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_nNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_Pecas: TFloatField;
    QrVSMOEnvEnvNFEMP_PesoKg: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaM2: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaP2: TFloatField;
    QrVSMOEnvEnvNFEMP_ValorT: TFloatField;
    QrVSMOEnvEnvCFTMP_FatID: TIntegerField;
    QrVSMOEnvEnvCFTMP_FatNum: TIntegerField;
    QrVSMOEnvEnvCFTMP_Empresa: TIntegerField;
    QrVSMOEnvEnvCFTMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvCFTMP_nItem: TIntegerField;
    QrVSMOEnvEnvCFTMP_SerCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_nCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_Pecas: TFloatField;
    QrVSMOEnvEnvCFTMP_PesoKg: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaM2: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaP2: TFloatField;
    QrVSMOEnvEnvCFTMP_PesTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_CusTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_ValorT: TFloatField;
    QrVSMOEnvEnvVSVMI_MovimCod: TIntegerField;
    QrVSMOEnvEnvLk: TIntegerField;
    QrVSMOEnvEnvDataCad: TDateField;
    QrVSMOEnvEnvDataAlt: TDateField;
    QrVSMOEnvEnvUserCad: TIntegerField;
    QrVSMOEnvEnvUserAlt: TIntegerField;
    QrVSMOEnvEnvAlterWeb: TSmallintField;
    QrVSMOEnvEnvAWServerID: TIntegerField;
    QrVSMOEnvEnvAWStatSinc: TSmallintField;
    QrVSMOEnvEnvAtivo: TSmallintField;
    DsVSMOEnvEnv: TDataSource;
    QrVSMOEnvEVMI: TmySQLQuery;
    QrVSMOEnvEVMICodigo: TIntegerField;
    QrVSMOEnvEVMIVSMOEnvEnv: TIntegerField;
    QrVSMOEnvEVMIVSMovIts: TIntegerField;
    QrVSMOEnvEVMILk: TIntegerField;
    QrVSMOEnvEVMIDataCad: TDateField;
    QrVSMOEnvEVMIDataAlt: TDateField;
    QrVSMOEnvEVMIUserCad: TIntegerField;
    QrVSMOEnvEVMIUserAlt: TIntegerField;
    QrVSMOEnvEVMIAlterWeb: TSmallintField;
    QrVSMOEnvEVMIAWServerID: TIntegerField;
    QrVSMOEnvEVMIAWStatSinc: TSmallintField;
    QrVSMOEnvEVMIAtivo: TSmallintField;
    QrVSMOEnvEVMIValorFrete: TFloatField;
    QrVSMOEnvEVMIPecas: TFloatField;
    QrVSMOEnvEVMIPesoKg: TFloatField;
    QrVSMOEnvEVMIAreaM2: TFloatField;
    QrVSMOEnvEVMIAreaP2: TFloatField;
    DsVSMOEnvEVMI: TDataSource;
    QrVSMOEnvRet: TmySQLQuery;
    QrVSMOEnvRetCodigo: TIntegerField;
    QrVSMOEnvRetNFCMO_FatID: TIntegerField;
    QrVSMOEnvRetNFCMO_FatNum: TIntegerField;
    QrVSMOEnvRetNFCMO_Empresa: TIntegerField;
    QrVSMOEnvRetNFCMO_nItem: TIntegerField;
    QrVSMOEnvRetNFCMO_SerNF: TIntegerField;
    QrVSMOEnvRetNFCMO_nNF: TIntegerField;
    QrVSMOEnvRetNFCMO_Pecas: TFloatField;
    QrVSMOEnvRetNFCMO_PesoKg: TFloatField;
    QrVSMOEnvRetNFCMO_AreaM2: TFloatField;
    QrVSMOEnvRetNFCMO_AreaP2: TFloatField;
    QrVSMOEnvRetNFCMO_CusMOM2: TFloatField;
    QrVSMOEnvRetNFCMO_CusMOKG: TFloatField;
    QrVSMOEnvRetNFCMO_ValorT: TFloatField;
    QrVSMOEnvRetNFCMO_Terceiro: TIntegerField;
    QrVSMOEnvRetNFRMP_FatID: TIntegerField;
    QrVSMOEnvRetNFRMP_FatNum: TIntegerField;
    QrVSMOEnvRetNFRMP_Empresa: TIntegerField;
    QrVSMOEnvRetNFRMP_nItem: TIntegerField;
    QrVSMOEnvRetNFRMP_SerNF: TIntegerField;
    QrVSMOEnvRetNFRMP_nNF: TIntegerField;
    QrVSMOEnvRetNFRMP_Pecas: TFloatField;
    QrVSMOEnvRetNFRMP_PesoKg: TFloatField;
    QrVSMOEnvRetNFRMP_AreaM2: TFloatField;
    QrVSMOEnvRetNFRMP_AreaP2: TFloatField;
    QrVSMOEnvRetNFRMP_ValorT: TFloatField;
    QrVSMOEnvRetNFRMP_Terceiro: TIntegerField;
    QrVSMOEnvRetCFTPA_FatID: TIntegerField;
    QrVSMOEnvRetCFTPA_FatNum: TIntegerField;
    QrVSMOEnvRetCFTPA_Empresa: TIntegerField;
    QrVSMOEnvRetCFTPA_Terceiro: TIntegerField;
    QrVSMOEnvRetCFTPA_nItem: TIntegerField;
    QrVSMOEnvRetCFTPA_SerCT: TIntegerField;
    QrVSMOEnvRetCFTPA_nCT: TIntegerField;
    QrVSMOEnvRetCFTPA_Pecas: TFloatField;
    QrVSMOEnvRetCFTPA_PesoKg: TFloatField;
    QrVSMOEnvRetCFTPA_AreaM2: TFloatField;
    QrVSMOEnvRetCFTPA_AreaP2: TFloatField;
    QrVSMOEnvRetCFTPA_PesTrKg: TFloatField;
    QrVSMOEnvRetCFTPA_CusTrKg: TFloatField;
    QrVSMOEnvRetCFTPA_ValorT: TFloatField;
    DsVSMOEnvRet: TDataSource;
    QrVSMOEnvRVmi: TmySQLQuery;
    QrVSMOEnvRVmiCodigo: TIntegerField;
    QrVSMOEnvRVmiVSMOEnvRet: TIntegerField;
    QrVSMOEnvRVmiVSMOEnvEnv: TIntegerField;
    QrVSMOEnvRVmiPecas: TFloatField;
    QrVSMOEnvRVmiPesoKg: TFloatField;
    QrVSMOEnvRVmiAreaM2: TFloatField;
    QrVSMOEnvRVmiAreaP2: TFloatField;
    QrVSMOEnvRVmiValorFrete: TFloatField;
    QrVSMOEnvRVmiLk: TIntegerField;
    QrVSMOEnvRVmiDataCad: TDateField;
    QrVSMOEnvRVmiDataAlt: TDateField;
    QrVSMOEnvRVmiUserCad: TIntegerField;
    QrVSMOEnvRVmiUserAlt: TIntegerField;
    QrVSMOEnvRVmiAlterWeb: TSmallintField;
    QrVSMOEnvRVmiAWServerID: TIntegerField;
    QrVSMOEnvRVmiAWStatSinc: TSmallintField;
    QrVSMOEnvRVmiAtivo: TSmallintField;
    QrVSMOEnvRVmiNFEMP_SerNF: TIntegerField;
    QrVSMOEnvRVmiNFEMP_nNF: TIntegerField;
    QrVSMOEnvRVmiValorT: TFloatField;
    DsVSMOEnvRVmi: TDataSource;
    QrVSMOEnvGVmi: TmySQLQuery;
    QrVSMOEnvGVmiCodigo: TIntegerField;
    QrVSMOEnvGVmiVSMOEnvRet: TIntegerField;
    QrVSMOEnvGVmiVSMovIts: TIntegerField;
    QrVSMOEnvGVmiPecas: TFloatField;
    QrVSMOEnvGVmiPesoKg: TFloatField;
    QrVSMOEnvGVmiAreaM2: TFloatField;
    QrVSMOEnvGVmiAreaP2: TFloatField;
    QrVSMOEnvGVmiValorFrete: TFloatField;
    QrVSMOEnvGVmiLk: TIntegerField;
    QrVSMOEnvGVmiDataCad: TDateField;
    QrVSMOEnvGVmiDataAlt: TDateField;
    QrVSMOEnvGVmiUserCad: TIntegerField;
    QrVSMOEnvGVmiUserAlt: TIntegerField;
    QrVSMOEnvGVmiAlterWeb: TSmallintField;
    QrVSMOEnvGVmiAWServerID: TIntegerField;
    QrVSMOEnvGVmiAWStatSinc: TSmallintField;
    QrVSMOEnvGVmiAtivo: TSmallintField;
    QrVSMOEnvGVmiVSMovimCod: TIntegerField;
    DsVSMOEnvGVmi: TDataSource;
    PnEnvioMO: TPanel;
    DBGVSMOEnvEnv: TdmkDBGridZTO;
    DBGVSMOEnvEVmi: TdmkDBGridZTO;
    PnRetornoMO: TPanel;
    DBGVSMOEnvRet: TdmkDBGridZTO;
    PnItensRetMO: TPanel;
    Splitter2: TSplitter;
    GroupBox8: TGroupBox;
    DBGVSMOEnvRVmi: TdmkDBGridZTO;
    GroupBox9: TGroupBox;
    DBGVSMOEnvGVmi: TdmkDBGridZTO;
    AtrelamentoNFsdeMO1: TMenuItem;
    IncluiAtrelamento1: TMenuItem;
    AlteraAtrelamento1: TMenuItem;
    ExcluiAtrelamento1: TMenuItem;
    QrIMEIDestCusFrtMOEnv: TFloatField;
    QrIMEIDestCusFrtMORet: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSTrfLocCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSTrfLocCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSTrfLocCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsExcluiIMEI1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSTrfLocCabBeforeClose(DataSet: TDataSet);
    procedure PorIMEI1Click(Sender: TObject);
    procedure PorPallet1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PackingList1Click(Sender: TObject);
    procedure QrPalletsCalcFields(DataSet: TDataSet);
    procedure frxWET_CURTI_019_00_AGetValue(const VarName: string;
      var Value: Variant);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Porkgsubproduto1Click(Sender: TObject);
    procedure QrIMEIDestAfterOpen(DataSet: TDataSet);
    procedure InformaNmerodaRME1Click(Sender: TObject);
    procedure BtVSOutNFeCabClick(Sender: TObject);
    procedure IncluiitemdeNF1Click(Sender: TObject);
    procedure AlteraitemdeNF1Click(Sender: TObject);
    procedure PMVSOutNFeItsPopup(Sender: TObject);
    procedure QrVSOutNFeItsBeforeClose(DataSet: TDataSet);
    procedure QrVSOutNFeItsAfterScroll(DataSet: TDataSet);
    procedure BtTribIncItsClick(Sender: TObject);
    procedure AlteraTributoAtual1Click(Sender: TObject);
    procedure ExcluiTributoAtual1Click(Sender: TObject);
    procedure CorrigeFornecedores1Click(Sender: TObject);
    procedure QrIMEIDestBeforeClose(DataSet: TDataSet);
    procedure QrIMEIDestAfterScroll(DataSet: TDataSet);
    procedure QrVSOutNFeCabBeforeClose(DataSet: TDataSet);
    procedure QrVSOutNFeCabAfterScroll(DataSet: TDataSet);
    procedure IncluiSerieNumeroDeNFe1Click(Sender: TObject);
    procedure BtVSOutNFeItsClick(Sender: TObject);
    procedure PMVSOutNFeCabPopup(Sender: TObject);
    procedure AlteraSerieNumeroDeNFe1Click(Sender: TObject);
    procedure ExcluiSerieNumeroDeNFe1Click(Sender: TObject);
    procedure ExcluiitemdeNF1Click(Sender: TObject);
    procedure IncluiTributo1Click(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure AlteraLocaldeestoque1Click(Sender: TObject);
    procedure InformaIEC1Click(Sender: TObject);
    procedure DGDadosDblClick(Sender: TObject);
    procedure QrVSMOEnvAvuAfterScroll(DataSet: TDataSet);
    procedure QrVSMOEnvAvuBeforeClose(DataSet: TDataSet);
    procedure IncluiAtrelamento3Click(Sender: TObject);
    procedure AlteraAtrelamento3Click(Sender: TObject);
    procedure ExcluiAtrelamento3Click(Sender: TObject);
    procedure QrVSMOEnvEnvAfterScroll(DataSet: TDataSet);
    procedure QrVSMOEnvEnvBeforeClose(DataSet: TDataSet);
    procedure QrVSMOEnvRetAfterScroll(DataSet: TDataSet);
    procedure QrVSMOEnvRetBeforeClose(DataSet: TDataSet);
    procedure ExcluiAtrelamento1Click(Sender: TObject);
    procedure IncluiAtrelamento1Click(Sender: TObject);
    procedure AlteraAtrelamento1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSTrfLocIMEI(SQLType: TSQLType);
    procedure MostraFormVSOutPeso(SQLType: TSQLType);
    procedure MostraFormVSTrfLocPall(SQLType: TSQLType);
    procedure ReopenIMEISorc();
    //
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    //
    procedure AtualizaNFeItens();
    procedure ReopenVSTrfLocIts(Controle: Integer);
    procedure ReopenVSOutNFeCab(Codigo: Integer);
    procedure ReopenVSOutNFeIts(Controle: Integer);
    procedure ReopenTribIncIts(Controle: Integer);
    procedure CorrigeFornecedores();

  end;

var
  FmVSTrfLocCab: TFmVSTrfLocCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, Principal,
VSTrfLocIMEI, VSTrfLocPal(*, VSOutPeso*), ModVS_CRC,
{$IfDef sAllVS} UnTributos_PF, {$EndIf}
UnVS_CRC_PF, UnVS_PF;

{$R *.DFM}

const
  FSubTitImp: array [0..1] of String = (
    'Lotes enviados para opra��es e/ou processos',
    'Lotes enviados para demonstra��o e/ou venda'
  );
var
  FSubTitSel: Integer;

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSTrfLocCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSTrfLocCab.MostraFormVSTrfLocIMEI(SQLType: TSQLType);
var
  Codigo: Integer;
begin
  if SQLType <> stIns then
  begin
    Geral.MB_Erro('Forma de SQL n�o implementada!');
    Exit;
  end;
  Codigo   := QrVSTrfLocCabCodigo.Value;
  if DBCheck.CriaFm(TFmVSTrfLocIMEI, FmVSTrfLocIMEI, afmoNegarComAviso) then
  begin
    FmVSTrfLocIMEI.ImgTipo.SQLType := SQLType;
    FmVSTrfLocIMEI.FQrCab := QrVSTrfLocCab;
    FmVSTrfLocIMEI.FDsCab := DsVSTrfLocCab;
    FmVSTrfLocIMEI.FQrIts := QrIMEIDest;
    FmVSTrfLocIMEI.FDataHora := QrVSTrfLocCabDtVenda.Value;
    if SQLType = stIns then
    begin
      //FmVSTrfLocIMEI.EdCPF1.ReadOnly := False
(*
    end else
    begin
      FmVSTrfLocIMEI.FDataHora := QrVSTrfLocCabDtVenda.Value;
      FmVSTrfLocIMEI.EdControle.ValueVariant := QrIMEIDestControle.Value;
      //
      FmVSTrfLocIMEI.EdGragruX.ValueVariant  := QrIMEIDestGraGruX.Value;
      FmVSTrfLocIMEI.CBGragruX.KeyValue      := QrIMEIDestGraGruX.Value;
      FmVSTrfLocIMEI.EdPecas.ValueVariant    := -QrIMEIDestPecas.Value;
      FmVSTrfLocIMEI.EdPesoKg.ValueVariant   := -QrIMEIDestPesoKg.Value;
      FmVSTrfLocIMEI.EdAreaM2.ValueVariant   := -QrIMEIDestAreaM2.Value;
      FmVSTrfLocIMEI.EdAreaP2.ValueVariant   := -QrIMEIDestAreaP2.Value;
      //FmVSTrfLocIMEI.EdValorT.ValueVariant   := -QrIMEIDestValorT.Value;
      FmVSTrfLocIMEI.EdPallet.ValueVariant   := QrIMEIDestPallet.Value;
      //
    end;
*)
    end;
    FmVSTrfLocIMEI.ShowModal;
    FmVSTrfLocIMEI.Destroy;
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSTrfLocCab.MostraFormVSTrfLocPall(SQLType: TSQLType);
var
  Codigo: Integer;
begin
  if SQLType <> stIns then
  begin
    Geral.MB_Erro('Forma de SQL n�o implementada!');
    Exit;
  end;
  Codigo   := QrVSTrfLocCabCodigo.Value;
  if DBCheck.CriaFm(TFmVSTrfLocPal, FmVSTrfLocPal, afmoNegarComAviso) then
  begin
    FmVSTrfLocPal.ImgTipo.SQLType := SQLType;
    FmVSTrfLocPal.FQrCab := QrVSTrfLocCab;
    FmVSTrfLocPal.FDsCab := DsVSTrfLocCab;
    FmVSTrfLocPal.FQrIts := QrIMEIDest;
    FmVSTrfLocPal.FDataHora := QrVSTrfLocCabDtVenda.Value;
    FmVSTrfLocPal.FEmpresa  := QrVSTrfLocCabEmpresa.Value;
    FmVSTrfLocPal.FClientMO := 0; //QrVSTrfLocCabClientMO.Value;
    FmVSTrfLocPal.PesquisaPallets();
    //
    FmVSTrfLocPal.ShowModal;
    FmVSTrfLocPal.Destroy;
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSTrfLocCab.IncluiSerieNumeroDeNFe1Click(Sender: TObject);
const
  Codigo    = 0;
  ide_serie = 0;
  ide_nNF   = 0;
begin
  if VS_CRC_PF.MostraFormVSNFeOutCab(stIns, QrVSOutNfeCab, QrVSTrfLocCabMovimCod.Value,
  QrVSTrfLocCabCodigo.Value, Codigo, ide_serie, ide_nNF) then
    VS_CRC_PF.MostraFormVSNFeOutIts(stIns, QrVSOutNfeIts, QrTribIncIts,
      QrVSOutNfeCabCodigo.Value, QrVSOutNfeCabMovimCod.Value,
      QrVSTrfLocCabEmpresa.Value, QrVSTrfLocCabDtVenda.Value,
      siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSTrfLocCabMovimCod.Value, TEstqMovimID.emidTransfLoc, [eminSorcLocal],
  [eminDestLocal]);
end;

procedure TFmVSTrfLocCab.IncluiTributo1Click(Sender: TObject);
const
  Controle = 0;
  Operacao = 0;
  Tributo  = 0;
  ValorFat = 0.00;
  BaseCalc = 0.00;
  Percent  = 0.00;
  VUsoCalc = 0.00;
  ValrTrib = 0.00;
var
  FatNum, FatParcela, Empresa: Integer;
  Data, Hora: TDateTime;
begin
{$IfDef sAllVS}
  FatNum     := QrVSOutNFeCabCodigo.Value;
  FatParcela := QrVSOutNFeItsItemNFe.Value;
  Empresa    := QrVSTrfLocCabEmpresa.Value;
  Data       := QrVSTrfLocCabDtVenda.Value;
  Hora       := QrVSTrfLocCabDtVenda.Value;
  //
  Tributos_PF.MostraFormTribIncIts(stIns, QrTribIncIts, VAR_FATID_1011,
  FatNum, FatParcela, Empresa, Controle, Data, Hora, Operacao, Tributo,
  ValorFat, BaseCalc, Percent, VUsoCalc, ValrTrib, siNegativo);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}end;

procedure TFmVSTrfLocCab.MostraFormVSOutPeso(SQLType: TSQLType);
var
  Codigo: Integer;
begin
{
  if SQLType <> stIns then
  begin
    Geral.MB_Erro('Forma de SQL n�o implementada!');
    Exit;
  end;
  Codigo   := QrVSTrfLocCabCodigo.Value;
  if DBCheck.CriaFm(TFmVSOutPeso, FmVSOutPeso, afmoNegarComAviso) then
  begin
    FmVSOutPeso.ImgTipo.SQLType := SQLType;
    FmVSOutPeso.FQrCab := QrVSTrfLocCab;
    FmVSOutPeso.FDsCab := DsVSTrfLocCab;
    FmVSOutPeso.FQrIts := QrIMEIDest;
    FmVSOutPeso.FDataHora := QrVSTrfLocCabDtVenda.Value;
    if SQLType = stIns then
    begin
      //FmVSOutPeso.EdCPF1.ReadOnly := False
(*
    end else
    begin
      FmVSOutPeso.FDataHora := QrVSTrfLocCabDtVenda.Value;
      FmVSOutPeso.EdControle.ValueVariant := QrIMEIDestControle.Value;
      //
      FmVSOutPeso.EdGragruX.ValueVariant  := QrIMEIDestGraGruX.Value;
      FmVSOutPeso.CBGragruX.KeyValue      := QrIMEIDestGraGruX.Value;
      FmVSOutPeso.EdPecas.ValueVariant    := -QrIMEIDestPecas.Value;
      FmVSOutPeso.EdPesoKg.ValueVariant   := -QrIMEIDestPesoKg.Value;
      FmVSOutPeso.EdAreaM2.ValueVariant   := -QrIMEIDestAreaM2.Value;
      FmVSOutPeso.EdAreaP2.ValueVariant   := -QrIMEIDestAreaP2.Value;
      //FmVSOutPeso.EdValorT.ValueVariant   := -QrIMEIDestValorT.Value;
      FmVSOutPeso.EdPallet.ValueVariant   := QrIMEIDestPallet.Value;
      //
    end;
*)
    end;
    FmVSOutPeso.ShowModal;
    FmVSOutPeso.Destroy;
    //
    LocCod(Codigo, Codigo);
  end;
}
end;

procedure TFmVSTrfLocCab.PackingList1Click(Sender: TObject);
var
  Linhas: String;
  QtdLin: Integer;
  MD003: TfrxMasterData;
begin
  FSubTitSel := MyObjects.SelRadioGroup('Tranfer�ncia de Estoque',
  'Selecione a finalidade', FSubTitImp, 1);
  if not (FSubTitSel in ([0,1])) then
    Exit;
  Linhas := '27';
  if InputQuery('Linhas de Baixa de Estoque', 'Informe a quantidade de linhas',
  Linhas) then
    QtdLin := Geral.IMV(Linhas)
  else
    QtdLin := 0;
  //
  MD003 := frxWET_CURTI_019_00_A.FindObject('MD003') as TfrxMasterData;
  MD003.RowCount := QtdLin;
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_019_00_A, [
  DModG.frxDsDono,
  frxDsPallets,
  frxDsVSOutNFeCab,
  frxDsVSOutNFeIts
  ]);
  MyObjects.frxMostra(frxWET_CURTI_019_00_A, 'Packing List');
end;

procedure TFmVSTrfLocCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSTrfLocCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrVSTrfLocCab, QrIMEIDest);
end;

procedure TFmVSTrfLocCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSTrfLocCab);
  //MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrIMEIDest);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiIMEI1, QrIMEIDest);
  ItsExcluiIMEI1.Enabled :=
    ItsExcluiIMEI1.Enabled and (PCItens.ActivePageIndex = 1);
  InformaNmerodaRME1.Enabled := ItsExcluiIMEI1.Enabled;
  InformaItemdeNFe1.Enabled := ItsExcluiIMEI1.Enabled;
  AlteraLocaldeestoque1.Enabled := ItsExcluiIMEI1.Enabled;
  //
  VS_CRC_PF.HabilitaComposVSAtivo(QrIMEIDestID_TTW.Value, [(*ItsAltera1,*) ItsExcluiIMEI1]);
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento3, QrIMEIDest);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento3, QrVSMOEnvAvu);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento3, QrVSMOEnvAvu);
  AtrelamentoFreteCompra1.Enabled := PCItens.ActivePage = TsFrCompr;
  //
  AtrelamentoNFsdeMO1.Enabled := PCItens.ActivePage = TsEnvioMO;
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento1, QrIMEIDest);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento1, QrVSMOEnvEnv);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento1, QrVSMOEnvEnv);
  //
end;

procedure TFmVSTrfLocCab.PMVSOutNFeCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiSerieNumeroDeNFe1, QrVSTrfLocCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraSerieNumeroDeNFe1, QrVSOutNFeCab);
  MyObjects.HabilitaMenuItemCabDel(ExcluiSerieNumeroDeNFe1, QrVSOutNFeCab, QrVSOutNFeIts);
end;

procedure TFmVSTrfLocCab.PMVSOutNFeItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiitemdeNF1, QrVSOutNFeCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraitemdeNF1, QrVSOutNFeIts);
  MyObjects.HabilitaMenuItemCabDel(ExcluiitemdeNF1, QrVSOutNFeIts, QrTribIncIts);
end;

procedure TFmVSTrfLocCab.PorIMEI1Click(Sender: TObject);
begin
  MostraFormVSTrfLocIMEI(stIns);
end;

procedure TFmVSTrfLocCab.Porkgsubproduto1Click(Sender: TObject);
begin
{
  MostraFormVSOutPeso(stIns);
}
end;

procedure TFmVSTrfLocCab.PorPallet1Click(Sender: TObject);
begin
  MostraFormVSTrfLocPall(stIns);
end;

procedure TFmVSTrfLocCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSTrfLocCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSTrfLocCab.DefParams;
begin
  VAR_GOTOTABELA := 'vstrfloccab';
  VAR_GOTOMYSQLTABLE := QrVSTrfLocCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_CLIENTE,');
  VAR_SQLx.Add('IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA');
  VAR_SQLx.Add('FROM vstrfloccab wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=wic.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades trn ON trn.Codigo=wic.transporta');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSTrfLocCab.DGDadosDblClick(Sender: TObject);
begin
  if VS_CRC_PF.EditaIxx(TDBGrid(DGDados), QrIMEIDest) then
    PCItens.ActivePageIndex := 1;
end;

procedure TFmVSTrfLocCab.EdClienteChange(Sender: TObject);
begin
  if EdCliente.VAlueVariant = 0 then
    LaCliente.Caption := '...'
  else
    LaCliente.Caption := Geral.FormataCNPJ_TT(QrClienteCNPJ_CPF.Value);
end;

procedure TFmVSTrfLocCab.ExcluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  //PCItens.ActivePageIndex := 2;
  PCItens.ActivePage := TsEnvioMO;
  //
  DmModVS_CRC.ExcluiAtrelamentoMOEnvEnv(QrVSMOEnvEnv, QrVSMOEnvEVMI);
  //
  VS_CRC_PF.AtualizaTotaisVSOpeCab(QrVSTrfLocCabMovimCod.Value);
  //
  LocCod(QrVSTrfLocCabCodigo.Value, QrVSTrfLocCabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSTrfLocCab.ExcluiAtrelamento3Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsFrCompr;
  //
  DmModVS_CRC.ExcluiAtrelamentoMOEnvAvu(QrVSMOEnvAvu, QrVSMOEnvAVMI);
  //
  LocCod(QrVSTrfLocCabCodigo.Value,QrVSTrfLocCabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSTrfLocCab.ExcluiitemdeNF1Click(Sender: TObject);
var
  Controle: Integer;
begin
{$IfDef sAllVS}
  if VS_CRC_PF.ExcluiVSNaoVMI('Confirma a exclus�o do item selecionado?',
  'vsoutnfeIts', 'Controle', QrVSOutNFeItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrVSOutNFeIts,
      QrVSOutNFeItsControle, QrVSOutNFeItsControle.Value);
    ReopenVSOutNFeIts(Controle);
  end;
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSTrfLocCab.ExcluiSerieNumeroDeNFe1Click(Sender: TObject);
var
  Codigo: Integer;
begin
{$IfDef sAllVS}
  if VS_CRC_PF.ExcluiVSNaoVMI('Confirma a exclus�o do item selecionado?',
  'vsoutnfecab', 'Codigo', QrVSOutNFeCabCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSOutNFeCab,
      QrVSOutNFeCabCodigo, QrVSOutNFeCabCodigo.Value);
    ReopenVSOutNFeCab(Codigo);
  end;
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSTrfLocCab.ExcluiTributoAtual1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrTribIncItsControle.Value;
  if VS_CRC_PF.ExcluiVSNaoVMI('Confirma a exclus�o deste item de tributo?',
  'tribincits', 'Controle', Controle, DMod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrTribIncIts,
      QrTribIncItsControle, QrTribIncItsControle.Value);
    ReopenTribIncIts(Controle);
  end;
end;

procedure TFmVSTrfLocCab.IncluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  //PCItens.ActivePageIndex := 2;
  PCItens.ActivePage := TsEnvioMO;
  //
  VS_PF.MostraFormVSMOEnvEnv(stIns, QrIMEIDest, QrVSMOEnvEnv, QrVSMOEnvEVMI,
  siNegativo, QrVSTrfLocCabide_serie.Value, QrVSTrfLocCabide_nNF.Value);
  //
  LocCod(QrVSTrfLocCabCodigo.Value,QrVSTrfLocCabCodigo.Value);
  //
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSTrfLocCab.IncluiAtrelamento3Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsFrCompr;
  //
  VS_PF.MostraFormVSMOEnvAvu(stIns, QrIMEIDest, QrVSMOEnvAvu, QrVSMOEnvAVMI,
  siPositivo,
  QrVSTrfLocCabCliente.Value, QrVSTrfLocCabide_serie.Value, QrVSTrfLocCabide_nNF.Value);

  //
  VS_CRC_PF.ReopenVSMOEnvAvu(QrVSMOEnvAvu, QrVSTrfLocCabMovimCod.Value, 0);
  //
  LocCod(QrVSTrfLocCabCodigo.Value,QrVSTrfLocCabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSTrfLocCab.IncluiitemdeNF1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSNFeOutIts(stIns, QrVSOutNfeIts, QrTribIncIts,
    QrVSOutNfeCabCodigo.Value, QrVSOutNfeCabMovimCod.Value,
      QrVSTrfLocCabEmpresa.Value, QrVSTrfLocCabDtVenda.Value,
      siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSTrfLocCabMovimCod.Value, TEstqMovimID.emidTransfLoc, [eminSorcLocal],
  [eminDestLocal]);
end;

procedure TFmVSTrfLocCab.InformaIEC1Click(Sender: TObject);
begin
  if VS_CRC_PF.InformaIxx(DGDados, QrIMEIDest, 'Controle') then
    PCItens.ActivePageIndex := 1;
end;

procedure TFmVSTrfLocCab.InformaNmerodaRME1Click(Sender: TObject);
begin
  VS_CRC_PF.InfoReqMovEstq(QrIMEIDestControle.Value,
    QrIMEIDestReqMovEstq.Value, QrIMEIDest);
  ReopenVSTrfLocIts(QrIMEIDestControle.Value);
  PCItens.ActivePageIndex := 1;
end;

procedure TFmVSTrfLocCab.CabExclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if MyObjects.FIC((QrPallets.RecordCount > 0) or (QrIMEIDest.RecordCount > 0)
  or (QrVSOutNFeCab.RecordCount > 0), nil,
  'Existem lan�anmentos em tabelas filhas que impedem a exclus�o do cabe�alho!')
  then Exit;
  //
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do cabe�alho selecionado?',
  'vstrfloccab', 'Codigo', QrVSTrfLocCabCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrVSTrfLocCab,
      QrVSTrfLocCabCodigo, QrVSTrfLocCabCodigo.Value);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSTrfLocCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast); Desabilita botao quando fecha mes!
end;

procedure TFmVSTrfLocCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSTrfLocCab.ItsExcluiIMEI1Click(Sender: TObject);
const
  CtrlBaix = 0;
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrIMEIDestCodigo.Value;
  MovimCod := QrIMEIDestMovimCod.Value;
  //
  if VS_CRC_PF.ExcluiControleVSMovIts(QrIMEIDest, TIntegerField(QrIMEIDestControle),
  QrIMEIDestControle.Value, QrIMEISorcControle.Value, QrIMEISorcSrcNivel2.Value,
  False, Integer(TEstqMotivDel.emtdWetCurti131)) then
  begin
    VS_CRC_PF.AtualizaTotaisVSXxxCab('vstrfloccab', MovimCod);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSTrfLocCab.ReopenIMEISorc();
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSTrfLocCabTemIMEIMrt.Value;
  //
  // Por IME-I
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  VS_CRC_PF.SQL_NO_FRN(),
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  VS_CRC_PF.SQL_LJ_FRN(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSTrfLocCabMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminSorcLocal)),
  'AND vmi.MovimTwn=' + Geral.FF0(QrIMEIDestMovimTwn.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrIMEISorc, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //Geral.MB_SQL(Self, QrIMEISorc);
end;

procedure TFmVSTrfLocCab.ReopenTribIncIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTribIncIts, Dmod.MyDB, [
  'SELECT tii.*, tdd.Nome NO_Tributo  ',
  'FROM tribincits tii ',
  'LEFT JOIN tribdefcad tdd ON tdd.Codigo=tii.Tributo',
  'WHERE tii.FatID=' + Geral.FF0(VAR_FATID_1011),
  'AND tii.FatNum=' + Geral.FF0(QrVSOutNFeCabCodigo.Value),
  'AND tii.FatParcela=' + Geral.FF0(QrVSOutNFeItsItemNFe.Value),
  'ORDER BY tii.Controle DESC ',
  '']);
end;

procedure TFmVSTrfLocCab.ReopenVSTrfLocIts(Controle: Integer);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSTrfLocCabTemIMEIMrt.Value;
  //
  // Por IME-I
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  VS_CRC_PF.SQL_NO_FRN(),
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  VS_CRC_PF.SQL_LJ_FRN(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSTrfLocCabMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestLocal)),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrIMEIDest, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //Geral.MB_SQL(Self, QrIMEIDest);
  //
  QrIMEIDest.Locate('Controle', Controle, []);
  // Por Pallet
  //
  SQL_Flds := Geral.ATS([
  ', IF(cou.ArtigoImp = "",  ',
  '  CONCAT(gg1.Nome,  ',
  '  IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  '  IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))), cou.ArtigoImp) TXT_ARTIGO, ',
  'cou.ArtigoImp, cou.ClasseImp,  ',
  'IF(SUM(vmi.Pecas) = 0, 0, SUM(vmi.AreaM2) / SUM(vmi.Pecas)) MediaM2,  ',
  'CASE  ',
  'WHEN SUM(vmi.AreaM2) / SUM(vmi.Pecas) < cou.MediaMinM2 THEN 0.000  ',
  'WHEN SUM(vmi.AreaM2) / SUM(vmi.Pecas) > cou.MediaMaxM2 THEN 2.000  ',
  'ELSE 1.000 END FaixaMediaM2  ',
  '']);
  SQL_Left := Geral.ATS([
  'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle  ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSTrfLocCabMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestLocal)),
  '']);
  SQL_Group := 'GROUP BY Pallet ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrPallets, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet ',
  '']);
end;

procedure TFmVSTrfLocCab.ReopenVSOutNFeCab(Codigo: Integer);
begin
{$IfDef sAllVS}
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSOutNFeCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsoutnfecab ',
  'WHERE MovimCod=' + Geral.FF0(QrVSTrfLocCabMovimCod.Value),
  '']);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSTrfLocCab.ReopenVSOutNFeIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSOutNFeIts, Dmod.MyDB, [
  'SELECT CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, voi.* ',
  'FROM vsoutnfeits voi',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=voi.GraGruX',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed',
  'WHERE voi.Codigo=' + Geral.FF0(QrVSOutNFeCabCodigo.Value),
  '']);
end;

procedure TFmVSTrfLocCab.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  CorTexto, CorFundo: TColor;
  Texto: String;
begin
  if Column.FieldName = 'FaixaMediaM2' then
  begin
    VS_CRC_PF.DadosDaFaixaDaMediaM2DoCouro(QrPalletsFaixaMediaM2.Value,
    CorFundo, CorTexto, Texto);
    MyObjects.DesenhaTextoEmDBGrid(DBGrid1, Rect, CorTexto, CorFundo,
      Column.Alignment, Texto(*Column.Field.DisplayText*));
  end;
end;

procedure TFmVSTrfLocCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSTrfLocCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSTrfLocCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSTrfLocCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSTrfLocCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSTrfLocCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSTrfLocCab.BtTribIncItsClick(Sender: TObject);
begin
  PCItens.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMTribIncIts, BtTribIncIts);
end;

procedure TFmVSTrfLocCab.BtVSOutNFeCabClick(Sender: TObject);
begin
  PCItens.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMVSOutNFeCab, BtVSOutNFeCab);
end;

procedure TFmVSTrfLocCab.BtVSOutNFeItsClick(Sender: TObject);
begin
  PCItens.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMVSOutNFeIts, BtVSOutNFeIts);
end;

procedure TFmVSTrfLocCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSTrfLocCabCodigo.Value;
  Close;
end;

procedure TFmVSTrfLocCab.CabAltera1Click(Sender: TObject);
var
  Empresa: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSTrfLocCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vstrfloccab');
  Empresa := DModG.ObtemFilialDeEntidade(QrVSTrfLocCabEmpresa.Value);
  EdEmpresa.ValueVariant := Empresa;
  CBEmpresa.KeyValue     := Empresa;
end;

procedure TFmVSTrfLocCab.BtConfirmaClick(Sender: TObject);
var
  DtVenda, DtViagem, DtEntrega, DataHora: String;
  //Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  DataChegadaInvalida: Boolean;
  SQLType: TSQLType;
  Codigo, MovimCod, Empresa, Cliente, Transporta, CliVenda, ide_serie, ide_nNF,
  Forma: Integer;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DtVenda        := Geral.FDT_TP_Ed(TPDtVenda.Date, EdDtVenda.Text);
  DtViagem       := Geral.FDT_TP_Ed(TPDtViagem.Date, EdDtViagem.Text);
  DtEntrega      := Geral.FDT_TP_Ed(TPDtEntrega.Date, EdDtEntrega.Text);
  Cliente        := EdCliente.ValueVariant;
  Transporta     := EdTransporta.ValueVariant;
  ide_serie      := Edide_serie.ValueVariant;
  ide_nNF        := Edide_nNF.ValueVariant;
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  if not VS_CRC_PF.ValidaCampoNF(1, Edide_nNF, True) then Exit;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(TPDtVenda.DateTime < 2, TPDtVenda, 'Defina uma data de Venda!') then Exit;
  //DataChegadaInvalida := TPDtEntrega.Date < TPDtViagem.Date;
  //if MyObjects.FIC(DataChegadaInvalida, TPDtViagem, 'Defina uma data de chegada v�lida!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vstrfloccab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vstrfloccab', False, [
  'MovimCod', 'Empresa', 'DtVenda',
  'DtViagem', 'DtEntrega', 'Cliente',
  'Transporta'(*, 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT'*),
  'ide_serie', 'ide_nNF'], [
  'Codigo'], [
  MovimCod, Empresa, DtVenda,
  DtViagem, DtEntrega, Cliente,
  Transporta(* (*Pecas, PesoKg,
  AreaM2, AreaP2, ValorT*),
  ide_serie, ide_nNF], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      VS_CRC_PF.InsereVSMovCab(MovimCod, emidTransfLoc, Codigo)
    else
    begin
      DataHora := DtVenda;
      CliVenda := Cliente;
      //
      if Edide_nNF.ValueVariant <> 0 then
        AtualizaNFeItens();
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', 'CliVenda', CO_DATA_HORA_VMI], ['MovimCod'], [
      Empresa, CliVenda, DataHora], [MovimCod], True);
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if (SQLType = stIns) or (QrIMEIDest.RecordCount = 0) then
    begin
      //MostraVSGArtOriPall(stIns);
      Forma := MyObjects.SelRadioGroup('Forma de sele��o',
      'Escolha a forma de sele��o', ['Pallet', 'IME-I', 'Peso kg (sub produto)'], -1);
      case Forma of
        0: MostraFormVSTrfLocPall(stIns);
        1: MostraFormVSTrfLocIMEI(stIns);
        2: MostraFormVSOutPeso(stIns);
      end;
    end;
  end;
end;

procedure TFmVSTrfLocCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vstrfloccab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vstrfloccab', 'Codigo');
end;

procedure TFmVSTrfLocCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSTrfLocCab.AlteraAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  //PCItens.ActivePageIndex := 2;
  PCItens.ActivePage := TsEnvioMO;
  //
  VS_PF.MostraFormVSMOEnvEnv(stUpd, QrIMEIDest, QrVSMOEnvEnv, QrVSMOEnvEVMI,
  siNegativo, 0, 0);
  //
  LocCod(QrVSTrfLocCabCodigo.Value,QrVSTrfLocCabCodigo.Value);
  //
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSTrfLocCab.AlteraAtrelamento3Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCItens.ActivePage := TsFrCompr;
  //
  VS_PF.MostraFormVSMOEnvAvu(stUpd, QrIMEIDest, QrVSMOEnvAvu, QrVSMOEnvAVMI,
  siPositivo,
  QrVSTrfLocCabCliente.Value, QrVSTrfLocCabide_serie.Value, QrVSTrfLocCabide_nNF.Value);
  //
  VS_CRC_PF.ReopenVSMOEnvAvu(QrVSMOEnvAvu, QrVSTrfLocCabMovimCod.Value, 0);
  //
  LocCod(QrVSTrfLocCabCodigo.Value,QrVSTrfLocCabCodigo.Value);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSTrfLocCab.AlteraitemdeNF1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSNFeOutIts(stUpd, QrVSOutNfeIts, QrTribIncIts,
    QrVSOutNfeCabCodigo.Value, QrVSOutNfeCabMovimCod.Value,
      QrVSTrfLocCabEmpresa.Value, QrVSTrfLocCabDtVenda.Value,
      siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSTrfLocCabMovimCod.Value, TEstqMovimID.emidTransfLoc, [eminSorcLocal],
  [eminDestLocal]);
end;

procedure TFmVSTrfLocCab.AlteraLocaldeestoque1Click(Sender: TObject);
begin
  VS_CRC_PF.InfoStqCenLoc(QrIMEIDestControle.Value,
    QrIMEIDestStqCenLoc.Value, QrIMEIDest);
end;

procedure TFmVSTrfLocCab.AlteraSerieNumeroDeNFe1Click(Sender: TObject);
var
  Codigo, ide_serie, ide_nNF: Integer;
begin
  Codigo    := QrVSOutNFeCabCodigo.Value;
  ide_serie := QrVSOutNFeCabide_serie.Value;
  ide_nNF   := QrVSOutNFeCabide_nNF.Value;
  //
  VS_CRC_PF.MostraFormVSNFeOutCab(stUpd, QrVSOutNfeCab, QrVSTrfLocCabMovimCod.Value,
  QrVSTrfLocCabCodigo.Value, Codigo, ide_serie, ide_nNF);
end;

procedure TFmVSTrfLocCab.AlteraTributoAtual1Click(Sender: TObject);
var
  Operacao, Tributo, FatID, FatNum, FatParcela, Empresa, Controle: Integer;
  Data, Hora: TDateTime;
  ValorFat, BaseCalc, Percent, VUsoCalc, ValrTrib: Double;
begin
{$IfDef sAllVS}
  FatNum     := QrTribIncItsFatNum.Value;
  FatParcela := QrTribIncItsFatParcela.Value;
  Empresa    := QrTribIncItsEmpresa.Value;
  Data       := QrTribIncItsData.Value;
  Hora       := QrTribIncItsHora.Value;
  //
  Controle   := QrTribIncItsControle.Value;
  Operacao   := QrTribIncItsOperacao.Value;
  Tributo    := QrTribIncItsTributo.Value;
  ValorFat   := QrTribIncItsValorFat.Value;
  BaseCalc   := QrTribIncItsBaseCalc.Value;
  Percent    := QrTribIncItsPercent.Value;
  VUsoCalc   := QrTribIncItsVUsoCalc.Value;
  ValrTrib   := QrTribIncItsValrTrib.Value;
  Data       := QrTribIncItsData.Value;
  Hora       := QrTribIncItsHora.Value;
  //
  Tributos_PF.MostraFormTribIncIts(stUpd, QrTribIncIts, VAR_FATID_1011,
  FatNum, FatParcela, Empresa, Controle, Data, Hora, Operacao, Tributo,
  ValorFat, BaseCalc, Percent, VUsoCalc, ValrTrib, siNegativo);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}end;

procedure TFmVSTrfLocCab.AtualizaNFeItens();
begin
  DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSTrfLocCabMovimCod.Value, TEstqMovimID.emidTransfLoc, [eminSorcLocal],
  [eminDestLocal]);
end;

procedure TFmVSTrfLocCab.BtCabClick(Sender: TObject);
begin
  VS_CRC_PF.HabilitaMenuInsOuAllVSAberto(QrVSTrfLocCab, QrVSTrfLocCabDtVenda.Value,
  BtCab, PMCab, [CabInclui1, CorrigeFornecedores1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSTrfLocCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTransporta, Dmod.MyDB);
  // deve ser depois dos open qry
  MyObjects.DefinePageControlActivePageByName(PCItens, VAR_PC_FRETE_VS_NAME, 0);
end;

procedure TFmVSTrfLocCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSTrfLocCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSTrfLocCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSTrfLocCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSTrfLocCab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSTrfLocCabCodigo.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSTrfLocCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSTrfLocCab.QrPalletsCalcFields(DataSet: TDataSet);
begin
  QrPalletsSEQ.Value := QrPallets.RecNo;
end;

procedure TFmVSTrfLocCab.QrVSTrfLocCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSTrfLocCab.QrVSTrfLocCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSTrfLocIts(0);
  ReopenVSOutNFeCab(0);
  VS_CRC_PF.ReopenVSMOEnvAvu(QrVSMOEnvAvu, QrVSTrfLocCabMovimCod.Value, 0);
  VS_CRC_PF.ReopenVSMOEnvEnv(QrVSMOEnvEnv, QrVSTrfLocCabMovimCod.Value, 0);
  VS_CRC_PF.ReopenVSMOEnvRet(QrVSMOEnvRet, QrVSTrfLocCabMovimCod.Value, 0);
end;

procedure TFmVSTrfLocCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSTrfLocCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSTrfLocCab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := VS_CRC_PF.MostraFormVSRMPPsq(emidTransfLoc, [], stPsq, Codigo,
  Controle, False);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    ReopenVSTrfLocIts(Controle);
  end;
end;

procedure TFmVSTrfLocCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSTrfLocCab.frxWET_CURTI_019_00_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := QrVSTrfLocCabNO_EMPRESA.Value
  else
  if VarName = 'VARF_CLIENTE' then
    Value := QrVSTrfLocCabNO_CLIENTE.Value
  else
  if VarName = 'VARF_CODIGO' then
    Value := QrVSTrfLocCabCodigo.Value
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_SUB_TIT' then
    Value := FSubTitImp[FSubTitSel]
  else
end;

procedure TFmVSTrfLocCab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSTrfLocCab, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vstrfloccab');
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPDtVenda.Date := Agora;
  EdDtVenda.ValueVariant := Agora;
  if EdEmpresa.ValueVariant > 0 then
    TPDtVenda.SetFocus;
end;

procedure TFmVSTrfLocCab.CorrigeFornecedores();
begin
{
  QrIMEIDest.First;
  while not QrIMEIDest.Eof do
  begin
    VS_CRC_PF.AtualizaFornecedorOut(QrIMEIDestControle.Value);
    //
    QrIMEIDest.Next;
  end;
  LocCod(QrVSTrfLocCabCodigo.Value, QrVSTrfLocCabCodigo.Value);
}
end;

procedure TFmVSTrfLocCab.CorrigeFornecedores1Click(Sender: TObject);
begin
  CorrigeFornecedores();
end;

procedure TFmVSTrfLocCab.QrVSTrfLocCabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSMOEnvEnv.Close;
  QrVSMOEnvRet.Close;
  QrVSMOEnvAvu.Close;
  QrIMEIDest.Close;
  QrVSOutNFeCab.Close;
end;

procedure TFmVSTrfLocCab.QrVSTrfLocCabBeforeOpen(DataSet: TDataSet);
begin
  QrVSTrfLocCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSTrfLocCab.QrIMEIDestAfterOpen(DataSet: TDataSet);
begin
  if (QrIMEIDest.RecordCount > 0) and (PCItens.ActivePageIndex < 2) then
  begin
    if QrIMEIDestPallet.Value > 0 then
      PCItens.ActivePageIndex := 0
    else
      PCItens.ActivePageIndex := 1;
  end;
end;

procedure TFmVSTrfLocCab.QrIMEIDestAfterScroll(DataSet: TDataSet);
begin
  ReopenIMEISorc();
end;

procedure TFmVSTrfLocCab.QrIMEIDestBeforeClose(DataSet: TDataSet);
begin
  QrIMEISorc.Close;
end;

procedure TFmVSTrfLocCab.QrVSMOEnvAvuAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvAVMI(QrVSMOEnvAVmi, QrVSMOEnvAvuCodigo.VAlue, 0);
end;

procedure TFmVSTrfLocCab.QrVSMOEnvAvuBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvAVMI.Close;
end;

procedure TFmVSTrfLocCab.QrVSMOEnvEnvAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvEVmi(QrVSMOEnvEVmi, QrVSMOEnvEnvCodigo.Value, 0);
end;

procedure TFmVSTrfLocCab.QrVSMOEnvEnvBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvEVMI.Close;
end;

procedure TFmVSTrfLocCab.QrVSMOEnvRetAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvRVmi(QrVSMOEnvRVmi, QrVSMOEnvRetCodigo.Value, 0);
  VS_CRC_PF.ReopenVSMOEnvGVmi(QrVSMOEnvGVmi, QrVSMOEnvRetCodigo.Value, 0);
end;

procedure TFmVSTrfLocCab.QrVSMOEnvRetBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvGVmi.Close;
  QrVSMOEnvRVmi.Close;
end;

procedure TFmVSTrfLocCab.QrVSOutNFeCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSOutNFeIts(0);
end;

procedure TFmVSTrfLocCab.QrVSOutNFeCabBeforeClose(DataSet: TDataSet);
begin
  QrVSOutNFeIts.Close;
end;

procedure TFmVSTrfLocCab.QrVSOutNFeItsAfterScroll(DataSet: TDataSet);
begin
  ReopenTribIncIts(0);
end;

procedure TFmVSTrfLocCab.QrVSOutNFeItsBeforeClose(DataSet: TDataSet);
begin
  QrTribIncIts.Close;
end;

{
object IncluiAtrelamento1: TMenuItem
  Caption = '&Inclui Atrelamento'
  OnClick = IncluiAtrelamento1Click
end
object AlteraAtrelamento1: TMenuItem
  Caption = '&Altera Atrelamento'
  OnClick = AlteraAtrelamento1Click
end
object ExcluiAtrelamento1: TMenuItem
  Caption = '&Exclui Atrelamento'
  OnClick = ExcluiAtrelamento1Click
end
}
end.

