object FmVSRtbCab: TFmVSRtbCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-106 :: Entrada de Artigo Por Devolu'#231#227'o'
  ClientHeight = 661
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 245
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label52: TLabel
        Left = 476
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Data / hora compra:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label53: TLabel
        Left = 628
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 780
        Top = 16
        Width = 103
        Height = 13
        Caption = 'Data / hora chegada:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 504
        Top = 56
        Width = 201
        Height = 13
        Caption = 'Fornecedor (Cliente que est'#225' devolvendo):'
      end
      object Label5: TLabel
        Left = 504
        Top = 96
        Width = 69
        Height = 13
        Caption = 'Transportador:'
      end
      object LaPecas: TLabel
        Left = 644
        Top = 136
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object LaAreaM2: TLabel
        Left = 732
        Top = 136
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        Enabled = False
      end
      object LaAreaP2: TLabel
        Left = 820
        Top = 136
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Enabled = False
      end
      object LaPeso: TLabel
        Left = 908
        Top = 136
        Width = 27
        Height = 13
        Caption = 'Peso:'
        Enabled = False
      end
      object Label6: TLabel
        Left = 932
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object SbFornecedor: TSpeedButton
        Left = 868
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbFornecedorClick
      end
      object SbTransportador: TSpeedButton
        Left = 968
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbTransportadorClick
      end
      object Label20: TLabel
        Left = 16
        Top = 56
        Width = 70
        Height = 13
        Caption = 'Cliente de MO:'
      end
      object SbClienteMO: TSpeedButton
        Left = 380
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbClienteMOClick
      end
      object Label21: TLabel
        Left = 16
        Top = 96
        Width = 229
        Height = 13
        Caption = 'Proced'#234'ncia: (Tipo de entidade: Fornece outros)'
        Enabled = False
      end
      object SbProcedenc: TSpeedButton
        Left = 480
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        Enabled = False
        OnClick = SbProcedencClick
      end
      object Label22: TLabel
        Left = 16
        Top = 136
        Width = 212
        Height = 13
        Caption = 'Motorista: (Tipo de entidade: Fornece outros)'
      end
      object SbMotorista: TSpeedButton
        Left = 480
        Top = 152
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbMotoristaClick
      end
      object Label23: TLabel
        Left = 504
        Top = 136
        Width = 30
        Height = 13
        Caption = 'Placa:'
      end
      object Label28: TLabel
        Left = 404
        Top = 56
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label29: TLabel
        Left = 436
        Top = 56
        Width = 63
        Height = 13
        Caption = 'N'#250'mero NFe:'
      end
      object Label31: TLabel
        Left = 892
        Top = 56
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label32: TLabel
        Left = 924
        Top = 56
        Width = 63
        Height = 13
        Caption = 'N'#250'mero NFe:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDtCompra: TdmkEditDateTimePicker
        Left = 476
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtCompra'
        UpdCampo = 'DtCompra'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtCompra: TdmkEdit
        Left = 584
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtCompra'
        UpdCampo = 'DtCompra'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtViagem: TdmkEditDateTimePicker
        Left = 628
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtViagem'
        UpdCampo = 'DtViagem'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtViagem: TdmkEdit
        Left = 736
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 6
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtViagem'
        UpdCampo = 'DtViagem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtEntrada: TdmkEditDateTimePicker
        Left = 780
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805900
        Time = 0.639644131944805900
        TabOrder = 7
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtEntrada'
        UpdCampo = 'DtEntrada'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtEntrada: TdmkEdit
        Left = 888
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 8
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtEntrada'
        UpdCampo = 'DtEntrada'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdFornecedor: TdmkEditCB
        Left = 504
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Fornecedor'
        UpdCampo = 'Fornecedor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornecedor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornecedor: TdmkDBLookupComboBox
        Left = 560
        Top = 72
        Width = 305
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsFornecedor
        TabOrder = 15
        dmkEditCB = EdFornecedor
        QryCampo = 'Fornecedor'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdTransporta: TdmkEditCB
        Left = 504
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 20
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Transporta'
        UpdCampo = 'Transporta'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTransporta
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTransporta: TdmkDBLookupComboBox
        Left = 560
        Top = 112
        Width = 408
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsTransporta
        TabOrder = 21
        dmkEditCB = EdTransporta
        QryCampo = 'Transporta'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdPecas: TdmkEdit
        Left = 644
        Top = 152
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 25
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdAreaM2: TdmkEditCalc
        Left = 732
        Top = 152
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 26
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaM2'
        UpdCampo = 'AreaM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaP2
        CalcType = ctM2toP2
        CalcFrac = cfQuarto
      end
      object EdAreaP2: TdmkEditCalc
        Left = 820
        Top = 152
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 27
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaP2'
        UpdCampo = 'AreaP2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaM2
        CalcType = ctP2toM2
        CalcFrac = cfCento
      end
      object EdPesoKg: TdmkEdit
        Left = 908
        Top = 152
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 28
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PesoKg'
        UpdCampo = 'PesoKg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 932
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 337
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdClienteMO: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ClienteMO'
        UpdCampo = 'ClienteMO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClienteMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClienteMO: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 305
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClienteMO
        TabOrder = 11
        dmkEditCB = EdClienteMO
        QryCampo = 'ClienteMO'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdProcednc: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 18
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Procednc'
        UpdCampo = 'Procednc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBProcednc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBProcednc: TdmkDBLookupComboBox
        Left = 72
        Top = 112
        Width = 408
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsProcednc
        TabOrder = 19
        dmkEditCB = EdProcednc
        QryCampo = 'Procednc'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdMotorista: TdmkEditCB
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 22
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Motorista'
        UpdCampo = 'Motorista'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBMotorista
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBMotorista: TdmkDBLookupComboBox
        Left = 72
        Top = 152
        Width = 408
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsMotorista
        TabOrder = 23
        dmkEditCB = EdMotorista
        QryCampo = 'Motorista'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdPlaca: TdmkEdit
        Left = 504
        Top = 152
        Width = 80
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 24
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Placa'
        UpdCampo = 'Placa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Edide_serie: TdmkEdit
        Left = 404
        Top = 72
        Width = 28
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ide_serie'
        UpdCampo = 'ide_serie'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edide_nNF: TdmkEdit
        Left = 436
        Top = 72
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999999999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ide_nNF'
        UpdCampo = 'ide_nNF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edemi_serie: TdmkEdit
        Left = 892
        Top = 72
        Width = 28
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'emi_serie'
        UpdCampo = 'emi_serie'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edemi_nNF: TdmkEdit
        Left = 924
        Top = 72
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999999999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'emi_nNF'
        UpdCampo = 'emi_nNF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 502
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 177
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label10: TLabel
        Left = 476
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Data / hora compra:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 628
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 780
        Top = 16
        Width = 103
        Height = 13
        Caption = 'Data / hora chegada:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 932
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label16: TLabel
        Left = 740
        Top = 136
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object Label17: TLabel
        Left = 592
        Top = 136
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        Visible = False
      end
      object Label18: TLabel
        Left = 664
        Top = 136
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Visible = False
      end
      object Label19: TLabel
        Left = 828
        Top = 136
        Width = 27
        Height = 13
        Caption = 'Peso:'
      end
      object Label9: TLabel
        Left = 916
        Top = 136
        Width = 50
        Height = 13
        Caption = 'Valor total:'
      end
      object Label14: TLabel
        Left = 16
        Top = 56
        Width = 70
        Height = 13
        Caption = 'Cliente de MO:'
      end
      object Label15: TLabel
        Left = 16
        Top = 96
        Width = 229
        Height = 13
        Caption = 'Proced'#234'ncia: (Tipo de entidade: Fornece outros)'
      end
      object Label24: TLabel
        Left = 16
        Top = 136
        Width = 212
        Height = 13
        Caption = 'Motorista: (Tipo de entidade: Fornece outros)'
      end
      object Label25: TLabel
        Left = 508
        Top = 56
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label26: TLabel
        Left = 508
        Top = 96
        Width = 69
        Height = 13
        Caption = 'Transportador:'
      end
      object Label27: TLabel
        Left = 508
        Top = 136
        Width = 30
        Height = 13
        Caption = 'Placa:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSRtbCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVSRtbCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 132
        Top = 32
        Width = 341
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsVSRtbCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 476
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtCompra'
        DataSource = DsVSRtbCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 628
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtViagem'
        DataSource = DsVSRtbCab
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 780
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtEntrada'
        DataSource = DsVSRtbCab
        TabOrder = 5
      end
      object DBEdit6: TDBEdit
        Left = 932
        Top = 32
        Width = 64
        Height = 21
        DataField = 'MovimCod'
        DataSource = DsVSRtbCab
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 508
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Fornecedor'
        DataSource = DsVSRtbCab
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 564
        Top = 72
        Width = 432
        Height = 21
        DataField = 'NO_FORNECE'
        DataSource = DsVSRtbCab
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 508
        Top = 112
        Width = 56
        Height = 21
        DataField = 'Transporta'
        DataSource = DsVSRtbCab
        TabOrder = 9
      end
      object DBEdit10: TDBEdit
        Left = 564
        Top = 112
        Width = 432
        Height = 21
        DataField = 'NO_TRANSPORTA'
        DataSource = DsVSRtbCab
        TabOrder = 10
      end
      object DBEdit11: TDBEdit
        Left = 740
        Top = 152
        Width = 84
        Height = 21
        DataField = 'Pecas'
        DataSource = DsVSRtbCab
        TabOrder = 11
      end
      object DBEdit12: TDBEdit
        Left = 828
        Top = 152
        Width = 84
        Height = 21
        DataField = 'PesoKg'
        DataSource = DsVSRtbCab
        TabOrder = 12
      end
      object DBEdit13: TDBEdit
        Left = 592
        Top = 152
        Width = 68
        Height = 21
        DataField = 'AreaM2'
        DataSource = DsVSRtbCab
        TabOrder = 13
        Visible = False
      end
      object DBEdit14: TDBEdit
        Left = 664
        Top = 152
        Width = 72
        Height = 21
        DataField = 'AreaP2'
        DataSource = DsVSRtbCab
        TabOrder = 14
        Visible = False
      end
      object DBEdit15: TDBEdit
        Left = 916
        Top = 152
        Width = 84
        Height = 21
        DataField = 'ValorT'
        DataSource = DsVSRtbCab
        TabOrder = 15
      end
      object DBEdit16: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'ClienteMO'
        DataSource = DsVSRtbCab
        TabOrder = 16
      end
      object DBEdit17: TDBEdit
        Left = 72
        Top = 72
        Width = 432
        Height = 21
        DataField = 'NO_CLIENTEMO'
        DataSource = DsVSRtbCab
        TabOrder = 17
      end
      object DBEdit18: TDBEdit
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        DataField = 'Motorista'
        DataSource = DsVSRtbCab
        TabOrder = 18
      end
      object DBEdit19: TDBEdit
        Left = 72
        Top = 152
        Width = 432
        Height = 21
        DataField = 'NO_MOTORISTA'
        DataSource = DsVSRtbCab
        TabOrder = 19
      end
      object DBEdit20: TDBEdit
        Left = 508
        Top = 152
        Width = 80
        Height = 21
        DataField = 'Placa'
        DataSource = DsVSRtbCab
        TabOrder = 20
      end
      object DBEdit21: TDBEdit
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        DataField = 'Procednc'
        DataSource = DsVSRtbCab
        TabOrder = 21
      end
      object DBEdit22: TDBEdit
        Left = 72
        Top = 112
        Width = 432
        Height = 21
        DataField = 'NO_PROCEDNC'
        DataSource = DsVSRtbCab
        TabOrder = 22
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 501
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 223
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 421
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtReclasif: TBitBtn
          Tag = 421
          Left = 252
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Gera artigos'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtReclasifClick
        end
      end
    end
    object PCItens: TPageControl
      Left = 4
      Top = 216
      Width = 1008
      Height = 285
      ActivePage = TsFrCompr
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' Dados da entrada'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 257
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GBIts: TGroupBox
            Left = 0
            Top = 0
            Width = 1000
            Height = 257
            Align = alClient
            Caption = ' Itens da entrada: '
            TabOrder = 0
            object DGDados: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 996
              Height = 240
              Align = alClient
              DataSource = DsVSRtbIts
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              PopupMenu = PMVRtbIts
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_TTW'
                  Title.Caption = 'Arquivo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SerieFch'
                  Title.Caption = 'ID Serie'
                  Width = 43
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Ficha'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pallet'
                  Title.Caption = 'ID Pallet'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PALLET'
                  Title.Caption = 'Pallet'
                  Width = 76
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'GraGruX'
                  Title.Caption = 'Reduzido'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PRD_TAM_COR'
                  Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                  Width = 240
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea m'#178
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaP2'
                  Title.Caption = #193'rea ft'#178
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PesoKg'
                  Title.Caption = 'Peso kg'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'IME-I'
                  Width = 60
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Observ'
                  Title.Caption = 'Observa'#231#245'es'
                  Width = 300
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValorMP'
                  Title.Caption = 'Valor MP'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CusFrtAvuls'
                  Title.Caption = 'Frete entrada'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CusFrtMOEnv'
                  Title.Caption = 'Frete envio MO'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValorT'
                  Title.Caption = 'Valor total'
                  Width = 68
                  Visible = True
                end>
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Hist'#243'rico da Ficha RMP selecionada'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Splitter2: TSplitter
          Left = 870
          Top = 0
          Width = 5
          Height = 257
        end
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 870
          Height = 257
          Align = alLeft
          DataSource = DsVSHisFch
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data / hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Palavras chave [TAG]'
              Width = 187
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Title.Caption = 'Texto hist'#243'rico:'
              Width = 460
              Visible = True
            end>
        end
        object DBMemo1: TDBMemo
          Left = 875
          Top = 0
          Width = 125
          Height = 257
          Align = alClient
          DataField = 'Observ'
          DataSource = DsVSHisFch
          TabOrder = 1
        end
      end
      object TsFrCompr: TTabSheet
        Caption = 'Dados frete devolu'#231#227'o (entrada empresa)'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnFrCompr: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 257
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGVSMOEnvAvu: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 529
            Height = 257
            Align = alLeft
            DataSource = DsVSMOEnvAvu
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMA_nCT'
                Title.Caption = 'N'#186' CT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMA_Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMA_AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMA_PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMA_ValorT'
                Title.Caption = 'Valor frete'
                Width = 72
                Visible = True
              end>
          end
          object DBGVSMOEnvAVMI: TdmkDBGridZTO
            Left = 529
            Top = 0
            Width = 471
            Height = 257
            Align = alClient
            DataSource = DsVSMOEnvAVMI
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VSMovIts'
                Title.Caption = 'IME-I'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorFrete'
                Title.Caption = '$ Frete'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end>
          end
        end
      end
      object TsEnvioMO: TTabSheet
        Caption = ' Dados do envio para M.O. '
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnEnvioMO: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 257
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGVSMOEnvEnv: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 529
            Height = 257
            Align = alLeft
            DataSource = DsVSMOEnvEnv
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NFEMP_nNF'
                Title.Caption = 'N'#186' NF'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_nCT'
                Title.Caption = 'N'#186' CT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CFTMP_ValorT'
                Title.Caption = 'Valor frete'
                Width = 72
                Visible = True
              end>
          end
          object DBGVSMOEnvEVMI: TdmkDBGridZTO
            Left = 529
            Top = 0
            Width = 471
            Height = 257
            Align = alClient
            DataSource = DsVSMOEnvEVMI
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VSMovIts'
                Title.Caption = 'IME-I'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorFrete'
                Title.Caption = '$ Frete'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AreaM2'
                Title.Caption = #193'rea m'#178
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PesoKg'
                Title.Caption = 'Peso kg'
                Width = 72
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 45
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 396
        Height = 32
        Caption = 'Entrada de Artigo Por Devolu'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 396
        Height = 32
        Caption = 'Entrada de Artigo Por Devolu'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 396
        Height = 32
        Caption = 'Entrada de Artigo Por Devolu'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 600
    Top = 44
  end
  object QrVSRtbCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSRtbCabBeforeOpen
    AfterOpen = QrVSRtbCabAfterOpen
    BeforeClose = QrVSRtbCabBeforeClose
    AfterScroll = QrVSRtbCabAfterScroll
    SQL.Strings = (
      'SELECT wic.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO,'
      'IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC,'
      'IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA'
      'FROM vsinncab wic'
      'LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor'
      'LEFT JOIN entidades trn ON trn.Codigo=wic.Transporta'
      'LEFT JOIN entidades cli ON cli.Codigo=wic.ClienteMO'
      'LEFT JOIN entidades prc ON prc.Codigo=wic.Procednc'
      'LEFT JOIN entidades mot ON mot.Codigo=wic.Motorista'
      ''
      'WHERE wic.Codigo > 0')
    Left = 684
    Top = 1
    object QrVSRtbCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSRtbCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSRtbCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSRtbCabDtCompra: TDateTimeField
      FieldName = 'DtCompra'
    end
    object QrVSRtbCabDtViagem: TDateTimeField
      FieldName = 'DtViagem'
    end
    object QrVSRtbCabDtEntrada: TDateTimeField
      FieldName = 'DtEntrada'
    end
    object QrVSRtbCabFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrVSRtbCabTransporta: TIntegerField
      FieldName = 'Transporta'
    end
    object QrVSRtbCabPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRtbCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRtbCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRtbCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRtbCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSRtbCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSRtbCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSRtbCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSRtbCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSRtbCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSRtbCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSRtbCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSRtbCabNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSRtbCabNO_TRANSPORTA: TWideStringField
      FieldName = 'NO_TRANSPORTA'
      Size = 100
    end
    object QrVSRtbCabValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRtbCabClienteMO: TIntegerField
      FieldName = 'ClienteMO'
    end
    object QrVSRtbCabProcednc: TIntegerField
      FieldName = 'Procednc'
    end
    object QrVSRtbCabMotorista: TIntegerField
      FieldName = 'Motorista'
    end
    object QrVSRtbCabPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrVSRtbCabNO_CLIENTEMO: TWideStringField
      FieldName = 'NO_CLIENTEMO'
      Size = 100
    end
    object QrVSRtbCabNO_PROCEDNC: TWideStringField
      FieldName = 'NO_PROCEDNC'
      Size = 100
    end
    object QrVSRtbCabNO_MOTORISTA: TWideStringField
      FieldName = 'NO_MOTORISTA'
      Size = 100
    end
    object QrVSRtbCabTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSRtbCabide_serie: TSmallintField
      FieldName = 'ide_serie'
    end
    object QrVSRtbCabide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrVSRtbCabemi_serie: TSmallintField
      FieldName = 'emi_serie'
    end
    object QrVSRtbCabemi_nNF: TIntegerField
      FieldName = 'emi_nNF'
    end
    object QrVSRtbCabNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
    end
  end
  object DsVSRtbCab: TDataSource
    DataSet = QrVSRtbCab
    Left = 688
    Top = 45
  end
  object QrVSRtbIts: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSRtbItsBeforeClose
    AfterScroll = QrVSRtbItsAfterScroll
    OnCalcFields = QrVSRtbItsCalcFields
    SQL.Strings = (
      'SELECT wmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,'
      'IF(wmi.SdoVrtPeca > 0, 0,'
      '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2,'
      'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT('
      '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3),'
      '  ",", ""), ".", ",")) RendKgm2_TXT,'
      'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE('
      'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT,'
      'IF(wmi.Misturou = 1, "SIM", "N'#195'O") Misturou_TXT,'
      'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,'
      'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT('
      '  IF(wmi.Pecas > 0, wmi.QtdGerArM2 / wmi.Pecas, 0), 3),'
      '  ",", ""), ".", ",")) m2_CouroTXT,'
      'IF(wmi.Pecas <> 0, wmi.PesoKg / wmi.Pecas, 0) KgMedioCouro'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE wmi.MovimCod=0'
      'ORDER BY NO_Pallet, wmi.Controle')
    Left = 768
    Top = 1
    object QrVSRtbItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSRtbItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSRtbItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSRtbItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSRtbItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSRtbItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSRtbItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSRtbItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSRtbItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSRtbItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSRtbItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSRtbItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSRtbItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRtbItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRtbItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRtbItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRtbItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRtbItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSRtbItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSRtbItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSRtbItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSRtbItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSRtbItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRtbItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRtbItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSRtbItsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSRtbItsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSRtbItsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSRtbItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSRtbItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRtbItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRtbItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRtbItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRtbItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSRtbItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSRtbItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSRtbItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSRtbItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSRtbItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRtbItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRtbItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRtbItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSRtbItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSRtbItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRtbItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRtbItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSRtbItsPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSRtbItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSRtbItsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSRtbItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSRtbItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSRtbItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSRtbItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSRtbItsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSRtbItsRendKgm2: TFloatField
      FieldName = 'RendKgm2'
      DisplayFormat = '0.000'
    end
    object QrVSRtbItsNotaMPAG_TXT: TWideStringField
      FieldName = 'NotaMPAG_TXT'
      Size = 30
    end
    object QrVSRtbItsRendKgm2_TXT: TWideStringField
      FieldName = 'RendKgm2_TXT'
      Size = 30
    end
    object QrVSRtbItsMisturou_TXT: TWideStringField
      FieldName = 'Misturou_TXT'
      Size = 3
    end
    object QrVSRtbItsNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrVSRtbItsSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrVSRtbItsm2_CouroTXT: TWideStringField
      FieldName = 'm2_CouroTXT'
      Size = 30
    end
    object QrVSRtbItsKgMedioCouro: TFloatField
      FieldName = 'KgMedioCouro'
      DisplayFormat = '0.000'
    end
    object QrVSRtbItsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrVSRtbItsClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSRtbItsIxxMovIX: TLargeintField
      FieldName = 'IxxMovIX'
    end
    object QrVSRtbItsIxxFolha: TLargeintField
      FieldName = 'IxxFolha'
    end
    object QrVSRtbItsIxxLinha: TLargeintField
      FieldName = 'IxxLinha'
    end
    object QrVSRtbItsCusFrtAvuls: TFloatField
      FieldName = 'CusFrtAvuls'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSRtbItsCusFrtMOEnv: TFloatField
      FieldName = 'CusFrtMOEnv'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsVSRtbIts: TDataSource
    DataSet = QrVSRtbIts
    Left = 772
    Top = 45
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 660
    Top = 580
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object AtrelamentoFreteCompra1: TMenuItem
      Caption = 'Atrelamento Frete entrada'
      object IncluiAtrelamento2: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento2Click
      end
      object AlteraAtrelamento2: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento2Click
      end
      object ExcluiAtrelamento2: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento2Click
      end
    end
    object AtrelamentoNFsdeMO1: TMenuItem
      Caption = 'Atrelamento NFs de MO'
      object IncluiAtrelamento1: TMenuItem
        Caption = '&Inclui Atrelamento'
        OnClick = IncluiAtrelamento1Click
      end
      object AlteraAtrelamento1: TMenuItem
        Caption = '&Altera Atrelamento'
        OnClick = AlteraAtrelamento1Click
      end
      object ExcluiAtrelamento1: TMenuItem
        Caption = '&Exclui Atrelamento'
        OnClick = ExcluiAtrelamento1Click
      end
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Atualizaestoque1: TMenuItem
      Caption = 'Atuali&za estoque'
      OnClick = Atualizaestoque1Click
    end
    object Histrico1: TMenuItem
      Caption = '&Hist'#243'rico'
      object Inclui1: TMenuItem
        Caption = '&Inclui'
        OnClick = Inclui1Click
      end
      object Altera1: TMenuItem
        Caption = '&Altera'
        OnClick = Altera1Click
      end
      object Exclui1: TMenuItem
        Caption = '&Exclui'
        OnClick = Exclui1Click
      end
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 536
    Top = 584
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrFornecedor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 844
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 844
    Top = 44
  end
  object QrTransporta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 924
    Top = 4
    object QrTransportaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportaNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransporta: TDataSource
    DataSet = QrTransporta
    Left = 924
    Top = 48
  end
  object PMImprime: TPopupMenu
    Left = 16
    Top = 4
    object FichadePallets1: TMenuItem
      Caption = 'Ficha do pallet selecionado'
      object FichaCOMnomedoPallet1: TMenuItem
        Caption = 'Ficha &COM nome do Pallet'
        OnClick = FichaCOMnomedoPallet1Click
      end
      object FichaSEMnomedoPallet1: TMenuItem
        Caption = 'Ficha &SEM nome do Pallet'
        OnClick = FichaSEMnomedoPallet1Click
      end
    end
    object Fichadetodospalletsdestacompra1: TMenuItem
      Caption = 'Ficha de todos pallets desta compra'
      object FichasCOMnomedoPallet1: TMenuItem
        Caption = 'Fichas &COM nome do Pallet'
        OnClick = FichasCOMnomedoPallet1Click
      end
      object FichasSEMnomedoPallet1: TMenuItem
        Caption = 'Fichas &SEM nome do Pallet'
        OnClick = FichasSEMnomedoPallet1Click
      end
    end
    object PackingList1: TMenuItem
      Caption = 'Packing List'
      OnClick = PackingList1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Estoque1: TMenuItem
      Caption = '&Estoque (mostrar'#225' outra janela)'
      OnClick = Estoque1Click
    end
  end
  object frxDsVSRtbCab: TfrxDBDataset
    UserName = 'frxDsVSRtbCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'DtCompra=DtCompra'
      'DtViagem=DtViagem'
      'DtEntrada=DtEntrada'
      'Fornecedor=Fornecedor'
      'Transporta=Transporta'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_FORNECE=NO_FORNECE'
      'NO_TRANSPORTA=NO_TRANSPORTA'
      'ValorT=ValorT')
    DataSet = QrVSRtbCab
    BCDToCurrency = False
    Left = 688
    Top = 92
  end
  object frxDsVSRtbIts: TfrxDBDataset
    UserName = 'frxDsVSRtbIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'MovimID=MovimID'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'SdoVrtArM2=SdoVrtArM2'
      'SdoVrtPeca=SdoVrtPeca'
      'Observ=Observ'
      'ValorT=ValorT'
      'MovimTwn=MovimTwn'
      'Misturou=Misturou'
      'Ficha=Ficha'
      'SerieFch=SerieFch'
      'NO_SerieFch=NO_SerieFch'
      'NotaMPAG=NotaMPAG'
      'RendKgm2=RendKgm2'
      'NotaMPAG_TXT=NotaMPAG_TXT'
      'RendKgm2_TXT=RendKgm2_TXT'
      'MovimNiv=MovimNiv'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'SdoVrtPeso=SdoVrtPeso'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'SrcGGX=SrcGGX'
      'DstGGX=DstGGX'
      'Misturou_TXT=Misturou_TXT'
      'NOMEUNIDMED=NOMEUNIDMED'
      'SIGLAUNIDMED=SIGLAUNIDMED'
      'm2_CouroTXT=m2_CouroTXT'
      'KgMedioCouro=KgMedioCouro')
    DataSet = QrVSRtbIts
    BCDToCurrency = False
    Left = 772
    Top = 92
  end
  object QrClienteMO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 864
    Top = 340
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClienteMO: TDataSource
    DataSet = QrClienteMO
    Left = 864
    Top = 388
  end
  object QrProcednc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 944
    Top = 340
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsProcednc: TDataSource
    DataSet = QrProcednc
    Left = 944
    Top = 388
  end
  object QrMotorista: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 796
    Top = 340
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsMotorista: TDataSource
    DataSet = QrMotorista
    Left = 796
    Top = 388
  end
  object QrVSMovDif: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM vsmovdif'
      'WHERE Controle=1')
    Left = 772
    Top = 144
    object QrVSMovDifControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovDifInfPecas: TFloatField
      FieldName = 'InfPecas'
    end
    object QrVSMovDifInfPesoKg: TFloatField
      FieldName = 'InfPesoKg'
    end
    object QrVSMovDifInfAreaM2: TFloatField
      FieldName = 'InfAreaM2'
    end
    object QrVSMovDifInfAreaP2: TFloatField
      FieldName = 'InfAreaP2'
    end
    object QrVSMovDifInfValorT: TFloatField
      FieldName = 'InfValorT'
    end
    object QrVSMovDifDifPecas: TFloatField
      FieldName = 'DifPecas'
    end
    object QrVSMovDifDifPesoKg: TFloatField
      FieldName = 'DifPesoKg'
    end
    object QrVSMovDifDifAreaM2: TFloatField
      FieldName = 'DifAreaM2'
    end
    object QrVSMovDifDifAreaP2: TFloatField
      FieldName = 'DifAreaP2'
    end
    object QrVSMovDifDifValorT: TFloatField
      FieldName = 'DifValorT'
    end
    object QrVSMovDifLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMovDifDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMovDifDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMovDifUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMovDifUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMovDifAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMovDifAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrVSHisFch: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vshisfch '
      'WHERE VSMovIts=0 '
      'OR ( '
      '  SerieFch=0 '
      '  AND '
      '  Ficha=0 '
      ') ')
    Left = 404
    Top = 336
    object QrVSHisFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSHisFchVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSHisFchSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSHisFchFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSHisFchDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSHisFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSHisFchObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSHisFchLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSHisFchDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSHisFchDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSHisFchUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSHisFchUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSHisFchAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSHisFchAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSHisFch: TDataSource
    DataSet = QrVSHisFch
    Left = 404
    Top = 384
  end
  object PMVRtbIts: TPopupMenu
    Left = 132
    Top = 368
    object IrparajaneladegerenciamentodeFichaRMP1: TMenuItem
      Caption = '&Ir para janela de gerenciamento de Ficha RMP'
      OnClick = IrparajaneladegerenciamentodeFichaRMP1Click
    end
  end
  object frxDsPallets: TfrxDBDataset
    UserName = 'frxDsPallets'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'MovimID=MovimID'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'Pallet=Pallet'
      'NO_PALLET=NO_Pallet'
      'SdoVrtArM2=SdoVrtArM2'
      'SdoVrtPeca=SdoVrtPeca'
      'Observ=Observ'
      'ValorT=ValorT'
      'MovimTwn=MovimTwn'
      'Misturou=Misturou'
      'Ficha=Ficha'
      'SerieFch=SerieFch'
      'NO_SerieFch=NO_SerieFch'
      'NotaMPAG=NotaMPAG'
      'RendKgm2=RendKgm2'
      'NotaMPAG_TXT=NotaMPAG_TXT'
      'RendKgm2_TXT=RendKgm2_TXT'
      'MovimNiv=MovimNiv'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'SdoVrtPeso=SdoVrtPeso'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'SrcGGX=SrcGGX'
      'DstGGX=DstGGX'
      'Misturou_TXT=Misturou_TXT'
      'NOMEUNIDMED=NOMEUNIDMED'
      'SIGLAUNIDMED=SIGLAUNIDMED'
      'm2_CouroTXT=m2_CouroTXT'
      'KgMedioCouro=KgMedioCouro'
      'Marca=Marca'
      'SEQ=SEQ')
    DataSet = QrVSRtbIts
    BCDToCurrency = False
    Left = 64
    Top = 388
  end
  object frxWET_CURTI_106_00_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444450000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxWET_CURTI_106_00_AGetValue
    Left = 60
    Top = 440
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPallets
        DataSetName = 'frxDsPallets'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 102.047295350000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 56.692950000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 34.015770000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 34.015770000000000000
          Width = 468.661720000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Packing List de Devolu'#231#227'o para Retrabalho -  ID [VARF_ID]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 34.015770000000000000
          Width = 98.267716540000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 34.015770000000000000
          Width = 98.267716540000000000
          Height = 22.677165350000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          Left = 132.283550000000000000
          Top = 79.370130000000000000
          Width = 366.614214720000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 45.354360000000000000
          Top = 79.370130000000000000
          Width = 86.929146060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pallet')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          Left = 498.897960000000000000
          Top = 79.370130000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 589.606680000000000000
          Top = 79.370130000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 79.370130000000000000
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Item')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 22.677165350000000000
        Top = 181.417440000000000000
        Width = 680.315400000000000000
        DataSet = frxDsPallets
        DataSetName = 'frxDsPallets'
        RowCount = 0
        object MeValNome: TfrxMemoView
          Left = 132.283550000000000000
          Width = 366.614214720000000000
          Height = 22.677165350000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPallets."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          Left = 45.354360000000000000
          Width = 86.929146060000000000
          Height = 22.677165350000000000
          DataField = 'Pallet'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."Pallet"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          Left = 498.897960000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          DataField = 'Pecas'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          DisplayFormat.FormatStr = '%2.n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPallets."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 589.606680000000000000
          Width = 90.708661420000000000
          Height = 22.677165350000000000
          DataField = 'AreaM2'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPallets."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Width = 45.354330710000000000
          Height = 22.677165350000000000
          DataField = 'SEQ'
          DataSet = frxDsPallets
          DataSetName = 'frxDsPallets'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPallets."SEQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 325.039580000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 37.795285350000000000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          Top = 15.118119999999980000
          Width = 498.897764720000000000
          Height = 22.677165354330710000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:  [COUNT(MD002)] Pallets  ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 589.606680000000000000
          Top = 15.118120000000000000
          Width = 90.708671180000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<SUM(<frxDsPallets."AreaM2">,MD002,1)>]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaP2: TfrxMemoView
          Left = 498.897960000000000000
          Top = 15.118120000000000000
          Width = 90.708671180000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = '%2.n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<SUM(<frxDsPallets."Pecas">,MD002,1)>]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrVSMOEnvEnv: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSMOEnvEnvBeforeClose
    AfterScroll = QrVSMOEnvEnvAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvenv cmo'
      'WHERE cmo.VSVMI_Controle=:P0')
    Left = 564
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvEnvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvEnvNFEMP_FatID: TIntegerField
      FieldName = 'NFEMP_FatID'
    end
    object QrVSMOEnvEnvNFEMP_FatNum: TIntegerField
      FieldName = 'NFEMP_FatNum'
    end
    object QrVSMOEnvEnvNFEMP_Empresa: TIntegerField
      FieldName = 'NFEMP_Empresa'
    end
    object QrVSMOEnvEnvNFEMP_Terceiro: TIntegerField
      FieldName = 'NFEMP_Terceiro'
    end
    object QrVSMOEnvEnvNFEMP_nItem: TIntegerField
      FieldName = 'NFEMP_nItem'
    end
    object QrVSMOEnvEnvNFEMP_SerNF: TIntegerField
      FieldName = 'NFEMP_SerNF'
    end
    object QrVSMOEnvEnvNFEMP_nNF: TIntegerField
      FieldName = 'NFEMP_nNF'
    end
    object QrVSMOEnvEnvNFEMP_Pecas: TFloatField
      FieldName = 'NFEMP_Pecas'
    end
    object QrVSMOEnvEnvNFEMP_PesoKg: TFloatField
      FieldName = 'NFEMP_PesoKg'
    end
    object QrVSMOEnvEnvNFEMP_AreaM2: TFloatField
      FieldName = 'NFEMP_AreaM2'
    end
    object QrVSMOEnvEnvNFEMP_AreaP2: TFloatField
      FieldName = 'NFEMP_AreaP2'
    end
    object QrVSMOEnvEnvNFEMP_ValorT: TFloatField
      FieldName = 'NFEMP_ValorT'
    end
    object QrVSMOEnvEnvCFTMP_FatID: TIntegerField
      FieldName = 'CFTMP_FatID'
    end
    object QrVSMOEnvEnvCFTMP_FatNum: TIntegerField
      FieldName = 'CFTMP_FatNum'
    end
    object QrVSMOEnvEnvCFTMP_Empresa: TIntegerField
      FieldName = 'CFTMP_Empresa'
    end
    object QrVSMOEnvEnvCFTMP_Terceiro: TIntegerField
      FieldName = 'CFTMP_Terceiro'
    end
    object QrVSMOEnvEnvCFTMP_nItem: TIntegerField
      FieldName = 'CFTMP_nItem'
    end
    object QrVSMOEnvEnvCFTMP_SerCT: TIntegerField
      FieldName = 'CFTMP_SerCT'
    end
    object QrVSMOEnvEnvCFTMP_nCT: TIntegerField
      FieldName = 'CFTMP_nCT'
    end
    object QrVSMOEnvEnvCFTMP_Pecas: TFloatField
      FieldName = 'CFTMP_Pecas'
    end
    object QrVSMOEnvEnvCFTMP_PesoKg: TFloatField
      FieldName = 'CFTMP_PesoKg'
    end
    object QrVSMOEnvEnvCFTMP_AreaM2: TFloatField
      FieldName = 'CFTMP_AreaM2'
    end
    object QrVSMOEnvEnvCFTMP_AreaP2: TFloatField
      FieldName = 'CFTMP_AreaP2'
    end
    object QrVSMOEnvEnvCFTMP_PesTrKg: TFloatField
      FieldName = 'CFTMP_PesTrKg'
    end
    object QrVSMOEnvEnvCFTMP_CusTrKg: TFloatField
      FieldName = 'CFTMP_CusTrKg'
    end
    object QrVSMOEnvEnvCFTMP_ValorT: TFloatField
      FieldName = 'CFTMP_ValorT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvEnvVSVMI_MovimCod: TIntegerField
      FieldName = 'VSVMI_MovimCod'
    end
    object QrVSMOEnvEnvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvEnvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvEnvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvEnvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvEnvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvEnvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvEnvAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvEnvAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvEnvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSMOEnvEnv: TDataSource
    DataSet = QrVSMOEnvEnv
    Left = 564
    Top = 384
  end
  object QrVSMOEnvEVMI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmoenvevmi'
      'WHERE VSMOEnvEnv=:p0')
    Left = 564
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvEVMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvEVMIVSMOEnvEnv: TIntegerField
      FieldName = 'VSMOEnvEnv'
    end
    object QrVSMOEnvEVMIVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSMOEnvEVMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvEVMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvEVMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvEVMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvEVMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvEVMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvEVMIAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvEVMIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvEVMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvEVMIValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvEVMIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0'
    end
    object QrVSMOEnvEVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSMOEnvEVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvEVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsVSMOEnvEVMI: TDataSource
    DataSet = QrVSMOEnvEVMI
    Left = 564
    Top = 484
  end
  object QrVSMOEnvAvu: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSMOEnvAvuBeforeClose
    AfterScroll = QrVSMOEnvAvuAfterScroll
    SQL.Strings = (
      'SELECT cmo.* '
      'FROM vsmoenvenv cmo'
      'WHERE cmo.VSVMI_Controle=:P0')
    Left = 660
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvAvuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvAvuCFTMA_FatID: TIntegerField
      FieldName = 'CFTMA_FatID'
    end
    object QrVSMOEnvAvuCFTMA_FatNum: TIntegerField
      FieldName = 'CFTMA_FatNum'
    end
    object QrVSMOEnvAvuCFTMA_Empresa: TIntegerField
      FieldName = 'CFTMA_Empresa'
    end
    object QrVSMOEnvAvuCFTMA_Terceiro: TIntegerField
      FieldName = 'CFTMA_Terceiro'
    end
    object QrVSMOEnvAvuCFTMA_nItem: TIntegerField
      FieldName = 'CFTMA_nItem'
    end
    object QrVSMOEnvAvuCFTMA_SerCT: TIntegerField
      FieldName = 'CFTMA_SerCT'
    end
    object QrVSMOEnvAvuCFTMA_nCT: TIntegerField
      FieldName = 'CFTMA_nCT'
    end
    object QrVSMOEnvAvuCFTMA_Pecas: TFloatField
      FieldName = 'CFTMA_Pecas'
    end
    object QrVSMOEnvAvuCFTMA_PesoKg: TFloatField
      FieldName = 'CFTMA_PesoKg'
    end
    object QrVSMOEnvAvuCFTMA_AreaM2: TFloatField
      FieldName = 'CFTMA_AreaM2'
    end
    object QrVSMOEnvAvuCFTMA_AreaP2: TFloatField
      FieldName = 'CFTMA_AreaP2'
    end
    object QrVSMOEnvAvuCFTMA_PesTrKg: TFloatField
      FieldName = 'CFTMA_PesTrKg'
    end
    object QrVSMOEnvAvuCFTMA_CusTrKg: TFloatField
      FieldName = 'CFTMA_CusTrKg'
    end
    object QrVSMOEnvAvuCFTMA_ValorT: TFloatField
      FieldName = 'CFTMA_ValorT'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvAvuVSVMI_MovimCod: TIntegerField
      FieldName = 'VSVMI_MovimCod'
    end
    object QrVSMOEnvAvuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvAvuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvAvuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvAvuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvAvuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvAvuAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvAvuAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvAvuAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvAvuAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSMOEnvAvu: TDataSource
    DataSet = QrVSMOEnvAvu
    Left = 660
    Top = 384
  end
  object QrVSMOEnvAVMI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmoenvevmi'
      'WHERE VSMOEnvEnv=:p0')
    Left = 660
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end>
    object QrVSMOEnvAVMICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMOEnvAVMIVSMOEnvAvu: TIntegerField
      FieldName = 'VSMOEnvAvu'
    end
    object QrVSMOEnvAVMIVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSMOEnvAVMILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMOEnvAVMIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMOEnvAVMIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMOEnvAVMIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMOEnvAVMIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMOEnvAVMIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMOEnvAVMIAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSMOEnvAVMIAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSMOEnvAVMIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvAVMIValorFrete: TFloatField
      FieldName = 'ValorFrete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvAVMIPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0'
    end
    object QrVSMOEnvAVMIPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrVSMOEnvAVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVSMOEnvAVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsVSMOEnvAVMI: TDataSource
    DataSet = QrVSMOEnvAVMI
    Left = 660
    Top = 484
  end
end
