object FmVSEstqCustoIntegr: TFmVSEstqCustoIntegr
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-201 :: Estoque - Custo Integrado'
  ClientHeight = 783
  ClientWidth = 849
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 849
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 801
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 753
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 320
        Height = 32
        Caption = 'Estoque - Custo Integrado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 320
        Height = 32
        Caption = 'Estoque - Custo Integrado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 320
        Height = 32
        Caption = 'Estoque - Custo Integrado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 849
    Height = 610
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 849
      Height = 610
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 849
        Height = 610
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 845
          Height = 593
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 845
            Height = 49
            Align = alTop
            TabOrder = 0
            object SpeedButton1: TSpeedButton
              Left = 124
              Top = 24
              Width = 23
              Height = 22
              Caption = '<'
              OnClick = SpeedButton1Click
            end
            object LaEmpresa: TLabel
              Left = 152
              Top = 8
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Ck26EstoqueEm: TCheckBox
              Left = 8
              Top = 4
              Width = 97
              Height = 17
              Caption = 'Estoque em...:'
              Checked = True
              Enabled = False
              State = cbChecked
              TabOrder = 0
            end
            object TP26EstoqueEm: TdmkEditDateTimePicker
              Left = 8
              Top = 24
              Width = 112
              Height = 21
              CalColors.TextColor = clMenuText
              Date = 43132.000000000000000000
              Time = 0.777157974502188200
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object CBEmpresa: TdmkDBLookupComboBox
              Left = 208
              Top = 24
              Width = 437
              Height = 21
              KeyField = 'Filial'
              ListField = 'NOMEFILIAL'
              ListSource = DModG.DsEmpresas
              TabOrder = 2
              TabStop = False
              dmkEditCB = EdEmpresa
              QryCampo = 'Empresa'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdEmpresa: TdmkEditCB
              Left = 152
              Top = 24
              Width = 56
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Empresa'
              UpdCampo = 'Empresa'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnRedefinido = EdEmpresaRedefinido
              DBLookupComboBox = CBEmpresa
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
          end
          object PCGeral: TPageControl
            Left = 0
            Top = 49
            Width = 845
            Height = 544
            ActivePage = TabSheet2
            Align = alClient
            TabOrder = 1
            object TabSheet1: TTabSheet
              Caption = 'Mensagens'
              object Memo1: TMemo
                Left = 0
                Top = 0
                Width = 837
                Height = 516
                Align = alClient
                TabOrder = 0
              end
            end
            object TabSheet2: TTabSheet
              Caption = 'Resultado da pesquisa'
              ImageIndex = 1
              object PCRelatorio: TPageControl
                Left = 0
                Top = 244
                Width = 837
                Height = 272
                ActivePage = TabSheet3
                Align = alBottom
                TabOrder = 0
                object TabSheet3: TTabSheet
                  Caption = 'Insumos'
                  object Panel23: TPanel
                    Left = 0
                    Top = 0
                    Width = 829
                    Height = 244
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object PnSetores: TPanel
                      Left = 588
                      Top = 0
                      Width = 241
                      Height = 244
                      Align = alRight
                      BevelOuter = bvNone
                      TabOrder = 0
                      object DBGSetores: TdmkDBGridZTO
                        Left = 0
                        Top = 69
                        Width = 241
                        Height = 175
                        Align = alClient
                        DataSource = DsListaSetores
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                        TabOrder = 0
                        TitleFont.Charset = ANSI_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -11
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        RowColors = <>
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'Codigo'
                            Title.Caption = 'C'#243'd'
                            Width = 32
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Nome'
                            Title.Caption = 'Nome setor'
                            Width = 148
                            Visible = True
                          end>
                      end
                      object Panel7: TPanel
                        Left = 0
                        Top = 0
                        Width = 241
                        Height = 69
                        Align = alTop
                        BevelOuter = bvNone
                        TabOrder = 1
                        object Label8: TLabel
                          Left = 0
                          Top = 0
                          Width = 39
                          Height = 13
                          Align = alTop
                          Alignment = taCenter
                          Caption = 'Setores:'
                          Font.Charset = ANSI_CHARSET
                          Font.Color = clWindowText
                          Font.Height = -12
                          Font.Name = 'MS Sans Serif'
                          Font.Style = []
                          ParentFont = False
                        end
                        object BtTodosSetores: TBitBtn
                          Tag = 127
                          Left = 5
                          Top = 24
                          Width = 90
                          Height = 40
                          Cursor = crHandPoint
                          Caption = '&Todos'
                          NumGlyphs = 2
                          ParentShowHint = False
                          ShowHint = True
                          TabOrder = 0
                          OnClick = BtTodosSetoresClick
                        end
                        object BtNenhumSetor: TBitBtn
                          Tag = 128
                          Left = 117
                          Top = 24
                          Width = 90
                          Height = 40
                          Cursor = crHandPoint
                          Caption = '&Nenhum'
                          NumGlyphs = 2
                          ParentShowHint = False
                          ShowHint = True
                          TabOrder = 1
                        end
                      end
                    end
                    object Panel8: TPanel
                      Left = 0
                      Top = 0
                      Width = 588
                      Height = 244
                      Align = alClient
                      BevelOuter = bvNone
                      TabOrder = 1
                      object Panel9: TPanel
                        Left = 0
                        Top = 87
                        Width = 588
                        Height = 48
                        Align = alBottom
                        BevelOuter = bvNone
                        TabOrder = 2
                        object LaCI1: TLabel
                          Left = 4
                          Top = 4
                          Width = 114
                          Height = 13
                          Caption = 'Cliente interno debitado:'
                        end
                        object Label4: TLabel
                          Left = 348
                          Top = 4
                          Width = 55
                          Height = 13
                          Caption = 'Data inicial:'
                        end
                        object Label5: TLabel
                          Left = 465
                          Top = 4
                          Width = 48
                          Height = 13
                          Caption = 'Data final:'
                        end
                        object EdCIDst: TdmkEditCB
                          Left = 4
                          Top = 20
                          Width = 65
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '-2147483647'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                          OnRedefinido = EdCIDstRedefinido
                          DBLookupComboBox = CBCIDst
                          IgnoraDBLookupComboBox = False
                          AutoSetIfOnlyOneReg = setregOnlyManual
                        end
                        object CBCIDst: TdmkDBLookupComboBox
                          Left = 72
                          Top = 20
                          Width = 269
                          Height = 21
                          KeyField = 'Codigo'
                          ListField = 'NOMECI'
                          ListSource = DsCIDst
                          TabOrder = 1
                          dmkEditCB = EdCIDst
                          UpdType = utYes
                          LocF7SQLMasc = '$#'
                          LocF7PreDefProc = f7pNone
                        end
                        object TPIni: TdmkEditDateTimePicker
                          Left = 348
                          Top = 20
                          Width = 112
                          Height = 21
                          Date = 37670.000000000000000000
                          Time = 0.521748657396528900
                          TabOrder = 2
                          ReadOnly = False
                          DefaultEditMask = '!99/99/99;1;_'
                          AutoApplyEditMask = True
                          UpdType = utYes
                          DatePurpose = dmkdpNone
                        end
                        object TPFim: TdmkEditDateTimePicker
                          Left = 465
                          Top = 20
                          Width = 112
                          Height = 21
                          Date = 37670.000000000000000000
                          Time = 0.521748657396528900
                          TabOrder = 3
                          ReadOnly = False
                          DefaultEditMask = '!99/99/99;1;_'
                          AutoApplyEditMask = True
                          UpdType = utYes
                          DatePurpose = dmkdpNone
                        end
                      end
                      object Panel11: TPanel
                        Left = 0
                        Top = 0
                        Width = 588
                        Height = 48
                        Align = alTop
                        BevelOuter = bvNone
                        TabOrder = 0
                        object LaPQ: TLabel
                          Left = 4
                          Top = 0
                          Width = 37
                          Height = 13
                          Caption = 'Insumo:'
                        end
                        object EdPQ: TdmkEditCB
                          Left = 4
                          Top = 16
                          Width = 65
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '-2147483647'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                          DBLookupComboBox = CBPQ
                          IgnoraDBLookupComboBox = False
                          AutoSetIfOnlyOneReg = setregOnlyManual
                        end
                        object CBPQ: TdmkDBLookupComboBox
                          Left = 72
                          Top = 16
                          Width = 505
                          Height = 21
                          KeyField = 'Codigo'
                          ListField = 'Nome'
                          ListSource = DsPQ
                          TabOrder = 1
                          dmkEditCB = EdPQ
                          UpdType = utYes
                          LocF7SQLMasc = '$#'
                          LocF7PreDefProc = f7pNone
                        end
                      end
                      object Panel10: TPanel
                        Left = 0
                        Top = 39
                        Width = 588
                        Height = 48
                        Align = alBottom
                        BevelOuter = bvNone
                        TabOrder = 1
                        object LaCI: TLabel
                          Left = 4
                          Top = 4
                          Width = 139
                          Height = 13
                          Caption = 'Cliente interno que consumiu:'
                        end
                        object EdCIOri: TdmkEditCB
                          Left = 4
                          Top = 20
                          Width = 65
                          Height = 21
                          Alignment = taRightJustify
                          TabOrder = 0
                          FormatType = dmktfInteger
                          MskType = fmtNone
                          DecimalSize = 0
                          LeftZeros = 0
                          NoEnterToTab = False
                          NoForceUppercase = False
                          ValMin = '-2147483647'
                          ForceNextYear = False
                          DataFormat = dmkdfShort
                          HoraFormat = dmkhfShort
                          Texto = '0'
                          UpdType = utYes
                          Obrigatorio = False
                          PermiteNulo = False
                          ValueVariant = 0
                          ValWarn = False
                          OnRedefinido = EdCIOriRedefinido
                          DBLookupComboBox = CBCIOri
                          IgnoraDBLookupComboBox = False
                          AutoSetIfOnlyOneReg = setregOnlyManual
                        end
                        object CBCIOri: TdmkDBLookupComboBox
                          Left = 72
                          Top = 20
                          Width = 505
                          Height = 21
                          KeyField = 'Codigo'
                          ListField = 'NOMECI'
                          ListSource = DsCIOri
                          TabOrder = 1
                          dmkEditCB = EdCIOri
                          UpdType = utYes
                          LocF7SQLMasc = '$#'
                          LocF7PreDefProc = f7pNone
                        end
                      end
                      object Panel12: TPanel
                        Left = 0
                        Top = 135
                        Width = 588
                        Height = 84
                        Align = alBottom
                        BevelOuter = bvNone
                        TabOrder = 3
                        object RGAgrupa: TRadioGroup
                          Left = 0
                          Top = 0
                          Width = 92
                          Height = 84
                          Align = alLeft
                          Caption = ' Agrupamentos: '
                          ItemIndex = 1
                          Items.Strings = (
                            '0'
                            '1'
                            '2')
                          TabOrder = 0
                        end
                        object RGOrdem1: TRadioGroup
                          Left = 92
                          Top = 0
                          Width = 92
                          Height = 84
                          Align = alLeft
                          Caption = ' Ordem 1: '
                          ItemIndex = 0
                          Items.Strings = (
                            'Cliente'
                            'Setor'
                            'Insumo'
                            'Fornecedor')
                          TabOrder = 1
                        end
                        object RGOrdem3: TRadioGroup
                          Left = 276
                          Top = 0
                          Width = 92
                          Height = 84
                          Align = alLeft
                          Caption = ' Ordem 3: '
                          ItemIndex = 2
                          Items.Strings = (
                            'Cliente'
                            'Setor'
                            'Insumo'
                            'Fornecedor')
                          TabOrder = 3
                        end
                        object RGOrdem2: TRadioGroup
                          Left = 184
                          Top = 0
                          Width = 92
                          Height = 84
                          Align = alLeft
                          Caption = ' Ordem 2: '
                          ItemIndex = 1
                          Items.Strings = (
                            'Cliente'
                            'Setor'
                            'Insumo'
                            'Fornecedor')
                          TabOrder = 2
                        end
                        object ProgressBar1: TProgressBar
                          Left = 376
                          Top = 12
                          Width = 205
                          Height = 17
                          TabOrder = 4
                        end
                        object PB2: TProgressBar
                          Left = 376
                          Top = 36
                          Width = 205
                          Height = 17
                          TabOrder = 5
                        end
                        object PB3: TProgressBar
                          Left = 376
                          Top = 60
                          Width = 205
                          Height = 17
                          TabOrder = 6
                        end
                      end
                      object Panel24: TPanel
                        Left = 0
                        Top = 219
                        Width = 588
                        Height = 25
                        Align = alBottom
                        BevelOuter = bvNone
                        TabOrder = 4
                        object CkGrade: TCheckBox
                          Left = 4
                          Top = 4
                          Width = 57
                          Height = 17
                          Caption = 'Grade.'
                          Checked = True
                          Enabled = False
                          State = cbChecked
                          TabOrder = 0
                        end
                      end
                    end
                  end
                end
              end
              object Panel13: TPanel
                Left = 0
                Top = 0
                Width = 837
                Height = 244
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 1
                object CGTipoCad: TdmkCheckGroup
                  Left = 8
                  Top = 56
                  Width = 561
                  Height = 125
                  Caption = ' Tipo de Cadastro: '
                  Columns = 3
                  Items.Strings = (
                    'FmPrincipal.VAR_TipoCadPQ')
                  TabOrder = 0
                  UpdType = utYes
                  Value = 0
                  OldValor = 0
                end
                object RG00OrigemInfo: TRadioGroup
                  Left = 8
                  Top = 184
                  Width = 561
                  Height = 56
                  Caption = ' Origem das informa'#231#245'es: '
                  Columns = 2
                  ItemIndex = 0
                  Items.Strings = (
                    'Tabela de insumo por cliente'
                    'Tabela de entrada por nota')
                  TabOrder = 1
                end
                object BtTodosTipCad: TBitBtn
                  Tag = 127
                  Left = 581
                  Top = 65
                  Width = 114
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Todos'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                  OnClick = BtTodosTipCadClick
                end
                object BtNenhumTipCad: TBitBtn
                  Tag = 128
                  Left = 581
                  Top = 108
                  Width = 114
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Nenhum'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 3
                  OnClick = BtNenhumTipCadClick
                end
                object RGPositivo1: TRadioGroup
                  Left = 8
                  Top = 0
                  Width = 561
                  Height = 53
                  Caption = ' Filtrar insumos pelo seu estoque em peso: '
                  Columns = 3
                  ItemIndex = 1
                  Items.Strings = (
                    'Todos'
                    'Negativos e positivos'
                    'Somente positivos')
                  TabOrder = 4
                end
                object RGAtivo: TRadioGroup
                  Left = 581
                  Top = 154
                  Width = 114
                  Height = 86
                  Margins.Left = 2
                  Margins.Top = 2
                  Margins.Right = 2
                  Margins.Bottom = 2
                  Caption = 'Ativo'
                  ItemIndex = 0
                  Items.Strings = (
                    'Todos'
                    'Apenas ativos'
                    'Apenas inativos')
                  TabOrder = 5
                end
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 658
    Width = 849
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 845
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 845
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 713
    Width = 849
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 703
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 701
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 22
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 28
    Top = 7
  end
  object QrFichas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT '
      'SerieFch, Ficha '
      'FROM _vsmovimp1_ '
      'ORDER BY SerieFch, Ficha ')
    Left = 144
    Top = 56
    object QrFichasSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrFichasFicha: TIntegerField
      FieldName = 'Ficha'
    end
  end
  object QrVSInn: TMySQLQuery
    Database = Dmod.MyDB
    Left = 192
    Top = 56
    object QrVSInnPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSInnPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSInnAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSInnValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
  object QrIMECs: TMySQLQuery
    Database = Dmod.MyDB
    Left = 240
    Top = 56
    object QrIMECsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
  end
  object QrVSMovImp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _vsmovimp1_ '
      'WHERE '
      '('
      '  (SdoVrtPeca >0) '
      '  OR '
      '  (Pecas = 0 AND SdoVrtPeso >0) '
      ')')
    Left = 320
    Top = 64
    object QrVSMovImpEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSMovImpGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSMovImpPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSMovImpPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSMovImpAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSMovImpAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSMovImpValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSMovImpSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovImpSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSMovImpSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVSMovImpLmbVrtPeca: TFloatField
      FieldName = 'LmbVrtPeca'
    end
    object QrVSMovImpLmbVrtPeso: TFloatField
      FieldName = 'LmbVrtPeso'
    end
    object QrVSMovImpLmbVrtArM2: TFloatField
      FieldName = 'LmbVrtArM2'
    end
    object QrVSMovImpGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrVSMovImpNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrVSMovImpPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSMovImpNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSMovImpTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSMovImpCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSMovImpStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrVSMovImpNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrVSMovImpNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSMovImpNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSMovImpNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
    end
    object QrVSMovImpDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSMovImpOrdGGX: TIntegerField
      FieldName = 'OrdGGX'
    end
    object QrVSMovImpOrdGGY: TIntegerField
      FieldName = 'OrdGGY'
    end
    object QrVSMovImpGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrVSMovImpNO_GGY: TWideStringField
      FieldName = 'NO_GGY'
      Size = 255
    end
    object QrVSMovImpPalStat: TIntegerField
      FieldName = 'PalStat'
    end
    object QrVSMovImpCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
    object QrVSMovImpCouNiv1: TIntegerField
      FieldName = 'CouNiv1'
    end
    object QrVSMovImpNO_CouNiv2: TWideStringField
      FieldName = 'NO_CouNiv2'
      Size = 60
    end
    object QrVSMovImpNO_CouNiv1: TWideStringField
      FieldName = 'NO_CouNiv1'
      Size = 60
    end
    object QrVSMovImpFatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSMovImpSdoInteiros: TFloatField
      FieldName = 'SdoInteiros'
    end
    object QrVSMovImpLmbInteiros: TFloatField
      FieldName = 'LmbInteiros'
    end
    object QrVSMovImpCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovImpIMEC: TIntegerField
      FieldName = 'IMEC'
    end
    object QrVSMovImpIMEI: TIntegerField
      FieldName = 'IMEI'
    end
    object QrVSMovImpMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSMovImpMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSMovImpNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 30
    end
    object QrVSMovImpNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 40
    end
    object QrVSMovImpSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSMovImpNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSMovImpFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSMovImpMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovImpStatPall: TIntegerField
      FieldName = 'StatPall'
    end
    object QrVSMovImpNO_StatPall: TWideStringField
      FieldName = 'NO_StatPall'
      Size = 60
    end
    object QrVSMovImpReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovImpStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
    end
    object QrVSMovImpNO_StqCenCad: TWideStringField
      FieldName = 'NO_StqCenCad'
      Size = 50
    end
    object QrVSMovImpStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrVSMovImpNO_LOC_CEN: TWideStringField
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrVSMovImpHistorico: TWideStringField
      FieldName = 'Historico'
      Size = 255
    end
    object QrVSMovImpVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSMovImpMulFornece: TIntegerField
      FieldName = 'MulFornece'
    end
    object QrVSMovImpClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVSMovImpID_UNQ: TWideStringField
      FieldName = 'ID_UNQ'
      Size = 60
    end
    object QrVSMovImpGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrVSMovImpNFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrVSMovImpNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrVSMovImpVSMulNFeCab: TIntegerField
      FieldName = 'VSMulNFeCab'
    end
    object QrVSMovImpMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSMovImpFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVSMovImpNO_ClientMO: TWideStringField
      FieldName = 'NO_ClientMO'
      Size = 100
    end
    object QrVSMovImpNO_FornecMO: TWideStringField
      FieldName = 'NO_FornecMO'
      Size = 100
    end
    object QrVSMovImpAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object Qr06_14: TMySQLQuery
    Database = Dmod.MyDB
    Left = 16
    Top = 140
    object Qr06_14MovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object Qr06_14Pecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object Qr06_15: TMySQLQuery
    Database = Dmod.MyDB
    Left = 68
    Top = 140
    object Qr06_15SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object Qr06_15SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object Qr06_15Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr06_15Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object Qr27_34: TMySQLQuery
    Database = Dmod.MyDB
    Left = 184
    Top = 336
    object Qr27_34Controle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr27_34MovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object Qr27_34Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr27_34SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object Qr27_34SrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object Qr27_34SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
  end
  object Qr27_35: TMySQLQuery
    Database = Dmod.MyDB
    Left = 236
    Top = 336
    object Qr27_35Controle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr27_35Pecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object Qr26_30: TMySQLQuery
    Database = Dmod.MyDB
    Left = 240
    Top = 288
    object Qr26_30Controle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr26_30Pecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object QrIMEI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 460
    Top = 72
    object QrIMEIPecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object Qr07_01: TMySQLQuery
    Database = Dmod.MyDB
    Left = 16
    Top = 188
    object Qr07_01Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object Qr08_01: TMySQLQuery
    Database = Dmod.MyDB
    Left = 16
    Top = 236
    object Qr08_01SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object Qr08_01SrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object Qr08_01SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object Qr08_01Pecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object Qr15_11: TMySQLQuery
    Database = Dmod.MyDB
    Left = 124
    Top = 320
    object Qr15_11SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object Qr15_11SrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object Qr15_11SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object Qr15_11Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object Qr15_11Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object Qr07_02: TMySQLQuery
    Database = Dmod.MyDB
    Left = 68
    Top = 188
    object Qr07_02MovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
  end
  object Qr25_28: TMySQLQuery
    Database = Dmod.MyDB
    Left = 184
    Top = 240
    object Qr25_28MovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
  end
  object Qr25_27: TMySQLQuery
    Database = Dmod.MyDB
    Left = 240
    Top = 240
    object Qr25_27SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object Qr25_27SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
  end
  object QrVmiPsq: TMySQLQuery
    Database = Dmod.MyDB
    Left = 392
    Top = 68
  end
  object Qr26_29: TMySQLQuery
    Database = Dmod.MyDB
    Left = 184
    Top = 288
    object Qr26_29SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object Qr26_29SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object Qr26_29Controle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr26_29Pecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object Qr11_07: TMySQLQuery
    Database = Dmod.MyDB
    Left = 16
    Top = 284
    object Qr11_07SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object Qr11_07SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
  end
  object Qr19_21: TMySQLQuery
    Database = Dmod.MyDB
    Left = 240
    Top = 140
    object Qr19_21Controle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr19_21Pecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object Qr19_20: TMySQLQuery
    Database = Dmod.MyDB
    Left = 184
    Top = 140
    object Qr19_20SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object Qr19_20SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
  end
  object Qr20_22: TMySQLQuery
    Database = Dmod.MyDB
    Left = 304
    Top = 140
    object Qr20_22MovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object Qr20_22Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object Qr33_54: TMySQLQuery
    Database = Dmod.MyDB
    Left = 184
    Top = 384
    object Qr33_54SrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object Qr33_54SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
  end
  object Qr33_55: TMySQLQuery
    Database = Dmod.MyDB
    Left = 236
    Top = 384
    object Qr33_55Controle: TIntegerField
      FieldName = 'Controle'
    end
    object Qr33_55Pecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object Qr14_01: TMySQLQuery
    Database = Dmod.MyDB
    Left = 16
    Top = 332
    object Qr14_01Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSum: TMySQLQuery
    Database = Dmod.MyDB
    Left = 496
    Top = 152
    object QrSumPecas: TFloatField
      FieldName = 'Pecas'
    end
  end
  object Qr24_01: TMySQLQuery
    Database = Dmod.MyDB
    Left = 184
    Top = 188
    object Qr24_01Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object frxEstq: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310190000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      '  if <VFR_ORD1> =  0 then GH1.Condition := '#39'frxDsEstq."NOMECI"'#39';'
      '  if <VFR_ORD1> =  1 then GH1.Condition := '#39'frxDsEstq."NOMESE"'#39';'
      '  if <VFR_ORD1> =  2 then GH1.Condition := '#39'frxDsEstq."NOMEPQ"'#39';'
      '  if <VFR_ORD1> =  3 then GH1.Condition := '#39'frxDsEstq."NOMEFO"'#39';'
      ''
      '  //'
      ''
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      '  if <VFR_ORD2> =  0 then GH2.Condition := '#39'frxDsEstq."NOMECI"'#39';'
      '  if <VFR_ORD2> =  1 then GH2.Condition := '#39'frxDsEstq."NOMESE"'#39';'
      '  if <VFR_ORD2> =  2 then GH2.Condition := '#39'frxDsEstq."NOMEPQ"'#39';'
      '  if <VFR_ORD2> =  3 then GH2.Condition := '#39'frxDsEstq."NOMEFO"'#39';'
      '  //'
      'end.')
    OnGetValue = frxEstqGetValue
    Left = 648
    Top = 72
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstq
        DataSetName = 'frxDsEstq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 563.149970000000100000
        Width = 971.339210000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050930
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 173.858370240000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574654250000000000
          Width = 971.338770630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 113.385734020000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setores:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 113.385734020000000000
          Width = 901.008040000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_SETORESTXT]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 0.928880000000000000
          Top = 98.267604250000000000
          Width = 966.079160000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_AMOSTRAS]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 143.621974020000000000
          Width = 355.275556380000000000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188971500000000000
          Top = 158.740094020000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897691500000000000
          Top = 158.740094020000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 83.149506220000000000
          Width = 63.779530000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto:')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 83.149506220000000000
          Width = 901.448980000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PQNOME]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Width = 971.339210000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 971.339210000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 661.417750000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'INSUMOS DISPON'#205'VEIS E APLICADOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 812.598950000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472479999999990000
          Width = 971.339210000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_DATA_ESTQ]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 143.622140000000000000
          Width = 52.913385830000000000
          Height = 30.236230240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'd. CI')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 158.740260000000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 680.315400000000000000
          Top = 158.740260000000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 771.024120000000000000
          Top = 158.740260000000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 861.732840000000000000
          Top = 158.740260000000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 143.622140000000000000
          Width = 181.417381420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Insumo em estoque dispon'#237'vel')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 143.622140000000000000
          Width = 181.417381420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Insumo aplicado em processos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 771.024120000000000000
          Top = 143.622140000000000000
          Width = 181.417381420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Total dispon'#237'vel + processo')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.000000000000000000
        Top = 472.441250000000000000
        Width = 971.339210000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527559054998000
          Width = 381.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."PclPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897901420000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."PclValor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606963150000000000
          Top = 3.779532440000026000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."uVSPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 680.315624570000000000
          Top = 3.779532440000026000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."uVSValor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 771.024403150000000000
          Top = 3.779532440000026000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."TotPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 861.733064570000000000
          Top = 3.779532440000026000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."TotValor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 336.378170000000000000
        Width = 971.339210000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEstq
        DataSetName = 'frxDsEstq'
        RowCount = 0
        object MeNomePQ: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Width = 302.362170550000000000
          Height = 15.118110240000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstq."NOMEPQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePeso: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188976380000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'PclPeso'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."PclPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'PclValor'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."PclValor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MePQ: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'PQ'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstq."PQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeCodProprio: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'CodProprio'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstq."CodProprio"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'uVSPeso'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."uVSPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 680.315321890000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."uVSValor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 771.024120000000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'TotPeso'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."TotPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 861.732761890000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'TotValor'
          DataSet = frxDsEstq
          DataSetName = 'frxDsEstq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstq."TotValor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 423.307360000000000000
        Width = 971.339210000000000000
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 3.779530000000022000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 54.456710000000000000
          Top = 3.779530000000022000
          Width = 353.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."PclPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897901420000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."PclValor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606963150000000000
          Top = 3.779532440000026000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."uVSPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 680.315624570000000000
          Top = 3.779532440000026000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."uVSValor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 771.024403150000000000
          Top = 3.779532440000026000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."TotPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 861.733064570000000000
          Top = 3.779532440000026000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."TotValor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 253.228510000000000000
        Width = 971.339210000000000000
        Condition = 'frxDsEstq."NOMECI"'
        object Memo09: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 294.803340000000000000
        Width = 971.339210000000000000
        Condition = 'frxDsEstq."NOMEPQ"'
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 22.000000000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 374.173470000000000000
        Width = 971.339210000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527560000020000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 54.220470000000000000
          Top = 3.779527560000020000
          Width = 353.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188956850000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."PclPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."PclValor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."uVSPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 680.315341420000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."uVSValor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 771.024120000000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."TotPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 861.732781420000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstq."TotValor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsEstq: TfrxDBDataset
    UserName = 'frxDsEstq'
    CloseDataSource = False
    FieldAliases.Strings = (
      'IQ=IQ'
      'NOMEFO=NOMEFO'
      'NOMECI=NOMECI'
      'Setor=Setor'
      'NOMESE=NOMESE'
      'PQ=PQ'
      'NOMEPQ=NOMEPQ'
      'CtrlPqCli=CtrlPqCli'
      'CI=CI'
      'CodProprio=CodProprio'
      'Empresa=Empresa'
      'PclPeso=PclPeso'
      'PclValor=PclValor'
      'uVSPeso=uVSPeso'
      'uVsValor=uVsValor'
      'TotPeso=TotPeso'
      'TotValor=TotValor')
    DataSet = QrEstq
    BCDToCurrency = False
    DataSetOptions = []
    Left = 364
    Top = 256
  end
  object DsEstq: TDataSource
    DataSet = QrEstq
    Left = 592
    Top = 188
  end
  object QrEstq: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT IQ, NOMEFO, NOMECI, Setor, NOMESE, PQ, '
      'NOMEPQ, CtrlPqCli, CI, CodProprio, Empresa,  '
      'SUM(PclPeso) PclPeso, SUM(PclValor) PclValor, '
      'SUM(uVSPeso) uVSPeso, SUM(uVsValor) uVsValor,'
      'SUM(PclPeso+uVSPeso) TotPeso, '
      'SUM(PclValor+uVSValor) TotValor '
      'FROM _estq_vs_pq_'
      'GROUP BY CI, Empresa, NOMEPQ')
    Left = 364
    Top = 208
    object QrEstqIQ: TIntegerField
      FieldName = 'IQ'
    end
    object QrEstqNOMEFO: TWideStringField
      FieldName = 'NOMEFO'
      Size = 100
    end
    object QrEstqNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrEstqSetor: TIntegerField
      FieldName = 'Setor'
    end
    object QrEstqNOMESE: TWideStringField
      FieldName = 'NOMESE'
    end
    object QrEstqPQ: TIntegerField
      FieldName = 'PQ'
    end
    object QrEstqNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrEstqCtrlPqCli: TIntegerField
      FieldName = 'CtrlPqCli'
    end
    object QrEstqCI: TIntegerField
      FieldName = 'CI'
    end
    object QrEstqCodProprio: TWideStringField
      FieldName = 'CodProprio'
    end
    object QrEstqEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEstqPclPeso: TFloatField
      FieldName = 'PclPeso'
    end
    object QrEstqPclValor: TFloatField
      FieldName = 'PclValor'
    end
    object QrEstquVSPeso: TFloatField
      FieldName = 'uVSPeso'
    end
    object QrEstquVsValor: TFloatField
      FieldName = 'uVsValor'
    end
    object QrEstqTotPeso: TFloatField
      FieldName = 'TotPeso'
    end
    object QrEstqTotValor: TFloatField
      FieldName = 'TotValor'
    end
  end
  object QrCIOri: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECI'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 616
    Top = 536
    object QrCIOriCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCIOriNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
  end
  object QrPQ: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM pq '
      'WHERE Ativo NOT IN (0,3)'
      'AND Codigo > 0'
      'ORDER BY Nome')
    Left = 656
    Top = 32
    object QrPQCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPQNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCIOri: TDataSource
    DataSet = QrCIOri
    Left = 616
    Top = 584
  end
  object DsPQ: TDataSource
    DataSet = QrPQ
    Left = 684
    Top = 32
  end
  object DsListaSetores: TDataSource
    DataSet = QrListaSetores
    Left = 748
    Top = 284
  end
  object QrListaSetores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT se.Codigo, se.Nome '
      'FROM listasetores se'
      'ORDER BY se.Nome')
    Left = 748
    Top = 236
    object QrListaSetoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaSetoresNome: TWideStringField
      FieldName = 'Nome'
    end
  end
  object frxEstqNFs: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310190000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      
        '  if <VFR_ORD1> =  0 then GH1.Condition := '#39'frxDsEstqNFs."NOMECI' +
        '"'#39';'
      
        '  if <VFR_ORD1> =  1 then GH1.Condition := '#39'frxDsEstqNFs."NOMESE' +
        '"'#39';'
      
        '  if <VFR_ORD1> =  2 then GH1.Condition := '#39'frxDsEstqNFs."NOMEPQ' +
        '"'#39';'
      
        '  if <VFR_ORD1> =  3 then GH1.Condition := '#39'frxDsEstqNFs."NOMEFO' +
        '"'#39';'
      ''
      '  //'
      ''
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      
        '  if <VFR_ORD2> =  0 then GH2.Condition := '#39'frxDsEstqNFs."NOMECI' +
        '"'#39';'
      
        '  if <VFR_ORD2> =  1 then GH2.Condition := '#39'frxDsEstqNFs."NOMESE' +
        '"'#39';'
      
        '  if <VFR_ORD2> =  2 then GH2.Condition := '#39'frxDsEstqNFs."NOMEPQ' +
        '"'#39';'
      
        '  if <VFR_ORD2> =  3 then GH2.Condition := '#39'frxDsEstqNFs."NOMEFO' +
        '"'#39';'
      '    '
      'end.')
    OnGetValue = frxEstqGetValue
    Left = 480
    Top = 224
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqNFs
        DataSetName = 'frxDsEstqNFs'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 548.031849999999900000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050930
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 158.740250240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574654250000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_CINOME]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 113.385734020000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Setores:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 113.385734020000000000
          Width = 613.763760000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_SETORESTXT]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 0.928880000000000000
          Top = 98.267604250000000000
          Width = 678.834880000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_AMOSTRAS]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 143.621974020000000000
          Width = 355.275556380000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188971500000000000
          Top = 143.621974020000000000
          Width = 90.708661420000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897691500000000000
          Top = 143.621974020000000000
          Width = 90.708661420000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606411500000000000
          Top = 143.621974020000000000
          Width = 90.708661420000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Top = 83.149506220000000000
          Width = 63.779530000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Produto:')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 66.000000000000000000
          Top = 83.149506220000000000
          Width = 614.204700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VAR_PQNOME]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE DE USO E CONSUMO DE NFs')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472479999999990000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_DATA_ESTQ]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 143.622140000000000000
          Width = 52.913385830000000000
          Height = 15.118110236220470000
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'd. CI')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.000000000000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527559054998000
          Width = 381.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqNFs."SdoPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897901420000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqNFs."SdoValr">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606562840000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstqNFs."SdoPeso">)=0,0,(SUM(<frxDsEstqNFs."SdoVa' +
              'lr">) / SUM(<frxDsEstqNFs."SdoPeso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEstqNFs
        DataSetName = 'frxDsEstqNFs'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Width = 302.362170550000000000
          Height = 15.118110240000000000
          DataField = 'NOMEPQ'
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqNFs."NOMEPQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188976380000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'SdoPeso'
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqNFs."SdoPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'SdoValr'
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqNFs."SdoValr"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606279690000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'Custo'
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqNFs."Custo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEstqNFs."Insumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'CodProprio'
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqNFs."CodProprio"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 26.456710000000000000
          Top = 3.779530000000022000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 54.456710000000000000
          Top = 3.779530000000022000
          Width = 353.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqNFs."SdoPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897901420000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqNFs."SdoValr">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606562840000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstqNFs."SdoPeso">)=0,0,(SUM(<frxDsEstqNFs."SdoVa' +
              'lr">) / SUM(<frxDsEstqNFs."SdoPeso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstqNFs."NOMECI"'
        object Memo09: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 279.685220000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsEstqNFs."NOMEPQ"'
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 22.000000000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527560000020000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 54.220470000000000000
          Top = 3.779527560000020000
          Width = 353.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VFR_AGR2NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188956850000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqNFs."SdoPeso">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsEstqNFs."SdoValr">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606279690000000000
          Top = 3.779527560000020000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsEstqNFs."SdoPeso">)=0,0,(SUM(<frxDsEstqNFs."SdoVa' +
              'lr">) / SUM(<frxDsEstqNFs."SdoPeso">)))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsEstqNFs: TfrxDBDataset
    UserName = 'frxDsEstqNFs'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEFO=NOMEFO'
      'NOMECI=NOMECI'
      'NOMEPQ=NOMEPQ'
      'NOMESE=NOMESE'
      'CodProprio=CodProprio'
      'NF=NF'
      'prod_CFOP=prod_CFOP'
      'DataX=DataX'
      'CliOrig=CliOrig'
      'Insumo=Insumo'
      'Peso=Peso'
      'Valor=Valor'
      'SdoPeso=SdoPeso'
      'SdoValr=SdoValr'
      'Custo=Custo')
    DataSet = QrEstqNFs
    BCDToCurrency = False
    DataSetOptions = []
    Left = 480
    Top = 272
  end
  object QrEstqNFs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ind.Tipo=0, ind.RazaoSocial, ind.Nome) NOMEFO,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECI, '
      'pq_.Nome NOMEPQ, lse.Nome NOMESE, pqc.CodProprio,'
      'pqe.NF, pqi.prod_CFOP,'
      'pqx.DataX, pqx.CliOrig, pqx.Insumo, pqx.Peso, pqx.Valor,'
      'pqx.SdoPeso, pqx.SdoValr, (pqx.Valor / pqx.Peso) Custo'
      'FROM pqx pqx'
      'LEFT JOIN pq pq_ ON pq_.Codigo=pqx.Insumo'
      'LEFT JOIN pqeits pqi'
      '  ON pqi.Codigo=pqx.OrigemCodi'
      '  AND pqi.Controle=pqx.OrigemCtrl'
      'LEFT JOIN pqe ON pqe.Codigo=pqi.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=pqx.CliOrig'
      'LEFT JOIN entidades ind ON ind.Codigo=pq_.IQ'
      'LEFT JOIN listasetores lse ON lse.Codigo=pq_.Setor'
      'LEFT JOIN pqcli pqc ON pqc.PQ=pqx.Insumo AND pqc.CI=pqx.CliOrig'
      'WHERE SdoPeso>0'
      'AND pqx.Tipo=10'
      'ORDER BY pq_.Codigo')
    Left = 480
    Top = 316
    object QrEstqNFsNOMEFO: TWideStringField
      FieldName = 'NOMEFO'
      Size = 100
    end
    object QrEstqNFsNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
    object QrEstqNFsNOMEPQ: TWideStringField
      FieldName = 'NOMEPQ'
      Size = 50
    end
    object QrEstqNFsNOMESE: TWideStringField
      FieldName = 'NOMESE'
    end
    object QrEstqNFsCodProprio: TWideStringField
      FieldName = 'CodProprio'
    end
    object QrEstqNFsNF: TIntegerField
      FieldName = 'NF'
    end
    object QrEstqNFsprod_CFOP: TWideStringField
      FieldName = 'prod_CFOP'
      Size = 4
    end
    object QrEstqNFsDataX: TDateField
      FieldName = 'DataX'
    end
    object QrEstqNFsCliOrig: TIntegerField
      FieldName = 'CliOrig'
    end
    object QrEstqNFsInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrEstqNFsPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrEstqNFsValor: TFloatField
      FieldName = 'Valor'
    end
    object QrEstqNFsSdoPeso: TFloatField
      FieldName = 'SdoPeso'
    end
    object QrEstqNFsSdoValr: TFloatField
      FieldName = 'SdoValr'
    end
    object QrEstqNFsCusto: TFloatField
      FieldName = 'Custo'
    end
  end
  object QrCIDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECI'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY Nome'
      '')
    Left = 664
    Top = 536
    object QrCIDstCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCIDstNOMECI: TWideStringField
      FieldName = 'NOMECI'
      Size = 100
    end
  end
  object DsCIDst: TDataSource
    DataSet = QrCIDst
    Left = 664
    Top = 584
  end
  object PMImprime: TPopupMenu
    Left = 308
    Top = 720
    object Estoqueagregado1: TMenuItem
      Caption = 'Estoque agregado'
      OnClick = Estoqueagregado1Click
    end
    object UsadoporclientedeMO1: TMenuItem
      Caption = 'Usado por cliente de M.O.'
      OnClick = UsadoporclientedeMO1Click
    end
  end
  object frxUsoCliMO: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.458486319400000000
    ReportOptions.LastChange = 41844.582013310200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '(*  '
      
        '  if <VFR_AGR> > 0 then GH1.Visible := True else GH1.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 0 then GF1.Visible := True else GF1.Visible := ' +
        'False;'
      '  //'
      
        '  if <VFR_ORD1> =  0 then GH1.Condition := '#39'frxDsEstqNFs."NOMECI' +
        '"'#39';'
      
        '  if <VFR_ORD1> =  1 then GH1.Condition := '#39'frxDsEstqNFs."NOMESE' +
        '"'#39';'
      
        '  if <VFR_ORD1> =  2 then GH1.Condition := '#39'frxDsEstqNFs."NOMEPQ' +
        '"'#39';'
      
        '  if <VFR_ORD1> =  3 then GH1.Condition := '#39'frxDsEstqNFs."NOMEFO' +
        '"'#39';'
      ''
      '  //'
      ''
      
        '  if <VFR_AGR> > 1 then GH2.Visible := True else GH2.Visible := ' +
        'False;'
      
        '  if <VFR_AGR> > 1 then GF2.Visible := True else GF2.Visible := ' +
        'False;'
      '  //'
      
        '  if <VFR_ORD2> =  0 then GH2.Condition := '#39'frxDsEstqNFs."NOMECI' +
        '"'#39';'
      
        '  if <VFR_ORD2> =  1 then GH2.Condition := '#39'frxDsEstqNFs."NOMESE' +
        '"'#39';'
      
        '  if <VFR_ORD2> =  2 then GH2.Condition := '#39'frxDsEstqNFs."NOMEPQ' +
        '"'#39';'
      
        '  if <VFR_ORD2> =  3 then GH2.Condition := '#39'frxDsEstqNFs."NOMEFO' +
        '"'#39';'
      '*)        '
      'end.')
    OnGetValue = frxEstqGetValue
    Left = 544
    Top = 224
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsUsoCliMO
        DataSetName = 'frxDsUsoCliMO'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 14.314470000000000000
        Top = 377.953000000000000000
        Width = 680.315400000000000000
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 507.763760000000000000
          Top = 0.094000000000050930
          Width = 172.000000000000000000
          Height = 14.220470000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 64.251844019999990000
          Width = 355.275556380000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Insumo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188971500000000000
          Top = 64.251844019999990000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897691500000000000
          Top = 64.251844019999990000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606411500000000000
          Top = 64.251844019999990000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Custo kg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 18.897650000000000000
          Width = 642.520100000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Insumos Usados nos Couros em Estoque por Cliente M.O.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 41.574830000000000000
          Width = 147.401670000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 521.575140000000000000
          Top = 41.574830000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 154.960730000000000000
          Top = 41.574830000000000000
          Width = 366.614410000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_DATA_ESTQ]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Top = 64.252010000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsEstqNFs
          DataSetName = 'frxDsEstqNFs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'd. CI')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterFooter1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.000000000000000000
        Top = 287.244280000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 26.220470000000000000
          Top = 3.779527559054998000
          Width = 381.543290000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUsoCliMO."PesoLote">,Band4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897901420000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUsoCliMO."ValorLote">,Band4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606562840000000000
          Top = 3.779530000000022000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsUsoCliMO."PesoLote">,Band4)=0,0,SUM(<frxDsUsoCliM' +
              'O."ValorLote">,Band4)/SUM(<frxDsUsoCliMO."PesoLote">,Band4))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Band4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsUsoCliMO
        DataSetName = 'frxDsUsoCliMO'
        RowCount = 0
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913385830000000000
          Width = 302.362170550000000000
          Height = 15.118110240000000000
          DataField = 'NOME_PQ'
          DataSet = frxDsUsoCliMO
          DataSetName = 'frxDsUsoCliMO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsUsoCliMO."NOME_PQ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 408.188976380000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'PesoLote'
          DataSet = frxDsUsoCliMO
          DataSetName = 'frxDsUsoCliMO'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUsoCliMO."PesoLote"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897618270000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'ValorLote'
          DataSet = frxDsUsoCliMO
          DataSetName = 'frxDsUsoCliMO'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUsoCliMO."ValorLote"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606279690000000000
          Width = 90.708661420000000000
          Height = 15.118110240000000000
          DataField = 'CuMedKg'
          DataSet = frxDsUsoCliMO
          DataSetName = 'frxDsUsoCliMO'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsUsoCliMO."CuMedKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Insumo'
          DataSet = frxDsUsoCliMO
          DataSetName = 'frxDsUsoCliMO'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsUsoCliMO."Insumo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 355.275820000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'CodProprio'
          DataSet = frxDsUsoCliMO
          DataSetName = 'frxDsUsoCliMO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsUsoCliMO."CodProprio"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.000000000000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779529999999994000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 35.559060000000000000
          Top = 3.779529999999994000
          Width = 372.440940000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsUsoCliMO."Estq_ClientMO"] - [frxDsUsoCliMO."NO_ClientMO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 3.779529999999994000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUsoCliMO."PesoLote">,Band4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897901420000000000
          Top = 3.779529999999994000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsUsoCliMO."ValorLote">,Band4)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606562840000000000
          Top = 3.779529999999994000
          Width = 90.708661420000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.0000;-#,###,##0.0000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsUsoCliMO."PesoLote">,Band4)=0,0,SUM(<frxDsUsoCliM' +
              'O."ValorLote">,Band4)/SUM(<frxDsUsoCliMO."PesoLote">,Band4))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 158.740260000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsUsoCliMO."Estq_ClientMO"'
        object Memo09: TfrxMemoView
          AllowVectorExport = True
          Left = 2.220470000000000000
          Width = 704.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsUsoCliMO."Estq_ClientMO"] - [frxDsUsoCliMO."NO_ClientMO"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
    end
  end
  object QrUsoCliMO: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT upv.Estq_ClientMO, upv.Estq_FornecMO,   '
      'upv.Insumo, SUM(upv.PesoLote) PesoLote,  '
      'SUM(upv.ValorLote) ValorLote, pq_.Nome NOME_PQ,'
      'IF(cmo.Tipo=0, cmo.RazaoSocial, cmo.Nome) NO_ClientMO,'
      'IF(SUM(upv.PesoLote)=0, 0, '
      '  SUM(upv.ValorLote)/SUM(upv.PesoLote)) CuMedKg '
      'FROM _vs_uso_pq_vmi_ upv  '
      'LEFT JOIN bluederm_fw.pq pq_ ON pq_.Codigo=upv.Insumo  '
      
        'LEFT JOIN bluederm_fw.entidades cmo ON cmo.Codigo=upv.Estq_Clien' +
        'tMO  '
      'GROUP BY Estq_ClientMO, Estq_FornecMO, Insumo  '
      'ORDER BY Estq_ClientMO, Estq_FornecMO, NOME_PQ ')
    Left = 548
    Top = 312
    object QrUsoCliMOEstq_ClientMO: TIntegerField
      FieldName = 'Estq_ClientMO'
    end
    object QrUsoCliMOEstq_FornecMO: TIntegerField
      FieldName = 'Estq_FornecMO'
    end
    object QrUsoCliMOInsumo: TIntegerField
      FieldName = 'Insumo'
    end
    object QrUsoCliMOPesoLote: TFloatField
      FieldName = 'PesoLote'
    end
    object QrUsoCliMOValorLote: TFloatField
      FieldName = 'ValorLote'
    end
    object QrUsoCliMONOME_PQ: TWideStringField
      FieldName = 'NOME_PQ'
      Size = 50
    end
    object QrUsoCliMONO_ClientMO: TWideStringField
      FieldName = 'NO_ClientMO'
      Size = 100
    end
    object QrUsoCliMOCuMedKg: TFloatField
      FieldName = 'CuMedKg'
    end
    object QrUsoCliMOCodProprio: TWideStringField
      FieldName = 'CodProprio'
    end
  end
  object frxDsUsoCliMO: TfrxDBDataset
    UserName = 'frxDsUsoCliMO'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Estq_ClientMO=Estq_ClientMO'
      'Estq_FornecMO=Estq_FornecMO'
      'Insumo=Insumo'
      'PesoLote=PesoLote'
      'ValorLote=ValorLote'
      'NOME_PQ=NOME_PQ'
      'NO_ClientMO=NO_ClientMO'
      'CuMedKg=CuMedKg'
      'CodProprio=CodProprio')
    DataSet = QrUsoCliMO
    BCDToCurrency = False
    DataSetOptions = []
    Left = 548
    Top = 272
  end
end
