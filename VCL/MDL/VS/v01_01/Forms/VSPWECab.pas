unit VSPWECab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkEditCB, dmkDBLookupComboBox, Vcl.ComCtrls,
  dmkEditDateTimePicker, Variants, frxClass, frxDBSet, UnMyObjects, UnGrl_Geral,
  UnProjGroup_Consts, dmkDBGridZTO, UnGrl_Consts, UnVS_EFD_ICMS_IPI, UnAppEnums,
  UnProjGroup_Vars;

type
  TFmVSPWECab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita1: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdit0: TdmkDBEdit;
    QrVSPWECab: TmySQLQuery;
    DsVSPWECab: TDataSource;
    QrVSPWEAtu: TmySQLQuery;
    DsVSPWEAtu: TDataSource;
    PMOri: TPopupMenu;
    ItsIncluiOri: TMenuItem;
    ItsExcluiOriIMEI: TMenuItem;
    ItsAlteraOri: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtOri: TBitBtn;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrVSPWECabCodigo: TIntegerField;
    EdEmpresa: TdmkEditCB;
    Label8: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    Label53: TLabel;
    TPDtHrAberto: TdmkEditDateTimePicker;
    EdDtHrAberto: TdmkEdit;
    EdMovimCod: TdmkEdit;
    Label4: TLabel;
    QrVSPWECabEmpresa: TIntegerField;
    QrVSPWECabDtHrAberto: TDateTimeField;
    QrVSPWECabNO_EMPRESA: TWideStringField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label11: TLabel;
    DBEdit3: TDBEdit;
    QrVSPWECabNome: TWideStringField;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    QrVSPWEOriIMEI: TmySQLQuery;
    DsVSPWEOriIMEI: TDataSource;
    QrVSPWECabMovimCod: TIntegerField;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBEdita2: TGroupBox;
    Label3: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdCustoMOTot: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label12: TLabel;
    EdObserv: TdmkEdit;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label19: TLabel;
    EdMovimTwn: TdmkEdit;
    N1: TMenuItem;
    CabLibera1: TMenuItem;
    DBEdit12: TDBEdit;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit13: TDBEdit;
    QrVSPWECabLk: TIntegerField;
    QrVSPWECabDataCad: TDateField;
    QrVSPWECabDataAlt: TDateField;
    QrVSPWECabUserCad: TIntegerField;
    QrVSPWECabUserAlt: TIntegerField;
    QrVSPWECabAlterWeb: TSmallintField;
    QrVSPWECabAtivo: TSmallintField;
    QrVSPWECabPecasMan: TFloatField;
    QrVSPWECabAreaManM2: TFloatField;
    QrVSPWECabAreaManP2: TFloatField;
    RGTipoArea: TdmkRadioGroup;
    QrVSPWECabTipoArea: TSmallintField;
    Label22: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label23: TLabel;
    QrVSPWECabNO_TIPO: TWideStringField;
    QrVSPWECabNO_DtHrFimOpe: TWideStringField;
    Label24: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    Label26: TLabel;
    DBEdit18: TDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    Label32: TLabel;
    DBEdit25: TDBEdit;
    Label33: TLabel;
    QrVSPWECabNO_DtHrLibOpe: TWideStringField;
    CabReeditar1: TMenuItem;
    DBEdit26: TDBEdit;
    Label34: TLabel;
    QrVSPWECabNO_DtHrCfgOpe: TWideStringField;
    Label35: TLabel;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    PMNumero: TPopupMenu;
    PeloCdigo1: TMenuItem;
    PeloIMEC1: TMenuItem;
    PeloIMEI1: TMenuItem;
    PMImprime: TPopupMenu;
    IMEIArtigodeRibeiragerado1: TMenuItem;
    N2: TMenuItem;
    Outrasimpresses1: TMenuItem;
    QrVSPWECabCacCod: TIntegerField;
    QrVSPWECabGraGruX: TIntegerField;
    QrVSPWECabCustoManMOKg: TFloatField;
    QrVSPWECabCustoManMOTot: TFloatField;
    QrVSPWECabValorManMP: TFloatField;
    QrVSPWECabValorManT: TFloatField;
    QrVSPWECabDtHrLibOpe: TDateTimeField;
    QrVSPWECabDtHrCfgOpe: TDateTimeField;
    QrVSPWECabDtHrFimOpe: TDateTimeField;
    QrVSPWECabPecasSrc: TFloatField;
    QrVSPWECabAreaSrcM2: TFloatField;
    QrVSPWECabAreaSrcP2: TFloatField;
    QrVSPWECabPecasDst: TFloatField;
    QrVSPWECabAreaDstM2: TFloatField;
    QrVSPWECabAreaDstP2: TFloatField;
    QrVSPWECabPecasSdo: TFloatField;
    QrVSPWECabAreaSdoM2: TFloatField;
    QrVSPWECabAreaSdoP2: TFloatField;
    QrVSPWEDst: TmySQLQuery;
    DsVSPWEDst: TDataSource;
    Splitter1: TSplitter;
    PMDst: TPopupMenu;
    ItsIncluiDst: TMenuItem;
    ItsAlteraDst: TMenuItem;
    ItsExcluiDst: TMenuItem;
    BtDst: TBitBtn;
    N3: TMenuItem;
    AtualizaestoqueEmProcesso1: TMenuItem;
    QrTwn: TmySQLQuery;
    QrTwnControle: TIntegerField;
    QrVSPWEBxa: TmySQLQuery;
    DsVSPWEBxa: TDataSource;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label36: TLabel;
    DBEdit27: TDBEdit;
    Label37: TLabel;
    DBEdit28: TDBEdit;
    Label38: TLabel;
    DBEdit29: TDBEdit;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    GroupBox5: TGroupBox;
    Panel9: TPanel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    QrVSPWECabPesoKgSrc: TFloatField;
    QrVSPWECabPesoKgMan: TFloatField;
    QrVSPWECabPesoKgDst: TFloatField;
    QrVSPWECabPesoKgSdo: TFloatField;
    QrVSPWECabValorTMan: TFloatField;
    QrVSPWECabValorTSrc: TFloatField;
    QrVSPWECabValorTSdo: TFloatField;
    QrVSPWECabPecasINI: TFloatField;
    QrVSPWECabAreaINIM2: TFloatField;
    QrVSPWECabAreaINIP2: TFloatField;
    QrVSPWECabPesoKgINI: TFloatField;
    GroupBox6: TGroupBox;
    Panel10: TPanel;
    Label39: TLabel;
    Label43: TLabel;
    Label47: TLabel;
    DBEdit30: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit38: TDBEdit;
    QrVSPWECabPesoKgBxa: TFloatField;
    QrVSPWECabPecasBxa: TFloatField;
    QrVSPWECabAreaBxaM2: TFloatField;
    QrVSPWECabAreaBxaP2: TFloatField;
    QrVSPWECabValorTBxa: TFloatField;
    Definiodequantidadesmanualmente1: TMenuItem;
    PCPWEOri: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DGDadosOri: TDBGrid;
    QrVSPWEOriPallet: TmySQLQuery;
    DsVSPWEOriPallet: TDataSource;
    DBGrid2: TDBGrid;
    ItsExcluiOriPallet: TMenuItem;
    QrForcados: TmySQLQuery;
    DsForcados: TDataSource;
    GroupBox7: TGroupBox;
    DBGrid3: TDBGrid;
    porPallet1: TMenuItem;
    porIMEI1: TMenuItem;
    Parcial1: TMenuItem;
    otal1: TMenuItem;
    Parcial2: TMenuItem;
    otal2: TMenuItem;
    GBVSPedIts: TGroupBox;
    LaPedItsLib: TLabel;
    EdPedItsLib: TdmkEditCB;
    CBPedItsLib: TdmkDBLookupComboBox;
    QrVSPedIts: TmySQLQuery;
    QrVSPedItsNO_PRD_TAM_COR: TWideStringField;
    QrVSPedItsControle: TIntegerField;
    DsVSPedIts: TDataSource;
    EdCustoMOM2: TdmkEdit;
    Label48: TLabel;
    InformaNmerodaRME1: TMenuItem;
    InformaNmerodaRME2: TMenuItem;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    DsStqCenLoc: TDataSource;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    EdReqMovEstq: TdmkEdit;
    Label50: TLabel;
    Panel11: TPanel;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label25: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    Label18: TLabel;
    DBEdit11: TDBEdit;
    Label51: TLabel;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    Label52: TLabel;
    DBEdit41: TDBEdit;
    Label54: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTIDADE: TWideStringField;
    DsCliente: TDataSource;
    QrVSPWECabCliente: TIntegerField;
    QrVSPWECabNO_Cliente: TWideStringField;
    Label55: TLabel;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    PMNovo: TPopupMenu;
    CorrigeFornecedoresdestinoapartirdestaoperao1: TMenuItem;
    CorrigirFornecedor1: TMenuItem;
    EdLPFMO: TdmkEdit;
    EdNFeRem: TdmkEdit;
    Label56: TLabel;
    QrVSPWECabNFeRem: TIntegerField;
    QrVSPWECabLPFMO: TWideStringField;
    Label58: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    DBEdit44: TDBEdit;
    OrdensdeProduoemAberto1: TMenuItem;
    QrVSPWECabTemIMEIMrt: TIntegerField;
    QrVSPWEAtuCodigo: TLargeintField;
    QrVSPWEAtuControle: TLargeintField;
    QrVSPWEAtuMovimCod: TLargeintField;
    QrVSPWEAtuMovimNiv: TLargeintField;
    QrVSPWEAtuMovimTwn: TLargeintField;
    QrVSPWEAtuEmpresa: TLargeintField;
    QrVSPWEAtuTerceiro: TLargeintField;
    QrVSPWEAtuCliVenda: TLargeintField;
    QrVSPWEAtuMovimID: TLargeintField;
    QrVSPWEAtuDataHora: TDateTimeField;
    QrVSPWEAtuPallet: TLargeintField;
    QrVSPWEAtuGraGruX: TLargeintField;
    QrVSPWEAtuPecas: TFloatField;
    QrVSPWEAtuPesoKg: TFloatField;
    QrVSPWEAtuAreaM2: TFloatField;
    QrVSPWEAtuAreaP2: TFloatField;
    QrVSPWEAtuValorT: TFloatField;
    QrVSPWEAtuSrcMovID: TLargeintField;
    QrVSPWEAtuSrcNivel1: TLargeintField;
    QrVSPWEAtuSrcNivel2: TLargeintField;
    QrVSPWEAtuSrcGGX: TLargeintField;
    QrVSPWEAtuSdoVrtPeca: TFloatField;
    QrVSPWEAtuSdoVrtPeso: TFloatField;
    QrVSPWEAtuSdoVrtArM2: TFloatField;
    QrVSPWEAtuObserv: TWideStringField;
    QrVSPWEAtuSerieFch: TLargeintField;
    QrVSPWEAtuFicha: TLargeintField;
    QrVSPWEAtuMisturou: TLargeintField;
    QrVSPWEAtuFornecMO: TLargeintField;
    QrVSPWEAtuCustoMOKg: TFloatField;
    QrVSPWEAtuCustoMOM2: TFloatField;
    QrVSPWEAtuCustoMOTot: TFloatField;
    QrVSPWEAtuValorMP: TFloatField;
    QrVSPWEAtuDstMovID: TLargeintField;
    QrVSPWEAtuDstNivel1: TLargeintField;
    QrVSPWEAtuDstNivel2: TLargeintField;
    QrVSPWEAtuDstGGX: TLargeintField;
    QrVSPWEAtuQtdGerPeca: TFloatField;
    QrVSPWEAtuQtdGerPeso: TFloatField;
    QrVSPWEAtuQtdGerArM2: TFloatField;
    QrVSPWEAtuQtdGerArP2: TFloatField;
    QrVSPWEAtuQtdAntPeca: TFloatField;
    QrVSPWEAtuQtdAntPeso: TFloatField;
    QrVSPWEAtuQtdAntArM2: TFloatField;
    QrVSPWEAtuQtdAntArP2: TFloatField;
    QrVSPWEAtuNotaMPAG: TFloatField;
    QrVSPWEAtuNO_PALLET: TWideStringField;
    QrVSPWEAtuNO_PRD_TAM_COR: TWideStringField;
    QrVSPWEAtuNO_TTW: TWideStringField;
    QrVSPWEAtuID_TTW: TLargeintField;
    QrVSPWEAtuNO_FORNECE: TWideStringField;
    QrVSPWEAtuReqMovEstq: TLargeintField;
    QrVSPWEAtuCUSTO_M2: TFloatField;
    QrVSPWEAtuCUSTO_P2: TFloatField;
    QrVSPWEAtuNO_LOC_CEN: TWideStringField;
    QrVSPWEAtuMarca: TWideStringField;
    QrVSPWEAtuPedItsLib: TLargeintField;
    QrVSPWEAtuStqCenLoc: TLargeintField;
    QrVSPWEAtuNO_FICHA: TWideStringField;
    QrVSPWEOriIMEICodigo: TLargeintField;
    QrVSPWEOriIMEIControle: TLargeintField;
    QrVSPWEOriIMEIMovimCod: TLargeintField;
    QrVSPWEOriIMEIMovimNiv: TLargeintField;
    QrVSPWEOriIMEIMovimTwn: TLargeintField;
    QrVSPWEOriIMEIEmpresa: TLargeintField;
    QrVSPWEOriIMEITerceiro: TLargeintField;
    QrVSPWEOriIMEICliVenda: TLargeintField;
    QrVSPWEOriIMEIMovimID: TLargeintField;
    QrVSPWEOriIMEIDataHora: TDateTimeField;
    QrVSPWEOriIMEIPallet: TLargeintField;
    QrVSPWEOriIMEIGraGruX: TLargeintField;
    QrVSPWEOriIMEIPecas: TFloatField;
    QrVSPWEOriIMEIPesoKg: TFloatField;
    QrVSPWEOriIMEIAreaM2: TFloatField;
    QrVSPWEOriIMEIAreaP2: TFloatField;
    QrVSPWEOriIMEIValorT: TFloatField;
    QrVSPWEOriIMEISrcMovID: TLargeintField;
    QrVSPWEOriIMEISrcNivel1: TLargeintField;
    QrVSPWEOriIMEISrcNivel2: TLargeintField;
    QrVSPWEOriIMEISrcGGX: TLargeintField;
    QrVSPWEOriIMEISdoVrtPeca: TFloatField;
    QrVSPWEOriIMEISdoVrtPeso: TFloatField;
    QrVSPWEOriIMEISdoVrtArM2: TFloatField;
    QrVSPWEOriIMEIObserv: TWideStringField;
    QrVSPWEOriIMEISerieFch: TLargeintField;
    QrVSPWEOriIMEIFicha: TLargeintField;
    QrVSPWEOriIMEIMisturou: TLargeintField;
    QrVSPWEOriIMEIFornecMO: TLargeintField;
    QrVSPWEOriIMEICustoMOKg: TFloatField;
    QrVSPWEOriIMEICustoMOM2: TFloatField;
    QrVSPWEOriIMEICustoMOTot: TFloatField;
    QrVSPWEOriIMEIValorMP: TFloatField;
    QrVSPWEOriIMEIDstMovID: TLargeintField;
    QrVSPWEOriIMEIDstNivel1: TLargeintField;
    QrVSPWEOriIMEIDstNivel2: TLargeintField;
    QrVSPWEOriIMEIDstGGX: TLargeintField;
    QrVSPWEOriIMEIQtdGerPeca: TFloatField;
    QrVSPWEOriIMEIQtdGerPeso: TFloatField;
    QrVSPWEOriIMEIQtdGerArM2: TFloatField;
    QrVSPWEOriIMEIQtdGerArP2: TFloatField;
    QrVSPWEOriIMEIQtdAntPeca: TFloatField;
    QrVSPWEOriIMEIQtdAntPeso: TFloatField;
    QrVSPWEOriIMEIQtdAntArM2: TFloatField;
    QrVSPWEOriIMEIQtdAntArP2: TFloatField;
    QrVSPWEOriIMEINotaMPAG: TFloatField;
    QrVSPWEOriIMEINO_PALLET: TWideStringField;
    QrVSPWEOriIMEINO_PRD_TAM_COR: TWideStringField;
    QrVSPWEOriIMEINO_TTW: TWideStringField;
    QrVSPWEOriIMEIID_TTW: TLargeintField;
    QrVSPWEOriIMEINO_FORNECE: TWideStringField;
    QrVSPWEOriIMEINO_SerieFch: TWideStringField;
    QrVSPWEOriIMEIReqMovEstq: TLargeintField;
    QrVSPWEOriPalletPallet: TLargeintField;
    QrVSPWEOriPalletGraGruX: TLargeintField;
    QrVSPWEOriPalletPecas: TFloatField;
    QrVSPWEOriPalletAreaM2: TFloatField;
    QrVSPWEOriPalletAreaP2: TFloatField;
    QrVSPWEOriPalletPesoKg: TFloatField;
    QrVSPWEOriPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSPWEOriPalletNO_Pallet: TWideStringField;
    QrVSPWEOriPalletSerieFch: TLargeintField;
    QrVSPWEOriPalletFicha: TLargeintField;
    QrVSPWEOriPalletNO_TTW: TWideStringField;
    QrVSPWEOriPalletID_TTW: TLargeintField;
    QrVSPWEOriPalletSeries_E_Fichas: TWideStringField;
    QrVSPWEOriPalletTerceiro: TLargeintField;
    QrVSPWEOriPalletMarca: TWideStringField;
    QrVSPWEOriPalletNO_FORNECE: TWideStringField;
    QrVSPWEOriPalletNO_SerieFch: TWideStringField;
    QrVSPWEOriPalletValorT: TFloatField;
    QrVSPWEOriPalletSdoVrtPeca: TFloatField;
    QrVSPWEOriPalletSdoVrtPeso: TFloatField;
    QrVSPWEOriPalletSdoVrtArM2: TFloatField;
    QrVSPWEDstCodigo: TLargeintField;
    QrVSPWEDstControle: TLargeintField;
    QrVSPWEDstMovimCod: TLargeintField;
    QrVSPWEDstMovimNiv: TLargeintField;
    QrVSPWEDstMovimTwn: TLargeintField;
    QrVSPWEDstEmpresa: TLargeintField;
    QrVSPWEDstTerceiro: TLargeintField;
    QrVSPWEDstCliVenda: TLargeintField;
    QrVSPWEDstMovimID: TLargeintField;
    QrVSPWEDstDataHora: TDateTimeField;
    QrVSPWEDstPallet: TLargeintField;
    QrVSPWEDstGraGruX: TLargeintField;
    QrVSPWEDstPecas: TFloatField;
    QrVSPWEDstPesoKg: TFloatField;
    QrVSPWEDstAreaM2: TFloatField;
    QrVSPWEDstAreaP2: TFloatField;
    QrVSPWEDstValorT: TFloatField;
    QrVSPWEDstSrcMovID: TLargeintField;
    QrVSPWEDstSrcNivel1: TLargeintField;
    QrVSPWEDstSrcNivel2: TLargeintField;
    QrVSPWEDstSrcGGX: TLargeintField;
    QrVSPWEDstSdoVrtPeca: TFloatField;
    QrVSPWEDstSdoVrtPeso: TFloatField;
    QrVSPWEDstSdoVrtArM2: TFloatField;
    QrVSPWEDstObserv: TWideStringField;
    QrVSPWEDstSerieFch: TLargeintField;
    QrVSPWEDstFicha: TLargeintField;
    QrVSPWEDstMisturou: TLargeintField;
    QrVSPWEDstFornecMO: TLargeintField;
    QrVSPWEDstCustoMOKg: TFloatField;
    QrVSPWEDstCustoMOM2: TFloatField;
    QrVSPWEDstCustoMOTot: TFloatField;
    QrVSPWEDstValorMP: TFloatField;
    QrVSPWEDstDstMovID: TLargeintField;
    QrVSPWEDstDstNivel1: TLargeintField;
    QrVSPWEDstDstNivel2: TLargeintField;
    QrVSPWEDstDstGGX: TLargeintField;
    QrVSPWEDstQtdGerPeca: TFloatField;
    QrVSPWEDstQtdGerPeso: TFloatField;
    QrVSPWEDstQtdGerArM2: TFloatField;
    QrVSPWEDstQtdGerArP2: TFloatField;
    QrVSPWEDstQtdAntPeca: TFloatField;
    QrVSPWEDstQtdAntPeso: TFloatField;
    QrVSPWEDstQtdAntArM2: TFloatField;
    QrVSPWEDstQtdAntArP2: TFloatField;
    QrVSPWEDstNotaMPAG: TFloatField;
    QrVSPWEDstNO_PALLET: TWideStringField;
    QrVSPWEDstNO_PRD_TAM_COR: TWideStringField;
    QrVSPWEDstNO_TTW: TWideStringField;
    QrVSPWEDstID_TTW: TLargeintField;
    QrVSPWEDstNO_FORNECE: TWideStringField;
    QrVSPWEDstNO_SerieFch: TWideStringField;
    QrVSPWEDstReqMovEstq: TLargeintField;
    QrVSPWEDstPedItsFin: TLargeintField;
    QrVSPWEDstMarca: TWideStringField;
    QrVSPWEBxaCodigo: TLargeintField;
    QrVSPWEBxaControle: TLargeintField;
    QrVSPWEBxaMovimCod: TLargeintField;
    QrVSPWEBxaMovimNiv: TLargeintField;
    QrVSPWEBxaMovimTwn: TLargeintField;
    QrVSPWEBxaEmpresa: TLargeintField;
    QrVSPWEBxaTerceiro: TLargeintField;
    QrVSPWEBxaCliVenda: TLargeintField;
    QrVSPWEBxaMovimID: TLargeintField;
    QrVSPWEBxaDataHora: TDateTimeField;
    QrVSPWEBxaPallet: TLargeintField;
    QrVSPWEBxaGraGruX: TLargeintField;
    QrVSPWEBxaPecas: TFloatField;
    QrVSPWEBxaPesoKg: TFloatField;
    QrVSPWEBxaAreaM2: TFloatField;
    QrVSPWEBxaAreaP2: TFloatField;
    QrVSPWEBxaValorT: TFloatField;
    QrVSPWEBxaSrcMovID: TLargeintField;
    QrVSPWEBxaSrcNivel1: TLargeintField;
    QrVSPWEBxaSrcNivel2: TLargeintField;
    QrVSPWEBxaSrcGGX: TLargeintField;
    QrVSPWEBxaSdoVrtPeca: TFloatField;
    QrVSPWEBxaSdoVrtPeso: TFloatField;
    QrVSPWEBxaSdoVrtArM2: TFloatField;
    QrVSPWEBxaObserv: TWideStringField;
    QrVSPWEBxaSerieFch: TLargeintField;
    QrVSPWEBxaFicha: TLargeintField;
    QrVSPWEBxaMisturou: TLargeintField;
    QrVSPWEBxaFornecMO: TLargeintField;
    QrVSPWEBxaCustoMOKg: TFloatField;
    QrVSPWEBxaCustoMOM2: TFloatField;
    QrVSPWEBxaCustoMOTot: TFloatField;
    QrVSPWEBxaValorMP: TFloatField;
    QrVSPWEBxaDstMovID: TLargeintField;
    QrVSPWEBxaDstNivel1: TLargeintField;
    QrVSPWEBxaDstNivel2: TLargeintField;
    QrVSPWEBxaDstGGX: TLargeintField;
    QrVSPWEBxaQtdGerPeca: TFloatField;
    QrVSPWEBxaQtdGerPeso: TFloatField;
    QrVSPWEBxaQtdGerArM2: TFloatField;
    QrVSPWEBxaQtdGerArP2: TFloatField;
    QrVSPWEBxaQtdAntPeca: TFloatField;
    QrVSPWEBxaQtdAntPeso: TFloatField;
    QrVSPWEBxaQtdAntArM2: TFloatField;
    QrVSPWEBxaQtdAntArP2: TFloatField;
    QrVSPWEBxaNotaMPAG: TFloatField;
    QrVSPWEBxaNO_PALLET: TWideStringField;
    QrVSPWEBxaNO_PRD_TAM_COR: TWideStringField;
    QrVSPWEBxaNO_TTW: TWideStringField;
    QrVSPWEBxaID_TTW: TLargeintField;
    QrVSPWEBxaNO_FORNECE: TWideStringField;
    QrVSPWEBxaNO_SerieFch: TWideStringField;
    QrVSPWEBxaReqMovEstq: TLargeintField;
    QrForcadosCodigo: TLargeintField;
    QrForcadosControle: TLargeintField;
    QrForcadosMovimCod: TLargeintField;
    QrForcadosMovimNiv: TLargeintField;
    QrForcadosMovimTwn: TLargeintField;
    QrForcadosEmpresa: TLargeintField;
    QrForcadosTerceiro: TLargeintField;
    QrForcadosCliVenda: TLargeintField;
    QrForcadosMovimID: TLargeintField;
    QrForcadosDataHora: TDateTimeField;
    QrForcadosPallet: TLargeintField;
    QrForcadosGraGruX: TLargeintField;
    QrForcadosPecas: TFloatField;
    QrForcadosPesoKg: TFloatField;
    QrForcadosAreaM2: TFloatField;
    QrForcadosAreaP2: TFloatField;
    QrForcadosValorT: TFloatField;
    QrForcadosSrcMovID: TLargeintField;
    QrForcadosSrcNivel1: TLargeintField;
    QrForcadosSrcNivel2: TLargeintField;
    QrForcadosSrcGGX: TLargeintField;
    QrForcadosSdoVrtPeca: TFloatField;
    QrForcadosSdoVrtPeso: TFloatField;
    QrForcadosSdoVrtArM2: TFloatField;
    QrForcadosObserv: TWideStringField;
    QrForcadosSerieFch: TLargeintField;
    QrForcadosFicha: TLargeintField;
    QrForcadosMisturou: TLargeintField;
    QrForcadosFornecMO: TLargeintField;
    QrForcadosCustoMOKg: TFloatField;
    QrForcadosCustoMOM2: TFloatField;
    QrForcadosCustoMOTot: TFloatField;
    QrForcadosValorMP: TFloatField;
    QrForcadosDstMovID: TLargeintField;
    QrForcadosDstNivel1: TLargeintField;
    QrForcadosDstNivel2: TLargeintField;
    QrForcadosDstGGX: TLargeintField;
    QrForcadosQtdGerPeca: TFloatField;
    QrForcadosQtdGerPeso: TFloatField;
    QrForcadosQtdGerArM2: TFloatField;
    QrForcadosQtdGerArP2: TFloatField;
    QrForcadosQtdAntPeca: TFloatField;
    QrForcadosQtdAntPeso: TFloatField;
    QrForcadosQtdAntArM2: TFloatField;
    QrForcadosQtdAntArP2: TFloatField;
    QrForcadosNotaMPAG: TFloatField;
    QrForcadosNO_PALLET: TWideStringField;
    QrForcadosNO_PRD_TAM_COR: TWideStringField;
    QrForcadosNO_TTW: TWideStringField;
    QrForcadosID_TTW: TLargeintField;
    Label60: TLabel;
    DBEdit45: TDBEdit;
    N4: TMenuItem;
    IrparajaneladoPallet1: TMenuItem;
    AlteraFornecedorMO1: TMenuItem;
    Label61: TLabel;
    DBEdit46: TDBEdit;
    DBEdit47: TDBEdit;
    QrVSPWEAtuNO_FORNEC_MO: TWideStringField;
    FornecedorMO1: TMenuItem;
    Localdoestoque1: TMenuItem;
    Cliente1: TMenuItem;
    QrVSPWEDstStqCenLoc: TLargeintField;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    Label62: TLabel;
    EdClientMO: TdmkEditCB;
    CBClientMO: TdmkDBLookupComboBox;
    QrVSPWEAtuClientMO: TLargeintField;
    OPpreenchida1: TMenuItem;
    QrGGXDst: TmySQLQuery;
    QrGGXDstGraGru1: TIntegerField;
    QrGGXDstControle: TIntegerField;
    QrGGXDstNO_PRD_TAM_COR: TWideStringField;
    QrGGXDstSIGLAUNIDMED: TWideStringField;
    QrGGXDstCODUSUUNIDMED: TIntegerField;
    QrGGXDstNOMEUNIDMED: TWideStringField;
    DsGGXDst: TDataSource;
    Label63: TLabel;
    CkCpermitirCrust: TCheckBox;
    EdGGXDst: TdmkEditCB;
    CBGGXDst: TdmkDBLookupComboBox;
    QrVSPWECabGGXDst: TIntegerField;
    IrparacadastrodoPallet1: TMenuItem;
    dmkDBEdit3: TdmkDBEdit;
    Label59: TLabel;
    QrVSPWECabSerieRem: TSmallintField;
    EdSerieRem: TdmkEdit;
    Label57: TLabel;
    N5: TMenuItem;
    MudarodestinoparaoutraOP1: TMenuItem;
    QrVSCOPCab: TmySQLQuery;
    QrVSCOPCabCodigo: TIntegerField;
    QrVSCOPCabNome: TWideStringField;
    DsVSCOPCab: TDataSource;
    GBConfig: TGroupBox;
    Label6: TLabel;
    Label64: TLabel;
    EdControle: TdmkEdit;
    EdVSCOPCab: TdmkEditCB;
    CBVSCOPCab: TdmkDBLookupComboBox;
    QrVSPWECabVSCOPCab: TIntegerField;
    QrVSPWECabNO_VSCOPCab: TWideStringField;
    SbVSCOPCabCad: TSpeedButton;
    SbVSCOPCabCpy: TSpeedButton;
    Adicionadesclassificado1: TMenuItem;
    QrVSPWEDesclDst: TmySQLQuery;
    QrVSPWEDesclDstCodigo: TLargeintField;
    QrVSPWEDesclDstControle: TLargeintField;
    QrVSPWEDesclDstMovimCod: TLargeintField;
    QrVSPWEDesclDstMovimNiv: TLargeintField;
    QrVSPWEDesclDstMovimTwn: TLargeintField;
    QrVSPWEDesclDstEmpresa: TLargeintField;
    QrVSPWEDesclDstTerceiro: TLargeintField;
    QrVSPWEDesclDstCliVenda: TLargeintField;
    QrVSPWEDesclDstMovimID: TLargeintField;
    QrVSPWEDesclDstDataHora: TDateTimeField;
    QrVSPWEDesclDstPallet: TLargeintField;
    QrVSPWEDesclDstGraGruX: TLargeintField;
    QrVSPWEDesclDstPecas: TFloatField;
    QrVSPWEDesclDstPesoKg: TFloatField;
    QrVSPWEDesclDstAreaM2: TFloatField;
    QrVSPWEDesclDstAreaP2: TFloatField;
    QrVSPWEDesclDstValorT: TFloatField;
    QrVSPWEDesclDstSrcMovID: TLargeintField;
    QrVSPWEDesclDstSrcNivel1: TLargeintField;
    QrVSPWEDesclDstSrcNivel2: TLargeintField;
    QrVSPWEDesclDstSrcGGX: TLargeintField;
    QrVSPWEDesclDstSdoVrtPeca: TFloatField;
    QrVSPWEDesclDstSdoVrtPeso: TFloatField;
    QrVSPWEDesclDstSdoVrtArM2: TFloatField;
    QrVSPWEDesclDstObserv: TWideStringField;
    QrVSPWEDesclDstSerieFch: TLargeintField;
    QrVSPWEDesclDstFicha: TLargeintField;
    QrVSPWEDesclDstMisturou: TLargeintField;
    QrVSPWEDesclDstFornecMO: TLargeintField;
    QrVSPWEDesclDstCustoMOKg: TFloatField;
    QrVSPWEDesclDstCustoMOM2: TFloatField;
    QrVSPWEDesclDstCustoMOTot: TFloatField;
    QrVSPWEDesclDstValorMP: TFloatField;
    QrVSPWEDesclDstDstMovID: TLargeintField;
    QrVSPWEDesclDstDstNivel1: TLargeintField;
    QrVSPWEDesclDstDstNivel2: TLargeintField;
    QrVSPWEDesclDstDstGGX: TLargeintField;
    QrVSPWEDesclDstQtdGerPeca: TFloatField;
    QrVSPWEDesclDstQtdGerPeso: TFloatField;
    QrVSPWEDesclDstQtdGerArM2: TFloatField;
    QrVSPWEDesclDstQtdGerArP2: TFloatField;
    QrVSPWEDesclDstQtdAntPeca: TFloatField;
    QrVSPWEDesclDstQtdAntPeso: TFloatField;
    QrVSPWEDesclDstQtdAntArM2: TFloatField;
    QrVSPWEDesclDstQtdAntArP2: TFloatField;
    QrVSPWEDesclDstNotaMPAG: TFloatField;
    QrVSPWEDesclDstNO_PALLET: TWideStringField;
    QrVSPWEDesclDstNO_PRD_TAM_COR: TWideStringField;
    QrVSPWEDesclDstNO_TTW: TWideStringField;
    QrVSPWEDesclDstID_TTW: TLargeintField;
    QrVSPWEDesclDstNO_FORNECE: TWideStringField;
    QrVSPWEDesclDstNO_SerieFch: TWideStringField;
    QrVSPWEDesclDstReqMovEstq: TLargeintField;
    QrVSPWEDesclDstPedItsFin: TLargeintField;
    QrVSPWEDesclDstMarca: TWideStringField;
    QrVSPWEDesclDstStqCenLoc: TLargeintField;
    DsVSPWEDesclDst: TDataSource;
    QrVSPWEDesclBxa: TmySQLQuery;
    QrVSPWEDesclBxaCodigo: TLargeintField;
    QrVSPWEDesclBxaControle: TLargeintField;
    QrVSPWEDesclBxaMovimCod: TLargeintField;
    QrVSPWEDesclBxaMovimNiv: TLargeintField;
    QrVSPWEDesclBxaMovimTwn: TLargeintField;
    QrVSPWEDesclBxaEmpresa: TLargeintField;
    QrVSPWEDesclBxaTerceiro: TLargeintField;
    QrVSPWEDesclBxaCliVenda: TLargeintField;
    QrVSPWEDesclBxaMovimID: TLargeintField;
    QrVSPWEDesclBxaDataHora: TDateTimeField;
    QrVSPWEDesclBxaPallet: TLargeintField;
    QrVSPWEDesclBxaGraGruX: TLargeintField;
    QrVSPWEDesclBxaPecas: TFloatField;
    QrVSPWEDesclBxaPesoKg: TFloatField;
    QrVSPWEDesclBxaAreaM2: TFloatField;
    QrVSPWEDesclBxaAreaP2: TFloatField;
    QrVSPWEDesclBxaValorT: TFloatField;
    QrVSPWEDesclBxaSrcMovID: TLargeintField;
    QrVSPWEDesclBxaSrcNivel1: TLargeintField;
    QrVSPWEDesclBxaSrcNivel2: TLargeintField;
    QrVSPWEDesclBxaSrcGGX: TLargeintField;
    QrVSPWEDesclBxaSdoVrtPeca: TFloatField;
    QrVSPWEDesclBxaSdoVrtPeso: TFloatField;
    QrVSPWEDesclBxaSdoVrtArM2: TFloatField;
    QrVSPWEDesclBxaObserv: TWideStringField;
    QrVSPWEDesclBxaSerieFch: TLargeintField;
    QrVSPWEDesclBxaFicha: TLargeintField;
    QrVSPWEDesclBxaMisturou: TLargeintField;
    QrVSPWEDesclBxaFornecMO: TLargeintField;
    QrVSPWEDesclBxaCustoMOKg: TFloatField;
    QrVSPWEDesclBxaCustoMOM2: TFloatField;
    QrVSPWEDesclBxaCustoMOTot: TFloatField;
    QrVSPWEDesclBxaValorMP: TFloatField;
    QrVSPWEDesclBxaDstMovID: TLargeintField;
    QrVSPWEDesclBxaDstNivel1: TLargeintField;
    QrVSPWEDesclBxaDstNivel2: TLargeintField;
    QrVSPWEDesclBxaDstGGX: TLargeintField;
    QrVSPWEDesclBxaQtdGerPeca: TFloatField;
    QrVSPWEDesclBxaQtdGerPeso: TFloatField;
    QrVSPWEDesclBxaQtdGerArM2: TFloatField;
    QrVSPWEDesclBxaQtdGerArP2: TFloatField;
    QrVSPWEDesclBxaQtdAntPeca: TFloatField;
    QrVSPWEDesclBxaQtdAntPeso: TFloatField;
    QrVSPWEDesclBxaQtdAntArM2: TFloatField;
    QrVSPWEDesclBxaQtdAntArP2: TFloatField;
    QrVSPWEDesclBxaNotaMPAG: TFloatField;
    QrVSPWEDesclBxaNO_PALLET: TWideStringField;
    QrVSPWEDesclBxaNO_PRD_TAM_COR: TWideStringField;
    QrVSPWEDesclBxaNO_TTW: TWideStringField;
    QrVSPWEDesclBxaID_TTW: TLargeintField;
    QrVSPWEDesclBxaNO_FORNECE: TWideStringField;
    QrVSPWEDesclBxaNO_SerieFch: TWideStringField;
    QrVSPWEDesclBxaReqMovEstq: TLargeintField;
    DsVSPWEDesclBxa: TDataSource;
    PCPWEDst: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Panel12: TPanel;
    Panel13: TPanel;
    DGDadosDst: TDBGrid;
    DBGrid4: TDBGrid;
    DBGrid5: TDBGrid;
    N6: TMenuItem;
    Removedesclassificado1: TMenuItem;
    Editaea1: TMenuItem;
    Corrigetodasbaixas1: TMenuItem;
    TabSheet5: TTabSheet;
    QrEmit: TmySQLQuery;
    QrEmitCodigo: TIntegerField;
    QrEmitDataEmis: TDateTimeField;
    QrEmitStatus: TSmallintField;
    QrEmitNumero: TIntegerField;
    QrEmitNOMECI: TWideStringField;
    QrEmitNOMESETOR: TWideStringField;
    QrEmitTecnico: TWideStringField;
    QrEmitNOME: TWideStringField;
    QrEmitClienteI: TIntegerField;
    QrEmitTipificacao: TSmallintField;
    QrEmitTempoR: TIntegerField;
    QrEmitTempoP: TIntegerField;
    QrEmitSetor: TSmallintField;
    QrEmitTipific: TSmallintField;
    QrEmitEspessura: TWideStringField;
    QrEmitDefPeca: TWideStringField;
    QrEmitPeso: TFloatField;
    QrEmitCusto: TFloatField;
    QrEmitQtde: TFloatField;
    QrEmitAreaM2: TFloatField;
    QrEmitFulao: TWideStringField;
    QrEmitObs: TWideStringField;
    QrEmitSetrEmi: TSmallintField;
    QrEmitSourcMP: TSmallintField;
    QrEmitCod_Espess: TIntegerField;
    QrEmitCodDefPeca: TIntegerField;
    QrEmitCustoTo: TFloatField;
    QrEmitCustoKg: TFloatField;
    QrEmitCustoM2: TFloatField;
    QrEmitCusUSM2: TFloatField;
    QrEmitLk: TIntegerField;
    QrEmitDataCad: TDateField;
    QrEmitDataAlt: TDateField;
    QrEmitUserCad: TIntegerField;
    QrEmitUserAlt: TIntegerField;
    QrEmitAlterWeb: TSmallintField;
    QrEmitAtivo: TSmallintField;
    QrEmitEmitGru: TIntegerField;
    QrEmitRetrabalho: TSmallintField;
    QrEmitSemiCodPeca: TIntegerField;
    QrEmitSemiTxtPeca: TWideStringField;
    QrEmitSemiPeso: TFloatField;
    QrEmitSemiQtde: TFloatField;
    QrEmitSemiAreaM2: TFloatField;
    QrEmitSemiRendim: TFloatField;
    QrEmitSemiCodEspe: TIntegerField;
    QrEmitSemiTxtEspe: TWideStringField;
    QrEmitBRL_USD: TFloatField;
    QrEmitBRL_EUR: TFloatField;
    QrEmitDtaCambio: TDateField;
    QrEmitVSMovCod: TIntegerField;
    DsEmit: TDataSource;
    DBGrid6: TDBGrid;
    PMPesagem: TPopupMenu;
    EmitePesagem2: TMenuItem;
    Reimprimereceita1: TMenuItem;
    ExcluiPesagem1: TMenuItem;
    PB1: TProgressBar;
    BtPesagem: TBitBtn;
    DBEdit48: TDBEdit;
    Label65: TLabel;
    QrVSPWEAtuCustoPQ: TFloatField;
    Recalculacusto1: TMenuItem;
    QrEmitGru: TmySQLQuery;
    QrEmitGruCodigo: TIntegerField;
    QrEmitGruNome: TWideStringField;
    DsEmitGru: TDataSource;
    QrVSPWECabEmitGru: TIntegerField;
    QrVSPWECabNO_EmitGru: TWideStringField;
    Label66: TLabel;
    EdVSArtCab: TdmkEditCB;
    CBVSArtCab: TdmkDBLookupComboBox;
    Label67: TLabel;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    Emiteoutrasbaixas1: TMenuItem;
    Novogrupo1: TMenuItem;
    Novoitem1: TMenuItem;
    TabSheet6: TTabSheet;
    DBGrid7: TDBGrid;
    QrPQO: TmySQLQuery;
    QrPQOCodigo: TIntegerField;
    QrPQOSetor: TIntegerField;
    QrPQOCustoInsumo: TFloatField;
    QrPQOCustoTotal: TFloatField;
    QrPQODataB: TDateField;
    QrPQOLk: TIntegerField;
    QrPQODataCad: TDateField;
    QrPQODataAlt: TDateField;
    QrPQOUserCad: TIntegerField;
    QrPQOUserAlt: TIntegerField;
    QrPQONOMESETOR: TWideStringField;
    QrPQONO_EMITGRU: TWideStringField;
    QrPQOAlterWeb: TSmallintField;
    QrPQOAtivo: TSmallintField;
    QrPQOEmitGru: TIntegerField;
    QrPQONome: TWideStringField;
    DsPQO: TDataSource;
    QrPQOIts: TmySQLQuery;
    QrPQOItsDataX: TDateField;
    QrPQOItsTipo: TSmallintField;
    QrPQOItsCliOrig: TIntegerField;
    QrPQOItsCliDest: TIntegerField;
    QrPQOItsInsumo: TIntegerField;
    QrPQOItsPeso: TFloatField;
    QrPQOItsValor: TFloatField;
    QrPQOItsOrigemCodi: TIntegerField;
    QrPQOItsOrigemCtrl: TIntegerField;
    QrPQOItsNOMEPQ: TWideStringField;
    QrPQOItsGrupoQuimico: TIntegerField;
    QrPQOItsNOMEGRUPO: TWideStringField;
    DsPQOIts: TDataSource;
    N7: TMenuItem;
    Excluiitem1: TMenuItem;
    Excluigrupo1: TMenuItem;
    DBGIts: TDBGrid;
    Emitepesagem1: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    AdicionasubprodutoWBRaspa1: TMenuItem;
    TabSheet7: TTabSheet;
    DBGrid8: TDBGrid;
    QrVSPWESubPrd: TmySQLQuery;
    QrVSPWESubPrdCodigo: TLargeintField;
    QrVSPWESubPrdControle: TLargeintField;
    QrVSPWESubPrdMovimCod: TLargeintField;
    QrVSPWESubPrdMovimNiv: TLargeintField;
    QrVSPWESubPrdMovimTwn: TLargeintField;
    QrVSPWESubPrdEmpresa: TLargeintField;
    QrVSPWESubPrdTerceiro: TLargeintField;
    QrVSPWESubPrdCliVenda: TLargeintField;
    QrVSPWESubPrdMovimID: TLargeintField;
    QrVSPWESubPrdDataHora: TDateTimeField;
    QrVSPWESubPrdPallet: TLargeintField;
    QrVSPWESubPrdGraGruX: TLargeintField;
    QrVSPWESubPrdPecas: TFloatField;
    QrVSPWESubPrdPesoKg: TFloatField;
    QrVSPWESubPrdAreaM2: TFloatField;
    QrVSPWESubPrdAreaP2: TFloatField;
    QrVSPWESubPrdValorT: TFloatField;
    QrVSPWESubPrdSrcMovID: TLargeintField;
    QrVSPWESubPrdSrcNivel1: TLargeintField;
    QrVSPWESubPrdSrcNivel2: TLargeintField;
    QrVSPWESubPrdSrcGGX: TLargeintField;
    QrVSPWESubPrdSdoVrtPeca: TFloatField;
    QrVSPWESubPrdSdoVrtPeso: TFloatField;
    QrVSPWESubPrdSdoVrtArM2: TFloatField;
    QrVSPWESubPrdObserv: TWideStringField;
    QrVSPWESubPrdSerieFch: TLargeintField;
    QrVSPWESubPrdFicha: TLargeintField;
    QrVSPWESubPrdMisturou: TLargeintField;
    QrVSPWESubPrdFornecMO: TLargeintField;
    QrVSPWESubPrdCustoMOKg: TFloatField;
    QrVSPWESubPrdCustoMOM2: TFloatField;
    QrVSPWESubPrdCustoMOTot: TFloatField;
    QrVSPWESubPrdValorMP: TFloatField;
    QrVSPWESubPrdDstMovID: TLargeintField;
    QrVSPWESubPrdDstNivel1: TLargeintField;
    QrVSPWESubPrdDstNivel2: TLargeintField;
    QrVSPWESubPrdDstGGX: TLargeintField;
    QrVSPWESubPrdQtdGerPeca: TFloatField;
    QrVSPWESubPrdQtdGerPeso: TFloatField;
    QrVSPWESubPrdQtdGerArM2: TFloatField;
    QrVSPWESubPrdQtdGerArP2: TFloatField;
    QrVSPWESubPrdQtdAntPeca: TFloatField;
    QrVSPWESubPrdQtdAntPeso: TFloatField;
    QrVSPWESubPrdQtdAntArM2: TFloatField;
    QrVSPWESubPrdQtdAntArP2: TFloatField;
    QrVSPWESubPrdNotaMPAG: TFloatField;
    QrVSPWESubPrdNO_PALLET: TWideStringField;
    QrVSPWESubPrdNO_PRD_TAM_COR: TWideStringField;
    QrVSPWESubPrdNO_TTW: TWideStringField;
    QrVSPWESubPrdID_TTW: TLargeintField;
    QrVSPWESubPrdNO_FORNECE: TWideStringField;
    QrVSPWESubPrdNO_SerieFch: TWideStringField;
    QrVSPWESubPrdReqMovEstq: TLargeintField;
    QrVSPWESubPrdPedItsFin: TLargeintField;
    QrVSPWESubPrdMarca: TWideStringField;
    QrVSPWESubPrdStqCenLoc: TLargeintField;
    DsVSPWESubPrd: TDataSource;
    RemovesubprodutoWBRaspa1: TMenuItem;
    CorrigeMO1: TMenuItem;
    Panel14: TPanel;
    PCPWEDestSub: TPageControl;
    TabSheet8: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet9: TTabSheet;
    dmkDBGridZTO1: TdmkDBGridZTO;
    AtrelamentoNFsdeMO2: TMenuItem;
    IncluiAtrelamento2: TMenuItem;
    AlteraAtrelamento2: TMenuItem;
    ExcluiAtrelamento2: TMenuItem;
    CobranadeMO1: TMenuItem;
    Recurtimento1: TMenuItem;
    Acabamento1: TMenuItem;
    OPpreenchidacomorigemedestino1: TMenuItem;
    QrVSPWEOriIMEIItemNFe: TLargeintField;
    QrVSPWEOriIMEIGGXRcl: TLargeintField;
    AlteraLocaldeEstque1: TMenuItem;
    QrVSPWEOriIMEIDtCorrApo: TDateTimeField;
    QrVSPWEDesclDstDtCorrApo: TDateTimeField;
    QrVSPWEDstDtCorrApo: TDateTimeField;
    QrVSPWESubPrdDtCorrApo: TDateTimeField;
    Corrigeartigodebaixa1: TMenuItem;
    Reabreitensteste1: TMenuItem;
    InformaPginaelinhadeIEC1: TMenuItem;
    QrVSPWEOriIMEIIxxMovIX: TLargeintField;
    QrVSPWEOriIMEIIxxFolha: TLargeintField;
    QrVSPWEOriIMEIIxxLinha: TLargeintField;
    QrEmitDtCorrApo_TXT: TWideStringField;
    QrEmitDtCorrApo: TDateTimeField;
    QrEmitDtaBaixa_TXT: TWideStringField;
    QrEmitDtaBaixa: TDateField;
    TsEnvioMO: TTabSheet;
    TsRetornoMO: TTabSheet;
    QrVSMOEnvEnv: TmySQLQuery;
    QrVSMOEnvEnvCodigo: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatID: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatNum: TIntegerField;
    QrVSMOEnvEnvNFEMP_Empresa: TIntegerField;
    QrVSMOEnvEnvNFEMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvNFEMP_nItem: TIntegerField;
    QrVSMOEnvEnvNFEMP_SerNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_nNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_Pecas: TFloatField;
    QrVSMOEnvEnvNFEMP_PesoKg: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaM2: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaP2: TFloatField;
    QrVSMOEnvEnvNFEMP_ValorT: TFloatField;
    QrVSMOEnvEnvCFTMP_FatID: TIntegerField;
    QrVSMOEnvEnvCFTMP_FatNum: TIntegerField;
    QrVSMOEnvEnvCFTMP_Empresa: TIntegerField;
    QrVSMOEnvEnvCFTMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvCFTMP_nItem: TIntegerField;
    QrVSMOEnvEnvCFTMP_SerCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_nCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_Pecas: TFloatField;
    QrVSMOEnvEnvCFTMP_PesoKg: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaM2: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaP2: TFloatField;
    QrVSMOEnvEnvCFTMP_PesTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_CusTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_ValorT: TFloatField;
    QrVSMOEnvEnvVSVMI_MovimCod: TIntegerField;
    QrVSMOEnvEnvLk: TIntegerField;
    QrVSMOEnvEnvDataCad: TDateField;
    QrVSMOEnvEnvDataAlt: TDateField;
    QrVSMOEnvEnvUserCad: TIntegerField;
    QrVSMOEnvEnvUserAlt: TIntegerField;
    QrVSMOEnvEnvAlterWeb: TSmallintField;
    QrVSMOEnvEnvAWServerID: TIntegerField;
    QrVSMOEnvEnvAWStatSinc: TSmallintField;
    QrVSMOEnvEnvAtivo: TSmallintField;
    DsVSMOEnvEnv: TDataSource;
    QrVSMOEnvEVMI: TmySQLQuery;
    QrVSMOEnvEVMICodigo: TIntegerField;
    QrVSMOEnvEVMIVSMOEnvEnv: TIntegerField;
    QrVSMOEnvEVMIVSMovIts: TIntegerField;
    QrVSMOEnvEVMILk: TIntegerField;
    QrVSMOEnvEVMIDataCad: TDateField;
    QrVSMOEnvEVMIDataAlt: TDateField;
    QrVSMOEnvEVMIUserCad: TIntegerField;
    QrVSMOEnvEVMIUserAlt: TIntegerField;
    QrVSMOEnvEVMIAlterWeb: TSmallintField;
    QrVSMOEnvEVMIAWServerID: TIntegerField;
    QrVSMOEnvEVMIAWStatSinc: TSmallintField;
    QrVSMOEnvEVMIAtivo: TSmallintField;
    QrVSMOEnvEVMIValorFrete: TFloatField;
    QrVSMOEnvEVMIPecas: TFloatField;
    QrVSMOEnvEVMIPesoKg: TFloatField;
    QrVSMOEnvEVMIAreaM2: TFloatField;
    QrVSMOEnvEVMIAreaP2: TFloatField;
    DsVSMOEnvEVMI: TDataSource;
    QrVSMOEnvRVmi: TmySQLQuery;
    QrVSMOEnvRVmiCodigo: TIntegerField;
    QrVSMOEnvRVmiVSMOEnvRet: TIntegerField;
    QrVSMOEnvRVmiVSMOEnvEnv: TIntegerField;
    QrVSMOEnvRVmiPecas: TFloatField;
    QrVSMOEnvRVmiPesoKg: TFloatField;
    QrVSMOEnvRVmiAreaM2: TFloatField;
    QrVSMOEnvRVmiAreaP2: TFloatField;
    QrVSMOEnvRVmiValorFrete: TFloatField;
    QrVSMOEnvRVmiLk: TIntegerField;
    QrVSMOEnvRVmiDataCad: TDateField;
    QrVSMOEnvRVmiDataAlt: TDateField;
    QrVSMOEnvRVmiUserCad: TIntegerField;
    QrVSMOEnvRVmiUserAlt: TIntegerField;
    QrVSMOEnvRVmiAlterWeb: TSmallintField;
    QrVSMOEnvRVmiAWServerID: TIntegerField;
    QrVSMOEnvRVmiAWStatSinc: TSmallintField;
    QrVSMOEnvRVmiAtivo: TSmallintField;
    QrVSMOEnvRVmiNFEMP_SerNF: TIntegerField;
    QrVSMOEnvRVmiNFEMP_nNF: TIntegerField;
    QrVSMOEnvRVmiValorT: TFloatField;
    DsVSMOEnvRVmi: TDataSource;
    QrVSMOEnvGVmi: TmySQLQuery;
    QrVSMOEnvGVmiCodigo: TIntegerField;
    QrVSMOEnvGVmiVSMOEnvRet: TIntegerField;
    QrVSMOEnvGVmiVSMovIts: TIntegerField;
    QrVSMOEnvGVmiPecas: TFloatField;
    QrVSMOEnvGVmiPesoKg: TFloatField;
    QrVSMOEnvGVmiAreaM2: TFloatField;
    QrVSMOEnvGVmiAreaP2: TFloatField;
    QrVSMOEnvGVmiValorFrete: TFloatField;
    QrVSMOEnvGVmiLk: TIntegerField;
    QrVSMOEnvGVmiDataCad: TDateField;
    QrVSMOEnvGVmiDataAlt: TDateField;
    QrVSMOEnvGVmiUserCad: TIntegerField;
    QrVSMOEnvGVmiUserAlt: TIntegerField;
    QrVSMOEnvGVmiAlterWeb: TSmallintField;
    QrVSMOEnvGVmiAWServerID: TIntegerField;
    QrVSMOEnvGVmiAWStatSinc: TSmallintField;
    QrVSMOEnvGVmiAtivo: TSmallintField;
    QrVSMOEnvGVmiVSMovimCod: TIntegerField;
    DsVSMOEnvGVmi: TDataSource;
    PnEnvioMO: TPanel;
    DBGVSMOEnvEnv: TdmkDBGridZTO;
    DBGVSMOEnvEVmi: TdmkDBGridZTO;
    PnRetornoMO: TPanel;
    DBGVSMOEnvRet: TdmkDBGridZTO;
    PnItensRetMO: TPanel;
    Splitter2: TSplitter;
    GroupBox8: TGroupBox;
    DBGVSMOEnvRVmi: TdmkDBGridZTO;
    GroupBox9: TGroupBox;
    DBGVSMOEnvGVmi: TdmkDBGridZTO;
    N10: TMenuItem;
    N11: TMenuItem;
    AtrelamentoNFsdeMO1: TMenuItem;
    N12: TMenuItem;
    IncluiAtrelamento1: TMenuItem;
    AlteraAtrelamento1: TMenuItem;
    ExcluiAtrelamento1: TMenuItem;
    QrVSMOEnvRet: TmySQLQuery;
    QrVSMOEnvRetCodigo: TIntegerField;
    QrVSMOEnvRetNFCMO_FatID: TIntegerField;
    QrVSMOEnvRetNFCMO_FatNum: TIntegerField;
    QrVSMOEnvRetNFCMO_Empresa: TIntegerField;
    QrVSMOEnvRetNFCMO_nItem: TIntegerField;
    QrVSMOEnvRetNFCMO_SerNF: TIntegerField;
    QrVSMOEnvRetNFCMO_nNF: TIntegerField;
    QrVSMOEnvRetNFCMO_Pecas: TFloatField;
    QrVSMOEnvRetNFCMO_PesoKg: TFloatField;
    QrVSMOEnvRetNFCMO_AreaM2: TFloatField;
    QrVSMOEnvRetNFCMO_AreaP2: TFloatField;
    QrVSMOEnvRetNFCMO_CusMOM2: TFloatField;
    QrVSMOEnvRetNFCMO_CusMOKG: TFloatField;
    QrVSMOEnvRetNFCMO_ValorT: TFloatField;
    QrVSMOEnvRetNFCMO_Terceiro: TIntegerField;
    QrVSMOEnvRetNFRMP_FatID: TIntegerField;
    QrVSMOEnvRetNFRMP_FatNum: TIntegerField;
    QrVSMOEnvRetNFRMP_Empresa: TIntegerField;
    QrVSMOEnvRetNFRMP_nItem: TIntegerField;
    QrVSMOEnvRetNFRMP_SerNF: TIntegerField;
    QrVSMOEnvRetNFRMP_nNF: TIntegerField;
    QrVSMOEnvRetNFRMP_Pecas: TFloatField;
    QrVSMOEnvRetNFRMP_PesoKg: TFloatField;
    QrVSMOEnvRetNFRMP_AreaM2: TFloatField;
    QrVSMOEnvRetNFRMP_AreaP2: TFloatField;
    QrVSMOEnvRetNFRMP_ValorT: TFloatField;
    QrVSMOEnvRetNFRMP_Terceiro: TIntegerField;
    QrVSMOEnvRetCFTPA_FatID: TIntegerField;
    QrVSMOEnvRetCFTPA_FatNum: TIntegerField;
    QrVSMOEnvRetCFTPA_Empresa: TIntegerField;
    QrVSMOEnvRetCFTPA_Terceiro: TIntegerField;
    QrVSMOEnvRetCFTPA_nItem: TIntegerField;
    QrVSMOEnvRetCFTPA_SerCT: TIntegerField;
    QrVSMOEnvRetCFTPA_nCT: TIntegerField;
    QrVSMOEnvRetCFTPA_Pecas: TFloatField;
    QrVSMOEnvRetCFTPA_PesoKg: TFloatField;
    QrVSMOEnvRetCFTPA_AreaM2: TFloatField;
    QrVSMOEnvRetCFTPA_AreaP2: TFloatField;
    QrVSMOEnvRetCFTPA_PesTrKg: TFloatField;
    QrVSMOEnvRetCFTPA_CusTrKg: TFloatField;
    QrVSMOEnvRetCFTPA_ValorT: TFloatField;
    DsVSMOEnvRet: TDataSource;
    QrVSPWEDstCusFrtMORet: TFloatField;
    QrVSPWEDstCustoMOPc: TFloatField;
    DBEdit51: TDBEdit;
    Label68: TLabel;
    QrVSPWEAtuCusFrtMOEnv: TFloatField;
    QrVSPWEAtuCusFrtMORet: TFloatField;
    DBEdit52: TDBEdit;
    Label69: TLabel;
    Label70: TLabel;
    EdEmitGru: TdmkEditCB;
    CBEmitGru: TdmkDBLookupComboBox;
    QrVSArtCab: TMySQLQuery;
    QrVSArtCabCodigo: TIntegerField;
    QrVSArtCabNome: TWideStringField;
    DsVSArtCab: TDataSource;
    Label71: TLabel;
    DBEdit53: TDBEdit;
    DBEdit54: TDBEdit;
    QrVSPWECabVSArtCab: TIntegerField;
    QrVSPWECabNO_VSArtCab: TWideStringField;
    OPFluxo1: TMenuItem;
    EdEmitGrLote: TdmkEdit;
    Label72: TLabel;
    QrVSPWECabEmitGrLote: TWideStringField;
    QrVSPWEAtuNO_CLIENT_MO: TWideStringField;
    QrVSPWECabNO_PRD_TAM_COR_DST: TWideStringField;
    Panel15: TPanel;
    Label73: TLabel;
    EdLinCulReb: TdmkEdit;
    Label74: TLabel;
    EdLinCabReb: TdmkEdit;
    Label75: TLabel;
    EdLinCulSem: TdmkEdit;
    Label76: TLabel;
    EdLinCabSem: TdmkEdit;
    Label77: TLabel;
    TPDataStart: TdmkEditDateTimePicker;
    EdDataStart: TdmkEdit;
    Label78: TLabel;
    TPDataCrust: TdmkEditDateTimePicker;
    EdDataCrust: TdmkEdit;
    Label79: TLabel;
    TPDataExped: TdmkEditDateTimePicker;
    EdDataExped: TdmkEdit;
    Label80: TLabel;
    EdReceiRecu: TdmkEditCB;
    CBReceiRecu: TdmkDBLookupComboBox;
    Label81: TLabel;
    EdReceiRefu: TdmkEditCB;
    CBReceiRefu: TdmkDBLookupComboBox;
    QrReceiRecu: TMySQLQuery;
    QrReceiRecuNumero: TIntegerField;
    QrReceiRecuNome: TWideStringField;
    DsReceiRecu: TDataSource;
    QrReceirefu: TMySQLQuery;
    QrReceirefuNumero: TIntegerField;
    QrReceirefuNome: TWideStringField;
    DsReceiRefu: TDataSource;
    QrVSPWECabValorTDst: TFloatField;
    QrVSPWECabKgCouPQ: TFloatField;
    QrVSPWECabNFeStatus: TIntegerField;
    QrVSPWECabGGXSrc: TIntegerField;
    QrVSPWECabOperacoes: TIntegerField;
    QrVSPWECabVSVmcWrn: TSmallintField;
    QrVSPWECabVSVmcObs: TWideStringField;
    QrVSPWECabVSVmcSeq: TWideStringField;
    QrVSPWECabVSVmcSta: TSmallintField;
    QrVSPWECabAWServerID: TIntegerField;
    QrVSPWECabAWStatSinc: TSmallintField;
    QrVSPWECabLinCulReb: TIntegerField;
    QrVSPWECabLinCabReb: TIntegerField;
    QrVSPWECabLinCulSem: TIntegerField;
    QrVSPWECabLinCabSem: TIntegerField;
    QrVSPWECabDataPed: TDateTimeField;
    QrVSPWECabDataStart: TDateTimeField;
    QrVSPWECabDataCrust: TDateTimeField;
    QrVSPWECabDataExped: TDateTimeField;
    QrVSPWECabReceiRecu: TIntegerField;
    QrVSPWECabReceiRefu: TIntegerField;
    EdClasses: TdmkEdit;
    Label82: TLabel;
    QrVSPWECabClasses: TWideStringField;
    OPFluxoB1: TMenuItem;
    IncluidePedidoitemMPV1: TMenuItem;
    SbGraGruX: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SbVSArtCab: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSPWECabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSPWECabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSPWECabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtOriClick(Sender: TObject);
    procedure ItsExcluiOriIMEIClick(Sender: TObject);
    procedure ItsAlteraOriClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMOriPopup(Sender: TObject);
    procedure QrVSPWECabBeforeClose(DataSet: TDataSet);
    procedure CabLibera1Click(Sender: TObject);
    procedure QrVSPWECabCalcFields(DataSet: TDataSet);
    procedure CabReeditar1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure PeloCdigo1Click(Sender: TObject);
    procedure PeloIMEC1Click(Sender: TObject);
    procedure PeloIMEI1Click(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure IMEIArtigodeRibeiragerado1Click(Sender: TObject);
    procedure Outrasimpresses1Click(Sender: TObject);
    procedure ItsIncluiDstClick(Sender: TObject);
    procedure ItsAlteraDstClick(Sender: TObject);
    procedure ItsExcluiDstClick(Sender: TObject);
    procedure PMDstPopup(Sender: TObject);
    procedure BtDstClick(Sender: TObject);
    procedure AtualizaestoqueEmProcesso1Click(Sender: TObject);
    procedure QrVSPWEDstBeforeClose(DataSet: TDataSet);
    procedure QrVSPWEDstAfterScroll(DataSet: TDataSet);
    procedure Definiodequantidadesmanualmente1Click(Sender: TObject);
    procedure ItsExcluiOriPalletClick(Sender: TObject);
    procedure Parcial1Click(Sender: TObject);
    procedure otal1Click(Sender: TObject);
    procedure Parcial2Click(Sender: TObject);
    procedure otal2Click(Sender: TObject);
    procedure InformaNmerodaRME2Click(Sender: TObject);
    procedure InformaNmerodaRME1Click(Sender: TObject);
    procedure CorrigeFornecedoresdestinoapartirdestaoperao1Click(
      Sender: TObject);
    procedure CorrigirFornecedor1Click(Sender: TObject);
    procedure OrdensdeProduoemAberto1Click(Sender: TObject);
    procedure IrparajaneladoPallet1Click(Sender: TObject);
    procedure EdCustoMOM2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FornecedorMO1Click(Sender: TObject);
    procedure Localdoestoque1Click(Sender: TObject);
    procedure Cliente1Click(Sender: TObject);
    procedure OPpreenchida1Click(Sender: TObject);
    procedure CkCpermitirCrustClick(Sender: TObject);
    procedure IrparacadastrodoPallet1Click(Sender: TObject);
    procedure MudarodestinoparaoutraOP1Click(Sender: TObject);
    procedure SbVSCOPCabCadClick(Sender: TObject);
    procedure SbVSCOPCabCpyClick(Sender: TObject);
    procedure Adicionadesclassificado1Click(Sender: TObject);
    procedure QrVSPWEDesclDstAfterScroll(DataSet: TDataSet);
    procedure Removedesclassificado1Click(Sender: TObject);
    procedure Editaea1Click(Sender: TObject);
    procedure Corrigetodasbaixas1Click(Sender: TObject);
    procedure PMPesagemPopup(Sender: TObject);
    procedure ExcluiPesagem1Click(Sender: TObject);
    procedure Reimprimereceita1Click(Sender: TObject);
    procedure BtPesagemClick(Sender: TObject);
    procedure Recalculacusto1Click(Sender: TObject);
    procedure Emiteoutrasbaixas1Click(Sender: TObject);
    procedure Novogrupo1Click(Sender: TObject);
    procedure Novoitem1Click(Sender: TObject);
    procedure QrPQOBeforeClose(DataSet: TDataSet);
    procedure QrPQOAfterScroll(DataSet: TDataSet);
    procedure Excluiitem1Click(Sender: TObject);
    procedure Excluigrupo1Click(Sender: TObject);
    procedure EmitePesagem2Click(Sender: TObject);
    procedure AdicionasubprodutoWBRaspa1Click(Sender: TObject);
    procedure RemovesubprodutoWBRaspa1Click(Sender: TObject);
    procedure CorrigeMO1Click(Sender: TObject);
    procedure IncluiAtrelamento2Click(Sender: TObject);
    procedure AlteraAtrelamento2Click(Sender: TObject);
    procedure ExcluiAtrelamento2Click(Sender: TObject);
    procedure CobranadeMO1Click(Sender: TObject);
    procedure Recurtimento1Click(Sender: TObject);
    procedure Acabamento1Click(Sender: TObject);
    procedure OPpreenchidacomorigemedestino1Click(Sender: TObject);
    procedure AlteraLocaldeEstque1Click(Sender: TObject);
    procedure Corrigeartigodebaixa1Click(Sender: TObject);
    procedure Reabreitensteste1Click(Sender: TObject);
    procedure InformaPginaelinhadeIEC1Click(Sender: TObject);
    procedure QrVSMOEnvEnvAfterScroll(DataSet: TDataSet);
    procedure QrVSMOEnvEnvBeforeClose(DataSet: TDataSet);
    procedure QrVSMOEnvRetAfterScroll(DataSet: TDataSet);
    procedure QrVSMOEnvRetBeforeClose(DataSet: TDataSet);
    procedure IncluiAtrelamento1Click(Sender: TObject);
    procedure AlteraAtrelamento1Click(Sender: TObject);
    procedure ExcluiAtrelamento1Click(Sender: TObject);
    procedure OPFluxo1Click(Sender: TObject);
    procedure OPFluxoB1Click(Sender: TObject);
    procedure IncluidePedidoitemMPV1Click(Sender: TObject);
    procedure SbGraGruXClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SbVSArtCabClick(Sender: TObject);
  private
    FItsExcluiOriIMEI_Enabled, FItsExcluiOriPallet_Enabled,
    FItsIncluiOri_Enabled, FItsIncluiDst_Enabled, FAtualizando: Boolean;
    //
    function  CalculoCustoOrigemM2(): Double;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraVSPWEOriIMEI(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraVSPWEOriPall(SQLType: TSQLType; Quanto: TPartOuTodo);
    procedure MostraVSPWEDst(SQLType: TSQLType);
    procedure MostraVSPWEDescl(SQLType: TSQLType);
    procedure MostraVSPWESubPrd(SQLType: TSQLType; DesabilitaBaixa: Boolean);
    procedure MostraFormVSPWEAltOriIMEI(SQLType: TSQLType);
    procedure HabilitaEmpresa(SQLType: TSQLType);
    procedure ReopenVSPWEDesclDst(Controle, Pallet: Integer);
    procedure ReopenVSPWESubPrd(Controle, Pallet: Integer);
    //procedure ReopenForcados(Controle: Integer);
    procedure AtualizaFornecedoresDeDestino();
    procedure CorrigeFornecedorePalletsDestino(Reabre: Boolean);
    procedure PesquisaArtigoJaFeito(Key: Word);
    procedure ReopenEmit();
    procedure RecalculaCusto();
    procedure EmiteReceita(ReceitaSetor: TReceitaTipoSetor);
    procedure ReopenGraGruX();

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure AtualizaNFeItens();
    procedure ReabreGGX();
    procedure Altera();
    procedure InsereArtigoEmProcesso(SQLType: TSQLType; Codigo, MovimCod,
              Empresa, ClientMO, GraGruX, CliVenda: Integer; DataHora: String;
              FornecMO, StqCenLoc: Integer);
  end;

var
  FmVSPWECab: TFmVSPWECab;
const
  FFormatFloat = '00000';

implementation

uses Module, MyDBCheck, DmkDAC_PF, VSPWEOriPall, ModuleGeral, VSPWEDst,
  (*VSPWEEnc,*) VSPWEOriIMEI, AppListas, ModVS_CRC, UnGrade_Jan, UnApp_Jan,
  {$IfDef sAllVS} UnPQ_PF, UnVS_PF, {$EndIf}
  UnVS_CRC_PF, UnEntities, VSPWEDescl, VSPWESubPrd, VSPWEAltOriIMEI, MeuDBUses;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////

const
  FGerArX2Env = True;
  FGerArX2Ret = False;

procedure TFmVSPWECab.Localdoestoque1Click(Sender: TObject);
begin
  if VS_CRC_PF.AlteraVMI_StqCenLoc(QrVSPWEAtuControle.Value,
  QrVSPWEAtuStqCenLoc.Value) then
    LocCod(QrVSPWECabCodigo.Value, QrVSPWECabCodigo.Value);
end;

procedure TFmVSPWECab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSPWECab.MostraFormVSPWEAltOriIMEI(SQLType: TSQLType);
var
  Codigo: Integer;
  MaxPecas, MaxPesoKg, MaxAreaM2, MaxValorT: Double;
begin
  if SQLType <> stUpd then
  begin
    Geral.MB_Erro('Forma de SQL n�o implementada!');
    Exit;
  end;
  Codigo   := QrVSPWECabCodigo.Value;
  if DBCheck.CriaFm(TFmVSPWEAltOriIMEI, FmVSPWEAltOriIMEI, afmoNegarComAviso) then
  begin
    FmVSPWEAltOriIMEI.ImgTipo.SQLType := SQLType;
    FmVSPWEAltOriIMEI.FQrCab := QrVSPWECab;
    FmVSPWEAltOriIMEI.FDsCab := DsVSPWECab;
    FmVSPWEAltOriIMEI.FQrIts := QrVSPWEOriIMEI;
    FmVSPWEAltOriIMEI.FDsIts := DsVSPWEOriIMEI;
    if SQLType = stIns then
    begin
      //FmVSPWEAltOriIMEI.EdCPF1.ReadOnly := False
      FmVSPWEAltOriIMEI.FDataHora := QrVSPWEOriIMEIDataHora.Value;
    end else
    begin
      //FmVSPWEAltOriIMEI.FDataHora := QrVSPWEOriIMEIDataHora.Value;
      //
      VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(QrVSPWEOriIMEIDataHora.Value,
        QrVSPWEOriIMEIDtCorrApo.Value,  FmVSPWEAltOriIMEI.FDataHora);
      //
      FmVSPWEAltOriIMEI.EdControle.ValueVariant := QrVSPWEOriIMEIControle.Value;
      //
      FmVSPWEAltOriIMEI.EdGGXRcl.ValueVariant    := QrVSPWEOriIMEIGGXRcl.Value;
      FmVSPWEAltOriIMEI.CBGGXRcl.KeyValue        := QrVSPWEOriIMEIGGXRcl.Value;
      //
      FmVSPWEAltOriIMEI.EdPecas.ValueVariant     := -QrVSPWEOriIMEIPecas.Value;
      FmVSPWEAltOriIMEI.EdPesoKg.ValueVariant    := -QrVSPWEOriIMEIPesoKg.Value;
      FmVSPWEAltOriIMEI.EdAreaM2.ValueVariant    := -QrVSPWEOriIMEIAreaM2.Value;
      FmVSPWEAltOriIMEI.EdAreaP2.ValueVariant    := -QrVSPWEOriIMEIAreaP2.Value;
      FmVSPWEAltOriIMEI.EdValorT.ValueVariant    := -QrVSPWEOriIMEIValorT.Value;
      //
      VS_CRC_PF.ObtemSaldoFuturoIMEI(QrVSPWEOriIMEISrcNivel2.Value,
      -QrVSPWEOriIMEIPecas.Value, -QrVSPWEOriIMEIPesoKg.Value,
      -QrVSPWEOriIMEIAreaM2.Value, MaxPecas, MaxPesoKg, MaxAreaM2, MaxValorT);
      FmVSPWEAltOriIMEI.EdMaxPecas.ValueVariant  := MaxPecas;
      FmVSPWEAltOriIMEI.EdMaxPesoKg.ValueVariant := MaxPesoKg;
      FmVSPWEAltOriIMEI.EdMaxAreaM2.ValueVariant := MaxAreaM2;
      FmVSPWEAltOriIMEI.EdMaxValorT.ValueVariant := MaxValorT;
    end;
    FmVSPWEAltOriIMEI.ShowModal;
    FmVSPWEAltOriIMEI.Destroy;
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSPWECab.MostraVSPWEDescl(SQLType: TSQLType);
begin
  PCPWEDst.ActivePageIndex := 1;
  if DBCheck.CriaFm(TFmVSPWEDescl, FmVSPWEDescl, afmoNegarComAviso) then
  begin
    FmVSPWEDescl.ImgTipo.SQLType := SQLType;
    FmVSPWEDescl.FQrCab := QrVSPWEDesclDst;
    FmVSPWEDescl.FDsCab := DsVSPWEDesclDst;
    FmVSPWEDescl.FDsPWE := DsVSPWECab;
    FmVSPWEDescl.FQrIts := QrVSPWEDesclDst;
    //
    FmVSPWEDescl.EdPWEControle.ValueVariant := QrVSPWEAtuControle.Value;
    //FmVSPWEDescl.FDataHora    := QrVSPWECabDtHrAberto.Value;
    FmVSPWEDescl.FEmpresa     := QrVSPWECabEmpresa.Value;
    FmVSPWEDescl.FClientMO    := QrVSPWEAtuClientMO.Value;
    FmVSPWEDescl.FFornecMO    := QrVSPWEAtuFornecMO.Value;
    FmVSPWEDescl.FValM2       := CalculoCustoOrigemM2();
    //FmVSPWEDescl.FFatorIntSrc := QrVSPWEAtuFatorInt.Value;
    FmVSPWEDescl.FGraGruXSrc  := QrVSPWEAtuGraGruX.Value;
    //
    //FmVSPWEDescl.EdCustoMOM2.ValueVariant := QrVSPWEAtuCustoMOM2.Value;
    //
    if SQLType = stIns then
    begin
      FmVSPWEDescl.FSdoPecas  := QrVSPWEAtuSdoVrtPeca.Value;
      FmVSPWEDescl.FSdoPesoKg := QrVSPWEAtuSdoVrtPeso.Value;
      FmVSPWEDescl.FSdoReaM2  := QrVSPWEAtuSdoVrtArM2.Value;
      //
      //FmVSPWEDescl.EdCPF1.ReadOnly := False
      VS_CRC_PF.ReopenPedItsXXX(FmVSPWEDescl.QrVSPedIts, QrVSPWEAtuControle.Value,
      QrVSPWEAtuControle.Value);
      // Sugerir o Lib do Novo gerado!
      FmVSPWEDescl.EdPedItsFin.ValueVariant := QrVSPWEAtuPedItsLib.Value;
      FmVSPWEDescl.CBPedItsFin.KeyValue     := QrVSPWEAtuPedItsLib.Value;
      //
      //FmVSPWEDescl.TPData.Date              := QrVSPWECabDtHrAberto.Value;
      //FmVSPWEDescl.EdHora.ValueVariant      := QrVSPWECabDtHrAberto.Value;
      //
      //FmVSPWEDescl.EdGragruX.ValueVariant    := QrVSPWECabGGXDst.Value;
      //FmVSPWEDescl.CBGragruX.KeyValue        := QrVSPWECabGGXDst.Value;
    end else
    begin
      FmVSPWEDescl.FSdoPecas  := 0;
      FmVSPWEDescl.FSdoPesoKg := 0;
      FmVSPWEDescl.FSdoReaM2  := 0;
      //
      VS_CRC_PF.ReopenPedItsXXX(FmVSPWEDescl.QrVSPedIts, QrVSPWEAtuControle.Value,
       QrVSPWEDesclDstControle.Value);
      FmVSPWEDescl.EdCtrl1.ValueVariant      := QrVSPWEDesclDstControle.Value;
      //FmVSPWEDescl.EdCtrl2.ValueVariant     := VS_CRC_PF.ObtemControleMovimTwin(
        //QrVSPWEDesclDstMovimCod.Value, QrVSPWEDesclDstMovimTwn.Value, emidEmProcWE,
        //eminEmWEndBxa);
      FmVSPWEDescl.EdDSCMovimTwn.ValueVariant:= QrVSPWEDesclDstMovimTwn.Value;
      FmVSPWEDescl.EdGragruX.ValueVariant    := QrVSPWEDesclDstGraGruX.Value;
      FmVSPWEDescl.CBGragruX.KeyValue        := QrVSPWEDesclDstGraGruX.Value;
      FmVSPWEDescl.EdPecas.ValueVariant      := QrVSPWEDesclDstPecas.Value;
      FmVSPWEDescl.EdPesoKg.ValueVariant     := QrVSPWEDesclDstPesoKg.Value;
      FmVSPWEDescl.EdAreaM2.ValueVariant     := QrVSPWEDesclDstAreaM2.Value;
      FmVSPWEDescl.EdAreaP2.ValueVariant     := QrVSPWEDesclDstAreaP2.Value;
      FmVSPWEDescl.EdValorT.ValueVariant     := QrVSPWEDesclDstValorT.Value;
      FmVSPWEDescl.EdObserv.ValueVariant     := QrVSPWEDesclDstObserv.Value;
      FmVSPWEDescl.EdSerieFch.ValueVariant   := QrVSPWEDesclDstSerieFch.Value;
      FmVSPWEDescl.CBSerieFch.KeyValue       := QrVSPWEDesclDstSerieFch.Value;
      FmVSPWEDescl.EdFicha.ValueVariant      := QrVSPWEDesclDstFicha.Value;
      FmVSPWEDescl.EdMarca.ValueVariant      := QrVSPWEDesclDstMarca.Value;
      //FmVSPWEDescl.RGMisturou.ItemIndex    := QrVSPWEDesclDstMisturou.Value;
      FmVSPWEDescl.EdPedItsFin.ValueVariant  := QrVSPWEDesclDstPedItsFin.Value;
      FmVSPWEDescl.CBPedItsFin.KeyValue      := QrVSPWEDesclDstPedItsFin.Value;
      //
      //FmVSPWEDescl.TPData.Date               := QrVSPWEDesclDstDataHora.Value;
      //FmVSPWEDescl.EdHora.ValueVariant       := QrVSPWEDesclDstDataHora.Value;
      VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(QrVSPWEDesclDstDataHora.Value,
      QrVSPWEDesclDstDtCorrApo.Value,  FmVSPWEDescl.TPData, FmVSPWEDescl.EdHora);
      //
      FmVSPWEDescl.EdPedItsFin.ValueVariant  := QrVSPWEDesclDstPedItsFin.Value;
      FmVSPWEDescl.CBPedItsFin.KeyValue      := QrVSPWEDesclDstPedItsFin.Value;
      FmVSPWEDescl.EdPallet.ValueVariant     := QrVSPWEDesclDstPallet.Value;
      FmVSPWEDescl.CBPallet.KeyValue         := QrVSPWEDesclDstPallet.Value;
      FmVSPWEDescl.EdStqCenLoc.ValueVariant  := QrVSPWEDesclDstStqCenLoc.Value;
      FmVSPWEDescl.CBStqCenLoc.KeyValue      := QrVSPWEDesclDstStqCenLoc.Value;
      FmVSPWEDescl.EdReqMovEstq.ValueVariant := QrVSPWEDesclDstReqMovEstq.Value;
      FmVSPWEDescl.EdCustoMOM2.ValueVariant  := QrVSPWEDesclDstCustoMOM2.Value;
      FmVSPWEDescl.EdCustoMOTot.ValueVariant := QrVSPWEDesclDstCustoMOTot.Value;
      FmVSPWEDescl.EdValorT.ValueVariant     := QrVSPWEDesclDstValorT.Value;
      //FmVSPWEDescl.EdRendimento.ValueVariant := QrVSPWEDesclDstRendimento.Value;
      //
      if QrVSPWEDesclDstSdoVrtPeca.Value < QrVSPWEDesclDstPecas.Value then
      begin
        FmVSPWEDescl.EdGraGruX.Enabled        := False;
        FmVSPWEDescl.CBGraGruX.Enabled        := False;
      end;
      if QrVSPWEBxa.RecordCount > 0 then
      begin
        FmVSPWEDescl.CkBaixa.Checked          := True;
        FmVSPWEDescl.EdBxaPecas.ValueVariant  := -QrVSPWEBxaPecas.Value;
        FmVSPWEDescl.EdBxaPesoKg.ValueVariant := -QrVSPWEBxaPesoKg.Value;
        FmVSPWEDescl.EdBxaAreaM2.ValueVariant := -QrVSPWEBxaAreaM2.Value;
        FmVSPWEDescl.EdBxaAreaP2.ValueVariant := -QrVSPWEBxaAreaP2.Value;
        FmVSPWEDescl.EdBxaValorT.ValueVariant := -QrVSPWEBxaValorT.Value;
        FmVSPWEDescl.EdCtrl2.ValueVariant     := QrVSPWEBxaControle.Value;
        FmVSPWEDescl.EdBxaObserv.ValueVariant := QrVSPWEBxaObserv.Value;
      end;
      if QrVSPWEDesclDstSdoVrtPeca.Value < QrVSPWEDesclDstPecas.Value then
      begin
        FmVSPWEDescl.EdGraGruX.Enabled  := False;
        FmVSPWEDescl.CBGraGruX.Enabled  := False;
        FmVSPWEDescl.EdSerieFch.Enabled := False;
        FmVSPWEDescl.CBSerieFch.Enabled := False;
        FmVSPWEDescl.EdFicha.Enabled    := False;
      end;
    end;
    //
    FmVSPWEDescl.LaBxaPesoKg.Enabled  := QrVSPWECabPesoKgINI.Value <> 0;
    FmVSPWEDescl.EdBxaPesoKg.Enabled  := QrVSPWECabPesoKgINI.Value <> 0;
    FmVSPWEDescl.LaBxaAreaM2.Enabled  := QrVSPWECabAreaINIM2.Value <> 0;
    FmVSPWEDescl.EdBxaAreaM2.Enabled  := QrVSPWECabAreaINIM2.Value <> 0;
    FmVSPWEDescl.LaBxaAreaP2.Enabled  := QrVSPWECabAreaINIM2.Value <> 0;
    FmVSPWEDescl.EdBxaAreaP2.Enabled  := QrVSPWECabAreaINIM2.Value <> 0;
    //
    FmVSPWEDescl.ShowModal;
    FmVSPWEDescl.Destroy;
    //
    VS_CRC_PF.AtualizaFornecedorPWE(QrVSPWECabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSPWECab.MostraVSPWEDst(SQLType: TSQLType);
begin
  if not FItsIncluiDst_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
  if DBCheck.CriaFm(TFmVSPWEDst, FmVSPWEDst, afmoNegarComAviso) then
  begin
    FmVSPWEDst.ImgTipo.SQLType := SQLType;
    FmVSPWEDst.FQrCab := QrVSPWECab;
    FmVSPWEDst.FDsCab := DsVSPWECab;
    FmVSPWEDst.FQrIts := QrVSPWEDst;
    //
    //FmVSPWEDst.FDataHora    := QrVSPWECabDtHrAberto.Value;
    FmVSPWEDst.FEmpresa     := QrVSPWECabEmpresa.Value;
    FmVSPWEDst.FClientMO    := QrVSPWEAtuClientMO.Value;
    FmVSPWEDst.FFornecMO    := QrVSPWEAtuFornecMO.Value;
    FmVSPWEDst.FValM2       := CalculoCustoOrigemM2();
    //FmVSPWEDst.FFatorIntSrc := QrVSPWEAtuFatorInt.Value;
    FmVSPWEDst.FGraGruXSrc  := QrVSPWEAtuGraGruX.Value;
    //
    FmVSPWEDst.EdCustoMOM2.ValueVariant := QrVSPWEAtuCustoMOM2.Value;
    //
    if SQLType = stIns then
    begin
      FmVSPWEDst.FSdoPecas  := QrVSPWEAtuSdoVrtPeca.Value;
      FmVSPWEDst.FSdoPesoKg := QrVSPWEAtuSdoVrtPeso.Value;
      FmVSPWEDst.FSdoReaM2  := QrVSPWEAtuSdoVrtArM2.Value;
      //
      //FmVSPWEDst.EdCPF1.ReadOnly := False
      VS_CRC_PF.ReopenPedItsXXX(FmVSPWEDst.QrVSPedIts, QrVSPWEAtuControle.Value,
      QrVSPWEAtuControle.Value);
      // Sugerir o Lib do Novo gerado!
      FmVSPWEDst.EdPedItsFin.ValueVariant := QrVSPWEAtuPedItsLib.Value;
      FmVSPWEDst.CBPedItsFin.KeyValue     := QrVSPWEAtuPedItsLib.Value;
      //
      //FmVSPWEDst.TPData.Date              := QrVSPWECabDtHrAberto.Value;
      //FmVSPWEDst.EdHora.ValueVariant      := QrVSPWECabDtHrAberto.Value;
      //
      FmVSPWEDst.EdGragruX.ValueVariant    := QrVSPWECabGGXDst.Value;
      FmVSPWEDst.CBGragruX.KeyValue        := QrVSPWECabGGXDst.Value;
    end else
    begin
      SetLength(FmVSPWEDst.FPallOnEdit, 1);
      FmVSPWEDst.FPallOnEdit[0] := QrVSPWEDstPallet.Value;
      FmVSPWEDst.FSdoPecas  := 0;
      FmVSPWEDst.FSdoPesoKg := 0;
      FmVSPWEDst.FSdoReaM2  := 0;
      //
      VS_CRC_PF.ReopenPedItsXXX(FmVSPWEDst.QrVSPedIts, QrVSPWEAtuControle.Value,
       QrVSPWEDstControle.Value);
      FmVSPWEDst.EdCtrl1.ValueVariant      := QrVSPWEDstControle.Value;
      //FmVSPWEDst.EdCtrl2.ValueVariant     := VS_CRC_PF.ObtemControleMovimTwin(
        //QrVSPWEDstMovimCod.Value, QrVSPWEDstMovimTwn.Value, emidEmProcWE,
        //eminEmWEndBxa);
      FmVSPWEDst.EdMovimTwn.ValueVariant     := QrVSPWEDstMovimTwn.Value;
      FmVSPWEDst.EdGragruX.ValueVariant      := QrVSPWEDstGraGruX.Value;
      FmVSPWEDst.CBGragruX.KeyValue          := QrVSPWEDstGraGruX.Value;
      FmVSPWEDst.EdPecas.ValueVariant        := QrVSPWEDstPecas.Value;
      FmVSPWEDst.EdPesoKg.ValueVariant       := QrVSPWEDstPesoKg.Value;
      FmVSPWEDst.EdAreaM2.ValueVariant       := QrVSPWEDstAreaM2.Value;
      FmVSPWEDst.EdAreaP2.ValueVariant       := QrVSPWEDstAreaP2.Value;
      FmVSPWEDst.EdValorT.ValueVariant       := QrVSPWEDstValorT.Value;
      FmVSPWEDst.EdObserv.ValueVariant       := QrVSPWEDstObserv.Value;
      FmVSPWEDst.EdSerieFch.ValueVariant     := QrVSPWEDstSerieFch.Value;
      FmVSPWEDst.CBSerieFch.KeyValue         := QrVSPWEDstSerieFch.Value;
      FmVSPWEDst.EdFicha.ValueVariant        := QrVSPWEDstFicha.Value;
      FmVSPWEDst.EdMarca.ValueVariant        := QrVSPWEDstMarca.Value;
      //FmVSPWEDst.RGMisturou.ItemIndex      := QrVSPWEDstMisturou.Value;
      FmVSPWEDst.EdPedItsFin.ValueVariant    := QrVSPWEDstPedItsFin.Value;
      FmVSPWEDst.CBPedItsFin.KeyValue        := QrVSPWEDstPedItsFin.Value;
      //
      //FmVSPWEDst.TPData.Date               := QrVSPWEDstDataHora.Value;
      //FmVSPWEDst.EdHora.ValueVariant       := QrVSPWEDstDataHora.Value;
      VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(QrVSPWEDstDataHora.Value,
      QrVSPWEDstDtCorrApo.Value,  FmVSPWEDst.TPData, FmVSPWEDst.EdHora);
      //
      FmVSPWEDst.EdPedItsFin.ValueVariant    := QrVSPWEDstPedItsFin.Value;
      FmVSPWEDst.CBPedItsFin.KeyValue        := QrVSPWEDstPedItsFin.Value;
      FmVSPWEDst.EdPallet.ValueVariant       := QrVSPWEDstPallet.Value;
      FmVSPWEDst.CBPallet.KeyValue           := QrVSPWEDstPallet.Value;
      FmVSPWEDst.EdStqCenLoc.ValueVariant    := QrVSPWEDstStqCenLoc.Value;
      FmVSPWEDst.CBStqCenLoc.KeyValue        := QrVSPWEDstStqCenLoc.Value;
      FmVSPWEDst.EdReqMovEstq.ValueVariant   := QrVSPWEDstReqMovEstq.Value;
      FmVSPWEDst.EdCustoMOPc.ValueVariant    := QrVSPWEDstCustoMOPc.Value;
      FmVSPWEDst.EdCustoMOKg.ValueVariant    := QrVSPWEDstCustoMOKg.Value;
      FmVSPWEDst.EdCustoMOM2.ValueVariant    := QrVSPWEDstCustoMOM2.Value;
      FmVSPWEDst.EdCustoMOTot.ValueVariant   := QrVSPWEDstCustoMOTot.Value;
      FmVSPWEDst.EdCusFrtMORet.ValueVariant  := QrVSPWEDstCusFrtMORet.Value;
      FmVSPWEDst.EdValorT.ValueVariant       := QrVSPWEDstValorT.Value;
      //FmVSPWEDst.EdRendimento.ValueVariant := QrVSPWEDstRendimento.Value;
      //
      if QrVSPWEDstSdoVrtPeca.Value < QrVSPWEDstPecas.Value then
      begin
        FmVSPWEDst.EdGraGruX.Enabled        := False;
        FmVSPWEDst.CBGraGruX.Enabled        := False;
      end;
      if QrVSPWEBxa.RecordCount > 0 then
      begin
        FmVSPWEDst.CkBaixa.Checked          := True;
        FmVSPWEDst.EdBxaPecas.ValueVariant  := -QrVSPWEBxaPecas.Value;
        FmVSPWEDst.EdBxaPesoKg.ValueVariant := -QrVSPWEBxaPesoKg.Value;
        FmVSPWEDst.EdBxaAreaM2.ValueVariant := -QrVSPWEBxaAreaM2.Value;
        FmVSPWEDst.EdBxaAreaP2.ValueVariant := -QrVSPWEBxaAreaP2.Value;
        FmVSPWEDst.EdBxaValorT.ValueVariant := -QrVSPWEBxaValorT.Value;
        FmVSPWEDst.EdCtrl2.ValueVariant     := QrVSPWEBxaControle.Value;
        FmVSPWEDst.EdBxaObserv.ValueVariant := QrVSPWEBxaObserv.Value;
      end;
      if QrVSPWEDstSdoVrtPeca.Value < QrVSPWEDstPecas.Value then
      begin
        FmVSPWEDst.EdGraGruX.Enabled  := False;
        FmVSPWEDst.CBGraGruX.Enabled  := False;
        FmVSPWEDst.EdSerieFch.Enabled := False;
        FmVSPWEDst.CBSerieFch.Enabled := False;
        FmVSPWEDst.EdFicha.Enabled    := False;
      end;
      FmVSPWEDst.CalculaCusto();
    end;
    //
    FmVSPWEDst.LaBxaPesoKg.Enabled  := QrVSPWECabPesoKgINI.Value <> 0;
    FmVSPWEDst.EdBxaPesoKg.Enabled  := QrVSPWECabPesoKgINI.Value <> 0;
    FmVSPWEDst.LaBxaAreaM2.Enabled  := QrVSPWECabAreaINIM2.Value <> 0;
    FmVSPWEDst.EdBxaAreaM2.Enabled  := QrVSPWECabAreaINIM2.Value <> 0;
    FmVSPWEDst.LaBxaAreaP2.Enabled  := QrVSPWECabAreaINIM2.Value <> 0;
    FmVSPWEDst.EdBxaAreaP2.Enabled  := QrVSPWECabAreaINIM2.Value <> 0;
    //
    FmVSPWEDst.ShowModal;
    FmVSPWEDst.Destroy;
    //
    VS_CRC_PF.AtualizaFornecedorPWE(QrVSPWECabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSPWECab.MostraVSPWEOriIMEI(SQLType: TSQLType; Quanto: TPartOuTodo);
begin
  if DBCheck.CriaFm(TFmVSPWEOriIMEI, FmVSPWEOriIMEI, afmoNegarComAviso) then
  begin
    FmVSPWEOriIMEI.ImgTipo.SQLType := SQLType;
    FmVSPWEOriIMEI.FQrCab          := QrVSPWECab;
    FmVSPWEOriIMEI.FDsCab          := DsVSPWECab;
    //FmVSPWEOriIMEI.FVSPWEOriIMEIIMEI   := QrVSPWEOriIMEIIMEI;
    //FmVSPWEOriIMEI.FVSPWEOriIMEIIMEIet := QrVSPWEOriIMEIIMEIet;
    FmVSPWEOriIMEI.FEmpresa        := QrVSPWECabEmpresa.Value;
    FmVSPWEOriIMEI.FClientMO       := QrVSPWEAtuClientMO.Value;
    FmVSPWEOriIMEI.FFornecMO       := QrVSPWEAtuFornecMO.Value;
    FmVSPWEOriIMEI.FStqCenLoc      := QrVSPWEAtuStqCenLoc.Value;
    FmVSPWEOriIMEI.FTipoArea       := QrVSPWECabTipoArea.Value;
    FmVSPWEOriIMEI.FOrigMovimNiv   := QrVSPWEAtuMovimNiv.Value;
    FmVSPWEOriIMEI.FOrigMovimCod   := QrVSPWEAtuMovimCod.Value;
    FmVSPWEOriIMEI.FOrigCodigo     := QrVSPWEAtuCodigo.Value;
    FmVSPWEOriIMEI.FNewGraGruX     := QrVSPWEAtuGraGruX.Value;
    //
    FmVSPWEOriIMEI.EdCodigo.ValueVariant    := QrVSPWECabCodigo.Value;
    FmVSPWEOriIMEI.EdMovimCod.ValueVariant  := QrVSPWECabMovimCod.Value;
    //
    FmVSPWEOriIMEI.EdSrcMovID.ValueVariant  := QrVSPWEAtuMovimID.Value;
    FmVSPWEOriIMEI.EdSrcNivel1.ValueVariant := QrVSPWEAtuCodigo.Value;
    FmVSPWEOriIMEI.EdSrcNivel2.ValueVariant := QrVSPWEAtuControle.Value;
    FmVSPWEOriIMEI.EdSrcGGX.ValueVariant    := QrVSPWEAtuGraGruX.Value;
    //
    FmVSPWEOriIMEI.ReopenItensAptos();
    FmVSPWEOriIMEI.DefineTipoArea();
    //
    if SQLType = stIns then
    begin
      FmVSPWEOriIMEI.FDataHora                := QrVSPWECabDtHrAberto.Value;
    end else
    begin
      //
      VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(QrVSPWEOriIMEIDataHora.Value,
        QrVSPWEOriIMEIDtCorrApo.Value,  FmVSPWEOriIMEI.FDataHora);
      //
{
      FmVSPWEOriIMEI.EdControle.ValueVariant := QrVSPWECabOldControle.Value;
      //
      FmVSPWEOriIMEI.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrVSPWECabOldCNPJ_CPF.Value);
      FmVSPWEOriIMEI.EdNomeEmiSac.Text := QrVSPWECabOldNome.Value;
      FmVSPWEOriIMEI.EdCPF1.ReadOnly := True;
}
    end;
    case Quanto of
      ptParcial:
      begin
        FmVSPWEOriIMEI.FParcial := True;
        FmVSPWEOriIMEI.DBG04Estq.Options := FmVSPWEOriIMEI.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmVSPWEOriIMEI.FParcial := False;
        FmVSPWEOriIMEI.DBG04Estq.Options := FmVSPWEOriIMEI.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmVSPWEOriIMEI.FParcial := False;
        FmVSPWEOriIMEI.DBG04Estq.Options := FmVSPWEOriIMEI.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    FmVSPWEOriIMEI.ShowModal;
    FmVSPWEOriIMEI.Destroy;
    RecalculaCusto();
    VS_CRC_PF.AtualizaFornecedorPWE(QrVSPWECabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
    AtualizaNFeItens();
  end;
end;

procedure TFmVSPWECab.MostraVSPWEOriPall(SQLType: TSQLType; Quanto: TPartOuTodo);
begin
  if not FItsIncluiOri_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
  if DBCheck.CriaFm(TFmVSPWEOriPall, FmVSPWEOriPall, afmoNegarComAviso) then
  begin
    FmVSPWEOriPall.ImgTipo.SQLType := SQLType;
    FmVSPWEOriPall.FQrCab          := QrVSPWECab;
    FmVSPWEOriPall.FDsCab          := DsVSPWECab;
    //FmVSPWEOriPall.FVSPWEOriPallIMEI   := QrVSPWEOriPallIMEI;
    //FmVSPWEOriPall.FVSPWEOriPallPallet := QrVSPWEOriPallPallet;
    FmVSPWEOriPall.FEmpresa        := QrVSPWECabEmpresa.Value;
    FmVSPWEOriPall.FStqCenLoc      := QrVSPWEAtuStqCenLoc.Value;
    FmVSPWEOriPall.FClientMO       := QrVSPWEAtuClientMO.Value;
    FmVSPWEoriPall.FFornecMO       := QrVSPWEAtuFornecMO.Value;
    FmVSPWEOriPall.FTipoArea       := QrVSPWECabTipoArea.Value;
    FmVSPWEOriPall.FOrigMovimNiv   := QrVSPWEAtuMovimNiv.Value;
    FmVSPWEOriPall.FOrigMovimCod   := QrVSPWEAtuMovimCod.Value;
    FmVSPWEOriPall.FOrigCodigo     := QrVSPWEAtuCodigo.Value;
    FmVSPWEOriPall.FNewGraGruX     := QrVSPWEAtuGraGruX.Value;
    //
    FmVSPWEOriPall.EdCodigo.ValueVariant    := QrVSPWECabCodigo.Value;
    FmVSPWEOriPall.EdMovimCod.ValueVariant  := QrVSPWECabMovimCod.Value;
    //
    FmVSPWEOriPall.EdSrcMovID.ValueVariant  := QrVSPWEAtuMovimID.Value;
    FmVSPWEOriPall.EdSrcNivel1.ValueVariant := QrVSPWEAtuCodigo.Value;
    FmVSPWEOriPall.EdSrcNivel2.ValueVariant := QrVSPWEAtuControle.Value;
    FmVSPWEOriPall.EdSrcGGX.ValueVariant    := QrVSPWEAtuGraGruX.Value;
    //
    FmVSPWEOriPall.ReopenItensAptos();
    FmVSPWEOriPall.DefineTipoArea();
    //
    if SQLType = stIns then
    begin
      FmVSPWEOriPall.FDataHora                := QrVSPWECabDtHrAberto.Value;
    end else
    begin
      //
      VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(QrVSPWEOriIMEIDataHora.Value,
        QrVSPWEOriIMEIDtCorrApo.Value,  FmVSPWEOriPall.FDataHora);
      //
{
      FmVSPWEOriPall.EdControle.ValueVariant := QrVSPWECabOldControle.Value;
      //
      FmVSPWEOriPall.EdCPF1.Text := Geral.FormataCNPJ_TFT(QrVSPWECabOldCNPJ_CPF.Value);
      FmVSPWEOriPall.EdNomeEmiSac.Text := QrVSPWECabOldNome.Value;
      FmVSPWEOriPall.EdCPF1.ReadOnly := True;
}
    end;
    case Quanto of
      ptParcial:
      begin
        FmVSPWEOriPall.FParcial := True;
        FmVSPWEOriPall.DBG04Estq.Options := FmVSPWEOriPall.DBG04Estq.Options - [dgMultiSelect];
      end;
      ptTotal:
      begin
        FmVSPWEOriPall.FParcial := False;
        FmVSPWEOriPall.DBG04Estq.Options := FmVSPWEOriPall.DBG04Estq.Options + [dgMultiSelect];
      end;
      else
      begin
        Geral.MB_Aviso('"Quanto" n�o implementado!');
        FmVSPWEOriPall.FParcial := False;
        FmVSPWEOriPall.DBG04Estq.Options := FmVSPWEOriPall.DBG04Estq.Options - [dgMultiSelect];
      end;
    end;
    FmVSPWEOriPall.ShowModal;
    FmVSPWEOriPall.Destroy;
    RecalculaCusto();
    VS_CRC_PF.AtualizaFornecedorPWE(QrVSPWECabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
    AtualizaNFeItens();
  end;
end;

procedure TFmVSPWECab.MostraVSPWESubPrd(SQLType: TSQLType; DesabilitaBaixa: Boolean);
begin
  PCPWEDst.ActivePageIndex := 2;
  if DBCheck.CriaFm(TFmVSPWESubPrd, FmVSPWESubPrd, afmoNegarComAviso) then
  begin
    FmVSPWESubPrd.ImgTipo.SQLType := SQLType;
    FmVSPWESubPrd.FQrCab := QrVSPWESubPrd;
    FmVSPWESubPrd.FDsCab := DsVSPWESubPrd;
    FmVSPWESubPrd.FDsPWE := DsVSPWECab;
    FmVSPWESubPrd.FQrIts := QrVSPWESubPrd;
    //
    FmVSPWESubPrd.EdPWEControle.ValueVariant := QrVSPWEAtuControle.Value;
    //FmVSPWESubPrd.FDataHora    := QrVSPWECabDtHrAberto.Value;
    FmVSPWESubPrd.FEmpresa     := QrVSPWECabEmpresa.Value;
    FmVSPWESubPrd.FClientMO    := QrVSPWEAtuClientMO.Value;
    FmVSPWESubPrd.FFornecMO    := QrVSPWEAtuFornecMO.Value;
    FmVSPWESubPrd.FValM2       := CalculoCustoOrigemM2();
    //FmVSPWESubPrd.FFatorIntSrc := QrVSPWEAtuFatorInt.Value;
    FmVSPWESubPrd.FGraGruXSrc  := QrVSPWEAtuGraGruX.Value;
    //
    //FmVSPWESubPrd.EdCustoMOM2.ValueVariant := QrVSPWEAtuCustoMOM2.Value;
    //
    if SQLType = stIns then
    begin
      FmVSPWESubPrd.FSdoPecas  := QrVSPWEAtuSdoVrtPeca.Value;
      FmVSPWESubPrd.FSdoPesoKg := QrVSPWEAtuSdoVrtPeso.Value;
      FmVSPWESubPrd.FSdoReaM2  := QrVSPWEAtuSdoVrtArM2.Value;
      //
      //FmVSPWESubPrd.EdCPF1.ReadOnly := False
      VS_CRC_PF.ReopenPedItsXXX(FmVSPWESubPrd.QrVSPedIts, QrVSPWEAtuControle.Value,
      QrVSPWEAtuControle.Value);
      // Sugerir o Lib do Novo gerado!
      FmVSPWESubPrd.EdPedItsFin.ValueVariant := QrVSPWEAtuPedItsLib.Value;
      FmVSPWESubPrd.CBPedItsFin.KeyValue     := QrVSPWEAtuPedItsLib.Value;
      //
      //FmVSPWESubPrd.TPData.Date              := QrVSPWECabDtHrAberto.Value;
      //FmVSPWESubPrd.EdHora.ValueVariant      := QrVSPWECabDtHrAberto.Value;
      //
      //FmVSPWESubPrd.EdGragruX.ValueVariant    := QrVSPWECabGGXDst.Value;
      //FmVSPWESubPrd.CBGragruX.KeyValue        := QrVSPWECabGGXDst.Value;
    end else
    begin
      FmVSPWESubPrd.FSdoPecas  := 0;
      FmVSPWESubPrd.FSdoPesoKg := 0;
      FmVSPWESubPrd.FSdoReaM2  := 0;
      //
      VS_CRC_PF.ReopenPedItsXXX(FmVSPWESubPrd.QrVSPedIts, QrVSPWEAtuControle.Value,
       QrVSPWESubPrdControle.Value);
      FmVSPWESubPrd.EdCtrl1.ValueVariant      := QrVSPWESubPrdControle.Value;
      //FmVSPWESubPrd.EdCtrl2.ValueVariant     := VS_CRC_PF.ObtemControleMovimTwin(
        //QrVSPWESubPrdMovimCod.Value, QrVSPWESubPrdMovimTwn.Value, emidEmProcWE,
        //eminEmWEndBxa);
      FmVSPWESubPrd.EdDSCMovimTwn.ValueVariant:= QrVSPWESubPrdMovimTwn.Value;
      FmVSPWESubPrd.EdGragruX.ValueVariant    := QrVSPWESubPrdGraGruX.Value;
      FmVSPWESubPrd.CBGragruX.KeyValue        := QrVSPWESubPrdGraGruX.Value;
      FmVSPWESubPrd.EdPecas.ValueVariant      := QrVSPWESubPrdPecas.Value;
      FmVSPWESubPrd.EdPesoKg.ValueVariant     := QrVSPWESubPrdPesoKg.Value;
      FmVSPWESubPrd.EdAreaM2.ValueVariant     := QrVSPWESubPrdAreaM2.Value;
      FmVSPWESubPrd.EdAreaP2.ValueVariant     := QrVSPWESubPrdAreaP2.Value;
      FmVSPWESubPrd.EdValorT.ValueVariant     := QrVSPWESubPrdValorT.Value;
      FmVSPWESubPrd.EdObserv.ValueVariant     := QrVSPWESubPrdObserv.Value;
      FmVSPWESubPrd.EdSerieFch.ValueVariant   := QrVSPWESubPrdSerieFch.Value;
      FmVSPWESubPrd.CBSerieFch.KeyValue       := QrVSPWESubPrdSerieFch.Value;
      FmVSPWESubPrd.EdFicha.ValueVariant      := QrVSPWESubPrdFicha.Value;
      FmVSPWESubPrd.EdMarca.ValueVariant      := QrVSPWESubPrdMarca.Value;
      //FmVSPWESubPrd.RGMisturou.ItemIndex    := QrVSPWESubPrdMisturou.Value;
      FmVSPWESubPrd.EdPedItsFin.ValueVariant  := QrVSPWESubPrdPedItsFin.Value;
      FmVSPWESubPrd.CBPedItsFin.KeyValue      := QrVSPWESubPrdPedItsFin.Value;
      //
      //FmVSPWESubPrd.TPData.Date               := QrVSPWESubPrdDataHora.Value;
      //FmVSPWESubPrd.EdHora.ValueVariant       := QrVSPWESubPrdDataHora.Value;
      VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(QrVSPWESubPrdDataHora.Value,
      QrVSPWESubPrdDtCorrApo.Value,  FmVSPWESubPrd.TPData, FmVSPWESubPrd.EdHora);
      //
      FmVSPWESubPrd.EdPedItsFin.ValueVariant  := QrVSPWESubPrdPedItsFin.Value;
      FmVSPWESubPrd.CBPedItsFin.KeyValue      := QrVSPWESubPrdPedItsFin.Value;
      FmVSPWESubPrd.EdPallet.ValueVariant     := QrVSPWESubPrdPallet.Value;
      FmVSPWESubPrd.CBPallet.KeyValue         := QrVSPWESubPrdPallet.Value;
      FmVSPWESubPrd.EdStqCenLoc.ValueVariant  := QrVSPWESubPrdStqCenLoc.Value;
      FmVSPWESubPrd.CBStqCenLoc.KeyValue      := QrVSPWESubPrdStqCenLoc.Value;
      FmVSPWESubPrd.EdReqMovEstq.ValueVariant := QrVSPWESubPrdReqMovEstq.Value;
      FmVSPWESubPrd.EdCustoMOM2.ValueVariant  := QrVSPWESubPrdCustoMOM2.Value;
      FmVSPWESubPrd.EdCustoMOTot.ValueVariant := QrVSPWESubPrdCustoMOTot.Value;
      FmVSPWESubPrd.EdValorT.ValueVariant     := QrVSPWESubPrdValorT.Value;
      //FmVSPWESubPrd.EdRendimento.ValueVariant := QrVSPWESubPrdRendimento.Value;
      //
      if QrVSPWESubPrdSdoVrtPeca.Value < QrVSPWESubPrdPecas.Value then
      begin
        FmVSPWESubPrd.EdGraGruX.Enabled        := False;
        FmVSPWESubPrd.CBGraGruX.Enabled        := False;
      end;
      (*
      if QrVSPWEBxa.RecordCount > 0 then
      begin*)
        FmVSPWESubPrd.CkBaixa.Checked          := False;
        (*FmVSPWESubPrd.EdBxaPecas.ValueVariant  := -QrVSPWEBxaPecas.Value;
        FmVSPWESubPrd.EdBxaPesoKg.ValueVariant := -QrVSPWEBxaPesoKg.Value;
        FmVSPWESubPrd.EdBxaAreaM2.ValueVariant := -QrVSPWEBxaAreaM2.Value;
        FmVSPWESubPrd.EdBxaAreaP2.ValueVariant := -QrVSPWEBxaAreaP2.Value;
        FmVSPWESubPrd.EdBxaValorT.ValueVariant := -QrVSPWEBxaValorT.Value;
        FmVSPWESubPrd.EdCtrl2.ValueVariant     := QrVSPWEBxaControle.Value;
        FmVSPWESubPrd.EdBxaObserv.ValueVariant := QrVSPWEBxaObserv.Value;
      end;*)
      if QrVSPWESubPrdSdoVrtPeca.Value < QrVSPWESubPrdPecas.Value then
      begin
        FmVSPWESubPrd.EdGraGruX.Enabled  := False;
        FmVSPWESubPrd.CBGraGruX.Enabled  := False;
        FmVSPWESubPrd.EdSerieFch.Enabled := False;
        FmVSPWESubPrd.CBSerieFch.Enabled := False;
        FmVSPWESubPrd.EdFicha.Enabled    := False;
      end;
    end;
    //
    FmVSPWESubPrd.LaBxaPesoKg.Enabled  := QrVSPWECabPesoKgINI.Value <> 0;
    FmVSPWESubPrd.EdBxaPesoKg.Enabled  := QrVSPWECabPesoKgINI.Value <> 0;
    FmVSPWESubPrd.LaBxaAreaM2.Enabled  := QrVSPWECabAreaINIM2.Value <> 0;
    FmVSPWESubPrd.EdBxaAreaM2.Enabled  := QrVSPWECabAreaINIM2.Value <> 0;
    FmVSPWESubPrd.LaBxaAreaP2.Enabled  := QrVSPWECabAreaINIM2.Value <> 0;
    FmVSPWESubPrd.EdBxaAreaP2.Enabled  := QrVSPWECabAreaINIM2.Value <> 0;
    //
    FmVSPWESubPrd.ShowModal;
    FmVSPWESubPrd.Destroy;
    //
    VS_CRC_PF.AtualizaFornecedorPWE(QrVSPWECabMovimCod.Value);
    CorrigeFornecedorePalletsDestino(True);
  end;
end;

procedure TFmVSPWECab.MudarodestinoparaoutraOP1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de OP';
  Prompt = 'Informe o n�mero da OP';
  Campo  = 'Descricao';
var
  //Terceiro, Controle, ThisCtrl: Integer;
  OP, Codigo, MovimCod, Controle, Ctrl1: Integer;
  Resp: Variant;
  Qry: TmySQLQuery;
begin
  if not DBCheck.LiberaPelaSenhaBoss then
    Exit;
  Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT cab.Codigo, cab.Nome Descricao ',
    'FROM vspwecab cab ',
    'ORDER BY Descricao ',
    ''], Dmod.MyDB, True);
  if Resp <> Null then
  begin
    OP := Resp;
    if OP <> 0 then
    begin
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * FROM vspwecab ',
        'WHERE Codigo=' + Geral.FF0(OP),
        '']);
        if Qry.recordCount > 0 then
        begin
          Codigo   := Qry.FieldByName('Codigo').AsInteger;
          MovimCod := Qry.FieldByName('MovimCod').AsInteger;
          //
          Controle := QrVSPWEBxaControle.Value;
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
          'Codigo', 'MovimCod'], [
          'Controle'], [
          Codigo, MovimCod], [
          Controle], True) then
          begin
            Ctrl1 := Controle;
            Controle := QrVSPWEDstControle.Value;
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
            'Codigo', 'MovimCod'], [
            'Controle'], [
            Codigo, MovimCod], [
            Controle], True) then
            begin
              VS_CRC_PF.AtualizaTotaisVSPWECab(MovimCod);
              VS_CRC_PF.AtualizaTotaisVSPWECab(QrVSPWECabMovimCod.Value);
              VS_CRC_PF.AtualizaNF_IMEI(QrVSPWECabMovimCod.Value, Ctrl1);
              VS_CRC_PF.AtualizaNF_IMEI(QrVSPWECabMovimCod.Value, Controle);
              //
              LocCod(QrVSPWECabMovimCod.Value, QrVSPWECabMovimCod.Value);
            end;
          end;
        end;
      finally
        Qry.Free;
      end;
    end;
  end;
end;

procedure TFmVSPWECab.Novogrupo1Click(Sender: TObject);
const
  PrecisaEmitGru = True;
var
  Codigo: Integer;
begin
{$IfDef sAllVS}
  PCPWEOri.ActivePageIndex := 3;
  if PQ_PF.MostraFormVSPQOCab(stIns, QrVSPWECabMovimCod.Value,
  QrVSPWECabEmitGru.Value, PrecisaEmitGru, Codigo) then
  begin
    VS_EFD_ICMS_IPI.ReopenVSMovCod_PQO(QrPQO, QrVSPWECabMovimCod.Value, Codigo);
    Novoitem1Click(Sender);
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPWECab.Novoitem1Click(Sender: TObject);
var
  Controle: Integer;
begin
{$IfDef sAllVS}
  PCPWEOri.ActivePageIndex := 3;
  if PQ_PF.MostraFormVSPQOIts(stIns, QrPQOCodigo.Value,
  QrPQODataB.Value, Controle) then
  begin
    RecalculaCusto();
    VS_EFD_ICMS_IPI.ReopenVSMovCod_PQOIts(QrPQOIts, QrPQOCodigo.Value, Controle);
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPWECab.OPFluxo1Click(Sender: TObject);
begin
  VS_CRC_PF.ImprimePWE_OP_Fluxo(QrVSPWECabCodigo.Value, 1);
end;

procedure TFmVSPWECab.OPFluxoB1Click(Sender: TObject);
begin
  VS_CRC_PF.ImprimePWE_OP_Fluxo(QrVSPWECabCodigo.Value, 2);
end;

procedure TFmVSPWECab.OPpreenchida1Click(Sender: TObject);
var
  NFeRem: String;
begin
  if QrVSPWECabNFeRem.Value <> 0 then
    NFeRem := Geral.FF0(QrVSPWECabNFeRem.Value)
  else
    NFeRem := '';
  VS_CRC_PF.ImprimeIMEI([QrVSPWEAtuControle.Value], viikOPPreenchida, QrVSPWECabLPFMO.Value,
    NFeRem, QrVSPWECab);
end;

procedure TFmVSPWECab.OPpreenchidacomorigemedestino1Click(Sender: TObject);
begin
  VS_CRC_PF.ImprimeOrdem(emidEmProcWE, QrVSPWECabEmpresa.Value,
  QrVSPWECabCodigo.Value, QrVSPWECabMovimCod.Value);
end;

procedure TFmVSPWECab.OrdensdeProduoemAberto1Click(Sender: TObject);
begin
  VS_CRC_PF.ImprimeOXsAbertas(0, 1, 2, 2, [TEstqMovimID.emidEmProcWE], True, False, '', 0);
end;

procedure TFmVSPWECab.otal1Click(Sender: TObject);
begin
  MostraVSPWEOriPall(stIns, ptTotal);
end;

procedure TFmVSPWECab.otal2Click(Sender: TObject);
begin
  MostraVSPWEOriIMEI(stIns, ptTotal);
end;

procedure TFmVSPWECab.Outrasimpresses1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSPWECab.Parcial1Click(Sender: TObject);
begin
  MostraVSPWEOriPall(stIns, ptParcial);
end;

procedure TFmVSPWECab.Parcial2Click(Sender: TObject);
begin
  MostraVSPWEOriIMEI(stIns, ptParcial);
end;

procedure TFmVSPWECab.PeloCdigo1Click(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSPWECabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSPWECab.PeloIMEC1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_CRC_PF.LocalizaPeloIMEC(emidEmProcWE);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSPWECab.PeloIMEI1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := VS_CRC_PF.LocalizaPeloIMEI(emidEmProcWE);
  if Codigo <> 0 then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSPWECab.PesquisaArtigoJaFeito(Key: Word);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  if Key = VK_F4 then
  begin
    Localizou := VS_CRC_PF.MostraFormVSRMPPsq(emidEmProcWE, [eminEmWEndInn], stCpy,
    Codigo, Controle, False);
    //
    if (Controle <> 0) and (DmModVS_CRC.QrIMEI.State <> dsInactive) then
    begin
      EdGraGruX.ValueVariant   := DmModVS_CRC.QrIMEIGraGruX.Value;
      CBGraGruX.KeyValue       := DmModVS_CRC.QrIMEIGraGruX.Value;
      EdFornecMO.ValueVariant  := DmModVS_CRC.QrIMEIFornecMO.Value;
      CBFornecMO.KeyValue      := DmModVS_CRC.QrIMEIFornecMO.Value;
      EdStqCenLoc.ValueVariant := DmModVS_CRC.QrIMEIStqCenLoc.Value;
      CBStqCenLoc.KeyValue     := DmModVS_CRC.QrIMEIStqCenLoc.Value;
      EdCustoMOM2.ValueVariant := DmModVS_CRC.QrIMEICustoMOM2.Value;
      EdCliente.ValueVariant   := DmModVS_CRC.QrIMEICliVenda.Value;
      CBCliente.KeyValue       := DmModVS_CRC.QrIMEICliVenda.Value;
    end;
  end;
end;

procedure TFmVSPWECab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSPWECab);
  MyObjects.HabilitaMenuItemCabDelC1I4(CabExclui1, QrVSPWECab, QrVSPWEOriIMEI,
    QrVSPWEOriPallet, QrVSPWEDst, QrEmit);
  MyObjects.HabilitaMenuItemCabUpd(CabLibera1, QrVSPWECab);
  MyObjects.HabilitaMenuItemCabUpd(CabReeditar1, QrVSPWECab);
  if QrVSPWECabDtHrFimOpe.Value > 2 then
  begin
    CabLibera1.Enabled := False;
    CabReeditar1.Enabled := False;
  end else
  if QrVSPWECabDtHrLibOpe.Value < 2 then
  begin
    CabReeditar1.Enabled := False;
  end;
  //
  //VS_CRC_PF.HabilitaMenuItensVSAberto(QrVSPWECab, QrVSPWECabDtHrAberto.Value, [CabAltera1, CabExclui1]);
end;

procedure TFmVSPWECab.PMDstPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiDst, QrVSPWECab);
  FItsIncluiDst_Enabled := ItsIncluiDst.Enabled;
  MyObjects.HabilitaMenuItemItsUpd(ItsAlteraDst, QrVSPWEDst);
  MyObjects.HabilitaMenuItemCabDel(ItsExcluiDst, QrVSPWEDst, QrVSMOEnvRet);
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME2, QrVSPWEDst);

  MyObjects.HabilitaMenuItemItsIns(Adicionadesclassificado1, QrVSPWECab);
  MyObjects.HabilitaMenuItemItsDel(Removedesclassificado1, QrVSPWEDesclDst);

(* Movidos para os click dos itens de menu!!!
  if (QrVSPWECabDtHrLibOpe.Value > 2) or (QrVSPWECabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiDst.Enabled := False;
    ItsAlteraDst.Enabled := False;
    ItsExcluiDst.Enabled := False;
  end;
*)
  VS_CRC_PF.HabilitaComposVSAtivo(QrVSPWEDstID_TTW.Value, [ItsExcluiDst]);
  MyObjects.HabilitaMenuItemItsUpd(Editaea1, QrVSPWEBxa);
  Corrigetodasbaixas1.Enabled :=
    (QrVSPWECab.State <> dsInactive) and (QrVSPWECabAreaSdoM2.Value < 0);
  //
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento1, QrVSPWEDst);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento1, QrVSMOEnvRet);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento1, QrVSMOEnvRet);
  //
  if (QrVSPWECabDtHrLibOpe.Value > 2) or (QrVSPWECabDtHrFimOpe.Value > 2) then
    ItsIncluiDst.Enabled := False;
  if FItsIncluiDst_Enabled then
  begin
    FItsIncluiDst_Enabled := ItsIncluiDst.Enabled;
    ItsIncluiDst.Enabled  := True;
  end;
  //AlteraLocaldeEstque1.Enabled := ?;
  Corrigeartigodebaixa1.Enabled := (QrVSPWEBxa.State <> dsInactive) and
    (QrVSPWEBxa.RecordCount > 0) and
    (QrVSPWEBxaGraGruX.Value <> QrVSPWEAtuGraGruX.Value);
  //
  AtrelamentoNFsdeMO2.Enabled := PCPWEOri.ActivePage = TsRetornoMO;
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento2, QrVSPWECab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento2, QrVSMOEnvRet);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento2, QrVSMOEnvRet);
  //
end;

procedure TFmVSPWECab.PMOriPopup(Sender: TObject);
begin
  //ver como vai fazer se precisar desfazer liberacao!
  MyObjects.HabilitaMenuItemItsIns(ItsIncluiOri, QrVSPWECab);
  FItsIncluiOri_Enabled := ItsIncluiOri.Enabled;
  MyObjects.HabilitaMenuItemItsUpd(ItsAlteraOri, QrVSPWEOriIMEI);
  MyObjects.HabilitaMenuItemItsUpd(InformaNmerodaRME1, QrVSPWEOriIMEI);
  MyObjects.HabilitaMenuItemItsUpd(InformaPginaelinhadeIEC1, QrVSPWEOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriIMEI, QrVSPWEOriIMEI);
  MyObjects.HabilitaMenuItemItsDel(ItsExcluiOriPallet, QrVSPWEOriPallet);
  if (QrVSPWECabDtHrLibOpe.Value > 2) or (QrVSPWECabDtHrFimOpe.Value > 2) then
  begin
    ItsIncluiOri.Enabled := False;
    ItsAlteraOri.Enabled := False;
    ItsExcluiOriIMEI.Enabled := False;
    ItsExcluiOriPallet.Enabled := False;
  end;
  InformaNmerodaRME1.Enabled := InformaNmerodaRME1.Enabled and
    (PCPWEOri.ActivePageIndex = 1);
  InformaPginaelinhadeIEC1.Enabled := InformaNmerodaRME1.Enabled;
  //
  VS_CRC_PF.HabilitaComposVSAtivo(QrVSPWEOriIMEIID_TTW.Value, [ItsIncluiOri, ItsExcluiOriIMEI]);
  VS_CRC_PF.HabilitaComposVSAtivo(QrVSPWEOriPalletID_TTW.Value, [ItsIncluiOri, ItsExcluiOriPallet]);
  //
  FItsExcluiOriIMEI_Enabled   := ItsExcluiOriIMEI.Enabled;
  ItsExcluiOriIMEI.Enabled    := True;
  FItsExcluiOriPallet_Enabled := ItsExcluiOriPallet.Enabled;
  ItsExcluiOriPallet.Enabled  := True;
  case PCPWEOri.ActivePageIndex of
    0: ItsExcluiOriIMEI.Enabled := False;
    1: ItsExcluiOriPallet.Enabled := False;
  end;
  if FItsIncluiOri_Enabled then
  begin
    FItsIncluiOri_Enabled := ItsIncluiOri.Enabled;
    ItsIncluiOri.Enabled  := True;
  end;
  ItsAlteraOri.Enabled := ((QrVSPWECabPecasSdo.Value <> 0) or
    ItsAlteraOri.Enabled) and (PCPWEOri.ActivePageIndex = 1);
  //
  AtrelamentoNFsdeMO1.Enabled := PCPWEOri.ActivePage = TsEnvioMO;
  MyObjects.HabilitaMenuItemItsIns(IncluiAtrelamento1, QrVSPWEOriIMEI);
  MyObjects.HabilitaMenuItemItsUpd(AlteraAtrelamento1, QrVSMOEnvEnv);
  MyObjects.HabilitaMenuItemItsDel(ExcluiAtrelamento1, QrVSMOEnvEnv);
  //
end;

procedure TFmVSPWECab.PMPesagemPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(EmitePesagem2, QrVSPWECab);
  MyObjects.HabilitaMenuItemItsIns(Emiteoutrasbaixas1, QrVSPWECab);
  MyObjects.HabilitaMenuItemItsIns(Novoitem1, QrPQO);
  MyObjects.HabilitaMenuItemItsUpd(Reimprimereceita1, QrEmit);
  MyObjects.HabilitaMenuItemItsDel(ExcluiPesagem1, QrEmit);
  MyObjects.HabilitaMenuItemItsDel(ExcluiItem1, QrPQOIts);
  MyObjects.HabilitaMenuItemItsDel(ExcluiGrupo1, QrPQO);
end;

procedure TFmVSPWECab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSPWECabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSPWECab.DefParams;
begin
  VAR_GOTOTABELA := 'vspwecab';
  VAR_GOTOMYSQLTABLE := QrVSPWECab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT vcc.Nome NO_VSCOPCab, ');
  VAR_SQLx.Add('IF(voc.PecasMan<>0, voc.PecasMan, -voc.PecasSrc) PecasINI,');
  VAR_SQLx.Add('IF(voc.AreaManM2<>0, voc.AreaManM2, -voc.AreaSrcM2) AreaINIM2,');
  VAR_SQLx.Add('IF(voc.AreaManP2<>0, voc.AreaManP2, -voc.AreaSrcP2) AreaINIP2,');
  VAR_SQLx.Add('IF(voc.PesoKgMan<>0, voc.PesoKgMan, -voc.PesoKgSrc) PesoKgINI,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, ');
  VAR_SQLx.Add('egr.Nome NO_EmitGru, vac.Nome NO_VSArtCab, ');
  VAR_SQLx.Add(VS_CRC_PF.SQL_NO_GGX_DST());
  VAR_SQLx.Add('voc.*');
  VAR_SQLx.Add('FROM vspwecab voc');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=voc.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=voc.Cliente');
  VAR_SQLx.Add('LEFT JOIN vscopcab  vcc ON vcc.Codigo=voc.VSCOPCab');
  VAR_SQLx.Add('LEFT JOIN emitgru   egr ON egr.Codigo=voc.EmitGru');
  VAR_SQLx.Add('LEFT JOIN vsartcab  vac ON vac.Codigo=voc.VSArtCab');
  VAR_SQLx.Add(VS_CRC_PF.SQL_LJ_GGX_DST('voc'));
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE voc.Codigo > 0');
  //
  VAR_SQL1.Add('AND voc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND voc.Nome Like :P0');
  //
end;

procedure TFmVSPWECab.EdClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSPWECab.EdCustoMOM2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSPWECab.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSPWECab.Editaea1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  PCPWEDst.ActivePageIndex := 0;
  PCPWEDestSub.ActivePageIndex := 0;
  //
  if VS_CRC_PF.AlteraVMI_AreaM2(QrVSPWEBxaMovimID.Value,
  QrVSPWEBxaMovimNiv.Value, QrVSPWEBxaControle.Value,
  QrVSPWEBxaAreaM2.Value, siNegativo) then
  begin
    VS_CRC_PF.AtualizaSaldoVirtualVSMovIts_Generico(QrVSPWEAtuControle.Value,
    QrVSPWEAtuMovimID.Value, QrVSPWEAtuMovimNiv.Value);
    LocCod(QrVSPWECabCodigo.Value, QrVSPWECabCodigo.Value);
  end;
end;

procedure TFmVSPWECab.Emiteoutrasbaixas1Click(Sender: TObject);
begin
  PCPWEOri.ActivePageIndex := 3;
end;

procedure TFmVSPWECab.EmitePesagem2Click(Sender: TObject);
begin
  PCPWEOri.ActivePageIndex := 2;
end;

procedure TFmVSPWECab.EmiteReceita(ReceitaSetor: TReceitaTipoSetor);
begin
{$IfDef sAllVS}
  case ReceitaSetor of
    rectipsetrRibeira:
      PQ_PF.MostraFormVSFormulasImp_BH(QrVSPWECabMovimCod.Value,
        QrVSPWEAtuControle.Value, QrVSPWECabTemIMEIMrt.Value,
        //QrVSPWECabEmitGru.Value,
        eminEmWEndInn, eminSorcWEnd, recribsetNaoAplic);
    rectipsetrRecurtimento:
      PQ_PF.MostraFormVSFormulasImp_WE(QrVSPWECabMovimCod.Value,
        QrVSPWEAtuControle.Value, QrVSPWECabTemIMEIMrt.Value,
        QrVSPWECabEmitGru.Value,
        eminEmWEndInn, eminSorcWEnd);
    rectipsetrAcabamento:
      PQ_PF.MostraFormVSFormulasImp_FI(QrVSPWECabMovimCod.Value,
        QrVSPWEAtuControle.Value, QrVSPWECabTemIMEIMrt.Value,
        QrVSPWECabEmitGru.Value,
        eminEmWEndInn, eminSorcWEnd);
    else Geral.MB_Aviso('Setor sem tipo de receita definida!');
  end;
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSPWECabMovimCod.Value);
  VS_CRC_PF.AtualizaTotaisVSPWECab(QrVSPWECabMovimCod.Value);
  LocCod(QrVSPWECabCodigo.Value, QrVSPWECabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPWECab.ExcluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  //PCOpeOri.ActivePageIndex := 2;
  PCPWEOri.ActivePage := TsEnvioMO;
  //
  DmModVS_CRC.ExcluiAtrelamentoMOEnvEnv(QrVSMOEnvEnv, QrVSMOEnvEVMI);
  //
  VS_CRC_PF.AtualizaTotaisVSPWECab(QrVSPWECabMovimCod.Value);
  //
  LocCod(QrVSPWECabCodigo.Value,QrVSPWECabCodigo.Value);
  //
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPWECab.ExcluiAtrelamento2Click(Sender: TObject);
begin
  DmModVS_CRC.ExcluiAtrelamentoMOEnvRet(QrVSMOEnvRet, QrVSMOEnvGVMI, QrVSMOEnvRVMI);
  //
  VS_CRC_PF.AtualizaTotaisVSPWECab(QrVSPWECabMovimCod.Value);
  //
  LocCod(QrVSPWECabCodigo.Value,QrVSPWECabCodigo.Value);
  //
end;

procedure TFmVSPWECab.Excluigrupo1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PCPWEOri.ActivePageIndex := 3;
  if QrPQOCodigo.Value <> 0 then
  begin
    PQ_PF.PQO_ExcluiCab(QrPQOCodigo.Value);
    RecalculaCusto();
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPWECab.Excluiitem1Click(Sender: TObject);
const
  Pergunta = True;
  AtzCusto = True;
begin
{$IfDef sAllVS}
  PCPWEOri.ActivePageIndex := 3;
  if QrPQOItsOrigemCtrl.Value <> 0 then
  begin
    PQ_PF.PQO_ExcluiItem(QrPQOItsOrigemCtrl.Value, Pergunta, AtzCusto);
    RecalculaCusto();
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPWECab.ExcluiPesagem1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  if PQ_PF.ExcluiPesagem(QrEmitCodigo.Value, VAR_FATID_0110, PB1) then
    LocCod(QrVSPWECabCodigo.Value, QrVSPWECabCodigo.Value);
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSPWECabMovimCod.Value);
  VS_CRC_PF.AtualizaTotaisVSPWECab(QrVSPWECabMovimCod.Value);
  LocCod(QrVSPWECabCodigo.Value, QrVSPWECabCodigo.Value);
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPWECab.CabLibera1Click(Sender: TObject);
var
  Codigo: Integer;
begin
{
  Codigo := QrVSPWECabCodigo.Value;
  //colocar metragem e encerrar!
  if DBCheck.CriaFm(TFmVSGerArtEnc, FmVSGerArtEnc, afmoNegarComAviso) then
  begin
    FmVSGerArtEnc.ImgTipo.SQLType := stUpd;
    //
    FmVSGerArtEnc.EdCodigo.ValueVariant := Codigo;
    FmVSGerArtEnc.EdMovimCod.ValueVariant := QrVSPWEAtuMovimCod.Value;
    FmVSGerArtEnc.EdControle.ValueVariant := QrVSPWEAtuControle.Value;
    //
    FmVSGerArtEnc.EdPecas.ValueVariant      := QrVSPWEAtuPecas.Value;
    FmVSGerArtEnc.EdAreaM2.ValueVariant     := QrVSPWEAtuAreaM2.Value;
    FmVSGerArtEnc.EdAreaP2.ValueVariant     := QrVSPWEAtuAreaP2.Value;
    FmVSGerArtEnc.EdCustoMOKg.ValueVariant  := QrVSPWEAtuCustoMOKg.Value;
      //  Devem ser os �ltimos? Na ordem abaixo?
    FmVSGerArtEnc.EdCustoMOTot.ValueVariant := QrVSPWEAtuCustoMOTot.Value;
    FmVSGerArtEnc.EdValorMP.ValueVariant    := QrVSPWEAtuValorMP.Value;
    FmVSGerArtEnc.EdValorT.ValueVariant     := QrVSPWEAtuValorT.Value;

    //
    FmVSGerArtEnc.EdPecasMan.ValueVariant      := QrVSPWECabPecasMan.Value;
    FmVSGerArtEnc.EdAreaManM2.ValueVariant     := QrVSPWECabAreaManM2.Value;
    FmVSGerArtEnc.EdAreaManP2.ValueVariant     := QrVSPWECabAreaManP2.Value;
    //
    FmVSGerArtEnc.EdCustoManMOKg.ValueVariant  := QrVSPWECabCustoManMOKg.Value;
    FmVSGerArtEnc.EdCustoManMOTot.ValueVariant := QrVSPWECabCustoManMOTot.Value;
    FmVSGerArtEnc.EdValorManMP.ValueVariant    := QrVSPWECabValorManMP.Value;
    FmVSGerArtEnc.EdValorManT.ValueVariant     := QrVSPWECabValorManT.Value;
    //
    FmVSGerArtEnc.ShowModal;
    //
    LocCod(Codigo, Codigo);
    FmVSGerArtEnc.Destroy;
  end;
}
end;

procedure TFmVSPWECab.CabReeditar1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSPWECabCodigo.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspwecab', False, [
  'DtHrLibOpe'], ['Codigo'], [
  '0000-00-00'], [Codigo], True) then
    LocCod(Codigo, Codigo);
end;

function TFmVSPWECab.CalculoCustoOrigemM2(): Double;
begin
  Result := VS_CRC_PF.CalculoValorOrigemM2(QrVSPWEAtuAreaM2.Value,
    QrVSPWEAtuCustoPQ.Value, QrVSPWEAtuValorMP.Value,
    QrVSPWEAtuCusFrtMOEnv.Value);
end;

procedure TFmVSPWECab.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  PesquisaArtigoJaFeito(Key);
end;

procedure TFmVSPWECab.CkCpermitirCrustClick(Sender: TObject);
begin
  ReabreGGX();
end;

procedure TFmVSPWECab.Cliente1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSPWECabCodigo.Value;
  if VS_CRC_PF.AlteraVMI_CliVenda(QrVSPWEAtuControle.Value,
  QrVSPWEAtuCliVenda.Value) then
  begin
    LocCod(Codigo, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspwecab', False, [
    'Cliente'], [
    'Codigo'], [
    QrVSPWEAtuCliVenda.Value], [
    Codigo], True) then
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSPWECab.CobranadeMO1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  VS_PF.MostraFormVSImpMOEnvRet();
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPWECab.Corrigeartigodebaixa1Click(Sender: TObject);
const
  SQLType = stUpd;
begin
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, CO_TAB_VMI, False, [
  'GraGruX'], ['Controle'], [
  QrVSPWEAtuGraGruX.Value], [QrVSPWEBxaControle.Value], True) then
    UnDmkDAC_PF.AbreQuery(QrVSPWEBxa, Dmod.MyDB);
end;

procedure TFmVSPWECab.CorrigeFornecedorePalletsDestino(Reabre: Boolean);
var
  IMEIAtu, MovimCod, Codigo: Integer;
  MovimNivSrc, MovimNivDst: TEstqMovimNiv;
begin
  IMEIAtu     := QrVSPWEAtuControle.Value;
  MovimCod    := QrVSPWECabMovimCod.Value;
  Codigo      := QrVSPWECabCodigo.Value;
  MovimNivSrc := eminSorcWEnd;
  MovimNivDst := eminDestWEnd;
  VS_CRC_PF.DefinirAscendenciasDeIMEIsDeMovimCod(IMEIAtu, MovimCod, MovimNivSrc,
  MovimNivDst);
  if Reabre then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSPWECab.CorrigeFornecedoresdestinoapartirdestaoperao1Click(
  Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  FAtualizando := not FAtualizando;
  if FAtualizando then
  begin
    AtualizaFornecedoresDeDestino();
    FAtualizando := False;
  end;
end;

procedure TFmVSPWECab.CorrigeMO1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  VS_PF.MostraFormVSMOPWEGer();
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPWECab.Corrigetodasbaixas1Click(Sender: TObject);
var
  Controle: Integer;
  Fator, AreaM2, AreaP2, Soma: Double;
begin
  if QrVSPWECabAreaINIM2.Value > 0 then
  begin
    Soma  := 0;
    Fator := QrVSPWECabAreaDstM2.Value / QrVSPWECabAreaINIM2.Value;
    QrVSPWEDst.First;
    while not QrVSPWEDst.Eof do
    begin
      Controle := QrVSPWEBxaControle.Value;
      if QrVSPWEDst.RecNo = QrVSPWEDst.RecordCount then
        AreaM2 := - (QrVSPWECabAreaINIM2.Value + Soma)
      else
        AreaM2 := QrVSPWEBxaAreaM2.Value * Fator;
      AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'AreaM2', 'AreaP2'], [
      'Controle'], [
      AreaM2, AreaP2], [
      Controle], True) then ;
        //VS_CRC_PF.AtualizaSaldoVirtualVSMovIts_Generico(Controle, MovimID, MovimNiv);
      //
      Soma := Soma + AreaM2;
      //
      QrVSPWEDst.Next;
    end;
    VS_CRC_PF.AtualizaSaldoVirtualVSMovIts_Generico(QrVSPWEAtuControle.Value,
    QrVSPWEAtuMovimID.Value, QrVSPWEAtuMovimNiv.Value);
    LocCod(QrVSPWECabCodigo.Value, QrVSPWECabCodigo.Value);
  end else
    Geral.MB_Aviso('�rea inicial inv�lida!');
end;

procedure TFmVSPWECab.CorrigirFornecedor1Click(Sender: TObject);
begin
  VS_CRC_PF.AtualizaFornecedorPWE(QrVSPWECabMovimCod.Value);
  CorrigeFornecedorePalletsDestino(True);
  LocCod(QrVSPWECabCodigo.Value, QrVSPWECabCodigo.Value);
end;

procedure TFmVSPWECab.IMEIArtigodeRibeiragerado1Click(Sender: TObject);
var
  NFeRem: String;
begin
  if QrVSPWECabNFeRem.Value <> 0 then
    NFeRem := Geral.FF0(QrVSPWECabNFeRem.Value)
  else
    NFeRem := '';
  VS_CRC_PF.ImprimeIMEI([QrVSPWEAtuControle.Value], viikOrdemOperacao,
    QrVSPWECabLPFMO.Value, NFeRem, QrVSPWECab);
end;

procedure TFmVSPWECab.IncluiAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  //PCOpeOri.ActivePageIndex := 2;
  PCPWEOri.ActivePage := TsEnvioMO;
  //
  VS_PF.MostraFormVSMOEnvEnv(stIns, QrVSPWEOriIMEI, QrVSMOEnvEnv, QrVSMOEnvEVMI,
  siNegativo, QrVSPWECabSerieRem.Value, QrVSPWECabNFeRem.Value);
  //
  LocCod(QrVSPWECabCodigo.Value,QrVSPWECabCodigo.Value);
  //
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPWECab.IncluiAtrelamento2Click(Sender: TObject);
var
  MovimCod: Integer;
begin
{$IfDef sAllVS}
  //PCPWEOri.ActivePageIndex := 2;
  PCPWEOri.ActivePage := TsRetornoMO;
  //
  MovimCod := QrVSPWECabMovimCod.Value;
  //
  VS_PF.MostraFormVSMOEnvRet(stIns, QrVSPWEAtu, QrVSPWEDst, QrVSPWEBxa,
  QrVSMOEnvRet, QrVSMOEnvGVMI, QrVSMOEnvRVMI, MovimCod, 0, 0, siPositivo,
  FGerArX2Ret);
  //
  LocCod(QrVSPWECabCodigo.Value,QrVSPWECabCodigo.Value);
  //
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPWECab.IncluidePedidoitemMPV1Click(Sender: TObject);
begin
  App_Jan.MostraFormOsMpvVs();
end;

procedure TFmVSPWECab.InformaNmerodaRME1Click(Sender: TObject);
begin
  VS_CRC_PF.InfoReqMovEstq(QrVSPWEOriIMEIControle.Value,
    QrVSPWEOriIMEIReqMovEstq.Value, QrVSPWEOriIMEI);
end;

procedure TFmVSPWECab.InformaNmerodaRME2Click(Sender: TObject);
begin
  VS_CRC_PF.InfoReqMovEstq(QrVSPWEDstControle.Value,
    QrVSPWEDstReqMovEstq.Value, QrVSPWEDst);
end;

procedure TFmVSPWECab.InformaPginaelinhadeIEC1Click(Sender: TObject);
begin
  if VS_CRC_PF.InformaIxx(DGDadosOri, QrVSPWEOriIMEI, 'Controle') then
    PCPWEOri.ActivePageIndex := 1;
end;

procedure TFmVSPWECab.InsereArtigoEmProcesso(SQLType: TSQLType; Codigo,
  MovimCod, Empresa, ClientMO, GraGruX, CliVenda: Integer; DataHora: String;
  FornecMO, StqCenLoc: Integer);
const
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  SrcGGX     = 0;
  DstMovID   = 0;
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  //CliVenda   = 0;
  Pallet     = 0;
  AreaM2     = 0;
  AreaP2     = 0;
  CustoMOKg  = 0;
  //CustoMOM2  = 0;
  ValorT     = 0;
  Fornecedor = 0;
  Pecas      = 0;
  PesoKg     = 0;
  Ficha      = 0;
  SerieFch   = 0;
  //Misturou   = 0;
  ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0; // Calcular depois a cada item
  Marca      = '';
  //
  ExigeFornecedor = False;
  //
  TpCalcAuto = -1;
  //
  EdPallet = nil;
  EdAreaM2 = nil;
  EdFicha  = nil;
  EdValorT = nil;
  //
  PedItsFin = 0;
  PedItsVda = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ItemNFe     = 0;
  VSMulFrnCab = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  Observ: String;
  MovimTwn, Controle,  PedItsLib, ReqMovEstq: Integer;
  CustoMOM2, CustoMOTot: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
begin
  //Codigo         :=
  Controle       := EdControle.ValueVariant;
  //MovimCod       :=
  //Empresa        :=
  //DataHora       :=
  MovimID        := emidEmProcWE;
  MovimNiv       := eminEmWEndInn;
  //GraGruX        :=
  Observ         := EdObserv.Text;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  MovimTwn       := EdMovimTwn.ValueVariant;
  PedItsLib      := EdPedItsLib.ValueVariant;
  CustoMOM2      := EdCustoMOM2.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
(*  N�o adianta aqui! J� emitiu cabe�alho!
  if Dmod.VSFic(GraGruX, Empresa, Fornecedor, Pallet, Ficha, Pecas, AreaM2,
  PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas, EdAreaM2,
  EdPesoKg, EdValorT, ExigeFornecedor) then
    Exit;
*)
  //
  //
  // Nao tem Gemeo!!
  //MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, SQLType, MovimTwn);
  MovimTwn := 0;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Controle);
  if VS_CRC_PF.InsUpdVSMovIts3(SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Fornecedor, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2,
  CustoMOTot, ValorMP, SrcMovID, SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso,
  QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX,
  Marca, TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei067(*Gera��o de couro que est� em processo*)) then
  begin
    // Nao se aplica. Calcula com function propria a seguir.
    //VS_CRC_PF.AtualizaTotaisVSXxxCab('vspwecab', MovimCod);
    VS_CRC_PF.AtualizaTotaisVSPWECab(QrVSPWECabMovimCod.Value);
    if GBVSPedIts.Visible then
      VS_CRC_PF.AtualizaVSPedIts_Lib(PedItsLib, Controle,
      Pecas, PesoKg, AreaM2, AreaP2);
  end;
end;

procedure TFmVSPWECab.IrparacadastrodoPallet1Click(Sender: TObject);
begin
  case PCPWEOri.ActivePageIndex of
    0: VS_CRC_PF.MostraFormVSPallet(QrVSPWEOriPalletPallet.Value);
    1: VS_CRC_PF.MostraFormVSPallet(QrVSPWEOriIMEIPallet.Value);
    else Geral.MB_Erro(
      '"PCPWEOri.ActivePageIndex" n�o implementado em "FmVSPWECab.IrparacadastrodoPallet"');
  end;
end;

procedure TFmVSPWECab.IrparajaneladoPallet1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSPallet(QrVSPWEDstPallet.Value);
end;

procedure TFmVSPWECab.ItsAlteraDstClick(Sender: TObject);
begin
  if (QrVSPWECabDtHrLibOpe.Value > 2) or (QrVSPWECabDtHrFimOpe.Value > 2) then
    if not DBCheck.LiberaPelaSenhaBoss then
      Exit;
  MostraVSPWEDst(stUpd);
end;

procedure TFmVSPWECab.ItsAlteraOriClick(Sender: TObject);
begin
  //MostraVSGArtOri(stUpd);
  MostraFormVSPWEAltOriIMEI(stUpd);
end;

procedure TFmVSPWECab.CabExclui1Click(Sender: TObject);
var
  Codigo, MovimCod: Integer;
begin
  Codigo   := QrVSPWECabCodigo.Value;
  MovimCod := QrVSPWECabMovimCod.Value;
  if VS_CRC_PF.ExcluiCabEIMEI_OpeCab(QrVSPWECabMovimCod.Value,
    QrVSPWEAtuControle.Value, emidEmProcWE, TEstqMotivDel.emtdWetCurti161) then
  begin
    UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
    'DELETE ',
    'FROM mpvitvs',
    'WHERE VSMovimID=' + Geral.FF0(Integer(TEstqMovimID.emidEmProcWE)),
    'AND VSMovimCod=' + Geral.FF0(MovimCod),
    '']));
  end;
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmVSPWECab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

procedure TFmVSPWECab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSPWECab.ReabreGGX();
begin
  if CkCpermitirCrust.Checked then
    VS_CRC_PF.AbreGraGruXY(QrGGXDst,
      'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_6144_VSFinCla))
  else
    VS_CRC_PF.AbreGraGruXY(QrGGXDst,
      'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_3072_VSRibCla));
end;

procedure TFmVSPWECab.Reabreitensteste1Click(Sender: TObject);
begin
  VS_EFD_ICMS_IPI.ReopenVSRibItss(QrVSPWEAtu, QrVSPWEOriIMEI,
  QrVSPWEOriPallet, QrVSPWEDst, (*QrVSPWEInd*)nil,
  (*QrSubPrd*)nil, (*QrDescl*)nil, QrForcados,
  QrEmit, QrPQO, QrVSPWECabCodigo.Value,
  QrVSPWECabMovimCod.Value, QrVSPWECabTemIMEIMrt.Value,
  //MovIDEmProc, MovIDPronto, MovIDIndireto
  emidEmProcWE, emidFinished, emidEmProcWE,
  //MovNivInn, MovNivSrc, MovNivDst:
  eminEmWEndInn, eminSorcWEnd, eminDestWEnd);
end;

procedure TFmVSPWECab.RecalculaCusto();
begin
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSPWECabMovimCod.Value);
  VS_CRC_PF.AtualizaTotaisVSPWECab(QrVSPWECabMovimCod.Value);
  LocCod(QrVSPWECabCodigo.Value, QrVSPWECabCodigo.Value);
end;

procedure TFmVSPWECab.Recalculacusto1Click(Sender: TObject);
begin
  RecalculaCusto();
end;

procedure TFmVSPWECab.Recurtimento1Click(Sender: TObject);
begin
  EmiteReceita(TReceitaTipoSetor.rectipsetrRecurtimento);
end;

procedure TFmVSPWECab.Reimprimereceita1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  PQ_PF.ReimprimePesagemNovo(QrEmitCodigo.Value, QrEmitSetrEmi.Value, TReceitaTipoSetor.rectipsetrRibeira, PB1);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPWECab.Removedesclassificado1Click(Sender: TObject);
var
  Codigo, CtrlDel, CtrlDst, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
  PCPWEDst.ActivePageIndex := 1;
  if Geral.MB_Pergunta('Confirma a exclus�o do item desclassificado?') = ID_YES then
  begin
    Codigo   := QrVSPWECabCodigo.Value;
    CtrlDel  := QrVSPWEDesclDstControle.Value;
    CtrlDst  := QrVSPWEDesclDstSrcNivel2.Value;
    MovimNiv := QrVSPWEAtuMovimNiv.Value;
    MovimCod := QrVSPWEAtuMovimCod.Value;
    //
    if VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      // Nao se aplica!
      //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
      VS_CRC_PF.AtualizaSaldoIMEI(QrVSPWEAtuControle.Value, True);
      // Excluir Baixa tambem
      CtrlDel := VS_CRC_PF.ObtemControleMovimTwin(
        QrVSPWEDesclDstMovimCod.Value, QrVSPWEDesclDstMovimTwn.Value, emidDesclasse,
        eminSorcClass);
      //
      VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
      Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
      //
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSPWEDesclDst,
        TIntegerField(QrVSPWEDesclDstControle), QrVSPWEDesclDstControle.Value);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_CRC_PF.AtualizaTotaisVSXxxCab('vspwecab', MovimCod);
      VS_CRC_PF.AtualizaTotaisVSPWECab(QrVSPWECabMovimCod.Value);
      //
      VS_CRC_PF.AtualizaFornecedorPWE(QrVSPWECabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      VS_CRC_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmProcWE, MovimCod);
      LocCod(Codigo, Codigo);
      ReopenVSPWEDesclDst(CtrlNxt, 0);
    end;
  end;
end;

procedure TFmVSPWECab.RemovesubprodutoWBRaspa1Click(Sender: TObject);
var
  Codigo, CtrlDel, CtrlDst, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
  PCPWEDst.ActivePageIndex := 2;
  if Geral.MB_Pergunta('Confirma a exclus�o do subproduto?') = ID_YES then
  begin
    Codigo   := QrVSPWECabCodigo.Value;
    CtrlDel  := QrVSPWESubPrdControle.Value;
    CtrlDst  := QrVSPWESubPrdSrcNivel2.Value;
    MovimNiv := QrVSPWEAtuMovimNiv.Value;
    MovimCod := QrVSPWEAtuMovimCod.Value;
    //
    if VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      // Nao se aplica!
      //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
      VS_CRC_PF.AtualizaSaldoIMEI(QrVSPWEAtuControle.Value, True);
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSPWESubPrd,
        TIntegerField(QrVSPWESubPrdControle), QrVSPWESubPrdControle.Value);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_CRC_PF.AtualizaTotaisVSXxxCab('vspwecab', MovimCod);
      VS_CRC_PF.AtualizaTotaisVSPWECab(QrVSPWECabMovimCod.Value);
      //
      VS_CRC_PF.AtualizaFornecedorPWE(QrVSPWECabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      VS_CRC_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmProcWE, MovimCod);
      LocCod(Codigo, Codigo);
      ReopenVSPWESubPrd(CtrlNxt, 0);
    end;
  end;
end;

procedure TFmVSPWECab.ReopenEmit();
begin
{$IfDef sAllVS}
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmit, Dmod.MyDB, [
  'SELECT emi.*, ',
  'IF(emi.DtaBaixa < 2, "??/??/????", DATE_FORMAT(emi.DtaBaixa, "%d/%m/%Y") ) DtaBaixa_TXT, ',
  'IF(emi.DtCorrApo < 2, "", DATE_FORMAT(emi.DtCorrApo, "%d/%m/%Y") ) DtCorrApo_TXT ',
  'FROM emit emi ',
  'WHERE emi.VSMovCod=' + Geral.FF0(QrVSPWECabMovimCod.Value),
  '']);
(*
  'SELECT * ',
  'FROM emit ',
  'WHERE VSMovCod=' + Geral.FF0(QrVSPWECabMovimCod.Value),
  '']);
*)
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPWECab.ReopenGraGruX();
begin
  VS_CRC_PF.AbreGraGruXY(QrGraGruX,
    'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_5120_VSWetEnd));
end;

procedure TFmVSPWECab.ReopenVSPWEDesclDst(Controle, Pallet: Integer);
(*
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
*)
begin
  VS_CRC_PF.ReopenVSXxxExtra(QrVSPWEDesclDst, QrVSPWECabCodigo.Value,
    (*QrVSPWEAtuControle.Value,*) QrVSPWECabTemIMEIMrt.Value,
    emidDesclasse, emidFinished, eminDestClass);
(*
  TemIMEIMrt := QrVSPWECabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(emidDesclasse)),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestClass)),
  'AND vmi.DstMovID=' + Geral.FF0(Integer(emidFinished)),
  'AND vmi.DstNivel1=' + Geral.FF0(QrVSPWECabCodigo.Value),
  'AND vmi.DstNivel2=' + Geral.FF0(QrVSPWEAtuControle.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPWEDesclDst, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
*)
end;

procedure TFmVSPWECab.ReopenVSPWESubPrd(Controle, Pallet: Integer);
(*
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
*)
begin
  VS_CRC_PF.ReopenVSXxxExtra(QrVSPWESubPrd, QrVSPWECabCodigo.Value,
    (*QrVSPWEAtuControle.Value,*) QrVSPWECabTemIMEIMrt.Value,
    emidGeraSubProd, emidFinished, eminSemNiv);
(*
  TemIMEIMrt := QrVSPWECabTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(emidGeraSubProd)),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSemNiv)),
  'AND vmi.DstMovID=' + Geral.FF0(Integer(emidFinished)),
  'AND vmi.DstNivel1=' + Geral.FF0(QrVSPWECabCodigo.Value),
  'AND vmi.DstNivel2=' + Geral.FF0(QrVSPWEAtuControle.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPWESubPrd, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
*)
end;

procedure TFmVSPWECab.ItsExcluiDstClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlDst, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
  if (QrVSPWECabDtHrLibOpe.Value > 2) or (QrVSPWECabDtHrFimOpe.Value > 2) then
    if not DBCheck.LiberaPelaSenhaBoss then
      Exit;
(*
  if QrVSPWEAtuSdoVrtPeca.Value < QrVSPWEAtuPecas.Value then
  begin
    Geral.MB_Aviso('Exclus�o de item de destino cancelada!' +
    sLineBreak + 'J� existe baixa do Artigo em Processo que foi gerado!');
    Exit;
  end else
*)
  if Geral.MB_Pergunta('Confirma a exclus�o do item de destino?') = ID_YES then
  begin
    Codigo   := QrVSPWECabCodigo.Value;
    CtrlDel  := QrVSPWEDstControle.Value;
    CtrlDst  := QrVSPWEDstSrcNivel2.Value;
    MovimNiv := QrVSPWEAtuMovimNiv.Value;
    MovimCod := QrVSPWEAtuMovimCod.Value;
    //
    if VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      // Nao se aplica!
      //Dmod.AtualizaTotaisVSXxxCab('vsinncab', MovimCod);
      // Aumentar Saldo e custo do Artigo de Ribeira gerado
(*    Precisa???
      VS_CRC_PF.DistribuiCusto...(MovimNiv, MovimCod, Codigo);
*)
      // Excluir Baixa tambem
      CtrlDel := VS_CRC_PF.ObtemControleMovimTwin(
        QrVSPWEDstMovimCod.Value, QrVSPWEDstMovimTwn.Value, emidEmProcWE,
        eminEmWEndBxa);
      //
      VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
      Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER);
      //
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSPWEDst,
        TIntegerField(QrVSPWEDstControle), QrVSPWEDstControle.Value);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_CRC_PF.AtualizaTotaisVSXxxCab('vspwecab', MovimCod);
      VS_CRC_PF.AtualizaTotaisVSPWECab(QrVSPWECabMovimCod.Value);
      //
      VS_CRC_PF.AtualizaFornecedorPWE(QrVSPWECabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      VS_CRC_PF.ReopenVSOpePrcDst(QrVSPWEDst, QrVSPWECabMovimCod.Value, CtrlNxt,
      QrVSPWECabTemIMEIMrt.Value, TEstqMovimNiv.eminDestWEnd);
    end;
  end;
end;

procedure TFmVSPWECab.ItsExcluiOriIMEIClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimNiv, MovimCod: Integer;
begin
  if not FItsExcluiOriIMEI_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
(*
  if QrVSPWEAtuSdoVrtPeca.Value < QrVSPWEAtuPecas.Value then
  begin
    Geral.MB_Aviso('Exclus�o de item de origem cancelada!' +
    sLineBreak + 'J� existe baixa do Artigo em Processo que foi gerado!');
    Exit;
  end else
*)
  if Geral.MB_Pergunta('Confirme a exclus�o do item de origem?') = ID_YES then
  begin
    Codigo    := QrVSPWECabCodigo.Value;
    CtrlDel   := QrVSPWEOriIMEIControle.Value;
    CtrlOri   := QrVSPWEOriIMEISrcNivel2.Value;
    MovimNiv  := QrVSPWEAtuMovimNiv.Value;
    MovimCod  := QrVSPWEAtuMovimCod.Value;
    //
    if VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
    Integer(TEstqMotivDel.emtdWetCurti161), Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
    begin
      VS_CRC_PF.AtualizaSaldoIMEI(CtrlOri, True);
      //
      // Nao se aplica. Calcula com function propria a seguir.
      //VS_CRC_PF.AtualizaTotaisVSXxxCab('vspwecab', MovimCod);
      VS_CRC_PF.AtualizaTotaisVSPWECab(QrVSPWECabMovimCod.Value);
     //
     CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSPWEOriIMEI,
        TIntegerField(QrVSPWEOriIMEIControle), QrVSPWEOriIMEIControle.Value);
      //
      RecalculaCusto();
      VS_CRC_PF.AtualizaFornecedorPWE(QrVSPWECabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      VS_CRC_PF.ReopenVSXxxOris(QrVSPWECab, QrVSPWEOriIMEI, QrVSPWEOriPallet,
      CtrlNxt, 0, eminSorcWEnd);
    end;
  end;
end;

procedure TFmVSPWECab.ItsExcluiOriPalletClick(Sender: TObject);
var
  Codigo, CtrlDel, CtrlOri, CtrlNxt, MovimNiv, MovimCod, Pallet, Itens: Integer;
begin
  if not FItsExcluiOriPallet_Enabled then
    if not DBCheck.LiberaPelaSenhaAdmin then
      Exit;
  if MyObjects.FIC(QrVSPWEOriPalletPallet.Value = 0, nil,
  'Item n�o � um pallet!') then
    Exit;
  //
  if (QrVSPWEAtuSdoVrtPeca.Value < QrVSPWEAtuPecas.Value) then
  begin
    (*
    if not DBCheck.LiberaPelaSenhaAdmin() then
      Exit;
     *)
  end;
  //else
  begin
    Pallet    := QrVSPWEOriPalletPallet.Value;
    Itens     := 0;
    //
    QrVSPWEOriIMEI.First;
    while not QrVSPWEOriIMEI.Eof do
    begin
      if QrVSPWEOriIMEIPallet.Value = Pallet then
        Itens := Itens + 1;
      //
      QrVSPWEOriIMEI.Next;
    end;
    if Itens > 0 then
    begin
      if Geral.MB_Pergunta('Confirma a remo��o do Pallet ' +
      Geral.FF0(Pallet) + '?') = ID_YES then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
          'Exclu�ndo IMEIS do Pallet selecionado!');
        //
        Itens := 0;
        QrVSPWEOriIMEI.First;
        while not QrVSPWEOriIMEI.Eof do
        begin
          if QrVSPWEOriIMEIPallet.Value = Pallet then
          begin
            Itens := Itens + 1;
            //
            Codigo    := QrVSPWECabCodigo.Value;
            CtrlDel   := QrVSPWEOriIMEIControle.Value;
            CtrlOri   := QrVSPWEOriIMEISrcNivel2.Value;
            MovimNiv  := QrVSPWEAtuMovimNiv.Value;
            MovimCod  := QrVSPWEAtuMovimCod.Value;
            //
            if VS_CRC_PF.ExcluiVSMovIts_EnviaArquivoExclu(CtrlDel,
            Integer(TEstqMotivDel.emtdWetCurti161),
            Dmod.QrUpd, Dmod.MyDB, CO_MASTER) then
            //Dmod.MyDB) = ID_YES then
            begin
              VS_CRC_PF.AtualizaSaldoIMEI(CtrlOri, True);
              //
              // Nao se aplica. Calcula com function propria a seguir.
              //VS_CRC_PF.AtualizaTotaisVSXxxCab('vspwecab', MovimCod);
              VS_CRC_PF.AtualizaTotaisVSPWECab(QrVSPWECabMovimCod.Value);
            end;
            //
          end;
          QrVSPWEOriIMEI.Next;
        end;
      end;
      CtrlNxt := GOTOy.LocalizaPriorNextIntQr(QrVSPWEOriIMEI,
      TIntegerField(QrVSPWEOriIMEIControle), QrVSPWEOriIMEIControle.Value);
      //
      RecalculaCusto();
      VS_CRC_PF.AtualizaFornecedorPWE(QrVSPWECabMovimCod.Value);
      CorrigeFornecedorePalletsDestino(False);
      LocCod(Codigo, Codigo);
      VS_CRC_PF.ReopenVSXxxOris(QrVSPWECab, QrVSPWEOriIMEI, QrVSPWEOriPallet,
      CtrlNxt, 0, eminSorcWEnd);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end else Geral.MB_Erro(
      'N�o foi localizado nenhum IMEI para o Pallet selecionado!');
  end;
end;

procedure TFmVSPWECab.DefineONomeDoForm;
begin
end;

procedure TFmVSPWECab.Definiodequantidadesmanualmente1Click(Sender: TObject);
begin
  Geral.MB_Aviso('Procedimento n�o implementado! Solicite � DERMATEK!');
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSPWECab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSPWECab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSPWECab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSPWECab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSPWECab.SpeedButton5Click(Sender: TObject);
var
  GGYDst: Integer;
begin
  VAR_CADASTRO2 := 0;
  if CkCpermitirCrust.Checked then
    GGYDst := CO_GraGruY_6144_VSFinCla
  else
    GGYDst := CO_GraGruY_3072_VSRibCla;
  Grade_Jan.MostraFormGraGruY(GGYDst, 0, '');
  ReabreGGX();
  if VAR_CADASTRO2 <> 0 then
  begin
    EdGGXDst.ValueVariant := VAR_CADASTRO2;
    CBGGXDst.KeyValue     := VAR_CADASTRO2;
  end;
end;

procedure TFmVSPWECab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPWECab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSPWECabCodigo.Value;
  Close;
end;

procedure TFmVSPWECab.ItsIncluiDstClick(Sender: TObject);
begin
  MostraVSPWEDst(stIns);
end;

procedure TFmVSPWECab.CabAltera1Click(Sender: TObject);
begin
  Altera();
end;

procedure TFmVSPWECab.BtConfirmaClick(Sender: TObject);
var
  Codigo, GraGruX, Empresa, ClientMO, MovimCod, TipoArea, FornecMO, MovimNiv,
  Forma, Quant, Cliente, CliVenda, StqCenLoc, NFeRem, GGXDst, SerieRem,
  EmitGru, VSArtCab, LinCulReb, LinCabReb, LinCulSem, LinCabSem, ReceiRecu,
  ReceiRefu: Integer;
  Nome, DtHrAberto, DataHora, LPFMO, EmitGrLote, DataPed, DataStart, DataCrust,
  DataExped: String;
  SQLType: TSQLType;
  ExigeNFe: Boolean;
  CustoMOM2: Double;
  Classes: String;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  // CUIDADO!!! Mesmo GraGruX vai nas duas tabelas !!!
  GraGruX        := EdGragruX.ValueVariant;
  GGXDst         := EdGGXDst.ValueVariant;
  DtHrAberto     := Geral.FDT_TP_Ed(TPDtHrAberto.Date, EdDtHrAberto.Text);
  Nome           := EdNome.Text;
  TipoArea       := RGTipoArea.ItemIndex;
  FornecMO       := EdFornecMO.ValueVariant;
  Cliente        := EdCliente.ValueVariant;
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  LPFMO          := EdLPFMO.Text;
  SerieRem       := EdSerieRem.ValueVariant;
  NFeRem         := EdNFeRem.ValueVariant;
  ClientMO       := EdClientMO.ValueVariant;
  EmitGru        := EdEmitGru.ValueVariant;
  VSArtCab       := EdVSArtCab.ValueVariant;
  EmitGrLote     := EdEmitGrLote.ValueVariant;
  if EmitGrLote = EmptyStr then
    EmitGrLote := '0';
  LinCulReb      := EdLinCulReb.ValueVariant;
  LinCabReb      := EdLinCabReb.ValueVariant;
  LinCulSem      := EdLinCulSem.ValueVariant;
  LinCabSem      := EdLinCabSem.ValueVariant;
  DataPed        := DtHrAberto;

  DataStart      := Geral.FDT_TP_Ed(TPDataStart.Date, EdDataStart.Text);
  DataCrust      := Geral.FDT_TP_Ed(TPDataCrust.Date, EdDataCrust.Text);
  DataExped      := Geral.FDT_TP_Ed(TPDataExped.Date, EdDataExped.Text);

  ReceiRecu      := EdReceiRecu.ValueVariant;
  ReceiRefu      := EdReceiRefu.ValueVariant;
  //
  Classes        := EdClasses.ValueVariant;
  //
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then
    Exit;
  if MyObjects.FIC(VSArtCab = 0, EdVSArtCab, 'Defina a configura��o do artigo semi-acabado / acabado!') then
    Exit;
  if MyObjects.FIC(ClientMO = 0, EdClientMO, 'Defina o dono do couro!') then
    Exit;
  if MyObjects.FIC(GraGruX = 0, EdGragruX, 'Informe o Artigo de Ribeira!') then
    Exit;
  if MyObjects.FIC(GGXDst = 0, EdGGXDst, 'Informe o Artigo a ser produzido!') then
    Exit;
  if MyObjects.FIC(FornecMO = 0, EdFornecMO, 'Informe o fornecedor da m�o de obra') then
    Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local/centro de estoque') then
    Exit;
  ExigeNFe := (FornecMO <> 0) and (FornecMO <> ClientMO) and (NFeRem = 0);
  if MyObjects.FIC(ExigeNFe, EdNFeRem, 'Defina a NFe de remessa!') then
    Exit;
  //
  if not VS_CRC_PF.ValidaCampoNF(4, EdNFeRem, True) then Exit;
  //
  Codigo := UMyMod.BPGS1I32('vspwecab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vspwecab', False, [
  'MovimCod', 'Empresa', 'DtHrAberto',
  'Nome', 'TipoArea', 'GraGruX',
  'Cliente', 'LPFMO', 'NFeRem',
  'SerieRem', 'GGXDst', 'EmitGru',
  'VSArtCab', 'EmitGrLote',
  'LinCulReb', 'LinCabReb', 'LinCulSem',
  'LinCabSem', 'DataPed', 'DataStart',
  'DataCrust', 'DataExped', 'ReceiRecu',
  'ReceiRefu', 'Classes'], [
  'Codigo'], [
  MovimCod, Empresa, DtHrAberto,
  Nome, TipoArea, GraGruX,
  Cliente, LPFMO, NFeRem,
  SerieRem, GGXDst, EmitGru,
  VSArtCab, EmitGrLote,
  LinCulReb, LinCabReb, LinCulSem,
  LinCabSem, DataPed, DataStart,
  DataCrust, DataExped, ReceiRecu,
  ReceiRefu, Classes], [
  Codigo], True) then
  begin
    if SQLType = stIns then
      VS_CRC_PF.InsereVSMovCab(MovimCod, emidEmProcWE, Codigo)
    else
    begin
      if EdNFeRem.ValueVariant <> 0 then
        AtualizaNFeItens();
      //
      CliVenda := Cliente;
      DataHora := DtHrAberto;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', 'CliVenda', CO_DATA_HORA_VMI], ['MovimCod'], [
      Empresa, CliVenda, DataHora], [MovimCod], True);
      //
      CustoMOM2 := EdCustoMOM2.ValueVariant;
      MovimNiv  := Integer(eminEmWEndInn); //21
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'CustoMOM2'], ['MovimCod', 'MovimNiv'], [
      CustoMOM2], [MovimCod, MovimNiv], True);
    end;
    FornecMO       := EdFornecMO.ValueVariant;
    StqCenLoc      := EdStqCenLoc.ValueVariant;
    InsereArtigoEmProcesso(ImgTipo.SQLType, Codigo, MovimCod, Empresa, ClientMO,
      GraGruX, Cliente, DtHrAberto, FornecMO, StqCenLoc);
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    //if FSeq = 1 then Close;
    if (SQLType = stIns) or (QrVSPWEOriIMEI.RecordCount = 0) then
    begin
      //MostraVSPWEOriPall(stIns);
      Forma := MyObjects.SelRadioGroup('Forma de sele��o',
      'Escolha a forma de sele��o', [
      'Pallet Total', 'Pallet Parcial',
      'IME-I Total', 'IME-I Parcial'], -1);
(*
      case Forma of
        0:
        begin
          Quant := MyObjects.SelRadioGroup('Quantidade na sele��o',
          'Escolha a quantidade de sele��o', ['Parcial', 'Total'], -1);
          case Quant of
            0: MostraVSPWEOriPall(stIns, ptParcial);
            1: MostraVSPWEOriPall(stIns, ptTotal);
          end;
        end;
        1:
        begin
          Quant := MyObjects.SelRadioGroup('Quantidade na sele��o',
          'Escolha a quantidade de sele��o', ['Parcial', 'Total'], -1);
          case Forma of
            0: MostraVSPWEOriIMEI(stIns, ptParcial);
            1: MostraVSPWEOriIMEI(stIns, ptTotal);
          end;
        end;
      end;
*)
      FItsIncluiOri_Enabled := True;
      case Forma of
        0: MostraVSPWEOriPall(stIns, ptTotal);
        1: MostraVSPWEOriPall(stIns, ptParcial);
        2: MostraVSPWEOriIMEI(stIns, ptTotal);
        3: MostraVSPWEOriIMEI(stIns, ptParcial);
      end;
    end;
    VS_CRC_PF.AtualizaDtHrFimOpe_MovimCod(TEstqMovimID.emidEmProcWE, MovimCod);
    VS_CRC_PF.AtualizaFornecedorPWE(QrVSPWECabMovimCod.Value);
  end;
  DmModVS_CRC.AtualizaVSMovIts_VS_BHWE(QrVSPWECabMovimCod.Value);
  VS_CRC_PF.AtualizaTotaisVSPWECab(QrVSPWECabMovimCod.Value);
  LocCod(Codigo, Codigo);
end;

procedure TFmVSPWECab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vspwecab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vspwecab', 'Codigo');
end;

procedure TFmVSPWECab.BtDstClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDst, BtDst);
end;

procedure TFmVSPWECab.BtOriClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOri, BtOri);
end;

procedure TFmVSPWECab.BtPesagemClick(Sender: TObject);
begin
  if not PCPWEOri.ActivePageIndex in [2,3] then
    PCPWEOri.ActivePageIndex := 2;
  MyObjects.MostraPopupDeBotao(PMPesagem, BtPesagem);
end;

procedure TFmVSPWECab.Acabamento1Click(Sender: TObject);
begin
  EmiteReceita(TReceitaTipoSetor.rectipsetrAcabamento);
end;

procedure TFmVSPWECab.Adicionadesclassificado1Click(Sender: TObject);
begin
  MostraVSPWEDescl(stIns);
end;

procedure TFmVSPWECab.AdicionasubprodutoWBRaspa1Click(Sender: TObject);
const
  DesabilitaBaixa = True;
begin
  MostraVSPWESubPrd(stIns, DesabilitaBaixa);
end;

procedure TFmVSPWECab.Altera;
var
  Empresa(*, VSPedIts*): Integer;
  Habilita: Boolean;
begin
  HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSPWECab, [PnDados],
  [PnEdita], TPDtHrAberto, ImgTipo, 'vspwecab');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrVSPWECabEmpresa.Value);
  EdEmpresa.ValueVariant    := Empresa;
  CBEmpresa.KeyValue        := Empresa;
  //
  EdClientMO.ValueVariant    := QrVSPWEAtuClientMO.Value;
  CBClientMO.KeyValue        := QrVSPWEAtuClientMO.Value;
  //
  EdCliente.ValueVariant    := QrVSPWECabCliente.Value;
  CBCliente.KeyValue        := QrVSPWECabCliente.Value;
  //
  EdSerieRem.ValueVariant   := QrVSPWECabSerieRem.Value;
  EdNFeRem.ValueVariant     := QrVSPWECabNFeRem.Value;
  EdLPFMO.ValueVariant      := QrVSPWECabLPFMO.Value;
  EdEmitGrLote.ValueVariant := QrVSPWECabEmitGrLote.Value;
  //
  EdControle.ValueVariant   := QrVSPWEAtuControle.Value;
  EdGraGruX.ValueVariant    := QrVSPWEAtuGraGruX.Value;
  CBGraGruX.KeyValue        := QrVSPWEAtuGraGruX.Value;
  //EdCustoMOTot.ValueVariant := QrVSPWEAtuCustoMOTot.Value;
  EdCustoMOM2.ValueVariant  := QrVSPWEAtuCustoMOM2.Value;
  EdPecas.ValueVariant      := QrVSPWEAtuPecas.Value;
  EdPesoKg.ValueVariant     := QrVSPWEAtuPesoKg.Value;
  EdMovimTwn.ValueVariant   := QrVSPWEAtuMovimTwn.Value;
  EdObserv.ValueVariant     := QrVSPWEAtuObserv.Value;
  //
  VS_CRC_PF.ReopenPedItsXXX(QrVSPedIts, QrVSPWEAtuControle.Value, QrVSPWEAtuControle.Value);
  Habilita := QrVSPWEAtuPedItsLib.Value = 0;
  EdPedItsLib.ValueVariant  := QrVSPWEAtuPedItsLib.Value;
  CBPedItsLib.KeyValue      := QrVSPWEAtuPedItsLib.Value;
  LaPedItsLib.Enabled       := Habilita;
  EdPedItsLib.Enabled       := Habilita;
  CBPedItsLib.Enabled       := Habilita;
  //
  EdStqCenLoc.ValueVariant  := QrVSPWEAtuStqCenLoc.Value;
  CBStqCenLoc.KeyValue      := QrVSPWEAtuStqCenLoc.Value;
  EdReqMovEstq.ValueVariant := QrVSPWEAtuReqMovEstq.Value;
  //
  EdFornecMO.ValueVariant   := QrVSPWEAtuFornecMO.Value;
  CBFornecMO.KeyValue       := QrVSPWEAtuFornecMO.Value;
  //
  EdClientMO.ValueVariant   := QrVSPWEAtuClientMO.Value;
  CBClientMO.KeyValue       := QrVSPWEAtuClientMO.Value;
  //
  EdVSCOPCab.ValueVariant   := QrVSPWECabVSCOPCab.Value;
  CBVSCOPCab.KeyValue       := QrVSPWECabVSCOPCab.Value;
  //
  EdEmitGru.ValueVariant    := QrVSPWECabEmitGru.Value;
  CBEmitGru.KeyValue        := QrVSPWECabEmitGru.Value;
  //
  EdVSArtCab.ValueVariant    := QrVSPWECabVSArtCab.Value;
  CBVSArtCab.KeyValue        := QrVSPWECabVSArtCab.Value;
  //
  EdLinCulReb.ValueVariant := QrVSPWECabLinCulReb.Value;
  EdLinCabReb.ValueVariant := QrVSPWECabLinCabReb.Value;
  EdLinCulSem.ValueVariant := QrVSPWECabLinCulSem.Value;
  EdLinCabSem.ValueVariant := QrVSPWECabLinCabSem.Value;
  TPDtHrAberto.Date        := QrVSPWECabDtHrAberto.Value;
  EdDtHrAberto.ValueVariant:= QrVSPWECabDtHrAberto.Value;
  TPDataStart.Date         := QrVSPWECabDataStart.Value;
  EdDataStart.ValueVariant := QrVSPWECabDataStart.Value;
  TPDataCrust.Date         := QrVSPWECabDataCrust.Value;
  EdDataCrust.ValueVariant := QrVSPWECabDataCrust.Value;
  TPDataExped.Date         := QrVSPWECabDataExped.Value;
  EdDataExped.ValueVariant := QrVSPWECabDataExped.Value;
  //
  EdReceiRecu.ValueVariant   := QrVSPWECabReceiRecu.Value;
  CBReceiRecu.KeyValue      := QrVSPWECabReceiRecu.Value;
  //
  EdReceiRefu.ValueVariant  := QrVSPWECabReceiRefu.Value;
  CBReceiRefu.KeyValue      := QrVSPWECabReceiRefu.Value;
  //
  EdVSCOPCab.SetFocus;
end;

procedure TFmVSPWECab.AlteraAtrelamento1Click(Sender: TObject);
begin
{$IfDef sAllVS}
  //PCPWEOri.ActivePageIndex := 2;
  PCPWEOri.ActivePage := TsEnvioMO;
  //
  VS_PF.MostraFormVSMOEnvEnv(stUpd, QrVSPWEOriIMEI, QrVSMOEnvEnv, QrVSMOEnvEVMI,
  siNegativo, 0, 0);
  //
  LocCod(QrVSPWECabCodigo.Value,QrVSPWECabCodigo.Value);
  //
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPWECab.AlteraAtrelamento2Click(Sender: TObject);
begin
{$IfDef sAllVS}
  //PCPWEOri.ActivePageIndex := 2;
  PCPWEOri.ActivePage := TsRetornoMO;
  //
  VS_PF.MostraFormVSMOEnvRet(stUpd, QrVSPWEAtu, QrVSPWEDst, QrVSPWEBxa,
  QrVSMOEnvRet, QrVSMOEnvGVMI, QrVSMOEnvRVMI, 0, 0, 0, siPositivo, FGerArX2Env);
  //
  LocCod(QrVSPWECabCodigo.Value,QrVSPWECabCodigo.Value);
  //
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPWECab.AlteraLocaldeEstque1Click(Sender: TObject);
begin
  VS_CRC_PF.InfoStqCenLoc(QrVSPWEDstControle.Value,
    QrVSPWEDstStqCenLoc.Value, QrVSPWEDst);
end;

procedure TFmVSPWECab.AtualizaestoqueEmProcesso1Click(Sender: TObject);
begin
  VS_CRC_PF.AtualizaTotaisVSPWECab(QrVSPWECabMovimCod.Value);
// Parei Aqui 2014-12-14
  LocCod(QrVSPWECabCodigo.Value, QrVSPWECabCodigo.Value);
end;

procedure TFmVSPWECab.AtualizaFornecedoresDeDestino;
var
  Qry: TmySQLQuery;
  Codigo: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo',
    'FROM vspwecab ',
    'WHERE Codigo>=' + Geral.FF0(QrVSPWECabCodigo.Value),
    //'ORDER BY Codigo DESC',
    'ORDER BY Codigo ',
    '']);
    Qry.First;
    while not Qry.Eof do
    begin
      if not FAtualizando then
        Qry.Last
      else
      begin
        Codigo := Qry.FieldByName('Codigo').AsInteger;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando o Pallet ' +
        Geral.FF0(Codigo));
        //
        LocCod(Codigo, Codigo);
        VS_CRC_PF.AtualizaFornecedorPWE(QrVSPWECabMovimCod.Value);
        CorrigeFornecedorePalletsDestino(False);
        //
      end;
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSPWECab.AtualizaNFeItens();
begin
  DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
  QrVSPWECabMovimCod.Value, TEstqMovimID.emidEmProcWE, [eminSorcWEnd],
  [eminEmWEndInn, eminDestWEnd, eminEmWEndBxa]);
  //
  if QrVSPWEDesclDst.RecordCount > 0 then
    DmModVS_CRC.AtualizaVSMulNFeCab(siNegativo, TEstqDefMulFldEMxx.edmfMovCod,
    QrVSPWEDesclDstMovimCod.Value, TEstqMovimID.emidDesclasse, [eminSorcClass],
    [eminDestClass]);
end;

procedure TFmVSPWECab.BtCabClick(Sender: TObject);
begin
  VS_CRC_PF.HabilitaMenuInsOuAllVSAberto(QrVSPWECab, QrVSPWECabDtHrAberto.Value,
  BtCab, PMCab, [CabInclui1, AlteraFornecedorMO1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSPWECab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAtualizando := False;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  //GBEdita.Align := alClient;
  //PnDst.Align := alClient;
  PCPWEDst.Align := alClient;
  PCPWEDestSub.ActivePageIndex := 1;
  CriaOForm;
  FSeq := 0;
  //
  ReopenGraGruX();
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0);
  UnDmkDAC_PF.AbreQuery(QrStqCenLoc, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSArtCab, Dmod.MyDB);
  VS_CRC_PF.ReopenVSCOPCab(QrVSCOPCab, emidEmProcWE);
  UnDmkDAC_PF.AbreQuery(QrEmitGru, Dmod.MyDB);
  ReabreGGX();
  UnDmkDAC_PF.AbreQuery(QrReceiRecu, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrReceiRefu, Dmod.MyDB);
  // deve ser depois dos open qry
  MyObjects.DefinePageControlActivePageByName(PCPWEOri, VAR_PC_FRETE_VS_NAME, 0);
  MyObjects.DefinePageControlActivePageByName(PCPWEDst, VAR_PC_FRETE_VS_NAME, 0);
end;

procedure TFmVSPWECab.SbGraGruXClick(Sender: TObject);
begin
  VAR_CADASTRO2 := 0;
  Grade_Jan.MostraFormGraGruY(CO_GraGruY_5120_VSWetEnd, 0, '');
  ReopenGraGruX();
  if VAR_CADASTRO2 <> 0 then
  begin
    EdGraGruX.ValueVariant := VAR_CADASTRO2;
    CBGraGruX.KeyValue     := VAR_CADASTRO2;
  end;
end;

procedure TFmVSPWECab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSPWECab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSPWECab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSPWECabCodigo.Value, LaRegistro.Caption);
  //Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  MyObjects.MostraPopupDeBotao(PMNovo, SbNovo);
end;

procedure TFmVSPWECab.SbNumeroClick(Sender: TObject);
begin
  MyObjects.MostrapopupDeBotao(PMNumero, SbNumero);
end;

procedure TFmVSPWECab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSPWECab.QrPQOAfterScroll(DataSet: TDataSet);
begin
  VS_EFD_ICMS_IPI.ReopenVSMovCod_PQOIts(QrPQOIts, QrPQOCodigo.Value, 0);
end;

procedure TFmVSPWECab.QrPQOBeforeClose(DataSet: TDataSet);
begin
  QrPQOIts.Close;
end;

procedure TFmVSPWECab.QrVSMOEnvEnvAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvEVmi(QrVSMOEnvEVmi, QrVSMOEnvEnvCodigo.Value, 0);
end;

procedure TFmVSPWECab.QrVSMOEnvEnvBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvEVmi.Close;
end;

procedure TFmVSPWECab.QrVSMOEnvRetAfterScroll(DataSet: TDataSet);
begin
  VS_CRC_PF.ReopenVSMOEnvRVmi(QrVSMOEnvRVmi, QrVSMOEnvRetCodigo.Value, 0);
  VS_CRC_PF.ReopenVSMOEnvGVmi(QrVSMOEnvGVmi, QrVSMOEnvRetCodigo.Value, 0);
end;

procedure TFmVSPWECab.QrVSMOEnvRetBeforeClose(DataSet: TDataSet);
begin
  QrVSMOEnvGVmi.Close;
  QrVSMOEnvRVmi.Close;
end;

procedure TFmVSPWECab.QrVSPWECabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSPWECab.QrVSPWECabAfterScroll(DataSet: TDataSet);
begin
  // Em processo
  VS_EFD_ICMS_IPI.ReopenVSOpePrcAtu(QrVSPWEAtu, QrVSPWECabMovimCod.Value, 0,
  QrVSPWECabTemIMEIMrt.Value, eminEmWEndInn);
  // materia-prima
  VS_CRC_PF.ReopenVSXxxOris(QrVSPWECab, QrVSPWEOriIMEI, QrVSPWEOriPallet, 0, 0,
  eminSorcWEnd);
  ReopenVSPWEDesclDst(0, 0);
  // Semi acabado
  VS_CRC_PF.ReopenVSOpePrcDst(QrVSPWEDst, QrVSPWECabMovimCod.Value, 0,
  QrVSPWECabTemIMEIMrt.Value, TEstqMovimNiv.eminDestWEnd);
  // Raspa WB
  ReopenVSPWESubPrd(0,0);
  // Baixa forcada
  VS_EFD_ICMS_IPI.ReopenVSOpePrcForcados(QrForcados, 0, QrVSPWECabCodigo.Value,
    QrVSPWEAtuControle.Value, QrVSPWECabTemIMEIMrt.Value, emidEmProcWE);
  // Insumos
  ReopenEmit();
  VS_EFD_ICMS_IPI.ReopenVSMovCod_PQO(QrPQO, QrVSPWECabMovimCod.Value, 0);
  // M.O.
{$IfDef sAllVS}
  VS_CRC_PF.ReopenVSMOEnvEnv(QrVSMOEnvEnv, QrVSPWECabMovimCod.Value, 0);
  VS_CRC_PF.ReopenVSMOEnvRet(QrVSMOEnvRet, QrVSPWECabMovimCod.Value, 0);
{$Else}
  //Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
{$EndIf}
end;

procedure TFmVSPWECab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSPWECabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSPWECab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  Codigo, Controle: Integer;
begin
  Localizou := VS_CRC_PF.MostraFormVSRMPPsq(emidEmProcWE, [], stPsq, Codigo,
  Controle, False);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    //ReopenVSPWEIts(Controle);
  end;
end;

procedure TFmVSPWECab.SbVSArtCabClick(Sender: TObject);
var
  Codigo, GraGruX: Integer;
begin
  Codigo  := EdVSArtCab.ValueVariant;
  GraGruX := 0;
  VAR_CADASTRO := 0;
  VS_PF.MostraFormVSArtCab(Codigo, GraGruX);
  UnDmkDAC_PF.AbreQuery(QrVSArtCab, Dmod.MyDB);
  if VAR_CADASTRO <> 0 then
  begin
    EdVSArtCab.ValueVariant := VAR_CADASTRO;
    CBVSArtCab.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmVSPWECab.SbVSCOPCabCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  VS_CRC_PF.MostraFormVSCOPCab(0);
  FmMeuDBUses.VirtualKey_F5();
  UMyMod.SetaCodigoPesquisado(EdVSCOPCab, CBVSCOPCab, QrVSCOPCab, VAR_CADASTRO);
end;

procedure TFmVSPWECab.SbVSCOPCabCpyClick(Sender: TObject);
const
  EdOperacoes = nil;
  CBOperacoes = nil;
  EdCustoMOKg = nil;
begin
  VS_CRC_PF.PreencheDadosDeVSCOPCab(EdVSCOPCab.ValueVariant, EdEmpresa, CBEmpresa, RGTipoArea,
  EdGraGruX, CBGraGruX, EdGGXDst, CBGGXDst, EdFornecMO, CBFornecMO, EdStqCenLoc,
  CBStqCenLoc, EdOperacoes, CBOperacoes, EdCliente, CBCliente, EdPedItsLib,
  CBPedItsLib, EdClientMO, CBClientMO, EdCustoMOM2, EdVSArtCab, CBVSArtCab,
  EdLinCulReb, EdLinCabReb, EdLinCulSem, EdLinCabSem, EdReceiRecu, EdReceiRefu,
  CBReceiRecu, CBReceiRefu);
end;

procedure TFmVSPWECab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPWECab.FornecedorMO1Click(Sender: TObject);
begin
  if VS_CRC_PF.AlteraVMI_FornecMO(QrVSPWEAtuControle.Value,
  QrVSPWEAtuFornecMO.Value) then
    LocCod(QrVSPWECabCodigo.Value, QrVSPWECabCodigo.Value);
end;

procedure TFmVSPWECab.HabilitaEmpresa(SQLType: TSQLType);
begin
  EdEmpresa.Enabled := SQLType = stIns;
  CBEmpresa.Enabled := SQLType = stIns;
end;

procedure TFmVSPWECab.CabInclui1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  EdControle.ValueVariant   := 0;
  EdGraGruX.ValueVariant    := 0;
  CBGraGruX.KeyValue        := 0;
  EdCustoMOTot.ValueVariant := 0;
  EdPecas.ValueVariant      := 0;
  EdPesoKg.ValueVariant     := 0;
  EdMovimTwn.ValueVariant   := 0;
  EdFornecMO.ValueVariant   := 0;
  CBFornecMO.KeyValue       := 0;
  EdObserv.ValueVariant     := '';
  //
  HabilitaEmpresa(stIns);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSPWECab, [PnDados],
  [PnEdita], EdVSCOPCab, ImgTipo, 'vspwecab');
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPDtHrAberto.Date := Agora;
  EdDtHrAberto.ValueVariant := Agora;
  VS_CRC_PF.ReopenPedItsXXX(QrVSPedIts, 0, 0);
end;

procedure TFmVSPWECab.QrVSPWECabBeforeClose(
  DataSet: TDataSet);
begin
  QrVSPWEAtu.Close;
  QrVSPWEOriIMEI.Close;
  QrVSPWEOriPallet.Close;
  QrVSPWEDst.Close;
  QrVSPWESubPrd.Close;
  QrForcados.Close;
  QrEmit.Close;
  QrPQO.Close;
  QrVSMOEnvEnv.Close;
  QrVSMOEnvRet.Close;
end;

procedure TFmVSPWECab.QrVSPWECabBeforeOpen(DataSet: TDataSet);
begin
  QrVSPWECabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmVSPWECab.QrVSPWECabCalcFields(DataSet: TDataSet);
begin
  case QrVSPWECabTipoArea.Value of
    0: QrVSPWECabNO_TIPO.Value := 'm�';
    1: QrVSPWECabNO_TIPO.Value := 'ft�';
    else QrVSPWECabNO_TIPO.Value := '???';
  end;
  QrVSPWECabNO_DtHrLibOpe.Value := Geral.FDT(QrVSPWECabDtHrLibOpe.Value, 106, True);
  QrVSPWECabNO_DtHrFimOpe.Value := Geral.FDT(QrVSPWECabDtHrFimOpe.Value, 106, True);
  QrVSPWECabNO_DtHrCfgOpe.Value := Geral.FDT(QrVSPWECabDtHrCfgOpe.Value, 106, True);
end;

procedure TFmVSPWECab.QrVSPWEDesclDstAfterScroll(DataSet: TDataSet);
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSPWECabTemIMEIMrt.Value;
  //
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimID=' + Geral.FF0(Integer(emidDesclasse)),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminSorcClass)),
  'AND vmi.SrcMovID=' + Geral.FF0(Integer(emidFinished)),
  'AND vmi.SrcNivel1=' + Geral.FF0(QrVSPWECabCodigo.Value),
  'AND vmi.SrcNivel2=' + Geral.FF0(QrVSPWEAtuControle.Value),
  'AND vmi.MovimTwn=' + Geral.FF0(QrVSPWEDesclDstMovimTwn.Value),
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPWEDesclBxa, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY NO_Pallet, Controle ',
  '']);
  //
end;

procedure TFmVSPWECab.QrVSPWEDstAfterScroll(DataSet: TDataSet);
const
  SQL_Limit = '';
  Controle = 0;
var
  TemIMEIMrt: Integer;
begin
  TemIMEIMrt := QrVSPWECabTemIMEIMrt.Value;
  VS_CRC_PF.ReopenVSOpePrcBxa(QrVSPWEBxa, QrVSPWECabMovimCod.Value,
  QrVSPWEDstMovimTwn.Value, Controle, TemIMEIMrt, eminEmWEndBxa, SQL_Limit);
  //
end;

procedure TFmVSPWECab.QrVSPWEDstBeforeClose(DataSet: TDataSet);
begin
  QrVSPWEBxa.Close;
  QrVSMOEnvRet.Close;
end;

end.

