unit VSOutNFeCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmVSOutNFeCab = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdCodigo: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    EdMovimCod: TdmkEdit;
    Edide_serie: TdmkEdit;
    Edide_nNF: TdmkEdit;
    Label2: TLabel;
    EdOriCod: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenVSOutNfeCab(Codigo: Integer);
  public
    { Public declarations }
    FInseriu: Boolean;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmVSOutNFeCab: TFmVSOutNFeCab;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmVSOutNFeCab.BtOKClick(Sender: TObject);
var
  Codigo, MovimCod, ide_serie, ide_nNF, OriCod: Integer;
  SQLType: TSQLType;
begin
  SQLTYpe        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  ide_serie      := Edide_serie.ValueVariant;
  ide_nNF        := Edide_nNF.ValueVariant;
  OriCod         := EdOriCod.ValueVariant;
  //
  Codigo := UMyMod.BPGS1I32('vsoutnfecab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsoutnfecab', False, [
  'MovimCod', 'ide_serie', 'ide_nNF',
  'OriCod'], [
  'Codigo'], [
  MovimCod, ide_serie, ide_nNF,
  OriCod], [
  Codigo], True) then
  begin
    FInseriu := True;
    ReopenVSOutNfeCab(Codigo);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdCodigo.ValueVariant    := 0;
      Edide_nNF.ValueVariant   := 0;
      //
      Edide_serie.SetFocus;
    end else Close;
  end;
end;

procedure TFmVSOutNFeCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSOutNFeCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSOutNFeCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FInseriu := False;
end;

procedure TFmVSOutNFeCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSOutNFeCab.ReopenVSOutNfeCab(Codigo: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Codigo <> 0 then
      FQrIts.Locate('Codigo', Codigo, []);
  end;
end;

end.
