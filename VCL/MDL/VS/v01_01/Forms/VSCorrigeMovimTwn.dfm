object FmVSCorrigeMovimTwn: TFmVSCorrigeMovimTwn
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-128 :: Corrige IME-Is G'#234'meos Orf'#227'os'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 376
        Height = 32
        Caption = 'Corrige IME-Is G'#234'meos Orf'#227'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 376
        Height = 32
        Caption = 'Corrige IME-Is G'#234'meos Orf'#227'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 376
        Height = 32
        Caption = 'Corrige IME-Is G'#234'meos Orf'#227'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 456
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 456
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 456
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 439
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Splitter1: TSplitter
            Left = 0
            Top = 189
            Width = 808
            Height = 5
            Cursor = crVSplit
            Align = alTop
            ExplicitTop = 105
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 808
            Height = 189
            Align = alTop
            Caption = ' Totais por tipo e subtipo: '
            TabOrder = 0
            object DBGSums: TDBGrid
              Left = 2
              Top = 15
              Width = 804
              Height = 172
              Align = alClient
              DataSource = DsSums
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'MovimID'
                  Title.Caption = 'ID Mov.'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_MovimID'
                  Title.Caption = 'Descri'#231#227'o ID Mov.'
                  Width = 196
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MovimNiv'
                  Title.Caption = 'Nivel'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_MovimNiv'
                  Title.Caption = 'Descri'#231#227'o n'#237'vel do movimento'
                  Width = 278
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ITENS'
                  Title.Caption = 'Itens'
                  Width = 79
                  Visible = True
                end>
            end
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 194
            Width = 808
            Height = 218
            Align = alClient
            Caption = 'Todos IME-IS orf'#227'os: '
            TabOrder = 1
            object DBGOrfaos: TDBGrid
              Left = 2
              Top = 15
              Width = 804
              Height = 201
              Align = alClient
              DataSource = DsOrfaos
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
          object Panel6: TPanel
            Left = 0
            Top = 412
            Width = 808
            Height = 27
            Align = alBottom
            TabOrder = 2
            object CkExcecao: TCheckBox
              Left = 8
              Top = 4
              Width = 789
              Height = 17
              Caption = 
                'Incluir na remo'#231#227'o o ID de g'#234'meos orf'#227'os de n'#237'vel destino de ope' +
                'ra'#231#227'o de couros. (ID=11 Niv=9 e CouNiv2=1)'
              TabOrder = 0
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 504
    Width = 812
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 808
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrOrfaos: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrOrfaosAfterOpen
    BeforeClose = QrOrfaosBeforeClose
    SQL.Strings = (
      'DROP TABLE IF EXISTS _TESTE_;'
      'CREATE TABLE _TESTE_'
      'SELECT MovimID, MovimTwn, COUNT(Controle) ITENS'
      'FROM bluederm_teste.vsmovits'
      'WHERE MovimTwn <> 0'
      'GROUP BY MovimTwn'
      'ORDER BY ITENS, MovimID, MovimTwn;'
      ''
      'SELECT *'
      'FROM bluederm_teste.vsmovits'
      'WHERE MovimTwn IN ('
      '  SELECT MovimTwn'
      '  FROM _TESTE_'
      '  WHERE ITENS=1'
      ')'
      'ORDER BY Controle')
    Left = 84
    Top = 280
    object QrOrfaosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOrfaosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOrfaosMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrOrfaosMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrOrfaosMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrOrfaosEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrOrfaosTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrOrfaosCliVenda: TIntegerField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrOrfaosMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrOrfaosLnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
      Required = True
    end
    object QrOrfaosLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
      Required = True
    end
    object QrOrfaosLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
      Required = True
    end
    object QrOrfaosDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrOrfaosPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrOrfaosGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrOrfaosPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrOrfaosPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrOrfaosAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrOrfaosAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
    end
    object QrOrfaosValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrOrfaosSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrOrfaosSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrOrfaosSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrOrfaosSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrOrfaosSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      Required = True
    end
    object QrOrfaosSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      Required = True
    end
    object QrOrfaosSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      Required = True
    end
    object QrOrfaosObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrOrfaosSerieFch: TIntegerField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrOrfaosFicha: TIntegerField
      FieldName = 'Ficha'
      Required = True
    end
    object QrOrfaosMisturou: TSmallintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrOrfaosFornecMO: TIntegerField
      FieldName = 'FornecMO'
      Required = True
    end
    object QrOrfaosCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      Required = True
    end
    object QrOrfaosCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      Required = True
    end
    object QrOrfaosValorMP: TFloatField
      FieldName = 'ValorMP'
      Required = True
    end
    object QrOrfaosDstMovID: TIntegerField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrOrfaosDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrOrfaosDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrOrfaosDstGGX: TIntegerField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrOrfaosQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
      Required = True
    end
    object QrOrfaosQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      Required = True
    end
    object QrOrfaosQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      Required = True
    end
    object QrOrfaosQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      Required = True
    end
    object QrOrfaosQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
      Required = True
    end
    object QrOrfaosQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      Required = True
    end
    object QrOrfaosQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      Required = True
    end
    object QrOrfaosQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      Required = True
    end
    object QrOrfaosAptoUso: TSmallintField
      FieldName = 'AptoUso'
      Required = True
    end
    object QrOrfaosNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
      Required = True
    end
    object QrOrfaosMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrOrfaosTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
      Required = True
    end
    object QrOrfaosZerado: TSmallintField
      FieldName = 'Zerado'
      Required = True
    end
    object QrOrfaosEmFluxo: TSmallintField
      FieldName = 'EmFluxo'
      Required = True
    end
    object QrOrfaosNotFluxo: TIntegerField
      FieldName = 'NotFluxo'
      Required = True
    end
    object QrOrfaosFatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
      Required = True
    end
    object QrOrfaosFatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
      Required = True
    end
    object QrOrfaosPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
      Required = True
    end
    object QrOrfaosPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
      Required = True
    end
    object QrOrfaosPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
      Required = True
    end
    object QrOrfaosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOrfaosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOrfaosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOrfaosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOrfaosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOrfaosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOrfaosAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOrfaosCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      Required = True
    end
    object QrOrfaosReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
      Required = True
    end
    object QrOrfaosStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
      Required = True
    end
    object QrOrfaosItemNFe: TIntegerField
      FieldName = 'ItemNFe'
      Required = True
    end
    object QrOrfaosVSMorCab: TIntegerField
      FieldName = 'VSMorCab'
      Required = True
    end
    object QrOrfaosVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
      Required = True
    end
    object QrOrfaosCouNiv2: TIntegerField
      FieldName = 'CouNiv2'
    end
  end
  object DsOrfaos: TDataSource
    DataSet = QrOrfaos
    Left = 84
    Top = 328
  end
  object QrSums: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _TESTE_; '
      'CREATE TABLE _TESTE_ '
      'SELECT MovimID, MovimTwn, COUNT(Controle) ITENS '
      'FROM bluederm_teste.vsmovits'
      'WHERE MovimTwn <> 0 '
      'GROUP BY MovimTwn '
      'ORDER BY ITENS, MovimID, MovimTwn; '
      ' '
      'SELECT '
      
        ' ELT(MovimID + 1,"[ND]","Compra","Venda","Reclasse","Baixa","Rec' +
        'urtido","Curtido","Classe Unit.","Reclasse Unit.","For'#231'ado","Sem' +
        ' Origem","Em Opera'#231#227'o","Residual","Ajuste","Classe Mult.","Pr'#233' r' +
        'eclasse","Compra de Classificado","Baixa extra","Saldo anterior"' +
        ',"Em processo WE","Acabado","Devolu'#231#227'o","Retrabalho","Sub Produt' +
        'o","Reclasse Mul.") NO_MovimID,'
      
        ' ELT(MovimNiv + 1,"Sem n'#237'vel","Origem classifica'#231#227'o","Destino cl' +
        'assifica'#231#227'o","Origem gera'#231#227'o de artigo","Destino gera'#231#227'o de arti' +
        'go","Origem reclassifica'#231#227'o","Destino reclassifica'#231#227'o","Origem o' +
        'pera'#231#227'o (Divis'#227'o,...)","Em opera'#231#227'o (Divis'#227'o,...)","Destino oper' +
        'a'#231#227'o (Divis'#227'o,...)","Baixa de opera'#231#227'o (Divis'#227'o,...)","Baixa em ' +
        'pr'#233' reclasse","Entrada para reclasse","Totalizador de Artigo Ger' +
        'ado","Item de gera'#231#227'o de Artigo Gerado","Item de baixa de Artigo' +
        ' In Natura","Artigo In Natura","Artigo Gerado","Artigo Classific' +
        'ado","Artigo em Opera'#231#227'o","Origem semi acabado em processo","Sem' +
        'i acabado em processo","Destino semi acabado em processo","Baixa' +
        ' de semi acabado em processo","Artigo Semi Acabado","Artigo Acab' +
        'ado","Sub produto") NO_MovimNiv,'
      'MovimID, MovimNiv, COUNT(Controle) ITENS '
      'FROM bluederm_teste.vsmovits'
      'WHERE MovimTwn IN ( '
      '  SELECT MovimTwn '
      '  FROM _TESTE_ '
      '  WHERE ITENS=1 '
      ') '
      'GROUP BY MovimID, MovimNiv'
      'ORDER BY MovimID, MovimNiv'
      '; '
      'DROP TABLE IF EXISTS _TESTE_; ')
    Left = 72
    Top = 128
    object QrSumsNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 22
    end
    object QrSumsNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 33
    end
    object QrSumsMovimID: TIntegerField
      FieldName = 'MovimID'
      Required = True
    end
    object QrSumsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrSumsITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsSums: TDataSource
    DataSet = QrSums
    Left = 72
    Top = 176
  end
end
