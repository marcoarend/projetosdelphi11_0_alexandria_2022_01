unit VSCfgMovEFD;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, mySQLDbTables,
  dmkDBGridZTO, Vcl.Mask, UnDmkProcFunc, dmkDBLookupComboBox, dmkEditCB,
  AppListas, UnProjGroup_Vars, UnProjGroup_Consts, mySQLDirectQuery,
  dmkCheckGroup, UMySQLDB, UnAppEnums;

type
  TFmVSCfgMovEFD = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrLista3: TmySQLQuery;
    DsLista3: TDataSource;
    QrLista3Controle: TIntegerField;
    QrLista3GraGruX: TIntegerField;
    QrLista3Pecas: TFloatField;
    QrLista3AreaM2: TFloatField;
    QrLista3PesoKg: TFloatField;
    QrLista3SdoVrtPeca: TFloatField;
    QrLista3SdoVrtArM2: TFloatField;
    QrLista3SdoVrtPeso: TFloatField;
    QrLista3BxaPeca: TFloatField;
    QrLista3BxaAreaM2: TFloatField;
    QrLista3BxaPesoKg: TFloatField;
    QrLista3DifPeca: TFloatField;
    QrLista3DifAreaM2: TFloatField;
    QrLista3DifPesoKg: TFloatField;
    QrSoma: TmySQLQuery;
    QrSomaPecas: TFloatField;
    QrSomaAreaM2: TFloatField;
    QrSomaPesoKg: TFloatField;
    PB1: TProgressBar;
    QrLista3DataHora: TDateTimeField;
    DBGLista3: TdmkDBGridZTO;
    QrLista0: TmySQLQuery;
    QrLista0Controle: TIntegerField;
    QrLista0DataHora: TDateTimeField;
    QrLista0GraGruX: TIntegerField;
    QrLista0Pecas: TFloatField;
    QrLista0AreaM2: TFloatField;
    QrLista0PesoKg: TFloatField;
    QrLista0SdoVrtPeca: TFloatField;
    QrLista0SdoVrtArM2: TFloatField;
    QrLista0SdoVrtPeso: TFloatField;
    QrLista0BxaPeca: TFloatField;
    QrLista0BxaAreaM2: TFloatField;
    QrLista0BxaPesoKg: TFloatField;
    QrLista0DifPeca: TFloatField;
    QrLista0DifAreaM2: TFloatField;
    QrLista0DifPesoKg: TFloatField;
    dmkDBGridZTO2: TdmkDBGridZTO;
    QrFonte: TmySQLQuery;
    DsFonte: TDataSource;
    Splitter1: TSplitter;
    Panel5: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DsSoma: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Panel6: TPanel;
    QrItens: TmySQLQuery;
    QrLista3ErrData: TWideStringField;
    QrLista3ErrSdo0: TWideStringField;
    QrMovimID: TmySQLQuery;
    DsMovimID: TDataSource;
    QrMovimIDCodigo: TIntegerField;
    QrMovimIDNome: TWideStringField;
    QrLista0MovimID: TIntegerField;
    QrLista0MovimNiv: TIntegerField;
    QrFonteDataHora: TDateTimeField;
    QrFonteControle: TIntegerField;
    QrFonteMovimID: TIntegerField;
    QrFonteCodigo: TIntegerField;
    QrFonteMovimCod: TIntegerField;
    QrFonteGraGruX: TIntegerField;
    QrFontePecas: TFloatField;
    QrFonteAreaM2: TFloatField;
    QrFontePesoKg: TFloatField;
    QrFonteKgPeca: TFloatField;
    QrFonteMovimNiv: TIntegerField;
    QrLista3MovimID: TIntegerField;
    QrLista3MovimNiv: TIntegerField;
    QrLista3MovimCod: TIntegerField;
    QrLista3Codigo: TIntegerField;
    QrGGXs: TmySQLQuery;
    QrGGXsDstCtrl: TIntegerField;
    QrGGXsDstGGX: TIntegerField;
    QrGGXsSrcCtrl: TIntegerField;
    QrGGXsSrcGGX: TIntegerField;
    QrListaPre: TmySQLQuery;
    QrListaPreMovimID: TIntegerField;
    QrListaPreMovimNiv: TIntegerField;
    QrListaPreCodigo: TIntegerField;
    QrListaPreMovimCod: TIntegerField;
    QrListaPreControle: TIntegerField;
    QrListaPreDataHora: TDateTimeField;
    QrListaPreGraGruX: TIntegerField;
    QrListaPrePecas: TFloatField;
    QrListaPreAreaM2: TFloatField;
    QrListaPrePesoKg: TFloatField;
    QrListaPreSdoVrtPeca: TFloatField;
    QrListaPreSdoVrtArM2: TFloatField;
    QrListaPreSdoVrtPeso: TFloatField;
    QrListaPreBxaPeca: TFloatField;
    QrListaPreBxaAreaM2: TFloatField;
    QrListaPreBxaPesoKg: TFloatField;
    QrListaPreDifPeca: TFloatField;
    QrListaPreDifAreaM2: TFloatField;
    QrListaPreDifPesoKg: TFloatField;
    QrListaPreErrData: TWideStringField;
    QrListaPreErrSdo0: TWideStringField;
    QrLista3ErrGGXs: TWideStringField;
    QrLista3ErrIMEI: TIntegerField;
    QrLista3QtdErros: TIntegerField;
    QrListaPreQtdErros: TIntegerField;
    QrBaixas: TmySQLQuery;
    QrBaixasCtrlInn: TIntegerField;
    QrBaixasCtrlBxa: TIntegerField;
    QrLista3ErrBxa: TWideStringField;
    QrBaixasGGXInn: TIntegerField;
    QrBaixasGGXBxa: TIntegerField;
    QrClaRcl: TmySQLQuery;
    QrClaRclControle: TIntegerField;
    QrClaRclGraGruX: TIntegerField;
    QrClaRclSrcGGX: TIntegerField;
    QrClaRclCtrl1: TIntegerField;
    QrClaRclDstGGX: TIntegerField;
    QrLista3ErrClRc: TWideStringField;
    BtCorrige: TBitBtn;
    MeAvisos: TMemo;
    DqErrCaleado: TmySQLDirectQuery;
    Panel7: TPanel;
    LaAno: TLabel;
    CBAno: TComboBox;
    CBMes: TComboBox;
    LaMes: TLabel;
    EdMovimID: TdmkEditCB;
    CBMovimID: TdmkDBLookupComboBox;
    CkIDMovimento: TCheckBox;
    CGPesquisas: TdmkCheckGroup;
    Panel8: TPanel;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    RGFiltro: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrLista3AfterScroll(DataSet: TDataSet);
    procedure DBGLista3DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dmkDBGridZTO2DblClick(Sender: TObject);
    procedure DBGLista3DblClick(Sender: TObject);
    procedure BtCorrigeClick(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure RGFiltroClick(Sender: TObject);
  private
    { Private declarations }
    FDiaIni, FDiaFim: TDateTime;
    //
    procedure PesquisaErros();
    procedure ReopenEfdIcmsIpiCfgErr();
  public
    { Public declarations }
    FImporExpor, FAnoMes, FEmpresa, FPeriApu: Integer;
    //
    procedure PreencheAnoMesPeriodo(AnoMes: Integer);
  end;

  var
  FmVSCfgMovEFD: TFmVSCfgMovEFD;

implementation

uses UnMyObjects, DmkDAC_PF, Module, ModuleGeral, UMySQLModule, UnVS_PF;

{$R *.DFM}

procedure TFmVSCfgMovEFD.BtCorrigeClick(Sender: TObject);
var
  Controle, SrcGGX, DstGGX: Integer;
begin
  if UpperCase(QrLista3ErrClRc.Value) = 'SIM' then
  begin
    Controle := QrLista3Controle.Value;
    SrcGGX := QrLista3GraGruX.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'SrcGGX'], ['Controle'], [SrcGGX], [Controle], True);
    //
    Controle := QrLista3ErrIMEI.Value;
    DstGGX := QrLista3GraGruX.Value;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'DstGGX'], ['Controle'], [DstGGX], [Controle], True);
    //
  end else
    Geral.MB_Erro('Corre��o de erro n�o implementada!');
end;

procedure TFmVSCfgMovEFD.BtNenhumClick(Sender: TObject);
begin
  CGPesquisas.Value := 0;
end;

procedure TFmVSCfgMovEFD.BtOKClick(Sender: TObject);
begin
  PesquisaErros();
end;

procedure TFmVSCfgMovEFD.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCfgMovEFD.BtTudoClick(Sender: TObject);
begin
  CGPesquisas.Value := CGPesquisas.MaxValue;
end;

procedure TFmVSCfgMovEFD.DBGLista3DblClick(Sender: TObject);
var
  IMEI: Integer;
begin
  IMEI := QrLista3.FieldByName('ErrIMEI').Value;
  if IMEI <> 0 then
    VS_PF.MostraFormVS_Do_IMEI(IMEI)
  else
    VS_PF.MostroFormVSMovimCod(QrLista3MovimCod.Value);
  Screen.Cursor := crDefault;
end;

procedure TFmVSCfgMovEFD.DBGLista3DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  CorTexto, CorFundo: TColor;
begin
  if (QrLista3ErrData.Value = 'Sim')
  or (QrLista3ErrSdo0.Value = 'Sim')
  or (QrLista3ErrGGXs.Value = 'Sim')
  or (QrLista3ErrBxa.Value  = 'Sim')
  or (QrLista3ErrClRc.Value = 'Sim') then
  begin
    CorTexto := clRed;
    CorFundo := clSilver;
    MyObjects.DesenhaTextoEmDBGrid(TDBGrid(DBGLista3), Rect, CorTexto, CorFundo,
      Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TFmVSCfgMovEFD.dmkDBGridZTO2DblClick(Sender: TObject);
begin
  VS_PF.MostroFormVSMovimCod(QrFonte.FieldByName('MovimCod').Value);
  Screen.Cursor := crDefault;
end;

procedure TFmVSCfgMovEFD.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSCfgMovEFD.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MeAvisos.Lines.Clear;
  //
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
  UnDmkDAC_PF.AbreMySQLQuery0(QrMovimID, Dmod.MyDB, [
  'SELECT Codigo, Nome',
  'FROM vsmovimid',
  'ORDER BY Nome',
  '']);
end;

procedure TFmVSCfgMovEFD.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCfgMovEFD.PesquisaErros();
var
  BxaPeca, BxaAreaM2, BxaPesoKg,
  SdoPeca, SdoAreaM2, SdoPesoKg,
  DifPeca, DifAreaM2, DifPesoKg: Double;
  Ano, Mes, Controle, QtdReg1: Integer;
  ErrData, ErrSdo0, SQL_Periodo, SQL_DiaIni, SQL_Data: String;
  MovimID, QtdErros, ErrIMEI, MyCtrl: Integer;
  SQL_WHERE, SQL_ID_WER, SQL_ID_AND, SQL_EXECUTE: String;
  MovimNiv: TEstqMovimNiv;
  T1, T2: Cardinal;
  DtHrPesq, CamposZ: String;
begin
  T1 := GetTickCount;
  //
  Screen.Cursor := crHourGlass;
  try
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando pesquisas');
  Ano := Geral.IMV(CBAno.Text);
  Mes := CBMes.ItemIndex + 1;
  FDiaIni := EncodeDate(Ano, Mes, 1);
  FDiaFim := IncMonth(FDiaIni, 1) -1;
  SQL_DiaIni  := '"' + Geral.FDT(FDiaIni, 1) + '"';
  SQL_Periodo := dmkPF.SQL_Periodo('WHERE DataHora ', FDiaIni, FDiaFim, True, True);
  MovimID := EdMovimID.ValueVariant;
  SQL_WHERE := 'WHERE vmi.Pecas + vmi.PesoKg >= 0 ';
  if CkIDMovimento.Checked then
  begin
    SQL_ID_WER := 'WHERE MovimID=' + Geral.FF0(MovimID);
    SQL_ID_AND := 'AND MovimID=' + Geral.FF0(MovimID);
  end else
  begin
    SQL_ID_WER := '';
    SQL_ID_AND := '';
    MovimNiv := VS_PF.ObtemMovimNivEFDDeMovimID(TEstqMovimID(MovimID));
    if MovimNiv <> eminSemNiv then
      SQL_WHERE := 'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(MovimNiv));
  end;
  //
  QrLista3.Close;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando g�meos orf�os');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _SPED_MOVIMTWN_1_;',
  'CREATE TABLE _SPED_MOVIMTWN_1_',
  '',
  'SELECT MovimTwn, MovimID, MovimNiv, ',
  'COUNT(Controle) Itens ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  'WHERE MovimTwn <> 0',
  'AND (',
  '  MovimID <> 25',
  '  AND ',
  '  MovimNiv <> 34',
  ')',
  'GROUP BY MovimTwn ',
  '',
  '']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando IME-Cs dentro do per�odo');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _sped_imecs_periodo_; ',
  'CREATE TABLE _sped_imecs_periodo_ ',
  'SELECT DISTINCT MovimCod ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  SQL_Periodo,
  ' ',
  '; ',
  ' ']));

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando IME-Cs fora do per�odo 1/2');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _SPED_NIVEIS1_XTRA_1_;',
  'CREATE TABLE _SPED_NIVEIS1_XTRA_1_',
  '',
  'SELECT DISTINCT SrcNivel1 Codigo, SrcMovID MovimID',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  SQL_Periodo,
  'AND SrcNivel1 <> 0',
  '',
  'UNION ',
  '',
  'SELECT DISTINCT DstNivel1 Codigo, DstMovID MovimID',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  SQL_Periodo,
  'AND DstNivel1 <> 0',
  '',
  'UNION ',
  '',
  'SELECT DISTINCT JmpNivel1 Codigo, JmpMovID MovimID',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  SQL_Periodo,
  'AND JmpNivel1 <> 0',
  '',
  'UNION ',
  '',
  'SELECT DISTINCT RmsNivel1 Codigo, RmsMovID MovimID',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  SQL_Periodo,
  'AND RmsNivel1 <> 0',
  ';',
  '']));
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando IME-Cs fora do per�odo 2/2');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _SPED_NIVEIS1_XTRA_2_;',
  'CREATE TABLE _SPED_NIVEIS1_XTRA_2_',
  'SELECT vmi.Controle, vmi.DataHora',
  'FROM _SPED_NIVEIS1_XTRA_1_ xt1, ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  'WHERE vmi.Codigo=xt1.Codigo',
  '  AND vmi.MovimID=xt1.MovimID',
  ';',
  //'SELECT * FROM _SPED_NIVEIS1_XTRA_2_',
  '']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando IME-Is fora do per�odo');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _Lista_GGX_1;  ',
  'CREATE TABLE _Lista_GGX_1  ',
  '  ',
////////////////////////////////////////////////////////////////////////////////
(*
  'SELECT SrcNivel2 Controle  ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  SQL_Periodo,
  'AND SrcNivel2 IN (  ',
  '  SELECT Controle  ',
  '  FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  '   ',
  ')  ',
  '  ',
  'UNION  ',
*)
////////////////////////////////////////////////////////////////////////////////
  'SELECT Controle   ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  'WHERE Controle IN (   ',
  '  SELECT SrcNivel2 ',
  '  FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  '  ' + SQL_Periodo,
  '  AND SrcNivel2 <> 0    ',
  ')   ',
  '  ',
////////////////////////////////////////////////////////////////////////////////
  'UNION  ',
////////////////////////////////////////////////////////////////////////////////
  'SELECT Controle   ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  'WHERE Controle IN (   ',
  '  SELECT JmpNivel2 ',
  '  FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  '  ' + SQL_Periodo,
  '  AND JmpNivel2 <> 0    ',
  ')   ',
  '  ',
////////////////////////////////////////////////////////////////////////////////
  'UNION  ',
////////////////////////////////////////////////////////////////////////////////
  'SELECT Controle   ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  'WHERE Controle IN (   ',
  '  SELECT DstNivel2 ',
  '  FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  '  ' + SQL_Periodo,
  '  AND DstNivel2 <> 0    ',
  ')   ',
  '  ',
////////////////////////////////////////////////////////////////////////////////
  'UNION  ',
////////////////////////////////////////////////////////////////////////////////
  'SELECT Controle   ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  'WHERE Controle IN (   ',
  '  SELECT RmsNivel2 ',
  '  FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  '  ' + SQL_Periodo,
  '  AND RmsNivel2 <> 0    ',
  ')   ',
  '  ',
////////////////////////////////////////////////////////////////////////////////
  'UNION  ',
////////////////////////////////////////////////////////////////////////////////
  'SELECT Controle   ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  'WHERE Controle IN (   ',
  '  SELECT GSPSrcNiv2 ',
  '  FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  '  ' + SQL_Periodo,
  '  AND GSPSrcNiv2 <> 0    ',
  ')   ',
  '  ',
////////////////////////////////////////////////////////////////////////////////
  'UNION  ',
////////////////////////////////////////////////////////////////////////////////
  'SELECT Controle   ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  'WHERE Controle IN (   ',
  '  SELECT GSPJmpNiv2 ',
  '  FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  '  ' + SQL_Periodo,
  '  AND GSPJmpNiv2 <> 0    ',
  ')   ',
  '  ',
////////////////////////////////////////////////////////////////////////////////
  'UNION  ',
////////////////////////////////////////////////////////////////////////////////
  '  ',
  'SELECT Controle  ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  SQL_Periodo,
  '  ',
  '  ',
////////////////////////////////////////////////////////////////////////////////
  'UNION  ',
////////////////////////////////////////////////////////////////////////////////
  '  ',
  'SELECT Controle  ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  'WHERE DataHora < ' + SQL_DiaIni,
  'AND SdoVrtPeca > 0  ',
  ' ',
////////////////////////////////////////////////////////////////////////////////
  'UNION  ',
////////////////////////////////////////////////////////////////////////////////
  ' ',
  'SELECT vmi.Controle ',
  'FROM _sped_imecs_periodo_ sip  ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ON sip.MovimCod=vmi.MovimCod ',
  ' ',
////////////////////////////////////////////////////////////////////////////////
  'UNION  ',
////////////////////////////////////////////////////////////////////////////////
  ' ',
  'SELECT Controle ',
  'FROM _SPED_NIVEIS1_XTRA_2_ ',
  ' ',
////////////////////////////////////////////////////////////////////////////////
  'UNION  ',
////////////////////////////////////////////////////////////////////////////////
  ' ',
  'SELECT vmi.Controle',
  'FROM _SPED_MOVIMTWN_1_ mt1 ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  '  ON vmi.MovimTwn=mt1.MovimTwn',
  'WHERE mt1.Itens=1',
  ' ',
////////////////////////////////////////////////////////////////////////////////
  //'UNION  ',
////////////////////////////////////////////////////////////////////////////////
  ';  ',
  '  ']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando IME-Is pesquisados');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _Lista_GGX_2;  ',
  'CREATE TABLE _Lista_GGX_2  ',
  '  ',
  'SELECT DISTINCT Controle ',
  'FROM _Lista_GGX_1  ',
  'ORDER BY Controle  ',
  ';  ',
  '  ',
  ' ']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando todos IMEI-s relavantes 1/3');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _lista_ggx_3; ',
  'CREATE TABLE _lista_ggx_3 ',
  'SELECT vmi.Controle, vmi.MovimID, vmi.MovimNiv, ',
  'vmi.Codigo, vmi.MovimCod, vmi.GraGruX, vmi.DataHora, ',
  'vmi.Pecas, vmi.AreaM2, vmi.PesoKg, ',
  'vmi.SdoVrtPeca, vmi.SdoVrtArM2, vmi.SdoVrtPeso, ',
  'vmi.Pecas BxaPeca, vmi.AreaM2 BxaAreaM2, vmi.PesoKg BxaPesoKg, ',
  'vmi.Pecas DifPeca, vmi.AreaM2 DifAreaM2, vmi.PesoKg DifPesoKg, ',
  '999999999 QtdErros, ',
  '"N�o" ErrData, "N�o" ErrSdo0, "N�o" ErrGGXs, "N�o" ErrBxa, ',
  '"N�o" ErrClRc, 999999999 ErrIMEI ',
  'FROM _Lista_GGX_2 lg2 ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi  ',
  '  ON vmi.Controle=lg2.Controle ',
  SQL_WHERE,
  SQL_ID_AND,
  //'AND MovimNiv NOT IN (14) ', // eminSorcCurtiXX
  ';',
  '']));
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando todos IMEI-s relavantes 2/3');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'UPDATE _lista_ggx_3 SET',
  '  QtdErros  = 0, ',
  '  BxaPeca   = 0, ',
  '  BxaAreaM2 = 0, ',
  '  BxaPesoKg = 0, ',
  '  DifPeca   = 0, ',
  '  DifAreaM2 = 0, ',
  '  DifPesoKg = 0, ',
  '  ErrIMEI   = 0; ',
  ' ']));
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando todos IMEI-s relavantes 3/3');
  UnDmkDAC_PF.AbreMySQLQuery0(QrLista0, DModG.MyPID_DB, [
  'SELECT *',
  'FROM _lista_ggx_3',
  '']);
  //
  if DmkPF.IntInConjunto2(1, CGPesquisas.Value) or
  DmkPF.IntInConjunto2(2, CGPesquisas.Value) then
  begin
    QrLista0.First;
    PB1.Position := 0;
    PB1.Max := QrLista0.RecordCount;
    while not QrLista0.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
      'Analisando datas e/ou saldos. ' + Geral.FF0(QrLista0.RecNo) + ' de ' +
      Geral.FF0(QrLista0.RecordCount));
      //
      QtdErros := 0;
      ErrData  := 'N�o';
      ErrSdo0  := 'N�o';
      if DmkPF.IntInConjunto2(1, CGPesquisas.Value) then
      begin
        VS_PF.AtualizaSaldoVirtualVSMovIts_Generico(
          QrLista0Controle.Value,
          QrLista0MovimID.Value,
          QrLista0MovimNiv.Value);
    (*
        UnDmkDAC_PF.AbreMySQLQuery0(QrSoma, Dmod.MyDB, [
        'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2,',
        'SUM(PesoKg) PesoKg',
        'FROM ' + CO_SEL_TAB_VMI + '',
        'WHERE SrcNivel2=' + Geral.FF0(QrLista0Controle.Value),
        '']);
        BxaPeca   := QrSomaPecas.Value;
        BxaAreaM2 := QrSomaAreaM2.Value;
        BxaPesoKg := QrSomaPesoKg.Value;
    *)
        BxaPeca   := VAR_BxaVrtPeca;
        BxaAreaM2 := VAR_BxaVrtArM2;
        BxaPesoKg := VAR_BxaVrtPeso;
        //
        SdoPeca   := VAR_SdoVrtPeca;
        SdoAreaM2 := VAR_SdoVrtArM2;
        SdoPesoKg := VAR_SdoVrtPeso;
        //
        DifPeca   := 0;
        DifAreaM2 := 0;
        DifPesoKg := 0;
    (*
        if (QrLista0Pecas.Value + QrLista0PesoKg.Value < 0)
        and (BxaPeca + BxaPesoKg < 0) then
        begin
          DifPeca   := -QrLista0Pecas.Value  + BxaPeca;
          DifAreaM2 := -QrLista0AreaM2.Value + BxaAreaM2;
          DifPesoKg := -QrLista0PesoKg.Value + BxaPesoKg;
        end else
        begin
          DifPeca   := QrLista0Pecas.Value  + BxaPeca;
          DifAreaM2 := QrLista0AreaM2.Value + BxaAreaM2;
          DifPesoKg := QrLista0PesoKg.Value + BxaPesoKg;
        end;
    *)
        Controle  := QrLista0Controle.Value;
        if (SdoPeca = 0 ) and (QrLista0Pecas.Value > 0) and (
        (SdoPesoKg <> 0) or (SdoAreaM2 <> 0)) then
        begin
          DifPeca   := SdoPeca;
          DifAreaM2 := SdoAreaM2;
          DifPesoKg := SdoPesoKg;
        end else
        begin
          if (QrLista0SdoVrtPeca.Value + QrLista0SdoVrtPeso.Value < 0)
          and (SdoPeca + SdoPesoKg < 0) then
          begin
            DifPeca   := -QrLista0SdoVrtPeca.Value  + SdoPeca;
            DifAreaM2 := -QrLista0SdoVrtArM2.Value + SdoAreaM2;
            DifPesoKg := -QrLista0SdoVrtPeso.Value + SdoPesoKg;
          end else
          begin
            DifPeca   := QrLista0SdoVrtPeca.Value  - SdoPeca;
            DifAreaM2 := QrLista0SdoVrtArM2.Value - SdoAreaM2;
            DifPesoKg := QrLista0SdoVrtPeso.Value - SdoPesoKg;
          end;
        end;
        //
        if DifPeca <=0 then
        begin
          if (DifAreaM2 >= 0.001) or (DifPesoKg >= 0.001)
          or (DifAreaM2 <= -0.001) or (DifPesoKg <= -0.001) then
          begin
            QtdErros := QtdErros + 1;
            ErrSdo0  := 'Sim';
          end;
        end;
      end;
     if DmkPF.IntInConjunto2(2, CGPesquisas.Value) then
     begin
        SQL_Data :=  '"' + Geral.FDT(Trunc(QrLista0DataHora.Value), 1) + '"';
        UnDmkDAC_PF.AbreMySQLQuery0(QrItens, Dmod.MyDB, [
        'SELECT Controle',
        'FROM ' + CO_SEL_TAB_VMI + '',
        'WHERE DATE(DataHora) <' + SQL_Data,
        'AND SrcNivel2=' + Geral.FF0(Controle),
        '']);
        if QrItens.RecordCount > 0 then
        begin
          QtdErros := QtdErros + 1;
          ErrData := 'Sim';
          //Geral.MB_Info(QrItens.SQL.Text);
          MeAvisos.Lines.Add(QrItens.SQL.Text);
        end;
      end;
      ErrIMEI := 0;
      //
      if DmkPF.IntInConjunto2(1, CGPesquisas.Value) and
      DmkPF.IntInConjunto2(2, CGPesquisas.Value) then
      begin
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_lista_ggx_3', False, [
        'BxaPeca', 'BxaAreaM2', 'BxaPesoKg',
        'DifPeca', 'DifAreaM2', 'DifPesoKg',
        'ErrData', 'ErrSdo0', 'ErrIMEI',
        'QtdErros'], [
        'Controle'], [
        BxaPeca, BxaAreaM2, BxaPesoKg,
        DifPeca, DifAreaM2, DifPesoKg,
        ErrData, ErrSdo0, ErrIMEI,
        QtdErros], [
        Controle], False);
      end
      else
      if DmkPF.IntInConjunto2(1, CGPesquisas.Value) then
      begin
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_lista_ggx_3', False, [
        'BxaPeca', 'BxaAreaM2', 'BxaPesoKg',
        'DifPeca', 'DifAreaM2', 'DifPesoKg',
        'ErrSdo0', 'ErrIMEI',
        'QtdErros'], [
        'Controle'], [
        BxaPeca, BxaAreaM2, BxaPesoKg,
        DifPeca, DifAreaM2, DifPesoKg,
        ErrSdo0, ErrIMEI,
        QtdErros], [
        Controle], False);
      end
      else
      if DmkPF.IntInConjunto2(2, CGPesquisas.Value) then
      begin
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_lista_ggx_3', False, [
        'ErrData', 'ErrIMEI',
        'QtdErros'], [
        'Controle'], [
        ErrData, ErrIMEI,
        QtdErros], [
        Controle], False);
      end;
      //
      QrLista0.Next;
    end;
  end;
////////////////////////////////////////////////////////////////////////////////
  if DmkPF.IntInConjunto2(4, CGPesquisas.Value) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '3. Pesquisando Reduzidos linkados. Gerando tabela espelho');
    //SQL_Periodo := dmkPF.SQL_Periodo('WHERE DataHora ', FDiaIni, FDiaFim, True, True);
    SQL_EXECUTE := Geral.ATS([
    //UnDmkDAC_PF.AbreMySQLQuery0(QrGGXs, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _compara_ggxs_imeis_src_; ',
    'CREATE TABLE _compara_ggxs_imeis_src_ ',
    'SELECT *  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
    //'WHERE DataHora BETWEEN "2017-02-01" AND "2017-02-28 23:59:59"; ',
    SQL_Periodo + ';',
    '']);
    //
    //Geral.MB_Info(SQL_EXECUTE);
    DModG.MyPID_CompressDB.Execute(SQL_EXECUTE);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando Reduzidos. Abrindo dados');
    UnDmkDAC_PF.AbreMySQLQuery0(QrGGXs, DModG.MyPID_DB, [
    'SELECT dst.Controle DstCtrl, dst.GraGruX DstGGX,  ',
    'src.Controle SrcCtrl, src.GragruX SrcGGX ',
    'FROM _compara_ggxs_imeis_src_ src ',
    'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' dst ',
    '  ON src.Controle=dst.SrcNivel2 ',
    'WHERE dst.Controle IS NOT NULL ',
    'AND dst.GraGruX <> src.GragruX ',
    '']);
    //Geral.MB_SQL(Self, QrGGXs);
    QrGGXs.First;
    PB1.Position := 0;
    PB1.Max := QrGGXs.RecordCount;
    while not QrGGXs.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
      'Pesquisando Reduzidos. ' + Geral.FF0(QrGGXs.RecNo) + ' de ' +
      Geral.FF0(QrGGXs.RecordCount));
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrListaPre, DModG.MyPID_DB, [
      'SELECT *',
      'FROM _lista_ggx_3',
      '']);
      if QrListaPre.Locate('Controle', QrGGXsSrcCtrl.Value, []) then
      begin
        QtdErros := QrListaPreQtdErros.Value + 1;
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_lista_ggx_3', False, [
        'QtdErros', 'ErrGGXs', 'ErrIMEI'], [
        'Controle'], [
        QtdErros, 'Sim', QrGGXsDstCtrl.Value], [
        QrGGXsSrcCtrl.Value], False);
      end else
      begin
        MeAvisos.Lines.Add('Aten��o! Erro de Reduzidos em movimento!' + sLineBreak +
        '  -> Reduzido origem: ' + Geral.FF0(QrGGXsSrcGGX.Value) +
        ' (IMEI ' + Geral.FF0(QrGGXsSrcCtrl.Value) + ')' + sLineBreak +
        '  -> Reduzido destino: ' + Geral.FF0(QrGGXsDstGGX.Value) +
        ' (IMEI ' + Geral.FF0(QrGGXsDstCtrl.Value) + ')' + sLineBreak);
      end;
      //
      QrGGXs.Next;
    end;
  end;
////////////////////////////////////////////////////////////////////////////////
  if DmkPF.IntInConjunto2(8, CGPesquisas.Value) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando Baixas de semi/acabado');
    //SQL_Periodo := dmkPF.SQL_Periodo('WHERE DataHora ', FDiaIni, FDiaFim, True, True);
    UnDmkDAC_PF.AbreMySQLQuery0(QrBaixas, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _SPED_ERR_PROC_BXA_; ',
    'CREATE TABLE _SPED_ERR_PROC_BXA_ ',
    'SELECT *  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
  //  'WHERE DataHora BETWEEN  ',
  //  '  "2017-07-01" AND "2017-07-31 23:59:59" ',
    SQL_Periodo,
    'AND MovimNiv=23; ',
    ' ',
    'DROP TABLE IF EXISTS _SPED_ERR_PROC_INN_; ',
    'CREATE TABLE _SPED_ERR_PROC_INN_ ',
    'SELECT inn.*  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' inn ',
    'LEFT JOIN _SPED_ERR_PROC_BXA_ bxa ON bxa.MovimCod=inn.MovimCod ',
    'WHERE inn.MovimNiv=21; ',
    ' ',
    'SELECT inn.Controle CtrlInn, bxa.Controle CtrlBxa,  ',
    'inn.GraGruX GGXInn, bxa.GraGruX GGXBxa  ',
    'FROM _SPED_ERR_PROC_BXA_ bxa ',
    'LEFT JOIN _SPED_ERR_PROC_INN_ inn ON inn.MovimCod=bxa.MovimCod ',
    'WHERE bxa.GraGruX <> inn.GraGruX ',
    '']);
    QrBaixas.First;
    QrGGXs.First;
    PB1.Position := 0;
    PB1.Max := QrBaixas.RecordCount;
    while not QrBaixas.Eof do
    begin
      MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
      'Pesquisando Baixas de semi/acabado. ' + Geral.FF0(QrBaixas.RecNo) + ' de ' +
      Geral.FF0(QrBaixas.RecordCount));
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrListaPre, DModG.MyPID_DB, [
      'SELECT *',
      'FROM _lista_ggx_3',
      '']);
      if QrListaPre.Locate('Controle', QrBaixasCtrlInn.Value, []) then
      begin
        QtdErros := QrListaPreQtdErros.Value + 1;
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_lista_ggx_3', False, [
        'QtdErros', 'ErrBxa', 'ErrIMEI'], [
        'Controle'], [
        QtdErros, 'Sim', QrBaixasCtrlBxa.Value], [
        QrBaixasCtrlInn.Value], False);
      end else
      begin
        MeAvisos.Lines.Add('Aten��o! Erro de Reduzidos em baixa!' + sLineBreak +
        '  -> Reduzido baixa: ' + Geral.FF0(QrBaixasGGXBxa.Value) +
        ' (IMEI ' + Geral.FF0(QrBaixasCtrlInn.Value) + ')' + sLineBreak +
        '  -> Reduzido em opera��o: ' + Geral.FF0(QrBaixasGGXInn.Value) +
        ' (IMEI ' + Geral.FF0(QrBaixasCtrlInn.Value) + ')' + sLineBreak);
      end;
      //
      QrBaixas.Next;
    end;
  end;
////////////////////////////////////////////////////////////////////////////////
  if DmkPF.IntInConjunto2(16, CGPesquisas.Value) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando couros classificados e reclassificados');
    // Pallets que tiveram seu Reduzido alterado!
    SQL_Periodo := dmkPF.SQL_Periodo('WHERE DataHora ', FDiaIni, FDiaFim, True, True);
    UnDmkDAC_PF.AbreMySQLQuery0(QrClaRcl, DModG.MyPID_DB, [
    'DROP TABLE IF EXISTS _IMV_CLA_RCL_1',
    ';',
    'CREATE TABLE _IMV_CLA_RCL_1',
    'SELECT Codigo, Controle, MovimCod, ',
    'MovimID, MovimNiv, MovimTwn, ',
    'GraGruX, SrcGGX, DstGGX',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '',
    //'WHERE DataHora BETWEEN "2018-11-01" AND "2018-11-30 23:59:59"',
    SQL_Periodo,
    'AND MovimID IN (7,8)',
    'AND MovimNiv=1',
    ';',
    'DROP TABLE IF EXISTS _IMV_CLA_RCL_2',
    ';',
    'CREATE TABLE _IMV_CLA_RCL_2',
    'SELECT Codigo, Controle, MovimCod, ',
    'MovimID, MovimNiv, MovimTwn, ',
    'GraGruX, SrcGGX, DstGGX',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '',
    //'WHERE DataHora BETWEEN "2018-11-01" AND "2018-11-30 23:59:59"',
    SQL_Periodo,
    'AND MovimID IN (7,8)',
    'AND MovimNiv=2',
    ';',
    'SELECT vm2.Controle, vm2.GraGruX, ',
    'vm2.SrcGGX, vm1.Controle Ctrl1, vm1.DstGGX ',
    'FROM _IMV_CLA_RCL_1 vm1',
    'LEFT JOIN _IMV_CLA_RCL_2 vm2 ON vm1.MovimTwn=vm2.MovimTwn',
    'WHERE vm2.GraGruX<>vm1.DstGGX',
    'OR vm2.SrcGGX<>vm1.DstGGX',
    ';',
    (*'DROP TABLE IF EXISTS _IMV_CLA_RCL_1',
    'DROP TABLE IF EXISTS _IMV_CLA_RCL_2',
    ';',*)
    '']);
    QrClaRcl.First;
    while not QrClaRcl.Eof do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrListaPre, DModG.MyPID_DB, [
      'SELECT *',
      'FROM _lista_ggx_3',
      '']);
      if QrListaPre.Locate('Controle', QrClaRclControle.Value, []) then
      begin
        QtdErros := QrListaPreQtdErros.Value + 1;
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_lista_ggx_3', False, [
        'QtdErros', 'ErrClRc', 'ErrIMEI'], [
        'Controle'], [
        QtdErros, 'Sim', QrClaRclCtrl1.Value], [
        QrClaRclControle.Value], False);
      end else
      begin
        MeAvisos.Lines.Add('Aten��o! Erro de Reduzidos em Classe / Reclasse!' + sLineBreak +
        '  -> Reduzido Destino: ' + Geral.FF0(QrClaRclGraGruX.Value) +
        ' (IMEI ' + Geral.FF0(QrClaRclControle.Value) + ')' + sLineBreak +
        '  -> Red. Src Destino: ' + Geral.FF0(QrClaRclSrcGGX.Value) +
        ' (IMEI ' + Geral.FF0(QrClaRclControle.Value) + ')' + sLineBreak +
        '  -> Reduzido Baixa: ' + Geral.FF0(QrClaRclDstGGX.Value) +
        ' (IMEI ' + Geral.FF0(QrClaRclCtrl1.Value) + ')' + sLineBreak);
      end;
      //
      QrClaRcl.Next;
    end;
  end;
////////////////////////////////////////////////////////////////////////////////
  if DmkPF.IntInConjunto2(32, CGPesquisas.Value) then
  begin
    // Couros caleados (MovimID=29) com MovimNiv=31 sem baixa instantanea MovimNiv=32
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Pesquisando couros caleados sem baixa instant�nea');
    //
    DModG.MyPID_CompressDB.Execute(Geral.ATS([
    'DROP TABLE IF EXISTS _SPED_ERR_CALEADO_31; ',
    'CREATE TABLE _SPED_ERR_CALEADO_31 ',
    ' ',
    'SELECT Controle ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimNiv=31 ',
    '; ',
    ' ',
    'DROP TABLE IF EXISTS _SPED_ERR_CALEADO_32; ',
    'CREATE TABLE _SPED_ERR_CALEADO_32 ',
    ' ',
    'SELECT Controle, SrcNivel2 ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
    'WHERE MovimNiv=32 ',
    '; ',
    '']));
    UnDmkDAC_PF.AbreMySQLDirectQuery0(DqErrCaleado, DModG.MyPID_CompressDB, [
    'SELECT _31.Controle  ',
    'FROM _SPED_ERR_CALEADO_31 _31 ',
    'LEFT JOIN _SPED_ERR_CALEADO_32 _32 ',
    'ON _32.SrcNivel2=_31.Controle ',
    'WHERE _32.SrcNivel2 IS NULL ',
    ' ',
    '']);
    //
    if DqErrCaleado.RecordCount > 0 then
    begin
      while not DqErrCaleado.Eof do
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrListaPre, DModG.MyPID_DB, [
        'SELECT *',
        'FROM _lista_ggx_3',
        '']);
        Controle := USQLDB.Int(DqErrCaleado, 'Controle');
        if QrListaPre.Locate('Controle', Controle, []) then
        begin
          QtdErros := QrListaPreQtdErros.Value + 1;
          ErrSdo0  := 'Sim';
          ErrIMEI  := Controle + 1;
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_lista_ggx_3', False, [
          'ErrSdo0', 'ErrIMEI', 'QtdErros'], [
          'Controle'], [
          ErrSdo0, ErrIMEI, QtdErros], [
          Controle], False);
        end else
          MeAvisos.Lines.Add('Aten��o! Erro de Reduzidos em Caleado!' + sLineBreak +
          '  -> IMEI ' + DqErrCaleado.FieldValueByFieldName('Controle') +
          ' IMEI de baixa n�o foi criado ou foi exclu�do!' + sLineBreak);
        //
         DqErrCaleado.Next;
      end;
    end;
  end;
  (*
  if DmkPF.IntInConjunto2(64, CGPesquisas.Value) then
  begin
    // pr�ximo aqui!
  end;
  *)
    //  No fim... Insere na tabela permanente e mostra resultados!
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando resultados 1/3');
  DModG.MyCompressDB.Execute(Geral.ATS([
  'DELETE FROM efdicmsipi_cfgerr ',
  'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
  'AND AnoMes=' + Geral.FF0(FAnoMes),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND PeriApu=' + Geral.FF0(FPeriApu),
  '']));
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando resultados 2/3');
  DtHrPesq := Geral.FDT(DMOdG.ObtemAgora(), 109);
  (*
  function ExcluiReceita_EnviaArquivoMorto(Numero, Motivo: Integer;
    Query: TmySQLQuery; DataBase: TmySQLDatabase): Boolean;
  var
    CamposZ: String;
  begin
  *)
  CamposZ := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, 'efdicmsipi_cfgerr', '', QtdReg1);
  //if Orientacao = TOrientTabArqBD.stabdAcessivelToMorto then
  //CamposZ := Geral.Substitui(CamposZ,
    //' ImporExpor', ' "' + Geral.FF0(FImporExpor) + '" ImporExpor');
  CamposZ := StringReplace(CamposZ, ' ImporExpor',
    ' ' + Geral.FF0(FImporExpor) + ' ImporExpor', [rfReplaceAll]);
  CamposZ := Geral.Substitui(CamposZ,
    ', AnoMes', ', ' + Geral.FF0(FAnoMes) + ' AnoMes');
  CamposZ := Geral.Substitui(CamposZ,
    ', Empresa', ', ' + Geral.FF0(FEmpresa) + ' Empresa');
  CamposZ := Geral.Substitui(CamposZ,
    ', PeriApu', ', ' + Geral.FF0(FPeriApu) + ' PeriApu');
  CamposZ := Geral.Substitui(CamposZ,
    ', DtHrPesq', ', "' + DtHrPesq + '" PeriApu');
  CamposZ := Geral.Substitui(CamposZ,
    ', Lk', ', 0 Lk');
  CamposZ := Geral.Substitui(CamposZ,
    ', DataCad', ', "' + DtHrPesq + '" DataCad');
  CamposZ := Geral.Substitui(CamposZ,
    ', UserCad', ', ' + FormatFloat('0', VAR_USUARIO) + ' UserCad');
  CamposZ := Geral.Substitui(CamposZ,
    ', DataAlt', ', "' + DtHrPesq + '" DataAlt');
  CamposZ := Geral.Substitui(CamposZ,
    ', UserAlt', ', ' + FormatFloat('0', VAR_USUARIO) + ' UserAlt');
  CamposZ := Geral.Substitui(CamposZ,
    ', AlterWeb', ', 0 AlterWeb');
  CamposZ := Geral.Substitui(CamposZ,
    ', AWServerID', ', 0 AWServerID');
  CamposZ := Geral.Substitui(CamposZ,
    ', AWStatSinc', ', 0 AWStatSinc');
  CamposZ := Geral.Substitui(CamposZ,
    ', Ativo', ', 1 Ativo');
  //
  CamposZ := 'INSERT INTO ' + TMeuDB + '.efdicmsipi_cfgerr SELECT ' + sLineBreak +
  CamposZ + sLineBreak +
  ' FROM _lista_ggx_3 ';
  //
  Geral.MB_Info(CamposZ);
  DModG.MyPID_CompressDB.Execute(CamposZ);
////////////////////////////////////////////////////////////////////////////////
(*
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'INSERT INTO ' + TMeuDB + '.efdicmsipi_cfgerr ',
  'SELECT ' + Geral.FF0(FImporExpor) + ' ImporExpor, ' +
  Geral.FF0(FAnoMes) + ' AnoMes, ',
  Geral.FF0(FEmpresa) + ' Empresa, ' +
  Geral.FF0(FPeriApu) + ' PeriApu, ',
  '"' + DtHrPesq + '" DtHrPesq, Controle, MovimID, MovimNiv, ' ,
  ' Codigo, MovimCod, GraGruX, DataHora, Pecas, AreaM2, PesoKg, ',
  ' SdoVrtPeca, SdoVrtArM2, SdoVrtPeso, BxaPeca, BxaAreaM2, BxaPesoKg, ' ,
  ' DifPeca, DifAreaM2, DifPesoKg, QtdErros, ErrData, ErrSdo0, ErrGGXs, ' ,
  ' ErrBxa, ErrClRc, ErrIMEI, 0 AWServerID, 0 AWStatSinc ' ,
  ' FROM _lista_ggx_3 ',
  ' ']));
*)
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Buscando resultados 3/3');
  ReopenEfdIcmsIpiCfgErr();
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
  T2 := GetTickCount;
  Geral.MB_Info('Tempo: ' + FloatToStr((T2-T1) / 1000) + ' s');
  if QrLista3.RecordCount = 0 then
    Geral.MB_Info('Nenhum registro foi encontrado para o MovimID selecionado!');
end;

procedure TFmVSCfgMovEFD.PreencheAnoMesPeriodo(AnoMes: Integer);
var
  Ano, Mes: Word;
begin
  DmkPF.AnoMesDecode(AnoMes, Ano, Mes);
  CBAno.Text := Geral.FF0(Ano);
  CBMes.ItemIndex := Mes - 1;
end;

procedure TFmVSCfgMovEFD.QrLista3AfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFonte, Dmod.MyDB, [
  'SELECT DataHora, Controle, MovimID, MovimNiv, Codigo,  ',
  'MovimCod, GraGruX, Pecas, AreaM2, PesoKg,  ',
  'IF(Pecas <> 0, PesoKg / Pecas, 0) KgPeca  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
  'WHERE vmi.SrcNivel2=' + Geral.FF0(QrLista3Controle.Value),
  'AND (vmi.Pecas + vmi.PesoKg < 0 ',
  'OR vmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_VS + ') ',
  ')  ',
  'UNION ',
  'SELECT DataHora, Controle, MovimID, MovimNiv, Codigo,  ',
  'MovimCod, GraGruX, Pecas, AreaM2, PesoKg,  ',
  'IF(Pecas <> 0, PesoKg / Pecas, 0) KgPeca  ',
  'FROM vsmovitb vmi  ',
  'WHERE vmi.SrcNivel2=' + Geral.FF0(QrLista3Controle.Value),
  'AND (vmi.Pecas + vmi.PesoKg < 0 ',
  'OR vmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_VS + ') ',
  ')  ',
  '']);
  //Geral.MB_SQL(Self, QrFonte);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSoma, Dmod.MyDB, [
  'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2,',
  'SUM(PesoKg) PesoKg',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'WHERE vmi.SrcNivel2=' + Geral.FF0(QrLista3Controle.Value),
  'AND (vmi.Pecas + vmi.PesoKg < 0 ',
  'OR vmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_VS + ') ',
  ') ',
  '',
  'UNION',
  '',
  'SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2,',
  'SUM(PesoKg) PesoKg',
  'FROM vsmovitb vmi ',
  'WHERE vmi.SrcNivel2=' + Geral.FF0(QrLista3Controle.Value),
  'AND (vmi.Pecas + vmi.PesoKg < 0 ',
  'OR vmi.MovimID IN (' + CO_ALL_CODS_BXA_POSIT_VS + ') ',
  ') ',
  '']);
  //
  BtCorrige.Enabled := UpperCase(QrLista3ErrClRc.Value) = 'SIM';
end;

procedure TFmVSCfgMovEFD.RGFiltroClick(Sender: TObject);
begin
  //if QrLista3.State <> dsInactive then
  ReopenEfdIcmsIpiCfgErr();
end;

procedure TFmVSCfgMovEFD.ReopenEfdIcmsIpiCfgErr();
var
  SQL_S_N: String;
begin
  case RGFiltro.ItemIndex of
    1: SQL_S_N := 'AND QtdErros > 0';
    else SQL_S_N := '';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLista3, DMod.MyDB, [
  'SELECT *',
  'FROM efdicmsipi_cfgerr ',
  'WHERE ImporExpor=' + Geral.FF0(FImporExpor),
  'AND AnoMes=' + Geral.FF0(FAnoMes),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  'AND PeriApu=' + Geral.FF0(FPeriApu),
  SQL_S_N,
  '']);
end;

end.
