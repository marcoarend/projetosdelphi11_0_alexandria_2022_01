object FmVSSifDipoaPesqBag: TFmVSSifDipoaPesqBag
  Left = 339
  Top = 185
  Caption = 'SIF-DIPOA-002 :: Pesquisa N'#186' de Identifica'#231#227'o do SIF/DIPOA'
  ClientHeight = 418
  ClientWidth = 692
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 692
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 644
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 596
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 528
        Height = 32
        Caption = 'Pesquisa N'#186' de Identifica'#231#227'o do SIF/DIPOA'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 528
        Height = 32
        Caption = 'Pesquisa N'#186' de Identifica'#231#227'o do SIF/DIPOA'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 528
        Height = 32
        Caption = 'Pesquisa N'#186' de Identifica'#231#227'o do SIF/DIPOA'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 304
    Width = 692
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    ExplicitTop = 515
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 688
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 348
    Width = 692
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 559
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 546
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 544
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 664
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 692
    Height = 256
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 812
    ExplicitHeight = 467
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 692
      Height = 45
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      object Label1: TLabel
        Left = 16
        Top = 4
        Width = 118
        Height = 13
        Caption = 'N'#250'mero de identifica'#231#227'o:'
      end
      object SbNumero: TSpeedButton
        Left = 72
        Top = 19
        Width = 23
        Height = 23
        Caption = '...'
        OnClick = SbNumeroClick
      end
      object EdNumero: TdmkEdit
        Left = 16
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object DBG04Estq: TdmkDBGridZTO
      Left = 0
      Top = 45
      Width = 692
      Height = 211
      Align = alClient
      DataSource = DsVSPallet
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      OnDblClick = DBG04EstqDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
          Width = 185
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID Pallet'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Pallet'
          Width = 112
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SeqSifDipoa'
          Title.Caption = 'N'#186' Identif.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataFab'
          Title.Caption = 'Validade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ObsSifDipoa'
          Title.Caption = 'Observa'#231#227'o'
          Width = 110
          Visible = True
        end>
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 65527
  end
  object QrVSPallet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT let.*,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome,'
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS'
      'FROM vspalleta let'
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa'
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat'
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      'WHERE SeqSifDipoa=1'
      'ORDER BY NO_PRD_TAM_COR'
      '')
    Left = 96
    Top = 145
    object QrVSPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPalletEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPalletNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPalletStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPalletCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPalletGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPalletNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPalletNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPalletDtHrEndAdd: TDateTimeField
      FieldName = 'DtHrEndAdd'
    end
    object QrVSPalletDtHrEndAdd_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DtHrEndAdd_TXT'
      Calculated = True
    end
    object QrVSPalletQtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
    object QrVSPalletGerRclCab: TIntegerField
      FieldName = 'GerRclCab'
    end
    object QrVSPalletDtHrFimRcl: TDateTimeField
      FieldName = 'DtHrFimRcl'
    end
    object QrVSPalletMovimIDGer: TIntegerField
      FieldName = 'MovimIDGer'
    end
    object QrVSPalletClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVSPalletStatPall: TIntegerField
      FieldName = 'StatPall'
      Required = True
    end
    object QrVSPalletSeqSifDipoa: TIntegerField
      FieldName = 'SeqSifDipoa'
      Required = True
    end
    object QrVSPalletObsSifDipoa: TWideStringField
      FieldName = 'ObsSifDipoa'
      Required = True
      Size = 60
    end
    object QrVSPalletDataFab: TDateField
      FieldName = 'DataFab'
    end
  end
  object DsVSPallet: TDataSource
    DataSet = QrVSPallet
    Left = 96
    Top = 193
  end
end
