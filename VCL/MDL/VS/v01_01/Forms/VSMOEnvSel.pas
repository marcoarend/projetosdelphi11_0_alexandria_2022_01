unit VSMOEnvSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBGridZTO, mySQLDbTables, dmkDBLookupComboBox, dmkEditCB;

type
  TFmVSMOEnvSel = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    DBGVSMOEnvEnv: TdmkDBGridZTO;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    Label9: TLabel;
    EdNFEMP_SerNF: TdmkEdit;
    EdNFEMP_nNF: TdmkEdit;
    Label12: TLabel;
    Label31: TLabel;
    EdNFEMP_Terceiro: TdmkEditCB;
    CBNFEMP_Terceiro: TdmkDBLookupComboBox;
    EdNFEMP_Empresa: TdmkEdit;
    Label11: TLabel;
    QrVSMOEnvEnv: TmySQLQuery;
    QrVSMOEnvEnvCodigo: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatID: TIntegerField;
    QrVSMOEnvEnvNFEMP_FatNum: TIntegerField;
    QrVSMOEnvEnvNFEMP_Empresa: TIntegerField;
    QrVSMOEnvEnvNFEMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvNFEMP_nItem: TIntegerField;
    QrVSMOEnvEnvNFEMP_SerNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_nNF: TIntegerField;
    QrVSMOEnvEnvNFEMP_Pecas: TFloatField;
    QrVSMOEnvEnvNFEMP_PesoKg: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaM2: TFloatField;
    QrVSMOEnvEnvNFEMP_AreaP2: TFloatField;
    QrVSMOEnvEnvNFEMP_ValorT: TFloatField;
    QrVSMOEnvEnvVSVMI_MovimCod: TIntegerField;
    QrVSMOEnvEnvCFTMP_FatID: TIntegerField;
    QrVSMOEnvEnvCFTMP_FatNum: TIntegerField;
    QrVSMOEnvEnvCFTMP_Empresa: TIntegerField;
    QrVSMOEnvEnvCFTMP_Terceiro: TIntegerField;
    QrVSMOEnvEnvCFTMP_nItem: TIntegerField;
    QrVSMOEnvEnvCFTMP_SerCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_nCT: TIntegerField;
    QrVSMOEnvEnvCFTMP_Pecas: TFloatField;
    QrVSMOEnvEnvCFTMP_PesoKg: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaM2: TFloatField;
    QrVSMOEnvEnvCFTMP_AreaP2: TFloatField;
    QrVSMOEnvEnvCFTMP_PesTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_CusTrKg: TFloatField;
    QrVSMOEnvEnvCFTMP_ValorT: TFloatField;
    DsVSMOEnvEnv: TDataSource;
    EdVSVMI_MovimCod: TdmkEdit;
    Label1: TLabel;
    QrVSMOEnvEnvNFEMP_SdoPeca: TFloatField;
    QrVSMOEnvEnvNFEMP_SdoPeso: TFloatField;
    QrVSMOEnvEnvNFEMP_SdoArM2: TFloatField;
    CkComSaldo: TCheckBox;
    BtPesquisa: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGVSMOEnvEnvDblClick(Sender: TObject);
    procedure QrVSMOEnvEnvAfterOpen(DataSet: TDataSet);
    procedure BtPesquisaClick(Sender: TObject);
    procedure EdVSVMI_MovimCodRedefinido(Sender: TObject);
  private
    { Private declarations }
    procedure ConfirmaEFecha();
  public
    { Public declarations }
    FTabVMIQtdEnvRVMI: String;
    //
    procedure Pesquisa();
  end;

  var
  FmVSMOEnvSel: TFmVSMOEnvSel;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule, ModuleGeral;

{$R *.DFM}

procedure TFmVSMOEnvSel.BtOKClick(Sender: TObject);
begin
  ConfirmaEFecha();
end;

procedure TFmVSMOEnvSel.BtPesquisaClick(Sender: TObject);
begin
  Pesquisa();
end;

procedure TFmVSMOEnvSel.BtSaidaClick(Sender: TObject);
begin
  //FVSVMI_MovimCod := 0;
  //FVSMOEnvEnv     := 0;
  //
  Close;
end;

procedure TFmVSMOEnvSel.ConfirmaEFecha();
  procedure InsereAtual();
  var
    VSMOEnvEnv, NFeSer, NFeNum, IDItem: Integer;
    Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
    SQLType: TSQLType;
  begin
    SQLType        := stIns;
    VSMOEnvEnv     := QrVSMOEnvEnvCodigo.Value;
    NFeSer         := QrVSMOEnvEnvNFEMP_SerNF.Value;
    NFeNum         := QrVSMOEnvEnvNFEMP_nNF.Value;
(*
    Pecas          := QrVSMOEnvEnvNFEMP_Pecas.Value;
    PesoKg         := QrVSMOEnvEnvNFEMP_PesoKg.Value;
    AreaM2         := QrVSMOEnvEnvNFEMP_AreaM2.Value;
    AreaP2         := QrVSMOEnvEnvNFEMP_AreaP2.Value;
    ValorT         := QrVSMOEnvEnvNFEMP_ValorT.Value;
*)
    Pecas          := QrVSMOEnvEnvNFEMP_SdoPeca.Value;
    PesoKg         := QrVSMOEnvEnvNFEMP_SdoPeso.Value;
    AreaM2         := QrVSMOEnvEnvNFEMP_SdoArM2.Value;
    AreaP2         := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    ValorT         := QrVSMOEnvEnvNFEMP_ValorT.Value;
    if ValorT <> 0 then
    begin
      if (QrVSMOEnvEnvNFEMP_AreaM2.Value <> 0) and (AreaM2 > 0) then
        ValorT := ValorT * (AreaM2 / QrVSMOEnvEnvNFEMP_AreaM2.Value)
      else
      if (QrVSMOEnvEnvNFEMP_PesoKg.Value <> 0) and (PesoKg > 0) then
        ValorT := ValorT * (PesoKg / QrVSMOEnvEnvNFEMP_PesoKg.Value)
      else
      if (QrVSMOEnvEnvNFEMP_Pecas.Value <> 0) and (Pecas > 0) then
        ValorT := ValorT * (Pecas / QrVSMOEnvEnvNFEMP_Pecas.Value);
    end;
    IDItem         := 0;
    //
    try
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, SQLType, FTabVMIQtdEnvRVMI, False, [
      'NFeSer', 'NFeNum', 'Pecas',
      'PesoKg', 'AreaM2', 'AreaP2',
      'ValorT', 'IDItem'], [
      'VSMOEnvEnv'], [
      NFeSer, NFeNum, Pecas,
      PesoKg, AreaM2, AreaP2,
      ValorT, IDItem], [
      VSMOEnvEnv], False);
    finally
      // continua?
    end;
  end;
var
  I: Integer;
begin
  if DBGVSMOEnvEnv.SelectedRows.Count = 0 then
  begin
    Geral.MB_Aviso('Selecione pelo menos um item!');
    Exit;
  end;
  //
  with DBGVSMOEnvEnv.DataSource.DataSet do
  for I := 0 to DBGVSMOEnvEnv.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DBGVSMOEnvEnv.SelectedRows.Items[I]));
    GotoBookmark(DBGVSMOEnvEnv.SelectedRows.Items[I]);
    //
    InsereAtual();
    //
  end;
  Close;
end;

procedure TFmVSMOEnvSel.DBGVSMOEnvEnvDblClick(Sender: TObject);
begin
  //ConfirmaEFecha();
end;

procedure TFmVSMOEnvSel.EdVSVMI_MovimCodRedefinido(Sender: TObject);
begin
  if EdVSVMI_MovimCod.ValueVariant = 0 then
    CkComSaldo.Checked := True;
end;

procedure TFmVSMOEnvSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSMOEnvSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  //FVSVMI_MovimCod := 0;
  //FVSMOEnvEnv     := 0;
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
end;

procedure TFmVSMOEnvSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSMOEnvSel.Pesquisa();
var
  NFEMP_SerNF, NFEMP_nNF, NFEMP_Terceiro, NFEMP_Empresa, VSVMI_MovimCod: Integer;
  SQL_SerNF, SQL_nNF, SQL_Terceiro, SQL_VMI, SQL_Saldo: String;
begin
  NFEMP_SerNF    := EdNFEMP_SerNF.ValueVariant;
  NFEMP_nNF      := EdNFEMP_nNF.ValueVariant;
  NFEMP_Terceiro := EdNFEMP_Terceiro.ValueVariant;
  NFEMP_Empresa  := EdNFEMP_Empresa.ValueVariant;
  VSVMI_MovimCod := EdVSVMI_MovimCod.ValueVariant;
  //
  SQL_SerNF      := '';
  SQL_nNF        := '';
  SQL_Terceiro   := '';
  SQL_VMI        := '';
  SQL_Saldo      := '';
  //
  if NFEMP_SerNF > - 1 then
    SQL_SerNF      := 'AND NFEMP_SerNF=' + Geral.FF0(NFEMP_SerNF);
  if NFEMP_nNF > 0 then
    SQL_nNF        := 'AND NFEMP_nNF=' + Geral.FF0(NFEMP_nNF);
  if NFEMP_Terceiro <> 0 then
    SQL_Terceiro   := 'AND NFEMP_Terceiro=' + Geral.FF0(NFEMP_Terceiro);
  if VSVMI_MovimCod <> 0 then
    SQL_VMI        := 'AND VSVMI_MovimCod=' + Geral.FF0(VSVMI_MovimCod);
  if CkComSaldo.Checked then
    SQL_Saldo :=  'AND NFEMP_SdoPeca>0';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMOEnvEnv, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsmoenvenv ',
  'WHERE NFEMP_Empresa=' + Geral.FF0(NFEMP_Empresa),
  SQL_SerNF,
  SQL_nNF,
  SQL_Terceiro,
  SQL_VMI,
  SQL_Saldo,
  '']);
  //Geral.MB_SQL(self, QrVSMOEnvEnv);
end;

procedure TFmVSMOEnvSel.QrVSMOEnvEnvAfterOpen(DataSet: TDataSet);
begin
  BtOK.Enabled := True;
end;

end.
