unit VSFormulasImp_FI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, Grids, dmkGeral, AppListas,
  DBGrids, frxClass, frxDBSet, UnDmkProcFunc, dmkImage, Vcl.ComCtrls,
  dmkEditDateTimePicker, UnDmkEnums, Vcl.Mask, UnProjGroup_Consts,
  UnVS_EFD_ICMS_IPI, UnAppEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmVSFormulasImp_FI = class(TForm)
    PnIUser: TPanel;
    QrTintasCab: TmySQLQuery;
    QrTintasCabNOMECLII: TWideStringField;
    QrTintasCabNOMESETOR: TWideStringField;
    QrTintasCabLINHAS: TWideStringField;
    QrTintasCabNumero: TIntegerField;
    QrTintasCabTxtUsu: TWideStringField;
    QrTintasCabNome: TWideStringField;
    QrTintasCabClienteI: TIntegerField;
    QrTintasCabTipificacao: TIntegerField;
    QrTintasCabDataI: TDateField;
    QrTintasCabDataA: TDateField;
    QrTintasCabTecnico: TWideStringField;
    QrTintasCabSetor: TIntegerField;
    QrTintasCabEspessura: TIntegerField;
    QrTintasCabAreaM2: TFloatField;
    QrTintasCabQtde: TFloatField;
    QrTintasCabLk: TIntegerField;
    QrTintasCabDataCad: TDateField;
    QrTintasCabDataAlt: TDateField;
    QrTintasCabUserCad: TIntegerField;
    QrTintasCabUserAlt: TIntegerField;
    QrTintasCabAlterWeb: TSmallintField;
    QrTintasCabAtivo: TSmallintField;
    DsTintasCab: TDataSource;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntNOMECI: TWideStringField;
    QrCliIntCodigo: TIntegerField;
    QrEspessuras: TmySQLQuery;
    QrEspessurasCodigo: TIntegerField;
    QrEspessurasLinhas: TWideStringField;
    QrEspessurasEMCM: TFloatField;
    DsEspessuras: TDataSource;
    DsImpriFlu: TDataSource;
    QrImpriIts: TmySQLQuery;
    QrTintasFlu: TmySQLQuery;
    QrTintasFluNOMEPROCESSO: TWideStringField;
    QrTintasFluOrdem: TIntegerField;
    QrTintasFluReordem: TIntegerField;
    QrTintasFluNumero: TIntegerField;
    QrTintasFluCodigo: TIntegerField;
    QrTintasFluNome: TWideStringField;
    QrTintasFluTintasTin: TIntegerField;
    QrTintasFluGramasM2: TFloatField;
    Panel3: TPanel;
    Label1: TLabel;
    EdTintasCab: TdmkEditCB;
    CBTintasCab: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    QrImpriFlu: TMySQLQuery;
    QrImpriFluAtivo: TSmallintField;
    QrImpriFluOrdemTin: TIntegerField;
    QrImpriFluCodigo: TIntegerField;
    QrImpriFluNomeFlu: TWideStringField;
    QrImpriFluNomeTin: TWideStringField;
    QrImpriFluTintasTin: TIntegerField;
    QrImpriFluGramasM2: TFloatField;
    QrTintasIts: TmySQLQuery;
    QrTintasItsNOMEPQ: TWideStringField;
    QrTintasItsCustoPadrao: TFloatField;
    QrTintasItsMoedaPadrao: TSmallintField;
    QrTintasItsTintasCab: TIntegerField;
    QrTintasItsTintasTin: TIntegerField;
    QrTintasItsControle: TIntegerField;
    QrTintasItsProduto: TIntegerField;
    QrTintasItsGramasTi: TFloatField;
    QrTintasItsGramaskg: TFloatField;
    QrTintasItsObs: TWideStringField;
    DsImpriIts: TDataSource;
    Panel4: TPanel;
    QrTintasItsORDEMITS: TIntegerField;
    QrTintasFluORDEMTIN: TIntegerField;
    QrImpriFluOrdemFlu: TIntegerField;
    QrImpriFluGramasTo: TFloatField;
    QrImpriFluCustoTo: TFloatField;
    QrImpriFluCustoKg: TFloatField;
    QrImpriItsOrdemFlu: TIntegerField;
    QrImpriItsOrdemIts: TIntegerField;
    QrImpriItsTintas: TIntegerField;
    QrImpriItsNome: TWideStringField;
    QrImpriItsGramasTi: TFloatField;
    QrImpriItsGramasKg: TFloatField;
    QrImpriItsGramasTo: TFloatField;
    QrImpriItsPrecoMo_Kg: TFloatField;
    QrImpriItsCotacao_Mo: TFloatField;
    QrImpriItsCustoRS_Kg: TFloatField;
    QrImpriItsCustoRS_To: TFloatField;
    QrImpriItsProduto: TIntegerField;
    DsDefPecas: TDataSource;
    QrDefPecas: TmySQLQuery;
    QrDefPecasCodigo: TIntegerField;
    QrDefPecasNome: TWideStringField;
    QrDefPecasGrandeza: TSmallintField;
    QrImpriItsSiglaMoeda: TWideStringField;
    QrTintasItsSIGLAMOEDA: TWideStringField;
    QrImpriFluAreaM2: TFloatField;
    QrImpriFluCustoM2: TFloatField;
    QrImpriFluCusUSM2: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    QrImpriItsPQCI: TIntegerField;
    QrImpriItsCustoPadrao: TFloatField;
    QrImpriItsMoedaPadrao: TSmallintField;
    QrImpriItsPRODUTOCI: TIntegerField;
    QrImpriItsControle: TIntegerField;
    Panel1: TPanel;
    PB1: TProgressBar;
    QrLotes: TmySQLQuery;
    QrPreuso: TmySQLQuery;
    QrPreusoCodigo: TIntegerField;
    QrPreusoControle: TIntegerField;
    QrPreusoPeso_PQ: TFloatField;
    QrPreusoProduto: TIntegerField;
    QrPreusoPQCI: TIntegerField;
    QrPreusoProdutoCI: TIntegerField;
    QrPreusoNomePQ: TWideStringField;
    QrPreusoPeso: TFloatField;
    QrPreusoPESOFUTURO: TFloatField;
    QrPreusoCli_Orig: TIntegerField;
    QrPreusoNOMECLI_ORIG: TWideStringField;
    QrPreusoDEFASAGEM: TFloatField;
    QrPreusoCustoPadrao: TFloatField;
    QrPreusoMoedaPadrao: TSmallintField;
    QrPreusoCustoMedio: TFloatField;
    QrPreusoEXISTE: TFloatField;
    QrPreusoAtivo: TSmallintField;
    QrSumCus: TmySQLQuery;
    QrSumCusCustoKg: TFloatField;
    QrSumCusCustoTo: TFloatField;
    QrSumCusCustoM2: TFloatField;
    QrSumCusCusUSM2: TFloatField;
    Panel5: TPanel;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    RGTipoPreco: TRadioGroup;
    DBGImpriFlu: TDBGrid;
    Panel2: TPanel;
    Panel8: TPanel;
    Label6: TLabel;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    EdMemo: TMemo;
    Panel9: TPanel;
    Label29: TLabel;
    EdAreaM2: TdmkEdit;
    Label30: TLabel;
    EdPecas: TdmkEdit;
    Label3: TLabel;
    EdDefPeca: TdmkEditCB;
    CBDefPeca: TdmkDBLookupComboBox;
    Label24: TLabel;
    EdEspessura: TdmkEditCB;
    CBEspessura: TdmkDBLookupComboBox;
    RGModeloImp: TRadioGroup;
    QrImpriItsAtivo: TSmallintField;
    QrTintasItsAtivo: TSmallintField;
    QrTintasItsCustoMedio: TFloatField;
    QrImpriItsCustoMedio: TFloatField;
    QrVal: TmySQLQuery;
    QrValCustoRS_To: TFloatField;
    QrSqr: TmySQLQuery;
    QrSqrAreaM2: TFloatField;
    QrTintasFluInfoCargM2: TFloatField;
    QrImpriFluInfoCargM2: TFloatField;
    QrTintasFluDescri: TWideStringField;
    QrImpriFluDescriFlu: TWideStringField;
    QrImpriFluOpProc: TSmallintField;
    QrTintasFluOpProc: TSmallintField;
    DsVMIAtu: TDataSource;
    QrVMIAtu: TmySQLQuery;
    QrVMIAtuCodigo: TLargeintField;
    QrVMIAtuControle: TLargeintField;
    QrVMIAtuMovimCod: TLargeintField;
    QrVMIAtuMovimNiv: TLargeintField;
    QrVMIAtuMovimTwn: TLargeintField;
    QrVMIAtuEmpresa: TLargeintField;
    QrVMIAtuTerceiro: TLargeintField;
    QrVMIAtuCliVenda: TLargeintField;
    QrVMIAtuMovimID: TLargeintField;
    QrVMIAtuDataHora: TDateTimeField;
    QrVMIAtuPallet: TLargeintField;
    QrVMIAtuGraGruX: TLargeintField;
    QrVMIAtuPecas: TFloatField;
    QrVMIAtuPesoKg: TFloatField;
    QrVMIAtuAreaM2: TFloatField;
    QrVMIAtuAreaP2: TFloatField;
    QrVMIAtuValorT: TFloatField;
    QrVMIAtuSrcMovID: TLargeintField;
    QrVMIAtuSrcNivel1: TLargeintField;
    QrVMIAtuSrcNivel2: TLargeintField;
    QrVMIAtuSrcGGX: TLargeintField;
    QrVMIAtuSdoVrtPeca: TFloatField;
    QrVMIAtuSdoVrtPeso: TFloatField;
    QrVMIAtuSdoVrtArM2: TFloatField;
    QrVMIAtuObserv: TWideStringField;
    QrVMIAtuSerieFch: TLargeintField;
    QrVMIAtuFicha: TLargeintField;
    QrVMIAtuMisturou: TLargeintField;
    QrVMIAtuFornecMO: TLargeintField;
    QrVMIAtuCustoMOKg: TFloatField;
    QrVMIAtuCustoMOM2: TFloatField;
    QrVMIAtuCustoMOTot: TFloatField;
    QrVMIAtuValorMP: TFloatField;
    QrVMIAtuDstMovID: TLargeintField;
    QrVMIAtuDstNivel1: TLargeintField;
    QrVMIAtuDstNivel2: TLargeintField;
    QrVMIAtuDstGGX: TLargeintField;
    QrVMIAtuQtdGerPeca: TFloatField;
    QrVMIAtuQtdGerPeso: TFloatField;
    QrVMIAtuQtdGerArM2: TFloatField;
    QrVMIAtuQtdGerArP2: TFloatField;
    QrVMIAtuQtdAntPeca: TFloatField;
    QrVMIAtuQtdAntPeso: TFloatField;
    QrVMIAtuQtdAntArM2: TFloatField;
    QrVMIAtuQtdAntArP2: TFloatField;
    QrVMIAtuNotaMPAG: TFloatField;
    QrVMIAtuNO_PALLET: TWideStringField;
    QrVMIAtuNO_PRD_TAM_COR: TWideStringField;
    QrVMIAtuNO_TTW: TWideStringField;
    QrVMIAtuID_TTW: TLargeintField;
    QrVMIAtuNO_FORNECE: TWideStringField;
    QrVMIAtuReqMovEstq: TLargeintField;
    QrVMIAtuCUSTO_M2: TFloatField;
    QrVMIAtuCUSTO_P2: TFloatField;
    QrVMIAtuNO_LOC_CEN: TWideStringField;
    QrVMIAtuMarca: TWideStringField;
    QrVMIAtuPedItsLib: TLargeintField;
    QrVMIAtuStqCenLoc: TLargeintField;
    QrVMIAtuNO_FICHA: TWideStringField;
    QrVMIAtuNO_FORNEC_MO: TWideStringField;
    QrVMIAtuClientMO: TLargeintField;
    DBEdit1: TDBEdit;
    Label8: TLabel;
    DBEdit2: TDBEdit;
    QrEmitGru: TmySQLQuery;
    QrEmitGruCodigo: TIntegerField;
    QrEmitGruNome: TWideStringField;
    DsEmitGru: TDataSource;
    EdEmitGru: TdmkEditCB;
    Label14: TLabel;
    CBEmitGru: TdmkDBLookupComboBox;
    QrVMIOriIMEI: TmySQLQuery;
    QrVMIOriIMEICodigo: TLargeintField;
    QrVMIOriIMEIControle: TLargeintField;
    QrVMIOriIMEIMovimCod: TLargeintField;
    QrVMIOriIMEIMovimNiv: TLargeintField;
    QrVMIOriIMEIMovimTwn: TLargeintField;
    QrVMIOriIMEIEmpresa: TLargeintField;
    QrVMIOriIMEITerceiro: TLargeintField;
    QrVMIOriIMEICliVenda: TLargeintField;
    QrVMIOriIMEIMovimID: TLargeintField;
    QrVMIOriIMEIDataHora: TDateTimeField;
    QrVMIOriIMEIPallet: TLargeintField;
    QrVMIOriIMEIGraGruX: TLargeintField;
    QrVMIOriIMEIPecas: TFloatField;
    QrVMIOriIMEIPesoKg: TFloatField;
    QrVMIOriIMEIAreaM2: TFloatField;
    QrVMIOriIMEIAreaP2: TFloatField;
    QrVMIOriIMEIValorT: TFloatField;
    QrVMIOriIMEISrcMovID: TLargeintField;
    QrVMIOriIMEISrcNivel1: TLargeintField;
    QrVMIOriIMEISrcNivel2: TLargeintField;
    QrVMIOriIMEISrcGGX: TLargeintField;
    QrVMIOriIMEISdoVrtPeca: TFloatField;
    QrVMIOriIMEISdoVrtPeso: TFloatField;
    QrVMIOriIMEISdoVrtArM2: TFloatField;
    QrVMIOriIMEIObserv: TWideStringField;
    QrVMIOriIMEISerieFch: TLargeintField;
    QrVMIOriIMEIFicha: TLargeintField;
    QrVMIOriIMEIMisturou: TLargeintField;
    QrVMIOriIMEIFornecMO: TLargeintField;
    QrVMIOriIMEICustoMOKg: TFloatField;
    QrVMIOriIMEICustoMOM2: TFloatField;
    QrVMIOriIMEICustoMOTot: TFloatField;
    QrVMIOriIMEIValorMP: TFloatField;
    QrVMIOriIMEIDstMovID: TLargeintField;
    QrVMIOriIMEIDstNivel1: TLargeintField;
    QrVMIOriIMEIDstNivel2: TLargeintField;
    QrVMIOriIMEIDstGGX: TLargeintField;
    QrVMIOriIMEIQtdGerPeca: TFloatField;
    QrVMIOriIMEIQtdGerPeso: TFloatField;
    QrVMIOriIMEIQtdGerArM2: TFloatField;
    QrVMIOriIMEIQtdGerArP2: TFloatField;
    QrVMIOriIMEIQtdAntPeca: TFloatField;
    QrVMIOriIMEIQtdAntPeso: TFloatField;
    QrVMIOriIMEIQtdAntArM2: TFloatField;
    QrVMIOriIMEIQtdAntArP2: TFloatField;
    QrVMIOriIMEINotaMPAG: TFloatField;
    QrVMIOriIMEINO_PALLET: TWideStringField;
    QrVMIOriIMEINO_PRD_TAM_COR: TWideStringField;
    QrVMIOriIMEINO_TTW: TWideStringField;
    QrVMIOriIMEIID_TTW: TLargeintField;
    QrVMIOriIMEINO_FORNECE: TWideStringField;
    QrVMIOriIMEINO_SerieFch: TWideStringField;
    QrVMIOriIMEIReqMovEstq: TLargeintField;
    QrVMIOriIMEIVSMovIts: TLargeintField;
    QrVMIOriIMEIPECAS_POSIT: TFloatField;
    QrVMIOriIMEIAREAM2_POSIT: TFloatField;
    QrVMIOriIMEIPESOKG_POSIT: TFloatField;
    DsVMIOriIMEI: TDataSource;
    LaData: TLabel;
    TPDataP: TdmkEditDateTimePicker;
    DBGrid1: TDBGrid;
    QrEmitCus: TmySQLQuery;
    QrEmitCusTextoECor: TWideStringField;
    QrEmitCusNOME_CLI: TWideStringField;
    QrEmitCusCodigo: TIntegerField;
    QrEmitCusControle: TIntegerField;
    QrEmitCusMPIn: TIntegerField;
    QrEmitCusFormula: TIntegerField;
    QrEmitCusDataEmis: TDateTimeField;
    QrEmitCusPeso: TFloatField;
    QrEmitCusCusto: TFloatField;
    QrEmitCusPecas: TFloatField;
    QrEmitCusAtivo: TSmallintField;
    QrEmitCusMPVIts: TIntegerField;
    QrEmitCusAreaM2: TFloatField;
    QrEmitCusAreaP2: TFloatField;
    QrEmitCusPercTotCus: TFloatField;
    DsEmitCus: TDataSource;
    EdHoraIni: TdmkEdit;
    LaHora: TLabel;
    CkDtCorrApo: TCheckBox;
    TPDtCorrApo: TdmkEditDateTimePicker;
    Label21: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdTintasCabChange(Sender: TObject);
    procedure DBGImpriFluDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGImpriFluCellClick(Column: TColumn);
    procedure DBGImpriFluColEnter(Sender: TObject);
    procedure DBGImpriFluColExit(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure QrImpriFluAfterScroll(DataSet: TDataSet);
    procedure QrTintasItsCalcFields(DataSet: TDataSet);
    procedure BtAdicionaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure QrVMIOriIMEICalcFields(DataSet: TDataSet);
    procedure CkDtCorrApoClick(Sender: TObject);
  private
    { Private declarations }
    FBaixa: Boolean;
    FDatabase: TmySQLDatabase;
    FQrySQL: TmySQLQuery;
    FTabBxaImpEmit: String;
    F_Impri_Flu, F_Impri_Its,
    F_Emit, F_EmitCus, F_EmitIts, F_EmitFlu: String;
    FUniqCod: Integer;
    FSeqCtrl: Integer;
    //
    procedure AtivaProcessos(Status: Byte);
    //
    function  InserirEmit(Codigo: Integer; Qry: TmySQLQuery;
              TabEmit, TabEmitCus, TabEmitFlu, TabEmitIts: String): Boolean;
    procedure InsereEmitCus(Codigo: Integer);
    function  InserirEmitCus(Codigo: Integer): Boolean;
    //
    procedure ReopenEmitCus();
    procedure ReopenImpriFlu(Codigo: Integer);
    procedure ReopenImpriIts();
    procedure ReopenTintasIts(Codigo: Integer);
  public
    { Public declarations }
    //FNomeForm: String;
    FMovimCod, FIMEI, FTemIMEIMrt: Integer;
    FMovimInn, FMovimSrc: TEstqMovimNiv;
    FCustoTotalTintas, FCustoTotalM2, FCustoTotUSM2: Double;
    //
    procedure DefineBaixa(Baixa: Boolean);
    procedure ReopenVMIAtu();
  end;

  var
  FmVSFormulasImp_FI: TFmVSFormulasImp_FI;

implementation

uses UnMyObjects, MyVCLSkin, Module, UnInternalConsts, ModuleGeral, DmkDAC_PF,
  CreateBlueDerm, UMySQLModule, GetValor, MyListas, ModEmit, FormulasImpOSs,
  UnPQ_PF, UnVS_PF, PQx;

{$R *.DFM}

const
  FAltLin = CO_POLEGADA / 2.16;
  FLinhaTxt = '________________________________________________________________________________________________________________________________________';
  FFloat_15_2 = '#,###,##0.00;-#,###,##0.00; ';
  FFloat_15_3 = '#,###,##0.000;-#,###,##0.000; ';
  FFloat_15_4 = '#,###,##0.0000;-#,###,##0.0000; ';

procedure TFmVSFormulasImp_FI.AtivaProcessos(Status: Byte);
var
  Codigo: Integer;
begin
  Codigo := QrImpriFluCodigo.Value;
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'UPDATE ' + F_Impri_Flu + ' SET Ativo=' + Geral.FF0(Status),
  '']);
  //
  ReopenImpriFlu(Codigo);
end;

procedure TFmVSFormulasImp_FI.BtAdicionaClick(Sender: TObject);
(*
const
  EdPeso = nil;
  Tabela = '_emit_cus_';
*)
begin
(*
object BtAdiciona: TBitBtn
  Tag = 10
  Left = 4
  Top = 22
  Width = 90
  Height = 40
  Hint = 'Confirma a senha digitada'
  Caption = '&Adiciona'
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  NumGlyphs = 2
  ParentFont = False
  TabOrder = 0
  OnClick = BtAdicionaClick
end
  if FBaixa then
  begin
    if FSeqCtrl = 0 then
    begin
      F_EmitCus := UnCreateBlueDerm.RecriaTempTableNovo(ntrttEmitCus,
                     DmodG.QrUpdPID1, False);
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DmodG.MyPID_DB, [
      'DELETE FROM ' + F_EmitCus,
      'WHERE Codigo=' + Geral.FF0(FUniqCod),
      '']);
    end;
    FSeqCtrl := FSeqCtrl + 1;
    Application.CreateForm(TFmFormulasImpOSs, FmFormulasImpOSs);
    FmFormulasImpOSs.ImgTipo.SQLType := stIns;
    FmFormulasImpOSs.FTipoReceitas := dmktfrmSetrEmi_ACABATO;
    //
    FmFormulasImpOSs.FEmit := FBaixa;
    FmFormulasImpOSs.F_EmitCus := F_EmitCus;
    FmFormulasImpOSs.FCodigo := FUniqCod;
    FmFormulasImpOSs.FControle := FSeqCtrl;
    FmFormulasImpOSs.FQryLote := QrEmitCus;
    FmFormulasImpOSs.FQryExe := DModG.QrUpdPID1;
    //
    FmFormulasImpOSs.ShowModal;
    FmFormulasImpOSs.Destroy;
    DmModEmit.AtualizaQuantidadesSourcMP_WetSome(Tabela, DModG.MyPID_DB,
      FUniqCod, EdPeso, EdPecas, EdAreaM2);
    ReopenEmitCus();
    //
    EdDefPeca.SetFocus;
  end;
*)
end;

procedure TFmVSFormulasImp_FI.BtExcluiClick(Sender: TObject);
(*
const
  EdPeso = nil;
  Tabela = '_emit_cus_';
*)
begin
(*
object BtExclui: TBitBtn
  Tag = 12
  Left = 4
  Top = 64
  Width = 90
  Height = 40
  Hint = 'Confirma a senha digitada'
  Caption = 'E&xclui'
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  NumGlyphs = 2
  ParentFont = False
  TabOrder = 0
  OnClick = BtExcluiClick
end
  if Geral.MB_Pergunta('Confirma a retirada do item selecionado?') = ID_YES then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'DELETE FROM ' + Tabela,
    'WHERE Controle=' + Geral.FF0(QrEmitCusControle.Value),
    '']);
    //
    DmModEmit.AtualizaQuantidadesSourcMP_WetSome(Tabela, DModG.MyPID_DB,
      FUniqCod, EdPeso, EdPecas, EdAreaM2);
    ReopenEmitCus();
    EdDefPeca.SetFocus;
  end;
*)
end;

procedure TFmVSFormulasImp_FI.BtNenhumClick(Sender: TObject);
begin
  AtivaProcessos(0);
end;

procedure TFmVSFormulasImp_FI.BtOKClick(Sender: TObject);
var
  TxtC, TxtD, Liga: String;
  PercTotCus, GramasTo, CustoTo, CustoKg, AreaM2,
  Area, Qtde, PesoItem, PrecoItem, CustoItem, PesoTinta, CustoTinta, Cotacao,
  CustoKgTinta, CustoM2, CusUSM2, PrecoMo_Kg, FatorM2, Custo: Double;
  TintasCab, ClienteI, TintasAtivas, Controle: Integer;
  //
  iOrdemTin, iOrdemIts, iTintas, iProduto, iNome, iGramasTi, iGramasKg,
  iGramasTo, iSiglaMoeda, iPrecoMo_Kg, iCotacao_Mo, iCustoRS_Kg,
  iCustoRS_To, Ativo: String;
  //
  EmitGru, CodIni, Codigo, TintasTin, DefPeca, Espessura, Formula, Empresa: Integer;
  TabEmit, TabEmitCus, TabEmitFlu, TabEmitIts, DtCorrApo: String;
  Qry: TmySQLQuery;
  frx: TfrxReport;
  CalculaCustoLote: Boolean;
begin
(*
  if QrEmitCus.State = dsInactive then
    ReopenEmitCus();
  //
*)
  try
    CodIni := QrImpriFluCodigo.Value;
    //
    FCustoTotalTintas := 0;
    FCustoTotalM2     := 0;
    FCustoTotUSM2     := 0;
    Qtde              := EdPecas.ValueVariant;
    Area              := EdAreaM2.ValueVariant;
    TintasCab         := EdTintasCab.ValueVariant;
    ClienteI          := EdCliInt.ValueVariant;
    Espessura         := EdEspessura.ValueVariant;
    DefPeca           := EdDefPeca.ValueVariant;
    DtCorrApo         := PQ_PF.DefineDtCorrApoTxt(CkDtCorrApo, TPDtCorrApo);
    //
    EmitGru := EdEmitGru.ValueVariant;
    if Dmod.ObrigaInformarEmitGru(FBaixa, EmitGru, EdEmitGru) then Exit;
    if MyObjects.FIC(TintasCab = 0, EdTintasCab, 'Informe a receita!') then
      Exit;
    //
    if not PQ_PF.ValidaProdutosFormulasAcabto(TintasCab) then
    begin
      if Geral.MB_Pergunta('A receita informada possui produtos irregulares ' +
        sLineBreak + 'e por isso n�o poder� ser impressa!' + sLineBreak +
        'Deseja localizar a receita para regularizar o cadastro?') = ID_YES then
      begin
        VAR_CADASTRO := TintasCab;
        Close;
      end;
      Exit;
    end;
    //
    if MyObjects.FIC(ClienteI = 0, EdCliInt, 'Informe o cliente interno!') then
      Exit;
    if MyObjects.FIC(Area = 0, EdAreaM2, 'Informe a �rea!') then
      Exit;
    if MyObjects.FIC(Qtde = 0, EdPecas, 'Informe a quantidade!') then
      Exit;
    if MyObjects.FIC(DefPeca = 0, EdDefPeca, 'Informe o tipo de pe�as!') then
      Exit;
    if MyObjects.FIC(Espessura = 0, EdEspessura, 'Informe a espessura!') then
      Exit;
    //
    if MyObjects.FIC((QrImpriFlu.State = dsInactive) or
      (QrImpriFlu.RecordCount = 0), nil, 'N�o h� itens cadastrados no fluxo!')
    then
      Exit;
    //
    // ini 2024-01-01
    if UnPQx.ImpedePeloBalanco(TPDataP.Date, True) then Exit;
    // fim 2024-01-01
    //
    //
    if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
    //
    CalculaCustoLote := True;
    //if FBaixa and ((QrEmitCus.State = dsInactive) or (QrEmitCus.RecordCount = 0)) then
    if FBaixa and ((QrVMIOriIMEI.State = dsInactive) or (QrVMIOriIMEI.RecordCount = 0)) then
    begin
      if Geral.MB_Pergunta('Nenhum lote de couro foi informado!' + sLineBreak +
        'N�o ser� poss�vel distribuir o custo da baixa por nenhum lote!' +
        sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES
      then
        Exit;
      CalculaCustoLote := False;
    end;
    //
    PnIUser.Enabled := False;
    Screen.Cursor   := crHourGlass;
    //
    if RGTipoPreco.ItemIndex = 2 then
      if not DmModEmit.MostraPrecosAlternativos(dmktfrmSetrEmi_ACABATO, TintasCab) then
        Exit;
    //
    QrImpriFlu.First;
    //
    Liga := '';
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB,
      ['DELETE FROM ' + F_Impri_Its,
      '']);
    //
    TxtC := 'INSERT INTO ' + F_Impri_Its +
      ' (UniqCod,Controle,OrdemFlu,OrdemIts,Tintas,Produto,Nome,' +
      'GramasTi,GramasKg,GramasTo,SiglaMoeda,PrecoMo_Kg,Cotacao_Mo,CustoRS_Kg,' +
      'CustoRS_To, Ativo) VALUES (';
      TintasAtivas := 0;
    while not QrImpriFlu.Eof do
    begin
      TintasTin := QrImpriFluTintasTin.Value;
      //
      if QrImpriFluAtivo.Value = 1 then
      begin
        inc(TintasAtivas, 1);
        ReopenTintasIts(TintasTin);
        //
        PesoTinta := 0;
        CustoTinta := 0;
        if QrTintasIts.RecordCount > 0 then
        begin
          QrTintasIts.First;
          while not QrTintasIts.Eof do
          begin
            Controle := QrTintasItsControle.Value;
            Cotacao := 0;
            case QrTintasItsMoedaPadrao.Value of
              0: Cotacao := 1;
              1: Cotacao := VAR_CAMBIO_USD;
              2: Cotacao := VAR_CAMBIO_EUR;
              3: Cotacao := VAR_CAMBIO_IDX;
              else Geral.MB_Aviso('Moeda padr�o (' + IntToStr(
              QrTintasItsMoedaPadrao.Value) + ') n�o definida no c�lculo de ' +
              'pre�o na impress�o de acabamento!');
            end;
            //PrecoItem    := Cotacao * QrTintasItsCustoPadraoValue;
            case RGTipoPreco.ItemIndex of
              1: // Cadastro manual
                PrecoItem := DmModEmit.PrecoEmReais(
                  QrTintasItsMoedaPadrao.Value,
                  QrTintasItsCustoPadrao.Value, 0);
              2: // desativado
                PrecoItem := 0;
              else   // 0: Estoque Cliente Interno
                PrecoItem := QrTintasItsCustoMedio.Value;
            end;
            case RGTipoPreco.ItemIndex of
              1: // Cadastro manual
                PrecoMo_Kg := QrTintasItsCustoPadrao.Value;
              2: // desativado
                PrecoItem := 0;
              else   // 0: Estoque Cliente Interno
                PrecoMo_Kg := QrTintasItsCustoMedio.Value;
            end;
            //
            PesoItem     := QrTintasItsGramaskg.Value * Area * QrImpriFluGramasM2.Value / 1000;
            CustoItem    := PrecoItem * (PesoItem / 1000);
            //
            PesoTinta    := PesoTinta  + PesoItem;
            CustoTinta   := CustoTinta + CustoItem;
            //
            iOrdemTin    := dmkPF.FFP(QrImpriFluOrdemFlu.Value     ,0);
            iOrdemIts    := dmkPF.FFP(QrTintasItsORDEMITS.Value    ,0);
            iTintas      := dmkPF.FFP(QrTintasItsTintasTin.Value   ,0);
            iProduto     := dmkPF.FFP(QrTintasItsProduto.Value   ,0);
            iNome        := '"' + QrTintasItsNOMEPQ.Value + '"';
            iGramasTi    := dmkPF.FFP(QrTintasItsGramasTi.Value    ,3);
            iGramasKg    := dmkPF.FFP(QrTintasItsGramaskg.Value    ,6);
            iGramasTo    := dmkPF.FFP(PesoItem                     ,3);
            iSiglaMoeda  := '"' + QrTintasItsSIGLAMOEDA.Value + '"';
            iPrecoMo_Kg  := dmkPF.FFP(PrecoMo_Kg                   ,4);
            iCotacao_Mo  := dmkPF.FFP(Cotacao                      ,4);
            iCustoRS_Kg  := dmkPF.FFP(PrecoItem                    ,4);
            iCustoRS_To  := dmkPF.FFP(CustoItem                    ,4);
            //
            Ativo        := dmkPF.FFP(QrTintasItsAtivo.Value       ,0);
            //
            TxtD := Geral.FF0(FUniqCod) + ', ' + Geral.FF0(Controle) +
                    ', ' + iOrdemTin + ', ' + iOrdemIts + ', ' + iTintas +
                    ', ' + iProduto + ', ' + iNome + ', ' + iGramasTi +
                    ', ' + iGramasKg + ', ' + iGramasTo + ', ' + iSiglaMoeda +
                    ', ' + iPrecoMo_Kg + ', ' + iCotacao_Mo + ', ' + iCustoRS_Kg +
                    ', ' + iCustoRS_To + ', ' + Ativo +
                    ');';
            //QrImpriIts.SQL.Add(TxtC + TxtD);
            UnDMkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DmodG.MyPID_DB,
              [TxtC + TxtD,
              '']);
            //
            QrTintasIts.Next;
          end;
          if PesoTinta = 0 then
            CustoKgTinta := 0
          else
            CustoKgTinta := CustoTinta / PesoTinta * 1000;
          if Area = 0 then CustoM2 := 0 else
            CustoM2 := CustoTinta / Area;
          if VAR_CAMBIO_USD = 0 then
            CusUSM2 := 0
          else
            CusUSM2 := CustoM2 / VAR_CAMBIO_USD;

          GramasTo := PesoTinta;
          CustoTo  := CustoTinta;
          CustoKg  := CustoKgTinta;
          AreaM2   := Area;
          Codigo   := QrImpriFluCodigo.Value;

          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, F_Impri_Flu, False, [
            'GramasTo', 'CustoTo', 'CustoKg',
            'AreaM2', 'CustoM2', 'CusUSM2'], [
            //'TintasTin'], [
            'TintasTin', 'Codigo'], [
            GramasTo, CustoTo, CustoKg,
            AreaM2, CustoM2, CusUSM2], [
            //TintasTin], False);
            TintasTin, Codigo], False);
          //
          FCustoTotalTintas := FCustoTotalTintas + CustoTinta;
        end else
          Geral.MB_Aviso('O processo "' + QrImpriFluNomeFlu.Value +
            '" n�o possui itens!');
        //
      end;
      QrImpriFlu.Next;
    end;
    if TintasAtivas = 0 then
    begin
      Geral.MB_Aviso('Selecione pelo menos um processo!');
      Exit;
    end;
    if Area > 0 then
    begin
      FCustoTotalM2 := FCustoTotalTintas / Area;
      if VAR_CAMBIO_USD = 0 then
        FCustoTotUSM2 := 0
      else
        FCustoTotUSM2 := FCustoTotalM2 / VAR_CAMBIO_USD;
    end;
    //
    ReopenImpriFlu(CodIni);
    ReopenImpriIts();
    //

    //
    if FBaixa  then
    begin
      if Dmod.QrControlePqNegBaixa.Value = 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(DmModEmit.QrFalta, DModG.MyPID_DB, [
(*
        'SELECT emi.PQCI, emi.NomePQ, IF(pcl.Peso IS NULL, 0 , pcl.Peso) QtdEstq, ',
        'SUM(emi.Peso_PQ) QtdGastar, ',
        '(IF(pcl.Peso IS NULL, 0 , pcl.Peso) - SUM(emi.Peso_PQ)) QTD_SALDO ',
        '',
        'FROM ' + F_Impri_Its + ' emi ',
        'LEFT JOIN ' + TMeuDB + '.pqcli pcl ON pcl.PQ=emi.PQCI ',
        '          AND pcl.CI=' + Geral.FF0(EdCliInt.ValueVariant),
        'WHERE emi.Produto > 0 ',
        'GROUP BY PQCI ',
        'ORDER BY QTD_SALDO ', // <- Nao mexer aqui!!!
*)

        'SELECT emi.Produto, emi.Nome, IF(pcl.Peso IS NULL, 0 , pcl.Peso) QtdEstq, ',
        'SUM(emi.GramasTo/1000) QtdGastar, ',
        '(IF(pcl.Peso IS NULL, 0 , pcl.Peso) - SUM(emi.GramasTo/1000)) QTD_SALDO ',
        '',
        'FROM ' + F_Impri_Its + ' emi ',
        'LEFT JOIN ' + TMeuDB + '.pqcli pcl ON pcl.PQ=emi.Produto ',
        '          AND pcl.CI=' + Geral.FF0(EdCliInt.ValueVariant),
        'WHERE emi.UniqCod=' + Geral.FF0(FUniqCod),
        'AND emi.Produto > 0 ',
        'GROUP BY Produto ',
        'ORDER BY QTD_SALDO ',
        '',
//
        '']);
        //
        DmModEmit.QrFalta.First;
        if DmModEmit.QrFaltaQTD_SALDO.Value < 0 then
        begin
          frx := DmModEmit.frxQUI_RECEI_105_003;
          frx.Variables['VARF_NO_EMPRESA'] := QuotedStr(CBCliInt.Text);
          // Mostrar form com faltas!
          MyObjects.frxDefineDataSets(frx, [
          DModG.frxDsDono,
          DmModEmit.frxDsFalta
          ]);
          MyObjects.frxMostra(frx, 'Falta de Insumos');
          //  Sai sem baixar!
          Exit;
        end;
      end;
      Codigo := UMyMod.BuscaEmLivreY(
            Dmod.MyDB, 'Livres', 'Controle', 'Emit', 'Emit', 'Codigo');
      //
      TabEmit := 'emit';
      TabEmitCus := 'emitcus';
      TabEmitFlu := 'emitflu';
      TabEmitIts := 'emitits';
      //
      Qry := Dmod.QrUpd;
    end else
    begin
      Codigo := FUniqCod;
      F_Emit :=
        UnCreateBlueDerm.RecriaTempTableNovo(ntrttEmit, DmodG.QrUpdPID1, False);
      F_EmitFlu :=
        UnCreateBlueDerm.RecriaTempTableNovo(ntrttEmitFlu, DmodG.QrUpdPID1, False);
      F_EmitIts :=
        UnCreateBlueDerm.RecriaTempTableNovo(ntrttEmitIts, DmodG.QrUpdPID1, False);
        Qry := DModG.QrUpdPID1;
      //
      TabEmit := F_Emit;
      TabEmitCus := F_EmitCus;
      TabEmitFlu := F_EmitFlu;
      TabEmitIts := F_EmitIts;
    end;
    //
    InserirEmit(Codigo, Qry, TabEmit, TabEmitCus, TabEmitFlu, TabEmitIts);
    //
    if FBaixa then
    begin
      //InserirEmitCus(Codigo);
      InsereEmitCus(Codigo);
      //

      {
      UnDmkDAC_PF.AbreMySQLQuery0(QrSqr, Dmod.MyDB, [
      'SELECT SUM(AreaM2) AreaM2 ',
      'FROM emitcus ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
      if QrSqrAreaM2.Value >= 0.01 then
      begin
        Formula := TintasCab;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrVal, Dmod.MyDB, [
        'SELECT SUM(fi.CustoRS_To) CustoRS_To ',
        'FROM emitits fi ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
        //
        if QrEmitCus.State <> dsInactive then
        begin
          QrEmitCus.DisableControls;
          try
            QrEmitCus.First;
            while not QrEmitCus.Eof do
            begin
              Controle := QrEmitCusControle.Value;
              FatorM2 := QrEmitCusAreaM2.Value / QrSqrAreaM2.Value;
              Custo := FatorM2 * QrValCustoRS_To.Value;
              PercTotCus := FatorM2 * 100;
              //
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'emitcus', False, [
              'Custo', 'Formula', 'PercTotCus'
              ], ['Controle'], [
              Custo, Formula, PercTotCus
              ], [Controle], True);
              //
              QrEmitCus.Next;
            end;
          finally
            QrEmitCus.EnableControls;
          end;
        end else
          Geral.MB_Aviso('Nenhum lote foi definido para rateio de custo!');
      end else
        // 2014-03-01
        //Geral.MB_Aviso('Total de �rea inv�lido para c�lculo de custo!');
        if CalculaCustoLote then
          Geral.MB_Aviso('Total de �rea inv�lido para c�lculo de custo!' +
          sLineBreak +
          'Para c�lculo de custo � necess�rio informar as �reas dos lotes (OS) envolvidos!');
        // FIM 2014-03-01
      //
      }
      UnDmkDAC_PF.AbreMySQLQuery0(QrPreUso, Dmod.MyDB, [
        'SELECT pq.GGXNiv2 Ativo, ems.Codigo, ems.Controle,  ',
        'pqc.MoedaPadrao, ems.ProdutoCI, ',
        'SUM(ems.Peso_PQ) Peso_PQ, ems.Produto, ems.PQCI, ',
        'ems.NomePQ, pqc.Peso, (pqc.Peso+999999999999) EXISTE, ',
        'IF(pqc.CustoPadrao > 0, pqc.CustoPadrao, ',
        'pqc.Valor/pqc.Peso) CustoPadrao, ',
        'IF((pqc.Valor/pqc.Peso>0) AND (pqc.Valor>0) AND (pqc.Peso>0), ',
        'pqc.Valor/pqc.Peso, pqc.CustoPadrao) CustoMedio, ',
        'ems.Cli_Orig, pqc.Peso-SUM(ems.Peso_PQ) PESOFUTURO,  ',
        'IF(cli.Tipo=0, cli.RazaoSocial,cli.Nome) NOMECLI_ORIG ',
        'FROM emitits ems ',
        'LEFT JOIN PQCli     pqc ON pqc.Controle=ems.ProdutoCI ',
        'LEFT JOIN PQ     pq ON pq.Codigo=pqc.PQ ',
        'LEFT JOIN Entidades cli ON cli.Codigo=ems.Cli_Orig ',
        'WHERE ems.Ativo <> ' + Geral.FF0(CO_TipoCadPQ_003_Cod_Texto),
        'AND ems.Codigo=' + Geral.FF0(Codigo),
        'GROUP BY ems.Produto, Cli_Orig ',
        '']);
        //Geral.MB_SQL(Self, QrPreUso);
      //
      //
      DmModEmit.EfetuaBaixa(PB1, TPDataP.Date, RGTipoPreco.ItemIndex, ClienteI,
        Codigo,  QrPreuso, QrVMIOriIMEI, QrPreusoMoedaPadrao, QrPreusoCustoPadrao,
        QrPreusoPeso_PQ, QrPreusoCustoMedio, QrPreusoCli_Orig, QrPreusoProduto,
        QrPreUsoCodigo, QrPreusoControle, dmktfrmSourcMP_VS_FI, DtCorrApo,
        Empresa);
    end;
    DmModEmit.MostraReceitaDeAcabamento(Codigo, RGModeloimp.ItemIndex, not FBaixa);
    //
    Close;
  finally
    PnIUser.Enabled := True;
    Screen.Cursor   := crDefault;
  end;
end;

procedure TFmVSFormulasImp_FI.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSFormulasImp_FI.BtTodosClick(Sender: TObject);
begin
  AtivaProcessos(1);
end;

procedure TFmVSFormulasImp_FI.CkDtCorrApoClick(Sender: TObject);
begin
  TPDtCorrApo.Enabled := CkDtCorrApo.Checked;
end;

procedure TFmVSFormulasImp_FI.DBGImpriFluCellClick(Column: TColumn);
  function ObtemQtde(var Qtde: Double): Boolean;
  var
    ResVar: Variant;
    CasasDecimais: Integer;
  begin
    Qtde          := 0;
    Result        := False;
    CasasDecimais := 3;
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    0, CasasDecimais, 0, '', '', True, 'g/m�', 'Informe a quantidade de g/m�: ',
    0, ResVar) then
    begin
      Qtde := Geral.DMV(ResVar);
      Result := True;
    end;
  end;
var
  Status, Codigo: Integer;
  GramasM2: Double;
begin
  Codigo := QrImpriFluCodigo.Value;
  //
  if Column.FieldName = 'Ativo' then
  begin
    Status := QrImpriFluAtivo.Value;
    if Status = 0 then Status := 1 else Status := 0;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'UPDATE ' + F_Impri_Flu + ' SET Ativo=' + Geral.FF0(Status),
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
  end else
  if Column.FieldName = 'GramasM2' then
  begin
    GramasM2 := QrImpriFluGramasM2.Value;
    if ObtemQtde(GramasM2) then
    begin
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, F_Impri_Flu, False, [
      'GramasM2'], ['Codigo'], [
      GramasM2], [Codigo], False) then
    end;
  end else
    Exit;
  //
  ReopenImpriFlu(Codigo);
end;

procedure TFmVSFormulasImp_FI.DBGImpriFluColEnter(Sender: TObject);
begin
  if DBGImpriFlu.Columns[THackDBGrid(DBGImpriFlu).Col -1].FieldName = 'Ativo' then
    DBGImpriFlu.Options := DBGImpriFlu.Options - [dgEditing] else
    DBGImpriFlu.Options := DBGImpriFlu.Options + [dgEditing];
end;

procedure TFmVSFormulasImp_FI.DBGImpriFluColExit(Sender: TObject);
begin
  DBGImpriFlu.Options := DBGImpriFlu.Options + [dgEditing];
end;

procedure TFmVSFormulasImp_FI.DBGImpriFluDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Ativo' then
    MeuVCLSkin.DrawGrid(DBGImpriFlu, Rect, 1, QrImpriFluAtivo.Value);
end;

procedure TFmVSFormulasImp_FI.DefineBaixa(Baixa: Boolean);
begin
  FBaixa := Baixa;
  //
  if FBaixa then
  begin
    FDatabase := Dmod.MyDB;
    FQrySQL   := Dmod.QrUpd;
    FTabBxaImpEmit := Lowercase('emit');
  end else
  begin
    FDatabase := DModG.MyPID_DB;
    FQrySQL   := DModG.QrUpdPID1;
    FTabBxaImpEmit := Lowercase('_emit_');
  end;
end;

procedure TFmVSFormulasImp_FI.EdTintasCabChange(Sender: TObject);
var
  TintasCab: Integer;
  Txt, Ativo, OrdemTin, OrdemFlu, Codigo, NomeFlu, DescriFlu, NomeTin,
  TintasTin, GramasM2, InfoCargM2, AreaM2, CustoM2, CusUSM2, OpProc: String;
begin
  QrImpriFlu.Close;
  TintasCab := Geral.IMV(EdTintasCab.Text);
  if TintasCab <> 0 then
  begin
    EdCliInt.ValueVariant := QrTintasCabClienteI.Value;
    CBCliInt.KeyValue     := QrTintasCabClienteI.Value;
    //
    EdEspessura.ValueVariant := QrTintasCabEspessura.Value;
    CBEspessura.KeyValue     := QrTintasCabEspessura.Value;
    //
    QrTintasFlu.Close;
    QrTintasFlu.Params[0].AsInteger := TintasCab;
    UnDmkDAC_PF.AbreQuery(QrTintasFlu, Dmod.MyDB);
    //
    if QrTintasFlu.RecordCount > 0 then
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
      'DELETE FROM ' + F_Impri_Flu,
      '']);
      UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
      'DELETE FROM ' + F_Impri_Its,
      '']);
      //
      while not QrTintasFlu.Eof do
      begin
        Ativo     := '0';
        OrdemTin  := dmkPF.FFP(QrTintasFluORDEMTIN.Value, 0);
        OrdemFlu  := dmkPF.FFP(QrTintasFluOrdem.Value, 0);
        Codigo    := dmkPF.FFP(QrTintasFluCodigo.Value, 0);
        NomeFlu   := '"' + QrTintasFluNome.Value + '"';
        DescriFlu := '"' + QrTintasFluDescri.Value + '"';
        NomeTin   := '"' + QrTintasFluNOMEPROCESSO.Value + '"';
        TintasTin := dmkPF.FFP(QrTintasFluTintasTin.Value, 0);
        GramasM2  := dmkPF.FFP(QrTintasFluGramasM2.Value, 3);
        InfoCargM2:= dmkPF.FFP(QrTintasFluInfoCargM2.Value, 3);
        OpProc    := Geral.FF0(QrTintasFluOpProc.Value);
        AreaM2    := '0';
        CustoM2   := '0';
        CusUSM2   := '0';
        //
        Txt := 'INSERT INTO ' + F_Impri_Flu +
               ' (UniqCod,Ativo,OrdemTin,OrdemFlu,Codigo,NomeFlu,DescriFlu,NomeTin,' +
               'TintasTin,InfoCargM2,GramasM2,AreaM2,CustoM2,CusUSM2,OpProc) VALUES(' +
               Geral.FF0(FUniqCod) + ', ' + Ativo + ', ' + OrdemTin + ', ' +
               OrdemFlu + ', ' + Codigo + ',' + NomeFlu + ', ' +
               DescriFlu + ', ' + NomeTin + ', ' + TintasTin + ', ' +
               InfoCargM2 + ', ' + GramasM2 + ', ' + AreaM2 + ', ' +
               CustoM2 + ', ' + CusUSM2 + ', ' + OpProc + ');';

        //
        UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
        Txt,
        '']);
        //
        QrTintasFlu.Next;
      end;
       ReopenImpriFlu(0);
    end;
  end;
end;

procedure TFmVSFormulasImp_FI.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
(*
  if not FBaixa then
  begin
    BtAdiciona.Enabled := False;
    BtExclui.Enabled := False;
  end;
*)
end;

procedure TFmVSFormulasImp_FI.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  //
  FMovimCod := 0;
  F_EmitCus := '';
  FUniqCod := dmkPF.ObtemInt32Random();
  //
  QrImpriFlu.Database := DModG.MyPID_DB;
  QrImpriIts.Database := DModG.MyPID_DB;
  //
  F_Impri_Flu :=
    UnCreateBlueDerm.RecriaTempTableNovo(ntrttTintasImpriFlu, DmodG.QrUpdPID1, False);
  F_Impri_Its :=
    UnCreateBlueDerm.RecriaTempTableNovo(ntrttTintasImpriIts, DmodG.QrUpdPID1, False);
  //
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTintasCab, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEspessuras, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDefPecas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEmitGru, Dmod.MyDB);
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  //
  Agora := DmodG.ObtemAgora();
  TPDataP.Date := Agora;
  EdHoraIni.ValueVariant := Agora - Int(Agora);
  FSeqCtrl := 0;
end;

procedure TFmVSFormulasImp_FI.FormDestroy(Sender: TObject);
begin
  if F_EmitCus <> '' then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DmodG.MyPID_DB, [
    'DELETE FROM ' + F_EmitCus,
    'WHERE Codigo=' + Geral.FF0(FUniqCod),
    '']);
  end;
end;

procedure TFmVSFormulasImp_FI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSFormulasImp_FI.InsereEmitCus(Codigo: Integer);
var
  DataEmis: String;
  //Codigo,
  Controle, MPIn, Formula, MPVIts, VSPedIts, VSMovIts, BxaEstqVS,
  GraGruX: Integer;
  Pecas, Peso, Custo, AreaM2, AreaP2, PercTotCus: Double;
  SQLType: TSQLType;
begin
  SQLType        := stIns; // ImgTipo.SQLType;
  //
  //Codigo         := FEmit;
  Controle       := 0;
  MPIn           := 0; // Deprecado
  Formula        := EdTintasCab.ValueVariant; // Depois? QrEmitNumero.Value;
  BxaEstqVS      := Integer(TBxaEstqVS.bevsNao);  // Deprecado
  DataEmis       := Geral.FDT(TPDataP.Date, 1); //'0000-00-00'; // Depois? QrEmitDataEmis.Value;
  Pecas          := EdPecas.ValueVariant;
  Peso           := 0.000;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
  Custo          := 0; // Calcula Depois ?
  MPVIts         := 0; // Deprecado?
  PercTotCus     := 0; // Depois? Peso do Item / Peso Pesagem * 100 >> % do custo da pesagem;
  VSPedIts       := 0; // ????
  VSMovIts       := FIMEI;

  //
  Controle := UMyMod.BuscaEmLivreY_Def('emitcus', 'Controle', SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'emitcus', False, [
  'Codigo', 'MPIn', 'Formula',
  'DataEmis', 'Pecas', 'Peso',
  'Custo', 'MPVIts', 'AreaM2',
  'AreaP2', 'PercTotCus', 'VSPedIts',
  CO_FLD_TAB_VMI, 'BxaEstqVS', 'GraGruX'], [
  'Controle'], [
  Codigo, MPIn, Formula,
  DataEmis, Pecas, Peso,
  Custo, MPVIts, AreaM2,
  AreaP2, PercTotCus, VSPedIts,
  VSMovIts, BxaEstqVS, GraGruX], [
  Controle], True) then
  ;
end;

function TFmVSFormulasImp_FI.InserirEmit(Codigo: Integer; Qry: TmySQLQuery;
TabEmit, TabEmitCus, TabEmitFlu, TabEmitIts: String): Boolean;
  function InsereEmitFlu(Controle: Integer): Boolean;
  var
    NomeFlu, DescriFlu, NomeTin: String;
    (*Codigo, Controle,*) OrdemTin, OrdemFlu, TintasFlu, TintasTin, OpProc: Integer;
    InfoCargM2, GramasM2, GramasTo, CustoTo, CustoKg, AreaM2, CustoM2, CusUSM2: Double;
  begin
    //Codigo         := ;
    //Controle       := ;
    OrdemTin       := QrImpriFluOrdemTin.Value;
    OrdemFlu       := QrImpriFluOrdemFlu.Value;
    TintasFlu      := QrImpriFluCodigo.Value; // <- Cuidado!!!
    NomeFlu        := QrImpriFluNomeFlu.Value;
    DescriFlu      := QrImpriFluDescriFlu.Value;
    NomeTin        := QrImpriFluNomeTin.Value;
    TintasTin      := QrImpriFluTintasTin.Value;
    GramasM2       := QrImpriFluGramasM2.Value;
    InfoCargM2     := QrImpriFluInfoCargM2.Value;
    GramasTo       := QrImpriFluGramasTo.Value;
    CustoTo        := QrImpriFluCustoTo.Value;
    CustoKg        := QrImpriFluCustoKg.Value;
    AreaM2         := QrImpriFluAreaM2.Value;
    CustoM2        := QrImpriFluCustoM2.Value;
    CusUSM2        := QrImpriFluCusUSM2.Value;
    OpProc         := QrImpriFluOpProc.Value;
    //
    Result := UMyMod.SQLInsUpd(FQrySQL, stIns, TabEmitFlu, False, [
    'Codigo', 'OrdemTin', 'OrdemFlu',
    'TintasFlu', 'NomeFlu', 'NomeTin',
    'TintasTin', 'GramasM2', 'GramasTo',
    'CustoTo', 'CustoKg', 'AreaM2',
    'CustoM2', 'CusUSM2', 'InfoCargM2',
    'OpProc', 'DescriFlu'], [
    'Controle'], [
    Codigo, OrdemTin, OrdemFlu,
    TintasFlu, NomeFlu, NomeTin,
    TintasTin, GramasM2, GramasTo,
    CustoTo, CustoKg, AreaM2,
    CustoM2, CusUSM2, InfoCargM2,
    OpProc, DescriFlu], [
    Controle], True);
  end;
  function InsereEmitIts(Controle: Integer): Boolean;
  var
    NomePQ, Obs, Boca, Processo, ObsProces, SiglaMoeda: String;
    (*Codigo, Controle,*) PQCI, Cli_Orig, Cli_Dest, MoedaPadrao, Numero, Ordem,
    Produto, TempoR, TempoP, Diluicao, Minimo, Maximo, ProdutoCI, GraGruX,
    OrdemFlu, OrdemIts, Tintas, Ativo: Integer;
    CustoPadrao, Porcent, pH, Be, Custo, Graus, Peso_PQ, pH_Min, pH_Max, Be_Min,
    Be_Max, GC_Min, GC_Max, GramasTi, GramasKg, GramasTo, PrecoMo_Kg,
    Cotacao_Mo, CustoRS_Kg, CustoRS_To, CustoMedio: Double;
  begin
    //Codigo         := ;
    //Controle       := ;
    NomePQ         := QrImpriItsNome.Value;
    PQCI           := QrImpriItsPQCI.Value;
    Cli_Orig       := EdCliInt.ValueVariant;
    Cli_Dest       := EdCliInt.ValueVariant;
    CustoPadrao    := QrImpriItsCustoPadrao.Value;
    CustoMedio     := QrImpriItsCustoMedio.Value;
    MoedaPadrao    := QrImpriItsMoedaPadrao.Value;
    Numero         := EdTintasCab.ValueVariant;
    Ordem          := QrImpriItsOrdemIts.Value;
    Porcent        := 0;
    Produto        := QrImpriItsProduto.Value;
    TempoR         := 0;
    TempoP         := 0;
    pH             := 0;
    Be             := 0;
    Obs            := '';
    Diluicao       := 0;
    Custo          := QrImpriItsCustoRS_To.Value;
    Minimo         := 0;
    Maximo         := 0;
    Graus          := 0;
    Boca           := '';
    Processo       := '';
    ObsProces      := '';
    Peso_PQ        := QrImpriItsGramasTo.Value / 1000;
    ProdutoCI      := QrImpriItsPRODUTOCI.Value;
    pH_Min         := 0;
    pH_Max         := 0;
    Be_Min         := 0;
    Be_Max         := 0;
    GC_Min         := 0;
    GC_Max         := 0;
    GraGruX        := 0;
    //
    OrdemFlu       := QrImpriItsOrdemFlu.Value;
    OrdemIts       := QrImpriItsOrdemIts.Value;
    Tintas         := QrImpriItsTintas.Value;
    GramasTi       := QrImpriItsGramasTi.Value;
    GramasKg       := QrImpriItsGramasKg.Value;
    GramasTo       := QrImpriItsGramasTo.Value;
    SiglaMoeda     := QrImpriItsSiglaMoeda.Value;
    PrecoMo_Kg     := QrImpriItsPrecoMo_Kg.Value;
    Cotacao_Mo     := QrImpriItsCotacao_Mo.Value;
    CustoRS_Kg     := QrImpriItsCustoRS_Kg.Value;
    CustoRS_To     := QrImpriItsCustoRS_To.Value;
    //
    Ativo          := QrImpriItsAtivo.Value;
    //
    Result := UMyMod.SQLInsUpd(FQrySQL, stIns, TabEmitIts, False, [
    'Codigo', 'Controle', 'NomePQ',
    'PQCI', 'Cli_Orig', 'Cli_Dest',
    'CustoPadrao', 'MoedaPadrao', 'Numero',
    'Ordem', 'Porcent', 'Produto',
    'TempoR', 'TempoP', 'pH',
    'Be', 'Obs', 'Diluicao',
    'Custo', 'Minimo', 'Maximo',
    'Graus', 'Boca', 'Processo',
    'ObsProces', 'Peso_PQ', 'ProdutoCI',
    'pH_Min', 'pH_Max', 'Be_Min',
    'Be_Max', 'GC_Min', 'GC_Max',
    'GraGruX', 'OrdemFlu', 'OrdemIts',
    'Tintas', 'GramasTi', 'GramasKg',
    'GramasTo', 'SiglaMoeda', 'PrecoMo_Kg',
    'Cotacao_Mo', 'CustoRS_Kg', 'CustoRS_To',
    'CustoMedio', 'Ativo'], [
    ], [
    Codigo, Controle, NomePQ,
    PQCI, Cli_Orig, Cli_Dest,
    CustoPadrao, MoedaPadrao, Numero,
    Ordem, Porcent, Produto,
    TempoR, TempoP, pH,
    Be, Obs, Diluicao,
    Custo, Minimo, Maximo,
    Graus, Boca, Processo,
    ObsProces, Peso_PQ, ProdutoCI,
    pH_Min, pH_Max, Be_Min,
    Be_Max, GC_Min, GC_Max,
    GraGruX, OrdemFlu, OrdemIts,
    Tintas, GramasTi, GramasKg,
    GramasTo, SiglaMoeda, PrecoMo_Kg,
    Cotacao_Mo, CustoRS_Kg, CustoRS_To,
    CustoMedio, Ativo], [
    ], True);
  end;
var
  DataEmis, NOMECI, NOMESETOR, Tecnico, NOME, Espessura, DefPeca, Fulao, Obs,
  HoraIni: String;
  (*Codigo,*) Status, Numero, ClienteI, Tipificacao, TempoR, TempoP, Setor,
  Tipific, SetrEmi, Controle, SourcMP, VSMovCod, EmitGru: Integer;
  Peso, Custo, Qtde, AreaM2, CustoTo, CustoKg, CustoM2, CusUSM2: Double;
  //
  Cod_Espess, CodDefPeca: Integer;
begin
  //Codigo         := ;
  DataEmis       := Geral.FDT(TPDataP.Date, 1);
  Status         := 0;
  Numero         := EdTintasCab.ValueVariant;
  NOMECI         := QrTintasCabNOMECLII.Value;
  NOMESETOR      := QrTintasCabNOMESETOR.Value;
  Tecnico        := QrTintasCabTecnico.Value;
  NOME           := QrTintasCabNome.Value;
  ClienteI       := QrTintasCabClienteI.Value;
  Tipificacao    := QrTintasCabTipificacao.Value;
  TempoR         := 0;
  TempoP         := 0;
  Setor          := QrTintasCabSetor.Value;
  Espessura      := CBEspessura.Text;
  DefPeca        := CBDefPeca.Text;
  Peso           := 0;
  Custo          := 0;
  Qtde           := EdPecas.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  Fulao          := '';
  Obs            := '';
  Tipific        := Tipificacao; // Porque duas vezes
  SourcMP        := Integer(dmktfrmSourcMP_VS_FI);
  SetrEmi        := CO_SetrEmi_ACABATO; // = 2
  Cod_Espess     := EdEspessura.ValueVariant;
  CodDefPeca     := EdDefPeca.ValueVariant;
  HoraIni        := EdHoraIni.Text;
  VSMovCod       := FMovimCod;
  EmitGru        := EdEmitGru.ValueVariant;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumCus, DmodG.MyPID_DB, [
  'SELECT SUM(CustoTo) CustoTo, ',
  'SUM(CustoKg) CustoKg, ',
  'SUM(CustoM2) CustoM2, ',
  'SUM(CusUSM2) CusUSM2 ',
  'FROM ' + F_Impri_Flu,
  'WHERE UniqCod=' + Geral.FF0(FUniqCod),
  '']);

  CustoTo        := QrSumCusCustoTo.Value;
  CustoKg        := QrSumCusCustoKg.Value;
  CustoM2        := QrSumCusCustoM2.Value;
  CusUSM2        := QrSumCusCusUSM2.Value;
  //
  Result := UMyMod.SQLInsUpd(FQrySQL, stIns, TabEmit, False, [
  'Codigo', 'DataEmis', 'Status',
  'Numero', 'NOMECI', 'NOMESETOR',
  'Tecnico', 'NOME', 'ClienteI',
  'Tipificacao', 'TempoR', 'TempoP',
  'Setor', 'Espessura', 'DefPeca',
  'Peso', 'Custo', 'Qtde',
  'AreaM2', 'Fulao', 'Obs',
  'Tipific', 'SetrEmi', 'Cod_Espess',
  'CodDefPeca', 'CustoTo', 'CustoKg',
  'CustoM2', 'CusUSM2', 'SourcMP',
  'HoraIni', 'VSMovCod', 'EmitGru'], [
  ], [
  Codigo, DataEmis, Status,
  Numero, NOMECI, NOMESETOR,
  Tecnico, NOME, ClienteI,
  Tipificacao, TempoR, TempoP,
  Setor, Espessura, DefPeca,
  Peso, Custo, Qtde,
  AreaM2, Fulao, Obs,
  Tipific, SetrEmi, Cod_Espess,
  CodDefPeca, CustoTo, CustoKg,
  CustoM2, CusUSM2, SourcMP,
  HoraIni, VSMovCod, EmitGru], [
  ], True);
  if Result then
  begin
    Result := False;
    //
    //  Fluxos
    QrImpriFlu.First;
    while not QrImpriFlu.Eof do
    begin
      if QrImpriFluAtivo.Value = 1 then
      begin
        if FBaixa and (Codigo <> 0) then
          Controle := UMyMod.BuscaEmLivreY_Def('emitflu', 'Controle', stIns, 0)
        else Controle := 0;
        //
        Result := InsereEmitFlu(Controle);
        //
      end;
      QrImpriFlu.Next;
    end;
    if not Result then
      Exit;

    // Itens das tintas
    QrImpriIts.First;
    while not QrImpriIts.Eof do
    begin
      if FBaixa and (Codigo <> 0) then
        Controle := UMyMod.BuscaIntSafe(
        Dmod.MyDB, 'Livres', 'Controle', 'EmitIts', 'EmitIts', 'Controle')
      else Controle := 0;
      //
      Result := InsereEmitIts(Controle);
      //
      QrImpriIts.Next;
    end;
  end;
end;

function TFmVSFormulasImp_FI.InserirEmitCus(Codigo: Integer): Boolean;
const
  EdPeso = nil;
var
  DataEmis: String;
  //Codigo,
  Controle, MPIn, Formula, MPVIts, VSPedIts, VSMovIts, GraGruX: Integer;
  Peso, Custo, Pecas, AreaM2, AreaP2, PercTotCus: Double;
begin
  Result := False;
  if QrEmitCus.State <> dsInactive then
  begin
    QrEmitCus.DisableControls;
    try
      QrEmitCus.First;
      UndmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'DELETE FROM emitcus ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
      while not QrEmitCus.Eof do
      begin
        //Codigo         := QrEmitCusCodigo.Value;
        Controle       := QrEmitCusControle.Value;
        MPIn           := QrEmitCusMPIn.Value;
        Formula        := QrEmitCusFormula.Value;
        DataEmis       := Geral.FDT(TPDataP.Date, 1);//109);
        Peso           := QrEmitCusPeso.Value;
        Custo          := QrEmitCusCusto.Value;
        Pecas          := QrEmitCusPecas.Value;
        MPVIts         := QrEmitCusMPVIts.Value;
        AreaM2         := QrEmitCusAreaM2.Value;
        AreaP2         := QrEmitCusAreaP2.Value;
        PercTotCus     := QrEmitCusPercTotCus.Value;
        VSPedIts       := 0; // ????
        VSMovIts       := FIMEI;
        //BxaEstqVS      := Integer(TBxaEstqVS.bevsNao);  // Deprecado
        GraGruX        := 0;

        //
        //Result :=
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'emitcus', False, [
        'Codigo', 'MPIn', 'Formula',
        'DataEmis', 'Peso', 'Custo',
        'Pecas', 'MPVIts', 'AreaM2',
        'AreaP2', 'PercTotCus', 'VSPedIts',
        CO_FLD_TAB_VMI, (*'BxaEstqVS',*) 'GraGruX'], [
        'Controle'], [
        Codigo, MPIn, Formula,
        DataEmis, Peso, Custo,
        Pecas, MPVIts, AreaM2,
        AreaP2, PercTotCus, VSPedIts,
        VSMovIts, (*BxaEstqVS, deprecado!*) GraGruX], [
        Controle], False);
        //
        QrEmitCus.Next;
      end;
      DmModEmit.AtualizaQuantidadesSourcMP_WetSome('emitcus', Dmod.MyDB,
        Codigo, EdPeso, EdPecas, EdAreaM2);
      Result := True;
    finally
      QrEmitCus.EnableControls;
    end;
  end;
end;

procedure TFmVSFormulasImp_FI.QrImpriFluAfterScroll(DataSet: TDataSet);
begin
  QrImpriIts.Filter := 'OrdemFlu=' + FormatFloat('0', QrImpriFluOrdemFlu.Value);
end;

procedure TFmVSFormulasImp_FI.QrTintasItsCalcFields(DataSet: TDataSet);
begin
  QrTintasItsSIGLAMOEDA.Value :=
    dmkPF.NomeMoeda(QrTintasItsMoedaPadrao.Value);
end;

procedure TFmVSFormulasImp_FI.QrVMIOriIMEICalcFields(DataSet: TDataSet);
begin
  // Compatibilidade TDmModEmit.EfetuaBaixa(
  QrVMIOriIMEIVSMovIts.Value := QrVMIOriIMEIControle.Value;
  //
  QrVMIOriIMEIPECAS_POSIT.Value := -QrVMIOriIMEIPecas.Value;
  QrVMIOriIMEIAREAM2_POSIT.Value := -QrVMIOriIMEIAreaM2.Value;
  QrVMIOriIMEIPESOKG_POSIT.Value := -QrVMIOriIMEIPesoKg.Value;
end;

procedure TFmVSFormulasImp_FI.ReopenEmitCus();
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrEmitCus, DModG.MyPID_DB, [
  'SELECT CONCAT(mpi.Texto, " ", mpi.CorTxt) TextoECor,',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME_CLI,',
  'ecu.* ',
  'FROM ' + F_EmitCus+ ' ecu',
  'LEFT JOIN ' + TMeuDB + '.mpvits mpi ON mpi.Controle=ecu.MPVIts',
  'LEFT JOIN ' + TMeuDB + '.mpp mpp ON mpp.Codigo=mpi.Pedido ',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=mpp.Cliente ',
  'WHERE ecu.Codigo=' + Geral.FF0(FUniqCod),
  '']);
end;

procedure TFmVSFormulasImp_FI.ReopenImpriFlu(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrImpriFlu, DModG.MyPID_DB, [
  'SELECT * FROM ' + F_Impri_Flu,
  'WHERE UniqCod=' + Geral.FF0(FUniqCod),
  'ORDER BY OrdemFlu',
  '']);
  //
  QrImpriFlu.Locate('Codigo', Codigo, []);
end;

procedure TFmVSFormulasImp_FI.ReopenImpriIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrImpriIts, DModG.MyPID_DB, [
  'SELECT pq.GGXNiv2 Ativo, pq.Nome, pcl.PQ PQCI, ',
  'IF(pcl.CustoPadrao > 0, pcl.CustoPadrao, ',
  'pcl.Valor/pcl.Peso) CustoPadrao, ',
  'IF((pcl.Valor/pcl.Peso>0) AND (pcl.Valor>0) AND (pcl.Peso>0), ',
  'pcl.Valor/pcl.Peso, pcl.CustoPadrao) CustoMedio, ',
  'pcl.MoedaPadrao, pcl.Controle PRODUTOCI, fi.* ',
  'FROM  ' + F_Impri_Its + '  fi ',
  'LEFT JOIN ' + TMeuDB + '.PQ    pq  ON pq.Codigo=fi.Produto ',
  'LEFT JOIN ' + TMeuDB + '.PQCli pcl ON ',
  '   pcl.PQ=fi.Produto AND pcl.CI=' + Geral.FF0(EdCliInt.ValueVariant),
  'WHERE fi.UniqCod=' + Geral.FF0(FUniqCod),
  '']);
  //Geral.MB_SQL(Self, QrImpriIts);
end;

procedure TFmVSFormulasImp_FI.ReopenTintasIts(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTintasIts, Dmod.MyDB, [
  'SELECT pq_.Nome NOMEPQ,',
  'IF(pqc.CustoPadrao > 0, pqc.CustoPadrao,',
  'pqc.Valor/pqc.Peso) CustoPadrao,',
  'IF((pqc.Valor/pqc.Peso>0) AND (pqc.Valor>0) AND (pqc.Peso>0),',
  'pqc.Valor/pqc.Peso, pqc.CustoPadrao) CustoMedio,',
  'pqc.MoedaPadrao, tii.Numero TintasCab,',
  'tii.Codigo TintasTin, tii.Ordem ORDEMITS,',
  'tii.Controle, tii.Produto, tii.GramasTi,',
  'tii.Gramaskg, tii.Obs, tii.Ativo',
  'FROM tintasits tii',
  'LEFT JOIN pq pq_ ON pq_.Codigo=tii.Produto',
  //'LEFT JOIN pqcli pqc ON pqc.pq=pq_.Codigo',
  'LEFT JOIN pqcli pqc ON pqc.PQ=pq_.Codigo ',
  '    AND pqc.CI=' + Geral.FF0(EdCliInt.ValueVariant),
  'WHERE tii.Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TFmVSFormulasImp_FI.ReopenVMIAtu();
const
  Controle   = 0;
  TemIMEIMrt = 0;
  SQL_Limit  = '';
begin
  VS_EFD_ICMS_IPI.ReopenVSOpePrcAtu(QrVMIAtu, FMovimCod, FIMEI, FTemIMEIMrt, FMovimInn);
  VS_EFD_ICMS_IPI.ReopenVSOpePrcOriIMEI(QrVMIOriIMEI, FMovimCod, Controle, TemIMEIMrt, FMovimSrc, SQL_Limit);
  //EdPeso.ValueVariant := QrVMIAtuPesoKg.Value;
  EdPecas.ValueVariant := QrVMIAtuPecas.Value;
  //EdPeca.ValueVariant := ;
  //CBPeca
  TPDataP.Date        := QrVMIAtuDataHora.Value;
end;

(*
  EmitCus


*)
end.

