unit VSSubPrdIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkEditCalc, UnProjGroup_Consts,
  Vcl.ComCtrls, dmkEditDateTimePicker, UnAppEnums, Vcl.Menus;

type
  TFmVSSubPrdIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    EdValorT: TdmkEdit;
    Label7: TLabel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrGraGruXGraGruY: TIntegerField;
    EdCustoMOTot: TdmkEdit;
    Label13: TLabel;
    Label14: TLabel;
    LaMarca: TLabel;
    EdMarca: TdmkEdit;
    EdObserv: TdmkEdit;
    Label16: TLabel;
    EdValorMP: TdmkEdit;
    Label8: TLabel;
    Label12: TLabel;
    EdCustoMOKg: TdmkEdit;
    EdFornecMO: TdmkEditCB;
    CBFornecMO: TdmkDBLookupComboBox;
    QrPrestador: TmySQLQuery;
    QrPrestadorCodigo: TIntegerField;
    QrPrestadorNOMEENTIDADE: TWideStringField;
    DsPrestador: TDataSource;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    Label25: TLabel;
    Label26: TLabel;
    GroupBox4: TGroupBox;
    CkBaixa: TCheckBox;
    PnBaixa: TPanel;
    Label5: TLabel;
    LaBxaPesoKg: TLabel;
    LaBxaAreaM2: TLabel;
    LaBxaAreaP2: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    EdBxaPecas: TdmkEdit;
    EdBxaPesoKg: TdmkEdit;
    EdBxaAreaM2: TdmkEditCalc;
    EdBxaAreaP2: TdmkEditCalc;
    EdBxaValorT: TdmkEdit;
    EdCtrl2: TdmkEdit;
    EdBxaObserv: TdmkEdit;
    TPData: TdmkEditDateTimePicker;
    Label53: TLabel;
    EdHora: TdmkEdit;
    QrClientMO: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsClientMO: TDataSource;
    Panel9: TPanel;
    GroupBox1: TGroupBox;
    Panel5: TPanel;
    Label4: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdCodigo: TdmkEdit;
    EdMovimCod: TdmkEdit;
    EdEmpresa: TdmkEdit;
    GroupBox3: TGroupBox;
    Panel3: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    EdGSPSrcMovID: TdmkEdit;
    EdGSPSrcNiv2: TdmkEdit;
    GroupBox5: TGroupBox;
    Panel8: TPanel;
    Label19: TLabel;
    Label20: TLabel;
    EdGSPJmpMovID: TdmkEdit;
    EdGSPJmpNiv2: TdmkEdit;
    Panel6: TPanel;
    EdClientMO: TdmkEditCB;
    Label62: TLabel;
    CBClientMO: TdmkDBLookupComboBox;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    SbStqCenLoc: TSpeedButton;
    EdReqMovEstq: TdmkEdit;
    Label50: TLabel;
    SbMarca: TSpeedButton;
    QrVSPallet: TMySQLQuery;
    QrVSPalletCodigo: TIntegerField;
    QrVSPalletNome: TWideStringField;
    QrVSPalletLk: TIntegerField;
    QrVSPalletDataCad: TDateField;
    QrVSPalletDataAlt: TDateField;
    QrVSPalletUserCad: TIntegerField;
    QrVSPalletUserAlt: TIntegerField;
    QrVSPalletAlterWeb: TSmallintField;
    QrVSPalletAtivo: TSmallintField;
    QrVSPalletEmpresa: TIntegerField;
    QrVSPalletNO_EMPRESA: TWideStringField;
    QrVSPalletStatus: TIntegerField;
    QrVSPalletCliStat: TIntegerField;
    QrVSPalletGraGruX: TIntegerField;
    QrVSPalletNO_CLISTAT: TWideStringField;
    QrVSPalletNO_PRD_TAM_COR: TWideStringField;
    QrVSPalletNO_STATUS: TWideStringField;
    DsVSPallet: TDataSource;
    Label9: TLabel;
    EdPallet: TdmkEditCB;
    CBPallet: TdmkDBLookupComboBox;
    LaVSRibCla: TLabel;
    SbPallet1: TSpeedButton;
    PMPallet: TPopupMenu;
    Criar1: TMenuItem;
    Gerenciamento1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGraGruXKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdValorMPChange(Sender: TObject);
    procedure EdFichaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPesoKgChange(Sender: TObject);
    procedure EdCustoMOKgChange(Sender: TObject);
    procedure dmkEdit1Redefinido(Sender: TObject);
    procedure EdCustoMOKgRedefinido(Sender: TObject);
    procedure EdPesoKgRedefinido(Sender: TObject);
    procedure SbStqCenLocClick(Sender: TObject);
    procedure SbMarcaClick(Sender: TObject);
    procedure SbPallet1Click(Sender: TObject);
    procedure Criar1Click(Sender: TObject);
    procedure Gerenciamento1Click(Sender: TObject);
  private
    { Private declarations }
    FUltGGX: Integer;
    //
    //procedure ReopenVSPallet();
    procedure ReopenVSSubPrdIts(Controle: Integer);
    procedure SetaUltimoGGX();
    //function  DefineProximoPallet(): String;
    procedure CalculaCustos();

  public
    { Public declarations }
    //FDataHora: TDateTime;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FGSPSrcMovID, FGSPJmpMovID: TEstqMovimID;
    FGSPSrcNiv2, FGSPJmpNiv2, FCodigo, FMovimCod, FEmpresa,
    FTerceiro, FFicha, FSerieFch, FVSMulFrnCab: Integer;
    // Baixa de Sub produto Aparas com cabelo compradas!
    //FOriGGX, FOriSerieFch, FOriFicha: Integer;
    //FOriMarca: String;
  end;

  var
  FmVSSubPrdIts: TFmVSSubPrdIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  Principal, UnVS_PF, ModuleGeral, AppListas, UnGrade_PF;

{$R *.DFM}

procedure TFmVSSubPrdIts.BtOKClick(Sender: TObject);
const
  ExigeFornecedor = False;
  ExigeAreaouPeca = True;
  SrcMovID   = TEstqMovimID(0);
  SrcNivel1  = 0;
  SrcNivel2  = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0;
  SrcGGX     = 0;
  DstGGX     = 0;
  //Pallet     = 0;
  CustoMOM2  = 0;
  //
  TpCalcAuto = -1;
  //
  EdPalletMP = nil;
  EdAreaM2MP = nil;
  EdPalletX  = nil;
  EdAreaM2X  = nil;
  EdFicha    = nil;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
  //
  Fornecedor = 0;
  //
  JmpMovID   = TEstqMovimID(0);
  JmpNivel1  = 0;
  JmpNivel2  = 0;
  JmpGGX     = 0;
  RmsMovID   = TEstqMovimID(0);
  RmsNivel1  = 0;
  RmsNivel2  = 0;
  RmsGGX     = 0;
var
  DataHora, Observ, Marca: String;
  GSPSrcMovID, GSPJmpMovID: TEstqMovimID;
  Ficha, SerieFch, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro,
  GraGruY, FornecMO, GSPSrcNiv2, GSPJmpNiv2, StqCenLoc, ReqMovEstq, ItemNFe,
  VSMulFrnCab, ClientMO, Ctrl2, MovimTwn: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, CustoMOKg, CustoMOTot, ValorMP, ValorT: Double;
  MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  SQLType, GeraNovo: TSQLType;
  Pallet: Integer;
begin
  SQLType := ImgTipo.SQLType;
  //if MyObjects.FIC(FCodigo=0, nil, 'Codigo n�o definido!') then Exit;
  //if MyObjects.FIC(FMovimCod=0, nil, 'MovimCod n�o definido!') then Exit;
  if MyObjects.FIC(FEmpresa=0, nil, 'Empresa n�o definido!') then Exit;
  if MyObjects.FIC(FTerceiro=0, nil, 'Terceiro n�o definido!') then Exit;
  Codigo         := FCodigo;
  MovimCod       := FMovimCod;
  Empresa        := FEmpresa;
  ClientMO       := EdClientMO.ValueVariant;
  Terceiro       := FTerceiro;
  VSMulFrnCab    := FVSMulFrnCab;
  DataHora       := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
  //DataHora       := Geral.FDT(FDataHora, 109);
  Ficha          := FFicha;
  SerieFch       := FSerieFch;
  //
  Controle       := EdControle.ValueVariant;
  Ctrl2          := EdCtrl2.ValueVariant;
  MovimID        := emidGeraSubProd;
  MovimNiv       := eminSemNiv;
  GraGruX        := EdGragruX.ValueVariant;
  FUltGGX        := GraGruX;
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  CustoMOKg      := EdCustoMOKg.ValueVariant;
  CustoMOTot     := EdCustoMOTot.ValueVariant;
  ValorMP        := EdValorMP.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
  Observ         := EdObserv.Text;
  GraGruY        := QrGraGruXGraGruY.Value;
  FornecMO       := EdFornecMO.ValueVariant;
  Marca          := EdMarca.ValueVariant;
  //
  GSPSrcMovID    := FGSPSrcMovID;
  GSPSrcNiv2     := FGSPSrcNiv2;
  GSPJmpMovID    := FGSPJmpMovID;
  GSPJmpNiv2     := FGSPJmpNiv2;
  //
  StqCenLoc      := EdStqCenLoc.ValueVariant;
  ReqMovEstq     := EdReqMovEstq.ValueVariant;
  //
  Pallet         := EdPallet.ValueVariant;
  //
(*
  case GraGruY of
    CO_GraGruY_0512_VSSubPrd,
    CO_GraGruY_1024_VSNatCad,
    CO_GraGruY_1072_VSNatInC,
    CO_GraGruY_1088_VSNatCon,
    CO_GraGruY_2048_VSRibCad:
    begin
      EdPalletX := nil;
      EdAreaM2X := nil;
    end;
    CO_GraGruY_3072_VSRibCla:
    begin
      EdPalletX := EdPallet;
      EdAreaM2X := EdAreaM2;
    end;
    CO_GraGruY_4096_VSRibOpe,
    CO_GraGruY_5120_VSWetEnd,
    CO_GraGruY_6144_VSFinCla:
    begin
      EdPalletX := nil;
      EdAreaM2X := EdAreaM2;
    end;
    else
    begin
      Geral.MB_Erro('"GraGruY" n�o implementado em "FmVSSubPrdIts.BtOKClick()"');
    end;
  end;
  if VS_PF.FichaErro(EdSerieFch, Empresa, Controle, Ficha) then
    Exit;
  if Pallet <> 0 then
  begin
    if MyObjects.FIC(QrGraGruXGraGruY.Value <> CO_GraGruY_3072_VSRibCla, nil,
    'Somente artigo classificado permite informa��o de pallet!') then
      Exit;
    //
    if MyObjects.FIC(GraGruX <> QrVSPalletGraGruX.Value, EdPallet,
    'Artigo do pallet selecionado difere do artigo selecionado!') then
      Exit;
  end;
*)
(*
  if VS_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas, AreaM2,
  PesoKg, ValorT, EdGraGruX, EdPalletX, EdFicha, EdPecas, EdAreaM2X,
  EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca) then
    Exit;
*)
  if MyObjects.FIC(TPData.Date<2, TPData, 'Data/hora n�o definida!') then Exit;
  if MyObjects.FIC(GraGruX = 0, EdGraGruX, 'Informe o sub produto!') then Exit;
  if MyObjects.FIC(PesoKg = 0, EdPesoKg, 'Informe o peso!') then Exit;
  if MyObjects.FIC(ClientMO = 0, EdClientMO, 'Defina quem fica com o subproduto!') then Exit;
  if MyObjects.FIC(FornecMO = 0, EdFornecMO, 'Defina o fornecedor da m�o de obra!') then Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Informe o local/centro de estoque') then Exit;
  if MyObjects.FIC(Pallet = 0, EdPallet, 'Informe o pallet (bag)!') then Exit;
  //
  if (SQLType = stIns) and (Codigo = 0) then
    GeraNovo := stIns
  else
    GeraNovo := stUpd;
  Codigo := UMyMod.BPGS1I32('vssubprdcab', 'Codigo', '', '', tsPos, GeraNovo, Codigo);
  //
  if (SQLType = stIns) and (MovimCod = 0) then
    GeraNovo := stIns
  else
    GeraNovo := stUpd;
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, GeraNovo, MovimCod);
  //
  if CkBaixa.Checked then
  begin
    MovimTwn := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'MovimTwn', '', '', tsPos, ImgTipo.SQLType, MovimTwn);
  end else
    MovimTwn := 0;
  //
  Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos,
    ImgTipo.SQLType, Controle);
  if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa,
  Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2,
  ValorT, DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1,
  LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot,
  ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2,
  QtdGerArP2, AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  JmpMovID, JmpNivel1, JmpNivel2, JmpGGX, RmsMovID, RmsNivel1, RmsNivel2,
  RmsGGX, GSPJmpMovID, GSPJmpNiv2,
(*
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
*)
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei008(*Gera��o de subproduto (UNI) em processo de caleiro*)) then
  begin
    VS_PF.AtualizaTotaisVSXxxCab('vssubprdcab', MovimCod);
    VS_PF.AtualizaSaldoIMEI(Controle, True);
    //FmVSSubPrdCab.LocCod(Codigo, Codigo);
    //FmVSSubPrdCab.ReopenVSSubPrdIts(Controle);

{
  //////////////////////////////////////////////////////////////////////////////
    // Baixa de Sub produto Aparas com cabelo compradas!
    if CkBaixa.Checked then
    begin
      GraGruX        := FOriGGX;
      SerieFch       := FOriSerieFch;
      Ficha          := FOriFicha;
      Marca          := FOriMarca;
      MovimID        := emidEmOperacao;
      MovimNiv       := eminEmOperBxa;
      AreaM2         := -EdBxaAreaM2.ValueVariant;
      AreaP2         := -EdBxaAreaP2.ValueVariant;
      Pecas          := -EdBxaPecas.ValueVariant;
      PesoKg         := -EdBxaPesoKg.ValueVariant;
      ValorT         := -EdBxaValorT.ValueVariant;
      Observ         := EdBxaObserv.Text;
      //
      //PedItsLib      := 0;
      //PedItsVda      := 0;
      //GSPSrcMovID     := 0;
      //GSPSrcNiv2     := 0;
      ReqMovEstq     := 0;
      StqCenLoc      := 0;
      if Ctrl2 = 0 then
        SQLType := stIns
      else
        SQLType := ImgTipo.SQLType;
      //
      Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, SQLType, Ctrl2);
      if VS_PF.InsUpdVSMovIts_(SQLType, Codigo, MovimCod, MovimTwn, Empresa, Fornecedor,
      MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
      DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
      CliVenda, Controle, Ficha, (*Misturou,*) CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, SrcMovID,
      SrcNivel1, SrcNivel2, QtdGerPeca, QtdGerPeso, QtdGerArM2, QtdGerArP2,
      AptoUso, FornecMO, SerieFch, NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto,
      PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
      ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
      QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2) then
      begin
        Ctrl2 := Controle;
        //VS_PF.AtualizaSaldoIMEI(Ctrl2, True);
        //VS_PF.AtualizaSaldoIMEI(FmVSOPeCab.QrVSOpeAtuControle.Value, True);
      end;
    end;
////////////////////////////////////////////////////////////////////////////////
}
    ReopenVSSubPrdIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      //EdFornecedor.ValueVariant  := 0;
      //CBFornecedor.KeyValue      := Null;
      //EdSerieFch.ValueVariant    := 0;
      //CBSerieFch.KeyValue        := Null;
      //EdFicha.ValueVariant       := 0;
      //EdPallet.ValueVariant      := 0;
      //CBPallet.KeyValue          := Null;
      EdValorT.ValueVariant      := 0;
      EdCustoMOTot.ValueVariant  := 0;
      EdPecas.ValueVariant       := 0;
      EdPesoKg.ValueVariant      := 0;
      EdAreaM2.ValueVariant      := 0;
      EdAreaP2.ValueVariant      := 0;
      EdCustoMOKg.ValueVariant   := 0;
      EdFornecMO.ValueVariant    := 0;
      CBFornecMO.KeyValue        := Null;
      EdObserv.Text              := '';
      //
      EdValorT.ValueVariant      := 0;
      EdValorMP.ValueVariant     := 0;
      EdCustoMOTot.ValueVariant  := 0;
      //
      //SBNewPallet.Enabled := True;
      //
      EdGraGruX.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmVSSubPrdIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSSubPrdIts.CalculaCustos();
var
  ValorMP, CustoMOKg, CustoMOTot, ValorT, PesoKg: Double;
begin
  ValorMP      := EdValorMP.ValueVariant;
  CustoMOKg    := EdCustoMOKg.ValueVariant;
  PesoKg       := EdPesoKg.ValueVariant;
  //
  CustoMOTot := CustoMOKg * PesoKg;
  EdCustoMOTot.ValueVariant := CustoMOTot;
  ValorT := ValorMP + CustoMOTot;
  EdValorT.ValueVariant := ValorT;
end;

procedure TFmVSSubPrdIts.CBGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSSubPrdIts.Criar1Click(Sender: TObject);
var
  GraGruX, Pallet, ClientMO: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  ClientMO := EdClientMO.ValueVariant;
  //
  Pallet := VS_PF.CadastraPalletRibCla(FEmpresa, ClientMO, EdPallet, CBPallet,
    QrVSPallet, emidEmProcCal, GraGruX);
  UnDmkDAC_PF.AbreQuery(QrVSPallet, Dmod.MyDB);
  if Pallet <> 0 then
  begin
    EdPallet.ValueVariant := Pallet;
    CBPallet.KeyValue := Pallet;
  end;
end;

procedure TFmVSSubPrdIts.dmkEdit1Redefinido(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSSubPrdIts.EdCustoMOKgChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSSubPrdIts.EdCustoMOKgRedefinido(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSSubPrdIts.EdFichaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
(*
var
  Ficha: Integer;
*)
begin
(*
  if Key = VK_F4 then
    if VS_PF.ObtemProximaFichaRMP(FEmpresa, EdSerieFch, Ficha) then
      EdFicha.ValueVariant := Ficha;
*)
end;

procedure TFmVSSubPrdIts.EdGraGruXKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    SetaUltimoGGX();
end;

procedure TFmVSSubPrdIts.EdPesoKgChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSSubPrdIts.EdPesoKgRedefinido(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSSubPrdIts.EdValorMPChange(Sender: TObject);
begin
  CalculaCustos();
end;

procedure TFmVSSubPrdIts.FormActivate(Sender: TObject);
begin
(*
  DBEdCodigo.DataSource     := FDsCab;
  DBEdMovimCod.DataSource   := FDsCab;
  DBEdEMpresa.DataSource    := FDsCab;
*)
  //
  MyObjects.CorIniComponente();
end;

procedure TFmVSSubPrdIts.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  FUltGGX := 0;
  Agora := DModG.ObtemAgora();
  TPData.Date := Agora;
  EdHora.ValueVariant := Agora;
    // Baixa de Sub produto Aparas com cabelo compradas!
(*
  FOriGGX := 0;
  FOriSerieFch := 0;
  FOriFicha := 0;
  FOriMarca := '';
*)
  //
  VS_PF.AbreGraGruXY(QrGraGruX,
  // ini 2023-12-27
  //'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_0512_VSSubPrd));
  Geral.ATS([
  'AND ggx.GragruY=' + Geral.FF0(CO_GraGruY_1536_VSCouCal),
  'AND nv2.Codigo > 1 '])); // Diferente de couro
  // fim 2023-12-27
  //
  //UnDmkDAC_PF.AbreQuery(QrFornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrestador, Dmod.MyDB);
  //VS_PF.AbreVSSerFch(QrVSSerFch);
  //ReopenVSPallet();
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0,
    TEstqMovimID.emidGeraSubProd);
  UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSPallet, Dmod.MyDB);
end;

procedure TFmVSSubPrdIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSSubPrdIts.Gerenciamento1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPallet(EdPallet.ValueVariant);
end;

procedure TFmVSSubPrdIts.ReopenVSSubPrdIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

(*
procedure TFmVSSubPrdIts.ReopenVSPallet();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
  'SELECT pla.Codigo, pla.GraGruX, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)), ',
  '" ", pla.Nome)  ',
  'NO_PRD_TAM_COR  ',
  'FROM vspalleta pla  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=pla.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE pla.Ativo=1  ',
  'ORDER BY NO_PRD_TAM_COR, pla.Codigo DESC ',
  '']);
end;
*)

procedure TFmVSSubPrdIts.SbMarcaClick(Sender: TObject);
begin
  if DBCheck.LiberaPelaSenhaBoss() then
  begin
    LaMarca.Enabled := True;
    EdMarca.Enabled := True;
    EdMarca.SetFocus;
  end;
end;

procedure TFmVSSubPrdIts.SbPallet1Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPallet, SbPallet1);
end;

procedure TFmVSSubPrdIts.SbStqCenLocClick(Sender: TObject);
begin
  VS_PF.MostraPopUpDeMovID_Ou_CenLoc(SbStqCenLoc, EdStqCenLoc, CBStqCenLoc,
  QrStqCenLoc, TEstqMovimID.emidGeraSubProd);
end;

procedure TFmVSSubPrdIts.SetaUltimoGGX();
begin
  EdGraGruX.ValueVariant := FUltGGX;
  CBGraGruX.KeyValue     := FUltGGX;
end;

end.
