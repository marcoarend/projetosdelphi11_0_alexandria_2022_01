object FmVSCGICab: TFmVSCGICab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-087 :: Classes Geradas de IMEIs VS'
  ClientHeight = 451
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 355
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 209
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 16
        Top = 56
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label52: TLabel
        Left = 416
        Top = 56
        Width = 96
        Height = 13
        Caption = 'Data / hora compra:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 641
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 337
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 3
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPDtHrIni: TdmkEditDateTimePicker
        Left = 416
        Top = 72
        Width = 108
        Height = 21
        Date = 0.639644131944805800
        Time = 0.639644131944805800
        TabOrder = 4
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrIni'
        UpdCampo = 'DtHrIni'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrIni: TdmkEdit
        Left = 524
        Top = 72
        Width = 40
        Height = 21
        TabOrder = 5
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtHrIni'
        UpdCampo = 'DtHrIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtHrFim: TdmkEditDateTimePicker
        Left = 568
        Top = 72
        Width = 108
        Height = 21
        Date = 0.639644131944805800
        Time = 0.639644131944805800
        TabOrder = 6
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrFim'
        UpdCampo = 'DtHrFim'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrFim: TdmkEdit
        Left = 676
        Top = 72
        Width = 40
        Height = 21
        TabOrder = 7
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtHrFim'
        UpdCampo = 'DtHrFim'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 292
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 355
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSCGICab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 689
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsVSCGICab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 291
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Grupo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TdmkDBGridZTO
      Left = 0
      Top = 65
      Width = 784
      Height = 148
      Align = alTop
      DataSource = DsVSCGIIts
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      RowColors = <>
      Columns = <
        item
          Expanded = False
          FieldName = 'NO_TTW'
          Title.Caption = 'Tabela'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VSMovIts'
          Title.Caption = 'IME-I'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_SerieFch'
          Title.Caption = 'S'#233'rie'
          Width = 104
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ficha'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Artigo'
          Width = 228
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FORNECE'
          Title.Caption = 'Fornecedor'
          Width = 246
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 379
        Height = 32
        Caption = 'Classes Geradas de IME-Is VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 379
        Height = 32
        Caption = 'Classes Geradas de IME-Is VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 379
        Height = 32
        Caption = 'Classes Geradas de IME-Is VS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrVSCGICab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSCGICabBeforeOpen
    AfterOpen = QrVSCGICabAfterOpen
    BeforeClose = QrVSCGICabBeforeClose
    AfterScroll = QrVSCGICabAfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMP, vcc.* '
      'FROM vscgicab vcc'
      'LEFT JOIN entidades ent ON ent.Codigo=vcc.Empresa'
      'WHERE vcc.Codigo > 0'
      '')
    Left = 92
    Top = 233
    object QrVSCGICabNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrVSCGICabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSCGICabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrVSCGICabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSCGICabDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
    object QrVSCGICabDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
    end
    object QrVSCGICabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSCGICabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSCGICabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSCGICabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSCGICabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSCGICabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSCGICabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSCGICab: TDataSource
    DataSet = QrVSCGICab
    Left = 92
    Top = 277
  end
  object QrVSCGIIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vci.*, vsf.Nome NO_SerieFch, vmi.Ficha,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))'
      ') NO_PRD_TAM_COR'
      'FROM vscgiits vci'
      'LEFT JOIN vsmovits vmi ON vmi.Controle=vci.VSMovIts'
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch'
      'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro'
      'WHERE vci.Codigo>0'
      'ORDER BY VSMovIts'
      '')
    Left = 188
    Top = 237
    object QrVSCGIItsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSCGIItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSCGIItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSCGIItsCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrVSCGIItsControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrVSCGIItsVSMovIts: TLargeintField
      FieldName = 'VSMovIts'
    end
    object QrVSCGIItsFicha: TLargeintField
      FieldName = 'Ficha'
    end
    object QrVSCGIItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSCGIItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
  end
  object DsVSCGIIts: TDataSource
    DataSet = QrVSCGIIts
    Left = 188
    Top = 281
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      Visible = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
end
