unit VSCfgEstqGeradoEFD;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, mySQLDbTables,
  dmkDBGridZTO, Vcl.Mask, UnDmkProcFunc, dmkDBLookupComboBox, dmkEditCB,
  AppListas, UnProjGroup_Vars, UnProjGroup_Consts, mySQLDirectQuery,
  dmkCheckGroup, UMySQLDB, Vcl.Menus, UnAppEnums;

type
  TFmVSCfgEstqGeradoEFD = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PB1: TProgressBar;
    DGPsq: TdmkDBGridZTO;
    DGGer: TdmkDBGridZTO;
    Splitter1: TSplitter;
    Panel5: TPanel;
    BtCorrige: TBitBtn;
    QrPsq: TmySQLQuery;
    DsPsq: TDataSource;
    QrGer: TmySQLQuery;
    DsGer: TDataSource;
    PMCorrige: TPopupMenu;
    QrPsqSrcGGX: TIntegerField;
    QrPsqDstGGX: TIntegerField;
    QrPsqGGXRcl: TIntegerField;
    QrPsqJmpGGX: TIntegerField;
    QrPsqRmsGGX: TIntegerField;
    QrPsqGGXInn: TIntegerField;
    QrPsqIMEIInn: TIntegerField;
    QrPsqDataHora: TDateTimeField;
    QrPsqMovimID: TIntegerField;
    QrPsqMovimNiv: TIntegerField;
    QrPsqSrcMovID: TIntegerField;
    QrPsqPecasInn: TFloatField;
    QrPsqAreaM2Inn: TFloatField;
    QrPsqPesoKgInn: TFloatField;
    QrPsqDstNivel2: TIntegerField;
    QrPsqPecasGer: TFloatField;
    QrPsqPesoKgGer: TFloatField;
    QrPsqAreaM2Ger: TFloatField;
    QrPsqSdoPc: TFloatField;
    QrPsqSdoM2: TFloatField;
    QrPsqSdoKg: TFloatField;
    QrPsqNO_MovimID: TWideStringField;
    QrPsqNO_MovimNiv: TWideStringField;
    QrPsqReduzido: TIntegerField;
    QrPsqNO_PRD_TAM_COR: TWideStringField;
    QrGerNO_PRD_TAM_COR: TWideStringField;
    QrGerNO_MovimID: TWideStringField;
    QrGerNO_MovimNiv: TWideStringField;
    QrGerIMEIGer: TIntegerField;
    QrGerDataHora: TDateTimeField;
    QrGerDstNivel2: TIntegerField;
    QrGerPecas: TFloatField;
    QrGerPesoKg: TFloatField;
    QrGerAreaM2: TFloatField;
    QrGerMovimID: TIntegerField;
    QrGerMovimNiv: TIntegerField;
    QrGerGraGruX: TIntegerField;
    CorrigeIMEIDestinodagerao1: TMenuItem;
    CorrigeIMEIOrigemdagerao1: TMenuItem;
    RecalculaGeraoesaldo1: TMenuItem;
    BtReopen: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtCorrigeClick(Sender: TObject);
    procedure QrPsqBeforeClose(DataSet: TDataSet);
    procedure QrPsqAfterScroll(DataSet: TDataSet);
    procedure QrGerAfterOpen(DataSet: TDataSet);
    procedure QrGerBeforeClose(DataSet: TDataSet);
    procedure PMCorrigePopup(Sender: TObject);
    procedure CorrigeIMEIOrigemdagerao1Click(Sender: TObject);
    procedure CorrigeIMEIDestinodagerao1Click(Sender: TObject);
    procedure BtReopenClick(Sender: TObject);
    procedure RecalculaGeraoesaldo1Click(Sender: TObject);
  private
    { Private declarations }
    FDiaIni, FDiaFim: TDateTime;
    //
    function  HabilitaCorrecaoPesoGeracaoPsqAtual(): Boolean;
    function  HabilitaCorrecaoPecaEPesoGeracaoPsqAtual(): Boolean;
    procedure RemoveIMEI(IMEIInn: Integer);
    procedure ReopenSdoInn(IMEIInn: Integer);
    procedure ReopenGeradores(IMEIGer: Integer);
    procedure PesquisaErros();
  public
    { Public declarations }
    FImporExpor, FAnoMes, FEmpresa, FPeriApu: Integer;
    //
    procedure PreencheAnoMesPeriodo(AnoMes: Integer);
  end;

  var
  FmVSCfgEstqGeradoEFD: TFmVSCfgEstqGeradoEFD;

implementation

uses UnMyObjects, DmkDAC_PF, Module, ModuleGeral, UMySQLModule, UnVS_CRC_PF,
  UnVS_PF;

{$R *.DFM}

procedure TFmVSCfgEstqGeradoEFD.BtCorrigeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCorrige, BtCorrige);
end;

procedure TFmVSCfgEstqGeradoEFD.BtOKClick(Sender: TObject);
begin
  PesquisaErros();
end;

procedure TFmVSCfgEstqGeradoEFD.BtReopenClick(Sender: TObject);
begin
  ReopenSdoInn(0);
end;

procedure TFmVSCfgEstqGeradoEFD.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCfgEstqGeradoEFD.CorrigeIMEIDestinodagerao1Click(
  Sender: TObject);
const
  AtualizaSaldoModoGenerico = False;
var
  Controle: Integer;
  Gerandos, Gerado: Double;
begin
  Controle := QrPsqIMEIInn.Value;
  VS_CRC_PF.MostraFormVSMovItsAlt(Controle, AtualizaSaldoModoGenerico,
    [eegbQtdOriginal]);
  ReopenSdoInn(Controle);
end;

procedure TFmVSCfgEstqGeradoEFD.CorrigeIMEIOrigemdagerao1Click(Sender: TObject);
const
  AtualizaSaldoModoGenerico = False;
var
  Controle: Integer;
begin
  Controle := QrGerIMEIGer.Value;
  VS_CRC_PF.MostraFormVSMovItsAlt(Controle, AtualizaSaldoModoGenerico,
    [eegbQtdOriginal]);
  ReopenGeradores(Controle);
end;

procedure TFmVSCfgEstqGeradoEFD.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSCfgEstqGeradoEFD.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSCfgEstqGeradoEFD.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmVSCfgEstqGeradoEFD.HabilitaCorrecaoPecaEPesoGeracaoPsqAtual: Boolean;
begin
  Result := (QrPsqSdoPc.Value < 0) and (QrPsqSdoKg.Value < 0) and
  (QrGer.State <> dsInactive) and (QrGer.RecordCount > 0)
  and (QrPsqMovimID.Value = Integer(emidCompra));
end;

function TFmVSCfgEstqGeradoEFD.HabilitaCorrecaoPesoGeracaoPsqAtual(): Boolean;
begin
  Result := (QrGer.State <> dsInactive) and (QrGer.RecordCount > 0)
  and (QrPsqMovimID.Value = Integer(emidCompra)) and (QrPsqSdoPc.Value = 0);
end;

procedure TFmVSCfgEstqGeradoEFD.PesquisaErros();
begin
  Screen.Cursor := crHourGlass;
  try
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '1/5 - Criando tabela tempor�ria de entrada / gera��o');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _Gerados_LCT_INN_;  ',
  'CREATE TABLE _Gerados_LCT_INN_  ',
  'SELECT  ',
  'SrcGGX, DstGGX, GGXRcl, JmpGGX, RmsGGX, ',
  'GraGruX GGXInn,  ',
  'Controle IMEIInn,   ',
  'DataHora, MovimID, MovimNiv,   ',
  'SrcMovID, Pecas PecasInn,   ',
  'AreaM2 AreaM2Inn, PesoKg PesoKgInn   ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + '  ',
  'WHERE  ',
  VS_PF.SQL_MovIDeNiv_Pos_All,
  ' ',
  'ORDER BY DataHora  ',
  '; ',
  '']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '2/5 - Criando tabela tempor�ria de gera��o');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _Gerados_LCT_GER_;  ',
  'CREATE TABLE _Gerados_LCT_GER_  ',
  ' ',
  'SELECT vmi.Controle IMEIGer, vmi.DataHora, vmi.DstNivel2,  ',
  'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, vmi.MovimID, vmi.MovimNiv, ',
  'vmi.GraGruX ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi  ',
  'WHERE DstNivel2 <> 0 ',
  'AND (vmi.Pecas + vmi.PesoKg < 0) ',
  'AND (NOT MovimID IN (26,27)) ',
  'ORDER BY IMEIGer ',
  '; ',
  '']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '3/5 - Criando tabela tempor�ria de soma de gera��o');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _Gerados_SUM_GER_;  ',
  'CREATE TABLE _Gerados_SUM_GER_  ',
  ' ',
  'SELECT DstNivel2,  ',
  'SUM(vmi.Pecas) PecasGer,  ',
  'SUM(vmi.PesoKg) PesoKgGer,  ',
  'SUM(vmi.AreaM2) AreaM2Ger ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi  ',
  'WHERE DstNivel2 <> 0 ',
  'AND (vmi.Pecas + vmi.PesoKg < 0) ',
  'AND (NOT MovimID IN (26,27)) ',
  'GROUP BY DstNivel2 ',
  'ORDER BY MovimID, MovimNiv ',
  ';  ',
  '']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '4/5 - Criando tabela tempor�ria de saldos');
  DModG.MyPID_CompressDB.Execute(Geral.ATS([
  'DROP TABLE IF EXISTS _Gerados_SDO_GER_;  ',
  'CREATE TABLE _Gerados_SDO_GER_  ',
  'SELECT inn.*, ger.*,  ',
  'inn.PecasInn+ger.PecasGer SdoPc,  ',
  'inn.AreaM2Inn+ger.AreaM2Ger SdoM2,  ',
  'inn.PesoKgInn+ger.PesoKgGer SdoKg   ',
  'FROM _Gerados_LCT_INN_ inn   ',
  'LEFT JOIN _Gerados_SUM_GER_ ger ON inn.IMEIInn=ger.DstNivel2  ',
  'WHERE (NOT MovimID IN (26,27)) ',
  ';  ',
  '']));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  '5/5 - Abrindo tabela tempor�ria de saldos');
  ReopenSdoInn(0);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSCfgEstqGeradoEFD.PMCorrigePopup(Sender: TObject);
var
  Habilita1, Habilita2: Boolean;
begin
  Habilita1 := (QrPsq.State <> dsInactive) and (QrPsq.RecordCount > 0);
  CorrigeIMEIDestinodagerao1.Enabled := Habilita1;
  RecalculaGeraoesaldo1.Enabled := Habilita1;
  //
  Habilita2 := (QrGer.State <> dsInactive) and (QrGer.RecordCount > 0);
  CorrigeIMEIOrigemdagerao1.Enabled := Habilita2;
  //////////////////////////////////////////////////////////////////////////////
end;

procedure TFmVSCfgEstqGeradoEFD.PreencheAnoMesPeriodo(AnoMes: Integer);
begin
end;

procedure TFmVSCfgEstqGeradoEFD.QrGerAfterOpen(DataSet: TDataSet);
begin
  BtCorrige.Enabled := QrGer.RecordCount > 0;
end;

procedure TFmVSCfgEstqGeradoEFD.QrGerBeforeClose(DataSet: TDataSet);
begin
  BtCorrige.Enabled := False;
end;

procedure TFmVSCfgEstqGeradoEFD.QrPsqAfterScroll(DataSet: TDataSet);
begin
  ReopenGeradores(0);
end;

procedure TFmVSCfgEstqGeradoEFD.QrPsqBeforeClose(DataSet: TDataSet);
begin
  QrGer.Close;
end;

procedure TFmVSCfgEstqGeradoEFD.RecalculaGeraoesaldo1Click(Sender: TObject);
var
  Controle, MovimID, MovimNiv: Integer;
begin
  Controle := QrPsqIMEIInn.Value;
  MovimID  := QrPsqMovimID.Value;
  MovimNiv := QrPsqMovimNiv.Value;
  //
  VS_CRC_PF.AtualizaSaldoVirtualVSMovIts_Generico(Controle, MovimID, MovimNiv);
  ReopenSdoInn(Controle);
end;

procedure TFmVSCfgEstqGeradoEFD.RemoveIMEI(IMEIInn: Integer);
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DELETE FROM _Gerados_LCT_GER_',
  'WHERE SrcNivel2=' + Geral.FF0(IMEIInn),
  '']);
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DELETE FROM _Gerados_SDO_GER_',
  'WHERE IMEIInn=' + Geral.FF0(IMEIInn),
  '']);
end;

procedure TFmVSCfgEstqGeradoEFD.ReopenGeradores(IMEIGer: Integer);
var
  ATT_MovimID, ATT_MovimNiv: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('ger.MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('ger.MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGer, DModG.MyPID_DB, [
  'SELECT ',
  'CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR,  ',
  ATT_MovimID,
  ATT_MovimNiv,
  'ger.*  ',
  'FROM _Gerados_LCT_GER_ ger ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=ger.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.Controle  ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'WHERE ger.DstNivel2=' + Geral.FF0(QrPsqIMEIInn.Value),
  'ORDER BY ger.IMEIGer DESC ',
  '']);
  //Geral.MB_Info(QrGer.SQL.Text);
  QrGer.Locate('IMEIGer', IMEIGer, []);
end;

procedure TFmVSCfgEstqGeradoEFD.ReopenSdoInn(IMEIInn: Integer);
var
  ATT_MovimID, ATT_MovimNiv: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('sdo.MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('sdo.MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, DModG.MyPID_DB, [
  'SELECT sdo.*, ',
  ATT_MovimID,
  ATT_MovimNiv,
  'ggx.Controle Reduzido, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR ',
  'FROM _Gerados_SDO_GER_ sdo ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=sdo.GGXInn ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.Controle ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE SdoPc <= 0 ',
  'AND ( ',
  '  SdoM2 <> 0 ',
  '  OR ',
  '  SdoKg <> 0 ',
  ') ',
  'ORDER BY DataHora ',
  '']);
  //Geral.MB_SQL(self, QrPsq);
  QrPsq.Locate('IMEIInn', IMEIInn, []);
end;

end.
