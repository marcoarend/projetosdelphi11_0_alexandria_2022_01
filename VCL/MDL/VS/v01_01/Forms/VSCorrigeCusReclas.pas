unit VSCorrigeCusReclas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, dmkDBGridZTO;

type
  TFmVSCorrigeCusReclas = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrVSPrePalCab: TMySQLQuery;
    DsVSPrePalCab: TDataSource;
    Panel2: TPanel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Panel3: TPanel;
    QrVSPrePalCabCodigo: TIntegerField;
    QrSum11: TMySQLQuery;
    QrSum11ValorT: TFloatField;
    QrVSPrePalCabControle: TIntegerField;
    GroupBox1: TGroupBox;
    Panel5: TPanel;
    SbCustoMinimo: TSpeedButton;
    EdCusMinPsq: TdmkEdit;
    EdGGXCusMin: TdmkEdit;
    Label2: TLabel;
    Label1: TLabel;
    DBGrid1: TDBGrid;
    QrCusMin: TMySQLQuery;
    DsCusMin: TDataSource;
    QrPPCab: TMySQLQuery;
    QrPPSrc: TMySQLQuery;
    QrPPSrcCodigo_1: TIntegerField;
    QrPPSrcControle_1: TIntegerField;
    QrPPSrcSrcNivel2_1: TIntegerField;
    QrPPSrcAreaM2_1: TFloatField;
    QrPPSrcValorT_1: TFloatField;
    QrPPSrcCustoM2_1: TFloatField;
    QrPPSrcCodigo_A: TIntegerField;
    QrPPSrcControle_A: TIntegerField;
    QrPPSrcMovimID_A: TIntegerField;
    QrPPSrcAreaM2_A: TFloatField;
    QrPPSrcValorT_A: TFloatField;
    QrPPSrcCustoM2_A: TFloatField;
    QrPPCabCodigo_1: TIntegerField;
    QrPPCabControle_1: TIntegerField;
    QrCusMinControle: TIntegerField;
    QrCusMinGraGruX: TIntegerField;
    QrCusMinPecas: TFloatField;
    QrCusMinAreaM2: TFloatField;
    QrCusMinValorT: TFloatField;
    QrCusMinCustoM2: TFloatField;
    PB1: TProgressBar;
    QrPPDst: TMySQLQuery;
    QrPPDstControle: TIntegerField;
    EdCusDefPsq: TdmkEdit;
    Label3: TLabel;
    QrCCDst: TMySQLQuery;
    QrCCDstVMI_Dest: TIntegerField;
    QrPPCabMovimNiv_1: TIntegerField;
    QrVenda: TMySQLQuery;
    QrVendaControle_V: TIntegerField;
    QrVendaSrcNivel2_V: TIntegerField;
    QrVendaAreaM2_V: TFloatField;
    QrVendaValorT_V: TFloatField;
    QrVendaCustoM2_V: TFloatField;
    QrVendaAreaM2_O: TFloatField;
    QrVendaValorV_O: TFloatField;
    QrVendaCustoM2_O: TFloatField;
    BtCorrigeSohVVPalPreCab: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtCorrigeSohVVPalPreCabClick(Sender: TObject);
    procedure SbCustoMinimoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FPPC_DH: String;
  end;

  var
  FmVSCorrigeCusReclas: TFmVSCorrigeCusReclas;

implementation

uses UnMyObjects, Module, DmkDAC_PF, ModuleGeral;

{$R *.DFM}

procedure TFmVSCorrigeCusReclas.BtCorrigeSohVVPalPreCabClick(Sender: TObject);
var
  sCountA, sCodigo: String;
begin
  Screen.Cursor := crHourGlass;
  try
  //////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando corre��o de pr� reclasse');
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPrePalCab, Dmod.MyDB, [
  'SELECT Codigo, Controle  ',
  'FROM vsmovits ',
  'WHERE MovimID=15 ',
  'AND MovimNiv=12 ',
  'ORDER BY Codigo ',
  '']);
  sCountA := Geral.FF0(QrVSPrePalCab.RecordCount);
  while not QrVSPrePalCab.Eof do
  begin
    sCodigo := Geral.FF0(QrVSPrePalCabCodigo.Value);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Corrigindo pr� reclasse ' +
      '. Item ' + Geral.FF0(QrVSPrePalCab.RecNo) + ' de ' + sCountA);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrSum11, Dmod.MyDB, [
    'SELECT SUM(ValorT) ValorT  ',
    'FROM vsmovits ',
    'WHERE Codigo=' + sCodigo,
    'AND MovimID=15 ',
    'AND MovimNiv=11 ',
    '']);
    //
     UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE vsmovits SET ValorT=' +
     Geral.FFT_Dot(-QrSum11ValorT.Value, 2, siPositivo) +
     ' WHERE Controle=' + Geral.FF0(QrVSPrePalCabControle.Value));
    //
    QrVSPrePalCab.Next;
  end;

(*
SELECT DISTINCT VMI_Dest
FROM vscacitsa
WHERE CacID=8



SELECT *
FROM vscacitsa
WHERE VMI_Sorc=9013
CacID=8 !!!!
*)
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Geral.MB_Info('Corre��o finalizada!');
  finally
  Screen.Cursor := crDefault;
  end;

(*
SELECT AreaM2, ValorT, ValorT/AreaM2 CustoM2
FROM vsmovits
WHERE Controle=(
  SELECT SrcNivel2
  FROM vsmovits
  WHERE Controle=729
)
*)
end;

procedure TFmVSCorrigeCusReclas.BtOKClick(Sender: TObject);
var
  CusMin, VMI_PPC_Dst, Corda: String;
  ValorT_1, SumValT_1, SumAM2T_1, PrcMedM2: Double;
begin
  if MyObjects.FIC(EdCusMinPsq.ValueVariant <=0, EdCusMinPsq,
  'Informe o custo m�nimo!') then Exit;
  if MyObjects.FIC(EdCusDefPsq.ValueVariant <=0, EdCusDefPsq,
  'Informe o custo default!') then Exit;
  Screen.Cursor := crHourGlass;
  try
  //////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando corre��o de pr� reclasse');
  CusMin := Geral.FFT_Dot(EdCusMinPsq.ValueVariant, 2, siPositivo);
  FPPC_DH := '_fix_recl_src_' + FormatDateTime('yymmddhhnn', DModG.ObtemAgora());
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT DISTINCT Codigo ',
  'FROM vsmovits ',
  'WHERE MovimID=15 ',
  //'AND MovimNiv=11', 11 e 12
  //'AND ValorT/AreaM2 < '  + CusMin,
  '']);
  Corda := MyObjects.CordaDeQuery(Dmod.QrAux, 'Codigo');
  if Trim(Corda) = EmptyStr then
  begin
    Geral.MB_Aviso('Nenhuma pr� reclasse foi encontrada com valores abaixo do custo m�nimo!');
    Exit;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrPPCab, DmodG.MyPID_DB, [
  'DROP TABLE IF EXISTS ' + FPPC_DH + ';',
  'CREATE TABLE ' + FPPC_DH + '',
  'SELECT DISTINCT Codigo Codigo_1, ',
  'Controle Controle_1, MovimNiv MovimNiv_1, ',
  'SrcNivel2 SrcNivel2_1, ',
  'AreaM2 AreaM2_1, ValorT ValorT_1, ',
  'ValorT/AreaM2 CustoM2_1',
  'FROM ' + TMeuDB + '.vsmovits ',
  'WHERE MovimID=15 ',
  'AND Codigo IN (' + Corda + ')',
  //'AND MovimNiv=11', 11 e 12!!!!
  //'AND ValorT/AreaM2 < '  + CusMin, todos!
  ';',
  'SELECT DISTINCT Codigo_1, Controle_1, MovimNiv_1 ',
  'FROM ' + FPPC_DH,
  'WHERE MovimNiv_1 = 12' + ';',
  '']);
  //Geral.MB_Teste(QrPPCab.SQL.Text);
  PB1.Position := 0;
  PB1.Max := QrPPCab.RecordCount;
  while not QrPPCab.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Corrigindo item.');
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPPSrc, DmodG.MyPID_DB, [
    'SELECT crs.*, vmi.Codigo Codigo_A, ',
    'vmi.Controle Controle_A, vmi.MovimID MovimID_A,',
    'vmi.AreaM2 AreaM2_A, vmi.ValorT ValorT_A, ',
    'vmi.ValorT/vmi.AreaM2 CustoM2_A',
    'FROM ' + FPPC_DH + ' crs',
    'LEFT JOIN ' + TMeuDB + '.vsmovits vmi ON vmi.Controle=crs.SrcNivel2_1',
    'WHERE crs.Codigo_1=' + Geral.FF0(QrPPCabCodigo_1.Value),
    'AND crs.MovimNiv_1=11 ', // somente origens!
    '']);
    //Geral.MB_Teste(QrPPSrc.SQL.Text);
    QrPPSrc.First;
    SumValT_1 := 0.00;
    SumAM2T_1 := 0.00;
    while not QrPPSrc.Eof do
    begin
      // Corrigindo item de origem
      ValorT_1 := -QrPPSrcAreaM2_1.Value * QrPPSrcCustoM2_A.Value;
      SumValT_1 := SumValT_1 + ValorT_1;
      SumAM2T_1 := SumAM2T_1 + (-QrPPSrcAreaM2_1.Value);
      //
      UnDMkDAC_PF.ExecutaDB(Dmod.MyDB,
      ' UPDATE vsmovits SET ValorT=' + Geral.FFT_Dot(ValorT_1, 2, siPositivo) +
      ' WHERE Controle=' + Geral.FF0(QrPPSrcControle_1.Value));
      //
      QrPPSrc.Next;
    end;
    if SumAM2T_1 > 0 then
      PrcMedM2 := SumValT_1 / SumAM2T_1
    else
      PrcMedM2 := EdCusDefPsq.ValueVariant;
    // Corrigindo IMEI destino no MovimID=15 (pr� reclasse) que � o IMEI de origem da reclasse
    UnDmkDAC_PF.AbreMySQLQuery0(QrPPDst, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM vsmovits ',
    'WHERE MovimID=15',
    'AND MovimNiv=12 ',
    'AND Codigo=' + Geral.FF0(QrPPSrcCodigo_1.Value),
    '']);
    //
    VMI_PPC_Dst := Geral.FF0(QrPPDstControle.Value);
    UnDMkDAC_PF.ExecutaDB(Dmod.MyDB,
    ' UPDATE vsmovits ' +
    ' SET ValorT=IF(AreaM2 < 0, -1, 1) * ' + Geral.FFT_Dot(SumValT_1, 2, siPositivo) +
    ' WHERE Controle=' + VMI_PPC_Dst);
    // Corrigir Valores positivos para area negativa!
    UnDMkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
    'UPDATE vsmovits ',
    'SET ValorT=-ValorT ',
    'WHERE MovimID=15 ',
    'AND MovimNiv=11 ',
    'AND ValorT > 0 ',
    'AND AreaM2<0; ',
    '; ']));
    // Corrigir IMEIS da reclasse MovimID=8 do IMEI destino MovimID=15 acima
    UnDmkDAC_PF.AbreMySQLQuery0(QrCCDst, Dmod.MyDB, [
    'SELECT Controle VMI_Dest',
    'FROM vsmovits',
    'WHERE SrcNivel2=' + VMI_PPC_Dst,
    'AND ValorT <> 0',
    '',
    'UNION',
    '',
    'SELECT DISTINCT VMI_Dest ',
    'FROM vscacitsa ',
    'WHERE VMI_Sorc=' + VMI_PPC_Dst,
    '']);
    //Geral.MB_Teste(QrCCDst.SQL.Text);
    QrCCDst.First;
    while not QrCCDst.Eof do
    begin
      UnDMkDAC_PF.ExecutaDB(Dmod.MyDB,
      ' UPDATE vsmovits ' +
      ' SET ValorT = AreaM2 * ' + Geral.FFT_Dot(PrcMedM2, 10, siPositivo) +
      ' WHERE Controle=' + Geral.FF0(QrCCDstVMI_Dest.Value) +
      ' ');
      //
      QrCCDst.Next;
    end;
    //
    QrPPCab.Next;
  end;
  // Corrigindo sa�das
  UnDmkDAC_PF.AbreMySQLQuery0(QrVenda, Dmod.MyDB, [
  'DROP TABLE IF EXISTs _fix_venda_vlr;',
  'CREATE TABLE _fix_venda_vlr',
  'SELECT Controle Controle_V, SrcNivel2 SrcNivel2_V, ',
  'AreaM2 AreaM2_V, ValorT ValorT_V, ',
  'ValorT / AreaM2 CustoM2_V',
  'FROM ' + TMeuDB + '.vsmovits',
  'WHERE MovimID IN (2,9,12,17)',
  'AND SrcNivel2 <> 0',
  'AND (AreaM2 < 0',
  'OR AreaM2 > 0);',
  ' ',
  'SELECT fvv.*, vmi.AreaM2 AreaM2_O, ',
  'vmi.ValorT ValorV_O, ',
  'vmi.ValorT / vmi.AreaM2 CustoM2_O ',
  'FROM _fix_venda_vlr fvv',
  'LEFT JOIN ' + TMeuDB + '.vsmovits vmi ON vmi.Controle=fvv.SrcNivel2_V;',
  '']);

  PB1.Position := 0;
  PB1.Max := QrVenda.RecordCount;
  QrVenda.First;
  while not QrVenda.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Corrigindo item de venda e baixa for�ada.');
    //
    UnDMkDAC_PF.ExecutaDB(Dmod.MyDB,
    ' UPDATE vsmovits ' +
    ' SET ValorT = AreaM2 * ' + Geral.FFT_Dot(QrVendaCustoM2_O.Value, 10, siPositivo) +
    ' WHERE Controle=' + Geral.FF0(QrVendaControle_V.Value) +
    ' ');
    //
    QrVenda.Next;
  end;

  finally
  Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSCorrigeCusReclas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCorrigeCusReclas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSCorrigeCusReclas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmVSCorrigeCusReclas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCorrigeCusReclas.SbCustoMinimoClick(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCusMin, Dmod.MyDB, [
  'SELECT Controle, GraGruX, Pecas, AreaM2, ValorT, ',
  'ValorT/AreaM2 CustoM2 ',
  'FROM vsmovits ',
  'WHERE MovimID=6 ',
  'AND MovimNiv=13 ',
  'AND GraGruX=' + Geral.FF0(EdGGXCusMin.ValueVariant),
  'ORDER BY DataHora DESC ',
  '']);
  EdCusDefPsq.ValueVariant := QrCusMinCustoM2.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCusMin, Dmod.MyDB, [
  'SELECT Controle, GraGruX, Pecas, AreaM2, ValorT, ',
  'ValorT/AreaM2 CustoM2 ',
  'FROM vsmovits ',
  'WHERE MovimID=6 ',
  'AND MovimNiv=13 ',
  'AND GraGruX=' + Geral.FF0(EdGGXCusMin.ValueVariant),
  'ORDER BY CustoM2 ',
  '']);
  EdCusMinPsq.ValueVariant := QrCusMinCustoM2.Value;
  //
end;

end.
