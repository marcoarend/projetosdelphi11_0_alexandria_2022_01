object FmVSOutKno: TFmVSOutKno
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-020 :: Item de Venda de Wet Blue Com Rastreio'
  ClientHeight = 691
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 509
        Height = 32
        Caption = 'Item de Venda de Wet Blue Com Rastreio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 509
        Height = 32
        Caption = 'Item de Venda de Wet Blue Com Rastreio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 509
        Height = 32
        Caption = 'Item de Venda de Wet Blue Com Rastreio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 577
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 756
        Height = 17
        Caption = 
          'Somente itens rastre'#225'veis ser'#227'o mostrados.     F4 na caixa de pe' +
          #231'as ou '#225'rea copia pe'#231'as+ '#225'rea do IME-I de origem!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 756
        Height = 17
        Caption = 
          'Somente itens rastre'#225'veis ser'#227'o mostrados.     F4 na caixa de pe' +
          #231'as ou '#225'rea copia pe'#231'as+ '#225'rea do IME-I de origem!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 621
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 136
        Top = 5
        Width = 120
        Height = 40
        Caption = '&Reabre'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtReabreClick
      end
      object CkContinuar: TCheckBox
        Left = 291
        Top = -4
        Width = 85
        Height = 49
        Caption = 'Continuar inserindo.'
        Checked = True
        State = cbChecked
        TabOrder = 2
        WordWrap = True
      end
    end
  end
  object DBGrid1: TdmkDBGridZTO
    Left = 0
    Top = 289
    Width = 1008
    Height = 84
    Align = alTop
    DataSource = DsVSMovIts
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    RowColors = <
      item
        BackgroundColor = clBtnFace
        ForegroundColor = clNone
      end>
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'NFeSer'
        Title.Caption = 'Ser'
        Width = 18
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NFeNum'
        Title.Caption = 'N NFe'
        Width = 62
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Controle'
        Title.Caption = 'IME-I'
        Width = 62
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Ficha'
        Title.Caption = 'Ficha RMP'
        Width = 62
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'GraGruX'
        Title.Caption = 'Reduzido'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_PRD_TAM_COR'
        Title.Caption = 'Artigo'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_FORNECE'
        Title.Caption = 'Fornecedor'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Pallet'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SdoVrtPeca'
        Title.Caption = 'Saldo Pe'#231'as'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SdoVrtPeso'
        Title.Caption = 'Sdo peso Kg'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SdoVrtArM2'
        Title.Caption = 'Sdo m'#178
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MediaM2'
        Title.Caption = 'M'#233'dia m'#178
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_Pallet'
        Title.Caption = 'Obs pallet'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MovimCod'
        Title.Caption = 'ID Estoque'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Pecas'
        Title.Caption = 'Pe'#231'as'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PesoKg'
        Title.Caption = 'Peso Kg'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AreaM2'
        Title.Caption = #193'rea m'#178
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AreaP2'
        Title.Caption = #193'rea ft'#178
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ValorT'
        Title.Caption = '$ Todo IME-I'
        Visible = True
      end>
  end
  object Panel11: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 241
    Align = alTop
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 4
    object Panel12: TPanel
      Left = 471
      Top = 0
      Width = 537
      Height = 241
      Align = alRight
      BevelOuter = bvNone
      Caption = 'Selecione o "Tipo de Couro"'
      ParentBackground = False
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 537
        Height = 65
        Align = alTop
        Caption = ' Dados do cabe'#231'alho:'
        Enabled = False
        TabOrder = 0
        object Label5: TLabel
          Left = 12
          Top = 20
          Width = 53
          Height = 13
          Caption = 'ID entrada:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 96
          Top = 20
          Width = 55
          Height = 13
          Caption = 'ID estoque:'
          FocusControl = DBEdMovimCod
        end
        object Label3: TLabel
          Left = 180
          Top = 20
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          FocusControl = DBEdEmpresa
        end
        object Label7: TLabel
          Left = 228
          Top = 20
          Width = 58
          Height = 13
          Caption = 'Data / hora:'
          FocusControl = DBEdDtEntrada
        end
        object Label8: TLabel
          Left = 344
          Top = 20
          Width = 35
          Height = 13
          Caption = 'Cliente:'
          FocusControl = DBEdCliVenda
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 12
          Top = 36
          Width = 80
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdMovimCod: TdmkDBEdit
          Left = 96
          Top = 36
          Width = 80
          Height = 21
          TabStop = False
          DataField = 'MovimCod'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdEmpresa: TdmkDBEdit
          Left = 180
          Top = 36
          Width = 45
          Height = 21
          TabStop = False
          DataField = 'Empresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 2
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdDtEntrada: TdmkDBEdit
          Left = 228
          Top = 36
          Width = 112
          Height = 21
          TabStop = False
          DataField = 'DtVenda'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 3
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdCliVenda: TdmkDBEdit
          Left = 344
          Top = 36
          Width = 61
          Height = 21
          TabStop = False
          DataField = 'Cliente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 4
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taRightJustify
        end
      end
      object GBDadosItem1: TGroupBox
        Left = 0
        Top = 65
        Width = 537
        Height = 176
        Align = alClient
        Caption = ' Dados do item: '
        TabOrder = 1
        Visible = False
        object Label6: TLabel
          Left = 12
          Top = 16
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label1: TLabel
          Left = 96
          Top = 16
          Width = 225
          Height = 13
          Caption = 'Material a baixar do estoque (F4 - '#250'ltima usada):'
        end
        object Label53: TLabel
          Left = 12
          Top = 56
          Width = 90
          Height = 13
          Caption = 'Centro de estoque:'
        end
        object Label49: TLabel
          Left = 12
          Top = 96
          Width = 60
          Height = 13
          Caption = 'Localiza'#231#227'o:'
        end
        object Label38: TLabel
          Left = 12
          Top = 136
          Width = 78
          Height = 13
          Caption = 'Tipo de material:'
        end
        object Label39: TLabel
          Left = 276
          Top = 136
          Width = 82
          Height = 13
          Caption = 'Parte do material:'
        end
        object EdControle: TdmkEdit
          Left = 12
          Top = 32
          Width = 80
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CBGraGruX: TdmkDBLookupComboBox
          Left = 152
          Top = 32
          Width = 380
          Height = 21
          KeyField = 'Controle'
          ListField = 'NO_PRD_TAM_COR'
          ListSource = DsGraGruX
          TabOrder = 2
          OnKeyDown = CBGraGruXKeyDown
          dmkEditCB = EdGraGruX
          QryCampo = 'Cargo'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdGraGruX: TdmkEditCB
          Left = 96
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cargo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnKeyDown = EdGraGruXKeyDown
          DBLookupComboBox = CBGraGruX
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdStqCenCad: TdmkEditCB
          Left = 12
          Top = 72
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdStqCenCadRedefinido
          DBLookupComboBox = CBStqCenCad
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBStqCenCad: TdmkDBLookupComboBox
          Left = 68
          Top = 72
          Width = 465
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsStqCenCad
          TabOrder = 4
          dmkEditCB = EdStqCenCad
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdStqCenLoc: TdmkEditCB
          Left = 12
          Top = 112
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'StqCenLoc'
          UpdCampo = 'StqCenLoc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdStqCenLocRedefinido
          DBLookupComboBox = CBStqCenLoc
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBStqCenLoc: TdmkDBLookupComboBox
          Left = 71
          Top = 112
          Width = 462
          Height = 21
          KeyField = 'Controle'
          ListField = 'NO_LOC_CEN'
          ListSource = DsStqCenLoc
          TabOrder = 6
          dmkEditCB = EdStqCenLoc
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdCouNiv2: TdmkEditCB
          Left = 12
          Top = 152
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrGrGruXCou'
          QryCampo = 'CouNiv2'
          UpdCampo = 'CouNiv2'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCouNiv2
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdCouNiv1: TdmkEditCB
          Left = 276
          Top = 152
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryName = 'QrGrGruXCou'
          QryCampo = 'CouNiv1'
          UpdCampo = 'CouNiv1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCouNiv1
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCouNiv1: TdmkDBLookupComboBox
          Left = 333
          Top = 152
          Width = 200
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCouNiv1
          TabOrder = 9
          dmkEditCB = EdCouNiv1
          QryName = 'QrGrGruXCou'
          QryCampo = 'CouNiv1'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CBCouNiv2: TdmkDBLookupComboBox
          Left = 68
          Top = 152
          Width = 205
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCouNiv2
          TabOrder = 10
          dmkEditCB = EdCouNiv2
          QryName = 'QrGrGruXCou'
          QryCampo = 'CouNiv2'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
    object Panel13: TPanel
      Left = 0
      Top = 0
      Width = 471
      Height = 241
      Align = alClient
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      object RGTipoCouro: TRadioGroup
        Left = 0
        Top = 0
        Width = 471
        Height = 184
        Align = alClient
        Caption = ' Tipo de Couro: '
        Columns = 2
        Items.Strings = (
          'Mat'#233'ria-prima In Natura'
          'Artigo de Ribeira'
          'Artigo de Ribeira Classificado'
          'Artigo em Opera'#231#227'o'
          'Artigo Semi Acabado'
          'Artigo Acabado'
          'Sub Produto')
        TabOrder = 0
        OnClick = RGTipoCouroClick
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 184
        Width = 471
        Height = 57
        Align = alBottom
        Caption = ' Filtros: '
        TabOrder = 1
        object Label10: TLabel
          Left = 12
          Top = 16
          Width = 29
          Height = 13
          Caption = 'Pallet:'
        end
        object Label11: TLabel
          Left = 96
          Top = 16
          Width = 29
          Height = 13
          Caption = 'Ficha:'
        end
        object Label13: TLabel
          Left = 180
          Top = 16
          Width = 28
          Height = 13
          Caption = 'IME-I:'
        end
        object Label16: TLabel
          Left = 264
          Top = 16
          Width = 62
          Height = 13
          Caption = 'IME-I origem:'
        end
        object EdPsqPallet: TdmkEdit
          Left = 12
          Top = 32
          Width = 80
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdPsqPalletChange
        end
        object EdPsqFicha: TdmkEdit
          Left = 96
          Top = 32
          Width = 80
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdPsqFichaChange
        end
        object EdPsqIMEI: TdmkEdit
          Left = 180
          Top = 32
          Width = 80
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdPsqFichaChange
        end
        object EdImeiSrc: TdmkEdit
          Left = 264
          Top = 32
          Width = 80
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnRedefinido = EdImeiSrcRedefinido
        end
        object CkDataMinima: TCheckBox
          Left = 352
          Top = 32
          Width = 121
          Height = 17
          Caption = 'Ignorar data m'#237'nima.'
          TabOrder = 4
        end
      end
    end
  end
  object Panel7: TPanel
    Left = 0
    Top = 537
    Width = 1008
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 5
    object Panel8: TPanel
      Left = 0
      Top = 0
      Width = 121
      Height = 40
      Align = alLeft
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Label4: TLabel
        Left = 4
        Top = 0
        Width = 29
        Height = 13
        Caption = 'Pallet:'
        Visible = False
      end
      object SBPallet: TSpeedButton
        Left = 104
        Top = 16
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SBPalletClick
      end
      object EdPallet: TdmkEditCB
        Left = 4
        Top = 16
        Width = 25
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Pallet'
        UpdCampo = 'Pallet'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPallet
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPallet: TdmkDBLookupComboBox
        Left = 32
        Top = 16
        Width = 69
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVSPallet
        TabOrder = 1
        Visible = False
        OnKeyDown = CBGraGruXKeyDown
        dmkEditCB = EdPallet
        QryCampo = 'Pallet'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object Panel14: TPanel
      Left = 121
      Top = 0
      Width = 252
      Height = 40
      Align = alLeft
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 252
        Height = 40
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 252
          Height = 40
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 245
            Height = 40
            Align = alLeft
            BevelOuter = bvNone
            Enabled = False
            TabOrder = 0
            object Label14: TLabel
              Left = 8
              Top = 0
              Width = 53
              Height = 13
              Caption = 'ID Reclas.:'
              Color = clBtnFace
              ParentColor = False
            end
            object Label9: TLabel
              Left = 68
              Top = 0
              Width = 56
              Height = 13
              Caption = 'ID Estoque:'
              Color = clBtnFace
              ParentColor = False
            end
            object Label17: TLabel
              Left = 128
              Top = 0
              Width = 43
              Height = 13
              Caption = 'ID Pallet:'
              Color = clBtnFace
              ParentColor = False
            end
            object Label12: TLabel
              Left = 188
              Top = 0
              Width = 48
              Height = 13
              Caption = 'Reduzido:'
              Color = clBtnFace
              ParentColor = False
            end
            object EdSrcMovID: TdmkEdit
              Left = 8
              Top = 16
              Width = 56
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Codigo'
              UpdCampo = 'Codigo'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdSrcMovIDChange
            end
            object EdSrcNivel1: TdmkEdit
              Left = 68
              Top = 16
              Width = 56
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'MovimCod'
              UpdCampo = 'MovimCod'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdSrcNivel1Change
            end
            object EdSrcNivel2: TdmkEdit
              Left = 128
              Top = 16
              Width = 56
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Controle'
              UpdCampo = 'Controle'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdSrcNivel2Change
            end
            object EdSrcGGX: TdmkEdit
              Left = 188
              Top = 16
              Width = 56
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Controle'
              UpdCampo = 'Controle'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdSrcNivel2Change
            end
          end
        end
      end
    end
    object Panel9: TPanel
      Left = 373
      Top = 0
      Width = 635
      Height = 40
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object Label20: TLabel
        Left = 423
        Top = 16
        Width = 29
        Height = 13
        Caption = 'Folha:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label21: TLabel
        Left = 515
        Top = 16
        Width = 29
        Height = 13
        Caption = 'Linha:'
        Color = clBtnFace
        ParentColor = False
      end
      object RGIxxMovIX: TRadioGroup
        Left = 0
        Top = 0
        Width = 416
        Height = 40
        Align = alLeft
        Caption = ' Informa'#231#227'o manual de movimenta'#231#227'o:'
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Indefinido'
          'Entrada de couro'
          'Classe / reclasse'
          'Sa'#237'da de couro')
        TabOrder = 0
      end
      object EdIxxFolha: TdmkEdit
        Left = 456
        Top = 12
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ReqMovEstq'
        UpdCampo = 'ReqMovEstq'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdIxxLinha: TdmkEdit
        Left = 548
        Top = 12
        Width = 33
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ReqMovEstq'
        UpdCampo = 'ReqMovEstq'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
  end
  object Panel15: TPanel
    Left = 0
    Top = 449
    Width = 1008
    Height = 88
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 6
    object Label48: TLabel
      Left = 8
      Top = 3
      Width = 114
      Height = 13
      Caption = 'Item de pedido atrelado:'
    end
    object Label26: TLabel
      Left = 720
      Top = 4
      Width = 46
      Height = 13
      Caption = 'Item NFe:'
    end
    object LaPecas: TLabel
      Left = 244
      Top = 4
      Width = 54
      Height = 13
      Caption = 'Pe'#231'as: [F4]'
    end
    object LaAreaM2: TLabel
      Left = 320
      Top = 4
      Width = 60
      Height = 13
      Caption = #193'rea m'#178' [F3]:'
    end
    object LaAreaP2: TLabel
      Left = 396
      Top = 4
      Width = 37
      Height = 13
      Caption = #193'rea ft'#178':'
    end
    object LaPeso: TLabel
      Left = 480
      Top = 4
      Width = 48
      Height = 13
      Caption = 'Peso [F4]:'
    end
    object LaValorT: TLabel
      Left = 553
      Top = 4
      Width = 53
      Height = 13
      Caption = 'Custo total:'
      Enabled = False
    end
    object SbValorT: TSpeedButton
      Left = 628
      Top = 20
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbValorTClick
    end
    object Label18: TLabel
      Left = 652
      Top = 4
      Width = 63
      Height = 13
      Caption = 'M'#233'dia m'#178'/p'#231':'
      Enabled = False
    end
    object Label19: TLabel
      Left = 768
      Top = 4
      Width = 149
      Height = 13
      Caption = 'Material usado para emitir NF-e:'
    end
    object Label22: TLabel
      Left = 8
      Top = 45
      Width = 36
      Height = 13
      Caption = 'Moeda:'
    end
    object Label15: TLabel
      Left = 156
      Top = 44
      Width = 31
      Height = 13
      Caption = 'Pre'#231'o:'
    end
    object Label23: TLabel
      Left = 240
      Top = 44
      Width = 77
      Height = 13
      Caption = '$ Total (moeda):'
      Enabled = False
    end
    object Label24: TLabel
      Left = 324
      Top = 45
      Width = 60
      Height = 13
      Caption = '% Desconto:'
    end
    object Label25: TLabel
      Left = 396
      Top = 45
      Width = 52
      Height = 13
      Caption = '% Tributos:'
    end
    object Label27: TLabel
      Left = 468
      Top = 44
      Width = 78
      Height = 13
      Caption = '$ Bruto (moeda):'
      Enabled = False
    end
    object Label28: TLabel
      Left = 552
      Top = 44
      Width = 72
      Height = 13
      Caption = '$ L'#237'q. (moeda):'
      Enabled = False
    end
    object Label29: TLabel
      Left = 636
      Top = 44
      Width = 43
      Height = 13
      Caption = 'Cota'#231#227'o:'
    end
    object Label30: TLabel
      Left = 752
      Top = 44
      Width = 50
      Height = 13
      Caption = 'Valor total:'
      Enabled = False
    end
    object Label31: TLabel
      Left = 836
      Top = 44
      Width = 54
      Height = 13
      Caption = 'Valor bruto:'
      Enabled = False
    end
    object Label32: TLabel
      Left = 920
      Top = 44
      Width = 62
      Height = 13
      Caption = 'Valor l'#237'quido:'
      Enabled = False
    end
    object EdPedItsVda: TdmkEditCB
      Left = 8
      Top = 19
      Width = 44
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdPedItsVdaRedefinido
      DBLookupComboBox = CBPedItsVda
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPedItsVda: TdmkDBLookupComboBox
      Left = 52
      Top = 19
      Width = 188
      Height = 21
      DropDownWidth = 300
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsVSPedIts
      TabOrder = 1
      dmkEditCB = EdPedItsVda
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdItemNFe: TdmkEdit
      Left = 720
      Top = 20
      Width = 44
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '999'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdItemNFeRedefinido
    end
    object EdPecas: TdmkEdit
      Left = 244
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'Pecas'
      UpdCampo = 'Pecas'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdPecasKeyDown
      OnRedefinido = EdPecasRedefinido
    end
    object EdAreaM2: TdmkEditCalc
      Left = 320
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'AreaM2'
      UpdCampo = 'AreaM2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdPecasKeyDown
      OnRedefinido = EdAreaM2Redefinido
      dmkEditCalcA = EdAreaP2
      CalcType = ctM2toP2
      CalcFrac = cfQuarto
    end
    object EdAreaP2: TdmkEditCalc
      Left = 396
      Top = 20
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'AreaP2'
      UpdCampo = 'AreaP2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      dmkEditCalcA = EdAreaM2
      CalcType = ctP2toM2
      CalcFrac = cfCento
    end
    object EdPesoKg: TdmkEdit
      Left = 480
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'PesoKg'
      UpdCampo = 'PesoKg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdPesoKgKeyDown
      OnRedefinido = EdPesoKgRedefinido
    end
    object EdValorT: TdmkEdit
      Left = 555
      Top = 20
      Width = 72
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ValorT'
      UpdCampo = 'ValorT'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdValorTKeyDown
    end
    object EdMediaM2Pc: TdmkEdit
      Left = 652
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'PesoKg'
      UpdCampo = 'PesoKg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdGGXRcl: TdmkEditCB
      Left = 768
      Top = 20
      Width = 44
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'GGXRcl'
      UpdCampo = 'GGXRcl'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnKeyDown = EdGraGruXKeyDown
      DBLookupComboBox = CBGGXRcl
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBGGXRcl: TdmkDBLookupComboBox
      Left = 812
      Top = 20
      Width = 188
      Height = 21
      DropDownWidth = 300
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGGXRcl
      TabOrder = 10
      OnKeyDown = CBGraGruXKeyDown
      dmkEditCB = EdGGXRcl
      QryCampo = 'GGXRcl'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdPrecoMoeda: TdmkEditCB
      Left = 8
      Top = 60
      Width = 21
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnKeyDown = EdPrecoMoedaKeyDown
      OnRedefinido = EdPrecoMoedaRedefinido
      DBLookupComboBox = CBPrecoMoeda
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPrecoMoeda: TdmkDBLookupComboBox
      Left = 30
      Top = 60
      Width = 123
      Height = 21
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DsCambioMda
      TabOrder = 12
      OnKeyDown = CBPrecoMoedaKeyDown
      dmkEditCB = EdPrecoMoeda
      UpdType = utNil
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdPrecoVal: TdmkEdit
      Left = 156
      Top = 60
      Width = 80
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 13
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Pecas'
      UpdCampo = 'Pecas'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdPrecoValKeyDown
      OnRedefinido = EdPrecoValRedefinido
    end
    object EdMdaTotal: TdmkEdit
      Left = 240
      Top = 60
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 14
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Pecas'
      UpdCampo = 'Pecas'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdPercDesco: TdmkEdit
      Left = 324
      Top = 60
      Width = 69
      Height = 21
      Alignment = taRightJustify
      TabOrder = 15
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '99'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      QryCampo = 'PercDesco'
      UpdCampo = 'PercDesco'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdPercDescoKeyDown
      OnRedefinido = EdPercDescoRedefinido
    end
    object EdPercTrib: TdmkEdit
      Left = 396
      Top = 60
      Width = 69
      Height = 21
      Alignment = taRightJustify
      TabOrder = 16
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '99'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      QryCampo = 'PercTrib'
      UpdCampo = 'PercTrib'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdPercTribKeyDown
      OnRedefinido = EdPercTribRedefinido
    end
    object EdMdaBruto: TdmkEdit
      Left = 468
      Top = 60
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 17
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'MdaBruto'
      UpdCampo = 'MdaBruto'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdMdaLiqui: TdmkEdit
      Left = 551
      Top = 60
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 18
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'MdaLiqui'
      UpdCampo = 'MdaLiqui'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdCambio: TdmkEdit
      Left = 636
      Top = 60
      Width = 113
      Height = 21
      Alignment = taRightJustify
      TabOrder = 19
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 10
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000000000'
      QryCampo = 'Cambio'
      UpdCampo = 'Cambio'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdCambioRedefinido
    end
    object EdNacTotal: TdmkEdit
      Left = 752
      Top = 60
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 20
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'NacTotal'
      UpdCampo = 'NacTotal'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdNacBruto: TdmkEdit
      Left = 836
      Top = 60
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 21
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'NacBruto'
      UpdCampo = 'NacBruto'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdNacLiqui: TdmkEdit
      Left = 920
      Top = 60
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      ReadOnly = True
      TabOrder = 22
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'NacLiqui'
      UpdCampo = 'NacLiqui'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 104
    Top = 308
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGruXGrandeza: TFloatField
      FieldName = 'Grandeza'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 104
    Top = 360
  end
  object QrDefPecas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Grandeza'
      'FROM defpecas')
    Left = 168
    Top = 308
    object QrDefPecasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.defpecas.Codigo'
    end
    object QrDefPecasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.defpecas.Nome'
      Size = 6
    end
    object QrDefPecasGrandeza: TSmallintField
      FieldName = 'Grandeza'
      Origin = 'DBMBWET.defpecas.Grandeza'
    end
  end
  object DsDefPecas: TDataSource
    DataSet = QrDefPecas
    Left = 172
    Top = 356
  end
  object QrVSPallet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM vspallet'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 32
    Top = 308
    object QrVSPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSPallet: TDataSource
    DataSet = QrVSPallet
    Left = 32
    Top = 360
  end
  object QrVSMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pal.Nome NO_Pallet, wmi.*  '
      'FROM vsmovits wmi '
      'LEFT JOIN vspalleta pal ON pal.Codigo=wmi.Pallet '
      'WHERE wmi.Empresa=-11 '
      'AND wmi.GraGruX=3328 '
      'AND ( '
      '  wmi.MovimID=1 '
      '  OR  '
      '  wmi.SrcMovID<>0 '
      ') '
      'AND wmi.SdoVrtPeca > 0 '
      'ORDER BY DataHora, Pallet ')
    Left = 484
    Top = 324
    object QrVSMovItsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrVSMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSMovItsPallet: TIntegerField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovItsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSMovItsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSMovItsMisturou: TIntegerField
      FieldName = 'Misturou'
    end
    object QrVSMovItsSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSMovItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSMovItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrVSMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVSMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVSMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVSMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVSMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovItsPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrVSMovItsPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrVSMovItsPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrVSMovItsMediaM2: TFloatField
      FieldName = 'MediaM2'
      DisplayFormat = '#,##0.00'
    end
    object QrVSMovItsVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSMovItsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrVSMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 512
    end
    object QrVSMovItsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVSMovItsNFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrVSMovItsNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
  end
  object DsVSMovIts: TDataSource
    DataSet = QrVSMovIts
    Left = 488
    Top = 376
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrStqCenCadBeforeClose
    AfterScroll = QrStqCenCadAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM stqcencad '
      'ORDER BY Nome')
    Left = 256
    Top = 312
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 256
    Top = 356
  end
  object QrGGXRcl: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 584
    Top = 328
    object QrGGXRclGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGGXRclControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGGXRclNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGGXRclSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGGXRclCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGGXRclNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGGXRcl: TDataSource
    DataSet = QrGGXRcl
    Left = 584
    Top = 380
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 348
    Top = 316
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 348
    Top = 364
  end
  object QrCouNiv1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv1'
      'ORDER BY Nome')
    Left = 680
    Top = 328
    object QrCouNiv1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCouNiv1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCouNiv1: TDataSource
    DataSet = QrCouNiv1
    Left = 680
    Top = 376
  end
  object QrCouNiv2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM couniv2'
      'ORDER BY Nome')
    Left = 756
    Top = 328
    object QrCouNiv2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCouNiv2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsCouNiv2: TDataSource
    DataSet = QrCouNiv2
    Left = 756
    Top = 376
  end
  object QrCambioMda: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM cambiomda'
      'ORDER BY Nome')
    Left = 332
    Top = 156
    object QrCambioMdaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCambioMdaCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrCambioMdaSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 5
    end
    object QrCambioMdaNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsCambioMda: TDataSource
    DataSet = QrCambioMda
    Left = 332
    Top = 204
  end
  object DsVSPedIts: TDataSource
    DataSet = QrVSPedIts
    Left = 428
    Top = 204
  end
  object QrVSPedIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vpi.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),'
      '" ", vpc.PedidCli, " ", Texto) '
      'NO_PRD_TAM_COR'
      'FROM vspedits vpi'
      'LEFT JOIN vspedcab   vpc ON vpc.Codigo=vpi.Codigo'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vpi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE vpi.VSWetEnd=0')
    Left = 428
    Top = 156
    object QrVSPedItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrVSPedItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPedItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPedItsPrecoTipo: TSmallintField
      FieldName = 'PrecoTipo'
      Required = True
    end
    object QrVSPedItsPrecoMoeda: TIntegerField
      FieldName = 'PrecoMoeda'
      Required = True
    end
    object QrVSPedItsPrecoVal: TFloatField
      FieldName = 'PrecoVal'
      Required = True
    end
    object QrVSPedItsPercTrib: TFloatField
      FieldName = 'PercTrib'
    end
    object QrVSPedItsPercDesco: TFloatField
      FieldName = 'PercDesco'
      Required = True
    end
    object QrVSPedItsSaldoQtd: TFloatField
      FieldName = 'SaldoQtd'
      Required = True
    end
  end
end
