unit VSFchRslCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, (*UnGOTOy,*) UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids, UnGOTOy,
  UnDmkProcFunc, UnDmkENums, AppListas, Variants, dmkDBGridZTO, frxClass,
  frxDBSet, UnProjGroup_Consts, UnAppEnums;

type
  TFmVSFchRslCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrLote: TmySQLQuery;
    DsLote: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrLoteSerieFch: TIntegerField;
    QrLoteFicha: TIntegerField;
    QrLoteFCH_SRE: TFloatField;
    QrLoteITENS: TLargeintField;
    QrLoteRefFirst: TDateTimeField;
    QrLoteRefLast: TDateTimeField;
    QrLoteNO_SERIEFCH: TWideStringField;
    QrIMEIs: TmySQLQuery;
    QrIMEIsControle: TIntegerField;
    QrIMEIsGraGruX: TIntegerField;
    QrIMEIsPecas: TFloatField;
    QrIMEIsPesoKg: TFloatField;
    QrIMEIsAreaM2: TFloatField;
    QrIMEIsAreaP2: TFloatField;
    QrIMEIsValorT: TFloatField;
    QrIMEIsSdoVrtPeca: TFloatField;
    QrIMEIsSdoVrtPeso: TFloatField;
    QrIMEIsSdoVrtArM2: TFloatField;
    QrIMEIsGraGru1: TIntegerField;
    QrIMEIsNO_PRD_TAM_COR: TWideStringField;
    QrIMEIsDataHora: TDateTimeField;
    QrIMEIsNO_MovimID: TWideStringField;
    QrIMEIsMovimID: TIntegerField;
    QrIMEIsSrcNivel1: TIntegerField;
    QrIMEIsSrcNivel2: TIntegerField;
    QrIMEIsDstNivel1: TIntegerField;
    QrIMEIsDstNivel2: TIntegerField;
    QrIMEIsMovimNiv: TIntegerField;
    QrIMEIsNO_MovimNiv: TWideStringField;
    QrIMEIsSrcMovID: TIntegerField;
    QrIMEIsDstMovID: TIntegerField;
    DsIMEIs: TDataSource;
    QrIMEIsPallet: TIntegerField;
    PMIMEIs: TPopupMenu;
    Irparajaneladomovimento1: TMenuItem;
    IrparajaneladedadosdoIMEI1: TMenuItem;
    N3: TMenuItem;
    ExcluiIMEIselecionado1: TMenuItem;
    AlteraIMEIorigemdoIMEISelecionado1: TMenuItem;
    teste1: TMenuItem;
    QrLotes: TmySQLQuery;
    QrLotesSerieFch: TIntegerField;
    QrLotesFicha: TIntegerField;
    QrFichas: TmySQLQuery;
    QrFichasCodigo: TIntegerField;
    QrFichasControle: TIntegerField;
    QrFichasMovimCod: TIntegerField;
    QrFichasMovimNiv: TIntegerField;
    QrFichasMovimTwn: TIntegerField;
    QrFichasEmpresa: TIntegerField;
    QrFichasTerceiro: TIntegerField;
    QrFichasCliVenda: TIntegerField;
    QrFichasMovimID: TIntegerField;
    QrFichasLnkNivXtr1: TIntegerField;
    QrFichasLnkNivXtr2: TIntegerField;
    QrFichasDataHora: TDateTimeField;
    QrFichasPallet: TIntegerField;
    QrFichasGraGruX: TIntegerField;
    QrFichasPecas: TFloatField;
    QrFichasPesoKg: TFloatField;
    QrFichasAreaM2: TFloatField;
    QrFichasAreaP2: TFloatField;
    QrFichasValorT: TFloatField;
    QrFichasSrcMovID: TIntegerField;
    QrFichasSrcNivel1: TIntegerField;
    QrFichasSrcNivel2: TIntegerField;
    QrFichasSrcGGX: TIntegerField;
    QrFichasSdoVrtPeca: TFloatField;
    QrFichasSdoVrtPeso: TFloatField;
    QrFichasSdoVrtArM2: TFloatField;
    QrFichasObserv: TWideStringField;
    QrFichasSerieFch: TIntegerField;
    QrFichasFicha: TIntegerField;
    QrFichasMisturou: TSmallintField;
    QrFichasFornecMO: TIntegerField;
    QrFichasCustoMOKg: TFloatField;
    QrFichasCustoMOTot: TFloatField;
    QrFichasValorMP: TFloatField;
    QrFichasDstMovID: TIntegerField;
    QrFichasDstNivel1: TIntegerField;
    QrFichasDstNivel2: TIntegerField;
    QrFichasDstGGX: TIntegerField;
    QrFichasQtdGerPeca: TFloatField;
    QrFichasQtdGerPeso: TFloatField;
    QrFichasQtdGerArM2: TFloatField;
    QrFichasQtdGerArP2: TFloatField;
    QrFichasQtdAntPeca: TFloatField;
    QrFichasQtdAntPeso: TFloatField;
    QrFichasQtdAntArM2: TFloatField;
    QrFichasQtdAntArP2: TFloatField;
    QrFichasAptoUso: TSmallintField;
    QrFichasNotaMPAG: TFloatField;
    QrFichasMarca: TWideStringField;
    QrFichasLk: TIntegerField;
    QrFichasDataCad: TDateField;
    QrFichasDataAlt: TDateField;
    QrFichasUserCad: TIntegerField;
    QrFichasUserAlt: TIntegerField;
    QrFichasAlterWeb: TSmallintField;
    QrFichasAtivo: TSmallintField;
    QrFichasTpCalcAuto: TIntegerField;
    QrFichasNO_SerieFch: TWideStringField;
    QrFichasNO_PRD_TAM_COR: TWideStringField;
    QrFichasNO_Pallet: TWideStringField;
    QrFichasNO_FORNECE: TWideStringField;
    DsFichas: TDataSource;
    QrVSPaClaIts: TmySQLQuery;
    QrVSPaClaItsCodigo: TIntegerField;
    QrVSPaClaItsControle: TIntegerField;
    QrVSPaClaItsVSPallet: TIntegerField;
    QrVSPaClaItsVMI_Sorc: TIntegerField;
    QrVSPaClaItsVMI_Baix: TIntegerField;
    QrVSPaClaItsVMI_Dest: TIntegerField;
    QrVSPaClaItsTecla: TIntegerField;
    QrVSPaClaItsDtHrIni: TDateTimeField;
    QrVSPaClaItsDtHrFim: TDateTimeField;
    QrVSPaClaItsLk: TIntegerField;
    QrVSPaClaItsDataCad: TDateField;
    QrVSPaClaItsDataAlt: TDateField;
    QrVSPaClaItsUserCad: TIntegerField;
    QrVSPaClaItsUserAlt: TIntegerField;
    QrVSPaClaItsAlterWeb: TSmallintField;
    QrVSPaClaItsAtivo: TSmallintField;
    QrVSPaClaItsDtHrFim_TXT: TWideStringField;
    DsVSPaClaIts: TDataSource;
    QrArtigos: TmySQLQuery;
    DsArtigos: TDataSource;
    QrArtigosGraGruX: TIntegerField;
    QrArtigosPecas: TFloatField;
    QrArtigosAreaM2: TFloatField;
    QrArtigosNO_PRD_TAM_COR: TWideStringField;
    Panel6: TPanel;
    PnItens: TPanel;
    QrMPrima: TmySQLQuery;
    DsMPrima: TDataSource;
    QrMPrimaControle: TIntegerField;
    QrMPrimaGraGruX: TIntegerField;
    QrMPrimaPecas: TFloatField;
    QrMPrimaPesoKg: TFloatField;
    QrMPrimaAreaM2: TFloatField;
    QrMPrimaAreaP2: TFloatField;
    QrMPrimaValorT: TFloatField;
    QrMPrimaSdoVrtPeca: TFloatField;
    QrMPrimaSdoVrtPeso: TFloatField;
    QrMPrimaSdoVrtArM2: TFloatField;
    QrMPrimaGraGru1: TIntegerField;
    QrMPrimaNO_PRD_TAM_COR: TWideStringField;
    QrMPrimaDataHora: TDateTimeField;
    QrMPrimaNO_MovimID: TWideStringField;
    QrMPrimaMovimID: TIntegerField;
    QrMPrimaSrcNivel1: TIntegerField;
    QrMPrimaSrcNivel2: TIntegerField;
    QrMPrimaDstNivel1: TIntegerField;
    QrMPrimaDstNivel2: TIntegerField;
    QrMPrimaMovimNiv: TIntegerField;
    QrMPrimaNO_MovimNiv: TWideStringField;
    QrMPrimaSrcMovID: TIntegerField;
    QrMPrimaDstMovID: TIntegerField;
    QrMPrimaPallet: TIntegerField;
    DBGIMEIs: TDBGrid;
    QrSumMP: TmySQLQuery;
    DsSumMP: TDataSource;
    QrSumMPPecas: TFloatField;
    QrSumMPPesoKg: TFloatField;
    QrSumMPAreaM2: TFloatField;
    QrSumArt: TmySQLQuery;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    DsSumArt: TDataSource;
    Panel8: TPanel;
    EdNotaMPAG: TdmkEdit;
    QrMPrimaFatorNota: TFloatField;
    QrVSFchRslCab: TmySQLQuery;
    DsVSFchRslCab: TDataSource;
    QrVSFchRslCabSerieFch: TIntegerField;
    QrVSFchRslCabFicha: TIntegerField;
    QrVSFchRslCabPecas: TFloatField;
    QrVSFchRslCabAreaM2: TFloatField;
    QrVSFchRslCabPesoKg: TFloatField;
    QrVSFchRslCabFrmPago: TSmallintField;
    QrVSFchRslCabCustoUni: TFloatField;
    QrVSFchRslCabCustoAll: TFloatField;
    QrVSFchRslCabLk: TIntegerField;
    QrVSFchRslCabDataCad: TDateField;
    QrVSFchRslCabDataAlt: TDateField;
    QrVSFchRslCabUserCad: TIntegerField;
    QrVSFchRslCabUserAlt: TIntegerField;
    QrVSFchRslCabAlterWeb: TSmallintField;
    QrVSFchRslCabAtivo: TSmallintField;
    QrVSFchRslCabNO_FrmPago: TWideStringField;
    Panel12: TPanel;
    GBDados: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    GroupBox2: TGroupBox;
    DBEdit13: TDBEdit;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit16: TDBEdit;
    Label20: TLabel;
    DBEdit17: TDBEdit;
    Label21: TLabel;
    DBEdit18: TDBEdit;
    QrVSFchRslIts: TmySQLQuery;
    DsVSFchRslIts: TDataSource;
    GroupBox3: TGroupBox;
    Panel9: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    QrVSFchRslItsSerieFch: TIntegerField;
    QrVSFchRslItsFicha: TIntegerField;
    QrVSFchRslItsGraGruX: TIntegerField;
    QrVSFchRslItsPecas: TFloatField;
    QrVSFchRslItsAreaM2: TFloatField;
    QrVSFchRslItsPesoKg: TFloatField;
    QrVSFchRslItsCustoKg: TFloatField;
    QrVSFchRslItsCustoMOKg: TFloatField;
    QrVSFchRslItsCusFretKg: TFloatField;
    QrVSFchRslItsPreco: TFloatField;
    QrVSFchRslItsImpostP: TFloatField;
    QrVSFchRslItsComissP: TFloatField;
    QrVSFchRslItsFreteM2: TFloatField;
    QrVSFchRslItsSrcMPAG: TFloatField;
    QrVSFchRslItsLk: TIntegerField;
    QrVSFchRslItsDataCad: TDateField;
    QrVSFchRslItsDataAlt: TDateField;
    QrVSFchRslItsUserCad: TIntegerField;
    QrVSFchRslItsUserAlt: TIntegerField;
    QrVSFchRslItsAlterWeb: TSmallintField;
    QrVSFchRslItsAtivo: TSmallintField;
    QrVSFchRslItsNO_PRD_TAM_COR: TWideStringField;
    Panel7: TPanel;
    Label7: TLabel;
    EdSerieFch: TdmkEdit;
    GroupBox1: TGroupBox;
    Panel11: TPanel;
    LaPecas: TLabel;
    LaPeso: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    EdPecas: TdmkEdit;
    EdPesoKg: TdmkEdit;
    EdCustoUni: TdmkEdit;
    EdCustoAll: TdmkEdit;
    GBBaseItens: TGroupBox;
    GroupBox5: TGroupBox;
    Panel10: TPanel;
    Label9: TLabel;
    Label22: TLabel;
    EdCustoMOKg: TdmkEdit;
    EdCusFretKg: TdmkEdit;
    GroupBox6: TGroupBox;
    Panel13: TPanel;
    Label23: TLabel;
    Label24: TLabel;
    EdPreco: TdmkEdit;
    EdFreteM2: TdmkEdit;
    Label25: TLabel;
    EdComissP: TdmkEdit;
    Label26: TLabel;
    EdImpostP: TdmkEdit;
    QrVSFchRslItsCustoMOTot: TFloatField;
    QrVSFchRslItsCusFretTot: TFloatField;
    QrVSFchRslItsImpostTotV: TFloatField;
    QrVSFchRslItsComissTotV: TFloatField;
    QrVSFchRslItsFretM2Tot: TFloatField;
    QrVSFchRslItsBrutoV: TFloatField;
    QrVSFchRslItsMargemV: TFloatField;
    QrVSFchRslItsCustoINTot: TFloatField;
    QrVSFchRslItsCustoTotal: TFloatField;
    QrVSFchRslItsMargemP: TFloatField;
    QrVSFchRslSum: TmySQLQuery;
    DsVSFchRslSum: TDataSource;
    Panel14: TPanel;
    QrVSFchRslSumPecas: TFloatField;
    QrVSFchRslSumAreaM2: TFloatField;
    QrVSFchRslSumPesoKg: TFloatField;
    QrVSFchRslSumCustoMOTot: TFloatField;
    QrVSFchRslSumCusFretTot: TFloatField;
    QrVSFchRslSumCustoINTot: TFloatField;
    QrVSFchRslSumCustoTotal: TFloatField;
    QrVSFchRslSumImpostTotV: TFloatField;
    QrVSFchRslSumComissTotV: TFloatField;
    QrVSFchRslSumFretM2Tot: TFloatField;
    QrVSFchRslSumBrutoV: TFloatField;
    QrVSFchRslSumMargemV: TFloatField;
    QrVSFchRslSumMargemP: TFloatField;
    Label27: TLabel;
    DBEdit19: TDBEdit;
    Label28: TLabel;
    DBEdit20: TDBEdit;
    Label29: TLabel;
    DBEdit21: TDBEdit;
    Label30: TLabel;
    DBEdit22: TDBEdit;
    Label31: TLabel;
    DBEdit23: TDBEdit;
    Label32: TLabel;
    DBEdit24: TDBEdit;
    Label33: TLabel;
    DBEdit25: TDBEdit;
    Label34: TLabel;
    DBEdit26: TDBEdit;
    Label35: TLabel;
    DBEdit27: TDBEdit;
    Label36: TLabel;
    DBEdit28: TDBEdit;
    Label37: TLabel;
    DBEdit29: TDBEdit;
    Label38: TLabel;
    DBEdit30: TDBEdit;
    Label39: TLabel;
    DBEdit31: TDBEdit;
    QrMul: TmySQLQuery;
    QrVSInnSum: TmySQLQuery;
    QrVSInnSumPecas: TFloatField;
    QrVSInnSumPesoKg: TFloatField;
    QrVSInnSumAreaM2: TFloatField;
    QrVSInnSumAreaP2: TFloatField;
    QrVSInnSumValorT: TFloatField;
    Label40: TLabel;
    EdTribtAll: TdmkEdit;
    Label41: TLabel;
    EdCustoLiq: TdmkEdit;
    QrTribIncIts: TmySQLQuery;
    QrTribIncItsValrTrib: TFloatField;
    RGTpCalcVal: TdmkRadioGroup;
    Panel15: TPanel;
    DBGIDs: TDBGrid;
    Panel16: TPanel;
    GroupBox7: TGroupBox;
    DGVSPaClaIts: TdmkDBGridZTO;
    GroupBox8: TGroupBox;
    QrVSPaMulCab: TmySQLQuery;
    QrVSPaMulCabCodigo: TIntegerField;
    QrVSPaMulCabMovimCod: TIntegerField;
    QrVSPaMulCabEmpresa: TIntegerField;
    QrVSPaMulCabDataHora: TDateTimeField;
    QrVSPaMulCabPecas: TFloatField;
    QrVSPaMulCabPesoKg: TFloatField;
    QrVSPaMulCabAreaM2: TFloatField;
    QrVSPaMulCabAreaP2: TFloatField;
    QrVSPaMulCabLk: TIntegerField;
    QrVSPaMulCabDataCad: TDateField;
    QrVSPaMulCabDataAlt: TDateField;
    QrVSPaMulCabUserCad: TIntegerField;
    QrVSPaMulCabUserAlt: TIntegerField;
    QrVSPaMulCabAlterWeb: TSmallintField;
    QrVSPaMulCabAtivo: TSmallintField;
    QrVSPaMulCabVMI_Sorc: TIntegerField;
    QrVSPaMulCabCacCod: TIntegerField;
    QrVSPaMulCabVSGerArt: TIntegerField;
    DsVSPaMulCab: TDataSource;
    QrVSPaMulCabDataHora_TXT: TWideStringField;
    dmkDBGridZTO1: TdmkDBGridZTO;
    frxWET_CURTI_058_001: TfrxReport;
    frxDsVSFchRslCab: TfrxDBDataset;
    frxDsMPrima: TfrxDBDataset;
    QrMPrimaNotaMPAG: TFloatField;
    QrMPrimaNO_FORNECE: TWideStringField;
    QrMPrimaObserv: TWideStringField;
    frxDsVSFchRslIts: TfrxDBDataset;
    frxDsLote: TfrxDBDataset;
    QrVSMovDif: TmySQLQuery;
    DsVSMovDif: TDataSource;
    QrVSMovDifControle: TIntegerField;
    QrVSMovDifInfPecas: TFloatField;
    QrVSMovDifInfPesoKg: TFloatField;
    QrVSMovDifInfAreaM2: TFloatField;
    QrVSMovDifInfAreaP2: TFloatField;
    QrVSMovDifInfValorT: TFloatField;
    QrVSMovDifDifPecas: TFloatField;
    QrVSMovDifDifPesoKg: TFloatField;
    QrVSMovDifDifAreaM2: TFloatField;
    QrVSMovDifDifAreaP2: TFloatField;
    QrVSMovDifDifValorT: TFloatField;
    QrVSMovDifPerQbrViag: TFloatField;
    QrVSMovDifPerQbrSal: TFloatField;
    QrVSMovDifPesoSalKg: TFloatField;
    QrVSMovDifRstCouPc: TFloatField;
    QrVSMovDifRstCouKg: TFloatField;
    QrVSMovDifRstCouVl: TFloatField;
    QrVSMovDifRstSalKg: TFloatField;
    QrVSMovDifRstSalVl: TFloatField;
    QrVSMovDifRstTotVl: TFloatField;
    QrVSMovDifTribDefSel: TIntegerField;
    frxDsVSMovDif: TfrxDBDataset;
    EdNO_SerieFch: TdmkEdit;
    EdFicha: TdmkEdit;
    Label43: TLabel;
    QrVSFchRslCabNO_SerieFch: TWideStringField;
    BtAtualiza: TBitBtn;
    QrVSFchRslCabTribtAll: TFloatField;
    QrVSFchRslCabCustoLiq: TFloatField;
    frxDsFichas: TfrxDBDataset;
    frxDsVSPaClaIts: TfrxDBDataset;
    frxDsVSPaMulCab: TfrxDBDataset;
    QrVSFchRslItsPercM2: TFloatField;
    QrVSFchRslItsPercPc: TFloatField;
    QrVSFchRslItsImpostCred: TFloatField;
    QrMPrimaMediaKg: TFloatField;
    QrFichasMediaM2: TFloatField;
    EdTribtMP: TdmkEdit;
    Label42: TLabel;
    EdTribtMO: TdmkEdit;
    Label44: TLabel;
    QrVSFchRslCabTribtMO: TFloatField;
    QrVSFchRslCabTribtMP: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrLoteAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrLoteAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrLoteBeforeClose(DataSet: TDataSet);
    procedure QrVSFchGerIDsAfterScroll(DataSet: TDataSet);
    procedure QrVSFchGerIDsBeforeClose(DataSet: TDataSet);
    procedure QrVSFchGerNivsAfterScroll(DataSet: TDataSet);
    procedure QrVSFchGerNivsBeforeClose(DataSet: TDataSet);
    procedure Irparajaneladomovimento1Click(Sender: TObject);
    procedure IrparajaneladedadosdoIMEI1Click(Sender: TObject);
    procedure ExcluiIMEIselecionado1Click(Sender: TObject);
    procedure AlteraIMEIorigemdoIMEISelecionado1Click(Sender: TObject);
    procedure teste1Click(Sender: TObject);
    procedure QrLotesAfterScroll(DataSet: TDataSet);
    procedure QrFichasAfterScroll(DataSet: TDataSet);
    procedure QrFichasBeforeClose(DataSet: TDataSet);
    procedure QrArtigosAfterScroll(DataSet: TDataSet);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdPecasRedefinido(Sender: TObject);
    procedure EdPesoKgRedefinido(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure QrVSFchRslCabBeforeClose(DataSet: TDataSet);
    procedure QrVSFchRslCabAfterScroll(DataSet: TDataSet);
    procedure EdCustoAllExit(Sender: TObject);
    procedure EdCustoAllEnter(Sender: TObject);
    procedure EdCustoUniEnter(Sender: TObject);
    procedure EdCustoUniExit(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure RGTpCalcValClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxWET_CURTI_058_001GetValue(const VarName: string;
      var Value: Variant);
    procedure QrMPrimaBeforeClose(DataSet: TDataSet);
    procedure QrMPrimaAfterScroll(DataSet: TDataSet);
    procedure BtAtualizaClick(Sender: TObject);
    procedure QrVSFchRslItsCalcFields(DataSet: TDataSet);
    procedure EdTribtMPRedefinido(Sender: TObject);
    procedure EdTribtMORedefinido(Sender: TObject);
  private
    procedure CriaOForm;
    procedure CalculaCustoMP(FromUni: Boolean);
    procedure CalculaTribtAll();
    procedure DefineONomeDoForm;
    procedure AlteraTodosItens(CustoKg: Double);
    procedure InsereTodosItens();
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSFchRslCab();
    procedure ReopenVSFchRslIts(GraGruX: Integer);
    procedure Va(Para: TVaiPara);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    FUni, FAll: Double;
    //
    procedure ReopenLote(SerieFch, Ficha: Integer);
    //procedure ReopenVSFchGerIDs(SerieFch, Ficha, MovimID: Integer);
    //procedure ReopenVSFchGerNivs(MovimNiv: Integer);
    procedure ReopenFichas();
    procedure ReopenArtigos();
    procedure ReopenIMEIs(Controle: Integer);
    procedure ReopenMPrima(Controle: Integer);
    procedure ReopenVSPaClaIts();
    procedure ReopenVSPaMulCab();
    procedure ReopenVSPaMulIts(Controle: Integer);
    procedure ReopenVSMovDif();
    procedure BuscaDadosVSInnFicha();
  end;

var
  FmVSFchRslCab: TFmVSFchRslCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnVS_PF, VSMovItsAlt, GetValor,
  VSFchRslIts, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSFchRslCab.LocCod(Atual, Codigo: Integer);
begin
  //DefParams;
  //GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSFchRslCab.PMCabPopup(Sender: TObject);
begin
  CabInclui1.Enabled := QrVSFchRslCab.RecordCount = 0 ;
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSFchRslCab);
  CabExclui1.Enabled := QrVSFchRslCab.RecordCount > 0 ;
end;

procedure TFmVSFchRslCab.PMItsPopup(Sender: TObject);
begin
  //MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSFchRslCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSFchRslIts);
  //MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSFchRslIts);
end;

procedure TFmVSFchRslCab.Va(Para: TVaiPara);
begin
(*
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSFchRslCabCodigo.Value, LaRegistro.Caption[2]);
*)
  Geral.MB_Info('N�o implementado! Solicite � DERMATEK!');
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSFchRslCab.DefParams;
begin
(*
  VAR_GOTOTABELA := 'vsfchrslcab';
  VAR_GOTOMYSQLTABLE := QrCadastro_Com_Itens_CAB;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM vsfchrslcab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
*)
end;

procedure TFmVSFchRslCab.EdTribtMPRedefinido(Sender: TObject);
begin
  CalculaTribtAll();
end;

procedure TFmVSFchRslCab.EdTribtMORedefinido(Sender: TObject);
begin
  CalculaTribtAll();
end;

procedure TFmVSFchRslCab.EdCustoAllEnter(Sender: TObject);
begin
  FAll := EdCustoAll.ValueVariant;
end;

procedure TFmVSFchRslCab.EdCustoAllExit(Sender: TObject);
begin
  if FAll <> EdCustoAll.ValueVariant then
    CalculaCustoMP(False);
end;

procedure TFmVSFchRslCab.EdCustoUniEnter(Sender: TObject);
begin
  FUni := EdCustoUni.ValueVariant;
end;

procedure TFmVSFchRslCab.EdCustoUniExit(Sender: TObject);
begin
  if FUni <> EdCustoUni.ValueVariant then
    CalculaCustoMP(True);
end;

procedure TFmVSFchRslCab.EdPecasRedefinido(Sender: TObject);
begin
  CalculaCustoMP(True);
end;

procedure TFmVSFchRslCab.EdPesoKgRedefinido(Sender: TObject);
begin
  CalculaCustoMP(True);
end;

procedure TFmVSFchRslCab.ExcluiIMEIselecionado1Click(Sender: TObject);
begin
(*
  if VS_PF.ExcluiVSMovIts_EnviaArquivoExclu(QrIMEIsControle.Value,
  Integer(TEstqMotivDel.emtdWetCurti013), Dmod.QrUpd, Dmod.MyDB) then
  begin
    LocCod(QrVSPalletCodigo.Value, QrVSPalletCodigo.Value);
  end;
*)
end;

procedure TFmVSFchRslCab.CabAltera1Click(Sender: TObject);
begin
  GBBaseItens.Visible := False;
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSFchRslCab, [PnDados],
  [PnEdita], EdPecas, ImgTipo, 'vsfchrslcab');
end;

procedure TFmVSFchRslCab.CabExclui1Click(Sender: TObject);
const
  Pergunta = 'Confirma a exclus�o do resultado do lote selecionado?';
var
  SerieFch, Ficha: Integer;
  Tabela: String;
begin
  if QrVSFchRslCab.RecordCount > 0 then
  begin
    SerieFch := QrVSFchRslCabSerieFch.Value;
    Ficha := QrVSFchRslCabFicha.Value;
    // Primeiro os itens...
    Tabela := 'vsfchrslits';
    if UMyMod.ExcluiRegistroIntArr(Pergunta, Tabela, [
    'SerieFch', 'Ficha'], [
    SerieFch, Ficha]) = ID_YES then
    begin
      // ... depois o cabecalho!
      Tabela := 'vsfchrslcab';
      if UMyMod.ExcluiRegistroIntArr('', Tabela, [
      'SerieFch', 'Ficha'], [
      SerieFch, Ficha]) = ID_YES then
      begin
        ReopenLote(SerieFch, Ficha);
      end;
    end;
  end;
end;

procedure TFmVSFchRslCab.CriaOForm;
begin
  DefineONomeDoForm;
  //DefParams;
  //Va(vpLast);
end;

procedure TFmVSFchRslCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSFchRslCab.InsereTodosItens();
var
  KgMedio: Double;
  //
  function InsereAtual(): Boolean;
  var
    SerieFch, Ficha, GraGruX: Integer;
    PesoKg, CustoKg, CustoMOKg, CusFretKg, Preco, ImpostP, ComissP, FreteM2,
    Pecas, AreaM2, SrcMPAG, ImpostCred: Double;
  begin
    SerieFch       := QrLoteSerieFch.Value;
    Ficha          := QrLoteFicha.Value;
    GraGruX        := QrArtigosGraGruX.Value;
    //                        //
    Pecas          := QrArtigosPecas.Value;
    AreaM2         := QrArtigosAreaM2.Value;
    PesoKg         := KgMedio * Pecas;
    if QrVSFchRslCabPesoKg.Value > 0 then
    begin
      CustoKg := QrVSFchRslCabCustoAll.Value / QrVSFchRslCabPesoKg.Value;
      ImpostCred := QrVSFchRslCabTribtAll.Value / QrVSFchRslCabPesoKg.Value * PesoKg
    end else
    begin
      CustoKg := 0;
      ImpostCred := 0;
    end;
    //
    CustoMOKg      := EdCustoMOKg.ValueVariant;
    CusFretKg      := EdCusFretKg.ValueVariant;
    Preco          := EdPreco.ValueVariant;
    ImpostP        := EdImpostP.ValueVariant;
    ComissP        := EdComissP.ValueVariant;
    FreteM2        := EdFreteM2.ValueVariant;
    //SrcMPAG        := EdSrcMPAG.ValueVariant;
    //
    VS_PF.InsereVSFchRslIts(stIns, SerieFch, Ficha, GraGruX, CustoKg, CustoMOKg,
    CusFretKg, Preco, ImpostP, ComissP, FreteM2, Pecas, AreaM2, PesoKg,
    ImpostCred);
  end;
var
  PecasReal, PesoReal: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    PecasReal := 0;
    (*
    QrArtigos.First;
    while not QrArtigos.Eof do
    begin
      PecasReal := PecasReal + QrArtigosPecas.Value;
      //
      QrArtigos.Next;
    end;
    if PecasReal > 0 then
      //KgMedio := QrVSFchRslCabPesoKg.Value / QrVSFchRslCabPecas.Value
      KgMedio := QrVSFchRslCabPesoKg.Value / PecasReal
    else
      KgMedio := 0;
    //
    *)
    if QrVSFchRslCabPecas.Value > 0 then
      KgMedio := QrVSFchRslCabPesoKg.Value / QrVSFchRslCabPecas.Value
    else
      KgMedio := 0;
    QrArtigos.First;
    while not QrArtigos.Eof do
    begin
      InsereAtual();
      //
      QrArtigos.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSFchRslCab.IrparajaneladedadosdoIMEI1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMovIts(QrIMEIsControle.Value);
end;

procedure TFmVSFchRslCab.Irparajaneladomovimento1Click(Sender: TObject);
begin
(*
  if QrVSFchGerNivs.RecordCount > 0 then
    VS_PF.MostraFormVS_Do_IMEI(QrIMEISControle.Value);
*)
end;

procedure TFmVSFchRslCab.ItsAltera1Click(Sender: TObject);
const
  SQLType = stUpd;
begin
  if DBCheck.CriaFm(TFmVSFchRslIts, FmVSFchRslIts, afmoNegarComAviso) then
  begin
    FmVSFchRslIts.ImgTipo.SQLType := SQLType;
    FmVSFchRslIts.FQrCab := QrVSFchRslCab;
    FmVSFchRslIts.FDsCab := DsVSFchRslCab;
    FmVSFchRslIts.FQrIts := QrVSFchRslIts;
    if SQLType = stIns then
    begin
      //FmVSFchRslIts.EdCPF1.ReadOnly := False
    end else
    begin
      FmVSFchRslIts.EdSerieFch.ValueVariant := QrVSFchRslItsSerieFch.Value;
      FmVSFchRslIts.EdFicha.ValueVariant    := QrVSFchRslItsFicha.Value;
      FmVSFchRslIts.EdGraGruX.ValueVariant  := QrVSFchRslItsGraGruX.Value;
      FmVSFchRslIts.EdNO_PRD_TAM_COR.ValueVariant := QrVSFchRslItsNO_PRD_TAM_COR.Value;
      //
      FmVSFchRslIts.EdPecas.ValueVariant     := QrVSFchRslItsPecas.Value;
      FmVSFchRslIts.EdPesoKg.ValueVariant    := QrVSFchRslItsPesoKg.Value;
      FmVSFchRslIts.EdCustoKg.ValueVariant   := QrVSFchRslItsCustoKg.Value;
      FmVSFchRslIts.EdCustoMOKg.ValueVariant := QrVSFchRslItsCustoMOKg.Value;
      FmVSFchRslIts.EdCusFretKg.ValueVariant := QrVSFchRslItsCusFretKg.Value;
      FmVSFchRslIts.EdAreaM2.ValueVariant    := QrVSFchRslItsAreaM2.Value;
      FmVSFchRslIts.EdPreco.ValueVariant     := QrVSFchRslItsPreco.Value;
      FmVSFchRslIts.EdFreteM2.ValueVariant   := QrVSFchRslItsFreteM2.Value;
      FmVSFchRslIts.EdComissP.ValueVariant   := QrVSFchRslItsComissP.Value;
      FmVSFchRslIts.EdImpostP.ValueVariant   := QrVSFchRslItsImpostP.Value;
      FmVSFchRslIts.EdImpostCred.ValueVariant := QrVSFchRslItsImpostCred.Value;
    end;
    FmVSFchRslIts.ShowModal;
    FmVSFchRslIts.Destroy;
    //
    ReopenVSFchRslIts(QrVSFchRslItsGraGruX.Value);
  end;
end;

procedure TFmVSFchRslCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if VS_PF.ExcluiVSNaoVMI('Confirma a exclus�o do item selecionado?',
  'Cadastro_Com_Itens_ITS', 'Controle', QrCadastro_Com_Itens_ITSControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrCadastro_Com_Itens_ITS,
      QrCadastro_Com_Itens_ITSControle, QrCadastro_Com_Itens_ITSControle.Value);
    ReopenVSFchGerIDs(Controle);
  end;
}
end;

procedure TFmVSFchRslCab.ReopenArtigos();
var
  Itens: String;
begin
 //
  UnDmkDAC_PF.AbreMySQLQuery0(QrMul, Dmod.MyDB, [
  '    SELECT DISTINCT(MovimCod) MovimCod ',
  '    FROM ' + CO_SEL_TAB_VMI + ' ',
  '    WHERE SerieFch=' + Geral.FF0(QrLoteSerieFch.Value),
  '    AND Ficha=' + Geral.FF0(QrLoteFicha.Value),
  '    AND MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)),//=6 ',
  '']);
  Itens := '';
  QrMul.First;
  while not QrMul.Eof do
  begin
    if Itens <> '' then
      Itens := Itens + ', ';
    Itens := Itens + Geral.FF0(QrMul.FieldByName('MovimCod').AsInteger);
    //
    QrMul.Next;
  end;
  if Itens = '' then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrMul, Dmod.MyDB, [
  '  SELECT DISTINCT wmi.Codigo ',
  '  FROM ' + CO_SEL_TAB_VMI + ' wmi ',
  '  WHERE wmi.MovimCod IN ',
  '    ( ' + Itens + ' ) ',
  '  AND wmi.MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminSorcCurtiXX)),//3 ',
  '']);
  Itens := '';
  QrMul.First;
  while not QrMul.Eof do
  begin
    if Itens <> '' then
      Itens := Itens + ', ';
    Itens := Itens + Geral.FF0(QrMul.FieldByName('Codigo').AsInteger);
    //
    QrMul.Next;
  end;
  if Itens = '' then
    Exit;
//sql dos couros classificados!
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo Artigos');
  UnDmkDAC_PF.AbreMySQLQuery0(QrArtigos, Dmod.MyDB, [
(*
  'SELECT vmi.GraGruX, ',
  'SUM(cia.Pecas) Pecas, SUM(cia.AreaM2) AreaM2, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM vspaclaitsa pri ',
  'LEFT JOIN vspaclacaba pra ON pra.Codigo=pri.Codigo ',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=pri.VMI_Dest ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vscacitsa cia ON cia.VSPaClaIts=pri.Controle',
  'WHERE pra.VSGerArt IN ( ',
  Itens,
  '  ) ',
  'GROUP BY GraGruX ',
*)
  'SELECT vmi.GraGruX,  ',
  '/*SUM(vmi.Pecas) Pecas, SUM(vmi.AreaM2) AreaM2, */ ',
  'SUM(cia.Pecas) Pecas, SUM(cia.AreaM2) AreaM2,  ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR  ',
  'FROM vspaclaitsa pri  ',
  'LEFT JOIN vspaclacaba pra ON pra.Codigo=pri.Codigo  ',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=pri.VMI_Dest  ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX  ',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN vscacitsa cia ON cia.VSPaClaIts=pri.Controle ',
  'WHERE pra.VSGerArt IN (  ',
  Itens,
  '  )  ',
  'GROUP BY GraGruX  ',
  ' ',
  'UNION ',
  ' ',
  'SELECT vmi.GraGruX,  ',
  'SUM(vmi.Pecas) Pecas, SUM(vmi.AreaM2) AreaM2,  ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN vspamulcaba pmc ON pmc.MovimCod=vmi.MovimCod ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX  ',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE pmc.VSGerArt IN (  ',
  Itens,
  '  )  ',
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestClass)),
  ' ',
  'GROUP BY GraGruX  ',
  '']);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //Geral.MB_SQL(Self, QrArtigos);

////////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo SumArt');
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumArt, Dmod.MyDB, [
  'SELECT SUM(vmi.Pecas) Pecas, SUM(vmi.AreaM2) AreaM2, ',
  'SUM(PesoKg) PesoKg',
  'FROM vspaclaitsa pri ',
  'LEFT JOIN vspaclacaba pra ON pra.Codigo=pri.Codigo ',
  'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=pri.VMI_Dest ',
  'WHERE pra.VSGerArt IN ( ',
  Itens,
  '  ) ',
  '']);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSFchRslCab.ReopenFichas();
begin
  //SQL dos IME-Is das classificacoes nas quais tem a ficha atual
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo Fichas');
  UnDmkDAC_PF.AbreMySQLQuery0(QrFichas, Dmod.MyDB, [
  'SELECT wmi.*, ',
  'IF(wmi.Pecas = 0, 0, wmi.AreaM2 / wmi.Pecas) MediaM2, ',
  'vsf.Nome NO_SerieFch, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE  ',
  'FROM ' + CO_SEL_TAB_VMI + ' wmi  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN vspalleta  wbp ON wbp.Codigo=wmi.Pallet  ',
  'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch  ',
  'WHERE wmi.MovimCod IN  ',
  '  ( ',
  'SELECT DISTINCT(MovimCod) MovimCod  ',
  'FROM ' + CO_SEL_TAB_VMI + ' ',
  'WHERE SerieFch=' + Geral.FF0(QrLoteSerieFch.Value),
  'AND Ficha=' + Geral.FF0(QrLoteFicha.Value),
  'AND MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)), //6
  '  ) ',
  'AND wmi.MovimNiv=' + Geral.FF0(Integer(TEstqMovimNiv.eminDestCurtiXX)),//13 ',
  'ORDER BY NO_Pallet, wmi.Controle  ',
  '']);
  //Geral.MB_SQL(Self, QrFichas);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSFchRslCab.ReopenIMEIs(Controle: Integer);
var
  ATT_MovimID, ATT_MovimNiv: String;
begin
(*
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrIMEIS, Dmod.MyDB, [
  'SELECT vmi.Controle, vmi.GraGruX, vmi.Pecas,',
  'vmi.PesoKg, vmi.AreaM2, vmi.AreaP2, vmi.ValorT,',
  'vmi.SdoVrtPeca, vmi.SdoVrtPeso, vmi.SdoVrtArM2,',
  ATT_MovimID,
  ATT_MovimNiv,
  'vmi.MovimID, vmi.MovimNiv, vmi.Pallet, ',
  'ggx.GraGru1, CONCAT(gg1.Nome,',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
  'NO_PRD_TAM_COR, vmi.DataHora, ',
  'vmi.SrcMovID, vmi.SrcNivel1, vmi.SrcNivel2, ',
  'vmi.DstMovID, vmi.DstNivel1, vmi.DstNivel2 ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'WHERE vmi.SerieFch=' + Geral.FF0(QrVSFchGerNivsSerieFch.Value),
  'AND vmi.Ficha=' + Geral.FF0(QrVSFchGerNivsFicha.Value),
  'AND vmi.MovimID=' + Geral.FF0(QrVSFchGerNivsMovimID.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(QrVSFchGerNivsMovimNiv.Value),
  '']);
  //Geral.MB_SQL(self, QrIMEIS);
  if Controle <> 0 then
    QrIMEIs.Locate('Controle', Controle, []);
*)
end;

procedure TFmVSFchRslCab.ReopenMPrima(Controle: Integer);
var
  ATT_MovimID, ATT_MovimNiv: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo MPrima');
  UnDmkDAC_PF.AbreMySQLQuery0(QrMPrima, Dmod.MyDB, [
  'SELECT vmi.Controle, vmi.GraGruX, vmi.Pecas,',
  'vmi.PesoKg, vmi.AreaM2, vmi.AreaP2, vmi.ValorT,',
  'vmi.SdoVrtPeca, vmi.SdoVrtPeso, vmi.SdoVrtArM2,',
  ATT_MovimID,
  ATT_MovimNiv,
  'vmi.MovimID, vmi.MovimNiv, vmi.Pallet, ',
  'vmi.NotaMPAG, vmi.Observ, ',
  'IF(vmi.Pecas = 0, 0, vmi.PesoKg / vmi.Pecas) MediaKg, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ,',
  'ggx.GraGru1, CONCAT(gg1.Nome,',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
  'NO_PRD_TAM_COR, vmi.DataHora, ',
  'vmi.SrcMovID, vmi.SrcNivel1, vmi.SrcNivel2, ',
  'vmi.DstMovID, vmi.DstNivel1, vmi.DstNivel2, ',
  'vnc.FatorNota ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vmi.GraGruX',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
  'LEFT JOIN vsnatcad   vnc ON vnc.GraGruX=ggx.Controle ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'WHERE vmi.SerieFch=' + Geral.FF0(QrLoteSerieFch.Value),
  'AND vmi.Ficha=' + Geral.FF0(QrLoteFicha.Value),
  'AND vmi.MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)), // 1
  //'AND vmi.MovimNiv=' + Geral.FF0(QrVSFchGerNivsMovimNiv.Value),
  '']);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //Geral.MB_SQL(self, QrMPrima);
  if Controle <> 0 then
    QrIMEIs.Locate('Controle', Controle, []);
////////////////////////////////////////////////////////////////////////////////
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo SumMP');
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumMP, Dmod.MyDB, [
  'SELECT SUM(vmi.Pecas) Pecas, SUM(vmi.AreaM2) AreaM2, ',
  'SUM(vmi.PesoKg) PesoKg ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'WHERE vmi.SerieFch=' + Geral.FF0(QrLoteSerieFch.Value),
  'AND vmi.Ficha=' + Geral.FF0(QrLoteFicha.Value),
  'AND vmi.MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)), // 1
  '']);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSFchRslCab.ReopenLote(SerieFch, Ficha: Integer);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo Lote');
  UnDmkDAC_PF.AbreMySQLQuery0(QrLote, Dmod.MyDB, [
  'SELECT vmi.SerieFch, vmi.Ficha,   ',
  '(vmi.Ficha + (vmi.SerieFch/10000)) FCH_SRE,   ',
  'COUNT(vmi.Codigo) ITENS,   ',
  'MIN(vmi.DataHora) RefFirst, MAX(vmi.DataHora) RefLast,  ',
  'vsf.Nome NO_SERIEFCH   ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
  'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch  ',
  'WHERE vmi.SerieFch=' + Geral.FF0(SerieFch),
  'AND vmi.Ficha=' + Geral.FF0(Ficha),
  'GROUP BY vmi.SerieFch, vmi.Ficha',
  '']);
  //Geral.MB_SQL(self, QrLote);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSFchRslCab.ReopenVSFchRslCab();
var
  ATT_FrmPago: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo VSFchRslCab');
  ATT_FrmPago := dmkPF.ArrayToTexto('rsl.FrmPago', 'NO_FrmPago', pvPos, True,
    sListaTipoCalcCouro);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSFchRslCab, Dmod.MyDB, [
  'SELECT vsf.Nome NO_SerieFch, ',
  ATT_FrmPago,
  'rsl.* ',
  'FROM vsfchrslcab rsl ',
  'LEFT JOIN vsserfch vsf ON vsf.Codigo=rsl.SerieFch ',
  'WHERE rsl.SerieFch=' + Geral.FF0(QrLoteSerieFch.Value),
  'AND rsl.Ficha=' + Geral.FF0(QrLoteFicha.Value),
  '']);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmVSFchRslCab.ReopenVSFchRslIts(GraGruX: Integer);
begin
  // Abrir antes por causa do CalcFields
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo VSFchRslSum');
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSFchRslSum, Dmod.MyDB, [
  'SELECT SUM(fsi.Pecas) Pecas, SUM(fsi.AreaM2) AreaM2,  ',
  'SUM(fsi.PesoKg) PesoKg, SUM(fsi.CustoMOTot) CustoMOTot, ',
  'SUM(fsi.CusFretTot) CusFretTot, SUM(fsi.CustoINTot) CustoINTot, ',
  'SUM(fsi.CustoTotal) CustoTotal, SUM(fsi.ImpostTotV) ImpostTotV, ',
  'SUM(fsi.ComissTotV) ComissTotV, SUM(fsi.FretM2Tot) FretM2Tot, ',
  'SUM(fsi.BrutoV) BrutoV, SUM(fsi.MargemV) MargemV, ',
  '(SUM(fsi.MargemV) / SUM(CustoTotal)) * 100 MargemP ',
  'FROM vsfchrslits fsi   ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=fsi.GraGruX   ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1    ',
  'WHERE fsi.SerieFch=' + Geral.FF0(QrLoteSerieFch.Value),
  'AND fsi.Ficha=' + Geral.FF0(QrLoteFicha.Value),
  '']);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo VSFchRslIts');
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSFchRslIts, Dmod.MyDB, [
  'SELECT fsi.*, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR  ',
  'FROM vsfchrslits fsi  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=fsi.GraGruX  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'WHERE fsi.SerieFch=' + Geral.FF0(QrLoteSerieFch.Value),
  'AND fsi.Ficha=' + Geral.FF0(QrLoteFicha.Value),
  '']);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
  if GraGruX <> 0 then
    QrVSFchRslIts.Locate('GraGruX', GraGruX, []);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
end;

procedure TFmVSFchRslCab.ReopenVSMovDif;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo VSMovDif');
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovDif, Dmod.MyDB, [
  'SELECT * ',
  'FROM vsmovdif ',
  'WHERE Controle=' + Geral.FF0(QrMPrimaControle.Value),
  '']);
end;

procedure TFmVSFchRslCab.ReopenVSPaClaIts();
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo VSPaClaIts');
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaClaIts, Dmod.MyDB, [
  'SELECT pri.*, IF(DtHrFim < "1900-01-01", "",  ',
  '  DATE_FORMAT(DtHrFim, "%d/%m/%y %h:%i:%s")) DtHrFim_TXT ',
  'FROM vspaclaitsa pri ',
  'LEFT JOIN  vspaclacaba pra ON pra.Codigo=pri.Codigo ',
  'WHERE pra.VSGerArt=' + Geral.FF0(QrFichasCodigo.Value),
  'ORDER BY pri.DtHrIni ',
  '']);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
  //Geral.MB_SQL(Self, QrVSPaClaIts);
end;

procedure TFmVSFchRslCab.ReopenVSPaMulCab();
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo VSPaMulCab');
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaMulCab, Dmod.MyDB, [
  'SELECT pma.*, ',
  'IF(DataHora < "1900-01-01", "",   ',
  '  DATE_FORMAT(DataHora, "%d/%m/%y %h:%i:%s")) DataHora_TXT ',
  'FROM vspamulcaba pma ',
  'WHERE pma.VSGerArt=' + Geral.FF0(QrFichasCodigo.Value),
  '']);
  //
end;

procedure TFmVSFchRslCab.ReopenVSPaMulIts(Controle: Integer);
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaMulIts, Dmod.MyDB, [
  'SELECT wmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
  'FROM ' + CO_SEL_TAB_VMI + ' wmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet ',
  'LEFT JOIN entidades  frn ON frn.Codigo=wmi.Terceiro ',
  'WHERE wmi.MovimCod=' + Geral.FF0(QrFichasMovimCod.Value),
  'AND MovimNiv=' + Geral.FF0(Integer(eminDestClass)), //2
  'ORDER BY wmi.Controle ',
  '']);
  //
  QrVSPaMulIts.Locate('Controle', Controle, []);
*)
end;

procedure TFmVSFchRslCab.RGTpCalcValClick(Sender: TObject);
begin
  CalculaCustoMP(True);
end;

{
procedure TFmVSFchRslCab.ReopenVSFchGerIDs(SerieFch, Ficha, MovimID: Integer);
var
  //ATT_MovimID, ATT_MovimNiv: String;
  ATT_MovimID: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  //ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
  //  sEstqMovimNiv);
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSFchGerIDs, Dmod.MyDB, [
  'SELECT vmi.MovimID, vmi.SerieFch, vmi.Ficha,   ',
  '(vmi.Ficha + (vmi.SerieFch/10000)) FCH_SRE,   ',
  'COUNT(vmi.Codigo) ITENS,   ',
  'MIN(vmi.DataHora) RefFirst, MAX(vmi.DataHora) RefLast,  ',
  ATT_MovimID,
  'vsf.Nome NO_SERIEFCH ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
  'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch  ',
  'WHERE vmi.SerieFch=' + Geral.FF0(SerieFch),
  'AND vmi.Ficha=' + Geral.FF0(Ficha),
  'GROUP BY vmi.SerieFch, vmi.Ficha, vmi.MovimID  ',
  '']);
  //
  QrVSFchGerIDs.Locate('MovimID', MovimID, []);
end;


procedure TFmVSFchRslCab.ReopenVSFchGerNivs(MovimNiv: Integer);
var
  //ATT_MovimID, ATT_MovimNiv: String;
  ATT_MovimNiv: String;
begin
  //ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
  //  sEstqMovimID);
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSFchGerNivs, Dmod.MyDB, [
  'SELECT vmi.MovimID, vmi.MovimNiv, vmi.SerieFch, vmi.Ficha, ',
  'COUNT(vmi.Codigo) ITENS,   ',
  'MIN(vmi.DataHora) RefFirst, MAX(vmi.DataHora) RefLast, ',
  ATT_MovimNiv,
  'SUM(vmi.Pecas) Pecas, SUM(vmi.AreaM2) AreaM2 ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi  ',
  'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch  ',
  'WHERE vmi.SerieFch=' + Geral.FF0(QrVSFchGerIDsSerieFch.Value),
  'AND vmi.Ficha=' + Geral.FF0(QrVSFchGerIDsFicha.Value),
  'AND vmi.MovimID=' + Geral.FF0(QrVSFchGerIDsMovimID.Value),
  'GROUP BY vmi.MovimNiv  ',
  '']);
  //
  //Geral.MB_SQL(Self, QrVSFchGerNivs);
  //
  QrVSFchGerNivs.Locate('MovimNiv', MovimNiv, []);
end;
}

procedure TFmVSFchRslCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSFchRslCab.SpeedButton1Click(Sender: TObject);
begin
  //Va(vpFirst);
  QrLotes.First;
end;

procedure TFmVSFchRslCab.SpeedButton2Click(Sender: TObject);
begin
  //Va(vpPrior);
  QrLotes.Prior;
end;

procedure TFmVSFchRslCab.SpeedButton3Click(Sender: TObject);
begin
  //Va(vpNext);
  QrLotes.Next;
end;

procedure TFmVSFchRslCab.SpeedButton4Click(Sender: TObject);
begin
  //Va(vpLast);
  QrLotes.Last;
end;

procedure TFmVSFchRslCab.teste1Click(Sender: TObject);
const
  AtualizaSaldoModoGenerico = False;
begin
  VS_PF.MostraFormVSMovItsAlt(QrIMEIsControle.Value, AtualizaSaldoModoGenerico,
    [eegbDadosArtigo]);
end;

procedure TFmVSFchRslCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSFchRslCab.BuscaDadosVSInnFicha();
var
  TribtAll, TribtMP, TribtMO, PecasReal: Double;
  UsaReal: Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSInnSum, Dmod.MyDB, [
  'SELECT SUM(vmi.Pecas) Pecas, SUM(vmi.PesoKg) PesoKg, ',
  'SUM(vmi.AreaM2) AreaM2, SUM(vmi.AreaP2) AreaP2,  ',
  'SUM(vmi.ValorT) ValorT  ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi',
  'WHERE vmi.SerieFch=' + Geral.FF0(QrLoteSerieFch.Value),
  'AND vmi.Ficha=' + Geral.FF0(QrLoteFicha.Value),
  'AND vmi.MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)), // 1
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTribIncIts, Dmod.MyDB, [
  'SELECT SUM(ValrTrib) ValrTrib ',
  'FROM tribincits',
  'WHERE FatID=' + Geral.FF0(VAR_FATID_1003),
  'AND FatNum IN (',
  '  SELECT Codigo ',
  '  FROM ' + CO_SEL_TAB_VMI + '',
  '  WHERE SerieFch=' + Geral.FF0(QrLoteSerieFch.Value),
  '  AND Ficha=' + Geral.FF0(QrLoteFicha.Value),
  '  AND MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidCompra)), // 1
  ')',
  '']);
  TribtMP := QrTribIncItsValrTrib.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTribIncIts, Dmod.MyDB, [
  'SELECT SUM(ValrTrib) ValrTrib ',
  'FROM tribincits',
  'WHERE FatID=' + Geral.FF0(VAR_FATID_1020),
  'AND FatNum IN (',
  '  SELECT Codigo ',
  '  FROM ' + CO_SEL_TAB_VMI + '',
  '  WHERE SerieFch=' + Geral.FF0(QrLoteSerieFch.Value),
  '  AND Ficha=' + Geral.FF0(QrLoteFicha.Value),
  '  AND MovimID=' + Geral.FF0(Integer(TEstqMovimID.emidIndsXX)), // 1
  ')',
  '']);
  TribtMO := QrTribIncItsValrTrib.Value;
  //
  QrArtigos.First;
  while not QrArtigos.Eof do
  begin
    PecasReal := PecasReal + QrArtigosPecas.Value;
    //
    QrArtigos.Next;
  end;
  if PecasReal <> QrSumMPPecas.Value then
  begin
    UsaReal := MyObjects.SelRadioGroup('Pe�as',
    'Selecione a quantidade de pe�as correta', [
    Geral.FF0(Trunc(QrSumMPPecas.Value)),
    Geral.FF0(Trunc(PecasReal))], 1) = 1;
  end else
    UsaReal := False;
  //
  RGTpCalcVal.ItemIndex := Integer(TTipoCalcCouro.ptcPesoKg);
  if UsaReal then
    EdPecas.ValueVariant := PecasReal
  else
    EdPecas.ValueVariant := QrSumMPPecas.Value;
  //
  EdPesoKg.ValueVariant := QrSumMPPesoKg.Value;
  //
  EdCustoAll.ValueVariant := QrVSInnSumValorT.Value;
  EdTribtMP.ValueVariant := TribtMP;
  EdTribtMO.ValueVariant := TribtMO;
  EdCustoLiq.ValueVariant := QrVSInnSumValorT.Value - (TribtMP + TribtMO);
  //
  CalculaCustoMP(False);
end;

procedure TFmVSFchRslCab.BtSaidaClick(Sender: TObject);
begin
  //VAR_CADASTRO := QrCadastro_Com_Itens_CABCodigo.Value;
  Close;
end;

procedure TFmVSFchRslCab.BtDesisteClick(Sender: TObject);
(*
var
  Codigo : Integer;
*)
begin
(*
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vsfchrslcab', Codigo);
*)
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
(*
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vsfchrslcab', 'Codigo');
*)
end;

procedure TFmVSFchRslCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSFchRslCab.AlteraIMEIorigemdoIMEISelecionado1Click(
  Sender: TObject);
(*
var
  ResVar: Variant;
  Controle, SrcNivel2: Integer;
*)
begin
(*
  if VAR_USUARIO <> -1 then
  begin
    Geral.MB_Aviso('Usu�rio deve ser Master!');
    Exit;
  end;
  SrcNivel2 := 0;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger,
  0, 0, 0, '', '', True, 'Troca IME-I Origem', 'Informe o novo IME-I de origem: ',
  0, ResVar) then
  begin
    SrcNivel2 := Geral.IMV(ResVar);
    Controle  := QrIMEIsControle.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
    'SrcNivel2'], ['Controle'], [
    SrcNivel2], [Controle], True) then
    begin
      ReopenIMEIs(Controle);
    end;
  end;
*)
end;

procedure TFmVSFchRslCab.AlteraTodosItens(CustoKg: Double);
  procedure AlteraAtual();
  var
    PesoKg, CustoMOTot, CusFretTot, ImpostTotV, ComissTotV, FretM2Tot, BrutoV,
    CustoINTot, CustoTotal, MargemV, MargemP, ImpostCred: Double;
    SerieFch, Ficha, GraGruX: Integer;
  begin
    PesoKg         := QrVSFchRslItsPesoKg.Value;
    CustoMOTot     := QrVSFchRslItsCustoMOTot.Value;
    CusFretTot     := QrVSFchRslItsCusFretTot.Value;
    ImpostTotV     := QrVSFchRslItsImpostTotV.Value;
    ComissTotV     := QrVSFchRslItsComissTotV.Value;
    FretM2Tot      := QrVSFchRslItsFretM2Tot.Value;
    BrutoV         := QrVSFchRslItsBrutoV.Value;
    ImpostCred     := QrVSFchRslItsImpostCred.Value;
    //
    CustoINTot     := Geral.RoundC(CustoKg * PesoKg, 2);
    CustoTotal     := CustoINTot + CustoMOTot + CusFretTot + ImpostTotV + ComissTotV + FretM2Tot;
    MargemV        := BrutoV - CustoTotal + ImpostCred;
    //
    if BrutoV = 0 then
      MargemP      := 0
    else
      MargemP      := Geral.RoundC((MargemV / CustoTotal) * 100, 4);
    //
    SerieFch       := QrVSFchRslItsSerieFch.Value;
    Ficha          := QrVSFchRslItsFicha.Value;
    GraGruX        := QrVSFchRslItsGraGruX.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsfchrslits', False, [
    (*'Pecas', 'AreaM2',*) 'PesoKg',
    'CustoKg', (*'CustoMOKg', 'CusFretKg',
    'Preco', 'ImpostP', 'ComissP',
    'FreteM2', 'SrcMPAG',*) 'CustoMOTot',
    'CusFretTot', 'ImpostTotV', 'ComissTotV',
    'FretM2Tot', 'BrutoV', 'MargemV',
    'MargemP', 'CustoINTot', 'CustoTotal'], [
    'SerieFch', 'Ficha', 'GraGruX'], [
    (*Pecas, AreaM2,*) PesoKg,
    CustoKg, (*CustoMOKg, CusFretKg,
    Preco, ImpostP, ComissP,
    FreteM2, SrcMPAG,*) CustoMOTot,
    CusFretTot, ImpostTotV, ComissTotV,
    FretM2Tot, BrutoV, MargemV,
    MargemP, CustoINTot, CustoTotal], [
    SerieFch, Ficha, GraGruX], True);





(*      FmVSFchRslIts.EdSerieFch.ValueVariant := QrVSFchRslItsSerieFch.Value;
      FmVSFchRslIts.EdFicha.ValueVariant    := QrVSFchRslItsFicha.Value;
      FmVSFchRslIts.EdGraGruX.ValueVariant  := QrVSFchRslItsGraGruX.Value;
      FmVSFchRslIts.EdNO_PRD_TAM_COR.ValueVariant := QrVSFchRslItsNO_PRD_TAM_COR.Value;
      //
      FmVSFchRslIts.EdPecas.ValueVariant     := QrVSFchRslItsPecas.Value;
      FmVSFchRslIts.EdPesoKg.ValueVariant    := QrVSFchRslItsPesoKg.Value;
      FmVSFchRslIts.EdCustoKg.ValueVariant   := QrVSFchRslItsCustoKg.Value;
      FmVSFchRslIts.EdCustoMOKg.ValueVariant := QrVSFchRslItsCustoMOKg.Value;
      FmVSFchRslIts.EdCusFretKg.ValueVariant := QrVSFchRslItsCusFretKg.Value;
      FmVSFchRslIts.EdAreaM2.ValueVariant    := QrVSFchRslItsAreaM2.Value;
      FmVSFchRslIts.EdPreco.ValueVariant     := QrVSFchRslItsPreco.Value;
      FmVSFchRslIts.EdFreteM2.ValueVariant   := QrVSFchRslItsFreteM2.Value;
      FmVSFchRslIts.EdComissP.ValueVariant   := QrVSFchRslItsComissP.Value;
      FmVSFchRslIts.EdImpostP.ValueVariant   := QrVSFchRslItsImpostP.Value;
*)

  end;
var
  SerieFch, Ficha, GraGruX: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    GraGruX  := QrVSFchRslItsGraGruX.Value;
    //
    SerieFch := QrVSFchRslCabSerieFch.Value;
    Ficha    := QrVSFchRslCabFicha.Value;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsfchrslits', False, [
    'CustoKg'], [
    'SerieFch', 'Ficha'], [
    CustoKg], [
    SerieFch, Ficha], True);
    //
    QrVSFchRslIts.First;
    while not QrVSFchRslIts.Eof do
    begin
      AlteraAtual();
      //
      QrVSFchRslIts.Next;
    end;
    ReopenVSFchRslIts(GraGruX);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmVSFchRslCab.BtAtualizaClick(Sender: TObject);
begin
  BuscaDadosVSInnFicha();
end;

procedure TFmVSFchRslCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSFchRslCab.BtConfirmaClick(Sender: TObject);
var
  SerieFch, Ficha, FrmPago: Integer;
  Pecas, AreaM2, PesoKg, CustoUni, CustoAll, TribtAll, CustoLiq, CustoKg,
  TribtMP, TribtMO: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
//  SerieFch       := EdSerieFch.ValueVariant;
//  Ficha          := EdFicha.ValueVariant;
  SerieFch       := QrLoteSerieFch.Value;
  Ficha          := QrLoteFicha.Value;
  //
  Pecas          := EdPecas.ValueVariant;
  AreaM2         := 0;
  PesoKg         := EdPesoKg.ValueVariant;
  FrmPago        := RGTpCalcVal.ItemIndex;
  CustoUni       := EdCustoUni.ValueVariant;
  CustoAll       := EdCustoAll.ValueVariant;
  TribtMP        := EdTribtMP.ValueVariant;
  TribtMO        := EdTribtMO.ValueVariant;
  TribtAll       := EdTribtAll.ValueVariant;
  CustoLiq       := EdCustoLiq.ValueVariant;
  //
  if MyObjects.FIC(FrmPago < 1, RGTpCalcVal, 'Informe a "Forma de pagamento"') then
    Exit;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'vsfchrslcab', False, [
  'Pecas', 'AreaM2', 'PesoKg',
  'FrmPago', 'CustoUni', 'CustoAll',
  'TribtAll', 'CustoLiq', 'TribtMP',
  'TribtMO'
  ], [
  'SerieFch', 'Ficha'], [
  Pecas, AreaM2, PesoKg,
  FrmPago, CustoUni, CustoAll,
  TribtAll, CustoLiq, TribtMP,
  TribtMO], [
  SerieFch, Ficha], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    ReopenVSFchRslCab();
    //
    if SQLType = stIns then
    begin
      InsereTodosItens();
      ReopenVSFchRslCab();
    end else
    begin
      CustoKg := CustoUni;
      AlteraTodosItens(CustoKg);
    end;
  end;
end;

procedure TFmVSFchRslCab.FormCreate(Sender: TObject);
begin
  //
  Geral.MB_Aviso('Esta janela trabalha apenas com os dados da tabela ativa!' +
  sLineBreak + 'Dados de arquivo morto n�o s�o mostrados!');
  ImgTipo.SQLType := stLok;
  PnDados.Align   := alClient;
  PnItens.Align   := alClient;
  GBEdita.Align   := alClient;
  //DBGIMEIs.Align    := alClient;
  CriaOForm;
  FSeq := 0;
  UnDmkDAC_PF.AbreQuery(QrLotes, Dmod.MyDB);
  QrLotes.Last;
  //
  MyObjects.ConfiguraRadioGroup(RGTpCalcVal, sListaTipoCalcCouro, 6, 0);
end;

procedure TFmVSFchRslCab.SbNumeroClick(Sender: TObject);
begin
(*
  LaRegistro.Caption := GOTOy.Codigo(QrCadastro_Com_Itens_CABCodigo.Value, LaRegistro.Caption);
*)
end;

procedure TFmVSFchRslCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxWET_CURTI_058_001, [
  DModG.frxDsDono,
  frxDsLote,
  frxDsVSMovDif,
  frxDsVSFchRslCab,
  frxDsMPrima,
  frxDsFichas,
  frxDsVSPaClaIts,
  frxDsVSPaMulCab,
  frxDsVSFchRslIts
  ]);
  MyObjects.frxMostra(frxWET_CURTI_058_001, 'Resultado de Ficha');
end;

procedure TFmVSFchRslCab.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmVSFchRslCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSFchRslCab.QrArtigosAfterScroll(DataSet: TDataSet);
var
  Pecas, Peso, AreaM2, FatorMP, FatorAR, NotaMPAG: Double;
begin
  Pecas   := 1;
  if QrSumMPPecas.Value = 0 then
    Peso   := 0
  else
    Peso   := QrSumMPPesoKg.Value /	QrSumMPPecas.Value;
  if QrArtigosPecas.Value = 0 then
    AreaM2 := 0
  else
    AreaM2 := QrArtigosAreaM2.Value / QrArtigosPecas.Value;
  //
  FatorMP  := QrMPrimaFatorNota.Value;
  FatorAR  := 1;
  NotaMPAG := VS_PF.NotaCouroRibeiraApuca(Pecas, Peso, AreaM2, FatorMP, FatorAR);
  EdNotaMPAG.ValueVariant := NotaMPAG;
end;

procedure TFmVSFchRslCab.QrFichasAfterScroll(DataSet: TDataSet);
begin
  ReopenVSPaClaIts();
  //ReopenVSPaMulIts(0);
  ReopenVSPaMulCab();
end;

procedure TFmVSFchRslCab.QrFichasBeforeClose(DataSet: TDataSet);
begin
  QrVSPaClaIts.Close;
  QrVSPaMulCab.Close;
end;

procedure TFmVSFchRslCab.QrLotesAfterScroll(DataSet: TDataSet);
begin
  ReopenLote(QrLotesSerieFch.Value, QrLotesFicha.Value);
end;

procedure TFmVSFchRslCab.QrMPrimaAfterScroll(DataSet: TDataSet);
begin
  ReopenVSMovDif();
end;

procedure TFmVSFchRslCab.QrMPrimaBeforeClose(DataSet: TDataSet);
begin
  QrVSMovDif.Close;
end;

procedure TFmVSFchRslCab.QrLoteAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSFchRslCab.QrLoteAfterScroll(DataSet: TDataSet);
begin
  //ReopenVSFchGerIDs(QrLoteSerieFch.Value, QrLoteFicha.Value, 0);
  ReopenVSFchRslCab();
  ReopenMPrima(0);
  ReopenFichas();
  ReopenArtigos();
end;

procedure TFmVSFchRslCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    //if QrCadastro_Com_Itens_CABCodigo.Value <> FCabIni then
      //Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSFchRslCab.SbQueryClick(Sender: TObject);
var
  Localizou: Boolean;
  SerieFch, Ficha: Integer;
begin
  Localizou := VS_PF.MostraFormVSRMPPsq2(SerieFch, Ficha);
  //
  if Localizou then
  begin
    QrLotes.Locate('SerieFch;Ficha', VarArrayOf([SerieFch, Ficha]), []);
    //ReopenLote(SerieFch, Ficha);
  end;
end;

procedure TFmVSFchRslCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSFchRslCab.frxWET_CURTI_058_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else

end;

procedure TFmVSFchRslCab.CabInclui1Click(Sender: TObject);
begin
  GBBaseItens.Visible := True;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSFchRslCab, [PnDados],
  [PnEdita], EdPecas, ImgTipo, 'vsfchrslcab');
  //
  BuscaDadosVSInnFicha();
end;

procedure TFmVSFchRslCab.CalculaCustoMP(FromUni: Boolean);
const
  AreaM2 = 0;
  AreaP2 = 0;
var
  CustoUni, CustoAll, Pecas, PesoKg: Double;
  TpCalcVal: Integer;
begin
  Pecas     := EdPecas.ValueVariant;
  PesoKg    := EdPesoKg.ValueVariant;
  TpCalcVal := RGTpCalcVal.ItemIndex;
  CustoAll  := EdCustoAll.ValueVariant;
  if FromUni then
  begin
    CustoUni := EdCustoUni.ValueVariant;
    CustoAll := VS_PF.CalculaValorCouros(TTipoCalcCouro(TpCalcVal),
                CustoUni, Pecas, PesoKg, AreaM2, AreaP2, CustoAll);
(*
    case RGFrmPago.ItemIndex of
      1: CustoAll  := CustoUni * PesoKg;
      2: CustoAll  := CustoUni * Pecas;
      else CustoAll  := 0;
    end;
*)
    EdCustoAll.ValueVariant := CustoAll;
  end else
  begin
(*
    CustoAll := EdCustoAll.ValueVariant;
    case RgFrmPago.ItemIndex of
*)
    case RGTpCalcVal.ItemIndex of
      1:
      begin
        if Pecas > 0 then
          CustoUni := CustoAll / Pecas
        else
          CustoUni := 0;
      end;
      2:
      begin
        if PesoKg > 0 then
          CustoUni := CustoAll / PesoKg
        else
          CustoUni := 0;
      end;
      else CustoUni  := 0;
    end;
    EdCustoUni.ValueVariant := CustoUni;
  end;
end;

procedure TFmVSFchRslCab.CalculaTribtAll();
begin
  EdTribtAll.ValueVariant := EdTribtMP.ValueVariant + EdTribtMO.ValueVariant;
end;

procedure TFmVSFchRslCab.QrLoteBeforeClose(
  DataSet: TDataSet);
begin
  //QrVSFchGerIDs.Close;
  QrVSFchRslCab.Close;
  QrMPrima.Close;
  QrFichas.Close;
  QrArtigos.Close;

end;

procedure TFmVSFchRslCab.QrVSFchGerIDsAfterScroll(DataSet: TDataSet);
begin
  //ReopenVSFchGerNivs(0);
end;

procedure TFmVSFchRslCab.QrVSFchGerIDsBeforeClose(DataSet: TDataSet);
begin
  //QrVSFchGerNivs.Close;
end;

procedure TFmVSFchRslCab.QrVSFchGerNivsAfterScroll(DataSet: TDataSet);
begin
  //ReopenIMEIs(0);
end;

procedure TFmVSFchRslCab.QrVSFchGerNivsBeforeClose(DataSet: TDataSet);
begin
  //QrIMEIs.Close;
end;


procedure TFmVSFchRslCab.QrVSFchRslCabAfterScroll(DataSet: TDataSet);
begin
  ReopenVSFchRslIts(0);
end;

procedure TFmVSFchRslCab.QrVSFchRslCabBeforeClose(DataSet: TDataSet);
begin
  QrVSFchRslIts.Close;
end;

procedure TFmVSFchRslCab.QrVSFchRslItsCalcFields(DataSet: TDataSet);
begin
  if QrVSFchRslSumAreaM2.Value > 0 then
    QrVSFchRslItsPercM2.Value := QrVSFchRslItsAreaM2.Value / QrVSFchRslSumAreaM2.Value * 100
  else
    QrVSFchRslItsPercM2.Value := 0;
  //
  if QrVSFchRslSumPecas.Value > 0 then
    QrVSFchRslItsPercPc.Value := QrVSFchRslItsPecas.Value / QrVSFchRslSumPecas.Value * 100
  else
    QrVSFchRslItsPercPc.Value := 0;
end;

{
object QrVSFchGerIDs: TmySQLQuery
  Database = Dmod.MyDB
  BeforeClose = QrVSFchGerIDsBeforeClose
  AfterScroll = QrVSFchGerIDsAfterScroll
  SQL.Strings = (
    'SELECT vmi.MovimID, vmi.SerieFch, vmi.Ficha,  '
    '(vmi.Ficha + (vmi.SerieFch/10000)) FCH_SRE,  '
    'COUNT(vmi.Codigo) ITENS,  '
    'MIN(vmi.DataHora) RefFirst, MAX(vmi.DataHora) RefLast, '
    'vsf.Nome NO_SERIEFCH  '
    'FROM ' + CO_SEL_TAB_VMI + ' vmi '
    'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch '
    'WHERE vmi.SerieFch=2'
    'AND vmi.Ficha=6440'
    'GROUP BY vmi.SerieFch, vmi.Ficha, vmi.MovimID ')
  Left = 232
  Top = 197
  object QrVSFchGerIDsMovimID: TIntegerField
    FieldName = 'MovimID'
  end
  object QrVSFchGerIDsSerieFch: TIntegerField
    FieldName = 'SerieFch'
  end
  object QrVSFchGerIDsFicha: TIntegerField
    FieldName = 'Ficha'
  end
  object QrVSFchGerIDsFCH_SRE: TFloatField
    FieldName = 'FCH_SRE'
  end
  object QrVSFchGerIDsITENS: TLargeintField
    FieldName = 'ITENS'
    Required = True
  end
  object QrVSFchGerIDsRefFirst: TDateTimeField
    FieldName = 'RefFirst'
    DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
  end
  object QrVSFchGerIDsRefLast: TDateTimeField
    FieldName = 'RefLast'
    DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
  end
  object QrVSFchGerIDsNO_SERIEFCH: TWideStringField
    FieldName = 'NO_SERIEFCH'
    Size = 60
  end
  object QrVSFchGerIDsNO_MovimID: TWideStringField
    FieldName = 'NO_MovimID'
    Size = 60
  end
end
object DsVSFchGerIDs: TDataSource
  DataSet = QrVSFchGerIDs
  Left = 232
  Top = 245
end
object DsVSFchGerNivs: TDataSource
  DataSet = QrVSFchGerNivs
  Left = 316
  Top = 245
end
object QrVSFchGerNivs: TmySQLQuery
  Database = Dmod.MyDB
  BeforeClose = QrVSFchGerNivsBeforeClose
  AfterScroll = QrVSFchGerNivsAfterScroll
  SQL.Strings = (
    'SELECT vmi.MovimID, vmi.SerieFch, vmi.Ficha,  '
    '(vmi.Ficha + (vmi.SerieFch/10000)) FCH_SRE,  '
    'COUNT(vmi.Codigo) ITENS,  '
    'MIN(vmi.DataHora) RefFirst, MAX(vmi.DataHora) RefLast, '
    'vsf.Nome NO_SERIEFCH  '
    'FROM ' + CO_SEL_TAB_VMI + ' vmi '
    'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch '
    'WHERE vmi.SerieFch=2'
    'AND vmi.Ficha=6440'
    'GROUP BY vmi.SerieFch, vmi.Ficha, vmi.MovimID ')
  Left = 316
  Top = 197
  object QrVSFchGerNivsMovimID: TIntegerField
    FieldName = 'MovimID'
  end
  object QrVSFchGerNivsMovimNiv: TIntegerField
    FieldName = 'MovimNiv'
  end
  object QrVSFchGerNivsSerieFch: TIntegerField
    FieldName = 'SerieFch'
  end
  object QrVSFchGerNivsFicha: TIntegerField
    FieldName = 'Ficha'
  end
  object QrVSFchGerNivsITENS: TLargeintField
    FieldName = 'ITENS'
    Required = True
  end
  object QrVSFchGerNivsRefFirst: TDateTimeField
    FieldName = 'RefFirst'
    DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
  end
  object QrVSFchGerNivsRefLast: TDateTimeField
    FieldName = 'RefLast'
    DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
  end
  object QrVSFchGerNivsNO_MovimID: TWideStringField
    FieldName = 'NO_MovimNiv'
    Size = 60
  end
  object QrVSFchGerNivsPecas: TFloatField
    FieldName = 'Pecas'
  end
  object QrVSFchGerNivsAreaM2: TFloatField
    FieldName = 'AreaM2'
    DisplayFormat = '#,###,##0.00'
  end
end
}

(*
MySQL Error Code: (1064)
You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'ggx.GraGru1, CONCAT(gg1.Nome,
IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" "

//


/* MySQL Error Code: (1064)
You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'ggx.GraGru1, CONCAT(gg1.Nome,
IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" "

Owner: FmVSFchRslCab
 */
SELECT DISTINCT SerieFch, Ficha
FROM v s m o v i t s
WHERE Ficha <> 0
ORDER BY Ficha, SerieFch

/*QrLotes*/

*)

end.

