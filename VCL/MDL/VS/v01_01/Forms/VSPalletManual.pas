unit VSPalletManual;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables;

type
  TFmVSPalletManual = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    EdPallet: TdmkEdit;
    Label1: TLabel;
    QrVSPallet: TmySQLQuery;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FPermiteIncluir: Boolean;
    FNumNewPallet: Integer;
  end;

  var
  FmVSPalletManual: TFmVSPalletManual;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSPalletManual.BtOKClick(Sender: TObject);
var
  Pallet: Integer;
  Pallet_Txt: String;
begin
  Pallet := EdPallet.ValueVariant;
  if MyObjects.FIC(Pallet = 0, EdPallet, 'Informe o n�mero do pallet!') then
    Exit;
  //
  Pallet_Txt := Geral.FF0(Pallet);
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
  'SELECT * ',
  'FROM vspalleta ',
  'WHERE Codigo=' + Pallet_Txt,
  ' ']);
  if QrVSPallet.RecordCount > 0 then
  begin
    if Geral.MB_Pergunta('O pallet ' + Pallet_Txt + ' j� existe!' + sLineBreak
    + 'Desej� localiz�-lo?') = ID_Yes then
      VS_CRC_PF.MostraFormVSPallet1(Pallet);
    //
    Close;
    Exit;
  end else
  begin
    FPermiteIncluir := True;
    FNumNewPallet   := EdPallet.ValueVariant;
    Close;
  end;
end;

procedure TFmVSPalletManual.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPalletManual.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSPalletManual.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FPermiteIncluir := False;
  FNumNewPallet   := 0;
end;

procedure TFmVSPalletManual.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPalletManual.SpeedButton1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Novo: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(Codigo) Codigo ',
    'FROM vspalleta ',
    '']);
    Novo := Qry.FieldByName('Codigo').AsInteger;
    // Evitar Nul !! ???
    Novo := Novo + 1;
    EdPallet.ValueVariant := Novo;
  finally
    Qry.Free;
  end;
end;

end.
