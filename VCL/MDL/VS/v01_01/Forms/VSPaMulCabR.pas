unit VSPaMulCabR;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker, Variants,
  dmkDBLookupComboBox, dmkEditCB, dmkEditCalc, frxClass, frxDBSet, dmkDBGridZTO,
  UnProjGroup_Consts, UnGrl_Consts, UnAppPF, UnAppEnums;

type
  TFmVSPaMulCabR = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    QrVSPaMulCabR: TmySQLQuery;
    DsVSPaMulCabR: TDataSource;
    QrVSPaMulItsR: TmySQLQuery;
    DsVSPaMulItsR: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label52: TLabel;
    TPDataHora: TdmkEditDateTimePicker;
    EdDataHora: TdmkEdit;
    LaPecas: TLabel;
    EdPecas: TdmkEdit;
    LaAreaM2: TLabel;
    EdAreaM2: TdmkEditCalc;
    LaAreaP2: TLabel;
    EdAreaP2: TdmkEditCalc;
    LaPeso: TLabel;
    EdPesoKg: TdmkEdit;
    Label6: TLabel;
    EdMovimCod: TdmkEdit;
    Label8: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrVSPaMulCabRCodigo: TIntegerField;
    QrVSPaMulCabRMovimCod: TIntegerField;
    QrVSPaMulCabREmpresa: TIntegerField;
    QrVSPaMulCabRDataHora: TDateTimeField;
    QrVSPaMulCabRPecas: TFloatField;
    QrVSPaMulCabRPesoKg: TFloatField;
    QrVSPaMulCabRAreaM2: TFloatField;
    QrVSPaMulCabRAreaP2: TFloatField;
    QrVSPaMulCabRLk: TIntegerField;
    QrVSPaMulCabRDataCad: TDateField;
    QrVSPaMulCabRDataAlt: TDateField;
    QrVSPaMulCabRUserCad: TIntegerField;
    QrVSPaMulCabRUserAlt: TIntegerField;
    QrVSPaMulCabRAlterWeb: TSmallintField;
    QrVSPaMulCabRAtivo: TSmallintField;
    QrVSPaMulCabRNO_EMPRESA: TWideStringField;
    Label2: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    GBIts: TGroupBox;
    DGDados: TdmkDBGridZTO;
    PMImprime: TPopupMenu;
    Estoque1: TMenuItem;
    frxDsVSPaMulIts: TfrxDBDataset;
    frxDsVSPaMulCab: TfrxDBDataset;
    N2: TMenuItem;
    Fichas1: TMenuItem;
    PnPartida: TPanel;
    Panel13: TPanel;
    Panel17: TPanel;
    QrVSPaMulCabRVMI_Sorc: TIntegerField;
    QrVSMovSrc: TmySQLQuery;
    DsVSMovSrc: TDataSource;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label9: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label26: TLabel;
    Label36: TLabel;
    DBEdit5: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit21: TDBEdit;
    Label11: TLabel;
    DBEdit19: TDBEdit;
    Label22: TLabel;
    DBEdit23: TDBEdit;
    QrSrc: TmySQLQuery;
    QrSrcControle: TIntegerField;
    QrSrcSrcNivel2: TIntegerField;
    QrVSPaMulCabRCacCod: TIntegerField;
    Label12: TLabel;
    DBEdit22: TDBEdit;
    QrVSPaMulCabRVSGerArt: TIntegerField;
    ClassesGeradas1: TMenuItem;
    BtClassesGeradas: TBitBtn;
    QrPallets: TmySQLQuery;
    QrPalletsEmpresa: TIntegerField;
    QrPalletsMovimID: TIntegerField;
    QrPalletsGraGruX: TIntegerField;
    QrPalletsPallet: TIntegerField;
    QrPalletsVMI_Codi: TIntegerField;
    QrPalletsVMI_Ctrl: TIntegerField;
    QrPalletsVMI_MovimNiv: TIntegerField;
    QrPalletsTerceiro: TIntegerField;
    QrPalletsFicha: TIntegerField;
    QrPalletsDataHora: TDateTimeField;
    QrPalletsPecas: TFloatField;
    QrPalletsAreaM2: TFloatField;
    QrPalletsAreaP2: TFloatField;
    QrPalletsPesoKg: TFloatField;
    QrPalletsNO_PRD_TAM_COR: TWideStringField;
    QrPalletsNO_Pallet: TWideStringField;
    QrPalletsNO_EMPRESA: TWideStringField;
    QrPalletsNO_FORNECE: TWideStringField;
    QrPalletsGraGruY: TIntegerField;
    DsPallets: TDataSource;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TSmallintField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsTpCalcAuto: TIntegerField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsGraGruY: TIntegerField;
    DsVSMovIts: TDataSource;
    CBPallet: TdmkDBLookupComboBox;
    SbPallet: TSpeedButton;
    EdPallet: TdmkEditCB;
    LaVSRibCad: TLabel;
    CkReclasse: TCheckBox;
    EdCacCod: TdmkEdit;
    Label30: TLabel;
    EdVMI_Sorc: TdmkEdit;
    EdVSGerArt: TdmkEdit;
    Label31: TLabel;
    Label32: TLabel;
    N1: TMenuItem;
    Baixaresidual1: TMenuItem;
    DBEdit24: TDBEdit;
    Label23: TLabel;
    QrResidual: TmySQLQuery;
    DsResidual: TDataSource;
    GroupBox2: TGroupBox;
    dmkDBGridZTO1: TdmkDBGridZTO;
    DBEdit41: TDBEdit;
    Label25: TLabel;
    FichasCOMnomedoPallet1: TMenuItem;
    FichasSEMnomedoPallet1: TMenuItem;
    QrVSPaMulCabRTemIMEIMrt: TIntegerField;
    QrVSPaMulItsRCodigo: TLargeintField;
    QrVSPaMulItsRControle: TLargeintField;
    QrVSPaMulItsRMovimCod: TLargeintField;
    QrVSPaMulItsRMovimNiv: TLargeintField;
    QrVSPaMulItsRMovimTwn: TLargeintField;
    QrVSPaMulItsREmpresa: TLargeintField;
    QrVSPaMulItsRTerceiro: TLargeintField;
    QrVSPaMulItsRCliVenda: TLargeintField;
    QrVSPaMulItsRMovimID: TLargeintField;
    QrVSPaMulItsRDataHora: TDateTimeField;
    QrVSPaMulItsRPallet: TLargeintField;
    QrVSPaMulItsRGraGruX: TLargeintField;
    QrVSPaMulItsRPecas: TFloatField;
    QrVSPaMulItsRPesoKg: TFloatField;
    QrVSPaMulItsRAreaM2: TFloatField;
    QrVSPaMulItsRAreaP2: TFloatField;
    QrVSPaMulItsRValorT: TFloatField;
    QrVSPaMulItsRSrcMovID: TLargeintField;
    QrVSPaMulItsRSrcNivel1: TLargeintField;
    QrVSPaMulItsRSrcNivel2: TLargeintField;
    QrVSPaMulItsRSrcGGX: TLargeintField;
    QrVSPaMulItsRSdoVrtPeca: TFloatField;
    QrVSPaMulItsRSdoVrtPeso: TFloatField;
    QrVSPaMulItsRSdoVrtArM2: TFloatField;
    QrVSPaMulItsRObserv: TWideStringField;
    QrVSPaMulItsRSerieFch: TLargeintField;
    QrVSPaMulItsRFicha: TLargeintField;
    QrVSPaMulItsRMisturou: TLargeintField;
    QrVSPaMulItsRFornecMO: TLargeintField;
    QrVSPaMulItsRCustoMOKg: TFloatField;
    QrVSPaMulItsRCustoMOM2: TFloatField;
    QrVSPaMulItsRCustoMOTot: TFloatField;
    QrVSPaMulItsRValorMP: TFloatField;
    QrVSPaMulItsRDstMovID: TLargeintField;
    QrVSPaMulItsRDstNivel1: TLargeintField;
    QrVSPaMulItsRDstNivel2: TLargeintField;
    QrVSPaMulItsRDstGGX: TLargeintField;
    QrVSPaMulItsRQtdGerPeca: TFloatField;
    QrVSPaMulItsRQtdGerPeso: TFloatField;
    QrVSPaMulItsRQtdGerArM2: TFloatField;
    QrVSPaMulItsRQtdGerArP2: TFloatField;
    QrVSPaMulItsRQtdAntPeca: TFloatField;
    QrVSPaMulItsRQtdAntPeso: TFloatField;
    QrVSPaMulItsRQtdAntArM2: TFloatField;
    QrVSPaMulItsRQtdAntArP2: TFloatField;
    QrVSPaMulItsRNotaMPAG: TFloatField;
    QrVSPaMulItsRNO_PALLET: TWideStringField;
    QrVSPaMulItsRNO_PRD_TAM_COR: TWideStringField;
    QrVSPaMulItsRNO_TTW: TWideStringField;
    QrVSPaMulItsRID_TTW: TLargeintField;
    QrVSPaMulItsRNO_FORNECE: TWideStringField;
    QrVSPaMulItsRNO_SerieFch: TWideStringField;
    QrVSPaMulItsRReqMovEstq: TLargeintField;
    QrResidualCodigo: TLargeintField;
    QrResidualControle: TLargeintField;
    QrResidualMovimCod: TLargeintField;
    QrResidualMovimNiv: TLargeintField;
    QrResidualMovimTwn: TLargeintField;
    QrResidualEmpresa: TLargeintField;
    QrResidualTerceiro: TLargeintField;
    QrResidualCliVenda: TLargeintField;
    QrResidualMovimID: TLargeintField;
    QrResidualDataHora: TDateTimeField;
    QrResidualPallet: TLargeintField;
    QrResidualGraGruX: TLargeintField;
    QrResidualPecas: TFloatField;
    QrResidualPesoKg: TFloatField;
    QrResidualAreaM2: TFloatField;
    QrResidualAreaP2: TFloatField;
    QrResidualValorT: TFloatField;
    QrResidualSrcMovID: TLargeintField;
    QrResidualSrcNivel1: TLargeintField;
    QrResidualSrcNivel2: TLargeintField;
    QrResidualSrcGGX: TLargeintField;
    QrResidualSdoVrtPeca: TFloatField;
    QrResidualSdoVrtPeso: TFloatField;
    QrResidualSdoVrtArM2: TFloatField;
    QrResidualObserv: TWideStringField;
    QrResidualSerieFch: TLargeintField;
    QrResidualFicha: TLargeintField;
    QrResidualMisturou: TLargeintField;
    QrResidualFornecMO: TLargeintField;
    QrResidualCustoMOKg: TFloatField;
    QrResidualCustoMOM2: TFloatField;
    QrResidualCustoMOTot: TFloatField;
    QrResidualValorMP: TFloatField;
    QrResidualDstMovID: TLargeintField;
    QrResidualDstNivel1: TLargeintField;
    QrResidualDstNivel2: TLargeintField;
    QrResidualDstGGX: TLargeintField;
    QrResidualQtdGerPeca: TFloatField;
    QrResidualQtdGerPeso: TFloatField;
    QrResidualQtdGerArM2: TFloatField;
    QrResidualQtdGerArP2: TFloatField;
    QrResidualQtdAntPeca: TFloatField;
    QrResidualQtdAntPeso: TFloatField;
    QrResidualQtdAntArM2: TFloatField;
    QrResidualQtdAntArP2: TFloatField;
    QrResidualNotaMPAG: TFloatField;
    QrResidualNO_PALLET: TWideStringField;
    QrResidualNO_PRD_TAM_COR: TWideStringField;
    QrResidualNO_TTW: TWideStringField;
    QrResidualID_TTW: TLargeintField;
    QrResidualNO_FORNECE: TWideStringField;
    QrResidualNO_SerieFch: TWideStringField;
    QrResidualReqMovEstq: TLargeintField;
    QrVSMovSrcGraGru1: TLargeintField;
    QrVSMovSrcNO_EstqMovimID: TWideStringField;
    QrVSMovSrcNO_MovimID: TWideStringField;
    QrVSMovSrcNO_MovimNiv: TWideStringField;
    QrVSMovSrcStqCenLoc: TLargeintField;
    QrVSMovSrcReqMovEstq: TLargeintField;
    QrVSMovSrcCodigo: TLargeintField;
    QrVSMovSrcControle: TLargeintField;
    QrVSMovSrcMovimCod: TLargeintField;
    QrVSMovSrcMovimNiv: TLargeintField;
    QrVSMovSrcMovimTwn: TLargeintField;
    QrVSMovSrcEmpresa: TLargeintField;
    QrVSMovSrcTerceiro: TLargeintField;
    QrVSMovSrcCliVenda: TLargeintField;
    QrVSMovSrcMovimID: TLargeintField;
    QrVSMovSrcDataHora: TDateTimeField;
    QrVSMovSrcPallet: TLargeintField;
    QrVSMovSrcGraGruX: TLargeintField;
    QrVSMovSrcPecas: TFloatField;
    QrVSMovSrcPesoKg: TFloatField;
    QrVSMovSrcAreaM2: TFloatField;
    QrVSMovSrcAreaP2: TFloatField;
    QrVSMovSrcValorT: TFloatField;
    QrVSMovSrcSrcMovID: TLargeintField;
    QrVSMovSrcSrcNivel1: TLargeintField;
    QrVSMovSrcSrcNivel2: TLargeintField;
    QrVSMovSrcSrcGGX: TLargeintField;
    QrVSMovSrcSdoVrtPeca: TFloatField;
    QrVSMovSrcSdoVrtPeso: TFloatField;
    QrVSMovSrcSdoVrtArM2: TFloatField;
    QrVSMovSrcObserv: TWideStringField;
    QrVSMovSrcSerieFch: TLargeintField;
    QrVSMovSrcFicha: TLargeintField;
    QrVSMovSrcMisturou: TLargeintField;
    QrVSMovSrcFornecMO: TLargeintField;
    QrVSMovSrcCustoMOKg: TFloatField;
    QrVSMovSrcCustoMOM2: TFloatField;
    QrVSMovSrcCustoMOTot: TFloatField;
    QrVSMovSrcValorMP: TFloatField;
    QrVSMovSrcDstMovID: TLargeintField;
    QrVSMovSrcDstNivel1: TLargeintField;
    QrVSMovSrcDstNivel2: TLargeintField;
    QrVSMovSrcDstGGX: TLargeintField;
    QrVSMovSrcQtdGerPeca: TFloatField;
    QrVSMovSrcQtdGerPeso: TFloatField;
    QrVSMovSrcQtdGerArM2: TFloatField;
    QrVSMovSrcQtdGerArP2: TFloatField;
    QrVSMovSrcQtdAntPeca: TFloatField;
    QrVSMovSrcQtdAntPeso: TFloatField;
    QrVSMovSrcQtdAntArM2: TFloatField;
    QrVSMovSrcQtdAntArP2: TFloatField;
    QrVSMovSrcNotaMPAG: TFloatField;
    QrVSMovSrcNO_PALLET: TWideStringField;
    QrVSMovSrcNO_PRD_TAM_COR: TWideStringField;
    QrVSMovSrcNO_TTW: TWideStringField;
    QrVSMovSrcID_TTW: TLargeintField;
    QrVSMovSrcNO_FORNECE: TWideStringField;
    QrVSMovSrcNO_SerieFch: TWideStringField;
    QrVSMovSrcNO_LOC_CEN: TWideStringField;
    QrVSPaMulItsRStqCenLoc: TLargeintField;
    N3: TMenuItem;
    CorrigeFornecedor1: TMenuItem;
    QrPalletsClientMO: TIntegerField;
    QrVSMovSrcClientMO: TLargeintField;
    QrVSPaMulItsRCusArM2: TFloatField;
    QrVSMovSrcNFeSer: TLargeintField;
    QrVSMovSrcNFeNum: TLargeintField;
    QrVSMovSrcVSMulNFeCab: TLargeintField;
    CabAltera2: TMenuItem;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    Label28: TLabel;
    Label29: TLabel;
    DBEdit26: TDBEdit;
    DBEdit28: TDBEdit;
    Label57: TLabel;
    EdSerieRem: TdmkEdit;
    EdNFeRem: TdmkEdit;
    QrVSPaMulCabRSerieRem: TSmallintField;
    QrVSPaMulCabRNFeRem: TIntegerField;
    QrVSPaMulItsRDtCorrApo: TDateTimeField;
    QrVSPaMulItsRIxxMovIX: TLargeintField;
    QrVSPaMulItsRIxxFolha: TLargeintField;
    QrVSPaMulItsRIxxLinha: TLargeintField;
    QrDel: TmySQLQuery;
    QrDelControle: TIntegerField;
    Panel7: TPanel;
    Label49: TLabel;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    SbStqCenLoc: TSpeedButton;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrVSPaMulCabRAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrVSPaMulCabRBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrVSPaMulCabRAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrVSPaMulCabRBeforeClose(DataSet: TDataSet);
    procedure Estoque1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxWET_RECUR_010_01GetValue(const VarName: string;
      var Value: Variant);
    procedure BtClassesGeradasClick(Sender: TObject);
    procedure ClassesGeradas1Click(Sender: TObject);
    procedure CkReclasseClick(Sender: TObject);
    procedure Baixaresidual1Click(Sender: TObject);
    procedure FichasCOMnomedoPallet1Click(Sender: TObject);
    procedure FichasSEMnomedoPallet1Click(Sender: TObject);
    procedure CorrigeFornecedor1Click(Sender: TObject);
    procedure CabAltera2Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraFormVSPaMulItsR(SQLType: TSQLType);
    procedure ReopenSrc();
    procedure ReopenPallets();
    function  ZeraEstoquePalletOrigem(): Boolean;
    procedure ImprimeTodosPallets(InfoNO_PALLET: Boolean);
    procedure AtualizaFornecedor();
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure AtualizaNFeItens();
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenVSPaMulIts(Controle: Integer);
    procedure ReopenResidual(Controle: Integer);

  end;

var
  FmVSPaMulCabR: TFmVSPaMulCabR;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, VSPaMulItsR, ModuleGeral,
  Principal, UnVS_CRC_PF, AppListas, CreateVS, ModVS_CRC;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmVSPaMulCabR.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmVSPaMulCabR.MostraFormVSPaMulItsR(SQLType: TSQLType);
var
  GraGruYOri, GraGruYDst: Integer;
begin
  if DBCheck.CriaFm(TFmVSPaMulItsR, FmVSPaMulItsR, afmoNegarComAviso) then
  begin
    FmVSPaMulItsR.ImgTipo.SQLType := SQLType;
    FmVSPaMulItsR.FQrCab := QrVSPaMulCabR;
    FmVSPaMulItsR.FDsCab := DsVSPaMulCabR;
    FmVSPaMulItsR.FQrIts := QrVSPaMulItsR;
    FmVSPaMulItsR.FDataHora := QrVSPaMulCabRDataHora.Value;
    FmVSPaMulItsR.FEmpresa  := QrVSPaMulCabREmpresa.Value;
    FmVSPaMulItsR.FClientMO  := QrVSMovSrcClientMO.Value;
    FmVSPaMulItsR.ReopenOrig(QrVSPaMulCabRVMI_Sorc.Value);
    FmVSPaMulItsR.FSrcValorT := QrVSMovSrcValorT.Value;
    FmVSPaMulItsR.FSrcAreaM2 := QrVSMovSrcAreaM2.Value;
    FmVSPaMulItsR.FVSGerArt  := QrVSPaMulCabRVSGerArt.Value;
    // ver melhor
    GraGruYOri := DmModVS_CRC.ObtemGraGruYDeGraGru1(QrVSMovSrcGraGru1.Value);
    GraGruYDst := VS_CRC_PF.ObtemNomeGraGruYDestdeOrig(GraGruYOri);
    FmVSPaMulItsR.FTabela  := AppPF.ObtemNomeTabelaGraGruY(GraGruYDst);
    FmVSPaMulItsR.ReopenVSRibCla();

    if SQLType = stIns then
    begin
      //FmVSPaMulItsR.EdCPF1.ReadOnly := False
    end else
    begin
      ReopenSrc();
      FmVSPaMulItsR.EdCtrlGera.ValueVariant := QrVSPaMulItsRControle.Value;
      FmVSPaMulItsR.EdCtrlBaix.ValueVariant := QrSrcControle.Value;
      FmVSPaMulItsR.EdMovimTwn.ValueVariant := QrVSPaMulItsRMovimTwn.Value;
      //
      FmVSPaMulItsR.EdGragruX.ValueVariant    := QrVSPaMulItsRGraGruX.Value;
      FmVSPaMulItsR.CBGragruX.KeyValue        := QrVSPaMulItsRGraGruX.Value;
      //FmVSPaMulItsR.EdFornecedor.ValueVariant := QrVSPaMulItsRTerceiro.Value;
      //FmVSPaMulItsR.CBFornecedor.KeyValue     := QrVSPaMulItsRTerceiro.Value;
      //FmVSPaMulItsR.EdSerieFch.ValueVariant   := QrVSPaMulItsRSerieFch.Value;
      //FmVSPaMulItsR.CBSerieFch.KeyValue       := QrVSPaMulItsRSerieFch.Value;
      //FmVSPaMulItsR.EdFicha.ValueVariant      := QrVSPaMulItsRFicha.Value;
      FmVSPaMulItsR.EdPallet.ValueVariant     := QrVSPaMulItsRPallet.Value;
      FmVSPaMulItsR.CBPallet.KeyValue         := QrVSPaMulItsRPallet.Value;
      FmVSPaMulItsR.EdPecas.ValueVariant      := QrVSPaMulItsRPecas.Value;
      FmVSPaMulItsR.EdPesoKg.ValueVariant     := QrVSPaMulItsRPesoKg.Value;
      FmVSPaMulItsR.EdAreaM2.ValueVariant     := QrVSPaMulItsRAreaM2.Value;
      FmVSPaMulItsR.EdAreaP2.ValueVariant     := QrVSPaMulItsRAreaP2.Value;
      FmVSPaMulItsR.EdValorMP.ValueVariant    := QrVSPaMulItsRValorMP.Value;
      FmVSPaMulItsR.EdCustoMOKg.ValueVariant  := QrVSPaMulItsRCustoMOKg.Value;
      FmVSPaMulItsR.EdFornecMO.ValueVariant   := QrVSPaMulItsRFornecMO.Value;
      FmVSPaMulItsR.CBFornecMO.KeyValue       := QrVSPaMulItsRFornecMO.Value;
      FmVSPaMulItsR.EdObserv.ValueVariant     := QrVSPaMulItsRObserv.Value;
      //  Devem ser os �ltimos? Na ordem abaixo?
      FmVSPaMulItsR.EdCustoMOTot.ValueVariant := QrVSPaMulItsRCustoMOTot.Value;
      FmVSPaMulItsR.EdValorT.ValueVariant     := QrVSPaMulItsRValorT.Value;
      // Tambem no final!
      FmVSPaMulItsR.ReopenVsCacItsA(QrVSPaMulCabRCacCod.Value);
      FmVSPaMulItsR.EdCACI.ValueVariant       := FmVSPaMulItsR.QrVSCacItsAControle.Value;
      FmVSPaMulItsR.EdDigitador.ValueVariant  := FmVSPaMulItsR.QrVSCacItsADigitador.Value;
      FmVSPaMulItsR.CbDigitador.KeyValue      := FmVSPaMulItsR.QrVSCacItsADigitador.Value;
      FmVSPaMulItsR.EdRevisor.ValueVariant    := FmVSPaMulItsR.QrVSCacItsARevisor.Value;
      FmVSPaMulItsR.CBRevisor.KeyValue        := FmVSPaMulItsR.QrVSCacItsARevisor.Value;
      //
      FmVSPaMulItsR.EdStqCenLoc.ValueVariant  := QrVSPaMulItsRStqCenLoc.Value;
      FmVSPaMulItsR.CBStqCenLoc.KeyValue      := QrVSPaMulItsRStqCenLoc.Value;
      FmVSPaMulItsR.EdReqMovEstq.ValueVariant := QrVSPaMulItsRReqMovEstq.Value;
      //
      //FmVSPaMulItsR.TPData.Date               := Int(QrVSPaMulItsDataHora.Value);
      //FmVSPaMulItsR.EdHora.ValueVariant       := QrVSPaMulItsDataHora.Value;
      VS_CRC_PF.DefineDataHoraOuDtCorrApoCompos(QrVSPaMulItsRDataHora.Value,
        QrVSPaMulItsRDtCorrApo.Value, FmVSPaMulItsR.TPData, FmVSPaMulItsR.EdHora);
      DmkPF.SetArrayOfInt(FmVSPaMulItsR.FPallOnEdit, [QrVSPaMulItsRPallet.Value], True);
      FmVSPaMulItsR.RGIxxMovIX.ItemIndex        := QrVSPaMulItsRIxxMovIX.Value;
      FmVSPaMulItsR.EdIxxFolha.ValueVariant     := QrVSPaMulItsRIxxFolha.Value;
      FmVSPaMulItsR.EdIxxLinha.ValueVariant     := QrVSPaMulItsRIxxLinha.Value;
    end;
    VS_CRC_PF.ReopenVSPallet(FmVSPaMulItsR.QrVSPallet, FmVSPaMulItsR.FEmpresa,
      FmVSPaMulItsR.FClientMO, 0, '', FmVSPaMulItsR.FPallOnEdit);
    FmVSPaMulItsR.ShowModal;
    FmVSPaMulItsR.Destroy;
    //
    AtualizaFornecedor();
  end;
end;

procedure TFmVSPaMulCabR.PMCabPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrVSPaMulCabR);
  MyObjects.HabilitaMenuItemCabDelC1I2(CabExclui1, QrVSPaMulCabR, QrVSPaMulItsR, QrResidual);
  MyObjects.HabilitaMenuItemCabDel(CabAltera2, QrVSPaMulCabR, QrVSPaMulItsR);
end;

procedure TFmVSPaMulCabR.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrVSPaMulCabR);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrVSPaMulItsR);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrVSPaMulItsR);
  //
  VS_CRC_PF.HabilitaComposVSAtivo(QrVSPaMulItsRID_TTW.Value, [ItsAltera1, ItsExclui1]);
end;

procedure TFmVSPaMulCabR.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrVSPaMulCabRCodigo.Value, LaRegistro.Caption[2]);
end;

function TFmVSPaMulCabR.ZeraEstoquePalletOrigem(): Boolean;
var
  Pallet: Integer;
  BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2, BxaValorT: Double;
begin
  Result := True;
  //
  Pallet := QrVSMovSrcPallet.Value;
  if QrVSMovSrcSdoVrtPeca.Value > 0  then
  if Geral.MB_Pergunta(
  'Deseja zerar o estoque residual de ' + FloatToStr(QrVSMovSrcSdoVrtPeca.Value) +
  ' pe�as do pallet de origem ' + Geral.FF0(Pallet) + '?') =
  ID_YES then
  begin
    BxaPecas    := QrVSMovSrcSdoVrtPeca.Value;
    BxaPesoKg   := 0;
    //
    BxaAreaM2 := QrVSMovSrcSdoVrtArM2.Value;
    BxaAreaP2 := Geral.ConverteArea(QrVSMovSrcSdoVrtArM2.Value, ctM2toP2, cfQuarto);
    //
    BxaValorT := VS_CRC_PF.CalculaValorT(QrVSMovSrcPecas.Value,
      QrVSMovSrcAreaM2.Value, QrVSMovSrcPesoKg.Value, QrVSMovSrcValorT.Value,
      BxaPecas, BxaAreaM2, BxaPesoKg);
    Result := VS_CRC_PF.ZeraEstoquePalletOrigemReclas(Pallet,
      QrVSPaMulCabRCodigo.Value, QrVSPaMulCabRMovimCod.Value,
      QrVSPaMulCabREmpresa.Value, BxaPecas, BxaPesoKg, BxaAreaM2, BxaAreaP2,
      BxaValorT, QrVSPaMulCabRDataHora.Value,
      iuvpei054(*Zeramento de estoque de pallet de origem 2/3*));
  end;
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmVSPaMulCabR.DefParams;
begin
  VAR_GOTOTABELA := 'vspamulcabr';
  VAR_GOTOMYSQLTABLE := QrVSPaMulCabR;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wic.*,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA ');
  VAR_SQLx.Add('FROM vspamulcabr wic');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE wic.Codigo > 0');
  //
  VAR_SQL1.Add('AND wic.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmVSPaMulCabR.Estoque1Click(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSMovImp(False, nil, nil, -1);
end;

procedure TFmVSPaMulCabR.ImprimeTodosPallets(InfoNO_PALLET: Boolean);
const
  VSLstPalBox = '';
var
  I, N, Empresa: Integer;
  MyArr: array of Integer;
  TempTab: String;
begin
  TempTab :=
      UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp4, DModG.QrUpdPID1,
      False, 1, '_vsmovimp4_' + Self.Name);
  Empresa := QrVSPaMulCabREmpresa.Value;
  N := 0;
  QrVSPaMulItsR.First;
  while not QrVSPaMulItsR.Eof do
  begin
    N := N + 1;
    SetLength(MyArr, N);
    MyArr[N - 1] := QrVSPaMulItsRPallet.Value;
    //
    QrVSPaMulItsR.Next;
  end;
  if N > 0 then
    VS_CRC_PF.ImprimePallet_Varios(Empresa, MyArr, TempTab, InfoNO_PALLET)
  else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmVSPaMulCabR.ItsAltera1Click(Sender: TObject);
begin
  MostraFormVSPaMulItsR(stUpd);
end;

procedure TFmVSPaMulCabR.CabExclui1Click(Sender: TObject);
var
  PreClasse, Reclasse: Integer;
begin
  PreClasse := QrVSMovSrcMovimCod.Value;
  ReClasse  := QrVSPaMulCabRMovimCod.Value;
  if VS_CRC_PF.ExcluiCabecalhoReclasse(emidReclasXXMul, PreClasse, Reclasse)then
    LocCod(ReClasse, ReClasse);
end;

procedure TFmVSPaMulCabR.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmVSPaMulCabR.QueryPrincipalAfterOpen;
begin
end;

procedure TFmVSPaMulCabR.ItsExclui1Click(Sender: TObject);
var
  Codigo, MovimCod, CtrlBaix, SrcNivel2: Integer;
var
  Qry: TmySQLQuery;
  CacCod, VMI_Sorc, VMI_Dest, VMI_Baix: Integer;
  CacItsA: Int64;
begin
  ReopenSrc();
  //
  Codigo    := QrVSPaMulItsRCodigo.Value;
  MovimCod  := QrVSPaMulItsRMovimCod.Value;
  CtrlBaix  := QrSrcControle.Value;
  SrcNivel2 := QrVSPaMulCabRVMI_Sorc.Value;
  //
  CacCod   := QrVSPaMulCabRCacCod.Value;
  VMI_Sorc := QrVSPaMulCabRVMI_Sorc.Value;
  VMI_Dest := QrVSPaMulItsRControle.Value;
  VMI_Baix := QrSrcControle.Value;
  //
  if VS_CRC_PF.ExcluiControleVSMovIts(QrVSPaMulItsR, TIntegerField(QrVSPaMulItsRMovimCod),
  QrVSPaMulItsRControle.Value, CtrlBaix, SrcNivel2, False,
  Integer(TEstqMotivDel.emtdWetCurti060)) then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      // Excluir Tamb�m da CacItsA!!!
      UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM vscacitsa ',
      'WHERE CacCod=' + Geral.FF0(CacCod),
      'AND VMI_Sorc=' + Geral.FF0(VMI_Sorc),
      'AND VMI_Baix=' + Geral.FF0(VMI_Baix),
      'AND VMI_Dest=' + Geral.FF0(VMI_Dest),
      '']);
      //
      CacItsA := Geral.I64(Qry.FieldByName('Controle').AsString);
      if CacItsA <> 0 then
        UMyMod.ExcluiRegistroBig1('', 'vscacitsa', 'Controle',
          CacItsA, Dmod.MyDB);
    finally
      Qry.Free;
    end;
    VS_CRC_PF.AtualizaTotaisVSXxxCab('vspamulcabr', MovimCod);
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmVSPaMulCabR.ReopenPallets();
var
  SQL_Rcl, SQL_15: String;
begin
  if CkReclasse.Checked then
  begin
    SQL_Rcl := ' ';
    SQL_15  := ', 15';
  end else
  begin
    SQL_Rcl  := 'AND vsp.GerRclCab = 0 ';
    SQL_15   := '';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPallets, Dmod.MyDB, [
  'SELECT vmi.Empresa, vmi.MovimID, vmi.GraGruX, ',
  'vmi.Pallet, vmi.Codigo VMI_Codi, vmi.Controle VMI_Ctrl, ',
  'vmi.MovimNiv VMI_MovimNiv, vmi.Terceiro, vmi.Ficha, ',
  'MAX(DataHora) DataHora, SUM(vmi.Pecas) Pecas,   ',
  'IF(MIN(AreaM2) > 0.01, 0, SUM(vmi.AreaM2)) AreaM2,   ',
  'IF(MIN(AreaP2) > 0.01, 0, SUM(vmi.AreaP2)) AreaP2,   ',
  'IF(MIN(PesoKg) > 0.001, 0, SUM(vmi.PesoKg)) PesoKg,   ',
  'vmi.ClientMO, ',
  'CONCAT(gg1.Nome,    ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),    ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))    ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,    ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'ggx.GraGruY ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX    ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1    ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet    ',
  'LEFT JOIN entidades ent ON ent.Codigo=vmi.Empresa  ',
  'LEFT JOIN entidades frn ON frn.Codigo=vmi.Terceiro   ',
  'WHERE vmi.Pecas > 0 ',
  //'AND vmi.Empresa=-11  ',
  'AND vmi.Pallet > 0 ',
  'AND vmi.SdoVrtPeca > 0 ',
  SQL_Rcl,
  'AND MovimID IN (' + CO_ALL_CODS_CLASS_VS + SQL_15 + ') ',
  'GROUP BY vmi.Pallet, vmi.GraGruX, vmi.Empresa ',
  'ORDER BY NO_PRD_TAM_COR ',
  ' ']);
end;

procedure TFmVSPaMulCabR.ReopenResidual(Controle: Integer);
(*
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrResidual, Dmod.MyDB, [
  'SELECT vmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
  'FROM  v s m o v i t s vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSPaMulCabRMovimCod.Value),
  'AND MovimID=' + Geral.FF0(Integer(eminDestPreReclas)), //12
  'ORDER BY vmi.Controle ',
  '']);
  //
*)
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  //VS_CRC_PF.ReopenVSOpeOriIMEI(QrVSPWEOriIMEI, QrVSPWECabMovimCod.Value, Controle);
  TemIMEIMrt := QrVSPaMulCabRTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSPaMulCabRMovimCod.Value),
  'AND MovimID=' + Geral.FF0(Integer(eminDestPreReclas)), //12
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrResidual, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //
  QrResidual.Locate('Controle', Controle, []);
end;

procedure TFmVSPaMulCabR.ReopenSrc();
  procedure Abre(Tabela: String);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrSrc, Dmod.MyDb, [
    'SELECT Controle, SrcNivel2 ',
    'FROM ' + Tabela,
    'WHERE MovimCod=' + Geral.FF0(QrVSPaMulItsRMovimCod.Value),
    'AND MovimTwn=' + Geral.FF0(QrVSPaMulItsRMovimTwn.Value),
    'AND MovimNiv=' + Geral.FF0(Integer(eminSorcClass)), //1
    '']);
  end;
begin
  Abre(CO_SEL_TAB_VMI);
(*
  if QrSrc.recordCount > 0 then
    Abre(CO_TAB_VMB);
*)
end;

procedure TFmVSPaMulCabR.ReopenVSPaMulIts(Controle: Integer);
(*
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaMulItsR, Dmod.MyDB, [
  'SELECT vmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ',
  'FROM v s m o v i t s vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSPaMulCabRMovimCod.Value),
  'AND MovimNiv=' + Geral.FF0(Integer(eminDestClass)), //2
  'ORDER BY vmi.Controle ',
  '']);
  //
*)
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  TemIMEIMrt: Integer;
begin
  //VS_CRC_PF.ReopenVSOpeOriIMEI(QrVSPWEOriIMEI, QrVSPWECabMovimCod.Value, Controle);
  TemIMEIMrt := QrVSPaMulCabRTemIMEIMrt.Value;
  SQL_Flds := Geral.ATS([
  VS_CRC_PF.SQL_NO_GGX(),
  VS_CRC_PF.SQL_NO_FRN(),
  'IF(vmi.AreaM2>0, vmi.ValorT / vmi.AreaM2, 0) CusArM2, ',
  'vsf.Nome NO_SerieFch, ',
  'vsp.Nome NO_Pallet, ',
  'CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN ',
  '']);
  SQL_Left := Geral.ATS([
  VS_CRC_PF.SQL_LJ_GGX(),
  VS_CRC_PF.SQL_LJ_FRN(),
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  '']);
  SQL_Wher := Geral.ATS([
  'WHERE vmi.MovimCod=' + Geral.FF0(QrVSPaMulCabRMovimCod.Value),
  'AND vmi.MovimNiv=' + Geral.FF0(Integer(eminDestClass)), //2
  '']);
  SQL_Group := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaMulItsR, Dmod.MyDB, [
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_CRC_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Controle ',
  '']);
  //
  QrVSPaMulItsR.Locate('Controle', Controle, []);
end;


procedure TFmVSPaMulCabR.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmVSPaMulCabR.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmVSPaMulCabR.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmVSPaMulCabR.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmVSPaMulCabR.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmVSPaMulCabR.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPaMulCabR.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrVSPaMulCabRCodigo.Value;
  Close;
end;

procedure TFmVSPaMulCabR.ItsInclui1Click(Sender: TObject);
begin
  MostraFormVSPaMulItsR(stIns);
end;

procedure TFmVSPaMulCabR.CabAltera1Click(Sender: TObject);
var
  I, Codigo, MovimCod, MovimID: Integer;
  Data, Hora: TDateTime;
  DataHora: String;
begin
  Hora := QrVSPaMulCabRDataHora.Value - Int(QrVSPaMulCabRDataHora.Value);
  if not DBCheck.ObtemData(QrVSPaMulCabRDataHora.Value, Data, 0, Hora, True) then
    Exit;
  //
  DataHora := Geral.FDT(Data, 109);
  // Pr� reclasse
  Codigo   := QrVSMovSrcCodigo.Value;
  MovimCod := QrVSMovSrcMovimCod.Value;
  MovimID  := Integer(emidPreReclasse); // 15
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  CO_DATA_HORA_VMI], [
  'Codigo', 'MovimCod', 'MovimID'], [DataHora], [
  Codigo, MovimCod, MovimID], True);
  // Itens reclasse
  Codigo   := QrVSPaMulCabRCodigo.Value;
  MovimCod := QrVSPaMulCabRMovimCod.Value;
  MovimID  := Integer(emidReclasXXMul); // 24
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
  CO_DATA_HORA_VMI], [
  'Codigo', 'MovimCod', 'MovimID'], [DataHora], [
  Codigo, MovimCod, MovimID], True) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspamulcabr', False, [
    CO_DATA_HORA_GRL], [
    'Codigo'], [
    DataHora], [
    Codigo], True) then
    begin
      AtualizaNFeItens();
      LocCod(Codigo, Codigo);
    end;
  end;
end;

procedure TFmVSPaMulCabR.CabAltera2Click(Sender: TObject);
var
  Empresa: Integer;
  Habilita: Boolean;
begin
  //HabilitaEmpresa(stUpd);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrVSPaMulCabR, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vspamulcabr');
  //
  Empresa := DModG.ObtemFilialDeEntidade(QrVSPaMulCabREmpresa.Value);
  EdEmpresa.ValueVariant    := Empresa;
  CBEmpresa.KeyValue        := Empresa;
  //
  ReopenPallets();
  EdPallet.ValueVariant := QrVSMovSrcPallet.Value;
  CBPallet.KeyValue     := QrVSMovSrcPallet.Value;
  //
end;

procedure TFmVSPaMulCabR.BtClassesGeradasClick(Sender: TObject);
begin
  VS_CRC_PF.ImprimeClassIMEI(QrVSMovSrcControle.Value, QrVSPaMulCabRVSGerArt.Value,
  QrVSMovSrcMovimCod.Value(*QrVSPaMulCabRMovimCod.Value*));
end;

procedure TFmVSPaMulCabR.BtConfirmaClick(Sender: TObject);
var
  DataHora, DtHrCfgCla: String;
  VSGerArt, CacCod, Codigo, MovimCod, Empresa, ClientMO, Terceiro, VMI_Sorc: Integer;
  DataChegadaInvalida: Boolean;
  //
  SQLType: TSQLType;
  Pallet, GraGruX, GraGruY, VMIOri, SerieRem, NFeRem, StqCenLoc: Integer;
  DtHr: TDateTime;
  Qry: TmySQLQuery;
begin
  if MyObjects.FIC(EdPallet.ValueVariant = 0, EdPallet, 'Informe o Pallet!') then
    Exit;
  //
  SQLType    := ImgTipo.SQLType;
  Codigo     := EdCodigo.ValueVariant;
  MovimCod   := EdMovimCod.ValueVariant;
  VMI_Sorc   := EdVMI_Sorc.ValueVariant;
  VSGerArt   := EdVSGerArt.ValueVariant;
  Pallet     := EdPallet.ValueVariant;
  Empresa    := QrPalletsEmpresa.Value;
  ClientMO   := QrPalletsClientMO.Value;
  GraGruX    := QrPalletsGraGruX.Value;
  GraGruY    := QrPalletsGraGruY.Value;
  SerieRem   := EdSerieRem.ValueVariant;
  NFeRem     := EdNFeRem.ValueVariant;
  StqCenLoc  := EdStqCenLoc.ValueVariant;
  //
  DtHr     := Trunc(TPDataHora.Date) + EdDataHora.ValueVariant;
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Defina uma empresa!') then Exit;
  if MyObjects.FIC(Pallet = 0, EdPallet, 'Defina o Pallet de origem!') then Exit;
  if MyObjects.FIC(TPDataHora.DateTime < 2, TPDataHora, 'Defina a data!') then Exit;
  if not VS_CRC_PF.ValidaCampoNF(3, EdNFeRem, True) then Exit;
  if MyObjects.FIC(StqCenLoc = 0, EdStqCenLoc, 'Defina o centro de estoque!') then Exit;
  //

  // Desfaz Anterior em troca
  VMIOri := 0;
  if SQLType = stUpd then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * FROM ' + CO_SEL_TAB_VMI,
      'WHERE MovimID=15 ',
      'AND MovimNiv=11 ',
      'AND MovimCod=' + Geral.FF0(QrVSMovSrcMovimCod.Value),
      '']);
      //
      VMIOri := Qry.FieldByName('SrcNivel2').AsInteger;
    finally
      Qry.Free;
    end;
    //
(*
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    D E L E T E  F R O M   + CO_SEL_TAB_VMI,
    'WHERE MovimID=15 ',
    'AND MovimCod<>0 ',
    'AND MovimCod=' + Geral.FF0(QrVSMovSrcMovimCod.Value),
    '']);
*)
    UnDmkDAC_PF.AbreMySQLQuery0(QrDel, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE MovimID=15 ',
    'AND MovimCod<>0 ',
    'AND MovimCod=' + Geral.FF0(QrVSMovSrcMovimCod.Value),
    '']);
    //
    QrDel.First;
    while not QrDel.Eof do
    begin
      if VS_CRC_PF.ExcluiControleVSMovIts(QrDel, TIntegerField(QrDelControle),
      QrDel.FieldByName('Controle').AsInteger, (*CtrlBaix*)0, (*SrcNivel2*)0,
      False, Integer(TEstqMotivDel.emtdWetCurti060)) then
      begin
        //
      end;
      //
      QrDel.Next;
    end;
    // Precisa ????
    VS_CRC_PF.AtualizaSaldoIMEI(QrVSPaMulCabRVMI_Sorc.Value, True);
    // Este precisa!!!!!!
    if VMIOri <> 0 then
      VS_CRC_PF.AtualizaSaldoIMEI(VMIOri, True);
  end;

  // Prepara Pallet selecionado
  if not DmModVS_CRC.PreparaPalletParaReclassificar((*SQLType*)stIns, CkReclasse.Checked,
  Empresa, ClientMO, Codigo, MovimCod, VMI_Sorc, Pallet, GraGruX, GraGruY,
  DtHr, StqCenLoc,
  iuvpei040(*Prepara��o de pallet para reclassifica��o 2/2*),
  iuvpei041(*Baixa na prepara��o de pallet para reclassifica��o 2/2*)) then
  begin
    Geral.MB_Aviso('N�o foi poss�vel preparar o pallet ' + Geral.FF0(Pallet) +
    ' para reclassificar!');
    Exit;
  end;
  //VSGerArt       := ???;
  Codigo         := EdCodigo.ValueVariant;
  MovimCod       := EdMovimCod.ValueVariant;
  (*Empresa*)       DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DataHora       := Geral.FDT(TPDataHora.Date, 1) + ' ' +
                    Geral.FDT(EdDataHora.ValueVariant, 100);
(*
  Pecas          := EdPecas.ValueVariant;
  PesoKg         := EdPesoKg.ValueVariant;
  AreaM2         := EdAreaM2.ValueVariant;
  AreaP2         := EdAreaP2.ValueVariant;
  ValorT         := EdValorT.ValueVariant;
*)
  //
  Codigo := UMyMod.BPGS1I32('vspamulcabr', 'Codigo', '', '', tsPos, SQLType, Codigo);
  //
  if SQLType = stIns then
  begin
    CacCod := UMyMod.BPGS1I32('vscaccab', 'Codigo', '', '', tsPos, SQLType, CacCod);
    VS_CRC_PF.InsereVSCacCab(CacCod, emidReclasXXMul, Codigo);
  end else
    CacCod := QrVSPaMulCabRCacCod.Value;
  //
  MovimCod := UMyMod.BPGS1I32('vsmovcab', 'Codigo', '', '', tsPos, SQLType, MovimCod);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vspamulcabr', False, [
  'MovimCod', 'Empresa', CO_DATA_HORA_GRL,
  'VMI_Sorc', 'CacCod'
  (*, 'Pecas', 'PesoKg',
  'AreaM2', 'AreaP2', 'ValorT',*),
  'VSGerArt', 'Pallet',
  'SerieRem', 'NFeRem'], [
  'Codigo'], [
  MovimCod, Empresa, DataHora,
  VMI_Sorc, CacCod
  (*, Pecas, PesoKg,
  AreaM2, AreaP2, ValorT*),
  VSGerArt, Pallet,
  SerieRem, NFeRem], [
  Codigo], True) then
  begin
    if SQLType = stIns then
    begin
      VS_CRC_PF.InsereVSMovCab(MovimCod, emidReclasXXMul, Codigo);
      // 2015-07-10
      //VSGerArt := QrVSGerArtNewCodigo.Value;
      DtHrCfgCla := DataHora;
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerarta', False, [
      'DtHrCfgCla', 'CacCod'
      ], ['Codigo'], [
      DtHrCfgCla, CacCod
      ], [VSGerArt], True);
      // Fim 2015-07-10
    end else
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, CO_UPD_TAB_VMI, False, [
      'Empresa', CO_DATA_HORA_GRL], ['MovimCod'], [
      Empresa, DataHora], [MovimCod], True);
      //
      LocCod(Codigo, Codigo);
      //
      if EdNFeRem.ValueVariant <> 0 then
        AtualizaNFeItens();
    end;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmVSPaMulCabR.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'vspamulcabr', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'vspamulcabr', 'Codigo');
end;

procedure TFmVSPaMulCabR.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmVSPaMulCabR.AtualizaFornecedor();
var
  MovimCod: Integer;
begin
  MovimCod := QrVSMovSrcMovimCod.Value;
  DmModVS_CRC.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfMovCod, MovimCod,
  [emidPreReclasse], [eminSorcPreReclas], [eminDestPreReclas]);
  //
  MovimCod := QrVSPaMulCabRMovimCod.Value;
  DmModVS_CRC.AtualizaVSMulFrnCabNew(siNegativo, TEstqDefMulFldEMxx.edmfMovCod, MovimCod,
  [emidReclasXXMul], [eminSorcClass], [eminDestClass]);
end;

procedure TFmVSPaMulCabR.AtualizaNFeItens();
var
  SrcNivel2, NFeSer, NFeNum, VSMulNFeCab: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    NFeSer      := QrVSPaMulCabRSerieRem.Value;
    NFeNum      := QrVSPaMulCabRNFeRem.Value;
    VSMulNFeCab := 0;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SrcNivel2 ',
    'FROM ' + CO_SEL_TAB_VMI,
    'WHERE MovimCod=' + Geral.FF0(QrVSMovSrcMovimCod.Value),
    'AND MovimID=' + Geral.FF0(Integer(emidPreReclasse)),
    'AND MovimNiv=' + Geral.FF0(Integer(eminSorcPreReclas)),
    '']);
    SrcNivel2 := Qry.FieldByName('SrcNivel2').AsInteger;
    if SrcNivel2 <> 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT NFeSer, NFeNum, VSMulNFeCab ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE Controle=' + Geral.FF0(SrcNivel2),
      '']);
      if (NFeSer = 0) and (NFeNum = 0) then
      begin
        NFeSer      := Qry.FieldByName('NFeSer').AsInteger;
        NFeNum      := Qry.FieldByName('NFeNum').AsInteger;
      end;
      VSMulNFeCab := Qry.FieldByName('VSMulNFeCab').AsInteger;
    end;
    VS_CRC_PF.AtualizaSerieNFeVMC(QrVSPaMulCabRMovimCod.Value, NFeSer, NFeNum,
      VSMulNFeCab);
    VS_CRC_PF.AtualizaSerieNFeVMI(QrVSPaMulCabRVMI_Sorc.Value, NFeSer, NFeNum,
      VSMulNFeCab);
    //
    QrVSPaMulItsR.First;
    while not QrVSPaMulItsR.Eof do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE SrcNivel2=' + Geral.FF0(QrVSPaMulItsRControle.Value),
      '']);
      Qry.First;
      while not Qry.Eof do
      begin
        //if (Qry.FieldByName('NFeNum').AsInteger = 0)
        //or (Qry.FieldByName('VSMulNFeCab').AsInteger = 0) then
          VS_CRC_PF.AtualizaSerieNFeVMC(QrVSPaMulCabRMovimCod.Value, NFeSer,
          NFeNum, VSMulNFeCab);
        //
        Qry.Next;
      end;
      //
      QrVSPaMulItsR.Next;
    end;
  finally
    Qry.free;
  end;
end;

procedure TFmVSPaMulCabR.Baixaresidual1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrVSPaMulCabRCodigo.Value;
  //
  if ZeraEstoquePalletOrigem() then
    LocCod(Codigo, Codigo);
end;

procedure TFmVSPaMulCabR.BtCabClick(Sender: TObject);
begin
  VS_CRC_PF.HabilitaMenuInsOuAllVSAberto(QrVSPaMulCabR, QrVSPaMulCabRDataHora.Value,
  BtCab, PMCab, [CabInclui1, CorrigeFornecedor1]);
  //MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmVSPaMulCabR.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //GBEdita.Align := alClient;
  GBIts.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //UnDmkDAC_PF.AbreQuery(QrStqCenLoc, Dmod.MyDB);
  VS_CRC_PF.ReopenStqCenLoc(QrStqCenLoc, CO_STQ_CEN_CAD_NULL, 0, TEstqMovimID.emidAjuste);
end;

procedure TFmVSPaMulCabR.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrVSPaMulCabRCodigo.Value, LaRegistro.Caption);
end;

procedure TFmVSPaMulCabR.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmVSPaMulCabR.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSPaMulCabR.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrVSPaMulCabRCodigo.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSPaMulCabR.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmVSPaMulCabR.QrVSPaMulCabRAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmVSPaMulCabR.QrVSPaMulCabRAfterScroll(DataSet: TDataSet);
const
  Campo = 'Controle';
  CtrlLoc = 0;
var
  IMEI, TemIMEIMrt: Integer;
begin
  IMEI := QrVSPaMulCabRVMI_Sorc.Value;
  TemIMEIMrt := QrVSPaMulCabRTemIMEIMrt.Value;
  //
  ReopenVSPaMulIts(0);
  ReopenResidual(0);
  VS_CRC_PF.ReopenVSMovXXX(QrVSMovSrc, Campo, IMEI, TemIMEIMrt, CtrlLoc);
end;

procedure TFmVSPaMulCabR.FichasCOMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(True);
end;

procedure TFmVSPaMulCabR.FichasSEMnomedoPallet1Click(Sender: TObject);
begin
  ImprimeTodosPallets(False);
end;

procedure TFmVSPaMulCabR.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrVSPaMulCabRCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmVSPaMulCabR.SbQueryClick(Sender: TObject);
begin
  (*
  LocCod(QrVSPaMulCabRCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'vspamulcabr', Dmod.MyDB, CO_VAZIO));
  *)
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmVSPaMulCabR.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPaMulCabR.frxWET_RECUR_010_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
end;

procedure TFmVSPaMulCabR.CabInclui1Click(Sender: TObject);
const
  Ficha = 0;
  Controle = 0;
var
  Agora: TDateTime;
begin
  ReopenPallets();
  EdPallet.ValueVariant := 0;
  CBPallet.KeyValue := Null;
  //
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrVSPaMulCabR, [PnDados],
  [PnEdita], EdEmpresa, ImgTipo, 'vspamulcabr');
  //
  DModG.SelecionaEmpresaSeUnica(EdEmpresa, CBEmpresa);
  Agora := DModG.ObtemAgora();
  TPDataHora.Date := Agora;
  EdDataHora.ValueVariant := Agora;
  if EdEmpresa.ValueVariant > 0 then
  begin
    if TPDataHora.CanFocus then
    TPDataHora.SetFocus;
  end;
end;

procedure TFmVSPaMulCabR.CkReclasseClick(Sender: TObject);
begin
  ReopenPallets();
end;

procedure TFmVSPaMulCabR.ClassesGeradas1Click(Sender: TObject);
begin
  VS_CRC_PF.ImprimeClassIMEI(QrVSMovSrcControle.Value, QrVSPaMulCabRVSGerArt.Value,
  QrVSMovSrcMovimCod.Value(*QrVSPaMulCabRMovimCod.Value*));
end;

procedure TFmVSPaMulCabR.CorrigeFornecedor1Click(Sender: TObject);
begin
  AtualizaFornecedor();
end;

procedure TFmVSPaMulCabR.QrVSPaMulCabRBeforeClose(
  DataSet: TDataSet);
begin
  QrVSPaMulItsR.Close;
  QrResidual.Close;
  QrVSMovSrc.Close;
end;

procedure TFmVSPaMulCabR.QrVSPaMulCabRBeforeOpen(DataSet: TDataSet);
begin
  QrVSPaMulCabRCodigo.DisplayFormat := FFormatFloat;
end;

end.

