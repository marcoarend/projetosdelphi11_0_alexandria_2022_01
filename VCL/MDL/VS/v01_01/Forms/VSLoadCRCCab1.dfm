object FmVSLoadCRCCab1: TFmVSLoadCRCCab1
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-208 :: Importa'#231#227'o de Dados do ClaReCo'
  ClientHeight = 643
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 125
    Width = 784
    Height = 518
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label53: TLabel
        Left = 76
        Top = 16
        Width = 58
        Height = 13
        Caption = 'Data / hora:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label5: TLabel
        Left = 228
        Top = 16
        Width = 82
        Height = 13
        Caption = 'Server ID origem:'
        Enabled = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPData: TdmkEditDateTimePicker
        Left = 76
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805800
        Time = 0.639644131944805800
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DataHora'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdHora: TdmkEdit
        Left = 184
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 2
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryName = 'QrVSGerArt'
        QryCampo = 'DataHora'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdOriServrID: TdmkEdit
        Left = 228
        Top = 32
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'OriServrID'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 455
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtImporta: TBitBtn
        Tag = 19
        Left = 136
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = 'I&mporta'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 125
    Width = 784
    Height = 518
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 58
        Height = 13
        Caption = 'Data / hora:'
      end
      object Label3: TLabel
        Left = 316
        Top = 16
        Width = 49
        Height = 13
        Caption = 'OriServrID'
        FocusControl = DBEdit2
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSLoadCRCCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 238
        Height = 21
        DataField = 'DataHora'
        DataSource = DsVSLoadCRCCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 316
        Top = 32
        Width = 134
        Height = 21
        DataField = 'OriServrID'
        DataSource = DsVSLoadCRCCab
        TabOrder = 2
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 454
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 65
      Width = 784
      Height = 320
      ActivePage = TabSheet3
      Align = alTop
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Importados ou atrelados'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DGDados: TDBGrid
          Left = 0
          Top = 0
          Width = 776
          Height = 292
          Align = alClient
          DataSource = DsVSLoadCRCTbs
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tabela'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Registros'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Impota'#231#227'o'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GBImportacao: TGroupBox
          Left = 0
          Top = 0
          Width = 776
          Height = 292
          Align = alClient
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          object Panel6: TPanel
            Left = 322
            Top = 15
            Width = 452
            Height = 275
            Align = alClient
            TabOrder = 0
            object Splitter1: TSplitter
              Left = 1
              Top = 191
              Width = 450
              Height = 5
              Cursor = crVSplit
              Align = alBottom
              ExplicitTop = 193
            end
            object Memo1: TMemo
              Left = 1
              Top = 61
              Width = 450
              Height = 60
              Align = alTop
              ReadOnly = True
              TabOrder = 0
            end
            object Memo2: TMemo
              Left = 1
              Top = 1
              Width = 450
              Height = 60
              Align = alTop
              ReadOnly = True
              TabOrder = 1
            end
            object DBGSel: TDBGrid
              Left = 1
              Top = 121
              Width = 450
              Height = 70
              Align = alClient
              DataSource = DsSel
              TabOrder = 2
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
            object GroupBox1: TGroupBox
              Left = 1
              Top = 196
              Width = 450
              Height = 78
              Align = alBottom
              Caption = ' Item j'#225' importado do item n'#227'o re-importado: '
              TabOrder = 3
              object PnReInn: TPanel
                Left = 2
                Top = 15
                Width = 446
                Height = 61
                Align = alClient
                BevelOuter = bvNone
                Caption = 
                  'Itens > Mostra item j'#225' importado do item da tabela n'#227'o re-import' +
                  'ado.'
                ParentBackground = False
                TabOrder = 0
                object DBGReInn: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 446
                  Height = 61
                  Align = alClient
                  DataSource = DsItReInn
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Visible = False
                end
              end
            end
          end
          object dmkDBGridZTO1: TdmkDBGridZTO
            Left = 2
            Top = 15
            Width = 320
            Height = 275
            Align = alLeft
            DataSource = DsItensPorTab
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID Psq'
                Width = 46
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Tabela'
                Width = 167
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Itens'
                Width = 56
                Visible = True
              end>
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Avisos e errros'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 776
          Height = 292
          Align = alClient
          DataSource = DsVSLoadCRCCabWrn
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Linha'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Visible = True
            end>
        end
      end
    end
    object Panel7: TPanel
      Left = 0
      Top = 408
      Width = 784
      Height = 46
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object RGPalletsAtrelados: TRadioGroup
        Left = 0
        Top = 0
        Width = 784
        Height = 46
        Align = alClient
        Caption = ' Pallets j'#225' cadastrados: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'A definir'
          'Ver antes de agir (um a um dos j'#225' cadastrados)'
          'Apenas atrelar (todos j'#225' cadastrados)')
        TabOrder = 0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 418
        Height = 32
        Caption = 'Importa'#231#227'o de Dados do ClaReCo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 418
        Height = 32
        Caption = 'Importa'#231#227'o de Dados do ClaReCo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 418
        Height = 32
        Caption = 'Importa'#231#227'o de Dados do ClaReCo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 73
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 56
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 22
        Width = 780
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
      object PB2: TProgressBar
        Left = 0
        Top = 39
        Width = 780
        Height = 17
        Align = alBottom
        TabOrder = 1
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrVSLoadCRCCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSLoadCRCCabBeforeClose
    AfterScroll = QrVSLoadCRCCabAfterScroll
    SQL.Strings = (
      'SELECT * FROM vsloadcrccab')
    Left = 92
    Top = 233
    object QrVSLoadCRCCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSLoadCRCCabDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSLoadCRCCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSLoadCRCCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSLoadCRCCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSLoadCRCCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSLoadCRCCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSLoadCRCCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSLoadCRCCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSLoadCRCCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSLoadCRCCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSLoadCRCCabOriServrID: TIntegerField
      FieldName = 'OriServrID'
    end
  end
  object DsVSLoadCRCCab: TDataSource
    DataSet = QrVSLoadCRCCab
    Left = 92
    Top = 277
  end
  object QrVSLoadCRCTbs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM vsloadcrcits')
    Left = 204
    Top = 233
    object QrVSLoadCRCTbsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSLoadCRCTbsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSLoadCRCTbsTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 60
    end
    object QrVSLoadCRCTbsRegistros: TIntegerField
      FieldName = 'Registros'
    end
  end
  object DsVSLoadCRCTbs: TDataSource
    DataSet = QrVSLoadCRCTbs
    Left = 200
    Top = 277
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 472
    Top = 484
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      Visible = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      Visible = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      Visible = False
      OnClick = ItsExclui1Click
    end
    object ItsImporta1: TMenuItem
      Caption = 'Importa'
      OnClick = ItsImporta1Click
    end
    object Pesquisanovosdados1: TMenuItem
      Caption = '&Pesquisa novos dados'
      OnClick = Pesquisanovosdados1Click
    end
    object Mostraitensdatabela1: TMenuItem
      Caption = 'Mostra itens n'#227'o importados da tabela'
      OnClick = Mostraitensdatabela1Click
    end
    object Mostraitemjimportadodoitemdatabelanoreimportado1: TMenuItem
      Caption = 'Mostra item j'#225' importado do item da tabela n'#227'o re-importado'
      OnClick = Mostraitemjimportadodoitemdatabelanoreimportado1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 260
    Top = 512
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object Corrigir011: TMenuItem
      Caption = 'Corrigir 0 > 1'
      OnClick = Corrigir011Click
    end
  end
  object QrItensPorTab: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrItensPorTabAfterOpen
    BeforeClose = QrItensPorTabBeforeClose
    AfterScroll = QrItensPorTabAfterScroll
    Left = 492
    Top = 260
    object QrItensPorTabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrItensPorTabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrItensPorTabItens: TLargeintField
      FieldName = 'Itens'
    end
    object QrItensPorTabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsItensPorTab: TDataSource
    DataSet = QrItensPorTab
    Left = 492
    Top = 308
  end
  object QrTabelas: TMySQLQuery
    Database = Dmod.MyDB
    Left = 596
    Top = 104
  end
  object QrRegistros: TMySQLQuery
    Database = Dmod.MyDB
    Left = 676
    Top = 112
  end
  object QrIdx: TMySQLQuery
    Database = Dmod.MyDB
    Left = 636
    Top = 340
  end
  object QrSel: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrSelBeforeClose
    AfterScroll = QrSelAfterScroll
    Left = 356
    Top = 356
  end
  object DsSel: TDataSource
    DataSet = QrSel
    Left = 356
    Top = 404
  end
  object QrPsqCross: TMySQLQuery
    Database = Dmod.MyDB
    Left = 568
    Top = 448
  end
  object QrPNI1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 100
    Top = 464
  end
  object QrPNI2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 100
    Top = 516
  end
  object Query1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 40
    Top = 388
  end
  object Query2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 40
    Top = 436
  end
  object QrVSLoadCRCWrn: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSLoadCRCWrnAfterOpen
    SQL.Strings = (
      'SELECT * FROM vsloadcrcits')
    Left = 312
    Top = 233
    object QrVSLoadCRCWrnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSLoadCRCWrnControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSLoadCRCWrnLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrVSLoadCRCWrnNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrVSLoadCRCWrnLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSLoadCRCWrnDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSLoadCRCWrnDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSLoadCRCWrnUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSLoadCRCWrnUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSLoadCRCWrnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSLoadCRCWrnAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object QrVSLoadCRCWrnAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object QrVSLoadCRCWrnAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSLoadCRCCabWrn: TDataSource
    DataSet = QrVSLoadCRCWrn
    Left = 312
    Top = 277
  end
  object QrPsqTbs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM vsloadcrctbs '
      'WHERE LOWER(Tabela)=LOWER("VSMov'#205'TS") ')
    Left = 636
    Top = 248
    object QrPsqTbsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPsqTbsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPsqTbsTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 60
    end
    object QrPsqTbsRegistros: TIntegerField
      FieldName = 'Registros'
    end
  end
  object QrItReInn: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrItReInnAfterOpen
    BeforeClose = QrItReInnBeforeClose
    Left = 432
    Top = 356
  end
  object DsItReInn: TDataSource
    DataSet = QrItReInn
    Left = 432
    Top = 404
  end
end
