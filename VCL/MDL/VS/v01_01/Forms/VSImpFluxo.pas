unit VSImpFluxo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass, frxDBSet, Data.DB,
  mySQLDbTables, Vcl.Grids, Vcl.DBGrids,
  UnInternalConsts, Vcl.ComCtrls, Vcl.StdCtrls, dmkEdit, Vcl.ExtCtrls,
  Vcl.Buttons, dmkDBGridZTO, Vcl.Menus, UnProjGroup_Consts, UnAppEnums;

type
  TFmVSImpFluxo = class(TForm)
    frxWET_CURTI_018_13_A: TfrxReport;
    Qr13VSSeqIts: TmySQLQuery;
    Qr13VSSeqItsCodigo: TIntegerField;
    Qr13VSSeqItsControle: TIntegerField;
    Qr13VSSeqItsMovimCod: TIntegerField;
    Qr13VSSeqItsMovimNiv: TIntegerField;
    Qr13VSSeqItsMovimTwn: TIntegerField;
    Qr13VSSeqItsEmpresa: TIntegerField;
    Qr13VSSeqItsTerceiro: TIntegerField;
    Qr13VSSeqItsCliVenda: TIntegerField;
    Qr13VSSeqItsMovimID: TIntegerField;
    Qr13VSSeqItsLnkNivXtr1: TIntegerField;
    Qr13VSSeqItsLnkNivXtr2: TIntegerField;
    Qr13VSSeqItsDataHora: TDateTimeField;
    Qr13VSSeqItsPallet: TIntegerField;
    Qr13VSSeqItsGraGruX: TIntegerField;
    Qr13VSSeqItsPecas: TFloatField;
    Qr13VSSeqItsPesoKg: TFloatField;
    Qr13VSSeqItsAreaM2: TFloatField;
    Qr13VSSeqItsAreaP2: TFloatField;
    Qr13VSSeqItsValorT: TFloatField;
    Qr13VSSeqItsSrcMovID: TIntegerField;
    Qr13VSSeqItsSrcNivel1: TIntegerField;
    Qr13VSSeqItsSrcNivel2: TIntegerField;
    Qr13VSSeqItsSrcGGX: TIntegerField;
    Qr13VSSeqItsSdoVrtPeca: TFloatField;
    Qr13VSSeqItsSdoVrtPeso: TFloatField;
    Qr13VSSeqItsSdoVrtArM2: TFloatField;
    Qr13VSSeqItsObserv: TWideStringField;
    Qr13VSSeqItsSerieFch: TIntegerField;
    Qr13VSSeqItsFicha: TIntegerField;
    Qr13VSSeqItsMisturou: TSmallintField;
    Qr13VSSeqItsFornecMO: TIntegerField;
    Qr13VSSeqItsCustoMOKg: TFloatField;
    Qr13VSSeqItsCustoMOTot: TFloatField;
    Qr13VSSeqItsValorMP: TFloatField;
    Qr13VSSeqItsDstMovID: TIntegerField;
    Qr13VSSeqItsDstNivel1: TIntegerField;
    Qr13VSSeqItsDstNivel2: TIntegerField;
    Qr13VSSeqItsDstGGX: TIntegerField;
    Qr13VSSeqItsQtdGerPeca: TFloatField;
    Qr13VSSeqItsQtdGerPeso: TFloatField;
    Qr13VSSeqItsQtdGerArM2: TFloatField;
    Qr13VSSeqItsQtdGerArP2: TFloatField;
    Qr13VSSeqItsQtdAntPeca: TFloatField;
    Qr13VSSeqItsQtdAntPeso: TFloatField;
    Qr13VSSeqItsQtdAntArM2: TFloatField;
    Qr13VSSeqItsQtdAntArP2: TFloatField;
    Qr13VSSeqItsAptoUso: TSmallintField;
    Qr13VSSeqItsNotaMPAG: TFloatField;
    Qr13VSSeqItsMarca: TWideStringField;
    Qr13VSSeqItsTpCalcAuto: TIntegerField;
    Qr13VSSeqItsInteiros: TFloatField;
    Qr13VSSeqItsAcumInteir: TFloatField;
    Qr13VSSeqItsSequencia: TIntegerField;
    Qr13VSSeqItsAtivo: TSmallintField;
    Qr13VSSeqItsNO_MovimID: TWideStringField;
    Qr13VSSeqItsNO_MovimNiv: TWideStringField;
    Qr13VSSeqItsNO_PRD_TAM_COR: TWideStringField;
    Qr13VSSeqItsNO_Pallet: TWideStringField;
    Qr13VSSeqItsVSP_CliStat: TLargeintField;
    Qr13VSSeqItsVSP_STATUS: TLargeintField;
    Qr13VSSeqItsNO_FORNECE: TWideStringField;
    Qr13VSSeqItsNO_CliStat: TWideStringField;
    Qr13VSSeqItsNO_EMPRESA: TWideStringField;
    Qr13VSSeqItsNO_STATUS: TWideStringField;
    Qr13VSSeqItsNO_SerieFch: TWideStringField;
    Ds13VSSeqIts: TDataSource;
    frxDs13VSSeqIts: TfrxDBDataset;
    Qr13VSSeqItsNO_SERIE_FICHA: TWideStringField;
    Qr13VSSeqItsGraGruY: TIntegerField;
    Qr13VSSeqItsIMEI_Src: TIntegerField;
    Qr13VSSeqItsIMEI_Ord: TIntegerField;
    Qr13VSSeqItsData: TDateField;
    Qr13Sintetico: TmySQLQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBG13Sintetico: TdmkDBGridZTO;
    Ds13Sintetico: TDataSource;
    Qr13SinteticoGraGruY: TIntegerField;
    Qr13SinteticoInteiros: TFloatField;
    Qr13SinteticoSerieFch: TIntegerField;
    Qr13SinteticoFicha: TIntegerField;
    Qr13SinteticoIMEI_Src: TIntegerField;
    Qr13SinteticoPallet: TIntegerField;
    Qr13SinteticoAtivo: TSmallintField;
    Qr13SinteticoNO_SERFCH: TWideStringField;
    Qr13SinteticoNO_GGY: TWideStringField;
    Panel1: TPanel;
    Label45: TLabel;
    Ed13_MaxInteiros: TdmkEdit;
    Qr13SinteticoLastDtHr: TDateTimeField;
    Qr13VSSeqItsOperacao: TIntegerField;
    Qr13SinteticoOperacao: TIntegerField;
    frxWET_CURTI_018_13_B: TfrxReport;
    frxDs13Sintetico: TfrxDBDataset;
    Qr13SinteticoLastDtHr_TXT: TWideStringField;
    PMSintetico: TPopupMenu;
    Irparajaneladegerenciamentodeficha1: TMenuItem;
    IrparajaneladegerenciamentodePallet1: TMenuItem;
    IrparajaneladegerenciamentodeIMEI1: TMenuItem;
    IrparajaneladeentradaInNatura1: TMenuItem;
    IrparajaneladeGeraodeArtigo1: TMenuItem;
    IrparajaneladeOrdemdeOperao1: TMenuItem;
    IrparajaneladeCassificaoMontagem1: TMenuItem;
    IrparajenaladereclassificaoMontagem1: TMenuItem;
    IrparajanaladeGerenciamentodeReclassificaoDesmontagem1: TMenuItem;
    QrErrMov1: TmySQLQuery;
    DsErrMov1: TDataSource;
    frxDsErrMov1: TfrxDBDataset;
    TabSheet3: TTabSheet;
    DBGDsErrMov1: TdmkDBGridZTO;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtBaixaExtra: TBitBtn;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    procedure frxWET_CURTI_018_13_AGetValue(const VarName: string;
      var Value: Variant);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtBaixaExtraClick(Sender: TObject);
    procedure Qr13SinteticoLastDtHrGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure Qr13SinteticoCalcFields(DataSet: TDataSet);
    procedure Irparajaneladegerenciamentodeficha1Click(Sender: TObject);
    procedure PMSinteticoPopup(Sender: TObject);
    procedure IrparajaneladegerenciamentodePallet1Click(Sender: TObject);
    procedure IrparajaneladeentradaInNatura1Click(Sender: TObject);
    procedure IrparajaneladegerenciamentodeIMEI1Click(Sender: TObject);
    procedure IrparajaneladeGeraodeArtigo1Click(Sender: TObject);
    procedure IrparajaneladeOrdemdeOperao1Click(Sender: TObject);
    procedure IrparajaneladeCassificaoMontagem1Click(Sender: TObject);
    procedure IrparajenaladereclassificaoMontagem1Click(Sender: TObject);
    procedure IrparajanaladeGerenciamentodeReclassificaoDesmontagem1Click(
      Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FVSFluxIncon: String;
    //
    procedure ImprimePreparado();
    procedure MostraJanelaMontagem(Tabela: String);
    procedure SetaTodosItens13(Ativo: Boolean);
    //procedure BaixaIMEIAtual();
  public
    { Public declarations }
    TP13DataIni_Date, TP13DataFim_Date: TDateTime;
    Ck13DataIni_Checked, Ck13DataFim_Checked, Ck13DataIni_Visible,
    Ck13DataIni_Enabled: Boolean;
    Ed13_Pallet_ValueVariant, Ed13_IMEI_ValueVariant, Ed13GraGruX_ValueVariant,
    EdEmpresa_ValueVariant: Variant;
    RG13TipoMov_ItemIndex, RG13_AnaliSinte_ItemIndex, RG13_GragruY_ItemIndex,
    Ed13SerieFch_ValueVariant, Ed13Ficha_ValueVariant: Integer;
    FRG13_AnaliSinte: TRadioGroup;
    FPB1: TProgressBar;
    FLaAviso1, FLaAviso2: TLabel;
    FBtImprime: TBitBtn;
    CBEmpresa_Text: String;
    //
    FVSSeqIts: String;
    //
    procedure ImprimeFluxo();
    procedure PesquisaFluxo();
    procedure ReabreSintetico();
  end;

var
  FmVSImpFluxo: TFmVSImpFluxo;

implementation

uses UnMyObjects, UnDmkProcFunc, ModuleGeral, UnDmkEnums, AppListas, dmkGeral,
  DmkDAC_PF, MyDBCheck, VSExBCab, UnVS_PF, Module, UMySQLModule, ModVS,
  CreateVS;

{$R *.dfm}

const
  Ordens: array[0..6] of String = (
  (*0000*)'DataHora, Controle',
  (*1024*)'Ficha, NO_SerieFch, DataHora, Controle',
  (*2048*)'IMEI_Src, IMEI_Ord, DataHora, Controle',
  (*3072*)'Pallet, IMEI_Src, IMEI_Ord, DataHora, Controle',
  (*4096*)'Operacao, IMEI_Ord, DataHora, Controle',
  (*5120*)'IMEI_Src, IMEI_Ord, DataHora, Controle',
  (*6144*)'IMEI_Src, IMEI_Ord, DataHora, Controle');


{
procedure TFmVSImpFluxo.BaixaIMEIAtual();
const
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  CustoMOKg  = 0;
  CustoMOTot = 0;
  ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  FornecMO   = 0;
  NotaMPAG   = 0;
  //
  TpCalcAuto = -1;
  //
  EdFicha  = nil;
  EdPallet = nil;
  ExigeFornecedor = False;
var
  DataHora, Observ, Marca: String;
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro, SerieFch,
  Ficha, SrcNivel1, SrcNivel2, (*Misturou,*) GraGruY, SrcGGX: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  SrcMovID, MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  ExigeAreaouPeca: Boolean;
begin
  SrcMovID       := TEstqmovimID(QrIMEIsMovimID.Value);
  SrcNivel1      := QrIMEIsCodigo.Value;
  SrcNivel2      := QrIMEIsControle.Value;
  SrcGGX         := QrIMEIsGraGruX.Value;
  //
  Codigo         := FCodigo;
  Controle       := 0;
  MovimCod       := FMovimCod;
  Empresa        := FEmpresa;
  Terceiro       := QrIMEIsTerceiro.Value;
  //DataHora       := Geral.FDT(DModG.ObtemAgora(), 109);
  MovimID        := emidExtraBxa;
  MovimNiv       := eminSemNiv;
  Pallet         := QrIMEIsPallet.Value;
  GraGruX        := QrIMEIsGraGruX.Value;
  Pecas          := -QrIMEIsSdoVrtPeca.Value;
  PesoKg         := QrIMEIsSdoVrtPeso.Value;
  AreaM2         := -QrIMEIsSdoVrtArM2.Value;
  AreaP2         := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
  ValorT         := 0;
  Observ         := '';
  //
  SerieFch       := QrIMEIsSerieFch.Value;
  Ficha          := QrIMEIsFicha.Value;
  Marca          := QrIMEIsMarca.Value;
  //Misturou       := QrIMEIsMisturou.Value;
  //
  GraGruY        := QrIMEIsGraGruY.Value;
  //
(*
  ExigeAreaouPeca :=
    (QrIMEIsSdoVrtPeso.Value > 0) or (QrIMEIsSdoVrtArM2.Value > 0);
  //
  if VS_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas,
  AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas,
  EdAreaM2, EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca) then
    Exit;
*)
  //
  //Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if not VS_PF.ObtemControleIMEI(ImgTipo.SQLType, Controle, EdSenha.Text) then
  begin
    Close;
    Exit;
  end;
  //if Trim(EdSenha.Text) <> '' then
    DataHora := Geral.FDT(TPDataSenha_Date, 1) + ' ' + EdHoraSenha.Text;
(*
  if Dmod.InsUpdVSMovIts_(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Terceiro,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle) then
*)
  if VS_PF.InsUpdVSMovIts_(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
  CustoMOKg, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca,
  QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca, TpCalcAuto) then
  begin
    VS_PF.AtualizaTotaisVSXxxCab('vsexbcab', MovimCod);
    VS_PF.AtualizaSaldoIMEI(SrcNivel2, False);
    FControle := Controle;
  end;
end;
}

procedure TFmVSImpFluxo.BtBaixaExtraClick(Sender: TObject);
var
  //Codigo, MovimCod:
  I: Integer;
  Continua: Boolean;
begin
  case PageControl1.ActivePageIndex of
    1:
    begin
      MyObjects.frxDefineDataSets(frxWET_CURTI_018_13_B, [
      DModG.frxDsDono,
      frxDs13Sintetico
      ]);
      MyObjects.frxMostra(frxWET_CURTI_018_13_B, 'Sobra / Falta de Couros em Fluxo');
    end;
    2: ImprimeFluxo();
    else Geral.MB_Info('Guia sem impress�o implementada!');
  end;
end;

procedure TFmVSImpFluxo.BtNenhumClick(Sender: TObject);
begin
  SetaTodosItens13(False);
end;

procedure TFmVSImpFluxo.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSImpFluxo.BtTudoClick(Sender: TObject);
begin
  SetaTodosItens13(True);
end;

procedure TFmVSImpFluxo.FormCreate(Sender: TObject);
begin
  Geral.MB_Aviso('Deprecado. Caso usar implementar transf. local!');
end;

procedure TFmVSImpFluxo.frxWET_CURTI_018_13_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa_Text, EdEmpresa_ValueVariant, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else

end;

procedure TFmVSImpFluxo.ImprimeFluxo();
var
  Ordem, ATT_MovimID, ATT_MovimNiv, SQL_GraGruX, SQL_SohInnOut: String;
  Grupo1, Grupo2: TfrxGroupHeader;
  Futer1, Futer2: TfrxGroupFooter;
  Me_GH1, Me_FT1, Me_GH2, Me_FT2, MeValNome, MeTitNome, MeTitCodi, MeValCodi: TfrxMemoView;
  Index: Integer;
begin
  case RG13TipoMov_ItemIndex of
    0: SQL_SohInnOut := '';
    1: SQL_SohInnOut := 'WHERE MovimID IN (0, 1, 2, 4, 9, 12, 13, 17, 16, 18) ';
    //1: SQL_SohInnOut := 'WHERE MovimID IN (0, 1, 2, 4, 9, 13, 16, 18) ';
    2: SQL_SohInnOut := 'WHERE MovimID IN (9, 12, 13, 17) ';
    else
    begin
      SQL_SohInnOut := '';
      Geral.MB_Erro('"Tipo de movimento" n�o imlementado!');
    end;
  end;
  //
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
  Ordem := 'ORDER BY ' +
  Ordens[RG13_GragruY_ItemIndex] + ', ' +
  'DataHora, Controle ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr13VSSeqIts, DModG.MyPID_DB, [
  'SELECT vmi.*, ',
  ATT_MovimID,
  ATT_MovimNiv,
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, ',
  'IF(vsp.CliStat IS NULL, 0, vsp.CliStat) VSP_CliStat, ',
  'IF(vsp.Status IS NULL, 0, vsp.Status) VSP_STATUS, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, ',
  'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",   ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,  ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  'vps.Nome NO_STATUS, ',
  'vsf.Nome NO_SerieFch, CONCAT(vsf.Nome, " ", vmi.Ficha) NO_SERIE_FICHA ',
  'FROM ' + FVSSeqIts + ' vmi ',
  'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=vmi.Pallet   ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.Pallet <> 0, vsp.GraGruX, vmi.GraGruX) ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vmi.Empresa ',
  'LEFT JOIN ' + TMeuDB + '.vspalsta   vps ON vps.Codigo=vsp.Status  ',
  'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
  'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
  'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  SQL_SohInnOut,
  Ordem,
  '']);
  //Geral.MB_SQL(Self, Qr13VSSeqIts);
  //
  Grupo1 := frxWET_CURTI_018_13_A.FindObject('GH_01') as TfrxGroupHeader;
  //Grupo1.Visible := RG00_Agrupa.ItemIndex > 0;
  Futer1 := frxWET_CURTI_018_13_A.FindObject('FT_01') as TfrxGroupFooter;
  Futer1.Visible := Grupo1.Visible;
  Me_GH1 := frxWET_CURTI_018_13_A.FindObject('Me_GH1') as TfrxMemoView;
  Me_FT1 := frxWET_CURTI_018_13_A.FindObject('Me_FT1') as TfrxMemoView;
  case RG13_GragruY_ItemIndex of
    0: // -> 0000 Todos!!!
    begin
      Grupo1.Condition := 'frxDs13VSSeqIts."Data"';
      Me_GH1.Memo.Text := '[frxDs13VSSeqIts."Data"]';
      Me_FT1.Memo.Text := 'Data: [frxDs13VSSeqIts."Data"]: ';
    end;
    1: // -> CO_GraGruY_1024_VSNatCad
    begin
      Grupo1.Condition := 'frxDs13VSSeqIts."NO_SERIE_FICHA"';
      Me_GH1.Memo.Text := '[frxDs13VSSeqIts."NO_SERIE_FICHA"]';
      Me_FT1.Memo.Text := 'Ficha RMP: [frxDs13VSSeqIts."NO_SERIE_FICHA"]: ';
    end;
    2:  // -> CO_GraGruY_2048_VSRibCad
    begin
      Grupo1.Condition := 'frxDs13VSSeqIts."IMEI_Src"';
      Me_GH1.Memo.Text := '[frxDs13VSSeqIts."IMEI_Src"]';
      Me_FT1.Memo.Text := 'IME-I: [frxDs13VSSeqIts."IMEI_Src"] ';
    end;
    3:  // -> CO_GraGruY_3072_VSRibCla
    begin
      Grupo1.Condition := 'frxDs13VSSeqIts."Pallet"';
      Me_GH1.Memo.Text := '[frxDs13VSSeqIts."Pallet"]';
      Me_FT1.Memo.Text := 'Pallet: [frxDs13VSSeqIts."Pallet"]   [frxDs13VSSeqIts."NO_Pallet"]';
    end;
    4:  // -> CO_GraGruY_4096_VSRibOpe
    begin
      Grupo1.Condition := 'frxDs13VSSeqIts."Operacao"';
      Me_GH1.Memo.Text := 'Ordem de Opera��o: [frxDs13VSSeqIts."Operacao"]';
      Me_FT1.Memo.Text := 'Ordem de Opera��o: [frxDs13VSSeqIts."Operacao"]';
    end;
    5:  // -> CO_GraGruY_5120_VSWetEnd
    begin
      Grupo1.Condition := 'frxDs13VSSeqIts."IMEI_Src"';
      Me_GH1.Memo.Text := '[frxDs13VSSeqIts."IMEI_Src"]';
      Me_FT1.Memo.Text := 'IME-I: [frxDs13VSSeqIts."IMEI_Src"] ';
    end;
    6:  // -> CO_GraGruY_6144_VSFinCla
    begin
      Grupo1.Condition := 'frxDs13VSSeqIts."IMEI_Src"';
      Me_GH1.Memo.Text := '[frxDs13VSSeqIts."IMEI_Src"]';
      Me_FT1.Memo.Text := 'IME-I: [frxDs13VSSeqIts."IMEI_Src"] ';
    end;
    7: // CO_GraGruY_0512_VSSubPrd
    begin
      //??????????????????????????????????????????????
      Grupo1.Condition := 'frxDs13VSSeqIts."IMEI_Src"';
      //??????????????????????????????????????????????
      Me_GH1.Memo.Text := '[frxDs13VSSeqIts."IMEI_Src"]';
      //??????????????????????????????????????????????
      Me_FT1.Memo.Text := 'IME-I: [frxDs13VSSeqIts."IMEI_Src"] ';
      //??????????????????????????????????????????????
    end;
    else
    begin
      Grupo1.Condition := '***ERRO***';
      Me_GH1.Memo.Text := '***ERRO***';
      Me_FT1.Memo.Text := '***ERRO***';
    end;
  end;
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_018_13_A, [
    DModG.frxDsDono,
    frxDs13VSSeqIts
  ]);
  MyObjects.frxMostra(frxWET_CURTI_018_13_A, 'Fluxo VS');
end;

procedure TFmVSImpFluxo.ImprimePreparado;
begin
  //Application.CreateForm(TFmVSImpFluxo, FmVSImpFluxo);
  //
  case RG13_AnaliSinte_ItemIndex of
    1: // Analitico
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(FmVSImpFluxo.QrErrMov1, DModG.MyPID_DB, [
      'DROP TABLE IF EXISTS _TESTE_; ',
      'CREATE TABLE _TESTE_ ',
      ' ',
      'SELECT Pallet, SUM(SdoVrtPeca) SdoVrtPeca, ',
      'SUM(Pecas) Pecas, SUM(Inteiros) Inteiros ',
      'FROM ' + FVSSeqIts,
      'GROUP BY Pallet; ',
      ' ',
      'SELECT * ',
      'FROM _TESTE_ ',
      'WHERE Pecas <> SdoVrtPeca ',
      'ORDER BY Pallet ',
      '']);
      if QrErrMov1.RecordCount > 0 then
        ShowModal
      else
        ImprimeFluxo();
    end;
    2: // Sintetico
    begin
      ReabreSintetico();
      PageControl1.ActivePageIndex := 1;
      ShowModal;
    end;
    else
      Geral.MB_Erro('Tipo de pesquisa n�o implementada!');
  end;
  Close;
end;

procedure TFmVSImpFluxo.IrparajanaladeGerenciamentodeReclassificaoDesmontagem1Click(
  Sender: TObject);
var
  Qry: TmySQLQuery;
  Pallet, Codigo: Integer;
  //
  function SelecionaVSGerRclA(): Boolean;
  const
    Aviso  = '...';
    Titulo = 'Sele��o de Reclassifica��o';
    Prompt = 'Informe a reclassifica��o';
    Campo  = 'Descricao';
  var
    Codigo: Integer;
    Resp: Variant;
  begin
    Result := False;
    Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Codigo, CONCAT("OC ", CacCod) Descricao ',
    'FROM vsgerrcla',
    'WHERE VSPallet=' + Geral.FF0(Pallet),
    ''], Dmod.MyDB, True);
    if Resp <> Null then
    begin
      Codigo := Resp;
      Result := Qry.Locate('Codigo', Codigo, []);
    end;
  end;
begin
  Pallet := Qr13SinteticoPallet.Value;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, CONCAT("OC ", CacCod) Descricao ',
    'FROM vsgerrcla',
    'WHERE VSPallet=' + Geral.FF0(Pallet),
    '']);
    //
    if Qry.RecordCount <> 1 then
    begin
      if not SelecionaVSGerRclA() then
        Exit;
    end;
    Codigo   := Qry.FieldByName('Codigo').AsInteger;
    VS_PF.MostraFormVSGerRclCab(Codigo, 0);
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpFluxo.IrparajaneladeCassificaoMontagem1Click(Sender: TObject);
begin
  MostraJanelaMontagem('vsgerarta');
end;

procedure TFmVSImpFluxo.IrparajaneladeentradaInNatura1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  SerieFch, Ficha, Codigo, Controle: Integer;
  Tabela: String;
  //
  function SelecionaVSInnCab(): Boolean;
  const
    Aviso  = '...';
    Titulo = 'Sele��o de Entrada In Natura';
    Prompt = 'Informe a entrada';
    Campo  = 'Descricao';
  var
    Codigo: Integer;
    Resp: Variant;
  begin
    Result := False;
    Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Codigo, CONCAT("IMEI ", Controle) ' + Campo,
    'FROM ' + Tabela,
    'WHERE MovimID=1 ',
    'AND SerieFch=' + Geral.FF0(SerieFch),
    'AND Ficha=' + Geral.FF0(Ficha),
    ''], Dmod.MyDB, True);
    if Resp <> Null then
    begin
      Codigo := Resp;
      Result := Qry.Locate('Codigo', Codigo, []);
    end;
  end;
  procedure ReopenQry();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, Controle ',
    'FROM ' + Tabela,
    'WHERE MovimID=1 ',
    'AND SerieFch=' + Geral.FF0(SerieFch),
    'AND Ficha=' + Geral.FF0(Ficha),
    '']);
  end;
begin
  SerieFch := Qr13SinteticoSerieFch.Value;
  Ficha    := Qr13SinteticoFicha.Value;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    Tabela := CO_SEL_TAB_VMI;
    ReopenQry();
    if Qry.RecordCount = 0 then
    begin
      Tabela := CO_TAB_VMB;
      ReopenQry();
    end;
    //
    if Qry.RecordCount <> 1 then
    begin
      if not SelecionaVSInnCab() then
        Exit;
    end;
    Codigo   := Qry.FieldByName('Codigo').AsInteger;
    Controle := Qry.FieldByName('Controle').AsInteger;
    VS_PF.MostraFormVSInnCab(Codigo, Controle, 0);
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpFluxo.IrparajaneladeGeraodeArtigo1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Codigo, Controle: Integer;
  //
  procedure ReopenQry(Tabela: String);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, Controle ',
    'FROM ' + Tabela,
    'WHERE Controle=' + Geral.FF0(Controle),
    '']);
  end;
begin
  Controle := Qr13SinteticoIMEI_Src.Value;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    ReopenQry(CO_SEL_TAB_VMI);
    if Qry.RecordCount = 0 then
      ReopenQry(CO_TAB_VMB);
    //
    Codigo   := Qry.FieldByName('Codigo').AsInteger;
    Controle := Qry.FieldByName('Controle').AsInteger;
    VS_PF.MostraFormVSGeraArt(Codigo, Controle);
  finally
    Qry.Free;
  end;
end;

procedure TFmVSImpFluxo.Irparajaneladegerenciamentodeficha1Click(
  Sender: TObject);
var
  SerieFch, Ficha: Integer;
begin
  SerieFch := Qr13SinteticoSerieFch.Value;
  Ficha    := Qr13SinteticoFicha.Value;
  //
  VS_PF.MostraFormVSFchGerCab(SerieFch, Ficha);
end;

procedure TFmVSImpFluxo.IrparajaneladegerenciamentodeIMEI1Click(
  Sender: TObject);
begin
  VS_PF.MostraFormVSMovIts(Qr13SinteticoIMEI_Src.Value);
end;

procedure TFmVSImpFluxo.IrparajaneladegerenciamentodePallet1Click(
  Sender: TObject);
begin
  VS_PF.MostraFormVSPallet(Qr13SinteticoPallet.Value);
end;

procedure TFmVSImpFluxo.IrparajaneladeOrdemdeOperao1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSOpeCab(0);
end;

procedure TFmVSImpFluxo.IrparajenaladereclassificaoMontagem1Click(
  Sender: TObject);
begin
  MostraJanelaMontagem('vsgerrcla');
end;

procedure TFmVSImpFluxo.MostraJanelaMontagem(Tabela: String);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Entrada In Natura';
  Prompt = 'Informe a entrada';
  Campo  = 'Descricao';
var
  Codigo: Integer;
  Resp: Variant;
begin
  Resp := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
  'SELECT Codigo, CONCAT("OC ", CacCod) ' + Campo,
  'FROM ' + Tabela,
  'WHERE CacCod IN ( ',
  '   ',
  '  SELECT DISTINCT CacCod ',
  '  FROM vscacitsa ',
  '  WHERE VSPallet=' + Geral.FF0(Qr13SinteticoPallet.Value),
  ') ',
  'ORDER BY CacCod ',
  ''], Dmod.MyDB, True);
  if Resp <> Null then
  begin
    Codigo := Resp;
    if LowerCase(Tabela) = 'vsgerarta' then
      VS_PF.MostraFormVSGerClaCab(Codigo)
    else
    if LowerCase(Tabela) = 'vsgerrcla' then
      VS_PF.MostraFormVSGerRclCab(Codigo, 0)
    else
      Geral.MB_Erro('Tabela n�o implementada!');
  end;
end;

procedure TFmVSImpFluxo.PesquisaFluxo;
var
  Qry: TmySQLQuery;
  AcumInteir: Double;
  Sequencia, Controle, GraGruX: Integer;
  Ordem, ATT_MovimID, ATT_MovimNiv, SQL_GraGruX, SQL_GraGruY, //SQL_ExtraY,
  SQL_IMEI_Src, SQL_IMEI_Ord,
  Emin, Emid, SQL_GGXi, SQL_GGX2, SQL_NotFluxo, SQL_Periodo, SQL_Operacao,
  SQL_DataAnt, SQL_FichaRMP: String;
  Inseriu: Boolean;
  //
  procedure InsereSaldoAnterior(GragruY: Integer);
  const
    MovimID = Integer(UnDmkEnums.TEstqMovimID.emidSaldoAnterior);
  var
    Data, DataHora: String;
    Inteiros: Double;
    MovimNiv: Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ',
    'SUM(vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) Inteiros ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
    'WHERE (vmi.Pecas <> 0 OR vmi.PesoKg <> 0 OR vmi.AreaM2 <> 0) ',
    //'AND vmi.EmFluxo=1 ',
    'AND (vmi.MovimNiv <> ' + Geral.FF0(Integer(eminSorcCurtiXX)) + ') ',  //14
    SQL_DataAnt,
    SQL_GraGruY,
    //SQL_ExtraY,
    SQL_GraGruX,
    SQL_NotFluxo,
    '']);
    Data     := Geral.FDT(TP13DataIni_Date - 1, 1);
    DataHora := Data + ' 23:59:59';
    Inteiros := Qry.FieldByName('Inteiros').AsFloat;
    case GraGruY of
      CO_GraGruY_0512_VSSubPrd: MovimNiv := Integer(TEstqMovimNiv.eminSdoSubPrd);
      CO_GraGruY_1024_VSNatCad: MovimNiv := Integer(TEstqMovimNiv.eminSdoArtInNat);
      CO_GraGruY_1088_VSNatCon: MovimNiv := Integer(TEstqMovimNiv.eminSdoArtEmCon);
      CO_GraGruY_2048_VSRibCad: MovimNiv := Integer(TEstqMovimNiv.eminSdoArtGerado);
      CO_GraGruY_3072_VSRibCla: MovimNiv := Integer(TEstqMovimNiv.eminSdoArtClassif);
      CO_GraGruY_4096_VSRibOpe: MovimNiv := Integer(TEstqMovimNiv.eminSdoArtEmOper);
      CO_GraGruY_5120_VSWetEnd: MovimNiv := Integer(TEstqMovimNiv.eminSdoArtEmWEnd);
      CO_GraGruY_6144_VSFinCla: MovimNiv := Integer(TEstqMovimNiv.eminSdoFinish);
      CO_GraGruY_7168_VSRepMer: MovimNiv := Integer(TEstqMovimNiv.eminSdoArtEmRRM);

      else
      begin
        Geral.MB_Erro('"GraGruY" n�o implementado (MovimNiv saldo inicial)!');
        MovimNiv := Integer(TEstqMovimNiv.eminSemNiv);
      end;
    end;
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FVSSeqIts, False, [
    CO_DATA_HORA_GRL, 'Data', 'Inteiros',
    'movimID', 'MovimNiv'], [
    'GraGruY', 'Controle'], [
    DataHora, Data, Inteiros,
    MovimID, MovimNiv], [
    GraGruY, 0], False);
  end;
  //
  function InsereItens(GraGruY: Integer): Boolean;
  var
    Pallet, IMEI: Integer;
    SQL_Pallet, SQL_IMEI: String;
  begin
    Pallet := Ed13_Pallet_ValueVariant;
    if Pallet <> 0 then
      SQL_Pallet := 'AND vmi.Pallet=' + Geral.FF0(Pallet)
    else
      SQL_IMEI := '';
    IMEI := Ed13_IMEI_ValueVariant;
    if IMEI <> 0 then
      SQL_IMEI := 'AND (vmi.Controle=' + Geral.FF0(IMEI) +
                  ' OR vmi.SrcNivel2=' + Geral.FF0(IMEI) + ')'
    else
      SQL_IMEI := '';
    if Ed13Ficha_ValueVariant <> 0 then
      SQL_FichaRMP := 'AND vmi.SerieFch=' + Geral.FF0(Ed13SerieFch_ValueVariant) +
      ' AND vmi.Ficha=' + Geral.FF0(Ed13Ficha_ValueVariant)
    else
      SQL_FichaRMP := '';
    if Ck13DataIni_Checked and Ck13DataIni_Visible and Ck13DataIni_Enabled then
      InsereSaldoAnterior(GraGruY);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO ' + FVSSeqIts + '',
    'SELECT ',
    'vmi.Codigo, vmi.Controle, vmi.MovimCod, vmi.MovimNiv, ',
    'vmi.MovimTwn, vmi.Empresa, vmi.Terceiro, ',
    'vmi.CliVenda, vmi.MovimID, vmi.LnkNivXtr1, ',
    'vmi.LnkNivXtr2, vmi.DataHora, vmi.DataHora Data, vmi.Pallet, ',
    'vmi.GraGruX, vmi.Pecas, vmi.PesoKg, ',
    'vmi.AreaM2, vmi.AreaP2, vmi.ValorT, ',
    'vmi.SrcMovID, vmi.SrcNivel1, vmi.SrcNivel2, ',
    'vmi.SrcGGX, vmi.SdoVrtPeca, vmi.SdoVrtPeso, ',
    'vmi.SdoVrtArM2, vmi.Observ, vmi.SerieFch, ',
    'vmi.Ficha, vmi.Misturou, vmi.FornecMO, ',
    'vmi.CustoMOKg, vmi.CustoMOTot, vmi.ValorMP, ',
    'vmi.DstMovID, vmi.DstNivel1, vmi.DstNivel2, ',
    'vmi.DstGGX, vmi.QtdGerPeca, vmi.QtdGerPeso, ',
    'vmi.QtdGerArM2, vmi.QtdGerArP2, vmi.QtdAntPeca, ',
    'vmi.QtdAntPeso, vmi.QtdAntArM2, vmi.QtdAntArP2, ',
    'vmi.AptoUso, vmi.NotaMPAG, vmi.Marca, ',
    'vmi.TpCalcAuto,  ',
    'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros, ',
    '0 AcumInteir, 0 Sequencia, ggx.GraGruY, ',
    SQL_IMEI_Src,
    SQL_Operacao,
    SQL_IMEI_Ord,
    '  vmi.Ativo ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=vmi.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
    'WHERE (vmi.Pecas <> 0 OR vmi.PesoKg <> 0 OR vmi.AreaM2 <> 0) ',
    //'AND vmi.EmFluxo=1 ',
    'AND (vmi.MovimNiv <> ' + Geral.FF0(Integer(eminSorcCurtiXX)) + ') ',  //14
    SQL_Periodo,
    SQL_GraGruY,
    //SQL_ExtraY,
    SQL_GraGruX,
    SQL_NotFluxo,
    SQL_Pallet,
    SQL_IMEI,
    SQL_FichaRMP,
    '']);
    //Geral.MB_Info(DModG.QrUpdPID1.SQL.Text);
  end;
var
  NomeExtra: String;
begin
  if Geral.MB_Pergunta('Pesquisa deprecada se houver arquivo morto!' +
  'Deseja continuar assim mesmo?') <> ID_YES then
    Exit;
  if MyObjects.FIC(RG13_AnaliSinte_ItemIndex = 0, FRG13_AnaliSinte,
  'Defina o tipo de pesquisa!') then
    Exit;
  //
  if VS_PF.VerificaGraGruXCouSemNivel1() > 0 then
    Exit;
  Screen.Cursor := crHourGlass;
  Qry := TmySQLQuery.Create(Dmod);
  try
    //SQL_Periodo := '';

    SQL_DataAnt := dmkPF.SQL_Periodo('AND vmi.DataHora ',
      0, TP13DataIni_Date -1, False, True);
    SQL_Periodo := dmkPF.SQL_Periodo('AND vmi.DataHora ',
      TP13DataIni_Date, TP13DataFim_Date, Ck13DataIni_Checked and
      Ck13DataIni_Visible and Ck13DataIni_Enabled, Ck13DataFim_Checked);

    //

    SQL_NotFluxo := '';
    Emin := '';
    Emid := '';
    SQL_IMEI_Src  := '0 IMEI_Src,';
    SQL_IMEI_Ord := '0 IMEI_Ord,';
    SQL_Operacao := '0 Operacao, ';
    //SQL_ExtraY := '';
    //
    NomeExtra := '';
    if InputQuery('Deseja definir nome �nico?', 'Defina o nome ou deixe vazio',
    NomeExtra) then
    begin
      //
    end;
    if Trim(NomeExtra) = '' then
      NomeExtra := Self.Name;
    FVSSeqIts := UnCreateVS.RecriaTempTableNovo(ntrttVSSeqIts,
        DModG.QrUpdPID1, False, 1, '_VSSeqIts_' + NomeExtra);
    GraGruX := Ed13GraGruX_ValueVariant;
    if GraGruX = 0 then
      SQL_GraGruX := ''
    else
      SQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX);
    //
    //
//    case RG13_GragruY.ItemIndex of
    if (RG13_GragruY_ItemIndex in ([0,1])) then
      //1: //-> 1024
      begin
        Emin := '';
        Emid := '';
        SQL_IMEI_Src  := '0 IMEI_Src,';
        SQL_IMEI_Ord := '0 IMEI_Ord,';
        //SQL_ExtraY := '';
        SQL_GraGruY := 'AND ggx.GraGruY=1024';
        SQL_NotFluxo := 'AND NOT (vmi.NotFluxo & 1)';
        //
        Inseriu := InsereItens(1024);
      end;
    if (RG13_GragruY_ItemIndex in ([0,2])) then
      //2: //-> 2048
      begin
        Emid := Geral.FF0(Integer(emidIndsXX)); // 6
        Emin := Geral.FF0(Integer(TEstqMovimNiv.eminDestCurtiXX)); // 13
        SQL_IMEI_Src := 'IF(vmi.MovimNiv=' + Emin + ', vmi.Controle, vmi.SrcNivel2) IMEI_Src, ';
        SQL_IMEI_Ord := 'IF(vmi.MovimNiv=' + Emin + ', 1, 2) IMEI_Ord, ';
        if GraGruX <> 0 then
        begin
          SQL_GGXi := ' AND vmi.GraGruX=' + Geral.FF0(GraGruX);
          SQL_GGX2 := ' AND vm2.GraGruX=' + Geral.FF0(GraGruX);
        end else
        begin
          SQL_GGXi := '';
          SQL_GGX2 := '';
        end;
(*
        SQL_GraGruY := Geral.ATS([
        'AND ( ',
        '  (ggx.GraGruY = 2048 AND vmi.Pallet = 0 AND vmi.MovimID=6' + SQL_GGXi + ') ',
        '  OR ',
        '  ( ',
        '  vmi.SrcNivel2 IN ( ',
        '  SELECT DISTINCT vm2.Controle ',
        '  FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vm2 ',
        '  LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=vm2.GraGruX  ',
        '  WHERE (ggx.GraGruY = 2048 AND vm2.Pallet = 0 AND vm2.MovimID=6' + SQL_GGX2 + ') ',
        ' ) ',
        '  ) ',
        ') ']);
*)
        SQL_GraGruY := 'AND ggx.GraGruY=2048';
        SQL_NotFluxo := 'AND NOT (vmi.NotFluxo & 2)';
        //
        Inseriu := InsereItens(2048);
      end;
      if (RG13_GragruY_ItemIndex in ([0,3])) then
      //3: // -> 3072
      begin
        SQL_GraGruX := ''; // Espec[ifico
        //  SQL_GraGruY := 'AND (ggx.GraGruY = 3072 OR (ggx.GraGruY = 2048 AND Pallet <> 0))';
        SQL_IMEI_Src := Geral.ATS([
        'IF( ',
        '  (MovimID=7 AND MovimNiv=2) ',
        '  OR ',
        '  (MovimID=8 AND MovimNiv=2) ',
        '  OR ',
        '  (MovimID=13 AND MovimNiv=0) ',
        '  OR ',
        '  (MovimID=14 AND MovimNiv=2) ',
        '  OR ',
        '  (MovimID=15 AND MovimNiv=2) ',
        '  OR ',
        '  (MovimID=16 AND MovimNiv=0) ',
        ', vmi.Controle, vmi.SrcNivel2) IMEI_Src,   ']);
        SQL_IMEI_Ord := Geral.ATS([
        'IF( ',
        '  (MovimID=7 AND MovimNiv=2) ',
        '  OR ',
        '  (MovimID=8 AND MovimNiv=2) ',
        '  OR ',
        '  (MovimID=13 AND MovimNiv=0) ',
        '  OR ',
        '  (MovimID=14 AND MovimNiv=2) ',
        '  OR ',
        '  (MovimID=15 AND MovimNiv=2) ',
        '  OR ',
        '  (MovimID=16 AND MovimNiv=0) ',
        ', 1, 2) IMEI_Ord,   ']);
        if GraGruX <> 0 then
        begin
          SQL_GGXi := Geral.ATS([
          '    vmi.GraGruX=' + Geral.FF0(GraGruX),
          '    AND ']);
          SQL_GGX2 := Geral.ATS([
          '      vm2.GraGruX=' + Geral.FF0(GraGruX),
          '      AND']);
        end else
        begin
          SQL_GGXi := '';
          SQL_GGX2 := '';
        end;
(*
        SQL_GraGruY := Geral.ATS([
        'AND (  ',
        '  ( ',
        SQL_GGXi,
        '    (',
        '       ggx.GraGruY = 3072  ',
        '       OR  ',
        '       (ggx.GraGruY = 2048 AND (vmi.Pallet <> 0 OR vmi.MovimID<>6)) ',
        '     )',
        '  ) ',
        '  OR  ',
        '  (  ',
        '    vmi.SrcNivel2 IN (  ',
        '    SELECT DISTINCT vm2.Controle  ',
        '    FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vm2  ',
        '    LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=vm2.GraGruX   ',
        '    WHERE ( ',
        SQL_GGX2,
        '      (ggx.GraGruY = 3072  ',
        '      OR  ',
        '      (ggx.GraGruY = 2048 AND (vm2.Pallet <> 0 )) ',
        '       ) ',
        '     )  ',
        '   )  ',
        '  )',
        ')  ',
        'AND (NOT (vmi.MovimID = 7 AND MovimNiv = 1)) ', // Baixa de ???
        'AND (NOT (vmi.MovimID IN (9,17,11) AND Pallet = 0)) ', // Baixa Forcada
        'AND vmi.Ativo=1 ',
        '']);
*)
        SQL_GraGruY := 'AND ggx.GraGruY=3072';
        SQL_NotFluxo := 'AND NOT (vmi.NotFluxo & 4)';
        //
        Inseriu := InsereItens(3072);
      end;
      if (RG13_GragruY_ItemIndex in ([0,4])) then
      //4: //-> 4096
      begin
        Emin := '';
        Emid := '';
        //SQL_IMEI_Src := 'IF(vmi.MovimID=11, vmi.Codigo, vmi.SrcNivel1) IMEI_Src, ';
        SQL_IMEI_Ord := 'IF(vmi.MovimNiv=8, 1, 2) IMEI_Ord, ';
        SQL_Operacao := 'IF(vmi.MovimID=11, vmi.Codigo, vmi.SrcNivel1) Operacao, ';
        //SQL_ExtraY := '';
(*
        SQL_GraGruY := Geral.ATS([
        'AND  ',
        '  ( ',
        '    ( ',
        '      vmi.MovimID=11 AND vmi.MovimNiv IN (8,10) ',
        '    ) ',
        '    OR ',
        '    ( ',
        '      vmi.SrcNivel2 IN  ',
        '      ( ',
        '        SELECT Controle  ',
        '        FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' ',
        '        WHERE MovimID=11 ',
        '        AND MovimNiv=8 ',
        '      ) ',
        '    ) ',
        '  ) ']);
*)
        SQL_GraGruY := 'AND ggx.GraGruY=4096';
        SQL_NotFluxo := 'AND NOT (vmi.NotFluxo & 8)';
        //
        Inseriu := InsereItens(4096);
      end;
    if (RG13_GragruY_ItemIndex in ([0,5])) then
      //1: //-> 1024
      begin
        Emin := '';
        Emid := '';
        SQL_IMEI_Src  := '0 IMEI_Src,';
        SQL_IMEI_Ord := '0 IMEI_Ord,';
        //SQL_ExtraY := '';
        SQL_GraGruY := 'AND ggx.GraGruY=5120';
        SQL_NotFluxo := 'AND NOT (vmi.NotFluxo & 16)';
        //
        Inseriu := InsereItens(5120);
      end;
    if (RG13_GragruY_ItemIndex in ([0,6])) then
      //1: //-> 1024
      begin
        Emin := '';
        Emid := '';
        SQL_IMEI_Src  := '0 IMEI_Src,';
        SQL_IMEI_Ord := '0 IMEI_Ord,';
        //SQL_ExtraY := '';
        SQL_GraGruY := 'AND ggx.GraGruY=6144';
        SQL_NotFluxo := 'AND NOT (vmi.NotFluxo & 32)';
        //
        Inseriu := InsereItens(6144);
      end;
{
      else
      begin
        Emin := '';
        Emid := '';
        SQL_IMEI_Src  := '0 IMEI_Src,';
        SQL_IMEI_Ord := '0 IMEI_Ord,';
        //SQL_ExtraY := '';
        SQL_GraGruY := '';
        SQL_NotFluxo := '';
      end;
    end;
}
    case RG13_AnaliSinte_ItemIndex of
      1: //Analitico
      begin
        Ordem := 'ORDER BY ' +
        Ordens[RG13_GragruY_ItemIndex] + ', ' +
        'DataHora, Controle ';
        //
        ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
        sEstqMovimID);
        //
        ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
        sEstqMovimNiv);
        //
        UnDmkDAC_PF.AbreMySQLQuery0(Qr13VSSeqIts, DModG.MyPID_DB, [
        'SELECT vmi.*, ',
        ATT_MovimID,
        ATT_MovimNiv,
        'CONCAT(gg1.Nome, ',
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
        'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
        'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, ',
        'IF(vsp.CliStat IS NULL, 0, vsp.CliStat) VSP_CliStat, ',
        'IF(vsp.Status IS NULL, 0, vsp.Status) VSP_STATUS, ',
        'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, ',
        'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",   ',
        'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,  ',
        'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
        'vps.Nome NO_STATUS, ',
        'vsf.Nome NO_SerieFch, CONCAT(vsf.Nome, " ", vmi.Ficha) NO_SERIE_FICHA ',
        'FROM ' + FVSSeqIts + ' vmi ',
        'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=vmi.Pallet   ',
        'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.Pallet <> 0, vsp.GraGruX, vmi.GraGruX) ',
        'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
        'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
        'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
        'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
        'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
        'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
        'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vmi.Terceiro ',
        'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vmi.Empresa ',
        'LEFT JOIN ' + TMeuDB + '.vspalsta   vps ON vps.Codigo=vsp.Status  ',
        'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
        'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
        'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
        Ordem,
        '']);
        //Geral.MB_SQL(Self, Qr13VSSeqIts);
        if FPB1 <> nil then
        begin
          FPB1.Position := 0;
          FPB1.Max := Qr13VSSeqIts.RecordCount;
        end;
        MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Calculando saldo acumulado!');
        //
        AcumInteir := 0;
        Sequencia  := 0;
        Qr13VSSeqIts.First;
        while not Qr13VSSeqIts.Eof do
        begin
          MyObjects.UpdPB(FPB1, nil, nil);
          Controle   := Qr13VSSeqIts.FieldByName('Controle').AsInteger;
          AcumInteir := AcumInteir + Qr13VSSeqIts.FieldByName('Inteiros').AsFloat;
          Sequencia  := Sequencia + 1;
          //
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FVSSeqIts, False, [
          'AcumInteir', 'Sequencia'], ['Controle'], [
          AcumInteir, Sequencia], [Controle], False);
          //
          Qr13VSSeqIts.Next;
        end;
        MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Abrindo pesquisa');
        UnDmkDAC_PF.AbreQuery(Qr13VSSeqIts, DModG.MyPID_DB);
      end;
      2: // Sintetico
      begin
        FVSFluxIncon := UnCreateVS.RecriaTempTableNovo(ntrttVSFluxIncon,
          DModG.QrUpdPID1, False, 1, '_VSFluxIncon_' + Self.Name);
        //
        if (RG13_GragruY_ItemIndex in ([0,1])) then
        //1: //-> 1024
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
          'DROP TABLE IF EXISTS _Fluxo_1024_; ',
          'CREATE TABLE _Fluxo_1024_ ',
          'SELECT Ficha, SerieFch, ',
          'SUM(IF(SdoVrtPeca <> 0, 1, 0)) ITENS, ',
          'SUM(Inteiros) Inteiros, ',
          'SUM(IF(Inteiros > 0, Inteiros, 0)) Int_Pos, ',
          'SUM(IF(Inteiros < 0, Inteiros, 0)) Int_Neg',
          'FROM ' + FVSSeqIts,
          'WHERE GraGruY=1024 ',
          'GROUP BY Ficha, SerieFch; ',
          'INSERT INTO ' + FVSFluxIncon,
          'SELECT 1024 GraGruY, Inteiros, 0 LastDtHr, ',
          'SerieFch, Ficha, 0 IMEI_Src, 0 Pallet, 0 Operacao, ',
          'Int_Pos, Int_Neg, 1 Ativo ',
          'FROM _Fluxo_1024_ ',
          'WHERE Inteiros <> 0 OR ITENS > 0; ',
          '']);
        end;
        if (RG13_GragruY_ItemIndex in ([0,2])) then
        //2: //-> 2048
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
          'DROP TABLE IF EXISTS _Fluxo_2048_; ',
          'CREATE TABLE _Fluxo_2048_ ',
          'SELECT IMEI_Src, ',
          'SUM(IF(SdoVrtPeca <> 0, 1, 0)) ITENS, ',
          'SUM(Inteiros) Inteiros, ',
          'SUM(IF(Inteiros > 0, Inteiros, 0)) Int_Pos, ',
          'SUM(IF(Inteiros < 0, Inteiros, 0)) Int_Neg',
          'FROM ' + FVSSeqIts,
          'WHERE GraGruY=2048 ',
          'GROUP BY IMEI_Src; ',
          'INSERT INTO ' + FVSFluxIncon,
          'SELECT 2048 GraGruY, Inteiros, 0 LastDtHr, ',
          '0 SerieFch, 0 Ficha, IMEI_Src, 0 Pallet, 0 Operacao, ',
          'Int_Pos, Int_Neg, 1 Ativo ',
          'FROM _Fluxo_2048_ ',
          'WHERE Inteiros <> 0 OR ITENS > 0; ',
          '']);
        end;
        if (RG13_GragruY_ItemIndex in ([0,3])) then
        //3: // -> 3072
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
          'DROP TABLE IF EXISTS _Fluxo_3072_; ',
          'CREATE TABLE _Fluxo_3072_ ',
          'SELECT Pallet, ',
          'SUM(IF(SdoVrtPeca <> 0, 1, 0)) ITENS, ',
          'SUM(Inteiros) Inteiros, ',
          'SUM(IF(Inteiros > 0, Inteiros, 0)) Int_Pos, ',
          'SUM(IF(Inteiros < 0, Inteiros, 0)) Int_Neg',
          'FROM ' + FVSSeqIts,
          'WHERE GraGruY=3072 ',
          'GROUP BY Pallet; ',
          'INSERT INTO ' + FVSFluxIncon,
          'SELECT 3072 GraGruY, Inteiros, 0 LastDtHr, ',
          '0 SerieFch, 0 Ficha, 0 IMEI_Src, Pallet, 0 Operacao, ',
          'Int_Pos, Int_Neg, 1 Ativo ',
          'FROM _Fluxo_3072_ ',
          'WHERE Inteiros <> 0 OR ITENS > 0; ',
          '']);
        end;
        if (RG13_GragruY_ItemIndex in ([0,4])) then
        //4: //-> 4096
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
          'DROP TABLE IF EXISTS _Fluxo_4096_; ',
          'CREATE TABLE _Fluxo_4096_ ',
          'SELECT Operacao, ',
          'SUM(IF(SdoVrtPeca <> 0, 1, 0)) ITENS, ',
          'SUM(Inteiros) Inteiros, ',
          'SUM(IF(Inteiros > 0, Inteiros, 0)) Int_Pos, ',
          'SUM(IF(Inteiros < 0, Inteiros, 0)) Int_Neg',
          'FROM ' + FVSSeqIts,
          'WHERE GraGruY=4096 ',
          'GROUP BY Operacao; ',
          'INSERT INTO ' + FVSFluxIncon,
          'SELECT 4096 GraGruY, Inteiros, 0 LastDtHr, ',
          '0 SerieFch, 0 Ficha, 0 IMEI_Src, 0 Pallet, Operacao, ',
          'Int_Pos, Int_Neg, 1 Ativo ',
          'FROM _Fluxo_4096_ ',
          'WHERE Inteiros <> 0 OR ITENS > 0; ',
          '']);
        end;
        //else
          //Geral.MB_Erro('Tipo de pesquisa n�o implementada!');
          //'ORDER BY vsi.Ficha, vsi.SerieFch; ',
        //
        DmModVS.Atualiza13SinteticoDtHr(EdEmpresa_ValueVariant, FVSFluxIncon,
          FPB1, FLaAviso1, FLaAviso2);
      end;
    end;
    //
    if FBtImprime <> nil then
      FBtImprime.Enabled := True;
    //
    MyObjects.Informa2(FLaAviso1, FLaAviso2, False, '...');
    //
    ImprimePreparado();
  finally
    Screen.Cursor := crDefault;
    Qry.Free;
  end;
end;

procedure TFmVSImpFluxo.PMSinteticoPopup(Sender: TObject);
var
  SerieFch, Ficha: Integer;
begin
  SerieFch := Qr13SinteticoSerieFch.Value;
  Ficha    := Qr13SinteticoFicha.Value;
  Irparajaneladegerenciamentodeficha1.Enabled := Ficha <> 0;
  IrparajaneladegerenciamentodePallet1.Enabled := Qr13SinteticoPallet.Value <> 0;
  IrparajaneladegerenciamentodeIMEI1.Enabled := Qr13SinteticoIMEI_Src.Value <> 0;
  IrparajaneladeentradaInNatura1.Enabled := Qr13SinteticoFicha.Value <> 0;
  IrparajaneladeGeraodeArtigo1.Enabled := Qr13SinteticoIMEI_Src.Value <> 0;
  IrparajaneladeOrdemdeOperao1.Enabled := Qr13SinteticoOperacao.Value <> 0;
  IrparajaneladeCassificaoMontagem1.Enabled := Qr13SinteticoPallet.Value <> 0;
end;

procedure TFmVSImpFluxo.Qr13SinteticoCalcFields(DataSet: TDataSet);
begin
  Qr13SinteticoLastDtHr_TXT.Value := Geral.FDT(Qr13SinteticoLastDtHr.Value, 0, True);
end;

procedure TFmVSImpFluxo.Qr13SinteticoLastDtHrGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := Geral.FDT(Qr13SinteticoLastDtHr.Value, 0, True);
end;

procedure TFmVSImpFluxo.ReabreSintetico();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr13Sintetico, DModG.MyPID_DB, [
  'SELECT vfi.*, vsf.Nome NO_SERFCH, ggy.Nome NO_GGY, ',
  'IF(vfi.LastDtHr < 2, "", ',
  '  DATE_FORMAT(vfi.LastDtHr, "%d/%m/%Y %H:%i:%s")) LastDtHr_TXT ',
  //'FROM _vsfluxincon_fmvsmovimp vfi ',
  'FROM ' + FVSFluxIncon + ' vfi ',
  'LEFT JOIN ' + TMeuDB + '.gragruy ggy ON ggy.Codigo=vfi.GraGruY ',
  'LEFT JOIN ' + TMeuDB + '.vsserfch vsf ON vsf.Codigo=vfi.SerieFch ',
  'WHERE vfi.Inteiros<=' + Geral.FFT_Dot(Ed13_MaxInteiros.ValueVariant, 1, siNegativo),
  'AND (vfi.Int_Pos > 0 OR vfi.Int_Neg < 0) ',
  'ORDER BY vfi.GraGruY, vfi.SerieFch,  ',
  'vfi.Ficha, vfi.IMEI_Src, vfi.Pallet ',
  '']);
  //
  //Geral.MB_SQL(Self, Qr13Sintetico);
  //
end;

procedure TFmVSImpFluxo.SetaTodosItens13(Ativo: Boolean);
begin
  if Ativo then
    MyObjects.DBGridSelectAll(TDBGrid(DBG13Sintetico))
  else
    DBG13Sintetico.SelectedRows.Clear;
end;

(*

 Diferenca de estoque!

DROP TABLE IF EXISTS _TESTE_1_;
CREATE TABLE _TESTE_1_
SELECT gg1.Nome, GragruX, SUM(Pecas) Pecas,
SUM(SdoVrtPeca) SdoVrtPeca
FROM _vsseqits_fmvsmovimp vmi
LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=vmi.GraGruX
LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1
GROUP BY GraGruX;
SELECT * FROM _TESTE_1_

WHERE SdoVrtPeca<>0
ORDER BY Nome, GraGruX


*)

end.
