object FmVSRclArtSel: TFmVSRclArtSel
  Left = 339
  Top = 185
  Caption = 
    'WET-CURTI-032 :: Continua'#231#227'o de Reclassifica'#231#227'o de Artigo de Rib' +
    'eira'
  ClientHeight = 262
  ClientWidth = 760
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 760
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 712
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 664
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 637
        Height = 32
        Caption = 'Continua'#231#227'o de Reclassifica'#231#227'o de Artigo de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 637
        Height = 32
        Caption = 'Continua'#231#227'o de Reclassifica'#231#227'o de Artigo de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 637
        Height = 32
        Caption = 'Continua'#231#227'o de Reclassifica'#231#227'o de Artigo de Ribeira'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 760
    Height = 100
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 760
      Height = 100
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 760
        Height = 100
        Align = alClient
        TabOrder = 0
        object Label35: TLabel
          Left = 12
          Top = 54
          Width = 34
          Height = 13
          Caption = 'Senha:'
        end
        object PnPartida: TPanel
          Left = 2
          Top = 15
          Width = 756
          Height = 44
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object LaVSRibCad: TLabel
            Left = 8
            Top = 0
            Width = 29
            Height = 13
            Caption = 'Pallet:'
          end
          object EdPallet: TdmkEditCB
            Left = 8
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBPallet
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBPallet: TdmkDBLookupComboBox
            Left = 67
            Top = 16
            Width = 654
            Height = 21
            KeyField = 'VSPallet'
            ListField = 'NO_PRD_TAM_COR'
            ListSource = DsPallet
            TabOrder = 1
            dmkEditCB = EdPallet
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
        object EdVSPwdDdClas: TEdit
          Left = 12
          Top = 70
          Width = 421
          Height = 20
          Font.Charset = SYMBOL_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Wingdings'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          PasswordChar = 'l'
          ShowHint = False
          TabOrder = 1
        end
        object CkSomentePositivos: TCheckBox
          Left = 448
          Top = 72
          Width = 121
          Height = 17
          Caption = 'Somente positivos'
          Checked = True
          State = cbChecked
          TabOrder = 2
          Visible = False
          OnClick = CkSomentePositivosClick
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 148
    Width = 760
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 756
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 192
    Width = 760
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 614
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 612
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrVSPaRclCab: TMySQLQuery
    Database = Dmod.MyDB
    Left = 356
    Top = 140
    object QrVSPaRclCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaRclCabCacCod: TIntegerField
      FieldName = 'CacCod'
    end
  end
  object QrPallet: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT VSPallet, prc.CacCod,'
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)), '
      '" ( OC ", prc.CacCod, " )",'
      'IF(vsp.Nome <> "", CONCAT(" (", vsp.Nome, ")"), ""))  '
      'NO_PRD_TAM_COR '
      'FROM vsparclcaba prc'
      'LEFT JOIN vspalleta vsp ON vsp.Codigo=prc.VSPallet'
      'LEFT JOIN gragrux ggx ON ggx.Controle=vsp.GraGruX  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'WHERE DtHrFimCla<"1900-01-01"'
      'ORDER BY NO_PRD_TAM_COR, prc.CacCod ')
    Left = 596
    Top = 68
    object QrPalletVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrPalletCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 239
    end
  end
  object DsPallet: TDataSource
    DataSet = QrPallet
    Left = 596
    Top = 116
  end
end
