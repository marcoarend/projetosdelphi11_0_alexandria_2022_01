unit VSClassifOneNw3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, UnInternalConsts,
  dmkDBGridZTO, Vcl.StdCtrls, dmkEdit, Data.DB, mySQLDbTables, dmkGeral,
  Vcl.Mask, Vcl.DBCtrls, dmkDBEdit, UnDmkEnums, Vcl.Menus, UnProjGroup_Vars,
  Vcl.Buttons, dmkDBLookupComboBox, dmkEditCB, UnDmkWeb, ShellAPI,
  UnProjGroup_Consts, mySQLDirectQuery, dmkCheckBox, UnGrl_Geral, UnGrl_Vars,
  UnAppEnums;

type
  TFLsItens = array of array[0..4] of String;
  TFmVSClassifOneNw3 = class(TForm)
    PanelOC: TPanel;
    PnAll: TPanel;
    QrVSPaClaCab: TmySQLQuery;
    DsVSPaClaCab: TDataSource;
    QrVSGerArtNew: TmySQLQuery;
    QrVSGerArtNewCodigo: TIntegerField;
    QrVSGerArtNewControle: TIntegerField;
    QrVSGerArtNewMovimCod: TIntegerField;
    QrVSGerArtNewEmpresa: TIntegerField;
    QrVSGerArtNewMovimID: TIntegerField;
    QrVSGerArtNewGraGruX: TIntegerField;
    QrVSGerArtNewPecas: TFloatField;
    QrVSGerArtNewPesoKg: TFloatField;
    QrVSGerArtNewAreaM2: TFloatField;
    QrVSGerArtNewAreaP2: TFloatField;
    QrVSGerArtNewLk: TIntegerField;
    QrVSGerArtNewDataCad: TDateField;
    QrVSGerArtNewDataAlt: TDateField;
    QrVSGerArtNewUserCad: TIntegerField;
    QrVSGerArtNewUserAlt: TIntegerField;
    QrVSGerArtNewAlterWeb: TSmallintField;
    QrVSGerArtNewAtivo: TSmallintField;
    QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField;
    QrVSGerArtNewSrcMovID: TIntegerField;
    QrVSGerArtNewSrcNivel1: TIntegerField;
    QrVSGerArtNewSrcNivel2: TIntegerField;
    QrVSGerArtNewPallet: TIntegerField;
    QrVSGerArtNewNO_PALLET: TWideStringField;
    QrVSGerArtNewSdoVrtArM2: TFloatField;
    QrVSGerArtNewSdoVrtPeca: TFloatField;
    QrVSGerArtNewObserv: TWideStringField;
    QrVSGerArtNewValorT: TFloatField;
    QrVSGerArtNewMovimTwn: TIntegerField;
    QrVSGerArtNewCustoMOKg: TFloatField;
    QrVSGerArtNewMovimNiv: TIntegerField;
    QrVSGerArtNewTerceiro: TIntegerField;
    QrVSGerArtNewCliVenda: TIntegerField;
    QrVSGerArtNewLnkNivXtr1: TIntegerField;
    QrVSGerArtNewFicha: TIntegerField;
    QrVSGerArtNewLnkNivXtr2: TIntegerField;
    QrVSGerArtNewMisturou: TSmallintField;
    QrVSGerArtNewCustoMOTot: TFloatField;
    QrVSGerArtNewSdoVrtPeso: TFloatField;
    QrVSGerArtNewValorMP: TFloatField;
    QrVSGerArtNewDstMovID: TIntegerField;
    QrVSGerArtNewDstNivel1: TIntegerField;
    QrVSGerArtNewDstNivel2: TIntegerField;
    QrVSGerArtNewQtdGerPeca: TFloatField;
    QrVSGerArtNewQtdGerPeso: TFloatField;
    QrVSGerArtNewQtdGerArM2: TFloatField;
    QrVSGerArtNewQtdGerArP2: TFloatField;
    QrVSGerArtNewQtdAntPeca: TFloatField;
    QrVSGerArtNewQtdAntPeso: TFloatField;
    QrVSGerArtNewQtdAntArM2: TFloatField;
    QrVSGerArtNewQtdAntArP2: TFloatField;
    QrVSGerArtNewNO_FORNECE: TWideStringField;
    QrVSGerArtNewNO_FICHA: TWideStringField;
    QrVSGerArtNewCUSTO_M2: TFloatField;
    QrVSGerArtNewCUSTO_P2: TFloatField;
    DsVSGerArtNew: TDataSource;
    QrVSPaClaCabNO_TIPO: TWideStringField;
    QrVSPaClaCabGraGruX: TIntegerField;
    QrVSPaClaCabNome: TWideStringField;
    QrVSPaClaCabTipoArea: TSmallintField;
    QrVSPaClaCabEmpresa: TIntegerField;
    QrVSPaClaCabMovimCod: TIntegerField;
    QrVSPaClaCabCodigo: TIntegerField;
    QrVSPaClaCabVSGerArt: TIntegerField;
    QrVSPaClaCabLstPal01: TIntegerField;
    QrVSPaClaCabLstPal02: TIntegerField;
    QrVSPaClaCabLstPal03: TIntegerField;
    QrVSPaClaCabLstPal04: TIntegerField;
    QrVSPaClaCabLstPal05: TIntegerField;
    QrVSPaClaCabLstPal06: TIntegerField;
    QrVSPaClaCabLk: TIntegerField;
    QrVSPaClaCabDataCad: TDateField;
    QrVSPaClaCabDataAlt: TDateField;
    QrVSPaClaCabUserCad: TIntegerField;
    QrVSPaClaCabUserAlt: TIntegerField;
    QrVSPaClaCabAlterWeb: TSmallintField;
    QrVSPaClaCabAtivo: TSmallintField;
    QrVSPaClaCabNO_PRD_TAM_COR: TWideStringField;
    QrVSPaClaCabNO_EMPRESA: TWideStringField;
    DsSumT: TDataSource;
    DsAll: TDataSource;
    PnInfoBig: TPanel;
    PnDigitacao: TPanel;
    Label12: TLabel;
    PnBox: TPanel;
    Label2: TLabel;
    EdBox: TdmkEdit;
    PnArea: TPanel;
    Label1: TLabel;
    LaTipoArea: TLabel;
    EdArea: TdmkEdit;
    PnJaClass: TPanel;
    Label15: TLabel;
    Panel10: TPanel;
    Label10: TLabel;
    DBEdJaFoi_PECA: TDBEdit;
    Panel9: TPanel;
    Label9: TLabel;
    DBEdJaFoi_AREA: TDBEdit;
    PnNaoClass: TPanel;
    Label19: TLabel;
    PnIntMei: TPanel;
    Label23: TLabel;
    DBEdSdoVrtPeca: TDBEdit;
    Panel15: TPanel;
    Label25: TLabel;
    DBEdSdoVrtArM2: TDBEdit;
    QrSumVMI: TmySQLQuery;
    QrSumVMIPecas: TFloatField;
    QrSumVMIAreaM2: TFloatField;
    QrSumVMIAreaP2: TFloatField;
    QrVSGerArtNewSerieFch: TIntegerField;
    QrVSGerArtNewNO_SerieFch: TWideStringField;
    PMEncerra: TPopupMenu;
    EstaOCOrdemdeclassificao1: TMenuItem;
    N1: TMenuItem;
    Palletdobox01: TMenuItem;
    Palletdobox02: TMenuItem;
    Palletdobox03: TMenuItem;
    Palletdobox04: TMenuItem;
    Palletdobox05: TMenuItem;
    Palletdobox06: TMenuItem;
    QrVSPaClaCabVSMovIts: TIntegerField;
    QrVSPaClaCabCacCod: TIntegerField;
    QrRevisores: TmySQLQuery;
    QrRevisoresCodigo: TIntegerField;
    QrRevisoresNOMEENTIDADE: TWideStringField;
    DsRevisores: TDataSource;
    QrDigitadores: TmySQLQuery;
    QrDigitadoresCodigo: TIntegerField;
    QrDigitadoresNOMEENTIDADE: TWideStringField;
    DsDigitadores: TDataSource;
    QrVMIsDePal: TmySQLQuery;
    QrVMIsDePalVMI_Dest: TIntegerField;
    PnInfoOC: TPanel;
    LaCacCod: TLabel;
    Label6: TLabel;
    Label22: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label26: TLabel;
    Label24: TLabel;
    Label18: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit19: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit11: TDBEdit;
    PnResponsaveis: TPanel;
    Label71: TLabel;
    Label70: TLabel;
    CBRevisor: TdmkDBLookupComboBox;
    CBDigitador: TdmkDBLookupComboBox;
    EdDigitador: TdmkEditCB;
    EdRevisor: TdmkEditCB;
    PMAll: TPopupMenu;
    Alteraitematual1: TMenuItem;
    Excluiitematual1: TMenuItem;
    N2: TMenuItem;
    Mostrartodosboxes1: TMenuItem;
    QrVSMrtCad: TmySQLQuery;
    DsVSMrtCad: TDataSource;
    QrVSMrtCadCodigo: TIntegerField;
    QrVSMrtCadNome: TWideStringField;
    PnMartelo: TPanel;
    Label72: TLabel;
    CBVSMrtCad: TdmkDBLookupComboBox;
    EdVSMrtCad: TdmkEditCB;
    ImprimirNmeroPallet1: TMenuItem;
    DadosPaletsemWordPad1: TMenuItem;
    QrSumDest1: TmySQLQuery;
    QrSumSorc1: TmySQLQuery;
    QrVMISorc1: TmySQLQuery;
    QrPalSorc1: TmySQLQuery;
    Label85: TLabel;
    EdTempo: TEdit;
    N3: TMenuItem;
    rocarVichaRMP1: TMenuItem;
    PnSubClass: TPanel;
    LaSubClass: TLabel;
    EdSubClass: TdmkEdit;
    PnExtras: TPanel;
    PnEqualize: TPanel;
    PnCfgEqz: TPanel;
    SbEqualize: TSpeedButton;
    Label88: TLabel;
    EdEqualize: TdmkEdit;
    CkEqualize: TCheckBox;
    CkNota: TCheckBox;
    DBGNotaEqz: TDBGrid;
    PnNota: TPanel;
    DBEdNotaEqzM2: TDBEdit;
    QrNotaAll: TmySQLQuery;
    QrNotaAllPecas: TFloatField;
    QrNotaAllAreaM2: TFloatField;
    QrNotaAllAreaP2: TFloatField;
    QrNotaAllBasNota: TFloatField;
    QrNotaAllNotaAll: TFloatField;
    QrNotaAllNotaBrutaM2: TFloatField;
    QrNotaAllNotaEqzM2: TFloatField;
    DsNotaAll: TDataSource;
    QrNotaCrr: TmySQLQuery;
    QrNotaCrrPecas: TFloatField;
    QrNotaCrrAreaM2: TFloatField;
    QrNotaCrrAreaP2: TFloatField;
    QrNotaCrrBasNota: TFloatField;
    QrNotaCrrNotaAll: TFloatField;
    QrNotaCrrNotaBrutaM2: TFloatField;
    QrNotaCrrNotaEqzM2: TFloatField;
    DsNotaCrr: TDataSource;
    QrNotas: TmySQLQuery;
    QrNotasSubClass: TWideStringField;
    QrNotasPecas: TFloatField;
    QrNotasAreaM2: TFloatField;
    QrNotasAreaP2: TFloatField;
    QrNotasBasNota: TFloatField;
    QrNotasNotaM2: TFloatField;
    QrNotasPercM2: TFloatField;
    DsNotas: TDataSource;
    Memo1: TMemo;
    rocarIMEI1: TMenuItem;
    QrVSGerArtNewFatorInt: TFloatField;
    QrSumT: TmySQLQuery;
    QrSumTAreaM2: TFloatField;
    QrSumTSdoVrtPeca: TFloatField;
    QrSumTSdoVrtArM2: TFloatField;
    QrSumTPecas: TFloatField;
    ImprimirfluxodemovimentodoIMEI1: TMenuItem;
    PMIMEI: TPopupMenu;
    ImprimirfluxodemovimentodoIMEI2: TMenuItem;
    QrSumTJaFoi_PECA: TFloatField;
    QrSumTJaFoi_AREA: TFloatField;
    QrVSGerArtNewVSMulFrnCab: TIntegerField;
    SGAll: TStringGrid;
    Panel3: TPanel;
    DqAll: TmySQLDirectQuery;
    DqItens: TmySQLDirectQuery;
    DqSumPall: TmySQLDirectQuery;
    GPBoxes: TGridPanel;
    QrVSPaClaCabDtHrFimCla: TDateTimeField;
    QrVSPaClaCabDtHrAberto: TDateTimeField;
    QrVSPaClaCabLstPal07: TIntegerField;
    QrVSPaClaCabLstPal08: TIntegerField;
    QrVSPaClaCabLstPal09: TIntegerField;
    QrVSPaClaCabLstPal10: TIntegerField;
    QrVSPaClaCabLstPal11: TIntegerField;
    QrVSPaClaCabLstPal12: TIntegerField;
    QrVSPaClaCabLstPal13: TIntegerField;
    QrVSPaClaCabLstPal14: TIntegerField;
    QrVSPaClaCabLstPal15: TIntegerField;
    Palletdobox07: TMenuItem;
    Palletdobox08: TMenuItem;
    Palletdobox09: TMenuItem;
    Palletdobox10: TMenuItem;
    Palletdobox11: TMenuItem;
    Palletdobox12: TMenuItem;
    Palletdobox13: TMenuItem;
    Palletdobox14: TMenuItem;
    Palletdobox15: TMenuItem;
    AumentarBoxesdisponveis1: TMenuItem;
    PnMenu: TPanel;
    CkSubClass: TCheckBox;
    CkMartelo: TCheckBox;
    BtReabre: TBitBtn;
    BtImprime: TBitBtn;
    BtEncerra: TBitBtn;
    EdItens: TEdit;
    N4: TMenuItem;
    TesteInclusao1: TMenuItem;
    QrVSGerArtNewClientMO: TIntegerField;
    QrVSGerArtNewStqCenLoc: TIntegerField;
    QrVSGerArtNewFornecMO: TIntegerField;
    Label4: TLabel;
    BitBtn1: TBitBtn;
    EdAll: TdmkEdit;
    EdArrAll: TdmkEdit;
    SGDefeiDefin: TStringGrid;
    Splitter1: TSplitter;
    QrVSGerArtNewMarca: TWideStringField;
    Memo2: TMemo;
    VerTextoMemo1: TMenuItem;
    ApagarTextoMemo1: TMenuItem;
    CkInverte: TCheckBox;
    QrVSGerArtNewMediaMinM2: TFloatField;
    QrVSGerArtNewMediaMaxM2: TFloatField;
    Qry: TMySQLQuery;
    QrVSGerArtNewMedEClas: TSmallintField;
    procedure FormCreate(Sender: TObject);
    procedure QrVSPaClaCabCalcFields(DataSet: TDataSet);
    procedure EdBoxChange(Sender: TObject);
    procedure QrVSPaClaCabAfterScroll(DataSet: TDataSet);
    procedure QrVSPaClaCabBeforeClose(DataSet: TDataSet);
    procedure PMitensPopup(Sender: TObject);
    procedure RemoverPalletClick(Sender: TObject);
    procedure AdicionarPalletClick(Sender: TObject);
    procedure EncerrarPalletClick(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EstaOCOrdemdeclassificao1Click(Sender: TObject);
    procedure PMEncerraPopup(Sender: TObject);
    procedure EdRevisorRedefinido(Sender: TObject);
    procedure EdDigitadorRedefinido(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Alteraitematual1Click(Sender: TObject);
    procedure Excluiitematual1Click(Sender: TObject);
    procedure Mostrartodosboxes1Click(Sender: TObject);
    procedure EdVSMrtCadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtReabreClick(Sender: TObject);
    procedure ImprimirNmeroPallet1Click(Sender: TObject);
    procedure DadosPaletsemWordPad1Click(Sender: TObject);
    procedure Mensagemdedados1Click(Sender: TObject);
    procedure rocarVichaRMP1Click(Sender: TObject);
    procedure CkSubClassClick(Sender: TObject);
    procedure CkMarteloClick(Sender: TObject);
    procedure EdSubClassKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbEqualizeClick(Sender: TObject);
    procedure CkNotaClick(Sender: TObject);
    procedure CkSubClass3Click(Sender: TObject);
    procedure CkSubClass2Click(Sender: TObject);
    procedure CkSubClass5Click(Sender: TObject);
    procedure CkSubClass6Click(Sender: TObject);
    procedure CkSubClass1Click(Sender: TObject);
    procedure CkSubClass4Click(Sender: TObject);
    function  ObrigaSubClass(Box: Integer; SubClass: String): Boolean;
    procedure EdSubClassExit(Sender: TObject);
    procedure EdSubClassChange(Sender: TObject);
    procedure rocarIMEI1Click(Sender: TObject);
    procedure QrVSGerArtNewAfterOpen(DataSet: TDataSet);
    procedure ImprimirfluxodemovimentodoIMEI1Click(Sender: TObject);
    procedure ImprimirfluxodemovimentodoIMEI2Click(Sender: TObject);
    procedure QrSumTCalcFields(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure CkUsaMemoryClick(Sender: TObject);
    procedure SGAllKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdPecasXXChange(Sender: TObject);
    procedure AumentarBoxesdisponveis1Click(Sender: TObject);
    procedure TesteInclusao1Click(Sender: TObject);
    procedure DBEdSdoVrtPecaChange(Sender: TObject);
    procedure EdAreaEnter(Sender: TObject);
    procedure EdBoxKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SGDefeiDefinKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdAreaChange(Sender: TObject);
    procedure VerTextoMemo1Click(Sender: TObject);
    procedure ApagarTextoMemo1Click(Sender: TObject);
    procedure EdAreaKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdAreaRedefinido(Sender: TObject);
    procedure EdVSMrtCadRedefinido(Sender: TObject);
  private
    { Private declarations }
    FLiberadoPecasNegativas, FCriando, FInverte: Boolean;
    FDifTime: TDateTime;
    FLsAll: array of array[0..12] of String;
    FMartelosCod: array of Integer;
    FMartelosNom: array of String;
    //
    FMaxPecas: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of Double;
    FSGItens: array [0..VAR_CLA_ART_RIB_MAX_BOX_15] of TStringGrid;
    FLsItens: array [0..VAR_CLA_ART_RIB_MAX_BOX_15] of TFLsItens;
    FEdPalletPecas, FEdPalletArea, FEdPalletMedia:
    array [0..VAR_CLA_ART_RIB_MAX_BOX_15] of TdmkEdit;
    FQrVSPallet: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TmySQLQuery;
    FDsVSPallet: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TDataSource;
    FQrVSPalClaIts: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TmySQLQuery;
    FDsVSPalClaIts: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TDataSource;
    FPnTxLx, FPnBox, FPnArt1N, FPnArt2N, FPnArt3N, FPnBxTot, FPnNumBx, FPnIDBox,
    FPnSubXx, FPnIMEICtrl, FPnIMEIImei, FPnPalletID, FPnPalletPecas,
    FPnPalletArea, FPnPalletMedia, FPnIntMei: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TPanel;
    FLaNumBx, FLaIMEICtrl, FLaIMEIImei, FLaPalletID, FLaPalletPecas,
    FLaPalletArea, FLaPalletMedia: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TLabel;
    FCkSubClass: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TCheckBox;
    FCkDuplicaArea: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TCheckBox;
    FDBEdArtNome, FDBEdArtNoSta, FDBEdArtNoCli, FDBEdIMEICtrl, FDBEdIMEIImei,
    FDBEdPalletID: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TDBEdit;
    FGBIMEI, FGBPallet: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TGroupBox;
    FPalletDoBox: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TMenuItem;
    FPMItens: array[0..VAR_CLA_ART_RIB_MAX_BOX_15] of TPopupMenu;
    FClasseVisivel: Boolean;

    function  EncerraOC(Forca: Boolean): Boolean;
    procedure ReopenVSGerArtDst();
    procedure ReopenVSPallet(Box: Integer; QrVSPallet, QrVSPalClaIts:
              TmySQLQuery; Pallet: Integer);
    procedure ReopenItens(Box, VSPaRclIts, VSPallet: Integer);
    procedure InsereCouroAtual();
    procedure InsereCouroArrAll(CacCod, CacID, Codigo, ClaAPalOri, RclAPalOri,
              VSPaClaIts, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest,
              Box: Integer; Pecas, AreaM2, AreaP2: Double; Revisor, Digitador,
              Martelo: Integer; DataHora, SubClass: String; FatorIntSrc,
              FatorIntDst: Double; Controle: Int64);
    function  BoxInBoxes(Box: Integer): Boolean;
    procedure AtualizaInfoOC();
    procedure UpdDelCouroSelecionado(SQLTipo: TSQLType);
    function  BoxDeComp(Compo: TObject): Integer;
    function  ObtemDadosBox(const Box: Integer; var VSPaRclIts, VSPallet,
              VMI_Sorc, VMI_Baix, VMI_Dest: Integer): Boolean;
    function  ObtemQryesBox(const Box: Integer; var QrVSPallet: TmySQLQuery): Boolean;
    procedure RemovePallet(Box: Integer; Reopen: Boolean);
    procedure AdicionaPallet(Box: Integer);
    procedure ReopenSumPal((*QrSumPal: TmySQLQuery;*) Box, VSPallet: Integer);
    function  EncerraPallet(Box: Integer; EncerrandoTodos: Boolean): Boolean;
    procedure LiberaDigitacao();
    //function  SubstituiOC_Antigo(): Boolean;
    function  SubstituiOC_Novo(): Boolean;
    procedure VerificaBoxes();
    procedure MensagemDadosBox(Box: Integer);
    procedure ReopenEqualize(Automatico: Boolean);
    procedure TentarFocarEdArea();
    procedure ConfigPnSubClass();
    procedure ReconfiguraPaineisIntMei(QrVSPallet: TmySQLQuery; Panel: TPanel);
    procedure ImprimeFluxoDeMovimentoDoIMEI();
    //
    procedure ReopenVSMrtCad();
    procedure ReopenAll();
    procedure CarregaSGAll();
    procedure CarregaSGItens(Box: Integer);
    procedure CarregaSGSumPall(Box: Integer; Acao: TAcaoMath; Pecas,
              AreaM2, AreaP2: Double);
    function  ObtemNO_MARTELO(Martelo: Integer): String;
    procedure OcultaMostraClasses();
    procedure InsereDefeitos(CacCod, CacID, Codigo: Integer; CacIts: Int64);
    procedure MostraJanelaDefeitos();
    //
    //procedure AddMemoExe(Txt: String);
  public
    { Public declarations }
    FBoxMax, FCodigo, FCacCod: Integer;
    FMovimID: TEstqMovimID;
    FInRedefinido: Boolean;
    //
    procedure DefineBoxes(Max: Integer);
    procedure ReopenVSPaClaCab();
  end;

var
  FmVSClassifOneNw3: TFmVSClassifOneNw3;

implementation

uses Module, ModuleGeral, DmkDAC_PF, UMySQLModule, UnMyObjects, VSClaArtPalAd3,
  {$IfDef sAllVS}UnVS_PF,{$EndIf}
  MyDBCheck, UnVS_CRC_PF, VSPalNumImp, UnDmkProcFunc, VSClaArtPrpMDz,
  VSClaArtPrpQnz, VSPallet, VSClassifOneDefei, Senha;

{$R *.dfm}
var
  FBoxes, FErrosBox: array [1..VAR_CLA_ART_RIB_MAX_BOX_15] of Boolean;
  //FUsaMemory: Boolean = True;

{
procedure TFmVSClassifOneNw3.AddMemoExe(Txt: String);
begin
  Memo2.Text := Geral.FDT(Now(), 109) + ' : ' + Txt + sLineBreak +
    Memo2.Text;
end;
}

procedure TFmVSClassifOneNw3.AdicionaPallet(Box: Integer);
var
  I: Integer;
begin
  if DBCheck.CriaFm(TFmVSClaArtPalAd3, FmVSClaArtPalAd3, afmoNegarComAviso) then
  begin
    FmVSClaArtPalAd3.FMovimIDGer              := emidClassArtXXUni;
    FmVSClaArtPalAd3.FEmpresa                 := QrVSGerArtNewEmpresa.Value;
    FmVSClaArtPalAd3.FFornecMO                := QrVSGerArtNewFornecMO.Value;
    FmVSClaArtPalAd3.FClientMO                := QrVSGerArtNewClientMO.Value;
    FmVSClaArtPalAd3.FFornecedor              := QrVSGerArtNewTerceiro.Value;
    FmVSClaArtPalAd3.FVSMulFrnCab             := QrVSGerArtNewVSMulFrnCab.Value;
    FmVSClaArtPalAd3.FMovimID                 := FMovimID;
    FmVSClaArtPalAd3.FBxaGraGruX              := QrVSGerArtNewGraGruX.Value;
    FmVSClaArtPalAd3.FBxaMovimID              := QrVSGerArtNewMovimID.Value;
    FmVSClaArtPalAd3.FBxaMovimNiv             := QrVSGerArtNewMovimNiv.Value;
    FmVSClaArtPalAd3.FBxaSrcNivel1            := QrVSGerArtNewCodigo.Value;
    FmVSClaArtPalAd3.FBxaSorcNivel2           := QrVSGerArtNewControle.Value;

    FmVSClaArtPalAd3.EdCodigo.ValueVariant    := QrVSPaClaCabCodigo.Value;
    FmVSClaArtPalAd3.EdSrcPallet.ValueVariant := 0;
    FmVSClaArtPalAd3.FExigeSrcPallet          := False;
    FmVSClaArtPalAd3.EdMovimCod.ValueVariant  := QrVSPaClaCabMovimCod.Value;
    FmVSClaArtPalAd3.EdIMEI.ValueVariant      := QrVSGerArtNewControle.Value;
    FmVSClaArtPalAd3.EdNO_PRD_TAM_COR.ValueVariant := QrVSGerArtNewNO_PRD_TAM_COR.Value;
    FmVSClaArtPalAd3.FBox := Box;
    FmVSClaArtPalAd3.PnBox.Caption := Geral.FF0(Box);
    //
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_15 do
      FmVSClaArtPalAd3.FPal[I] := QrVSPaClaCab.FieldByName('LstPal' +
      Geral.FFF(I, 2)).AsInteger;
    //
    VS_CRC_PF.ReopenVSPallet(FmVSClaArtPalAd3.QrVSPallet,
      FmVSClaArtPalAd3.FEmpresa, FmVSClaArtPalAd3.FClientMO, 0, '', []);
    //
    FmVSClaArtPalAd3.ShowModal;
    FmVSClaArtPalAd3.Destroy;
    //
    ReopenVSPaClaCab();
  end;
end;

procedure TFmVSClassifOneNw3.AdicionarPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  AdicionaPallet(Box);
  //
  if PnDigitacao.Enabled then
    EdArea.SetFocus;
end;

procedure TFmVSClassifOneNw3.Alteraitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stUpd);
end;

procedure TFmVSClassifOneNw3.ApagarTextoMemo1Click(Sender: TObject);
begin
  Memo2.Lines.Clear;
end;

procedure TFmVSClassifOneNw3.AtualizaInfoOC();
var
  AreaT: Double;
begin
  //if not FUsaMemory then
    //ReopenAll();
  //////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumT, Dmod.MyDB, [
  'SELECT Pecas, AreaM2, SdoVrtPeca, SdoVrtArM2',
  'FROM ' + CO_SEL_TAB_VMI,
  'WHERE Controle=' + Geral.FF0(QrVSGerArtNewControle.Value),
  '']);
  //
end;

procedure TFmVSClassifOneNw3.AumentarBoxesdisponveis1Click(Sender: TObject);
var
  I, Boxes, BoxIni, BoxFim: Integer;
begin
  if FBoxMax = VAR_CLA_ART_RIB_MAX_BOX_15 then
  begin
    Geral.MB_Info('A quantidade m�xima de boxes j� foi atingida!');
  end else
  begin
    Boxes := MyObjects.SelRadioGroup('Aumento de disponibilidade de Boxes',
    'Selecione a quantidade', [
    '06', '07', '08', '09', '10', '11', '12', '13', '14', '15'], 5, FBoxMax - 5);
    Boxes := Boxes + 6;
    //
    if Boxes > FBoxMax then
    begin
      BoxIni := FBoxMax + 1;
      BoxFim := Boxes;
      for I := 1 to FBoxMax do
      begin
        //FPnTxLx[I].Destroy;
        FPnTxLx[I].Free;
      end;
      DefineBoxes(BoxFim);
      ReopenVSPaClaCab();
      if not Mostrartodosboxes1.Checked then
        Mostrartodosboxes1Click(Mostrartodosboxes1);
    end;
  end;
end;

procedure TFmVSClassifOneNw3.BitBtn1Click(Sender: TObject);
begin
  ReopenAll();
end;

function TFmVSClassifOneNw3.BoxDeComp(Compo: TObject): Integer;
var
  CompName: String;
begin
  CompName := TMenuItem(Compo).Name;
  CompName := Copy(CompName, Length(CompName) -1);

  Result := Geral.IMV(CompName);
end;

function TFmVSClassifOneNw3.BoxInBoxes(Box: Integer): Boolean;
begin
  if (Box > 0) and (Box <= FBoxMax) then
    Result := FBoxes[Box]
  else
    Result := False;
end;

procedure TFmVSClassifOneNw3.BtEncerraClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMEncerra, BtEncerra);
end;

procedure TFmVSClassifOneNw3.BtImprimeClick(Sender: TObject);
begin
  VS_CRC_PF.MostraFormVSMovImp(False, nil, nil, 4);
end;

procedure TFmVSClassifOneNw3.BtReabreClick(Sender: TObject);
begin
  ReopenVSPaClaCab();
end;

procedure TFmVSClassifOneNw3.CarregaSGAll();
var
  I, J, K, Linhas: Integer;
begin
  K := Length(FLsAll);
  SGAll.RowCount := K + 1;
  J := 0;
  for I := K -1 downto 0 do
  begin
    J := J + 1;
    SGAll.Cells[00, J] := IntToStr(K - J + 1);
    SGAll.Cells[01, J] := FLsAll[I, 01];   // Box
    SGAll.Cells[02, J] := FLsAll[I, 02];   // SubClass
    SGAll.Cells[03, J] := FLsAll[I, 03];   // AreaM2
    SGAll.Cells[04, J] := FLsAll[I, 04];   // NO_MARTELO
    SGAll.Cells[05, J] := FLsAll[I, 05];   // VMI_Dest
    SGAll.Cells[06, J] := FLsAll[I, 06];   // Controle
    SGAll.Cells[07, J] := FLsAll[I, 07];   // VSPaRclIts
    SGAll.Cells[08, J] := FLsAll[I, 08];   // VSPallet
    SGAll.Cells[09, J] := FLsAll[I, 09];   // Pecas
    SGAll.Cells[10, J] := FLsAll[I, 10];   // AreaP2
    SGAll.Cells[11, J] := FLsAll[I, 11];   // VMI_Sorc
    SGAll.Cells[12, J] := FLsAll[I, 12];   // VMI_Dest
  end;
end;

procedure TFmVSClassifOneNw3.CarregaSGItens(Box: Integer);
var
  I, J, K, Linhas: Integer;
begin
  K := Length(FLsItens[Box]);
  FSGItens[Box].RowCount := K + 1;
  J := 0;
  for I := K -1 downto 0 do
  begin
    J := J + 1;
    FSGItens[Box].Cells[00, J] := IntToStr(K - J + 1);
    case QrVSPaClaCabTipoArea.Value of
      0: FSGItens[Box].Cells[01, J] := FLsItens[Box][I, 03];
      1: FSGItens[Box].Cells[01, J] := FLsItens[Box][I, 04];
      else FSGItens[Box].Cells[01, J] := '?.??';
    end;
(*
    SGItens[Box].Cells[01, J] := FLsItens[Box][I, 01];   // Controle
    SGItens[Box].Cells[02, J] := FLsItens[Box][I, 02];   // Pecas
    SGItens[Box].Cells[03, J] := FLsItens[Box][I, 03];   // AreaM2
    SGItens[Box].Cells[04, J] := FLsItens[Box][I, 04];   // AreaP2
*)
  end;
end;

procedure TFmVSClassifOneNw3.CarregaSGSumPall(Box: Integer; Acao: TAcaoMath;
  Pecas, AreaM2, AreaP2: Double);
var
  Area, P, A, M: Double;
begin
  if QrVSPaClaCabTipoArea.Value = 0 then
    Area := AreaM2
  else
    Area := AreaP2;
  //
  case Acao of
    amathZera:
    begin
      P := 0;
      A := 0;
    end;
    amathSubtrai:
    begin
      P := FEdPalletPecas[Box].ValueVariant - Pecas;
      A := FEdPalletArea[Box].ValueVariant - Area;
    end;
    amathSoma:
    begin
      P := FEdPalletPecas[Box].ValueVariant + Pecas;
      A := FEdPalletArea[Box].ValueVariant + Area;
    end;
    amathSubstitui:
    begin
      P := Pecas;
      A := Area;
    end;
  end;
  if P = 0 then
    M := 0
  else
    M := A / P;
  //
  FEdPalletPecas[Box].ValueVariant := P;
  FEdPalletArea[Box].ValueVariant  := A;
  FEdPalletMedia[Box].ValueVariant := M;
(*
    SGSumPall[Box].Cells[02, J] := FLsSumPall[Box][I, 01];   // Pecas
    SGSumPall[Box].Cells[03, J] := FLsSumPall[Box][I, 02];   // AreaM2
    SGSumPall[Box].Cells[04, J] := FLsSumPall[Box][I, 03];   // AreaP2
*)
end;

procedure TFmVSClassifOneNw3.CkMarteloClick(Sender: TObject);
begin
  PnMartelo.Visible := CkMartelo.Checked;
end;

procedure TFmVSClassifOneNw3.CkNotaClick(Sender: TObject);
begin
  PnNota.Visible := CkNota.Checked;
  ReopenEqualize(not CkNota.Checked);
end;

procedure TFmVSClassifOneNw3.CkSubClass1Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNw3.CkSubClass2Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNw3.CkSubClass3Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNw3.CkSubClass4Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNw3.CkSubClass5Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNw3.CkSubClass6Click(Sender: TObject);
begin
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNw3.CkSubClassClick(Sender: TObject);
begin
  ConfigPnSubClass();
end;

procedure TFmVSClassifOneNw3.CkUsaMemoryClick(Sender: TObject);
begin
  //FUsaMemory := CkUsaMemory.Checked;
end;

procedure TFmVSClassifOneNw3.ConfigPnSubClass();
var
  I: Integer;
  Check: Boolean;
begin
  //if not CkSubClass.Checked then
  begin
    Check := False;
    for I := 1 to FBoxMax do
      if (FCkSubClass[I] <> nil) and
      FCkSubClass[I].Checked and FPnBox[I].Visible then Check := True;
  end;
  PnSubClass.Visible := Check and CkSubClass.Checked;
end;

procedure TFmVSClassifOneNw3.DadosPaletsemWordPad1Click(Sender: TObject);
(*
  procedure MostraCaptura();
  var
    Caminho: String;
  begin
    Caminho := ExtractFileDir(Application.ExeName) + '\Mensagem.txt';
    if FileExists(Caminho) then
      DeleteFile(Caminho);
    ReAviso.Lines.SaveToFile(Caminho);
    ShellExecute(GetDesktopWindow, 'open', pchar(Caminho), nil, nil, sw_ShowNormal);
  end;
*)
var
  Dir, NomeArq, Arq: String;
begin
  Dir := 'C:\Dermatek\OCsCapt\';
  NomeArq := 'OC' + DBEdCodigo.Text +
    Geral.SoNumero_TT(Geral.FDT(DModG.ObtemAgora(), 109)) + '.jpg';
  try
    Arq := DmkWeb.CapturaTela(Dir, NomeArq);
    ShellExecute(Application.Handle, 'open', pchar(Arq), nil, nil, sw_ShowNormal);
  except
    ForceDirectories(Dir);
    raise;
  end;
end;

procedure TFmVSClassifOneNw3.DBEdSdoVrtPecaChange(Sender: TObject);
var
  CorTexto, CorFundo: TColor;
begin
  CorTexto := clWindowText;
  CorFundo := clWindow;
  if (QrSumT.State <> dsInactive) and (QrSumTSdoVrtPeca.Value = 0) then
  begin
    CorTexto := clYellow;
    CorFundo := clBlack;
  end else
  if (QrSumT.State <> dsInactive) and (QrSumTSdoVrtPeca.Value < 0) then
  begin
    CorTexto := clRed;
    CorFundo := clYellow;
  end;
  DBEdSdoVrtPeca.Color := CorFundo;
  DBEdSdoVrtPeca.Font.Color := CorTexto;
  DBEdSdoVrtArM2.Color := CorFundo;
  DBEdSdoVrtArM2.Font.Color := CorTexto;
  EdArea.Color := CorFundo;
  EdArea.Font.Color := CorTexto;
  EdBox.Color := CorFundo;
  EdBox.Font.Color := CorTexto;
end;

procedure TFmVSClassifOneNw3.DefineBoxes(Max: Integer);
var
  I, Col, Row: Integer;
begin
  GPBoxes.Visible := False;
  try
    if FBoxMax < Max then
      FBoxMax := Max;
    //
    GPBoxes.ColumnCollection.BeginUpdate;
    GPBoxes.ColumnCollection.Clear;
    GPBoxes.RowCollection.BeginUpdate;
    GPBoxes.RowCollection.Clear;
    case FBoxMax of
      00..06: begin Col := 3; Row := 2; end;
      07..09: begin Col := 3; Row := 3; end;
      10..12: begin Col := 4; Row := 3; end;
      13..15: begin Col := 5; Row := 3; end;
    end;
    for I := 0 to Col - 1 do
      GPBoxes.ColumnCollection.Add;
    for I := 0 to Row - 1 do
      GPBoxes.RowCollection.Add;
    //
    GPBoxes.ColumnCollection.EndUpdate;
    GPBoxes.RowCollection.EndUpdate;
    for I := 1 to FBoxMax do
    begin
      VS_CRC_PF.CriaBoxEmGridPanel(I, Self, GPBoxes, FPnTxLx, FPnBox, FPnArt1N,    // *&�
      FPnArt2N, FPnArt3N, FPnNumBx, FPnIDBox, FPnSubXx, FPnBxTot, FPnIMEIImei,
      FPnIMEICtrl, FPnPalletMedia, FPnPalletArea, FPnPalletPecas, FPnPalletID,
      FPnIntMei, FLaNumBx, FLaIMEIImei, FLaIMEICtrl, FLaPalletMedia, FLaPalletArea,
      FLaPalletPecas, FLaPalletID, FCkSubClass, FCkDuplicaArea, FDBEdArtNome, FDBEdArtNoCli,
      FDBEdArtNoSta, FDBEdIMEIImei, FDBEdIMEICtrl, FDBEdPalletID, FGBIMEI,
      FGBPallet, FEdPalletMedia, FEdPalletArea, FEdPalletPecas, FSGItens,
      FPMItens);
      FEdPalletPecas[I].OnChange := EdPecasXXChange;
      //
      FBoxes[I] := False;
      FErrosBox[I] := False;
      //
      FQrVSPallet[I] := TmySQLQuery.Create(Dmod);
      FDsVSPallet[I] := TDataSource.Create(Self);
      FDsVSPallet[I].DataSet := FQrVSPallet[I];
      FDBEdPalletID[I].DataSource := FDsVSPallet[I];
      FDBEdArtNome[I].DataSource  := FDsVSPallet[I];
      FDBEdArtNoCli[I].DataSource := FDsVSPallet[I];
      FDBEdArtNoSta[I].DataSource := FDsVSPallet[I];
      //
      FQrVSPalClaIts[I] := TmySQLQuery.Create(Dmod);
      FDsVSPalClaIts[I] := TDataSource.Create(Self);
      FDsVSPalClaIts[I].DataSet := FQrVSPalClaIts[I];
      FDBEdIMEICtrl[I].DataSource := FDsVSPalClaIts[I];
      FDBEdIMEIImei[I].DataSource := FDsVSPalClaIts[I];
      //
    end;
    //for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_15
    FPalletDoBox[01] := PalletDoBox01;
    FPalletDoBox[02] := PalletDoBox02;
    FPalletDoBox[03] := PalletDoBox03;
    FPalletDoBox[04] := PalletDoBox04;
    FPalletDoBox[05] := PalletDoBox05;
    FPalletDoBox[06] := PalletDoBox06;
    FPalletDoBox[07] := PalletDoBox07;
    FPalletDoBox[08] := PalletDoBox08;
    FPalletDoBox[09] := PalletDoBox09;
    FPalletDoBox[10] := PalletDoBox10;
    FPalletDoBox[11] := PalletDoBox11;
    FPalletDoBox[12] := PalletDoBox12;
    FPalletDoBox[13] := PalletDoBox13;
    FPalletDoBox[14] := PalletDoBox14;
    FPalletDoBox[15] := PalletDoBox15;
  finally
    GPBoxes.Visible := True;
  end;
  //
end;

procedure TFmVSClassifOneNw3.EdAreaChange(Sender: TObject);
begin
    //AddMemoExe('EdAreaChange: ' + EdArea.Text);
end;

procedure TFmVSClassifOneNw3.EdAreaEnter(Sender: TObject);
begin
  FInverte := False;
  //AddMemoExe('EdAreaEnter');
end;

procedure TFmVSClassifOneNw3.EdAreaKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //AddMemoExe('EdAreaKeyDown inicio');
  if Key = VK_F5 then
  begin
    //AddMemoExe('EdAreaKeyDown VK_F5 inicio');
    MostraJanelaDefeitos();
    //AddMemoExe('EdAreaKeyDown VK_F5 Fim');
  end;
  // ini 2023-05-06
  {
  // ini 2023-04-13
  if Key = VK_F12 then
  begin
    EdArea.ValueVariant := EdArea.ValueVariant * 2;
  end;
  // fim 2023-04-13
  }
  // fim 2023-05-06
end;

procedure TFmVSClassifOneNw3.EdAreaRedefinido(Sender: TObject);
var
  AreaA, AreaB: Double;
begin
  if not FInRedefinido then
  begin
    FInRedefinido := True;
    if CkInverte.Checked then
    begin
      AreaA := EdArea.ValueVariant / 100;
      case QrVSPaClaCabTipoArea.Value of
        0: AreaB := Geral.ConverteArea(AreaA, ctP2toM2, cfCento);
        1: AreaB := Geral.ConverteArea(AreaA, ctM2toP2, cfQuarto);
        else AreaB := 0;
      end;
      EdArea.ValueVariant := Int(AreaB * 100);
    end;
    FInRedefinido := False;
  end;
end;

procedure TFmVSClassifOneNw3.EdBoxChange(Sender: TObject);
var
  Box: Integer;
begin
  if (FBoxMax > 9) and (Length(EdBox.Text) < 2) then
    Exit;
  Box := EdBox.ValueVariant;
  if BoxInBoxes(Box) then
  begin
    if FErrosBox[Box] = True then
    begin
      Geral.MB_Aviso('Box ou artigo com necessidade de configura��o!');
      Exit;
    end;
    if PnSubClass.Visible then
      EdSubClass.SetFocus
    else
    if PnMartelo.Visible then
      EdVSMrtCad.SetFocus
    else
      InsereCouroAtual();
  end else
  if (EdBox.Text = '-') then
    UpdDelCouroSelecionado(stDel);
end;

procedure TFmVSClassifOneNw3.EdBoxKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    MostraJanelaDefeitos()
end;

procedure TFmVSClassifOneNw3.EdDigitadorRedefinido(Sender: TObject);
begin
  LiberaDigitacao();
end;

procedure TFmVSClassifOneNw3.EdPecasXXChange(Sender: TObject);
var
  Box: Integer;
begin
  Box := Geral.IMV(Copy(TdmkEdit(Sender).Name, 8));
  if TdmkEdit(Sender).ValueVariant >= FMaxPecas[Box] then
  begin
    TdmkEdit(Sender).Font.Color := clRed;
    TdmkEdit(Sender).Color := clYellow;
  end else
  begin
    TdmkEdit(Sender).Font.Color := clBlue;
    TdmkEdit(Sender).Color := clWindow;
  end;
end;

procedure TFmVSClassifOneNw3.EdRevisorRedefinido(Sender: TObject);
begin
  LiberaDigitacao();
end;

procedure TFmVSClassifOneNw3.EdSubClassChange(Sender: TObject);
var
  Texto: String;
begin
  if pos(#13, EdSubClass.Text) > 0 then
  begin
    Texto := EdSubClass.ValueVariant;
    Texto := Geral.Substitui(Texto, #13, '');
    Texto := Geral.Substitui(Texto, #10, '');
    EdSubClass.Text := Texto;
    if EdSubClass.Focused then
      EdSubClass.SelStart := EdSubClass.SelLength;
  end;
end;

procedure TFmVSClassifOneNw3.EdSubClassExit(Sender: TObject);
var
  Box: Integer;
begin
  Box := EdBox.ValueVariant;
  if BoxInBoxes(Box) then
  begin
    if ObrigaSubClass(Box, EdSubClass.Text) then
      EdSubClass.SetFocus;
  end;
end;

procedure TFmVSClassifOneNw3.EdSubClassKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Box: Integer;
begin
  if Key = VK_F5 then
    MostraJanelaDefeitos()
  else
  if Key = VK_RETURN then
  begin
    Box := EdBox.ValueVariant;
    if BoxInBoxes(Box) then
    begin
      if ObrigaSubClass(Box, EdSubClass.Text) then
      begin
        Key := 0;
        EdSubClass.SetFocus;
        Exit;
      end;
    end;
    if PnMartelo.Visible then
      EdVSMrtCad.SetFocus
    else
      InsereCouroAtual();
  end;
  //FormKeyDown(Sender, Key, Shift);
end;

procedure TFmVSClassifOneNw3.EdVSMrtCadKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then
    MostraJanelaDefeitos()
  // ini 2022-03-16
(*
  else
  if Key = VK_RETURN then
    InsereCouroAtual();
*)
  // fim 2022-03-16
end;

procedure TFmVSClassifOneNw3.EdVSMrtCadRedefinido(Sender: TObject);
begin
  // ini 2022-03-16
  if Length(Trim(EdVSMrtCad.ValueVariant)) >  0 then
    InsereCouroAtual();
  // fim 2022-03-16
end;

function TFmVSClassifOneNw3.EncerraOC(Forca: Boolean): Boolean;
const
  LstPal01 = 0;
  LstPal02 = 0;
  LstPal03 = 0;
  LstPal04 = 0;
  LstPal05 = 0;
  LstPal06 = 0;
  LstPal07 = 0;
  LstPal08 = 0;
  LstPal09 = 0;
  LstPal10 = 0;
  LstPal11 = 0;
  LstPal12 = 0;
  LstPal13 = 0;
  LstPal14 = 0;
  LstPal15 = 0;
var
  DtHrFimCla: String;
  Codigo, I, Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Dest: Integer;
  Continua: Boolean;
begin
  Result := False;
  if Forca or (Geral.MB_Pergunta(
  'Confirma relamente o encerramento de toda ordem de classifica��o?' +
  sLineBreak + 'AVISO: Esta a��o N�O encerra nenhum Pallet!' + sLineBreak +
  'Para encerrar um ou mais pallets cancele esta a��o e clique com o bot�o contr�rio do mouse na grade desejada!'
  ) = ID_YES) then
  begin
    { DESATIVADO! Encerra todos pallets em aberto na OC!
    Continua := True;
    for I := 1 to VAR_CLA_ART_RIB_MAX_BOX do
    begin
      if Continua then
      begin
        Box := I;
        ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest);
        if VMI_Dest <> 0 then
          Continua := EncerraPallet(Box, True)
        else
          Continua := True;
      end;
    end;
    if not Continua then
    begin
      ReopenVSPaClaCab();
      Geral.MB_Erro('N�o foi poss�vel encerrar toda ordem!');
      Exit;
    end;
    }
    DtHrFimCla := Geral.FDT(DModG.ObtemAgora(), 109);
    //
    Codigo     := QrVSGerArtNewCodigo.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vsgerarta', False, [
    'DtHrFimCla'], ['Codigo'], [DtHrFimCla], [Codigo], True) then
    begin
      //
      Codigo := QrVSPaClaCabCodigo.Value;
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspaclacaba', False, [
      //'DtHrFimCla'], ['Codigo'], [DtHrFimCla
      'DtHrFimCla',
      'LstPal01', 'LstPal02', 'LstPal03',
      'LstPal04', 'LstPal05', 'LstPal06',
      'LstPal07', 'LstPal08', 'LstPal09',
      'LstPal10', 'LstPal11', 'LstPal12',
      'LstPal13', 'LstPal14', 'LstPal15'
      ], ['Codigo'], [
      DtHrFimCla,
      LstPal01, LstPal02, LstPal03,
      LstPal04, LstPal05, LstPal06,
      LstPal07, LstPal08, LstPal09,
      LstPal10, LstPal11, LstPal12,
      LstPal13, LstPal14, LstPal15
      ], [Codigo], True);
(*
      then
      begin
        if not SubstituiOC() then
          Close;
      end;
*)
    end;
  end;

end;

function TFmVSClassifOneNw3.EncerraPallet(Box: Integer; EncerrandoTodos: Boolean): Boolean;
const
  Pergunta = True;
var
  VSPaClaIts, VMI_Sorc, VMI_Baix, VMI_Dest, VSPallet: Integer;
  FromBox: Variant;
begin
  Result := False;
  //
  FromBox := Box;
  if not ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest) then
    Exit;
  Result := VS_CRC_PF.EncerraPalletNew(VSPallet, Pergunta);
  ReopenVSPaClaCab();
end;

procedure TFmVSClassifOneNw3.EncerrarPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  if EncerraPallet(Box, False) then
  begin
    if Geral.MB_Pergunta('Pallet encerrado e removido!' + sLineBreak +
    'Deseja adicionar outro em seu lugar?') = ID_Yes then
      AdicionaPallet(Box)
    else
      ReopenVSPaClaCab();
  end;
end;

procedure TFmVSClassifOneNw3.EstaOCOrdemdeclassificao1Click(Sender: TObject);
begin
  VS_CRC_PF.AtualizaAreaOrigemSemAreaPreDefinida(QrVSGerArtNewControle.Value);
  if EncerraOC(False) then
  begin
    if not SubstituiOC_Novo() then
      Close;
  end;
end;

procedure TFmVSClassifOneNw3.TesteInclusao1Click(Sender: TObject);
var
  Vezes: Integer;
  V_Txt: String;
  I: Integer;
  //
var
  Ant, Box, Desvio: Integer;
  OK: Boolean;
  Area, Fator, FtMeio, SdoVrtArM2: Double;
  Boxes, Revisor, Digitador: Integer;
begin
  if not DBCheck.LiberaPelaSenhaAdmin then
    Exit;
  //
  FtMeio         := 1;
  Revisor        := EdRevisor.ValueVariant;
  Digitador      := EdDigitador.ValueVariant;
  //
  if MyObjects.FIC(Revisor = 0, EdRevisor, 'Informe o classificador!') then
    Exit;
  if MyObjects.FIC(Digitador = 0, EdDigitador, 'Informe o digitador!') then
    Exit;
  //
  SdoVrtArM2 := QrSumTSdoVrtArM2.Value;
  if SdoVrtArM2 <= 0 then
  begin
    V_Txt := '0,00';
    if InputQuery('�rea m� total a classificar', 'Informe a �rea m� total a classificar:', V_Txt) then
    begin
      SdoVrtArM2 := Geral.DMV(V_Txt);
      if SdoVrtArM2 <= 0 then
        Exit;
    end else
      Exit;
  end;
  //
  V_Txt := DBEdSdoVrtPeca.Text;
  if InputQuery('Itens a classificar', 'Informe a quantidade de vezes:', V_Txt) then
  begin
    Vezes := Geral.IMV(V_Txt);
    //
    Boxes := 0;
    for I := 1 to FBoxMax do
    begin
      if FBoxes[I] then
        Boxes := Boxes + 1;
    end;
    //
    Ant := 0;
    for I := 1 to Vezes do
    begin
      if QrSumTSdoVrtPeca.Value = 0 then
        Exit;
      // BOX
      OK := False;
      while not OK do
      begin
        Randomize();
        //Box := Trunc(Random(1000) / 166.666); //  Random(6)
        Box := Trunc(Random(1000) / 66.666666666); //  Random(15)
        if BoxInBoxes(Box) then
        begin
          if ((Boxes > 2) and (Ant <> Box))
          or (Boxes <=2) then
          begin
            OK := True;
            EdBox.ValueVariant := Box;
            if FPnIntMei[Box].Visible then
              FtMeio := 0.5
            else
              FtMeio := 1;
            Ant := Box;
          end;
        end;
      end;
      if QrSumTSdoVrtPeca.Value > 1 then
      begin
        //Area := QrSumTSdoVrtArM2.Value / QrSumTSdoVrtPeca.Value;
        Area := SdoVrtArM2 / QrSumTSdoVrtPeca.Value;
        Randomize;
        Desvio := Random(3000);
        Fator := 1 + ((Desvio - 1500) / 10000);
        Area := Area * Fator * FtMeio;
      end else
        Area := QrSumTSdoVrtArM2.Value;
      //
      SdoVrtArM2 := SdoVrtArM2 - Area;
      //
      EdArea.ValueVariant := Area * 100;
      InsereCouroAtual();
    end;
  end;
end;

procedure TFmVSClassifOneNw3.Excluiitematual1Click(Sender: TObject);
begin
  UpdDelCouroSelecionado(stDel);
end;

procedure TFmVSClassifOneNw3.UpdDelCouroSelecionado(SQLTipo: TSQLType);
const
  Pergunta = 'Deseja realmente excluir este couro?';
  Tabela =  'vscacitsa';
  Campo = 'Controle';
var
  Box, All_VSPaClaIts, All_VSPallet, Box_VSPaClaIts, Box_VSPallet,
  Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest: Integer;
  Controle: Int64;
  QrVSPallet(*, QrItens, QrSum, QrSumPal*): TmySQLQuery;
  Pecas, AreaM2, AreaP2, AntM2, AntP2, ValorUni: Double;
  Txt: String;
  Continua: Boolean;
  //
  SelTop, SelBot, cIni, cFim, Count, I: Integer;
begin
  SelTop := SGAll.Selection.Top;
  SelBot := SGAll.Selection.Bottom;
  Count  := Length(FLsAll);
  cIni   := Count - SelTop + 1;
  cFim   := Count - SelBot + 1;
  Geral.MB_Info(IntToStr(cIni) + ' ate ' + IntToStr(cFim));
  //
(*
  FLsAll[I, 00] := IntToStr(I + 1);
  FLsAll[I, 01] := IntToStr(Box);                          // Box
  FLsAll[I, 02] := SubClass;                               // SubClass
  FLsAll[I, 03] := Geral.FFT_Dot(AreaM2, 2, siNegativo);   // AreaM2
  FLsAll[I, 04] := ObtemNO_MARTELO(Martelo);               // NO_MARTELO
  FLsAll[I, 05] := IntToStr(VMI_Dest);                     // VMI_Dest
  FLsAll[I, 06] := IntToStr(Controle);                     // Controle
  FLsAll[I, 07] := IntToStr(VSPaClaIts);                   // VSPaClaIts
  FLsAll[I, 08] := IntToStr(VSPallet);                     // VSPallet
  FLsAll[I, 09] := Geral.FFT_Dot(Pecas, 3, siNegativo);    // Pecas
  FLsAll[I, 10] := Geral.FFT_Dot(AreaP2, 2, siNegativo);   // AreaP2
  FLsAll[I, 11] := IntToStr(VMI_Sorc);                     // VMI_Sorc
  FLsAll[I, 12] := IntToStr(VMI_Dest);                     // VMI_Dest
*)
  //cIni := Count - cIni + 1;
  //cFim := Count - cFim + 1;
  //
  for I := cIni -1 downto cFim - 1 do
  begin
    if (I < Length(FLsAll)) then
    begin
      //Box := QrAllBox.Value;
      Box := Geral.IMV(FLsAll[I, 01]); //QrAllBox.Value;
      if not ObtemQryesBox(Box, QrVSPallet) then
        Exit;
      if not ObtemDadosBox(Box, Box_VSPaClaIts, Box_VSPallet, Box_VMI_Sorc,
      Box_VMI_Baix, Box_VMI_Dest) then
        Exit;
      //Controle       := QrAllControle.Value;
      Controle       := Geral.I64(FLsAll[I, 06]);
      //All_VSPaClaIts := QrAllVSPaClaIts.Value;
      All_VSPaClaIts := Geral.IMV(FLsAll[I, 07]);
      //All_VSPallet   := QrAllVSPallet.Value;
      All_VSPallet   := Geral.IMV(FLsAll[I, 08]);
      //
      Continua := False;
      //
      if (All_VSPaClaIts <> Box_VSPaClaIts) or (Box_VSPallet <> All_VSPallet) then
      begin
        Geral.MB_Aviso('Item finalizado! N�o pode mais ser exclu�do ou alterado!');
        Exit;
      end;
      case SQLTipo of
        stUpd:
        begin
          Pecas  := 1;
          //
          if QrVSPaClaCabTipoArea.Value = 0 then
            //Txt := FloatToStr(QrAllAreaM2.Value * 100)
            Txt := Geral.SoNumero_TT(FLsAll[I, 03])
          else
            //Txt := FloatToStr(QrAllAreaP2.Value * 100);
            Txt := Geral.SoNumero_TT(FLsAll[I, 10]);
          //
          if InputQuery('Altera��o de dados', 'Altera��o de dados: (Apenas n�meros sem v�rgula)', Txt) then
          begin
            AreaM2 := Geral.IMV(Txt);
            //
            if QrVSPaClaCabTipoArea.Value = 0 then
            begin
              AreaM2 := AreaM2 / 100;
              AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
            end else
            begin
              AreaP2 := AreaM2 / 100;
              AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
            end;
            AntM2  := Geral.DMV_Dot(FLsAll[I, 03]);
            AntP2  := Geral.DMV_Dot(FLsAll[I, 03]);
            Continua := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vscacitsa', False, [
                          'AreaM2', 'AreaP2'], ['Controle'], [AreaM2, AreaP2],
                          [Controle], VAR_InsUpd_AWServerID);
            FLsAll[I, 03] := Geral.FFT_Dot(AreaM2, 2, siNegativo);
            FLsAll[I, 10] := Geral.FFT_Dot(AreaP2, 2, siNegativo);
            //
            CarregaSGSumPall(Box, amathSubtrai, Pecas, AntM2, AntP2);
            CarregaSGSumPall(Box, amathSoma, Pecas, AreaM2, AreaP2);
            //
          end;
        end;
        stDel:
        begin
          // ini 2023-11-15
          //Continua := VS_CRC_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo, Controle, Dmod.MyDB) = ID_YES;
          Pecas  := Geral.DMV_Dot(FLsAll[I, 09]);
          AreaM2 := Geral.DMV_Dot(FLsAll[I, 03]);
          AreaP2 := Geral.DMV_Dot(FLsAll[I, 10]);
          //
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT ValorT/AreaM2 ',
          'FROM vsmovits ',
          'WHERE Controle=' + Geral.FF0(Box_VMI_Sorc),
          '']);
          ValorUni := Qry.Fields[0].AsFloat * AreaM2;
          Continua := VS_CRC_PF.ExcluiVSNaoVMI(Pergunta, Tabela, Campo, Controle, Dmod.MyDB) = ID_YES;
          if Continua then
          begin
            if Box_VMI_Baix <> 0 then
            begin
              UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, Geral.ATS([
              'UPDATE vsmovits SET ',
              '  Pecas=Pecas+' + Geral.FFT_Dot(Pecas, 3, siPositivo),
              ', AreaM2=AreaM2+' + Geral.FFT_Dot(AreaM2, 2, siPositivo),
              ', AreaP2=AreaP2+' + Geral.FFT_Dot(AreaP2, 2, siPositivo),
              ', ValorT=ValorT+' + Geral.FFT_Dot(ValorUni, 4, siPositivo),
              'WHERE Controle=' + Geral.FF0(Box_VMI_Baix),
              '']));
            end;
          end;
          // fim 2023-11-15
          CarregaSGSumPall(Box, amathSubtrai, Pecas, AreaM2, AreaP2);
        end;
        else
          Geral.MB_Erro('N�o implementado!');
      end;
      if Continua then
      begin
        VS_CRC_PF.AtualizaVMIsDeBox(Box_VSPallet, Box_VMI_Dest, Box_VMI_Baix,
        Box_VMI_Sorc, QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
        //
        if PnDigitacao.Enabled then
        begin
          EdBox.ValueVariant  := 0;
          EdArea.ValueVariant := 0;
          EdVSMrtCad.Text     := '';
          CBVSMrtCad.KeyValue := Null;
          EdArea.SetFocus;
          EdArea.SelectAll;
        end;
        //
        ReopenItens(Box, All_VSPaClaIts, All_VSPallet);
        AtualizaInfoOC();
      end;
    end else
      Geral.MB_Aviso('Item n�o existe!');
  end;
  ReopenAll();
end;

procedure TFmVSClassifOneNw3.VerificaBoxes();
var
  LstPal, I: Integer;
begin
(*
  if not Mostrartodosboxes1.Checked then
  begin
    PnBoxT1L1.Visible := QrVSPaClaCabLstPal01.Value <> 0;
    PnBoxT1L2.Visible := QrVSPaClaCabLstPal02.Value <> 0;
    PnBoxT1L3.Visible := QrVSPaClaCabLstPal03.Value <> 0;
    PnBoxT2L1.Visible := QrVSPaClaCabLstPal04.Value <> 0;
    PnBoxT2L2.Visible := QrVSPaClaCabLstPal05.Value <> 0;
    PnBoxT2L3.Visible := QrVSPaClaCabLstPal06.Value <> 0;
  end else
  begin
    PnBoxT1L1.Visible := True;
    PnBoxT1L2.Visible := True;
    PnBoxT1L3.Visible := True;
    PnBoxT2L1.Visible := True;
    PnBoxT2L2.Visible := True;
    PnBoxT2L3.Visible := True;
  end;
*)
  if not Mostrartodosboxes1.Checked then
  begin
    for I := 1 to FBoxMax do
    begin
      if FPnBox[I] <> nil then
      begin
        LstPal := QrVSPaClaCab.FieldByName('LstPal' + Geral.FFF(I, 2)).AsInteger;
        FPnBox[I].Visible := LstPal <> 0;
      end;
    end;
  end else
  begin
    for I := 1 to FBoxMax do
    begin
      if FPnBox[I] <> nil then
        FPnBox[I].Visible := True;
    end;
  end;
end;

procedure TFmVSClassifOneNw3.VerTextoMemo1Click(Sender: TObject);
begin
  Geral.MB_Info(Memo2.Text);
end;

procedure TFmVSClassifOneNw3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FCriando := False;
end;

procedure TFmVSClassifOneNw3.FormCreate(Sender: TObject);
  function GetFontHeight(Base: Integer): Integer;
  begin
    Result := -Trunc(-Base * (Screen.Width / 1920));
  end;
  function ObtemWidth(Base: Integer): Integer;
  begin
    Result := Trunc(Base * (Screen.Width / 1920));
  end;
  function ObtemHeigth(Base: Integer): Integer;
  var
    H1, H2: Integer;
  begin
    H1 := Trunc(Base * (Screen.Height / 1080));
    //H2 := Trunc(Base * (Screen.Width * 1920));
    //if H1 < H2 then
      Result := H1
    //else
      //Result := H2;
  end;
var
  MenuItem: TMenuItem;
  IdTXT: String;
  I: Integer;
  OK: Boolean;
begin
  FLiberadoPecasNegativas := False;
  FInRedefinido := False;
  FCriando := True;
  FClasseVisivel := True;
  FInverte := False;
  FDifTime := DModG.ObtemAgora() - Now();
  //
  DqAll.Database     := Dmod.MyDB;
  DqItens.Database   := Dmod.MyDB;
  DqSumPall.Database := Dmod.MyDB;
  //
  UnDmkDAC_PF.AbreQuery(QrRevisores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDigitadores, Dmod.MyDB);
  ReopenVSMrtCad();
  //
  SGAll.PopupMenu := PMAll;
  //
  TesteInclusao1.Enabled := VAR_USUARIO = -1;
  //
  SGAll.Cells[00, 0] := 'Seq';
  SGAll.Cells[01, 0] := 'Box';
  SGAll.Cells[02, 0] := 'SubClas';
  SGAll.Cells[03, 0] := 'AreaM2';
  SGAll.Cells[04, 0] := 'Martelo';
  SGAll.Cells[05, 0] := 'VMI_Dest';
  SGAll.Cells[06, 0] := 'Controle';
  SGAll.Cells[07, 0] := 'VSPaClaIts';
  SGAll.Cells[08, 0] := 'VSPallet';
  SGAll.Cells[09, 0] := 'Pecas';
  SGAll.Cells[10, 0] := 'AreaP2';
  SGAll.Cells[11, 0] := 'VMI_Sorc';
  SGAll.Cells[12, 0] := 'VMI_Dest';
  //
  SGAll.ColWidths[00] := 40;
  SGAll.ColWidths[01] := 40; //Box
  SGAll.ColWidths[02] := 72; //SubClass
  SGAll.ColWidths[03] := 56; //AreaM2
  SGAll.ColWidths[04] := 72; //NO_MARTELO
  SGAll.ColWidths[05] := 90; //VMI_Dest
  SGAll.ColWidths[06] := 90; //Controle
  SGAll.ColWidths[07] := 90; //VSPaClaIts
  SGAll.ColWidths[08] := 90; //VSPallet
  SGAll.ColWidths[09] := 40; //Pecas
  SGAll.ColWidths[10] := 40; //AreaP2
  SGAll.ColWidths[11] := 90; //VMI_Sorc
  SGAll.ColWidths[12] := 90; //VMI_Dest
  //
  for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_15 do
  begin
    IdTXT := Geral.FFF(I, 2);
    //
    FPMItens[I] := TPopupMenu.Create(Self);
    FPMItens[I].Name := 'PMItens' + IdTXT;
    FPMItens[I].OnPopup := PMItensPopup;
    //
    MenuItem := TMenuItem.Create(Self);
    MenuItem.Name := 'AdicionarPallet' + IdTXT;
    MenuItem.Caption := '&Adicionar pallet';
    MenuItem.OnClick := AdicionarPalletClick;
    FPMItens[I].Items.Add(MenuItem);
    //
    MenuItem := TMenuItem.Create(Self);
    MenuItem.Name := 'EncerrarPallet' + IdTXT;
    MenuItem.Caption := '&Encerrar pallet';
    MenuItem.OnClick := EncerrarPalletClick;
    FPMItens[I].Items.Add(MenuItem);
    //
    MenuItem := TMenuItem.Create(Self);
    MenuItem.Name := 'RemoverPallet' + IdTXT;
    MenuItem.Caption := '&Remover pallet';
    MenuItem.OnClick := RemoverPalletClick;
    FPMItens[I].Items.Add(MenuItem);
    //
    MenuItem := TMenuItem.Create(Self);
    MenuItem.Name := 'MensagemDeDados' + IdTXT;
    MenuItem.Caption := '&Mensagem de dados';
    MenuItem.OnClick := Mensagemdedados1Click;
    FPMItens[I].Items.Add(MenuItem);
  end;
  //
  if (Screen.Width < 1920) or (Screen.Height < 1080) then
  begin
    PnAll.Width := ObtemWidth(320);
    PnAll.Font.Size := GetFontHeight(-19(*14*));
    for I := 0 to SGAll.ColCount - 1 do
      SGAll.ColWidths[I] := ObtemWidth(SGAll.ColWidths[I]);
    //
    PnExtras.Width := ObtemWidth(233);
    PnExtras.Font.Size := GetFontHeight(-19(*14*));
    DBGNotaEqz.Font.Size := GetFontHeight(-19(*14*));
    TStringGrid(DBGNotaEqz).DefaultRowHeight := ObtemHeigth(24);
    for I := 0 to TStringGrid(DBGNotaEqz).ColCount - 1 do
      TStringGrid(DBGNotaEqz).ColWidths[I] :=
      ObtemWidth(TStringGrid(DBGNotaEqz).ColWidths[I]);
    DBEdNotaEqzM2.Font.Height := GetFontHeight(-32(*24*));
    //
    PnInfoBig.Font.Height := GetFontHeight(-19(*14*));
    PnInfoBig.Height := ObtemHeigth(97);
    //PnNaoClass.Height := ObtemHeigth(97);
    PnNaoClass.Width := ObtemWidth(320);
    PnIntMei.Width := ObtemWidth(120);
    //
    DBEdSdoVrtPeca.Font.Height := GetFontHeight(-32(*24*));
    DBEdSdoVrtArM2.Font.Height := GetFontHeight(-32(*24*));
    DBEdJaFoi_PECA.Font.Height := GetFontHeight(-32(*24*));
    DBEdJaFoi_AREA.Font.Height := GetFontHeight(-32(*24*));
    //
    PnJaClass.Visible := False;
    //
    PnArea.Width := ObtemWidth(140);
    PnBox.Width := ObtemWidth(52);
    PnSubClass.Width := ObtemWidth(82);
    PnDigitacao.Width := ObtemWidth(532);
    //
    EdArea.Font.Height     := GetFontHeight(-32(*24*));
    LaTipoArea.Font.Height := GetFontHeight(-32(*24*));
    EdBox.Font.Height      := GetFontHeight(-32(*24*));
    EdSubClass.Font.Height := GetFontHeight(-32(*24*));
    EdVSMrtCad.Font.Height := GetFontHeight(-32(*24*));
    CBVSMrtCad.Font.Height := GetFontHeight(-32(*24*));
    //
    SGAll.Font.Size        := GetFontHeight(-19(*14*));
    SGAll.DefaultRowHeight := ObtemHeigth(28);
    //
    PanelOC.Height := ObtemHeigth(102);
    PnInfoOC.Font.Height := GetFontHeight(-19(*14*));
    PnResponsaveis.Font.Height := GetFontHeight(-19(*14*));
    PnCfgEqz.Height := ObtemHeigth(46);
    PnCfgEqz.Font.Height := GetFontHeight(-16(*12*));
    for I := 0 to Self.ComponentCount - 1 do
    begin
      OK := False;
      if Components[I] is TLabel then
      begin
        if TLabel(Components[I]).FocusControl <> nil then
          OK :=
            (TControl(TLabel(Components[I]).FocusControl).Parent = PnInfoOC)
          or
            (TControl(TLabel(Components[I]).FocusControl).Parent = PnResponsaveis)
          or
            (TControl(TLabel(Components[I]).FocusControl).Parent = PnMenu)
          or
            (TControl(TLabel(Components[I]).FocusControl).Parent = PnCfgEqz);
      end else
      if Components[I] is TControl then
        OK :=
          (TControl(Components[I]).Parent = PnInfoOC)
        or
          (TControl(Components[I]).Parent = PnResponsaveis)
        or
          (TControl(Components[I]).Parent = PnMenu)
        or
          (TControl(Components[I]).Parent = PnCfgEqz);
      if OK then
      begin
        TControl(Components[I]).Top := ObtemHeigth(TControl(Components[I]).Top);
        TControl(Components[I]).Left := ObtemHeigth(TControl(Components[I]).Left);
        TControl(Components[I]).Width := ObtemHeigth(TControl(Components[I]).Width);
        TControl(Components[I]).Height := ObtemHeigth(TControl(Components[I]).Height);
      end;
    end;
  end;
  //
  SGDefeiDefin.Cells[0,0] := 'Seq.';
  SGDefeiDefin.Cells[1,0] := 'X';
  SGDefeiDefin.Cells[2,0] := 'Y';
  SGDefeiDefin.Cells[3,0] := 'C�d.';
  SGDefeiDefin.Cells[4,0] := 'Nome defeito';
  SGDefeiDefin.Cells[5,0] := 'S�mbolo';
  SGDefeiDefin.Cells[6,0] := 'Cor borda';
  SGDefeiDefin.Cells[7,0] := 'Cor miolo';
  //
end;

procedure TFmVSClassifOneNw3.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //if Key = VK_MULTIPLY then
  if Key = VK_F12 then
  begin
    if PnDigitacao.Enabled then
      EdArea.SetFocus;
    OcultaMostraClasses();
  end;
end;

procedure TFmVSClassifOneNw3.ImprimeFluxoDeMovimentoDoIMEI();
begin
  {$IfDef sAllVS}
  VS_PF.MostraRelatoroFluxoIMEI(QrVSGerArtNewControle.Value);
  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$EndIf}
end;

procedure TFmVSClassifOneNw3.ImprimirfluxodemovimentodoIMEI1Click(
  Sender: TObject);
begin
  ImprimeFluxoDeMovimentoDoIMEI();
end;

procedure TFmVSClassifOneNw3.ImprimirfluxodemovimentodoIMEI2Click(
  Sender: TObject);
begin
  ImprimeFluxoDeMovimentoDoIMEI();
end;

procedure TFmVSClassifOneNw3.ImprimirNmeroPallet1Click(Sender: TObject);
  procedure ConfigBox(Box: Integer; QrVSPallet: TmySQLQuery; Check: TCheckBox);
  var
    Pallet: Integer;
  begin
    Pallet := QrVSPallet.FieldByName('Codigo').AsInteger;
    if (FQrVSPallet[01].State = dsInactive) then
    begin
      Check.Caption := 'Box ' + Geral.FF0(Box);
      Check.Enabled := False;
      FmVSPalNumImp.Boxes[Box] := 0;
    end else
    begin
      Check.Caption := 'Box ' + Geral.FF0(Box) + ' - Pallet ' + Geral.FF0(Pallet);
      Check.Enabled := True;
      FmVSPalNumImp.Boxes[Box] := Pallet;
    end;
  end;
begin
  if DBCheck.CriaFm(TFmVSPalNumImp, FmVSPalNumImp, afmoNegarComAviso) then
  begin
    ConfigBox(1, FQrVSPallet[01], FmVSPalNumImp.Ck1);
    ConfigBox(2, FQrVSPallet[02], FmVSPalNumImp.Ck2);
    ConfigBox(3, FQrVSPallet[03], FmVSPalNumImp.Ck3);
    ConfigBox(4, FQrVSPallet[04], FmVSPalNumImp.Ck4);
    ConfigBox(5, FQrVSPallet[05], FmVSPalNumImp.Ck5);
    ConfigBox(6, FQrVSPallet[06], FmVSPalNumImp.Ck6);
    //
    FmVSPalNumImp.ShowModal;
    FmVSPalNumImp.Destroy;
  end;
end;

procedure TFmVSClassifOneNw3.InsereCouroArrAll(CacCod, CacID, Codigo,
  ClaAPalOri, RclAPalOri, VSPaClaIts, VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix,
  VMI_Dest, Box: Integer; Pecas, AreaM2, AreaP2: Double; Revisor, Digitador,
  Martelo: Integer; DataHora, SubClass: String; FatorIntSrc,
  FatorIntDst: Double; Controle: Int64);
var
  I: Integer;
  a, b, t: Cardinal;
begin
  EdAll.Text := '';
  EdArrAll.Text := '';
  EdItens.Text := '';
  a := GetTickCount();
  //
  I := Length(FLsAll);
  SetLength(FLsAll, I + 1);
  FLsAll[I, 00] := IntToStr(I + 1);
  FLsAll[I, 01] := IntToStr(Box);                          // Box
  FLsAll[I, 02] := SubClass;                               // SubClass
  FLsAll[I, 03] := Geral.FFT_Dot(AreaM2, 2, siNegativo);   // AreaM2
  FLsAll[I, 04] := ObtemNO_MARTELO(Martelo);               // NO_MARTELO
  FLsAll[I, 05] := IntToStr(VMI_Dest);                     // VMI_Dest
  FLsAll[I, 06] := IntToStr(Controle);                     // Controle
  FLsAll[I, 07] := IntToStr(VSPaClaIts);                   // VSPaClaIts
  FLsAll[I, 08] := IntToStr(VSPallet);                     // VSPallet
  FLsAll[I, 09] := Geral.FFT_Dot(Pecas, 3, siNegativo);    // Pecas
  FLsAll[I, 10] := Geral.FFT_Dot(AreaP2, 2, siNegativo);   // AreaP2
  FLsAll[I, 11] := IntToStr(VMI_Sorc);                     // VMI_Sorc
  FLsAll[I, 12] := IntToStr(VMI_Dest);                     // VMI_Dest
  //
  CarregaSGAll();
  //
  b := GetTickCount();
  t := b - a;
  EdArrAll.Text := FloatToStr(t / 1000) + 's';

  //////////////////////////////////////////////////////////////////////////////

  a := GetTickCount();
  //
  I := Length(FLsItens[Box]);
  SetLength(FLsItens[Box], I + 1);
  FLsItens[Box][I, 00] := IntToStr(I + 1);
  FLsItens[Box][I, 01] := IntToStr(Controle);                     // Conrole
  FLsItens[Box][I, 02] := Geral.FFT_Dot(Pecas, 3, siNegativo);    // Pecas
  FLsItens[Box][I, 03] := Geral.FFT_Dot(AreaM2, 2, siNegativo);   // AreaM2
  FLsItens[Box][I, 04] := Geral.FFT_Dot(AreaP2, 2, siNegativo);   // AreaP2
  //
  CarregaSGItens(Box);
  //
  b := GetTickCount();
  t := b - a;
  EdItens.Text := FloatToStr(t / 1000) + 's';
end;

procedure TFmVSClassifOneNw3.InsereCouroAtual;
var
  VSPaClaIts: Integer;
  QrVSPallet(*, QrItens, QrSum, QrSumPal*): TmySQLQuery;
var
  DataHora, SubClass: String;
  CacCod, CacID, Codigo, ClaAPalOri, RclAPalOri, VSPaRclIts, VSPallet, VMI_Sorc,
  VMI_Baix, VMI_Dest, Box, Revisor, Digitador, Martelo: Integer;
  Controle: Int64;
  Pecas, AreaM2, AreaP2, FatorIntSrc, FatorIntDst: Double;
  //
  Ta, Tb, Tt: Cardinal;
  PcBaix_TXT, PcGera_TXT, AreaM2_TXT, AreaP2_TXT, ValorT_TXT: String;
  Continua: Boolean;
  Area: Double;
  Fator: Double;
begin
  Ta := GetTickCount();
  //
  Revisor        := EdRevisor.ValueVariant;
  Digitador      := EdDigitador.ValueVariant;
  //
  if MyObjects.FIC(Revisor = 0, EdRevisor, 'Informe o classificador!') then
    Exit;
  if MyObjects.FIC(Digitador = 0, EdDigitador, 'Informe o digitador!') then
    Exit;
  //
  CacCod         := FCacCod;
  CacID          := Integer(emidClassArtXXUni);
  Codigo         := FCodigo;
  Controle       := 0;
  ClaAPalOri     := 0;
  RclAPalOri     := 0;
  //
  if PnMartelo.Visible then
  begin
    if CBVSMrtCad.KeyValue = QrVSMrtCadNome.Value then
      Martelo := QrVSMrtCadCodigo.Value
    else
      Martelo := 0;
  end else
    Martelo := 0;
  //
  //VSPaClaIts     := ;   ver abaixo!!!
  VSPaRclIts     := 0;
  //VSPallet       := ;
  //VMI_Sorc       := ;
  //VMI_Dest       := ;
  Box            := EdBox.ValueVariant;
  Revisor        := EdRevisor.ValueVariant;
  Digitador      := EdDigitador.ValueVariant;
  DataHora       := Geral.FDT(Now() + FDifTime, 109);
  if PnSubClass.Visible then
    SubClass     := dmkPF.SoTextoLayout(EdSubClass.Text)
  else
    SubClass     := '';
//begin
  // ini 2023-04-13
(*
  if EdArea.ValueVariant < 0.01 then
  begin
    EdArea.SetFocus;
    EdArea.SelectAll;
    Exit;
  end;
*)
  if FCkDuplicaArea[Box].Checked then
    Fator := 2
  else
    Fator := 1;
  case QrVSPaClaCabTipoArea.Value of
    0: Area := EdArea.ValueVariant / 100 * Fator;
    1: Area := EdArea.ValueVariant / 100 * 10.7639104167097 * Fator;
    else Area := 0;
  end;

  Continua := Area >= 0.01;
  if Continua then
  begin
    Continua :=
      //(Area <= QrVSGerArtNewMediaMaxM2.Value)
      (Area <= FQrVSPAllet[Box].FieldByName('MediaMaxM2').AsFloat)
      and
      //(Area >= QrVSGerArtNewMediaMinM2.Value);
      (Area >= FQrVSPAllet[Box].FieldByName('MediaMinM2').AsFloat);
    if Continua = False then
    begin
      MyObjects.Senha(TFmSenha, FmSenha, 9, '', '', '', '', '', False, '', 0, '',
      '�rea informada fora do permitido!');
      Continua := VAR_SENHARESULT = 2;
    end;
  end;
  if Continua = False then
  begin
    EdArea.SetFocus;
    EdArea.SelectAll;
    Exit;
  end;
  Continua := (QrSumTSdoVrtPeca.Value > 0) or (FLiberadoPecasNegativas = True);
  if Continua = False then
  begin
    MyObjects.Senha(TFmSenha, FmSenha, 9, '', '', '', '', '', False, '', 0, '',
    'Saldo de pe�as insuficiente!');
    Continua := VAR_SENHARESULT = 2;
    if Continua then
      FLiberadoPecasNegativas := True;
  end;
  if Continua = False then
    Exit;
  // fim 2023-04-13
  if BoxInBoxes(EdBox.ValueVariant) then
  //if (Box > 0) and (Box <= VAR_CLA_ART_RIB_MAX_BOX) then
  begin
    Pecas          := 1;
    //
    if QrVSPaClaCabTipoArea.Value = 0 then
    begin
      AreaM2 := EdArea.ValueVariant / 100 * Fator;
      AreaP2 := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
    end else
    begin
      AreaP2 := EdArea.ValueVariant / 100 * Fator;
      AreaM2 := Geral.ConverteArea(AreaP2, ctP2toM2, cfCento);
    end;
    if not ObtemQryesBox(Box, QrVSPallet) then
      Exit;
    if not ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest) then
      Exit;
    //
    FatorIntSrc    := QrVSGerArtNewFatorInt.Value;
    FatorIntDst    := QrVSPallet.FieldByName('FatorInt').AsFloat;
    if MyObjects.FIC(FatorIntSrc <= 0, nil,
    'O artigo do pallet de origem n�o tem a parte de material definida em seu cadastro!')
    then
      Exit;
    if MyObjects.FIC(FatorIntSrc <= 0, nil,
    'O artigo do pallet de destino n�o tem a parte de material definida em seu cadastro!')
    then
      Exit;
    if MyObjects.FIC(FatorIntSrc < FatorIntDst, nil,
    'O artigo de destino n�o pode ter parte de material maior que a origem!')
    then
      Exit;
   //
    VMI_Sorc := QrVSGerArtNewControle.Value;
    //VMI_Dest :=
    Controle   := UMyMod.BPGS1I64('vscacitsa', 'Controle', '', '', tsPos, stIns, Controle);

    ClaAPalOri := Controle;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'vscacitsa', False, [
    'CacCod', 'CacID', 'Codigo',
    'ClaAPalOri', 'RclAPalOri', 'VSPaClaIts',
    'VSPaRclIts', 'VSPallet',
    'VMI_Sorc', 'VMI_Baix', 'VMI_Dest',
    'Box', 'Pecas',
    'AreaM2', 'AreaP2', 'Revisor',
    'Digitador', 'Martelo', CO_DATA_HORA_GRL,
    'SubClass', 'FatorIntSrc', 'FatorIntDst'], [
    'Controle'], [
    CacCod, CacID, Codigo,
    ClaAPalOri, RclAPalOri, VSPaClaIts,
    VSPaRclIts, VSPallet,
    VMI_Sorc, VMI_Baix, VMI_Dest,
    Box, Pecas,
    AreaM2, AreaP2, Revisor,
    Digitador, Martelo, DataHora,
    SubClass, FatorIntSrc, FatorIntDst], [
    Controle], VAR_InsUpd_AWServerID) then
    begin
      if Trim(SGDefeiDefin.Cells[3, 1]) <> '' then
         InsereDefeitos(CacCod, CacID, Codigo, Controle);
      (*if FUsaMemory then
      begin
      *)
        InsereCouroArrAll(CacCod, CacID, Codigo,
        ClaAPalOri, RclAPalOri, VSPaClaIts,
        VSPaRclIts, VSPallet,
        VMI_Sorc, VMI_Baix, VMI_Dest,
        Box, Pecas,
        AreaM2, AreaP2, Revisor,
        Digitador, Martelo, DataHora,
        SubClass, FatorIntSrc, FatorIntDst,
        Controle);
        //
        PcGera_TXT := Geral.FFT_Dot(Pecas, 3, siNegativo);
        PcBaix_TXT := Geral.FFT_Dot(Pecas / FatorIntSrc * FatorIntDst, 3, siNegativo);
        AreaM2_TXT := Geral.FFT_Dot(AreaM2, 2, siNegativo);
        AreaP2_TXT := Geral.FFT_Dot(AreaP2, 2, siNegativo);
        ValorT_TXT := Geral.FFT_Dot(QrVSGerArtNewCUSTO_M2.Value * AreaM2, 2, siNegativo);
        //
        Dmod.MyDB.Execute(
        ' UPDATE ' + CO_UPD_TAB_VMI +
        ' SET SdoVrtPeca=SdoVrtPeca-' + PcBaix_TXT +
        ', SdoVrtArM2=SdoVrtArM2-' + AreaM2_TXT +
        ' WHERE Controle=' + Geral.FF0(VMI_Sorc) +
        '');
        Dmod.MyDB.Execute(
        ' UPDATE ' + CO_UPD_TAB_VMI +
        ' SET Pecas=Pecas-' + PcBaix_TXT +
        ', AreaM2=AreaM2-' + AreaM2_TXT +
        ', AreaP2=AreaP2-' + AreaP2_TXT +
        ', ValorT=ValorT-' + ValorT_TXT +
        ' WHERE Controle=' + Geral.FF0(VMI_Baix) +
        '');
        Dmod.MyDB.Execute(
        ' UPDATE ' + CO_UPD_TAB_VMI +
        ' SET SdoVrtPeca=SdoVrtPeca+' + PcGera_TXT +
        ', SdoVrtArM2=SdoVrtArM2+' + AreaM2_TXT +
        ', Pecas=Pecas+' + PcGera_TXT +
        ', AreaM2=AreaM2+' + AreaM2_TXT +
        ', AreaP2=AreaP2+' + AreaP2_TXT +
        ', ValorT=ValorT+' + ValorT_TXT +
        ' WHERE Controle=' + Geral.FF0(VMI_Dest) +
        '');
        //
        CarregaSGSumPall(Box, amathSoma, Pecas, AreaM2, AreaP2);
      (*
      end else
      begin
        ReopenItens(Box, VSPaClaIts, VSPallet);
        //
        VS_CRC_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
          QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
      end;
      *)
      //
      EdSubClass.Text     := '';
      EdBox.ValueVariant  := 0;
      EdArea.ValueVariant := 0;
      EdVSMrtCad.Text     := '';
      CBVSMrtCad.KeyValue := Null;
      EdArea.SetFocus;
      EdArea.SelectAll;
      //
      AtualizaInfoOC();
      //
      Tb := GetTickCount();
      Tt := Tb - Ta;
      EdTempo.Text := FloatToStr(Tt / 1000) + 's';
    end;
  end else
  begin
    Geral.MB_Aviso('Box inv�lido!');
    EdBox.SetFocus;
    EdBox.SelectAll;
  end;
end;

procedure TFmVSClassifOneNw3.InsereDefeitos(CacCod, CacID, Codigo: Integer;
  CacIts: Int64);
var
  Marca: String;
  SerieFch, Ficha, DefeitLocX, DefeitLocY, DefeitTipo, DefeitQtde: Integer;
  Controle: Int64;
  SQLType: TSQLType;
  //
  I: Integer;
begin
  //
  SQLType        := stIns;
  //CacCod         := ;
  //CacID          := ;
  //Codigo         := ;
  //CacIts         := ;
  Controle       := 0;
  SerieFch       := QrVSGerArtNewSerieFch.Value;
  Ficha          := QrVSGerArtNewFicha.Value;
  Marca          := QrVSGerArtNewMarca.Value;
  for I := 1 to SGDefeiDefin.RowCount - 1 do
  begin
(*
  SGDefeiDefin.Cells[0,0] := 'Seq.';
  SGDefeiDefin.Cells[1,0] := 'X';
  SGDefeiDefin.Cells[2,0] := 'Y';
  SGDefeiDefin.Cells[3,0] := 'C�d.';
  SGDefeiDefin.Cells[4,0] := 'Nome do defeito';
*)
    DefeitLocX     := Geral.IMV(SGDefeiDefin.Cells[1, I]);
    DefeitLocY     := Geral.IMV(SGDefeiDefin.Cells[2, I]);
    DefeitTipo     := Geral.IMV(SGDefeiDefin.Cells[3, I]);
    DefeitQtde     := 1;
    //
    if DefeitTipo <> 0 then
    begin
      Controle := UMyMod.BPGS1I64('vsdefeicac', 'Controle', '', '', tsPos, SQLType, Controle);
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'vsdefeicac', False, [
      'CacCod', 'CacID', 'Codigo',
      'CacIts', 'SerieFch', 'Ficha',
      'Marca', 'DefeitLocX', 'DefeitLocY',
      'DefeitTipo', 'DefeitQtde'], [
      'Controle'], [
      CacCod, CacID, Codigo,
      CacIts, SerieFch, Ficha,
      Marca, DefeitLocX, DefeitLocY,
      DefeitTipo, DefeitQtde], [
      Controle], True);
    end;
  end;
  //
  MyObjects.LimpaGrade(SGDefeiDefin, 1, 1, True);
end;

procedure TFmVSClassifOneNw3.LiberaDigitacao();
var
  Libera: Boolean;
begin
  Libera := (EdRevisor.ValueVariant <> 0) and (EdDigitador.ValueVariant <> 0);
  //
  PnDigitacao.Enabled := Libera;
  N4.Visible := Libera;
  TesteInclusao1.Visible := Libera;
  //
  TentarFocarEdArea();
end;

procedure TFmVSClassifOneNw3.MensagemDadosBox(Box: Integer);
  procedure MostraCaptura();
  var
    Caminho: String;
  begin
    Caminho := ExtractFileDir(Application.ExeName) + '\Mensagem.txt';
    if FileExists(Caminho) then
      DeleteFile(Caminho);
    Memo1.Lines.SaveToFile(Caminho);
    ShellExecute(GetDesktopWindow, 'open', pchar(Caminho), nil, nil, sw_ShowNormal);
  end;
var
  //QrSum,
  QrSumPal: TmySQLQuery;
  //EdPercent,
  EdMedia: TdmkEdit;
  VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer;
begin
(* ver o que fazer!
  ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest);
  case Box of
    1:
    begin
      //QrSum     := QrSum1;
      QrSumPal  := QrSumPal1;
      EdMedia   := EdMedia1;
    end;
    2:
    begin
      //QrSum     := QrSum2;
      QrSumPal  := QrSumPal2;
      EdMedia   := EdMedia2;
    end;
    3:
    begin
      //QrSum     := QrSum3;
      QrSumPal  := QrSumPal3;
      EdMedia   := EdMedia3;
    end;
    4:
    begin
      //QrSum     := QrSum4;
      QrSumPal  := QrSumPal4;
      EdMedia   := EdMedia4;
    end;
    5:
    begin
      //QrSum     := QrSum5;
      QrSumPal  := QrSumPal5;
      EdMedia   := EdMedia5;
    end;
    6:
    begin
      //QrSum     := QrSum6;
      QrSumPal  := QrSumPal6;
      EdMedia   := EdMedia6;
    end;
    else
    begin
      Geral.MB_Erro('Box n�o implementado para mensagem de dados!');
      Exit;
    end;
  end;
  Memo1.Lines.Clear;
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add(Geral.FDT(DModG.ObtemAgora(), 109));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('CLASSIFICA��O - OC  ' + Geral.FF0(QrVSPaClaCabCacCod.Value));
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('CLASSIFICA��O DA FICHA RMP:  ' + QrVSGerArtNewNO_Ficha.Value);
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('Artigo na classe - ID:  ' + Geral.FF0(VSPaClaIts));
  Memo1.Lines.Add('Artigo na classe - IME-I:  ' + Geral.FF0(VMI_Dest));
  //Memo1.Lines.Add('Artigo na classe - Pe�as:  ' + FloatToStr(QrSum.FieldByName('Pecas').AsFloat));
  //Memo1.Lines.Add('Artigo na classe - �rea:  ' + Geral.FFT(QrSum.FieldByName('AreaM2').AsFloat, 2, siNegativo));
  //Memo1.Lines.Add('Artigo na classe - % �rea:  ' + EdPercent.Text);
  Memo1.Lines.Add('==================================================');
  Memo1.Lines.Add('Palet - N�:  ' + Geral.FF0(VSPallet));
  Memo1.Lines.Add('Palet - Pe�as:  ' + FloatToStr(QrSumPal.FieldByName('Pecas').AsFloat));
  Memo1.Lines.Add('Palet - �rea:  ' + Geral.FFT(QrSumPal.FieldByName('AreaM2').AsFloat, 2, siNegativo));
  Memo1.Lines.Add('Palet - m� / Pe�a:  ' + EdMedia.Text);
  Memo1.Lines.Add('==================================================');
  //
  MostraCaptura();
*)
end;

procedure TFmVSClassifOneNw3.Mensagemdedados1Click(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  //
  MensagemDadosBox(Box);
  //
end;

procedure TFmVSClassifOneNw3.MostraJanelaDefeitos();
begin
  Application.CreateForm(TFmVSClassifOneDefei, FmVSClassifOneDefei);
  FmVSClassifOneDefei.FSGDefeiDefin := SGDefeiDefin;
  if Trim(SGDefeiDefin.Cells[0,1]) = '' then
    FmVSClassifOneDefei.FSeqDefIni := 1
  else
    FmVSClassifOneDefei.FSeqDefIni := SGDefeiDefin.RowCount;
  FmVSClassifOneDefei.ShowModal;
  FmVSClassifOneDefei.Destroy;
end;

procedure TFmVSClassifOneNw3.Mostrartodosboxes1Click(Sender: TObject);
begin
  Mostrartodosboxes1.Checked := not Mostrartodosboxes1.Checked;
  VerificaBoxes();
end;

function TFmVSClassifOneNw3.ObrigaSubClass(Box: Integer;
  SubClass: String): Boolean;
begin
  Result := (FCkSubClass[Box] <> nil) and (FCkSubClass[Box].Checked);
  if Result and (Trim(EdSubClass.Text) <> '') then
    Result := False;
end;

function TFmVSClassifOneNw3.ObtemDadosBox(const Box: Integer; var
VSPaRclIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest: Integer): Boolean;
begin
  Result := False;
  VMI_Sorc := QrVSGerArtNewControle.Value;
  VSPallet   := FQrVSPallet[Box].FieldByName('Codigo').AsInteger;
  if FQrVSPalClaIts[Box].State <> dsInactive then
  begin
    VSPaRclIts := FQrVSPalClaIts[Box].FieldByName('Controle').AsInteger;
    VMI_Baix   := FQrVSPalClaIts[Box].FieldByName('VMI_Baix').AsInteger;
    VMI_Dest   := FQrVSPalClaIts[Box].FieldByName('VMI_Dest').AsInteger;
//    Result := True;
    Result := (VSPaRclIts <> 0) and (VMI_Baix <> 0) and (VMI_Dest <> 0);
  end;
  if not Result then
    Geral.MB_Aviso('Configura��o de Box ou artigo incompleta!')
end;

function TFmVSClassifOneNw3.ObtemNO_MARTELO(Martelo: Integer): String;
var
  I, N: Integer;
begin
  Result := '' ;
  if Martelo <> 0 then
  begin
    Result := '?????' ;
    for I := 0 to Length(FMartelosCod) - 1 do
    begin
      if Martelo = FMartelosCod[I] then
      begin
        Result := FMartelosNom[I];
        Exit;
      end;
    end;
  end;
end;

function TFmVSClassifOneNw3.ObtemQryesBox(const Box: Integer; var QrVSPallet:
TmySQLQuery): Boolean;
begin
  QrVSPallet := FQrVSPallet[Box];
  Result := True;
end;

procedure TFmVSClassifOneNw3.OcultaMostraClasses();
var
  I: Integer;
begin
  FClasseVisivel := not FClasseVisivel;
  for I := 1 to FBoxMax do
  begin
    if FDBEdArtNome[I] <> nil then
      FDBEdArtNome[I].Visible  := FClasseVisivel;
  end;
end;

procedure TFmVSClassifOneNw3.PMEncerraPopup(Sender: TObject);
var
  I: Integer;
begin
  for I := 1 to FBoxMax do
  begin
    FPalletdobox[I].Visible := True;
    FPalletdobox[I].Enabled :=
    (FQrVSPallet[I] <> nil) and
    (FQrVSPallet[I].State <> dsInactive) and
    (FQrVSPallet[I].RecordCount > 0);
  end;
  for I := FBoxMax + 1 to VAR_CLA_ART_RIB_MAX_BOX_15 do
    FPalletdobox[I].Visible := False;
end;

procedure TFmVSClassifOneNw3.PMitensPopup(Sender: TObject);
var
  I, Box: Integer;
  Nome: String;
  QrVSPallet, QrItens, QrSum, QrSumPal: TmySQLQuery;
  PopupMenu: TMenuItem;
begin
  Box := Geral.IMV(Copy(TPopupMenu(Sender).Name, Length(TPopupMenu(Sender).Name) - 2));
  if not ObtemQryesBox(Box, QrVSPallet) then
    Exit;
  for I := 0 to TPopupMenu(Sender).Items.Count -1 do
  begin
    PopupMenu := TPopupMenu(Sender).Items[I];
    //
    Nome := Lowercase(PopupMenu.Name);
    Nome := Copy(Nome, 1, Length(Nome) - 2);
    //
    if Nome = 'adicionarpallet' then
      PopupMenu.Enabled := (QrVSPallet.State = dsInactive) or (QrVSPallet.RecordCount = 0)
    else
    if Nome = 'encerrarpallet' then
      MyObjects.HabilitaMenuItemCabUpd(PopupMenu, QrVSPallet)
    else
    if Nome = 'removerpallet' then
      MyObjects.HabilitaMenuItemItsDel(PopupMenu, QrVSPallet);
  end;
end;

procedure TFmVSClassifOneNw3.QrSumTCalcFields(DataSet: TDataSet);
begin
  case QrVSPaClaCabTipoArea.Value of
    //0: QrSumTJaFoi_AREA.Value := QrVSGerArtNewAreaM2.Value - QrSumTSdoVrtArM2.Value;
    0: QrSumTJaFoi_AREA.Value := QrSumTAreaM2.Value - QrSumTSdoVrtArM2.Value;
    //1: QrSumTJaFoi_AREA.Value := QrVSGerArtNewAreaP2.Value - Geral.ConverteArea(QrSumTAreaM2.Value - QrSumTSdoVrtArM2.Value, ctM2toP2, cfQuarto);
    1: QrSumTJaFoi_AREA.Value := Geral.ConverteArea(QrSumTAreaM2.Value - QrSumTSdoVrtArM2.Value, ctM2toP2, cfQuarto);
    else QrSumTJaFoi_AREA.Value := 0;
  end;
  //QrSumTJaFoi_PECA.Value := QrVSGerArtNewPecas.Value - QrSumTSdoVrtPeca.Value;
  QrSumTJaFoi_PECA.Value := QrSumTPecas.Value - QrSumTSdoVrtPeca.Value;
end;

procedure TFmVSClassifOneNw3.QrVSGerArtNewAfterOpen(DataSet: TDataSet);
var
  I: Integer;
begin
  for I := 1 to FBoxMax do
    ReconfiguraPaineisIntMei(FQrVSPallet[I], FPnIntMei[I]);
end;

procedure TFmVSClassifOneNw3.QrVSPaClaCabAfterScroll(DataSet: TDataSet);
var
  I, LstPal, Controle, Codigo: Integer;
begin
  for I := 1 to FBoxMax do
  begin
    if QrVSPaClaCab.FieldByName(VS_CRC_PF.CampoLstPal(I)).AsInteger > 0 then
      FBoxes[I] := True
    else
      FBoxes[I] := False;
    FErrosBox[I] := False;
  end;
  //
  ReopenVSGerArtDst();
  //
  VerificaBoxes();
  //
  for I := 1 to FBoxMax do
  begin
    if FQrVSPallet[I] <> nil then
    begin
      LstPal := QrVSPaClaCab.FieldByName('LstPal' + Geral.FFN(I, 2)).AsInteger;
      ReopenVSPallet(I, FQrVSPallet[I], FQrVSPalClaIts[I], LstPal);
      if (FQrVSPalClaIts[I].State <> dsInactive) and
      (FQrVSPallet[I].State <> dsInactive) then
      begin
        Controle := FQrVSPalClaIts[I].FieldByName('Controle').AsInteger;
        Codigo   := FQrVSPallet[I].FieldByName('Codigo').Value;
        ReopenItens(I, Controle, Codigo);
      end;
    end;
  end;
  //
  //if FUsaMemory then
    ReopenAll();
  AtualizaInfoOC();
  //
end;

procedure TFmVSClassifOneNw3.QrVSPaClaCabBeforeClose(DataSet: TDataSet);
var
  I: Integer;
begin
  QrVSGerArtNew.Close;
  for I := 1 to FBoxMax do
  begin
    FPnBox[I].Visible := True;
    FQrVSPallet[I].Close;
    FQrVSPalClaIts[I].Close;
  end;
end;

procedure TFmVSClassifOneNw3.QrVSPaClaCabCalcFields(DataSet: TDataSet);
begin
  case QrVSPaClaCabTipoArea.Value of
    0: QrVSPaClaCabNO_TIPO.Value := 'm�';
    1: QrVSPaClaCabNO_TIPO.Value := 'ft�';
    else QrVSPaClaCabNO_TIPO.Value := '???';
  end;
  LaTipoArea.Caption := QrVSPaClaCabNO_TIPO.Value;
end;

procedure TFmVSClassifOneNw3.ReconfiguraPaineisIntMei(QrVSPallet: TmySQLQuery;
Panel: TPanel);
var
  Habilita: Boolean;
begin
  if QrVSPallet <> nil then
  begin
    Habilita :=
      (QrVSPallet.State <> dsInactive) and
      (QrVSGerArtNew.State <> dsInactive) and
      (QrVSPallet.FieldByName('FatorInt').AsFloat < QrVSGerArtNewFatorInt.Value);
    if (Panel <> nil) and (Habilita <> Panel.Visible) then
      Panel.Visible := Habilita;
  end;
end;

procedure TFmVSClassifOneNw3.RemovePallet(Box: Integer; Reopen: Boolean);
const
  Valor = 0;
var
  Campo: String;
  VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest, Codigo: Integer;
  DtHrFim: String;
begin
  Campo := VS_CRC_PF.CampoLstPal(Box);
  Codigo := QrVSPaClaCabCodigo.Value;
  DtHrFim := Geral.FDT(DModG.ObtemAgora(), 109);
  //
  if not ObtemDadosBox(Box, VSPaClaIts, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest) then
    Exit;
  if VS_CRC_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
  QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1) then
  begin
    // Encerra IME-I
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspaclaitsa', False, [
    'DtHrFim'], ['Controle'], [DtHrFim], [VSPaClaIts], True) then
    begin
      //Tira IMEI do CAC
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspaclacaba', False, [
      Campo], ['Codigo'], [Valor], [Codigo], True) then
      begin
        VS_CRC_PF.AtualizaStatPall(VSPallet);
        if Reopen then
          ReopenVSPaClaCab();
      end;
    end;
  end;
end;

procedure TFmVSClassifOneNw3.RemoverPalletClick(Sender: TObject);
var
  Box: Integer;
begin
  Box := BoxDeComp(Sender);
  RemovePallet(Box, True);
  //
  if PnDigitacao.Enabled then
    EdArea.SetFocus;
end;

procedure TFmVSClassifOneNw3.ReopenAll();
var
  I, J: Integer;
  a, b, t: Cardinal;
begin
  a := GetTickCount();
  DqAll.SQL.Text :=
  ' SELECT cia.Controle, cia.VSPaClaIts, cia.VSPallet, cia.Pecas, ' + // 0 a 3
  ' cia.AreaM2, cia.AreaP2, cia.VMI_Sorc, cia.VMI_Dest, cia.Box, ' +  // 4 a 8
  ' cia.SubClass, mrt.Nome NO_MARTELO ' +                             // 9 a 10
  ' FROM vscacitsa cia ' +
  ' LEFT JOIN vsmrtcad mrt ON mrt.Codigo=cia.Martelo' +
  ' WHERE cia.CacCod=' + Geral.FF0(FCacCod) +
  //' ORDER BY cia.Controle DESC ';
  ' ORDER BY cia.Controle ';
  //
  DqAll.Open();
  SetLength(FLsAll, DqAll.RecordCount);
  DqAll.First();
  while not DqAll.Eof do
  begin
    J := DqAll.RecNo;
    //
    FLsAll[J, 01] := DqAll.FieldValues[08];   // Box
    FLsAll[J, 02] := DqAll.FieldValues[09];   // SubClass
    FLsAll[J, 03] := DqAll.FieldValues[04];   // AreaM2
    FLsAll[J, 04] := DqAll.FieldValues[10];   // NO_MARTELO
    FLsAll[J, 05] := DqAll.FieldValues[07];   // VMI_Dest
    FLsAll[J, 06] := DqAll.FieldValues[00];   // Controle
    FLsAll[J, 07] := DqAll.FieldValues[01];   // VSPaClaIts
    FLsAll[J, 08] := DqAll.FieldValues[02];   // VSPallet
    FLsAll[J, 09] := DqAll.FieldValues[03];   // Pecas
    FLsAll[J, 10] := DqAll.FieldValues[05];   // AreaP2
    FLsAll[J, 11] := DqAll.FieldValues[06];   // VMI_Sorc
    FLsAll[J, 12] := DqAll.FieldValues[07];   // VMI_Dest
    //
    DqAll.Next();
  end;
  DqAll.Close();
  CarregaSGAll();
  //
  b := GetTickCount();
  t := b - a;
  EdAll.Text := FloatToStr(t / 1000) + 's';
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrAll, Dmod.MyDB, [
  'SELECT cia.Controle, cia.VSPaClaIts, cia.VSPallet, cia.Pecas, ',
  'cia.AreaM2, cia.AreaP2, cia.VMI_Sorc, cia.VMI_Dest, cia.Box, ',
  'cia.SubClass, mrt.Nome NO_MARTELO',
  'FROM vscacitsa cia',
  'LEFT JOIN vsmrtcad mrt ON mrt.Codigo=cia.Martelo',
  'WHERE cia.CacCod=' + Geral.FF0(FCacCod),
  'ORDER BY cia.Controle DESC ',
  '']);
*)
end;

procedure TFmVSClassifOneNw3.ReopenEqualize(Automatico: Boolean);
const
  GraGruX = 0;
var
  Codigo: Integer;
begin
  {$IfDef sAllVS}
  Codigo := EdEqualize.ValueVariant;
  if Codigo = 0 then
  begin
    if not Automatico then
      Geral.MB_Aviso('Informe o c�digo do equ�lize!');
  end else
  if CkNota.Checked then
    VS_PF.ReopenNotasEqualize(EdEqualize.ValueVariant, QrNotaAll, QrNotaCrr, QrNotas)
  else
    VS_PF.ReopenNotasEqualize(EdEqualize.ValueVariant, QrNotaAll, nil, QrNotas);
  {$Else}
    Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappVS);
  {$EndIf}
end;

procedure TFmVSClassifOneNw3.ReopenItens(Box, VSPaRclIts, VSPallet: Integer);
var
  I, J: Integer;
  a, b, t: Cardinal;
  //
begin
  a := GetTickCount();
  DqItens.SQL.Text :=
  ' SELECT Controle, Pecas, AreaM2, AreaP2 ' +
  ' FROM vscacitsa ' +
  ' WHERE CacCod=' + Geral.FF0(FCacCod) +
  ' AND VSPaRclIts=' + Geral.FF0(VSPaRclIts) +
  ' AND VSPallet=' + Geral.FF0(VSPallet) +
  //' ORDER BY Controle DESC ' +
  ' ORDER BY Controle ' +
  '';

  //
  DqItens.Open();
  SetLength(FLsItens[Box], DqItens.RecordCount);
  DqItens.First();
  while not DqItens.Eof do
  begin
    J := DqItens.RecNo;
    //
    FLsItens[Box][J, 01] := DqItens.FieldValues[00];   // Conrole
    FLsItens[Box][J, 02] := DqItens.FieldValues[01];   // Pecas
    FLsItens[Box][J, 03] := DqItens.FieldValues[02];   // AreaM2
    FLsItens[Box][J, 04] := DqItens.FieldValues[03];   // AreaP2
    //
    DqItens.Next();
  end;
  DqItens.Close();
  CarregaSGItens(Box);
  //
  b := GetTickCount();
  t := b - a;
  EdItens.Text := FloatToStr(t / 1000) + 's';
  //
  ReopenSumPal((*QrSumPal,*) Box, VSPallet);
end;

procedure TFmVSClassifOneNw3.ReopenSumPal((*QrSumPal: TmySQLQuery;*) Box,
  VSPallet: Integer);
var
  J: Integer;
  Pecas, AreaM2, AreaP2: Double;
begin
  DqSumPall.SQL.Text :=
  ' SELECT ' +
  ' SUM(vmi.SdoVrtPeca) SdoVrtPeca, ' +
  ' SUM(vmi.SdoVrtArM2) SdoVrtArM2' +
  ' FROM ' + CO_SEL_TAB_VMI + ' vmi' +
  ' WHERE vmi.Pallet=' + Geral.FF0(VSPallet) +
  ' AND vmi.Pecas>0 ' +
(*
  ' SELECT SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ' +
  ' SUM(AreaP2) AreaP2 ' +
  ' FROM vscacitsa ' +
  ' WHERE VSPallet=' + Geral.FF0(VSPallet) +
*)
  '';
  //
  DqSumPall.Open();
  Pecas  := Geral.DMV_Dot(DqSumPall.FieldValues[00]);   // Pecas
  AreaM2 := Geral.DMV_Dot(DqSumPall.FieldValues[01]);   // AreaM2
  AreaP2 := Geral.ConverteArea(AreaM2, TCalcType.ctM2toP2, TCalcFrac.cfQuarto);
  DqSumPall.Close();
  CarregaSGSumPall(Box, amathSubstitui, Pecas, AreaM2, AreaP2);
end;

procedure TFmVSClassifOneNw3.ReopenVSGerArtDst();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSGerArtNew, Dmod.MyDB, [
  'SELECT vmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, wbp.Nome NO_Pallet,',
  'IF(vmi.Terceiro=0, "V�rios", ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
  ') NO_FORNECE, ',
  'IF(vmi.Ficha=0, "V�rias", CONCAT(IF(vsf.Nome IS NULL, ',
  '"?", vsf.Nome), " ", vmi.Ficha)) NO_FICHA, ',
  'IF(AreaM2=0, 0, ValorT / AreaM2) CUSTO_M2, ',
  'IF(AreaP2=0, 0, ValorT / AreaP2) CUSTO_P2, cn1.FatorInt, ',
  'xco.MediaMinM2, xco.MediaMaxM2 ',  // 2023-04-13
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  wbp ON wbp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro',
  'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=vmi.GraGruX ',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
  'WHERE vmi.MovimNiv=' + Geral.FF0(Integer(eminDestCurtiXX)),
  'AND vmi.MovimCod=' + Geral.FF0(QrVSPaClaCabMovimCod.Value),
  '']);
  //Geral.MB_SQL(Self, QrVSGerArtNew);
end;

procedure TFmVSClassifOneNw3.ReopenVSMrtCad();
var
  I, N: Integer;
begin
  UnDmkDAC_PF.AbreQuery(QrVSMrtCad, Dmod.MyDB);
  N := QrVSMrtCad.RecordCount;
  SetLength(FMartelosCod, N);
  SetLength(FMartelosNom, N);
  I := 0;
  while not QrVSMrtCad.Eof do
  begin
    FMartelosCod[I] := QrVSMrtCadCodigo.Value;
    FMartelosNom[I] := QrVSMrtCadNome.Value;
    //
    I := I + 1;
    QrVSMrtCad.Next;
  end;
end;

procedure TFmVSClassifOneNw3.ReopenVSPaClaCab();
begin
  GPBoxes.Visible := False;
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaClaCab, Dmod.MyDB, [
  'SELECT vga.GraGruX, vga.Nome, vga.DtHrAberto, ',
  'vga.TipoArea, vga.Empresa, vga.MovimCod, pcc.*,',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA',
  'FROM vspaclacaba pcc',
  'LEFT JOIN vsgerarta  vga ON vga.Codigo=pcc.vsgerart',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=vga.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa',
  'WHERE pcc.Codigo=' + Geral.FF0(FCodigo),
  '']);
  //
  GPBoxes.Visible := True;
end;

procedure TFmVSClassifOneNw3.ReopenVSPallet(Box: Integer; QrVSPallet,
QrVSPalClaIts: TmySQLQuery; Pallet: Integer);
var
  I: Integer;
begin
  if QrVSPallet <> nil then
  begin
    if (Pallet > 0) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrVSPallet, Dmod.MyDB, [
      'SELECT let.*, ',
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT, ',
      ' CONCAT(gg1.Nome, ',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS, cn1.FatorInt, ',
      'xco.MediaMinM2, xco.MediaMaxM2 ',  // 2023-05-06
      'FROM vspalleta let ',
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa ',
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat ',
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status ',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=let.GraGruX ',
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
      'WHERE let.Codigo=' + Geral.FF0(Pallet),
      '']);
      ReconfiguraPaineisIntMei(QrVSPallet, FPnIntMei[Box]);
      //
      FMaxPecas[Box] := QrVSPallet.FieldByName('QtdPrevPc').AsFloat;
      if (QrVSPallet.State <> dsInactive) and (QrVSPallet.RecordCount > 0) and
      (QrVSPallet.FieldByName('FatorInt').AsFloat < 0.1) then
      begin
        FErrosBox[Box] := True;
        //if Box = 0 then
        Geral.MB_Aviso(
        'O artigo do pallet de origem do box ' + geral.FFF(Box, 2) +
        ' n�o tem a parte de material definida em seu cadastro!' + sLineBreak +
        'Para evitar erros de estoque o artigo deve ser configurado!');
        for I := 0 to ComponentCount - 1 do
        begin
          FSGItens[Box].Color := clRed;
          FEdPalletPecas[Box].Color := clRed;
          FEdPalletArea[Box].Color := clRed;
          FEdPalletMedia[Box].Color := clRed;
          FDBEdArtNome[Box].Color := clRed;
          FDBEdArtNoSta[Box].Color := clRed;
          FDBEdArtNoCli[Box].Color := clRed;
          FDBEdIMEICtrl[Box].Color := clRed;
          FDBEdIMEIImei[Box].Color := clRed;
          FDBEdPalletID[Box].Color := clRed;
        end;
      end else
      begin
        FSGItens[Box].Color := clWindow;
        FEdPalletPecas[Box].Color := clWindow;
        FEdPalletArea[Box].Color := clWindow;
        FEdPalletMedia[Box].Color := clWindow;
        FDBEdArtNome[Box].Color := clWindow;
        FDBEdArtNoSta[Box].Color := clWindow;
        FDBEdArtNoCli[Box].Color := clWindow;
        FDBEdIMEICtrl[Box].Color := clWindow;
        FDBEdIMEIImei[Box].Color := clWindow;
        FDBEdPalletID[Box].Color := clWindow;
      end;
      //
      if QrVSPalClaIts <> nil then
        UnDmkDAC_PF.AbreMySQLQuery0(QrVSPalClaIts, Dmod.MyDB, [
        'SELECT Controle, DtHrIni, VMI_Sorc, VMI_Baix, VMI_Dest  ',
        'FROM vspaclaitsa ',
        'WHERE Codigo=' + Geral.FF0(FCodigo),
        'AND Tecla=' + Geral.FF0(Box),
        'AND VSPallet=' + Geral.FF0(Pallet),
        'ORDER BY DtHrIni DESC, Controle DESC ',
        'LIMIT 1 ',
        '']);
    end else
    begin
      QrVSPallet.Close;
    end;
  end;
end;

procedure TFmVSClassifOneNw3.rocarIMEI1Click(Sender: TObject);
var
  Digitador, Revisor: Integer;
  Pal01, Pal02, Pal03, Pal04, Pal05,
  Pal06, Pal07, Pal08, Pal09, Pal10,
  Pal11, Pal12, Pal13, Pal14, Pal15: Integer;
begin
  Digitador := EdDigitador.ValueVariant;
  Revisor   := EdRevisor.ValueVariant;
  //
  Pal01 := QrVSPaClaCabLstPal01.Value;
  Pal02 := QrVSPaClaCabLstPal02.Value;
  Pal03 := QrVSPaClaCabLstPal03.Value;
  Pal04 := QrVSPaClaCabLstPal04.Value;
  Pal05 := QrVSPaClaCabLstPal05.Value;
  Pal06 := QrVSPaClaCabLstPal06.Value;
  //
  Pal07 := QrVSPaClaCabLstPal07.Value;
  Pal08 := QrVSPaClaCabLstPal08.Value;
  Pal09 := QrVSPaClaCabLstPal09.Value;
  Pal10 := QrVSPaClaCabLstPal10.Value;
  Pal11 := QrVSPaClaCabLstPal11.Value;
  Pal12 := QrVSPaClaCabLstPal12.Value;
  Pal13 := QrVSPaClaCabLstPal13.Value;
  Pal14 := QrVSPaClaCabLstPal14.Value;
  Pal15 := QrVSPaClaCabLstPal15.Value;
  //
  if QrSumTSdoVrtPeca.Value <= 0 then
    EncerraOC(True);
  if FBoxMax <= 6 then
  begin
    VS_CRC_PF.MostraFormVSClassifOneRetIMEI_06(
      Pal01,
      Pal02,
      Pal03,
      Pal04,
      Pal05,
      Pal06,
      Self,
      FBoxMax);
  end else
  begin
    VS_CRC_PF.MostraFormVSClassifOneRetIMEI_15(
      Pal01,
      Pal02,
      Pal03,
      Pal04,
      Pal05,
      Pal06,
      Pal07,
      Pal08,
      Pal09,
      Pal10,
      Pal11,
      Pal12,
      Pal13,
      Pal14,
      Pal15,
      Self,
      FBoxMax);
  end;
  //
  EdDigitador.ValueVariant := Digitador;
  EdRevisor.ValueVariant   := Revisor;
  //
end;

procedure TFmVSClassifOneNw3.rocarVichaRMP1Click(Sender: TObject);
(*
var
  Digitador, Revisor: Integer;
  Pal01, Pal02, Pal03, Pal04, Pal05, Pal06: Integer;
*)
begin
(* Form removido para eviatr erro de usuario na ecolha do IMEI!
  Digitador := EdDigitador.ValueVariant;
  Revisor   := EdRevisor.ValueVariant;
  //
  Pal01 := QrVSPaClaCabLstPal01.Value;
  Pal02 := QrVSPaClaCabLstPal02.Value;
  Pal03 := QrVSPaClaCabLstPal03.Value;
  Pal04 := QrVSPaClaCabLstPal04.Value;
  Pal05 := QrVSPaClaCabLstPal05.Value;
  Pal06 := QrVSPaClaCabLstPal06.Value;
  //
  if QrSumTSdoVrtPeca.Value <= 0 then
    EncerraOC(True);
  //
  VS_CRC_PF.MostraFormVSClassifOneRetFichaRMP(
    Pal01,
    Pal02,
    Pal03,
    Pal04,
    Pal05,
    Pal06,
    Self);
  //
  EdDigitador.ValueVariant := Digitador;
  EdRevisor.ValueVariant   := Revisor;
  //
*)
end;

procedure TFmVSClassifOneNw3.SbEqualizeClick(Sender: TObject);
begin
  ReopenEqualize(False);
end;

procedure TFmVSClassifOneNw3.SGAllKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DELETE then
    UpdDelCouroSelecionado(stDel)
  else
  if Key = VK_RETURN then
    UpdDelCouroSelecionado(stUpd);
end;

procedure TFmVSClassifOneNw3.SGDefeiDefinKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DELETE then
    if SGDefeiDefin.Row > 0 then
      MyObjects.ExcluiLinhaStringGrid(SGDefeiDefin, True);
end;

{
function TFmVSClassifOneNw3.SubstituiOC_Antigo(): Boolean;
var
  Codigo, CacCod: Integer;
  MovimID: TEstqMovimID;
begin
  Result := False;
  if DBCheck.CriaFm(TFmVSClaArtPrpMDz, FmVSClaArtPrpMDz, afmoNegarComAviso) then
  begin
    FmVSClaArtPrpMDz.ImgTipo.SQLType := stIns;
    //
    FmVSClaArtPrpMDz.EdPallet1.ValueVariant := QrVSPaClaCabLstPal01.Value;
    FmVSClaArtPrpMDz.EdPallet2.ValueVariant := QrVSPaClaCabLstPal02.Value;
    FmVSClaArtPrpMDz.EdPallet3.ValueVariant := QrVSPaClaCabLstPal03.Value;
    FmVSClaArtPrpMDz.EdPallet4.ValueVariant := QrVSPaClaCabLstPal04.Value;
    FmVSClaArtPrpMDz.EdPallet5.ValueVariant := QrVSPaClaCabLstPal05.Value;
    FmVSClaArtPrpMDz.EdPallet6.ValueVariant := QrVSPaClaCabLstPal06.Value;
    //
    FmVSClaArtPrpMDz.CBPallet1.KeyValue := QrVSPaClaCabLstPal01.Value;
    FmVSClaArtPrpMDz.CBPallet2.KeyValue := QrVSPaClaCabLstPal02.Value;
    FmVSClaArtPrpMDz.CBPallet3.KeyValue := QrVSPaClaCabLstPal03.Value;
    FmVSClaArtPrpMDz.CBPallet4.KeyValue := QrVSPaClaCabLstPal04.Value;
    FmVSClaArtPrpMDz.CBPallet5.KeyValue := QrVSPaClaCabLstPal05.Value;
    FmVSClaArtPrpMDz.CBPallet6.KeyValue := QrVSPaClaCabLstPal06.Value;
    //
    FmVSClaArtPrpMDz.ShowModal;
    Codigo := FmVSClaArtPrpMDz.FCodigo;
    CacCod := FmVSClaArtPrpMDz.FCacCod;
    MovimID := FmVSClaArtPrpMDz.FMovimID;
    FmVSClaArtPrpMDz.Destroy;
    //
    if Codigo <> 0 then
    begin
      FCodigo := Codigo;
      FCacCod := CacCod;
      FMovimID := MovimID;
      ReopenVSPaClaCab();
      //
      Result := True;
    end else
      Result := False;
  end;
end;
}

function TFmVSClassifOneNw3.SubstituiOC_Novo(): Boolean;
var
  Codigo, CacCod: Integer;
  MovimID: TEstqMovimID;
begin
  Result := False;
  if DBCheck.CriaFm(TFmVSClaArtPrpQnz, FmVSClaArtPrpQnz, afmoNegarComAviso) then
  begin
    FmVSClaArtPrpQnz.ImgTipo.SQLType := stIns;
    //
    FmVSClaArtPrpQnz.EdPallet01.ValueVariant := QrVSPaClaCabLstPal01.Value;
    FmVSClaArtPrpQnz.EdPallet02.ValueVariant := QrVSPaClaCabLstPal02.Value;
    FmVSClaArtPrpQnz.EdPallet03.ValueVariant := QrVSPaClaCabLstPal03.Value;
    FmVSClaArtPrpQnz.EdPallet04.ValueVariant := QrVSPaClaCabLstPal04.Value;
    FmVSClaArtPrpQnz.EdPallet05.ValueVariant := QrVSPaClaCabLstPal05.Value;
    FmVSClaArtPrpQnz.EdPallet06.ValueVariant := QrVSPaClaCabLstPal06.Value;
    FmVSClaArtPrpQnz.EdPallet07.ValueVariant := QrVSPaClaCabLstPal07.Value;
    FmVSClaArtPrpQnz.EdPallet08.ValueVariant := QrVSPaClaCabLstPal08.Value;
    FmVSClaArtPrpQnz.EdPallet09.ValueVariant := QrVSPaClaCabLstPal09.Value;
    FmVSClaArtPrpQnz.EdPallet10.ValueVariant := QrVSPaClaCabLstPal10.Value;
    FmVSClaArtPrpQnz.EdPallet11.ValueVariant := QrVSPaClaCabLstPal11.Value;
    FmVSClaArtPrpQnz.EdPallet12.ValueVariant := QrVSPaClaCabLstPal12.Value;
    FmVSClaArtPrpQnz.EdPallet13.ValueVariant := QrVSPaClaCabLstPal13.Value;
    FmVSClaArtPrpQnz.EdPallet14.ValueVariant := QrVSPaClaCabLstPal14.Value;
    FmVSClaArtPrpQnz.EdPallet15.ValueVariant := QrVSPaClaCabLstPal15.Value;
    //
    FmVSClaArtPrpQnz.CBPallet01.KeyValue := QrVSPaClaCabLstPal01.Value;
    FmVSClaArtPrpQnz.CBPallet02.KeyValue := QrVSPaClaCabLstPal02.Value;
    FmVSClaArtPrpQnz.CBPallet03.KeyValue := QrVSPaClaCabLstPal03.Value;
    FmVSClaArtPrpQnz.CBPallet04.KeyValue := QrVSPaClaCabLstPal04.Value;
    FmVSClaArtPrpQnz.CBPallet05.KeyValue := QrVSPaClaCabLstPal05.Value;
    FmVSClaArtPrpQnz.CBPallet06.KeyValue := QrVSPaClaCabLstPal06.Value;
    FmVSClaArtPrpQnz.CBPallet07.KeyValue := QrVSPaClaCabLstPal07.Value;
    FmVSClaArtPrpQnz.CBPallet08.KeyValue := QrVSPaClaCabLstPal08.Value;
    FmVSClaArtPrpQnz.CBPallet09.KeyValue := QrVSPaClaCabLstPal09.Value;
    FmVSClaArtPrpQnz.CBPallet10.KeyValue := QrVSPaClaCabLstPal10.Value;
    FmVSClaArtPrpQnz.CBPallet11.KeyValue := QrVSPaClaCabLstPal11.Value;
    FmVSClaArtPrpQnz.CBPallet12.KeyValue := QrVSPaClaCabLstPal12.Value;
    FmVSClaArtPrpQnz.CBPallet13.KeyValue := QrVSPaClaCabLstPal13.Value;
    FmVSClaArtPrpQnz.CBPallet14.KeyValue := QrVSPaClaCabLstPal14.Value;
    FmVSClaArtPrpQnz.CBPallet15.KeyValue := QrVSPaClaCabLstPal15.Value;
    //
    FmVSClaArtPrpQnz.ShowModal;
    Codigo := FmVSClaArtPrpQnz.FCodigo;
    CacCod := FmVSClaArtPrpQnz.FCacCod;
    MovimID := FmVSClaArtPrpQnz.FMovimID;
    FmVSClaArtPrpQnz.Destroy;
    //
    if Codigo <> 0 then
    begin
      FCodigo := Codigo;
      FCacCod := CacCod;
      FMovimID := MovimID;
      ReopenVSPaClaCab();
      //
      Result := True;
    end else
      Result := False;
  end;
end;

procedure TFmVSClassifOneNw3.TentarFocarEdArea();
begin
  ConfigPnSubClass();
  if not FCriando and PnArea.Enabled and PnDigitacao.Enabled and
  EdArea.Visible and EdArea.Enabled and EdArea.CanFocus then
    EdArea.SetFocus
end;

(*
  Completo 1,450         >>
  Sem QrSumX 1,350       >> - 0,100
  Sem QrItensX 1,050     >> - 0,300
  Sem QrAll 0,750        >> - 0,300
*)

{
Buscar total de pecas e area de pallet pelo Saldo!
calcular imei ao:
-retirar pallet ou
-encerrar OC ou
-fechar janela!
Exclusao em massa
}

end.
