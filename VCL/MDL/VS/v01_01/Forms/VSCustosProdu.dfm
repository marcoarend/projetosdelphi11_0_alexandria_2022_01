object FmVSCustosProdu: TFmVSCustosProdu
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-254 :: Custos de Produ'#231#227'o'
  ClientHeight = 338
  ClientWidth = 796
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 796
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 748
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 700
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 247
        Height = 32
        Caption = 'Custos de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 247
        Height = 32
        Caption = 'Custos de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 247
        Height = 32
        Caption = 'Custos de Produ'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 209
    Width = 796
    Height = 59
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 792
      Height = 42
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 25
        Width = 792
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 268
    Width = 796
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 650
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 648
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 796
    Height = 161
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 796
      Height = 85
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 12
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 28
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 64
        Top = 28
        Width = 469
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEFILIAL'
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object GroupBox3: TGroupBox
        Left = 544
        Top = 8
        Width = 245
        Height = 73
        Caption = ' Per'#237'odo: '
        TabOrder = 2
        object TPDataIni: TdmkEditDateTimePicker
          Left = 8
          Top = 40
          Width = 112
          Height = 21
          CalColors.TextColor = clMenuText
          Date = 37636.000000000000000000
          Time = 0.777157974502188200
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object CkDataIni: TCheckBox
          Left = 8
          Top = 20
          Width = 112
          Height = 17
          Caption = 'Data inicial'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object CkDataFim: TCheckBox
          Left = 124
          Top = 20
          Width = 112
          Height = 17
          Caption = 'Data final'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object TPDataFim: TdmkEditDateTimePicker
          Left = 124
          Top = 40
          Width = 112
          Height = 21
          Date = 37636.000000000000000000
          Time = 0.777203761601413100
          TabOrder = 3
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
      end
      object CkSdoPositiv: TCheckBox
        Left = 12
        Top = 56
        Width = 201
        Height = 17
        Caption = 'Informar somente saldos positivos.'
        Checked = True
        State = cbChecked
        TabOrder = 3
      end
    end
    object PCTipoRel: TPageControl
      Left = 0
      Top = 85
      Width = 796
      Height = 76
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Artigo '
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 788
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitHeight = 44
          object Label2: TLabel
            Left = 4
            Top = 4
            Width = 30
            Height = 13
            Caption = 'Artigo:'
          end
          object EdGraGruX: TdmkEditCB
            Left = 4
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cargo'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBGraGruX
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBGraGruX: TdmkDBLookupComboBox
            Left = 60
            Top = 20
            Width = 469
            Height = 21
            KeyField = 'Controle'
            ListField = 'NO_PRD_TAM_COR'
            ListSource = DsGraGruX
            TabOrder = 1
            dmkEditCB = EdGraGruX
            QryCampo = 'Cargo'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Grupo de Estoque'
        ImageIndex = 1
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 788
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 4
            Top = 4
            Width = 88
            Height = 13
            Caption = 'Grupo de estoque:'
          end
          object EdGraGruY: TdmkEditCB
            Left = 4
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cargo'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBGraGruY
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBGraGruY: TdmkDBLookupComboBox
            Left = 60
            Top = 20
            Width = 469
            Height = 21
            Enabled = False
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsGraGruY
            TabOrder = 1
            dmkEditCB = EdGraGruY
            QryCampo = 'Cargo'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 4
    Top = 65523
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGruY, ggy.Nome NO_GraGruY, '
      'ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED,'
      'unm.Grandeza'
      'FROM gragrux ggx '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY'
      'WHERE ggx.GraGruY=1024'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 220
    Top = 68
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXGraGruY: TIntegerField
      FieldName = 'GraGruY'
      Required = True
    end
    object QrGraGruXNO_GraGruY: TWideStringField
      FieldName = 'NO_GraGruY'
      Size = 255
    end
    object QrGraGruXGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 220
    Top = 120
  end
  object DsGerados: TDataSource
    DataSet = QrGerados
    Left = 452
    Top = 68
  end
  object frxDsGerados: TfrxDBDataset
    UserName = 'frxDsGerados'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'DataHora=DataHora'
      'GGXAnt=GGXAnt'
      'Pecas=Pecas'
      'ValorTAnt=ValorTAnt'
      'AreaM2Atu=AreaM2Atu'
      'CustoM2Ant=CustoM2Ant'
      'PesoKgAtu=PesoKgAtu'
      'CustoKgAnt=CustoKgAnt'
      'PesoKgAnt=PesoKgAnt'
      'KgPecaAnt=KgPecaAnt'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtArM2=SdoVrtArM2'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'InteirosSVPc=InteirosSVPc'
      'ValorTAtu=ValorTAtu'
      'CustoM2Atu=CustoM2Atu'
      'CustoKgAtu=CustoKgAtu'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'ValorMPAnt=ValorMPAnt'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'NFeNum=NFeNum'
      'InteirosPeca=InteirosPeca'
      'NO_PRD_TAM_COR_Ant=NO_PRD_TAM_COR_Ant'
      'NO_PRD_TAM_COR_Atu=NO_PRD_TAM_COR_Atu'
      'Observ=Observ'
      'DstGGX=DstGGX'
      'CredPereImposto=CredPereImposto'
      'CredValrImposto=CredValrImposto'
      'CustoMOAtu=CustoMOAtu')
    DataSet = QrGerados
    BCDToCurrency = False
    DataSetOptions = []
    Left = 450
    Top = 119
  end
  object frxWET_CURTI_254_1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 45038.752643136600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxWET_CURTI_248_A_1GetValue
    Left = 548
    Top = 12
    Datasets = <
      item
        DataSet = frxDsGerados
        DataSetName = 'frxDsGerados'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsVSInnIts
        DataSetName = 'frxDsVSInnIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      PrintOnPreviousPage = True
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 64.252010000000000000
        Top = 18.897650000000000000
        Width = 1009.134510000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 1009.134510000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 994.016390000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 1009.134510000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 797.480830000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Custos de Produ'#231#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 903.307670000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378170000000000000
          Top = 45.354360000000000000
          Width = 332.598425200000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 45.354360000000000000
          Width = 332.598425200000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_GGX_GGY]')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 668.976810000000000000
          Top = 45.354360000000000000
          Width = 332.598425200000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 264.567100000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsVSInnIts
        DataSetName = 'frxDsVSInnIts'
        RowCount = 0
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."ValorMP"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsVSInnIts."Cus' +
              'FrtAvuls">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'ValorT'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Width = 60.472440944881900000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSInnIts."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 937.323440000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataField = 'SdoVrtPeca'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."SdoVrtPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 967.559680000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'SdoVrtPeso'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."SdoVrtPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Left = 279.685220000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo153: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsVSInnIts."Cus' +
              'KgComiss">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo154: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsVSInnIts."Cus' +
              'toComiss">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo156: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsVSInnIts."Cre' +
              'dPereImposto">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo157: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsVSInnIts."Cre' +
              'dValrImposto">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo161: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Marca'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."Marca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo162: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."SerieFch"] - [frxDsVSInnIts."Ficha"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 714.331170000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsVSInnIts."Qtd' +
              'GerArM2">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 646.299630000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."CustoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 789.921770000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataField = 'kgm2'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."kgm2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo125: TfrxMemoView
          AllowVectorExport = True
          Left = 763.465060000000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataField = 'm2Pc'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."m2Pc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 684.094930000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataField = 'ArePecas'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."ArePecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo139: TfrxMemoView
          AllowVectorExport = True
          Left = 846.614720000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,##0.00;-#,###,##0.00; '#39', <frxDsVSInnIts."Pes' +
              'QtdGerPeso">)]')
          ParentFont = False
          Duplicates = dmClear
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo140: TfrxMemoView
          AllowVectorExport = True
          Left = 816.378480000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."pesPecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo149: TfrxMemoView
          AllowVectorExport = True
          Left = 899.528140000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'kgPc'
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSInnIts."kgPc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 816.378480000000000000
        Width = 1009.134510000000000000
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Width = 744.567410000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 687.874460000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 52.913410240000000000
        Top = 143.622140000000000000
        Width = 1009.134510000000000000
        ReprintOnNewPage = True
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 37.795300000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 22.677180000000000000
          Width = 434.645754720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Entrada')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Top = 37.795300000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ MPrima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Top = 37.795300000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Frete')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Top = 37.795300000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ total')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 60.472440944881900000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data/hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 937.323440000000000000
          Top = 37.795300000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 937.323440000000000000
          Top = 22.677180000000000000
          Width = 71.810874720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldo entrada')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 22.677180000000000000
          Width = 188.976456060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 967.559680000000000000
          Top = 37.795300000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'PesoKg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 279.685220000000000000
          Top = 37.795300000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo155: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 37.795300000000000000
          Width = 75.590578030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '$/kg e total comiss'#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo158: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 37.795300000000000000
          Width = 83.149638030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '% e $ cred. impostos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo160: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 37.795300000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo163: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Top = 37.795300000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie-ficha')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo164: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 37.795300000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 714.331170000000000000
          Top = 37.795300000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'm'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 646.299630000000000000
          Top = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$/kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 789.921770000000000000
          Top = 37.795287800000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg/m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 763.465060000000000000
          Top = 37.795287800000000000
          Width = 26.456692910000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'm'#178'/p'#231)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo133: TfrxMemoView
          AllowVectorExport = True
          Left = 684.094930000000000000
          Top = 22.677180000000000000
          Width = 132.283427950000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Gera'#231#227'o de '#225'rea')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 684.094930000000000000
          Top = 37.795300000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo141: TfrxMemoView
          AllowVectorExport = True
          Left = 846.614720000000000000
          Top = 37.795300000000000000
          Width = 52.913388270000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo142: TfrxMemoView
          AllowVectorExport = True
          Left = 816.378480000000000000
          Top = 37.795300000000000000
          Width = 30.236220470000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo147: TfrxMemoView
          AllowVectorExport = True
          Left = 816.378480000000000000
          Top = 22.677180000000000000
          Width = 120.944837950000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Gera'#231#227'o de peso')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo150: TfrxMemoView
          AllowVectorExport = True
          Left = 899.528140000000000000
          Top = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'kg/pe'#231'a')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo191: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 1009.134466060000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ARTIGOS DE ORIGEM')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015767560000000000
        Top = 355.275820000000000000
        Width = 1009.134510000000000000
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 249.448980000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL ARTIGOS DE ORIGEM:')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 3.779530000000000000
          Width = 30.236220470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."Pecas">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."ValorMP">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."CusFrtAvuls">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 279.685220000000000000
          Top = 3.779530000000000000
          Width = 41.574803150000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."PesoKg">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 3.779530000000000000
          Width = 26.456692910000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."CustoComiss">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 3.779530000000000000
          Width = 26.456692910000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."CredValrImposto">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Top = 3.779530000000000000
          Width = 60.472440940000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."ValorT">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 937.323440000000000000
          Top = 3.779530000000000000
          Width = 30.236220470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."SdoVrtPeca">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 967.559680000000000000
          Top = 3.779530000000000000
          Width = 41.574803150000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."SdoVrtPeso">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Left = 646.299630000000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."CustoKg">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo131: TfrxMemoView
          AllowVectorExport = True
          Left = 714.331170000000000000
          Top = 3.779530000000000000
          Width = 49.133858270000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."QtdGerArM2">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo143: TfrxMemoView
          AllowVectorExport = True
          Left = 684.094930000000000000
          Top = 3.779530000000000000
          Width = 30.236220470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."ArePecas">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo144: TfrxMemoView
          AllowVectorExport = True
          Left = 846.614720000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."PesQtdGerPeso">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo148: TfrxMemoView
          AllowVectorExport = True
          Left = 816.378480000000000000
          Top = 3.779530000000000000
          Width = 30.236220470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."PesPecas">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo176: TfrxMemoView
          AllowVectorExport = True
          Left = 789.921770000000000000
          Top = 3.779530000000000000
          Width = 26.456692910000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[('
            'SUM(<frxDsVSInnIts."PesQtdGerPeso">, MD001)'
            '/'
            'SUM(<frxDsVSInnIts."QtdGerArM2">, MD001)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo177: TfrxMemoView
          AllowVectorExport = True
          Left = 763.465060000000000000
          Top = 3.779530000000000000
          Width = 26.456692910000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[('
            'IIF(SUM(<frxDsVSInnIts."ArePecas">, MD001) <> 0,'
            'SUM(<frxDsVSInnIts."QtdGerArM2">, MD001)'
            '/'
            'SUM(<frxDsVSInnIts."ArePecas">, MD001)'
            ',0)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo178: TfrxMemoView
          AllowVectorExport = True
          Left = 899.528140000000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[('
            'SUM(<frxDsVSInnIts."PesQtdGerPeso">, MD001)'
            '/'
            'SUM(<frxDsVSInnIts."pesPecas">, MD001)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 578.268090000000000000
        Width = 1009.134510000000000000
        DataSet = frxDsGerados
        DataSetName = 'frxDsGerados'
        RowCount = 0
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGerados."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39',<frxDsGerado' +
              's."AreaM2Atu">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'ValorTAtu'
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGerados."ValorTAtu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Width = 60.472440944881890000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGerados."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 786.142240000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'SdoVrtPeca'
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGerados."SdoVrtPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 857.953310000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'SdoVrtArM2'
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGerados."SdoVrtArM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Width = 45.354330708661420000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGerados."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo184: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'Marca'
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGerados."Marca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo185: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGerados."SerieFch"] - [frxDsGerados."Ficha"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo179: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'InteirosPeca'
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGerados."InteirosPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo187: TfrxMemoView
          AllowVectorExport = True
          Left = 820.158010000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataField = 'InteirosSVPc'
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGerados."InteirosSVPc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo203: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'PesoKgAnt'
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGerados."PesoKgAnt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo204: TfrxMemoView
          AllowVectorExport = True
          Left = 287.244280000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.0000;-#,###,###,##0.0000; '#39',<frxDsGe' +
              'rados."CustoMOKg">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo207: TfrxMemoView
          AllowVectorExport = True
          Left = 706.772110000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39',<frxDsGerado' +
              's."CustoM2Atu">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo209: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.0000;-#,###,###,##0.0000; '#39',<frxDsGe' +
              'rados."CustoKgAtu">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo211: TfrxMemoView
          AllowVectorExport = True
          Left = 740.787880000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39',<frxDsGerado' +
              's."NotaMPAG">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo213: TfrxMemoView
          AllowVectorExport = True
          Left = 903.307670000000000000
          Width = 105.826771650000000000
          Height = 15.118110240000000000
          DataField = 'Observ'
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGerados."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo216: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39',<frxDsGerado' +
              's."CredValrImposto">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo215: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Width = 22.677165350000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39',<frxDsGerado' +
              's."CredPereImposto">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo219: TfrxMemoView
          AllowVectorExport = True
          Left = 570.709030000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39',<frxDsGerado' +
              's."PesoKgAtu">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo229: TfrxMemoView
          AllowVectorExport = True
          Left = 241.889920000000000000
          Width = 45.354330708661420000
          Height = 15.118110240000000000
          DataField = 'ValorMPAnt'
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsGerados."ValorMPAnt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo232: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39',<frxDsGerado' +
              's."CustoMOAtu">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 52.913410240000000000
        Top = 411.968770000000000000
        Width = 1009.134510000000000000
        ReprintOnNewPage = True
        object Memo108: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 37.795300000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo109: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 22.677180000000000000
          Width = 287.244084720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Entrada - Gera'#231#227'o do artigo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Top = 37.795300000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 37.795300000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ total')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo113: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 60.472440944881890000
          Height = 30.236230240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data/hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 786.142240000000000000
          Top = 37.795300000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo129: TfrxMemoView
          AllowVectorExport = True
          Left = 786.142240000000000000
          Top = 22.677180000000000000
          Width = 117.165234720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo130: TfrxMemoView
          AllowVectorExport = True
          Left = 857.953310000000000000
          Top = 37.795300000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo137: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 22.677180000000000000
          Width = 136.063036060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo151: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 37.795300000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo182: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 37.795300000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie-ficha')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo183: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 37.795300000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IME-I')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo186: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inteiros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo188: TfrxMemoView
          AllowVectorExport = True
          Left = 820.158010000000000000
          Top = 37.795300000000000000
          Width = 37.795275590000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Inteiros')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo192: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 1009.134143860000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ARTIGOS DE DESTINO')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo205: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Top = 37.795300000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo206: TfrxMemoView
          AllowVectorExport = True
          Left = 287.244280000000000000
          Top = 37.795300000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ MO kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo208: TfrxMemoView
          AllowVectorExport = True
          Left = 706.772110000000000000
          Top = 37.795300000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo210: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Top = 37.795300000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo212: TfrxMemoView
          AllowVectorExport = True
          Left = 740.787880000000000000
          Top = 37.795300000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Nota')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo214: TfrxMemoView
          AllowVectorExport = True
          Left = 903.307670000000000000
          Top = 22.677180000000000000
          Width = 105.826771650000000000
          Height = 30.236230240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo217: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Top = 37.795300000000000000
          Width = 68.031518030000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '% e $ cred. impostos')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo218: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Top = 22.677180000000000000
          Width = 302.362356060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dados base e total do custo')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo220: TfrxMemoView
          AllowVectorExport = True
          Left = 570.709030000000000000
          Top = 37.795300000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo230: TfrxMemoView
          AllowVectorExport = True
          Left = 241.889920000000000000
          Top = 37.795300000000000000
          Width = 45.354330708661420000
          Height = 15.118110240000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$M.Prima')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo233: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Top = 37.795300000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ M.Obra')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 34.015750470000000000
        Top = 721.890230000000000000
        Width = 1009.134510000000000000
        object Memo169: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 196.535516060000000000
          Height = 30.236220470000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL GERAL')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo244: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Top = 3.779530000000000000
          Width = 34.015750470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."Pecas">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo245: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Top = 3.779530000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39','
            'SUM(<frxDsGerados."AreaM2Atu">, MD002))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo246: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 3.779530000000000000
          Width = 52.913385830000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."ValorTAtu">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo247: TfrxMemoView
          AllowVectorExport = True
          Left = 706.772110000000000000
          Top = 3.779530000000000000
          Width = 34.015750470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39',('
            'SUM(<frxDsGerados."ValorTAtu">, MD002)'
            '/'
            'SUM(<frxDsGerados."AreaM2Atu">, MD002)'
            '))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo248: TfrxMemoView
          AllowVectorExport = True
          Left = 786.142240000000000000
          Top = 3.779530000000000000
          Width = 34.015750470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."SdoVrtPeca">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo249: TfrxMemoView
          AllowVectorExport = True
          Left = 857.953310000000000000
          Top = 3.779530000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."SdoVrtArM2">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo250: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."InteirosPeca">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo251: TfrxMemoView
          AllowVectorExport = True
          Left = 820.158010000000000000
          Top = 3.779530000000000000
          Width = 37.795275590000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."InteirosSVPc">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo252: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Top = 3.779530000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."PesoKgAnt">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo253: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Top = 3.779530000000000000
          Width = 22.677165350000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo254: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39','
            'SUM(<frxDsGerados."CredValrImposto">, MD002))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo255: TfrxMemoView
          AllowVectorExport = True
          Left = 287.244280000000000000
          Top = 3.779530000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.0000;-#,###,###,##0.0000; '#39',('
            '  ('
            '  SUM(<frxDsGerados."CustoMOTot">, MD002)'
            '  +'
            '  SUM(<frxDsGerados."CredValrImposto">, MD002)'
            '  )'
            '  /'
            '  SUM(<frxDsGerados."PesoKgAnt">, MD002)'
            '))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo256: TfrxMemoView
          AllowVectorExport = True
          Left = 903.307670000000000000
          Top = 3.779530000000000000
          Width = 105.826771650000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo257: TfrxMemoView
          AllowVectorExport = True
          Left = 740.787880000000000000
          Top = 3.779530000000000000
          Width = 45.354342910000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo258: TfrxMemoView
          AllowVectorExport = True
          Left = 570.709030000000000000
          Top = 3.779530000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."PesoKgAtu">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo259: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Top = 3.779530000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.0000;-#,###,###,##0.0000; '#39',('
            '  SUM(<frxDsGerados."ValorTAtu">, MD002)'
            '  /'
            '  SUM(<frxDsGerados."PesoKgAtu">, MD002)'
            '))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo260: TfrxMemoView
          AllowVectorExport = True
          Left = 241.889920000000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."ValorMPAnt">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo261: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Top = 3.779530000000000000
          Width = 45.354330710000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39','
            'SUM(<frxDsGerados."CustoMOAtu">, MD002))]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 22.677165354330710000
        ParentFont = False
        Top = 219.212740000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsVSInnIts."GraGruX"'
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779527559055118000
          Width = 1009.134466060000000000
          Height = 18.897637800000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSInnIts."GraGruX"]-[frxDsVSInnIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 302.362400000000000000
        Width = 1009.134510000000000000
        object Memo99: TfrxMemoView
          AllowVectorExport = True
          Width = 249.448936060000000000
          Height = 30.236220470000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL [frxDsVSInnIts."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo101: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 30.236220470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."Pecas">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 52.913385830000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."ValorMP">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 374.173470000000000000
          Width = 52.913385830000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."CusFrtAvuls">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          AllowVectorExport = True
          Left = 279.685220000000000000
          Width = 41.574803150000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."PesoKg">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Width = 26.456692910000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 49.133858270000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."CustoComiss">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Width = 26.456692910000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 56.692913390000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."CredValrImposto">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo116: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Width = 60.472440940000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."ValorT">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          AllowVectorExport = True
          Left = 937.323440000000000000
          Width = 30.236220470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."SdoVrtPeca">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo118: TfrxMemoView
          AllowVectorExport = True
          Left = 967.559680000000000000
          Width = 41.574803150000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."SdoVrtPeso">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo119: TfrxMemoView
          AllowVectorExport = True
          Left = 714.331170000000000000
          Width = 49.133858270000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."QtdGerArM2">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo121: TfrxMemoView
          AllowVectorExport = True
          Left = 646.299630000000000000
          Width = 37.795275590000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."CustoKg">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo126: TfrxMemoView
          AllowVectorExport = True
          Left = 789.921770000000000000
          Width = 26.456692910000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[('
            'SUM(<frxDsVSInnIts."PesQtdGerPeso">, MD001)'
            '/'
            'SUM(<frxDsVSInnIts."QtdGerArM2">, MD001)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo127: TfrxMemoView
          AllowVectorExport = True
          Left = 763.465060000000000000
          Width = 26.456692910000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[('
            'IIF(SUM(<frxDsVSInnIts."ArePecas">, MD001) <> 0,'
            'SUM(<frxDsVSInnIts."QtdGerArM2">, MD001)'
            '/'
            'SUM(<frxDsVSInnIts."ArePecas">, MD001)'
            ',0)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo138: TfrxMemoView
          AllowVectorExport = True
          Left = 684.094930000000000000
          Width = 30.236220470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."ArePecas">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo145: TfrxMemoView
          AllowVectorExport = True
          Left = 846.614720000000000000
          Width = 52.913385830000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."PesQtdGerPeso">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo146: TfrxMemoView
          AllowVectorExport = True
          Left = 816.378480000000000000
          Width = 30.236220470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsVSInnIts."PesPecas">, MD001)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo159: TfrxMemoView
          AllowVectorExport = True
          Left = 899.528140000000000000
          Width = 37.795275590000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[('
            'SUM(<frxDsVSInnIts."PesQtdGerPeso">, MD001)'
            '/'
            'SUM(<frxDsVSInnIts."pesPecas">, MD001)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 22.677170240000000000
        ParentFont = False
        Top = 487.559370000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsGerados."DstGGX"'
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 1009.134143860000000000
          Height = 18.897637800000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGerados."DstGGX"] - [frxDsGerados."NO_PRD_TAM_COR_Atu"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 668.976810000000000000
        Width = 1009.134510000000000000
        object Memo132: TfrxMemoView
          AllowVectorExport = True
          Width = 196.535516060000000000
          Height = 30.236220470000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL [frxDsGerados."NO_PRD_TAM_COR_Atu"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo170: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 34.015750470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."Pecas">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo171: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39','
            'SUM(<frxDsGerados."AreaM2Atu">, MD002))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo172: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 52.913385830000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."ValorTAtu">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo173: TfrxMemoView
          AllowVectorExport = True
          Left = 706.772110000000000000
          Width = 34.015750470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39',('
            'SUM(<frxDsGerados."ValorTAtu">, MD002)'
            '/'
            'SUM(<frxDsGerados."AreaM2Atu">, MD002)'
            '))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo174: TfrxMemoView
          AllowVectorExport = True
          Left = 786.142240000000000000
          Width = 34.015750470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."SdoVrtPeca">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo180: TfrxMemoView
          AllowVectorExport = True
          Left = 857.953310000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."SdoVrtArM2">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo181: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Width = 37.795275590000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."InteirosPeca">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo189: TfrxMemoView
          AllowVectorExport = True
          Left = 820.158010000000000000
          Width = 37.795275590000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."InteirosSVPc">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo190: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."PesoKgAnt">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo235: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Width = 22.677165350000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo236: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 45.354330710000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39','
            'SUM(<frxDsGerados."CredValrImposto">, MD002))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo237: TfrxMemoView
          AllowVectorExport = True
          Left = 287.244280000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.0000;-#,###,###,##0.0000; '#39',('
            '  ('
            '  SUM(<frxDsGerados."CustoMOTot">, MD002)'
            '  +'
            '  SUM(<frxDsGerados."CredValrImposto">, MD002)'
            '  )'
            '  /'
            '  SUM(<frxDsGerados."PesoKgAnt">, MD002)'
            '))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo238: TfrxMemoView
          AllowVectorExport = True
          Left = 903.307670000000000000
          Width = 105.826771650000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo239: TfrxMemoView
          AllowVectorExport = True
          Left = 740.787880000000000000
          Width = 45.354342910000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo240: TfrxMemoView
          AllowVectorExport = True
          Left = 570.709030000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."PesoKgAtu">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo241: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.0000;-#,###,###,##0.0000; '#39',('
            '  SUM(<frxDsGerados."ValorTAtu">, MD002)'
            '  /'
            '  SUM(<frxDsGerados."PesoKgAtu">, MD002)'
            '))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo242: TfrxMemoView
          AllowVectorExport = True
          Left = 241.889920000000000000
          Width = 45.354330710000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."ValorMPAnt">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo243: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Width = 45.354330710000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39','
            'SUM(<frxDsGerados."CustoMOAtu">, MD002))]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 532.913730000000000000
        Width = 1009.134510000000000000
        Condition = 'frxDsGerados."GGXAnt"'
        object Memo193: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 1009.134143860000000000
          Height = 18.897637800000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGerados."GGXAnt"] - [frxDsGerados."NO_PRD_TAM_COR_Ant"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236220470000000000
        Top = 616.063390000000000000
        Width = 1009.134510000000000000
        object Memo194: TfrxMemoView
          AllowVectorExport = True
          Width = 196.535516060000000000
          Height = 30.236220470000000000
          DataSet = frxDsGerados
          DataSetName = 'frxDsGerados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL [frxDsGerados."NO_PRD_TAM_COR_Ant"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo195: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 34.015750470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."Pecas">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo196: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39','
            'SUM(<frxDsGerados."AreaM2Atu">, MD002))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo197: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 52.913385830000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."ValorTAtu">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo198: TfrxMemoView
          AllowVectorExport = True
          Left = 706.772110000000000000
          Width = 34.015750470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39',('
            'SUM(<frxDsGerados."ValorTAtu">, MD002)'
            '/'
            'SUM(<frxDsGerados."AreaM2Atu">, MD002)'
            '))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo199: TfrxMemoView
          AllowVectorExport = True
          Left = 786.142240000000000000
          Width = 34.015750470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.0n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."SdoVrtPeca">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo200: TfrxMemoView
          AllowVectorExport = True
          Left = 857.953310000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."SdoVrtArM2">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo201: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Width = 37.795275590000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."InteirosPeca">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo202: TfrxMemoView
          AllowVectorExport = True
          Left = 820.158010000000000000
          Width = 37.795275590000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.1n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."InteirosSVPc">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo221: TfrxMemoView
          AllowVectorExport = True
          Left = 196.535560000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."PesoKgAnt">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo222: TfrxMemoView
          AllowVectorExport = True
          Left = 430.866420000000000000
          Width = 22.677165350000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo223: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 45.354330710000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39','
            'SUM(<frxDsGerados."CredValrImposto">, MD002))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo224: TfrxMemoView
          AllowVectorExport = True
          Left = 287.244280000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.0000;-#,###,###,##0.0000; '#39',('
            '  ('
            '  SUM(<frxDsGerados."CustoMOTot">, MD002)'
            '  +'
            '  SUM(<frxDsGerados."CredValrImposto">, MD002)'
            '  )'
            '  /'
            '  SUM(<frxDsGerados."PesoKgAnt">, MD002)'
            '))]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo225: TfrxMemoView
          AllowVectorExport = True
          Left = 903.307670000000000000
          Width = 105.826771650000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo226: TfrxMemoView
          AllowVectorExport = True
          Left = 740.787880000000000000
          Width = 45.354342910000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo227: TfrxMemoView
          AllowVectorExport = True
          Left = 570.709030000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."PesoKgAtu">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo228: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Width = 45.354340470000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.0000;-#,###,###,##0.0000; '#39',('
            '  SUM(<frxDsGerados."ValorTAtu">, MD002)'
            '  /'
            '  SUM(<frxDsGerados."PesoKgAtu">, MD002)'
            '))]'
            '')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo231: TfrxMemoView
          AllowVectorExport = True
          Left = 241.889920000000000000
          Width = 45.354330708661420000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsGerados."ValorMPAnt">, MD002)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo234: TfrxMemoView
          AllowVectorExport = True
          Left = 332.598640000000000000
          Width = 45.354330710000000000
          Height = 30.236220470000000000
          DataSet = frxDsVSInnIts
          DataSetName = 'frxDsVSInnIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39','
            'SUM(<frxDsGerados."CustoMOAtu">, MD002))]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrGraGruY: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggy.* '
      'FROM gragruy ggy'
      'WHERE ggy.Codigo > 0'
      '')
    Left = 300
    Top = 69
    object QrGraGruYCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraGruYTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 30
    end
    object QrGraGruYNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrGraGruYOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrGraGruYLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGraGruYDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGraGruYDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGraGruYUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGraGruYUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGraGruYAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrGraGruYAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsGraGruY: TDataSource
    DataSet = QrGraGruY
    Left = 300
    Top = 121
  end
  object QrVSInnIts: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT vmi.Codigo, vmi.Controle, vmi.MovimCod,'
      'vmi.DataHora, vmi.GraGruX, vmi.Pecas, vmi.PesoKg,'
      'vmi.ValorT, vmi.ValorT / vmi.PesoKg CustoKg,'
      'IF(vmi.SdoVrtPeca > 0.0, vmi.SdoVrtPeca, 0.0) SdoVrtPeca,'
      'IF(vmi.SdoVrtPeca > 0.000, vmi.SdoVrtPeso, 0.000) SdoVrtPeso,'
      ''
      'vmi.Observ, vmi.SerieFch,'
      'vmi.Ficha, vmi.ValorMP, vmi.QtdGerArM2, vmi.NotaMPAG,'
      'vmi.Marca, vmi.NFeNum, vmi.CusFrtAvuls, vmi.CustoComiss,'
      'vmi.CusKgComiss, CredValrImposto, CredPereImposto,'
      'CONCAT(gg1.Nome,'
      
        '    IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti' +
        '.Nome)),'
      
        '    IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc' +
        '.Nome)))'
      '    NO_PRD_TAM_COR,'
      ''
      'are.Pecas ArePecas, are.PesoKg ArePesoKg, are.m2Pc, are.kgm2,'
      'pes.Pecas pesPecas, pes.QtdGerPeso PesQtdGerPeso, pes.kgPc'
      ''
      'FROM bluederm.vsmovits vmi'
      'LEFT JOIN bluederm.gragrux ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN bluederm.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN bluederm.gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN bluederm.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN bluederm.gratamits  gti ON gti.Controle=ggx.GraTamI'
      ''
      
        'LEFT JOIN _vs_custo_produ_ger_area_ are ON are.SrcNivel2=vmi.Con' +
        'trole'
      
        'LEFT JOIN _vs_custo_produ_ger_peso_ pes ON pes.SrcNivel2=vmi.Con' +
        'trole'
      ''
      
        'WHERE vmi.Controle IN (7972, 7973, 7974, 7975, 7976, 7977, 7978,' +
        ' 7979, 7980)'
      'ORDER BY vmi.GraGruX, vmi.DataHora, vmi.Controle')
    Left = 376
    Top = 16
    object QrVSInnItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSInnItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSInnItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSInnItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrVSInnItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSInnItsPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrVSInnItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrVSInnItsValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrVSInnItsCustoKg: TFloatField
      FieldName = 'CustoKg'
    end
    object QrVSInnItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      Required = True
    end
    object QrVSInnItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      Required = True
    end
    object QrVSInnItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSInnItsSerieFch: TIntegerField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSInnItsFicha: TIntegerField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSInnItsValorMP: TFloatField
      FieldName = 'ValorMP'
      Required = True
    end
    object QrVSInnItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      Required = True
    end
    object QrVSInnItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
      Required = True
    end
    object QrVSInnItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSInnItsNFeNum: TIntegerField
      FieldName = 'NFeNum'
      Required = True
    end
    object QrVSInnItsCusFrtAvuls: TFloatField
      FieldName = 'CusFrtAvuls'
      Required = True
    end
    object QrVSInnItsCustoComiss: TFloatField
      FieldName = 'CustoComiss'
      Required = True
    end
    object QrVSInnItsCusKgComiss: TFloatField
      FieldName = 'CusKgComiss'
      Required = True
    end
    object QrVSInnItsCredValrImposto: TFloatField
      FieldName = 'CredValrImposto'
      Required = True
    end
    object QrVSInnItsCredPereImposto: TFloatField
      FieldName = 'CredPereImposto'
      Required = True
    end
    object QrVSInnItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSInnItsArePecas: TFloatField
      FieldName = 'ArePecas'
    end
    object QrVSInnItsArePesoKg: TFloatField
      FieldName = 'ArePesoKg'
    end
    object QrVSInnItsm2Pc: TFloatField
      FieldName = 'm2Pc'
    end
    object QrVSInnItskgm2: TFloatField
      FieldName = 'kgm2'
    end
    object QrVSInnItspesPecas: TFloatField
      FieldName = 'pesPecas'
    end
    object QrVSInnItsPesQtdGerPeso: TFloatField
      FieldName = 'PesQtdGerPeso'
    end
    object QrVSInnItskgPc: TFloatField
      FieldName = 'kgPc'
    end
  end
  object DsVSInnIts: TDataSource
    DataSet = QrVSInnIts
    Left = 376
    Top = 68
  end
  object frxDsVSInnIts: TfrxDBDataset
    UserName = 'frxDsVSInnIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'DataHora=DataHora'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'ValorT=ValorT'
      'CustoKg=CustoKg'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'ValorMP=ValorMP'
      'QtdGerArM2=QtdGerArM2'
      'NotaMPAG=NotaMPAG'
      'Marca=Marca'
      'NFeNum=NFeNum'
      'CusFrtAvuls=CusFrtAvuls'
      'CustoComiss=CustoComiss'
      'CusKgComiss=CusKgComiss'
      'CredValrImposto=CredValrImposto'
      'CredPereImposto=CredPereImposto'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'ArePecas=ArePecas'
      'ArePesoKg=ArePesoKg'
      'm2Pc=m2Pc'
      'kgm2=kgm2'
      'pesPecas=pesPecas'
      'PesQtdGerPeso=PesQtdGerPeso'
      'kgPc=kgPc')
    DataSet = QrVSInnIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 374
    Top = 119
  end
  object QrCorda: TMySQLQuery
    Database = Dmod.MyDB
    Left = 152
    Top = 68
  end
  object QrGerados: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT'
      'vmi.Codigo, vmi.Controle, vmi.MovimCod,'
      
        'vmi.DataHora, vmi.GraGruX  GGXAnt, -vmi.Pecas Pecas, -vmi.ValorT' +
        ' ValorTAnt,'
      
        '-vmi.QtdGerArM2 AreaM2Atu, vmi.ValorT / vmi.QtdGerArM2 CustoM2An' +
        't,'
      
        '-vmi.QtdGerPeso PesoKgAtu, vmi.ValorT / vmi.QtdGerPeso CustoKgAn' +
        't,'
      '-vmi.PesoKg PesoKgAnt,'
      'vmi.PesoKg / vmi.Pecas KgPecaAnt,'
      'vmi.Pecas/g13.Pecas * g13.SdoVrtPeca SdoVrtPeca,'
      'vmi.Pecas/g13.Pecas * g13.SdoVrtArM2 SdoVrtArM2,'
      'g13.CustoMOKg, g13.CustoMOTot, CredPereImposto, '
      '(vmi.Pecas/g13.Pecas * g13.SdoVrtPeca) *'
      '  IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) InteirosSVPc,'
      '(-vmi.ValorT + (g13.CustoMOKg * -vmi.PesoKg)) ValorTAtu,'
      
        '(-vmi.ValorT + (g13.CustoMOKg * -vmi.PesoKg)) / -vmi.QtdGerArM2 ' +
        'CustoM2Atu,'
      
        '(-vmi.ValorT + (g13.CustoMOKg * -vmi.PesoKg)) / -vmi.QtdGerPeso ' +
        'CustoKgAtu,'
      'vmi.SerieFch,'
      'vmi.Ficha, -vmi.ValorMP ValorMPAnt, vmi.NotaMPAG,'
      'vmi.Marca, vmi.NFeNum,'
      
        '-vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) InteirosP' +
        'eca,'
      'CONCAT(gg1.Nome,'
      
        '    IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti' +
        '.Nome)),'
      
        '    IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc' +
        '.Nome)))'
      '    NO_PRD_TAM_COR_Ant,'
      'g13.NO_PRD_TAM_COR NO_PRD_TAM_COR_Atu, vmi.Observ'
      'FROM bluederm.vsmovits vmi'
      'LEFT JOIN bluederm.gragrux    ggx ON ggx.Controle=vmi.GraGruX'
      'LEFT JOIN bluederm.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN bluederm.gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN bluederm.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN bluederm.gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN bluederm.gragruxcou xco ON xco.GraGruX=ggx.Controle'
      'LEFT JOIN bluederm.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1'
      
        'LEFT JOIN _vs_custo_produ_06_13_ g13 ON g13.MovimCod=vmi.MovimCo' +
        'd'
      
        'WHERE vmi.SrcNivel2 IN (3052, 3053, 3054, 3055, 3056, 3057, 3058' +
        ', 3059, 3060, 3061, 3066, 3067, 3068, 3069, 3070, 3071)'
      'AND MovimNiv=15'
      'ORDER BY vmi.GraGruX, vmi.DataHora, vmi.Controle'
      '')
    Left = 452
    Top = 16
    object QrGeradosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrGeradosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGeradosMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrGeradosDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrGeradosGGXAnt: TIntegerField
      FieldName = 'GGXAnt'
      Required = True
    end
    object QrGeradosPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrGeradosValorTAnt: TFloatField
      FieldName = 'ValorTAnt'
      Required = True
    end
    object QrGeradosAreaM2Atu: TFloatField
      FieldName = 'AreaM2Atu'
      Required = True
    end
    object QrGeradosCustoM2Ant: TFloatField
      FieldName = 'CustoM2Ant'
    end
    object QrGeradosPesoKgAtu: TFloatField
      FieldName = 'PesoKgAtu'
      Required = True
    end
    object QrGeradosCustoKgAnt: TFloatField
      FieldName = 'CustoKgAnt'
    end
    object QrGeradosPesoKgAnt: TFloatField
      FieldName = 'PesoKgAnt'
      Required = True
    end
    object QrGeradosKgPecaAnt: TFloatField
      FieldName = 'KgPecaAnt'
    end
    object QrGeradosSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrGeradosSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrGeradosCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrGeradosCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrGeradosInteirosSVPc: TFloatField
      FieldName = 'InteirosSVPc'
    end
    object QrGeradosValorTAtu: TFloatField
      FieldName = 'ValorTAtu'
    end
    object QrGeradosCustoM2Atu: TFloatField
      FieldName = 'CustoM2Atu'
    end
    object QrGeradosCustoKgAtu: TFloatField
      FieldName = 'CustoKgAtu'
    end
    object QrGeradosSerieFch: TIntegerField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrGeradosFicha: TIntegerField
      FieldName = 'Ficha'
      Required = True
    end
    object QrGeradosValorMPAnt: TFloatField
      FieldName = 'ValorMPAnt'
      Required = True
    end
    object QrGeradosNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
      Required = True
    end
    object QrGeradosMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrGeradosNFeNum: TIntegerField
      FieldName = 'NFeNum'
      Required = True
    end
    object QrGeradosInteirosPeca: TFloatField
      FieldName = 'InteirosPeca'
    end
    object QrGeradosNO_PRD_TAM_COR_Ant: TWideStringField
      FieldName = 'NO_PRD_TAM_COR_Ant'
      Size = 157
    end
    object QrGeradosNO_PRD_TAM_COR_Atu: TWideStringField
      FieldName = 'NO_PRD_TAM_COR_Atu'
      Size = 157
    end
    object QrGeradosObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrGeradosDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrGeradosCredPereImposto: TFloatField
      FieldName = 'CredPereImposto'
    end
    object QrGeradosCredValrImposto: TFloatField
      FieldName = 'CredValrImposto'
    end
    object QrGeradosCustoMOAtu: TFloatField
      FieldName = 'CustoMOAtu'
    end
  end
  object QrGerM2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SrcNivel2, -SUM(Pecas) Pecas, '
      '-SUM(PesoKg) PesoKg, SUM(QtdGerArM2) QtdGerArM2,'
      'SUM(QtdGerArM2) / SUM(Pecas) m2Pc, '
      'SUM(PesoKg) / SUM(QtdGerArM2) kgm2'
      'FROM vsmovits'
      'WHERE QtdGerArM2 <> 0'
      'AND SrcNivel2=3053'
      'GROUP BY SrcNivel2')
    Left = 636
    Top = 12
    object QrGerM2SrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrGerM2Pecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrGerM2PesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrGerM2QtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrGerM2m2Pc: TFloatField
      FieldName = 'm2Pc'
    end
    object QrGerM2kgm2: TFloatField
      FieldName = 'kgm2'
    end
  end
  object QrGerKg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SrcNivel2, -SUM(Pecas) Pecas,  '
      '-SUM(PesoKg) PesoKg, -SUM(QtdGerPeso) QtdGerPeso, '
      'SUM(QtdGerPeso) / SUM(Pecas) kgPc'
      'FROM vsmovits '
      'WHERE QtdGerArM2 = 0 '
      'AND QtdGerPeso <> 0'
      'AND SrcNivel2=3053 '
      'GROUP BY SrcNivel2 '
      ' ')
    Left = 712
    Top = 12
    object QrGerKgSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrGerKgPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrGerKgPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrGerKgQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrGerKgkgPc: TFloatField
      FieldName = 'kgPc'
    end
  end
end
