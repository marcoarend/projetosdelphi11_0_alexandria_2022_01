object FmVSImpMOEnvRet2: TFmVSImpMOEnvRet2
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-168 :: Relat'#243'rio de Controle de Cobran'#231'a de MO'
  ClientHeight = 691
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 512
        Height = 32
        Caption = 'Relat'#243'rio de Controle de Cobran'#231'a de MO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 512
        Height = 32
        Caption = 'Relat'#243'rio de Controle de Cobran'#231'a de MO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 512
        Height = 32
        Caption = 'Relat'#243'rio de Controle de Cobran'#231'a de MO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 529
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 529
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 137
        Align = alTop
        TabOrder = 0
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 42
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object Label35: TLabel
            Left = 496
            Top = 4
            Width = 240
            Height = 13
            Caption = 'Fornecedor da M'#227'o-de-obra (prestador do servi'#231'o):'
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 64
            Top = 20
            Width = 425
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            QryName = 'QrVSGerArt'
            QryCampo = 'Empresa'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdEmpresa: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryName = 'QrVSGerArt'
            QryCampo = 'Empresa'
            UpdCampo = 'Empresa'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdFornecMO: TdmkEditCB
            Left = 496
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBFornecMO
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBFornecMO: TdmkDBLookupComboBox
            Left = 552
            Top = 20
            Width = 425
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsPrestador
            TabOrder = 3
            dmkEditCB = EdFornecMO
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object Panel55: TPanel
          Left = 2
          Top = 57
          Width = 1004
          Height = 81
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object GroupBox23: TGroupBox
            Left = 8
            Top = 4
            Width = 257
            Height = 73
            Caption = ' Per'#237'odo: '
            TabOrder = 0
            object TP24DataIni: TdmkEditDateTimePicker
              Left = 8
              Top = 40
              Width = 120
              Height = 21
              CalColors.TextColor = clMenuText
              Date = 37636.777157974500000000
              Time = 37636.777157974500000000
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object Ck24DataIni: TCheckBox
              Left = 8
              Top = 20
              Width = 120
              Height = 17
              Caption = 'Data inicial'
              Checked = True
              State = cbChecked
              TabOrder = 0
            end
            object Ck24DataFim: TCheckBox
              Left = 132
              Top = 20
              Width = 120
              Height = 17
              Caption = 'Data final'
              TabOrder = 2
            end
            object TP24DataFim: TdmkEditDateTimePicker
              Left = 132
              Top = 40
              Width = 120
              Height = 21
              Date = 37636.777203761600000000
              Time = 37636.777203761600000000
              TabOrder = 3
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
          end
          object GroupBox24: TGroupBox
            Left = 268
            Top = 4
            Width = 253
            Height = 73
            Caption = ' Intervalo de NF-es emitidas: '
            TabOrder = 1
            object Panel57: TPanel
              Left = 2
              Top = 15
              Width = 249
              Height = 56
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label57: TLabel
                Left = 12
                Top = 4
                Width = 30
                Height = 13
                Caption = 'Inicial:'
              end
              object Label58: TLabel
                Left = 92
                Top = 4
                Width = 25
                Height = 13
                Caption = 'Final:'
              end
              object SpeedButton1: TSpeedButton
                Left = 224
                Top = 20
                Width = 21
                Height = 21
                Caption = '?'
                OnClick = SpeedButton1Click
              end
              object Ed24NFeIni: TdmkEdit
                Left = 8
                Top = 20
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object Ed24NFeFim: TdmkEdit
                Left = 92
                Top = 20
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object Ck24Serie: TCheckBox
                Left = 176
                Top = 0
                Width = 53
                Height = 17
                Caption = 'S'#233'rie:'
                TabOrder = 2
              end
              object Ed24Serie: TdmkEdit
                Left = 176
                Top = 20
                Width = 45
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
          end
          object GBRemessa: TGroupBox
            Left = 524
            Top = 4
            Width = 221
            Height = 73
            Caption = ' Notas de remessa de MP para M.O.: '
            TabOrder = 2
            object Panel10: TPanel
              Left = 2
              Top = 15
              Width = 217
              Height = 56
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object CkSaldo: TCheckBox
                Left = 8
                Top = 4
                Width = 209
                Height = 17
                Caption = 'Somente NF-es de remessa com saldo.'
                TabOrder = 0
              end
            end
          end
        end
      end
      object PCRelatorios: TPageControl
        Left = 0
        Top = 137
        Width = 1008
        Height = 392
        ActivePage = TsRemessa
        Align = alClient
        TabOrder = 1
        OnChange = PCRelatoriosChange
        object TsListaEnv: TTabSheet
          Caption = 'Lista de NF-es Simples e de Remessa'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 364
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Splitter1: TSplitter
              Left = 417
              Top = 0
              Height = 364
              ExplicitLeft = 772
              ExplicitTop = 108
              ExplicitHeight = 100
            end
            object dmkDBGridZTO1: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 417
              Height = 364
              Align = alLeft
              DataSource = Ds00NFes
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'SerNF'
                  Title.Caption = 'S'#233'rie'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'nNF'
                  Title.Caption = 'NF-e'
                  Width = 62
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PesoKg'
                  Title.Caption = 'Peso Kg'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea m'#178
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValorT'
                  Title.Caption = 'Valor total'
                  Width = 72
                  Visible = True
                end>
            end
            object Panel7: TPanel
              Left = 420
              Top = 0
              Width = 580
              Height = 364
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object Label2: TLabel
                Left = 0
                Top = 0
                Width = 243
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'Duplo clique no item abre janela de origem'
                Font.Charset = ANSI_CHARSET
                Font.Color = clRed
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object DBGrid2: TDBGrid
                Left = 0
                Top = 13
                Width = 580
                Height = 351
                Align = alClient
                DataSource = Ds00NFeIts
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnDblClick = DBGrid2DblClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'DataHora'
                    Title.Caption = 'Data / hora'
                    Width = 94
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Width = 49
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PesoKg'
                    Title.Caption = 'Peso Kg'
                    Width = 70
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AreaM2'
                    Title.Caption = 'Area m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValorT'
                    Title.Caption = 'Valor total'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'TipoFrete'
                    Title.Caption = 'Tipo de Frete'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Terceiro'
                    Width = 37
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_Terceiro'
                    Title.Caption = 'Nome do Terceiro'
                    Width = 177
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'VSVMI_MovimCod'
                    Title.Caption = 'IME-C'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID envio'
                    Visible = True
                  end>
              end
            end
          end
        end
        object TsRemessa: TTabSheet
          Caption = 'Notas de remessa de MP para M.O.'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 364
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Splitter2: TSplitter
              Left = 0
              Top = 200
              Width = 1000
              Height = 5
              Cursor = crVSplit
              Align = alTop
            end
            object dmkDBGridZTO2: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 1000
              Height = 200
              Align = alTop
              DataSource = Ds01NFes
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'DataHora'
                  Title.Caption = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SerNF'
                  Title.Caption = 'S'#233'rie'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'nNF'
                  Title.Caption = 'NF-e'
                  Width = 62
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PesoKg'
                  Title.Caption = 'Peso Kg'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea m'#178
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValorT'
                  Title.Caption = 'Valor total'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SdoPeca'
                  Title.Caption = 'Saldo p'#231
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SdoPeso'
                  Title.Caption = 'Saldo kg'
                  Width = 62
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SdoArM2'
                  Title.Caption = 'Saldo m'#178
                  Width = 62
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Terceiro'
                  Title.Caption = 'Fornecedor'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_TERCEIRO'
                  Title.Caption = 'Nome fornecedor'
                  Visible = True
                end>
            end
            object Panel9: TPanel
              Left = 0
              Top = 205
              Width = 1000
              Height = 159
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object Label3: TLabel
                Left = 0
                Top = 0
                Width = 70
                Height = 13
                Align = alTop
                Alignment = taCenter
                Caption = 'NFs de retorno'
                Font.Charset = ANSI_CHARSET
                Font.Color = clRed
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object dmkDBGridZTO3: TdmkDBGridZTO
                Left = 0
                Top = 13
                Width = 1000
                Height = 146
                Align = alClient
                DataSource = Ds01EnvRet
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                RowColors = <>
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NFRMP_SerNF'
                    Title.Caption = 'S'#233'rie'
                    Width = 28
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NFRMP_nNF'
                    Title.Caption = 'N'#176' NFe'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NFRMP_Terceiro'
                    Title.Caption = 'Terceiro'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NFRMP_nItem'
                    Title.Caption = 'Item'
                    Width = 18
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NFRMP_Pecas'
                    Title.Caption = 'Pe'#231'as'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NFRMP_PesoKg'
                    Title.Caption = 'Peso Kg'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NFRMP_AreaM2'
                    Title.Caption = 'Area m'#178
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NFRMP_ValorT'
                    Title.Caption = 'Valor total'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Title.Caption = 'Descri'#231#227'o'
                    Width = 600
                    Visible = True
                  end>
              end
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'NF-es de retorno e/ou cobran'#231'a de M.O.'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 269
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 577
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 621
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtPesquisa: TBitBtn
        Tag = 20
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 132
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 65528
    Top = 65523
  end
  object QrPrestador: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 516
    Top = 228
    object QrPrestadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestadorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador: TDataSource
    DataSet = QrPrestador
    Left = 516
    Top = 276
  end
  object Qr00NFes: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = Qr00NFesBeforeClose
    AfterScroll = Qr00NFesAfterScroll
    SQL.Strings = (
      'SELECT ecg.Empresa, ecg.Terceiro,'
      'IF(tra.Tipo=0, tra.RazaoSocial, tra.Nome) NO_Transp,'
      'ecg.SerCT, ecg.nCT,'
      'SUM(ecg.Pecas) Pecas, SUM(ecg.PesoKg) PesoKg,'
      'SUM(ecg.AreaM2) AreaM2, SUM(ecg.AreaP2) AreaP2,'
      'SUM(ecg. PesTrKg)  PesTrKg, SUM(ecg.CusTrKg) CusTrKg,'
      'SUM(ecg.ValorT) ValorT'
      'FROM _vs_mo_env_cte_ger ecg'
      'LEFT JOIN bluederm_meza.entidades tra ON tra.Codigo=ecg.Terceiro'
      'GROUP BY Empresa, Terceiro, SerCT, nCT'
      'ORDER BY ecg.DataCad DESC')
    Left = 608
    Top = 208
    object Qr00NFesEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr00NFesSerNF: TIntegerField
      FieldName = 'SerNF'
    end
    object Qr00NFesnNF: TIntegerField
      FieldName = 'nNF'
    end
    object Qr00NFesPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object Qr00NFesPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr00NFesAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object Qr00NFesAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr00NFesValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object Ds00NFes: TDataSource
    DataSet = Qr00NFes
    Left = 700
    Top = 208
  end
  object Qr00NFeIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM _vs_mo_env_cte_ger'
      'WHERE Empresa=-11'
      'AND Terceiro=9'
      'AND SerCT=0'
      'AND nCT=1258')
    Left = 604
    Top = 256
    object Qr00NFeItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn'
    end
    object Qr00NFeItsTipoFrete: TWideStringField
      FieldName = 'TipoFrete'
      Required = True
      Size = 10
    end
    object Qr00NFeItsTabela: TWideStringField
      FieldName = 'Tabela'
      Required = True
      Size = 15
    end
    object Qr00NFeItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr00NFeItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object Qr00NFeItsSerNF: TIntegerField
      FieldName = 'SerNF'
    end
    object Qr00NFeItsnNF: TIntegerField
      FieldName = 'nNF'
    end
    object Qr00NFeItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object Qr00NFeItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object Qr00NFeItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr00NFeItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr00NFeItsNO_Terceiro: TWideStringField
      FieldName = 'NO_Terceiro'
      Size = 100
    end
    object Qr00NFeItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr00NFeItsVSVMI_MovimCod: TIntegerField
      FieldName = 'VSVMI_MovimCod'
    end
    object Qr00NFeItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object Ds00NFeIts: TDataSource
    DataSet = Qr00NFeIts
    Left = 700
    Top = 256
  end
  object Qr01NFes: TmySQLQuery
    Database = Dmod.MyDB
    Filter = 'SdoPeca>0'
    AfterScroll = Qr01NFesAfterScroll
    SQL.Strings = (
      'SELECT ecg.Empresa, ecg.Terceiro,'
      'IF(tra.Tipo=0, tra.RazaoSocial, tra.Nome) NO_Transp,'
      'ecg.SerCT, ecg.nCT,'
      'SUM(ecg.Pecas) Pecas, SUM(ecg.PesoKg) PesoKg,'
      'SUM(ecg.AreaM2) AreaM2, SUM(ecg.AreaP2) AreaP2,'
      'SUM(ecg. PesTrKg)  PesTrKg, SUM(ecg.CusTrKg) CusTrKg,'
      'SUM(ecg.ValorT) ValorT'
      'FROM _vs_mo_env_cte_ger ecg'
      'LEFT JOIN bluederm_meza.entidades tra ON tra.Codigo=ecg.Terceiro'
      'GROUP BY Empresa, Terceiro, SerCT, nCT'
      'ORDER BY ecg.DataCad DESC')
    Left = 792
    Top = 208
    object Qr01NFesEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object Qr01NFesSerNF: TIntegerField
      FieldName = 'SerNF'
    end
    object Qr01NFesnNF: TIntegerField
      FieldName = 'nNF'
    end
    object Qr01NFesPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object Qr01NFesPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object Qr01NFesAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr01NFesAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr01NFesValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr01NFesSdoPeca: TFloatField
      FieldName = 'SdoPeca'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object Qr01NFesSdoPeso: TFloatField
      FieldName = 'SdoPeso'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object Qr01NFesSdoArM2: TFloatField
      FieldName = 'SdoArM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr01NFesTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object Qr01NFesNO_TERCEIRO: TWideStringField
      FieldName = 'NO_TERCEIRO'
      Size = 100
    end
    object Qr01NFesDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object Ds01NFes: TDataSource
    DataSet = Qr01NFes
    Left = 872
    Top = 208
  end
  object Qr01EnvRet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ret.* '
      'FROM vsmoenvret ret '
      'LEFT JOIN vsmoenvrvmi rei ON rei.VSMOEnvRet=ret.Codigo '
      'LEFT JOIN vsmoenvenv env ON rei.VSMOEnvEnv=env.Codigo '
      'WHERE env.NFEMP_Empresa=-11 '
      'AND env.NFEMP_SerNF=1 '
      'AND env.NFEMP_nNF=3345 ')
    Left = 792
    Top = 256
    object Qr01EnvRetCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr01EnvRetNFCMO_FatID: TIntegerField
      FieldName = 'NFCMO_FatID'
    end
    object Qr01EnvRetNFCMO_FatNum: TIntegerField
      FieldName = 'NFCMO_FatNum'
    end
    object Qr01EnvRetNFCMO_Empresa: TIntegerField
      FieldName = 'NFCMO_Empresa'
    end
    object Qr01EnvRetNFCMO_Terceiro: TIntegerField
      FieldName = 'NFCMO_Terceiro'
    end
    object Qr01EnvRetNFCMO_nItem: TIntegerField
      FieldName = 'NFCMO_nItem'
    end
    object Qr01EnvRetNFCMO_SerNF: TIntegerField
      FieldName = 'NFCMO_SerNF'
    end
    object Qr01EnvRetNFCMO_nNF: TIntegerField
      FieldName = 'NFCMO_nNF'
    end
    object Qr01EnvRetNFCMO_Pecas: TFloatField
      FieldName = 'NFCMO_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object Qr01EnvRetNFCMO_PesoKg: TFloatField
      FieldName = 'NFCMO_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object Qr01EnvRetNFCMO_AreaM2: TFloatField
      FieldName = 'NFCMO_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr01EnvRetNFCMO_AreaP2: TFloatField
      FieldName = 'NFCMO_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr01EnvRetNFCMO_CusMOM2: TFloatField
      FieldName = 'NFCMO_CusMOM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr01EnvRetNFCMO_CusMOKG: TFloatField
      FieldName = 'NFCMO_CusMOKG'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr01EnvRetNFCMO_ValorT: TFloatField
      FieldName = 'NFCMO_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr01EnvRetNFRMP_FatID: TIntegerField
      FieldName = 'NFRMP_FatID'
    end
    object Qr01EnvRetNFRMP_FatNum: TIntegerField
      FieldName = 'NFRMP_FatNum'
    end
    object Qr01EnvRetNFRMP_Empresa: TIntegerField
      FieldName = 'NFRMP_Empresa'
    end
    object Qr01EnvRetNFRMP_Terceiro: TIntegerField
      FieldName = 'NFRMP_Terceiro'
    end
    object Qr01EnvRetNFRMP_nItem: TIntegerField
      FieldName = 'NFRMP_nItem'
    end
    object Qr01EnvRetNFRMP_SerNF: TIntegerField
      FieldName = 'NFRMP_SerNF'
    end
    object Qr01EnvRetNFRMP_nNF: TIntegerField
      FieldName = 'NFRMP_nNF'
    end
    object Qr01EnvRetNFRMP_Pecas: TFloatField
      FieldName = 'NFRMP_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object Qr01EnvRetNFRMP_PesoKg: TFloatField
      FieldName = 'NFRMP_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object Qr01EnvRetNFRMP_AreaM2: TFloatField
      FieldName = 'NFRMP_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr01EnvRetNFRMP_AreaP2: TFloatField
      FieldName = 'NFRMP_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr01EnvRetNFRMP_ValorT: TFloatField
      FieldName = 'NFRMP_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr01EnvRetLk: TIntegerField
      FieldName = 'Lk'
    end
    object Qr01EnvRetDataCad: TDateField
      FieldName = 'DataCad'
    end
    object Qr01EnvRetDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object Qr01EnvRetUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object Qr01EnvRetUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object Qr01EnvRetAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object Qr01EnvRetAWServerID: TIntegerField
      FieldName = 'AWServerID'
    end
    object Qr01EnvRetAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
    end
    object Qr01EnvRetAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object Qr01EnvRetNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object Qr01EnvRetDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object Qr01EnvRetCFTPA_FatID: TIntegerField
      FieldName = 'CFTPA_FatID'
    end
    object Qr01EnvRetCFTPA_FatNum: TIntegerField
      FieldName = 'CFTPA_FatNum'
    end
    object Qr01EnvRetCFTPA_Empresa: TIntegerField
      FieldName = 'CFTPA_Empresa'
    end
    object Qr01EnvRetCFTPA_Terceiro: TIntegerField
      FieldName = 'CFTPA_Terceiro'
    end
    object Qr01EnvRetCFTPA_nItem: TIntegerField
      FieldName = 'CFTPA_nItem'
    end
    object Qr01EnvRetCFTPA_SerCT: TIntegerField
      FieldName = 'CFTPA_SerCT'
    end
    object Qr01EnvRetCFTPA_nCT: TIntegerField
      FieldName = 'CFTPA_nCT'
    end
    object Qr01EnvRetCFTPA_Pecas: TFloatField
      FieldName = 'CFTPA_Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object Qr01EnvRetCFTPA_PesoKg: TFloatField
      FieldName = 'CFTPA_PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object Qr01EnvRetCFTPA_AreaM2: TFloatField
      FieldName = 'CFTPA_AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr01EnvRetCFTPA_AreaP2: TFloatField
      FieldName = 'CFTPA_AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object Qr01EnvRetCFTPA_PesTrKg: TFloatField
      FieldName = 'CFTPA_PesTrKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object Qr01EnvRetCFTPA_CusTrKg: TFloatField
      FieldName = 'CFTPA_CusTrKg'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object Qr01EnvRetCFTPA_ValorT: TFloatField
      FieldName = 'CFTPA_ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object Ds01EnvRet: TDataSource
    DataSet = Qr01EnvRet
    Left = 872
    Top = 256
  end
  object frxDs01NFes: TfrxDBDataset
    UserName = 'frxDs01NFes'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Empresa=Empresa'
      'SerNF=SerNF'
      'nNF=nNF'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SdoPeca=SdoPeca'
      'SdoPeso=SdoPeso'
      'SdoArM2=SdoArM2'
      'Terceiro=Terceiro'
      'NO_TERCEIRO=NO_TERCEIRO'
      'DataHora=DataHora')
    DataSet = Qr01NFes
    BCDToCurrency = False
    Left = 792
    Top = 352
  end
  object frxDs01EnvRet: TfrxDBDataset
    UserName = 'frxDs01EnvRet'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'NFCMO_FatID=NFCMO_FatID'
      'NFCMO_FatNum=NFCMO_FatNum'
      'NFCMO_Empresa=NFCMO_Empresa'
      'NFCMO_Terceiro=NFCMO_Terceiro'
      'NFCMO_nItem=NFCMO_nItem'
      'NFCMO_SerNF=NFCMO_SerNF'
      'NFCMO_nNF=NFCMO_nNF'
      'NFCMO_Pecas=NFCMO_Pecas'
      'NFCMO_PesoKg=NFCMO_PesoKg'
      'NFCMO_AreaM2=NFCMO_AreaM2'
      'NFCMO_AreaP2=NFCMO_AreaP2'
      'NFCMO_CusMOM2=NFCMO_CusMOM2'
      'NFCMO_CusMOKG=NFCMO_CusMOKG'
      'NFCMO_ValorT=NFCMO_ValorT'
      'NFRMP_FatID=NFRMP_FatID'
      'NFRMP_FatNum=NFRMP_FatNum'
      'NFRMP_Empresa=NFRMP_Empresa'
      'NFRMP_Terceiro=NFRMP_Terceiro'
      'NFRMP_nItem=NFRMP_nItem'
      'NFRMP_SerNF=NFRMP_SerNF'
      'NFRMP_nNF=NFRMP_nNF'
      'NFRMP_Pecas=NFRMP_Pecas'
      'NFRMP_PesoKg=NFRMP_PesoKg'
      'NFRMP_AreaM2=NFRMP_AreaM2'
      'NFRMP_AreaP2=NFRMP_AreaP2'
      'NFRMP_ValorT=NFRMP_ValorT'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'AWServerID=AWServerID'
      'AWStatSinc=AWStatSinc'
      'Ativo=Ativo'
      'Nome=Nome'
      'DataHora=DataHora'
      'CFTPA_FatID=CFTPA_FatID'
      'CFTPA_FatNum=CFTPA_FatNum'
      'CFTPA_Empresa=CFTPA_Empresa'
      'CFTPA_Terceiro=CFTPA_Terceiro'
      'CFTPA_nItem=CFTPA_nItem'
      'CFTPA_SerCT=CFTPA_SerCT'
      'CFTPA_nCT=CFTPA_nCT'
      'CFTPA_Pecas=CFTPA_Pecas'
      'CFTPA_PesoKg=CFTPA_PesoKg'
      'CFTPA_AreaM2=CFTPA_AreaM2'
      'CFTPA_AreaP2=CFTPA_AreaP2'
      'CFTPA_PesTrKg=CFTPA_PesTrKg'
      'CFTPA_CusTrKg=CFTPA_CusTrKg'
      'CFTPA_ValorT=CFTPA_ValorT')
    DataSet = Qr01EnvRet
    BCDToCurrency = False
    Left = 876
    Top = 352
  end
  object frxWET_CURTI_168_01_0: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 42013.497466747690000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '(*'
      '  GH001.Visible := <VARF_AGRUP1>;'
      '  GF001.Visible := <VARF_AGRUP1>;'
      '  GH001.Condition := <VARF_GRUPO1>;'
      '  MeGrupo1Head.Memo.Text := <VARF_HEADR1>;            '
      '  MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;'
      ''
      '  GH002.Visible := <VARF_AGRUP2>;'
      '  GF002.Visible := <VARF_AGRUP2>;'
      '  GH002.Condition := <VARF_GRUPO2>;'
      '  MeGrupo2Head.Memo.Text := <VARF_HEADR2>;            '
      '  MeGrupo2Foot.Memo.Text := <VARF_FOOTR2>;'
      '*)                                            '
      'end.')
    OnGetValue = frxWET_CURTI_168_01_0GetValue
    Left = 649
    Top = 300
    Datasets = <
      item
        DataSet = frxDs00NFeIts
        DataSetName = 'frxDs00NFeIts'
      end
      item
        DataSet = frxDs00NFes
        DataSetName = 'frxDs00NFes'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        object Shape3: TfrxShapeView
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 778.583180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Lista de NF-es Simples e de Remessa')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 884.410020000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 3.779530000000000000
          Top = 41.574830000000000000
          Width = 616.063390000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Empresa: [VARF_EMPRESA]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          Left = 162.519790000000000000
          Top = 64.252010000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 419.527830000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor Total')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 215.433210000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Top = 64.252010000000000000
          Width = 49.133841180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / Hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 487.559370000000000000
          Top = 64.252010000000000000
          Width = 41.574729920000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Terceiro')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 351.496290000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 529.134199999999900000
          Top = 64.252010000000000000
          Width = 461.102616060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome terceiro')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 370.393940000000000000
          Top = 41.574830000000000000
          Width = 616.063390000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Fornecedor de MO: [VARF_FORNEC_MO]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 283.464750000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 49.133890000000000000
          Top = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 105.826840000000000000
          Top = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#176' NFe')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 158.740260000000000000
        Width = 990.236860000000000000
        DataSet = frxDs00NFes
        DataSetName = 'frxDs00NFes'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object MeValPecas: TfrxMemoView
          Left = 162.519790000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDs00NFes
          DataSetName = 'frxDs00NFes'
          DisplayFormat.FormatStr = '#,###,###,##0.0;-#,###,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs00NFes."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 419.527830000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'ValorT'
          DataSet = frxDs00NFes
          DataSetName = 'frxDs00NFes'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs00NFes."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 215.433210000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDs00NFes
          DataSetName = 'frxDs00NFes'
          DisplayFormat.FormatStr = '#,###,###,##0.000;-#,###,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs00NFes."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 351.496290000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDs00NFes
          DataSetName = 'frxDs00NFes'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs00NFes."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 283.464750000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2'
          DataSet = frxDs00NFes
          DataSetName = 'frxDs00NFes'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs00NFes."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 49.133890000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'SerNF'
          DataSet = frxDs00NFes
          DataSetName = 'frxDs00NFes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs00NFes."SerNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 105.826840000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'nNF'
          DataSet = frxDs00NFes
          DataSetName = 'frxDs00NFes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs00NFes."nNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 317.480520000000000000
        Width = 990.236860000000000000
        object Memo93: TfrxMemoView
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 668.976810000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 272.126160000000000000
        Width = 990.236860000000000000
        object Me_FTT: TfrxMemoView
          Top = 3.779529999999965000
          Width = 162.519594720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo33: TfrxMemoView
          Left = 487.559370000000000000
          Top = 3.779530000000022000
          Width = 502.677460710000000000
          Height = 15.118110240000000000
          DataSet = FmVSImpMOEnvRet.frxDsVSMO
          DataSetName = 'frxDsVSMO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 162.519790000000000000
          Top = 3.779529999999965000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.0;-#,###,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs00NFes."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 215.433210000000000000
          Top = 3.779529999999965000
          Width = 68.031505830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.000;-#,###,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs00NFes."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 283.464750000000000000
          Top = 3.779529999999965000
          Width = 68.031505830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs00NFes."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 351.496290000000000000
          Top = 3.779529999999965000
          Width = 68.031505830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs00NFes."AreaP2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 419.527830000000000000
          Top = 3.779529999999965000
          Width = 68.031505830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs00NFes."ValorT">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 196.535560000000000000
        Width = 990.236860000000000000
        DataSet = frxDs00NFeIts
        DataSetName = 'frxDs00NFeIts'
        RowCount = 0
        object Memo3: TfrxMemoView
          Width = 49.133841180000000000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDs00NFeIts
          DataSetName = 'frxDs00NFeIts'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs00NFeIts."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 487.559370000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'Terceiro'
          DataSet = frxDs00NFeIts
          DataSetName = 'frxDs00NFeIts'
          DisplayFormat.FormatStr = '#;-#;??????'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs00NFeIts."Terceiro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 529.134199999999900000
          Width = 461.102616060000000000
          Height = 15.118110240000000000
          DataField = 'NO_Terceiro'
          DataSet = frxDs00NFeIts
          DataSetName = 'frxDs00NFeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs00NFeIts."NO_Terceiro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 162.519790000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDs00NFeIts
          DataSetName = 'frxDs00NFeIts'
          DisplayFormat.FormatStr = '#,###,###,##0.0;-#,###,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs00NFeIts."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 419.527830000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'ValorT'
          DataSet = frxDs00NFeIts
          DataSetName = 'frxDs00NFeIts'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs00NFeIts."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 215.433210000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDs00NFeIts
          DataSetName = 'frxDs00NFeIts'
          DisplayFormat.FormatStr = '#,###,###,##0.000;-#,###,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs00NFeIts."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 351.496290000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDs00NFeIts
          DataSetName = 'frxDs00NFeIts'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs00NFeIts."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 283.464750000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2'
          DataSet = frxDs00NFeIts
          DataSetName = 'frxDs00NFeIts'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs00NFeIts."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 49.133890000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'SerNF'
          DataSet = frxDs00NFeIts
          DataSetName = 'frxDs00NFeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs00NFeIts."SerNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 105.826840000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'nNF'
          DataSet = frxDs00NFeIts
          DataSetName = 'frxDs00NFeIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs00NFeIts."nNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDs00NFes: TfrxDBDataset
    UserName = 'frxDs00NFes'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Empresa=Empresa'
      'SerNF=SerNF'
      'nNF=nNF'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT')
    DataSet = Qr00NFes
    BCDToCurrency = False
    Left = 604
    Top = 348
  end
  object frxDs00NFeIts: TfrxDBDataset
    UserName = 'frxDs00NFeIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DataHora=DataHora'
      'TipoFrete=TipoFrete'
      'Tabela=Tabela'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'SerNF=SerNF'
      'nNF=nNF'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'NO_Terceiro=NO_Terceiro'
      'ValorT=ValorT'
      'VSVMI_MovimCod=VSVMI_MovimCod'
      'Codigo=Codigo')
    DataSet = Qr00NFeIts
    BCDToCurrency = False
    Left = 704
    Top = 348
  end
  object frxWET_CURTI_168_01_1: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 42013.497466747690000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '(*'
      '  GH001.Visible := <VARF_AGRUP1>;'
      '  GF001.Visible := <VARF_AGRUP1>;'
      '  GH001.Condition := <VARF_GRUPO1>;'
      '  MeGrupo1Head.Memo.Text := <VARF_HEADR1>;            '
      '  MeGrupo1Foot.Memo.Text := <VARF_FOOTR1>;'
      ''
      '  GH002.Visible := <VARF_AGRUP2>;'
      '  GF002.Visible := <VARF_AGRUP2>;'
      '  GH002.Condition := <VARF_GRUPO2>;'
      '  MeGrupo2Head.Memo.Text := <VARF_HEADR2>;            '
      '  MeGrupo2Foot.Memo.Text := <VARF_FOOTR2>;'
      '*)                                            '
      'end.')
    OnGetValue = frxWET_CURTI_168_01_0GetValue
    Left = 837
    Top = 300
    Datasets = <
      item
        DataSet = frxDs01EnvRet
        DataSetName = 'frxDs01EnvRet'
      end
      item
        DataSet = frxDs01NFes
        DataSetName = 'frxDs01NFes'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 79.370120240000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        object Shape3: TfrxShapeView
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 778.583180000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'NF-es de Remessa e seus Retornos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 884.410020000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 3.779530000000000000
          Top = 41.574830000000000000
          Width = 616.063390000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Empresa: [VARF_EMPRESA]')
          ParentFont = False
        end
        object MeTitPecas: TfrxMemoView
          Left = 162.519790000000000000
          Top = 64.252010000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          Left = 419.527830000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor Total')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPesoKg: TfrxMemoView
          Left = 215.433210000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Top = 64.252010000000000000
          Width = 49.133841180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data / Hora')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 676.535870000000000000
          Top = 64.252010000000000000
          Width = 41.574729920000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Terceiro')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 351.496290000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 718.110700000000000000
          Top = 64.252010000000000000
          Width = 272.126116060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome terceiro')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 370.393940000000000000
          Top = 41.574830000000000000
          Width = 616.063390000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Fornecedor de MO: [VARF_FORNEC_MO]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 283.464750000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 49.133890000000000000
          Top = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 105.826840000000000000
          Top = 64.252010000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#176' NFe')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 487.559370000000000000
          Top = 64.252010000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDs00NFes
          DataSetName = 'frxDs00NFes'
          DisplayFormat.FormatStr = '#,###,###,###.##;-#,###,###,###.##; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 540.472790000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDs00NFes
          DataSetName = 'frxDs00NFes'
          DisplayFormat.FormatStr = '#,###,###,##0.000;-#,###,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo peso Kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 608.504330000000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataSet = frxDs00NFes
          DataSetName = 'frxDs00NFes'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo '#225'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 158.740260000000000000
        Width = 990.236860000000000000
        DataSet = frxDs01NFes
        DataSetName = 'frxDs01NFes'
        PrintChildIfInvisible = True
        PrintIfDetailEmpty = True
        RowCount = 0
        object MeValPecas: TfrxMemoView
          Left = 162.519790000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDs01NFes
          DataSetName = 'frxDs01NFes'
          DisplayFormat.FormatStr = '#,###,###,##0.0;-#,###,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01NFes."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          Left = 419.527830000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'ValorT'
          DataSet = frxDs01NFes
          DataSetName = 'frxDs01NFes'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01NFes."ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPesoKg: TfrxMemoView
          Left = 215.433210000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg'
          DataSet = frxDs01NFes
          DataSetName = 'frxDs01NFes'
          DisplayFormat.FormatStr = '#,###,###,##0.000;-#,###,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01NFes."PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 351.496290000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDs01NFes
          DataSetName = 'frxDs01NFes'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01NFes."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 283.464750000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2'
          DataSet = frxDs01NFes
          DataSetName = 'frxDs01NFes'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01NFes."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 49.133890000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'SerNF'
          DataSet = frxDs01NFes
          DataSetName = 'frxDs01NFes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01NFes."SerNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 105.826840000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDs01NFes
          DataSetName = 'frxDs01NFes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01NFes."nNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 487.559370000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDs01NFes
          DataSetName = 'frxDs01NFes'
          DisplayFormat.FormatStr = '#,###,###,##0.0;-#,###,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01NFes."SdoPeca"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 540.472790000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'SdoPeso'
          DataSet = frxDs01NFes
          DataSetName = 'frxDs01NFes'
          DisplayFormat.FormatStr = '#,###,###,##0.000;-#,###,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01NFes."SdoPeso"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 608.504330000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'SdoArM2'
          DataSet = frxDs01NFes
          DataSetName = 'frxDs01NFes'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01NFes."SdoArM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 676.535870000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'Terceiro'
          DataSet = frxDs01NFes
          DataSetName = 'frxDs01NFes'
          DisplayFormat.FormatStr = '#;-#;??????'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01NFes."Terceiro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 718.110700000000000000
          Width = 272.126116060000000000
          Height = 15.118110240000000000
          DataField = 'NO_TERCEIRO'
          DataSet = frxDs01NFes
          DataSetName = 'frxDs01NFes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs01NFes."NO_TERCEIRO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 317.480520000000000000
        Width = 990.236860000000000000
        object Memo93: TfrxMemoView
          Width = 672.756340000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 668.976810000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 272.126160000000000000
        Width = 990.236860000000000000
        object Me_FTT: TfrxMemoView
          Top = 3.779529999999965000
          Width = 162.519594720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo33: TfrxMemoView
          Left = 487.559370000000000000
          Top = 3.779530000000022000
          Width = 502.677460710000000000
          Height = 15.118110240000000000
          DataSet = FmVSImpMOEnvRet.frxDsVSMO
          DataSetName = 'frxDsVSMO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = clWhite
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 162.519790000000000000
          Top = 3.779530000000022000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.0;-#,###,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs01NFes."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 215.433210000000000000
          Top = 3.779530000000022000
          Width = 68.031505830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.000;-#,###,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs01NFes."PesoKg">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 283.464750000000000000
          Top = 3.779530000000022000
          Width = 68.031505830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs01NFes."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 351.496290000000000000
          Top = 3.779530000000022000
          Width = 68.031505830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs01NFes."AreaP2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 419.527830000000000000
          Top = 3.779530000000022000
          Width = 68.031505830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs01NFes."ValorT">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 487.559370000000000000
          Top = 3.779530000000022000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.0;-#,###,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs01NFes."SdoPeca">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 540.472790000000000000
          Top = 3.779530000000022000
          Width = 68.031505830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.000;-#,###,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs01NFes."SdoPeso">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 608.504330000000000000
          Top = 3.779530000000022000
          Width = 68.031505830000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs01NFes."SdoArM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object DD002: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 196.535560000000000000
        Width = 990.236860000000000000
        DataSet = frxDs01EnvRet
        DataSetName = 'frxDs01EnvRet'
        RowCount = 0
        object Memo3: TfrxMemoView
          Width = 49.133841180000000000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDs01EnvRet
          DataSetName = 'frxDs01EnvRet'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs01EnvRet."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 162.519790000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'NFRMP_Pecas'
          DataSet = frxDs01EnvRet
          DataSetName = 'frxDs01EnvRet'
          DisplayFormat.FormatStr = '#,###,###,##0.0;-#,###,###,##0.0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01EnvRet."NFRMP_Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 419.527830000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'NFRMP_ValorT'
          DataSet = frxDs01EnvRet
          DataSetName = 'frxDs01EnvRet'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01EnvRet."NFRMP_ValorT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 215.433210000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'NFRMP_PesoKg'
          DataSet = frxDs01EnvRet
          DataSetName = 'frxDs01EnvRet'
          DisplayFormat.FormatStr = '#,###,###,##0.000;-#,###,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01EnvRet."NFRMP_PesoKg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 351.496290000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'NFRMP_AreaP2'
          DataSet = frxDs01EnvRet
          DataSetName = 'frxDs01EnvRet'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01EnvRet."NFRMP_AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 283.464750000000000000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'NFRMP_AreaM2'
          DataSet = frxDs01EnvRet
          DataSetName = 'frxDs01EnvRet'
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01EnvRet."NFRMP_AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 49.133890000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'NFRMP_SerNF'
          DataSet = frxDs01EnvRet
          DataSetName = 'frxDs01EnvRet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01EnvRet."NFRMP_SerNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 105.826840000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'NFRMP_nNF'
          DataSet = frxDs01EnvRet
          DataSetName = 'frxDs01EnvRet'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs01EnvRet."NFRMP_nNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
end
