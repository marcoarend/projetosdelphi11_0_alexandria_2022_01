unit VSImpHistPall;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, mySQLDbTables,
  DmkGeral, UnDmkEnums, frxClass, frxDBSet, AppListas, UnAppEnums;

type
  TFmVSImpHistPall = class(TForm)
    QrMovHist: TmySQLQuery;
    QrPallets: TmySQLQuery;
    QrPalletsPallet: TLargeintField;
    QrPalletsGraGruX: TLargeintField;
    QrPalletsPecas: TFloatField;
    QrPalletsAreaM2: TFloatField;
    QrPalletsAreaP2: TFloatField;
    QrPalletsPesoKg: TFloatField;
    QrPalletsNO_PRD_TAM_COR: TWideStringField;
    QrPalletsNO_Pallet: TWideStringField;
    QrPalletsNO_TTW: TWideStringField;
    QrPalletsID_TTW: TLargeintField;
    QrPalletsValorT: TFloatField;
    QrPalletsSdoVrtPeca: TFloatField;
    QrPalletsSdoVrtPeso: TFloatField;
    QrPalletsSdoVrtArM2: TFloatField;
    frxWET_CURTI_130_01: TfrxReport;
    frxDsPallets: TfrxDBDataset;
    QrMulFrn_: TmySQLQuery;
    QrMulFrn_SiglaVS: TWideStringField;
    QrMulFrn_FrnCod: TLargeintField;
    QrMulFrn_Pecas: TFloatField;
    QrTotFrn_: TmySQLQuery;
    QrTotFrn_Pecas: TFloatField;
    QrMovHistNO_MovimID: TWideStringField;
    QrMovHistNO_MovimNiv: TWideStringField;
    QrMovHistMinDataHora: TDateTimeField;
    QrMovHistMaxDataHora: TDateTimeField;
    QrMovHistMovimID: TIntegerField;
    QrMovHistMovimNiv: TIntegerField;
    QrMovHistITENS: TLargeintField;
    QrMovHistPecas: TFloatField;
    QrMovHistAreaM2: TFloatField;
    QrMovHistPesoKg: TFloatField;
    frxDsMovHist: TfrxDBDataset;
    QrRevIts: TmySQLQuery;
    QrRevItsPecas: TFloatField;
    QrRevItsRevisor: TLargeintField;
    QrRevItsNO_Revisor: TWideStringField;
    procedure frxWET_CURTI_130_01GetValue(const VarName: string;
      var Value: Variant);
    procedure QrPalletsAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    //function  ObtemFornecedores(): String;
    function  ObtemClassificadores(): String;
  public
    { Public declarations }
    FEmpresa_Cod: Integer;
    FEmpresa_Txt: String;
    FPallets: array of Integer;
    //
    procedure ImprimeHistPall();
  end;

var
  FmVSImpHistPall: TFmVSImpHistPall;

implementation

uses Module, ModuleGeral, DmkDAC_PF, UnMyObjects, UnVS_PF, UnDmkProcFunc,
ModVS_CRC;

{$R *.dfm}

{ TFmVSImpHistPall }

procedure TFmVSImpHistPall.frxWET_CURTI_130_01GetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', FEmpresa_Txt, FEmpresa_Cod, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName = 'VARF_FORNECEDORES' then
    Value := DmModVS_CRC.ObtemFornecedores_Pallet(QrPalletsPallet.Value)
  else
  if VarName = 'VARF_CLASSIFICADORES' then
    Value := ObtemClassificadores()
end;

procedure TFmVSImpHistPall.ImprimeHistPall();
const
  TemIMEIMrt = 1;
var
  SQL_Flds, SQL_Left, SQL_Wher, SQL_Group,
  SQL_Pallets, ID_e_Niv: String;
begin
  SQL_Pallets := MyObjects.CordaDeArrayInt(FPallets);
  ID_e_Niv    := VS_PF.SQL_MovIDeNiv_Pos_Inn();
  SQL_Flds    := '';
  SQL_Left    := '';
  SQL_Wher    := 'WHERE vmi.Pallet IN (' + SQL_Pallets + ') ' + sLineBreak +
                 'AND ( ' + ID_e_Niv + ') ';
  SQL_Group   := 'GROUP BY Pallet';
  //
   UnDmkDAC_PF.AbreMySQLQuery0(QrPallets, Dmod.MyDB, [
  VS_PF.GeraSQL_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
  VS_PF.GeraSQL_Pall(SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
  'ORDER BY Pallet ',
  '']);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_130_01, [
  DModG.frxDsDono,
  frxDsPallets,
  frxDsMovHist
  ]);
  MyObjects.frxMostra(frxWET_CURTI_130_01, 'Histórico de Pallets');
end;

function TFmVSImpHistPall.ObtemClassificadores(): String;
  function SQL_Parcial_Mnt(Tab: TTabToWork): String;
  begin
    Result := Geral.ATS([
    'SELECT CAST(cia.Revisor AS SIGNED) Revisor, SUM(cia.Pecas) Pecas, ',
    'LEFT(IF(ent.Tipo=0, IF(ent.Fantasia<>"", ent.Fantasia, ent.RazaoSocial), ',
    '  IF(ent.Apelido<>"", ent.Apelido, ent.Nome)), 15) NO_Revisor ',
    'FROM ' + VS_PF.TabCacVS_Tab(tab) + ' cia ',
    'LEFT JOIN ' + VS_PF.TabMovVS_Tab(tab) + ' vmi ON vmi.Controle=cia.VMI_Dest ',
    'LEFT JOIN entidades ent ON ent.Codigo=cia.Revisor ',
    'WHERE cia.VSPallet=' + Geral.FF0(QrPalletsPallet.Value),
    'AND vmi.MovimID=' + Geral.FF0(QrMovHistMovimID.Value),
    'AND vmi.MovimNiv=' + Geral.FF0(QrMovHistMovimNiv.Value),
    'GROUP BY Revisor ',
    Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
    '']);
  end;
  //
  function SQL_Parcial_Dsm(Tab: TTabToWork): String;
  begin
    Result := Geral.ATS([
    'SELECT CAST(cia.Revisor AS SIGNED) Revisor, SUM(cia.Pecas) Pecas, ',
    'LEFT(IF(ent.Tipo=0, IF(ent.Fantasia<>"", ent.Fantasia, ent.RazaoSocial), ',
    '  IF(ent.Apelido<>"", ent.Apelido, ent.Nome)), 15) NO_Revisor ',
    'FROM vscacitsa cia',
    'LEFT JOIN vsparclcaba pra ON pra.CacCod=cia.CacCod',
    'LEFT JOIN entidades ent ON ent.Codigo=cia.Revisor ',
    'WHERE pra.VSPallet=' + Geral.FF0(QrPalletsPallet.Value),
    'GROUP BY cia.Revisor',
    Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
    '']);
  end;
  //
var
  Fator, PerPecas: Double;
  Apelido, Percentual: String;
begin
  Result := '';
  //
  if   ((TEstqMovimID(QrMovHistMovimID.Value) in ([emidClassArtXXUni, emidClassArtXXMul]))
       and (TEstqMovimNiv(QrMovHistMovimNiv.Value) = eminDestClass))
  or   ((TEstqMovimID(QrMovHistMovimID.Value) in ([emidReclasXXUni, emidReclasXXMul]))
       and (TEstqMovimNiv(QrMovHistMovimNiv.Value) = eminDestClass))
  then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrRevIts, Dmod.MyDB, [
    SQL_Parcial_Mnt(ttwB),
    SQL_Parcial_Mnt(ttwA),
    '',
    'ORDER BY Pecas DESC ',
    '']);
  end else
  if   ((TEstqMovimID(QrMovHistMovimID.Value) in ([emidReclasXXUni, emidReclasXXMul]))
       and (TEstqMovimNiv(QrMovHistMovimNiv.Value) = eminSorcClass))
  then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrRevIts, Dmod.MyDB, [
    SQL_Parcial_Dsm(ttwB),
    SQL_Parcial_Dsm(ttwA),
    '',
    'ORDER BY Pecas DESC ',
    '']);
    //Geral.MB_SQL(Self, QrRevIts);
  end else
    Exit;
////////////////////////////////////////////////////////////////////////////////
  Fator := 0;
  QrRevIts.First;
  while not(QrRevIts.Eof) do
  begin
    Fator := Fator + QrRevItsPecas.Value;
    //
    QrRevIts.Next;
  end;
  if Fator <> 0 then
    Fator := Fator / 100
  else
    Fator := High(Integer);
  //
  QrRevIts.First;
  while not(QrRevIts.Eof) do
  begin
    Apelido := QrRevItsNO_Revisor.Value;
    if Fator <> 0 then
    begin
      PerPecas   := QrRevItsPecas.Value / Fator;
      Percentual := Geral.FI64(Round(PerPecas));
    end else
    begin
      PerPecas   := 0;
      Percentual := '??';
    end;
    //
    if (Apelido <> '') or (PerPecas <> 0) then
      Result := Result + Apelido + ' ' + Percentual + '% ';
    //
    QrRevIts.Next;
  end;
end;

(*
function TFmVSImpHistPall.ObtemFornecedores(): String;
  function SQL_Parcial_Its(Tab: TTabToWork): String;
  begin
    Result := Geral.ATS([
    'SELECT IF((vem.SiglaVS="") OR (vem.SiglaVS IS NULL),   ',
    '  CONCAT("FRN-", mfi.Fornece), vem.SiglaVS) SiglaVS,  ',
    'CAST(IF(vmi.VSMulFrnCab<>0, mfi.Fornece, vmi.Terceiro) AS SIGNED) FrnCod,   ',
    'CAST(SUM(IF(vmi.VSMulFrnCab<>0, mfi.Pecas, vmi.Pecas)) AS DECIMAL (15,3)) Pecas  ',
    'FROM ' + VS_PF.TabMovVS_Tab(tab) + ' vmi ',
    'LEFT JOIN vsmulfrnits mfi ON vmi.VSMulFrnCab=mfi.Codigo  ',
    'LEFT JOIN vsentimp vem ON vem.Codigo=IF(vmi.VSMulFrnCab<>0, mfi.Fornece, vmi.Terceiro)  ',
    'WHERE vmi.Pallet=' + Geral.FF0(QrPalletsPallet.Value),
    'AND ( ' + VS_PF.SQL_MovIDeNiv_Pos_Inn() + ') ',
    'GROUP BY FrnCod  ',
    Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
    '']);
  end;
  //
  function SQL_Parcial_Sum(Tab: TTabToWork): String;
  begin
    Result := Geral.ATS([
    'SELECT ',
    'CAST(SUM(IF(vmi.VSMulFrnCab<>0, mfi.Pecas, vmi.Pecas)) AS DECIMAL (15,3)) Pecas  ',
    'FROM ' + VS_PF.TabMovVS_Tab(tab) + ' vmi ',
    'LEFT JOIN vsmulfrnits mfi ON vmi.VSMulFrnCab=mfi.Codigo  ',
    'WHERE vmi.Pallet=' + Geral.FF0(QrPalletsPallet.Value),
    'AND ( ' + VS_PF.SQL_MovIDeNiv_Pos_Inn() + ') ',
    Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
    '']);
  end;
  //
var
  Fator, PerPecas: Double;
  SiglaVS, Percentual: String;
begin
  Result := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrTotFrn, Dmod.MyDB, [
  SQL_Parcial_Sum(ttwB),
  SQL_Parcial_Sum(ttwA),
  '']);
  Fator := 0;
  QrTotFrn.First;
  while not QrTotFrn.Eof do
  begin
    Fator := Fator + QrTotFrnPecas.Value;
    //
    QrTotFrn.Next;
  end;
  if Fator > 0 then
    Fator := Fator / 100
  else
    Fator := High(Integer);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrMulFrn, Dmod.MyDB, [
  SQL_Parcial_Its(ttwB),
  SQL_Parcial_Its(ttwA),
  '',
  'ORDER BY Pecas DESC ',
  '']);
  QrMulFrn.First;
  while not QrMulFrn.Eof do
  begin
    SiglaVS := QrMulFrnSiglaVS.Value;
    if QrTotFrnPecas.Value <> 0 then
    begin
      PerPecas   := QrMulFrnPecas.Value / Fator;
      Percentual := Geral.FI64(Round(PerPecas));
    end else
    begin
      PerPecas   := 0;
      Percentual := '??';
    end;
    //
    if (SiglaVS <> '') or (PerPecas <> 0) then
    begin
      if Trim(SiglaVS) = '' then
        SiglaVS := '???';
      Result := Result + SiglaVS + ' ' + Percentual + '% ';
    end;
    //
    QrMulFrn.Next;
  end;
end;
*)

procedure TFmVSImpHistPall.QrPalletsAfterScroll(DataSet: TDataSet);
var
  ATT_MovimID, ATT_MovimNiv: String;
  //
  function SQL_Parcial(Tab: TTabToWork): String;
  begin
    Result := Geral.ATS([
    'SELECT ',
    ATT_MovimID,
    ATT_MovimNiv,
    'MIN(DataHora) MinDataHora, ',
    'MAX(DataHora) MaxDataHora, MovimID, ',
    'MovimNiv, COUNT(Controle) ITENS, ',
    'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2, ',
    'SUM(PesoKg) PesoKg ',
    'FROM ' + VS_PF.TabMovVS_Tab(tab) + ' vmi ',
    'WHERE Pallet=' + Geral.FF0(QrPalletsPallet.Value),
    'GROUP BY MovimID, MovimNiv ',
     Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
    '']);
  end;
  //
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrMovHist, Dmod.MyDB, [
  SQL_Parcial(ttwB),
  SQL_Parcial(ttwA),
  '',
  'ORDER BY MinDataHora, MaxDataHora ',
  '']);
end;

end.
