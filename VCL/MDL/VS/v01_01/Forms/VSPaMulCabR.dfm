object FmVSPaMulCabR: TFmVSPaMulCabR
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-060 :: Classifica'#231#227'o de Artigo de Ribeira - M'#250'ltiplo'
  ClientHeight = 661
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 140
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label10: TLabel
        Left = 428
        Top = 16
        Width = 58
        Height = 13
        Caption = 'Data / hora:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 932
        Top = 16
        Width = 56
        Height = 13
        Caption = 'ID Estoque:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label16: TLabel
        Left = 580
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object Label17: TLabel
        Left = 668
        Top = 16
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
      end
      object Label18: TLabel
        Left = 756
        Top = 16
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
      end
      object Label19: TLabel
        Left = 844
        Top = 16
        Width = 27
        Height = 13
        Caption = 'Peso:'
      end
      object Label12: TLabel
        Left = 68
        Top = 16
        Width = 58
        Height = 13
        Caption = 'ID Classific.:'
        FocusControl = DBEdit22
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSPaMulCabR
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 140
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVSPaMulCabR
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 196
        Top = 32
        Width = 229
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsVSPaMulCabR
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 428
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DataHora'
        DataSource = DsVSPaMulCabR
        TabOrder = 3
      end
      object DBEdit6: TDBEdit
        Left = 932
        Top = 32
        Width = 64
        Height = 21
        DataField = 'MovimCod'
        DataSource = DsVSPaMulCabR
        TabOrder = 4
      end
      object DBEdit11: TDBEdit
        Left = 580
        Top = 32
        Width = 84
        Height = 21
        DataField = 'Pecas'
        DataSource = DsVSPaMulCabR
        TabOrder = 5
      end
      object DBEdit12: TDBEdit
        Left = 844
        Top = 32
        Width = 84
        Height = 21
        DataField = 'PesoKg'
        DataSource = DsVSPaMulCabR
        TabOrder = 6
      end
      object DBEdit13: TDBEdit
        Left = 668
        Top = 32
        Width = 84
        Height = 21
        DataField = 'AreaM2'
        DataSource = DsVSPaMulCabR
        TabOrder = 7
      end
      object DBEdit14: TDBEdit
        Left = 756
        Top = 32
        Width = 84
        Height = 21
        DataField = 'AreaP2'
        DataSource = DsVSPaMulCabR
        TabOrder = 8
      end
      object DBEdit22: TDBEdit
        Left = 68
        Top = 32
        Width = 69
        Height = 21
        DataField = 'CacCod'
        DataSource = DsVSPaMulCabR
        TabOrder = 9
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 501
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtClassesGeradas: TBitBtn
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Classes Geradas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtClassesGeradasClick
        end
      end
    end
    object GBIts: TGroupBox
      Left = 0
      Top = 209
      Width = 1008
      Height = 105
      Align = alTop
      Caption = ' Itens da classifica'#231#227'o: '
      TabOrder = 2
      object DGDados: TdmkDBGridZTO
        Left = 2
        Top = 15
        Width = 1004
        Height = 88
        Align = alClient
        DataSource = DsVSPaMulItsR
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'NO_TTW'
            Title.Caption = 'Tabela'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IxxMovIX'
            Title.Caption = 'IXX'
            Width = 20
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IxxFolha'
            Title.Caption = 'Folha'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IxxLinha'
            Title.Caption = 'Lin'
            Width = 22
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'IME-I'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'StqCenLoc'
            Title.Caption = 'Local'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ReqMovEstq'
            Title.Caption = 'N'#176' RME'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pallet'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Artigo'
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaP2'
            Title.Caption = #193'rea ft'#178
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesoKg'
            Title.Caption = 'Peso kg'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorT'
            Title.Caption = '$ total'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataHora'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CusArM2'
            Title.Caption = '$ m'#178
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_FORNECE'
            Title.Caption = 'Fornecedor / Cliente'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PALLET'
            Title.Caption = 'Obs. Pallet'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Observ'
            Title.Caption = 'Observa'#231#245'es'
            Width = 300
            Visible = True
          end>
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 109
      Width = 1008
      Height = 100
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 3
      object Label3: TLabel
        Left = 16
        Top = 16
        Width = 28
        Height = 13
        Caption = 'IME-I:'
      end
      object Label14: TLabel
        Left = 100
        Top = 16
        Width = 76
        Height = 13
        Caption = 'Artigo de ribeira:'
      end
      object Label15: TLabel
        Left = 576
        Top = 16
        Width = 62
        Height = 13
        Caption = '$/kg M.obra:'
      end
      object Label4: TLabel
        Left = 672
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object Label5: TLabel
        Left = 748
        Top = 16
        Width = 67
        Height = 13
        Caption = 'Peso (origem):'
        Enabled = False
      end
      object Label9: TLabel
        Left = 200
        Top = 56
        Width = 164
        Height = 13
        Caption = 'Observa'#231#227'o sobre o artigo gerado:'
      end
      object Label20: TLabel
        Left = 836
        Top = 16
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        Enabled = False
      end
      object Label21: TLabel
        Left = 916
        Top = 16
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Enabled = False
      end
      object Label26: TLabel
        Left = 84
        Top = 56
        Width = 91
        Height = 13
        Caption = 'S'#233'rie / Ficha RMP:'
        FocusControl = DBEdit20
      end
      object Label36: TLabel
        Left = 932
        Top = 56
        Width = 60
        Height = 13
        Caption = 'Nota MPAG:'
        FocusControl = DBEdit27
      end
      object Label11: TLabel
        Left = 776
        Top = 56
        Width = 55
        Height = 13
        Caption = 'Sdo Pe'#231'as:'
        FocusControl = DBEdit19
      end
      object Label22: TLabel
        Left = 852
        Top = 56
        Width = 60
        Height = 13
        Caption = 'Sdo '#225'rea m'#178':'
        FocusControl = DBEdit23
      end
      object Label23: TLabel
        Left = 16
        Top = 56
        Width = 29
        Height = 13
        Caption = 'Pallet:'
        FocusControl = DBEdit24
      end
      object Label25: TLabel
        Left = 704
        Top = 56
        Width = 42
        Height = 13
        Caption = 'N'#176' RME:'
        FocusControl = DBEdit41
      end
      object DBEdit5: TDBEdit
        Left = 16
        Top = 32
        Width = 81
        Height = 21
        DataField = 'Controle'
        DataSource = DsVSMovSrc
        TabOrder = 0
      end
      object DBEdit4: TDBEdit
        Left = 100
        Top = 32
        Width = 57
        Height = 21
        DataField = 'GraGruX'
        DataSource = DsVSMovSrc
        TabOrder = 1
      end
      object DBEdit7: TDBEdit
        Left = 156
        Top = 32
        Width = 417
        Height = 21
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsVSMovSrc
        TabOrder = 2
      end
      object DBEdit8: TDBEdit
        Left = 576
        Top = 32
        Width = 94
        Height = 21
        DataField = 'CustoMOKg'
        DataSource = DsVSMovSrc
        TabOrder = 3
      end
      object DBEdit9: TDBEdit
        Left = 672
        Top = 32
        Width = 72
        Height = 21
        DataField = 'Pecas'
        DataSource = DsVSMovSrc
        TabOrder = 4
      end
      object DBEdit10: TDBEdit
        Left = 748
        Top = 32
        Width = 84
        Height = 21
        DataField = 'QtdAntPeso'
        DataSource = DsVSMovSrc
        TabOrder = 5
      end
      object DBEdit15: TDBEdit
        Left = 200
        Top = 72
        Width = 501
        Height = 21
        DataField = 'Observ'
        DataSource = DsVSMovSrc
        TabOrder = 6
      end
      object DBEdit16: TDBEdit
        Left = 836
        Top = 32
        Width = 76
        Height = 21
        DataField = 'AreaM2'
        DataSource = DsVSMovSrc
        TabOrder = 7
      end
      object DBEdit17: TDBEdit
        Left = 916
        Top = 32
        Width = 84
        Height = 21
        DataField = 'AreaP2'
        DataSource = DsVSMovSrc
        TabOrder = 8
      end
      object DBEdit20: TDBEdit
        Left = 84
        Top = 72
        Width = 29
        Height = 21
        DataField = 'SerieFch'
        DataSource = DsVSMovSrc
        TabOrder = 9
      end
      object DBEdit27: TDBEdit
        Left = 932
        Top = 72
        Width = 68
        Height = 21
        DataField = 'NotaMPAG'
        DataSource = DsVSMovSrc
        TabOrder = 10
      end
      object DBEdit21: TDBEdit
        Left = 112
        Top = 72
        Width = 85
        Height = 21
        DataField = 'Ficha'
        DataSource = DsVSMovSrc
        TabOrder = 11
      end
      object DBEdit19: TDBEdit
        Left = 776
        Top = 72
        Width = 72
        Height = 21
        DataField = 'SdoVrtPeca'
        DataSource = DsVSMovSrc
        TabOrder = 12
      end
      object DBEdit23: TDBEdit
        Left = 852
        Top = 72
        Width = 76
        Height = 21
        DataField = 'SdoVrtArM2'
        DataSource = DsVSMovSrc
        TabOrder = 13
      end
      object DBEdit24: TDBEdit
        Left = 16
        Top = 72
        Width = 64
        Height = 21
        DataField = 'Pallet'
        DataSource = DsVSMovSrc
        TabOrder = 14
      end
      object DBEdit41: TDBEdit
        Left = 704
        Top = 72
        Width = 68
        Height = 21
        DataField = 'ReqMovEstq'
        DataSource = DsVSMovSrc
        TabOrder = 15
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 404
      Width = 1008
      Height = 97
      Align = alBottom
      Caption = ' Itens residuais: '
      TabOrder = 4
      object dmkDBGridZTO1: TdmkDBGridZTO
        Left = 2
        Top = 15
        Width = 1004
        Height = 80
        Align = alClient
        DataSource = DsResidual
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'NO_TTW'
            Title.Caption = 'Tabela'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'IME-I'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pallet'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GraGruX'
            Title.Caption = 'Reduzido'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRD_TAM_COR'
            Title.Caption = 'Artigo'
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaP2'
            Title.Caption = #193'rea ft'#178
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PesoKg'
            Title.Caption = 'Peso kg'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_FORNECE'
            Title.Caption = 'Fornecedor / Cliente'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PALLET'
            Title.Caption = 'Obs. Pallet'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Observ'
            Title.Caption = 'Observa'#231#245'es'
            Width = 300
            Visible = True
          end>
      end
    end
    object GroupBox3: TGroupBox
      Left = 0
      Top = 61
      Width = 1008
      Height = 48
      Align = alTop
      Caption = ' Pr'#233'-reclasse: '
      TabOrder = 5
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 31
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label28: TLabel
          Left = 8
          Top = 8
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit26
        end
        object Label29: TLabel
          Left = 128
          Top = 8
          Width = 56
          Height = 13
          Caption = 'ID Estoque:'
          FocusControl = DBEdit28
        end
        object DBEdit26: TDBEdit
          Left = 52
          Top = 4
          Width = 62
          Height = 21
          DataField = 'Codigo'
          DataSource = DsVSMovSrc
          TabOrder = 0
        end
        object DBEdit28: TDBEdit
          Left = 188
          Top = 4
          Width = 62
          Height = 21
          DataField = 'MovimCod'
          DataSource = DsVSMovSrc
          TabOrder = 1
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label52: TLabel
        Left = 420
        Top = 16
        Width = 58
        Height = 13
        Caption = 'Data / hora:'
        Color = clBtnFace
        ParentColor = False
      end
      object LaPecas: TLabel
        Left = 588
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object LaAreaM2: TLabel
        Left = 676
        Top = 16
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        Enabled = False
      end
      object LaAreaP2: TLabel
        Left = 764
        Top = 16
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Enabled = False
      end
      object LaPeso: TLabel
        Left = 852
        Top = 16
        Width = 27
        Height = 13
        Caption = 'Peso:'
        Enabled = False
      end
      object Label6: TLabel
        Left = 940
        Top = 16
        Width = 56
        Height = 13
        Caption = 'ID Estoque:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 68
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDataHora: TdmkEditDateTimePicker
        Left = 420
        Top = 32
        Width = 108
        Height = 21
        Date = 0.639644131944805800
        Time = 0.639644131944805800
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DataHora'
        UpdCampo = 'DataHora'
        UpdType = utYes
        DatePurpose = dmkdpSPED_EFD_MIN
      end
      object EdDataHora: TdmkEdit
        Left = 528
        Top = 32
        Width = 54
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        QryCampo = 'DataHora'
        UpdCampo = 'DataHora'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPecas: TdmkEdit
        Left = 588
        Top = 32
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdAreaM2: TdmkEditCalc
        Left = 676
        Top = 32
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaM2'
        UpdCampo = 'AreaM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaP2
        CalcType = ctM2toP2
        CalcFrac = cfQuarto
      end
      object EdAreaP2: TdmkEditCalc
        Left = 764
        Top = 32
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaP2'
        UpdCampo = 'AreaP2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaM2
        CalcType = ctP2toM2
        CalcFrac = cfCento
      end
      object EdPesoKg: TdmkEdit
        Left = 852
        Top = 32
        Width = 84
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PesoKg'
        UpdCampo = 'PesoKg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 940
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 68
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 124
        Top = 32
        Width = 293
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 502
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 2
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 16
        Width = 120
        Height = 41
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PnPartida: TPanel
      Left = 0
      Top = 61
      Width = 1008
      Height = 44
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Panel13: TPanel
        Left = 0
        Top = 0
        Width = 613
        Height = 44
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object SbPallet: TSpeedButton
          Left = 584
          Top = 20
          Width = 21
          Height = 21
          Caption = '>'
          Enabled = False
        end
        object LaVSRibCad: TLabel
          Left = 8
          Top = 4
          Width = 29
          Height = 13
          Caption = 'Pallet:'
        end
        object CBPallet: TdmkDBLookupComboBox
          Left = 64
          Top = 20
          Width = 517
          Height = 21
          KeyField = 'Pallet'
          ListField = 'NO_PRD_TAM_COR'
          ListSource = DsPallets
          TabOrder = 1
          dmkEditCB = EdPallet
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdPallet: TdmkEditCB
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPallet
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CkReclasse: TCheckBox
          Left = 44
          Top = 2
          Width = 513
          Height = 17
          TabStop = False
          Caption = 'Mostrar tamb'#233'm pallets j'#225' em reclassifica'#231#227'o.'
          TabOrder = 2
          OnClick = CkReclasseClick
        end
      end
      object Panel17: TPanel
        Left = 613
        Top = 0
        Width = 395
        Height = 44
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label30: TLabel
          Left = 140
          Top = 4
          Width = 28
          Height = 13
          Caption = 'IME-I:'
          Enabled = False
        end
        object Label31: TLabel
          Left = 224
          Top = 4
          Width = 53
          Height = 13
          Caption = 'ID Ger.Art.:'
          Enabled = False
        end
        object Label32: TLabel
          Left = 308
          Top = 4
          Width = 18
          Height = 13
          Caption = 'OC:'
          Enabled = False
        end
        object Label57: TLabel
          Left = 4
          Top = 4
          Width = 116
          Height = 13
          Caption = 'S'#233'rie e N'#176' NFe remessa:'
          Color = clBtnFace
          ParentColor = False
        end
        object EdCacCod: TdmkEdit
          Left = 304
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdVMI_Sorc: TdmkEdit
          Left = 140
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'VMI_Sorc'
          UpdCampo = 'VMI_Sorc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdVSGerArt: TdmkEdit
          Left = 224
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'VSGerArt'
          UpdCampo = 'VSGerArt'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdSerieRem: TdmkEdit
          Left = 4
          Top = 20
          Width = 29
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNFeRem: TdmkEdit
          Left = 36
          Top = 20
          Width = 101
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
    object Panel7: TPanel
      Left = 0
      Top = 105
      Width = 1008
      Height = 156
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object Label49: TLabel
        Left = 8
        Top = 3
        Width = 60
        Height = 13
        Caption = 'Localiza'#231#227'o:'
      end
      object SbStqCenLoc: TSpeedButton
        Left = 485
        Top = 19
        Width = 21
        Height = 21
        Caption = '...'
      end
      object EdStqCenLoc: TdmkEditCB
        Left = 8
        Top = 19
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdCampo = 'StqCenLoc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStqCenLoc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnFormActivate
      end
      object CBStqCenLoc: TdmkDBLookupComboBox
        Left = 63
        Top = 19
        Width = 422
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_LOC_CEN'
        ListSource = DsStqCenLoc
        TabOrder = 1
        dmkEditCB = EdStqCenLoc
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 528
        Height = 32
        Caption = 'Classifica'#231#227'o de Artigo de Ribeira - M'#250'ltiplo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 528
        Height = 32
        Caption = 'Classifica'#231#227'o de Artigo de Ribeira - M'#250'ltiplo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 528
        Height = 32
        Caption = 'Classifica'#231#227'o de Artigo de Ribeira - M'#250'ltiplo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 84
    Top = 8
  end
  object QrVSPaMulCabR: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSPaMulCabRBeforeOpen
    AfterOpen = QrVSPaMulCabRAfterOpen
    BeforeClose = QrVSPaMulCabRBeforeClose
    AfterScroll = QrVSPaMulCabRAfterScroll
    Left = 28
    Top = 325
    object QrVSPaMulCabRCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaMulCabRMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSPaMulCabREmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPaMulCabRDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSPaMulCabRPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrVSPaMulCabRPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrVSPaMulCabRAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSPaMulCabRAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSPaMulCabRLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPaMulCabRDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPaMulCabRDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPaMulCabRUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPaMulCabRUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPaMulCabRAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPaMulCabRAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPaMulCabRNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPaMulCabRVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSPaMulCabRCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSPaMulCabRVSGerArt: TIntegerField
      FieldName = 'VSGerArt'
    end
    object QrVSPaMulCabRTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSPaMulCabRSerieRem: TSmallintField
      FieldName = 'SerieRem'
    end
    object QrVSPaMulCabRNFeRem: TIntegerField
      FieldName = 'NFeRem'
    end
  end
  object DsVSPaMulCabR: TDataSource
    DataSet = QrVSPaMulCabR
    Left = 32
    Top = 369
  end
  object QrVSPaMulItsR: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY wmi.Controle')
    Left = 124
    Top = 325
    object QrVSPaMulItsRCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPaMulItsRControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPaMulItsRMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPaMulItsRMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPaMulItsRMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPaMulItsREmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPaMulItsRTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPaMulItsRCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPaMulItsRMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPaMulItsRDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPaMulItsRPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPaMulItsRGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPaMulItsRPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPaMulItsRPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPaMulItsRAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsRAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsRValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsRSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPaMulItsRSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPaMulItsRSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPaMulItsRSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPaMulItsRSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPaMulItsRSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPaMulItsRSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsRObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPaMulItsRSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPaMulItsRFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPaMulItsRMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPaMulItsRFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPaMulItsRCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsRCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsRCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsRValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsRDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPaMulItsRDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPaMulItsRDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPaMulItsRDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPaMulItsRQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPaMulItsRQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPaMulItsRQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsRQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsRQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPaMulItsRQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPaMulItsRQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsRQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPaMulItsRNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPaMulItsRNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPaMulItsRNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPaMulItsRNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPaMulItsRID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPaMulItsRNO_FORNECE: TWideStringField
      DisplayWidth = 255
      FieldName = 'NO_FORNECE'
      Size = 255
    end
    object QrVSPaMulItsRNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSPaMulItsRReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSPaMulItsRStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSPaMulItsRCusArM2: TFloatField
      FieldName = 'CusArM2'
      DisplayFormat = '#,##0.00'
    end
    object QrVSPaMulItsRDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVSPaMulItsRIxxMovIX: TLargeintField
      FieldName = 'IxxMovIX'
    end
    object QrVSPaMulItsRIxxFolha: TLargeintField
      FieldName = 'IxxFolha'
    end
    object QrVSPaMulItsRIxxLinha: TLargeintField
      FieldName = 'IxxLinha'
    end
  end
  object DsVSPaMulItsR: TDataSource
    DataSet = QrVSPaMulItsR
    Left = 124
    Top = 369
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 652
    Top = 568
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Baixaresidual1: TMenuItem
      Caption = 'Baixa residual'
      OnClick = Baixaresidual1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 536
    Top = 568
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera &data'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabAltera2: TMenuItem
      Caption = 'Altera &tudo'
      Enabled = False
      OnClick = CabAltera2Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object CorrigeFornecedor1: TMenuItem
      Caption = '&Corrige Fornecedor'
      OnClick = CorrigeFornecedor1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 12
    Top = 8
    object ClassesGeradas1: TMenuItem
      Caption = 'Classes &Geradas'
      OnClick = ClassesGeradas1Click
    end
    object Fichas1: TMenuItem
      Caption = '&Fichas de Pallets afetados'
      object FichasCOMnomedoPallet1: TMenuItem
        Caption = 'Fichas &COM nome do Pallet'
        OnClick = FichasCOMnomedoPallet1Click
      end
      object FichasSEMnomedoPallet1: TMenuItem
        Caption = 'Fichas &SEM nome do Pallet'
        OnClick = FichasSEMnomedoPallet1Click
      end
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Estoque1: TMenuItem
      Caption = '&Estoque (mostrar'#225' outra janela)'
      OnClick = Estoque1Click
    end
  end
  object frxDsVSPaMulIts: TfrxDBDataset
    UserName = 'frxDsVSPaMulIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'MovimNiv=MovimNiv'
      'MovimTwn=MovimTwn'
      'Empresa=Empresa'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'MovimID=MovimID'
      'DataHora=DataHora'
      'Pallet=Pallet'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'ValorT=ValorT'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'SrcGGX=SrcGGX'
      'SdoVrtPeca=SdoVrtPeca'
      'SdoVrtPeso=SdoVrtPeso'
      'SdoVrtArM2=SdoVrtArM2'
      'Observ=Observ'
      'SerieFch=SerieFch'
      'Ficha=Ficha'
      'Misturou=Misturou'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOM2=CustoMOM2'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'DstGGX=DstGGX'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'NotaMPAG=NotaMPAG'
      'NO_PALLET=NO_PALLET'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_TTW=NO_TTW'
      'ID_TTW=ID_TTW'
      'NO_FORNECE=NO_FORNECE'
      'NO_SerieFch=NO_SerieFch'
      'ReqMovEstq=ReqMovEstq')
    DataSet = QrVSPaMulItsR
    BCDToCurrency = False
    DataSetOptions = []
    Left = 120
    Top = 416
  end
  object frxDsVSPaMulCab: TfrxDBDataset
    UserName = 'frxDsVSPaMulCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'DataHora=DataHora'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_EMPRESA=NO_EMPRESA')
    DataSet = QrVSPaMulCabR
    BCDToCurrency = False
    DataSetOptions = []
    Left = 28
    Top = 420
  end
  object QrVSMovSrc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cadastro_com_itens_its'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 120
    Top = 465
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVSMovSrcCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSMovSrcControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSMovSrcMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSMovSrcMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSMovSrcMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSMovSrcEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSMovSrcTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSMovSrcCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSMovSrcMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSMovSrcDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSMovSrcPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSMovSrcGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSMovSrcPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovSrcPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovSrcAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSMovSrcSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSMovSrcSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSMovSrcSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSMovSrcSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovSrcSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovSrcSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSMovSrcSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSMovSrcFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSMovSrcMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSMovSrcFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSMovSrcCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSMovSrcDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSMovSrcDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSMovSrcDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSMovSrcQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovSrcQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovSrcQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovSrcQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSMovSrcQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSMovSrcNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
      DisplayFormat = '0.000'
    end
    object QrVSMovSrcStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSMovSrcReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovSrcGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrVSMovSrcNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrVSMovSrcNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 255
    end
    object QrVSMovSrcNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 255
    end
    object QrVSMovSrcNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSMovSrcNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovSrcNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSMovSrcNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSMovSrcNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSMovSrcID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSMovSrcNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrVSMovSrcClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSMovSrcNFeSer: TLargeintField
      FieldName = 'NFeSer'
    end
    object QrVSMovSrcNFeNum: TLargeintField
      FieldName = 'NFeNum'
    end
    object QrVSMovSrcVSMulNFeCab: TLargeintField
      FieldName = 'VSMulNFeCab'
    end
  end
  object DsVSMovSrc: TDataSource
    DataSet = QrVSMovSrc
    Left = 120
    Top = 513
  end
  object QrSrc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 432
    Top = 416
    object QrSrcControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSrcSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
  end
  object QrPallets: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.Empresa, wmi.MovimID, wmi.GraGruX, '
      'wmi.Pallet, wmi.Codigo VMI_Codi, wmi.Controle VMI_Ctrl, '
      'wmi.MovimNiv VMI_MovimNiv, wmi.Terceiro, wmi.Ficha, '
      'MAX(DataHora) DataHora, SUM(wmi.Pecas) Pecas,   '
      'IF(MIN(AreaM2) > 0.01, 0, SUM(wmi.AreaM2)) AreaM2,   '
      'IF(MIN(AreaP2) > 0.01, 0, SUM(wmi.AreaP2)) AreaP2,   '
      'IF(MIN(PesoKg) > 0.001, 0, SUM(wmi.PesoKg)) PesoKg,   '
      'CONCAT(gg1.Nome,    '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),    '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))    '
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,    '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,   '
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE   '
      'FROM vsmovits wmi    '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX    '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC    '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad    '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI    '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1    '
      'LEFT JOIN vspallet   vsp ON vsp.Codigo=wmi.Pallet    '
      'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa  '
      'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro   '
      'WHERE wmi.Pecas > 0 '
      'AND wmi.Empresa=-11  '
      'AND wmi.Pallet > 0 '
      'GROUP BY wmi.Pallet, wmi.GraGruX, wmi.Empresa '
      'ORDER BY NO_PRD_TAM_COR')
    Left = 208
    Top = 45
    object QrPalletsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPalletsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrPalletsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrPalletsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrPalletsVMI_Codi: TIntegerField
      FieldName = 'VMI_Codi'
    end
    object QrPalletsVMI_Ctrl: TIntegerField
      FieldName = 'VMI_Ctrl'
    end
    object QrPalletsVMI_MovimNiv: TIntegerField
      FieldName = 'VMI_MovimNiv'
    end
    object QrPalletsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrPalletsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrPalletsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrPalletsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrPalletsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrPalletsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrPalletsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrPalletsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrPalletsNO_Pallet: TWideStringField
      FieldName = 'NO_Pallet'
      Size = 60
    end
    object QrPalletsNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrPalletsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrPalletsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrPalletsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
  end
  object DsPallets: TDataSource
    DataSet = QrPallets
    Left = 208
    Top = 93
  end
  object QrVSMovIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, '
      'CONCAT(gg1.Nome,  '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),  '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, ggx.GraGruY'
      'FROM vsmovits wmi  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX  '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet  '
      'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa  '
      'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro '
      'WHERE wmi.Pecas > 0 '
      'AND wmi.Empresa=-11'
      'AND wmi.Pallet > 0 '
      'AND wmi.Pallet=505'
      'AND wmi.SdoVrtPeca > 0 '
      'AND MovimID IN (0,7,8,11,13,14) '
      'ORDER BY Controle ')
    Left = 296
    Top = 45
    object QrVSMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSMovItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSMovItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSMovItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrVSMovItsSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSMovItsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSMovItsMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVSMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVSMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMovItsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrVSMovItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSMovItsGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
  end
  object DsVSMovIts: TDataSource
    DataSet = QrVSMovIts
    Left = 296
    Top = 93
  end
  object QrResidual: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'ORDER BY wmi.Controle')
    Left = 228
    Top = 325
    object QrResidualCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrResidualControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrResidualMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrResidualMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrResidualMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrResidualEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrResidualTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrResidualCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrResidualMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrResidualDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrResidualPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrResidualGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrResidualPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrResidualPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrResidualAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrResidualAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrResidualValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrResidualSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrResidualSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrResidualSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrResidualSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrResidualSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrResidualSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrResidualSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrResidualObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrResidualSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrResidualFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrResidualMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrResidualFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrResidualCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrResidualCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrResidualCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrResidualValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrResidualDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrResidualDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrResidualDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrResidualDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrResidualQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrResidualQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrResidualQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrResidualQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrResidualQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrResidualQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrResidualQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrResidualQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrResidualNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrResidualNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrResidualNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrResidualNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrResidualID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrResidualNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrResidualNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrResidualReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
  end
  object DsResidual: TDataSource
    DataSet = QrResidual
    Left = 228
    Top = 369
  end
  object QrDel: TMySQLQuery
    Database = Dmod.MyDB
    Left = 668
    Top = 252
    object QrDelControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 380
    Top = 44
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 380
    Top = 92
  end
end
