object FmVSMOEnvRet: TFmVSMOEnvRet
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-167 :: Edi'#231#227'o de Controle de Cobran'#231'a de MO'
  ClientHeight = 713
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 486
        Height = 32
        Caption = 'Edi'#231#227'o de Controle de Cobran'#231'a de MO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 486
        Height = 32
        Caption = 'Edi'#231#227'o de Controle de Cobran'#231'a de MO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 486
        Height = 32
        Caption = 'Edi'#231#227'o de Controle de Cobran'#231'a de MO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 595
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 551
    object Panel3: TPanel
      Left = 0
      Top = 236
      Width = 1008
      Height = 359
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitTop = 192
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 153
        Align = alTop
        Caption = ' NFe Retorno mat'#233'ria-prima: '
        TabOrder = 0
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 136
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          ExplicitHeight = 90
          object Label7: TLabel
            Left = 12
            Top = 44
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object Label10: TLabel
            Left = 44
            Top = 44
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
          end
          object Label11: TLabel
            Left = 532
            Top = 0
            Width = 44
            Height = 13
            Caption = 'Empresa:'
            Enabled = False
          end
          object Label9: TLabel
            Left = 12
            Top = 0
            Width = 34
            Height = 13
            Caption = 'S'#233'rie:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label12: TLabel
            Left = 48
            Top = 0
            Width = 50
            Height = 13
            Caption = 'Num NF:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label13: TLabel
            Left = 120
            Top = 44
            Width = 23
            Height = 13
            Caption = 'Item:'
          end
          object LaPecas: TLabel
            Left = 156
            Top = 44
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object LaAreaM2: TLabel
            Left = 228
            Top = 44
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object LaAreaP2: TLabel
            Left = 312
            Top = 44
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
          end
          object LaPeso: TLabel
            Left = 396
            Top = 44
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label14: TLabel
            Left = 480
            Top = 44
            Width = 36
            Height = 13
            Caption = '$ Total:'
          end
          object SbRMP: TSpeedButton
            Left = 508
            Top = 16
            Width = 21
            Height = 21
            OnClick = SbRMPClick
          end
          object Label46: TLabel
            Left = 124
            Top = 0
            Width = 171
            Height = 13
            Caption = 'Prestador do servi'#231'o da M.O.:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label3: TLabel
            Left = 12
            Top = 82
            Width = 100
            Height = 13
            Caption = 'Descri'#231#227'o (opcional):'
          end
          object EdNFRMP_FatID: TdmkEdit
            Left = 12
            Top = 60
            Width = 29
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '-1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = -1
            ValWarn = False
          end
          object EdNFRMP_FatNum: TdmkEdit
            Left = 44
            Top = 60
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '-1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = -1
            ValWarn = False
          end
          object EdNFRMP_Empresa: TdmkEdit
            Left = 532
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNFRMP_SerNF: TdmkEdit
            Left = 12
            Top = 16
            Width = 32
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '-1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = -1
            ValWarn = False
          end
          object EdNFRMP_nNF: TdmkEdit
            Left = 47
            Top = 16
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNFRMP_nItem: TdmkEdit
            Left = 119
            Top = 60
            Width = 32
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNFRMP_Pecas: TdmkEdit
            Left = 156
            Top = 60
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Pecas'
            UpdCampo = 'Pecas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdNFRMP_AreaM2: TdmkEditCalc
            Left = 228
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaM2'
            UpdCampo = 'AreaM2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdNFRMP_AreaP2
            CalcType = ctM2toP2
            CalcFrac = cfQuarto
          end
          object EdNFRMP_AreaP2: TdmkEditCalc
            Left = 312
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaP2'
            UpdCampo = 'AreaP2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdNFRMP_AreaM2
            CalcType = ctP2toM2
            CalcFrac = cfCento
          end
          object EdNFRMP_PesoKg: TdmkEdit
            Left = 396
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdNFRMP_ValorT: TdmkEdit
            Left = 480
            Top = 60
            Width = 109
            Height = 21
            Alignment = taRightJustify
            TabOrder = 12
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ValorT'
            UpdCampo = 'ValorT'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdNFRMP_Terceiro: TdmkEditCB
            Left = 124
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Transporta'
            UpdCampo = 'Transporta'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBNFRMP_Terceiro
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBNFRMP_Terceiro: TdmkDBLookupComboBox
            Left = 180
            Top = 16
            Width = 325
            Height = 21
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsPrestador1
            ParentFont = False
            TabOrder = 3
            dmkEditCB = EdNFRMP_Terceiro
            QryCampo = 'Transporta'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object MeNome: TdmkMemo
            Left = 12
            Top = 98
            Width = 645
            Height = 33
            MaxLength = 240
            TabOrder = 13
            OnKeyDown = MeNomeKeyDown
            UpdType = utYes
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 153
        Width = 1008
        Height = 100
        Align = alTop
        Caption = ' NFe Cobran'#231'a de m'#227'o-de-obra: '
        TabOrder = 1
        ExplicitTop = 104
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 83
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label15: TLabel
            Left = 12
            Top = 40
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object Label16: TLabel
            Left = 44
            Top = 40
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
          end
          object Label17: TLabel
            Left = 532
            Top = 0
            Width = 44
            Height = 13
            Caption = 'Empresa:'
            Enabled = False
          end
          object Label18: TLabel
            Left = 12
            Top = 0
            Width = 34
            Height = 13
            Caption = 'S'#233'rie:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label19: TLabel
            Left = 48
            Top = 0
            Width = 50
            Height = 13
            Caption = 'Num NF:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label20: TLabel
            Left = 120
            Top = 40
            Width = 23
            Height = 13
            Caption = 'Item:'
          end
          object Label21: TLabel
            Left = 156
            Top = 40
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label22: TLabel
            Left = 228
            Top = 40
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object Label23: TLabel
            Left = 312
            Top = 40
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
          end
          object Label24: TLabel
            Left = 396
            Top = 40
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label25: TLabel
            Left = 648
            Top = 40
            Width = 36
            Height = 13
            Caption = '$ Total:'
          end
          object SbCMO: TSpeedButton
            Left = 508
            Top = 16
            Width = 21
            Height = 21
            OnClick = SbCMOClick
          end
          object Label28: TLabel
            Left = 480
            Top = 40
            Width = 59
            Height = 13
            Caption = 'Cus. MO kg:'
          end
          object Label29: TLabel
            Left = 564
            Top = 40
            Width = 58
            Height = 13
            Caption = 'Cus. MO m'#178':'
          end
          object Label31: TLabel
            Left = 124
            Top = 0
            Width = 171
            Height = 13
            Caption = 'Prestador do servi'#231'o da M.O.:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object EdNFCMO_FatID: TdmkEdit
            Left = 12
            Top = 56
            Width = 29
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '-1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = -1
            ValWarn = False
          end
          object EdNFCMO_FatNum: TdmkEdit
            Left = 44
            Top = 56
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '-1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = -1
            ValWarn = False
          end
          object EdNFCMO_Empresa: TdmkEdit
            Left = 532
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNFCMO_SerNF: TdmkEdit
            Left = 12
            Top = 16
            Width = 32
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '-1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = -1
            ValWarn = False
          end
          object EdNFCMO_nNF: TdmkEdit
            Left = 47
            Top = 16
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNFCMO_nItem: TdmkEdit
            Left = 119
            Top = 56
            Width = 32
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNFCMO_Pecas: TdmkEdit
            Left = 156
            Top = 56
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Pecas'
            UpdCampo = 'Pecas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdNFCMO_AreaM2: TdmkEditCalc
            Left = 228
            Top = 56
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaM2'
            UpdCampo = 'AreaM2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdNFCMO_AreaM2Redefinido
            dmkEditCalcA = EdNFCMO_AreaP2
            CalcType = ctM2toP2
            CalcFrac = cfQuarto
          end
          object EdNFCMO_AreaP2: TdmkEditCalc
            Left = 312
            Top = 56
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaP2'
            UpdCampo = 'AreaP2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdNFCMO_AreaM2
            CalcType = ctP2toM2
            CalcFrac = cfCento
          end
          object EdNFCMO_PesoKg: TdmkEdit
            Left = 396
            Top = 56
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdNFCMO_PesoKgRedefinido
          end
          object EdNFCMO_ValorT: TdmkEdit
            Left = 648
            Top = 56
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 14
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ValorT'
            UpdCampo = 'ValorT'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdNFCMO_CusMOKG: TdmkEdit
            Left = 480
            Top = 56
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 12
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdNFCMO_CusMOKGRedefinido
          end
          object EdNFCMO_CusMOM2: TdmkEdit
            Left = 564
            Top = 56
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 13
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdNFCMO_CusMOM2Redefinido
          end
          object EdNFCMO_Terceiro: TdmkEditCB
            Left = 124
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Transporta'
            UpdCampo = 'Transporta'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBNFCMO_Terceiro
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBNFCMO_Terceiro: TdmkDBLookupComboBox
            Left = 180
            Top = 16
            Width = 325
            Height = 21
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsPrestador1
            ParentFont = False
            TabOrder = 3
            dmkEditCB = EdNFCMO_Terceiro
            QryCampo = 'Transporta'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
      end
      object GroupBox5: TGroupBox
        Left = 0
        Top = 253
        Width = 1008
        Height = 104
        Align = alTop
        Caption = ' CTe Cobran'#231'a de frete: '
        TabOrder = 2
        ExplicitTop = 204
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 87
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label32: TLabel
            Left = 72
            Top = 4
            Width = 14
            Height = 13
            Caption = 'ID:'
            Enabled = False
          end
          object Label33: TLabel
            Left = 104
            Top = 4
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
            Enabled = False
          end
          object Label34: TLabel
            Left = 12
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Empresa:'
            Enabled = False
          end
          object Label35: TLabel
            Left = 588
            Top = 4
            Width = 34
            Height = 13
            Caption = 'S'#233'rie:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label36: TLabel
            Left = 624
            Top = 4
            Width = 49
            Height = 13
            Caption = 'Num CF:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label37: TLabel
            Left = 700
            Top = 4
            Width = 23
            Height = 13
            Caption = 'Item:'
          end
          object Label38: TLabel
            Left = 12
            Top = 44
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label39: TLabel
            Left = 84
            Top = 44
            Width = 39
            Height = 13
            Caption = #193'rea m'#178':'
          end
          object Label40: TLabel
            Left = 168
            Top = 44
            Width = 37
            Height = 13
            Caption = #193'rea ft'#178':'
          end
          object Label41: TLabel
            Left = 252
            Top = 44
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label42: TLabel
            Left = 544
            Top = 44
            Width = 45
            Height = 13
            Caption = '$ Total:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object SbCF: TSpeedButton
            Left = 648
            Top = 60
            Width = 21
            Height = 21
            Enabled = False
          end
          object Label43: TLabel
            Left = 336
            Top = 44
            Width = 81
            Height = 13
            Caption = 'Peso kg frete:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label44: TLabel
            Left = 440
            Top = 44
            Width = 78
            Height = 13
            Caption = 'Cus. frete kg:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label45: TLabel
            Left = 180
            Top = 4
            Width = 84
            Height = 13
            Caption = 'Transportador:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object EdCFTPA_FatID: TdmkEdit
            Left = 72
            Top = 20
            Width = 29
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '-1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = -1
            ValWarn = False
          end
          object EdCFTPA_FatNum: TdmkEdit
            Left = 104
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '-1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = -1
            ValWarn = False
          end
          object EdCFTPA_Empresa: TdmkEdit
            Left = 12
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdCFTPA_SerCT: TdmkEdit
            Left = 587
            Top = 20
            Width = 32
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '-1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = -1
            ValWarn = False
          end
          object EdCFTPA_nCT: TdmkEdit
            Left = 623
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnKeyDown = EdCFTPA_nCTKeyDown
          end
          object EdCFTPA_nItem: TdmkEdit
            Left = 699
            Top = 20
            Width = 32
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdCFTPA_Pecas: TdmkEdit
            Left = 12
            Top = 60
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Pecas'
            UpdCampo = 'Pecas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCFTPA_AreaM2: TdmkEditCalc
            Left = 84
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaM2'
            UpdCampo = 'AreaM2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdCFTPA_AreaP2
            CalcType = ctM2toP2
            CalcFrac = cfQuarto
          end
          object EdCFTPA_AreaP2: TdmkEditCalc
            Left = 168
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AreaP2'
            UpdCampo = 'AreaP2'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            dmkEditCalcA = EdCFTPA_AreaM2
            CalcType = ctP2toM2
            CalcFrac = cfCento
          end
          object EdCFTPA_PesoKg: TdmkEdit
            Left = 252
            Top = 60
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCFTPA_ValorT: TdmkEdit
            Left = 544
            Top = 60
            Width = 100
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 14
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ValorT'
            UpdCampo = 'ValorT'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdCFTPA_PesTrKg: TdmkEdit
            Left = 336
            Top = 60
            Width = 100
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 12
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdCFTPA_PesTrKgRedefinido
          end
          object EdCFTPA_CusTrKg: TdmkEdit
            Left = 440
            Top = 60
            Width = 100
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 13
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdCFTPA_CusTrKgRedefinido
          end
          object EdCFTPA_Terceiro: TdmkEditCB
            Left = 180
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Transporta'
            UpdCampo = 'Transporta'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCFTPA_Terceiro
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCFTPA_Terceiro: TdmkDBLookupComboBox
            Left = 236
            Top = 20
            Width = 345
            Height = 21
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsTransporta
            ParentFont = False
            TabOrder = 4
            dmkEditCB = EdCFTPA_Terceiro
            QryCampo = 'Transporta'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
      end
    end
    object Panel10: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 236
      Align = alClient
      TabOrder = 0
      ExplicitHeight = 244
      object Splitter1: TSplitter
        Left = 447
        Top = 1
        Width = 5
        Height = 189
        Align = alRight
        ExplicitLeft = 1
        ExplicitHeight = 197
      end
      object Panel11: TPanel
        Left = 1
        Top = 190
        Width = 1006
        Height = 45
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        ExplicitTop = 198
        object Label4: TLabel
          Left = 404
          Top = 4
          Width = 29
          Height = 13
          Caption = 'Data: '
        end
        object LaHora: TLabel
          Left = 520
          Top = 4
          Width = 26
          Height = 13
          Caption = 'Hora:'
        end
        object Panel12: TPanel
          Left = 0
          Top = 0
          Width = 240
          Height = 45
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object BtItem: TBitBtn
            Tag = 11
            Left = 5
            Top = 3
            Width = 232
            Height = 40
            Caption = '&Editar dados item selecionado'
            NumGlyphs = 2
            TabOrder = 0
            TabStop = False
            OnClick = BtItemClick
          end
        end
        object Panel13: TPanel
          Left = 760
          Top = 0
          Width = 246
          Height = 45
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtProntosRetornados: TBitBtn
            Tag = 11
            Left = 5
            Top = 3
            Width = 232
            Height = 40
            Caption = '&Editar dados item selecionado'
            NumGlyphs = 2
            TabOrder = 0
            TabStop = False
            OnClick = BtProntosRetornadosClick
          end
        end
        object GroupBox1: TGroupBox
          Left = 240
          Top = 0
          Width = 157
          Height = 45
          Align = alLeft
          Caption = ' Dados do registro: '
          Enabled = False
          TabOrder = 2
          object Label1: TLabel
            Left = 16
            Top = 20
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
          end
          object EdCodigo: TdmkEdit
            Left = 60
            Top = 16
            Width = 80
            Height = 21
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '0'
            ValWarn = False
          end
        end
        object TPData: TdmkEditDateTimePicker
          Left = 404
          Top = 20
          Width = 113
          Height = 21
          Date = 40656.000000000000000000
          Time = 40656.000000000000000000
          ShowCheckbox = True
          TabOrder = 3
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdHora: TdmkEdit
          Left = 520
          Top = 20
          Width = 80
          Height = 21
          TabOrder = 4
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox7: TGroupBox
        Left = 1
        Top = 1
        Width = 446
        Height = 189
        Align = alClient
        Caption = 
          ' Itens de NFes de MAT'#200'RIA-PRIMA RETORNADOS de itens enviados par' +
          'a M.O.: '
        TabOrder = 0
        ExplicitHeight = 197
        object DBGVSMOEnvRVmi: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 442
          Height = 172
          TabStop = False
          Align = alClient
          DataSource = DsVSMOEnvRVmi
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'NFeSer'
              Title.Caption = 'S'#233'rie NF'
              Width = 46
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFeNum'
              Title.Caption = 'n'#186' NF'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea m'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaP2'
              Title.Caption = #193'rea ft'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorT'
              Title.Caption = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IDItem'
              Title.Caption = 'ID Item'
              Visible = True
            end>
        end
      end
      object GroupBox8: TGroupBox
        Left = 452
        Top = 1
        Width = 555
        Height = 189
        Align = alRight
        Caption = ' IME-Is de couros PRONTOS RETORNADOS: '
        TabOrder = 1
        ExplicitHeight = 197
        object DBGVSMOEnvGVmi: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 551
          Height = 172
          TabStop = False
          Align = alClient
          DataSource = DsVSMOEnvGVmi
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRDA_TAM_COR'
              Title.Caption = 'Nome'
              Width = 197
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea m'#178
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso kg'
              Width = 62
              Visible = True
            end>
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 643
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtMONaoCobr: TBitBtn
        Tag = 14
        Left = 320
        Top = 4
        Width = 180
        Height = 40
        Caption = '&MO ainda n'#227'o cobrada!'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtMONaoCobrClick
      end
      object BtContinoaCobranca: TBitBtn
        Tag = 14
        Left = 136
        Top = 4
        Width = 180
        Height = 40
        Caption = '&Igual a '#250'ltima MO lan'#231'ada!'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtContinoaCobrancaClick
      end
      object GBAvisos1: TGroupBox
        Left = 504
        Top = -1
        Width = 352
        Height = 46
        Caption = ' Avisos: '
        TabOrder = 3
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 348
          Height = 29
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 401
          ExplicitHeight = 25
          object LaAviso1: TLabel
            Left = 13
            Top = 2
            Width = 120
            Height = 17
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2: TLabel
            Left = 12
            Top = 1
            Width = 120
            Height = 17
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 65532
    Top = 65531
  end
  object QrCabA: TmySQLQuery
    Database = Dmod.MyDB
    Left = 96
    Top = 104
  end
  object DsCabA: TDataSource
    DataSet = QrCabA
    Left = 96
    Top = 152
  end
  object QrTransporta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 24
    Top = 104
    object QrTransportaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportaNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransporta: TDataSource
    DataSet = QrTransporta
    Left = 24
    Top = 152
  end
  object QrPrestador1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 164
    Top = 104
    object QrPrestador1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestador1NOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador1: TDataSource
    DataSet = QrPrestador1
    Left = 160
    Top = 152
  end
  object QrPrestador2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece7="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 236
    Top = 104
    object QrPrestador2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrestador2NOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsPrestador2: TDataSource
    DataSet = QrPrestador2
    Left = 236
    Top = 152
  end
  object QrVSMOEnvGVmi: TmySQLQuery
   
    AfterOpen = QrVSMOEnvGVmiAfterOpen
    SQL.Strings = (
      'SELECT * FROM _vmi_qtd_env_gvmi')
    Left = 460
    Top = 104
    object QrVSMOEnvGVmiControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMOEnvGVmiMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSMOEnvGVmiGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSMOEnvGVmiPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvGVmiPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvGVmiAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvGVmiAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvGVmiNO_PRDA_TAM_COR: TWideStringField
      FieldName = 'NO_PRDA_TAM_COR'
      Size = 255
    end
    object QrVSMOEnvGVmiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMOEnvGVmiValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvGVmiIDItem: TIntegerField
      FieldName = 'IDItem'
    end
  end
  object DsVSMOEnvGVmi: TDataSource
    DataSet = QrVSMOEnvGVmi
    Left = 456
    Top = 152
  end
  object QrVSMOEnvRVmi: TmySQLQuery
   
    AfterOpen = QrVSMOEnvRVmiAfterOpen
    SQL.Strings = (
      'SELECT * FROM _vmi_qtd_env_rvmi')
    Left = 340
    Top = 104
    object QrVSMOEnvRVmiVSMOEnvEnv: TIntegerField
      FieldName = 'VSMOEnvEnv'
    end
    object QrVSMOEnvRVmiNFeSer: TIntegerField
      FieldName = 'NFeSer'
    end
    object QrVSMOEnvRVmiNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrVSMOEnvRVmiPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0;-#,###,##0.0; '
    end
    object QrVSMOEnvRVmiPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrVSMOEnvRVmiAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRVmiAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRVmiValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrVSMOEnvRVmiIDItem: TIntegerField
      FieldName = 'IDItem'
    end
    object QrVSMOEnvRVmiAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSMOEnvRVmi: TDataSource
    DataSet = QrVSMOEnvRVmi
    Left = 340
    Top = 152
  end
  object QrSumG: TmySQLQuery
    Database = Dmod.MyDB
    Left = 456
    Top = 200
    object QrSumGPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0'
    end
    object QrSumGPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrSumGAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumGAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumGAreaKg: TFloatField
      FieldName = 'AreaKg'
    end
    object QrSumGValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
  object QrSumR: TmySQLQuery
    Database = Dmod.MyDB
    Left = 340
    Top = 200
    object QrSumRPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,##0.0'
    end
    object QrSumRPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrSumRAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumRAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumRAreaKg: TFloatField
      FieldName = 'AreaKg'
    end
    object QrSumRValorT: TFloatField
      FieldName = 'ValorT'
    end
  end
  object QrEnvR: TmySQLQuery
   
    AfterOpen = QrVSMOEnvRVmiAfterOpen
    SQL.Strings = (
      'SELECT * FROM _vmi_qtd_env_rvmi')
    Left = 600
    Top = 196
    object QrEnvRVSMOEnvEnv: TIntegerField
      FieldName = 'VSMOEnvEnv'
    end
  end
  object PMProntosRetornados: TPopupMenu
    Left = 852
    Top = 188
    object Incluiitemprontoretornado1: TMenuItem
      Caption = '&Inclui item pronto retornado'
      Enabled = False
      OnClick = Incluiitemprontoretornado1Click
    end
    object Alteraitemprontoretornadoselecionado1: TMenuItem
      Caption = '&Altera item pronto retornado selecionado'
      OnClick = Alteraitemprontoretornadoselecionado1Click
    end
    object Excluiitemprontoretornadoselecionado1: TMenuItem
      Caption = '&Exclui item pronto retornado selecionado'
      OnClick = Excluiitemprontoretornadoselecionado1Click
    end
  end
end
