object FmVSPlCCab: TFmVSPlCCab
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-076 :: Entrada de Artigo Classificado'
  ClientHeight = 661
  ClientWidth = 1014
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1014
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1014
      Height = 285
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label52: TLabel
        Left = 476
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Data / hora compra:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label53: TLabel
        Left = 628
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 780
        Top = 16
        Width = 103
        Height = 13
        Caption = 'Data / hora chegada:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 504
        Top = 56
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label5: TLabel
        Left = 504
        Top = 96
        Width = 69
        Height = 13
        Caption = 'Transportador:'
      end
      object Label6: TLabel
        Left = 932
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object SbFornecedor: TSpeedButton
        Left = 868
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbFornecedorClick
      end
      object SbTransportador: TSpeedButton
        Left = 968
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbTransportadorClick
      end
      object Label20: TLabel
        Left = 16
        Top = 56
        Width = 70
        Height = 13
        Caption = 'Cliente de MO:'
      end
      object SbClienteMO: TSpeedButton
        Left = 380
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbClienteMOClick
      end
      object Label21: TLabel
        Left = 16
        Top = 96
        Width = 229
        Height = 13
        Caption = 'Proced'#234'ncia: (Tipo de entidade: Fornece outros)'
      end
      object SbProcedenc: TSpeedButton
        Left = 480
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbProcedencClick
      end
      object Label22: TLabel
        Left = 16
        Top = 136
        Width = 212
        Height = 13
        Caption = 'Motorista: (Tipo de entidade: Fornece outros)'
      end
      object SbMotorista: TSpeedButton
        Left = 480
        Top = 152
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbMotoristaClick
      end
      object Label23: TLabel
        Left = 504
        Top = 136
        Width = 30
        Height = 13
        Caption = 'Placa:'
      end
      object Label28: TLabel
        Left = 404
        Top = 56
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label29: TLabel
        Left = 436
        Top = 56
        Width = 63
        Height = 13
        Caption = 'N'#250'mero NFe:'
      end
      object Label31: TLabel
        Left = 892
        Top = 56
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label32: TLabel
        Left = 924
        Top = 56
        Width = 63
        Height = 13
        Caption = 'N'#250'mero NFe:'
      end
      object LaPecas: TLabel
        Left = 588
        Top = 136
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        Enabled = False
      end
      object LaAreaM2: TLabel
        Left = 664
        Top = 136
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        Enabled = False
      end
      object LaAreaP2: TLabel
        Left = 740
        Top = 136
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        Enabled = False
      end
      object LaPeso: TLabel
        Left = 816
        Top = 136
        Width = 42
        Height = 13
        Caption = 'Peso kg:'
        Enabled = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDtCompra: TdmkEditDateTimePicker
        Left = 476
        Top = 32
        Width = 108
        Height = 21
        Date = 44663.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtCompra'
        UpdCampo = 'DtCompra'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtCompra: TdmkEdit
        Left = 584
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtCompra'
        UpdCampo = 'DtCompra'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtViagem: TdmkEditDateTimePicker
        Left = 628
        Top = 32
        Width = 108
        Height = 21
        Date = 44663.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtViagem'
        UpdCampo = 'DtViagem'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtViagem: TdmkEdit
        Left = 736
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 6
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtViagem'
        UpdCampo = 'DtViagem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtEntrada: TdmkEditDateTimePicker
        Left = 780
        Top = 32
        Width = 108
        Height = 21
        Date = 44663.000000000000000000
        Time = 0.639644131944805800
        TabOrder = 7
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtEntrada'
        UpdCampo = 'DtEntrada'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtEntrada: TdmkEdit
        Left = 888
        Top = 32
        Width = 40
        Height = 21
        TabOrder = 8
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtEntrada'
        UpdCampo = 'DtEntrada'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdFornecedor: TdmkEditCB
        Left = 504
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Fornecedor'
        UpdCampo = 'Fornecedor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFornecedor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFornecedor: TdmkDBLookupComboBox
        Left = 560
        Top = 72
        Width = 309
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsFornecedor
        TabOrder = 15
        dmkEditCB = EdFornecedor
        QryCampo = 'Fornecedor'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTransporta: TdmkEditCB
        Left = 504
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 20
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Transporta'
        UpdCampo = 'Transporta'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTransporta
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTransporta: TdmkDBLookupComboBox
        Left = 560
        Top = 112
        Width = 408
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsTransporta
        TabOrder = 21
        dmkEditCB = EdTransporta
        QryCampo = 'Transporta'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdMovimCod: TdmkEdit
        Left = 932
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 337
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdClienteMO: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ClienteMO'
        UpdCampo = 'ClienteMO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClienteMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClienteMO: TdmkDBLookupComboBox
        Left = 72
        Top = 72
        Width = 309
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClienteMO
        TabOrder = 11
        dmkEditCB = EdClienteMO
        QryCampo = 'ClienteMO'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdProcednc: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 18
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Procednc'
        UpdCampo = 'Procednc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBProcednc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBProcednc: TdmkDBLookupComboBox
        Left = 72
        Top = 112
        Width = 408
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsProcednc
        TabOrder = 19
        dmkEditCB = EdProcednc
        QryCampo = 'Procednc'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdMotorista: TdmkEditCB
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 22
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Motorista'
        UpdCampo = 'Motorista'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBMotorista
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBMotorista: TdmkDBLookupComboBox
        Left = 72
        Top = 152
        Width = 408
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsMotorista
        TabOrder = 23
        dmkEditCB = EdMotorista
        QryCampo = 'Motorista'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPlaca: TdmkEdit
        Left = 504
        Top = 152
        Width = 80
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 24
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Placa'
        UpdCampo = 'Placa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Edide_serie: TdmkEdit
        Left = 404
        Top = 72
        Width = 28
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ide_serie'
        UpdCampo = 'ide_serie'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edide_nNF: TdmkEdit
        Left = 436
        Top = 72
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999999999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ide_nNF'
        UpdCampo = 'ide_nNF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edemi_serie: TdmkEdit
        Left = 892
        Top = 72
        Width = 28
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'emi_serie'
        UpdCampo = 'emi_serie'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Edemi_nNF: TdmkEdit
        Left = 924
        Top = 72
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '999999999'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'emi_nNF'
        UpdCampo = 'emi_nNF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdPecas: TdmkEdit
        Left = 588
        Top = 152
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 25
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Pecas'
        UpdCampo = 'Pecas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdAreaM2: TdmkEditCalc
        Left = 664
        Top = 152
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 26
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaM2'
        UpdCampo = 'AreaM2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaP2
        CalcType = ctM2toP2
        CalcFrac = cfQuarto
      end
      object EdAreaP2: TdmkEditCalc
        Left = 740
        Top = 152
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 27
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'AreaP2'
        UpdCampo = 'AreaP2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        dmkEditCalcA = EdAreaM2
        CalcType = ctP2toM2
        CalcFrac = cfCento
      end
      object EdPesoKg: TdmkEdit
        Left = 816
        Top = 152
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 28
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'PesoKg'
        UpdCampo = 'PesoKg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 502
      Width = 1014
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 874
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1014
    Height = 565
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1014
      Height = 177
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label10: TLabel
        Left = 476
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Data / hora compra:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 628
        Top = 16
        Width = 88
        Height = 13
        Caption = 'Data / hora sa'#237'da:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 780
        Top = 16
        Width = 103
        Height = 13
        Caption = 'Data / hora chegada:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 932
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label14: TLabel
        Left = 16
        Top = 56
        Width = 70
        Height = 13
        Caption = 'Cliente de MO:'
      end
      object Label15: TLabel
        Left = 16
        Top = 96
        Width = 229
        Height = 13
        Caption = 'Proced'#234'ncia: (Tipo de entidade: Fornece outros)'
      end
      object Label24: TLabel
        Left = 16
        Top = 136
        Width = 212
        Height = 13
        Caption = 'Motorista: (Tipo de entidade: Fornece outros)'
      end
      object Label25: TLabel
        Left = 508
        Top = 56
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
      end
      object Label26: TLabel
        Left = 508
        Top = 96
        Width = 69
        Height = 13
        Caption = 'Transportador:'
      end
      object Label27: TLabel
        Left = 508
        Top = 136
        Width = 30
        Height = 13
        Caption = 'Placa:'
      end
      object Label9: TLabel
        Left = 688
        Top = 136
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
      end
      object Label16: TLabel
        Left = 440
        Top = 56
        Width = 63
        Height = 13
        Caption = 'N'#250'mero NFe:'
      end
      object Label17: TLabel
        Left = 408
        Top = 56
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label18: TLabel
        Left = 764
        Top = 136
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
      end
      object Label19: TLabel
        Left = 916
        Top = 136
        Width = 42
        Height = 13
        Caption = 'Peso kg:'
      end
      object Label30: TLabel
        Left = 840
        Top = 136
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
      end
      object Label33: TLabel
        Left = 900
        Top = 56
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label34: TLabel
        Left = 932
        Top = 56
        Width = 63
        Height = 13
        Caption = 'N'#250'mero NFe:'
      end
      object Label35: TLabel
        Left = 432
        Top = 136
        Width = 34
        Height = 13
        Caption = 'Valor T'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSPlCCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVSPlCCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 132
        Top = 32
        Width = 341
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsVSPlCCab
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 476
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtCompra'
        DataSource = DsVSPlCCab
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 628
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtViagem'
        DataSource = DsVSPlCCab
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 780
        Top = 32
        Width = 148
        Height = 21
        DataField = 'DtEntrada'
        DataSource = DsVSPlCCab
        TabOrder = 5
      end
      object DBEdit6: TDBEdit
        Left = 932
        Top = 32
        Width = 64
        Height = 21
        DataField = 'MovimCod'
        DataSource = DsVSPlCCab
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 508
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Fornecedor'
        DataSource = DsVSPlCCab
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 564
        Top = 72
        Width = 333
        Height = 21
        DataField = 'NO_FORNECE'
        DataSource = DsVSPlCCab
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 508
        Top = 112
        Width = 56
        Height = 21
        DataField = 'Transporta'
        DataSource = DsVSPlCCab
        TabOrder = 9
      end
      object DBEdit10: TDBEdit
        Left = 564
        Top = 112
        Width = 432
        Height = 21
        DataField = 'NO_TRANSPORTA'
        DataSource = DsVSPlCCab
        TabOrder = 10
      end
      object DBEdit11: TDBEdit
        Left = 840
        Top = 152
        Width = 72
        Height = 21
        DataField = 'AreaP2'
        DataSource = DsVSPlCCab
        TabOrder = 11
      end
      object DBEdit12: TDBEdit
        Left = 916
        Top = 152
        Width = 80
        Height = 21
        DataField = 'PesoKg'
        DataSource = DsVSPlCCab
        TabOrder = 12
      end
      object DBEdit13: TDBEdit
        Left = 688
        Top = 152
        Width = 72
        Height = 21
        DataField = 'Pecas'
        DataSource = DsVSPlCCab
        TabOrder = 13
      end
      object DBEdit14: TDBEdit
        Left = 764
        Top = 152
        Width = 72
        Height = 21
        DataField = 'AreaM2'
        DataSource = DsVSPlCCab
        TabOrder = 14
      end
      object DBEdit15: TDBEdit
        Left = 432
        Top = 152
        Width = 72
        Height = 21
        DataField = 'ValorT'
        DataSource = DsVSPlCCab
        TabOrder = 15
      end
      object DBEdit16: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'ClienteMO'
        DataSource = DsVSPlCCab
        TabOrder = 16
      end
      object DBEdit17: TDBEdit
        Left = 72
        Top = 72
        Width = 333
        Height = 21
        DataField = 'NO_CLIENTEMO'
        DataSource = DsVSPlCCab
        TabOrder = 17
      end
      object DBEdit18: TDBEdit
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        DataField = 'Motorista'
        DataSource = DsVSPlCCab
        TabOrder = 18
      end
      object DBEdit19: TDBEdit
        Left = 72
        Top = 152
        Width = 357
        Height = 21
        DataField = 'NO_MOTORISTA'
        DataSource = DsVSPlCCab
        TabOrder = 19
      end
      object DBEdit20: TDBEdit
        Left = 508
        Top = 152
        Width = 77
        Height = 21
        DataField = 'Placa'
        DataSource = DsVSPlCCab
        TabOrder = 20
      end
      object DBEdit21: TDBEdit
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        DataField = 'Procednc'
        DataSource = DsVSPlCCab
        TabOrder = 21
      end
      object DBEdit22: TDBEdit
        Left = 72
        Top = 112
        Width = 432
        Height = 21
        DataField = 'NO_PROCEDNC'
        DataSource = DsVSPlCCab
        TabOrder = 22
      end
      object DBEdit23: TDBEdit
        Left = 408
        Top = 72
        Width = 28
        Height = 21
        DataField = 'ide_serie'
        DataSource = DsVSPlCCab
        TabOrder = 23
      end
      object DBEdit24: TDBEdit
        Left = 440
        Top = 72
        Width = 64
        Height = 21
        DataField = 'ide_nNF'
        DataSource = DsVSPlCCab
        TabOrder = 24
      end
      object DBEdit25: TDBEdit
        Left = 900
        Top = 72
        Width = 28
        Height = 21
        DataField = 'emi_serie'
        DataSource = DsVSPlCCab
        TabOrder = 25
      end
      object DBEdit26: TDBEdit
        Left = 932
        Top = 72
        Width = 64
        Height = 21
        DataField = 'emi_nNF'
        DataSource = DsVSPlCCab
        TabOrder = 26
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 501
      Width = 1014
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 114
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 288
        Top = 15
        Width = 724
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 608
          Top = 0
          Width = 116
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 3
            Width = 104
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 223
          Left = 4
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cabe'#231'alho'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 421
          Left = 108
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtReclasif: TBitBtn
          Tag = 421
          Left = 420
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&Gera artigos'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtReclasifClick
        end
        object BtFiscal: TBitBtn
          Tag = 1000516
          Left = 212
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&NF-e'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtFiscalClick
        end
        object BtCTe: TBitBtn
          Tag = 1000516
          Left = 316
          Top = 4
          Width = 104
          Height = 40
          Cursor = crHandPoint
          Caption = '&CT-e'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtCTeClick
        end
      end
    end
    object PCBottom: TPageControl
      Left = 4
      Top = 216
      Width = 1008
      Height = 285
      ActivePage = TabSheet1
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' Dados da entrada'
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 257
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GBIts: TGroupBox
            Left = 0
            Top = 0
            Width = 1000
            Height = 257
            Align = alClient
            Caption = ' Itens da entrada: '
            TabOrder = 0
            object DGDados: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 996
              Height = 240
              Align = alClient
              DataSource = DsVSPlCIts
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              PopupMenu = PMVPlcIts
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_TTW'
                  Title.Caption = 'Tabela'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SerieFch'
                  Title.Caption = 'ID Serie'
                  Width = 43
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Ficha'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pallet'
                  Title.Caption = 'ID Pallet'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PALLET'
                  Title.Caption = 'Descr. Pallet'
                  Width = 76
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Marca'
                  Width = 90
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'GraGruX'
                  Title.Caption = 'Reduzido'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_PRD_TAM_COR'
                  Title.Caption = 'Mat'#233'ria-prima / tamanho / cor'
                  Width = 240
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CustoM2'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pecas'
                  Title.Caption = 'Pe'#231'as'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaM2'
                  Title.Caption = #193'rea m'#178
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AreaP2'
                  Title.Caption = #193'rea ft'#178
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PesoKg'
                  Title.Caption = 'Peso kg'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'IME-I'
                  Width = 60
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Observ'
                  Title.Caption = 'Observa'#231#245'es'
                  Width = 300
                  Visible = True
                end>
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Hist'#243'rico da Ficha RMP selecionada'
        ImageIndex = 1
        object Splitter2: TSplitter
          Left = 870
          Top = 0
          Width = 5
          Height = 257
        end
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 870
          Height = 257
          Align = alLeft
          DataSource = DsVSHisFch
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data / hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Palavras chave [TAG]'
              Width = 187
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Title.Caption = 'Texto hist'#243'rico:'
              Width = 460
              Visible = True
            end>
        end
        object DBMemo1: TDBMemo
          Left = 875
          Top = 0
          Width = 125
          Height = 257
          Align = alClient
          DataField = 'Observ'
          DataSource = DsVSHisFch
          TabOrder = 1
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'NF-es de entrada'
        ImageIndex = 2
        object Splitter1: TSplitter
          Left = 0
          Top = 81
          Width = 1000
          Height = 5
          Cursor = crVSplit
          Align = alTop
          ExplicitTop = 117
        end
        object DBGInnNFsCab: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 81
          Align = alTop
          DataSource = DsEfdInnNFsCab
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Terceiro'
              Title.Caption = 'Fornecedor'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TER'
              Title.Caption = 'Nome do fornecedor'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SER'
              Title.Caption = 'S'#233'rie'
              Width = 26
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NUM_DOC'
              Title.Caption = 'NF'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DT_DOC'
              Title.Caption = 'Emiss'#227'o'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DT_E_S'
              Title.Caption = 'Entrada'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_DOC'
              Title.Caption = '$ Docum.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_DESC'
              Title.Caption = '$ Desc.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_MERC'
              Title.Caption = '$ Produtos'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_FRT'
              Title.Caption = '$ Frete'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_SEG'
              Title.Caption = '$ Seguro'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_OUT_DA'
              Title.Caption = '$ Outros'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_BC_ICMS'
              Title.Caption = '$ BC ICMS'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_ICMS'
              Title.Caption = '$ ICMS'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_IPI'
              Title.Caption = '$ IPI'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_PIS'
              Title.Caption = '$ PIS'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_COFINS'
              Title.Caption = '$ COFINS'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_PIS_ST'
              Title.Caption = '$ PIS ST'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_COFINS_ST'
              Title.Caption = '$ COFINS ST'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_BC_ICMS_ST'
              Title.Caption = '$ BC ICMS ST'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_ICMS_ST'
              Title.Caption = '$ ICMS ST'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFe_FatID'
              Title.Caption = 'FatID NFe'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFe_FatNum'
              Title.Caption = 'Fat.Num.NFe'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFe_StaLnk'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovFatID'
              Title.Caption = 'FatID Mov'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovFatNum'
              Title.Caption = 'Fat.Num. Mov'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovimCod'
              Title.Caption = 'IME-C'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COD_MOD'
              Title.Caption = 'Modelo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CHV_NFE'
              Title.Caption = 'Chave NFe'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COD_SIT'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IND_PGTO'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IND_FRT'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFeStatus'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Empresa'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CliInt'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaP2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorT'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Motorista'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Placa'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_ABAT_NT'
              Visible = True
            end>
        end
        object DBGInnNFsIts: TDBGrid
          Left = 0
          Top = 86
          Width = 1000
          Height = 171
          Align = alClient
          DataSource = DsEfdInnNFsIts
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Conta'
              Title.Caption = 'ID'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CFOP'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NCM'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGru1'
              Title.Caption = 'C'#243'digo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Nome do Produto'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UNID'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTD'
              Title.Caption = 'Quantid.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'xLote'
              Title.Caption = 'Lote'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_ITEM'
              Title.Caption = '$ Item'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_DESC'
              Title.Caption = '$ Desc.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CST_ICMS'
              Title.Caption = 'CST ICMS'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_BC_ICMS'
              Title.Caption = '$ BC ICMS'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ALIQ_ICMS'
              Title.Caption = '% ICMS'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_ICMS'
              Title.Caption = '$ ICMS'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CST_IPI'
              Title.Caption = 'CST IPI'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_BC_IPI'
              Title.Caption = '$ BC IPI'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ALIQ_IPI'
              Title.Caption = '% IPI'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_IPI'
              Title.Caption = '$ IPI'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CST_PIS'
              Title.Caption = 'CST PIS'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_BC_PIS'
              Title.Caption = '$ BC PIS'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ALIQ_PIS_p'
              Title.Caption = '% PIS'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_PIS'
              Title.Caption = '$ PIS'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CST_COFINS'
              Title.Caption = 'CST COFINS'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_BC_COFINS'
              Title.Caption = '$ BC COFINS'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ALIQ_COFINS_p'
              Title.Caption = '% COFINS'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_COFINS'
              Title.Caption = '$ COFINS'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_BC_ICMS_ST'
              Title.Caption = '$ BC ICMS ST'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ALIQ_ST'
              Title.Caption = '% ST'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_ICMS_ST'
              Title.Caption = '$ ICMS ST'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COD_ENQ'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COD_CTA'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_ABAT_NT'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QUANT_BC_PIS'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ALIQ_PIS_r'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QUANT_BC_COFINS'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ALIQ_COFINS_r'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtCorrApo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ori_IPIpIPI'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IND_MOV'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IND_APUR'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_vProd'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_vFrete'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_vSeg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'prod_vOutro'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ori_IPIvIPI'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GerBxaEstq'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COD_NAT'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UnidMed'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SIGLAUNIDMED'
              Title.Caption = 'Unidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ex_TIPI'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Grandeza'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tipo_Item'
              Visible = True
            end>
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'CT-es de entrada'
        ImageIndex = 3
        object DBGFretes: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 257
          Align = alClient
          DataSource = DsEfdInnCTsCab
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Terceiro'
              Title.Caption = 'Fornecedor'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TER'
              Title.Caption = 'Nome do fornecedor'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SER'
              Title.Caption = 'S'#233'rie'
              Width = 26
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUB'
              Title.Caption = 'Sub'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NUM_DOC'
              Title.Caption = 'NF'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DT_DOC'
              Title.Caption = 'Emiss'#227'o'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DT_A_P'
              Title.Caption = 'Aquisi'#231#227'o'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_MERC'
              Title.Caption = '$ Servi'#231'os'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_DESC'
              Title.Caption = '$ Desc.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_DOC'
              Title.Caption = '$ Docum.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_BC_ICMS'
              Title.Caption = '$ BC ICMS'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_ICMS'
              Title.Caption = '$ ICMS'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFe_FatID'
              Title.Caption = 'FatID NFe'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFe_FatNum'
              Title.Caption = 'Fat.Num.NFe'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFe_StaLnk'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovFatID'
              Title.Caption = 'FatID Mov'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovFatNum'
              Title.Caption = 'Fat.Num. Mov'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovimCod'
              Title.Caption = 'IME-C'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COD_MOD'
              Title.Caption = 'Modelo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CHV_CTE'
              Title.Caption = 'Chave CTe'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COD_SIT'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IND_FRT'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CTeStatus'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Empresa'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CliInt'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaP2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorT'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Motorista'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Placa'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VL_NT'
              Title.Caption = 'VL_ABAT_NT'
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1014
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 966
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 45
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 750
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 366
        Height = 32
        Caption = 'Entrada de Artigo Classificado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 366
        Height = 32
        Caption = 'Entrada de Artigo Classificado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 366
        Height = 32
        Caption = 'Entrada de Artigo Classificado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1014
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1010
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 600
    Top = 44
  end
  object QrVSPlCCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSPlCCabBeforeOpen
    AfterOpen = QrVSPlCCabAfterOpen
    BeforeClose = QrVSPlCCabBeforeClose
    AfterScroll = QrVSPlCCabAfterScroll
    SQL.Strings = (
      'SELECT wic.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_EMPRESA,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'IF(trn.Tipo=0, trn.RazaoSocial, trn.Nome) NO_TRANSPORTA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLIENTEMO,'
      'IF(prc.Tipo=0, prc.RazaoSocial, prc.Nome) NO_PROCEDNC,'
      'IF(mot.Tipo=0, mot.RazaoSocial, mot.Nome) NO_MOTORISTA'
      'FROM vsinncab wic'
      'LEFT JOIN entidades ent ON ent.Codigo=wic.Empresa'
      'LEFT JOIN entidades frn ON frn.Codigo=wic.Fornecedor'
      'LEFT JOIN entidades trn ON trn.Codigo=wic.Transporta'
      'LEFT JOIN entidades cli ON cli.Codigo=wic.ClienteMO'
      'LEFT JOIN entidades prc ON prc.Codigo=wic.Procednc'
      'LEFT JOIN entidades mot ON mot.Codigo=wic.Motorista'
      ''
      'WHERE wic.Codigo > 0')
    Left = 684
    Top = 1
    object QrVSPlCCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPlCCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSPlCCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPlCCabDtCompra: TDateTimeField
      FieldName = 'DtCompra'
    end
    object QrVSPlCCabDtViagem: TDateTimeField
      FieldName = 'DtViagem'
    end
    object QrVSPlCCabDtEntrada: TDateTimeField
      FieldName = 'DtEntrada'
    end
    object QrVSPlCCabFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrVSPlCCabTransporta: TIntegerField
      FieldName = 'Transporta'
    end
    object QrVSPlCCabPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPlCCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPlCCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPlCCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPlCCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPlCCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPlCCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPlCCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPlCCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPlCCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPlCCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPlCCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPlCCabNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSPlCCabNO_TRANSPORTA: TWideStringField
      FieldName = 'NO_TRANSPORTA'
      Size = 100
    end
    object QrVSPlCCabValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPlCCabClienteMO: TIntegerField
      FieldName = 'ClienteMO'
    end
    object QrVSPlCCabProcednc: TIntegerField
      FieldName = 'Procednc'
    end
    object QrVSPlCCabMotorista: TIntegerField
      FieldName = 'Motorista'
    end
    object QrVSPlCCabPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrVSPlCCabNO_CLIENTEMO: TWideStringField
      FieldName = 'NO_CLIENTEMO'
      Size = 100
    end
    object QrVSPlCCabNO_PROCEDNC: TWideStringField
      FieldName = 'NO_PROCEDNC'
      Size = 100
    end
    object QrVSPlCCabNO_MOTORISTA: TWideStringField
      FieldName = 'NO_MOTORISTA'
      Size = 100
    end
    object QrVSPlCCabTemIMEIMrt: TIntegerField
      FieldName = 'TemIMEIMrt'
    end
    object QrVSPlCCabide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrVSPlCCabide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrVSPlCCabemi_serie: TIntegerField
      FieldName = 'emi_serie'
    end
    object QrVSPlCCabemi_nNF: TIntegerField
      FieldName = 'emi_nNF'
    end
    object QrVSPlCCabNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
    end
    object QrVSPlCCabUF_EMPRESA: TSmallintField
      FieldName = 'UF_EMPRESA'
    end
    object QrVSPlCCabUF_FORNECE: TSmallintField
      FieldName = 'UF_FORNECE'
    end
  end
  object DsVSPlCCab: TDataSource
    DataSet = QrVSPlCCab
    Left = 688
    Top = 45
  end
  object QrVSPlCIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPlCItsBeforeClose
    AfterScroll = QrVSPlCItsAfterScroll
    SQL.Strings = (
      'SELECT wmi.*, vsf.Nome NO_SerieFch, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,'
      'IF(wmi.SdoVrtPeca > 0, 0,'
      '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2,'
      'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT('
      '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3),'
      '  ",", ""), ".", ",")) RendKgm2_TXT,'
      'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE('
      'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT,'
      'IF(wmi.Misturou = 1, "SIM", "N'#195'O") Misturou_TXT,'
      'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,'
      'IF(wmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT('
      '  IF(wmi.Pecas > 0, wmi.QtdGerArM2 / wmi.Pecas, 0), 3),'
      '  ",", ""), ".", ",")) m2_CouroTXT,'
      'IF(wmi.Pecas <> 0, wmi.PesoKg / wmi.Pecas, 0) KgMedioCouro'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet'
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=wmi.SerieFch'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed'
      'WHERE wmi.MovimCod=0'
      'ORDER BY NO_Pallet, wmi.Controle')
    Left = 768
    Top = 1
    object QrVSPlCItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVSPlCItsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrVSPlCItsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrVSPlCItsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrVSPlCItsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrVSPlCItsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrVSPlCItsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrVSPlCItsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrVSPlCItsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrVSPlCItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrVSPlCItsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrVSPlCItsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrVSPlCItsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPlCItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPlCItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPlCItsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPlCItsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPlCItsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrVSPlCItsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrVSPlCItsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrVSPlCItsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrVSPlCItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSPlCItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPlCItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPlCItsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSPlCItsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrVSPlCItsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrVSPlCItsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrVSPlCItsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrVSPlCItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPlCItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPlCItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPlCItsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPlCItsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrVSPlCItsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrVSPlCItsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrVSPlCItsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrVSPlCItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSPlCItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPlCItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPlCItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPlCItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSPlCItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSPlCItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPlCItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSPlCItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSPlCItsPedItsFin: TLargeintField
      FieldName = 'PedItsFin'
    end
    object QrVSPlCItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSPlCItsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrVSPlCItsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSPlCItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPlCItsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrVSPlCItsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrVSPlCItsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrVSPlCItsRendKgm2: TFloatField
      FieldName = 'RendKgm2'
      DisplayFormat = '0.000'
    end
    object QrVSPlCItsNotaMPAG_TXT: TWideStringField
      FieldName = 'NotaMPAG_TXT'
      Size = 30
    end
    object QrVSPlCItsRendKgm2_TXT: TWideStringField
      FieldName = 'RendKgm2_TXT'
      Size = 30
    end
    object QrVSPlCItsMisturou_TXT: TWideStringField
      FieldName = 'Misturou_TXT'
      Size = 3
    end
    object QrVSPlCItsNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrVSPlCItsSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrVSPlCItsm2_CouroTXT: TWideStringField
      FieldName = 'm2_CouroTXT'
      Size = 30
    end
    object QrVSPlCItsKgMedioCouro: TFloatField
      FieldName = 'KgMedioCouro'
      DisplayFormat = '0.000'
    end
    object QrVSPlCItsSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrVSPlCItsClientMO: TLargeintField
      FieldName = 'ClientMO'
    end
    object QrVSPlCItsGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrVSPlCItsCustoM2: TFloatField
      FieldName = 'CustoM2'
    end
  end
  object DsVSPlCIts: TDataSource
    DataSet = QrVSPlCIts
    Left = 772
    Top = 45
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 440
    Top = 576
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Atualizaestoque1: TMenuItem
      Caption = 'Atuali&za estoque'
      OnClick = Atualizaestoque1Click
    end
    object Histrico1: TMenuItem
      Caption = '&Hist'#243'rico'
      object Inclui1: TMenuItem
        Caption = '&Inclui'
        OnClick = Inclui1Click
      end
      object Altera1: TMenuItem
        Caption = '&Altera'
        OnClick = Altera1Click
      end
      object Exclui1: TMenuItem
        Caption = '&Exclui'
        OnClick = Exclui1Click
      end
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 336
    Top = 568
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrFornecedor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'ORDER BY NOMEENTIDADE')
    Left = 844
    object QrFornecedorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFornecedorNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFornecedor: TDataSource
    DataSet = QrFornecedor
    Left = 844
    Top = 44
  end
  object QrTransporta: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece3="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 924
    Top = 4
    object QrTransportaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTransportaNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTransporta: TDataSource
    DataSet = QrTransporta
    Left = 924
    Top = 48
  end
  object PMImprime: TPopupMenu
    Left = 12
    Top = 4
    object FichadePallets1: TMenuItem
      Caption = 'Ficha do pallet selecionado'
      object FichaCOMnomedoPallet1: TMenuItem
        Caption = 'Ficha &COM nome do Pallet'
        OnClick = FichaCOMnomedoPallet1Click
      end
      object FichaSEMnomedoPallet1: TMenuItem
        Caption = 'Ficha &SEM nome do Pallet'
        OnClick = FichaSEMnomedoPallet1Click
      end
    end
    object Fichadetodospalletsdestacompra1: TMenuItem
      Caption = 'Ficha de todos pallets desta compra'
      object FichasCOMnomedoPallet1: TMenuItem
        Caption = 'Fichas &COM nome do Pallet'
        OnClick = FichasCOMnomedoPallet1Click
      end
      object FichasSEMnomedoPallet1: TMenuItem
        Caption = 'Fichas &SEM nome do Pallet'
        OnClick = FichasSEMnomedoPallet1Click
      end
    end
    object Rendimentodesemiacabado1: TMenuItem
      Caption = 'Rendimento de semi acabado'
      OnClick = Rendimentodesemiacabado1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Estoque1: TMenuItem
      Caption = '&Estoque (mostrar'#225' outra janela)'
      OnClick = Estoque1Click
    end
  end
  object frxDsVSPlCCab: TfrxDBDataset
    UserName = 'frxDsVSPlCCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'DtCompra=DtCompra'
      'DtViagem=DtViagem'
      'DtEntrada=DtEntrada'
      'Fornecedor=Fornecedor'
      'Transporta=Transporta'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_EMPRESA=NO_EMPRESA'
      'NO_FORNECE=NO_FORNECE'
      'NO_TRANSPORTA=NO_TRANSPORTA'
      'ValorT=ValorT')
    DataSet = QrVSPlCCab
    BCDToCurrency = False
    DataSetOptions = []
    Left = 688
    Top = 92
  end
  object frxDsVSPlCIts: TfrxDBDataset
    UserName = 'frxDsVSPlCIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'MovimCod=MovimCod'
      'Empresa=Empresa'
      'MovimID=MovimID'
      'GraGruX=GraGruX'
      'Pecas=Pecas'
      'PesoKg=PesoKg'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'SrcMovID=SrcMovID'
      'SrcNivel1=SrcNivel1'
      'SrcNivel2=SrcNivel2'
      'Pallet=Pallet'
      'NO_PALLET=NO_PALLET'
      'SdoVrtArM2=SdoVrtArM2'
      'SdoVrtPeca=SdoVrtPeca'
      'Observ=Observ'
      'ValorT=ValorT'
      'MovimTwn=MovimTwn'
      'Misturou=Misturou'
      'Ficha=Ficha'
      'SerieFch=SerieFch'
      'NO_SerieFch=NO_SerieFch'
      'NotaMPAG=NotaMPAG'
      'RendKgm2=RendKgm2'
      'NotaMPAG_TXT=NotaMPAG_TXT'
      'RendKgm2_TXT=RendKgm2_TXT'
      'MovimNiv=MovimNiv'
      'Terceiro=Terceiro'
      'CliVenda=CliVenda'
      'LnkNivXtr1=LnkNivXtr1'
      'LnkNivXtr2=LnkNivXtr2'
      'DataHora=DataHora'
      'SdoVrtPeso=SdoVrtPeso'
      'FornecMO=FornecMO'
      'CustoMOKg=CustoMOKg'
      'CustoMOTot=CustoMOTot'
      'ValorMP=ValorMP'
      'DstMovID=DstMovID'
      'DstNivel1=DstNivel1'
      'DstNivel2=DstNivel2'
      'QtdGerPeca=QtdGerPeca'
      'QtdGerPeso=QtdGerPeso'
      'QtdGerArM2=QtdGerArM2'
      'QtdGerArP2=QtdGerArP2'
      'QtdAntPeca=QtdAntPeca'
      'QtdAntPeso=QtdAntPeso'
      'QtdAntArM2=QtdAntArM2'
      'QtdAntArP2=QtdAntArP2'
      'AptoUso=AptoUso'
      'SrcGGX=SrcGGX'
      'DstGGX=DstGGX'
      'Misturou_TXT=Misturou_TXT'
      'NOMEUNIDMED=NOMEUNIDMED'
      'SIGLAUNIDMED=SIGLAUNIDMED'
      'm2_CouroTXT=m2_CouroTXT'
      'KgMedioCouro=KgMedioCouro')
    DataSet = QrVSPlCIts
    BCDToCurrency = False
    DataSetOptions = []
    Left = 772
    Top = 92
  end
  object QrClienteMO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 684
    Top = 464
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClienteMO: TDataSource
    DataSet = QrClienteMO
    Left = 684
    Top = 512
  end
  object QrProcednc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 764
    Top = 464
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsProcednc: TDataSource
    DataSet = QrProcednc
    Left = 764
    Top = 512
  end
  object QrMotorista: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece4="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 616
    Top = 464
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsMotorista: TDataSource
    DataSet = QrMotorista
    Left = 616
    Top = 512
  end
  object QrVSMovDif: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM vsmovdif'
      'WHERE Controle=1')
    Left = 772
    Top = 144
    object QrVSMovDifControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovDifInfPecas: TFloatField
      FieldName = 'InfPecas'
    end
    object QrVSMovDifInfPesoKg: TFloatField
      FieldName = 'InfPesoKg'
    end
    object QrVSMovDifInfAreaM2: TFloatField
      FieldName = 'InfAreaM2'
    end
    object QrVSMovDifInfAreaP2: TFloatField
      FieldName = 'InfAreaP2'
    end
    object QrVSMovDifInfValorT: TFloatField
      FieldName = 'InfValorT'
    end
    object QrVSMovDifDifPecas: TFloatField
      FieldName = 'DifPecas'
    end
    object QrVSMovDifDifPesoKg: TFloatField
      FieldName = 'DifPesoKg'
    end
    object QrVSMovDifDifAreaM2: TFloatField
      FieldName = 'DifAreaM2'
    end
    object QrVSMovDifDifAreaP2: TFloatField
      FieldName = 'DifAreaP2'
    end
    object QrVSMovDifDifValorT: TFloatField
      FieldName = 'DifValorT'
    end
    object QrVSMovDifLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMovDifDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMovDifDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMovDifUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMovDifUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMovDifAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMovDifAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrVSHisFch: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vshisfch '
      'WHERE VSMovIts=0 '
      'OR ( '
      '  SerieFch=0 '
      '  AND '
      '  Ficha=0 '
      ') ')
    Left = 404
    Top = 336
    object QrVSHisFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSHisFchVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrVSHisFchSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSHisFchFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSHisFchDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSHisFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSHisFchObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSHisFchLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSHisFchDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSHisFchDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSHisFchUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSHisFchUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSHisFchAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSHisFchAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsVSHisFch: TDataSource
    DataSet = QrVSHisFch
    Left = 404
    Top = 384
  end
  object PMVPlcIts: TPopupMenu
    Left = 132
    Top = 368
    object IrparajaneladegerenciamentodeFichaRMP1: TMenuItem
      Caption = '&Ir para janela de gerenciamento de Ficha RMP'
      OnClick = IrparajaneladegerenciamentodeFichaRMP1Click
    end
  end
  object QrGraGru1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CST_B, IPI_CST, PIS_CST, COFINS_CST, '
      'ICMSRec_pAliq, IPIRec_pAliq, PISRec_pAliq,  '
      'COFINSRec_pAliq  '
      'FROM gragru1 '
      'WHERE Nivel1>0')
    Left = 896
    Top = 392
    object QrGraGru1CST_B: TSmallintField
      FieldName = 'CST_B'
      Required = True
    end
    object QrGraGru1IPI_CST: TSmallintField
      FieldName = 'IPI_CST'
      Required = True
    end
    object QrGraGru1PIS_CST: TSmallintField
      FieldName = 'PIS_CST'
      Required = True
    end
    object QrGraGru1COFINS_CST: TSmallintField
      FieldName = 'COFINS_CST'
      Required = True
    end
    object QrGraGru1ICMSRec_pAliq: TFloatField
      FieldName = 'ICMSRec_pAliq'
      Required = True
    end
    object QrGraGru1IPIRec_pAliq: TFloatField
      FieldName = 'IPIRec_pAliq'
      Required = True
    end
    object QrGraGru1PISRec_pAliq: TFloatField
      FieldName = 'PISRec_pAliq'
      Required = True
    end
    object QrGraGru1COFINSRec_pAliq: TFloatField
      FieldName = 'COFINSRec_pAliq'
      Required = True
    end
  end
  object PMFiscal: TPopupMenu
    OnPopup = PMFiscalPopup
    Left = 544
    Top = 574
    object IncluiDocumento1: TMenuItem
      Caption = '&Inclui Documento'
      OnClick = IncluiDocumento1Click
    end
    object AlteraDocumento1: TMenuItem
      Caption = '&Altera Documento'
      OnClick = AlteraDocumento1Click
    end
    object ExcluiDocumento1: TMenuItem
      Caption = '&Exclui Documento'
      OnClick = ExcluiDocumento1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object IncluiItemdodocumento1: TMenuItem
      Caption = 'Inclui Item do documento'
      OnClick = IncluiItemdodocumento1Click
    end
    object AlteraoItemselecionadododocumento1: TMenuItem
      Caption = 'Altera o  Item selecionado do documento'
      OnClick = AlteraoItemselecionadododocumento1Click
    end
    object ExcluioItemselecionadododocumento1: TMenuItem
      Caption = 'Exclui o  Item selecionado do documento'
      OnClick = ExcluioItemselecionadododocumento1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object otalizarfisicopeloescritural1: TMenuItem
      Caption = '&Totalizar fisico pelo escritural'
      Visible = False
    end
  end
  object QrEfdInnNFsCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEfdInnNFsCabBeforeClose
    AfterScroll = QrEfdInnNFsCabAfterScroll
    SQL.Strings = (
      'SELECT vin.*, '
      'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  '
      'FROM efdinnnfscab vin '
      'LEFT JOIN entidades ter ON ter.Codigo=vin.Terceiro '
      ' ')
    Left = 844
    Top = 290
    object QrEfdInnNFsCabNO_TER: TWideStringField
      FieldName = 'NO_TER'
      Size = 100
    end
    object QrEfdInnNFsCabMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrEfdInnNFsCabMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrEfdInnNFsCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrEfdInnNFsCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEfdInnNFsCabControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEfdInnNFsCabTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrEfdInnNFsCabPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrEfdInnNFsCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrEfdInnNFsCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrEfdInnNFsCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
    end
    object QrEfdInnNFsCabValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrEfdInnNFsCabMotorista: TIntegerField
      FieldName = 'Motorista'
      Required = True
    end
    object QrEfdInnNFsCabPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrEfdInnNFsCabCOD_MOD: TSmallintField
      FieldName = 'COD_MOD'
      Required = True
    end
    object QrEfdInnNFsCabCOD_SIT: TSmallintField
      FieldName = 'COD_SIT'
      Required = True
    end
    object QrEfdInnNFsCabSER: TIntegerField
      FieldName = 'SER'
      Required = True
    end
    object QrEfdInnNFsCabNUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Required = True
    end
    object QrEfdInnNFsCabCHV_NFE: TWideStringField
      FieldName = 'CHV_NFE'
      Size = 44
    end
    object QrEfdInnNFsCabNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
      Required = True
    end
    object QrEfdInnNFsCabDT_DOC: TDateField
      FieldName = 'DT_DOC'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEfdInnNFsCabDT_E_S: TDateField
      FieldName = 'DT_E_S'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEfdInnNFsCabVL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabIND_PGTO: TWideStringField
      FieldName = 'IND_PGTO'
      Size = 1
    end
    object QrEfdInnNFsCabVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_MERC: TFloatField
      FieldName = 'VL_MERC'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabIND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Size = 1
    end
    object QrEfdInnNFsCabVL_FRT: TFloatField
      FieldName = 'VL_FRT'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_SEG: TFloatField
      FieldName = 'VL_SEG'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_OUT_DA: TFloatField
      FieldName = 'VL_OUT_DA'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_IPI: TFloatField
      FieldName = 'VL_IPI'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_PIS_ST: TFloatField
      FieldName = 'VL_PIS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabVL_COFINS_ST: TFloatField
      FieldName = 'VL_COFINS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsCabNFe_FatID: TIntegerField
      FieldName = 'NFe_FatID'
      Required = True
    end
    object QrEfdInnNFsCabNFe_FatNum: TIntegerField
      FieldName = 'NFe_FatNum'
      Required = True
    end
    object QrEfdInnNFsCabNFe_StaLnk: TSmallintField
      FieldName = 'NFe_StaLnk'
      Required = True
    end
    object QrEfdInnNFsCabVSVmcWrn: TSmallintField
      FieldName = 'VSVmcWrn'
      Required = True
    end
    object QrEfdInnNFsCabVSVmcObs: TWideStringField
      FieldName = 'VSVmcObs'
      Size = 60
    end
    object QrEfdInnNFsCabVSVmcSeq: TWideStringField
      FieldName = 'VSVmcSeq'
      Size = 25
    end
    object QrEfdInnNFsCabVSVmcSta: TSmallintField
      FieldName = 'VSVmcSta'
      Required = True
    end
    object QrEfdInnNFsCabRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
    end
    object QrEfdInnNFsCabCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrEfdInnNFsCabTpEntrd: TIntegerField
      FieldName = 'TpEntrd'
    end
  end
  object DsEfdInnNFsCab: TDataSource
    DataSet = QrEfdInnNFsCab
    Left = 844
    Top = 340
  end
  object QrEfdInnNFsIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vin.*, ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,  '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, gg1.GerBxaEstq, '
      'gg1.NCM, gg1.UnidMed, gg1.Ex_TIPI, unm.Grandeza, '
      'IF(gg2.Tipo_Item >= 0, gg2.Tipo_Item, '
      'pgt.Tipo_Item) Tipo_Item '
      'FROM efdinnnfsits     vin'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vin.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN gragru2    gg2 ON gg2.Nivel2=gg1.Nivel2'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdgrupTip'
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ')
    Left = 956
    Top = 286
    object QrEfdInnNFsItsMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrEfdInnNFsItsMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrEfdInnNFsItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrEfdInnNFsItsEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEfdInnNFsItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrEfdInnNFsItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrEfdInnNFsItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEfdInnNFsItsConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEfdInnNFsItsGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Required = True
    end
    object QrEfdInnNFsItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrEfdInnNFsItsprod_vProd: TFloatField
      FieldName = 'prod_vProd'
      Required = True
    end
    object QrEfdInnNFsItsprod_vFrete: TFloatField
      FieldName = 'prod_vFrete'
      Required = True
    end
    object QrEfdInnNFsItsprod_vSeg: TFloatField
      FieldName = 'prod_vSeg'
      Required = True
    end
    object QrEfdInnNFsItsprod_vOutro: TFloatField
      FieldName = 'prod_vOutro'
      Required = True
    end
    object QrEfdInnNFsItsQTD: TFloatField
      FieldName = 'QTD'
      Required = True
    end
    object QrEfdInnNFsItsUNID: TWideStringField
      FieldName = 'UNID'
      Size = 6
    end
    object QrEfdInnNFsItsVL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsIND_MOV: TWideStringField
      FieldName = 'IND_MOV'
      Size = 1
    end
    object QrEfdInnNFsItsCFOP: TIntegerField
      FieldName = 'CFOP'
    end
    object QrEfdInnNFsItsCOD_NAT: TWideStringField
      FieldName = 'COD_NAT'
      Size = 10
    end
    object QrEfdInnNFsItsVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_BC_ICMS_ST: TFloatField
      FieldName = 'VL_BC_ICMS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsALIQ_ST: TFloatField
      FieldName = 'ALIQ_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_ICMS_ST: TFloatField
      FieldName = 'VL_ICMS_ST'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsIND_APUR: TWideStringField
      FieldName = 'IND_APUR'
      Size = 1
    end
    object QrEfdInnNFsItsCOD_ENQ: TWideStringField
      FieldName = 'COD_ENQ'
      Size = 3
    end
    object QrEfdInnNFsItsVL_BC_IPI: TFloatField
      FieldName = 'VL_BC_IPI'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsALIQ_IPI: TFloatField
      FieldName = 'ALIQ_IPI'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_IPI: TFloatField
      FieldName = 'VL_IPI'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_BC_PIS: TFloatField
      FieldName = 'VL_BC_PIS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsALIQ_PIS_p: TFloatField
      FieldName = 'ALIQ_PIS_p'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsQUANT_BC_PIS: TFloatField
      FieldName = 'QUANT_BC_PIS'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_PIS_r: TFloatField
      FieldName = 'ALIQ_PIS_r'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_BC_COFINS: TFloatField
      FieldName = 'VL_BC_COFINS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsALIQ_COFINS_p: TFloatField
      FieldName = 'ALIQ_COFINS_p'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsQUANT_BC_COFINS: TFloatField
      FieldName = 'QUANT_BC_COFINS'
      Required = True
    end
    object QrEfdInnNFsItsALIQ_COFINS_r: TFloatField
      FieldName = 'ALIQ_COFINS_r'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsCOD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 255
    end
    object QrEfdInnNFsItsVL_ABAT_NT: TFloatField
      FieldName = 'VL_ABAT_NT'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrEfdInnNFsItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
      Required = True
    end
    object QrEfdInnNFsItsxLote: TWideStringField
      FieldName = 'xLote'
    end
    object QrEfdInnNFsItsOri_IPIpIPI: TFloatField
      FieldName = 'Ori_IPIpIPI'
      Required = True
    end
    object QrEfdInnNFsItsOri_IPIvIPI: TFloatField
      FieldName = 'Ori_IPIvIPI'
      Required = True
    end
    object QrEfdInnNFsItsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrEfdInnNFsItsSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrEfdInnNFsItsGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrEfdInnNFsItsNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrEfdInnNFsItsUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrEfdInnNFsItsEx_TIPI: TWideStringField
      FieldName = 'Ex_TIPI'
      Size = 3
    end
    object QrEfdInnNFsItsGrandeza: TSmallintField
      FieldName = 'Grandeza'
    end
    object QrEfdInnNFsItsTipo_Item: TSmallintField
      FieldName = 'Tipo_Item'
    end
    object QrEfdInnNFsItsCST_IPI: TWideStringField
      FieldName = 'CST_IPI'
      Size = 2
    end
    object QrEfdInnNFsItsCST_PIS: TWideStringField
      FieldName = 'CST_PIS'
      Size = 2
    end
    object QrEfdInnNFsItsCST_COFINS: TWideStringField
      FieldName = 'CST_COFINS'
      Size = 2
    end
    object QrEfdInnNFsItsCST_ICMS: TWideStringField
      FieldName = 'CST_ICMS'
      Size = 3
    end
  end
  object DsEfdInnNFsIts: TDataSource
    DataSet = QrEfdInnNFsIts
    Left = 956
    Top = 336
  end
  object QrEfdInnCTsCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vic.*, '
      'IF(ter.Tipo=0, ter.RazaoSocial, ter.Nome) NO_TER  '
      'FROM efdinnctscab vic '
      'LEFT JOIN entidades ter ON ter.Codigo=vic.Terceiro ')
    Left = 718
    Top = 295
    object QrEfdInnCTsCabMovFatID: TIntegerField
      FieldName = 'MovFatID'
      Required = True
    end
    object QrEfdInnCTsCabMovFatNum: TIntegerField
      FieldName = 'MovFatNum'
      Required = True
    end
    object QrEfdInnCTsCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrEfdInnCTsCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrEfdInnCTsCabControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEfdInnCTsCabIsLinked: TSmallintField
      FieldName = 'IsLinked'
      Required = True
    end
    object QrEfdInnCTsCabSqLinked: TIntegerField
      FieldName = 'SqLinked'
      Required = True
    end
    object QrEfdInnCTsCabCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrEfdInnCTsCabTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrEfdInnCTsCabPecas: TFloatField
      FieldName = 'Pecas'
      Required = True
    end
    object QrEfdInnCTsCabPesoKg: TFloatField
      FieldName = 'PesoKg'
      Required = True
    end
    object QrEfdInnCTsCabAreaM2: TFloatField
      FieldName = 'AreaM2'
      Required = True
    end
    object QrEfdInnCTsCabAreaP2: TFloatField
      FieldName = 'AreaP2'
      Required = True
    end
    object QrEfdInnCTsCabValorT: TFloatField
      FieldName = 'ValorT'
      Required = True
    end
    object QrEfdInnCTsCabMotorista: TIntegerField
      FieldName = 'Motorista'
      Required = True
    end
    object QrEfdInnCTsCabPlaca: TWideStringField
      FieldName = 'Placa'
    end
    object QrEfdInnCTsCabIND_OPER: TWideStringField
      FieldName = 'IND_OPER'
      Required = True
      Size = 1
    end
    object QrEfdInnCTsCabIND_EMIT: TWideStringField
      FieldName = 'IND_EMIT'
      Required = True
      Size = 1
    end
    object QrEfdInnCTsCabCOD_MOD: TWideStringField
      FieldName = 'COD_MOD'
      Required = True
      Size = 2
    end
    object QrEfdInnCTsCabCOD_SIT: TSmallintField
      FieldName = 'COD_SIT'
      Required = True
    end
    object QrEfdInnCTsCabSER: TWideStringField
      FieldName = 'SER'
      Size = 4
    end
    object QrEfdInnCTsCabSUB: TWideStringField
      FieldName = 'SUB'
      Size = 3
    end
    object QrEfdInnCTsCabNUM_DOC: TIntegerField
      FieldName = 'NUM_DOC'
      Required = True
    end
    object QrEfdInnCTsCabCHV_CTE: TWideStringField
      FieldName = 'CHV_CTE'
      Size = 44
    end
    object QrEfdInnCTsCabCTeStatus: TIntegerField
      FieldName = 'CTeStatus'
      Required = True
    end
    object QrEfdInnCTsCabDT_DOC: TDateField
      FieldName = 'DT_DOC'
      Required = True
    end
    object QrEfdInnCTsCabDT_A_P: TDateField
      FieldName = 'DT_A_P'
      Required = True
    end
    object QrEfdInnCTsCabTP_CT_e: TSmallintField
      FieldName = 'TP_CT_e'
      Required = True
    end
    object QrEfdInnCTsCabCHV_CTE_REF: TWideStringField
      FieldName = 'CHV_CTE_REF'
      Size = 44
    end
    object QrEfdInnCTsCabVL_DOC: TFloatField
      FieldName = 'VL_DOC'
      Required = True
    end
    object QrEfdInnCTsCabVL_DESC: TFloatField
      FieldName = 'VL_DESC'
      Required = True
    end
    object QrEfdInnCTsCabIND_FRT: TWideStringField
      FieldName = 'IND_FRT'
      Size = 1
    end
    object QrEfdInnCTsCabVL_SERV: TFloatField
      FieldName = 'VL_SERV'
      Required = True
    end
    object QrEfdInnCTsCabVL_BC_ICMS: TFloatField
      FieldName = 'VL_BC_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabVL_ICMS: TFloatField
      FieldName = 'VL_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabVL_NT: TFloatField
      FieldName = 'VL_NT'
      Required = True
    end
    object QrEfdInnCTsCabCOD_INF: TWideStringField
      FieldName = 'COD_INF'
      Size = 6
    end
    object QrEfdInnCTsCabCOD_CTA: TWideStringField
      FieldName = 'COD_CTA'
      Size = 30
    end
    object QrEfdInnCTsCabCOD_MUN_ORIG: TIntegerField
      FieldName = 'COD_MUN_ORIG'
      Required = True
    end
    object QrEfdInnCTsCabCOD_MUN_DEST: TIntegerField
      FieldName = 'COD_MUN_DEST'
      Required = True
    end
    object QrEfdInnCTsCabNFe_FatID: TIntegerField
      FieldName = 'NFe_FatID'
      Required = True
    end
    object QrEfdInnCTsCabNFe_FatNum: TIntegerField
      FieldName = 'NFe_FatNum'
      Required = True
    end
    object QrEfdInnCTsCabNFe_StaLnk: TSmallintField
      FieldName = 'NFe_StaLnk'
      Required = True
    end
    object QrEfdInnCTsCabVSVmcWrn: TSmallintField
      FieldName = 'VSVmcWrn'
      Required = True
    end
    object QrEfdInnCTsCabVSVmcObs: TWideStringField
      FieldName = 'VSVmcObs'
      Size = 60
    end
    object QrEfdInnCTsCabVSVmcSeq: TWideStringField
      FieldName = 'VSVmcSeq'
      Size = 25
    end
    object QrEfdInnCTsCabVSVmcSta: TSmallintField
      FieldName = 'VSVmcSta'
      Required = True
    end
    object QrEfdInnCTsCabCST_ICMS: TIntegerField
      FieldName = 'CST_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabCFOP: TIntegerField
      FieldName = 'CFOP'
      Required = True
    end
    object QrEfdInnCTsCabALIQ_ICMS: TFloatField
      FieldName = 'ALIQ_ICMS'
      Required = True
    end
    object QrEfdInnCTsCabVL_OPR: TFloatField
      FieldName = 'VL_OPR'
      Required = True
    end
    object QrEfdInnCTsCabVL_RED_BC: TFloatField
      FieldName = 'VL_RED_BC'
      Required = True
    end
    object QrEfdInnCTsCabCOD_OBS: TWideStringField
      FieldName = 'COD_OBS'
      Size = 6
    end
    object QrEfdInnCTsCabNO_TER: TWideStringField
      FieldName = 'NO_TER'
      Size = 100
    end
    object QrEfdInnCTsCabRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
    end
    object QrEfdInnCTsCabIND_NAT_FRT: TWideStringField
      FieldName = 'IND_NAT_FRT'
      Required = True
      Size = 1
    end
    object QrEfdInnCTsCabVL_ITEM: TFloatField
      FieldName = 'VL_ITEM'
      Required = True
    end
    object QrEfdInnCTsCabCST_PIS: TWideStringField
      FieldName = 'CST_PIS'
      Size = 2
    end
    object QrEfdInnCTsCabNAT_BC_CRED: TWideStringField
      FieldName = 'NAT_BC_CRED'
      Required = True
      Size = 2
    end
    object QrEfdInnCTsCabVL_BC_PIS: TFloatField
      FieldName = 'VL_BC_PIS'
      Required = True
    end
    object QrEfdInnCTsCabALIQ_PIS: TFloatField
      FieldName = 'ALIQ_PIS'
      Required = True
    end
    object QrEfdInnCTsCabVL_PIS: TFloatField
      FieldName = 'VL_PIS'
      Required = True
    end
    object QrEfdInnCTsCabCST_COFINS: TWideStringField
      FieldName = 'CST_COFINS'
      Size = 2
    end
    object QrEfdInnCTsCabVL_BC_COFINS: TFloatField
      FieldName = 'VL_BC_COFINS'
      Required = True
    end
    object QrEfdInnCTsCabALIQ_COFINS: TFloatField
      FieldName = 'ALIQ_COFINS'
      Required = True
    end
    object QrEfdInnCTsCabVL_COFINS: TFloatField
      FieldName = 'VL_COFINS'
      Required = True
    end
  end
  object DsEfdInnCTsCab: TDataSource
    DataSet = QrEfdInnCTsCab
    Left = 718
    Top = 348
  end
  object PMCTe: TPopupMenu
    OnPopup = PMCTePopup
    Left = 644
    Top = 579
    object IncluiConhecimentodefrete1: TMenuItem
      Caption = '&Inclui Conhecimento de frete'
      OnClick = IncluiConhecimentodefrete1Click
    end
    object AlteraConhecimentodefrete1: TMenuItem
      Caption = '&Altera Conhecimento de frete'
      OnClick = AlteraConhecimentodefrete1Click
    end
    object ExcluiConhecimentodefrete1: TMenuItem
      Caption = '&Exclui Conhecimento de frete'
      OnClick = ExcluiConhecimentodefrete1Click
    end
  end
end
