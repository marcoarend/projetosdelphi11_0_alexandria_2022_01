unit VSPaCRIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGridZTO, mySQLDbTables,
  DmkDAC_PF, Vcl.Menus;

type
  TFmVSPaCRIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DBGVSPaCRIts: TdmkDBGridZTO;
    QrVSPaCRIts: TmySQLQuery;
    QrVSPaCRItsClaOuRcl: TFloatField;
    QrVSPaCRItsCodigo: TIntegerField;
    QrVSPaCRItsControle: TIntegerField;
    QrVSPaCRItsVSPallet: TIntegerField;
    QrVSPaCRItsVMI_Sorc: TIntegerField;
    QrVSPaCRItsVMI_Baix: TIntegerField;
    QrVSPaCRItsVMI_Dest: TIntegerField;
    QrVSPaCRItsTecla: TIntegerField;
    QrVSPaCRItsDtHrIni: TDateTimeField;
    QrVSPaCRItsDtHrFim: TDateTimeField;
    QrVSPaCRItsQIt_Sorc: TIntegerField;
    QrVSPaCRItsQIt_Baix: TIntegerField;
    DsVSPaCRIts: TDataSource;
    QrVSPaCRItsNO_ClaOuRcl: TWideStringField;
    BtEncerra: TBitBtn;
    QrRcl: TmySQLQuery;
    QrRclCodigo: TIntegerField;
    QrRclCacCod: TIntegerField;
    QrRclVSPallet: TIntegerField;
    QrRclMontPalt: TIntegerField;
    QrCla: TmySQLQuery;
    QrClaCodigo: TIntegerField;
    QrClaCacCod: TIntegerField;
    QrClaVSGerArt: TIntegerField;
    QrClaMontPalt: TIntegerField;
    QrVSPaCRItsEstahNaOC: TFloatField;
    QrVSPaCRItsCacCod: TIntegerField;
    PMEncerra: TPopupMenu;
    Encerraitematual1: TMenuItem;
    EncerraTodositenspossiveis1: TMenuItem;
    N1: TMenuItem;
    Atualizatodosboxes1: TMenuItem;
    QrSumDest1: TmySQLQuery;
    QrSumSorc1: TmySQLQuery;
    QrVMISorc1: TmySQLQuery;
    QrPalSorc1: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure DBGVSPaCRItsDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Encerraitematual1Click(Sender: TObject);
    procedure EncerraTodositenspossiveis1Click(Sender: TObject);
    procedure Atualizatodosboxes1Click(Sender: TObject);
  private
    { Private declarations }
    procedure EncerraItemAtual(Pergunta, PodeReabrir: Boolean);
    //
    procedure ReopenVSPaCrIts(Controle: Integer);
  public
    { Public declarations }
  end;

  var
  FmVSPaCRIts: TFmVSPaCRIts;

implementation

uses UnMyObjects, Module, MyDBCheck, VSMod, ModuleGeral, UMySQLModule,
  UnVS_PF;

{$R *.DFM}

procedure TFmVSPaCRIts.Atualizatodosboxes1Click(Sender: TObject);
var
  VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    QrVSPaCRIts.DisableControls;
    try
      QrVSPaCRIts.First;
      while not QrVSPaCRIts.Eof do
      begin
        VSPallet := QrVSPaCRItsVSPallet.Value;
        VMI_Dest := QrVSPaCRItsVMI_Dest.Value;
        VMI_Baix := QrVSPaCRItsVMI_Baix.Value;
        VMI_Sorc := QrVSPaCRItsVMI_Sorc.Value;
        VS_PF.AtualizaVMIsDeBox(VSPallet, VMI_Dest, VMI_Baix, VMI_Sorc,
          QrSumDest1, QrSumSorc1, QrVMISorc1, QrPalSorc1);
        //
        QrVSPaCRIts.Next;
      end;
    finally
      QrVSPaCRIts.EnableControls;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  ReopenVSPaCRIts(0);
end;

procedure TFmVSPaCRIts.BtEncerraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEncerra, BtEncerra);
end;

procedure TFmVSPaCRIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSPaCRIts.DBGVSPaCRItsDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if QrVSPaCRItsEstahNaOC.Value = 0 then
    Cor := clBlack
  else
    Cor := clRed;
  with DBGVSPaCRIts.Canvas do
  begin
    if Cor = clBlack then Font.Style := [] else Font.Style := [fsBold];
    Font.Color := Cor;
    FillRect(Rect);
    TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
  end;
end;

procedure TFmVSPaCRIts.EncerraItemAtual(Pergunta, PodeReabrir: Boolean);
  function MontaSQLCla(Pallet, Codigo: Integer; Coluna, Tabela: String; Uniao: Boolean): String;
  begin
    Result := Geral.ATS([
    'SELECT Codigo, CacCod, VSGerArt, LstPal' + Coluna + ' MontPalt, 1 Ativo ',
    'FROM ' + TMeuDB + '.vspaclacaba ',
    'WHERE LstPal' + Coluna + ' <> 0 ',
    'AND DtHrFimCla < "1900-01-01" ',
    'AND LstPal' + Coluna + ' =' + Geral.FF0(Pallet),
    'AND Codigo=' + Geral.FF0(Codigo),
    '']);
    if Uniao then
      Result := Geral.ATS([Result, 'UNION ']);
  end;
  function MontaSQLRcl(Pallet, Codigo: Integer; Coluna, Tabela: String; Uniao: Boolean): String;
  begin
    Result := Geral.ATS([
    'SELECT Codigo, CacCod, VSPallet, LstPal' + Coluna + ' MontPalt, 1 Ativo ',
    'FROM ' + TMeuDB + '.vspaclacaba ',
    'WHERE LstPal' + Coluna + ' <> 0 ',
    'AND DtHrFimCla < "1900-01-01" ',
    'AND LstPal' + Coluna + ' =' + Geral.FF0(Pallet),
    'AND Codigo=' + Geral.FF0(Codigo),
    '']);
    if Uniao then
      Result := Geral.ATS([Result, 'UNION ']);
  end;
var
  MyCursor: TCursor;
  DtHrFim, Tabela: String;
  Pallet, Controle, Codigo: Integer;
  Reabre: Boolean;
begin
  Reabre := False;
  if (QrVSPaCRIts.State <> dsInactive) and (QrVSPaCRIts.RecordCount > 0) then
  begin
    if QrVSPaCRItsEstahNaOC.Value >= 0.001 then
    begin
      Geral.MB_Aviso('A��o abortada! Item ativo em OC!');
      Exit;
    end;
    Controle := QrVSPaCRItsControle.Value;
    if (Controle <> 0) then
    begin
      DtHrFim  := Geral.FDT(DmodG.ObtemAgora(), 109);
      Pallet   := QrVSPaCRItsVSPallet.Value;
      Codigo   := QrVSPaCRItsCodigo.Value;
      if (not Pergunta) or (Geral.MB_Pergunta(
      'Confirma a inclus�o de informa��o de data de encerramento?') = ID_YES) then
      begin
        MyCursor := Screen.Cursor;
        Screen.Cursor := crHourGlass;
        try
          case Trunc(QrVSPaCRItsClaOuRcl.Value) of
            1: // Classificacao
            begin
(*
              ///  Pallets em Montagem!
              Tabela := 'vspaclacaba';
              UnDmkDAC_PF.AbreMySQLQuery0(QrCla, Dmod.MyDB, [
              //'INSERT INTO  ' + VSLstPalBox,
              MontaSQLCla(Pallet, Codigo, '01', Tabela, True),
              MontaSQLCla(Pallet, Codigo, '02', Tabela, True),
              MontaSQLCla(Pallet, Codigo, '03', Tabela, True),
              MontaSQLCla(Pallet, Codigo, '04', Tabela, True),
              MontaSQLCla(Pallet, Codigo, '05', Tabela, True),
              MontaSQLCla(Pallet, Codigo, '06', Tabela, False),
              ' ']);
              if QrCla.RecordCount > 0 then
              begin
                Geral.MB_Aviso(
                'Pallet n�o pode ter hora de encerramento definido!' +
                sLineBreak +
                'Ele est� aberto na classifica��o:' + sLineBreak +
                'C�digo: ' + Geral.FF0(QrClaCodigo.Value) + sLineBreak +
                'OC: ' + Geral.FF0(QrClaCacCod.Value) + sLineBreak +
                'da Gera��o de artigo: ' + Geral.FF0(QrClaVSGerArt.Value));
                Exit;
              end;
              ///
*)
              Reabre := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'vspaclaitsa', False, [
              'DtHrFim'], ['Controle'], [DtHrFim], [Controle], True);
            end;
            2: //Reclassifcacao
            begin
(*
              ///  Pallets em Montagem!
              Tabela := 'vsparclcaba';
              UnDmkDAC_PF.AbreMySQLQuery0(QrRcl, Dmod.MyDB, [
              //'INSERT INTO  ' + VSLstPalBox,
              MontaSQLRcl(Pallet, Codigo, '01', Tabela, True),
              MontaSQLRcl(Pallet, Codigo, '02', Tabela, True),
              MontaSQLRcl(Pallet, Codigo, '03', Tabela, True),
              MontaSQLRcl(Pallet, Codigo, '04', Tabela, True),
              MontaSQLRcl(Pallet, Codigo, '05', Tabela, True),
              MontaSQLRcl(Pallet, Codigo, '06', Tabela, False),
              ' ']);
              if QrRcl.RecordCount > 0 then
              begin
                Geral.MB_Aviso(
                'Pallet n�o pode ter hora de encerramento definido!' +
                sLineBreak +
                'Ele est� aberto na reclassifica��o:' + sLineBreak +
                'C�digo: ' + Geral.FF0(QrRclCodigo.Value) + sLineBreak +
                'OC: ' + Geral.FF0(QrRclCacCod.Value) + sLineBreak +
                'do Pallet: ' + Geral.FF0(QrRclVSPallet.Value));
                Exit;
              end;
              ///
*)
              if DBCheck.CriaFm(TDfVSMod, DfVSMod, afmoSemVerificar) then
              begin
    (*
                Result := DfVSMod.EncerraPalletReclassificacao(VSPaRclCabCacCod,
                VSPaRclCabCodigo, VSPaRclCabVSPallet, Box_Box, Box_VSPaClaIts,
                Box_VSPallet, Box_VMI_Sorc, Box_VMI_Baix, Box_VMI_Dest, EncerrandoTodos);
                //
                ReabreVSPaRclCab := DfVSMod.FReabreVSPaRclCab;
    *)
                DfVSMod.EncerraVSPaRclItsA(DtHrFim, Controle);
                Reabre := True;
                //
                DfVSMod.Destroy;
              end;
            end;
            else
              Geral.MB_Erro(
                'Tipo de classifica��o n�o definido em "TFmVSPaCRIts.BtEncerraClick()"');
          end;
        finally
          Screen.Cursor := MyCursor;
        end;
      end;
      if Reabre and PodeReabrir then
      begin
        Controle := UMyMod.ProximoRegistro(QrVSPaCRIts, 'Controle', Controle);
        ReopenVSPaCRIts(Controle);
      end;
    end;
  end;
end;

procedure TFmVSPaCRIts.Encerraitematual1Click(Sender: TObject);
begin
  EncerraItemAtual(True, True);
end;

procedure TFmVSPaCRIts.EncerraTodositenspossiveis1Click(Sender: TObject);
begin
////////////////////////////////////////////////////////////////////////////////
///
///  Nao pode! Encerra sem calcular saldos!
  EXIT;
///
////////////////////////////////////////////////////////////////////////////////
(*
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  Screen.Cursor := crHourGlass;
  try
    QrVSPaCRIts.DisableControls;
    try
      QrVSPaCRIts.First;
      while not QrVSPaCRIts.Eof do
      begin
        if QrVSPaCRItsEstahNaOC.Value = 0 then
          EncerraItemAtual(False, False);
        //
        QrVSPaCRIts.Next;
      end;
    finally
      QrVSPaCRIts.EnableControls;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  ReopenVSPaCRIts(0);
*)
end;

procedure TFmVSPaCRIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSPaCRIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenVSPaCrIts(0);
end;

procedure TFmVSPaCRIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSPaCRIts.ReopenVSPaCrIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSPaCrIts, Dmod.MyDB, [
  'SELECT pal.DtHrEndAdd, cab.DtHrFimCla, (',
  //2015-03-30
  //'IF(pal.DtHrEndAdd < "100-01-01", 1, 0) + ',
  'IF(pal.DtHrEndAdd < "1900-01-01", 0, 0) + ',
  //FIM2015-03-30
  'IF(cab.LstPal01=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", 2, 0) +',
  'IF(cab.LstPal02=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", 4, 0) +',
  'IF(cab.LstPal03=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", 8, 0) +',
  'IF(cab.LstPal04=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", 16, 0) +',
  'IF(cab.LstPal05=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", 32, 0) +',
  'IF(cab.LstPal06=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", 64, 0) +',
  'IF(cab.LstPal07=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", 128, 0) +',
  'IF(cab.LstPal08=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", 256, 0) +',
  'IF(cab.LstPal09=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", 512, 0) +',
  'IF(cab.LstPal10=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", 1024, 0) +',
  'IF(cab.LstPal11=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", 2048, 0) +',
  'IF(cab.LstPal12=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", 4096, 0) +',
  'IF(cab.LstPal13=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", 8192, 0) +',
  'IF(cab.LstPal14=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", 16384, 0) +',
  'IF(cab.LstPal15=its.VSPallet AND cab.DtHrFimCla < "1900-01-01", 32768, 0) ) ',
  '+ 0.000 EstahNaOC,',
  '1.000 ClaOuRcl,"Classifica��o" NO_ClaOuRcl, ',
  'cab.CacCod, its.Codigo, its.Controle, its.VSPallet,  ',
  'its.VMI_Sorc, its.VMI_Baix, its.VMI_Dest, its.Tecla,  ',
  'its.DtHrIni, its.DtHrFim, its.QIt_Sorc, its.QIt_Baix ',
  'FROM vspaclaitsa its ',
  'LEFT JOIN vspaclacaba cab ON cab.Codigo=its.Codigo ',
  'LEFT JOIN vspalleta pal ON pal.Codigo=its.VSPallet',
  'WHERE its.DtHrFim < "1900-01-01" ',
  ' ',
  'UNION ',
  ' ',
  'SELECT pal.DtHrEndAdd, cab.DtHrFimCla, (',
  //2015-03-30
  //'IF(pal.DtHrEndAdd < "100-01-01", 1, 0) + ',
  'IF(pal.DtHrEndAdd < "1900-01-01", 0, 0) + ',
  //FIM2015-03-30
  'IF(cab.LstPal01=its.VSPallet, 2, 0) +',
  'IF(cab.LstPal02=its.VSPallet, 4, 0) +',
  'IF(cab.LstPal03=its.VSPallet, 8, 0) +',
  'IF(cab.LstPal04=its.VSPallet, 16, 0) +',
  'IF(cab.LstPal05=its.VSPallet, 32, 0) +',
  'IF(cab.LstPal06=its.VSPallet, 64, 0) +',
  'IF(cab.LstPal07=its.VSPallet, 128, 0) +',
  'IF(cab.LstPal08=its.VSPallet, 256, 0) +',
  'IF(cab.LstPal09=its.VSPallet, 512, 0) +',
  'IF(cab.LstPal10=its.VSPallet, 1024, 0) +',
  'IF(cab.LstPal11=its.VSPallet, 2048, 0) +',
  'IF(cab.LstPal12=its.VSPallet, 4096, 0) +',
  'IF(cab.LstPal13=its.VSPallet, 8192, 0) +',
  'IF(cab.LstPal14=its.VSPallet, 16384, 0) +',
  'IF(cab.LstPal15=its.VSPallet, 32768, 0) ) + 0.000 EstahNaOC,',
  '2.000 ClaOuRcl,"Reclassifica��o" NO_ClaOuRcl, ',
  'cab.CacCod, its.Codigo, its.Controle, its.VSPallet,  ',
  'its.VMI_Sorc, its.VMI_Baix, its.VMI_Dest, its.Tecla,  ',
  'its.DtHrIni, its.DtHrFim, its.QIt_Sorc, its.QIt_Baix ',
  'FROM vsparclitsa its ',
  'LEFT JOIN vsparclcaba cab ON cab.Codigo=its.Codigo ',
  'LEFT JOIN vspalleta pal ON pal.Codigo=its.VSPallet',
  'WHERE its.DtHrFim < "1900-01-01" ',
  'ORDER BY VSPallet, Controle ',
  '',
  ' ']);
  //
  if Controle <> 0 then
    QrVSPaCrIts.Locate('Controle', Controle, []);
end;

end.
