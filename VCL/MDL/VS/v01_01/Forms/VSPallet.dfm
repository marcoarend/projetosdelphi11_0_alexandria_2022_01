object FmVSPallet: TFmVSPallet
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-013 :: Pallet de Artigo de Ribeira Classificado'
  ClientHeight = 602
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 550
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 264
    ExplicitTop = 28
    object GBCntrl: TGroupBox
      Left = 0
      Top = 486
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 47
        Align = alLeft
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 624
        Top = 15
        Width = 382
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 249
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 223
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pallet'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIMEI: TBitBtn
          Tag = 589
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&IME-I'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtIMEIClick
        end
      end
      object GBAvisos1: TGroupBox
        Left = 204
        Top = 15
        Width = 420
        Height = 47
        Align = alClient
        Caption = ' Avisos: '
        TabOrder = 3
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 416
          Height = 30
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object LaAviso1: TLabel
            Left = 13
            Top = 2
            Width = 12
            Height = 17
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2: TLabel
            Left = 12
            Top = 1
            Width = 12
            Height = 17
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object PB1: TProgressBar
            Left = 0
            Top = 13
            Width = 416
            Height = 17
            Align = alBottom
            TabOrder = 0
          end
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 197
      Width = 1008
      Height = 112
      ActivePage = TabSheet3
      Align = alTop
      TabOrder = 1
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'IME-Is'
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 84
          Align = alClient
          DataSource = DsIMEIs
          PopupMenu = PMIMEI
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'IME-I'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_FORNECE'
              Title.Caption = 'Fornecedor'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ReqMovEstq'
              Title.Caption = 'RME'
              Width = 78
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MovimID'
              Title.Caption = 'Movimento'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MovimNiv'
              Title.Caption = 'Nivel Movimento'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GraGruX'
              Title.Caption = 'Reduzido'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRD_TAM_COR'
              Title.Caption = 'Artigo'
              Width = 264
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CustoM2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PesoKg'
              Title.Caption = 'Peso Kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea m'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaP2'
              Title.Caption = #193'rea ft'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoVrtPeca'
              Title.Caption = 'Sdo Pe'#231'as'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoVrtPeso'
              Title.Caption = 'Sdo Peso kg'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoVrtArM2'
              Title.Caption = 'Sdo m'#178
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = #218'ltima data/hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SrcMovID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SrcNivel1'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SrcNivel2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DstMovID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DstNivel1'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DstNivel2'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MovimCod'
              Title.Caption = 'IME-C'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Gera'#231#227'o por classe/reclasse'
        ImageIndex = 1
        object DBGVSPaIts: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 473
          Height = 84
          Align = alLeft
          DataSource = DsVSPaIts
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'VSPallet'
              Title.Caption = 'Pallet destino'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VSPaClaIts'
              Title.Caption = 'ID add classe'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VSPaRclIts'
              Title.Caption = 'ID add reclasse'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VMI_Sorc'
              Title.Caption = 'IME-I origem'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VMI_Dest'
              Title.Caption = 'IME-I destino'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pecas'
              Title.Caption = 'Pe'#231'as'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AreaM2'
              Title.Caption = #193'rea m'#178
              Width = 80
              Visible = True
            end>
        end
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 473
          Top = 0
          Width = 527
          Height = 84
          Align = alClient
          DataSource = DsVSPaItens
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VSPallet'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VMI_Sorc'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VMI_Dest'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VMI_Baix'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tecla'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrIni'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrFim'
              Visible = True
            end>
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Couros em classifica'#231#227'o'
        ImageIndex = 2
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 84
          Align = alClient
          DataSource = DsVSCacItsAOpn
          PopupMenu = PMCacIts
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Pallets interdependentes'
        ImageIndex = 3
        object GroupBox5: TGroupBox
          Left = 635
          Top = 0
          Width = 365
          Height = 84
          Align = alRight
          Caption = ' Destino: '
          TabOrder = 0
          object dmkDBGridZTO5: TdmkDBGridZTO
            Left = 2
            Top = 15
            Width = 361
            Height = 67
            Align = alClient
            DataSource = DsPallDst
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'VSPallet'
                Title.Caption = 'Pallet'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VMI_Dest'
                Title.Caption = 'IME-I'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtHrEndAdd_TXT'
                Title.Caption = 'Encerramento'
                Visible = True
              end>
          end
        end
        object GroupBox6: TGroupBox
          Left = 0
          Top = 0
          Width = 635
          Height = 84
          Align = alClient
          Caption = ' Origem: '
          TabOrder = 1
          object dmkDBGridZTO6: TdmkDBGridZTO
            Left = 2
            Top = 15
            Width = 631
            Height = 67
            Align = alClient
            DataSource = DsPallOri
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_ClaRcl'
                Title.Caption = 'Forma'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CacCod'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_SerieFch'
                Title.Caption = 'Serie'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ficha'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VSPallet'
                Title.Caption = 'Pallet'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pecas'
                Title.Caption = 'Pe'#231'as'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtHrFimCla_TXT'
                Title.Caption = 'Encerramento'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VSMovIts'
                Title.Caption = 'IME-I'
                Visible = True
              end>
          end
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Origem x destino classifica'#231#227'o'
        ImageIndex = 4
        object Splitter1: TSplitter
          Left = 497
          Top = 0
          Width = 5
          Height = 84
          ExplicitHeight = 86
        end
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 497
          Height = 84
          Align = alLeft
          Caption = ' Destino: '
          TabOrder = 0
          object dmkDBGridZTO2: TdmkDBGridZTO
            Left = 2
            Top = 15
            Width = 493
            Height = 36
            Align = alClient
            DataSource = DsDestino
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
          end
          object Panel6: TPanel
            Left = 2
            Top = 51
            Width = 493
            Height = 31
            Align = alBottom
            ParentBackground = False
            TabOrder = 1
            object Label25: TLabel
              Left = 0
              Top = 8
              Width = 84
              Height = 13
              Caption = 'Total de registros:'
            end
            object EdDestino: TdmkEdit
              Left = 88
              Top = 4
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
        end
        object GroupBox2: TGroupBox
          Left = 502
          Top = 0
          Width = 498
          Height = 84
          Align = alClient
          Caption = ' Origem:  '
          TabOrder = 1
          object Splitter2: TSplitter
            Left = 187
            Top = 15
            Height = 67
            ExplicitHeight = 70
          end
          object GroupBox3: TGroupBox
            Left = 2
            Top = 15
            Width = 185
            Height = 67
            Align = alLeft
            Caption = 'Classifica'#231#227'o: '
            TabOrder = 0
            object dmkDBGridZTO3: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 181
              Height = 20
              Align = alClient
              DataSource = DsOriCla
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
            end
            object Panel7: TPanel
              Left = 2
              Top = 35
              Width = 181
              Height = 30
              Align = alBottom
              ParentBackground = False
              TabOrder = 1
              object Label26: TLabel
                Left = 0
                Top = 8
                Width = 84
                Height = 13
                Caption = 'Total de registros:'
              end
              object EdOriCla: TdmkEdit
                Left = 88
                Top = 4
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
          end
          object GroupBox4: TGroupBox
            Left = 190
            Top = 15
            Width = 306
            Height = 67
            Align = alClient
            Caption = 'Reclassifica'#231#227'o: '
            TabOrder = 1
            object dmkDBGridZTO4: TdmkDBGridZTO
              Left = 2
              Top = 15
              Width = 302
              Height = 20
              Align = alClient
              DataSource = DsOriRcl
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
            end
            object Panel8: TPanel
              Left = 2
              Top = 35
              Width = 302
              Height = 30
              Align = alBottom
              ParentBackground = False
              TabOrder = 1
              object Label27: TLabel
                Left = 0
                Top = 8
                Width = 84
                Height = 13
                Caption = 'Total de registros:'
              end
              object EdOriRcl: TdmkEdit
                Left = 88
                Top = 4
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
          end
        end
      end
    end
    object Panel10: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 197
      Align = alTop
      Caption = 'Panel10'
      TabOrder = 2
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 72
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 496
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label6: TLabel
        Left = 12
        Top = 44
        Width = 194
        Height = 13
        Caption = 'Artigo de Ribeira Classificado (Reduzido):'
      end
      object Label10: TLabel
        Left = 420
        Top = 44
        Width = 93
        Height = 13
        Caption = 'Cliente preferencial:'
      end
      object Label11: TLabel
        Left = 792
        Top = 44
        Width = 33
        Height = 13
        Caption = 'Status:'
      end
      object Label13: TLabel
        Left = 12
        Top = 84
        Width = 126
        Height = 13
        Caption = 'Data / hora encerramento:'
        FocusControl = DBEdit9
      end
      object Label14: TLabel
        Left = 144
        Top = 84
        Width = 30
        Height = 13
        Caption = 'Artigo:'
        FocusControl = DBEdit10
      end
      object Label15: TLabel
        Left = 416
        Top = 84
        Width = 33
        Height = 13
        Caption = 'Pe'#231'as:'
        FocusControl = DBEdit12
      end
      object Label16: TLabel
        Left = 488
        Top = 84
        Width = 43
        Height = 13
        Caption = 'Peso Kg:'
        FocusControl = DBEdit13
      end
      object Label17: TLabel
        Left = 560
        Top = 84
        Width = 39
        Height = 13
        Caption = #193'rea m'#178':'
        FocusControl = DBEdit14
      end
      object Label18: TLabel
        Left = 632
        Top = 84
        Width = 37
        Height = 13
        Caption = #193'rea ft'#178':'
        FocusControl = DBEdit15
      end
      object Label19: TLabel
        Left = 704
        Top = 84
        Width = 39
        Height = 13
        Caption = '$ Custo:'
        FocusControl = DBEdit16
      end
      object Label20: TLabel
        Left = 776
        Top = 84
        Width = 50
        Height = 13
        Caption = 'Sdo Pe'#231'a:'
        FocusControl = DBEdit17
      end
      object Label21: TLabel
        Left = 848
        Top = 84
        Width = 38
        Height = 13
        Caption = 'Sdo Kg:'
        FocusControl = DBEdit18
      end
      object Label22: TLabel
        Left = 920
        Top = 84
        Width = 36
        Height = 13
        Caption = 'Sdo m'#178':'
        FocusControl = DBEdit19
      end
      object Label30: TLabel
        Left = 872
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSPallet
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 72
        Top = 20
        Width = 420
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsVSPallet
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 496
        Top = 20
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVSPallet
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 556
        Top = 20
        Width = 313
        Height = 21
        DataField = 'NO_EMPRESA'
        DataSource = DsVSPallet
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 12
        Top = 60
        Width = 56
        Height = 21
        DataField = 'GraGruX'
        DataSource = DsVSPallet
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 68
        Top = 60
        Width = 349
        Height = 21
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsVSPallet
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Left = 420
        Top = 60
        Width = 56
        Height = 21
        DataField = 'CliStat'
        DataSource = DsVSPallet
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 476
        Top = 60
        Width = 313
        Height = 21
        DataField = 'NO_CLISTAT'
        DataSource = DsVSPallet
        TabOrder = 7
      end
      object DBEdit7: TDBEdit
        Left = 792
        Top = 60
        Width = 56
        Height = 21
        DataField = 'Status'
        DataSource = DsVSPallet
        TabOrder = 8
      end
      object DBEdit8: TDBEdit
        Left = 848
        Top = 60
        Width = 141
        Height = 21
        DataField = 'NO_STATUS'
        DataSource = DsVSPallet
        TabOrder = 9
      end
      object DBEdit9: TDBEdit
        Left = 12
        Top = 100
        Width = 129
        Height = 21
        DataField = 'DtHrEndAdd_TXT'
        DataSource = DsVSPallet
        TabOrder = 10
      end
      object DBEdit10: TDBEdit
        Left = 144
        Top = 100
        Width = 56
        Height = 21
        DataField = 'GraGruX'
        DataSource = DsSumPall
        TabOrder = 11
      end
      object DBEdit11: TDBEdit
        Left = 200
        Top = 100
        Width = 213
        Height = 21
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsSumPall
        TabOrder = 12
      end
      object DBEdit12: TDBEdit
        Left = 416
        Top = 100
        Width = 68
        Height = 21
        DataField = 'Pecas'
        DataSource = DsSumPall
        TabOrder = 13
      end
      object DBEdit13: TDBEdit
        Left = 488
        Top = 100
        Width = 68
        Height = 21
        DataField = 'PesoKg'
        DataSource = DsSumPall
        TabOrder = 14
      end
      object DBEdit14: TDBEdit
        Left = 560
        Top = 100
        Width = 68
        Height = 21
        DataField = 'AreaM2'
        DataSource = DsSumPall
        TabOrder = 15
      end
      object DBEdit15: TDBEdit
        Left = 632
        Top = 100
        Width = 68
        Height = 21
        DataField = 'AreaP2'
        DataSource = DsSumPall
        TabOrder = 16
      end
      object DBEdit16: TDBEdit
        Left = 704
        Top = 100
        Width = 68
        Height = 21
        DataField = 'ValorT'
        DataSource = DsSumPall
        TabOrder = 17
      end
      object DBEdit17: TDBEdit
        Left = 776
        Top = 100
        Width = 68
        Height = 21
        DataField = 'SdoVrtPeca'
        DataSource = DsSumPall
        TabOrder = 18
      end
      object DBEdit18: TDBEdit
        Left = 848
        Top = 100
        Width = 68
        Height = 21
        DataField = 'SdoVrtPeso'
        DataSource = DsSumPall
        TabOrder = 19
      end
      object DBEdit19: TDBEdit
        Left = 920
        Top = 100
        Width = 68
        Height = 21
        DataField = 'SdoVrtArM2'
        DataSource = DsSumPall
        TabOrder = 20
      end
      object GroupBox8: TGroupBox
        Left = 12
        Top = 124
        Width = 701
        Height = 69
        Caption = ' SIF/DIPOA:'
        TabOrder = 21
        object Panel11: TPanel
          Left = 2
          Top = 15
          Width = 697
          Height = 52
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label28: TLabel
            Left = 8
            Top = 8
            Width = 129
            Height = 13
            Caption = 'N'#186' de identifica'#231#227'o do bag:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label29: TLabel
            Left = 168
            Top = 8
            Width = 61
            Height = 13
            Caption = 'Observa'#231#227'o:'
            Color = clBtnFace
            ParentColor = False
          end
          object DBEdit20: TDBEdit
            Left = 8
            Top = 24
            Width = 157
            Height = 21
            DataField = 'SeqSifDipoa'
            DataSource = DsVSPallet
            TabOrder = 0
          end
          object DBEdit22: TDBEdit
            Left = 168
            Top = 24
            Width = 516
            Height = 21
            DataField = 'ObsSifDipoa'
            DataSource = DsVSPallet
            TabOrder = 1
          end
        end
      end
      object DBEdit21: TDBEdit
        Left = 872
        Top = 20
        Width = 117
        Height = 21
        DataField = 'DataFab'
        DataSource = DsVSPallet
        TabOrder = 22
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 550
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 141
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 120
        Height = 13
        Caption = 'Descri'#231#227'o / Observa'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 500
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label5: TLabel
        Left = 796
        Top = 56
        Width = 33
        Height = 13
        Caption = 'Status:'
      end
      object SBCliente: TSpeedButton
        Left = 772
        Top = 72
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = SBClienteClick
      end
      object LaVSRibCla: TLabel
        Left = 16
        Top = 56
        Width = 84
        Height = 13
        Caption = 'Artigo (Reduzido):'
        Enabled = False
      end
      object Label4: TLabel
        Left = 424
        Top = 56
        Width = 93
        Height = 13
        Caption = 'Cliente preferencial:'
      end
      object Label12: TLabel
        Left = 844
        Top = 16
        Width = 103
        Height = 13
        Caption = 'Data / hora chegada:'
        Color = clBtnFace
        Enabled = False
        ParentColor = False
      end
      object Label23: TLabel
        Left = 796
        Top = 16
        Width = 28
        Height = 13
        Caption = 'PPP*:'
      end
      object Label62: TLabel
        Left = 16
        Top = 96
        Width = 74
        Height = 13
        Caption = 'Dono do couro:'
      end
      object LaDataFab: TLabel
        Left = 564
        Top = 96
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 420
        Height = 21
        MaxLength = 60
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 500
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 560
        Top = 32
        Width = 233
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 3
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCliStat: TdmkEditCB
        Left = 424
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CliStat'
        UpdCampo = 'CliStat'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliStat
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliStat: TdmkDBLookupComboBox
        Left = 480
        Top = 72
        Width = 293
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsClientes
        TabOrder = 10
        dmkEditCB = EdCliStat
        QryCampo = 'CliStat'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdStatus: TdmkEditCB
        Left = 796
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Status'
        UpdCampo = 'Status'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBStatus
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBStatus: TdmkDBLookupComboBox
        Left = 852
        Top = 72
        Width = 141
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVSPalSta
        TabOrder = 12
        dmkEditCB = EdStatus
        QryCampo = 'Status'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdGraGruX: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GraGruX'
        UpdCampo = 'GraGruX'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdGraGruXRedefinido
        DBLookupComboBox = CBGraGruX
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGruX: TdmkDBLookupComboBox
        Left = 75
        Top = 72
        Width = 346
        Height = 21
        Enabled = False
        KeyField = 'Controle'
        ListField = 'NO_PRD_TAM_COR'
        ListSource = DsGraGruX
        TabOrder = 8
        dmkEditCB = EdGraGruX
        QryCampo = 'GraGruX'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPDtHrEndAdd: TdmkEditDateTimePicker
        Left = 844
        Top = 32
        Width = 108
        Height = 21
        Date = 45030.000000000000000000
        Time = 0.639644131944805800
        Enabled = False
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtHrEndAdd'
        UpdCampo = 'DtHrEndAdd'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtHrEndAdd: TdmkEdit
        Left = 952
        Top = 32
        Width = 40
        Height = 21
        Enabled = False
        TabOrder = 6
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00:00'
        QryCampo = 'DtHrEndAdd'
        UpdCampo = 'DtHrEndAdd'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdQtdPrevPc: TdmkEdit
        Left = 796
        Top = 32
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '110'
        QryCampo = 'QtdPrevPc'
        UpdCampo = 'QtdPrevPc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 110
        ValWarn = False
      end
      object EdClientMO: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ClientMO'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClientMO
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClientMO: TdmkDBLookupComboBox
        Left = 72
        Top = 112
        Width = 489
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsClientMO
        TabOrder = 14
        dmkEditCB = EdClientMO
        QryCampo = 'ClientMO'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object TPDataFab: TdmkEditDateTimePicker
        Left = 564
        Top = 112
        Width = 112
        Height = 21
        Date = 45287.000000000000000000
        Time = 0.852971273146977200
        TabOrder = 15
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 487
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object Label24: TLabel
        Left = 152
        Top = 34
        Width = 200
        Height = 13
        Caption = 'PPP*: Previs'#227'o de pe'#231'as no pallet.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GroupBox7: TGroupBox
      Left = 12
      Top = 144
      Width = 701
      Height = 69
      Caption = 
        ' SIF/DIPOA:  (para habilitar a edi'#231#227'o ative o uso do SIF/DIPOA n' +
        'o cadastro do artigo no seu grupo de estoque)'
      TabOrder = 2
      object Panel9: TPanel
        Left = 2
        Top = 15
        Width = 697
        Height = 52
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaSeqSifDipoa: TLabel
          Left = 8
          Top = 8
          Width = 150
          Height = 13
          Caption = 'N'#186' de identifica'#231#227'o do bag [F4]:'
          Color = clBtnFace
          Enabled = False
          ParentColor = False
        end
        object LaObsSifDipoa: TLabel
          Left = 172
          Top = 8
          Width = 61
          Height = 13
          Caption = 'Observa'#231#227'o:'
          Color = clBtnFace
          Enabled = False
          ParentColor = False
        end
        object EdSeqSifDipoa: TdmkEdit
          Left = 8
          Top = 24
          Width = 157
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'SeqSifDipoa'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdObsSifDipoa: TdmkEdit
          Left = 172
          Top = 24
          Width = 521
          Height = 21
          Enabled = False
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'ObsSifDipoa'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 470
        Height = 32
        Caption = 'Pallet de Artigo de Ribeira Classificado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 470
        Height = 32
        Caption = 'Pallet de Artigo de Ribeira Classificado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 470
        Height = 32
        Caption = 'Pallet de Artigo de Ribeira Classificado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 64
    Top = 56
  end
  object QrVSPallet: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSPalletBeforeOpen
    AfterOpen = QrVSPalletAfterOpen
    BeforeClose = QrVSPalletBeforeClose
    AfterScroll = QrVSPalletAfterScroll
    OnCalcFields = QrVSPalletCalcFields
    SQL.Strings = (
      'SELECT let.*,  '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLISTAT,'
      ' CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  '
      'NO_PRD_TAM_COR, vps.Nome NO_STATUS   '
      'FROM vspalleta let  '
      'LEFT JOIN entidades emp ON emp.Codigo=let.Empresa  '
      'LEFT JOIN entidades cli ON cli.Codigo=let.CliStat  '
      'LEFT JOIN gragrux ggx ON ggx.Controle=let.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN vspalsta   vps ON vps.Codigo=let.Status'
      'WHERE let.Codigo > 0')
    Left = 236
    Top = 69
    object QrVSPalletCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPalletNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrVSPalletLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSPalletDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSPalletDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSPalletUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSPalletUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSPalletAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSPalletAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSPalletEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSPalletNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPalletStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrVSPalletCliStat: TIntegerField
      FieldName = 'CliStat'
    end
    object QrVSPalletGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSPalletNO_CLISTAT: TWideStringField
      FieldName = 'NO_CLISTAT'
      Size = 100
    end
    object QrVSPalletNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPalletNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrVSPalletDtHrEndAdd: TDateTimeField
      FieldName = 'DtHrEndAdd'
    end
    object QrVSPalletDtHrEndAdd_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DtHrEndAdd_TXT'
      Calculated = True
    end
    object QrVSPalletQtdPrevPc: TIntegerField
      FieldName = 'QtdPrevPc'
    end
    object QrVSPalletGerRclCab: TIntegerField
      FieldName = 'GerRclCab'
    end
    object QrVSPalletDtHrFimRcl: TDateTimeField
      FieldName = 'DtHrFimRcl'
    end
    object QrVSPalletMovimIDGer: TIntegerField
      FieldName = 'MovimIDGer'
    end
    object QrVSPalletClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVSPalletStatPall: TIntegerField
      FieldName = 'StatPall'
      Required = True
    end
    object QrVSPalletSeqSifDipoa: TIntegerField
      FieldName = 'SeqSifDipoa'
      Required = True
    end
    object QrVSPalletObsSifDipoa: TWideStringField
      FieldName = 'ObsSifDipoa'
      Required = True
      Size = 60
    end
    object QrVSPalletDataFab: TDateField
      FieldName = 'DataFab'
    end
  end
  object DsVSPallet: TDataSource
    DataSet = QrVSPallet
    Left = 236
    Top = 113
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 528
    Top = 504
    object CabInclui1: TMenuItem
      Caption = '&Inclui Automaticamente'
      OnClick = CabInclui1Click
    end
    object IncluiManualmente1: TMenuItem
      Caption = 'Inclui &Manualmente'
      OnClick = IncluiManualmente1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      object udo1: TMenuItem
        Caption = '&Tudo'
        OnClick = udo1Click
      end
      object ApenasaDescrio1: TMenuItem
        Caption = 'Apenas a Descri'#231#227'o'
        OnClick = ApenasaDescrio1Click
      end
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object EncerraPallet1: TMenuItem
      Caption = 'Encerra &Pallet'
      OnClick = EncerraPallet1Click
    end
    object Encerraclassificao1: TMenuItem
      Caption = 'Encerra &Classifica'#231#227'o'
      OnClick = Encerraclassificao1Click
    end
    object Desfazencerramento1: TMenuItem
      Caption = '&Desfaz encerramento'
      OnClick = Desfazencerramento1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Atualizasaldo1: TMenuItem
      Caption = 'Atuali&za saldo'
      OnClick = Atualizasaldo1Click
    end
    object AtualizaStatus1: TMenuItem
      Caption = 'Atualiza &Status'
      OnClick = AtualizaStatus1Click
    end
    object RecalculaIMEIsdeclassereclasse1: TMenuItem
      Caption = 'Recalcula IME-Is de classe/&reclasse'
      object IMEIsdegerao1: TMenuItem
        Caption = 'IME_Is de &gera'#231#227'o'
        OnClick = IMEIsdegerao1Click
      end
    end
    object AlteraFornecedor1: TMenuItem
      Caption = 'Altera &Fornecedor'
      OnClick = AlteraFornecedor1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Mudademeiosparainteiro1: TMenuItem
      Caption = 'Muda de meios para inteiro'
      OnClick = Mudademeiosparainteiro1Click
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NO_ENT')
    Left = 428
    Top = 69
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 428
    Top = 113
  end
  object QrVSPalSta: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM vspalsta'
      'ORDER BY Nome')
    Left = 496
    Top = 68
    object QrVSPalStaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPalStaNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSPalSta: TDataSource
    DataSet = QrVSPalSta
    Left = 496
    Top = 112
  end
  object QrSumPall: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.GraGruX, SUM(wmi.Pecas) Pecas,'
      'SUM(wmi.PesoKg) PesoKg, SUM(wmi.AreaM2) AreaM2,'
      'SUM(AreaP2) AreaP2, SUM(ValorT) ValorT,'
      'SUM(wmi.SdoVrtPeca) SdoVrtPeca, SUM(wmi.SdoVrtPeso) SdoVrtPeso,'
      'SUM(wmi.SdoVrtArM2) SdoVrtArM2,'
      'ggx.GraGru1, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, MAX(wmi.DataHora) DataHora'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE wmi.Pallet = 47')
    Left = 632
    Top = 68
    object QrSumPallGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrSumPallPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumPallPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,##0.000'
    end
    object QrSumPallAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPallAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPallValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrSumPallSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrSumPallSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,##0.000'
    end
    object QrSumPallSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumPallGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrSumPallNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrSumPallDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
  end
  object QrIMEIs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT wmi.Controle, wmi.GraGruX, wmi.Pecas,'
      'wmi.PesoKg, wmi.AreaM2, wmi.AreaP2, wmi.ValorT,'
      'wmi.SdoVrtPeca, wmi.SdoVrtPeso, wmi.SdoVrtArM2,'
      'ggx.GraGru1, CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR, wmi.DataHora,'
      'wmi.SrcNivel1, wmi.SrcNivel2,'
      'wmi.DstNivel1, wmi.DstNivel2'
      'FROM vsmovits wmi'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=wmi.GraGruX'
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE wmi.Pallet = 47')
    Left = 708
    Top = 68
    object QrIMEIsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object QrIMEIsControle: TLargeintField
      FieldName = 'Controle'
      Required = True
    end
    object QrIMEIsMovimCod: TLargeintField
      FieldName = 'MovimCod'
      Required = True
    end
    object QrIMEIsMovimNiv: TLargeintField
      FieldName = 'MovimNiv'
      Required = True
    end
    object QrIMEIsMovimTwn: TLargeintField
      FieldName = 'MovimTwn'
      Required = True
    end
    object QrIMEIsEmpresa: TLargeintField
      FieldName = 'Empresa'
      Required = True
    end
    object QrIMEIsTerceiro: TLargeintField
      FieldName = 'Terceiro'
      Required = True
    end
    object QrIMEIsCliVenda: TLargeintField
      FieldName = 'CliVenda'
      Required = True
    end
    object QrIMEIsMovimID: TLargeintField
      FieldName = 'MovimID'
      Required = True
    end
    object QrIMEIsDataHora: TDateTimeField
      FieldName = 'DataHora'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrIMEIsPallet: TLargeintField
      FieldName = 'Pallet'
      Required = True
    end
    object QrIMEIsGraGruX: TLargeintField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrIMEIsPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsSrcMovID: TLargeintField
      FieldName = 'SrcMovID'
      Required = True
    end
    object QrIMEIsSrcNivel1: TLargeintField
      FieldName = 'SrcNivel1'
      Required = True
    end
    object QrIMEIsSrcNivel2: TLargeintField
      FieldName = 'SrcNivel2'
      Required = True
    end
    object QrIMEIsSrcGGX: TLargeintField
      FieldName = 'SrcGGX'
      Required = True
    end
    object QrIMEIsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrIMEIsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrIMEIsSerieFch: TLargeintField
      FieldName = 'SerieFch'
      Required = True
    end
    object QrIMEIsFicha: TLargeintField
      FieldName = 'Ficha'
      Required = True
    end
    object QrIMEIsMisturou: TLargeintField
      FieldName = 'Misturou'
      Required = True
    end
    object QrIMEIsFornecMO: TLargeintField
      FieldName = 'FornecMO'
    end
    object QrIMEIsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsDstMovID: TLargeintField
      FieldName = 'DstMovID'
      Required = True
    end
    object QrIMEIsDstNivel1: TLargeintField
      FieldName = 'DstNivel1'
      Required = True
    end
    object QrIMEIsDstNivel2: TLargeintField
      FieldName = 'DstNivel2'
      Required = True
    end
    object QrIMEIsDstGGX: TLargeintField
      FieldName = 'DstGGX'
      Required = True
    end
    object QrIMEIsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrIMEIsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrIMEIsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrIMEIsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrIMEIsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrIMEIsStqCenLoc: TLargeintField
      FieldName = 'StqCenLoc'
    end
    object QrIMEIsReqMovEstq: TLargeintField
      FieldName = 'ReqMovEstq'
    end
    object QrIMEIsGraGru1: TLargeintField
      FieldName = 'GraGru1'
    end
    object QrIMEIsNO_EstqMovimID: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EstqMovimID'
      Size = 255
      Calculated = True
    end
    object QrIMEIsNO_MovimID: TWideStringField
      FieldName = 'NO_MovimID'
      Size = 255
    end
    object QrIMEIsNO_MovimNiv: TWideStringField
      FieldName = 'NO_MovimNiv'
      Size = 255
    end
    object QrIMEIsNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrIMEIsNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrIMEIsNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrIMEIsNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrIMEIsNO_TTW: TWideStringField
      DisplayWidth = 5
      FieldName = 'NO_TTW'
      Size = 5
    end
    object QrIMEIsID_TTW: TLargeintField
      FieldName = 'ID_TTW'
    end
    object QrIMEIsNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Size = 120
    end
    object QrIMEIsCustoM2: TFloatField
      FieldName = 'CustoM2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSumPall: TDataSource
    DataSet = QrSumPall
    Left = 632
    Top = 116
  end
  object DsIMEIs: TDataSource
    DataSet = QrIMEIs
    Left = 708
    Top = 116
  end
  object QrVSCacItsAOpn: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM vscacitsa'
      'WHERE VSPallet=17')
    Left = 320
    Top = 344
    object QrVSCacItsAOpnCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSCacItsAOpnCacID: TIntegerField
      FieldName = 'CacID'
    end
    object QrVSCacItsAOpnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSCacItsAOpnControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrVSCacItsAOpnClaAPalOri: TIntegerField
      FieldName = 'ClaAPalOri'
    end
    object QrVSCacItsAOpnRclAPalOri: TIntegerField
      FieldName = 'RclAPalOri'
    end
    object QrVSCacItsAOpnVSPaClaIts: TIntegerField
      FieldName = 'VSPaClaIts'
    end
    object QrVSCacItsAOpnVSPaRclIts: TIntegerField
      FieldName = 'VSPaRclIts'
    end
    object QrVSCacItsAOpnVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSCacItsAOpnVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSCacItsAOpnVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSCacItsAOpnBox: TIntegerField
      FieldName = 'Box'
    end
    object QrVSCacItsAOpnPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSCacItsAOpnAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSCacItsAOpnAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSCacItsAOpnRevisor: TIntegerField
      FieldName = 'Revisor'
    end
    object QrVSCacItsAOpnDigitador: TIntegerField
      FieldName = 'Digitador'
    end
    object QrVSCacItsAOpnDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSCacItsAOpnAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSCacItsAOpnAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSCacItsAOpnSumido: TSmallintField
      FieldName = 'Sumido'
    end
    object QrVSCacItsAOpnRclAPalDst: TIntegerField
      FieldName = 'RclAPalDst'
    end
    object QrVSCacItsAOpnVMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSCacItsAOpnMartelo: TIntegerField
      FieldName = 'Martelo'
    end
    object QrVSCacItsAOpnFrmaIns: TSmallintField
      FieldName = 'FrmaIns'
    end
    object QrVSCacItsAOpnSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
  end
  object DsVSCacItsAOpn: TDataSource
    DataSet = QrVSCacItsAOpn
    Left = 320
    Top = 392
  end
  object QrVSPaIts: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPaItsBeforeClose
    AfterScroll = QrVSPaItsAfterScroll
    SQL.Strings = (
      'SELECT VSPallet, VSPaClaIts, VSPaRclIts, VMI_Sorc, VMI_Dest, '
      'SUM(Pecas) Pecas, SUM(AreaM2) AreaM2'
      'FROM vscacitsa'
      'WHERE VSPallet=318 '
      'GROUP BY VSPallet, VSPaClaIts, VSPaRclIts, '
      'VMI_Sorc, VMI_Dest')
    Left = 56
    Top = 356
    object QrVSPaItsVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSPaItsVSPaClaIts: TIntegerField
      FieldName = 'VSPaClaIts'
    end
    object QrVSPaItsVSPaRclIts: TIntegerField
      FieldName = 'VSPaRclIts'
    end
    object QrVSPaItsVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSPaItsVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPaItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSPaItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsVSPaIts: TDataSource
    DataSet = QrVSPaIts
    Left = 56
    Top = 404
  end
  object QrVSPaItens: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Controle, VSPallet, VMI_Sorc, VMI_Baix, VMI_Dest'
      'Tecla, DtHrIni, DtHrFim, QIt_Sorc, QIt_Baix '
      'FROM vspaclaitsa'
      'WHERE Controle=138')
    Left = 132
    Top = 356
    object QrVSPaItensCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSPaItensControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSPaItensVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSPaItensVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSPaItensVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSPaItensVMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSPaItensTecla: TIntegerField
      FieldName = 'Tecla'
    end
    object QrVSPaItensDtHrIni: TDateTimeField
      FieldName = 'DtHrIni'
    end
    object QrVSPaItensDtHrFim: TDateTimeField
      FieldName = 'DtHrFim'
    end
    object QrVSPaItensQIt_Sorc: TIntegerField
      FieldName = 'QIt_Sorc'
    end
    object QrVSPaItensQIt_Baix: TIntegerField
      FieldName = 'QIt_Baix'
    end
  end
  object DsVSPaItens: TDataSource
    DataSet = QrVSPaItens
    Left = 132
    Top = 404
  end
  object PMIMEI: TPopupMenu
    OnPopup = PMIMEIPopup
    Left = 660
    Top = 512
    object Irparajaneladomovimento1: TMenuItem
      Caption = 'Ir para janela do movimento'
      OnClick = Irparajaneladomovimento1Click
    end
    object IrparajaneladedadosdoIMEI1: TMenuItem
      Caption = 'Ir para janela de dados do IME-I '
      OnClick = IrparajaneladedadosdoIMEI1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object ExcluiIMEIselecionado1: TMenuItem
      Caption = 'Exclui IME-I selecionado'
      OnClick = ExcluiIMEIselecionado1Click
    end
    object AlteraIMEIorigemdoIMEISelecionado1: TMenuItem
      Caption = 'Altera IME-I origem do IME-I Selecionado'
      OnClick = AlteraIMEIorigemdoIMEISelecionado1Click
    end
    object Atualizasaldo2: TMenuItem
      Caption = 'Atualiza saldo'
      OnClick = Atualizasaldo2Click
    end
  end
  object QrOriRcl: TMySQLQuery
    Database = Dmod.MyDB
    Left = 792
    Top = 300
  end
  object DsOriRcl: TDataSource
    DataSet = QrOriRcl
    Left = 792
    Top = 348
  end
  object QrOriCla: TMySQLQuery
    Database = Dmod.MyDB
    Left = 876
    Top = 304
  end
  object DsOriCla: TDataSource
    DataSet = QrOriCla
    Left = 876
    Top = 352
  end
  object QrDestino: TMySQLQuery
    Database = Dmod.MyDB
    Left = 736
    Top = 304
  end
  object DsDestino: TDataSource
    DataSet = QrDestino
    Left = 736
    Top = 352
  end
  object QrPallDst: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cia.VSPallet, SUM(Pecas) Pecas, VMI_Dest, pal.DtHrEndAdd,'
      'IF(pal.DtHrEndAdd < "1900-01-01", "",'
      'DATE_FORMAT(pal.DtHrEndAdd, "%d/%m/%Y %h:%i:%s")) DtHrEndAdd_TXT'
      'FROM vscacitsa cia'
      'LEFT JOIN vsparclcaba rcl ON cia.CacCod=rcl.CacCod'
      'LEFT JOIN vspalleta   pal ON pal.Codigo=cia.VSPallet'
      'WHERE rcl.VSPallet=489'
      'GROUP BY VMI_Dest')
    Left = 520
    Top = 292
    object QrPallDstVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrPallDstPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrPallDstVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrPallDstDtHrEndAdd: TDateTimeField
      FieldName = 'DtHrEndAdd'
    end
    object QrPallDstDtHrEndAdd_TXT: TWideStringField
      FieldName = 'DtHrEndAdd_TXT'
      Size = 19
    end
  end
  object DsPallDst: TDataSource
    DataSet = QrPallDst
    Left = 520
    Top = 340
  end
  object QrPallOri: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT 2.000 ClaRcl, "Reclassifica'#231#227'o" NO_ClaRcl,'
      'cia.CacCod, cia.Codigo, SUM(cia.Pecas) Pecas,'
      'pcc.VSMovIts, pcc.VSPallet + 0.000 VSPallet, pcc.DtHrFimCla,'
      'vmi.SerieFch, vmi.Ficha, vsf.Nome NO_SerieFch,'
      'IF(pcc.DtHrFimCla < "1900-01-01", "",'
      'DATE_FORMAT(pcc.DtHrFimCla, "%d/%m/%Y %h:%i:%s")) DtHrFimCla_TXT'
      'FROM vscacitsa cia'
      'LEFT JOIN vsparclcaba pcc ON pcc.CacCod=cia.CacCod'
      'LEFT JOIN vsmovits vmi ON vmi.Controle=pcc.VSMovIts'
      'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch'
      'WHERE cia.VSPallet IN (474, 480)'
      'AND cia.CacID=8'
      'GROUP BY cia.CacCod, cia.Codigo'
      ''
      'UNION'
      ''
      'SELECT 1.000 ClaRcl, "Classifica'#231#227'o" NO_ClaRcl,'
      'cia.CacCod, cia.Codigo, SUM(cia.Pecas) Pecas,'
      'pcc.VSMovIts, 0 VSPallet, pcc.DtHrFimCla,'
      'vmi.SerieFch, vmi.Ficha, vsf.Nome NO_SerieFch,'
      'IF(pcc.DtHrFimCla < "1900-01-01", "",'
      'DATE_FORMAT(pcc.DtHrFimCla, "%d/%m/%Y %h:%i:%s")) DtHrFimCla_TXT'
      'FROM vscacitsa cia'
      'LEFT JOIN vspaclacaba pcc ON pcc.CacCod=cia.CacCod'
      'LEFT JOIN vsmovits vmi ON vmi.Controle=pcc.VSMovIts'
      'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch'
      'WHERE cia.VSPallet IN (474, 480)'
      'AND cia.CacID=7'
      'GROUP BY cia.CacCod, cia.Codigo')
    Left = 596
    Top = 292
    object QrPallOriClaRcl: TFloatField
      FieldName = 'ClaRcl'
      Required = True
    end
    object QrPallOriNO_ClaRcl: TWideStringField
      FieldName = 'NO_ClaRcl'
      Required = True
      Size = 15
    end
    object QrPallOriCacCod: TIntegerField
      FieldName = 'CacCod'
      Required = True
    end
    object QrPallOriCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPallOriPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrPallOriVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
    end
    object QrPallOriVSPallet: TFloatField
      FieldName = 'VSPallet'
    end
    object QrPallOriDtHrFimCla: TDateTimeField
      FieldName = 'DtHrFimCla'
    end
    object QrPallOriDtHrFimCla_TXT: TWideStringField
      FieldName = 'DtHrFimCla_TXT'
      Size = 19
    end
    object QrPallOriSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrPallOriFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrPallOriNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
  end
  object DsPallOri: TDataSource
    DataSet = QrPallOri
    Left = 596
    Top = 340
  end
  object PMCacIts: TPopupMenu
    OnPopup = PMCacItsPopup
    Left = 228
    Top = 376
    object Alteradadosdiitematual1: TMenuItem
      Caption = '&Altera dados do item atual'
      OnClick = Alteradadosdiitematual1Click
    end
    object Excluiitematual1: TMenuItem
      Caption = '&Exclui item atual'
      OnClick = Excluiitematual1Click
    end
  end
  object QrSumVMI: TMySQLQuery
    Database = Dmod.MyDB
    Left = 416
    Top = 460
    object QrSumVMIPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
    end
  end
  object PMImprime: TPopupMenu
    Left = 124
    Top = 52
    object FichaCOMonomedoPallet1: TMenuItem
      Caption = 'Ficha &COM o nome do Pallet'
      OnClick = FichaCOMonomedoPallet1Click
    end
    object FichaSEMonomedoPallet1: TMenuItem
      Caption = 'Ficha &SEM o nome do Pallet'
      OnClick = FichaSEMonomedoPallet1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Itensnopallet1: TMenuItem
      Caption = 'Itens no pallet'
      OnClick = Itensnopallet1Click
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 112
    Top = 192
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXGraGruY: TIntegerField
      FieldName = 'GraGruY'
    end
    object QrGraGruXUsaSifDipoa: TSmallintField
      FieldName = 'UsaSifDipoa'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 112
    Top = 240
  end
  object QrClientMO: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 40
    Top = 464
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientMO: TDataSource
    DataSet = QrClientMO
    Left = 40
    Top = 512
  end
  object frxDsVSCacItsAOpnImp: TfrxDBDataset
    UserName = 'frxDsVSCacItsAOpnImp'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CacCod=CacCod'
      'CacID=CacID'
      'Codigo=Codigo'
      'Controle=Controle'
      'ClaAPalOri=ClaAPalOri'
      'RclAPalOri=RclAPalOri'
      'VSPaClaIts=VSPaClaIts'
      'VSPaRclIts=VSPaRclIts'
      'VSPallet=VSPallet'
      'VMI_Sorc=VMI_Sorc'
      'VMI_Dest=VMI_Dest'
      'Box=Box'
      'Pecas=Pecas'
      'AreaM2=AreaM2'
      'AreaP2=AreaP2'
      'Revisor=Revisor'
      'Digitador=Digitador'
      'DataHora=DataHora'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'Sumido=Sumido'
      'RclAPalDst=RclAPalDst'
      'VMI_Baix=VMI_Baix'
      'Martelo=Martelo'
      'FrmaIns=FrmaIns'
      'SubClass=SubClass'
      'NoRev=NoRev'
      'NoDig=NoDig')
    DataSet = QrVSCacItsAOpnImp
    BCDToCurrency = False
    DataSetOptions = []
    Left = 884
    Top = 52
  end
  object frxWET_CURTI_013_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '(*'
      'var'
      '  FVSNivGer: Integer;                                     '
      '  FTamTexto1, FTamTexto2: Extended;'
      '*)        '
      'begin'
      '(*'
      '  FVSNivGer := <VARF_VSNIVGER>;'
      '  //'
      '  MeTitPesoKg.Visible := FVSNivGer > 0;'
      '  MeTitAreaM2.Visible := FVSNivGer > 0;'
      '  MeTitAreaP2.Visible := FVSNivGer > 0;'
      '  MeTitValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeValPesoKg.Visible := FVSNivGer > 0;'
      '  MeValAreaM2.Visible := FVSNivGer > 0;'
      '  MeValAreaP2.Visible := FVSNivGer > 0;'
      '  MeValValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu1PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu1AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu1AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu1ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSu2PesoKg.Visible := FVSNivGer > 0;'
      '  MeSu2AreaM2.Visible := FVSNivGer > 0;'
      '  MeSu2AreaP2.Visible := FVSNivGer > 0;'
      '  MeSu2ValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  MeSuTPesoKg.Visible := FVSNivGer > 0;'
      '  MeSuTAreaM2.Visible := FVSNivGer > 0;'
      '  MeSuTAreaP2.Visible := FVSNivGer > 0;'
      '  MeSuTValorT.Visible := FVSNivGer > 1;'
      '  //        '
      '  case FVSNivGer of'
      '    0:'
      '    begin'
      
        '      FTamTexto1 := MeTitPecas.Left - MeTitNome.Left;           ' +
        '         '
      
        '      FTamTexto2 := MeValPecas.Left - Me_FT1.Left;              ' +
        '      '
      '    end;'
      '    1:'
      '    begin'
      
        '      FTamTexto1 := MeTitPesoKg.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValPesoKg.Left - Me_FT1.Left;             ' +
        '       '
      '    end;               '
      '    2:'
      '    begin'
      
        '      FTamTexto1 := MeTitValorT.Left - MeTitNome.Left;          ' +
        '          '
      
        '      FTamTexto2 := MeValValorT.Left - Me_FT1.Left;             ' +
        '       '
      '    end;'
      '  end;              '
      '  MeTitNome.Width := FTamTexto1;'
      '  MeValNome.Width := FTamTexto1;'
      '  //'
      '  Me_FT1.Width := FTamTexto2;'
      '  Me_FT2.Width := FTamTexto2;'
      '  Me_FTT.Width := FTamTexto2;'
      '  //          '
      '*)'
      'end.')
    OnGetValue = frxWET_CURTI_013_AGetValue
    Left = 888
    Top = 100
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsVSCacItsAOpnImp
        DataSetName = 'frxDsVSCacItsAOpnImp'
      end
      item
        DataSet = frxDsVSPallet
        DataSetName = 'frxDsVSPallet'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 71.811060240000000000
        Top = 16.000000000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Itens no Pallet [frxDsVSPallet."Codigo"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Top = 56.692949999999990000
          Width = 234.330664720000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Revisor')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692949999999990000
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID Item')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 56.692949999999990000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pe'#231'as')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaP2: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 56.692949999999990000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeTitAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 56.692949999999990000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #193'rea m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 56.692949999999990000
          Width = 151.181151180000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data / hora revis'#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSPallet."Nome"]')
          ParentFont = False
        end
      end
      object MD002: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 148.000000000000000000
        Width = 680.315400000000000000
        DataSet = frxDsVSCacItsAOpnImp
        DataSetName = 'frxDsVSCacItsAOpnImp'
        RowCount = 0
        object MeValNome: TfrxMemoView
          AllowVectorExport = True
          Left = 68.031540000000000000
          Width = 41.574634720000000000
          Height = 15.118110240000000000
          DataField = 'Revisor'
          DataSet = frxDsVSCacItsAOpnImp
          DataSetName = 'frxDsVSCacItsAOpnImp'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsVSCacItsAOpnImp."Revisor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValCodi: TfrxMemoView
          AllowVectorExport = True
          Width = 68.031496060000000000
          Height = 15.118110240000000000
          DataField = 'Controle'
          DataSet = frxDsVSCacItsAOpnImp
          DataSetName = 'frxDsVSCacItsAOpnImp'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSCacItsAOpnImp."Controle"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValPecas: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'Pecas'
          DataSet = frxDsVSCacItsAOpnImp
          DataSetName = 'frxDsVSCacItsAOpnImp'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSCacItsAOpnImp."Pecas"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreap2: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2'
          DataSet = frxDsVSCacItsAOpnImp
          DataSetName = 'frxDsVSCacItsAOpnImp'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSCacItsAOpnImp."AreaP2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeValAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2'
          DataSet = frxDsVSCacItsAOpnImp
          DataSetName = 'frxDsVSCacItsAOpnImp'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSCacItsAOpnImp."AreaM2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 151.181151180000000000
          Height = 15.118110240000000000
          DataField = 'DataHora'
          DataSet = frxDsVSCacItsAOpnImp
          DataSetName = 'frxDsVSCacItsAOpnImp'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsVSCacItsAOpnImp."DataHora"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 192.755834720000000000
          Height = 15.118110240000000000
          DataField = 'NoRev'
          DataSet = frxDsVSCacItsAOpnImp
          DataSetName = 'frxDsVSCacItsAOpnImp'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsVSCacItsAOpnImp."NoRev"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 276.000000000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236230240000000000
        Top = 224.000000000000000000
        Width = 680.315400000000000000
        object Me_FTT: TfrxMemoView
          AllowVectorExport = True
          Top = 15.118120000000400000
          Width = 302.362204720000000000
          Height = 15.118110236220500000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL: ')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaP2: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 15.118120000000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSCacItsAOpnImp."AreaP2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTAreaM2: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 15.118120000000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSCacItsAOpnImp."AreaM2">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeSuTValorT: TfrxMemoView
          AllowVectorExport = True
          Left = 302.362400000000000000
          Top = 15.118120000000010000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsVSCacItsAOpnImp."Pecas">,MD002,1)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 15.118120000000010000
          Width = 151.181151180000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsVSPallet: TfrxDBDataset
    UserName = 'frxDsVSPallet'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'Empresa=Empresa'
      'NO_EMPRESA=NO_EMPRESA'
      'Status=Status'
      'CliStat=CliStat'
      'GraGruX=GraGruX'
      'NO_CLISTAT=NO_CLISTAT'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_STATUS=NO_STATUS'
      'DtHrEndAdd=DtHrEndAdd'
      'DtHrEndAdd_TXT=DtHrEndAdd_TXT'
      'QtdPrevPc=QtdPrevPc'
      'GerRclCab=GerRclCab'
      'DtHrFimRcl=DtHrFimRcl'
      'MovimIDGer=MovimIDGer'
      'ClientMO=ClientMO')
    DataSet = QrVSPallet
    BCDToCurrency = False
    DataSetOptions = []
    Left = 236
    Top = 160
  end
  object QrVSCacItsAOpnImp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM vscacitsa'
      'WHERE VSPallet=17')
    Left = 884
    Top = 8
    object QrVSCacItsAOpnImpCacCod: TIntegerField
      FieldName = 'CacCod'
    end
    object QrVSCacItsAOpnImpCacID: TIntegerField
      FieldName = 'CacID'
    end
    object QrVSCacItsAOpnImpCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSCacItsAOpnImpControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrVSCacItsAOpnImpClaAPalOri: TIntegerField
      FieldName = 'ClaAPalOri'
    end
    object QrVSCacItsAOpnImpRclAPalOri: TIntegerField
      FieldName = 'RclAPalOri'
    end
    object QrVSCacItsAOpnImpVSPaClaIts: TIntegerField
      FieldName = 'VSPaClaIts'
    end
    object QrVSCacItsAOpnImpVSPaRclIts: TIntegerField
      FieldName = 'VSPaRclIts'
    end
    object QrVSCacItsAOpnImpVSPallet: TIntegerField
      FieldName = 'VSPallet'
    end
    object QrVSCacItsAOpnImpVMI_Sorc: TIntegerField
      FieldName = 'VMI_Sorc'
    end
    object QrVSCacItsAOpnImpVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
    object QrVSCacItsAOpnImpBox: TIntegerField
      FieldName = 'Box'
    end
    object QrVSCacItsAOpnImpPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSCacItsAOpnImpAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSCacItsAOpnImpAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSCacItsAOpnImpRevisor: TIntegerField
      FieldName = 'Revisor'
    end
    object QrVSCacItsAOpnImpDigitador: TIntegerField
      FieldName = 'Digitador'
    end
    object QrVSCacItsAOpnImpDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSCacItsAOpnImpAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSCacItsAOpnImpAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSCacItsAOpnImpSumido: TSmallintField
      FieldName = 'Sumido'
    end
    object QrVSCacItsAOpnImpRclAPalDst: TIntegerField
      FieldName = 'RclAPalDst'
    end
    object QrVSCacItsAOpnImpVMI_Baix: TIntegerField
      FieldName = 'VMI_Baix'
    end
    object QrVSCacItsAOpnImpMartelo: TIntegerField
      FieldName = 'Martelo'
    end
    object QrVSCacItsAOpnImpFrmaIns: TSmallintField
      FieldName = 'FrmaIns'
    end
    object QrVSCacItsAOpnImpSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
    object QrVSCacItsAOpnImpNoRev: TWideStringField
      FieldName = 'NoRev'
      Size = 100
    end
    object QrVSCacItsAOpnImpNoDig: TWideStringField
      FieldName = 'NoDig'
      Size = 100
    end
  end
  object Qr1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 564
    Top = 464
  end
  object PMNumero: TPopupMenu
    Left = 92
    Top = 1
    object NdoPallet1: TMenuItem
      Caption = 'N'#186' do Pallet'
      OnClick = NdoPallet1Click
    end
    object NdeidentificaodoSIFDIPOA1: TMenuItem
      Caption = 'N'#186' de identifica'#231#227'o do SIF/DIPOA'
      OnClick = NdeidentificaodoSIFDIPOA1Click
    end
  end
end
