object FmVSClassifOneNw3: TFmVSClassifOneNw3
  Left = 0
  Top = 0
  Caption = 
    'WET-CURTI-015 :: Classifica'#231#227'o de Artigo de Ribeira Couro a Cour' +
    'o'
  ClientHeight = 741
  ClientWidth = 1924
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object PanelOC: TPanel
    Left = 0
    Top = 639
    Width = 1924
    Height = 102
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object PnInfoOC: TPanel
      Left = 0
      Top = 0
      Width = 1452
      Height = 102
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object LaCacCod: TLabel
        Left = 4
        Top = 8
        Width = 31
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'OC:'
        FocusControl = DBEdCodigo
      end
      object Label6: TLabel
        Left = 172
        Top = 8
        Width = 80
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Empresa:'
        FocusControl = DBEdit19
      end
      object Label22: TLabel
        Left = 520
        Top = 8
        Width = 45
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = #193'rea:'
        FocusControl = DBEdit14
      end
      object Label13: TLabel
        Left = 612
        Top = 8
        Width = 54
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'IME-I:'
        FocusControl = DBEdit5
      end
      object Label14: TLabel
        Left = 792
        Top = 8
        Width = 142
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Artigo de ribeira:'
        FocusControl = DBEdit6
      end
      object Label16: TLabel
        Left = 4
        Top = 40
        Width = 54
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Pe'#231'as:'
        FocusControl = DBEdit9
      end
      object Label17: TLabel
        Left = 192
        Top = 40
        Width = 123
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Peso (origem):'
        FocusControl = DBEdit10
      end
      object Label20: TLabel
        Left = 444
        Top = 40
        Width = 76
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = #193'rea m'#178':'
        FocusControl = DBEdit12
      end
      object Label21: TLabel
        Left = 680
        Top = 40
        Width = 72
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = #193'rea ft'#178':'
        FocusControl = DBEdit13
      end
      object Label26: TLabel
        Left = 920
        Top = 40
        Width = 94
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Ficha RMP:'
        FocusControl = DBEdit18
      end
      object Label24: TLabel
        Left = 1216
        Top = 40
        Width = 102
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Fornecedor:'
        FocusControl = DBEdit16
      end
      object Label18: TLabel
        Left = 4
        Top = 72
        Width = 291
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Observa'#231#227'o sobre o artigo gerado:'
        FocusControl = DBEdit11
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 44
        Top = 4
        Width = 120
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'CacCod'
        DataSource = DsVSPaClaCab
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit19: TDBEdit
        Left = 252
        Top = 4
        Width = 60
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Empresa'
        DataSource = DsVSPaClaCab
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 312
        Top = 4
        Width = 206
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_EMPRESA'
        DataSource = DsVSPaClaCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object DBEdit14: TDBEdit
        Left = 568
        Top = 4
        Width = 40
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_TIPO'
        DataSource = DsVSPaClaCab
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 668
        Top = 4
        Width = 120
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Controle'
        DataSource = DsVSGerArtNew
        PopupMenu = PMIMEI
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 940
        Top = 4
        Width = 60
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'GraGruX'
        DataSource = DsVSGerArtNew
        TabOrder = 5
      end
      object DBEdit7: TDBEdit
        Left = 1000
        Top = 4
        Width = 430
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_PRD_TAM_COR'
        DataSource = DsVSGerArtNew
        TabOrder = 6
      end
      object DBEdit9: TDBEdit
        Left = 64
        Top = 36
        Width = 120
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Pecas'
        DataSource = DsVSGerArtNew
        TabOrder = 7
      end
      object DBEdit10: TDBEdit
        Left = 320
        Top = 36
        Width = 120
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'QtdAntPeso'
        DataSource = DsVSGerArtNew
        TabOrder = 8
      end
      object DBEdit12: TDBEdit
        Left = 524
        Top = 36
        Width = 150
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'AreaM2'
        DataSource = DsVSGerArtNew
        TabOrder = 9
      end
      object DBEdit13: TDBEdit
        Left = 760
        Top = 36
        Width = 150
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'AreaP2'
        DataSource = DsVSGerArtNew
        TabOrder = 10
      end
      object DBEdit18: TDBEdit
        Left = 1020
        Top = 36
        Width = 190
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_FICHA'
        DataSource = DsVSGerArtNew
        TabOrder = 11
      end
      object DBEdit16: TDBEdit
        Left = 1324
        Top = 36
        Width = 106
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Terceiro'
        DataSource = DsVSGerArtNew
        TabOrder = 12
      end
      object DBEdit11: TDBEdit
        Left = 300
        Top = 68
        Width = 1130
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Observ'
        DataSource = DsVSGerArtNew
        TabOrder = 13
      end
    end
    object PnResponsaveis: TPanel
      Left = 1452
      Top = 0
      Width = 472
      Height = 102
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvLowered
      TabOrder = 1
      object Label71: TLabel
        Left = 4
        Top = 8
        Width = 83
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Digitador:'
        FocusControl = EdDigitador
      end
      object Label70: TLabel
        Left = 4
        Top = 40
        Width = 109
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Classificador:'
        FocusControl = EdRevisor
      end
      object Label85: TLabel
        Left = 300
        Top = 72
        Width = 65
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        AutoSize = False
        Caption = 'Tempo:'
        FocusControl = EdTempo
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object CBRevisor: TdmkDBLookupComboBox
        Left = 180
        Top = 36
        Width = 286
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsRevisores
        TabOrder = 0
        dmkEditCB = EdRevisor
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CBDigitador: TdmkDBLookupComboBox
        Left = 180
        Top = 4
        Width = 286
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsDigitadores
        TabOrder = 1
        dmkEditCB = EdDigitador
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdDigitador: TdmkEditCB
        Left = 124
        Top = 4
        Width = 56
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdDigitadorRedefinido
        DBLookupComboBox = CBDigitador
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdRevisor: TdmkEditCB
        Left = 124
        Top = 36
        Width = 56
        Height = 29
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnRedefinido = EdRevisorRedefinido
        DBLookupComboBox = CBRevisor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdTempo: TEdit
        Left = 368
        Top = 68
        Width = 96
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        Text = '0,000'
      end
      object BitBtn1: TBitBtn
        Left = 4
        Top = 68
        Width = 76
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Reabre'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        OnClick = BitBtn1Click
      end
      object EdAll: TdmkEdit
        Left = 84
        Top = 68
        Width = 96
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'X seg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'X seg'
        ValWarn = False
      end
      object EdArrAll: TdmkEdit
        Left = 184
        Top = 68
        Width = 96
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'X seg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'X seg'
        ValWarn = False
      end
    end
  end
  object PnAll: TPanel
    Left = 1604
    Top = 92
    Width = 320
    Height = 547
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alRight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object Splitter1: TSplitter
      Left = 1
      Top = 117
      Width = 318
      Height = 5
      Cursor = crVSplit
      Align = alTop
    end
    object SGAll: TStringGrid
      Left = 1
      Top = 122
      Width = 318
      Height = 335
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      ColCount = 13
      DefaultRowHeight = 28
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnKeyDown = SGAllKeyDown
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 318
      Height = 116
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 1
      object SGDefeiDefin: TStringGrid
        Left = 1
        Top = 1
        Width = 316
        Height = 114
        Align = alClient
        ColCount = 8
        DefaultColWidth = 40
        DefaultRowHeight = 21
        RowCount = 2
        TabOrder = 0
        OnKeyDown = SGDefeiDefinKeyDown
        ColWidths = (
          40
          40
          40
          40
          125
          40
          40
          40)
      end
    end
    object Memo2: TMemo
      Left = 1
      Top = 457
      Width = 318
      Height = 89
      Align = alBottom
      TabOrder = 2
      Visible = False
    end
  end
  object PnInfoBig: TPanel
    Left = 0
    Top = 0
    Width = 1924
    Height = 92
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -17
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PnDigitacao: TPanel
      Left = 1390
      Top = 1
      Width = 533
      Height = 90
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Enabled = False
      TabOrder = 0
      object Label12: TLabel
        Left = 1
        Top = 1
        Width = 531
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Alignment = taCenter
        ExplicitWidth = 5
      end
      object Label4: TLabel
        Left = 194
        Top = 0
        Width = 156
        Height = 21
        Caption = #39'F5'#39' Informa defeito! '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object PnBox: TPanel
        Left = 141
        Top = 22
        Width = 52
        Height = 67
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Label2: TLabel
          Left = 0
          Top = 0
          Width = 52
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Caption = 'Box:'
          ExplicitWidth = 33
        end
        object EdBox: TdmkEdit
          Left = 0
          Top = 21
          Width = 52
          Height = 46
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 2
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdBoxChange
          OnKeyDown = EdBoxKeyDown
        end
      end
      object PnArea: TPanel
        Left = 1
        Top = 22
        Width = 140
        Height = 67
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 0
          Top = 0
          Width = 140
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Caption = #193'rea: '
          ExplicitWidth = 46
        end
        object LaTipoArea: TLabel
          Left = 105
          Top = 21
          Width = 35
          Height = 46
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Caption = '?'#178
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ExplicitHeight = 39
        end
        object EdArea: TdmkEdit
          Left = 0
          Top = 21
          Width = 105
          Height = 46
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdAreaChange
          OnEnter = EdAreaEnter
          OnKeyUp = EdAreaKeyUp
          OnRedefinido = EdAreaRedefinido
        end
      end
      object PnMartelo: TPanel
        Left = 275
        Top = 22
        Width = 257
        Height = 67
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 3
        Visible = False
        object Label72: TLabel
          Left = 0
          Top = 0
          Width = 257
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Caption = 'Martelo:'
          ExplicitWidth = 62
        end
        object CBVSMrtCad: TdmkDBLookupComboBox
          Left = 84
          Top = 21
          Width = 173
          Height = 47
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          KeyField = 'Nome'
          ListField = 'Nome'
          ListSource = DsVSMrtCad
          ParentFont = False
          TabOrder = 1
          TabStop = False
          dmkEditCB = EdVSMrtCad
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdVSMrtCad: TdmkEditCB
          Left = 0
          Top = 21
          Width = 84
          Height = 46
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnKeyDown = EdVSMrtCadKeyDown
          OnRedefinido = EdVSMrtCadRedefinido
          DBLookupComboBox = CBVSMrtCad
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
      end
      object PnSubClass: TPanel
        Left = 193
        Top = 22
        Width = 82
        Height = 67
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
        Visible = False
        object LaSubClass: TLabel
          Left = 0
          Top = 0
          Width = 82
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Caption = 'SubClas:'
          ExplicitWidth = 64
        end
        object EdSubClass: TdmkEdit
          Left = 0
          Top = 21
          Width = 82
          Height = 46
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          MaxLength = 10
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdSubClassChange
          OnExit = EdSubClassExit
          OnKeyDown = EdSubClassKeyDown
        end
      end
      object CkInverte: TCheckBox
        Left = 4
        Top = 4
        Width = 189
        Height = 17
        Caption = 'Digitar em grandeza invertida.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
      end
    end
    object PnJaClass: TPanel
      Left = 321
      Top = 1
      Width = 320
      Height = 90
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object Label15: TLabel
        Left = 1
        Top = 1
        Width = 318
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Alignment = taCenter
        Caption = 'Couros j'#225' classificados'
        ExplicitWidth = 168
      end
      object Panel10: TPanel
        Left = 1
        Top = 22
        Width = 120
        Height = 67
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label10: TLabel
          Left = 0
          Top = 0
          Width = 120
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Caption = ' Int. ou 1/2:'
          FocusControl = DBEdJaFoi_PECA
          ExplicitWidth = 90
        end
        object DBEdJaFoi_PECA: TDBEdit
          Left = 0
          Top = 21
          Width = 120
          Height = 46
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          Align = alClient
          DataField = 'JaFoi_PECA'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
      object Panel9: TPanel
        Left = 121
        Top = 22
        Width = 198
        Height = 67
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label9: TLabel
          Left = 0
          Top = 0
          Width = 198
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Caption = ' '#193'rea:'
          FocusControl = DBEdJaFoi_AREA
          ExplicitWidth = 46
        end
        object DBEdJaFoi_AREA: TDBEdit
          Left = 0
          Top = 21
          Width = 198
          Height = 46
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          Align = alClient
          DataField = 'JaFoi_AREA'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
    end
    object PnNaoClass: TPanel
      Left = 1
      Top = 1
      Width = 320
      Height = 90
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 2
      object Label19: TLabel
        Left = 1
        Top = 1
        Width = 318
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        Caption = 'Couros que faltam classificar'
        ExplicitWidth = 213
      end
      object PnIntMei: TPanel
        Left = 1
        Top = 22
        Width = 120
        Height = 67
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label23: TLabel
          Left = 0
          Top = 0
          Width = 120
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Caption = ' Int. ou 1/2:'
          FocusControl = DBEdSdoVrtPeca
          ExplicitWidth = 90
        end
        object DBEdSdoVrtPeca: TDBEdit
          Left = 0
          Top = 21
          Width = 120
          Height = 46
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          Align = alClient
          DataField = 'SdoVrtPeca'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnChange = DBEdSdoVrtPecaChange
          ExplicitHeight = 47
        end
      end
      object Panel15: TPanel
        Left = 121
        Top = 22
        Width = 198
        Height = 67
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Label25: TLabel
          Left = 0
          Top = 0
          Width = 198
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Caption = ' '#193'rea:'
          FocusControl = DBEdSdoVrtArM2
          ExplicitWidth = 46
        end
        object DBEdSdoVrtArM2: TDBEdit
          Left = 0
          Top = 21
          Width = 198
          Height = 46
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          Align = alClient
          DataField = 'SdoVrtArM2'
          DataSource = DsSumT
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
    end
    object PnMenu: TPanel
      Left = 641
      Top = 1
      Width = 749
      Height = 90
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 3
      object CkSubClass: TCheckBox
        Left = 524
        Top = 32
        Width = 114
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Sub Classe'
        TabOrder = 0
        OnClick = CkSubClassClick
      end
      object CkMartelo: TCheckBox
        Left = 524
        Top = 8
        Width = 98
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Martelo'
        TabOrder = 1
        OnClick = CkMarteloClick
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 444
        Top = 4
        Width = 72
        Height = 72
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = BtReabreClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 224
        Top = 4
        Width = 216
        Height = 72
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Imprimir'
        TabOrder = 3
        TabStop = False
        OnClick = BtImprimeClick
      end
      object BtEncerra: TBitBtn
        Tag = 10134
        Left = 4
        Top = 4
        Width = 216
        Height = 72
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Menu'
        TabOrder = 4
        TabStop = False
        OnClick = BtEncerraClick
      end
      object EdItens: TEdit
        Left = 652
        Top = 68
        Width = 78
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        Text = 'EdItens'
      end
    end
  end
  object PnExtras: TPanel
    Left = 0
    Top = 92
    Width = 234
    Height = 547
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 3
    object PnEqualize: TPanel
      Left = 0
      Top = 0
      Width = 234
      Height = 547
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnCfgEqz: TPanel
        Left = 0
        Top = 0
        Width = 234
        Height = 45
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object SbEqualize: TSpeedButton
          Left = 100
          Top = 20
          Width = 24
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          OnClick = SbEqualizeClick
        end
        object Label88: TLabel
          Left = 4
          Top = 0
          Width = 68
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Equ'#225'lize:'
          FocusControl = EdEqualize
        end
        object EdEqualize: TdmkEdit
          Left = 4
          Top = 20
          Width = 93
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object CkEqualize: TCheckBox
          Left = 132
          Top = 2
          Width = 100
          Height = 18
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Auto.'
          TabOrder = 1
        end
        object CkNota: TCheckBox
          Left = 132
          Top = 22
          Width = 100
          Height = 18
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Nota.'
          TabOrder = 2
          OnClick = CkNotaClick
        end
      end
      object DBGNotaEqz: TDBGrid
        Left = 0
        Top = 85
        Width = 234
        Height = 462
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SubClass'
            Title.Caption = 'Sub classe'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PercM2'
            Title.Caption = '% m'#178
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pecas'
            Title.Caption = 'Pe'#231'as'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AreaM2'
            Title.Caption = #193'rea m'#178
            Visible = True
          end>
      end
      object PnNota: TPanel
        Left = 0
        Top = 45
        Width = 234
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        TabOrder = 2
        Visible = False
        object DBEdNotaEqzM2: TDBEdit
          Left = 1
          Top = 1
          Width = 232
          Height = 38
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataField = 'NotaEqzM2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Font.Quality = fqAntialiased
          ParentFont = False
          TabOrder = 0
          ExplicitHeight = 47
        end
      end
      object Memo1: TMemo
        Left = 20
        Top = 348
        Width = 186
        Height = 90
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Lines.Strings = (
          'Memo1')
        TabOrder = 3
        Visible = False
      end
    end
  end
  object GPBoxes: TGridPanel
    Left = 234
    Top = 92
    Width = 1370
    Height = 547
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Caption = '...'
    ColumnCollection = <
      item
        Value = 50.000000000000000000
      end
      item
        Value = 50.000000000000000000
      end>
    ControlCollection = <>
    RowCollection = <
      item
        Value = 50.000000000000000000
      end
      item
        Value = 50.000000000000000000
      end>
    TabOrder = 4
  end
  object QrVSPaClaCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrVSPaClaCabBeforeClose
    AfterScroll = QrVSPaClaCabAfterScroll
    OnCalcFields = QrVSPaClaCabCalcFields
    SQL.Strings = (
      'SELECT vga.GraGruX, vga.Nome, vga.DtHrAberto,'
      'vga.TipoArea, vga.Empresa, vga.MovimCod, pcc.*,'
      'CONCAT(gg1.Nome,'
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)),'
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))'
      'NO_PRD_TAM_COR,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA'
      'FROM vspaclacaba pcc'
      'LEFT JOIN vsgerarta  vga ON vga.Codigo=pcc.vsgerart'
      'LEFT JOIN gragrux    ggx ON ggx.Controle=vga.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN entidades  emp ON emp.Codigo=vga.Empresa')
    Left = 328
    Top = 104
    object QrVSPaClaCabNO_TIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO'
      Size = 10
      Calculated = True
    end
    object QrVSPaClaCabGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'vsgerarta.GraGruX'
    end
    object QrVSPaClaCabNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'vsgerarta.Nome'
      Size = 100
    end
    object QrVSPaClaCabTipoArea: TSmallintField
      FieldName = 'TipoArea'
      Origin = 'vsgerarta.TipoArea'
    end
    object QrVSPaClaCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'vsgerarta.Empresa'
    end
    object QrVSPaClaCabMovimCod: TIntegerField
      FieldName = 'MovimCod'
      Origin = 'vsgerarta.MovimCod'
    end
    object QrVSPaClaCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'vspaclacaba.Codigo'
    end
    object QrVSPaClaCabVSGerArt: TIntegerField
      FieldName = 'VSGerArt'
      Origin = 'vspaclacaba.VSGerArt'
    end
    object QrVSPaClaCabLstPal01: TIntegerField
      FieldName = 'LstPal01'
      Origin = 'vspaclacaba.LstPal01'
    end
    object QrVSPaClaCabLstPal02: TIntegerField
      FieldName = 'LstPal02'
      Origin = 'vspaclacaba.LstPal02'
    end
    object QrVSPaClaCabLstPal03: TIntegerField
      FieldName = 'LstPal03'
      Origin = 'vspaclacaba.LstPal03'
    end
    object QrVSPaClaCabLstPal04: TIntegerField
      FieldName = 'LstPal04'
      Origin = 'vspaclacaba.LstPal04'
    end
    object QrVSPaClaCabLstPal05: TIntegerField
      FieldName = 'LstPal05'
      Origin = 'vspaclacaba.LstPal05'
    end
    object QrVSPaClaCabLstPal06: TIntegerField
      FieldName = 'LstPal06'
      Origin = 'vspaclacaba.LstPal06'
    end
    object QrVSPaClaCabLstPal07: TIntegerField
      FieldName = 'LstPal07'
      Origin = 'vspaclacaba.LstPal07'
    end
    object QrVSPaClaCabLstPal08: TIntegerField
      FieldName = 'LstPal08'
      Origin = 'vspaclacaba.LstPal08'
    end
    object QrVSPaClaCabLstPal09: TIntegerField
      FieldName = 'LstPal09'
      Origin = 'vspaclacaba.LstPal09'
    end
    object QrVSPaClaCabLstPal10: TIntegerField
      FieldName = 'LstPal10'
      Origin = 'vspaclacaba.LstPal10'
    end
    object QrVSPaClaCabLstPal11: TIntegerField
      FieldName = 'LstPal11'
      Origin = 'vspaclacaba.LstPal11'
    end
    object QrVSPaClaCabLstPal12: TIntegerField
      FieldName = 'LstPal12'
      Origin = 'vspaclacaba.LstPal12'
    end
    object QrVSPaClaCabLstPal13: TIntegerField
      FieldName = 'LstPal13'
      Origin = 'vspaclacaba.LstPal13'
    end
    object QrVSPaClaCabLstPal14: TIntegerField
      FieldName = 'LstPal14'
      Origin = 'vspaclacaba.LstPal14'
    end
    object QrVSPaClaCabLstPal15: TIntegerField
      FieldName = 'LstPal15'
      Origin = 'vspaclacaba.LstPal15'
    end
    object QrVSPaClaCabLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'vspaclacaba.Lk'
    end
    object QrVSPaClaCabDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'vspaclacaba.DataCad'
    end
    object QrVSPaClaCabDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'vspaclacaba.DataAlt'
    end
    object QrVSPaClaCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'vspaclacaba.UserCad'
    end
    object QrVSPaClaCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'vspaclacaba.UserAlt'
    end
    object QrVSPaClaCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'vspaclacaba.AlterWeb'
    end
    object QrVSPaClaCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'vspaclacaba.Ativo'
    end
    object QrVSPaClaCabNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSPaClaCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrVSPaClaCabVSMovIts: TIntegerField
      FieldName = 'VSMovIts'
      Origin = 'vspaclacaba.VSMovIts'
    end
    object QrVSPaClaCabCacCod: TIntegerField
      FieldName = 'CacCod'
      Origin = 'vspaclacaba.CacCod'
    end
    object QrVSPaClaCabDtHrFimCla: TDateTimeField
      FieldName = 'DtHrFimCla'
      Origin = 'vspaclacaba.DtHrFimCla'
    end
    object QrVSPaClaCabDtHrAberto: TDateTimeField
      FieldName = 'DtHrAberto'
      Origin = 'vsgerarta.DtHrAberto'
    end
  end
  object DsVSPaClaCab: TDataSource
    DataSet = QrVSPaClaCab
    Left = 328
    Top = 152
  end
  object QrVSGerArtNew: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrVSGerArtNewAfterOpen
    SQL.Strings = (
      'SELECT wmi.*, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, wbp.Nome NO_Pallet '
      'FROM vsmovits wmi '
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX '
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN vspallet   wbp ON wbp.Codigo=wmi.Pallet ')
    Left = 420
    Top = 105
    object QrVSGerArtNewCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSGerArtNewControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSGerArtNewMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSGerArtNewEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSGerArtNewMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSGerArtNewGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSGerArtNewPecas: TFloatField
      FieldName = 'Pecas'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSGerArtNewDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSGerArtNewDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSGerArtNewUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSGerArtNewUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSGerArtNewAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSGerArtNewAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSGerArtNewNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrVSGerArtNewSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSGerArtNewSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSGerArtNewSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSGerArtNewPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSGerArtNewNO_PALLET: TWideStringField
      FieldName = 'NO_PALLET'
      Size = 60
    end
    object QrVSGerArtNewSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrVSGerArtNewValorT: TFloatField
      FieldName = 'ValorT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSGerArtNewCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrVSGerArtNewMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSGerArtNewTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSGerArtNewCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSGerArtNewLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSGerArtNewFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSGerArtNewLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSGerArtNewMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSGerArtNewCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSGerArtNewValorMP: TFloatField
      FieldName = 'ValorMP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSGerArtNewDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSGerArtNewDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSGerArtNewQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrVSGerArtNewQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrVSGerArtNewNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
    object QrVSGerArtNewNO_FICHA: TWideStringField
      FieldName = 'NO_FICHA'
    end
    object QrVSGerArtNewCUSTO_M2: TFloatField
      FieldName = 'CUSTO_M2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSGerArtNewCUSTO_P2: TFloatField
      FieldName = 'CUSTO_P2'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrVSGerArtNewSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSGerArtNewNO_SerieFch: TWideStringField
      FieldName = 'NO_SerieFch'
      Size = 60
    end
    object QrVSGerArtNewFatorInt: TFloatField
      FieldName = 'FatorInt'
    end
    object QrVSGerArtNewVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSGerArtNewClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVSGerArtNewStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrVSGerArtNewFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVSGerArtNewMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSGerArtNewMediaMinM2: TFloatField
      FieldName = 'MediaMinM2'
    end
    object QrVSGerArtNewMediaMaxM2: TFloatField
      FieldName = 'MediaMaxM2'
    end
    object QrVSGerArtNewMedEClas: TSmallintField
      FieldName = 'MedEClas'
    end
  end
  object DsVSGerArtNew: TDataSource
    DataSet = QrVSGerArtNew
    Left = 420
    Top = 153
  end
  object DsSumT: TDataSource
    DataSet = QrSumT
    Left = 328
    Top = 248
  end
  object DsAll: TDataSource
    Left = 328
    Top = 340
  end
  object QrSumVMI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 1552
    Top = 612
    object QrSumVMIPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumVMIAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumVMIAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object PMEncerra: TPopupMenu
    OnPopup = PMEncerraPopup
    Left = 504
    Top = 104
    object EstaOCOrdemdeclassificao1: TMenuItem
      Caption = 'Encerra esta OC (Ordem de classifica'#231#227'o)'
      OnClick = EstaOCOrdemdeclassificao1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Palletdobox01: TMenuItem
      Tag = 1
      Caption = 'Encerra o pallet do box 0&1'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox02: TMenuItem
      Tag = 2
      Caption = 'Encerra o pallet do box 0&2'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox03: TMenuItem
      Tag = 3
      Caption = 'Encerra o pallet do box 0&3'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox04: TMenuItem
      Tag = 4
      Caption = 'Encerra o pallet do box 0&4'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox05: TMenuItem
      Tag = 5
      Caption = 'Encerra o pallet do box 0&5'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox06: TMenuItem
      Tag = 6
      Caption = 'Encerra o pallet do box 0&6'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox07: TMenuItem
      Tag = 7
      Caption = 'Encerra o pallet do box 0&7'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox08: TMenuItem
      Tag = 8
      Caption = 'Encerra o pallet do box 0&8'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox09: TMenuItem
      Tag = 9
      Caption = 'Encerra o pallet do box 0&9'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox10: TMenuItem
      Tag = 10
      Caption = 'Encerra o pallet do box 10'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox11: TMenuItem
      Tag = 11
      Caption = 'Encerra o pallet do box 11'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox12: TMenuItem
      Tag = 12
      Caption = 'Encerra o pallet do box 12'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox13: TMenuItem
      Tag = 13
      Caption = 'Encerra o pallet do box 13'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox14: TMenuItem
      Tag = 14
      Caption = 'Encerra o pallet do box 14'
      OnClick = EncerrarPalletClick
    end
    object Palletdobox15: TMenuItem
      Tag = 15
      Caption = 'Encerra o pallet do box 15'
      OnClick = EncerrarPalletClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Mostrartodosboxes1: TMenuItem
      Caption = 'Mostrar todos boxes'
      OnClick = Mostrartodosboxes1Click
    end
    object ImprimirNmeroPallet1: TMenuItem
      Caption = 'Imprimir N'#250'mero Pallet'
      Enabled = False
      OnClick = ImprimirNmeroPallet1Click
    end
    object ImprimirfluxodemovimentodoIMEI1: TMenuItem
      Caption = 'Imprimir fluxo de movimento do IMEI'
      OnClick = ImprimirfluxodemovimentodoIMEI1Click
    end
    object DadosPaletsemWordPad1: TMenuItem
      Caption = 'Dados Palets no Paint'
      OnClick = DadosPaletsemWordPad1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object rocarVichaRMP1: TMenuItem
      Caption = 'Trocar Ficha RMP'
      Enabled = False
      OnClick = rocarVichaRMP1Click
    end
    object rocarIMEI1: TMenuItem
      Caption = 'Trocar IME-I'
      OnClick = rocarIMEI1Click
    end
    object AumentarBoxesdisponveis1: TMenuItem
      Caption = '&Aumentar Boxes dispon'#237'veis'
      OnClick = AumentarBoxesdisponveis1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object TesteInclusao1: TMenuItem
      Caption = '&Teste inclus'#227'o'
      OnClick = TesteInclusao1Click
    end
    object VerTextoMemo1: TMenuItem
      Caption = 'Ver Texto Memo'
      Visible = False
      OnClick = VerTextoMemo1Click
    end
    object ApagarTextoMemo1: TMenuItem
      Caption = 'Apagar Texto Memo'
      Visible = False
      OnClick = ApagarTextoMemo1Click
    end
  end
  object QrRevisores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece5="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 828
    Top = 108
    object QrRevisoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRevisoresNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsRevisores: TDataSource
    DataSet = QrRevisores
    Left = 828
    Top = 152
  end
  object QrDigitadores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE'
      'FROM entidades ent'
      'WHERE ent.Fornece5="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 908
    Top = 112
    object QrDigitadoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDigitadoresNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsDigitadores: TDataSource
    DataSet = QrDigitadores
    Left = 908
    Top = 156
  end
  object QrVMIsDePal: TMySQLQuery
    Database = Dmod.MyDB
    Left = 420
    Top = 204
    object QrVMIsDePalVMI_Dest: TIntegerField
      FieldName = 'VMI_Dest'
    end
  end
  object PMAll: TPopupMenu
    Left = 504
    Top = 148
    object Alteraitematual1: TMenuItem
      Caption = '&Altera item atual'
      OnClick = Alteraitematual1Click
    end
    object Excluiitematual1: TMenuItem
      Caption = '&Exclui item atual'
      OnClick = Excluiitematual1Click
    end
  end
  object QrVSMrtCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM vsmrtcad'
      'ORDER BY Nome')
    Left = 752
    Top = 108
    object QrVSMrtCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMrtCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSMrtCad: TDataSource
    DataSet = QrVSMrtCad
    Left = 752
    Top = 152
  end
  object QrSumDest1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 588
    Top = 104
  end
  object QrSumSorc1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 592
    Top = 152
  end
  object QrVMISorc1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 672
    Top = 104
  end
  object QrPalSorc1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 672
    Top = 152
  end
  object QrNotaAll: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaAll,'
      'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) NotaBrutaM2,'
      
        'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) / vec.BasNotZero' +
        ' * 100 NotaEqzM2'
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vei ON vei.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vec ON vec.Codigo=vei.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vec.Codigo=1')
    Left = 12
    Top = 216
    object QrNotaAllPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotaAllAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaAll: TFloatField
      FieldName = 'NotaAll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaBrutaM2: TFloatField
      FieldName = 'NotaBrutaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaAllNotaEqzM2: TFloatField
      FieldName = 'NotaEqzM2'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsNotaAll: TDataSource
    DataSet = QrNotaAll
    Left = 12
    Top = 260
  end
  object QrNotaCrr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaAll,'
      'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) NotaBrutaM2,'
      
        'SUM(cia.AreaM2 * vez.BasNota) / SUM(cia.AreaM2) / vec.BasNotZero' +
        ' * 100 NotaEqzM2'
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vei ON vei.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vec ON vec.Codigo=vei.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vec.Codigo=1')
    Left = 72
    Top = 216
    object QrNotaCrrPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotaCrrAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaAll: TFloatField
      FieldName = 'NotaAll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaBrutaM2: TFloatField
      FieldName = 'NotaBrutaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNotaCrrNotaEqzM2: TFloatField
      FieldName = 'NotaEqzM2'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsNotaCrr: TDataSource
    DataSet = QrNotaCrr
    Left = 72
    Top = 260
  end
  object QrNotas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cia.SubClass, SUM(cia.Pecas) Pecas, '
      'SUM(cia.AreaM2) AreaM2, SUM(cia.AreaP2) AreaP2, '
      'vez.BasNota, SUM(cia.AreaM2 * vez.BasNota) NotaM2,'
      'SUM(cia.AreaM2) / 27.0034 PercM2 '
      'FROM vscacitsa cia '
      'LEFT JOIN vseqzits vdi ON vdi.Pallet=cia.VSPallet'
      'LEFT JOIN vseqzcab vdc ON vdc.Codigo=vdi.Codigo'
      'LEFT JOIN vseqzsub vez ON vez.SubClass=cia.SubClass'
      'WHERE vdc.Codigo=1'
      'GROUP BY cia.SubClass')
    Left = 132
    Top = 216
    object QrNotasSubClass: TWideStringField
      FieldName = 'SubClass'
      Size = 10
    end
    object QrNotasPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrNotasAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasAreaP2: TFloatField
      FieldName = 'AreaP2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasBasNota: TFloatField
      FieldName = 'BasNota'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasNotaM2: TFloatField
      FieldName = 'NotaM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrNotasPercM2: TFloatField
      FieldName = 'PercM2'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsNotas: TDataSource
    DataSet = QrNotas
    Left = 132
    Top = 260
  end
  object QrSumT: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSumTCalcFields
    SQL.Strings = (
      'SELECT Controle, Pecas, AreaM2, AreaP2 '
      'FROM vsgerapala '
      'WHERE Codigo=5 '
      'AND VSPaClaIts=1 '
      'AND VSPallet=1 ')
    Left = 328
    Top = 204
    object QrSumTPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrSumTAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumTSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrSumTSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumTJaFoi_PECA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JaFoi_PECA'
      Calculated = True
    end
    object QrSumTJaFoi_AREA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JaFoi_AREA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object PMIMEI: TPopupMenu
    Left = 584
    Top = 836
    object ImprimirfluxodemovimentodoIMEI2: TMenuItem
      Caption = 'Imprimir fluxo de movimento do IMEI'
      OnClick = ImprimirfluxodemovimentodoIMEI2Click
    end
  end
  object DqAll: TMySQLDirectQuery
    Database = Dmod.MyDB
    Left = 32
    Top = 464
  end
  object DqItens: TMySQLDirectQuery
    Database = Dmod.MyDB
    Left = 72
    Top = 464
  end
  object DqSumPall: TMySQLDirectQuery
    Database = Dmod.MyDB
    Left = 124
    Top = 464
  end
  object Qry: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 510
    Top = 284
  end
end
