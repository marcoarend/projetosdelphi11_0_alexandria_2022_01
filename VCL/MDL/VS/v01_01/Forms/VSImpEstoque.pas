unit VSImpEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  frxClass, frxDBSet, Data.DB, mySQLDbTables, dmkGeral, DmkEditCB, dmkDBGridZTO,
  UnDmkProcFunc, AppListas, UnInternalConsts, UnProjGroup_Consts, Vcl.Grids,
  Vcl.DBGrids, UnDmkEnums, UnAppEnums;

type
  TFmVSImpEstoque = class(TForm)
    QrEstqR1: TmySQLQuery;
    QrEstqR1Empresa: TIntegerField;
    QrEstqR1GraGruX: TIntegerField;
    QrEstqR1Pecas: TFloatField;
    QrEstqR1PesoKg: TFloatField;
    QrEstqR1AreaM2: TFloatField;
    QrEstqR1AreaP2: TFloatField;
    QrEstqR1ValorT: TFloatField;
    QrEstqR1SdoVrtPeca: TFloatField;
    QrEstqR1SdoVrtPeso: TFloatField;
    QrEstqR1SdoVrtArM2: TFloatField;
    QrEstqR1PalVrtPeca: TFloatField;
    QrEstqR1PalVrtPeso: TFloatField;
    QrEstqR1PalVrtArM2: TFloatField;
    QrEstqR1LmbVrtPeca: TFloatField;
    QrEstqR1LmbVrtPeso: TFloatField;
    QrEstqR1LmbVrtArM2: TFloatField;
    QrEstqR1GraGru1: TIntegerField;
    QrEstqR1NO_PRD_TAM_COR: TWideStringField;
    QrEstqR1Pallet: TIntegerField;
    QrEstqR1NO_PALLET: TWideStringField;
    QrEstqR1Terceiro: TIntegerField;
    QrEstqR1CliStat: TIntegerField;
    QrEstqR1Status: TIntegerField;
    QrEstqR1NO_FORNECE: TWideStringField;
    QrEstqR1NO_CLISTAT: TWideStringField;
    QrEstqR1NO_EMPRESA: TWideStringField;
    QrEstqR1NO_STATUS: TWideStringField;
    QrEstqR1DataHora: TDateTimeField;
    QrEstqR1OrdGGX: TIntegerField;
    QrEstqR1OrdGGY: TIntegerField;
    QrEstqR1GraGruY: TIntegerField;
    QrEstqR1NO_GGY: TWideStringField;
    QrEstqR1NO_PalStat: TWideStringField;
    QrEstqR1Ativo: TSmallintField;
    QrEstqR1Media: TFloatField;
    QrEstqR1NO_MovimNiv: TWideStringField;
    QrEstqR1NO_MovimID: TWideStringField;
    QrEstqR1MovimNiv: TIntegerField;
    QrEstqR1MovimID: TIntegerField;
    QrEstqR1IMEC: TIntegerField;
    QrEstqR1Codigo: TIntegerField;
    QrEstqR1IMEI: TIntegerField;
    QrEstqR1SerieFch: TIntegerField;
    QrEstqR1NO_SerieFch: TWideStringField;
    QrEstqR1Ficha: TIntegerField;
    QrEstqR1Inteiros: TFloatField;
    QrEstqR1PalStat: TIntegerField;
    QrEstqR1CUS_UNIT_M2: TFloatField;
    QrEstqR1CUS_UNIT_KG: TFloatField;
    QrEstqR1ReqMovEstq: TIntegerField;
    QrEstqR1StqCenCad: TIntegerField;
    QrEstqR1NO_StqCenCad: TWideStringField;
    QrEstqR1StqCenLoc: TIntegerField;
    QrEstqR1NO_LOC_CEN: TWideStringField;
    frxDsEstqR1: TfrxDBDataset;
    DsEstqR1: TDataSource;
    dmkDBGridZTO2: TdmkDBGridZTO;
    QrEstqR1Historico: TWideStringField;
    QrEstqR1VSMulFrnCab: TIntegerField;
    QrEstqR1MulFornece: TIntegerField;
    QrEstqR1NO_MulFornece: TWideStringField;
    QrEstqR1NO_CouNiv1: TWideStringField;
    QrEstqR1NO_CouNiv2: TWideStringField;
    QrEstqR1CouNiv1: TIntegerField;
    QrEstqR1CouNiv2: TIntegerField;
    QrBarraca: TmySQLQuery;
    QrBarracaPecas: TFloatField;
    QrBarracaPesoKg: TFloatField;
    QrBarracaSdoVrtPeca: TFloatField;
    QrBarracaSdoVrtPeso: TFloatField;
    QrBarracaIMEI: TIntegerField;
    QrEmitCus: TmySQLQuery;
    QrEmitCusPecas: TFloatField;
    QrEmitCusPeso: TFloatField;
    QrBarracaEmpresa: TIntegerField;
    QrBarracaGraGruX: TIntegerField;
    QrBarracaGraGruY: TIntegerField;
    QrEmitCusGraGruX: TIntegerField;
    QrEmitCusAreaM2: TFloatField;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    QrGraGruXOrdGGY: TIntegerField;
    QrGraGruXGraGruY: TIntegerField;
    QrGraGruXNO_GGY: TWideStringField;
    QrGraGruXNO_CouNiv1: TWideStringField;
    QrGraGruXNO_CouNiv2: TWideStringField;
    QrGraGruXFatorInt: TFloatField;
    QrGraGruXCouNiv1: TLargeintField;
    QrGraGruXCouNiv2: TLargeintField;
    QrBarracaValorT: TFloatField;
    QrEmitCusCusto: TFloatField;
    frxWET_CURTI_018_00_C1: TfrxReport;
    QrEstqR1ID_UNQ: TWideStringField;
    frxWET_CURTI_018_00_C2: TfrxReport;
    QrEstqR1Grandeza: TSmallintField;
    QrEstqR1GraGruValU: TFloatField;
    QrEstqR1GraGruValT: TFloatField;
    QrEstqR1NO_GRANDEZA: TWideStringField;
    QrEstqR1GGVU_m2: TFloatField;
    QrEstqR1GGVU_kg: TFloatField;
    QrEstqR1GGVU_pc: TFloatField;
    QrEstqR1ClientMO: TIntegerField;
    QrEstqR1NFeSer: TSmallintField;
    QrEstqR1NFeNum: TIntegerField;
    QrEstqR1VSMulNFeCab: TIntegerField;
    QrEstqR1NFeAgrup: TWideStringField;
    QrEstqR1Marca: TWideStringField;
    QrEstqR1MovimCod: TIntegerField;
    QrEstqR1FornecMO: TIntegerField;
    QrEstqR1NO_ClientMO: TWideStringField;
    QrEstqR1NO_FornecMO: TWideStringField;
    QrEstqR1ValorTx: TFloatField;
    QrEstqR1BaseValVenda: TFloatField;
    QrEstqR1BaseValLiq: TFloatField;
    frxWET_CURTI_018_00_C3: TfrxReport;
    QrEstqR1BaseValCusto: TFloatField;
    procedure frxWET_CURTI_018_00_AGetValue(const VarName: string;
      var Value: Variant);
    procedure QrEstqR1CalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FVSMovImp1, FVSMovImp1_View, FVmiEstqEmVmiPosit: String;
    //
    function  MostraFrxEstoque(): Boolean;
    procedure InsereCouroEmProcesso(Ori_Empresa, Ori_GraGruX, Ori_GraGruY,
              Ori_IMEI, Dst_GraGruX: Integer; Dst_Pecas, Dst_Peso, Dst_AreaM2,
              Ori_FatorInt, ValorKg, TotCustoPQ: Double);
  public
    { Public declarations }
    FEntidade, FFilial, Ed00Terceiro_ValueVariant, RG00_Ordem, RG00_Ordem1_ItemIndex,
    RG00_Ordem2_ItemIndex, RG00_Ordem3_ItemIndex, RG00_Ordem4_ItemIndex,
    RG00_Ordem5_ItemIndex, RG00_Agrupa_ItemIndex, Ed00StqCenCad_ValueVariant,
    RG00ZeroNegat_ItemIndex: Integer;
    {2021-01-09 FGraCusPrc*) 2021-01-09}
    FRelatorio: TRelatorioVSImpEstoque;
    FNO_EMPRESA, CB00StqCenCad_Text, CB00Terceiro_Text: String;
    TPDataRelativa_Date: TDateTime;
    Ck00DataCompra_Checked, Ck00EmProcessoBH_Checked, FMostraFrx, FEmBH,
    Ck00DescrAgruNoItm_Checked: Boolean;
    //EdEmpresa: TDmkEditCB;
    DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2: TdmkDBGridZTO;
    Qr00GraGruY, Qr00GraGruX, Qr00CouNiv2: TmySQLQuery;
    //
    FDataEstoque: TDateTime;
    FDataRetroativa, FTableSrc: String;
    FEd00NFeIni_ValueVariant, FEd00NFeFim_ValueVariant: Integer;
    FCk00Serie_Checked: Boolean;
    FEd00Serie_ValueVariant, FMovimCod, FClientMO: Integer;
    //
    //function  GeraEstoque(): Boolean;
    function  GeraEstoque(): Boolean;
    //procedure ImprimeEstoque();
    function  ImprimeEstoque(): Boolean;
  end;

var
  FmVSImpEstoque: TFmVSImpEstoque;

implementation

{$R *.dfm}

uses ModuleGeral, CreateVS, UnMyObjects, DmkDAC_PF, Module, UMySQLModule,
  UnVS_CRC_PF;

{ TFmVSImpEstoque }

const
  FGraGruVal = '_gragruval_';

procedure TFmVSImpEstoque.FormCreate(Sender: TObject);
begin
  FMostraFrx := True;
  FEmBH := False;
end;

procedure TFmVSImpEstoque.frxWET_CURTI_018_00_AGetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', FNO_EMPRESA, FFilial, 'TODAS')
  else
  if VarName = 'VARF_NO_STQCENCAD' then
    Value := dmkPF.ParValueCodTxt('Centro de estoque: ',
    CB00StqCenCad_Text, Ed00StqCenCad_ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_NO_FORNECE' then
    Value := dmkPF.ParValueCodTxt('Fornecedor: ',
    CB00Terceiro_Text, Ed00Terceiro_ValueVariant, 'TODOS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_DATA_MEU' then
    Value := TPDataRelativa_Date
  else
  if VarName ='VARF_DATA_IMP' then
    Value := Now()
  else
  if VarName ='VARF_VSNIVGER' then
    Value := '00000' //Dmod.QrControleVSNivGer.Value    <=== Nao usa! Tirar!
  else
  if VarName ='VARF_TITULO' then
  begin
    if FDataRetroativa = '' then
      Value := 'Estoque de Peles e Couros'
    else
      Value := 'Estoque de Peles e Couros em ' + FDataRetroativa;
  end
  else
end;

function TFmVSImpEstoque.GeraEstoque(): Boolean;
const
  Agrups: array[0..Max_VS_IMP_ESTQ_ORD] of String = (
    'GraGruX',
    'MulFornece',
    'ID_UNQ',
    'IMEI',
    'Ficha',
    'Marca, Ficha',
    'StqCenCad',
    'StqCenLoc',
    'CouNiv2',
    'GraGruY',
    'NFeSer, NFeNum, VSMulNFeCab',
    'MovimCod',
    'MovimID',
    'ClientMO',
    'FornecMO'
    );
  Ordens: array[0..Max_VS_IMP_ESTQ_ORD] of String = (
    'NO_PRD_TAM_COR',
    'NO_FORNECE',
    'Pallet',
    'IMEI',
    'Ficha, NO_SerieFch',
    'Marca, Ficha, NO_SerieFch',
    'NO_StqCenCad',
    'NO_LOC_CEN',
    'NO_CouNiv2',
    'OrdGGY, NO_GGY',
    'NFeSer, NFeNum, VSMulNFeCab',
    'MovimCod',
    'MovimID',
    'ClientMO',
    'FornecMO');
var
  I, Terceiro, Index: Integer;
  SQL_Empresa, SQL_DtHr, SQL_ZeroNegat, Ordem, GroupBy: String;
  SQL_IMEI, ATT_MovimID, ATT_MovimNiv, GraGruYs, GraGruXs, SQL_GraGruY,
  SQL_GraGruX, ATT_StatPall, SQL_StqCenCad, SQL_CouNiv2, CouNiv2s,
  SQL_Terceiro, SQL_Historico, SQL_NFeNum, SQL_NFeSer, SQL_MovimCod,
  SQL_ClientMO: String;
  //
  Pecas, Peso, AreaM2, JaBxPc, SdoVrtPeca, SdoVrtPeso, FatorInt, ValorT,
  ValorKg, Custo: Double;
  Empresa, GraGruX, GraGruY, IMEI: Integer;
begin
  Result := False;
  FVSMovImp1 :=
    UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp1, DModG.QrUpdPID1, False);
  FVSMovImp1_View := UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp1,
    DModG.QrUpdPID1, False, 1, '_vsmovimp1_view');
  //
  if FEntidade <> 0 then
    SQL_Empresa := 'AND vmi.Empresa=' + Geral.FF0(FEntidade)
  else
    SQL_Empresa := '';
  //
  if Ck00DataCompra_Checked then
    SQL_DtHr := 'AND DataHora > "1900-01-01"'
  else
    SQL_DtHr := '';
  //
  GraGruYs := '';
  if (DBG00GraGruY <> nil) and (Qr00GraGruY <> nil) then
  begin
    if (DBG00GraGruY.SelectedRows.Count > 0) and
    (DBG00GraGruY.SelectedRows.Count < Qr00GraGruY.RecordCount) then
    begin
      for I := 0 to DBG00GraGruY.SelectedRows.Count - 1 do
      begin
        //Qr00GraGruY.GotoBookmark(pointer(DBG00GraGruY.SelectedRows.Items[I]));
        Qr00GraGruY.GotoBookmark(DBG00GraGruY.SelectedRows.Items[I]);
        //
        if GraGruYs <> '' then
          GraGruYs := GraGruYs + ',';
        GraGruYs := GraGruYs + Geral.FF0(Qr00GraGruY.FieldByName('Codigo').AsInteger);
      end;
    end;
  end;
  if GraGruYs <> '' then
    SQL_GraGruY := 'AND ggx.GraGruY IN (' + GraGruYs + ') '
  else
    SQL_GraGruY := '';
  //
  GraGruXs := '';
  if (DBG00GraGruX <> nil) and (Qr00GraGruX <> nil) then
  begin
    if (DBG00GraGruX.SelectedRows.Count > 0) (*and
    (DBG00GraGruX.SelectedRows.Count < Qr00GraGruX.RecordCount)*) then
    begin
      for I := 0 to DBG00GraGruX.SelectedRows.Count - 1 do
      begin
        //Qr00GraGruX.GotoBookmark(pointer(DBG00GraGruX.SelectedRows.Items[I]));
        Qr00GraGruX.GotoBookmark(DBG00GraGruX.SelectedRows.Items[I]);
        //
        if GraGruXs <> '' then
          GraGruXs := GraGruXs + ',';
        GraGruXs := GraGruXs + Geral.FF0(Qr00GraGruX.FieldByName('Controle').AsInteger);
      end;
    end;
  end;
  if GraGruXs <> '' then
    SQL_GraGruX := 'AND vmi.GraGruX IN (' + GraGruXs + ') '
  else
    SQL_GraGruX := '';
  //
  //
  CouNiv2s := '';
  if (DBG00CouNiv2 <> nil) and (Qr00CouNiv2 <> nil) then
  begin
    if (DBG00CouNiv2.SelectedRows.Count > 0) and
    (DBG00CouNiv2.SelectedRows.Count < Qr00CouNiv2.RecordCount) then
    begin
      for I := 0 to DBG00CouNiv2.SelectedRows.Count - 1 do
      begin
        //Qr00CouNiv2.GotoBookmark(pointer(DBG00CouNiv2.SelectedRows.Items[I]));
        Qr00CouNiv2.GotoBookmark(DBG00CouNiv2.SelectedRows.Items[I]);
        //
        if CouNiv2s <> '' then
          CouNiv2s := CouNiv2s + ',';
        CouNiv2s := CouNiv2s + Geral.FF0(Qr00CouNiv2.FieldByName('Codigo').AsInteger);
      end;
    end;
  end;
  if CouNiv2s <> '' then
    SQL_CouNiv2 := 'AND xco.CouNiv2 IN (' + CouNiv2s + ') '
  else
    SQL_CouNiv2 := '';
  //
  //
{
  if (RG00_Ordem1_ItemIndex = 3)
  or (RG00_Ordem2_ItemIndex = 3)
  or (RG00_Ordem3_ItemIndex = 3)
  or (RG00_Ordem4_ItemIndex = 3)
  or (RG00_Ordem5_ItemIndex = 3) then
  begin
    ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
    //
    ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
    //
    SQL_IMEI := Geral.ATS([
    'vmi.Codigo, vmi.MovimCod IMEC, vmi.Controle IMEI, ',
    'vmi.MovimID, vmi.MovimNiv, ',
    ATT_MovimID, ATT_MovimNiv ]);
  end else
  begin
    SQL_IMEI := Geral.ATS([
    'vmi.Codigo, vmi.MovimCod IMEC, vmi.Controle IMEI, ',
    'vmi.MovimID, vmi.MovimNiv, "" NO_MovimID, "" NO_MovimNiv, ']);
  end;
}
    ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
    //
    ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
    sEstqMovimNiv);
    //
    SQL_IMEI := Geral.ATS([
    'vmi.Codigo, vmi.MovimCod IMEC, vmi.Controle IMEI, ',
    'vmi.MovimID, vmi.MovimNiv, ',
    ATT_MovimID, ATT_MovimNiv ]);
////////////////////////////////////////////////////////////////////////////////
  if Ed00StqCenCad_ValueVariant <> 0 then
    SQL_StqCenCad := 'AND scc.Codigo=' + Geral.FF0(Ed00StqCenCad_ValueVariant)
  else
    SQL_StqCenCad := '';

  if (FEd00NFeIni_ValueVariant <> 0) or (FEd00NFeFim_ValueVariant <> 0) then
    SQL_NFeNum := 'AND vmi.NFeNum BETWEEN ' + Geral.FF0(FEd00NFeIni_ValueVariant)
    + ' AND ' + Geral.FF0(FEd00NFeFim_ValueVariant)
  else
    SQL_NFeNum := '';
  if FCk00Serie_Checked then
    SQL_NFeSer := 'AND vmi.NFeSer=' + Geral.FF0(FEd00Serie_ValueVariant)
  else
    SQL_NFeSer := '';
  if FMovimCod <> 0 then
    SQL_MovimCod := 'AND vmi.MovimCod=' + Geral.FF0(FMovimCod)
  else
    SQL_MovimCod := '';
  if FClientMO <> 0 then
    SQL_ClientMO := 'AND vmi.ClientMO=' + Geral.FF0(FClientMO)
  else
    SQL_ClientMO := '';
////////////////////////////////////////////////////////////////////////////////

  if Ck00EmProcessoBH_Checked then
    GroupBy := 'GROUP BY IMEI, ' + Agrups[RG00_Ordem1_ItemIndex]
  else
    GroupBy := 'GROUP BY ' + Agrups[RG00_Ordem1_ItemIndex];
  if RG00_Agrupa_ItemIndex > 0 then
    GroupBy := GroupBy + ', ' + Agrups[RG00_Ordem2_ItemIndex];
  if RG00_Agrupa_ItemIndex > 1 then
    GroupBy := GroupBy + ', ' + Agrups[RG00_Ordem3_ItemIndex];
  if RG00_Agrupa_ItemIndex > 2 then
    GroupBy := GroupBy + ', ' + Agrups[RG00_Ordem4_ItemIndex];
  if RG00_Agrupa_ItemIndex > 3 then
    GroupBy := GroupBy + ', ' + Agrups[RG00_Ordem5_ItemIndex];
  //
  Ordem := 'ORDER BY OrdGGY, NO_GGY, ' +
    Ordens[RG00_Ordem1_ItemIndex] + ', ' +
    Ordens[RG00_Ordem2_ItemIndex] + ', ' +
    Ordens[RG00_Ordem3_ItemIndex] + ', ' +
    Ordens[RG00_Ordem4_ItemIndex] + ', ' +
    Ordens[RG00_Ordem5_ItemIndex];

  ATT_StatPall := dmkPF.ArrayToTexto('vsp.StatPall', 'NO_StatPall', pvPos, True,
  sVSStatPall);
  Terceiro := Ed00Terceiro_ValueVariant;
  if Terceiro = 0 then
    SQL_Terceiro := ''
  else
    SQL_Terceiro := 'AND vmi.Terceiro=' + Geral.FF0(Terceiro);
  //

(*
  case RG00_Agrupa_Itemindex of
    0: Index := 0;
    1: Index := RG00_Ordem1_ItemIndex;
    2: Index := RG00_Ordem2_ItemIndex;
    3: Index := RG00_Ordem3_ItemIndex;
    4: Index := RG00_Ordem4_ItemIndex;
    5: Index := RG00_Ordem5_ItemIndex;
    else Index := -1;
  end;
*)
  case RG00_Agrupa_Itemindex of
    0: Index := RG00_Ordem1_ItemIndex;
    1: Index := RG00_Ordem2_ItemIndex;
    2: Index := RG00_Ordem3_ItemIndex;
    3: Index := RG00_Ordem4_ItemIndex;
    4: Index := RG00_Ordem5_ItemIndex;
    else Index := -1;
  end;
  case Index of
(*
0-MP / Artigo
1-Fornecedor
2-Pallet
3-IME-I
4-Ficha RMP
5-Marca
6-Centro
7-Local
8-Tipo Material
9-Est�gio Artigo

*)
      2: SQL_Historico := 'vsp.Nome Historico, ';
      3: SQL_Historico := 'vmi.Observ Historico, ';
      0, 1, 4, 5, 6, 7, 8, 9: SQL_Historico := '"" Historico, ';
      else
      begin
        SQL_Historico := '"" Historico, ';
        Geral.MB_Erro('Campo "Historico" n�o implementado para esta configura��o!');
      end;
    end;

  Result := UnDmkDAC_PF.ExecutaMySQLQuery0(QrEstqR1, DModG.MyPID_DB, [
  'DELETE FROM ' + FVSMovImp1 + ';',
  '',
  'INSERT INTO ' + FVSMovImp1 + '',
  'SELECT vmi.Empresa, IF(vmi.Pallet <> 0, vsp.GraGruX, ',
  'vmi.GraGruX) GraGruX, SUM(vmi.SdoVrtPeca) Pecas,  ',
  'SUM(vmi.PesoKg) PesoKg, SUM(vmi.AreaM2) AreaM2,  ',
  'SUM(FLOOR((vmi.AreaM2 / 0.09290304)) + ',
  'FLOOR(((MOD((vmi.AreaM2 / 0.09290304), 1)) + ',
  '0.12499) * 4) * 0.25) AreaP2, ',
  'SUM( ',
  '  IF(vmi.AreaM2 > 0, vmi.SdoVrtArM2 / vmi.AreaM2 * vmi.ValorT, ',
  '  IF(vmi.PesoKg > 0, vmi.SdoVrtPeso / vmi.PesoKg * vmi.ValorT, ',
  '  0))) ValorT, ',
  'SUM(vmi.SdoVrtPeca) SdoVrtPeca, SUM(vmi.SdoVrtPeso) SdoVrtPeso, ',
  'SUM(vmi.SdoVrtArM2) SdoVrtArM2, ',
  '0.000 LmbVrtPeca, 0.000 LmbVrtPeso, 0.000 LmbVrtArM2, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vmi.Pallet, vsp.Nome NO_Pallet, ',
  'vmi.Terceiro, ',
  'IF(vsp.CliStat IS NULL, 0, vsp.CliStat), ',
  'IF(vsp.Status IS NULL, 0, vsp.Status), ',
  'IF(vmi.Terceiro <> 0, ',
  '  IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome), ',
  '  mfc.Nome) NO_FORNECE, ',
  'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",   ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,  ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  'vps.Nome NO_STATUS, ',
  '"0000-00-00 00:00:00" DataHora, 0 OrdGGX, ',
  'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
  '0 PalStat, ',
  'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
  'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
  'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
  'SUM(vmi.SdoVrtPeca * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) SdoInteiros, ',
  '0 LmbInteiros, ',
  SQL_IMEI,
  'vmi.SerieFch, vsf.Nome NO_SerieFch, vmi.Ficha, vmi.Marca, ',
  'IF(vsp.StatPall IS NULL, 0, vsp.StatPall) PalStat, ',
  ATT_StatPall,
  'vmi.ReqMovEstq, ',
  'IF(scc.Codigo IS NULL, 0, scc.Codigo) StqCenCad, ',
  'IF(scc.Codigo IS NULL, "Local indefinido", scc.Nome) NO_StqCenCad, ',
  'vmi.StqCenLoc, CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, ',
  SQL_Historico + 'vmi.VSMulFrnCab, ',
  'IF(vmi.Terceiro <>0, vmi.Terceiro, vmi.VSMulFrnCab) MulFornece, ',
  'vmi.ClientMO, ',
  'IF(vmi.Pallet <> 0, CONCAT("PAL ", vmi.Pallet), ',
  '   CONCAT("IME-I ", vmi.Controle)) ID_UNQ, ',
  //
  VS_CRC_PF.SQL_TipoEstq_DefinirCodi(False, 'Grandeza', True,
  'vmi.AreaM2', 'vmi.PesoKg', 'vmi.Pecas'),
  //
  'vmi.NFeSer, vmi.NFeNum, vmi.VSMulNFeCab, ',
  //'vmi.StqCenLoc, ',
  //
  'vmi.MovimCod, vmi.FornecMO, ',
  'IF(cmo.Tipo=0, cmo.RazaoSocial, cmo.Nome) NO_ClientMO,  ',
  'IF(fmo.Tipo=0, fmo.RazaoSocial, fmo.Nome) NO_FornecMO, ',
  //
  'xco.BaseValVenda, xco.BaseCliente, xco.BaseImpostos, xco.BasePerComis, ',
  'xco.BasFrteVendM2, xco.BaseValLiq, xco.BaseValCusto, ',
  '1 Ativo ',
  'FROM ' + FTableSrc + ' vmi ',
  'LEFT JOIN ' + TMeuDB + '.vspalleta   vsp ON vsp.Codigo=vmi.Pallet   ',
  'LEFT JOIN ' + TMeuDB + '.gragrux     ggx ON ggx.Controle=IF(vmi.Pallet <> 0, vsp.GraGruX, vmi.GraGruX) ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou  xco ON xco.GraGruX=ggx.Controle  ',
  'LEFT JOIN ' + TMeuDB + '.gragruy     ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN ' + TMeuDB + '.gragruc     ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad   gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits   gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1     gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.entidades   ent ON ent.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB + '.entidades   emp ON emp.Codigo=vmi.Empresa ',
  'LEFT JOIN ' + TMeuDB + '.entidades   cmo ON cmo.Codigo=vmi.ClientMO ',
  'LEFT JOIN ' + TMeuDB + '.entidades   fmo ON fmo.Codigo=vmi.FornecMO ',
  'LEFT JOIN ' + TMeuDB + '.vspalsta    vps ON vps.Codigo=vsp.Status  ',
  'LEFT JOIN ' + TMeuDB + '.couniv2     cn2 ON cn2.Codigo=xco.CouNiv2',
  'LEFT JOIN ' + TMeuDB + '.couniv1     cn1 ON cn1.Codigo=xco.CouNiv1',
  'LEFT JOIN ' + TMeuDB + '.vsserfch    vsf ON vsf.Codigo=vmi.SerieFch ',
  'LEFT JOIN ' + TMeuDB + '.stqcenloc   scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN ' + TMeuDB + '.stqcencad   scc ON scc.Codigo=scl.Codigo ',
  'LEFT JOIN ' + TMeuDB + '.vsmulfrncab mfc ON mfc.Codigo=vmi.VSMulFrnCab ',
  'LEFT JOIN ' + TMeuDB + '.unidmed     med ON med.Codigo=gg1.UnidMed',
  'WHERE vmi.Controle <> 0 ',
  'AND vmi.GraGruX<>0 ',
  'AND (vmi.SdoVrtPeca > 0 OR (vmi.SdoVrtPeso > 0 AND (vmi.Pecas=0 OR vmi.MovimID IN (' +
  CO_ALL_CODS_ESTQ_SEM_PC + '))))',
  //
  SQL_Empresa,
  SQL_DtHr,
  SQL_GraGruY,
  SQL_GraGruX,
  SQL_StqCenCad,
  SQL_CouNiv2,
  SQL_Terceiro,
  SQL_NFeNum,
  SQL_NFeSer,
  SQL_MovimCod,
  SQL_ClientMO,
  GroupBy + ', GraGruX', // <<< Calcular inteiros!
  '']);
  //Geral.MB_SQL(Self, QrEstqR1);
  //
  if Ck00EmProcessoBH_Checked then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrBarraca, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + FVSMovImp1,
    'WHERE GraGruY=' + Geral.FF0(CO_GraGruY_1024_VSNatCad),
    '']);
    QrBarraca.First;
    while not QrBarraca.Eof do
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrEmitCus, Dmod.MyDB, [
      'SELECT SUM(Peso) Peso, SUM(Pecas) Pecas, ',
      'SUM(AreaM2) AreaM2, SUM(Custo) Custo, GraGruX ',
      'FROM emitcus ',
      'WHERE BxaEstqVS=' + Geral.FF0(Integer(TBxaEstqVS.bevsSim)),
      'AND VSMovIts=' + Geral.FF0(QrBarracaIMEI.Value),
      '']);
      //
      Pecas  := QrEmitCusPecas.Value;
      Peso   := QrEmitCusPeso.Value;
      AreaM2 := QrEmitCusAreaM2.Value;
      Custo  := QrEmitCusCusto.Value;
      FatorInt := 1; // ???? Mudar� algum dia (Caleiro de meios!)
      //
      JaBxPc := QrBarracaPecas.Value - QrBarracaSdoVrtPeca.Value;
      if JaBxPc < Pecas then
      begin
        SdoVrtPeca := QrBarracaPecas.Value - Pecas;
        SdoVrtPeso := QrBarracaPesoKg.Value - Peso;
        ValorKg    :=  0;
        if QrBarracaSdoVrtPeso.Value > 0 then
          ValorKg := QrBarracaValorT.Value / QrBarracaSdoVrtPeso.Value;
        //
        ValorT     := ValorKg * SdoVrtPeso;
        //
        Empresa    := QrBarracaEmpresa.Value;
        GraGruX    := QrBarracaGraGruX.Value;
        GraGruY    := QrBarracaGraGruY.Value;
        IMEI       := QrBarracaIMEI.Value;
        //
        if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, '_vsmovimp1_', False, [
        'SdoVrtPeca', 'SdoVrtPeso', 'ValorT'
        ], [
        'Empresa', 'GraGruX',
        'GraGruY', 'IMEI'], [
        SdoVrtPeca, SdoVrtPeso, ValorT
        ], [
        Empresa, GraGruX,
        GraGruY, IMEI], False) then
        begin
          // Parei aqui!
          // Incluir item de caleiro
          InsereCouroEmProcesso(Empresa, GraGruX, GraGruY, IMEI,
          QrEmitCusGraGruX.Value, Pecas, Peso, AreaM2, FatorInt,
          ValorKg, Custo);
        end;
      end;
      QrBarraca.Next;
    end;
  end;
end;

function TFmVSImpEstoque.ImprimeEstoque(): Boolean;
begin
  //
  Result := GeraEstoque();
  //
  if FMostraFrx and Result then
    Result := MostraFrxEstoque();
end;

procedure TFmVSImpEstoque.InsereCouroEmProcesso(Ori_Empresa, Ori_GraGruX,
  Ori_GraGruY, Ori_IMEI, Dst_GraGruX: Integer; Dst_Pecas, Dst_Peso, Dst_AreaM2,
  Ori_FatorInt, ValorKg, TotCustoPQ: Double);
//function ExcluiReceita_EnviaArquivoMorto(Numero, Motivo: Integer;
  //Query: TmySQLQuery; DataBase: TmySQLDatabase): Boolean;
var
  Campos: String;
  Inteiros, ValorT: Double;
  QtdReg1: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED, ',
  'ggx.GraGruY, ggy.Ordem OrdGGY, ggy.Nome NO_GGY, ',
  'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
  'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
  'cn2.Nome NO_CouNiv2, cn1.Nome NO_CouNiv1, ',
  'IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt ',
  'FROM gragrux ggx',
  //'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.Controle',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed ',
  'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'LEFT JOIN couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
  'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
  'WHERE ggx.Controle=' + Geral.FF0(Dst_GraGruX),
  //'ORDER BY ggy.Ordem, NO_PRD_TAM_COR, ggx.Controle ',
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
  '']);

  Campos := UMyMod.ObtemCamposDeTabelaIdentica(DModG.MyPID_DB, FVSMovImp1, '', QtdReg1);
  Campos := Geral.Substitui(Campos,
    ', Pecas', ', ' + Geral.FFT_Dot(Dst_Pecas, 3, siNegativo) + ' Pecas');
  Campos := Geral.Substitui(Campos,
    ', PesoKg', ', ' + Geral.FFT_Dot(Dst_Peso, 3, siNegativo) + ' PesoKg');
  Campos := Geral.Substitui(Campos,
    ', AreaM2', ', ' + Geral.FFT_Dot(Dst_AreaM2, 2, siNegativo) + ' AreaM2');
  Campos := Geral.Substitui(Campos,
    ', SdoVrtPeca', ', ' + Geral.FFT_Dot(Dst_Pecas, 3, siNegativo) + ' SdoVrtPeca');
  Campos := Geral.Substitui(Campos,
    ', SdoVrtPeso', ', ' + Geral.FFT_Dot(Dst_Peso, 3, siNegativo) + ' SdoVrtPeso');
  Campos := Geral.Substitui(Campos,
    ', SdoVrtArM2', ', ' + Geral.FFT_Dot(Dst_AreaM2, 2, siNegativo) + ' SdoVrtArM2');
  Campos := Geral.Substitui(Campos,
    ', GraGruX', ', ' + Geral.FF0(Dst_GraGruX) + ' GraGruX');
  Campos := Geral.Substitui(Campos,
    ', GraGru1', ', ' + Geral.FF0(QrGraGruXGraGru1.Value) + ' GraGru1');
  Campos := Geral.Substitui(Campos,
    ', NO_PRD_TAM_COR', ', "' + QrGraGruXNO_PRD_TAM_COR.Value + '" NO_PRD_TAM_COR');
  Campos := Geral.Substitui(Campos,
    ', GraGruY', ', ' + Geral.FF0(QrGraGruXGraGruY.Value) + ' GraGruY');
  Campos := Geral.Substitui(Campos,
    ', NO_GGY', ', "' + QrGraGruXNO_GGY.Value + '" NO_GGY');
  Campos := Geral.Substitui(Campos,
    ', OrdGGY', ', ' + Geral.FF0(QrGraGruXOrdGGY.Value) + ' OrdGGY');
  Campos := Geral.Substitui(Campos,
    ', CouNiv2', ', ' + Geral.FF0(QrGraGruXCouNiv2.Value) + ' CouNiv2');
  Campos := Geral.Substitui(Campos,
    ', CouNiv1', ', ' + Geral.FF0(QrGraGruXCouNiv1.Value) + ' CouNiv1');
  Campos := Geral.Substitui(Campos,
    ', NO_CouNiv2', ', "' + QrGraGruXNO_CouNiv2.Value + '" NO_CouNiv2');
  Campos := Geral.Substitui(Campos,
    ', NO_CouNiv1', ', "' + QrGraGruXNO_CouNiv1.Value + '" NO_CouNiv1');
  Campos := Geral.Substitui(Campos,
    ', FatorInt', ', ' + Geral.FFT_Dot(QrGraGruXFatorInt.Value, 3, siNegativo) + ' FatorInt');
  //
  Inteiros := Dst_Pecas / Ori_FatorInt * QrGraGruXFatorInt.Value;
  Campos := Geral.Substitui(Campos,
    ', SdoInteiros', ', ' + Geral.FFT_Dot(Inteiros, 3, siNegativo) + ' SdoInteiros');
  Campos := Geral.Substitui(Campos,
    ', MovimID', ', ' + Geral.FF0(Integer(emidEmProcCal)) + ' MovimID');
  Campos := Geral.Substitui(Campos,
    ', NO_MovimID', ', "' + 'Couro em Processo de Ribeira' + '" NO_MovimID');
  //
  ValorT := (ValorKg * Dst_Peso) + TotCustoPQ;
  Campos := Geral.Substitui(Campos,
    ', ValorT', ', ' + Geral.FFT_Dot(ValorT, 2, siNegativo) + ' ValorT');

  Campos := 'INSERT INTO ' + FVSMovImp1 + ' SELECT ' + sLineBreak +
  Campos + sLineBreak +
  'FROM ' + FVSMovImp1 + sLineBreak +
  'WHERE Empresa=' + Geral.FF0(Ori_Empresa) +
  ' AND GraGruX=' + Geral.FF0(Ori_GraGruX) +
  ' AND GraGruY=' + Geral.FF0(Ori_GraGruY) +
  ' AND IMEI=' + Geral.FF0(Ori_IMEI) +
  ' ;';
  //
  //Geral.MB_Aviso(Campos);
  DModG.QrUpdPID1.SQL.Text := Campos;
  //
  UMyMod.ExecutaQuery(DModG.QrUpdPID1);
end;

function TFmVSImpEstoque.MostraFrxEstoque(): Boolean;
const
  sProcName = 'TFmVSImpEstoque.MostraFrxEstoque()';
  //
  Agrups: array[0..Max_VS_IMP_ESTQ_ORD] of String = (
    'GraGruX',
    'MulFornece',
    'ID_UNQ',
    'IMEI',
    'Ficha',
    'Marca, Ficha',
    'StqCenCad',
    'StqCenLoc',
    'CouNiv2',
    'GraGruY',
    'NFeSer, NFeNum, VSMulNFeCab',
    'MovimCod',
    'MovimID',
    'ClientMO',
    'FornecMO'
    );
  Ordens: array[0..Max_VS_IMP_ESTQ_ORD] of String = (
    'NO_PRD_TAM_COR',
    'NO_FORNECE',
    'Pallet',
    'IMEI',
    'Ficha, NO_SerieFch',
    'Marca, Ficha, NO_SerieFch',
    'NO_StqCenCad',
    'NO_LOC_CEN',
    'NO_CouNiv2',
    'OrdGGY, NO_GGY', 'NFeSer, NFeNum, VSMulNFeCab',
    'MovimCod',
    'NO_MovimID',
    'NO_ClientMO',
    'NO_FornecMO'
    );
var
  Index: Integer;
  Ordem, GroupBy, NO_Arq: String;
  Grupo0, Grupo1, Grupo2, Grupo3: TfrxGroupHeader;
  Futer0, Futer1, Futer2, Futer3: TfrxGroupFooter;
  Me_GH0, Me_FT0, Me_GH1, Me_FT1, Me_GH2, Me_FT2, Me_GH3, Me_FT3,
  MeValNome, MeTitNome, MeTitCodi, MeValCodi: TfrxMemoView;
  frxReport: TfrxReport;
begin
  Result := False;
(*
  if (FGraCusPrc <> 0) and (FGraCusPrc <> -1) then
*)
  case FRelatorio of
    //TRelatorioVSImpEstoque.rieNenhum: frxReport := nil;
    TRelatorioVSImpEstoque.rieCusto:      frxReport := frxWET_CURTI_018_00_C1;
    TRelatorioVSImpEstoque.rieBaseVenda:  frxReport := frxWET_CURTI_018_00_C2;
    TRelatorioVSImpEstoque.rieBaseCusto:  frxReport := frxWET_CURTI_018_00_C3;
    else
    begin
      Geral.MB_Erro('"TRelatorioVSImpEstoque" n�o implementado em ' + sProcName);
      Exit;
    end;
  end;

  //
  FVmiEstqEmVmiPosit :=
    UnCreateVS.RecriaTempTableNovo(ntrttVmiEstqEmVmiPosit, DModG.QrUpdPID1, False);
  //
  case RG00_Agrupa_Itemindex of
    0: Index := RG00_Ordem1_ItemIndex;
    1: Index := RG00_Ordem2_ItemIndex;
    2: Index := RG00_Ordem3_ItemIndex;
    3: Index := RG00_Ordem4_ItemIndex;
    4: Index := RG00_Ordem5_ItemIndex;
    5: Index := 0;
    else Index := -1;
  end;
  GroupBy := 'GROUP BY ' + Agrups[RG00_Ordem1_ItemIndex];
  if RG00_Agrupa_ItemIndex > 0 then
    GroupBy := GroupBy + ', ' + Agrups[RG00_Ordem2_ItemIndex];
  if RG00_Agrupa_ItemIndex > 1 then
    GroupBy := GroupBy + ', ' + Agrups[RG00_Ordem3_ItemIndex];
  if RG00_Agrupa_ItemIndex > 2 then
    GroupBy := GroupBy + ', ' + Agrups[RG00_Ordem4_ItemIndex];
  if RG00_Agrupa_ItemIndex > 3 then
    GroupBy := GroupBy + ', ' + Agrups[RG00_Ordem5_ItemIndex];
  //
  Ordem := 'ORDER BY ' +
    Ordens[RG00_Ordem1_ItemIndex] + ', ' +
    Ordens[RG00_Ordem2_ItemIndex] + ', ' +
    Ordens[RG00_Ordem3_ItemIndex] + ', ' +
    Ordens[RG00_Ordem4_ItemIndex] + ', ' +
    Ordens[RG00_Ordem5_ItemIndex];

  UnDmkDAC_PF.AbreMySQLQuery0(QrEstqR1, DModG.MyPID_DB, [
  //'DROP TABLE IF EXISTS ' + FVmiEstqEmVmiPosit + ';',
  //'CREATE TABLE ' + FVmiEstqEmVmiPosit + '',
  'INSERT INTO ' + FVmiEstqEmVmiPosit + '',
  'SELECT mi1.Empresa, mi1.GraGruX, SUM(mi1.Pecas) Pecas, SUM(mi1.PesoKg) PesoKg, ',
  'Sum(mi1.AreaM2) AreaM2, SUM(mi1.AreaP2) AreaP2, SUM(mi1.ValorT) ValorT, ',
  'SUM(mi1.SdoVrtPeca) SdoVrtPeca, SUM(mi1.SdoVrtPeso) SdoVrtPeso, ',
  'Sum(mi1.SdoVrtArM2) SdoVrtArM2, ',
  //
  'SUM(mi1.SdoVrtPeca+mi1.LmbVrtPeca) PalVrtPeca, ',
  'SUM(mi1.SdoVrtPeso+mi1.LmbVrtPeso) PalVrtPeso, ',
  'SUM(mi1.LmbVrtArM2+mi1.SdoVrtArM2) PalVrtArM2, ',
  //
  'SUM(mi1.LmbVrtPeca) LmbVrtPeca, ',
  'SUM(mi1.LmbVrtPeso) LmbVrtPeso, SUM(mi1.LmbVrtArM2) LmbVrtArM2, ',
  'mi1.GraGru1, mi1.NO_PRD_TAM_COR, mi1.Pallet, mi1.NO_PALLET, mi1.Terceiro, ',
  'mi1.CliStat, mi1.Status, mi1.NO_FORNECE,  mi1.NO_CLISTAT, mi1.NO_EMPRESA, ',
  'mi1.NO_STATUS, mi1.DataHora, mi1.OrdGGX, mi1.OrdGGY, mi1.GraGruY, ',
  'mi1.NO_GGY, StatPall PalStat, NO_StatPall NO_PalStat, ',
  'IF(SUM(mi1.SdoVrtPeca+mi1.LmbVrtPeca)  <> 0, ',
  'SUM(mi1.LmbVrtArM2+mi1.SdoVrtArM2) / SUM(mi1.SdoVrtPeca+mi1.LmbVrtPeca), 0) ',
  'Media, ',
  'mi1.NO_MovimNiv, mi1.NO_MovimID, mi1.MovimNiv, ',
  'mi1.MovimID, mi1.IMEC, mi1.Codigo, mi1.IMEI, ',
  'mi1.SerieFch, mi1.NO_SerieFch, mi1.Ficha, mi1.Marca, ',
  'SUM(mi1.SdoInteiros + mi1.LmbInteiros) Inteiros, ',
  'mi1.ReqMovEstq, mi1.StqCenCad, mi1.NO_StqCenCad, ',
  'mi1.StqCenLoc, mi1.NO_LOC_CEN, mi1.Historico, ',
  'mi1.VSMulFrnCab, mi1.MulFornece, ',
  'IF(Terceiro<>0, "Fornecedor", "Mistura") NO_MulFornece, ',
  'mi1.CouNiv2, mi1.CouNiv1, mi1.NO_CouNiv2, mi1.NO_CouNiv1, ',
  'mi1.ClientMO, ',
  'IF(mi1.Pallet <> 0, CONCAT("PAL ", mi1.Pallet), ',
  '   CONCAT("IME-I ", mi1.IMEI)) ID_UNQ, ',
  'mi1.Grandeza, 0 GraGruValU, 0 GraGruValT, ',
  //
  'mi1.NFeSer, mi1.NFeNum, mi1.VSMulNFeCab, ',
  'CONCAT(LPAD(mi1.NFeSer, 3, "0"), ".", LPAD(mi1.NFeNum, 9, "0"), ',
  '  IF(mi1.VSMulNFeCab<>0, CONCAT("-", ',
  '  LPAD(mi1.VSMulNFeCab, 9, "0")), "")) NFeAgrup, ',
  'mi1.MovimCod, mi1.FornecMO, mi1.NO_ClientMO, mi1.NO_FornecMO, ',
  //
  'mi1.BaseValVenda, mi1.BaseCliente, mi1.BaseImpostos, mi1.BasePerComis, ',
  'mi1.BasFrteVendM2, mi1.BaseValLiq, mi1.BaseValCusto, ',
  //
  'mi1.Ativo ',
  'FROM  ' + FVSMovImp1 + ' mi1 ',
  'WHERE (SdoVrtPeca > 0 OR LmbVrtPeca <> 0 OR (SdoVrtPeso > 0 AND (Pecas=0 OR MovimID IN (' +
  CO_ALL_CODS_ESTQ_SEM_PC + ')))) ',
  GroupBy,
  '; ',
  'SELECT * FROM ' + FVmiEstqEmVmiPosit + '',
  'WHERE PalVrtPeca > 0 OR (SdoVrtPeso > 0 AND (Pecas=0 OR MovimID IN (' +
  CO_ALL_CODS_ESTQ_SEM_PC + ')))',
  Ordem,
  '']);
  //Geral.MB_SQL(Self, QrEstqR1);
  //
{2021-01-09 >> Substituido pelo valor no cadastro do artigo (gragruxcou.BaseValLiq)
  N�o usa mais este c�digo pois busca direto da tabela gragruxcou
  if (FGraCusPrc <> 0) and (FGraCusPrc <> -1) then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'UPDATE ' + FVmiEstqEmVmiPosit+ ' T1  ',
    'LEFT JOIN ' + FGraGruVal + ' T2 ON T1.GraGruX = T2.GraGruX ',
    'SET T1.GraGruValU = T2.CustoPreco,  ',
    '    T1.GraGruValT = 0.00 ',
    '']);
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'UPDATE ' + FVmiEstqEmVmiPosit,
    'SET GraGruValT = GraGruValU *  ',
    '  CASE Grandeza  ',
    '  WHEN 1 THEN SdoVrtArM2  ',
    '  WHEN 2 THEN SdoVrtPeso  ',
    '  ELSE SdoVrtPeca END ',
    ' ',
    '']);
    UnDmkDAC_PF.AbreMySQLQuery0(QrEstqR1, DModG.MyPID_DB, [
    'SELECT * FROM ' + FVmiEstqEmVmiPosit,
    'WHERE PalVrtPeca > 0 OR (SdoVrtPeso > 0 AND (Pecas=0 OR  MovimID IN (' +
    CO_ALL_CODS_ESTQ_SEM_PC + ')))',
    Ordem,
    '']);
  end;
2021-01-09}
  //
  Grupo0 := frxReport.FindObject('GH_00') as TfrxGroupHeader;
  Grupo0.Visible := RG00_Agrupa_ItemIndex > 0;
  Futer0 := frxReport.FindObject('FT_00') as TfrxGroupFooter;
  Futer0.Visible := Grupo0.Visible;
  Me_GH0 := frxReport.FindObject('Me_GH0') as TfrxMemoView;
  Me_FT0 := frxReport.FindObject('Me_FT0') as TfrxMemoView;
  case RG00_Ordem1_ItemIndex of
    0:
    begin
      Grupo0.Condition := 'frxDsEstqR1."GraGruX"';
      Me_GH0.Memo.Text := '[frxDsEstqR1."GraGruX"] - [frxDsEstqR1."NO_PRD_TAM_COR"]';
      Me_FT0.Memo.Text := '[frxDsEstqR1."GraGruX"] - [frxDsEstqR1."NO_PRD_TAM_COR"]: ';
    end;
    1:
    begin
      Grupo0.Condition := 'frxDsEstqR1."MulFornece"';
      Me_GH0.Memo.Text := '[frxDsEstqR1."MulFornece"] - [frxDsEstqR1."NO_FORNECE"]';
      Me_FT0.Memo.Text := '[frxDsEstqR1."MulFornece"] - [frxDsEstqR1."NO_FORNECE"]: ';
    end;
    2:
    begin
(* ID_UNQ
      Grupo0.Condition := 'frxDsEstqR1."Pallet"';
      Me_GH0.Memo.Text := '[frxDsEstqR1."Pallet"] - [frxDsEstqR1."NO_PALLET"]';
      Me_FT0.Memo.Text := '[frxDsEstqR1."Pallet"] - [frxDsEstqR1."NO_PALLET"]: ';
*)
      Grupo0.Condition := 'frxDsEstqR1."ID_UNQ"';
      Me_GH0.Memo.Text := '[frxDsEstqR1."ID_UNQ"] - [frxDsEstqR1."NO_PALLET"]';
      Me_FT0.Memo.Text := '[frxDsEstqR1."ID_UNQ"] - [frxDsEstqR1."NO_PALLET"]: ';
    end;
    3:
    begin
      Grupo0.Condition := 'frxDsEstqR1."IMEI"';
      Me_GH0.Memo.Text := 'IME-I: [frxDsEstqR1."IMEI"]';
      Me_FT0.Memo.Text := 'IME-C: [frxDsEstqR1."IMEC"]: ';
    end;
    4:
    begin
      Grupo0.Condition := 'frxDsEstqR1."Ficha"';
      Me_GH0.Memo.Text := 'Ficha RMP: [frxDsEstqR1."NO_SerieFch"] [frxDsEstqR1."Ficha"]';
      Me_FT0.Memo.Text := 'Ficha RMP: [frxDsEstqR1."NO_SerieFch"] [frxDsEstqR1."Ficha"]: ';
    end;
    5:
    begin
      Grupo0.Condition := 'frxDsEstqR1."Marca"';
      Me_GH0.Memo.Text := 'Marca: [frxDsEstqR1."Marca"]';
      Me_FT0.Memo.Text := 'Marca: [frxDsEstqR1."Marca"]: ';
    end;
    6:
    begin
      Grupo0.Condition := 'frxDsEstqR1."StqCenCad"';
      Me_GH0.Memo.Text := 'Centro de estoque: [frxDsEstqR1."NO_StqCenCad"]';
      Me_FT0.Memo.Text := 'Centro de estoque: [frxDsEstqR1."NO_StqCenCad"]: ';
    end;
    7:
    begin
      Grupo0.Condition := 'frxDsEstqR1."StqCenLoc"';
      Me_GH0.Memo.Text := 'Local (e centro) do estoque: [frxDsEstqR1."NO_LOC_CEN"]';
      Me_FT0.Memo.Text := 'Local (e centro) do estoque: [frxDsEstqR1."NO_LOC_CEN"]: ';
    end;
    8:
    begin
      Grupo0.Condition := 'frxDsEstqR1."CouNiv2"';
      Me_GH0.Memo.Text := 'Tipo de material: [frxDsEstqR1."NO_CouNiv2"]';
      Me_FT0.Memo.Text := 'Tipo de material: [frxDsEstqR1."NO_CouNiv2"]: ';
    end;
    9:
    begin
      Grupo0.Condition := 'frxDsEstqR1."GraGruY"';
      Me_GH0.Memo.Text := 'Est�gio de Artigo: [frxDsEstqR1."NO_GGY"]';
      Me_FT0.Memo.Text := 'Est�gio de Artigo: [frxDsEstqR1."NO_GGY"]: ';
    end;
    10:
    begin
      Grupo0.Condition := 'frxDsEstqR1."NFeAgrup"';
      Me_GH0.Memo.Text := 'NF-e: [frxDsEstqR1."NFeAgrup"]';
      Me_FT0.Memo.Text := 'S�rie-NFe-Multi: [frxDsEstqR1."NFeAgrup"]: ';
    end;
    11:
    begin
      Grupo0.Condition := 'frxDsEstqR1."MovimCod"';
      Me_GH0.Memo.Text := 'IME-C [frxDsEstqR1."MovimCod"]';
      Me_FT0.Memo.Text := 'IME-C [frxDsEstqR1."MovimCod"]: ';
    end;
    12:
    begin
      Grupo0.Condition := 'frxDsEstqR1."MovimID"';
      Me_GH0.Memo.Text := 'Movimento: [frxDsEstqR1."NO_MovimID"]';
      Me_FT0.Memo.Text := 'Movimento: [frxDsEstqR1."NO_MovimID"]: ';
    end;
    13:
    begin
      Grupo0.Condition := 'frxDsEstqR1."ClientMO"';
      Me_GH0.Memo.Text := 'Cliente M.O.: [frxDsEstqR1."ClientMO"]';
      Me_FT0.Memo.Text := 'Cliente M.O.: [frxDsEstqR1."NO_ClientMO"]: ';
    end;
    14:
    begin
      Grupo0.Condition := 'frxDsEstqR1."FornecMO"';
      Me_GH0.Memo.Text := 'Fornecedor M.O.: [frxDsEstqR1."FornecMO"]';
      Me_FT0.Memo.Text := 'Fornecedor M.O.: [frxDsEstqR1."NO_FornecMO"]: ';
    end;
    else Grupo0.Condition := '***ERRO***';
  end;
  //
  Grupo1 := frxReport.FindObject('GH_01') as TfrxGroupHeader;
  Grupo1.Visible := RG00_Agrupa_ItemIndex > 1;
  Futer1 := frxReport.FindObject('FT_01') as TfrxGroupFooter;
  Futer1.Visible := Grupo1.Visible;
  Me_GH1 := frxReport.FindObject('Me_GH1') as TfrxMemoView;
  Me_FT1 := frxReport.FindObject('Me_FT1') as TfrxMemoView;
  case RG00_Ordem2_ItemIndex of
    0:
    begin
      Grupo1.Condition := 'frxDsEstqR1."GraGruX"';
      Me_GH1.Memo.Text := '[frxDsEstqR1."GraGruX"] - [frxDsEstqR1."NO_PRD_TAM_COR"]';
      Me_FT1.Memo.Text := '[frxDsEstqR1."GraGruX"] - [frxDsEstqR1."NO_PRD_TAM_COR"]: ';
    end;
    1:
    begin
      Grupo1.Condition := 'frxDsEstqR1."MulFornece"';
      Me_GH1.Memo.Text := '[frxDsEstqR1."MulFornece"] - [frxDsEstqR1."NO_FORNECE"]';
      Me_FT1.Memo.Text := '[frxDsEstqR1."MulFornece"] - [frxDsEstqR1."NO_FORNECE"]: ';
    end;
    2:
    begin
(*      Grupo1.Condition := 'frxDsEstqR1."Pallet"';
      Me_GH1.Memo.Text := '[frxDsEstqR1."Pallet"] - [frxDsEstqR1."NO_PALLET"]';
      Me_FT1.Memo.Text := '[frxDsEstqR1."Pallet"] - [frxDsEstqR1."NO_PALLET"]: ';
*)
      Grupo1.Condition := 'frxDsEstqR1."ID_UNQ"';
      Me_GH1.Memo.Text := '[frxDsEstqR1."ID_UNQ"] - [frxDsEstqR1."NO_PALLET"]';
      Me_FT1.Memo.Text := '[frxDsEstqR1."ID_UNQ"] - [frxDsEstqR1."NO_PALLET"]: ';
    end;
    3:
    begin
      Grupo1.Condition := 'frxDsEstqR1."IMEI"';
      Me_GH1.Memo.Text := '[frxDsEstqR1."IMEI"]';
      Me_FT1.Memo.Text := 'IME-C [frxDsEstqR1."IMEC"]: ';
    end;
    4:
    begin
      Grupo1.Condition := 'frxDsEstqR1."Ficha"';
      Me_GH1.Memo.Text := '[frxDsEstqR1."NO_SerieFch"] [frxDsEstqR1."Ficha"]';
      Me_FT1.Memo.Text := 'Ficha RMP [frxDsEstqR1."NO_SerieFch"] [frxDsEstqR1."Ficha"]: ';
    end;
    5:
    begin
      Grupo1.Condition := 'frxDsEstqR1."Marca"';
      Me_GH1.Memo.Text := '[frxDsEstqR1."Marca"]';
      Me_FT1.Memo.Text := 'Marca [frxDsEstqR1."Marca"]: ';
    end;
    6:
    begin
      Grupo1.Condition := 'frxDsEstqR1."StqCenCad"';
      Me_GH1.Memo.Text := '[frxDsEstqR1."NO_StqCenCad"]';
      Me_FT1.Memo.Text := 'Centro de estoque [frxDsEstqR1."NO_StqCenCad"]: ';
    end;
    7:
    begin
      Grupo1.Condition := 'frxDsEstqR1."StqCenLoc"';
      Me_GH1.Memo.Text := '[frxDsEstqR1."NO_LOC_CEN"]';
      Me_FT1.Memo.Text := 'Local (e centro) do estoque [frxDsEstqR1."NO_LOC_CEN"]: ';
    end;
    8:
    begin
      Grupo1.Condition := 'frxDsEstqR1."CouNiv2"';
      Me_GH1.Memo.Text := '[frxDsEstqR1."NO_CouNiv2"]';
      Me_FT1.Memo.Text := 'Tipo de material [frxDsEstqR1."NO_CouNiv2"]: ';
    end;
    9:
    begin
      Grupo1.Condition := 'frxDsEstqR1."GraGruY"';
      Me_GH1.Memo.Text := '[frxDsEstqR1."NO_GGY"]';
      Me_FT1.Memo.Text := 'Est�gio de Artigo [frxDsEstqR1."NO_GGY"]: ';
    end;
    10:
    begin
      Grupo1.Condition := 'frxDsEstqR1."NFeAgrup"';
      Me_GH1.Memo.Text := '[frxDsEstqR1."NFeAgrup"]';
      Me_FT1.Memo.Text := 'S�rie-NFe-Multi [frxDsEstqR1."NFeAgrup"]: ';
    end;
    11:
    begin
      Grupo1.Condition := 'frxDsEstqR1."MovimCod"';
      Me_GH1.Memo.Text := 'IME-C [frxDsEstqR1."MovimCod"]';
      Me_FT1.Memo.Text := 'IME-C [frxDsEstqR1."MovimCod"]: ';
    end;
    12:
    begin
      Grupo1.Condition := 'frxDsEstqR1."MovimID"';
      Me_GH1.Memo.Text := 'Movimento [frxDsEstqR1."NO_MovimID"]';
      Me_FT1.Memo.Text := 'Movimento [frxDsEstqR1."NO_MovimID"]: ';
    end;
    13:
    begin
      Grupo1.Condition := 'frxDsEstqR1."ClientMO"';
      Me_GH1.Memo.Text := 'Cliente M.O. [frxDsEstqR1."ClientMO"]';
      Me_FT1.Memo.Text := 'Cliente M.O. [frxDsEstqR1."NO_ClientMO"]: ';
    end;
    14:
    begin
      Grupo1.Condition := 'frxDsEstqR1."FornecMO"';
      Me_GH1.Memo.Text := 'Fornecedor M.O. [frxDsEstqR1."FornecMO"]';
      Me_FT1.Memo.Text := 'Fornecedor M.O. [frxDsEstqR1."NO_FornecMO"]: ';
    end;
    else Grupo1.Condition := '***ERRO***';
  end;
  //
  Grupo2 := frxReport.FindObject('GH_02') as TfrxGroupHeader;
  Grupo2.Visible := RG00_Agrupa_ItemIndex > 2;
  Futer2 := frxReport.FindObject('FT_02') as TfrxGroupFooter;
  Futer2.Visible := Grupo2.Visible;
  Me_GH2 := frxReport.FindObject('Me_GH2') as TfrxMemoView;
  Me_FT2 := frxReport.FindObject('Me_FT2') as TfrxMemoView;
  case RG00_Ordem3_ItemIndex of
    0:
    begin
      Grupo2.Condition := 'frxDsEstqR1."GraGruX"';
      Me_GH2.Memo.Text := 'Reduzido [frxDsEstqR1."GraGruX"] - [frxDsEstqR1."NO_PRD_TAM_COR"]';
      Me_FT2.Memo.Text := '[frxDsEstqR1."GraGruX"] - [frxDsEstqR1."NO_PRD_TAM_COR"]: ';
    end;
    1:
    begin
      Grupo2.Condition := 'frxDsEstqR1."MulFornece"';
      Me_GH2.Memo.Text := '[frxDsEstqR1."NO_MulFornece"]: [frxDsEstqR1."MulFornece"] - [frxDsEstqR1."NO_FORNECE"]';
      Me_FT2.Memo.Text := '[frxDsEstqR1."NO_MulFornece"] [frxDsEstqR1."MulFornece"] - [frxDsEstqR1."NO_FORNECE"]: ';
    end;
    2:
    begin
(*
      Grupo2.Condition := 'frxDsEstqR1."Pallet"';
      Me_GH2.Memo.Text := 'Pallet [frxDsEstqR1."Pallet"] - [frxDsEstqR1."NO_PALLET"]';
      Me_FT2.Memo.Text := '[frxDsEstqR1."Pallet"] - [frxDsEstqR1."NO_PALLET"]: ';
*)
      Grupo2.Condition := 'frxDsEstqR1."ID_UNQ"';
      Me_GH2.Memo.Text := '[frxDsEstqR1."ID_UNQ"] - [frxDsEstqR1."NO_PALLET"]';
      Me_FT2.Memo.Text := '[frxDsEstqR1."ID_UNQ"] - [frxDsEstqR1."NO_PALLET"]: ';
    end;
    3:
    begin
      Grupo2.Condition := 'frxDsEstqR1."IMEI"';
      Me_GH2.Memo.Text := 'IME-I [frxDsEstqR1."IMEI"]';
      Me_FT2.Memo.Text := 'IME-C [frxDsEstqR1."IMEC"]: ';
    end;
    4:
    begin
      Grupo2.Condition := 'frxDsEstqR1."Ficha"';
      Me_GH2.Memo.Text := '[frxDsEstqR1."NO_SerieFch"] [frxDsEstqR1."Ficha"]';
      Me_FT2.Memo.Text := 'Ficha RMP [frxDsEstqR1."NO_SerieFch"] [frxDsEstqR1."Ficha"]: ';
    end;
    5:
    begin
      Grupo2.Condition := 'frxDsEstqR1."Marca"';
      Me_GH2.Memo.Text := '[frxDsEstqR1."Marca"]';
      Me_FT2.Memo.Text := 'Marca [frxDsEstqR1."Marca"]: ';
    end;
    6:
    begin
      Grupo2.Condition := 'frxDsEstqR1."StqCenCad"';
      Me_GH2.Memo.Text := '[frxDsEstqR1."NO_StqCenCad"]';
      Me_FT2.Memo.Text := 'Centro de estoque > [frxDsEstqR1."NO_StqCenCad"]: ';
    end;
    7:begin
      Grupo2.Condition := 'frxDsEstqR1."StqCenLoc"';
      Me_GH2.Memo.Text := '[frxDsEstqR1."NO_LOC_CEN"]';
      Me_FT2.Memo.Text := 'Local (e centro) do estoque > [frxDsEstqR1."NO_LOC_CEN"]: ';
    end;
    8:
    begin
      Grupo2.Condition := 'frxDsEstqR1."CouNiv2"';
      Me_GH2.Memo.Text := '[frxDsEstqR1."NO_CouNiv2"]';
      Me_FT2.Memo.Text := 'Tipo de Material > [frxDsEstqR1."NO_CouNiv2"]: ';
    end;
    9:
    begin
      Grupo2.Condition := 'frxDsEstqR1."GraGruY"';
      Me_GH2.Memo.Text := '[frxDsEstqR1."NO_GGY"]';
      Me_FT2.Memo.Text := 'Est�gio de Artigo > [frxDsEstqR1."NO_GGY"]: ';
    end;
    10:
    begin
      Grupo2.Condition := 'frxDsEstqR1."NFeAgrup"';
      Me_GH2.Memo.Text := '[frxDsEstqR1."NFeAgrup"]';
      Me_FT2.Memo.Text := 'S�rie-NFe-Multi [frxDsEstqR1."NFeAgrup"]: ';
    end;
    11:
    begin
      Grupo2.Condition := 'frxDsEstqR1."MovimCod"';
      Me_GH2.Memo.Text := 'IME-C [frxDsEstqR1."MovimCod"]';
      Me_FT2.Memo.Text := 'IME-C [frxDsEstqR1."MovimCod"]: ';
    end;
    12:
    begin
      Grupo2.Condition := 'frxDsEstqR1."MovimID"';
      Me_GH2.Memo.Text := 'Movimento [frxDsEstqR1."NO_MovimID"]';
      Me_FT2.Memo.Text := 'Movimento [frxDsEstqR1."NO_MovimID"]: ';
    end;
    13:
    begin
      Grupo2.Condition := 'frxDsEstqR1."ClientMO"';
      Me_GH2.Memo.Text := 'Cliente M.O. [frxDsEstqR1."ClientMO"]';
      Me_FT2.Memo.Text := 'Cliente M.O. [frxDsEstqR1."NO_ClientMO"]: ';
    end;
    14:
    begin
      Grupo2.Condition := 'frxDsEstqR1."FornecMO"';
      Me_GH2.Memo.Text := 'Fornecedor M.O. [frxDsEstqR1."FornecMO"]';
      Me_FT2.Memo.Text := 'Fornecedor M.O. [frxDsEstqR1."NO_FornecMO"]: ';
    end;
    else Grupo2.Condition := '***ERRO***';
  end;
  //
  //
  Grupo3 := frxReport.FindObject('GH_03') as TfrxGroupHeader;
  Grupo3.Visible := RG00_Agrupa_ItemIndex > 3;
  Futer3 := frxReport.FindObject('FT_03') as TfrxGroupFooter;
  Futer3.Visible := Grupo3.Visible;
  Me_GH3 := frxReport.FindObject('Me_GH3') as TfrxMemoView;
  Me_FT3 := frxReport.FindObject('Me_FT3') as TfrxMemoView;
  case RG00_Ordem4_ItemIndex of
    0:
    begin
      Grupo3.Condition := 'frxDsEstqR1."GraGruX"';
      Me_GH3.Memo.Text := 'Reduzido [frxDsEstqR1."GraGruX"] - [frxDsEstqR1."NO_PRD_TAM_COR"]';
      Me_FT3.Memo.Text := '[frxDsEstqR1."GraGruX"] - [frxDsEstqR1."NO_PRD_TAM_COR"]: ';
    end;
    1:
    begin
      Grupo3.Condition := 'frxDsEstqR1."MulFornece"';
      Me_GH3.Memo.Text := '[frxDsEstqR1."NO_MulFornece"]: [frxDsEstqR1."MulFornece"] - [frxDsEstqR1."NO_FORNECE"]';
      Me_FT3.Memo.Text := '[frxDsEstqR1."NO_MulFornece"] [frxDsEstqR1."MulFornece"] - [frxDsEstqR1."NO_FORNECE"]: ';
    end;
    2:
    begin
(*
      Grupo3.Condition := 'frxDsEstqR1."Pallet"';
      Me_GH3.Memo.Text := 'Pallet [frxDsEstqR1."Pallet"] - [frxDsEstqR1."NO_PALLET"]';
      Me_FT3.Memo.Text := '[frxDsEstqR1."Pallet"] - [frxDsEstqR1."NO_PALLET"]: ';
*)
      Grupo3.Condition := 'frxDsEstqR1."ID_UNQ"';
      Me_GH3.Memo.Text := '[frxDsEstqR1."ID_UNQ"] - [frxDsEstqR1."NO_PALLET"]';
      Me_FT3.Memo.Text := '[frxDsEstqR1."ID_UNQ"] - [frxDsEstqR1."NO_PALLET"]: ';
    end;
    3:
    begin
      Grupo3.Condition := 'frxDsEstqR1."IMEI"';
      Me_GH3.Memo.Text := 'IME-I [frxDsEstqR1."IMEI"]';
      Me_FT3.Memo.Text := 'IME-C [frxDsEstqR1."IMEC"]: ';
    end;
    4:
    begin
      Grupo3.Condition := 'frxDsEstqR1."Ficha"';
      Me_GH3.Memo.Text := '[frxDsEstqR1."NO_SerieFch"] [frxDsEstqR1."Ficha"]';
      Me_FT3.Memo.Text := 'Ficha RMP [frxDsEstqR1."NO_SerieFch"] [frxDsEstqR1."Ficha"]: ';
    end;
    5:
    begin
      Grupo3.Condition := 'frxDsEstqR1."Marca"';
      Me_GH3.Memo.Text := '[frxDsEstqR1."Marca"]';
      Me_FT3.Memo.Text := 'Marca [frxDsEstqR1."Marca"]: ';
    end;
    6:
    begin
      Grupo3.Condition := 'frxDsEstqR1."StqCenCad"';
      Me_GH3.Memo.Text := '[frxDsEstqR1."NO_StqCenCad"]';
      Me_FT3.Memo.Text := 'Centro de estoque > [frxDsEstqR1."NO_StqCenCad"]: ';
    end;
    7:
    begin
      Grupo3.Condition := 'frxDsEstqR1."StqCenLoc"';
      Me_GH3.Memo.Text := '[frxDsEstqR1."NO_LOC_CEN"]';
      Me_FT3.Memo.Text := 'Local (e centro) do estoque > [frxDsEstqR1."NO_LOC_CEN"]: ';
    end;
    8:
    begin
      Grupo3.Condition := 'frxDsEstqR1."CouNiv2"';
      Me_GH3.Memo.Text := '[frxDsEstqR1."NO_CouNiv2"]';
      Me_FT3.Memo.Text := 'Tipo de Material > [frxDsEstqR1."NO_CouNiv2"]: ';
    end;
    9:
    begin
      Grupo3.Condition := 'frxDsEstqR1."GraGruY"';
      Me_GH3.Memo.Text := '[frxDsEstqR1."NO_GGY"]';
      Me_FT3.Memo.Text := 'Est�gio de Artigo > [frxDsEstqR1."NO_GGY"]: ';
    end;
    10:
    begin
      Grupo3.Condition := 'frxDsEstqR1."NFeAgrup"';
      Me_GH3.Memo.Text := '[frxDsEstqR1."NFeAgrup"]';
      Me_FT3.Memo.Text := 'S�rie-NFe-Multi [frxDsEstqR1."NFeAgrup"]: ';
    end;
    11:
    begin
      Grupo3.Condition := 'frxDsEstqR1."MovimCod"';
      Me_GH3.Memo.Text := 'IME-C [frxDsEstqR1."MovimCod"]';
      Me_FT3.Memo.Text := 'IME-C [frxDsEstqR1."MovimCod"]: ';
    end;
    12:
    begin
      Grupo3.Condition := 'frxDsEstqR1."MovimID"';
      Me_GH3.Memo.Text := 'Movimento [frxDsEstqR1."NO_MovimID"]';
      Me_FT3.Memo.Text := 'Movimento [frxDsEstqR1."NO_MovimID"]: ';
    end;
    13:
    begin
      Grupo3.Condition := 'frxDsEstqR1."ClientMO"';
      Me_GH3.Memo.Text := 'Cliente M.O. [frxDsEstqR1."ClientMO"]';
      Me_FT3.Memo.Text := 'Cliente M.O. [frxDsEstqR1."NO_ClientMO"]: ';
    end;
    14:
    begin
      Grupo3.Condition := 'frxDsEstqR1."FornecMO"';
      Me_GH3.Memo.Text := 'Fornecedor M.O. [frxDsEstqR1."FornecMO"]';
      Me_FT3.Memo.Text := 'Fornecedor M.O. [frxDsEstqR1."NO_FornecMO"]: ';
    end;
    else Grupo3.Condition := '***ERRO***';
  end;
  //
  MeValNome := frxReport.FindObject('MeValNome') as TfrxMemoView;
  MeTitNome := frxReport.FindObject('MeTitNome') as TfrxMemoView;
  MeTitCodi := frxReport.FindObject('MeTitCodi') as TfrxMemoView;
  MeValCodi := frxReport.FindObject('MeValCodi') as TfrxMemoView;
  //
  if Ck00DescrAgruNoItm_Checked then
  begin
    case Index of
      0:
      begin
        MeTitCodi.Memo.Text := 'Reduzido';
        MeValCodi.Memo.Text := '[frxDsEstqR1."GraGruX"]';
        MeTitNome.Memo.Text := 'Nome da mat�ria-prima';
        MeValNome.Memo.Text := '[frxDsEstqR1."NO_PRD_TAM_COR"]';
      end;
      1:
      begin
        MeTitCodi.Memo.Text := 'Fornecedor';
        MeValCodi.Memo.Text := '[frxDsEstqR1."MulFornece"]';
        MeTitNome.Memo.Text := 'Nome do fornecedor';
        MeValNome.Memo.Text := '[frxDsEstqR1."NO_FORNECE"]';
      end;
      2:
      begin
        MeTitCodi.Memo.Text := 'Pallet/IMEI';
        MeValCodi.Memo.Text := '[frxDsEstqR1."ID_UNQ"]';
        MeTitNome.Memo.Text := 'Nome da mat�ria-prima';
        MeValNome.Memo.Text := '[frxDsEstqR1."NO_PRD_TAM_COR"]';
      end;
      3:
      begin
        MeTitCodi.Memo.Text := 'IME-I';
        MeValCodi.Memo.Text := '[frxDsEstqR1."IMEI"]';
        MeTitNome.Memo.Text := 'Nome da mat�ria-prima';
        MeValNome.Memo.Text := '[frxDsEstqR1."NO_PRD_TAM_COR"]';
      end;
      4:
      begin
        MeTitCodi.Memo.Text := 'Ficha';
        MeValCodi.Memo.Text := '[frxDsEstqR1."Ficha"]';
        MeTitNome.Memo.Text := 'S�rie';
        MeValNome.Memo.Text := '[frxDsEstqR1."NO_SerieFch"]';
      end;
      5:
      begin
        MeTitCodi.Memo.Text := 'Marca';
        MeValCodi.Memo.Text := '[frxDsEstqR1."Marca"]';
        MeTitNome.Memo.Text := 'Marca';
        MeValNome.Memo.Text := '[frxDsEstqR1."Marca"]';
      end;
      6:
      begin
        MeTitCodi.Memo.Text := 'Centro';
        MeValCodi.Memo.Text := '[frxDsEstqR1."StqCenCad"]';
        MeTitNome.Memo.Text := 'Centro de Estoque';
        MeValNome.Memo.Text := '[frxDsEstqR1."NO_StqCenCad"]';
      end;
      7:
      begin
        MeTitCodi.Memo.Text := 'Local';
        MeValCodi.Memo.Text := '[frxDsEstqR1."StqCenLoc"]';
        MeTitNome.Memo.Text := 'Local (e Centro) de Estoque';
        MeValNome.Memo.Text := '[frxDsEstqR1."NO_LOC_CEN"]';
      end;
      8:
      begin
        MeTitCodi.Memo.Text := 'Material';
        MeValCodi.Memo.Text := '[frxDsEstqR1."CouNiv2"]';
        MeTitNome.Memo.Text := 'Tipo de Material';
        MeValNome.Memo.Text := '[frxDsEstqR1."NO_CouNiv2"]';
      end;
      9:
      begin
        MeTitCodi.Memo.Text := 'Est�gio';
        MeValCodi.Memo.Text := '[frxDsEstqR1."GraGruY"]';
        MeTitNome.Memo.Text := 'Est�gio do Artigo';
        MeValNome.Memo.Text := '[frxDsEstqR1."NO_GGY"]';
      end;
      10:
      begin
        MeTitCodi.Memo.Text := 'S�rie-NFe-Multi';
        MeValCodi.Memo.Text := '[frxDsEstqR1."NFeAgrup"]';
        MeTitNome.Memo.Text := 'S�rie-NFe-Multi';
        MeValNome.Memo.Text := '[frxDsEstqR1."NFeAgrup"]';
      end;
      11:
      begin
        MeTitCodi.Memo.Text := 'IME-C';
        MeValCodi.Memo.Text := '[frxDsEstqR1."MovimCod"]';
        MeTitNome.Memo.Text := 'IME-C';
        MeValNome.Memo.Text := '[frxDsEstqR1."MovimCod"]';
      end;
      12:
      begin
        MeTitCodi.Memo.Text := 'Movimento';
        MeValCodi.Memo.Text := '[frxDsEstqR1."NO_MovimID"]';
        MeTitNome.Memo.Text := 'Movimento';
        MeValNome.Memo.Text := '[frxDsEstqR1."NO_MovimID"]';
      end;
      13:
      begin
        MeTitCodi.Memo.Text := 'Cliente M.O.';
        MeValCodi.Memo.Text := '[frxDsEstqR1."NO_ClientMO"]';
        MeTitNome.Memo.Text := 'Cliente M.O.';
        MeValNome.Memo.Text := '[frxDsEstqR1."NO_ClientMO"]';
      end;
      14:
      begin
        MeTitCodi.Memo.Text := 'Fornecedor M.O.';
        MeValCodi.Memo.Text := '[frxDsEstqR1."NO_FornecMO"]';
        MeTitNome.Memo.Text := 'Fornecedor M.O.';
        MeValNome.Memo.Text := '[frxDsEstqR1."NO_FornecMO"]';
      end;
      else
      begin
        MeTitCodi.Memo.Text := 'C�digo?';
        MeValCodi.Memo.Text := '[frxDsEstqR1."???"]';
        MeTitNome.Memo.Text := '?????';
        MeValNome.Memo.Text := '[frxDsEstqR1."???"]'
      end;
    end;
  end else
  begin
      MeTitCodi.Memo.Text := 'Reduzido';
      MeValCodi.Memo.Text := '[frxDsEstqR1."GraGruX"]';
      MeTitNome.Memo.Text := 'Nome da mat�ria-prima';
      MeValNome.Memo.Text := '[frxDsEstqR1."NO_PRD_TAM_COR"]';
  end;
  //
  MyObjects.frxDefineDataSets(frxReport, [
    DModG.frxDsDono,
    frxDsEstqR1
  ]);
  //
  NO_Arq := VS_CRC_PF.ObtemNomeFrxEstoque(FDataRetroativa,
    RG00_Agrupa_ItemIndex, RG00_Ordem1_ItemIndex, RG00_Ordem2_ItemIndex,
    RG00_Ordem3_ItemIndex, RG00_Ordem4_ItemIndex, RG00_Ordem5_ItemIndex,
    CB00StqCenCad_Text);
    MyObjects.frxMostra(frxReport, 'Estoque de Produtos', NO_Arq);
  Result := True;
end;

procedure TFmVSImpEstoque.QrEstqR1CalcFields(DataSet: TDataSet);
const
  sProcName = 'TFmVSImpEstoque.QrEstqR1CalcFields()';
begin
  case FRelatorio of
    TRelatorioVSImpEstoque.rieCusto:
    begin
      QrEstqR1ValorTx.Value := QrEstqR1ValorT.Value;
      if QrEstqR1SdoVrtArM2.Value > 0 then
        QrEstqR1CUS_UNIT_M2.Value :=
          QrEstqR1ValorT.Value / QrEstqR1SdoVrtArM2.Value
      else
        QrEstqR1CUS_UNIT_M2.Value := 0;
      //
      if QrEstqR1SdoVrtPeso.Value > 0 then
        QrEstqR1CUS_UNIT_KG.Value :=
          QrEstqR1ValorT.Value / QrEstqR1SdoVrtPeso.Value
      else
        QrEstqR1CUS_UNIT_KG.Value := 0;
      //
      QrEstqR1GGVU_pc.Value := 0;
      QrEstqR1GGVU_m2.Value := 0;
      QrEstqR1GGVU_kg.Value := 0;
      case QrEstqR1Grandeza.Value of
        0: QrEstqR1GGVU_pc.Value := QrEstqR1GraGruValU.Value;
        1: QrEstqR1GGVU_m2.Value := QrEstqR1GraGruValU.Value;
        2: QrEstqR1GGVU_kg.Value := QrEstqR1GraGruValU.Value;
      end;
    end;
    TRelatorioVSImpEstoque.rieNenhum,
    TRelatorioVSImpEstoque.rieBaseVenda:
    begin
      QrEstqR1ValorTx.Value := 0;
      QrEstqR1CUS_UNIT_M2.Value := 0;
      QrEstqR1CUS_UNIT_KG.Value := 0;
      QrEstqR1GGVU_pc.Value := 0;
      QrEstqR1GGVU_m2.Value := 0;
      QrEstqR1GGVU_kg.Value := 0;
    end;
    TRelatorioVSImpEstoque.rieBaseCusto:
    begin
      QrEstqR1ValorTx.Value := 0;
      QrEstqR1CUS_UNIT_M2.Value := 0;
      QrEstqR1CUS_UNIT_KG.Value := 0;
      QrEstqR1GGVU_pc.Value := 0;
      QrEstqR1GGVU_m2.Value := 0;
      QrEstqR1GGVU_kg.Value := 0;
    end;
    else
      Geral.MB_Erro('"TRelatorioVSImpEstoque" n�o implementado em ' + sProcName);
  end;
  //
  QrEstqR1NO_GRANDEZA.Value := s_VSGrandeza[QrEstqR1Grandeza.Value];
  //
end;

{
[IIF(SUM(<frxDsEstqR1."PalVrtPeso">,MD002,1) > 0, SUM(<frxDsEstqR1."ValorTx">,MD002,1) / SUM(<frxDsEstqR1."PalVrtPeso">,MD002,1), 0)]
[IIF(SUM(<frxDsEstqR1."PalVrtPeso">,MD002,1) > 0, SUM(IIF(<frxDsEstqR1."PalVrtPeso"> > 0, <frxDsEstqR1."ValorTx">, 0),MD002,1) / SUM(<frxDsEstqR1."PalVrtPeso">,MD002,1), 0)]
}

end.
