unit VSImpPackList;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass,
  dmkGeral, UnInternalConsts, Data.DB, mySQLDbTables, frxDBSet,
  UnProjGroup_Consts, UnDmkEnums;

type
  TFmVSImpPackList = class(TForm)
    QrEstqR5: TmySQLQuery;
    QrEstqR5Empresa: TIntegerField;
    QrEstqR5GraGruX: TIntegerField;
    QrEstqR5Pecas: TFloatField;
    QrEstqR5PesoKg: TFloatField;
    QrEstqR5AreaM2: TFloatField;
    QrEstqR5AreaP2: TFloatField;
    QrEstqR5GraGru1: TIntegerField;
    QrEstqR5NO_PRD_TAM_COR: TWideStringField;
    QrEstqR5Terceiro: TIntegerField;
    QrEstqR5NO_FORNECE: TWideStringField;
    QrEstqR5Pallet: TIntegerField;
    QrEstqR5NO_PALLET: TWideStringField;
    QrEstqR5ValorT: TFloatField;
    QrEstqR5Ativo: TSmallintField;
    QrEstqR5OrdGGX: TIntegerField;
    QrEstqR5NO_STATUS: TWideStringField;
    frxDsEstqR5: TfrxDBDataset;
    QrEstqR5NO_EMPRESA: TWideStringField;
    QrEstqR5DataHora: TDateTimeField;
    QrVSCacItsA: TmySQLQuery;
    QrVSCacItsACacCod: TIntegerField;
    QrVSCacItsACacID: TIntegerField;
    QrVSCacItsACodigo: TIntegerField;
    QrVSCacItsAControle: TLargeintField;
    QrVSCacItsAClaAPalOri: TIntegerField;
    QrVSCacItsARclAPalOri: TIntegerField;
    QrVSCacItsARclAPalDst: TIntegerField;
    QrVSCacItsAVSPaClaIts: TIntegerField;
    QrVSCacItsAVSPaRclIts: TIntegerField;
    QrVSCacItsAVSPallet: TIntegerField;
    QrVSCacItsAVMI_Sorc: TIntegerField;
    QrVSCacItsAVMI_Baix: TIntegerField;
    QrVSCacItsAVMI_Dest: TIntegerField;
    QrVSCacItsABox: TIntegerField;
    QrVSCacItsAPecas: TFloatField;
    QrVSCacItsAAreaM2: TFloatField;
    QrVSCacItsAAreaP2: TFloatField;
    QrVSCacItsARevisor: TIntegerField;
    QrVSCacItsADigitador: TIntegerField;
    QrVSCacItsADataHora: TDateTimeField;
    QrVSCacItsASumido: TSmallintField;
    QrVSCacItsAAlterWeb: TSmallintField;
    QrVSCacItsAAtivo: TSmallintField;
    frxDsVSCacItsA: TfrxDBDataset;
    QrVSCacItsASEQ: TIntegerField;
    frxWET_CURTI_034_01: TfrxReport;
    ArrayDS: TfrxUserDataSet;
    frxWET_CURTI_034_02: TfrxReport;
    QrEstqR5SdoVrtPeca: TFloatField;
    QrEstqR5SdoVrtPeso: TFloatField;
    QrEstqR5SdoVrtArM2: TFloatField;
    QrEstqR5PalVrtPeca: TFloatField;
    QrEstqR5PalVrtPeso: TFloatField;
    QrEstqR5PalVrtArM2: TFloatField;
    QrEstqR5LmbVrtPeca: TFloatField;
    QrEstqR5LmbVrtPeso: TFloatField;
    QrEstqR5LmbVrtArM2: TFloatField;
    QrEstqR5CliStat: TIntegerField;
    QrEstqR5Status: TIntegerField;
    QrEstqR5NO_CLISTAT: TWideStringField;
    QrEstqR5OrdGGY: TIntegerField;
    QrEstqR5GraGruY: TIntegerField;
    QrEstqR5NO_GGY: TWideStringField;
    QrEstqR5NO_PalStat: TWideStringField;
    QrEstqR5Media: TFloatField;
    QrEstqR5PalStat: TIntegerField;
    frxWET_CURTI_034_03: TfrxReport;
    frxWET_CURTI_034_04: TfrxReport;
    QrVSMovIts: TmySQLQuery;
    QrVSMovItsCodigo: TIntegerField;
    QrVSMovItsControle: TIntegerField;
    QrVSMovItsMovimCod: TIntegerField;
    QrVSMovItsMovimNiv: TIntegerField;
    QrVSMovItsMovimTwn: TIntegerField;
    QrVSMovItsEmpresa: TIntegerField;
    QrVSMovItsTerceiro: TIntegerField;
    QrVSMovItsCliVenda: TIntegerField;
    QrVSMovItsMovimID: TIntegerField;
    QrVSMovItsLnkNivXtr1: TIntegerField;
    QrVSMovItsLnkNivXtr2: TIntegerField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TIntegerField;
    QrVSMovItsGraGruX: TIntegerField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TIntegerField;
    QrVSMovItsSrcNivel1: TIntegerField;
    QrVSMovItsSrcNivel2: TIntegerField;
    QrVSMovItsSrcGGX: TIntegerField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TIntegerField;
    QrVSMovItsFicha: TIntegerField;
    QrVSMovItsMisturou: TSmallintField;
    QrVSMovItsFornecMO: TIntegerField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TIntegerField;
    QrVSMovItsDstNivel1: TIntegerField;
    QrVSMovItsDstNivel2: TIntegerField;
    QrVSMovItsDstGGX: TIntegerField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsAptoUso: TSmallintField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsMarca: TWideStringField;
    QrVSMovItsLk: TIntegerField;
    QrVSMovItsDataCad: TDateField;
    QrVSMovItsDataAlt: TDateField;
    QrVSMovItsUserCad: TIntegerField;
    QrVSMovItsUserAlt: TIntegerField;
    QrVSMovItsAlterWeb: TSmallintField;
    QrVSMovItsAtivo: TSmallintField;
    QrVSMovItsTpCalcAuto: TIntegerField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsGraGruY: TIntegerField;
    QrVSMovItsReqMovEstq: TIntegerField;
    QrVSMovItsStqCenLoc: TIntegerField;
    frxDsVSMovIts: TfrxDBDataset;
    procedure QrEstqR5AfterScroll(DataSet: TDataSet);
    procedure QrVSCacItsACalcFields(DataSet: TDataSet);
    procedure frxWET_CURTI_034_01GetValue(const VarName: string;
      var Value: Variant);
    procedure QrVSMovItsAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    //FVSMovImp5: String;
    procedure DefineListaCaC();
    //
  public
    { Public declarations }
    FEmpresa: Integer;
    FPallets, FIMEIs: array of Integer;
    FVertical: Boolean;
    FListaArea: array of Double;
    FListaItem: array of Integer;
    FGrandeza: TGrandezaArea;
    //
    procedure ImprimePallets(Vertical: Boolean; VSMovImp4, VSLstPalBox: String);
    procedure ImprimeIMEIs(Vertical: Boolean);
  end;

var
  FmVSImpPackList: TFmVSImpPackList;

implementation

uses Module, ModuleGeral, DmkDAC_PF, UnMyObjects, CreateBlueDerm;

{$R *.dfm}

procedure TFmVSImpPackList.DefineListaCaC;
const
  Colunas = 10;
var
  Registros, Linhas, LinAtu, ColAtu, Indice, ColMaior, RegIniColMenor, AtuVrt: Integer;
begin
  if FVertical then
  begin
    Registros := QrVSCacItsA.RecordCount;
    Linhas := (Registros + Colunas - 1) div Colunas;
    LinAtu := 0;
    ColMaior := Registros mod Colunas;
    if ColMaior = 0 then
      RegIniColMenor := 0
    else
      RegIniColMenor := (ColMaior + 1) * Linhas;
    SetLength(FListaArea, Registros);
    SetLength(FListaItem, Registros);
    QrVSCacItsA.First;
    while not QrVSCacItsA.Eof do
    begin
      if QrVSCacItsA.RecNo < RegIniColMenor then
      begin
        LinAtu := (QrVSCacItsA.RecNo - 1) mod Linhas;
        ColAtu := (QrVSCacItsA.RecNo - 1) div Linhas;
        Indice := (LinAtu * Colunas) + (ColAtu (** Colunas*));
        FListaItem[Indice] := QrVSCacItsA.RecNo;
        case TGrandezaArea(FGrandeza) of
          TGrandezaArea.grandareaAreaM2: FListaArea[Indice] := QrVSCacItsAAreaM2.Value;
          TGrandezaArea.grandareaAreaP2: FListaArea[Indice] := QrVSCacItsAAreaP2.Value;
        end;
      end else
      begin
        AtuVrt := QrVSCacItsA.RecNo - RegIniColMenor;
        LinAtu := (AtuVrt mod (Linhas - 1));
        ColAtu := (AtuVrt div (Linhas - 1)) + (ColMaior + 1);
        Indice := (LinAtu * Colunas) + (ColAtu (** Colunas*));
        FListaItem[Indice] := QrVSCacItsA.RecNo;
        case TGrandezaArea(FGrandeza) of
          TGrandezaArea.grandareaAreaM2: FListaArea[Indice] := QrVSCacItsAAreaM2.Value;
          TGrandezaArea.grandareaAreaP2: FListaArea[Indice] := QrVSCacItsAAreaP2.Value;
        end;
      end;
      //
      QrVSCacItsA.Next;
    end;
    //
    ArrayDS.RangeEnd := reCount;
    ArrayDS.RangeEndCount := Registros;
  end;
end;

procedure TFmVSImpPackList.frxWET_CURTI_034_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_DATA' then
    Value := Now()
  else
  if VarName = 'elemento' then
    Value := FListaArea[ArrayDS.RecNo]
  else
  if VarName = 'indice' then
    //Value := FListaArea[ArrayDS.RecNo]
    Value := FListaItem[ArrayDS.RecNo]
  else
  if VarName = 'VARF_REC_COUNT' then
    Value := QrVSCacItsA.RecordCount
  else
  if VarName = 'VARF_AREA_SOMA' then
  begin
    case TGrandezaArea(FGrandeza) of
      TGrandezaArea.grandareaAreaM2: Value := QrEstqR5SdoVrtArM2.Value;
      TGrandezaArea.grandareaAreaP2: Value := Geral.ConverteArea(QrEstqR5SdoVrtArM2.Value, ctM2toP2, cfQuarto);
    end;
    //Value := QrEstqR5SdoVrtArM2.Value;
  end
  else
  if VarName = 'VARF_AREA_ITEM' then
  begin
    case TGrandezaArea(FGrandeza) of
      TGrandezaArea.grandareaAreaM2: Value := QrVSCacItsAAreaM2.Value;
      TGrandezaArea.grandareaAreaP2: Value := QrVSCacItsAAreaP2.Value;
    end;
    //Value := QrVSCacItsAAreaM2.Value
  end
end;

procedure TFmVSImpPackList.ImprimeIMEIs(Vertical: Boolean);
var
  SQL_Empresa: String;
begin
  FVertical := Vertical;
  if FEmpresa <> 0 then
    SQL_Empresa := 'AND wmi.Empresa=' + Geral.FF0(FEmpresa)
  else
    SQL_Empresa := '';
  //
  //fazer ficha e testar
  UnDmkDAC_PF.AbreMySQLQUery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT wmi.*, ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR, ggx.GraGruY',
  'FROM ' + CO_SEL_TAB_VMI + ' wmi  ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=wmi.GraGruX  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=wmi.Pallet  ',
  'LEFT JOIN entidades ent ON ent.Codigo=wmi.Empresa  ',
  'LEFT JOIN entidades frn ON frn.Codigo=wmi.Terceiro ',
  'WHERE wmi.Controle IN (' + MyObjects.CordaDeArrayInt(FIMEIs) + ') ',
  'ORDER BY Controle',
  '']);
  //
  if FVertical then
  begin
    MyObjects.frxDefineDataSets(frxWET_CURTI_034_04, [
    DModG.frxDsDono,
    frxDsVSMovIts,
    ArrayDS
    ]);
    MyObjects.frxMostra(frxWET_CURTI_034_04, 'Packing List de IME-I - Vertical');
  end else
  begin
    MyObjects.frxDefineDataSets(frxWET_CURTI_034_03, [
    DModG.frxDsDono,
    frxDsVSMovIts,
    frxDsVSCacItsA
    ]);
    MyObjects.frxMostra(frxWET_CURTI_034_03, 'Packing List de IME-I - Horizontal');
  end;
end;

procedure TFmVSImpPackList.ImprimePallets(Vertical: Boolean; VSMovImp4,
VSLstPalBox: String);
var
  SQL_Empresa: String;
begin
  FVertical := Vertical;
  if FEmpresa <> 0 then
    SQL_Empresa := 'AND wmi.Empresa=' + Geral.FF0(FEmpresa)
  else
    SQL_Empresa := '';
  //
  //fazer ficha e testar
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstqR5, DModG.MyPID_DB, [
  'SELECT mi4.Empresa, mi4.GraGruX, SUM(mi4.Pecas) Pecas, SUM(mi4.PesoKg) PesoKg, ',
  'Sum(mi4.AreaM2) AreaM2, SUM(mi4.AreaP2) AreaP2, SUM(mi4.ValorT) ValorT,  ',
  'SUM(mi4.SdoVrtPeca) SdoVrtPeca, SUM(mi4.SdoVrtPeso) SdoVrtPeso,  ',
  'Sum(mi4.SdoVrtArM2) SdoVrtArM2, ',
  '',
  'SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca) PalVrtPeca,  ',
  'SUM(mi4.SdoVrtPeso+mi4.LmbVrtPeso) PalVrtPeso, ',
  'SUM(mi4.LmbVrtArM2+mi4.SdoVrtArM2) PalVrtArM2,  ',
  '',
  'SUM(mi4.LmbVrtPeca) LmbVrtPeca,  ',
  'SUM(mi4.LmbVrtPeso) LmbVrtPeso, SUM(mi4.LmbVrtArM2) LmbVrtArM2,  ',
  '',
  '',
  'mi4.GraGru1, mi4.NO_PRD_TAM_COR, mi4.Pallet, mi4.NO_PALLET, mi4.Terceiro, ',
  'mi4.CliStat, mi4.Status, mi4.NO_FORNECE,  mi4.NO_CLISTAT, mi4.NO_EMPRESA, ',
  'mi4.NO_STATUS, mi4.DataHora, mi4.OrdGGX, mi4.OrdGGY, mi4.GraGruY, ',
  'mi4.NO_GGY,StatPall PalStat, NO_StatPall NO_PalStat, mi4.Ativo,',
  'IF(SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca)  <> 0, ',
  'SUM(mi4.LmbVrtArM2+mi4.SdoVrtArM2) / SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca), 0) ',
  'Media ',
  'FROM  ' + VSMovImp4 + ' mi4',
  'WHERE (mi4.SdoVrtPeca > 0 OR mi4.LmbVrtPeca <> 0)  ',
  'AND mi4.Pallet IN (' + MyObjects.CordaDeArrayInt(FPallets) + ') ',
  'GROUP BY mi4.Pallet ',
  'ORDER BY Pallet DESC, PalStat ',
  //
  '']);
  //
  if FVertical then
  begin
    MyObjects.frxDefineDataSets(frxWET_CURTI_034_02, [
    DModG.frxDsDono,
    frxDsEstqR5,
    ArrayDS
    ]);
    MyObjects.frxMostra(frxWET_CURTI_034_02, 'Packing List de Pallet - Vertical');
  end else
  begin
    MyObjects.frxDefineDataSets(frxWET_CURTI_034_01, [
    DModG.frxDsDono,
    frxDsEstqR5,
    frxDsVSCacItsA
    ]);
    MyObjects.frxMostra(frxWET_CURTI_034_01, 'Packing List de Pallet - Horizontal');
  end;
end;

procedure TFmVSImpPackList.QrEstqR5AfterScroll(DataSet: TDataSet);
const
  Colunas = 10;
var
  Registros, Linhas, LinAtu, ColAtu, Indice, ColMaior, RegIniColMenor, AtuVrt: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacItsA, Dmod.MyDB, [
  'SELECT * ',
  'FROM vscacitsa ',
  'WHERE VSPallet=' + Geral.FF0(QrEstqR5Pallet.Value),
  'ORDER BY Controle DESC ',
  '']);
  //
  DefineListaCaC();
end;

procedure TFmVSImpPackList.QrVSCacItsACalcFields(DataSet: TDataSet);
begin
  QrVSCacItsASEQ.Value := QrVSCacItsA.RecNo;
end;

procedure TFmVSImpPackList.QrVSMovItsAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSCacItsA, Dmod.MyDB, [
  'SELECT * ',
  'FROM vscacitsa ',
  'WHERE VMI_Sorc=' + Geral.FF0(QrVSMovItsControle.Value),
  'ORDER BY Controle DESC ',
  '']);
  //
  DefineListaCaC();
end;

end.
