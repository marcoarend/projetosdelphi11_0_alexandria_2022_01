unit VSImpDif;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  math,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass, frxDBSet, Data.DB,
  mySQLDbTables, Vcl.Grids, Vcl.DBGrids, UnProjGroup_Consts, UnDmkEnums;

type
  TFmVSImpDif = class(TForm)
    QrMovDif: TmySQLQuery;
    DsMovDif: TDataSource;
    frxDsMovDif: TfrxDBDataset;
    frxWET_CURTI_018_08_A: TfrxReport;
    frxWET_CURTI_018_08_B: TfrxReport;
    DBGMovDif: TDBGrid;
    Qr08MovAjustes: TmySQLQuery;
    Ds08MovAjustes: TDataSource;
    frxDs08MovAjustes: TfrxDBDataset;
    frxWET_CURTI_018_08_01_A: TfrxReport;
    QrMovDifDATA_TXT: TWideStringField;
    QrMovDifInfPecas: TFloatField;
    QrMovDifInfPesoKg: TFloatField;
    QrMovDifInfAreaM2: TFloatField;
    QrMovDifinfAreaP2: TFloatField;
    QrMovDifinfValorT: TFloatField;
    QrMovDifDifPecas: TFloatField;
    QrMovDifDifPesoKg: TFloatField;
    QrMovDifDifAreaM2: TFloatField;
    QrMovDifDifAreaP2: TFloatField;
    QrMovDifDifValorT: TFloatField;
    QrMovDifObserv: TWideStringField;
    QrMovDifNO_SerieFch: TWideStringField;
    QrMovDifNO_PRD_TAM_COR: TWideStringField;
    QrMovDifNO_Pallet: TWideStringField;
    QrMovDifRendKgm2: TFloatField;
    QrMovDifRendKgm2_TXT: TWideStringField;
    QrMovDifNotaMPAG_TXT: TWideStringField;
    QrMovDifMisturou_TXT: TWideStringField;
    QrMovDifSIGLAUNIDMED: TWideStringField;
    QrMovDifNOMEUNIDMED: TWideStringField;
    QrMovDifm2_CouroTXT: TWideStringField;
    QrMovDifKgMedioCouro: TFloatField;
    QrMovDifNO_FORNECE: TWideStringField;
    QrMovDifPecas: TFloatField;
    QrMovDifPesoKg: TFloatField;
    QrMovDifAreaM2: TFloatField;
    QrMovDifValorT: TFloatField;
    QrMovDifDataHora: TDateTimeField;
    QrMovDifAreaP2: TFloatField;
    Qr08MovAjustesDataHora: TDateTimeField;
    Qr08MovAjustesPecas: TFloatField;
    Qr08MovAjustesPesoKg: TFloatField;
    Qr08MovAjustesAreaM2: TFloatField;
    Qr08MovAjustesAreaP2: TFloatField;
    Qr08MovAjustesValorT: TFloatField;
    Qr08MovAjustesNO_MovimID: TWideStringField;
    Qr08MovAjustesNO_MovimNiv: TWideStringField;
    Qr08MovAjustesNO_PRD_TAM_COR: TWideStringField;
    Qr08MovAjustesNO_Pallet: TWideStringField;
    Qr08MovAjustesVSP_CliStat: TLargeintField;
    Qr08MovAjustesVSP_STATUS: TLargeintField;
    Qr08MovAjustesNO_FORNECE: TWideStringField;
    Qr08MovAjustesNO_CliStat: TWideStringField;
    Qr08MovAjustesNO_EMPRESA: TWideStringField;
    Qr08MovAjustesNO_STATUS: TWideStringField;
    Qr08MovAjustesNO_SerieFch: TWideStringField;
    Qr08MovAjustesNO_SERIE_FICHA: TWideStringField;
    Qr08MovAjustesInteiros: TFloatField;
    Qr08MovAjustesData_TXT: TWideStringField;
    Qr08MovAjustesData_BR: TWideStringField;
    Qr08MovAjustesSentidoMov: TFloatField;
    Qr08MovAjustesSentidoMov_TXT: TWideStringField;
    Qr08MovAjustesGraGruY: TIntegerField;
    Qr08MovAjustesNO_GGY: TWideStringField;
    Qr08MovAjustesPallet: TLargeintField;
    Qr08MovAjustesCodigo: TLargeintField;
    Qr08MovAjustesMovimCod: TLargeintField;
    Qr08MovAjustesControle: TLargeintField;
    Qr08MovAjustesGraGruX: TLargeintField;
    Qr08MovAjustesTerceiro: TLargeintField;
    Qr08MovAjustesSerieFch: TLargeintField;
    Qr08MovAjustesFicha: TLargeintField;
    Qr08MovAjustesMovimNiv: TLargeintField;
    Qr08MovAjustesMovimID: TLargeintField;
    QrMovDifControle: TLargeintField;
    QrMovDifGraGruX: TLargeintField;
    QrMovDifTerceiro: TLargeintField;
    QrMovDifSerieFch: TLargeintField;
    QrMovDifFicha: TLargeintField;
    procedure frxWET_CURTI_018_08_AGetValue(const VarName: string;
      var Value: Variant);
    procedure frxWET_CURTI_018_08_01_AGetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
  public
    { Public declarations }
    FUsaVSMovItb: Boolean;
    FPeriodo: String;
    FEmpresa_Cod, FTerceiro_Cod, FTipoMovim_Cod, FAgrupa, FOrdem: Integer;
    FEmpresa_Txt, FTerceiro_Txt, FTipoMovim_Txt: String;
    FDataIniVal, FDataFimVal: TDateTime;
    FDataIniUsa, FDataFimUsa, FImpSimples: Boolean;
    FOrdem1, FOrdem2: Integer;
    //
    FMovimIDSign: Integer;
    FMovimIDResi: Integer;
    FSdoResidual: Boolean; // 2021-03-30
    //
    procedure PesquisaFaltaDeCouros();
    procedure ImprimeMovimentos();
    function  DefineMorto(): Integer;
  end;

var
  FmVSImpDif: TFmVSImpDif;

implementation

{$R *.dfm}

uses UnDmkProcFunc, DmkGeral, UnMyObjects, Module, ModuleGeral, DmkDAC_PF,
  AppListas, UnInternalConsts, UnVS_PF;

{ TFmVSImpDif }

procedure TFmVSImpDif.frxWET_CURTI_018_08_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', FEmpresa_Txt, FEmpresa_Cod, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_PERIODO' then
    Value := FPeriodo
  else
  if VarName = 'VARF_TERCEIRO_1' then
    Value := dmkPF.ParValueCodTxt(
      '', FTerceiro_Txt, FTerceiro_Cod, 'TODOS')
  else
  if VarName = 'VARF_PERIODO_1' then
    Value := dmkPF.PeriodoImp1(FDataIniVal, FDataFimVal,
    FDataIniUsa, FDataFimUsa, '', 'at�', '')
  else
  if VarName = 'VARF_AGRUP1' then
    Value := FAgrupa >= 1
  else
  if VarName = 'VARF_TIPO_MOV' then
    Value := FTipoMovim_Txt
  else
  //
end;

function TFmVSImpDif.DefineMorto(): Integer;
begin
  if FUsaVSMovItb then
    Result := 1
  else
    Result := 0;
end;

procedure TFmVSImpDif.frxWET_CURTI_018_08_01_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', FEmpresa_Txt, FEmpresa_Cod, 'TODAS')
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_PERIODO' then
    Value := FPeriodo
  else
  if VarName = 'VARF_TITULO' then
  begin
    if FSdoResidual then
      Value := 'Relat�rio de Couros Excedentes (Sobras)'
    else
      Value := 'Ajustes de Estoque de Couros Inteiros';
  end;
end;

procedure TFmVSImpDif.ImprimeMovimentos();
const
  Ordens: array[0..4] of String = (
  'SentidoMov', 'GraGruY', 'NO_MovimID', 'NO_PRD_TAM_COR', 'Data_TXT');
var
  Ordem, ATT_MovimID, ATT_MovimNiv, SQL_GraGruX, SQL_SohInnOut: String;
  Grupo1, Grupo2: TfrxGroupHeader;
  Futer1, Futer2: TfrxGroupFooter;
  Me_GH1, Me_FT1, Me_GH2, Me_FT2, MeValNome, MeTitNome, MeTitCodi, MeValCodi: TfrxMemoView;
  Index: Integer;
  //
  SQL_MovIDs, SQL_OR, SQL_Periodo: String;
  //
{
  procedure AgregaMovID(Pos, Neg, MovimID: Integer);
  var
    TxtID: String;
  begin
    TxtID := Geral.FF0(MovimID);
    if (dmkPF.IntInConjunto2(Pos, FMovimIDSign)) and
       (dmkPF.IntInConjunto2(Neg, FMovimIDSign)) then
    begin
      SQL_MovIDs := SQL_MovIDs + SQL_OR + '(MovimID=' + TxtID + ') ' + sLineBreak;
      SQL_OR := ' OR ' + sLineBreak;
    end else
    if dmkPF.IntInConjunto2(pos, FMovimIDSign) then
    begin
      SQL_MovIDs := SQL_MovIDs + SQL_OR + '(MovimID=' + TxtID + ' AND Pecas > 0) ' + sLineBreak;
      SQL_OR := ' OR ' + sLineBreak;
    end else
    if dmkPF.IntInConjunto2(Neg, FMovimIDSign) then
    begin
      SQL_MovIDs := SQL_MovIDs + SQL_OR + '(MovimID=' + TxtID + ' AND Pecas < 0) ' + sLineBreak;
      SQL_OR := ' OR ' + sLineBreak;
    end;
  end;
}
  //
  procedure AgregaMovID(Pos, Neg, MovimID: Integer);
  var
    TxtID, Campo: String;
  begin
    if FSdoResidual then
    begin
      if dmkPF.IntInConjunto2(Neg, FMovimIDResi) then
      begin
        TxtID := Geral.FF0(MovimID);
        SQL_MovIDs := SQL_MovIDs + SQL_OR + '(MovimID=' + TxtID + ' AND SdoVrtPeca < 0) ' + sLineBreak;
        SQL_OR := ' OR ' + sLineBreak;
      end;
    end else
    begin
      TxtID := Geral.FF0(MovimID);
      if (dmkPF.IntInConjunto2(Pos, FMovimIDSign)) and
         (dmkPF.IntInConjunto2(Neg, FMovimIDSign)) then
      begin
        SQL_MovIDs := SQL_MovIDs + SQL_OR + '(MovimID=' + TxtID + ') ' + sLineBreak;
        SQL_OR := ' OR ' + sLineBreak;
      end else
      if dmkPF.IntInConjunto2(pos, FMovimIDSign) then
      begin
        SQL_MovIDs := SQL_MovIDs + SQL_OR + '(MovimID=' + TxtID + ' AND Pecas > 0) ' + sLineBreak;
        SQL_OR := ' OR ' + sLineBreak;
      end else
      if dmkPF.IntInConjunto2(Neg, FMovimIDSign) then
      begin
        SQL_MovIDs := SQL_MovIDs + SQL_OR + '(MovimID=' + TxtID + ' AND Pecas < 0) ' + sLineBreak;
        SQL_OR := ' OR ' + sLineBreak;
      end;
    end;
  end;
  //
  function GeraSQLVSMovItxCorretivo(Tab: TTabToWork; TemIMEIMrt: Integer = 0): String;
  begin
    if VS_PF.GeraSQLTabMov(Result, Tab, TemIMEIMrt) then
      Result := Geral.ATS([
      'SELECT ' + VS_PF.TabMovVS_Fld_IMEI(tab),
      'CAST(vmi.Codigo AS SIGNED) Codigo, ',
      'CAST(vmi.Controle AS SIGNED) Controle, ',
      'CAST(vmi.MovimCod AS SIGNED) MovimCod, ',
      'CAST(vmi.MovimNiv AS SIGNED) MovimNiv, ',
      'CAST(vmi.MovimID AS SIGNED) MovimID, ',
      'CAST(vmi.Terceiro AS SIGNED) Terceiro, ',
      'CAST(vmi.Pallet AS SIGNED) Pallet, ',
      'CAST(vmi.GraGruX AS SIGNED) GraGruX, ',
      'CAST(vmi.SerieFch AS SIGNED) SerieFch, ',
      'CAST(vmi.Ficha AS SIGNED) Ficha, ',
      'CAST(vmi.DataHora AS DATETIME) DataHora, ',
      'CAST(vmi.Pecas AS DECIMAL (15,3)) Pecas, ',
      'CAST(vmi.PesoKg AS DECIMAL (15,3)) PesoKg, ',
      'CAST(vmi.AreaM2 AS DECIMAL (15,2)) AreaM2, ',
      'CAST(vmi.AreaP2 AS DECIMAL (15,2)) AreaP2, ',
      'CAST(vmi.ValorT AS DECIMAL (15,2)) ValorT, ',
      'CAST(vmi.SdoVrtPeca AS DECIMAL (15,3)) SdoVrtPeca, ',
      'CAST(vmi.SdoVrtPeso AS DECIMAL (15,3)) SdoVrtPeso, ',
      'CAST(vmi.SdoVrtArM2 AS DECIMAL (15,2)) SdoVrtArM2, ',
      'CAST(vmi.Observ AS CHAR) Observ, ',
      ATT_MovimID,
      ATT_MovimNiv,
      VS_PF.SQL_NO_GGX(),
      'vsp.Nome NO_Pallet, ',
      'IF(vsp.CliStat IS NULL, 0, vsp.CliStat) VSP_CliStat, ',
      'IF(vsp.Status IS NULL, 0, vsp.Status) VSP_STATUS, ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, ',
      'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",   ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,  ',
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
      'vps.Nome NO_STATUS, ',
      'vsf.Nome NO_SerieFch, CONCAT(vsf.Nome, " ", vmi.Ficha) NO_SERIE_FICHA, ',
      'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros, ',
      'DATE_FORMAT(vmi.DataHora, "%Y/%m/%d") Data_TXT, ',
      'DATE_FORMAT(vmi.DataHora, "%d/%m/%Y") Data_BR, ',
      'IF(vmi.Pecas < 0, -1.000, 1.000) SentidoMov, ',
      'IF(vmi.Pecas < 0, "Sa�da", "Entrada") SentidoMov_TXT, ',
      'ggx.GraGruY, ggy.Nome NO_GGY ',
      'FROM ' + VS_PF.TabMovVS_Tab(tab) + ' vmi ',
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet   ',
      'LEFT JOIN gragrux    ggx ON ggx.Controle=IF(vmi.Pallet <> 0, vsp.GraGruX, vmi.GraGruX) ',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro ',
      'LEFT JOIN entidades  emp ON emp.Codigo=vmi.Empresa ',
      'LEFT JOIN vspalsta   vps ON vps.Codigo=vsp.Status  ',
      'LEFT JOIN couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
      SQL_Periodo,
      SQL_MovIDs,
      Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
      '']);
  end;
  function GeraSQLVSMovItxResidual(Tab: TTabToWork; TemIMEIMrt: Integer = 0): String;
  begin
    if VS_PF.GeraSQLTabMov(Result, Tab, TemIMEIMrt) then
      Result := Geral.ATS([
      'SELECT ' + VS_PF.TabMovVS_Fld_IMEI(tab),
      'CAST(vmi.Codigo AS SIGNED) Codigo, ',
      'CAST(vmi.Controle AS SIGNED) Controle, ',
      'CAST(vmi.MovimCod AS SIGNED) MovimCod, ',
      'CAST(vmi.MovimNiv AS SIGNED) MovimNiv, ',
      'CAST(vmi.MovimID AS SIGNED) MovimID, ',
      'CAST(vmi.Terceiro AS SIGNED) Terceiro, ',
      'CAST(vmi.Pallet AS SIGNED) Pallet, ',
      'CAST(vmi.GraGruX AS SIGNED) GraGruX, ',
      'CAST(vmi.SerieFch AS SIGNED) SerieFch, ',
      'CAST(vmi.Ficha AS SIGNED) Ficha, ',
      'CAST(vmi.DataHora AS DATETIME) DataHora, ',
      // ini Difere aqui!!!
      'CAST(-vmi.SdoVrtPeca AS DECIMAL (15,3)) Pecas, ',
      'CAST(-vmi.SdoVrtPeso AS DECIMAL (15,3)) PesoKg, ',
      'CAST(-vmi.SdoVrtArM2 AS DECIMAL (15,2)) AreaM2, ',
      'CAST(-vmi.SdoVrtArM2*10.7639104167 AS DECIMAL (15,2)) AreaP2, ',
      // fim Difere aqui!!!
      'CAST(vmi.ValorT AS DECIMAL (15,2)) ValorT, ',
      'CAST(vmi.SdoVrtPeca AS DECIMAL (15,3)) SdoVrtPeca, ',
      'CAST(vmi.SdoVrtPeso AS DECIMAL (15,3)) SdoVrtPeso, ',
      'CAST(vmi.SdoVrtArM2 AS DECIMAL (15,2)) SdoVrtArM2, ',
      'CAST(vmi.Observ AS CHAR) Observ, ',
      ATT_MovimID,
      ATT_MovimNiv,
      VS_PF.SQL_NO_GGX(),
      'vsp.Nome NO_Pallet, ',
      'IF(vsp.CliStat IS NULL, 0, vsp.CliStat) VSP_CliStat, ',
      'IF(vsp.Status IS NULL, 0, vsp.Status) VSP_STATUS, ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, ',
      'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",   ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,  ',
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
      'vps.Nome NO_STATUS, ',
      'vsf.Nome NO_SerieFch, CONCAT(vsf.Nome, " ", vmi.Ficha) NO_SERIE_FICHA, ',
      // ini Difere aqui!!!
      'vmi.SdoVrtPeca * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros, ',
      // fim Difere aqui!!!
      'DATE_FORMAT(vmi.DataHora, "%Y/%m/%d") Data_TXT, ',
      'DATE_FORMAT(vmi.DataHora, "%d/%m/%Y") Data_BR, ',
      'IF(vmi.Pecas < 0, -1.000, 1.000) SentidoMov, ',
      'IF(vmi.Pecas < 0, "Sa�da", "Entrada") SentidoMov_TXT, ',
      'ggx.GraGruY, ggy.Nome NO_GGY ',
      'FROM ' + VS_PF.TabMovVS_Tab(tab) + ' vmi ',
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet   ',
      'LEFT JOIN gragrux    ggx ON ggx.Controle=IF(vmi.Pallet <> 0, vsp.GraGruX, vmi.GraGruX) ',
      'LEFT JOIN gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
      'LEFT JOIN gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro ',
      'LEFT JOIN entidades  emp ON emp.Codigo=vmi.Empresa ',
      'LEFT JOIN vspalsta   vps ON vps.Codigo=vsp.Status  ',
      'LEFT JOIN couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
      'LEFT JOIN couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
      SQL_Periodo,
      SQL_MovIDs,
      Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
      '']);
  end;
begin
  SQL_OR := '';
  SQL_MovIDs := '';
  //
  if FSdoResidual then
  begin
    AgregaMovID(0, Trunc(Power(2,  0)), 01);
    AgregaMovID(0, Trunc(Power(2,  1)), 06);
    AgregaMovID(0, Trunc(Power(2,  2)), 07);
    AgregaMovID(0, Trunc(Power(2,  3)), 08);
    AgregaMovID(0, Trunc(Power(2,  4)), 10);
    AgregaMovID(0, Trunc(Power(2,  5)), 11);
    AgregaMovID(0, Trunc(Power(2,  6)), 13);
    AgregaMovID(0, Trunc(Power(2,  7)), 14);
    AgregaMovID(0, Trunc(Power(2,  8)), 15);
    AgregaMovID(0, Trunc(Power(2,  9)), 16);
    AgregaMovID(0, Trunc(Power(2, 10)), 19);
    AgregaMovID(0, Trunc(Power(2, 11)), 20);
    AgregaMovID(0, Trunc(Power(2, 12)), 21);
    AgregaMovID(0, Trunc(Power(2, 13)), 22);
    AgregaMovID(0, Trunc(Power(2, 14)), 23);
    AgregaMovID(0, Trunc(Power(2, 15)), 24);
    AgregaMovID(0, Trunc(Power(2, 16)), 25);
    AgregaMovID(0, Trunc(Power(2, 17)), 26);
    AgregaMovID(0, Trunc(Power(2, 18)), 27);
    AgregaMovID(0, Trunc(Power(2, 19)), 28);
    AgregaMovID(0, Trunc(Power(2, 20)), 29);
    AgregaMovID(0, Trunc(Power(2, 21)), 30);
    AgregaMovID(0, Trunc(Power(2, 22)), 31);
    AgregaMovID(0, Trunc(Power(2, 23)), 32);
    AgregaMovID(0, Trunc(Power(2, 24)), 33);
    AgregaMovID(0, Trunc(Power(2, 25)), 34);
    AgregaMovID(0, Trunc(Power(2, 26)), 36);
    AgregaMovID(0, Trunc(Power(2, 27)), 37);
    AgregaMovID(0, Trunc(Power(2, 28)), 38);
  end else
  begin
    AgregaMovID(1, 2, 0);
    AgregaMovID(4, 8, 9);
    AgregaMovID(16, 32, 12);
    AgregaMovID(64, 128, 13);
    AgregaMovID(256, 512, 17);
  end;
  //
  SQL_MovIDs := 'AND (' + sLineBreak + SQL_MovIDs + ') ';
  //
  SQL_Periodo := dmkPF.SQL_Periodo('WHERE vmi.DataHora ', FDataIniVal,
    FDataFimVal, FDataIniUsa, FDataFimUsa);
  //
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
    //
  Ordem := 'ORDER BY ' +
  Ordens[FOrdem1] + ', ' +
  Ordens[FOrdem2] + ', ' +
  'DataHora, Controle ';
  //
(*
  UnDmkDAC_PF.AbreMySQLQuery0(Qr08MovAjustes, DModG.MyPID_DB, [
  'SELECT vmi.Codigo, vmi.MovimCod, vmi.Controle, vmi.Pallet, ',
  'vmi.GraGruX, vmi.Terceiro, vmi.SerieFch, vmi.Ficha, ',
  'vmi.DataHora, vmi.MovimNiv, vmi.MovimID, ',
  'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, vmi.AreaP2, vmi.ValorT,',
  ATT_MovimID,
  ATT_MovimNiv,
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, vsp.Nome NO_Pallet, ',
  'IF(vsp.CliStat IS NULL, 0, vsp.CliStat) VSP_CliStat, ',
  'IF(vsp.Status IS NULL, 0, vsp.Status) VSP_STATUS, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, ',
  'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",   ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,  ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
  'vps.Nome NO_STATUS, ',
  'vsf.Nome NO_SerieFch, CONCAT(vsf.Nome, " ", vmi.Ficha) NO_SERIE_FICHA, ',
  'vmi.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) Inteiros, ',
  'DATE_FORMAT(vmi.DataHora, "%Y/%m/%d") Data_TXT, ',
  'DATE_FORMAT(vmi.DataHora, "%d/%m/%Y") Data_BR, ',
  'IF(vmi.Pecas < 0, -1.000, 1.000) SentidoMov, ',
  'IF(vmi.Pecas < 0, "Sa�da", "Entrada") SentidoMov_TXT, ',
  'ggx.GraGruY, ggy.Nome NO_GGY ',
  'FROM ' + TMeuDB + '. v s m o v i t s vmi ',
  'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=vmi.Pallet   ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.Pallet <> 0, vsp.GraGruX, vmi.GraGruX) ',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vmi.Terceiro ',
  'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vmi.Empresa ',
  'LEFT JOIN ' + TMeuDB + '.vspalsta   vps ON vps.Codigo=vsp.Status  ',
  'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
  'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
  'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
  SQL_Periodo,
  SQL_MovIDs,
  Ordem,
  '']);
*)
  if FSdoResidual then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr08MovAjustes, Dmod.MyDB, [
    GeraSQLVSMovItxResidual(ttwB, DefineMorto()),
    GeraSQLVSMovItxResidual(ttwA),
    Ordem,
    '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr08MovAjustes, Dmod.MyDB, [
    GeraSQLVSMovItxCorretivo(ttwB, DefineMorto()),
    GeraSQLVSMovItxCorretivo(ttwA),
    Ordem,
    '']);
  end;
  //Geral.MB_Teste(Qr08MovAjustes.SQL.Text);
  //
  Grupo1 := frxWET_CURTI_018_08_01_A.FindObject('GH_01') as TfrxGroupHeader;
  Grupo1.Visible := FAgrupa > 0;
  Futer1 := frxWET_CURTI_018_08_01_A.FindObject('FT_01') as TfrxGroupFooter;
  Futer1.Visible := Grupo1.Visible;
  Me_GH1 := frxWET_CURTI_018_08_01_A.FindObject('Me_GH1') as TfrxMemoView;
  Me_FT1 := frxWET_CURTI_018_08_01_A.FindObject('Me_FT1') as TfrxMemoView;
  case FOrdem1 of
    0: // -> Sentido do movimento
    begin
      Grupo1.Condition := 'frxDs08MovAjustes."SentidoMov"';
      Me_GH1.Memo.Text := '[frxDs08MovAjustes."SentidoMov_TXT"]';
      Me_FT1.Memo.Text := 'Movimento de: [frxDs08MovAjustes."SentidoMov_TXT"]: ';
    end;
    1:  // -> Grupo de estoque
    begin
      Grupo1.Condition := 'frxDs08MovAjustes."GraGruY"';
      Me_GH1.Memo.Text := '[frxDs08MovAjustes."GraGruY"]   [frxDs08MovAjustes."NO_GGY"] ';
      Me_FT1.Memo.Text := 'Grupo de estoque: [frxDs08MovAjustes."GraGruY"]   [frxDs08MovAjustes."NO_GGY"] ';
    end;
    2: // -> Processo / opera��o
    begin
      Grupo1.Condition := 'frxDs08MovAjustes."MovimID"';
      Me_GH1.Memo.Text := '[frxDs08MovAjustes."NO_MovimID"]';
      Me_FT1.Memo.Text := 'Processo / opera��o: [frxDs08MovAjustes."NO_MovimID"]: ';
    end;
    3:  // -> Artigo
    begin
      Grupo1.Condition := 'frxDs08MovAjustes."GraGruX"';
      Me_GH1.Memo.Text := '[frxDs08MovAjustes."NO_PRD_TAM_COR"]';
      Me_FT1.Memo.Text := 'Pallet: [frxDs08MovAjustes."GraGruX"]   [frxDs08MovAjustes."NO_PRD_TAM_COR"]';
    end;
    4:  // -> Data
    begin
      Grupo1.Condition := 'frxDs08MovAjustes."Data_TXT"';
      Me_GH1.Memo.Text := '[frxDs08MovAjustes."Data_BR"]';
      Me_FT1.Memo.Text := 'Data: [frxDs08MovAjustes."Data_BR"]';
    end;
    else
    begin
      Grupo1.Condition := '***ERRO***';
      Me_GH1.Memo.Text := '***ERRO***';
      Me_FT1.Memo.Text := '***ERRO***';
    end;
  end;
  //
  //
  Grupo2 := frxWET_CURTI_018_08_01_A.FindObject('GH_02') as TfrxGroupHeader;
  Grupo2.Visible := FAgrupa > 1;
  Futer2 := frxWET_CURTI_018_08_01_A.FindObject('FT_02') as TfrxGroupFooter;
  Futer2.Visible := Grupo2.Visible;
  Me_GH2 := frxWET_CURTI_018_08_01_A.FindObject('Me_GH2') as TfrxMemoView;
  Me_FT2 := frxWET_CURTI_018_08_01_A.FindObject('Me_FT2') as TfrxMemoView;
  case FOrdem2 of
    0: // -> Sentido do movimento
    begin
      Grupo2.Condition := 'frxDs08MovAjustes."SentidoMov"';
      Me_GH2.Memo.Text := '[frxDs08MovAjustes."SentidoMov_TXT"]';
      Me_FT2.Memo.Text := 'Movimento de: [frxDs08MovAjustes."SentidoMov_TXT"]: ';
    end;
    1:  // -> Grupo de estoque
    begin
      Grupo2.Condition := 'frxDs08MovAjustes."GraGruY"';
      Me_GH2.Memo.Text := '[frxDs08MovAjustes."GraGruY"]   [frxDs08MovAjustes."NO_GGY"] ';
      Me_FT2.Memo.Text := 'Grupo de estoque: [frxDs08MovAjustes."GraGruY"]   [frxDs08MovAjustes."NO_GGY"] ';
    end;
    2: // -> Processo / opera��o
    begin
      Grupo2.Condition := 'frxDs08MovAjustes."MovimID"';
      Me_GH2.Memo.Text := '[frxDs08MovAjustes."NO_MovimID"]';
      Me_FT2.Memo.Text := 'Processo / opera��o: [frxDs08MovAjustes."NO_MovimID"]: ';
    end;
    3:  // -> Artigo
    begin
      Grupo2.Condition := 'frxDs08MovAjustes."GraGruX"';
      Me_GH2.Memo.Text := '[frxDs08MovAjustes."NO_PRD_TAM_COR"]';
      Me_FT2.Memo.Text := 'Pallet: [frxDs08MovAjustes."GraGruX"]   [frxDs08MovAjustes."NO_PRD_TAM_COR"]';
    end;
    4:  // -> Data
    begin
      Grupo2.Condition := 'frxDs08MovAjustes."Data_TXT"';
      Me_GH2.Memo.Text := '[frxDs08MovAjustes."Data_BR"]';
      Me_FT2.Memo.Text := 'Data: [frxDs08MovAjustes."Data_BR"]';
    end;
    else
    begin
      Grupo2.Condition := '***ERRO***';
      Me_GH2.Memo.Text := '***ERRO***';
      Me_FT2.Memo.Text := '***ERRO***';
    end;
  end;
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_018_08_01_A, [
    DModG.frxDsDono,
    frxDs08MovAjustes
  ]);
  MyObjects.frxMostra(frxWET_CURTI_018_08_01_A, 'Movimento de Ajuse');
end;

procedure TFmVSImpDif.PesquisaFaltaDeCouros();
var
  Terceiro: Integer;
  SQL_Periodo, SQL_Terceiro, SQL_ORDEM: String;
  //
  Report: TfrxReport;
  Grupo0: TfrxGroupHeader;
  Futer0: TfrxGroupFooter;
  Me_GH0, Me_FT0, MeValNome, MeTitNome, MeValCodi: TfrxMemoView;
  //
  function GeraSQLVSMovItx(Tab: TTabToWork; TemIMEIMrt: Integer = 0): String;
  begin
    if VS_PF.GeraSQLTabMov(Result, Tab, TemIMEIMrt) then
      Result := Geral.ATS([
      'SELECT ' + VS_PF.TabMovVS_Fld_IMEI(tab),
      'CAST(vmi.Codigo AS SIGNED) Codigo, ',
      'CAST(vmi.Controle AS SIGNED) Controle, ',
      'CAST(vmi.MovimCod AS SIGNED) MovimCod, ',
      'CAST(vmi.MovimNiv AS SIGNED) MovimNiv, ',
      'CAST(vmi.MovimID AS SIGNED) MovimID, ',
      'CAST(vmi.Terceiro AS SIGNED) Terceiro, ',
      'CAST(vmi.Pallet AS SIGNED) Pallet, ',
      'CAST(vmi.GraGruX AS SIGNED) GraGruX, ',
      'CAST(vmi.SerieFch AS SIGNED) SerieFch, ',
      'CAST(vmi.Ficha AS SIGNED) Ficha, ',
      'CAST(vmi.DataHora AS DATETIME) DataHora, ',
      'CAST(vmi.Pecas AS DECIMAL (15,3)) Pecas, ',
      'CAST(vmi.PesoKg AS DECIMAL (15,3)) PesoKg, ',
      'CAST(vmi.AreaM2 AS DECIMAL (15,2)) AreaM2, ',
      'CAST(vmi.AreaP2 AS DECIMAL (15,2)) AreaP2, ',
      'CAST(vmi.ValorT AS DECIMAL (15,2)) ValorT, ',
      'CAST(vmi.SdoVrtPeca AS DECIMAL (15,3)) SdoVrtPeca, ',
      'CAST(vmi.SdoVrtPeso AS DECIMAL (15,3)) SdoVrtPeso, ',
      'CAST(vmi.SdoVrtArM2 AS DECIMAL (15,2)) SdoVrtArM2, ',
      'CAST(vmi.Observ AS CHAR) Observ, ',
      //
      'vsf.Nome NO_SerieFch, ',
      'DATE_FORMAT(vmi.DataHora, "%d/%m/%Y") DATA_TXT,',
      'vmd.InfPecas, vmd.InfPesoKg, vmd.InfAreaM2, ',
      'vmd.infAreaP2, vmd.infValorT, vmd.DifPecas, vmd.DifPesoKg, ',
      'vmd.DifAreaM2, vmd.DifAreaP2, vmd.DifValorT, ',
      'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, vmi.AreaP2, vmi.ValorT, ',
      //
      VS_PF.SQL_NO_GGX(),
      'vsp.Nome NO_Pallet, ',
      //
      'IF(vmi.SdoVrtPeca > 0, 0,',
      '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2,',
      'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
      '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3),',
      '  ",", ""), ".", ",")) RendKgm2_TXT,',
      'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(',
      'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT,',
      'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,',
      'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,',
      'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
      '  IF(vmi.Pecas > 0, vmi.QtdGerArM2 / vmi.Pecas, 0), 3),',
      '  ",", ""), ".", ",")) m2_CouroTXT,',
      'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro, ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE ',
      'FROM vsmovdif vmd',
      'LEFT JOIN ' + VS_PF.TabMovVS_Tab(tab) + ' vmi ON vmi.Controle=vmd.Controle',
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet',
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch',
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed',
      'LEFT JOIN entidades ent ON ent.Codigo=vmi.Terceiro ',
      'WHERE vmd.DifPecas <> 0',
      SQL_Periodo,
      SQL_Terceiro,
      //
      Geral.ATS_If(Tab <> ttwA, ['', 'UNION']),
      '']);
  end;
begin
  case FTipoMovim_Cod of
    0:
    begin
      SQL_Periodo := dmkPF.SQL_Periodo('AND vmi.DataHora ', FDataIniVal,
        FDataFimVal, FDataIniUsa, FDataFimUsa);
      //
      Terceiro := FTerceiro_Cod;
      if Terceiro <> 0 then
        SQL_Terceiro := 'AND vmi.Terceiro=' + Geral.FF0(Terceiro)
      else
        SQL_Terceiro := '';
      //
      case FOrdem of
        0: SQL_ORDEM := 'ORDER BY DataHora, Terceiro, Controle';
        1: SQL_ORDEM := 'ORDER BY Terceiro, DataHora, Controle';
        else
        begin
          SQL_ORDEM := '';
          Geral.MB_Aviso(
          'Tipo de Ordem n�o implementado para guia 08 (diferen�as)!');
        end;
      end;
      //
(*
      UnDmkDAC_PF.AbreMySQLQuery0(QrMovDif, Dmod.MyDB, [
      'SELECT DATE_FORMAT(vmi.DataHora, "%d/%m/%Y") DATA_TXT,',
      'vmd.InfPecas, vmd.InfPesoKg, vmd.InfAreaM2, ',
      'vmd.infAreaP2, vmd.infValorT, vmd.DifPecas, vmd.DifPesoKg, ',
      'vmd.DifAreaM2, vmd.DifAreaP2, vmd.DifValorT, ',
      'vmi.Pecas, vmi.PesoKg, vmi.AreaM2, vmi.AreaP2, vmi.ValorT, ',
      'vmi.Controle, vmi.GraGruX, vmi.Terceiro, vmi.SerieFch, ',
      'vmi.Ficha, vmi.Observ, vmi.DataHora,',
      'vsf.Nome NO_SerieFch, CONCAT(gg1.Nome,',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,',
      'IF(vmi.SdoVrtPeca > 0, 0,',
      '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2,',
      'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
      '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3),',
      '  ",", ""), ".", ",")) RendKgm2_TXT,',
      'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(',
      'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT,',
      'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,',
      'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,',
      'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
      '  IF(vmi.Pecas > 0, vmi.QtdGerArM2 / vmi.Pecas, 0), 3),',
      '  ",", ""), ".", ",")) m2_CouroTXT,',
      'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro, ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE ',
      'FROM vsmovdif vmd',
      'LEFT JOIN v s m o v i t s vmi ON vmi.Controle=vmd.Controle',
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet',
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch',
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed',
      'LEFT JOIN entidades ent ON ent.Codigo=vmi.Terceiro ',
      'WHERE vmd.DifPecas <> 0',
      SQL_Periodo,
      SQL_Terceiro,
      //
      SQL_ORDEM,
      '']);
*)
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrMovDif, Dmod.MyDB, [
      GeraSQLVSMovItx(ttwB, DefineMorto()),
      GeraSQLVSMovItx(ttwA),
      SQL_ORDEM,
      '']);
      /////
      ///  OS Dmk: 3139
      //Sugiro que esta janela de diferen�a de movimento de couro seja mais
      //simples;ou seja..contendo apenas as informa�ao de
      //data,fornecedo,peso,artigo e total de diferen�a !!
      if FImpSimples then
        Report := frxWET_CURTI_018_08_B
      else
        Report := frxWET_CURTI_018_08_A;
      //
      MyObjects.frxDefineDataSets(Report, [
        DModG.frxDsDono,
        frxDsMovDif
      ]);
      ///////
      Grupo0 := Report.FindObject('GH_00') as TfrxGroupHeader;
      Grupo0.Visible := FAgrupa = 1;
      Futer0 := Report.FindObject('FT_00') as TfrxGroupFooter;
      Futer0.Visible := Grupo0.Visible;
      Me_GH0 := Report.FindObject('Me_GH0') as TfrxMemoView;
      Me_FT0 := Report.FindObject('Me_FT0') as TfrxMemoView;
      case FOrdem of
        0:
        begin
          Grupo0.Condition := 'frxDsMovDif."DATA_TXT"';
          Me_GH0.Memo.Text := '[frxDsMovDif."DATA_TXT"]';
          Me_FT0.Memo.Text := '[frxDsMovDif."DATA_TXT"]: ';
        end;
        1:
        begin
          Grupo0.Condition := 'frxDsMovDif."Terceiro"';
          Me_GH0.Memo.Text := '[frxDsMovDif."Terceiro"] - [frxDsMovDif."NO_FORNECE"]';
          Me_FT0.Memo.Text := '[frxDsMovDif."Terceiro"] - [frxDsMovDif."NO_FORNECE"]: ';
        end;
        else Grupo0.Condition := '***ERRO***';
      end;
      MyObjects.frxMostra(Report, 'Diferen�as de couros');
    end;
    else Geral.MB_Aviso(
    'Tipo de Movimento n�o implementado para guia 08 (diferen�as)!');
  end;
end;

end.
