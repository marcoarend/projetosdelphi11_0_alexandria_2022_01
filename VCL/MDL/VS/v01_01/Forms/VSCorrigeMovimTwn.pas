unit VSCorrigeMovimTwn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables,
  UnProjGroup_Consts, AppListas, UnDMkProcFunc;

type
  TFmVSCorrigeMovimTwn = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrOrfaos: TmySQLQuery;
    DsOrfaos: TDataSource;
    Panel5: TPanel;
    PB1: TProgressBar;
    QrOrfaosCodigo: TIntegerField;
    QrOrfaosControle: TIntegerField;
    QrOrfaosMovimCod: TIntegerField;
    QrOrfaosMovimNiv: TIntegerField;
    QrOrfaosMovimTwn: TIntegerField;
    QrOrfaosEmpresa: TIntegerField;
    QrOrfaosTerceiro: TIntegerField;
    QrOrfaosCliVenda: TIntegerField;
    QrOrfaosMovimID: TIntegerField;
    QrOrfaosLnkIDXtr: TIntegerField;
    QrOrfaosLnkNivXtr1: TIntegerField;
    QrOrfaosLnkNivXtr2: TIntegerField;
    QrOrfaosDataHora: TDateTimeField;
    QrOrfaosPallet: TIntegerField;
    QrOrfaosGraGruX: TIntegerField;
    QrOrfaosPecas: TFloatField;
    QrOrfaosPesoKg: TFloatField;
    QrOrfaosAreaM2: TFloatField;
    QrOrfaosAreaP2: TFloatField;
    QrOrfaosValorT: TFloatField;
    QrOrfaosSrcMovID: TIntegerField;
    QrOrfaosSrcNivel1: TIntegerField;
    QrOrfaosSrcNivel2: TIntegerField;
    QrOrfaosSrcGGX: TIntegerField;
    QrOrfaosSdoVrtPeca: TFloatField;
    QrOrfaosSdoVrtPeso: TFloatField;
    QrOrfaosSdoVrtArM2: TFloatField;
    QrOrfaosObserv: TWideStringField;
    QrOrfaosSerieFch: TIntegerField;
    QrOrfaosFicha: TIntegerField;
    QrOrfaosMisturou: TSmallintField;
    QrOrfaosFornecMO: TIntegerField;
    QrOrfaosCustoMOKg: TFloatField;
    QrOrfaosCustoMOTot: TFloatField;
    QrOrfaosValorMP: TFloatField;
    QrOrfaosDstMovID: TIntegerField;
    QrOrfaosDstNivel1: TIntegerField;
    QrOrfaosDstNivel2: TIntegerField;
    QrOrfaosDstGGX: TIntegerField;
    QrOrfaosQtdGerPeca: TFloatField;
    QrOrfaosQtdGerPeso: TFloatField;
    QrOrfaosQtdGerArM2: TFloatField;
    QrOrfaosQtdGerArP2: TFloatField;
    QrOrfaosQtdAntPeca: TFloatField;
    QrOrfaosQtdAntPeso: TFloatField;
    QrOrfaosQtdAntArM2: TFloatField;
    QrOrfaosQtdAntArP2: TFloatField;
    QrOrfaosAptoUso: TSmallintField;
    QrOrfaosNotaMPAG: TFloatField;
    QrOrfaosMarca: TWideStringField;
    QrOrfaosTpCalcAuto: TIntegerField;
    QrOrfaosZerado: TSmallintField;
    QrOrfaosEmFluxo: TSmallintField;
    QrOrfaosNotFluxo: TIntegerField;
    QrOrfaosFatNotaVNC: TFloatField;
    QrOrfaosFatNotaVRC: TFloatField;
    QrOrfaosPedItsLib: TIntegerField;
    QrOrfaosPedItsFin: TIntegerField;
    QrOrfaosPedItsVda: TIntegerField;
    QrOrfaosLk: TIntegerField;
    QrOrfaosDataCad: TDateField;
    QrOrfaosDataAlt: TDateField;
    QrOrfaosUserCad: TIntegerField;
    QrOrfaosUserAlt: TIntegerField;
    QrOrfaosAlterWeb: TSmallintField;
    QrOrfaosAtivo: TSmallintField;
    QrOrfaosCustoMOM2: TFloatField;
    QrOrfaosReqMovEstq: TIntegerField;
    QrOrfaosStqCenLoc: TIntegerField;
    QrOrfaosItemNFe: TIntegerField;
    QrOrfaosVSMorCab: TIntegerField;
    QrOrfaosVSMulFrnCab: TIntegerField;
    QrSums: TmySQLQuery;
    DsSums: TDataSource;
    GroupBox2: TGroupBox;
    DBGSums: TDBGrid;
    Splitter1: TSplitter;
    GroupBox3: TGroupBox;
    DBGOrfaos: TDBGrid;
    QrSumsNO_MovimID: TWideStringField;
    QrSumsNO_MovimNiv: TWideStringField;
    QrSumsMovimID: TIntegerField;
    QrSumsMovimNiv: TIntegerField;
    QrSumsITENS: TLargeintField;
    QrOrfaosCouNiv2: TIntegerField;
    Panel6: TPanel;
    CkExcecao: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrOrfaosBeforeClose(DataSet: TDataSet);
    procedure QrOrfaosAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    function  ReopenOrfaos(): Integer;
  public
    { Public declarations }
    FProblemas: Integer;
  end;

  var
  FmVSCorrigeMovimTwn: TFmVSCorrigeMovimTwn;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmVSCorrigeMovimTwn.BtOKClick(Sender: TObject);
var
  MovimID, MovimNiv, Controle: Integer;
  //
  procedure ZeraMovimTwn();
  begin
    Dmod.MyDB.Execute('UPDATE ' + CO_UPD_TAB_VMI + ' SET MovimTwn=0 ' +
    'WHERE Controle=' + Geral.FF0(Controle));
  end;
begin
  PB1.Max := QrOrfaos.RecordCount;
  PB1.Position := 0;
  QrOrfaos.First;
  QrOrfaos.DisableControls;
  try
    while not QrOrfaos.Eof do
    begin
      MyObjects.UpdPBOnly(PB1);
      //
      MovimID  := QrOrfaosMovimID.Value;
      MovimNiv := QrOrfaosMovimNiv.Value;
      Controle := QrOrfaosControle.Value;
      //
      if ((MovimID =  6) and (MovimNiv = 13))
      or ((MovimID =  8) and (MovimNiv =  6))
      or ((MovimID = 11) and (MovimNiv =  8))
      or ((MovimID = 19) and (MovimNiv = 21)) then
        ZeraMovimTwn()
      else
      if ((MovimID = 11) and (MovimNiv =  9)) then
      begin
        if (QrOrfaosCouNiv2.Value <> 1)
        or CkExcecao.Checked then
          ZeraMovimTwn()
      end else // ????
       ;
       //
      QrOrfaos.Next;
    end;
    QrOrfaos.DisableControls;
    DBGOrfaos.Refresh;
    DBGOrfaos.Invalidate;
    ReopenOrfaos();
    PB1.Position := 0;
  finally
    QrOrfaos.DisableControls;
  end;
end;

procedure TFmVSCorrigeMovimTwn.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSCorrigeMovimTwn.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSCorrigeMovimTwn.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FProblemas := ReopenOrfaos();
end;

procedure TFmVSCorrigeMovimTwn.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSCorrigeMovimTwn.QrOrfaosAfterOpen(DataSet: TDataSet);
begin
  BtOK.Enabled := QrOrfaos.RecordCount > 0;
end;

procedure TFmVSCorrigeMovimTwn.QrOrfaosBeforeClose(DataSet: TDataSet);
begin
  BtOK.Enabled := False;
end;

function TFmVSCorrigeMovimTwn.ReopenOrfaos(): Integer;
var
  ATT_MovimID, ATT_MovimNiv, SQL_IMEI: String;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
  //
  ATT_MovimNiv := dmkPF.ArrayToTexto('MovimNiv', 'NO_MovimNiv', pvPos, True,
  sEstqMovimNiv);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSums, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _TESTE_; ',
  'CREATE TABLE _TESTE_ ',
  'SELECT MovimID, MovimTwn, COUNT(Controle) ITENS ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI,
  'WHERE MovimTwn <> 0 ',
  'GROUP BY MovimTwn ',
  'ORDER BY ITENS, MovimID, MovimTwn; ',
  ' ',
  'DELETE FROM _TESTE_ ',
  'WHERE ITENS <> 1; ',
  '',
  'SELECT ',
  ATT_MovimID,
  ATT_MovimNiv,
  'MovimID, MovimNiv, COUNT(Controle) ITENS ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI,
  'WHERE MovimTwn IN ( ',
  '  SELECT MovimTwn ',
  '  FROM _TESTE_ ',
  '  WHERE ITENS=1 ',
  ') ',
  'GROUP BY MovimID, MovimNiv',
  'ORDER BY MovimID, MovimNiv',
  '; ',
  'DROP TABLE IF EXISTS _TESTE_; ',
  '']);
  //
  //Geral.MB_SQL(Self, QrSums);

  UnDmkDAC_PF.AbreMySQLQuery0(QrOrfaos, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS _TESTE_; ',
  'CREATE TABLE _TESTE_ ',
  'SELECT MovimID, MovimTwn, COUNT(Controle) ITENS ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI,
  'WHERE MovimTwn <> 0 ',
  'GROUP BY MovimTwn ',
  'ORDER BY ITENS, MovimID, MovimTwn; ',
  ' ',
  'DELETE FROM _TESTE_ ',
  'WHERE ITENS <> 1; ',
  '',
  'SELECT cou.CouNiv2, vmi.*',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi',
  'LEFT JOIN ' + TMeuDB + '.gragruxcou cou ON cou.GraGruX=vmi.GraGruX',
  'WHERE MovimTwn IN ( ',
  '  SELECT MovimTwn ',
  '  FROM _TESTE_ ',
  '  WHERE ITENS=1 ',
  ') ',
  'ORDER BY Controle ',
  '; ',
  'DROP TABLE IF EXISTS _TESTE_; ',
  '']);
  //
  //Geral.MB_SQL(Self, QrOrfaos);
  Result := QrOrfaos.RecordCount;
end;

end.
