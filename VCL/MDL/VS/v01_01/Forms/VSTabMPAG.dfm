object FmVSTabMPAG: TFmVSTabMPAG
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-263 :: Tabela Rendimento In Natura '#224' Curtido'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 548
        Height = 32
        Caption = 'Tabela de Rendimento de In Natura '#224' Curtido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 548
        Height = 32
        Caption = 'Tabela de Rendimento de In Natura '#224' Curtido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 548
        Height = 32
        Caption = 'Tabela de Rendimento de In Natura '#224' Curtido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 504
    Width = 812
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 808
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitLeft = 6
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Gera'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 136
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImprimeClick
      end
      object BitBtn1: TBitBtn
        Tag = 5
        Left = 260
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Imprime'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BitBtn1Click
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 456
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 93
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 8
        Top = 48
        Width = 71
        Height = 13
        Caption = 'Peso kg inicial:'
      end
      object LaFatorMP: TLabel
        Left = 380
        Top = 48
        Width = 46
        Height = 13
        Caption = 'Fator MP:'
        Enabled = False
      end
      object LaFatorAR: TLabel
        Left = 488
        Top = 48
        Width = 45
        Height = 13
        Caption = 'Fator AR:'
        Enabled = False
      end
      object Label8: TLabel
        Left = 260
        Top = 48
        Width = 44
        Height = 13
        Caption = '% Comp.:'
      end
      object Label1: TLabel
        Left = 92
        Top = 48
        Width = 64
        Height = 13
        Caption = 'Peso kg final:'
      end
      object Label3: TLabel
        Left = 176
        Top = 48
        Width = 58
        Height = 13
        Caption = 'kg intervalo:'
      end
      object Label4: TLabel
        Left = 320
        Top = 48
        Width = 36
        Height = 13
        Caption = '% Cura:'
      end
      object SbFatorMP: TSpeedButton
        Left = 460
        Top = 64
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = SbFatorMPClick
      end
      object SbFatorAR: TSpeedButton
        Left = 568
        Top = 64
        Width = 23
        Height = 22
        Caption = '...'
        OnClick = SbFatorARClick
      end
      object EdPesoKgIni: TdmkEdit
        Left = 8
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '15,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 15.000000000000000000
        ValWarn = False
        OnRedefinido = EdPesoKgIniRedefinido
      end
      object EdFatorMP: TdmkEdit
        Left = 380
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 8
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1,25000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1.250000000000000000
        ValWarn = False
        OnRedefinido = EdFatorMPRedefinido
      end
      object EdFatorAR: TdmkEdit
        Left = 488
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 8
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1,05000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1.050000000000000000
        ValWarn = False
        OnRedefinido = EdFatorARRedefinido
      end
      object EdPerCompe: TdmkEdit
        Left = 260
        Top = 64
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-9'
        ValMax = '9'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '3,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 3.000000000000000000
        ValWarn = False
        OnRedefinido = EdPerCompeRedefinido
      end
      object EdPesoKgFim: TdmkEdit
        Left = 92
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '85,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 85.000000000000000000
        ValWarn = False
        OnRedefinido = EdPesoKgFimRedefinido
      end
      object EdPesoKgStp: TdmkEdit
        Left = 176
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0,01'
        ValMax = '0,1'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,100'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.100000000000000000
        ValWarn = False
        OnRedefinido = EdPesoKgStpRedefinido
      end
      object RGMP: TRadioGroup
        Left = 8
        Top = 4
        Width = 193
        Height = 41
        Caption = ' Mat'#233'ria-prima: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'N/D'
          'Verde'
          'Salgado')
        TabOrder = 6
        OnClick = RGMPClick
      end
      object RGAR: TRadioGroup
        Left = 204
        Top = 4
        Width = 525
        Height = 41
        Caption = ' Artigo: '
        Columns = 5
        ItemIndex = 0
        Items.Strings = (
          'N/D'
          'Dividido tripa'
          'Laminado'
          'Integral'
          'Chrom Free')
        TabOrder = 7
        OnClick = RGARClick
      end
      object EdPerCura: TdmkEdit
        Left = 320
        Top = 64
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '10'
        ValMax = '100'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '100,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 100.000000000000000000
        ValWarn = False
        OnRedefinido = EdPerCuraRedefinido
      end
    end
    object SGTabela: TStringGrid
      Left = 0
      Top = 93
      Width = 812
      Height = 363
      Align = alClient
      ColCount = 4
      DefaultRowHeight = 21
      RowCount = 2
      TabOrder = 1
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrTab: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT *'
      'FROM _vs_tab_mpag_'
      'ORDER BY Linha')
    Left = 48
    Top = 240
    object QrTabLinha: TIntegerField
      FieldName = 'Linha'
      Required = True
    end
    object QrTabPesoKg_A: TWideStringField
      FieldName = 'PesoKg_A'
    end
    object QrTabAreaM2_A: TWideStringField
      FieldName = 'AreaM2_A'
    end
    object QrTabAreaP2_A: TWideStringField
      FieldName = 'AreaP2_A'
    end
    object QrTabKgM2_A: TWideStringField
      FieldName = 'KgM2_A'
    end
    object QrTabPesoKg_B: TWideStringField
      FieldName = 'PesoKg_B'
    end
    object QrTabAreaM2_B: TWideStringField
      FieldName = 'AreaM2_B'
    end
    object QrTabAreaP2_B: TWideStringField
      FieldName = 'AreaP2_B'
    end
    object QrTabKgM2_B: TWideStringField
      FieldName = 'KgM2_B'
    end
    object QrTabPesoKg_C: TWideStringField
      FieldName = 'PesoKg_C'
    end
    object QrTabAreaM2_C: TWideStringField
      FieldName = 'AreaM2_C'
    end
    object QrTabAreaP2_C: TWideStringField
      FieldName = 'AreaP2_C'
    end
    object QrTabKgM2_C: TWideStringField
      FieldName = 'KgM2_C'
    end
    object QrTabPesoKg_D: TWideStringField
      FieldName = 'PesoKg_D'
    end
    object QrTabAreaM2_D: TWideStringField
      FieldName = 'AreaM2_D'
    end
    object QrTabAreaP2_D: TWideStringField
      FieldName = 'AreaP2_D'
    end
    object QrTabKgM2_D: TWideStringField
      FieldName = 'KgM2_D'
    end
    object QrTabPesoKg_E: TWideStringField
      FieldName = 'PesoKg_E'
    end
    object QrTabAreaM2_E: TWideStringField
      FieldName = 'AreaM2_E'
    end
    object QrTabAreaP2_E: TWideStringField
      FieldName = 'AreaP2_E'
    end
    object QrTabKgM2_E: TWideStringField
      FieldName = 'KgM2_E'
    end
    object QrTabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object frxWET_CURTI_263: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42123.670599108790000000
    ReportOptions.LastChange = 42123.670599108790000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxWET_CURTI_263GetValue
    Left = 48
    Top = 192
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsTab
        DataSetName = 'frxDsTab'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 94.488240240000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Tabela de Rendimento de In Natura a Curtido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeTitNome: TfrxMemoView
          AllowVectorExport = True
          Left = 47.244094490000000000
          Top = 79.370130000000000000
          Width = 32.125984250000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'm'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 79.370130000000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            
              'Mat'#233'ria-prima: [VARF_MP]    Artigo: [VARF_AR]     Compensa'#231#227'o: [' +
              'VARF_COMPENSACAO]%     [VARF_CURA]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end
            item
            end>
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 119.055118110000000000
          Top = 79.370130000000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg/m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Top = 79.370130000000000000
          Width = 39.685039370000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 217.322944490000000000
          Top = 79.370130000000000000
          Width = 32.125984250000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'm'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Top = 79.370130000000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 289.133968110000000000
          Top = 79.370130000000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg/m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 79.370130000000000000
          Width = 39.685039370000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 387.401794490000000000
          Top = 79.370130000000000000
          Width = 32.125984250000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'm'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 79.370130000000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 459.212818110000000000
          Top = 79.370130000000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg/m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527830000000000000
          Top = 79.370130000000000000
          Width = 39.685039370000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 557.480644490000000000
          Top = 79.370130000000000000
          Width = 32.125984250000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'm'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 79.370130000000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 629.291668110000000000
          Top = 79.370130000000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'kg/m'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Top = 79.370130000000000000
          Width = 39.685039370000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ft'#178)
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MD001: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 173.858380000000000000
        Width = 680.315400000000000000
        DataSet = frxDsTab
        DataSetName = 'frxDsTab'
        RowCount = 0
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 47.244094490000000000
          Width = 32.125984250000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2_A'
          DataSet = frxDsTab
          DataSetName = 'frxDsTab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTab."AreaM2_A"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg_A'
          DataSet = frxDsTab
          DataSetName = 'frxDsTab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTab."PesoKg_A"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 119.055118110000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DataField = 'KgM2_A'
          DataSet = frxDsTab
          DataSetName = 'frxDsTab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTab."KgM2_A"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Left = 79.370130000000000000
          Width = 39.685039370000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2_A'
          DataSet = frxDsTab
          DataSetName = 'frxDsTab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTab."AreaP2_A"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 217.322944490000000000
          Width = 32.125984250000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2_B'
          DataSet = frxDsTab
          DataSetName = 'frxDsTab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTab."AreaM2_B"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 170.078850000000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg_B'
          DataSet = frxDsTab
          DataSetName = 'frxDsTab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTab."PesoKg_B"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 289.133968110000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DataField = 'KgM2_B'
          DataSet = frxDsTab
          DataSetName = 'frxDsTab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTab."KgM2_B"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 39.685039370000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2_B'
          DataSet = frxDsTab
          DataSetName = 'frxDsTab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTab."AreaP2_B"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 387.401794490000000000
          Width = 32.125984250000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2_C'
          DataSet = frxDsTab
          DataSetName = 'frxDsTab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTab."AreaM2_C"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg_C'
          DataSet = frxDsTab
          DataSetName = 'frxDsTab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTab."PesoKg_C"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Left = 459.212818110000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DataField = 'KgM2_C'
          DataSet = frxDsTab
          DataSetName = 'frxDsTab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTab."KgM2_C"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Left = 419.527830000000000000
          Width = 39.685039370000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2_C'
          DataSet = frxDsTab
          DataSetName = 'frxDsTab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTab."AreaP2_C"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 557.480644490000000000
          Width = 32.125984250000000000
          Height = 15.118110240000000000
          DataField = 'AreaM2_D'
          DataSet = frxDsTab
          DataSetName = 'frxDsTab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTab."AreaM2_D"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 510.236550000000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DataField = 'PesoKg_D'
          DataSet = frxDsTab
          DataSetName = 'frxDsTab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTab."PesoKg_D"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 629.291668110000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DataField = 'KgM2_D'
          DataSet = frxDsTab
          DataSetName = 'frxDsTab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTab."KgM2_D"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 589.606680000000000000
          Width = 39.685039370000000000
          Height = 15.118110240000000000
          DataField = 'AreaP2_D'
          DataSet = frxDsTab
          DataSetName = 'frxDsTab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTab."AreaP2_D"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 249.448980000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsTab: TfrxDBDataset
    UserName = 'frxDsTab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Linha=Linha'
      'PesoKg_A=PesoKg_A'
      'AreaM2_A=AreaM2_A'
      'AreaP2_A=AreaP2_A'
      'KgM2_A=KgM2_A'
      'PesoKg_B=PesoKg_B'
      'AreaM2_B=AreaM2_B'
      'AreaP2_B=AreaP2_B'
      'KgM2_B=KgM2_B'
      'PesoKg_C=PesoKg_C'
      'AreaM2_C=AreaM2_C'
      'AreaP2_C=AreaP2_C'
      'KgM2_C=KgM2_C'
      'PesoKg_D=PesoKg_D'
      'AreaM2_D=AreaM2_D'
      'AreaP2_D=AreaP2_D'
      'KgM2_D=KgM2_D'
      'PesoKg_E=PesoKg_E'
      'AreaM2_E=AreaM2_E'
      'AreaP2_E=AreaP2_E'
      'KgM2_E=KgM2_E'
      'Ativo=Ativo')
    DataSet = QrTab
    BCDToCurrency = False
    DataSetOptions = []
    Left = 48
    Top = 288
  end
end
