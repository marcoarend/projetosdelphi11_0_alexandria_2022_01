object FmVSInvNFe: TFmVSInvNFe
  Left = 368
  Top = 194
  Caption = 'WET-CURTI-169 :: NFe VS Outros ou N'#227'o Autorizada'
  ClientHeight = 493
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 397
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitHeight = 312
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 185
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label10: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label11: TLabel
        Left = 696
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 16
        Top = 56
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 16
        Top = 100
        Width = 68
        Height = 13
        Caption = 'ID movimento:'
      end
      object Label14: TLabel
        Left = 16
        Top = 140
        Width = 26
        Height = 13
        Caption = 'Data:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label15: TLabel
        Left = 128
        Top = 140
        Width = 116
        Height = 13
        Caption = 'S'#233'rie e N'#176' NFe remessa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label16: TLabel
        Left = 256
        Top = 140
        Width = 59
        Height = 13
        Caption = 'Status NFe: '
        Color = clBtnFace
        ParentColor = False
      end
      object Label17: TLabel
        Left = 696
        Top = 100
        Width = 73
        Height = 13
        Caption = 'IME-C atrelado:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label2: TLabel
        Left = 644
        Top = 16
        Width = 42
        Height = 13
        Caption = 'Terceiro:'
        FocusControl = DBEdit10
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsVSInvNFe
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 696
        Top = 32
        Width = 72
        Height = 21
        DataField = 'MovimCod'
        DataSource = DsVSInvNFe
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 16
        Top = 116
        Width = 56
        Height = 21
        DataField = 'MovimID'
        DataSource = DsVSInvNFe
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 696
        Top = 116
        Width = 72
        Height = 21
        DataField = 'MovCodOri'
        DataSource = DsVSInvNFe
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 16
        Top = 72
        Width = 753
        Height = 21
        DataField = 'Nome'
        DataSource = DsVSInvNFe
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsVSInvNFe
        TabOrder = 5
      end
      object DBEdit6: TDBEdit
        Left = 16
        Top = 156
        Width = 109
        Height = 21
        DataField = 'Data'
        DataSource = DsVSInvNFe
        TabOrder = 6
      end
      object DBEdit7: TDBEdit
        Left = 128
        Top = 156
        Width = 33
        Height = 21
        DataField = 'ide_serie'
        DataSource = DsVSInvNFe
        TabOrder = 7
      end
      object DBEdit8: TDBEdit
        Left = 164
        Top = 156
        Width = 89
        Height = 21
        DataField = 'ide_nNF'
        DataSource = DsVSInvNFe
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 256
        Top = 156
        Width = 37
        Height = 21
        DataField = 'NFeStatus'
        DataSource = DsVSInvNFe
        TabOrder = 9
      end
      object DBEdit10: TDBEdit
        Left = 644
        Top = 32
        Width = 49
        Height = 21
        DataField = 'Terceiro'
        DataSource = DsVSInvNFe
        TabOrder = 10
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 333
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 248
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExcluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object GBStatusShw: TGroupBox
      Left = 0
      Top = 185
      Width = 784
      Height = 110
      Align = alTop
      TabOrder = 2
      ExplicitTop = 0
      ExplicitWidth = 812
      ExplicitHeight = 123
      object Label20: TLabel
        Left = 20
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Status:'
      end
      object Label21: TLabel
        Left = 528
        Top = 16
        Width = 169
        Height = 13
        Caption = 'Ano/m'#234's/dia/p'#225'g/etc. (indexa'#231#227'o):'
      end
      object Label22: TLabel
        Left = 20
        Top = 56
        Width = 109
        Height = 13
        Caption = 'Hist'#243'rico (observa'#231#227'o):'
      end
      object DBEdit11: TDBEdit
        Left = 20
        Top = 32
        Width = 65
        Height = 21
        DataField = 'VSVmcWrn'
        DataSource = DsVSInvNFe
        TabOrder = 0
      end
      object DBEdit12: TDBEdit
        Left = 528
        Top = 32
        Width = 232
        Height = 21
        DataField = 'VSVmcSeq'
        DataSource = DsVSInvNFe
        TabOrder = 1
      end
      object DBEdit13: TDBEdit
        Left = 20
        Top = 72
        Width = 741
        Height = 21
        DataField = 'VSVmcObs'
        DataSource = DsVSInvNFe
        TabOrder = 2
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 397
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitHeight = 312
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 193
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 16
        Top = 56
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 696
        Top = 16
        Width = 32
        Height = 13
        Caption = 'IME-C:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 16
        Top = 100
        Width = 68
        Height = 13
        Caption = 'ID movimento:'
      end
      object Label53: TLabel
        Left = 16
        Top = 140
        Width = 26
        Height = 13
        Caption = 'Data:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label5: TLabel
        Left = 696
        Top = 100
        Width = 73
        Height = 13
        Caption = 'IME-C atrelado:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 76
        Top = 16
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label57: TLabel
        Left = 128
        Top = 140
        Width = 116
        Height = 13
        Caption = 'S'#233'rie e N'#176' NFe remessa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label6: TLabel
        Left = 256
        Top = 140
        Width = 78
        Height = 13
        Caption = 'Status NFe: [F4}'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 72
        Width = 753
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdMovimCod: TdmkEdit
        Left = 696
        Top = 32
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimCod'
        UpdCampo = 'MovimCod'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMovimID: TdmkEditCB
        Left = 16
        Top = 116
        Width = 36
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovimID'
        UpdCampo = 'MovimID'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBMovimID
        IgnoraDBLookupComboBox = False
      end
      object CBMovimID: TdmkDBLookupComboBox
        Left = 52
        Top = 116
        Width = 641
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVSMovimID
        TabOrder = 6
        dmkEditCB = EdMovimID
        QryCampo = 'MovimID'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPData: TdmkEditDateTimePicker
        Left = 16
        Top = 156
        Width = 108
        Height = 21
        Date = 0.639644131944805800
        Time = 0.639644131944805800
        TabOrder = 8
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'Data'
        UpdCampo = 'Data'
        UpdType = utYes
      end
      object EdMovCodOri: TdmkEdit
        Left = 696
        Top = 116
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'MovCodOri'
        UpdCampo = 'MovCodOri'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 561
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryName = 'QrVSGerArt'
        QryCampo = 'Empresa'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object Edide_serie: TdmkEdit
        Left = 128
        Top = 156
        Width = 29
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '-1'
        QryCampo = 'ide_serie'
        UpdCampo = 'ide_serie'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = -1
        ValWarn = False
      end
      object Edide_nNF: TdmkEdit
        Left = 160
        Top = 156
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ide_nNF'
        UpdCampo = 'ide_nNF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNFeStatus: TdmkEditCB
        Left = 256
        Top = 156
        Width = 36
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'NFeStatus'
        UpdCampo = 'NFeStatus'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBNFeStatus
        IgnoraDBLookupComboBox = False
      end
      object CBNFeStatus: TdmkDBLookupComboBox
        Left = 292
        Top = 156
        Width = 477
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsNFeMsg
        TabOrder = 12
        dmkEditCB = EdNFeStatus
        QryCampo = 'NFeStatus'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 334
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 249
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBStatusAlt: TGroupBox
      Left = 0
      Top = 193
      Width = 784
      Height = 110
      Align = alTop
      TabOrder = 2
      ExplicitTop = 0
      ExplicitWidth = 812
      ExplicitHeight = 123
      object LaPrompt: TLabel
        Left = 20
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Status:'
      end
      object Label18: TLabel
        Left = 528
        Top = 16
        Width = 169
        Height = 13
        Caption = 'Ano/m'#234's/dia/p'#225'g/etc. (indexa'#231#227'o):'
      end
      object Label19: TLabel
        Left = 20
        Top = 56
        Width = 109
        Height = 13
        Caption = 'Hist'#243'rico (observa'#231#227'o):'
      end
      object EdVSVmcWrn: TdmkEditCB
        Left = 20
        Top = 32
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'VSVmcWrn'
        UpdCampo = 'VSVmcWrn'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBVSVmcWrn
        IgnoraDBLookupComboBox = False
      end
      object CBVSVmcWrn: TdmkDBLookupComboBox
        Left = 88
        Top = 32
        Width = 437
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsVSVmcWrn
        TabOrder = 1
        dmkEditCB = EdVSVmcWrn
        QryCampo = 'VSVmcWrn'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdVSVmcSeq: TdmkEdit
        Left = 528
        Top = 32
        Width = 233
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'VSVmcSeq'
        UpdCampo = 'VSVmcSeq'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdVSVmcObs: TdmkEdit
        Left = 20
        Top = 72
        Width = 741
        Height = 21
        MaxLength = 60
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'VSVmcObs'
        UpdCampo = 'VSVmcObs'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 415
        Height = 32
        Caption = 'NFe VS Outros ou N'#227'o Autorizada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 415
        Height = 32
        Caption = 'NFe VS Outros ou N'#227'o Autorizada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 415
        Height = 32
        Caption = 'NFe VS Outros ou N'#227'o Autorizada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrVSInvNFe: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrVSInvNFeBeforeOpen
    AfterOpen = QrVSInvNFeAfterOpen
    SQL.Strings = (
      'SELECT *'
      'FROM vsinvnfe')
    Left = 64
    Top = 64
    object QrVSInvNFeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSInvNFeMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSInvNFeMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSInvNFeNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrVSInvNFeEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSInvNFeide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrVSInvNFeide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrVSInvNFeNFeStatus: TIntegerField
      FieldName = 'NFeStatus'
    end
    object QrVSInvNFeMovCodOri: TIntegerField
      FieldName = 'MovCodOri'
    end
    object QrVSInvNFeLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSInvNFeDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSInvNFeDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSInvNFeUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSInvNFeUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSInvNFeAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSInvNFeAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSInvNFeTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSInvNFeData: TDateTimeField
      FieldName = 'Data'
    end
    object QrVSInvNFeVSVmcWrn: TSmallintField
      FieldName = 'VSVmcWrn'
    end
    object QrVSInvNFeVSVmcObs: TWideStringField
      FieldName = 'VSVmcObs'
      Size = 60
    end
    object QrVSInvNFeVSVmcSeq: TWideStringField
      FieldName = 'VSVmcSeq'
      Size = 60
    end
  end
  object DsVSInvNFe: TDataSource
    DataSet = QrVSInvNFe
    Left = 64
    Top = 104
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrVSMovimID: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM vsmovimid'
      'ORDER BY Nome')
    Left = 408
    Top = 68
    object QrVSMovimIDCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovimIDNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSMovimID: TDataSource
    DataSet = QrVSMovimID
    Left = 408
    Top = 120
  end
  object QrNFeMsg: TmySQLQuery
    Database = Dmod.MyDB
    Left = 528
    Top = 68
  end
  object DsNFeMsg: TDataSource
    DataSet = QrNFeMsg
    Left = 532
    Top = 116
  end
  object QrVSVmcWrn: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM vsvmcwrn'
      'ORDER BY Nome')
    Left = 184
    Top = 2
    object QrVSVmcWrnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSVmcWrnNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSVmcWrn: TDataSource
    DataSet = QrVSVmcWrn
    Left = 184
    Top = 50
  end
end
