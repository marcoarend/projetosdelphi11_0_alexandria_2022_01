unit VSMovImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables, frxClass, frxDBSet, dmkEditDateTimePicker, Variants,
  // Chart
  frxChart, VCLTee.Chart, VCLTee.Series,
  // Fim Chart
  dmkDBGridDAC, Vcl.Menus, dmkDBGridZTO, dmkCheckBox, dmkCheckGroup,
  UnProjGroup_Consts, ModuleNFe_0000, dmkCompoStore, dmkPermissoes, UnGrl_Consts,
  DmkDBGrid, dmkRadioGroup, UnAppEnums;

type
  TFmVSMovImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Label7: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    PCRelatorio: TPageControl;
    TabSheet1: TTabSheet;
    Panel6: TPanel;
    TabSheet2: TTabSheet;
    PB1: TProgressBar;
    TabSheet3: TTabSheet;
    Panel8: TPanel;
    Qr02Fornecedor: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    Ds02Fornecedor: TDataSource;
    Qr02VSPallet: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    Ds02VSPallet: TDataSource;
    Qr02GraGruX: TmySQLQuery;
    Ds02GraGruX: TDataSource;
    Panel9: TPanel;
    Label2: TLabel;
    Ed02GraGruX: TdmkEditCB;
    CB02GraGruX: TdmkDBLookupComboBox;
    Label3: TLabel;
    Ed02Pallet: TdmkEditCB;
    CB02Pallet: TdmkDBLookupComboBox;
    Label5: TLabel;
    Ed02Terceiro: TdmkEditCB;
    CB02Terceiro: TdmkDBLookupComboBox;
    GroupBox3: TGroupBox;
    TP02DataIni: TdmkEditDateTimePicker;
    Ck02DataIni: TCheckBox;
    Ck02DataFim: TCheckBox;
    TP02DataFim: TdmkEditDateTimePicker;
    Panel10: TPanel;
    QrVSMovIts: TmySQLQuery;
    DsVSMovIts: TDataSource;
    DG02VSMovIts: TdmkDBGridZTO;
    QrVSMovItsNO_MovimID: TWideStringField;
    Label6: TLabel;
    Ed02Controle: TdmkEdit;
    TabSheet4: TTabSheet;
    Panel11: TPanel;
    QrEstqR3: TmySQLQuery;
    DBG03Estq: TdmkDBGridZTO;
    DsEstqR3: TDataSource;
    QrGGXPalTer: TmySQLQuery;
    frxDsGGXPalTer: TfrxDBDataset;
    QrEstqR3GraGruX: TIntegerField;
    QrEstqR3Pecas: TFloatField;
    QrEstqR3PesoKg: TFloatField;
    QrEstqR3AreaM2: TFloatField;
    QrEstqR3AreaP2: TFloatField;
    QrEstqR3GraGru1: TIntegerField;
    QrEstqR3NO_PRD_TAM_COR: TWideStringField;
    QrEstqR3Terceiro: TIntegerField;
    QrEstqR3NO_FORNECE: TWideStringField;
    QrEstqR3Pallet: TIntegerField;
    QrEstqR3NO_PALLET: TWideStringField;
    QrEstqR3ValorT: TFloatField;
    QrEstqR3Ativo: TSmallintField;
    QrEstqR3Empresa: TIntegerField;
    PM04Imprime: TPopupMenu;
    Fichas1: TMenuItem;
    Lista1: TMenuItem;
    QrEstqR3OrdGGX: TIntegerField;
    Bt02AtzSdoVrt: TBitBtn;
    QrEstqR3NO_STATUS: TWideStringField;
    TabSheet5: TTabSheet;
    QrEstqR3Controle: TIntegerField;
    QrEstqR4: TmySQLQuery;
    DsEstqR4: TDataSource;
    QrEstqR4Empresa: TIntegerField;
    QrEstqR4GraGruX: TIntegerField;
    QrEstqR4Pecas: TFloatField;
    QrEstqR4PesoKg: TFloatField;
    QrEstqR4AreaM2: TFloatField;
    QrEstqR4AreaP2: TFloatField;
    QrEstqR4GraGru1: TIntegerField;
    QrEstqR4NO_PRD_TAM_COR: TWideStringField;
    QrEstqR4Terceiro: TIntegerField;
    QrEstqR4NO_FORNECE: TWideStringField;
    QrEstqR4Pallet: TIntegerField;
    QrEstqR4NO_PALLET: TWideStringField;
    QrEstqR4ValorT: TFloatField;
    QrEstqR4Ativo: TSmallintField;
    QrEstqR4OrdGGX: TIntegerField;
    QrEstqR4NO_STATUS: TWideStringField;
    Panel14: TPanel;
    BtOK: TBitBtn;
    PnFichas: TPanel;
    BtImprime: TBitBtn;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    frxWET_CURTI_018_03_A: TfrxReport;
    PackLists1: TMenuItem;
    N1: TMenuItem;
    Vertical1: TMenuItem;
    Horizontal1: TMenuItem;
    TabSheet6: TTabSheet;
    Panel12: TPanel;
    Qr04GraGruX: TmySQLQuery;
    Ds04GraGruX: TDataSource;
    Qr04GraGruXGraGru1: TIntegerField;
    Qr04GraGruXControle: TIntegerField;
    Qr04GraGruXNO_PRD_TAM_COR: TWideStringField;
    Qr04GraGruXSIGLAUNIDMED: TWideStringField;
    Qr04GraGruXCODUSUUNIDMED: TIntegerField;
    Qr04GraGruXNOMEUNIDMED: TWideStringField;
    Qr02GraGruXGraGru1: TIntegerField;
    Qr02GraGruXControle: TIntegerField;
    Qr02GraGruXNO_PRD_TAM_COR: TWideStringField;
    Qr02GraGruXSIGLAUNIDMED: TWideStringField;
    Qr02GraGruXCODUSUUNIDMED: TIntegerField;
    Qr02GraGruXNOMEUNIDMED: TWideStringField;
    frxWET_CURTI_018_03_B2: TfrxReport;
    QrEstqR4SdoVrtPeca: TFloatField;
    QrEstqR4SdoVrtPeso: TFloatField;
    QrEstqR4SdoVrtArM2: TFloatField;
    QrEstqR4Status: TIntegerField;
    QrEstqR4NO_EMPRESA: TWideStringField;
    QrEstqR4DataHora: TDateTimeField;
    QrEstqR4OrdGGY: TIntegerField;
    QrEstqR4GraGruY: TIntegerField;
    QrEstqR4NO_GGY: TWideStringField;
    QrEstqR4SdoVrtArP2: TFloatField;
    QrGGXPalTerEmpresa: TIntegerField;
    QrGGXPalTerGraGruX: TIntegerField;
    QrGGXPalTerPecas: TFloatField;
    QrGGXPalTerPesoKg: TFloatField;
    QrGGXPalTerAreaM2: TFloatField;
    QrGGXPalTerAreaP2: TFloatField;
    QrGGXPalTerGraGru1: TIntegerField;
    QrGGXPalTerNO_PRD_TAM_COR: TWideStringField;
    QrGGXPalTerTerceiro: TIntegerField;
    QrGGXPalTerNO_FORNECE: TWideStringField;
    QrGGXPalTerPallet: TIntegerField;
    QrGGXPalTerNO_PALLET: TWideStringField;
    QrGGXPalTerValorT: TFloatField;
    QrGGXPalTerAtivo: TSmallintField;
    QrGGXPalTerOrdGGX: TIntegerField;
    QrGGXPalTerNO_STATUS: TWideStringField;
    QrGGXPalTerSdoVrtPeca: TFloatField;
    QrGGXPalTerSdoVrtPeso: TFloatField;
    QrGGXPalTerSdoVrtArM2: TFloatField;
    QrGGXPalTerStatus: TIntegerField;
    QrGGXPalTerNO_EMPRESA: TWideStringField;
    QrGGXPalTerDataHora: TDateTimeField;
    QrGGXPalTerOrdGGY: TIntegerField;
    QrGGXPalTerGraGruY: TIntegerField;
    QrGGXPalTerNO_GGY: TWideStringField;
    QrGGXPalTerSdoVrtArP2: TFloatField;
    TabSheet7: TTabSheet;
    Qr06Fornecedor: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    Ds06Fornecedor: TDataSource;
    Qr06VSPalletSrc: TmySQLQuery;
    Ds06VSPalletSrc: TDataSource;
    Qr06GraGruX: TmySQLQuery;
    IntegerField5: TIntegerField;
    IntegerField11: TIntegerField;
    StringField5: TWideStringField;
    StringField11: TWideStringField;
    IntegerField12: TIntegerField;
    StringField12: TWideStringField;
    Ds06GraGruX: TDataSource;
    Qr06VSSerFch: TmySQLQuery;
    Qr06VSSerFchCodigo: TIntegerField;
    Qr06VSSerFchNome: TWideStringField;
    Ds06VSSerFch: TDataSource;
    Panel16: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Ed06GraGruXSrc: TdmkEditCB;
    CB06GraGruXSrc: TdmkDBLookupComboBox;
    Ed06VSPalletSrc: TdmkEditCB;
    CB06VSPalletSrc: TdmkDBLookupComboBox;
    Ed06Terceiro: TdmkEditCB;
    CB06Terceiro: TdmkDBLookupComboBox;
    GroupBox5: TGroupBox;
    TP06DataIni: TdmkEditDateTimePicker;
    Ck06DataIni: TCheckBox;
    Ck06DataFim: TCheckBox;
    TP06DataFim: TdmkEditDateTimePicker;
    Ed06VMI_Sorc: TdmkEdit;
    Ed06Martelo: TdmkEditCB;
    CB06Martelo: TdmkDBLookupComboBox;
    Ed06VMI_Dest: TdmkEdit;
    Ed06SerieFch: TdmkEditCB;
    CB06SerieFch: TdmkDBLookupComboBox;
    Ed06Ficha: TdmkEdit;
    Ed06Marca: TdmkEdit;
    Ed06VSCacCod: TdmkEdit;
    Label30: TLabel;
    Ed06Pequeno: TdmkEdit;
    Label31: TLabel;
    Ed06Grande: TdmkEdit;
    frxWET_CURTI_018_06_A: TfrxReport;
    Qr06VSCacGBY: TmySQLQuery;
    frxDs06VSCacGBY: TfrxDBDataset;
    Qr06SumGBY: TmySQLQuery;
    Qr06VSCacGBYVMI_Dest: TIntegerField;
    Qr06VSCacGBYGraGruX: TIntegerField;
    Qr06VSCacGBYGraGru1: TIntegerField;
    Qr06VSCacGBYNO_PRD_TAM_COR: TWideStringField;
    Qr06VSCacGBYPecas: TFloatField;
    Qr06VSCacGBYAreaM2: TFloatField;
    Qr06VSCacGBYAreaP2: TFloatField;
    Qr06VSCacGBYPercentual: TFloatField;
    Qr06SumGBYPecas: TFloatField;
    Qr06SumGBYAreaM2: TFloatField;
    Qr06SumGBYAreaP2: TFloatField;
    Qr06VSCacGBYNO_TAMANHO: TWideStringField;
    Qr06VSCacGBYTamanho: TIntegerField;
    Qr06VSCacGBYMediaM2: TFloatField;
    Label32: TLabel;
    Ed06VSPalletDst: TdmkEditCB;
    CB06VSPalletDst: TdmkDBLookupComboBox;
    Ds06VSPalletDst: TDataSource;
    Qr06VSPalletDst: TmySQLQuery;
    Qr06VSPalletSrcCodigo: TIntegerField;
    Qr06VSPalletSrcNome: TWideStringField;
    Qr06VSPalletDstCodigo: TIntegerField;
    Qr06VSPalletDstNome: TWideStringField;
    Qr06CouNiv1: TmySQLQuery;
    Qr06CouNiv1Codigo: TIntegerField;
    Qr06CouNiv1Nome: TWideStringField;
    Ds06CouNiv1: TDataSource;
    Qr06CouNiv2: TmySQLQuery;
    Qr06CouNiv2Codigo: TIntegerField;
    Qr06CouNiv2Nome: TWideStringField;
    Ds06CouNiv2: TDataSource;
    Label33: TLabel;
    Ed06CouNiv2: TdmkEditCB;
    CB06CouNiv2: TdmkDBLookupComboBox;
    Label34: TLabel;
    Ed06CouNiv1: TdmkEditCB;
    CB06CouNiv1: TdmkDBLookupComboBox;
    RGCacID: TRadioGroup;
    TabSheet8: TTabSheet;
    Panel17: TPanel;
    GroupBox6: TGroupBox;
    Panel18: TPanel;
    Label35: TLabel;
    Label36: TLabel;
    Ed07PallIni: TdmkEdit;
    Ed07PallFim: TdmkEdit;
    Qr07PrevPal: TmySQLQuery;
    Ds07PrevPal: TDataSource;
    DBG07Estq: TdmkDBGridZTO;
    Qr07PrevPalVSPallet: TIntegerField;
    Qr07PrevPalPecas: TFloatField;
    Qr07PrevPalAreaM2: TFloatField;
    Qr07PrevPalAreaP2: TFloatField;
    Qr07PrevPalNO_PRD_TAM_COR: TWideStringField;
    Qr07PrevPalCodigo: TIntegerField;
    Qr07PrevPalNome: TWideStringField;
    Qr07PrevPalEmpresa: TIntegerField;
    Qr07PrevPalStatus: TIntegerField;
    Qr07PrevPalCliStat: TIntegerField;
    Qr07PrevPalGraGruX: TIntegerField;
    Qr07PrevPalDtHrEndAdd: TDateTimeField;
    Qr07PrevPalGerRclCab: TIntegerField;
    Qr07PrevPalDtHrFimRcl: TDateTimeField;
    Qr07PrevPalMovimIDGer: TIntegerField;
    Qr07PrevPalLk: TIntegerField;
    Qr07PrevPalDataCad: TDateField;
    Qr07PrevPalDataAlt: TDateField;
    Qr07PrevPalUserCad: TIntegerField;
    Qr07PrevPalUserAlt: TIntegerField;
    Qr07PrevPalAlterWeb: TSmallintField;
    Qr07PrevPalAtivo: TSmallintField;
    Qr07PrevPalQtdPrevPc: TIntegerField;
    PM07Imprime: TPopupMenu;
    Fichas2: TMenuItem;
    Label37: TLabel;
    Ed02MovimID: TdmkEdit;
    Qr04CouNiv1: TmySQLQuery;
    Qr04CouNiv1Codigo: TIntegerField;
    Qr04CouNiv1Nome: TWideStringField;
    Ds04CouNiv1: TDataSource;
    Qr04CouNiv2: TmySQLQuery;
    Qr04CouNiv2Codigo: TIntegerField;
    Qr04CouNiv2Nome: TWideStringField;
    Ds04CouNiv2: TDataSource;
    TabSheet9: TTabSheet;
    Panel19: TPanel;
    Qr08Fornecedor: TmySQLQuery;
    Ds08Fornecedor: TDataSource;
    Qr08FornecedorCodigo: TIntegerField;
    Qr08FornecedorNOMEENTIDADE: TWideStringField;
    QrNimboRcl: TmySQLQuery;
    QrNimboRclVSPallet: TIntegerField;
    QrNimboRclPecas: TFloatField;
    QrNimboRclAreaM2: TFloatField;
    DsNimboRcl: TDataSource;
    QrEstqR4LmbVrtPeca: TFloatField;
    QrEstqR4LmbVrtPeso: TFloatField;
    QrEstqR4LmbVrtArM2: TFloatField;
    QrEstqR4CliStat: TIntegerField;
    QrEstqR4NO_CLISTAT: TWideStringField;
    QrEstqR4NO_PalStat: TWideStringField;
    QrEstqR4PalVrtPeca: TFloatField;
    QrEstqR4PalVrtPeso: TFloatField;
    QrEstqR4PalVrtArM2: TFloatField;
    QrEstqR4Media: TFloatField;
    QrGGXPalTerPalVrtPeca: TFloatField;
    QrGGXPalTerPalVrtPeso: TFloatField;
    QrGGXPalTerPalVrtArM2: TFloatField;
    QrGGXPalTerLmbVrtPeca: TFloatField;
    QrGGXPalTerLmbVrtPeso: TFloatField;
    QrGGXPalTerLmbVrtArM2: TFloatField;
    QrGGXPalTerCliStat: TIntegerField;
    QrGGXPalTerNO_CLISTAT: TWideStringField;
    QrGGXPalTerNO_PalStat: TWideStringField;
    QrGGXPalTerMedia: TFloatField;
    QrGGXPalTerCouNiv2: TIntegerField;
    QrGGXPalTerNO_CouNiv2: TWideStringField;
    QrGGXPalTerCouNiv1: TIntegerField;
    QrGGXPalTerNO_CouNiv1: TWideStringField;
    QrGGXPalTerInteiros: TFloatField;
    PM04Estq: TPopupMenu;
    Irparaajaneladepallets1: TMenuItem;
    Qr04GraGruY: TmySQLQuery;
    Ds04GraGruY: TDataSource;
    Qr04GraGruYCodigo: TIntegerField;
    Qr04GraGruYTabela: TWideStringField;
    Qr04GraGruYNome: TWideStringField;
    Qr04GraGruYOrdem: TIntegerField;
    QrEstqR4NO_MovimNiv: TWideStringField;
    QrEstqR4NO_MovimID: TWideStringField;
    QrEstqR4MovimNiv: TIntegerField;
    QrEstqR4MovimID: TIntegerField;
    QrEstqR4IMEC: TIntegerField;
    QrEstqR4Codigo: TIntegerField;
    QrEstqR4IMEI: TIntegerField;
    QrEstqR4SerieFch: TIntegerField;
    QrEstqR4NO_SerieFch: TWideStringField;
    QrEstqR4Ficha: TIntegerField;
    Qr00GraGruY: TmySQLQuery;
    Ds00GraGruY: TDataSource;
    Qr00GraGruYCodigo: TIntegerField;
    Qr00GraGruYTabela: TWideStringField;
    Qr00GraGruYNome: TWideStringField;
    Qr00GraGruYOrdem: TIntegerField;
    Label41: TLabel;
    Ed06GraGruXDst: TdmkEditCB;
    CB06GraGruXDst: TdmkDBLookupComboBox;
    Qr06GraGruXDst: TmySQLQuery;
    Ds06GraGruXDst: TDataSource;
    Qr06GraGruXDstGraGru1: TIntegerField;
    Qr06GraGruXDstControle: TIntegerField;
    Qr06GraGruXDstNO_PRD_TAM_COR: TWideStringField;
    Qr06GraGruXDstSIGLAUNIDMED: TWideStringField;
    Qr06GraGruXDstCODUSUUNIDMED: TIntegerField;
    Qr06GraGruXDstNOMEUNIDMED: TWideStringField;
    QrEstqR4PalStat: TIntegerField;
    QrGGXPalTerPalStat: TIntegerField;
    QrEstqR4FaixaMediaM2: TFloatField;
    TabSheet10: TTabSheet;
    QrCiaDst: TmySQLQuery;
    PageControl1: TPageControl;
    TabSheet11: TTabSheet;
    dmkDBGridZTO3: TdmkDBGridZTO;
    DsCiaDst: TDataSource;
    QrCiaDstVSPallet: TIntegerField;
    QrCiaDstVMI_Dest: TIntegerField;
    QrCiaDstCiaPecas: TFloatField;
    QrCiaDstDstPecas: TFloatField;
    TabSheet12: TTabSheet;
    Panel27: TPanel;
    Qr10FichasRMP: TmySQLQuery;
    Ds10FichasRMP: TDataSource;
    Qr10FichasRMPSerieFch: TIntegerField;
    Qr10FichasRMPFicha: TIntegerField;
    Qr10FichasRMPFCH_SRE: TFloatField;
    Qr10FichasRMPITENS: TLargeintField;
    Qr10FichasRMPRefFirst: TDateTimeField;
    Qr10FichasRMPRefLast: TDateTimeField;
    Qr10FichasRMPNO_SERIEFCH: TWideStringField;
    DG10FichasRMP: TdmkDBGridZTO;
    PM10FichasRMP: TPopupMenu;
    IrparajaneladegerenciamentodeFichasRMP1: TMenuItem;
    TabSheet13: TTabSheet;
    frxWET_CURTI_018_11_A: TfrxReport;
    Qr11_1: TmySQLQuery;
    frxDs11_1: TfrxDBDataset;
    Qr11_1GraGruX: TIntegerField;
    Qr11_1Pecas: TFloatField;
    Qr11_1PesoKg: TFloatField;
    Qr11_1AreaM2: TFloatField;
    Qr11_1AreaP2: TFloatField;
    Qr11_1ValorT: TFloatField;
    Qr11_1SdoVrtPeca: TFloatField;
    Qr11_1SdoVrtPeso: TFloatField;
    Qr11_1SdoVrtArM2: TFloatField;
    Qr11_1PalVrtPeca: TFloatField;
    Qr11_1PalVrtPeso: TFloatField;
    Qr11_1PalVrtArM2: TFloatField;
    Qr11_1LmbVrtPeca: TFloatField;
    Qr11_1LmbVrtPeso: TFloatField;
    Qr11_1LmbVrtArM2: TFloatField;
    Qr11_1GraGru1: TIntegerField;
    Qr11_1NO_PRD_TAM_COR: TWideStringField;
    Qr11_1Pallet: TIntegerField;
    Qr11_1NO_PALLET: TWideStringField;
    Qr11_1Terceiro: TIntegerField;
    Qr11_1CliStat: TIntegerField;
    Qr11_1Status: TIntegerField;
    Qr11_1NO_FORNECE: TWideStringField;
    Qr11_1NO_CLISTAT: TWideStringField;
    Qr11_1NO_EMPRESA: TWideStringField;
    Qr11_1NO_STATUS: TWideStringField;
    Qr11_1DataHora: TDateTimeField;
    Qr11_1OrdGGX: TIntegerField;
    Qr11_1OrdGGY: TIntegerField;
    Qr11_1GraGruY: TIntegerField;
    Qr11_1NO_GGY: TWideStringField;
    Qr11_1NO_PalStat: TWideStringField;
    Qr11_1Ativo: TSmallintField;
    Qr11_1Media: TFloatField;
    Qr11_1NO_MovimNiv: TWideStringField;
    Qr11_1NO_MovimID: TWideStringField;
    Qr11_1MovimNiv: TIntegerField;
    Qr11_1MovimID: TIntegerField;
    Qr11_1IMEC: TIntegerField;
    Qr11_1Codigo: TIntegerField;
    Qr11_1IMEI: TIntegerField;
    Qr11_1SerieFch: TIntegerField;
    Qr11_1NO_SerieFch: TWideStringField;
    Qr11_1Ficha: TIntegerField;
    Qr11_1Inteiros: TFloatField;
    Qr11_1PalStat: TIntegerField;
    Qr11_1Empresa: TIntegerField;
    Qr11_2: TmySQLQuery;
    frxDs11_2: TfrxDBDataset;
    Qr11_2GraGruX: TIntegerField;
    Qr11_2Pecas: TFloatField;
    Qr11_2PesoKg: TFloatField;
    Qr11_2AreaM2: TFloatField;
    Qr11_2AreaP2: TFloatField;
    Qr11_2ValorT: TFloatField;
    Qr11_2SdoVrtPeca: TFloatField;
    Qr11_2SdoVrtPeso: TFloatField;
    Qr11_2SdoVrtArM2: TFloatField;
    Qr11_2PalVrtPeca: TFloatField;
    Qr11_2PalVrtPeso: TFloatField;
    Qr11_2PalVrtArM2: TFloatField;
    Qr11_2LmbVrtPeca: TFloatField;
    Qr11_2LmbVrtPeso: TFloatField;
    Qr11_2LmbVrtArM2: TFloatField;
    Qr11_2GraGru1: TIntegerField;
    Qr11_2NO_PRD_TAM_COR: TWideStringField;
    Qr11_2Pallet: TIntegerField;
    Qr11_2NO_PALLET: TWideStringField;
    Qr11_2Terceiro: TIntegerField;
    Qr11_2CliStat: TIntegerField;
    Qr11_2Status: TIntegerField;
    Qr11_2NO_FORNECE: TWideStringField;
    Qr11_2NO_CLISTAT: TWideStringField;
    Qr11_2NO_EMPRESA: TWideStringField;
    Qr11_2NO_STATUS: TWideStringField;
    Qr11_2DataHora: TDateTimeField;
    Qr11_2OrdGGX: TIntegerField;
    Qr11_2OrdGGY: TIntegerField;
    Qr11_2GraGruY: TIntegerField;
    Qr11_2NO_GGY: TWideStringField;
    Qr11_2NO_PalStat: TWideStringField;
    Qr11_2Ativo: TSmallintField;
    Qr11_2Media: TFloatField;
    Qr11_2NO_MovimNiv: TWideStringField;
    Qr11_2NO_MovimID: TWideStringField;
    Qr11_2MovimNiv: TIntegerField;
    Qr11_2MovimID: TIntegerField;
    Qr11_2IMEC: TIntegerField;
    Qr11_2Codigo: TIntegerField;
    Qr11_2IMEI: TIntegerField;
    Qr11_2SerieFch: TIntegerField;
    Qr11_2NO_SerieFch: TWideStringField;
    Qr11_2Ficha: TIntegerField;
    Qr11_2Inteiros: TFloatField;
    Qr11_2PalStat: TIntegerField;
    Qr11_2Empresa: TIntegerField;
    Qr11_3: TmySQLQuery;
    Qr11_4: TmySQLQuery;
    frxDs11_3: TfrxDBDataset;
    frxDs11_4: TfrxDBDataset;
    Qr11_3GraGruX: TIntegerField;
    Qr11_3Pecas: TFloatField;
    Qr11_3PesoKg: TFloatField;
    Qr11_3AreaM2: TFloatField;
    Qr11_3AreaP2: TFloatField;
    Qr11_3ValorT: TFloatField;
    Qr11_3SdoVrtPeca: TFloatField;
    Qr11_3SdoVrtPeso: TFloatField;
    Qr11_3SdoVrtArM2: TFloatField;
    Qr11_3PalVrtPeca: TFloatField;
    Qr11_3PalVrtPeso: TFloatField;
    Qr11_3PalVrtArM2: TFloatField;
    Qr11_3LmbVrtPeca: TFloatField;
    Qr11_3LmbVrtPeso: TFloatField;
    Qr11_3LmbVrtArM2: TFloatField;
    Qr11_3GraGru1: TIntegerField;
    Qr11_3NO_PRD_TAM_COR: TWideStringField;
    Qr11_3Pallet: TIntegerField;
    Qr11_3NO_PALLET: TWideStringField;
    Qr11_3Terceiro: TIntegerField;
    Qr11_3CliStat: TIntegerField;
    Qr11_3Status: TIntegerField;
    Qr11_3NO_FORNECE: TWideStringField;
    Qr11_3NO_CLISTAT: TWideStringField;
    Qr11_3NO_EMPRESA: TWideStringField;
    Qr11_3NO_STATUS: TWideStringField;
    Qr11_3DataHora: TDateTimeField;
    Qr11_3OrdGGX: TIntegerField;
    Qr11_3OrdGGY: TIntegerField;
    Qr11_3GraGruY: TIntegerField;
    Qr11_3NO_GGY: TWideStringField;
    Qr11_3NO_PalStat: TWideStringField;
    Qr11_3Ativo: TSmallintField;
    Qr11_3Media: TFloatField;
    Qr11_3NO_MovimNiv: TWideStringField;
    Qr11_3NO_MovimID: TWideStringField;
    Qr11_3MovimNiv: TIntegerField;
    Qr11_3MovimID: TIntegerField;
    Qr11_3IMEC: TIntegerField;
    Qr11_3Codigo: TIntegerField;
    Qr11_3IMEI: TIntegerField;
    Qr11_3SerieFch: TIntegerField;
    Qr11_3NO_SerieFch: TWideStringField;
    Qr11_3Ficha: TIntegerField;
    Qr11_3Inteiros: TFloatField;
    Qr11_3PalStat: TIntegerField;
    Qr11_3Empresa: TIntegerField;
    Qr11_4GraGruX: TIntegerField;
    Qr11_4Pecas: TFloatField;
    Qr11_4PesoKg: TFloatField;
    Qr11_4AreaM2: TFloatField;
    Qr11_4AreaP2: TFloatField;
    Qr11_4ValorT: TFloatField;
    Qr11_4SdoVrtPeca: TFloatField;
    Qr11_4SdoVrtPeso: TFloatField;
    Qr11_4SdoVrtArM2: TFloatField;
    Qr11_4PalVrtPeca: TFloatField;
    Qr11_4PalVrtPeso: TFloatField;
    Qr11_4PalVrtArM2: TFloatField;
    Qr11_4LmbVrtPeca: TFloatField;
    Qr11_4LmbVrtPeso: TFloatField;
    Qr11_4LmbVrtArM2: TFloatField;
    Qr11_4GraGru1: TIntegerField;
    Qr11_4NO_PRD_TAM_COR: TWideStringField;
    Qr11_4Pallet: TIntegerField;
    Qr11_4NO_PALLET: TWideStringField;
    Qr11_4Terceiro: TIntegerField;
    Qr11_4CliStat: TIntegerField;
    Qr11_4Status: TIntegerField;
    Qr11_4NO_FORNECE: TWideStringField;
    Qr11_4NO_CLISTAT: TWideStringField;
    Qr11_4NO_EMPRESA: TWideStringField;
    Qr11_4NO_STATUS: TWideStringField;
    Qr11_4DataHora: TDateTimeField;
    Qr11_4OrdGGX: TIntegerField;
    Qr11_4OrdGGY: TIntegerField;
    Qr11_4GraGruY: TIntegerField;
    Qr11_4NO_GGY: TWideStringField;
    Qr11_4NO_PalStat: TWideStringField;
    Qr11_4Ativo: TSmallintField;
    Qr11_4Media: TFloatField;
    Qr11_4NO_MovimNiv: TWideStringField;
    Qr11_4NO_MovimID: TWideStringField;
    Qr11_4MovimNiv: TIntegerField;
    Qr11_4MovimID: TIntegerField;
    Qr11_4IMEC: TIntegerField;
    Qr11_4Codigo: TIntegerField;
    Qr11_4IMEI: TIntegerField;
    Qr11_4SerieFch: TIntegerField;
    Qr11_4NO_SerieFch: TWideStringField;
    Qr11_4Ficha: TIntegerField;
    Qr11_4Inteiros: TFloatField;
    Qr11_4PalStat: TIntegerField;
    Qr11_4Empresa: TIntegerField;
    Qr11_T: TmySQLQuery;
    frxDs11_T: TfrxDBDataset;
    Qr11_TGraGruX: TIntegerField;
    Qr11_TPecas: TFloatField;
    Qr11_TPesoKg: TFloatField;
    Qr11_TAreaM2: TFloatField;
    Qr11_TAreaP2: TFloatField;
    Qr11_TValorT: TFloatField;
    Qr11_TSdoVrtPeca: TFloatField;
    Qr11_TSdoVrtPeso: TFloatField;
    Qr11_TSdoVrtArM2: TFloatField;
    Qr11_TPalVrtPeca: TFloatField;
    Qr11_TPalVrtPeso: TFloatField;
    Qr11_TPalVrtArM2: TFloatField;
    Qr11_TLmbVrtPeca: TFloatField;
    Qr11_TLmbVrtPeso: TFloatField;
    Qr11_TLmbVrtArM2: TFloatField;
    Qr11_TGraGru1: TIntegerField;
    Qr11_TNO_PRD_TAM_COR: TWideStringField;
    Qr11_TPallet: TIntegerField;
    Qr11_TNO_PALLET: TWideStringField;
    Qr11_TTerceiro: TIntegerField;
    Qr11_TCliStat: TIntegerField;
    Qr11_TStatus: TIntegerField;
    Qr11_TNO_FORNECE: TWideStringField;
    Qr11_TNO_CLISTAT: TWideStringField;
    Qr11_TNO_EMPRESA: TWideStringField;
    Qr11_TNO_STATUS: TWideStringField;
    Qr11_TDataHora: TDateTimeField;
    Qr11_TOrdGGX: TIntegerField;
    Qr11_TOrdGGY: TIntegerField;
    Qr11_TGraGruY: TIntegerField;
    Qr11_TNO_GGY: TWideStringField;
    Qr11_TNO_PalStat: TWideStringField;
    Qr11_TAtivo: TSmallintField;
    Qr11_TMedia: TFloatField;
    Qr11_TNO_MovimNiv: TWideStringField;
    Qr11_TNO_MovimID: TWideStringField;
    Qr11_TMovimNiv: TIntegerField;
    Qr11_TMovimID: TIntegerField;
    Qr11_TIMEC: TIntegerField;
    Qr11_TCodigo: TIntegerField;
    Qr11_TIMEI: TIntegerField;
    Qr11_TSerieFch: TIntegerField;
    Qr11_TNO_SerieFch: TWideStringField;
    Qr11_TFicha: TIntegerField;
    Qr11_TInteiros: TFloatField;
    Qr11_TPalStat: TIntegerField;
    Qr11_TEmpresa: TIntegerField;
    TabSheet14: TTabSheet;
    Panel28: TPanel;
    PC04_Pallets: TPageControl;
    TabSheet15: TTabSheet;
    TabSheet16: TTabSheet;
    Panel15: TPanel;
    Panel23: TPanel;
    GroupBox10: TGroupBox;
    Panel22: TPanel;
    RG04Ordem1: TRadioGroup;
    RG04Ordem2: TRadioGroup;
    RG04Agrupa: TRadioGroup;
    RG04Ordem3: TRadioGroup;
    GroupBox9: TGroupBox;
    Panel21: TPanel;
    Label15: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Ed04GraGruX: TdmkEditCB;
    CB04GraGruX: TdmkDBLookupComboBox;
    Ed04CouNiv2: TdmkEditCB;
    CB04CouNiv2: TdmkDBLookupComboBox;
    Ed04CouNiv1: TdmkEditCB;
    CB04CouNiv1: TdmkDBLookupComboBox;
    CkStatusReal: TCheckBox;
    DBG04GraGruY: TdmkDBGridZTO;
    GroupBox8: TGroupBox;
    dmkDBGridZTO1: TdmkDBGridZTO;
    DBG04Estq: TdmkDBGridZTO;
    Panel29: TPanel;
    Label42: TLabel;
    DBG04_01BlendVS: TdmkDBGridZTO;
    Qr04_01Sum: TmySQLQuery;
    Qr04_01SumPecas: TFloatField;
    Qr04_01SumAreaM2: TFloatField;
    TabSheet17: TTabSheet;
    Panel30: TPanel;
    Qr13GraGruX: TmySQLQuery;
    Ds13GraGruX: TDataSource;
    Qr13GraGruXGraGru1: TIntegerField;
    Qr13GraGruXControle: TIntegerField;
    Qr13GraGruXNO_PRD_TAM_COR: TWideStringField;
    Qr13GraGruXSIGLAUNIDMED: TWideStringField;
    Qr13GraGruXCODUSUUNIDMED: TIntegerField;
    Qr13GraGruXNOMEUNIDMED: TWideStringField;
    Qr13VSSeqIts: TmySQLQuery;
    Ds13VSSeqIts: TDataSource;
    DBGrid1: TDBGrid;
    Panel32: TPanel;
    Panel31: TPanel;
    RG13_GragruY: TRadioGroup;
    Qr13VSSeqItsCodigo: TIntegerField;
    Qr13VSSeqItsControle: TIntegerField;
    Qr13VSSeqItsMovimCod: TIntegerField;
    Qr13VSSeqItsMovimNiv: TIntegerField;
    Qr13VSSeqItsMovimTwn: TIntegerField;
    Qr13VSSeqItsEmpresa: TIntegerField;
    Qr13VSSeqItsTerceiro: TIntegerField;
    Qr13VSSeqItsCliVenda: TIntegerField;
    Qr13VSSeqItsMovimID: TIntegerField;
    Qr13VSSeqItsLnkNivXtr1: TIntegerField;
    Qr13VSSeqItsLnkNivXtr2: TIntegerField;
    Qr13VSSeqItsDataHora: TDateTimeField;
    Qr13VSSeqItsPallet: TIntegerField;
    Qr13VSSeqItsGraGruX: TIntegerField;
    Qr13VSSeqItsPecas: TFloatField;
    Qr13VSSeqItsPesoKg: TFloatField;
    Qr13VSSeqItsAreaM2: TFloatField;
    Qr13VSSeqItsAreaP2: TFloatField;
    Qr13VSSeqItsValorT: TFloatField;
    Qr13VSSeqItsSrcMovID: TIntegerField;
    Qr13VSSeqItsSrcNivel1: TIntegerField;
    Qr13VSSeqItsSrcNivel2: TIntegerField;
    Qr13VSSeqItsSrcGGX: TIntegerField;
    Qr13VSSeqItsSdoVrtPeca: TFloatField;
    Qr13VSSeqItsSdoVrtPeso: TFloatField;
    Qr13VSSeqItsSdoVrtArM2: TFloatField;
    Qr13VSSeqItsObserv: TWideStringField;
    Qr13VSSeqItsSerieFch: TIntegerField;
    Qr13VSSeqItsFicha: TIntegerField;
    Qr13VSSeqItsMisturou: TSmallintField;
    Qr13VSSeqItsFornecMO: TIntegerField;
    Qr13VSSeqItsCustoMOKg: TFloatField;
    Qr13VSSeqItsCustoMOTot: TFloatField;
    Qr13VSSeqItsValorMP: TFloatField;
    Qr13VSSeqItsDstMovID: TIntegerField;
    Qr13VSSeqItsDstNivel1: TIntegerField;
    Qr13VSSeqItsDstNivel2: TIntegerField;
    Qr13VSSeqItsDstGGX: TIntegerField;
    Qr13VSSeqItsQtdGerPeca: TFloatField;
    Qr13VSSeqItsQtdGerPeso: TFloatField;
    Qr13VSSeqItsQtdGerArM2: TFloatField;
    Qr13VSSeqItsQtdGerArP2: TFloatField;
    Qr13VSSeqItsQtdAntPeca: TFloatField;
    Qr13VSSeqItsQtdAntPeso: TFloatField;
    Qr13VSSeqItsQtdAntArM2: TFloatField;
    Qr13VSSeqItsQtdAntArP2: TFloatField;
    Qr13VSSeqItsAptoUso: TSmallintField;
    Qr13VSSeqItsNotaMPAG: TFloatField;
    Qr13VSSeqItsMarca: TWideStringField;
    Qr13VSSeqItsTpCalcAuto: TIntegerField;
    Qr13VSSeqItsInteiros: TFloatField;
    Qr13VSSeqItsAcumInteir: TFloatField;
    Qr13VSSeqItsSequencia: TIntegerField;
    Qr13VSSeqItsAtivo: TSmallintField;
    Qr13VSSeqItsNO_MovimID: TWideStringField;
    Qr13VSSeqItsNO_MovimNiv: TWideStringField;
    Qr13VSSeqItsNO_PRD_TAM_COR: TWideStringField;
    Qr13VSSeqItsNO_Pallet: TWideStringField;
    Qr13VSSeqItsVSP_CliStat: TLargeintField;
    Qr13VSSeqItsVSP_STATUS: TLargeintField;
    Qr13VSSeqItsNO_FORNECE: TWideStringField;
    Qr13VSSeqItsNO_CliStat: TWideStringField;
    Qr13VSSeqItsNO_EMPRESA: TWideStringField;
    Qr13VSSeqItsNO_STATUS: TWideStringField;
    Qr13VSSeqItsNO_SerieFch: TWideStringField;
    Qr13VSSeqItsNO_SERIE_FICHA: TWideStringField;
    Panel33: TPanel;
    Label43: TLabel;
    Ed13GraGruX: TdmkEditCB;
    CB13GraGruX: TdmkDBLookupComboBox;
    Me13: TMemo;
    frxWET_CURTI_018_03_B3: TfrxReport;
    QrNotaVS: TmySQLQuery;
    QrNotaVSNotaVS: TFloatField;
    QrNotaVSSiglaVS: TWideStringField;
    QrNotaVSTerceiro: TIntegerField;
    QrNotaVSPecas: TFloatField;
    QrNotaVSCodigo: TIntegerField;
    ListacomNota1: TMenuItem;
    GroupBox12: TGroupBox;
    TP13DataIni: TdmkEditDateTimePicker;
    Ck13DataIni: TCheckBox;
    Ck13DataFim: TCheckBox;
    TP13DataFim: TdmkEditDateTimePicker;
    Panel34: TPanel;
    Panel35: TPanel;
    DBG04Artigos: TdmkDBGridZTO;
    Bt04GraGruXNenhum: TBitBtn;
    Bt04GraGruXTodos: TBitBtn;
    TabSheet18: TTabSheet;
    Panel36: TPanel;
    Label44: TLabel;
    Label48: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Ed14GraGruX: TdmkEditCB;
    CB14GraGruX: TdmkDBLookupComboBox;
    Ed14Terceiro: TdmkEditCB;
    CB14Terceiro: TdmkDBLookupComboBox;
    GroupBox13: TGroupBox;
    TP14DataIni: TdmkEditDateTimePicker;
    Ck14DataIni: TCheckBox;
    Ck14DataFim: TCheckBox;
    TP14DataFim: TdmkEditDateTimePicker;
    Ed14SerieFch: TdmkEditCB;
    CB14SerieFch: TdmkDBLookupComboBox;
    Ed14Ficha: TdmkEdit;
    Ed14Marca: TdmkEdit;
    RG14FontePsq: TRadioGroup;
    DBG14VSMovIts: TdmkDBGridZTO;
    GroupBox14: TGroupBox;
    Panel37: TPanel;
    RG14Ordem1: TRadioGroup;
    RG14Ordem2: TRadioGroup;
    RG14Agrupa: TRadioGroup;
    RG14Ordem3: TRadioGroup;
    RG14Periodo: TRadioGroup;
    Ck14SoMPAGNo0: TCheckBox;
    Ck14Cor: TCheckBox;
    PM13Grade: TPopupMenu;
    IrparajeneladoIMEIselecionado1: TMenuItem;
    Fluxo1: TMenuItem;
    Remover1: TMenuItem;
    Adicionar1: TMenuItem;
    TabSheet19: TTabSheet;
    Me15ListaIMEIs: TMemo;
    RG13_AnaliSinte: TRadioGroup;
    La13_MaxInteiros: TLabel;
    Ed13_MaxInteiros: TdmkEdit;
    RG13TipoMov: TRadioGroup;
    Panel20: TPanel;
    Panel38: TPanel;
    PC08: TPageControl;
    TabSheet20: TTabSheet;
    Panel39: TPanel;
    Label40: TLabel;
    RG08TipoMovim: TRadioGroup;
    RG08Ordem: TRadioGroup;
    RG08Agrupa: TRadioGroup;
    Ed08Terceiro: TdmkEditCB;
    CB08Terceiro: TdmkDBLookupComboBox;
    Ck08Simples: TdmkCheckBox;
    TabSheet21: TTabSheet;
    Panel41: TPanel;
    GroupBox7: TGroupBox;
    TP08DataIni: TdmkEditDateTimePicker;
    Ck08DataIni: TCheckBox;
    Ck08DataFim: TCheckBox;
    TP08DataFim: TdmkEditDateTimePicker;
    PM08_01_Todos: TPopupMenu;
    Sadas1: TMenuItem;
    Entradas1: TMenuItem;
    odos1: TMenuItem;
    Qr10FichasRMPNO_FORNECE: TWideStringField;
    N2: TMenuItem;
    PalletxFornecedor1: TMenuItem;
    otalizarporfornecedor1: TMenuItem;
    otalizarporPallet1: TMenuItem;
    Ed13_Pallet: TdmkEdit;
    Label45: TLabel;
    Label46: TLabel;
    Ed13_IMEI: TdmkEdit;
    TabSheet22: TTabSheet;
    Panel42: TPanel;
    GroupBox15: TGroupBox;
    TP16DataIni: TdmkEditDateTimePicker;
    Ck16DataIni: TCheckBox;
    Ck16DataFim: TCheckBox;
    TP16DataFim: TdmkEditDateTimePicker;
    DBG16Fatores: TdmkDBGridZTO;
    PM16Fatores: TPopupMenu;
    IrparajaneladeOC1: TMenuItem;
    Irparajaneladepalletdeorigem1: TMenuItem;
    Irparajaneladepalletdedestino1: TMenuItem;
    Label47: TLabel;
    Ed13SerieFch: TdmkEditCB;
    CB13SerieFch: TdmkDBLookupComboBox;
    Ed13Ficha: TdmkEdit;
    Label49: TLabel;
    ListacomRevisor1: TMenuItem;
    QrRevisores: TmySQLQuery;
    QrRevisoresRevisor: TIntegerField;
    QrRevisoresPecas: TFloatField;
    QrRevisoresDataHora_TXT: TWideStringField;
    QrRevisoresDataHora: TDateTimeField;
    QrRevisoresNO_REVISOR: TWideStringField;
    Label54: TLabel;
    TPDataRelativa: TdmkEditDateTimePicker;
    TabSheet23: TTabSheet;
    Qr03GraGruY: TmySQLQuery;
    Ds03GraGruY: TDataSource;
    Qr03GraGruYCodigo: TIntegerField;
    Qr03GraGruYTabela: TWideStringField;
    Qr03GraGruYNome: TWideStringField;
    Qr03GraGruYOrdem: TIntegerField;
    Panel44: TPanel;
    DBG03GraGruY: TdmkDBGridZTO;
    DBG03GraGruX: TdmkDBGridZTO;
    Qr03GraGruX: TmySQLQuery;
    Ds03GraGruX: TDataSource;
    Qr03GraGruXGraGru1: TIntegerField;
    Qr03GraGruXControle: TIntegerField;
    Qr03GraGruXNO_PRD_TAM_COR: TWideStringField;
    Qr03GraGruXSIGLAUNIDMED: TWideStringField;
    Qr03GraGruXCODUSUUNIDMED: TIntegerField;
    Qr03GraGruXNOMEUNIDMED: TWideStringField;
    Panel45: TPanel;
    GroupBox17: TGroupBox;
    TP17DataIni: TdmkEditDateTimePicker;
    Ck17DataIni: TCheckBox;
    Ck17DataFim: TCheckBox;
    TP17DataFim: TdmkEditDateTimePicker;
    Qr00GraGruX: TmySQLQuery;
    Ds00GraGruX: TDataSource;
    Qr00GraGruXGraGru1: TIntegerField;
    Qr00GraGruXControle: TIntegerField;
    Qr00GraGruXNO_PRD_TAM_COR: TWideStringField;
    Qr00GraGruXSIGLAUNIDMED: TWideStringField;
    Qr00GraGruXCODUSUUNIDMED: TIntegerField;
    Qr00GraGruXNOMEUNIDMED: TWideStringField;
    Bt02Janela: TBitBtn;
    Qr00StqCenCad: TmySQLQuery;
    Qr00StqCenCadCodigo: TIntegerField;
    Qr00StqCenCadNome: TWideStringField;
    Ds00StqCenCad: TDataSource;
    Qr00CouNiv2: TmySQLQuery;
    Ds00CouNiv2: TDataSource;
    Qr00CouNiv2Codigo: TIntegerField;
    Qr00CouNiv2Nome: TWideStringField;
    FichasCOMonomedoPallet1: TMenuItem;
    FichasSEMonomedoPallet1: TMenuItem;
    TabSheet24: TTabSheet;
    GroupBox20: TGroupBox;
    Panel48: TPanel;
    RG18Ordem1: TRadioGroup;
    RG18Ordem2: TRadioGroup;
    RG18Agrupa: TRadioGroup;
    RG18Ordem3: TRadioGroup;
    Qr00Fornecedor: TmySQLQuery;
    Ds00Fornecedor: TDataSource;
    Qr00FornecedorCodigo: TIntegerField;
    Qr00FornecedorNOMEENTIDADE: TWideStringField;
    PM03Imprime: TPopupMenu;
    Etiqueta1: TMenuItem;
    PackingList1: TMenuItem;
    PackingListHorizontal1: TMenuItem;
    Ck17VsMovItb: TdmkCheckBox;
    Ck08VSMovItb: TCheckBox;
    QrVSMovItsCodigo: TLargeintField;
    QrVSMovItsControle: TLargeintField;
    QrVSMovItsMovimCod: TLargeintField;
    QrVSMovItsMovimNiv: TLargeintField;
    QrVSMovItsMovimTwn: TLargeintField;
    QrVSMovItsEmpresa: TLargeintField;
    QrVSMovItsTerceiro: TLargeintField;
    QrVSMovItsCliVenda: TLargeintField;
    QrVSMovItsMovimID: TLargeintField;
    QrVSMovItsDataHora: TDateTimeField;
    QrVSMovItsPallet: TLargeintField;
    QrVSMovItsGraGruX: TLargeintField;
    QrVSMovItsPecas: TFloatField;
    QrVSMovItsPesoKg: TFloatField;
    QrVSMovItsAreaM2: TFloatField;
    QrVSMovItsAreaP2: TFloatField;
    QrVSMovItsValorT: TFloatField;
    QrVSMovItsSrcMovID: TLargeintField;
    QrVSMovItsSrcNivel1: TLargeintField;
    QrVSMovItsSrcNivel2: TLargeintField;
    QrVSMovItsSrcGGX: TLargeintField;
    QrVSMovItsSdoVrtPeca: TFloatField;
    QrVSMovItsSdoVrtPeso: TFloatField;
    QrVSMovItsSdoVrtArM2: TFloatField;
    QrVSMovItsObserv: TWideStringField;
    QrVSMovItsSerieFch: TLargeintField;
    QrVSMovItsFicha: TLargeintField;
    QrVSMovItsMisturou: TLargeintField;
    QrVSMovItsFornecMO: TLargeintField;
    QrVSMovItsCustoMOKg: TFloatField;
    QrVSMovItsCustoMOM2: TFloatField;
    QrVSMovItsCustoMOTot: TFloatField;
    QrVSMovItsValorMP: TFloatField;
    QrVSMovItsDstMovID: TLargeintField;
    QrVSMovItsDstNivel1: TLargeintField;
    QrVSMovItsDstNivel2: TLargeintField;
    QrVSMovItsDstGGX: TLargeintField;
    QrVSMovItsQtdGerPeca: TFloatField;
    QrVSMovItsQtdGerPeso: TFloatField;
    QrVSMovItsQtdGerArM2: TFloatField;
    QrVSMovItsQtdGerArP2: TFloatField;
    QrVSMovItsQtdAntPeca: TFloatField;
    QrVSMovItsQtdAntPeso: TFloatField;
    QrVSMovItsQtdAntArM2: TFloatField;
    QrVSMovItsQtdAntArP2: TFloatField;
    QrVSMovItsNotaMPAG: TFloatField;
    QrVSMovItsNO_PALLET: TWideStringField;
    QrVSMovItsNO_PRD_TAM_COR: TWideStringField;
    QrVSMovItsNO_TTW: TWideStringField;
    QrVSMovItsID_TTW: TLargeintField;
    QrVSMovItsNO_FORNECE: TWideStringField;
    QrVSMovItsNO_SerieFch: TWideStringField;
    QrVSMovItsReqMovEstq: TLargeintField;
    QrEstqR1: TmySQLQuery;
    QrEstqR1Empresa: TIntegerField;
    QrEstqR1GraGruX: TIntegerField;
    QrEstqR1Pecas: TFloatField;
    QrEstqR1PesoKg: TFloatField;
    QrEstqR1AreaM2: TFloatField;
    QrEstqR1AreaP2: TFloatField;
    QrEstqR1ValorT: TFloatField;
    QrEstqR1SdoVrtPeca: TFloatField;
    QrEstqR1SdoVrtPeso: TFloatField;
    QrEstqR1SdoVrtArM2: TFloatField;
    QrEstqR1PalVrtPeca: TFloatField;
    QrEstqR1PalVrtPeso: TFloatField;
    QrEstqR1PalVrtArM2: TFloatField;
    QrEstqR1LmbVrtPeca: TFloatField;
    QrEstqR1LmbVrtPeso: TFloatField;
    QrEstqR1LmbVrtArM2: TFloatField;
    QrEstqR1GraGru1: TIntegerField;
    QrEstqR1NO_PRD_TAM_COR: TWideStringField;
    QrEstqR1Pallet: TIntegerField;
    QrEstqR1NO_PALLET: TWideStringField;
    QrEstqR1Terceiro: TIntegerField;
    QrEstqR1CliStat: TIntegerField;
    QrEstqR1Status: TIntegerField;
    QrEstqR1NO_FORNECE: TWideStringField;
    QrEstqR1NO_CLISTAT: TWideStringField;
    QrEstqR1NO_EMPRESA: TWideStringField;
    QrEstqR1NO_STATUS: TWideStringField;
    QrEstqR1DataHora: TDateTimeField;
    QrEstqR1OrdGGX: TIntegerField;
    QrEstqR1OrdGGY: TIntegerField;
    QrEstqR1GraGruY: TIntegerField;
    QrEstqR1NO_GGY: TWideStringField;
    QrEstqR1NO_PalStat: TWideStringField;
    QrEstqR1Ativo: TSmallintField;
    QrEstqR1Media: TFloatField;
    QrEstqR1NO_MovimNiv: TWideStringField;
    QrEstqR1NO_MovimID: TWideStringField;
    QrEstqR1MovimNiv: TIntegerField;
    QrEstqR1MovimID: TIntegerField;
    QrEstqR1IMEC: TIntegerField;
    QrEstqR1Codigo: TIntegerField;
    QrEstqR1IMEI: TIntegerField;
    QrEstqR1SerieFch: TIntegerField;
    QrEstqR1NO_SerieFch: TWideStringField;
    QrEstqR1Ficha: TIntegerField;
    QrEstqR1Inteiros: TFloatField;
    QrEstqR1PalStat: TIntegerField;
    QrEstqR1CUS_UNIT_M2: TFloatField;
    QrEstqR1CUS_UNIT_KG: TFloatField;
    QrEstqR1ReqMovEstq: TIntegerField;
    QrEstqR1StqCenCad: TIntegerField;
    QrEstqR1NO_StqCenCad: TWideStringField;
    QrEstqR1StqCenLoc: TIntegerField;
    QrEstqR1NO_LOC_CEN: TWideStringField;
    frxDsEstqR1: TfrxDBDataset;
    DsEstqR1: TDataSource;
    CG18Ordens: TdmkCheckGroup;
    Ck18Cor: TCheckBox;
    Panel43: TPanel;
    Splitter1: TSplitter;
    GroupBox16: TGroupBox;
    DBG00GraGruY: TdmkDBGridZTO;
    GroupBox18: TGroupBox;
    DBG00GraGruX: TdmkDBGridZTO;
    ListacomRevisor2: TMenuItem;
    GroupBox21: TGroupBox;
    Panel51: TPanel;
    Ed18Especificos: TdmkEdit;
    Ck18Especificos: TCheckBox;
    SpeedButton1: TSpeedButton;
    BtIncosist: TBitBtn;
    TabSheet25: TTabSheet;
    Panel53: TPanel;
    GroupBox22: TGroupBox;
    TP19DataIni: TdmkEditDateTimePicker;
    Ck19DataIni: TCheckBox;
    Ck19DataFim: TCheckBox;
    TP19DataFim: TdmkEditDateTimePicker;
    Argox1: TMenuItem;
    Ck06Pallets: TCheckBox;
    TabSheet26: TTabSheet;
    Qr18Fornecedor: TmySQLQuery;
    Qr18FornecedorCodigo: TIntegerField;
    Qr18FornecedorNOMEENTIDADE: TWideStringField;
    Ds18Fornecedor: TDataSource;
    Panel54: TPanel;
    Label56: TLabel;
    Ed18Fornecedor: TdmkEditCB;
    CB18Fornecedor: TdmkDBLookupComboBox;
    Panel55: TPanel;
    DBGNFes: TdmkDBGridZTO;
    QrNFes: TmySQLQuery;
    DsNFes: TDataSource;
    QrNFescStat: TIntegerField;
    QrNFesNFe: TIntegerField;
    QrNFesMovimCod: TIntegerField;
    QrNFesMovimID: TFloatField;
    QrNFesData: TDateTimeField;
    QrNFesTerceiro: TIntegerField;
    QrNFesPecas: TFloatField;
    QrNFesPesoKg: TFloatField;
    QrNFesAreaM2: TFloatField;
    QrNFesValorT: TFloatField;
    QrNFesNO_ENT: TWideStringField;
    QrNFesNO_MovimID: TWideStringField;
    PM20NFes: TPopupMenu;
    Janeladomovimento1: TMenuItem;
    AlteraStatusNFe1: TMenuItem;
    QrNFesTabela: TWideStringField;
    QrNFesNO_cStat: TWideStringField;
    QrNFesCampo: TWideStringField;
    Panel56: TPanel;
    Ed04NomeGG1: TEdit;
    QrNFesSerie: TIntegerField;
    TabSheet27: TTabSheet;
    Qr21Fornecedor: TmySQLQuery;
    Qr21FornecedorCodigo: TIntegerField;
    Qr21FornecedorNOMEENTIDADE: TWideStringField;
    Ds21Fornecedor: TDataSource;
    Panel59: TPanel;
    RadioGroup1: TRadioGroup;
    Panel60: TPanel;
    Panel61: TPanel;
    RG21Ordem1: TRadioGroup;
    RG21Ordem2: TRadioGroup;
    RG21Ordem3: TRadioGroup;
    RG21Agrupa: TRadioGroup;
    RG21Ordem4: TRadioGroup;
    GroupBox27: TGroupBox;
    DBG21CouNiv2: TdmkDBGridZTO;
    Panel62: TPanel;
    Label60: TLabel;
    Ed21Terceiro: TdmkEditCB;
    CB21Terceiro: TdmkDBLookupComboBox;
    GroupBox25: TGroupBox;
    TP21DataIni: TdmkEditDateTimePicker;
    Ck21DataIni: TCheckBox;
    Ck21DataFim: TCheckBox;
    TP21DataFim: TdmkEditDateTimePicker;
    CG21Bastidao: TdmkCheckGroup;
    GroupBox26: TGroupBox;
    Ck21OPIni: TCheckBox;
    Ck21OPFim: TCheckBox;
    Ed21OPIni: TdmkEdit;
    Ed21OPFim: TdmkEdit;
    Qr21CouNiv2: TmySQLQuery;
    Ds21CouNiv2: TDataSource;
    Qr21CouNiv2Codigo: TIntegerField;
    Qr21CouNiv2Nome: TWideStringField;
    Ed21ImeiIni: TdmkEdit;
    Label59: TLabel;
    CkFichaPallet: TCheckBox;
    TabSheet28: TTabSheet;
    Panel64: TPanel;
    Label62: TLabel;
    Ed22PalletA: TdmkEdit;
    Label63: TLabel;
    Ed22PalletB: TdmkEdit;
    PM10Imprime: TPopupMenu;
    PesquisaFichaRMP1: TMenuItem;
    RelatriodeResultados1: TMenuItem;
    N3: TMenuItem;
    NFeavulsa1: TMenuItem;
    dmkPermissoes1: TdmkPermissoes;
    CSTabSheetChamou: TdmkCompoStore;
    TabSheet29: TTabSheet;
    Panel66: TPanel;
    Panel67: TPanel;
    DBG23VSMORet: TdmkDBGridZTO;
    Ck23Indef: TCheckBox;
    Qr23VSMORet: TmySQLQuery;
    Ds23VSMORet: TDataSource;
    Qr23VSMORetCodigo: TIntegerField;
    Qr23VSMORetNFCMO_Pecas: TFloatField;
    Qr23VSMORetNFCMO_PesoKg: TFloatField;
    Qr23VSMORetNFCMO_AreaM2: TFloatField;
    Qr23VSMORetVSVMI_Controle: TIntegerField;
    Qr23VSMORetVSVMI_Codigo: TIntegerField;
    Qr23VSMORetVSVMI_MovimID: TIntegerField;
    Qr23VSMORetVSVMI_MovimNiv: TIntegerField;
    Qr23VSMORetVSVMI_MovimCod: TIntegerField;
    Qr23VSMORetVSVMI_SerNF: TIntegerField;
    Qr23VSMORetVSVMI_nNF: TIntegerField;
    TabSheet30: TTabSheet;
    Panel68: TPanel;
    QrNFesNO_VmcWarn: TWideStringField;
    QrNFesVSVmcWrn: TIntegerField;
    CG20VmcWrn: TdmkCheckGroup;
    QrNFesVSVmcObs: TWideStringField;
    N4: TMenuItem;
    Informaesdomovimento1: TMenuItem;
    QrNFesTxtSaldo: TWideStringField;
    frxWET_CURTI_018_20: TfrxReport;
    frxDsNFes: TfrxDBDataset;
    QrNFesVSVmcSeq: TWideStringField;
    BtDesfazOrdenacao: TBitBtn;
    RG20Ordem1: TRadioGroup;
    Panel7: TPanel;
    GroupBox23: TGroupBox;
    TP20DataIni: TdmkEditDateTimePicker;
    Ck20DataIni: TCheckBox;
    Ck20DataFim: TCheckBox;
    TP20DataFim: TdmkEditDateTimePicker;
    GroupBox24: TGroupBox;
    Panel57: TPanel;
    Label57: TLabel;
    Label58: TLabel;
    Ed20NFeIni: TdmkEdit;
    Ed20NFeFim: TdmkEdit;
    Ck20Serie: TCheckBox;
    Ed20Serie: TdmkEdit;
    RG20Ordem2: TRadioGroup;
    RG20Ordem3: TRadioGroup;
    RG20Ordem4: TRadioGroup;
    RG20Direcao: TRadioGroup;
    CG20VmcSta: TdmkCheckGroup;
    QrNFesVSVmcSta: TSmallintField;
    TabSheet31: TTabSheet;
    Etiquetas8porpgina1: TMenuItem;
    Qr04StqCenCad: TmySQLQuery;
    Qr04StqCenCadCodigo: TIntegerField;
    Qr04StqCenCadNome: TWideStringField;
    Ds04StqCenCad: TDataSource;
    Label4: TLabel;
    Ed04StqCenCad: TdmkEditCB;
    CB04StqCenCad: TdmkDBLookupComboBox;
    Panel70: TPanel;
    TabSheet32: TTabSheet;
    Panel71: TPanel;
    ScrollBox1: TScrollBox;
    Panel24: TPanel;
    Panel25: TPanel;
    Panel47: TPanel;
    Panel46: TPanel;
    Label53: TLabel;
    Label55: TLabel;
    Ed00StqCenCad: TdmkEditCB;
    CB00StqCenCad: TdmkDBLookupComboBox;
    Ck00DataCompra: TCheckBox;
    RG00ZeroNegat: TRadioGroup;
    Ed00Terceiro: TdmkEditCB;
    CB00Terceiro: TdmkDBLookupComboBox;
    Ck00EmProcessoBH: TCheckBox;
    GroupBox19: TGroupBox;
    DBG00CouNiv2: TdmkDBGridZTO;
    Panel69: TPanel;
    Label1: TLabel;
    Ed00MovimCod: TdmkEdit;
    Panel26: TPanel;
    RG00_Ordem1: TRadioGroup;
    RG00_Ordem2: TRadioGroup;
    RG00_Ordem3: TRadioGroup;
    Panel49: TPanel;
    RG00_Agrupa: TRadioGroup;
    Panel50: TPanel;
    Panel58: TPanel;
    Ck00EstoqueEm: TCheckBox;
    TP00EstoqueEm: TdmkEditDateTimePicker;
    RG00DescrAgruNoItm: TRadioGroup;
    RG00_Ordem4: TRadioGroup;
    RG00_Ordem5: TRadioGroup;
    Panel63: TPanel;
    GroupBox28: TGroupBox;
    Panel65: TPanel;
    Label64: TLabel;
    Label65: TLabel;
    Ed00NFeIni: TdmkEdit;
    Ed00NFeFim: TdmkEdit;
    Ck00Serie: TCheckBox;
    Ed00Serie: TdmkEdit;
    QrVSMovItsMediaArM2: TFloatField;
    QrVSMovItsMediaPeso: TFloatField;
    Verticalcolunasft1: TMenuItem;
    Horizontallinhasft1: TMenuItem;
    Qr06VSMrtCad: TMySQLQuery;
    Qr06VSMrtCadCodigo: TIntegerField;
    Qr06VSMrtCadNome: TWideStringField;
    Ds06VSMrtCad: TDataSource;
    RG00Relatorio: TRadioGroup;
    Panel13: TPanel;
    PC_Dif_Movim: TPageControl;
    TabSheet33: TTabSheet;
    Panel40: TPanel;
    CG08_01_MovimIDSign: TdmkCheckGroup;
    Bt08_01_Todos: TBitBtn;
    Bt08_01_Nenhum: TBitBtn;
    TabSheet34: TTabSheet;
    Panel72: TPanel;
    Panel73: TPanel;
    BtNenhumResidual: TBitBtn;
    BrTodosResidual: TBitBtn;
    CG08_01_MovimIDResi: TdmkCheckGroup;
    Panel74: TPanel;
    RG08_01_Ordem1: TRadioGroup;
    RG08_01_Ordem2: TRadioGroup;
    RG08_01_Agrupa: TRadioGroup;
    Panel75: TPanel;
    Panel52: TPanel;
    Ed00NomeGG1: TEdit;
    Label8: TLabel;
    Panel76: TPanel;
    Label9: TLabel;
    EdGGXs: TEdit;
    QrClientMO: TMySQLQuery;
    QrClientMONOMECI: TWideStringField;
    QrClientMOCodigo: TIntegerField;
    DsClientMO: TDataSource;
    CBClientMO: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdClientMO: TdmkEditCB;
    rmica95x22ZebraArgoxTSC1: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxWET_CURTI_018_XX_AGetValue(const VarName: string;
      var Value: Variant);
    procedure DG02VSMovItsDblClick(Sender: TObject);
    procedure QrEstqR3BeforeClose(DataSet: TDataSet);
    procedure QrEstqR3AfterOpen(DataSet: TDataSet);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure Lista1Click(Sender: TObject);
    procedure Bt02AtzSdoVrtClick(Sender: TObject);
    procedure PCRelatorioChange(Sender: TObject);
    procedure QrEstqR4AfterOpen(DataSet: TDataSet);
    procedure QrEstqR4BeforeClose(DataSet: TDataSet);
    procedure Vertical1Click(Sender: TObject);
    procedure Ed04GraGruXChange(Sender: TObject);
    procedure QrEstqR4CalcFields(DataSet: TDataSet);
    procedure QrGGXPalTerCalcFields(DataSet: TDataSet);
    procedure Qr06VSCacGBYCalcFields(DataSet: TDataSet);
    procedure Ed07PallIniExit(Sender: TObject);
    procedure Qr07PrevPalBeforeClose(DataSet: TDataSet);
    procedure Qr07PrevPalAfterOpen(DataSet: TDataSet);
    procedure Fichas2Click(Sender: TObject);
    procedure DBG04EstqDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Irparaajaneladepallets1Click(Sender: TObject);
    procedure PM04EstqPopup(Sender: TObject);
    procedure IrparajaneladegerenciamentodeFichasRMP1Click(Sender: TObject);
    procedure PM10FichasRMPPopup(Sender: TObject);
    procedure ListacomNota1Click(Sender: TObject);
    procedure Bt04GraGruXTodosClick(Sender: TObject);
    procedure Bt04GraGruXNenhumClick(Sender: TObject);
    procedure IrparajeneladoIMEIselecionado1Click(Sender: TObject);
    procedure Remover1Click(Sender: TObject);
    procedure Adicionar1Click(Sender: TObject);
    procedure RG13_GragruYClick(Sender: TObject);
    procedure RG13_AnaliSinteClick(Sender: TObject);
    procedure Bt08_01_TodosClick(Sender: TObject);
    procedure Bt08_01_NenhumClick(Sender: TObject);
    procedure odos1Click(Sender: TObject);
    procedure Sadas1Click(Sender: TObject);
    procedure Entradas1Click(Sender: TObject);
    procedure otalizarporfornecedor1Click(Sender: TObject);
    procedure otalizarporPallet1Click(Sender: TObject);
    procedure IrparajaneladeOC1Click(Sender: TObject);
    procedure PM16FatoresPopup(Sender: TObject);
    procedure Irparajaneladepalletdeorigem1Click(Sender: TObject);
    procedure Irparajaneladepalletdedestino1Click(Sender: TObject);
    procedure ListacomRevisor1Click(Sender: TObject);
    procedure Bt02JanelaClick(Sender: TObject);
    procedure FichasCOMonomedoPallet1Click(Sender: TObject);
    procedure FichasSEMonomedoPallet1Click(Sender: TObject);
    procedure Etiqueta1Click(Sender: TObject);
    procedure PackingList1Click(Sender: TObject);
    procedure PackingListHorizontal1Click(Sender: TObject);
    procedure QrEstqR1CalcFields(DataSet: TDataSet);
    procedure ListacomRevisor2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtIncosistClick(Sender: TObject);
    procedure Ed00NomeGG1Change(Sender: TObject);
    procedure Argox1Click(Sender: TObject);
    procedure DBGNFesDblClick(Sender: TObject);
    procedure Janeladomovimento1Click(Sender: TObject);
    procedure AlteraStatusNFe1Click(Sender: TObject);
    procedure DBGNFesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Ed04NomeGG1Change(Sender: TObject);
    procedure Ed20NFeIniRedefinido(Sender: TObject);
    procedure CkFichaPalletClick(Sender: TObject);
    procedure PesquisaFichaRMP1Click(Sender: TObject);
    procedure RelatriodeResultados1Click(Sender: TObject);
    procedure NFeavulsa1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBG23VSMORetDblClick(Sender: TObject);
    procedure Informaesdomovimento1Click(Sender: TObject);
    procedure frxWET_CURTI_018_20GetValue(const VarName: string;
      var Value: Variant);
    procedure QrNFesBeforeClose(DataSet: TDataSet);
    procedure QrNFesAfterOpen(DataSet: TDataSet);
    procedure BtDesfazOrdenacaoClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Etiquetas8porpgina1Click(Sender: TObject);
    procedure Horizontal1Click(Sender: TObject);
    procedure Horizontallinhasft1Click(Sender: TObject);
    procedure Verticalcolunasft1Click(Sender: TObject);
    procedure BrTodosResidualClick(Sender: TObject);
    procedure BtNenhumResidualClick(Sender: TObject);
    procedure rmica95x22ZebraArgoxTSC1Click(Sender: TObject);
  private
    { Private declarations }
    FVSMovImp1, FVSMovImp1_View, FVSMovImp2, FVSMovImp3, FVSMovImp4,
    (*FVSMovImp5,*) FVSCacItsX, FVSMovIts, FVSCacGBY, FVSLstPalBox,
    FVSBlendVS, FVSLstPalGhost, FVSSeqIts,(*FVSFluxIncon, *)
    FVMI_019_Sorc, FVMI_019_Dest, FVMI_019_Main: String;
    FNotaRevisor: Integer;
    MyArr03: array of Integer;

    //
    procedure AtualizaPalletsEmMontagem();
    procedure AtualizaPalletsFantasmas();
    procedure Configura13DataIni();
    function  DefineMyArr03(): Boolean;
    function  DefineSQLMovimID(const Obrigatorio: Boolean; const EdMovimID:
              TdmkEdit; var SQL: String): Boolean;
    procedure PesquisaIMEIS();
    procedure PesquisaPallets();
    procedure PesquisaMartelos();
    procedure PesquisaTamanhos();
    procedure PesquisaPrevia();
    procedure PesquisaFaltaDeCouros();
    procedure PesquisaMovimDeDifer();
    procedure PesquisaFichasRMP();
    procedure PesquisaBlendVS();
    procedure ImprimeClassesGeradasVariosIMEIs();
    procedure ImprimeEstoqueReal();
    procedure ImprimeEstoquePorGrupo();
    procedure ImprimeEtiquetasDePallets(InfoNO_PALLET: Boolean; LabelsPerPage: Integer);
    procedure ImprimeFichasDePallets(InfoNO_PALLET: Boolean; DestImprFichaPallet: TDestImprFichaPallet);
    procedure ImprimeFluxo();
    procedure ImprimeHistorico();
    procedure ImprimeNotaMPAG();
    procedure ImprimeOrdens();
    procedure ImprimeDiario();
    procedure ImprimePackLists(Vertical: Boolean; VSMovImp4, VSLstPalBox:
              String; Grandeza: TGrandezaArea);
    procedure ImprimePallets(frxReport: TfrxReport; Titulo: String);
    procedure ImprimeFatorAParaFatorB();
    procedure ImprimeRequisicaoDeMovimentacao();
    procedure ImprimeListaNFesEmitidasVS();
    procedure ImprimeRendimentoSemi();
    procedure ImprimeComparaPallet();
    procedure ImprimeNFRetMOIndef();
    procedure ImprimeClasificacoesInconsistentes();
    procedure ImprimeEstoqueCustoIntegrado();

    //
    procedure JanelaMovimentoID();
    //
    procedure PesquisaLancamentos();

    procedure Reopen00GraGruX();
    //procedure VS_PF.ReopenGraGruX_A(Qry: TmySQLQuery; NomeParcial: String);
    //procedure VS_PF.ReopenPallet_A(Qry: TmySQLQuery);
    procedure ReopenVSMovImp3(Empresa, GraGruX, Pallet, Terceiro: Integer);
    procedure ReopenVSMovImp4(Empresa, GraGruX, Pallet, Terceiro, CouNiv1,
              CouNiv2: Integer; GraGruYs: String);

    procedure SetHabilitado(Habilitado: Boolean);
    procedure SetaTodosItens03(Ativo: Boolean);
    procedure SetaTodosItens04(Ativo: Boolean);
    procedure SetaTodosItensGraGruX04(Ativo: Boolean);
    procedure SetaTodosItens07(Ativo: Boolean);

    procedure MostraMovimento();

    function SQL_PalletsReal(): String;
  public
    { Public declarations }
  end;

  var
  FmVSMovImp: TFmVSMovImp;

implementation

uses UnMyObjects, Module, ModuleGeral, UnDmkProcFunc, DmkDAC_PF, UMySQLModule,
  CreateVS, AppListas, UnVS_PF, UnVS_CRC_PF, GraGruX, VSImpFluxo, ModVS,
  VSImpDif, VSImpRequis, NFe_PF, MyGlyfs, Principal, GetValor;

{$R *.DFM}

procedure TFmVSMovImp.BtImprimeClick(Sender: TObject);
const
  LPFMO = '';
var
  I, N: Integer;
begin
  case PCRelatorio.ActivePageIndex of
    0: ImprimeEstoqueReal();
    3: MyObjects.MostraPopUpDeBotao(PM03Imprime, BtImprime);
    4: MyObjects.MostraPopUpDeBotao(PM04Imprime, BtImprime);
    7: MyObjects.MostraPopUpDeBotao(PM07Imprime, BtImprime);
    20:
    begin
      MyObjects.frxDefineDataSets(frxWET_CURTI_018_20, [
        DModG.frxDsDono,
        frxDsNFes
      ]);
      MyObjects.frxMostra(frxWET_CURTI_018_20, 'NF-es VS');
    end;
    else Geral.MB_Erro('Item nao habilitado em "FmVSMovImp.BtImprimeClick()"');
  end;
  //ImprimePallets(frxWET_CURTI_018_03_A, 'Fichas de Pallets');
  //
end;

procedure TFmVSMovImp.BtIncosistClick(Sender: TObject);
begin
  VS_PF.VerificaInconsistencias(PB1, LaAviso1, LaAviso2);
end;

procedure TFmVSMovImp.BtNenhumClick(Sender: TObject);
begin
  case PCRelatorio.ActivePageIndex of
    3: SetaTodosItens03(False);
    4: SetaTodosItens04(False);
    7: SetaTodosItens07(False);
    else Geral.MB_Erro('Item nao habilitado em "FmVSMovImp.BtTudoClick()"');
  end;
end;

procedure TFmVSMovImp.BtNenhumResidualClick(Sender: TObject);
begin
  CG08_01_MovimIDResi.Value := 0;
end;

procedure TFmVSMovImp.BtTudoClick(Sender: TObject);
begin
  case PCRelatorio.ActivePageIndex of
    3: SetaTodosItens03(True);
    4: SetaTodosItens04(True);
    7: SetaTodosItens07(True);
    else Geral.MB_Erro('Item nao habilitado em "FmVSMovImp.BtTudoClick()"');
  end;
end;

procedure TFmVSMovImp.CkFichaPalletClick(Sender: TObject);
var
  ImpEmp: Integer;
begin
  ImpEmp := Geral.BoolToInt(CkFichaPallet.Checked);
  //
  Geral.WriteAppKeyLM2('Ficha_Pallet', Application.Title, ImpEmp, ktInteger);
end;

procedure TFmVSMovImp.Configura13DataIni();
var
  Habilita: Boolean;
begin
  Habilita := (RG13_GragruY.ItemIndex = 0) and (RG13_AnaliSinte.ItemIndex = 1);
  //
  Ck13DataIni.Enabled := Habilita;
  TP13DataIni.Enabled := Habilita;
  //
  Ck13DataIni.Visible := Habilita;
  TP13DataIni.Visible := Habilita;
end;

procedure TFmVSMovImp.Adicionar1Click(Sender: TObject);
begin
  VS_PF.AlterarNotFluxo(Qr13VSSeqItsControle.Value, 0);
end;

procedure TFmVSMovImp.AlteraStatusNFe1Click(Sender: TObject);
const
  TbNFs = 'vsoutnfecab';
var
  NFeStatus, MovimCod, NFe: Integer;
  Tabela, Campo: String;
  EstqMovimID: TEstqMovimID;
begin
  EstqMovimID := TEstqMovimID(Trunc(QrNFesMovimID.Value));
  NFeStatus   := UnNFe_PF.SelecionaStatusNFe(QrNFescStat.Value);
  if NFeStatus <> QrNFescStat.Value then
  begin
(*
    Tabela      := VS_PF.ObtemNomeTabelaVSXxxCab(EstqMovimID);
    case QrNFesMovimID.Value of
      emidVenda:
      begin
        if
      end;
      emidIndsXX,
      emidTransfLoc: Tabela := TbNFs;
    end;
*)
    Tabela   := QrNFesTabela.Value;
    Campo    := QrNFesCampo.Value;
    MovimCod := QrNFesMovimCod.Value;
    NFe      := QrNFesNFe.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
    'NFeStatus'], ['MovimCod', Campo], [NFeStatus], [MovimCod, NFe], True) then
    begin
      UnDmkDAC_PF.AbreQuery(QrNFes, Dmod.MyDB);
      QrNFes.Locate('MovimCod', MovimCod, []);
    end;
  end;
end;

procedure TFmVSMovImp.Argox1Click(Sender: TObject);
begin
  ImprimeFichasDePallets(True, difpTermica10x15);
end;

procedure TFmVSMovImp.AtualizaPalletsEmMontagem();
  function MontaSQL_Rcl(Coluna: String; Uniao: Boolean): String;
  begin
    Result := Geral.ATS([
    'SELECT LstPal' + Coluna + ' MontPalt, 1 Ativo ',
    'FROM ' + TMeuDB + '.vsparclcaba ',
    'WHERE LstPal' + Coluna + ' <> 0 ',
    'AND DtHrFimCla < "1900-01-01" ',
    '']);
    if Uniao then
      Result := Geral.ATS([Result, 'UNION ']);
  end;
  function MontaSQL_Cla(Coluna: String; Uniao: Boolean): String;
  begin
    Result := Geral.ATS([
    'SELECT LstPal' + Coluna + ' MontPalt, 1 Ativo ',
    'FROM ' + TMeuDB + '.vspaclacaba ',
    'WHERE LstPal' + Coluna + ' <> 0 ',
    'AND DtHrFimCla < "1900-01-01" ',
    '']);
    if Uniao then
      Result := Geral.ATS([Result, 'UNION ']);
  end;
begin

  EXIT;

  ///  Pallets em Montagem!
  FVSLstPalBox := UnCreateVS.RecriaTempTableNovo(ntrttVSLstPalBox,
    DModG.QrUpdPID1, False, 1, '_vslstpalbox_' + Self.Name);




  //EXIT;







  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO  ' + FVSLstPalBox,
  MontaSQL_Rcl('01', True),
  MontaSQL_Rcl('02', True),
  MontaSQL_Rcl('03', True),
  MontaSQL_Rcl('04', True),
  MontaSQL_Rcl('05', True),
  MontaSQL_Rcl('06', True),
  //
  MontaSQL_Cla('01', True),
  MontaSQL_Cla('02', True),
  MontaSQL_Cla('03', True),
  MontaSQL_Cla('04', True),
  MontaSQL_Cla('05', True),
  MontaSQL_Cla('06', False),
  //
  ' ']);
end;

procedure TFmVSMovImp.AtualizaPalletsFantasmas();
begin
////////////////////////////////////////////////////////////////////////////////
  EXIT;
////////////////////////////////////////////////////////////////////////////////
  FVSLstPalGhost := UnCreateVS.RecriaTempTableNovo(ntrttVSLstPalGhost,
    DModG.QrUpdPID1, False, 1, '_vslstpalghost_' + Self.Name);


  //EXIT;


  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO  ' + FVSLstPalGhost,
  'SELECT Pallet, SUM(SdoVrtPeca+LmbVrtPeca) Pecas, 1 Ativo ',
  'FROM ' + FVSMovImp1,
  'GROUP BY Pallet ',
  '']);
end;

procedure TFmVSMovImp.BrTodosResidualClick(Sender: TObject);
begin
  CG08_01_MovimIDResi.SetMaxValue;
end;

procedure TFmVSMovImp.Bt02AtzSdoVrtClick(Sender: TObject);
var
  Controle: Integer;
begin
  VS_PF.AdicionarNovosVS_emid();
  if QrVSMovIts.RecordCount > 0 then
  begin
    case TEstqMovimID(QrVSMovItsMovimID.Value) of
      //emidAjuste=0, ???
      //emidReclasWE=3,
      //emidBaixa=4,
      //emidIndsWE=5,
      //emidForcado=9,
      emidCompra(*1*),
      emidIndsXX(*6*),
      emidClassArtXXUni(*7*),
      emidReclasXXUni(*8*),
      emidClassArtXXMul(*14*),
      emidReclasXXMul(*24*),
      emidDesclasse(*28*),
      emidCaleado(*29*)(*,
      emidSPCaleirad(*42*):
      begin
        if QrVSMovItsPecas.Value > 0 then
        begin
          Controle := QrVSMovItsControle.Value;
          VS_PF.AtualizaSaldoIMEI(Controle, False);
          PesquisaLancamentos();
          QrVSMovIts.Locate('Controle', Controle, []);
        end;
      end;
      emidVenda(*2*):
      begin
        Geral.MB_Aviso('Tipo de movimento n�o permite atualiza��o!?');
      end;
     //emidSemOrigem=10,
     //emidEmOperacao=11,
     //emidResiduoReclas=12,
     //emidInventario=13,
     //14 acima
     //emidPreReclasse=15,
     //emidEntradaPlC=16,
     //emidExtraBxa=17,
     //emidSaldoAnterior=18,
     //emidEmProcWE=19,
     //emidFinished=20,
     //emidDevolucao=21,
     //emidRetrabalho=22,
     //emidGeraSubProd=23
      else Geral.MB_Aviso('Tipo de movimento n�o implementado!');
    end;
  end
  else
    Geral.MB_Erro('IME-I inv�lido!');
end;

procedure TFmVSMovImp.Bt02JanelaClick(Sender: TObject);
begin
  MostraMovimento();
end;

procedure TFmVSMovImp.Bt04GraGruXNenhumClick(Sender: TObject);
begin
  SetaTodosItensGraGruX04(False);
end;

procedure TFmVSMovImp.Bt04GraGruXTodosClick(Sender: TObject);
begin
  SetaTodosItensGraGruX04(True);
end;

procedure TFmVSMovImp.Bt08_01_NenhumClick(Sender: TObject);
begin
  CG08_01_MovimIDSign.Value := 0;
end;

procedure TFmVSMovImp.Bt08_01_TodosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PM08_01_Todos, Bt08_01_Todos);
end;

procedure TFmVSMovImp.BtDesfazOrdenacaoClick(Sender: TObject);
begin
  case PCRelatorio.ActivePageIndex of
    20: UnDmkDAC_PF.DesfazOrdenacaoDmkDBGrid(TDmkDBGrid(DBGNFes), 'MovimCod');
    //
    else Geral.MB_Info('Grade n�o implementada!');
  end;
end;

procedure TFmVSMovImp.BtOKClick(Sender: TObject);
begin
  case PCRelatorio.ActivePageIndex of
    0: ImprimeEstoqueReal();
    1: ImprimeHistorico();
    2: PesquisaLancamentos();
    3: PesquisaIMEIs();
    4:
    begin
      case PC04_Pallets.ActivePageIndex of
        0: PesquisaPallets();
        1: PesquisaBlendVS();
      end;
    end;
    5: PesquisaMartelos();
    6: PesquisaTamanhos();
    7: PesquisaPrevia();
    8:
    begin
      case PC08.ActivePageIndex of
        0: PesquisaFaltaDeCouros();
        1: PesquisaMovimDeDifer();
      end;
    end;
    10: MyObjects.MostraPopupDeBotao(PM10Imprime, BtOK);
    11: ImprimeEstoquePorGrupo();
    12: VS_PF.MostraRelatorioVSImpCompraVenda2();
    13: ImprimeFluxo();
    14: ImprimeNotaMPAG();
    15: ImprimeClassesGeradasVariosIMEIs();
    16: ImprimeFatorAParaFatorB();
    17: ImprimeRequisicaoDeMovimentacao();
    18: ImprimeOrdens();
    19: ImprimeDiario();
    20: ImprimeListaNFesEmitidasVS();
    21: ImprimeRendimentoSemi();
    22: ImprimeComparaPallet();
    23: ImprimeNFRetMOIndef();
    24: VS_PF.MostraFormVSImpMOEnvRet();
    25: ImprimeClasificacoesInconsistentes();
    26: ImprimeEstoqueCustoIntegrado();
    //
    else Geral.MB_Erro('Relat�rio n�o implementado!');
  end;
end;

procedure TFmVSMovImp.BtSaidaClick(Sender: TObject);
begin
  if TFmVSMovImp(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmVSMovImp.DBG04EstqDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
var
  CorTexto, CorFundo: TColor;
  Texto: String;
begin
  if (Column.FieldName = 'NO_PalStat') then
  begin
    case Trunc(QrEstqR4PalStat.Value) of
      1: Cor := clGreen;    // Montando
      2: Cor := clRed;      // Desmontando
      4: Cor := clBlue;     // Encerrado
      8: Cor := $002267F2;  // Removido - Laranja
      //3:  Remo + Desmo   $0075479D Roxo?
      else Cor := clFuchsia;
    end;
    with DBG04Estq.Canvas do
    begin
      if Cor = clBlack then Font.Style := [] else Font.Style := [fsBold];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end else
  if Column.FieldName = 'FaixaMediaM2' then
  begin
    VS_CRC_PF.DadosDaFaixaDaMediaM2DoCouro(QrEstqR4FaixaMediaM2.Value,
    CorFundo, CorTexto, Texto);
    MyObjects.DesenhaTextoEmDBGrid(TDBGrid(DBG04Estq), Rect, CorTexto, CorFundo,
      Column.Alignment, Texto(*Column.Field.DisplayText*));
  end;
end;

procedure TFmVSMovImp.DBG23VSMORetDblClick(Sender: TObject);
var
  MovimCod, Codigo, Controle, MovimID: Integer;
begin
  MovimCod := Qr23VSMORetVSVMI_MovimCod.Value;
  if MovimCod <> 0 then
  begin
    Codigo   := VS_PF.ObtemCodigoDeMovimCod(MovimCod);
    Controle := Qr23VSMORetVSVMI_Controle.Value;
    MovimID  := Qr23VSMORetVSVMI_MovimID.Value;
    VS_PF.MostraFormVS_XXX(MovimID, Codigo, Controle);
  end else
    Geral.MB_Erro('MovimCod indefinido!');
end;

function TFmVSMovImp.DefineMyArr03(): Boolean;
var
  N, I: Integer;
begin
  N := 0;
  with DBG03Estq.DataSource.DataSet do
  for I := 0 to DBG03Estq.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DBG03Estq.SelectedRows.Items[I]));
    GotoBookmark(DBG03Estq.SelectedRows.Items[I]);
    N := N + 1;
    SetLength(MyArr03, N);
    MyArr03[N - 1] := QrEstqR3Controle.Value;
  end;
  Result := N > 0;
  if not Result then
    Geral.MB_Aviso('Nenhum IME-I foi selecionado!');
end;

function TFmVSMovImp.DefineSQLMovimID(const Obrigatorio: Boolean;
  const EdMovimID: TdmkEdit; var SQL: String): Boolean;
var
  MovimID: Integer;
begin
  Result := False;
  SQL := '';
  MovimID := EdMovimID.ValueVariant;
  if MyObjects.FIC((MovimID < 0) and Obrigatorio, EdMovimID,
  'Informe o ID do movimento!') then
    Exit;
  if MovimID > -1 then
    SQL := 'AND vmi.MovimID=' + Geral.FF0(MovimID);
  Result := True;
end;

procedure TFmVSMovImp.DG02VSMovItsDblClick(Sender: TObject);
begin
  MostraMovimento();
end;

procedure TFmVSMovImp.DBGNFesDblClick(Sender: TObject);
begin
  JanelaMovimentoID();
end;

procedure TFmVSMovImp.DBGNFesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  DmNFe_0000.ColoreStatusNFeEmDBGrid(TDBGrid(DBGNFes), Rect, DataCol, Column,
  State, Trunc(QrNFescStat.Value), 55);
end;

procedure TFmVSMovImp.Ed00NomeGG1Change(Sender: TObject);
begin
  Reopen00GraGruX();
end;

procedure TFmVSMovImp.Ed04GraGruXChange(Sender: TObject);
begin
  QrEstqR4.Close;
end;

procedure TFmVSMovImp.Ed04NomeGG1Change(Sender: TObject);
begin
  VS_PF.ReopenGraGruX_A(Qr04GraGruX, Ed04NomeGG1.Text);
end;

procedure TFmVSMovImp.Ed07PallIniExit(Sender: TObject);
begin
  if Ed07PallIni.ValueVariant > Ed07PallFim.ValueVariant then
    Ed07PallFim.ValueVariant := Ed07PallIni.ValueVariant;
end;

procedure TFmVSMovImp.Ed20NFeIniRedefinido(Sender: TObject);
begin
  if Ed20NFeFim.ValueVariant < Ed20NFeIni.ValueVariant then
    Ed20NFeFim.ValueVariant := Ed20NFeIni.ValueVariant;
end;

procedure TFmVSMovImp.Entradas1Click(Sender: TObject);
begin
  CG08_01_MovimIDSign.Value := 1 + 4 + 16 + 64 + 256;
end;

procedure TFmVSMovImp.Etiqueta1Click(Sender: TObject);
const
  LPFMO   = '';
  FNFeRem = '';
begin
  if DefineMyArr03() then
    VS_PF.ImprimeIMEI(MyArr03, viikArtigoGerado, LPFMO, FNFeRem, nil);
end;

procedure TFmVSMovImp.Etiquetas8porpgina1Click(Sender: TObject);
begin
  ImprimeEtiquetasDePallets(True, 8);
end;

procedure TFmVSMovImp.Fichas2Click(Sender: TObject);
var
  I, N, Empresa: Integer;
  MyArr: array of Integer;
begin
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  N := 0;
  with DBG07Estq.DataSource.DataSet do
  for I := 0 to DBG07Estq.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DBG07Estq.SelectedRows.Items[I]));
    GotoBookmark(DBG07Estq.SelectedRows.Items[I]);
    N := N + 1;
    SetLength(MyArr, N);
    MyArr[N - 1] := Qr07PrevPalVSPallet.Value;
  end;
  if N > 0 then
    VS_PF.ImprimePrevia(Empresa, MyArr)
  else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmVSMovImp.FichasCOMonomedoPallet1Click(Sender: TObject);
begin
  ImprimeFichasDePallets(True, difpPapelA4);
end;

procedure TFmVSMovImp.FichasSEMonomedoPallet1Click(Sender: TObject);
begin
  ImprimeFichasDePallets(False, difpPapelA4);
end;

procedure TFmVSMovImp.FormActivate(Sender: TObject);
begin
  if TFmVSMovImp(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmVSMovImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  PCRelatorio.ActivePageIndex := 0;
  Application.ProcessMessages;
  PCRelatorio.ActivePageIndex := 4;
  //
  PC04_Pallets.ActivePageIndex := 0;
  PC08.ActivePageIndex := 0;
  //
(*
  VS_PF.ReopenGraGruX_A(Qr02GraGruX, '');
  VS_PF.ReopenGraGruX_A(Qr04GraGruX, '');
  VS_PF.ReopenGraGruX_A(Qr05GraGruX, '');
  //
  VS_PF.ReopenPallet_A(Qr02VSPallet);
  VS_PF.ReopenPallet_A(Qr05VSPallet);
  //
  UnDmkDAC_PF.AbreQuery(Qr02Fornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(Qr05Fornecedor, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrVSMrtCad, Dmod.MyDB);
*)
  UnDmkDAC_PF.AbreQuery(Qr04StqCenCad, Dmod.MyDB);
  //
  TPDataRelativa.Date := Date;
  //
  TP00EstoqueEm.Date := Geral.PrimeiroDiaDoMes(date()) - 1;
  //
  TP02DataIni.Date := Date - 90;
  TP02DataFim.Date := Date;
  //
(*
  TP05DataIni.Date := Date - 30;
  TP05DataFim.Date := Date;
*)
  //
  TP06DataIni.Date := Date - 30;
  TP06DataFim.Date := Date;
  //
  TP08DataIni.Date := Geral.PrimeiroDiaDoMes(date());
  //TP08DataIni.Date := Date - 30;
  TP08DataFim.Date := Date;
  //
  TP13DataIni.Date := Geral.PrimeiroDiaDoMes(date());
  TP13DataFim.Date := Date;
  //
  TP14DataIni.Date := Date - 90;
  TP14DataFim.Date := Date;
  //
  TP16DataIni.Date := Geral.PrimeiroDiaDoMes(date());
  TP16DataFim.Date := Date;
  //
  TP17DataIni.Date := Geral.PrimeiroDiaDoMes(date());
  TP17DataFim.Date := Date;
  //
  PCRelatorioChange(Self);
  //
  CG08_01_MovimIDSign.SetMaxValue;
  CG18Ordens.SetMaxValue;
  //
  TP20DataIni.Date := Date - 120;
  TP20DataFim.Date := Date;
  //
  TP21DataIni.Date := IncMonth(Date, -6) + 1;
  TP21DataFim.Date := Date;
  MyObjects.ConfiguraCheckGroup(CG21Bastidao, sVSBastidao, 4, High(Integer), False);
  //
  CkFichaPallet.Checked := Geral.IntToBool(Geral.ReadAppKeyLM('Ficha_Pallet', Application.Title, ktInteger, 1));
  //
  CG20VmcWrn.Value := CG20VmcWrn.MaxValue;
  CG20VmcSta.Value := 2; // Ignorar
  //
  MyObjects.PreencheComponente(RG00_Ordem1, sMax_VS_IMP_ESTQ_ORD, 1);
  MyObjects.PreencheComponente(RG00_Ordem2, sMax_VS_IMP_ESTQ_ORD, 1);
  MyObjects.PreencheComponente(RG00_Ordem3, sMax_VS_IMP_ESTQ_ORD, 1);
  MyObjects.PreencheComponente(RG00_Ordem4, sMax_VS_IMP_ESTQ_ORD, 1);
  MyObjects.PreencheComponente(RG00_Ordem5, sMax_VS_IMP_ESTQ_ORD, 1);
  RG00_Ordem1.ItemIndex := 5;
  RG00_Ordem2.ItemIndex := 8;
  RG00_Ordem3.ItemIndex := 0;
  RG00_Ordem4.ItemIndex := 2;
  RG00_Ordem5.ItemIndex := 1;
  //
  Ed04StqCenCad.ValueVariant := Dmod.QrControleStqCenCadEstqPall.Value;
  CB04StqCenCad.KeyValue     := Dmod.QrControleStqCenCadEstqPall.Value;
end;

procedure TFmVSMovImp.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Prompts, Values: array of string;
begin
  if Shift = [ssCtrl] then
  begin
    if dmkPF.EhControlF(Sender, Key, Shift) then
    begin
      if PCRelatorio.ActivePageIndex = 20 then
      begin
        SetLength(Prompts, 2);
        Prompts[0] := 'Informe a NFe:';
        Prompts[1] := 'Informe a s�rie:';
        SetLength(Values, 2);
        Values[0] := '';
        Values[1] := '1';
        if InputQuery('Pesquisa de Nota', Prompts, Values) then
        begin
          if Values[0] <> '' then
            QrNFes.Locate('Serie; NFe', VarArrayOf([Values[1], Values[0]]), [])
          else
            QrNFes.Locate('NFe', Values[0], [])
        end;
      end;
    end;
  end;
end;

procedure TFmVSMovImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSMovImp.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmVSMovImp.frxWET_CURTI_018_20GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt('', CBEmpresa.Text, EdEmpresa.ValueVariant)
end;

procedure TFmVSMovImp.frxWET_CURTI_018_XX_AGetValue(const VarName: string;
  var Value: Variant);
var
  QtdCouros: Double;
  SumNota, QtdNota, Nota, Percent: Double;
  Sigla, NotaTxt, MaxDtHr_TXT: String;
  Invalido: Boolean;
  MaxDataHora: TDateTime;
begin
  if VarName = 'VARF_EMPRESA' then
    Value := dmkPF.ParValueCodTxt(
      'Empresa: ', CBEmpresa.Text, EdEmpresa.ValueVariant, 'TODAS')
(*
  else
  if VarName = 'VARF_NO_STQCENCAD' then
    Value := dmkPF.ParValueCodTxt('Centro de estoque: ',
    CB00StqCenCad.Text, Ed00StqCenCad.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_NO_FORNECE' then
    Value := dmkPF.ParValueCodTxt('Fornecedor: ',
    CB00Terceiro.Text, Ed00Terceiro.ValueVariant, 'TODOS')
*)
  else
  if VarName ='VARF_DATA' then
    Value := Now()
  else
  if VarName ='VARF_DATA_MEU' then
    Value := TPDataRelativa.Date
  else
  if VarName ='VARF_DATA_IMP' then
    Value := Now()
  else
  if VarName ='VARF_VSNIVGER' then
    Value := '00000' //Dmod.QrControleVSNivGer.Value    <=== Nao usa! Tirar!
(*
  else
  if VarName = 'VARF_TERCEIRO_1' then
    Value := dmkPF.ParValueCodTxt(
      '', CB01Terceiro.Text, Ed01Terceiro.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_PALLET_1' then
    Value := dmkPF.ParValueCodTxt(
      '', CB01Pallet.Text, Ed01Pallet.ValueVariant, 'TODOS')
  else
  if VarName = 'VARF_GRAGRUX_1' then
    Value := dmkPF.ParValueCodTxt(
      '', CB01GraGruX.Text, Ed01GraGruX.ValueVariant, 'TODAS')
  else
  if VarName = 'VARF_PERIODO_1' then
    Value := dmkPF.PeriodoImp1(TP01DataIni.Date, TP01DataFim.Date,
    Ck01DataIni.Checked, Ck01DataFim.Checked, '', 'at�', '')
*)
  else
  if VarName = 'VARF_AGRUP1' then
    Value := RG04Agrupa.ItemIndex >= 1
  else
  if VarName = 'VARF_AGRUP2' then
    Value := RG04Agrupa.ItemIndex >= 2
  else
  if VarName = 'VARF_NotaVS' then
  begin
    case FNotaRevisor of
      1: // Nota
      begin
        Invalido := False;
        UnDmkDAC_PF.AbreMySQLQuery0(QrNotaVS, DModG.MyPID_DB, [
        'SELECT vem.Codigo, vem.NotaVS, vem.SiglaVS,  ',
        'vmi.Terceiro, SUM(vmi.Pecas) Pecas  ',
        'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
        'LEFT JOIN ' + TMeuDB + '.vsentimp vem ON vem.Codigo=vmi.Terceiro ',
        'WHERE Pecas > 0 ',
        'AND Pallet=' + Geral.FF0(QrGGXPalTerPallet.Value),
        'AND Pallet IN ( ',
        '  SELECT Pallet  ',
        '  FROM _vsmovimp4_fmvsmovimp ',
        ') ',
        'GROUP BY vmi.Terceiro ',
        '']);
        Value := '';
        QrNotaVS.First;
        while not QrNotaVS.Eof do
        begin
          if (QrNotaVSCodigo.Value = 0)  and (QrNotaVSTerceiro.Value <> 0) then
            Invalido := True;
          QtdCouros := QtdCouros + QrNotaVSPecas.Value;
          if QrNotaVSTerceiro.Value <> 0 then
          begin
            QtdNota := QtdNota + QrNotaVSPecas.Value;
            SumNota := SumNota + (QrNotaVSPecas.Value * QrNotaVSNotaVS.Value);
          end;
          //
          QrNotaVS.Next;
        end;
        QrNotaVS.First;
        while not QrNotaVS.Eof do
        begin
          if Trim(QrNotaVSSiglaVS.Value) = '' then
          begin
            if QrNotaVSTerceiro.Value = 0 then
              Sigla := '**Misturado**'
            else
              Sigla := 'Fornecedor ' + Geral.FF0(QrNotaVSTerceiro.Value);
          end else
            Sigla := QrNotaVSSiglaVS.Value;
          if QtdCouros > 0 then
            Percent := QrNotaVSPecas.Value / QtdCouros * 100
          else
            Percent := 0;
          Value := Value + Sigla + ' (' + Geral.FFT(Percent, 2, siNegativo) + '%)   ';
          //
          QrNotaVS.Next;
        end;
        if Invalido then
          NotaTxt := '[F/I]'
        else
        begin
          if QtdNota > 0 then
            Nota := SumNota / QtdNota
          else
            Nota := 0;
          NotaTxt := Geral.FFT(Nota, 2, siNegativo);
        end;
        Value := 'Nota: ' + NotaTxt + '  |    ' + Value;
      end;
      2: // Revisor
      begin
        MaxDataHora := 0;
        MaxDtHr_TXT := '';
        UnDmkDAC_PF.AbreMySQLQuery0(QrRevisores, DMod.MyDB, [
        'SELECT Revisor, SUM(Pecas) Pecas, MAX(DataHora) DataHora, ',
        'DATE_FORMAT(MAX(DataHora), "%d/%m/%Y %H:%i:%s" ) DataHora_TXT, ',
        'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_REVISOR ',
        'FROM vscacitsa cia ',
        'LEFT JOIN entidades ent ON ent.Codigo=cia.Revisor  ',
        'WHERE VSPallet=' + Geral.FF0(QrGGXPalTerPallet.Value),
        'GROUP BY Revisor ',
        '']);
        QrRevisores.First;
        while not QrRevisores.Eof do
        begin
          QtdCouros := QtdCouros + QrRevisoresPecas.Value;
          if QrRevisoresDataHora.Value > MaxDataHora then
          begin
            MaxDataHora := QrRevisoresDataHora.Value;
            MaxDtHr_TXT := QrRevisoresDataHora_TXT.Value;
          end;
          //
          QrRevisores.Next;
        end;
        QrRevisores.First;
        while not QrRevisores.Eof do
        begin
          if QtdCouros > 0 then
            Percent := QrRevisoresPecas.Value / QtdCouros * 100
          else
            Percent := 0;
          Value := Value + QrRevisoresNO_REVISOR.Value + ' > ' +
            FloatToStr(QrRevisoresPecas.Value) + ' p� (' +
            Geral.FFT(Percent, 2, siNegativo) + '% Fim ' +
            QrRevisoresDataHora_TXT.Value + ')   ';
          //
          QrRevisores.Next;
        end;
        Value := 'Fim: ' + MaxDtHr_TXT + '  |    ' + Value;
      end;
      else
        Value := '? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ? ';
    end;
  end
end;

procedure TFmVSMovImp.Horizontal1Click(Sender: TObject);
begin
  ImprimePackLists(False, FVSMovImp4, FVSLstPalBox, TGrandezaArea.grandareaAreaM2);
end;

procedure TFmVSMovImp.Horizontallinhasft1Click(Sender: TObject);
begin
  ImprimePackLists(False, FVSMovImp4, FVSLstPalBox, TGrandezaArea.grandareaAreaP2);
end;

procedure TFmVSMovImp.ImprimeClasificacoesInconsistentes();
begin
  Geral.MB_Info('Em desenvolvimento!')
(*
SELECT *
FROM ' + CO_SEL_TAB_VMI + '
WHERE MovimID=15
AND
(SdoVrtPeca <> 0
OR SdoVrtPeso <> 0
OR SdoVrtArM2 <> 0)
*)

end;

procedure TFmVSMovImp.ImprimeClassesGeradasVariosIMEIs();
var
  Txt: String;
  IMEIs: array of Integer;
  I, N, K, IMEI: Integer;
begin
  K := 0;
  N := Me15ListaIMEIs.Lines.Count;
  //SetLength(IMEIs, N);
  for I := 0 to N - 1 do
  begin
    Txt := Trim(Me15ListaIMEIs.Lines[I]);
    IMEI := Geral.IMV(Txt);
    if IMEI > 0 then
    begin
      K := K + 1;
      SetLength(IMEIs, K);
      IMEIs[K - 1] := IMEI;
    end else
      if Txt <> '' then
        Geral.MB_Aviso('O texto "' + Me15ListaIMEIs.Lines[I] +
        '" n�o � um n�mero de IME-I v�lido e n�o ser� pesquisado!');
  end;
  VS_PF.ImprimeClassIMEIs(IMEIs, [1], True);
end;

procedure TFmVSMovImp.ImprimeComparaPallet();
begin
  VS_PF.ImprimeComparaCacIts(Ed22PalletA.ValueVariant, Ed22PalletB.ValueVariant);
end;

procedure TFmVSMovImp.ImprimeDiario();
  function CriarTabelaVMI19(Nome: String): String;
  begin
    Result := UnCreateVS.RecriaTempTableNovo(ntrttVSMovIts,
      DModG.QrUpdPID1, False, 1, Nome);
  end;
begin
  FVMI_019_Sorc := CriarTabelaVMI19('_vmi_19_sorc_');
  FVMI_019_Dest := CriarTabelaVMI19('_vmi_19_dest_');
  FVMI_019_Main := CriarTabelaVMI19('_vmi_19_main_');
  //
(*
  /
SELECT DISTINCT MovimCod
FROM ' + CO_SEL_TAB_VMI + '
WHERE DataHora BETWEEN "2016-03-21" AND "2016-03-25"

UNION

SELECT DISTINCT MovimCod
FROM ' + CO_SEL_TAB_VMI + '
WHERE Controle IN (
  SELECT VMI_Dest
  FROM vscacitsa
  WHERE DataHora BETWEEN "2016-03-21" AND "2016-03-25"
)
*)
end;

procedure TFmVSMovImp.ImprimeEstoquePorGrupo();
var
  SQL_Empresa, ATT_StatPall: String;
  //
  procedure InsereGrupoAtual(SQL_GraGruY, GroupBy: String; Grupo: Integer;
  PorIMEI: Boolean);
  const
    FldSdoVrtArM2 = 'vmi.SdoVrtArM2';
    FldSdoVrtPeso = 'vmi.SdoVrtPeso';
    FldSdoVrtPeca = 'vmi.SdoVrtPeca';
  var
    ATT_MovimID, ATT_MovimNiv, SQL_IMEI: String;
  begin
    if PorIMEI then
    begin
      ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
      sEstqMovimID);
      //
      ATT_MovimNiv := dmkPF.ArrayToTexto('vmi.MovimNiv', 'NO_MovimNiv', pvPos, True,
      sEstqMovimNiv);
      //
      SQL_IMEI := Geral.ATS([
      'vmi.Codigo, vmi.MovimCod IMEC, vmi.Controle IMEI, ',
      'vmi.MovimID, vmi.MovimNiv, ',
      ATT_MovimID, ATT_MovimNiv ]);
    end else
    begin
      SQL_IMEI := Geral.ATS([
      'vmi.Codigo, vmi.MovimCod IMEC, vmi.Controle IMEI, ',
      'vmi.MovimID, vmi.MovimNiv, "" NO_MovimID, "" NO_MovimNiv, ']);
    end;
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(QrEstqR1, DModG.MyPID_DB, [
    'INSERT INTO ' + FVSMovImp1 + '',
    'SELECT vmi.Empresa, IF(vmi.Pallet <> 0, vsp.GraGruX, vmi.GraGruX) GraGruX, SUM(vmi.SdoVrtPeca) Pecas,  ',
    'SUM(vmi.PesoKg) PesoKg, SUM(vmi.AreaM2) AreaM2,  ',
    'SUM(FLOOR((vmi.AreaM2 / 0.09290304)) + ',
    'FLOOR(((MOD((vmi.AreaM2 / 0.09290304), 1)) + ',
    '0.12499) * 4) * 0.25) AreaP2, 0.00 ValorT,  ',
    'SUM(vmi.SdoVrtPeca) SdoVrtPeca, SUM(vmi.SdoVrtPeso) SdoVrtPeso, ',
    'SUM(vmi.SdoVrtArM2) SdoVrtArM2, ',
    '0.000 LmbVrtPeca, 0.000 LmbVrtPeso, 0.000 LmbVrtArM2, ',
    'ggx.GraGru1, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, vmi.Pallet, vsp.Nome NO_Pallet, ',
    'vmi.Terceiro, ',
    'IF(vsp.CliStat IS NULL, 0, vsp.CliStat), ',
    'IF(vsp.Status IS NULL, 0, vsp.Status), ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, ',
    'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",   ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,  ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
    'vps.Nome NO_STATUS, ',
    '"0000-00-00 00:00:00" DataHora, 0 OrdGGX, ',
    'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
    '0 PalStat, ',
    'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
    'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
    'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
    'SUM(vmi.SdoVrtPeca * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) SdoInteiros, ',
    '0 LmbInteiros, ',
    SQL_IMEI,
    'vmi.SerieFch, vsf.Nome NO_SerieFch, vmi.Ficha, vmi.Marca, ',
    'IF(vsp.StatPall IS NULL, 0, vsp.StatPall) PalStat, ',
    ATT_StatPall,
    'vmi.ReqMovEstq, ',
    'IF(scc.Codigo IS NULL, 0, scc.Codigo) StqCenCad, ',
    'IF(scc.Codigo IS NULL, "Local indefinido", scc.Nome) NO_StqCenCad, ',
    'vmi.StqCenLoc, CONCAT(scl.Nome, " (", scc.Nome, ")") NO_LOC_CEN, ',
    '"" Historico, vmi.VSMulFrnCab, ', // Ver o que fazer (2015-10-01)
    'IF(vmi.Terceiro <>0, vmi.Terceiro, vmi.VSMulFrnCab) MulFornece, ',
    'vmi.ClientMO, ',
    'IF(vmi.Pallet <> 0, CONCAT("PAL ", vmi.Pallet), ',
    '   CONCAT("IME-I ", vmi.Controle)) ID_UNQ, ',
    //
    VS_PF.SQL_TipoEstq_DefinirCodi(False, 'Grandeza', True,
    FldSdoVrtArM2, FldSdoVrtPeso, FldSdoVrtPeca),
    //
    'vmi.NFeSer, vmi.NFeNum, vmi.VSMulNFeCab, ',
    Geral.FF0(Grupo) + ' Ativo ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=vmi.Pallet   ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=IF(vmi.Pallet <> 0, vsp.GraGruX, vmi.GraGruX) ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle  ',
    'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vmi.Terceiro ',
    'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vmi.Empresa ',
    'LEFT JOIN ' + TMeuDB + '.vspalsta   vps ON vps.Codigo=vsp.Status  ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
    'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
    'LEFT JOIN ' + TMeuDB + '.vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
    'LEFT JOIN ' + TMeuDB + '.stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
    'LEFT JOIN ' + TMeuDB + '.stqcencad  scc ON scc.Codigo=scl.Codigo ',
    'LEFT JOIN ' + TMeuDB + '.unidmed    med ON med.Codigo=gg1.UnidMed',
    'WHERE vmi.Controle <> 0 ',
    'AND vmi.GraGruX<>0 ',
    'AND (vmi.SdoVrtPeca > 0 OR (SdoVrtPeso > 0 AND (vmi.Pecas=0 OR MovimID IN (' +
    CO_ALL_CODS_ESTQ_SEM_PC + '))))',
    SQL_Empresa,
    //SQL_DtHr,
    SQL_GraGruY,
    GroupBy + ', GraGruX', // <<< Calcular inteiros!
    '']);
  end;
  procedure AbreQuery(Group, Ordem: String; Qry: TmySQLQuery; Grupo: Integer);
  var
    Tabela: String;
  begin
    Tabela := '_TESTE_' + Geral.FF0(Grupo);
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DModG.MyPID_DB, [
    //'DROP TABLE IF EXISTS ' + Tabela + ';',
    //'CREATE TABLE ' + Tabela,
    'SELECT mi1.Empresa, mi1.GraGruX, SUM(mi1.Pecas) Pecas, SUM(mi1.PesoKg) PesoKg, ',
    'Sum(mi1.AreaM2) AreaM2, SUM(mi1.AreaP2) AreaP2, SUM(mi1.ValorT) ValorT, ',
    'SUM(mi1.SdoVrtPeca) SdoVrtPeca, SUM(mi1.SdoVrtPeso) SdoVrtPeso, ',
    'Sum(mi1.SdoVrtArM2) SdoVrtArM2, ',
    //
    'SUM(mi1.SdoVrtPeca+mi1.LmbVrtPeca) PalVrtPeca, ',
    'SUM(mi1.SdoVrtPeso+mi1.LmbVrtPeso) PalVrtPeso, ',
    'SUM(mi1.LmbVrtArM2+mi1.SdoVrtArM2) PalVrtArM2, ',
    //
    'SUM(mi1.LmbVrtPeca) LmbVrtPeca, ',
    'SUM(mi1.LmbVrtPeso) LmbVrtPeso, SUM(mi1.LmbVrtArM2) LmbVrtArM2, ',
    'mi1.GraGru1, mi1.NO_PRD_TAM_COR, mi1.Pallet, mi1.NO_PALLET, mi1.Terceiro, ',
    'mi1.CliStat, mi1.Status, mi1.NO_FORNECE,  mi1.NO_CLISTAT, mi1.NO_EMPRESA, ',
    'mi1.NO_STATUS, mi1.DataHora, mi1.OrdGGX, mi1.OrdGGY, mi1.GraGruY, ',
    'mi1.NO_GGY, StatPall PalStat, NO_StatPall NO_PalStat, ',
    'IF(SUM(mi1.SdoVrtPeca+mi1.LmbVrtPeca)  <> 0, ',
    'SUM(mi1.LmbVrtArM2+mi1.SdoVrtArM2) / SUM(mi1.SdoVrtPeca+mi1.LmbVrtPeca), 0) ',
    'Media, ',
    'mi1.NO_MovimNiv, mi1.NO_MovimID, mi1.MovimNiv, ',
    'mi1.MovimID, mi1.IMEC, mi1.Codigo, mi1.IMEI, ',
    'mi1.SerieFch, mi1.NO_SerieFch, mi1.Ficha, mi1.Marca, ',
    'SUM(mi1.SdoInteiros + mi1.LmbInteiros) Inteiros, ',
    'mi1.Ativo ',
    'FROM  ' + FVSMovImp1 + ' mi1 ',
    'WHERE (SdoVrtPeca > 0 OR LmbVrtPeca <> 0 OR (SdoVrtPeso > 0 AND (Pecas=0 OR MovimID IN ('
    + CO_ALL_CODS_ESTQ_SEM_PC +')))) ',
    Geral.ATS_IF(Grupo <> 0, ['AND Ativo=' + Geral.FF0(Grupo)]),
    //GroupBy,

    //'; ',
    //'SELECT * FROM ' + Tabela,
    //'WHERE PalVrtPeca > 0',
    Group,
    Ordem,
    '']);
    //Geral.MB_SQL(Self, Qry);
  end;
const
  Ordens: array[0..4] of String = ('NO_PRD_TAM_COR', 'NO_FORNECE', 'Pallet', 'IMEI', 'Ficha, NO_SerieFch');
  Agrups: array[0..4] of String = ('GraGruX', 'Terceiro', 'Pallet', 'IMEI', 'Ficha');
  GraGruX = 0;
  CouNiv2 = 0;
  CouNiv1 = 0;
var
  //I, Index,
  Empresa, Grupo: Integer;
  //SQL_DtHr, GraGruYs,
  //SQL_ZeroNegat, Ordem,
  GroupBy, SQL_GraGruY, SQL_GraGruX, Group, Ordem: String;
  PorIMEI: Boolean;
{
  Grupo1, Grupo2: TfrxGroupHeader;
  Futer1, Futer2: TfrxGroupFooter;
  Me_GH1, Me_FT1, Me_GH2, Me_FT2, MeValNome, MeTitNome, MeTitCodi, MeValCodi: TfrxMemoView;
  : String;
}
begin
  if VS_PF.VerificaGraGruXCouSemNivel1() > 0 then
    Exit;
  FVSMovImp1 :=
    UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp1, DModG.QrUpdPID1, False);
(*
  FVSMovImp1_View := UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp1,
    DModG.QrUpdPID1, False, 1, '_vsmovimp1_view');
*)
  //
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  if Empresa <> 0 then
    SQL_Empresa := 'AND vmi.Empresa=' + Geral.FF0(Empresa)
  else
    SQL_Empresa := '';
  //
  ATT_StatPall := dmkPF.ArrayToTexto('vsp.StatPall', 'NO_StatPall', pvPos, True,
  sVSStatPall);

  UnDmkDAC_PF.ExecutaMySQLQuery0(QrEstqR1, DModG.MyPID_DB, [
  'DELETE FROM ' + FVSMovImp1 + ';',
  '']);
  // 1024 In Natura
  SQL_GraGruY := 'AND ggx.GraGruY = 1024';
  GroupBy     := 'GROUP BY GraGruY, Ficha, GraGruX';
  PorIMEI     := False;
  Grupo       := 1;
  InsereGrupoAtual(SQL_GraGruY, GroupBy, Grupo, PorIMEI);
  //
  // 2048 Couro curtido, para classificar
  //SQL_GraGruY := 'AND ggx.GraGruY = 2048 AND Pallet = 0 ';
  SQL_GraGruY := 'AND (ggx.GraGruY = 2048 AND Pallet = 0) ';
  GroupBy     := 'GROUP BY GraGruY, IMEI, GraGruX';
  PorIMEI     := True;
  Grupo       := 2;
  InsereGrupoAtual(SQL_GraGruY, GroupBy, Grupo, PorIMEI);
  //
  // 3072 Couro Classificado
  SQL_GraGruY := 'AND (ggx.GraGruY = 3072 OR (ggx.GraGruY = 2048 AND Pallet <> 0))';
  GroupBy     := 'GROUP BY GraGruY, Pallet, GraGruX';
  PorIMEI     := False;
  Grupo       := 3;
  InsereGrupoAtual(SQL_GraGruY, GroupBy, Grupo, PorIMEI);
  //
  // 4048 Couro em  Operacao
  SQL_GraGruY := 'AND ggx.GraGruY = 4096';
  GroupBy     := 'GROUP BY GraGruY, GraGruX, IMEI, GraGruX';
  PorIMEI     := True;
  Grupo       := 4;
  InsereGrupoAtual(SQL_GraGruY, GroupBy, Grupo, PorIMEI);
  //
  //
  Group := 'GROUP BY Terceiro, Ficha, SerieFch ';
  Ordem := 'ORDER BY Terceiro, Ficha, SerieFch ';
  AbreQuery(Group, Ordem, Qr11_1, 1);
  //
  Group := 'GROUP BY GraGruX, IMEI ';
  Ordem := 'ORDER BY NO_PRD_TAM_COR, GraGruX, IMEI ';
  AbreQuery(Group, Ordem, Qr11_2, 2);
  //
  Group := 'GROUP BY GraGruX, Pallet';
  Ordem := 'ORDER BY NO_PRD_TAM_COR, GraGruX, Pallet ';
  AbreQuery(Group, Ordem, Qr11_3, 3);
  //
  Group := 'GROUP BY GraGruX, Codigo ';
  Ordem := 'ORDER BY NO_PRD_TAM_COR, GraGruX, Codigo ';
  AbreQuery(Group, Ordem, Qr11_4, 4);
  //
  Group := '';
  Ordem := '';
  AbreQuery(Group, Ordem, Qr11_T, 0);
  //
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_018_11_A, [
    DModG.frxDsDono,
    frxDs11_1,
    frxDs11_2,
    frxDs11_3,
    frxDs11_4,
    frxDs11_T
  ]);
  MyObjects.frxMostra(frxWET_CURTI_018_11_A, 'Estoque VS');
end;

procedure TFmVSMovImp.ImprimeEstoqueReal();
const
  DataRetroativa = '';
  MostraFrx = True;
var
  Entidade, Filial, ItemAgruNoItm: Integer;
  TableSrc: String;
  DescrAgruNoItm: Boolean;
begin
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Entidade);
  if MyObjects.FIC(Entidade = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  if MyObjects.FIC(RG00Relatorio.ItemIndex = 0, RG00Relatorio,
  'Informe a forma de apresenta��o do " Relat�rio de Estoque: "') then
    Exit;
  //////////////////////////////////////////////////////////////////////////////
  Filial := EdEmpresa.ValueVariant;
  DescrAgruNoItm := RG00DescrAgruNoItm.ItemIndex = 2;
  if not DescrAgruNoItm then
  begin
    ItemAgruNoItm := -1;
    ItemAgruNoItm := MyObjects.SelRadioGroup('Descri��o do item',
      'Selecione a forma de "Descri��o do item"',
      Geral.TSTA(RG00DescrAgruNoItm.Items), 1, -1);
    RG00DescrAgruNoItm.ItemIndex := ItemAgruNoItm;
    DescrAgruNoItm := RG00DescrAgruNoItm.ItemIndex = 2;
  end;
  if ItemAgruNoItm < 0 then
    Exit;
  if MyObjects.FIC(RG00DescrAgruNoItm.ItemIndex = 0, RG00DescrAgruNoItm,
  'Informe a forma de "Descri��o do item"') then
    Exit;
  if Ck00EstoqueEm.Checked then
  begin
    VS_PF.ImprimeEstoqueEm(Entidade, Filial, Ed00Terceiro.ValueVariant,
    RG00_Ordem1.ItemIndex, RG00_Ordem2.ItemIndex, RG00_Ordem3.ItemIndex,
    RG00_Ordem4.ItemIndex, RG00_Ordem5.ItemIndex, RG00_Agrupa.ItemIndex,
    DescrAgruNoItm, Ed00StqCenCad.ValueVariant,
    RG00ZeroNegat.ItemIndex, CBEmpresa.Text, CB00StqCenCad.Text,
    CB00Terceiro.Text, TPDataRelativa.Date, Ck00DataCompra.Checked,
    Ck00EmProcessoBH.Checked, DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2,
    Qr00GraGruY, Qr00GraGruX, Qr00CouNiv2, TP00EstoqueEm.Date,
    // 2021-01-09 ini
    //Ed00GraCusPrc.ValueVariant,
    TRelatorioVSImpEstoque(RG00Relatorio.ItemIndex),
    // 2021-01-09 fim
    Ed00NFeIni.ValueVariant,
    Ed00NFeFim.ValueVariant, Ck00Serie.Checked, Ed00Serie.ValueVariant,
    Ed00MovimCod.ValueVariant, EdClientMO.ValueVariant, LaAviso1, LaAviso2,
    MostraFrx)
  end else
  begin
    TableSrc := TMeuDB + '.' + CO_SEL_TAB_VMI;
    //
    VS_CRC_PF.ImprimeEstoqueReal(Entidade, Filial, Ed00Terceiro.ValueVariant,
    RG00_Ordem1.ItemIndex, RG00_Ordem2.ItemIndex, RG00_Ordem3.ItemIndex,
    RG00_Ordem4.ItemIndex, RG00_Ordem5.ItemIndex, RG00_Agrupa.ItemIndex,
    DescrAgruNoItm, Ed00StqCenCad.ValueVariant,
    RG00ZeroNegat.ItemIndex, CBEmpresa.Text, CB00StqCenCad.Text,
    CB00Terceiro.Text, TPDataRelativa.Date, Ck00DataCompra.Checked,
    Ck00EmProcessoBH.Checked, DBG00GraGruY, DBG00GraGruX, DBG00CouNiv2,
    Qr00GraGruY, Qr00GraGruX, Qr00CouNiv2, TableSrc, DataRetroativa,
    // 2021-01-09 ini
    //Ed00GraCusPrc.ValueVariant,
    TRelatorioVSImpEstoque(RG00Relatorio.ItemIndex),
    // 2021-01-09 fim
    DmodG.ObtemAgora(), Ed00NFeIni.ValueVariant,
    Ed00NFeFim.ValueVariant, Ck00Serie.Checked, Ed00Serie.ValueVariant,
    Ed00MovimCod.ValueVariant, EdClientMO.ValueVariant, MostraFrx);
  end;
end;

procedure TFmVSMovImp.ImprimeEstoqueCustoIntegrado();
var
  Empresa: Integer;
begin
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  VS_PF.MostraFormVSEstqCustoIntegr(Empresa);
end;

procedure TFmVSMovImp.ImprimeEtiquetasDePallets(InfoNO_PALLET: Boolean; LabelsPerPage: Integer);
var
  EmpImp: Boolean;
  I, N, Empresa: Integer;
  MyArr: array of Integer;
  EmptyLabels: Variant;
begin
  //ImprimePallets(frxWET_CURTI_018_03_A, 'Fichas de Pallets');
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  N      := 0;
  EmpImp := CkFichaPallet.Checked;
  //
  with DBG04Estq.DataSource.DataSet do
  for I := 0 to DBG04Estq.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
    GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
    N := N + 1;
    SetLength(MyArr, N);
    MyArr[N - 1] := QrEstqR4Pallet.Value;
  end;
  if N > 0 then
  begin
    EmptyLabels := 0;
    //
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, EmptyLabels,
    0, 0, '', '', True, 'Etiquetas vazias',
    'Informe a quantidade de etiquetas vazias: ', 0, EmptyLabels) then
    begin
      VS_PF.ImprimePallets(Empresa, MyArr, FVSMovImp4, FVSLstPalBox,
      InfoNO_PALLET, difpEtiq2x4, EmpImp, Integer(EmptyLabels));
    end;
  end else
  begin
    //Geral.MB_Aviso('Nenhum Pallet foi selecionado!')*);
    EmptyLabels := 8;
    VS_PF.ImprimePallets(Empresa, MyArr, FVSMovImp4, FVSLstPalBox,
    InfoNO_PALLET, difpEtiq2x4, EmpImp, Integer(EmptyLabels));
  end;
end;

procedure TFmVSMovImp.ImprimeFatorAParaFatorB();
begin
  DmModVS.Reopen16Fatores(TP16DataIni.Date, TP16DataFim.Date,
  Ck16DataIni.Checked, Ck16DataFim.Checked);
  //
  DBG16Fatores.DataSource := DmModVS.Ds16Fatores;
end;

procedure TFmVSMovImp.ImprimeFichasDePallets(InfoNO_PALLET: Boolean; DestImprFichaPallet: TDestImprFichaPallet);
var
  EmpImp: Boolean;
  I, N, Empresa: Integer;
  MyArr: array of Integer;
begin
  //ImprimePallets(frxWET_CURTI_018_03_A, 'Fichas de Pallets');
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  N      := 0;
  EmpImp := CkFichaPallet.Checked;
  //
  with DBG04Estq.DataSource.DataSet do
  for I := 0 to DBG04Estq.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
    GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
    N := N + 1;
    SetLength(MyArr, N);
    MyArr[N - 1] := QrEstqR4Pallet.Value;
  end;
  if N > 0 then
    VS_PF.ImprimePallets(Empresa, MyArr, FVSMovImp4, FVSLstPalBox, InfoNO_PALLET,
    DestImprFichaPallet, EmpImp)
  else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmVSMovImp.ImprimeFluxo;
begin
  VS_PF.MostraRelatorioVSImpFluxo(TP13DataIni.Date, TP13DataFim.Date,
  Ck13DataIni.Checked, Ck13DataFim.Checked, Ck13DataIni.Visible,
  Ck13DataIni.Enabled, Ed13_Pallet.ValueVariant, Ed13_IMEI.ValueVariant,
  Ed13GraGruX.ValueVariant, EdEmpresa.ValueVariant,
  Ed13_MaxInteiros.ValueVariant, RG13TipoMov.ItemIndex,
  RG13_AnaliSinte.ItemIndex, RG13_GragruY.ItemIndex, RG13_AnaliSinte, PB1,
  LaAviso1, LaAviso2, BtImprime, CBEmpresa.Text,
  Ed13SerieFch.ValueVariant, Ed13Ficha.ValueVariant);
end;

procedure TFmVSMovImp.PesquisaPallets();
const
  SQL_Especificos = '';
// Falta fazer
  ClientMO  = 0;
  StqCenLoc = 0;
  MovimID   = 0;
  MovimCod  = 0;
var
  StqCenCad, I, Empresa, CouNiv2, CouNiv1: Integer;
  SQL_Empresa, GraGruYs: String;
  GraGruXs: array of Integer;
begin
  if VS_PF.VerificaGraGruXCouSemNivel1() > 0 then
    Exit;
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  //
  GraGruYs := '';
  if (DBG04GraGruY.SelectedRows.Count > 0) and
  (DBG04GraGruY.SelectedRows.Count < Qr04GraGruY.RecordCount) then
  begin
    for I := 0 to DBG04GraGruY.SelectedRows.Count - 1 do
    begin
      //Qr04GraGruY.GotoBookmark(pointer(DBG04GraGruY.SelectedRows.Items[I]));
      Qr04GraGruY.GotoBookmark(DBG04GraGruY.SelectedRows.Items[I]);
      //
      if GraGruYs <> '' then
        GraGruYs := GraGruYs + ',';
      GraGruYs := GraGruYs + Geral.FF0(Qr04GraGruYCodigo.Value);
    end;
  end;
  SetLength(GraGruXs, 0);
  if (DBG04Artigos.SelectedRows.Count > 0) and
  (DBG04Artigos.SelectedRows.Count < Qr04GraGruX.RecordCount) then
  begin
    for I := 0 to DBG04Artigos.SelectedRows.Count - 1 do
    begin
      //Qr04GraGruX.GotoBookmark(pointer(DBG04Artigos.SelectedRows.Items[I]));
      Qr04GraGruX.GotoBookmark(DBG04Artigos.SelectedRows.Items[I]);
      //
      SetLength(GraGruXs, I + 1);
      GraGruXs[I] := Qr04GraGruXControle.Value;
    end;
  end;
  StqCenCad  := Ed04StqCenCad.ValueVariant;
  //GraGruX := Ed04GraGruX.ValueVariant;
  CouNiv2 := Ed04CouNiv2.ValueVariant;
  CouNiv1 := Ed04CouNiv1.ValueVariant;
  if VS_PF.PesquisaPallets(Empresa, ClientMO, CouNiv2, CouNiv1, StqCenCad,
  StqCenLoc, Self.Name, GraGruYs, GraGruXs, [], SQL_Especificos, TEstqMovimID(
  MovimID), MovimCod, FVSMovImp4
  (*, FVSMovImp5*)) then
    ReopenVSMovImp4(Empresa, 0, 0, 0, CouNiv1, CouNiv2, GraGruYS);
end;

procedure TFmVSMovImp.PesquisaPrevia();
var
  PalIni, PalFim: Integer;
  SQLPallets: String;
begin
  PalIni := Ed07PallIni.ValueVariant;
  PalFim := Ed07PallFim.ValueVariant;
(*
  SQLPallets :=   'WHERE cia.VSPallet BETWEEN ' + Geral.FF0(PalIni) +
  ' AND ' + Geral.FF0(PalFim);
*)
  SQLPallets :=   'WHERE pal.Codigo BETWEEN ' + Geral.FF0(PalIni) +
  ' AND ' + Geral.FF0(PalFim);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr07PrevPal, Dmod.MyDB, [
 'SELECT pal.Codigo VSPallet,  ',
 'SUM(cia.Pecas) Pecas,  ',
 'SUM(cia.AreaM2) AreaM2, ',
 'SUM(cia.AreaP2) AreaP2, ',
 'CONCAT(gg1.Nome,   ',
 'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
 'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)),  ',
 'IF(pal.Nome <> "", CONCAT(" (", pal.Nome, ")"), ""))   ',
 'NO_PRD_TAM_COR, pal.*   ',
(*
 'FROM vscacitsa cia ',
 'LEFT JOIN vspalleta pal ON pal.Codigo=cia.VSPallet ',
*)
 'FROM  vspalleta pal ',
 'LEFT JOIN vscacitsa  cia ON cia.VSPallet=pal.Codigo ',
 'LEFT JOIN gragrux    ggx ON ggx.Controle=pal.GraGruX   ',
 'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
 'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
 'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
 'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
 SQLPallets,
(*
 'GROUP BY cia.VSPallet ',
 'ORDER BY cia.VSPallet DESC ',
*)
 'GROUP BY VSPallet ',
 'ORDER BY VSPallet DESC ',
 ' ']);
 //Geral.MB_SQL(Self, Qr07PrevPal);
end;

procedure TFmVSMovImp.PesquisaTamanhos();
  function I2T(Variable: String; Codigo: Integer; Texto: String): String;
  var
    Txt: String;
  begin
    if Codigo = 0 then
      Txt := 'Qualquer'
    else
      Txt := Texto;
    frxWET_CURTI_018_06_A.Variables[Variable] := QuotedStr(Txt);
  end;
  function T2T(Variable, Texto: String): String;
  var
    Txt: String;
  begin
    if Trim(Texto) = '' then
      Txt := 'Qualquer'
    else
      Txt := Texto;
    frxWET_CURTI_018_06_A.Variables[Variable] := QuotedStr(Txt);
  end;
(*
  function T1T(Variable, Texto: String): String;
  begin
    frxWET_CURTI_018_06_A.Variables[Variable] := QuotedStr(Texto);
  end;
*)
var
  CacCod, CacID, Codigo, VSPalletSrc, VSPalletDst, VMI_Sorc, VMI_Dest,
  VMI_Baix, Box, Revisor, Digitador, Martelo, Ficha, SerieFicha, GraGruXSrc,
  GraGruXDst, Terceiro, CouNiv1, CouNiv2: Integer;
  Marca, Periodo, Pequeno, Grande,
  LJN_VMI_SRC, LJN_VMI_DST, LJN_GGX_COU, SQL_Marca,
  SQL_CacCod, SQL_CacID, SQL_Codigo, SQL_VSPalletSrc, SQL_VSPalletDst,
  SQL_VMI_Sorc, SQL_VMI_Dest, SQL_VMI_Baix, SQL_Box, SQL_Revisor, SQL_Digitador,
  SQL_Martelo, SQL_Ficha, SQL_SerieFicha, SQL_GraGruXSrc, SQL_GraGruXDst,
  SQL_Terceiro, SQL_Periodo, SQL_CouNiv1, SQL_CouNiv2, SQL_Pallets: String;
  Chart1: TfrxChartView;
  PieSeries: TPieSeries;
begin
  FVSCacGBY :=
    UnCreateVS.RecriaTempTableNovo(ntrttVSCacGBY, DModG.QrUpdPID1, False);
    //
  LJN_VMI_SRC     := '';
  LJN_VMI_DST     := '';
  //
  SQL_CacID       := '';
  SQL_CacCod      := '';
  SQL_Codigo      := '';
  SQL_VSPalletSrc := '';
  SQL_VSPalletDst := '';
  SQL_VMI_Sorc    := '';
  SQL_VMI_Dest    := '';
  SQL_VMI_Baix    := '';
  SQL_Box         := '';
  SQL_Revisor     := '';
  SQL_Digitador   := '';
  SQL_Martelo     := '';
  SQL_Periodo     := '';
  SQL_Ficha       := '';
  SQL_SerieFicha  := '';
  SQL_GraGruXSrc  := '';
  SQL_GraGruXDst  := '';
  SQL_Terceiro    := '';
  SQL_Marca       := '';
  //
  SQL_CouNiv1     := '';
  SQL_CouNiv2     := '';
  SQL_Pallets     := '';
  //
  CacCod          := Ed06VSCacCod.ValueVariant;
  Codigo          := 0;//
  VSPalletSrc     := Ed06VSPalletSrc.ValueVariant;
  VSPalletDst     := Ed06VSPalletDst.ValueVariant;
  VMI_Sorc        := Ed06VMI_Sorc.ValueVariant;
  VMI_Dest        := Ed06VMI_Dest.ValueVariant;
  VMI_Baix        := 0;//Ed06VMI_Baix.ValueVariant;
  Box             := 0;//Ed06Box.ValueVariant;
  Revisor         := 0;//
  Digitador       := 0;//
  Martelo         := Ed06Martelo.ValueVariant;
  Ficha           := Ed06Ficha.ValueVariant;
  GraGruXSrc      := Ed06GraGruXSrc.ValueVariant;
  GraGruXDst      := Ed06GraGruXDst.ValueVariant;
  Terceiro        := Ed06Terceiro.ValueVariant;
  Marca           := Ed06Marca.Text;
  Periodo         := dmkPF.PeriodoImp1(TP06DataIni.Date, TP06DataFim.Date,
    Ck06DataIni.Checked, Ck06DataFim.Checked, '', 'at�', '');
  //
  CouNiv1         := Ed06CouNiv1.ValueVariant;
  CouNiv2         := Ed06CouNiv2.ValueVariant;
  CacID           := RGCacID.ItemIndex;
  //
  if CacCod <> 0 then
    SQL_CacCod := 'AND cia.CacCod=' + Geral.FF0(CacCod);
  //SQL_Codigo    := 'AND cia.Codigo = 5';
  //if VSPallet <> 0 then
  //  SQL_VSPallet  := 'AND cia.VSPallet=' + Geral.FF0(VSPallet);
  if VMI_Sorc <> 0 then
    SQL_VMI_Sorc  := 'AND cia.VMI_Sorc=' + Geral.FF0(VMI_Sorc);
  if VMI_Dest <> 0 then
    SQL_VMI_Dest  := 'AND cia.VMI_Dest=' + Geral.FF0(VMI_Dest);
  if VMI_Baix <> 0 then
    SQL_VMI_Baix  := 'AND cia.VMI_Baix=' + Geral.FF0(VMI_Baix);
  if Box <> 0 then
    SQL_Box  := 'AND cia.Box=' + Geral.FF0(Box);
  if Revisor <> 0 then
    SQL_Revisor  := 'AND cia.Revisor=' + Geral.FF0(Revisor);
  if Digitador <> 0 then
    SQL_Digitador  := 'AND cia.Digitador=' + Geral.FF0(Digitador);
  if Martelo <> 0 then
    SQL_Martelo  := 'AND cia.Martelo=' + Geral.FF0(Martelo);
  SQL_Periodo := dmkPF.SQL_Periodo('AND cia.DataHora ',
    TP06DataIni.Date, TP06DataFim.Date, Ck06DataIni.Checked, Ck06DataFim.Checked);
  if VSPalletDst <> 0 then
    SQL_VSPalletDst  := 'AND dst.Pallet=' + Geral.FF0(VSPalletDst);
  if VSPalletSrc <> 0 then
    SQL_VSPalletSrc  := 'AND src.Pallet=' + Geral.FF0(VSPalletSrc);
  if Ficha <> 0 then
    SQL_Ficha  := 'AND src.Ficha=' + Geral.FF0(Ficha);
  if SerieFicha <> 0 then
    SQL_SerieFicha  := 'AND src.SerieFicha=' + Geral.FF0(SerieFicha);
  if GraGruXSrc <> 0 then
    SQL_GraGruXSrc  := 'AND src.GraGruX=' + Geral.FF0(GraGruXSrc);
  if GraGruXDst <> 0 then
    SQL_GraGruXDst  := 'AND dst.GraGruX=' + Geral.FF0(GraGruXDst);
  if Terceiro <> 0 then
    SQL_Terceiro  := 'AND src.Terceiro=' + Geral.FF0(Terceiro);
  if Marca <> '' then
    SQL_Marca  := 'AND src.Marca = "' + Marca + '"';
  //
  //if (VMI_Sorc <> 0) or (Ficha <> 0) or (SerieFicha <> 0) or (GraGruX <> 0)
  //or (VSPallet <> 0) or (GraGruX <> 0) or (Terceiro <> 0) or (Marca <> "") then
    LJN_VMI_SRC := 'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' src ON src.Controle=cia.VMI_Sorc';
  //if VSPalletDst <> 0 then
    LJN_VMI_DST := 'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' dst ON dst.Controle=cia.VMI_Dest';
    LJN_GGX_COU := 'LEFT JOIN ' + TMeuDB + '.gragruxcou gxc ON gxc.GraGruX=dst.GraGruX';
  //
  if CouNiv1 <> 0 then
    SQL_CouNiv1 :=  'AND gxc.CouNiv1=' + Geral.FF0(CouNiv1);
  if CouNiv2 <> 0 then
    SQL_CouNiv2 :=  'AND gxc.CouNiv2=' + Geral.FF0(CouNiv2);
  case CacID of
    0: SQL_CacID := 'AND cia.CacID=' + Geral.FF0(Integer(TEstqMovimID.emidClassArtXXUni));
    1: SQL_CacID := 'AND cia.CacID=' + Geral.FF0(Integer(TEstqMovimID.emidReclasXXUni));
    2: SQL_CacID := ''; // Ambos
    else SQL_CacID := 'AND cia.CacID ???';
  end;
  if Ck06Pallets.Checked then
  begin
    SQL_Pallets := SQL_PalletsReal();
    if SQL_Pallets <> '' then
      SQL_Pallets := 'AND dst.Pallet IN (' + SQL_Pallets + ')'
    else
      Exit;
  end;
  //
  Pequeno := Geral.FFT_Dot(Ed06Pequeno.ValueVariant, 2, siNegativo);
  Grande  := Geral.FFT_Dot(Ed06Grande.ValueVariant, 2, siNegativo);
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'INSERT INTO ' + FVSCacGBY,
  '',
  'SELECT cia.CacCod, cia.CacID, cia.Codigo, cia.VSPallet, cia.VMI_Sorc, ',
  'cia.VMI_Dest, cia.VMI_Baix, cia.Box, cia.Revisor, cia.Digitador, ',
  'cia.Martelo, SUM(cia.Pecas) Pecas, SUM(cia.AreaM2) AreaM2, ',
  'SUM(cia.AreaP2) AreaP2, IF(cia.AreaM2 < ' + Pequeno +
  ', 1, IF(cia.AreaM2 > ' + Grande + ', 3, 2)) Tamanho, ',
  // ini 2021-03-05 Compatibilidade!
  '0.00 OrigArM2, 0.00 OrigValT, ',
  // fim 2021-03-05
  '1 Ativo ',
  'FROM ' + TMeuDB + '.vscacitsa cia ',
  LJN_VMI_SRC,
  LJN_VMI_DST,
  LJN_GGX_COU,
  'WHERE cia.Controle<>0 ',
  Geral.ATS([
  SQL_CacID,
  SQL_CacCod,
  SQL_Codigo,
  SQL_VSPalletSrc,
  SQL_VSPalletDst,
  SQL_VMI_Sorc,
  SQL_VMI_Dest,
  SQL_VMI_Baix,
  SQL_Box,
  SQL_Revisor,
  SQL_Digitador,
  SQL_Martelo,
  SQL_SerieFicha,
  SQL_Ficha,
  SQL_GraGruXSrc,
  SQL_GraGruXDst,
  SQL_Terceiro,
  SQL_Marca,
  SQL_Periodo,
  SQL_CouNiv1,
  SQL_CouNiv2,
  SQL_Pallets], True),
  'GROUP BY Tamanho ',
  '']);
  //
  // Deve ser antes por causa do CalcFields
  UnDmkDAC_PF.AbreMySQLQuery0(Qr06SumGBY, DModG.MyPID_DB, [
  'SELECT SUM(gby.Pecas) Pecas, ',
  'SUM(gby.AreaM2) AreaM2, SUM(gby.AreaP2) AreaP2 ',
  'FROM ' + FVSCacGBY + ' gby ',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr06VSCacGBY, DModG.MyPID_DB, [
  'SELECT gby.VMI_Dest, vmi.GraGruX, ',
  'ggx.GraGru1, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  'SUM(gby.Pecas) Pecas, ',
  'SUM(gby.AreaM2) AreaM2, SUM(gby.AreaP2) AreaP2, ',
  'gby.Tamanho ',
  'FROM ' + FVSCacGBY + ' gby ',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=gby.VMI_Dest ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'WHERE gby.Ativo=1',
  'GROUP BY Tamanho ',
  'ORDER BY AreaM2 DESC ',
  '']);
  if Qr06VSCacGBY.RecordCount = 0 then
  begin
    Geral.MB_Info('N�o h� informa��o a ser mostrada!');
    Exit;
  end;
  I2T('VARF_GraGruX', GraGruXSrc, CB06GraGruXSrc.Text);
  //I2T('VARF_GraGruX', GraGruXDst, CB06GraGruXDst.Text);
  I2T('VARF_VSPalletSrc', VSPalletSrc, Geral.FF0(VSPalletSrc));
  I2T('VARF_VSPalletDst', VSPalletDst, Geral.FF0(VSPalletDst));
  I2T('VARF_Terceiro', Terceiro, CB06Terceiro.Text);
  I2T('VARF_SerieFch', SerieFicha, CB06SerieFch.Text);
  I2T('VARF_Ficha',    Ficha, Geral.FF0(Ficha));
  T2T('VARF_Marca',    Marca);
  I2T('VARF_VSCacCod', CacCod, Geral.FF0(CacCod));
  I2T('VARF_VMI_Sorc', VMI_Sorc, Geral.FF0(VMI_Sorc));
  I2T('VARF_VMI_Dest', VMI_Dest, Geral.FF0(VMI_Dest));
  I2T('VARF_Martelo',  Martelo, Geral.FF0(Martelo));
  T2T('VARF_Periodo',  Periodo);
  T2T('VARF_CacID',    RGCacID.Items[RGCacID.ItemIndex]);
  I2T('VARF_CouNiv2',  CouNiv2, CB06CouNiv2.Text);
  I2T('VARF_CouNiv1',  CouNiv1, CB06CouNiv1.Text);
  //
  MyObjects.frxDefineDataSets(frxWET_CURTI_018_06_A, [
    DModG.frxDsDono,
    frxDs06VSCacGBY
  ]);
  Chart1 := frxWET_CURTI_018_06_A.FindObject('ChartPie0') as TfrxChartView;
  Chart1.Chart.Legend.Visible := False;
  //Chart1.Chart.Legend.Alignment := laBottom;
  //Chart1.Chart.Legend.Shadow.Visible := False;
  Chart1.Chart.View3D := False;

  // do VCLTee.Chart.CustomChart:
  Chart1.Chart.Series[0].Marks.Font.Size := 6;
  Chart1.Chart.Series[0].Marks.Transparent := True;
  Chart1.Chart.Series[0].Marks.Shadow.Visible := False;
  PieSeries := TPieSeries(Chart1.Chart.Series[0]);
  PieSeries.Circled := True;



    // do frxChat
  //Chart1.SeriesData[0].TopN := 20;
  //Chart1.SeriesData[0].TopNCaption := FOutrosChart;
  //Chart1.SeriesData.Items[0].Circled := True;

  MyObjects.frxMostra(frxWET_CURTI_018_06_A, 'Resultado de Classifica��o');
end;

procedure TFmVSMovImp.PM04EstqPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsUpd(Irparaajaneladepallets1, QrEstqR4);
end;

procedure TFmVSMovImp.PM10FichasRMPPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsUpd(IrparajaneladegerenciamentodeFichasRMP1, Qr10FichasRMP);
end;

procedure TFmVSMovImp.PM16FatoresPopup(Sender: TObject);
var
  Habilita, TemPalSrc, TemPalDst: Boolean;
begin
  Habilita := (DmModVS.Qr16Fatores.State <> dsInactive)
              and (DmModVS.Qr16Fatores.RecordCount > 0);
  TemPalSrc := DmModVS.Qr16FatoresPallet_Src.Value > 0;
  TemPalDst := DmModVS.Qr16FatoresPallet_Dst.Value > 0;
  //
  IrparajaneladeOC1.Enabled              := Habilita;
  Irparajaneladepalletdeorigem1.Enabled  := Habilita and TemPalSrc;
  Irparajaneladepalletdedestino1.Enabled := Habilita and TemPalDst;
end;

procedure TFmVSMovImp.PackingList1Click(Sender: TObject);
const
  Vertical = True;
begin
  if DefineMyArr03() then
    VS_PF.ImprimePackListsIMEIs(EdEmpresa.ValueVariant, MyArr03, Vertical)
end;

procedure TFmVSMovImp.PackingListHorizontal1Click(Sender: TObject);
const
  Horizontal = False;
begin
  if DefineMyArr03() then
    VS_PF.ImprimePackListsIMEIs(EdEmpresa.ValueVariant, MyArr03, Horizontal)
end;

procedure TFmVSMovImp.PCRelatorioChange(Sender: TObject);
var
  Qry: TmySQLQuery;
begin
  case PCRelatorio.ActivePageIndex of
    0:
    begin
      if Qr00GraGruY.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr00GraGruY, Dmod.MyDB);
      if Qr00GraGruX.State = dsInactive then
        Reopen00GraGruX();
      if Qr00StqCenCad.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr00StqCenCad, Dmod.MyDB);
      if Qr00CouNiv2.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr00CouNiv2, Dmod.MyDB);
      if Qr00Fornecedor.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr00Fornecedor, Dmod.MyDB);
      // 2021-01-09 ini
      // de > GraCusPrc: Integer; para >  Relatorio: TRelatorioVSImpEstoque;
      //if Qr00GraCusPrc.State = dsInactive then
      //  UnDmkDAC_PF.AbreQuery(Qr00GraCusPrc, Dmod.MyDB);
      // 2021-01-09 fim
      if QrClientMO.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(QrClientMO, Dmod.MyDB);
    end;
    2:
    begin
      if Qr02Fornecedor.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr02Fornecedor, Dmod.MyDB);
      if Qr02GraGruX.State = dsInactive then
        VS_PF.ReopenGraGruX_A(Qr02GraGruX, '');
      if Qr02VSPallet.State = dsInactive then
        VS_PF.ReopenPallet_A(Qr02VSPallet);
    end;
    3:
    begin
      PnFichas.Visible := True;
      if Qr03GraGruY.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr03GraGruY, Dmod.MyDB);
      if Qr03GraGruX.State = dsInactive then
          VS_PF.ReopenGraGruX_A(Qr03GraGruX, '');
      SetHabilitado((QrEstqR3.State <> dsInactive) and (QrEstqR3.RecordCount > 0));
    end;
    4:
    begin
      PnFichas.Visible := True;
      SetHabilitado((QrEstqR4.State <> dsInactive) and (QrEstqR4.RecordCount > 0));
      if Qr04CouNiv1.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr04CouNiv1, Dmod.MyDB);
      if Qr04CouNiv2.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr04CouNiv2, Dmod.MyDB);
      if Qr04GraGruY.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr04GraGruY, Dmod.MyDB);
      if Qr04StqCenCad.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr04StqCenCad, Dmod.MyDB);
      if Qr04GraGruX.State = dsInactive then
        VS_PF.ReopenGraGruX_A(Qr04GraGruX, '');
    end;
    5:
    begin
(*
      if Qr05GraGruX.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr05GraGruX, Dmod.MyDB);
      if Qr05VSPallet.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr05VSPallet, Dmod.MyDB);
      if QrVSMrtCad.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(QrVSMrtCad, Dmod.MyDB);
      if Qr05Fornecedor.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr05Fornecedor, Dmod.MyDB);
      if Qr05VSSerFch.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr05VSSerFch, Dmod.MyDB);
*)
    end;
    6:
    begin
      if Qr06GraGruX.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr06GraGruX, Dmod.MyDB);
      if Qr06GraGruXDst.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr06GraGruXDst, Dmod.MyDB);
      if Qr06VSPalletSrc.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr06VSPalletSrc, Dmod.MyDB);
      if Qr06VSPalletDst.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr06VSPalletDst, Dmod.MyDB);
      if Qr06VSMrtCad.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr06VSMrtCad, Dmod.MyDB);
      if Qr06Fornecedor.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr06Fornecedor, Dmod.MyDB);
      if Qr06VSSerFch.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr06VSSerFch, Dmod.MyDB);
      if Qr06CouNiv1.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr06CouNiv1, Dmod.MyDB);
      if Qr06CouNiv2.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr06CouNiv2, Dmod.MyDB);
    end;
    7:
    begin
      PnFichas.Visible := True;
      SetHabilitado((Qr07PrevPal.State <> dsInactive) and (Qr07PrevPal.RecordCount > 0));
    end;
    8:
    begin
      if Qr08Fornecedor.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr08Fornecedor, Dmod.MyDB);
    end;
    9: // Erros
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrCiaDst, DModG.MyPID_DB, [
      'DROP TABLE IF EXISTS _sum_vmi_dest_;',
      'CREATE TABLE _sum_vmi_dest_',
      'SELECT cia.VSPallet, cia.VMI_Dest, ',
      'SUM(cia.Pecas) CiaPecas, vmi.Pecas DstPecas',
      'FROM ' + TMeuDB + '.vscacitsa cia',
      'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=cia.VMI_Dest',
      'GROUP BY cia.VMI_Dest;',
      'SELECT * FROM _sum_vmi_dest_',
      'WHERE CiaPecas <> DstPecas',
      '']);
    end;
    13: //Fluxo
    begin
      PnFichas.Visible := True;
      VS_PF.ReopenGraGruX_A(Qr13GraGruX, '');
      //
      CB13SerieFch.ListSource := DmModVS.Ds13VSSerFch;
      UnDMkDAC_PF.AbreQuery(DmModVS.Qr13VSSerFch, Dmod.MyDB);
      //
    end;
    14: // Nota MPAG
    begin
      CB14GraGruX.ListSource := DmModVS.Ds14GraGruX;
      VS_PF.ReopenGraGruX_A(DmModVS.Qr14GraGruX, '');
      //
      CB14Terceiro.ListSource := DmModVS.Ds14Fornecedor;
      UnDMkDAC_PF.AbreQuery(DmModVS.Qr14Fornecedor, Dmod.MyDB);
      //
      CB14SerieFch.ListSource := DmModVS.Ds14VSSerFch;
      UnDMkDAC_PF.AbreQuery(DmModVS.Qr14VSSerFch, Dmod.MyDB);
      //
      //DBG14VSMovIts.DataSource := DmModVS.Ds14VSMovIts;
    end;
    17: // Requisicao de Movimentacao de Estoque
    begin
(*
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT *  ',
        'FROM vsreqmov ',
        'WHERE Empresa=' + Geral.FF0(EdEmpresa.ValueVariant),
        'ORDER BY Codigo DESC ',
        '']);
        Ed17NumIni.ValueVariant := Qry.FieldByName('NumFim').AsInteger + 1;
      finally
        Qry.Free;
      end;
*)
    end;
    18: // Ordens de Opera��o de produ��o
    begin
      if Qr18Fornecedor.State = dsInactive then
        UnDmkDAC_PF.AbreQuery(Qr18Fornecedor, Dmod.MyDB);
    end;
    20: // NFes emitidas
    begin
      PnFichas.Visible := True;
      SetHabilitado(False);
    end;
    21: // Rendimento de semi acabado
    begin
      if Qr21CouNiv2.State = dsInactive then
      begin
        UnDmkDAC_PF.AbreQuery(Qr21CouNiv2, Dmod.MyDB);
        MyObjects.SetaItensBookmark(Self, TDBGrid(DBG21CouNiv2), 'Codigo', [1], True);
      end;
      if Qr21Fornecedor.State = dsInactive then
      UnDmkDAC_PF.AbreQuery(Qr21Fornecedor, Dmod.MyDB);
    end;
    else
    begin
      PnFichas.Visible := False;
    end;
  end;
end;

procedure TFmVSMovImp.PesquisaBlendVS();
(*
var
  NO_FORNECE: String;
  Pallet, MovimID, SerieFch, Ficha, Terceiro: Integer;
  Pecas, AreaM2, PercPecas, PercAreaM2: Double;
begin
  Pallet         := ;
  MovimID        := ;
  SerieFch       := ;
  Ficha          := ;
  Pecas          := ;
  AreaM2         := ;
  PercPecas      := ;
  PercAreaM2     := ;
  Terceiro       := ;
  NO_FORNECE    := ;

  //
? := UMyMod.BuscaEmLivreY_Def('_vsmovimp4_1_fmvsmovimp', , ImgTipo.SQLType?, CodAtual?);
ou > ? := UMyMod.BPGS1I32('_vsmovimp4_1_fmvsmovimp', , '', '', tsPosNeg?, stInsUpd?, CodAtual?);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, ImgTipo.SQLType?, '_vsmovimp4_1_fmvsmovimp', auto_increment?[
'Pallet', 'MovimID', 'SerieFch',
'Ficha', 'Pecas', 'AreaM2',
'PercPecas', 'PercAreaM2', 'Terceiro',
'NO_FORNECE'], [
], [
Pallet, MovimID, SerieFch,
Ficha, Pecas, AreaM2,
PercPecas, PercAreaM2, Terceiro,
NO_FORNECE], [
], UserDataAlterweb?, IGNORE?
*)
var
  I, N: Integer;
  MyArr: array of Integer;
function SQL_ListaControles(): String;
var
  I, N: Integer;
begin
  Result := '';
  N := High(MyArr);
  for I := Low(MyArr) to N do
  begin
    Result := Result + sLineBreak + Geral.FF0(MyArr[I]);
    if I < N then
      Result := Result + ',';
  end;
end;
var
  Lista, SumPecas, SumAreaM2: String;
begin
  N := 0;
  with DBG04Estq.DataSource.DataSet do
  for I := 0 to DBG04Estq.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
    GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
    N := N + 1;
    SetLength(MyArr, N);
    MyArr[N - 1] := QrEstqR4Pallet.Value;
  end;
  if N > 0 then
  begin
    FVSBlendVS := UnCreateVS.RecriaTempTableNovo(ntrttVSBlendVS,
      DModG.QrUpdPID1, False, 1, '_vsmovimp4_1_' + Self.Name);
    //
    Lista := SQL_ListaControles();
    UnDmkDAC_PF.AbreMySQLQuery0(Qr04_01Sum, Dmod.MyDB, [
    'SELECT SUM(vmi.Pecas) Pecas, SUM(AreaM2) AreaM2 ',
    'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
    'WHERE vmi.Pallet>0 ',
    'AND vmi.Pecas > 0 ',
    'AND vmi.MovimID IN (' + CO_ALL_CODS_BLEND_VS + ') ', // emin emid
    'AND vmi.Pallet IN (' + Lista + ') ',
    '']);
    SumPecas  := Geral.FFT_Dot(Qr04_01SumPecas.Value, 3, siNegativo);
    SumAreaM2 := Geral.FFT_Dot(Qr04_01SumAreaM2.Value, 2, siNegativo);
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'DELETE FROM  ' + FVSBlendVS + ';',
    'INSERT INTO  ' + FVSBlendVS,
    'SELECT vmi.Pallet, vmi.MovimID, vmi.SerieFch, vmi.Ficha,  ',
    'SUM(vmi.Pecas) Pecas, SUM(AreaM2) AreaM2,  ',
    'SUM(vmi.Pecas) / ' + SumPecas + ' PerPecas, ',
    'SUM(AreaM2) / ' + SumAreaM2 + ' PercAreaM2, vmi.Terceiro,  ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE, 1 Ativo  ',
    'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
    'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=vmi.Terceiro ',
    'WHERE vmi.Pallet>0 ',
    'AND vmi.Pecas > 0 ',
    'AND vmi.MovimID IN (' + CO_ALL_CODS_BLEND_VS + ') ', // emin emid
    'AND vmi.Pallet IN (' + Lista + ') ',
    'GROUP BY vmi.Pallet, vmi.MovimID, vmi.SerieFch, vmi.Ficha ',
    'ORDER BY vmi.Pallet, vmi.MovimID, vmi.SerieFch, vmi.Ficha ',
    '']);
(*
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO  ' + FVSBlendVS,
SELECT Pallet, MovimID, SerieFch, Ficha, SUM(Pecas) Pecas
FROM ' + CO_SEL_TAB_VMI + '
WHERE Pallet>0
AND Pecas > 0
AND MovimID IN (' + CO_ALL_CODS_BLEND_VS + ')
/*AND Pallet IN (920, 930, 940)*/
GROUP BY Pallet, MovimID, SerieFch, Ficha
ORDER BY Pallet, MovimID, SerieFch, Ficha
*)
  end else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmVSMovImp.PesquisaFaltaDeCouros();
{
var
  Terceiro: Integer;
  SQL_Periodo, SQL_Terceiro, SQL_ORDEM: String;
  //
  Report: TfrxReport;
  Grupo0: TfrxGroupHeader;
  Futer0: TfrxGroupFooter;
  Me_GH0, Me_FT0, MeValNome, MeTitNome, MeValCodi: TfrxMemoView;
}
begin
  Application.CreateForm(TFmVSImpDif, FmVSImpDif);
  FmVSImpDif.FEmpresa_Cod   := EdEmpresa.ValueVariant;
  FmVSImpDif.FEmpresa_Txt   := CBEmpresa.Text;
  FmVSImpDif.FTerceiro_Txt  := CB08Terceiro.Text;
  FmVSImpDif.FTerceiro_Cod  := Ed08Terceiro.ValueVariant;
  FmVSImpDif.FDataIniVal    := TP08DataIni.Date;
  FmVSImpDif.FDataFimVal    := TP08DataFim.Date;
  FmVSImpDif.FDataIniUsa    := Ck08DataIni.Checked;
  FmVSImpDif.FDataFimUsa    := Ck08DataFim.Checked;
  FmVSImpDif.FAgrupa        := RG08Agrupa.ItemIndex;
  FmVSImpDif.FTipoMovim_Cod := RG08TipoMovim.ItemIndex;
  FmVSImpDif.FTipoMovim_Txt := RG08TipoMovim.Items[RG08TipoMovim.ItemIndex];
  FmVSImpDif.FOrdem         := RG08Ordem.ItemIndex;
  FmVSImpDif.FImpSimples    := Ck08Simples.Checked;
  FmVSImpDif.FPeriodo       :=
      dmkPF.PeriodoImp1(TP08DataIni.Date, TP08DataFim.Date,
    Ck08DataIni.Checked, Ck08DataFim.Checked, '', 'at�', '');
  FmVSImpDif.FUsaVSMovItb   := Ck08VSMovItb.Checked;
  //
  FmVSImpDif.PesquisaFaltaDeCouros();
  //
  FmVSImpDif.Destroy;

{
  case RG08TipoMovim.ItemIndex of
    0:
    begin
      SQL_Periodo := dmkPF.SQL_Periodo('AND vmi.DataHora ', TP08DataIni.Date,
        TP08DataFim.Date, Ck08DataIni.Checked, Ck08DataFim.Checked);
      //
      Terceiro := Ed08Terceiro.ValueVariant;
      if Terceiro <> 0 then
        SQL_Terceiro := 'AND vmi.Terceiro=' + Geral.FF0(Terceiro)
      else
        SQL_Terceiro := '';
      //
      case RG08Ordem.ItemIndex of
        0: SQL_ORDEM := 'ORDER BY vmi.DataHora, vmi.Terceiro, vmi.Controle';
        1: SQL_ORDEM := 'ORDER BY vmi.Terceiro, vmi.DataHora, vmi.Controle';
        else
        begin
          SQL_ORDEM := '';
          Geral.MB_Aviso(
          'Tipo de Ordem n�o implementado para guia 08 (diferen�as)!');
        end;
      end;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrMovDif, Dmod.MyDB, [
      'SELECT DATE_FORMAT(vmi.DataHora, "%d/%m/%Y") DATA_TXT,',
      'vmd.InfPecas, vmd.InfPesoKg, vmd.InfAreaM2, ',
      'vmd.infAreaP2, vmd.infValorT, vmd.DifPecas, vmd.DifPesoKg, ',
      'vmd.DifAreaM2, vmd.DifAreaP2, vmd.DifValorT, vmi.*,',
      'vsf.Nome NO_SerieFch, CONCAT(gg1.Nome,',
      'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
      'NO_PRD_TAM_COR, vsp.Nome NO_Pallet,',
      'IF(vmi.SdoVrtPeca > 0, 0,',
      '  IF(QtdGerArM2 > 0, QtdGerArM2 / Pecas, 0)) RendKgm2,',
      'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
      '  IF(QtdGerArM2 > 0, PesoKg/QtdGerArM2, 0), 3),',
      '  ",", ""), ".", ",")) RendKgm2_TXT,',
      'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(',
      'FORMAT(NotaMPAG, 4), ",", ""), ".", ",")) NotaMPAG_TXT,',
      'IF(vmi.Misturou = 1, "SIM", "N�O") Misturou_TXT,',
      'unm.Sigla SIGLAUNIDMED, unm.Nome NOMEUNIDMED,',
      'IF(vmi.SdoVrtPeca > 0, "Incompleto", REPLACE(REPLACE(FORMAT(',
      '  IF(vmi.Pecas > 0, vmi.QtdGerArM2 / vmi.Pecas, 0), 3),',
      '  ",", ""), ".", ",")) m2_CouroTXT,',
      'IF(vmi.Pecas <> 0, vmi.PesoKg / vmi.Pecas, 0) KgMedioCouro, ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE ',
      'FROM vsmovdif vmd',
      'LEFT JOIN ' + CO_SEL_TAB_VMI + ' vmi ON vmi.Controle=vmd.Controle',
      'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
      'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet',
      'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch',
      'LEFT JOIN unidmed    unm ON unm.Codigo=gg1.UnidMed',
      'LEFT JOIN entidades ent ON ent.Codigo=vmi.Terceiro ',
      'WHERE vmd.DifPecas <> 0',
      SQL_Periodo,
      SQL_Terceiro,
      //
      SQL_ORDEM,
      '']);
      //
      /////
      ///  OS Dmk: 3139
      //Sugiro que esta janela de diferen�a de movimento de couro seja mais
      //simples;ou seja..contendo apenas as informa�ao de
      //data,fornecedo,peso,artigo e total de diferen�a !!
      if Ck08Simples.Checked then
        Report := frxWET_CURTI_018_08_B
      else
        Report := frxWET_CURTI_018_08_A;
      //
      MyObjects.frxDefineDataSets(Report, [
        DModG.frxDsDono,
        frxDsMovDif
      ]);
      ///////
      Grupo0 := Report.FindObject('GH_00') as TfrxGroupHeader;
      Grupo0.Visible := RG08Agrupa.ItemIndex = 1;
      Futer0 := Report.FindObject('FT_00') as TfrxGroupFooter;
      Futer0.Visible := Grupo0.Visible;
      Me_GH0 := Report.FindObject('Me_GH0') as TfrxMemoView;
      Me_FT0 := Report.FindObject('Me_FT0') as TfrxMemoView;
      case RG08Ordem.ItemIndex of
        0:
        begin
          Grupo0.Condition := 'frxDsMovDif."DATA_TXT"';
          Me_GH0.Memo.Text := '[frxDsMovDif."DATA_TXT"]';
          Me_FT0.Memo.Text := '[frxDsMovDif."DATA_TXT"]: ';
        end;
        1:
        begin
          Grupo0.Condition := 'frxDsMovDif."Terceiro"';
          Me_GH0.Memo.Text := '[frxDsMovDif."Terceiro"] - [frxDsMovDif."NO_FORNECE"]';
          Me_FT0.Memo.Text := '[frxDsMovDif."Terceiro"] - [frxDsMovDif."NO_FORNECE"]: ';
        end;
        else Grupo0.Condition := '***ERRO***';
      end;
      MyObjects.frxMostra(Report, 'Diferen�as de couros');
    end;
    else Geral.MB_Aviso(
    'Tipo de Movimento n�o implementado para guia 08 (diferen�as)!');
  end;
}
end;

procedure TFmVSMovImp.PesquisaFichaRMP1Click(Sender: TObject);
begin
  PesquisaFichasRMP();
end;

procedure TFmVSMovImp.PesquisaFichasRMP();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr10FichasRMP, Dmod.MyDB, [
  'SELECT vmi.SerieFch, vmi.Ficha,    ',
  '(vmi.Ficha + (vmi.SerieFch/10000)) FCH_SRE,    ',
  'COUNT(vmi.Codigo) ITENS,    ',
  'MIN(vmi.DataHora) RefFirst, MAX(vmi.DataHora) RefLast,   ',
  'vsf.Nome NO_SERIEFCH,',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE    ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi   ',
  'LEFT JOIN vsserfch vsf ON vsf.Codigo=vmi.SerieFch   ',
  'LEFT JOIN vsfchrmpcab smc ON ',
  '  smc.SerieFch=vmi.SerieFch',
  '  AND ',
  '  smc.Ficha=vmi.Ficha',
  '  AND ',
  '  smc.MovimID=vmi.MovimID',
  'LEFT JOIN entidades frn ON frn.Codigo=smc.Terceiro',
  'GROUP BY vmi.SerieFch, vmi.Ficha ',
  'ORDER BY FICHA DESC, SerieFch ',
  '']);
end;

procedure TFmVSMovImp.PesquisaIMEIS();
var
  Empresa: Integer;
  SQL_Empresa, GraGruYs, SQL_GraGruY, GraGruXs, SQL_GraGruX: String;
begin
  FVSMovImp3 :=
    UnCreateVS.RecriaTempTableNovo(ntrttVSMovImp3, DModG.QrUpdPID1, False);
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  if Empresa <> 0 then
    SQL_Empresa := 'AND wmi.Empresa=' + Geral.FF0(Empresa)
  else
    SQL_Empresa := '';
  //
  GraGruYs := VS_PF.DefineIDs_Str(DBG03GraGruY, Qr03GraGruY, 'Codigo');
  if Trim(GraGruYs) <> '' then
    SQL_GraGruY := 'AND ggx.GraGruY IN (' + GraGruYs + ') '
  else
    SQL_GraGruY := '';
  //
  GraGruXs := VS_PF.DefineIDs_Str(DBG03GraGruX, Qr03GraGruX, 'Controle');
  if Trim(GraGruXs) <> '' then
    SQL_GraGruX := 'AND ggx.Controle IN (' + GraGruXs + ') '
  else
    SQL_GraGruX := '';
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(QrEstqR3, DModG.MyPID_DB, [
  'DELETE FROM  ' + FVSMovImp3 + '; ',
  'INSERT INTO  ' + FVSMovImp3,
  'SELECT wmi.Empresa, wmi.Codigo, wmi.Controle, wmi.MovimCod, wmi.GraGruX, ',
  'wmi.Pecas, wmi.PesoKg, wmi.AreaM2, AreaP2, ValorT, ',
  'wmi.SdoVrtPeca, wmi.SdoVrtPeso, wmi.SdoVrtArM2,    ',
  'ggx.GraGru1, CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, wmi.Pallet, vsp.Nome NO_Pallet,   ',
  'IF(vsp.CliStat IS NULL, 0, vsp.CliStat) CliStat,  ',
  'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",    ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,   ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,   ',
  'vps.Nome NO_STATUS, DataHora, 0 OrdGGX,   ',
  'ggy.Ordem, ggy.Codigo, ggy.Nome, 1 Ativo   ',
  'FROM ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' wmi   ',
  'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=wmi.GraGruX   ',
  'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY   ',
  'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN ' + TMeuDB + '.vspalleta   vsp ON vsp.Codigo=wmi.Pallet   ',
  'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vsp.CliStat   ',
  'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=wmi.Empresa   ',
  'LEFT JOIN ' + TMeuDB + '.vspalsta  vps ON vps.Codigo=vsp.Status   ',
  'WHERE wmi.Controle <> 0   ',
  SQL_Empresa,
  SQL_GraGruY,
  SQL_GraGruX,
  'AND Pecas > 0 ',
  'AND SdoVrtPeca > 0 OR (SdoVrtPeso > 0 AND (Pecas=0 OR MovimID IN (' +
  CO_ALL_CODS_ESTQ_SEM_PC + '))) ',
  '']);
  //
  ReopenVSMovImp3(0, 0, 0, 0);
end;

procedure TFmVSMovImp.ImprimeHistorico();
const
  Ed01GraGruX_ValueVariant = 0;
  Ed01Pallet_ValueVariant = 0;
  Ed01Terceiro_ValueVariant = 0;
begin
  VS_PF.MostraRelatorioVSImpHistorico(Ed01GraGruX_ValueVariant, Ed01Pallet_ValueVariant, Ed01Terceiro_ValueVariant,
  (*TP01DataIni.Date, TP01DataFim.Date,*) Date - 90, Date, TPDataRelativa.Date,
  (*Ck01DataIni.Checked, Ck02DataFim.Checked,*)True, False,  CBEmpresa.Text,
  EdEmpresa,
  (*CB01GraGruX.Text, CB01Pallet.Text, CB01Terceiro.Text,*)
  PB1, LaAviso1, LaAviso2);
end;

procedure TFmVSMovImp.ImprimeListaNFesEmitidasVS();
{
  function SQL_NFes(FldSer, FldNum: String): String;
  var
    Ser, Ini, Fim: Integer;
  begin
    Ser := Ed20Serie.ValueVariant;
    Ini := Ed20NFeIni.ValueVariant;
    Fim := Ed20NFeFim.ValueVariant;
    Result :='';
    if Ck20Serie.Checked then
      Result := Result + 'AND ' + FldSer + '=' + Geral.FF0(Ser);
    if (Ini <> 0) or (Fim > Ini) then
    begin
      if Ini = Fim then
        Result := Result + 'AND ' + FldNum + ' = ' + Geral.FF0(ini)
      else
        Result := Result +
        'AND ' + FldNum + ' BETWEEN ' + Geral.FF0(ini) + ' AND ' + Geral.FF0(Fim);
    end;
  end;
}
const
  // Tem_Wrn DESC, NFe DESC, Data DESC, MovimCod DESC
  Ordens: array[0..4] of String = ('Tem_Wrn', 'NFe', 'Data', 'MovimCod', 'VSVmcSeq');
var
  //SQL_NFesA,
  SQL_Periodo, ATT_MovimID, SQL_VmcWrn, SQL_VmcSta, Direcao, Ordenacao: String;
begin
  SQL_Periodo := dmkPF.SQL_Periodo('WHERE Data',
    TP20DataIni.Date, TP20DataFim.Date, Ck20DataIni.Checked, Ck20DataFim.Checked);
  //
  ATT_MovimID := dmkPF.ArrayToTexto('MovimID', 'NO_MovimID', pvPos, True,
  sEstqMovimID);
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFes, DModG.MyPID_DB, [
  'DROP TABLE IF EXISTS ' + CO_NFes_XX + ';',
  'CREATE TABLE ' + CO_NFes_XX + '',
  '',
  'SELECT "vsoutcab" Tabela, "NFV" Campo, ',
  'cab.NFeStatus cStat, cab.SerieV Serie, cab.NFV NFe, cab.MovimCod, 2.000 MovimID, ',
  'cab.DtVenda Data, cab.Cliente Terceiro, -cab.Pecas Pecas, ',
  '-cab.PesoKg PesoKg, -cab.AreaM2 AreaM2, -ValorT ValorT',
  'FROM ' + TMeuDB + '.vsoutcab cab',
  'WHERE cab.NFV > 0',
  SQL_NFes('SerieV', 'NFV'),
  '',
  'UNION ',
  '',
  'SELECT "vsoutnfecab" Tabela, "ide_nNF" Campo, ',
  'nfe.NFeStatus cStat, nfe.ide_serie Serie, nfe.ide_nNF NFe, cab.MovimCod, ',
  '2.000 MovimID, cab.DtVenda Data, ',
  'cab.Cliente Terceiro, -cab.Pecas Pecas, ',
  '-cab.PesoKg PesoKg, -cab.AreaM2 AreaM2, -ValorT ValorT',
  'FROM ' + TMeuDB + '.vsoutnfecab nfe ',
  'LEFT JOIN ' + TMeuDB + '.vsoutcab cab ON cab.Codigo=nfe.OriCod',
  'WHERE nfe.ide_nNF <> 0 ',
  'AND nfe.ide_nNF <> cab.NFV',
  'AND cab.MovimCod=nfe.MovimCod',
  SQL_NFes('nfe.ide_serie', 'nfe.ide_nNF'),
  '',
  'UNION ',
  '',
  'SELECT "vstrfloccab" Tabela, "ide_nNF" Campo, ',
  'NFeStatus cStat, ide_serie Serie, ide_nNF NFe, MovimCod, 25.000 MovimID, ',
  'DtVenda Data, Cliente Terceiro, ',
  'Pecas, PesoKg, AreaM2, ValorT',
  'FROM ' + TMeuDB + '.vstrfloccab ',
  'WHERE ide_nNF > 0',
  SQL_NFes('ide_serie', 'ide_nNF'),
  '',
  'UNION',
  '',
  'SELECT "vsoutnfecab" Tabela, "ide_nNF" Campo, ',
  'nfe.NFeStatus cStat, nfe.ide_serie Serie, nfe.ide_nNF NFe, cab.MovimCod, ',
  '25.000 MovimID, cab.DtVenda Data, ',
  'cab.Cliente Terceiro, cab.Pecas Pecas, ',
  'cab.PesoKg PesoKg, cab.AreaM2 AreaM2, ValorT ValorT',
  'FROM ' + TMeuDB + '.vsoutnfecab nfe ',
  'LEFT JOIN ' + TMeuDB + '.vstrfloccab cab ON cab.Codigo=nfe.OriCod',
  'WHERE nfe.ide_nNF <> 0 ',
  'AND cab.MovimCod=nfe.MovimCod',
  SQL_NFes('nfe.ide_serie', 'nfe.ide_nNF'),
  '',
  'UNION ',
  '',
  'SELECT "vsoutnfecab" Tabela, "ide_nNF" Campo, ',
  'nfe.NFeStatus cStat, nfe.ide_serie Serie, nfe.ide_nNF NFe, cab.MovimCod, ',
  '6.000 MovimID, cab.DtHrAberto Data, ',
  'vmi.FornecMO Terceiro, Pecas, PesoKg, AreaM2, ValorT',
  'FROM ' + TMeuDB + '.vsoutnfecab nfe ',
  'LEFT JOIN ' + TMeuDB + '.vsgerarta cab ON cab.Codigo=nfe.OriCod',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  '  ON vmi.MovimCod=cab.MovimCod ',
  'WHERE vmi.MovimNiv=13 AND vmi.MovimID=6',
  'AND cab.MovimCod=nfe.MovimCod',
  SQL_NFes('nfe.ide_serie', 'nfe.ide_nNF'),
  '',
  'UNION ',
  '',
  'SELECT "vsopecab" Tabela, "NFeRem" Campo, ',
  'cab.NFeStatus cStat, cab.SerieRem Serie, cab.NFeRem NFe, cab.MovimCod, vmi.MovimID, ',
  'cab.DtHrAberto Data, vmi.FornecMO Terceiro, ',
  '-cab.PecasSrc Pecas, -cab.PesoKgSrc PesoKg, ',
  '-cab.AreaSrcM2 AreaM2, -ValorTSrc ValorT',
  'FROM ' + TMeuDB + '.vsopecab cab',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  '  ON vmi.MovimCod=cab.MovimCod ',
  'WHERE vmi.MovimNiv=8 AND vmi.MovimID=11',
  'AND cab.NFeRem >0',
  SQL_NFes('SerieRem', 'NFeRem'),
  ' ',
  'UNION ',
  ' ',
  'SELECT "vspwecab" Tabela, "NFeRem" Campo, ',
  'cab.NFeStatus cStat, cab.SerieRem Serie, cab.NFeRem NFe, cab.MovimCod, vmi.MovimID, ',
  'cab.DtHrAberto Data, vmi.FornecMO Terceiro, ',
  '-cab.PecasSrc Pecas, -cab.PesoKgSrc PesoKg, ',
  '-cab.AreaSrcM2 AreaM2, -ValorTSrc ValorT ',
  'FROM ' + TMeuDB + '.vspwecab cab',
  'LEFT JOIN ' + TMeuDB + '.' + CO_SEL_TAB_VMI + ' vmi ',
  '  ON vmi.MovimCod=cab.MovimCod ',
  'WHERE vmi.MovimNiv=21 AND vmi.MovimID=19',
  'AND cab.NFeRem >0 ',
  SQL_NFes('SerieRem', 'NFeRem'),
  '',
  'UNION ',
  ' ',
  'SELECT "vsinncab" Tabela, "ide_nNF" Campo, ',
  'NFeStatus cStat, ide_serie Serie, ide_nNF NFe, MovimCod, 1.000 MovimID, ',
  'DtCompra Data, Fornecedor Terceiro, ',
  'Pecas, PesoKg, AreaM2, ValorT',
  'FROM ' + TMeuDB + '.vsinncab ',
  'WHERE ide_nNF > 0',
  SQL_NFes('ide_serie', 'ide_nNF'),
  '',
  ' ',
  'UNION ',
  ' ',
  'SELECT "vsplccab" Tabela, "ide_nNF" Campo,  ',
  'NFeStatus cStat, ide_serie Serie, ide_nNF NFe, MovimCod, 16.000 MovimID, ',
  'DtCompra Data, Fornecedor Terceiro, ',
  'Pecas, PesoKg, AreaM2, ValorT',
  'FROM ' + TMeuDB + '.vsplccab ',
  'WHERE ide_nNF > 0',
  SQL_NFes('ide_serie', 'ide_nNF'),
  ' ',
  'UNION ',
  ' ',
  'SELECT "vsdvlcab" Tabela, "ide_nNF" Campo,  ',
  'NFeStatus cStat, ide_serie Serie, ide_nNF NFe, MovimCod, 21.000 MovimID, ',
  'DtCompra Data, Fornecedor Terceiro, ',
  'Pecas, PesoKg, AreaM2, ValorT',
  'FROM ' + TMeuDB + '.vsdvlcab ',
  'WHERE ide_nNF > 0',
  SQL_NFes('ide_serie', 'ide_nNF'),
  ' ',
  'UNION ',
  ' ',
  'SELECT "vsrtbcab", "ide_nNF" Campo,  ',
  'NFeStatus cStat, ide_serie Serie, ide_nNF NFe, MovimCod, 22.000 MovimID, ',
  'DtCompra Data, Fornecedor Terceiro, ',
  'Pecas, PesoKg, AreaM2, ValorT',
  'FROM ' + TMeuDB + '.vsrtbcab ',
  'WHERE ide_nNF > 0',
  SQL_NFes('ide_serie', 'ide_nNF'),
  '',
  '',
  'UNION ',
  ' ',
  'SELECT "vsinvnfe", "ide_nNF" Campo,   ',
  'NFeStatus cStat, ide_serie Serie, ide_nNF NFe, MovimCod,  ',
  'MovimID + 0.000 MovimID, Data, Terceiro,  ',
  '0.000 Pecas, 0.000 PesoKg, 0.00 AreaM2, 0.00 ValorT ',
  'FROM ' + TMeuDB + '.vsinvnfe  ',
  'WHERE ide_nNF > 0 ',
  ' ',
  'ORDER BY NFe DESC  ',
  ' ',
  '; ',
  '',
  'SELECT nfm.Nome NO_cStat, nfe.*, ',
  ATT_MovimID,
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
  'FROM ' + CO' + CO_NFes_XX + 'VS + ' nfe',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=nfe.Terceiro',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.nfemsg nfm ON nfm.Codigo=nfe.cStat ',
  SQL_Periodo,
  'ORDER BY NFe DESC, Data DESC',
  ' ',
  '']);
  //
  //Geral.MB_SQL(Self, QrNFes);
}
  SQL_VmcWrn := '';
  if (CG20VmcWrn.Value > 0) and (CG20VmcWrn.Value < CG20VmcWrn.MaxValue) then
  begin
    if CG20VmcWrn.Checked[0] then
      SQL_VmcWrn := SQL_VmcWrn + ',0';
    if CG20VmcWrn.Checked[1] then
      SQL_VmcWrn := SQL_VmcWrn + ',16';
    if CG20VmcWrn.Checked[2] then
      SQL_VmcWrn := SQL_VmcWrn + ',32';
    if CG20VmcWrn.Checked[3] then
      SQL_VmcWrn := SQL_VmcWrn + ',48';
    if CG20VmcWrn.Checked[4] then
      SQL_VmcWrn := SQL_VmcWrn + ',64';
    SQL_VmcWrn := 'AND nfe.vsvmcwrn IN (' + Copy(SQL_VmcWrn, 2) + ')';
    //
    if (CG20VmcSta.Value > 0) and (CG20VmcSta.Value < CG20VmcSta.MaxValue) then
    begin
      case CG20VmcSta.Value of
        1: SQL_VmcWrn := SQL_VmcWrn + ' AND nfe.VSVmcSta=1';
        2: SQL_VmcWrn := SQL_VmcWrn + ' AND nfe.VSVmcSta=0';
        else Geral.MB_Erro('"VmcSta" n�o implementado!');
      end;
    end;
  end;
  //
  SQL_VmcSta := '';
  if (CG20VmcSta.Value > 0) and (CG20VmcSta.Value < CG20VmcSta.MaxValue) then
  begin
    case CG20VmcSta.Value of
      //1: SQL_VmcSta := 'AND nfe.VSVmcSta=0';
      1: SQL_VmcSta := '';
      //2: SQL_VmcSta := 'AND nfe.VSVmcSta=1';
      2: SQL_VmcSta := 'AND (VSVmcSta=0)';
      else Geral.MB_Erro('"VmcSta" n�o implementado!');
    end;
  end;
  //  'ORDER BY Tem_Wrn DESC, NFe DESC, Data DESC, MovimCod DESC']));
  case RG20Direcao.ItemIndex of
    0: Direcao := '';
    1: Direcao := ' DESC';
    2: Direcao := ' ????';
  end;
  Ordenacao := 'ORDER BY ' +
    Ordens[RG20Ordem1.ItemIndex] + Direcao + ',' +
    Ordens[RG20Ordem2.ItemIndex] + Direcao + ',' +
    Ordens[RG20Ordem3.ItemIndex] + Direcao + ',' +
    Ordens[RG20Ordem4.ItemIndex] + Direcao + ', NFe' + Direcao;
  VS_PF.AbreSQLListaNFesEmitidasVS(QrNFes, Ck20Serie.Checked,
  Ed20Serie.ValueVariant, Ed20NFeIni.ValueVariant, Ed20NFeFim.ValueVariant,
  //TP20DataIni.Date, TP20DataFim.Date, Ck20DataIni.Checked, Ck20DataFim.Checked,
  Geral.ATS(['SELECT nfm.Nome NO_cStat, wrn.Nome NO_VmcWarn, nfe.*, ',
  ATT_MovimID,
  'IF((VSVmcWrn > 0) ' + SQL_VmcSta + ', 1, 0) Tem_Wrn, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
  'FROM ' + CO_NFes_XX + ' nfe',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=nfe.Terceiro',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.nfemsg nfm ON nfm.Codigo=nfe.cStat ',
  'LEFT JOIN ' + TMeuDB + '.vsvmcwrn wrn ON wrn.Codigo=nfe.vsvmcwrn ',
  SQL_Periodo,
  SQL_VmcWrn,
  Ordenacao]));
  //Geral.MB_SQL(self, QrNFes);
end;

procedure TFmVSMovImp.ImprimeNFRetMOIndef();
var
  Empresa: Integer;
begin
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  //
  if Ck23Indef.Checked then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr23VSMORet, Dmod.MyDB, [
    'SELECT Codigo, NFCMO_Pecas, NFCMO_PesoKg, ',
    'NFCMO_AreaM2, VSVMI_Controle, VSVMI_Codigo, ',
    'VSVMI_MovimID, VSVMI_MovimNiv, VSVMI_MovimCod, ',
    'VSVMI_SerNF, VSVMI_nNF  ',
    'FROM vsmoenvret ',
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND ( ',
    '   NFCMO_FatID<1 ',
    'OR NFCMO_FatNum<1 ',
    'OR NFCMO_nNF<1 ',
    'OR NFRMP_FatID<1 ',
    'OR NFRMP_FatNum<1 ',
    'OR NFRMP_nNF<1 ',
    ') ',
    '']);
  end else
    Geral.MB_Aviso(
    '"ImprimeNFRetMOIndef" n�o implementado para "Ck23Indef.UnChecked"');
end;

procedure TFmVSMovImp.ImprimeNotaMPAG();
begin
  VS_PF.ImprimeNotaMPAG(RG14Periodo.ItemIndex,
  RG14FontePsq.ItemIndex, Ed14Terceiro.ValueVariant, Ed14SerieFch.ValueVariant,
  Ed14Ficha.ValueVariant, RG14Ordem1.ItemIndex, RG14Ordem2.ItemIndex,
  RG14Ordem3.ItemIndex, Ed14GraGruX.ValueVariant, RG14Agrupa.ItemIndex,
  TP14DataIni.Date, TP14DataFim.Date, Ck14DataIni.Checked,
  Ck14DataFim.Checked, Ck14SoMPAGNo0.Checked, Ck14Cor.Checked,
  EdEmpresa, CB14SerieFch, CBEmpresa.Text,
  CB14Terceiro.Text, Ed14Marca.Text, CB14GraGruX.Text);
end;

procedure TFmVSMovImp.ImprimeOrdens();
var
  MovimIDs: array of TEstqMovimID;
  procedure Add(MovID: TEstqMovimID);
  var
    I: Integer;
  begin
    I := Length(MovimIDs);
    SetLength(MovimIDs, I + 1);
    MovimIDs[I] := MovID;
  end;
begin
  SetLength(MovimIDs, 0);
{
  begin
    if CG18Ordens.Value > 0 then
    begin
      if dmkPF.IntInConjunto2(1, CG18Ordens.Value) then Add(emidEmOperacao);
      if dmkPF.IntInConjunto2(2, CG18Ordens.Value) then Add(emidEmProcWE);
      if Length(MovimIDs) > 0 then
  (*
        0: VS_PF.ImprimeOXsAbertas(RG18Ordem1.ItemIndex, RG18Ordem2.ItemIndex,
           RG18Ordem3.ItemIndex, RG18Agrupa.ItemIndex, emidEmOperacao);
        1: VS_PF.ImprimeOXsAbertas(RG18Ordem1.ItemIndex, RG18Ordem2.ItemIndex,
           RG18Ordem3.ItemIndex, RG18Agrupa.ItemIndex, emidEmProcWE);
        else Geral.MB_Erro('Aba indefinida para Rel 18!');
      end;
  *)
      VS_PF.ImprimeOXsAbertas(RG18Ordem1.ItemIndex, RG18Ordem2.ItemIndex,
           RG18Ordem3.ItemIndex, RG18Agrupa.ItemIndex, MovimIDs, Ck18Cor.Checked);
    end else
      Geral.MB_Erro('Defina pelo menos um tipo de Ordem!');

  end;
}
  if CG18Ordens.Value > 0 then
  begin
    if dmkPF.IntInConjunto2(1, CG18Ordens.Value) then Add(emidEmOperacao);
    if dmkPF.IntInConjunto2(2, CG18Ordens.Value) then Add(emidEmProcWE);
    VS_PF.ImprimeOXsAbertas(RG18Ordem1.ItemIndex, RG18Ordem2.ItemIndex,
      RG18Ordem3.ItemIndex, RG18Agrupa.ItemIndex, MovimIDs, Ck18Cor.Checked,
      Ck18Especificos.Checked, Ed18Especificos.Text, Ed18Fornecedor.ValueVariant);
  end;
end;

procedure TFmVSMovImp.ImprimePackLists(Vertical: Boolean; VSMovImp4,
  VSLstPalBox: String; Grandeza: TGrandezaArea);
var
  I, N, Empresa: Integer;
  MyArr: array of Integer;
begin


  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  N := 0;
  with DBG04Estq.DataSource.DataSet do
  for I := 0 to DBG04Estq.SelectedRows.Count-1 do
  begin
    //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
    GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
    N := N + 1;
    SetLength(MyArr, N);
    MyArr[N - 1] := QrEstqR4Pallet.Value;
  end;
  if N > 0 then
    VS_PF.ImprimePackListsPallets(Empresa, MyArr, Vertical, VSMovImp4,
    VSLstPalBox, Grandeza)
  else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmVSMovImp.ImprimePallets(frxReport: TfrxReport; Titulo: String);
const
  Ordens: array[0..6] of String = ('Pallet', 'NO_PRD_TAM_COR', 'NO_FORNECE',
  'Media', 'NO_PalStat', 'NO_CouNiv2', 'NO_CouNiv1');
var
  SQL, Campo, Ordem, TitGru: String;
begin
  SQL := SQL_PalletsReal();
  if SQL <> '' then
  begin
    Ordem := 'ORDER BY ' + Ordens[RG04Ordem1.ItemIndex] + ', ' +
      Ordens[RG04Ordem2.ItemIndex] + ', Pallet DESC, OrdGGX ';
    UnDmkDAC_PF.AbreMySQLQuery0(QrGGXPalTer, DModG.MyPID_DB, [
(*
    'SELECT mi4.Empresa, mi4.GraGruX, SUM(mi4.Pecas) Pecas, SUM(mi4.PesoKg) PesoKg, ',
    'Sum(mi4.AreaM2) AreaM2, SUM(mi4.AreaP2) AreaP2, SUM(mi4.ValorT) ValorT,  ',
    'SUM(mi4.SdoVrtPeca) SdoVrtPeca, SUM(mi4.SdoVrtPeso) SdoVrtPeso,  ',
    'Sum(mi4.SdoVrtArM2) SdoVrtArM2, ',
    '',
    'SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca) PalVrtPeca,  ',
    'SUM(mi4.SdoVrtPeso+mi4.LmbVrtPeso) PalVrtPeso, ',
    'SUM(mi4.LmbVrtArM2+mi4.SdoVrtArM2) PalVrtArM2,  ',
    '',
    'SUM(mi4.LmbVrtPeca) LmbVrtPeca,  ',
    'SUM(mi4.LmbVrtPeso) LmbVrtPeso, SUM(mi4.LmbVrtArM2) LmbVrtArM2,  ',
    '',
    '',
    'mi4.GraGru1, mi4.NO_PRD_TAM_COR, mi4.Pallet, mi4.NO_PALLET, mi4.Terceiro, ',
    'mi4.CliStat, mi4.Status, mi4.NO_FORNECE,  mi4.NO_CLISTAT, mi4.NO_EMPRESA, ',
    'mi4.NO_STATUS, mi4.DataHora, mi4.OrdGGX, mi4.OrdGGY, mi4.GraGruY, ',
    'mi4.NO_GGY, IF(plb.MontPalt IS NULL, SUM(DISTINCT(mi4.PalStat)), 4) PalStat, ',
    'ELT(IF(plb.MontPalt IS NULL, SUM(DISTINCT(mi4.PalStat)), 4) + 1, "Encerrado", ',
    '"Removido", "Desmontando", "Remo+Desmo", "Montando", "Multi[5+]") ',
    'NO_PalStat, mi4.Ativo,',
    'IF(SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca)  <> 0, ',
    'SUM(mi4.LmbVrtArM2+mi4.SdoVrtArM2) / SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca), 0) ',
    'Media, CouNiv2, NO_CouNiv2, CouNiv1, NO_CouNiv1, ',
    'SUM(SdoInteiros + LmbInteiros) Inteiros, ',
    'IF(plb.MontPalt IS NULL, 0.000, 4.000) MontPalt ',
    'FROM  ' + FVSMovImp4 + ' mi4',
    'WHERE (mi4.SdoVrtPeca > 0 OR mi4.LmbVrtPeca <> 0)  ',
    'AND mi4.Pallet IN (' + SQL + ') ',
    'GROUP BY mi4.Pallet ',
    Ordem,
*)
    'SELECT mi4.Empresa, mi4.GraGruX, SUM(mi4.Pecas) Pecas, SUM(mi4.PesoKg) PesoKg, ',
    'Sum(mi4.AreaM2) AreaM2, SUM(mi4.AreaP2) AreaP2, SUM(mi4.ValorT) ValorT, ',
    'SUM(mi4.SdoVrtPeca) SdoVrtPeca, SUM(mi4.SdoVrtPeso) SdoVrtPeso, ',
    'Sum(mi4.SdoVrtArM2) SdoVrtArM2, ',
    'SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca) PalVrtPeca, ',
    'SUM(mi4.SdoVrtPeso+mi4.LmbVrtPeso) PalVrtPeso, ',
    'SUM(mi4.LmbVrtArM2+mi4.SdoVrtArM2) PalVrtArM2, ',
    'SUM(mi4.LmbVrtPeca) LmbVrtPeca, ',
    'SUM(mi4.LmbVrtPeso) LmbVrtPeso, SUM(mi4.LmbVrtArM2) LmbVrtArM2, ',
    'mi4.GraGru1, mi4.NO_PRD_TAM_COR, mi4.Pallet, mi4.NO_PALLET, mi4.Terceiro, ',
    'mi4.CliStat, mi4.Status, mi4.NO_FORNECE,  mi4.NO_CLISTAT, mi4.NO_EMPRESA, ',
    'mi4.NO_STATUS, mi4.DataHora, mi4.OrdGGX, mi4.OrdGGY, mi4.GraGruY, ',
    'mi4.NO_GGY, StatPall PalStat, ',
    'NO_StatPall NO_PalStat, mi4.Ativo, ',
    'IF(SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca)  <> 0, ',
    'SUM(mi4.LmbVrtArM2+mi4.SdoVrtArM2) / SUM(mi4.SdoVrtPeca+mi4.LmbVrtPeca), 0) ',
    'Media, CouNiv2, NO_CouNiv2, CouNiv1, NO_CouNiv1, ',
    'SUM(SdoInteiros + LmbInteiros) Inteiros ',
    'FROM  ' + FVSMovImp4 + ' mi4',
    'WHERE (mi4.SdoVrtPeca > 0 OR mi4.LmbVrtPeca <> 0 OR (mi4.SdoVrtPeso > 0 AND (mi4.Pecas=0 OR mi4.MovimID IN ('
    + CO_ALL_CODS_ESTQ_SEM_PC + '))) )  ',
    'AND mi4.Pallet IN (' + SQL + ') ',
    'GROUP BY mi4.Pallet ',
    Ordem,
    '']);

    //
    MyObjects.frxDefineDataSets(frxReport, [
      DModG.frxDsDono,
      frxDsGGXPalTer
    ]);
    //
    Campo  := Ordens[RG04Ordem1.ItemIndex];
    TitGru := RG04Ordem1.Items[RG04Ordem1.ItemIndex];
    frxReport.Variables['VARF_GRUPO1']   := ''''+'frxDsGGXPalTer."'+Campo+'"''';
    frxReport.Variables['VARF_HEADR1']   := ''''+TitGru+': [frxDsGGXPalTer."'+Campo+'"]''';
    frxReport.Variables['VARF_FOOTR1']   := ''''+TitGru+': [frxDsGGXPalTer."'+Campo+'"]''';
    //
    Campo  := Ordens[RG04Ordem2.ItemIndex];
    TitGru := RG04Ordem2.Items[RG04Ordem2.ItemIndex];
    frxReport.Variables['VARF_GRUPO2']   := ''''+'frxDsGGXPalTer."'+Campo+'"''';
    frxReport.Variables['VARF_HEADR2']   := ''''+TitGru+': [frxDsGGXPalTer."'+Campo+'"]''';
    frxReport.Variables['VARF_FOOTR2']   := ''''+TitGru+': [frxDsGGXPalTer."'+Campo+'"]''';
    //
    MyObjects.frxMostra(frxReport, Titulo);
  end;
end;

procedure TFmVSMovImp.ImprimeRendimentoSemi();
begin
(*
  VS_PF.ImprimeRendimentoSemi(EdEmpresa.ValueVariant, Ed21Terceiro.ValueVariant,
  Ed21OPIni.ValueVariant, Ed21OPFim.ValueVariant, CG21Bastidao.Value,
  RG21Agrupa.ItemIndex, RG21Ordem1.ItemIndex, RG21Ordem2.ItemIndex,
  RG21Ordem3.ItemIndex, RG21Ordem4.ItemIndex, CBEmpresa.Text, CB21Terceiro.Text,
  TP21DataIni.Date, TP21DataFim.Date, Ck21DataIni.Checked, Ck21DataFim.Checked,
  Ck21OPIni.Checked, Ck21OPFim.Checked, DBG21CouNiv2, Qr21CouNiv2);
*)
  //VS_PF.ImprimeRendimentoIMEIS([3363], [True], [True], [1], [1298]); //NF1298 JBS
  VS_PF.ImprimeRendimentoIMEIS([Ed21ImeiIni.ValueVariant], [False], [False], [], []); //NF1298 JBS
end;

procedure TFmVSMovImp.ImprimeRequisicaoDeMovimentacao();
begin
(*
  Application.CreateForm(TFmVSImpRequis, FmVSImpRequis);
  FmVSImpRequis.FEmpresa_Cod   := EdEmpresa.ValueVariant;
  FmVSImpRequis.FEmpresa_Txt   := CBEmpresa.Text;
  FmVSImpRequis.FResponsa      := Ed17Responsa.Text;
  //
  FmVSImpRequis.FQuantidade    :=  Ed17Qtde.ValueVariant;
  FmVSImpRequis.FNumInicial    :=  Ed17NumIni.ValueVariant;
  //
  FmVSImpRequis.ImprimeRequisicoes();
  //
  FmVSImpRequis.Destroy;
*)
  Application.CreateForm(TFmVSImpRequis, FmVSImpRequis);
  FmVSImpRequis.FEmpresa_Cod   := EdEmpresa.ValueVariant;
  FmVSImpRequis.FEmpresa_Txt   := CBEmpresa.Text;
  //FmVSImpRequis.FResponsa      := Ed17Responsa.Text;
  //
  //FmVSImpRequis.FQuantidade    :=  Ed17Qtde.ValueVariant;
  //FmVSImpRequis.FNumInicial    :=  Ed17NumIni.ValueVariant;
  //
  FmVSImpRequis.FTP17DataIni_Date    := TP17DataIni.Date;
  FmVSImpRequis.FTP17DataFim_Date    := TP17DataFim.Date;
  FmVSImpRequis.FCk17DataIni_Checked := Ck17DataIni.Checked;
  FmVSImpRequis.FCk17DataFim_Checked := Ck17DataFim.Checked;

  FmVSImpRequis.ImprimeListaRequisicoes(Ck17VsMovItb.Checked);
  //
  FmVSImpRequis.Destroy;
end;

procedure TFmVSMovImp.Informaesdomovimento1Click(Sender: TObject);
(*
var
  VSVmcWrn, MovimCod, NFe: Integer;
  VSVmcObs, Tabela, Campo: String;
  EstqMovimID: TEstqMovimID;
begin
  EstqMovimID := TEstqMovimID(Trunc(QrNFesMovimID.Value));
  VSVmcWrn    := QrNFesVSVmcWrn.Value;
  if VS_PF.SelecionaStatusVSVmcWrn(VSVmcWrn) then
  begin
    Tabela   := QrNFesTabela.Value;
    Campo    := QrNFesCampo.Value;
    MovimCod := QrNFesMovimCod.Value;
    NFe      := QrNFesNFe.Value;
    VSVmcObs := QrNFesVSVmcObs.Value;
    InputQuery('Hist�rico de Status de Movimento', 'Hist�rico', VSVmcObs);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
    'VSVmcWrn', 'VSVmcObs'], ['MovimCod', Campo], [
    VSVmcWrn, VSVmcObs], [MovimCod, NFe], True) then
    begin
      UnDmkDAC_PF.AbreQuery(QrNFes, Dmod.MyDB);
      QrNFes.Locate('MovimCod', MovimCod, []);
    end;
  end;
*)
begin
  VS_PF.SelecionaStatusVSVmcWrn(QrNFes);
end;

procedure TFmVSMovImp.Irparaajaneladepallets1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPallet(QrEstqR4Pallet.Value);
end;

procedure TFmVSMovImp.IrparajaneladegerenciamentodeFichasRMP1Click(
  Sender: TObject);
var
  SerieFch, Ficha: Integer;
begin
  SerieFch := Qr10FichasRMPSerieFch.Value;
  Ficha    := Qr10FichasRMPFicha.Value;
  VS_PF.MostraFormVSFchGerCab(SerieFch, Ficha);
end;

procedure TFmVSMovImp.IrparajaneladeOC1Click(Sender: TObject);
begin
  case Trunc(DmModVS.Qr16FatoresClaRcl.Value) of
    1: VS_PF.MostraFormVSGerClaCab(DmModVS.Qr16FatoresIDGeracao.Value);
    2: VS_PF.MostraFormVSGerRclCab(DmModVS.Qr16FatoresCodigoClaRcl.Value, 0);
    else Geral.MB_Erro('Tipo "ClaRcl" n�o implementado!');
  end;
end;

procedure TFmVSMovImp.Irparajaneladepalletdedestino1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPallet(Trunc(DmModVS.Qr16FatoresPallet_Dst.Value));
end;

procedure TFmVSMovImp.Irparajaneladepalletdeorigem1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSPallet(Trunc(DmModVS.Qr16FatoresPallet_Src.Value));
end;

procedure TFmVSMovImp.IrparajeneladoIMEIselecionado1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSMovIts(Qr13VSSeqItsControle.Value);
end;

procedure TFmVSMovImp.Janeladomovimento1Click(Sender: TObject);
begin
  JanelaMovimentoID();
end;

procedure TFmVSMovImp.JanelaMovimentoID();
var
  MovimID, MovimCod, Codigo, Controle: Integer;
  Qry: TmySQLQuery;
begin
  MovimID  := Trunc(QrNFesMovimID.Value);
  if Lowercase(QrNFesTabela.Value) = 'vsinvnfe' then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM vsinvnfe ',
      'WHERE MovimCod=' + Geral.FF0(QrNFesMovimCod.Value),
      '']);
      //
      MovimCod := Qry.FieldByName('MovCodOri').AsInteger;
      if MovimCod = 0 then
      begin
        Geral.MB_Aviso('NFe avulsa sem IME-C atrelado!');
        Codigo := Qry.FieldByName('Codigo').AsInteger;
        VS_PF.MostraFormVSInvNFe(Codigo);
        //
        Exit;
      end;
    finally
      Qry.Free;
    end;
  end else
    MovimCod := QrNFesMovimCod.Value;
  if MovimCod <> 0 then
  begin
    Codigo   := VS_PF.ObtemCodigoDeMovimCod(MovimCod);
    Controle := 0;
    VS_PF.MostraFormVS_XXX(MovimID, Codigo, Controle);
  end else
    Geral.MB_Erro('MovimCod indefinido!');
end;

procedure TFmVSMovImp.Lista1Click(Sender: TObject);
begin
  ImprimePallets(frxWET_CURTI_018_03_B2, 'Lista de Pallets');
end;

procedure TFmVSMovImp.ListacomNota1Click(Sender: TObject);
begin
  FNotaRevisor := 1; // Nota
  ImprimePallets(frxWET_CURTI_018_03_B3, 'Lista de Pallets com nota VS');
end;

procedure TFmVSMovImp.ListacomRevisor1Click(Sender: TObject);
begin
  FNotaRevisor := 2; // Revisor
  ImprimePallets(frxWET_CURTI_018_03_B3, 'Lista de Pallets com nota VS');
end;

procedure TFmVSMovImp.ListacomRevisor2Click(Sender: TObject);
var
  I, N, Empresa: Integer;
  MyArr: TPallArr;
begin
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  VS_PF.ArrayDePallets(TDBGrid(DBG07Estq), Qr07PrevPalVSPallet, MyArr);
  if Length(MyArr) > 0 then
    VS_PF.ImprimeHistPall(Empresa, CBEmpresa.Text, MyArr)
  else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmVSMovImp.MostraMovimento();
  procedure ObtemCodigoControleDeOrigem(const SrcMovID, SrcNivel1, SrcNivel2:
  Integer; var Codigo, Controle: Integer; var MovimID: TEstqMovimID);
  var
    Qry: TmySQLQuery;
  begin
    Qry := TmySQLQuery.Create(Self);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Codigo, Controle, MovimID ',
      'FROM ' + CO_SEL_TAB_VMI,
      'WHERE MovimCod=' + Geral.FF0(SrcMovID),
      'AND Codigo=' + Geral.FF0(SrcNivel1),
      'AND Controle=' + Geral.FF0(SrcNivel2),
      '',
      'UNION',
      '',
      'SELECT Codigo, Controle, MovimID ',
      'FROM ' + CO_TAB_VMB,
      'WHERE MovimCod=' + Geral.FF0(SrcMovID),
      'AND Codigo=' + Geral.FF0(SrcNivel1),
      'AND Controle=' + Geral.FF0(SrcNivel2),
      '']);
      //
      Codigo   := Qry.FieldByName('Codigo').AsInteger;
      Controle := Qry.FieldByName('Controle').AsInteger;
      MovimID  := TEstqMovimID(Qry.FieldByName('MovimID').AsInteger);
    finally
      Qry.Free;
    end;
  end;
var
  Codigo, Controle, SrcMovID, SrcNivel1, SrcNivel2, Reclasif, LocCtrl: Integer;
  Qry: TmySQLQuery;
  MovimID: TEstqMovimID;
begin
  Codigo    := QrVSMovItsCodigo.Value;
  Controle  := QrVSMovItsControle.Value;
  SrcMovID  := QrVSMovItsSrcMovID.Value;
  SrcNivel1 := QrVSMovItsSrcNivel1.Value;
  SrcNivel2 := QrVSMovItsSrcNivel2.Value;
  Reclasif  := QrVSMovItsControle.Value;
  LocCtrl   := QrVSMovItsControle.Value;
  //
  MovimID := TEstqMovimID(QrVSMovItsMovimID.Value);
  if MovimID = emidReclasWE then
    ObtemCodigoControleDeOrigem(
        SrcMovID, SrcNivel1, SrcNivel2, Codigo, Controle, MovimID);
  VS_PF.MostraFormVSMovimID(MovimID);
  //
  UnDmkDAC_PF.AbreQuery(QrVSMovIts, Dmod.MyDB);
  QrVSMovIts.Locate('Controle', LocCtrl, []);
end;

procedure TFmVSMovImp.NFeavulsa1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSInvNFe(QrNFesMovimCod.Value);
  ImprimeListaNFesEmitidasVS();
end;

procedure TFmVSMovImp.odos1Click(Sender: TObject);
begin
  CG08_01_MovimIDSign.SetMaxValue;
end;

procedure TFmVSMovImp.otalizarporfornecedor1Click(Sender: TObject);
var
  I, N, Empresa: Integer;
  MyArr: TPallArr;
begin
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  VS_PF.ArrayDePallets(TDBGrid(DBG04Estq), QrEstqR4Pallet, MyArr);
  if Length(MyArr) > 0 then
    VS_PF.ImprimeIMEIsPallets(Empresa, CBEmpresa.Text, MyArr, 2)
  else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmVSMovImp.otalizarporPallet1Click(Sender: TObject);
var
  I, N, Empresa: Integer;
  MyArr: TPallArr;
begin
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  VS_PF.ArrayDePallets(TDBGrid(DBG04Estq), QrEstqR4Pallet, MyArr);
  if Length(MyArr) > 0 then
    VS_PF.ImprimeIMEIsPallets(Empresa, CBEmpresa.Text, MyArr, 1)
  else
    Geral.MB_Aviso('Nenhum Pallet foi selecionado!');
end;

procedure TFmVSMovImp.PesquisaLancamentos();
var
  DataI, SQL_Periodo, SQL_Terceiro, SQL_GraGruX, SQL_Pallet, SQL_Controle,
  ATT_MovimID, SQL_MovimID: String;
  Empresa, GraGruX, Pallet: Integer;
  //
  DataHora, TxtItem: String;
  AcumPecas, AcumPesoKg, AcumAreaM2, AcumAreaP2, AcumValorT: Double;
  Controle: Integer;
  //
  procedure ReopenVSMovIts();
  const
    TemIMEIMrt = 1;
  var
    SQL_Flds, SQL_Left, SQL_Wher, SQL_Group: String;
  begin
    SQL_Flds := Geral.ATS([
    VS_PF.SQL_NO_GGX(),
    ATT_MovimID,
    'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE, ',
    'vsf.Nome NO_SerieFch, ',
    'vsp.Nome NO_Pallet, ',
    'IF(Pecas>0, AreaM2/Pecas, 0) MediaArM2, ',
    'IF(Pecas>0, PesoKg/Pecas, 0) MediaPeso ',
    '']);
    SQL_Left := Geral.ATS([
    VS_PF.SQL_LJ_GGX(),
    'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
    'LEFT JOIN vsserfch   vsf ON vsf.Codigo=vmi.SerieFch ',
    'LEFT JOIN entidades  frn ON frn.Codigo=vmi.Terceiro',
    '']);
    SQL_Wher := Geral.ATS([
    'WHERE vmi.Empresa=' + Geral.FF0(Empresa),
    SQL_GraGruX,
    SQL_Periodo,
    SQL_Terceiro,
    SQL_Pallet,
    SQL_Controle,
    SQL_MovimID,
    '']);
    SQL_Group := '';
    UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
    VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwB, TemIMEIMrt),
    VS_PF.GeraSQLVSMovItx_IMEI(CO_SELECT_, SQL_Flds, SQL_Left, SQL_Wher, SQL_Group, ttwA),
    'ORDER BY NO_Pallet, Controle ',
    '']);
    //
    //Geral.MB_SQL(Self, QrVSMovIts);
    QrVSMovIts.Locate('Controle', Controle, []);
    //
  end;
begin
  ATT_MovimID := dmkPF.ArrayToTexto('vmi.MovimID', 'NO_MovimID', pvPos, True,
    sEstqMovimID);
  //
  GraGruX := Ed02GraGruX.ValueVariant;
  if GraGruX = 0 then
    SQL_GraGruX := ''
  else
    SQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX);
  //
  (*Empresa*) DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  if MyObjects.FIC(Empresa = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  DataI := Geral.FDT(TP02DataIni.Date, 1);
  SQL_Periodo := dmkPF.SQL_Periodo('AND vmi.DataHora ',
    TP02DataIni.Date, TP02DataFim.Date, Ck02DataIni.Checked, Ck02DataFim.Checked);
  //
  VS_PF.DefineSQLTerceiro(False, Ed02Terceiro, SQL_Terceiro);
  VS_PF.DefineSQLPallet(False, Ed02Pallet, SQL_Pallet);
  DefineSQLMovimID(False, Ed02MovimID, SQL_MovimID);
  //
  Controle := Ed02Controle.ValueVariant;
  if Controle = 0 then
    SQL_Controle := ''
  else
    SQL_Controle := 'AND vmi.Controle=' + Geral.FF0(Controle);
  //
  ReopenVSMovIts();
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrVSMovIts, Dmod.MyDB, [
  'SELECT vmi.*, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ',
  ATT_MovimID,
  'vsp.Nome NO_Pallet, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE ',
  'FROM v s m o v i t s vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN vspalleta  vsp ON vsp.Codigo=vmi.Pallet ',
  'LEFT JOIN entidades  ent ON ent.Codigo=vmi.Terceiro ',
  'WHERE vmi.Empresa=' + Geral.FF0(Empresa),
  SQL_GraGruX,
  SQL_Periodo,
  SQL_Terceiro,
  SQL_Pallet,
  SQL_Controle,
  SQL_MovimID,
  'ORDER BY vmi.DataHora, vmi.Pecas DESC; ',
  ' ']);
*)
  //
end;

procedure TFmVSMovImp.PesquisaMartelos();
var
  Entidade: Integer;
begin
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Entidade);
  if MyObjects.FIC(Entidade = 0, EdEmpresa, 'Informe a empresa') then
    Exit;
  //
  VS_PF.ImprimePesquisaMartelos2(Entidade, CBEmpresa.Text);
(*
  Ed05VSCacCod.ValueVariant, Ed05VSPallet.ValueVariant,
  Ed05VMI_Sorc.ValueVariant, Ed05VMI_Dest.ValueVariant,
  Ed05Martelo.ValueVariant, Ed05Ficha.ValueVariant, Ed05GraGruX.ValueVariant,
  Ed05Terceiro.ValueVariant, CBEmpresa.Text, Ed05Marca.Text,
  CB05GraGruX.Text, CB05Terceiro.Text, CB05SerieFch.Text,
  TP05DataIni.Date, TP05DataFim.Date, Ck05DataIni.Checked,
  Ck05DataFim.Checked, Ck05Revisores.Checked, RG05OperClass.ItemIndex,
  RG05Ordem.ItemIndex, RG05AscDesc.ItemIndex, Ck05MostraPontos.Checked,
  Ck05Morto.Checked);*)
end;

procedure TFmVSMovImp.PesquisaMovimDeDifer();
begin
  Application.CreateForm(TFmVSImpDif, FmVSImpDif);
  FmVSImpDif.FEmpresa_Cod   := EdEmpresa.ValueVariant;
  FmVSImpDif.FEmpresa_Txt   := CBEmpresa.Text;
  FmVSImpDif.FTerceiro_Txt  := CB08Terceiro.Text;
  FmVSImpDif.FTerceiro_Cod  := Ed08Terceiro.ValueVariant;
  FmVSImpDif.FDataIniVal    := TP08DataIni.Date;
  FmVSImpDif.FDataFimVal    := TP08DataFim.Date;
  FmVSImpDif.FDataIniUsa    := Ck08DataIni.Checked;
  FmVSImpDif.FDataFimUsa    := Ck08DataFim.Checked;
  //FmVSImpDif.FAgrupa        := RG08Agrupa.ItemIndex;
  FmVSImpDif.FTipoMovim_Cod := RG08TipoMovim.ItemIndex;
  FmVSImpDif.FTipoMovim_Txt := RG08TipoMovim.Items[RG08TipoMovim.ItemIndex];
  //FmVSImpDif.FOrdem         := RG08Ordem.ItemIndex;
  FmVSImpDif.FImpSimples    := Ck08Simples.Checked;
  //
  FmVSImpDif.FMovimIDSign   := CG08_01_MovimIDSign.Value;
  // ini 2021-03-30
  FmVSImpDif.FMovimIDResi   := CG08_01_MovimIDResi.Value;
  FmVSImpDif.FSdoResidual   := PC_Dif_Movim.ActivePageIndex = 1;
  // fim 2021-03-30
  FmVSImpDif.FOrdem1        := RG08_01_Ordem1.ItemIndex;
  FmVSImpDif.FOrdem2        := RG08_01_Ordem2.ItemIndex;
  FmVSImpDif.FAgrupa        := RG08_01_Agrupa.ItemIndex;
  FmVSImpDif.FPeriodo       :=
    dmkPF.PeriodoImp1(TP08DataIni.Date, TP08DataFim.Date,
    Ck08DataIni.Checked, Ck08DataFim.Checked, '', 'at�', '');
  FmVSImpDif.FUsaVSMovItb   := Ck08VSMovItb.Checked;
  //
  FmVSImpDif.ImprimeMovimentos();
  //
  FmVSImpDif.Destroy;
end;

procedure TFmVSMovImp.Qr06VSCacGBYCalcFields(DataSet: TDataSet);
begin
  if Qr06SumGBYAreaM2.Value = 0 then
    Qr06VSCacGBYPercentual.Value := 0
  else
    Qr06VSCacGBYPercentual.Value := Qr06VSCacGBYAreaM2.Value /
    Qr06SumGBYAreaM2.Value * 100;
  case Qr06VSCacGBYTamanho.Value of
    1: Qr06VSCacGBYNO_Tamanho.Value := '< ' + Ed06Pequeno.Text;
    2: Qr06VSCacGBYNO_Tamanho.Value := Ed06Pequeno.Text + ' a ' + Ed06Grande.Text;
    3: Qr06VSCacGBYNO_Tamanho.Value := '> ' + Ed06Grande.Text;
  end;
  if Qr06VSCacGBYPecas.Value > 0 then
    Qr06VSCacGBYMediaM2.Value := Qr06VSCacGBYAreaM2.Value / Qr06VSCacGBYPecas.Value
  else
    Qr06VSCacGBYMediaM2.Value := 0;
end;

procedure TFmVSMovImp.Qr07PrevPalAfterOpen(DataSet: TDataSet);
begin
  SetHabilitado(Qr07PrevPal.RecordCount > 0);
end;

procedure TFmVSMovImp.Qr07PrevPalBeforeClose(DataSet: TDataSet);
begin
  SetHabilitado(False);
end;

procedure TFmVSMovImp.QrEstqR1CalcFields(DataSet: TDataSet);
begin
  if QrEstqR1SdoVrtArM2.Value > 0 then
    QrEstqR1CUS_UNIT_M2.Value :=
      QrEstqR1ValorT.Value / QrEstqR1SdoVrtArM2.Value
  else
    QrEstqR1CUS_UNIT_M2.Value := 0;
  //
  if QrEstqR1SdoVrtPeso.Value > 0 then
    QrEstqR1CUS_UNIT_KG.Value :=
      QrEstqR1ValorT.Value / QrEstqR1SdoVrtPeso.Value
  else
    QrEstqR1CUS_UNIT_KG.Value := 0;
  //
end;

procedure TFmVSMovImp.QrEstqR3AfterOpen(DataSet: TDataSet);
begin
  SetHabilitado(QrEstqR3.RecordCount > 0);
end;

procedure TFmVSMovImp.QrEstqR3BeforeClose(DataSet: TDataSet);
begin
  SetHabilitado(False);
end;

procedure TFmVSMovImp.QrEstqR4AfterOpen(DataSet: TDataSet);
begin
  SetHabilitado(QrEstqR4.RecordCount > 0);
end;

procedure TFmVSMovImp.QrEstqR4BeforeClose(DataSet: TDataSet);
begin
  SetHabilitado(False);
end;

procedure TFmVSMovImp.QrEstqR4CalcFields(DataSet: TDataSet);
begin
  QrEstqR4SdoVrtArP2.Value :=
    Geral.ConverteArea(QrEstqR4SdoVrtArM2.Value, ctM2toP2, cfQuarto);
end;

procedure TFmVSMovImp.QrGGXPalTerCalcFields(DataSet: TDataSet);
begin
  QrGGXPalTerSdoVrtArP2.Value :=
    Geral.ConverteArea(QrGGXPalTerSdoVrtArM2.Value, ctM2toP2, cfQuarto);
end;

procedure TFmVSMovImp.QrNFesAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := QrNFes.RecordCount > 0;
end;

procedure TFmVSMovImp.QrNFesBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

procedure TFmVSMovImp.RelatriodeResultados1Click(Sender: TObject);
begin
  VS_PF.MostraFormVSImpResultVS();
end;

procedure TFmVSMovImp.Remover1Click(Sender: TObject);
begin
  VS_PF.AlterarNotFluxo(Qr13VSSeqItsControle.Value, 1);
end;

procedure TFmVSMovImp.Reopen00GraGruX();
var
  Texto, sGGXs, SQL_GGXs: String;
begin
  Texto := Ed00NomeGG1.Text;
  if Length(Texto) < 3 then
    Texto := ''
  else
    Texto := Geral.ATS(['AND CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))  ',
  'LIKE "' + Texto + '"']);
  //
  sGGXs := Trim(EdGGXs.Text);
  if sGGXs <> EmptyStr then
  begin
    //SQL_GGXs := dmkPF.SQLNumRange('AND', sGGXs);
     dmkPF.SQL_ExplodeInBetween('AND', 'ggx.Controle', sGGXs, SQL_GGXs);
  end else
    SQL_GGXs := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr00GraGruX, Dmod.MyDB, [
  'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome,   ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),   ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))   ',
  'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED,   ',
  'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED  ',
  'FROM gragrux ggx   ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC   ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad   ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI   ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1   ',
  'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed  ',
  'WHERE ggx.Controle > 0  ',
  Texto,
  SQL_GGXs,
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle   ',
  '']);
  //Geral.MB_Teste(Qr00GraGruX.SQL.Text);
end;

procedure TFmVSMovImp.ReopenVSMovImp3(Empresa, GraGruX, Pallet,
  Terceiro: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEstqR3, DModG.MyPID_DB, [
  'SELECT * FROM  ' + FVSMovImp3,
  'WHERE (Pecas >= 0.001 OR Pecas <= -0.001) ',
  //'ORDER BY NO_PALLET, NO_FORNECE, OrdGGX, NO_PRD_TAM_COR ',
  'ORDER BY Controle DESC ',
  '']);
  //
  QrEstqR3.Locate('Empresa;GraGruX;Pallet;Terceiro',
  VarArrayOf([Empresa, GraGruX, Pallet, Terceiro]), []);
end;

procedure TFmVSMovImp.ReopenVSMovImp4(Empresa, GraGruX, Pallet,
  Terceiro, CouNiv1, CouNiv2: Integer; GraGruYs: String);
//const
  //ordens: array[0..3] of String = (['VSPallet', 'NO_PRD_TAM_COR', 'NO_FORNECE', 'Media']);
var
  OrderBy, SQL_GraGruY: String;
var
  SQL_Empresa, SQL_GraGruX, SQL_CouNiv1, SQL_CouNiv2, SQL_IMEI: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNimboRcl, Dmod.MyDB, [
  'SELECT rca.VSPallet,',
  'SUM(cia.Pecas) Pecas, SUM(cia.AreaM2) AreaM2 ',
  'FROM vscacitsa cia',
  'LEFT JOIN vsparclcaba rca ON rca.Codigo=cia.Codigo',
  'WHERE cia.VSPaRclIts IN (',
  '     SELECT Controle ',
  '     FROM vsparclitsa pri ',
  '     WHERE DtHrFim < "1900-01-01"',
  ')',
  'GROUP BY rca.VSPallet',
  'ORDER BY rca.VSPallet',
  '',
  '']);
  if CkStatusReal.Checked then
  begin
    if GraGruYs <> '' then
      SQL_GraGruY := 'AND ggx.GraGruY IN (' + GraGruYs + ') '
    else
      SQL_GraGruY := '';
    //



(*
    VS_PF.InsereVSMovImpEmClassificacao(Empresa, GraGruX, CouNiv2, CouNiv1,
      FVSMovImp4, SQL_GraGruY);
*)



{
    if Empresa <> 0 then
      SQL_Empresa := 'AND vsp.Empresa=' + Geral.FF0(Empresa)
    else
      SQL_Empresa := '';
    //
    if GraGruX <> 0 then
      //SQL_GraGruX := 'AND wmi.GraGruX=' + Geral.FF0(GraGruX)
      SQL_GraGruX := 'AND vsp.GraGruX=' + Geral.FF0(GraGruX)
    else
      SQL_GraGruX := '';
    //
    if CouNiv2 <> 0 then
      SQL_CouNiv2 := 'AND xco.CouNiv2=' + Geral.FF0(CouNiv2)
    else
      SQL_CouNiv2 := '';
    //
    if CouNiv1 <> 0 then
      SQL_CouNiv1 := 'AND xco.CouNiv1=' + Geral.FF0(CouNiv1)
    else
      SQL_CouNiv1 := '';
    //
    SQL_IMEI := Geral.ATS([
    '0 Codigo, 0 IMEC, 0 IMEI, ',
    '0 MovimID, 0 MovimNiv, "" NO_MovimID, "" NO_MovimNiv, ']);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO  ' + FVSMovImp4,
    'SELECT vsp.Empresa, vsp.GraGruX, 0 Pecas, 0 PesoKg,',
    '0 AreaM2, 0 AreaP2, 0 ValorT, 0 SdoVrtPeca, ',
    '0 SdoVrtPeso, 0 SdoVrtArM2, -SUM(cia.Pecas) LmbVrtPeca, ',
    '0 LmbVrtPeso, -SUM(cia.AreaM2) LmbVrtArM2,',
    'ggx.GraGru1, CONCAT(gg1.Nome,',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
    'NO_PRD_TAM_COR, rca.VSPallet Pallet, vsp.Nome NO_Pallet,',
    '0 Terceiro,',
    'IF(vsp.CliStat IS NULL, 0, vsp.CliStat),',
    'IF(vsp.Status IS NULL, 0, vsp.Status),',
    '"" NO_FORNECE,',
    'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,',
    'vps.Nome NO_STATUS,',
    'vsp.DtHrEndAdd DataHora, 0 OrdGGX,',
    'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
    //
    '2 PalStat, ',
    'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
    'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
    'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
    '0 SdoInteiros, ',
    '-SUM(cia.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) LmbInteiros, ',
    SQL_IMEI,
    '1 Ativo  ',
    'FROM ' + TMeuDB + '.vscacitsa cia',
    'LEFT JOIN ' + TMeuDB + '.vsparclcaba rca ON rca.Codigo=cia.Codigo',
    'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=rca.VSPallet',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsp.GraGruX',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle',
    'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    '', // Sem terceiro???
    'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vsp.CliStat',
    'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vsp.Empresa',
    'LEFT JOIN ' + TMeuDB + '.vspalsta  vps ON vps.Codigo=vsp.Status',
    'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
    'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
////////////  2015-03-05 ///////////////////////////////////////////////////////
    'WHERE cia.VSPaRclIts IN (',
    '     SELECT Controle ',
    '     FROM ' + TMeuDB + '.vsparclitsa pri ',
    '     WHERE DtHrFim < "1900-01-01"',
    ')',
(*
    'WHERE cia.VMI_Dest =0 ',
    'AND  cia.VSPaRclIts IN (',
    '     SELECT Controle ',
    '     FROM ' + TMeuDB + '.vsparclitsa pri ',
    '     WHERE DtHrFim < "1900-01-01"',
    ')',
*)
//////////// FIM 2015-03-05 ////////////////////////////////////////////////////
    SQL_Empresa,
    SQL_GraGruX,
    SQL_CouNiv2,
    SQL_CouNiv1,
    'GROUP BY rca.VSPallet',
    '']);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO  ' + FVSMovImp4,
    'SELECT vsp.Empresa, vsp.GraGruX, 0 Pecas, 0 PesoKg,',
    '0 AreaM2, 0 AreaP2, 0 ValorT, 0 SdoVrtPeca, ',
    '0 SdoVrtPeso, 0 SdoVrtArM2, SUM(cia.Pecas) LmbVrtPeca, ',
    '0 LmbVrtPeso, SUM(cia.AreaM2) LmbVrtArM2,',
    'ggx.GraGru1, CONCAT(gg1.Nome,',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))',
    'NO_PRD_TAM_COR, cia.VSPallet Pallet, vsp.Nome NO_Pallet,',
    '0 Terceiro,',
    'IF(vsp.CliStat IS NULL, 0, vsp.CliStat),',
    'IF(vsp.Status IS NULL, 0, vsp.Status),',
    '"" NO_FORNECE,',
    'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "",',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat,',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA,',
    'vps.Nome NO_STATUS,',
    'vsp.DtHrEndAdd DataHora, 0 OrdGGX,',
    'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
    //
    '1 PalStat, ',
    'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
    'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
    'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
    '0 SdoInteiros, ',
    'SUM(cia.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) LmbInteiros, ',
    SQL_IMEI,
    '1 Ativo  ',
    'FROM ' + TMeuDB + '.vscacitsa cia',
    'LEFT JOIN ' + TMeuDB + '.vsparclcaba rca ON rca.Codigo=cia.Codigo',
    'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=rca.VSPallet',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsp.GraGruX',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle',
    'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1',
    '', // Sem terceiro???
    'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vsp.CliStat',
    'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vsp.Empresa',
    'LEFT JOIN ' + TMeuDB + '.vspalsta   vps ON vps.Codigo=vsp.Status',
    'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2',
    'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1',
////////////  2015-03-05 ///////////////////////////////////////////////////////
    'WHERE cia.VSPaRclIts IN (',
    '     SELECT Controle ',
    '     FROM ' + TMeuDB + '.vsparclitsa pri ',
    '     WHERE DtHrFim < "1900-01-01"',
    ')',
(*
    'WHERE cia.VMI_Dest =0 ',
    'AND  cia.VSPaRclIts IN (',
    '     SELECT Controle ',
    '     FROM ' + TMeuDB + '.vsparclitsa pri ',
    '     WHERE DtHrFim < "1900-01-01"',
    ')',
*)
//////////// FIM 2015-03-05 ////////////////////////////////////////////////////
    SQL_Empresa,
    SQL_GraGruX,
    SQL_CouNiv2,
    SQL_CouNiv1,
    'GROUP BY cia.VSPallet',
    '']);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
    'INSERT INTO  ' + FVSMovImp4,
    'SELECT vsp.Empresa, vsp.GraGruX, 0 Pecas, 0 PesoKg, ',
    '0 AreaM2, 0 AreaP2, 0 ValorT, 0 SdoVrtPeca, ',
    '0 SdoVrtPeso, 0 SdoVrtArM2, SUM(cia.Pecas) LmbVrtPeca, ',
    '0 LmbVrtPeso, SUM(cia.AreaM2) LmbVrtArM2, ',
    'ggx.GraGru1, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, cia.VSPallet Pallet, vsp.Nome NO_Pallet, ',
    '0 Terceiro, ',
    'IF(vsp.CliStat IS NULL, 0, vsp.CliStat), ',
    'IF(vsp.Status IS NULL, 0, vsp.Status), ',
    '"" NO_FORNECE, ',
    'IF((vsp.CliStat=0) OR (vsp.CliStat IS NULL), "", ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_CliStat, ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMPRESA, ',
    'vps.Nome NO_STATUS, ',
    'vsp.DtHrEndAdd DataHora, 0 OrdGGX, ',
    'ggy.Ordem, ggy.Codigo, ggy.Nome, ',
    '1 PalStat, ',
    'IF(xco.CouNiv2 IS NULL, 0, xco.CouNiv2) CouNiv2, ',
    'IF(xco.CouNiv1 IS NULL, 0, xco.CouNiv1) CouNiv1, ',
    'cn2.Nome, cn1.Nome, IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt) FatorInt, ',
    '0 SdoInteiros, ',
    'SUM(cia.Pecas * IF(cn1.FatorInt IS NULL, 0, cn1.FatorInt)) LmbInteiros, ',
    SQL_IMEI,
    '1 Ativo ',
    'FROM ' + TMeuDB + '.vscacitsa cia ',
    'LEFT JOIN ' + TMeuDB + '.vspaclacaba rca ON rca.Codigo=cia.Codigo ',
    'LEFT JOIN ' + TMeuDB + '.vspalleta  vsp ON vsp.Codigo=cia.VSPallet ',
    'LEFT JOIN ' + TMeuDB + '.gragrux    ggx ON ggx.Controle=vsp.GraGruX ',
    'LEFT JOIN ' + TMeuDB + '.gragruxcou xco ON xco.GraGruX=ggx.Controle ',
    'LEFT JOIN ' + TMeuDB + '.gragruy    ggy ON ggy.Codigo=ggx.GraGruY ',
    'LEFT JOIN ' + TMeuDB + '.gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN ' + TMeuDB + '.gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN ' + TMeuDB + '.gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN ' + TMeuDB + '.gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    ' ',
    'LEFT JOIN ' + TMeuDB + '.entidades  ent ON ent.Codigo=vsp.CliStat ',
    'LEFT JOIN ' + TMeuDB + '.entidades  emp ON emp.Codigo=vsp.Empresa ',
    'LEFT JOIN ' + TMeuDB + '.vspalsta   vps ON vps.Codigo=vsp.Status ',
    'LEFT JOIN ' + TMeuDB + '.couniv2    cn2 ON cn2.Codigo=xco.CouNiv2 ',
    'LEFT JOIN ' + TMeuDB + '.couniv1    cn1 ON cn1.Codigo=xco.CouNiv1 ',
    'WHERE ( ',
    '(cia.VSPaClaIts <> 0) AND ',
    '(cia.VSPaClaIts IN ( ',
    '     SELECT DISTINCT Controle ',
    '     FROM ' + TMeuDB + '.vspaclaitsa ',
    '     WHERE DtHrFim < "1900-01-01") ',
    ')) ',
    SQL_Empresa,
    SQL_GraGruX,
    SQL_CouNiv2,
    SQL_CouNiv1,
    'GROUP BY cia.VSPallet',
    '']);
    //
}
  end;
  //
  OrderBy := '';
  AtualizaPalletsEmMontagem();
  VS_PF.ReopenVSListaPallet2(QrEstqR4, FVSMovImp4, FVSLstPalBox,
    Empresa, GraGruX, Pallet, Terceiro, GraGruYs, OrderBy);
end;

procedure TFmVSMovImp.RG13_AnaliSinteClick(Sender: TObject);
begin
  Configura13DataIni();
  La13_MaxInteiros.Visible := RG13_AnaliSinte.ItemIndex = 2;
  Ed13_MaxInteiros.Visible := RG13_AnaliSinte.ItemIndex = 2;
  RG13TipoMov.Visible      := RG13_AnaliSinte.ItemIndex = 1;
end;

procedure TFmVSMovImp.RG13_GragruYClick(Sender: TObject);
begin
  Configura13DataIni();
end;

procedure TFmVSMovImp.rmica95x22ZebraArgoxTSC1Click(Sender: TObject);
begin
  ImprimeFichasDePallets(True, difpTermica10x22);
end;

procedure TFmVSMovImp.Sadas1Click(Sender: TObject);
begin
  CG08_01_MovimIDSign.Value := 2 + 8 + 32 + 128 + 512;
end;

procedure TFmVSMovImp.SetaTodosItens03(Ativo: Boolean);
begin
  if Ativo then
    MyObjects.DBGridSelectAll(TDBGrid(DBG03Estq))
  else
    DBG03Estq.SelectedRows.Clear;
end;

procedure TFmVSMovImp.SetaTodosItens04(Ativo: Boolean);
begin
  if Ativo then
    MyObjects.DBGridSelectAll(TDBGrid(DBG04Estq))
  else
    DBG04Estq.SelectedRows.Clear;
end;

procedure TFmVSMovImp.SetaTodosItens07(Ativo: Boolean);
begin
  if Ativo then
    MyObjects.DBGridSelectAll(TDBGrid(DBG07Estq))
  else
    DBG07Estq.SelectedRows.Clear;
end;

procedure TFmVSMovImp.SetaTodosItensGraGruX04(Ativo: Boolean);
begin
  if Ativo then
    MyObjects.DBGridSelectAll(TDBGrid(DBG04Artigos))
  else
    DBG04Artigos.SelectedRows.Clear;
end;

procedure TFmVSMovImp.SetHabilitado(Habilitado: Boolean);
begin
  BtImprime.Enabled := Habilitado;
  BtNenhum.Enabled := Habilitado;
  BtTudo.Enabled := Habilitado;
end;

procedure TFmVSMovImp.SpeedButton1Click(Sender: TObject);
var
  SQL_Res: String;
begin
  if DmkPF.SQL_ExplodeInBetween(EmptyStr, 'MovimCod', Ed18Especificos.Text, SQL_Res) then
    Geral.MB_Info(SQL_Res)
  else
    Geral.MB_Info('Erro')
end;

function TFmVSMovImp.SQL_PalletsReal(): String;
var
  I, N, Empresa: Integer;
begin
  Result := '';
  DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  //
  N := DBG04Estq.SelectedRows.Count-1;
  //
  if N > -1 then
  begin
    with DBG04Estq.DataSource.DataSet do
    for I := 0 to N do
    begin
      //GotoBookmark(pointer(DBG04Estq.SelectedRows.Items[I]));
      GotoBookmark(DBG04Estq.SelectedRows.Items[I]);
      //
      Result := Result + sLineBreak + Geral.FF0(QrEstqR4Pallet.Value);
      if I < N then
        Result := Result + ',';
    end;
  end;
  if N = -1 then
    Geral.MB_Aviso('Nenhum Pallet foi selecionado na guia "Pallet"!');
end;

procedure TFmVSMovImp.Vertical1Click(Sender: TObject);
begin
  ImprimePackLists(True, FVSMovImp4, FVSLstPalBox, TGrandezaArea.grandareaAreaM2);
end;




procedure TFmVSMovImp.Verticalcolunasft1Click(Sender: TObject);
begin
  ImprimePackLists(True, FVSMovImp4, FVSLstPalBox, TGrandezaArea.grandareaAreaP2);
end;

(*
DELETE FROM vscaccab;
DELETE FROM vscacitsa;
DELETE FROM vsgerarta;
DELETE FROM vsinncab;
DELETE FROM vsmovcab;
DELETE FROM vsmovdif;
DELETE FROM vs movits;
DELETE FROM vspaclacaba;
DELETE FROM vspaclaitsa;
DELETE FROM vspalleta;
*)


(*
DROP TABLE IF EXISTS _Fluxo_1024_;
CREATE TABLE _Fluxo_1024_
SELECT vsi.Ficha, vsi.SerieFch,
SUM(vsi.Inteiros) Inteiros
FROM _vsseqits_fmvsmovimp vsi
GROUP BY vsi.Ficha, vsi.SerieFch
ORDER BY vsi.Ficha, vsi.SerieFch;
SELECT *
FROM _Fluxo_1024_
WHERE Inteiros <> 0;


DROP TABLE IF EXISTS _Fluxo_2048_;
CREATE TABLE _Fluxo_2048_
SELECT vsi.IMEI_Src,
SUM(vsi.Inteiros) Inteiros
FROM _vsseqits_fmvsmovimp vsi
GROUP BY vsi.IMEI_Src;
SELECT *
FROM _Fluxo_2048_
WHERE Inteiros <> 0
ORDER BY IMEI_Src;

DROP TABLE IF EXISTS _Fluxo_2072_;
CREATE TABLE _Fluxo_2072_
SELECT vsi.Pallet,
SUM(vsi.Inteiros) Inteiros
FROM _vsseqits_fmvsmovimp vsi
GROUP BY vsi.Pallet;
SELECT *
FROM _Fluxo_2072_
WHERE Inteiros <> 0
ORDER BY Pallet;
*)

(*

UPDATE vspalleta
SET GraGruX=53
WHERE GraGruX=3;

UPDATE vspalleta
SET GraGruX=54
WHERE GraGruX=4;

UPDATE v s m o v i ts
SET GraGruX=53
WHERE Pecas>0
AND Pallet>0
AND GraGruX=3;

UPDATE v s m o v it s
SET GraGruX=54
WHERE Pecas>0
AND Pallet>0
AND GraGruX=4;


*)



(*
vspaclaitsa
 Codigo 307 Controle 1861 > Hora = 12/05/2015 16:24:48

v s m o v i  t s Controle 9817 DataHotra = 12/05/2015 16:24:48
MovimCod = 4   ????
*)


(*  ESTOQUE EM...
SELECT vmi.GraGruX, SUM(vmi.Pecas) Pecas,
SUM(vmi.PesoKg) PesoKg,
SUM(vmi.AreaM2) AreaM2, SUM(vmi.ValorT) ValorT,
CONCAT(gg1.Nome,
IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),
IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome)))
NO_PRD_TAM_COR
FROM v s m o v i t s vmi
LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX
LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC
LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad
LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI
LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1
WHERE vmi.DataHora <= "2015-04-30 23:59:59"
GROUP BY vmi.GraGruX
*)

{ Couros que entraram em curtimento do caleiro
DROP TABLE IF EXISTS _CalToCur_;
CREATE TABLE _CalToCur_
SELECT * FROM vsmovits
WHERE DataHora >= "2020-08-21"
AND MovimID=27
AND Pecas < 0;

SELECT * FROM vsmovits
WHERE Controle IN (
  SELECT SrcNivel2
  FROM _CalToCur_
)
}



end.
