object FmVSIxx: TFmVSIxx
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-200 :: IXX - Atrelamento de Informa'#231#227'o Manual'
  ClientHeight = 286
  ClientWidth = 597
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 597
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 661
    object GB_R: TGroupBox
      Left = 549
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 613
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 501
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 565
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 490
        Height = 32
        Caption = 'IXX - Atrelamento de Informa'#231#227'o Manual'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 490
        Height = 32
        Caption = 'IXX - Atrelamento de Informa'#231#227'o Manual'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 490
        Height = 32
        Caption = 'IXX - Atrelamento de Informa'#231#227'o Manual'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 597
    Height = 124
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 661
    ExplicitHeight = 284
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 597
      Height = 124
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 661
      ExplicitHeight = 284
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 597
        Height = 124
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 661
        ExplicitHeight = 284
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 593
          Height = 58
          Align = alTop
          Caption = ' Dados do Item:'
          TabOrder = 0
          object Panel5: TPanel
            Left = 2
            Top = 15
            Width = 589
            Height = 41
            Align = alClient
            BevelOuter = bvNone
            Enabled = False
            TabOrder = 0
            ExplicitTop = 19
            ExplicitHeight = 158
            object Label5: TLabel
              Left = 12
              Top = 0
              Width = 14
              Height = 13
              Caption = 'ID:'
            end
            object Label2: TLabel
              Left = 76
              Top = 0
              Width = 32
              Height = 13
              Caption = 'IME-C:'
            end
            object Label3: TLabel
              Left = 140
              Top = 0
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Label1: TLabel
              Left = 192
              Top = 0
              Width = 32
              Height = 13
              Caption = 'IME-C:'
            end
            object Label4: TLabel
              Left = 256
              Top = 0
              Width = 41
              Height = 13
              Caption = 'GraGruX'
              FocusControl = DBEdit5
            end
            object Label6: TLabel
              Left = 316
              Top = 0
              Width = 33
              Height = 13
              Caption = 'Pecas:'
              FocusControl = DBEdit6
            end
            object Label7: TLabel
              Left = 400
              Top = 0
              Width = 42
              Height = 13
              Caption = 'Peso kg:'
              FocusControl = DBEdit7
            end
            object Label8: TLabel
              Left = 492
              Top = 0
              Width = 39
              Height = 13
              Caption = #193'rea m'#178':'
              FocusControl = DBEdit8
            end
            object DBEdit1: TDBEdit
              Left = 12
              Top = 16
              Width = 62
              Height = 21
              DataField = 'Codigo'
              DataSource = DsVSMovIts
              TabOrder = 0
            end
            object DBEdit2: TDBEdit
              Left = 76
              Top = 16
              Width = 62
              Height = 21
              DataField = 'MovimCod'
              DataSource = DsVSMovIts
              TabOrder = 1
            end
            object DBEdit3: TDBEdit
              Left = 140
              Top = 16
              Width = 49
              Height = 21
              DataField = 'Empresa'
              DataSource = DsVSMovIts
              TabOrder = 2
            end
            object DBEdit4: TDBEdit
              Left = 192
              Top = 16
              Width = 62
              Height = 21
              DataField = 'Controle'
              DataSource = DsVSMovIts
              TabOrder = 3
            end
            object DBEdit5: TDBEdit
              Left = 256
              Top = 16
              Width = 56
              Height = 21
              DataField = 'GraGruX'
              DataSource = DsVSMovIts
              TabOrder = 4
            end
            object DBEdit6: TDBEdit
              Left = 316
              Top = 16
              Width = 80
              Height = 21
              DataField = 'Pecas'
              DataSource = DsVSMovIts
              TabOrder = 5
            end
            object DBEdit7: TDBEdit
              Left = 400
              Top = 16
              Width = 88
              Height = 21
              DataField = 'PesoKg'
              DataSource = DsVSMovIts
              TabOrder = 6
            end
            object DBEdit8: TDBEdit
              Left = 492
              Top = 16
              Width = 88
              Height = 21
              DataField = 'AreaM2'
              DataSource = DsVSMovIts
              TabOrder = 7
            end
          end
        end
        object Panel9: TPanel
          Left = 2
          Top = 73
          Width = 593
          Height = 36
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitWidth = 657
          ExplicitHeight = 209
          object Label20: TLabel
            Left = 423
            Top = 16
            Width = 29
            Height = 13
            Caption = 'Folha:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label21: TLabel
            Left = 515
            Top = 16
            Width = 29
            Height = 13
            Caption = 'Linha:'
            Color = clBtnFace
            ParentColor = False
          end
          object RGIxxMovIX: TRadioGroup
            Left = 0
            Top = 0
            Width = 416
            Height = 36
            Align = alLeft
            Caption = ' Informa'#231#227'o manual de movimenta'#231#227'o:'
            Columns = 4
            ItemIndex = 0
            Items.Strings = (
              'Indefinido'
              'Entrada de couro'
              'Classe / reclasse'
              'Sa'#237'da de couro')
            TabOrder = 0
            ExplicitHeight = 209
          end
          object EdIxxFolha: TdmkEdit
            Left = 456
            Top = 12
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ReqMovEstq'
            UpdCampo = 'ReqMovEstq'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdIxxLinha: TdmkEdit
            Left = 548
            Top = 12
            Width = 33
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ReqMovEstq'
            UpdCampo = 'ReqMovEstq'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 172
    Width = 597
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 332
    ExplicitWidth = 661
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 593
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 657
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 216
    Width = 597
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 376
    ExplicitWidth = 661
    object PnSaiDesis: TPanel
      Left = 451
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 515
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 449
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 513
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 380
    Top = 199
  end
  object QrVSMovIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM vsmovits'
      'WHERE Controle=1')
    Left = 216
    Top = 164
    object QrVSMovItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSMovItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVSMovItsMovimCod: TIntegerField
      FieldName = 'MovimCod'
    end
    object QrVSMovItsMovimNiv: TIntegerField
      FieldName = 'MovimNiv'
    end
    object QrVSMovItsMovimTwn: TIntegerField
      FieldName = 'MovimTwn'
    end
    object QrVSMovItsEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrVSMovItsTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrVSMovItsCliVenda: TIntegerField
      FieldName = 'CliVenda'
    end
    object QrVSMovItsMovimID: TIntegerField
      FieldName = 'MovimID'
    end
    object QrVSMovItsLnkIDXtr: TIntegerField
      FieldName = 'LnkIDXtr'
    end
    object QrVSMovItsLnkNivXtr1: TIntegerField
      FieldName = 'LnkNivXtr1'
    end
    object QrVSMovItsLnkNivXtr2: TIntegerField
      FieldName = 'LnkNivXtr2'
    end
    object QrVSMovItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVSMovItsPallet: TIntegerField
      FieldName = 'Pallet'
    end
    object QrVSMovItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrVSMovItsPecas: TFloatField
      FieldName = 'Pecas'
    end
    object QrVSMovItsPesoKg: TFloatField
      FieldName = 'PesoKg'
      DisplayFormat = '#,###,###,##0.000'
    end
    object QrVSMovItsAreaM2: TFloatField
      FieldName = 'AreaM2'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrVSMovItsAreaP2: TFloatField
      FieldName = 'AreaP2'
    end
    object QrVSMovItsValorT: TFloatField
      FieldName = 'ValorT'
    end
    object QrVSMovItsSrcMovID: TIntegerField
      FieldName = 'SrcMovID'
    end
    object QrVSMovItsSrcNivel1: TIntegerField
      FieldName = 'SrcNivel1'
    end
    object QrVSMovItsSrcNivel2: TIntegerField
      FieldName = 'SrcNivel2'
    end
    object QrVSMovItsSrcGGX: TIntegerField
      FieldName = 'SrcGGX'
    end
    object QrVSMovItsSdoVrtPeca: TFloatField
      FieldName = 'SdoVrtPeca'
    end
    object QrVSMovItsSdoVrtPeso: TFloatField
      FieldName = 'SdoVrtPeso'
    end
    object QrVSMovItsSdoVrtArM2: TFloatField
      FieldName = 'SdoVrtArM2'
    end
    object QrVSMovItsObserv: TWideStringField
      FieldName = 'Observ'
      Required = True
      Size = 255
    end
    object QrVSMovItsSerieFch: TIntegerField
      FieldName = 'SerieFch'
    end
    object QrVSMovItsFicha: TIntegerField
      FieldName = 'Ficha'
    end
    object QrVSMovItsMisturou: TSmallintField
      FieldName = 'Misturou'
    end
    object QrVSMovItsFornecMO: TIntegerField
      FieldName = 'FornecMO'
    end
    object QrVSMovItsCustoMOKg: TFloatField
      FieldName = 'CustoMOKg'
    end
    object QrVSMovItsCustoMOTot: TFloatField
      FieldName = 'CustoMOTot'
    end
    object QrVSMovItsValorMP: TFloatField
      FieldName = 'ValorMP'
    end
    object QrVSMovItsDstMovID: TIntegerField
      FieldName = 'DstMovID'
    end
    object QrVSMovItsDstNivel1: TIntegerField
      FieldName = 'DstNivel1'
    end
    object QrVSMovItsDstNivel2: TIntegerField
      FieldName = 'DstNivel2'
    end
    object QrVSMovItsDstGGX: TIntegerField
      FieldName = 'DstGGX'
    end
    object QrVSMovItsQtdGerPeca: TFloatField
      FieldName = 'QtdGerPeca'
    end
    object QrVSMovItsQtdGerPeso: TFloatField
      FieldName = 'QtdGerPeso'
    end
    object QrVSMovItsQtdGerArM2: TFloatField
      FieldName = 'QtdGerArM2'
    end
    object QrVSMovItsQtdGerArP2: TFloatField
      FieldName = 'QtdGerArP2'
    end
    object QrVSMovItsQtdAntPeca: TFloatField
      FieldName = 'QtdAntPeca'
    end
    object QrVSMovItsQtdAntPeso: TFloatField
      FieldName = 'QtdAntPeso'
    end
    object QrVSMovItsQtdAntArM2: TFloatField
      FieldName = 'QtdAntArM2'
    end
    object QrVSMovItsQtdAntArP2: TFloatField
      FieldName = 'QtdAntArP2'
    end
    object QrVSMovItsAptoUso: TSmallintField
      FieldName = 'AptoUso'
    end
    object QrVSMovItsNotaMPAG: TFloatField
      FieldName = 'NotaMPAG'
    end
    object QrVSMovItsMarca: TWideStringField
      FieldName = 'Marca'
    end
    object QrVSMovItsTpCalcAuto: TIntegerField
      FieldName = 'TpCalcAuto'
    end
    object QrVSMovItsZerado: TSmallintField
      FieldName = 'Zerado'
    end
    object QrVSMovItsEmFluxo: TSmallintField
      FieldName = 'EmFluxo'
    end
    object QrVSMovItsNotFluxo: TIntegerField
      FieldName = 'NotFluxo'
    end
    object QrVSMovItsFatNotaVNC: TFloatField
      FieldName = 'FatNotaVNC'
    end
    object QrVSMovItsFatNotaVRC: TFloatField
      FieldName = 'FatNotaVRC'
    end
    object QrVSMovItsPedItsLib: TIntegerField
      FieldName = 'PedItsLib'
    end
    object QrVSMovItsPedItsFin: TIntegerField
      FieldName = 'PedItsFin'
    end
    object QrVSMovItsPedItsVda: TIntegerField
      FieldName = 'PedItsVda'
    end
    object QrVSMovItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrVSMovItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrVSMovItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrVSMovItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrVSMovItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrVSMovItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrVSMovItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrVSMovItsGSPInnNiv2: TIntegerField
      FieldName = 'GSPInnNiv2'
    end
    object QrVSMovItsGSPArtNiv2: TIntegerField
      FieldName = 'GSPArtNiv2'
    end
    object QrVSMovItsCustoMOM2: TFloatField
      FieldName = 'CustoMOM2'
    end
    object QrVSMovItsReqMovEstq: TIntegerField
      FieldName = 'ReqMovEstq'
    end
    object QrVSMovItsStqCenLoc: TIntegerField
      FieldName = 'StqCenLoc'
    end
    object QrVSMovItsItemNFe: TIntegerField
      FieldName = 'ItemNFe'
    end
    object QrVSMovItsVSMorCab: TIntegerField
      FieldName = 'VSMorCab'
    end
    object QrVSMovItsVSMulFrnCab: TIntegerField
      FieldName = 'VSMulFrnCab'
    end
    object QrVSMovItsClientMO: TIntegerField
      FieldName = 'ClientMO'
    end
    object QrVSMovItsCustoPQ: TFloatField
      FieldName = 'CustoPQ'
    end
    object QrVSMovItsKgCouPQ: TFloatField
      FieldName = 'KgCouPQ'
    end
    object QrVSMovItsNFeSer: TSmallintField
      FieldName = 'NFeSer'
    end
    object QrVSMovItsNFeNum: TIntegerField
      FieldName = 'NFeNum'
    end
    object QrVSMovItsVSMulNFeCab: TIntegerField
      FieldName = 'VSMulNFeCab'
    end
    object QrVSMovItsGGXRcl: TIntegerField
      FieldName = 'GGXRcl'
    end
    object QrVSMovItsJmpMovID: TIntegerField
      FieldName = 'JmpMovID'
    end
    object QrVSMovItsJmpNivel1: TIntegerField
      FieldName = 'JmpNivel1'
    end
    object QrVSMovItsJmpNivel2: TIntegerField
      FieldName = 'JmpNivel2'
    end
    object QrVSMovItsJmpGGX: TIntegerField
      FieldName = 'JmpGGX'
    end
    object QrVSMovItsRmsMovID: TIntegerField
      FieldName = 'RmsMovID'
    end
    object QrVSMovItsRmsNivel1: TIntegerField
      FieldName = 'RmsNivel1'
    end
    object QrVSMovItsRmsNivel2: TIntegerField
      FieldName = 'RmsNivel2'
    end
    object QrVSMovItsRmsGGX: TIntegerField
      FieldName = 'RmsGGX'
    end
    object QrVSMovItsGSPSrcMovID: TIntegerField
      FieldName = 'GSPSrcMovID'
    end
    object QrVSMovItsGSPSrcNiv2: TIntegerField
      FieldName = 'GSPSrcNiv2'
    end
    object QrVSMovItsGSPJmpMovID: TIntegerField
      FieldName = 'GSPJmpMovID'
    end
    object QrVSMovItsGSPJmpNiv2: TIntegerField
      FieldName = 'GSPJmpNiv2'
    end
    object QrVSMovItsDtCorrApo: TDateTimeField
      FieldName = 'DtCorrApo'
    end
    object QrVSMovItsMovCodPai: TIntegerField
      FieldName = 'MovCodPai'
    end
    object QrVSMovItsIxxMovIX: TSmallintField
      FieldName = 'IxxMovIX'
    end
    object QrVSMovItsIxxFolha: TIntegerField
      FieldName = 'IxxFolha'
    end
    object QrVSMovItsIxxLinha: TIntegerField
      FieldName = 'IxxLinha'
    end
  end
  object DsVSMovIts: TDataSource
    DataSet = QrVSMovIts
    Left = 216
    Top = 212
  end
end
