object FmVSInnIts: TFmVSInnIts
  Left = 339
  Top = 185
  Caption = 'WET-CURTI-007 :: Item de Entrada de Peles In Natura'
  ClientHeight = 587
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 451
    Width = 1008
    Height = 22
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 4
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 62
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 6
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdCodigo
    end
    object Label2: TLabel
      Left = 96
      Top = 20
      Width = 32
      Height = 13
      Caption = 'IME-C:'
      FocusControl = DBEdMovimCod
    end
    object Label3: TLabel
      Left = 180
      Top = 20
      Width = 44
      Height = 13
      Caption = 'Empresa:'
      FocusControl = DBEdEmpresa
    end
    object Label7: TLabel
      Left = 228
      Top = 20
      Width = 58
      Height = 13
      Caption = 'Data / hora:'
      FocusControl = DBEdDtEntrada
    end
    object Label8: TLabel
      Left = 344
      Top = 20
      Width = 57
      Height = 13
      Caption = 'Fornecedor:'
      FocusControl = DBEdFornecedor
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdMovimCod: TdmkDBEdit
      Left = 96
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      DataField = 'MovimCod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 1
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdEmpresa: TdmkDBEdit
      Left = 180
      Top = 36
      Width = 45
      Height = 21
      TabStop = False
      DataField = 'Empresa'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 2
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdDtEntrada: TdmkDBEdit
      Left = 228
      Top = 36
      Width = 112
      Height = 21
      TabStop = False
      DataField = 'DtEntrada'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 3
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdFornecedor: TdmkDBEdit
      Left = 344
      Top = 36
      Width = 61
      Height = 21
      TabStop = False
      DataField = 'Fornecedor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 4
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 110
    Width = 1008
    Height = 159
    Align = alTop
    Caption = ' Dados do item que realmente entraram: '
    TabOrder = 0
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 28
      Height = 13
      Caption = 'IME-I:'
    end
    object Label1: TLabel
      Left = 76
      Top = 16
      Width = 66
      Height = 13
      Caption = 'Mat'#233'ria-prima:'
    end
    object LaPecas: TLabel
      Left = 696
      Top = 16
      Width = 54
      Height = 13
      Caption = 'Pe'#231'as: [F5]'
    end
    object LaPeso: TLabel
      Left = 784
      Top = 16
      Width = 48
      Height = 13
      Caption = 'Peso: [F5]'
    end
    object Label9: TLabel
      Left = 516
      Top = 116
      Width = 61
      Height = 13
      Caption = 'Observa'#231#227'o:'
    end
    object Label10: TLabel
      Left = 876
      Top = 16
      Width = 108
      Height = 13
      Caption = 'Valor MP total [F4][F5]:'
    end
    object Label4: TLabel
      Left = 592
      Top = 16
      Width = 77
      Height = 13
      Caption = 'Ficha RMP [F4]:'
    end
    object Label11: TLabel
      Left = 384
      Top = 16
      Width = 83
      Height = 13
      Caption = 'S'#233'rie Ficha RMP:'
    end
    object Label12: TLabel
      Left = 420
      Top = 116
      Width = 85
      Height = 13
      Caption = 'Marca do martelo:'
    end
    object Label29: TLabel
      Left = 228
      Top = 116
      Width = 63
      Height = 13
      Caption = 'Frete $ Total:'
    end
    object Label32: TLabel
      Left = 324
      Top = 116
      Width = 53
      Height = 13
      Caption = 'Custo total:'
      Enabled = False
    end
    object SbCustoComiss: TSpeedButton
      Left = 204
      Top = 132
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbCustoComissClick
    end
    object LaCusKgComiss: TLabel
      Left = 12
      Top = 116
      Width = 74
      Height = 13
      Caption = 'Comiss'#227'o $/kg:'
    end
    object LaCustoComiss: TLabel
      Left = 109
      Top = 116
      Width = 84
      Height = 13
      Caption = 'Comiss'#227'o $ Total:'
      Enabled = False
    end
    object SpeedButton2: TSpeedButton
      Left = 670
      Top = 32
      Width = 23
      Height = 22
      Caption = '...'
      OnClick = SpeedButton2Click
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 32
      Width = 62
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object CBGraGruX: TdmkDBLookupComboBox
      Left = 132
      Top = 32
      Width = 249
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_PRD_TAM_COR'
      ListSource = DsGraGruX
      TabOrder = 2
      dmkEditCB = EdGraGruX
      QryCampo = 'GraGruX'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdGraGruX: TdmkEditCB
      Left = 76
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'GraGruX'
      UpdCampo = 'GraGruX'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBGraGruX
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnFormActivate
    end
    object EdPecas: TdmkEdit
      Left = 696
      Top = 32
      Width = 84
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'Pecas'
      UpdCampo = 'Pecas'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdPecasKeyDown
      OnRedefinido = EdPecasRedefinido
    end
    object EdPesoKg: TdmkEdit
      Left = 784
      Top = 32
      Width = 92
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000'
      QryCampo = 'PesoKg'
      UpdCampo = 'PesoKg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdPesoKgKeyDown
      OnRedefinido = EdPesoKgRedefinido
    end
    object EdObserv: TdmkEdit
      Left = 516
      Top = 132
      Width = 485
      Height = 21
      TabOrder = 15
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Observ'
      UpdCampo = 'Observ'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdValorMP: TdmkEdit
      Left = 880
      Top = 32
      Width = 117
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ValorMP'
      UpdCampo = 'ValorMP'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = RecalculaRetTributos
      OnKeyDown = EdValorMPKeyDown
      OnRedefinido = EdValorMPRedefinido
    end
    object EdFicha: TdmkEdit
      Left = 592
      Top = 32
      Width = 77
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 3
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Pecas'
      UpdCampo = 'Pecas'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnKeyDown = EdFichaKeyDown
    end
    object EdSerieFch: TdmkEditCB
      Left = 384
      Top = 32
      Width = 33
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'SerieFch'
      UpdCampo = 'SerieFch'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBSerieFch
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnFormActivate
    end
    object CBSerieFch: TdmkDBLookupComboBox
      Left = 420
      Top = 32
      Width = 169
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsVSSerFch
      TabOrder = 4
      dmkEditCB = EdSerieFch
      QryCampo = 'SerieFch'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdMarca: TdmkEdit
      Left = 420
      Top = 132
      Width = 93
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 14
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Marca'
      UpdCampo = 'Marca'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdCusFrtAvuls: TdmkEdit
      Left = 228
      Top = 132
      Width = 92
      Height = 21
      Alignment = taRightJustify
      TabOrder = 12
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'CusFrtAvuls'
      UpdCampo = 'CusFrtAvuls'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdCusFrtAvulsRedefinido
    end
    object EdValorT: TdmkEdit
      Left = 324
      Top = 132
      Width = 92
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 13
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'ValorT'
      UpdCampo = 'ValorT'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnKeyDown = EdValorMPKeyDown
    end
    object GBRecuImpost: TGroupBox
      Left = 12
      Top = 56
      Width = 989
      Height = 57
      Caption = ' Recupera'#231#227'o de impostos: '
      TabOrder = 9
      object Panel13: TPanel
        Left = 2
        Top = 15
        Width = 985
        Height = 40
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 10
        object Label27: TLabel
          Left = 4
          Top = 0
          Width = 116
          Height = 13
          Caption = 'ICMS (cr'#233'dito % e valor):'
        end
        object Label28: TLabel
          Left = 156
          Top = 0
          Width = 107
          Height = 13
          Caption = 'PIS (cr'#233'dito % e valor):'
        end
        object Label33: TLabel
          Left = 308
          Top = 0
          Width = 129
          Height = 13
          Caption = 'COFINS (cr'#233'dito % e valor):'
        end
        object Label34: TLabel
          Left = 456
          Top = 0
          Width = 103
          Height = 13
          Caption = 'IPI (cr'#233'dito % e valor):'
        end
        object Label31: TLabel
          Left = 700
          Top = 0
          Width = 89
          Height = 13
          Caption = '$ Ret.Tributos MP:'
          Enabled = False
        end
        object Label30: TLabel
          Left = 604
          Top = 0
          Width = 91
          Height = 13
          Caption = '% Ret.Tributos MP:'
          Enabled = False
        end
        object EdRpICMS: TdmkEdit
          Left = 4
          Top = 15
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = RecalculaRetTributos
        end
        object EdRvICMS: TdmkEdit
          Left = 65
          Top = 15
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = RecalculaRetTributos
        end
        object EdRpPIS: TdmkEdit
          Left = 156
          Top = 15
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = RecalculaRetTributos
        end
        object EdRvPIS: TdmkEdit
          Left = 217
          Top = 15
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = RecalculaRetTributos
        end
        object EdRpCOFINS: TdmkEdit
          Left = 308
          Top = 15
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = RecalculaRetTributos
        end
        object EdRvCOFINS: TdmkEdit
          Left = 369
          Top = 15
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = RecalculaRetTributos
        end
        object EdRpIPI: TdmkEdit
          Left = 456
          Top = 15
          Width = 60
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = RecalculaRetTributos
        end
        object EdRvIPI: TdmkEdit
          Left = 517
          Top = 15
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = RecalculaRetTributos
        end
        object EdCredPereImposto: TdmkEdit
          Left = 604
          Top = 15
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 10
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000000000'
          QryCampo = 'CredPereImposto'
          UpdCampo = 'CredPereImposto'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdCredPereImpostoRedefinido
        end
        object EdCredValrImposto: TdmkEdit
          Left = 700
          Top = 15
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 9
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'CredValrImposto'
          UpdCampo = 'CredValrImposto'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdCredValrImpostoRedefinido
        end
      end
    end
    object EdCusKgComiss: TdmkEdit
      Left = 12
      Top = 131
      Width = 92
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      QryCampo = 'CusKgComiss'
      UpdCampo = 'CusKgComiss'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = EdCusKgComissChange
      OnRedefinido = EdCusKgComissRedefinido
    end
    object EdCustoComiss: TdmkEdit
      Left = 109
      Top = 131
      Width = 92
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 11
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'CustoComiss'
      UpdCampo = 'CustoComiss'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdCustoComissRedefinido
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 7
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 427
        Height = 32
        Caption = 'Item de Entrada de Peles In Natura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 427
        Height = 32
        Caption = 'Item de Entrada de Peles In Natura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 427
        Height = 32
        Caption = 'Item de Entrada de Peles In Natura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 473
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 8
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 571
        Height = 17
        Caption = 
          '$ kg MO: valor que ser'#225' recebido quando a empresa '#233' prestadora d' +
          'e MO para terceiros.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 571
        Height = 17
        Caption = 
          '$ kg MO: valor que ser'#225' recebido quando a empresa '#233' prestadora d' +
          'e MO para terceiros.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 517
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 269
    Width = 1008
    Height = 64
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GroupBox3: TGroupBox
      Left = 0
      Top = 0
      Width = 333
      Height = 64
      Align = alLeft
      Caption = ' Dados informados pelo fornecedor: '
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 329
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label13: TLabel
          Left = 12
          Top = 4
          Width = 33
          Height = 13
          Caption = 'Pe'#231'as:'
        end
        object Label14: TLabel
          Left = 108
          Top = 4
          Width = 42
          Height = 13
          Caption = 'Peso kg:'
        end
        object Label15: TLabel
          Left = 204
          Top = 4
          Width = 71
          Height = 13
          Caption = 'Valor total [F4]:'
        end
        object SpeedButton1: TSpeedButton
          Left = 300
          Top = 18
          Width = 25
          Height = 25
          Caption = '<'
          OnClick = SpeedButton1Click
        end
        object EdInfPecas: TdmkEdit
          Left = 12
          Top = 20
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'Pecas'
          UpdCampo = 'Pecas'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdInfPecasRedefinido
        end
        object EdInfPesoKg: TdmkEdit
          Left = 108
          Top = 20
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'PesoKg'
          UpdCampo = 'PesoKg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdInfPesoKgRedefinido
        end
        object EdInfValorT: TdmkEdit
          Left = 204
          Top = 20
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValorT'
          UpdCampo = 'ValorT'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnKeyDown = EdInfValorTKeyDown
          OnRedefinido = EdInfValorTRedefinido
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 333
      Top = 0
      Width = 180
      Height = 64
      Align = alLeft
      Caption = ' % de quebra de peso permitidas: '
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 176
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label16: TLabel
          Left = 12
          Top = 4
          Width = 38
          Height = 13
          Caption = 'Viagem:'
        end
        object Label17: TLabel
          Left = 84
          Top = 4
          Width = 18
          Height = 13
          Caption = 'Sal:'
        end
        object EdPerQbrViag: TdmkEdit
          Left = 12
          Top = 20
          Width = 68
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'PerQbrViag'
          UpdCampo = 'PerQbrViag'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdPerQbrViagRedefinido
        end
        object EdPerQbrSal: TdmkEdit
          Left = 84
          Top = 20
          Width = 68
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'PerQbrSal'
          UpdCampo = 'PerQbrSal'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdPerQbrSalRedefinido
        end
      end
    end
    object GroupBox6: TGroupBox
      Left = 513
      Top = 0
      Width = 120
      Height = 64
      Align = alLeft
      Caption = ' Sal: '
      TabOrder = 2
      object Panel9: TPanel
        Left = 2
        Top = 15
        Width = 116
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label18: TLabel
          Left = 12
          Top = 4
          Width = 60
          Height = 13
          Caption = 'Peso kg Sal:'
        end
        object EdPesoSalKg: TdmkEdit
          Left = 12
          Top = 20
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'PerQbrViag'
          UpdCampo = 'PerQbrViag'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdPesoSalKgRedefinido
        end
      end
    end
  end
  object Panel7: TPanel
    Left = 0
    Top = 411
    Width = 1008
    Height = 40
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object Label49: TLabel
      Left = 12
      Top = 3
      Width = 60
      Height = 13
      Caption = 'Localiza'#231#227'o:'
    end
    object Label50: TLabel
      Left = 410
      Top = 3
      Width = 74
      Height = 13
      Caption = 'Req.Mov.Estq.:'
      Color = clBtnFace
      ParentColor = False
    end
    object LaTribDefSel: TLabel
      Left = 508
      Top = 3
      Width = 131
      Height = 13
      Caption = 'Configura'#231#227'o de tributa'#231#227'o:'
    end
    object SbTribDefSel: TSpeedButton
      Left = 816
      Top = 19
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbTribDefSelClick
    end
    object Label25: TLabel
      Left = 840
      Top = 3
      Width = 50
      Height = 13
      Caption = '$ kg M.O.:'
    end
    object Label26: TLabel
      Left = 908
      Top = 3
      Width = 58
      Height = 13
      Caption = '$ total M.O.:'
    end
    object SbStqCenLoc: TSpeedButton
      Left = 389
      Top = 19
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbStqCenLocClick
    end
    object EdStqCenLoc: TdmkEditCB
      Left = 12
      Top = 19
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'StqCenLoc'
      UpdCampo = 'StqCenLoc'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBStqCenLoc
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnFormActivate
    end
    object CBStqCenLoc: TdmkDBLookupComboBox
      Left = 67
      Top = 19
      Width = 318
      Height = 21
      KeyField = 'Controle'
      ListField = 'NO_LOC_CEN'
      ListSource = DsStqCenLoc
      TabOrder = 1
      dmkEditCB = EdStqCenLoc
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdReqMovEstq: TdmkEdit
      Left = 411
      Top = 19
      Width = 93
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'ReqMovEstq'
      UpdCampo = 'ReqMovEstq'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdTribDefSel: TdmkEditCB
      Left = 508
      Top = 19
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'TribDefSel'
      UpdCampo = 'TribDefSel'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBTribDefSel
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBTribDefSel: TdmkDBLookupComboBox
      Left = 563
      Top = 19
      Width = 250
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsTribDefCab
      TabOrder = 4
      dmkEditCB = EdTribDefSel
      QryCampo = 'TribDefSel'
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdCustoMOKg: TdmkEdit
      Left = 840
      Top = 19
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      QryCampo = 'CustoMOKg'
      UpdCampo = 'CustoMOKg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnRedefinido = EdCustoMOKgRedefinido
    end
    object EdCustoMOTot: TdmkEdit
      Left = 908
      Top = 19
      Width = 92
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'CustoMOTot'
      UpdCampo = 'CustoMOTot'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object GroupBox5: TGroupBox
    Left = 0
    Top = 333
    Width = 1008
    Height = 78
    Align = alTop
    Caption = 'Restitui'#231#227'o $ do fornecedor:'
    TabOrder = 2
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 61
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox7: TGroupBox
        Left = 0
        Top = 0
        Width = 305
        Height = 61
        Align = alLeft
        Caption = ' Couro: '
        TabOrder = 0
        object Panel10: TPanel
          Left = 2
          Top = 15
          Width = 301
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label19: TLabel
            Left = 12
            Top = 4
            Width = 33
            Height = 13
            Caption = 'Pe'#231'as:'
          end
          object Label20: TLabel
            Left = 108
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Peso kg:'
          end
          object Label21: TLabel
            Left = 204
            Top = 4
            Width = 71
            Height = 13
            Caption = 'Valor total [F4]:'
          end
          object EdRstCouPc: TdmkEdit
            Left = 12
            Top = 20
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Pecas'
            UpdCampo = 'Pecas'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdRstCouPcRedefinido
          end
          object EdRstCouKg: TdmkEdit
            Left = 108
            Top = 20
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PesoKg'
            UpdCampo = 'PesoKg'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdRstCouKgRedefinido
          end
          object EdRstCouVl: TdmkEdit
            Left = 204
            Top = 20
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'ValorT'
            UpdCampo = 'ValorT'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdRstCouVlRedefinido
          end
        end
      end
      object GroupBox8: TGroupBox
        Left = 305
        Top = 0
        Width = 180
        Height = 61
        Align = alLeft
        Caption = 'Sal:'
        TabOrder = 1
        object Panel11: TPanel
          Left = 2
          Top = 15
          Width = 176
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label22: TLabel
            Left = 12
            Top = 4
            Width = 60
            Height = 13
            Caption = 'Peso kg Sal:'
          end
          object Label23: TLabel
            Left = 84
            Top = 4
            Width = 71
            Height = 13
            Caption = 'Valor total [F4]:'
          end
          object EdRstSalKg: TdmkEdit
            Left = 12
            Top = 20
            Width = 68
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PerQbrViag'
            UpdCampo = 'PerQbrViag'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdRstSalKgRedefinido
          end
          object EdRstSalVl: TdmkEdit
            Left = 84
            Top = 20
            Width = 68
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'PerQbrSal'
            UpdCampo = 'PerQbrSal'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnRedefinido = EdRstSalVlRedefinido
          end
        end
      end
      object GroupBox9: TGroupBox
        Left = 485
        Top = 0
        Width = 120
        Height = 61
        Align = alLeft
        Caption = 'Total: '
        Enabled = False
        TabOrder = 2
        object Panel12: TPanel
          Left = 2
          Top = 15
          Width = 116
          Height = 44
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label24: TLabel
            Left = 12
            Top = 4
            Width = 50
            Height = 13
            Caption = 'Valor total:'
          end
          object EdRstTotVl: TdmkEdit
            Left = 12
            Top = 20
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'PerQbrViag'
            UpdCampo = 'PerQbrViag'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
    end
  end
  object QrGraGruX: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, '
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), '
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) '
      'NO_PRD_TAM_COR, unm.Sigla SIGLAUNIDMED, '
      'unm.CodUsu CODUSUUNIDMED, unm.Nome NOMEUNIDMED'
      'FROM vsnatcad wmp'
      'LEFT JOIN gragrux ggx ON ggx.Controle=wmp.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ')
    Left = 680
    Top = 65532
    object QrGraGruXGraGru1: TIntegerField
      FieldName = 'GraGru1'
    end
    object QrGraGruXControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraGruXNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 157
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGruXCouNiv2: TFloatField
      FieldName = 'CouNiv2'
    end
    object QrGraGruXGrandeza: TFloatField
      FieldName = 'Grandeza'
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 680
    Top = 44
  end
  object QrVSSerFch: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM vsserfch'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 752
    Top = 65532
    object QrVSSerFchCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVSSerFchNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsVSSerFch: TDataSource
    DataSet = QrVSSerFch
    Left = 752
    Top = 44
  end
  object QrStqCenLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM stqcenloc scl'
      'ORDER BY Nome')
    Left = 520
    Top = 65532
    object QrStqCenLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrStqCenLocNO_LOC_CEN: TWideStringField
      DisplayWidth = 120
      FieldName = 'NO_LOC_CEN'
      Required = True
      Size = 120
    end
  end
  object DsStqCenLoc: TDataSource
    DataSet = QrStqCenLoc
    Left = 520
    Top = 44
  end
  object QrTribDefCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM tribdefcab'
      'WHERE Ativo=1'
      'ORDER BY Nome')
    Left = 604
    Top = 65532
    object QrTribDefCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTribDefCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTribDefCab: TDataSource
    DataSet = QrTribDefCab
    Left = 604
    Top = 44
  end
  object QrGraGruXCou: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cn1.Nome NO_CouNiv1, '
      'cn2.Nome NO_CouNiv2, gxc.* '
      'FROM gragruxcou gxc'
      'LEFT JOIN couniv1 cn1 ON cn1.Codigo=gxc.CouNiv1'
      'LEFT JOIN couniv2 cn2 ON cn2.Codigo=gxc.CouNiv2'
      'WHERE gxc.GraGruX=1')
    Left = 660
    Top = 304
    object QrGraGruXCouPrecoMin: TFloatField
      FieldName = 'PrecoMin'
    end
    object QrGraGruXCouPrecoMax: TFloatField
      FieldName = 'PrecoMax'
    end
  end
end
