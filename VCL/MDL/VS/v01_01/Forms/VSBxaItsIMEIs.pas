unit VSBxaItsIMEIs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, mySQLDbTables, dmkDBGridZTO,
  Vcl.Menus, UnProjGroup_Consts, dmkEditCB, dmkDBLookupComboBox, dmkEdit,
  UnAppEnums;

type
  TFmVSBxaItsIMEIs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrGraGruY: TmySQLQuery;
    QrGraGruYCodigo: TIntegerField;
    QrGraGruYTabela: TWideStringField;
    QrGraGruYNome: TWideStringField;
    QrGraGruYOrdem: TIntegerField;
    DsGraGruY: TDataSource;
    QrIMEIs: TmySQLQuery;
    DsIMEIs: TDataSource;
    QrIMEIsNO_PRD_TAM_COR: TWideStringField;
    QrIMEIsCodigo: TIntegerField;
    QrIMEIsControle: TIntegerField;
    QrIMEIsMovimCod: TIntegerField;
    QrIMEIsMovimNiv: TIntegerField;
    QrIMEIsMovimTwn: TIntegerField;
    QrIMEIsEmpresa: TIntegerField;
    QrIMEIsTerceiro: TIntegerField;
    QrIMEIsCliVenda: TIntegerField;
    QrIMEIsMovimID: TIntegerField;
    QrIMEIsLnkNivXtr1: TIntegerField;
    QrIMEIsLnkNivXtr2: TIntegerField;
    QrIMEIsDataHora: TDateTimeField;
    QrIMEIsPallet: TIntegerField;
    QrIMEIsGraGruX: TIntegerField;
    QrIMEIsPecas: TFloatField;
    QrIMEIsPesoKg: TFloatField;
    QrIMEIsAreaM2: TFloatField;
    QrIMEIsAreaP2: TFloatField;
    QrIMEIsValorT: TFloatField;
    QrIMEIsSrcMovID: TIntegerField;
    QrIMEIsSrcNivel1: TIntegerField;
    QrIMEIsSrcNivel2: TIntegerField;
    QrIMEIsSrcGGX: TIntegerField;
    QrIMEIsSdoVrtPeca: TFloatField;
    QrIMEIsSdoVrtPeso: TFloatField;
    QrIMEIsSdoVrtArM2: TFloatField;
    QrIMEIsObserv: TWideStringField;
    QrIMEIsSerieFch: TIntegerField;
    QrIMEIsFicha: TIntegerField;
    QrIMEIsMisturou: TSmallintField;
    QrIMEIsFornecMO: TIntegerField;
    QrIMEIsCustoMOKg: TFloatField;
    QrIMEIsCustoMOTot: TFloatField;
    QrIMEIsValorMP: TFloatField;
    QrIMEIsDstMovID: TIntegerField;
    QrIMEIsDstNivel1: TIntegerField;
    QrIMEIsDstNivel2: TIntegerField;
    QrIMEIsDstGGX: TIntegerField;
    QrIMEIsQtdGerPeca: TFloatField;
    QrIMEIsQtdGerPeso: TFloatField;
    QrIMEIsQtdGerArM2: TFloatField;
    QrIMEIsQtdGerArP2: TFloatField;
    QrIMEIsQtdAntPeca: TFloatField;
    QrIMEIsQtdAntPeso: TFloatField;
    QrIMEIsQtdAntArM2: TFloatField;
    QrIMEIsQtdAntArP2: TFloatField;
    QrIMEIsAptoUso: TSmallintField;
    QrIMEIsNotaMPAG: TFloatField;
    QrIMEIsMarca: TWideStringField;
    QrIMEIsLk: TIntegerField;
    QrIMEIsDataCad: TDateField;
    QrIMEIsDataAlt: TDateField;
    QrIMEIsUserCad: TIntegerField;
    QrIMEIsUserAlt: TIntegerField;
    QrIMEIsAlterWeb: TSmallintField;
    QrIMEIsAtivo: TSmallintField;
    QrIMEIsTpCalcAuto: TIntegerField;
    QrIMEIsGraGruY: TIntegerField;
    CkContinuar: TCheckBox;
    QrIMEIsMediaSdoVrtArM2: TFloatField;
    QrIMEIsFaixaMediaM2: TFloatField;
    Label1: TLabel;
    EdSenha: TEdit;
    Panel5: TPanel;
    Splitter1: TSplitter;
    DBGGraGruY: TdmkDBGridZTO;
    Panel6: TPanel;
    RGPosiNega: TRadioGroup;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    BtProvisorio: TBitBtn;
    PMProvisorio: TPopupMenu;
    ObterIMEIs11: TMenuItem;
    QrIMEIsVSMulFrnCab: TIntegerField;
    QrIMEIsClientMO: TIntegerField;
    Panel7: TPanel;
    DBGIMEIs: TdmkDBGridZTO;
    Panel8: TPanel;
    QrGraGruX: TmySQLQuery;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrGraGruXSIGLAUNIDMED: TWideStringField;
    QrGraGruXCODUSUUNIDMED: TIntegerField;
    QrGraGruXNOMEUNIDMED: TWideStringField;
    DsGraGruX: TDataSource;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    DsStqCenCad: TDataSource;
    QrStqCenLoc: TmySQLQuery;
    QrStqCenLocControle: TIntegerField;
    QrStqCenLocNO_LOC_CEN: TWideStringField;
    DsStqCenLoc: TDataSource;
    QrCouNiv1: TmySQLQuery;
    QrCouNiv1Codigo: TIntegerField;
    QrCouNiv1Nome: TWideStringField;
    DsCouNiv1: TDataSource;
    QrCouNiv2: TmySQLQuery;
    QrCouNiv2Codigo: TIntegerField;
    QrCouNiv2Nome: TWideStringField;
    DsCouNiv2: TDataSource;
    GBDadosItem1: TGroupBox;
    Label6: TLabel;
    Label2: TLabel;
    Label53: TLabel;
    Label49: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    EdControle: TdmkEdit;
    CBGraGruX: TdmkDBLookupComboBox;
    EdGraGruX: TdmkEditCB;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    EdStqCenLoc: TdmkEditCB;
    CBStqCenLoc: TdmkDBLookupComboBox;
    EdCouNiv2: TdmkEditCB;
    EdCouNiv1: TdmkEditCB;
    CBCouNiv1: TdmkDBLookupComboBox;
    CBCouNiv2: TdmkDBLookupComboBox;
    QrIMEIsStqCenLoc: TIntegerField;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGGraGruYAfterMultiselect(aSender: TObject;
      aMultiSelectEventTrigger: TMultiSelectEventTrigger);
    procedure BtOKClick(Sender: TObject);
    procedure DBGIMEIsDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure RGPosiNegaClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure ObterIMEIs11Click(Sender: TObject);
    procedure BtProvisorioClick(Sender: TObject);
    procedure QrStqCenCadAfterScroll(DataSet: TDataSet);
    procedure EdControleRedefinido(Sender: TObject);
    procedure EdGraGruXRedefinido(Sender: TObject);
    procedure EdStqCenCadRedefinido(Sender: TObject);
    procedure EdStqCenLocChange(Sender: TObject);
    procedure EdCouNiv2Redefinido(Sender: TObject);
    procedure EdCouNiv1Redefinido(Sender: TObject);
  private
    { Private declarations }
    procedure BaixaIMEIAtual();
    procedure ReopenIMEIs();
    procedure SetaTodos(Ativo: Boolean);
  public
    { Public declarations }
    FControle: Integer;
    FCodigo, FMovimCod, FEmpresa: Integer;
  end;

  var
  FmVSBxaItsIMEIs: TFmVSBxaItsIMEIs;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnVS_PF, ModuleGeral, UMySQLModule,
  UnGrade_PF, UnVS_CRC_PF;

{$R *.DFM}

procedure TFmVSBxaItsIMEIs.BaixaIMEIAtual();
const
  LnkNivXtr1 = 0;
  LnkNivXtr2 = 0;
  CliVenda   = 0;
  MovimTwn   = 0;
  DstMovID   = TEstqMovimID(0);
  DstNivel1  = 0;
  DstNivel2  = 0;
  DstGGX     = 0;
  CustoMOKg  = 0;
  CustoMOM2  = 0;
  CustoMOTot = 0;
  ValorMP    = 0;
  QtdGerPeca = 0;
  QtdGerPeso = 0;
  QtdGerArM2 = 0;
  QtdGerArP2 = 0;
  AptoUso    = 0;
  NotaMPAG   = 0;
  //
  TpCalcAuto = -1;
  //
  EdFicha  = nil;
  EdPallet = nil;
  ExigeFornecedor = False;
  //
  PedItsLib  = 0;
  PedItsFin  = 0;
  PedItsVda  = 0;
  //
  GSPSrcMovID = TEstqMovimID(0);
  GSPSrcNiv2 = 0;
  //
  ReqMovEstq  = 0;
  //
  ItemNFe     = 0;
  //
  QtdAntPeca = 0;
  QtdAntPeso = 0;
  QtdAntArM2 = 0;
  QtdAntArP2 = 0;
var
  DataHora, Observ, Marca: String;
  Pallet, Codigo, Controle, MovimCod, Empresa, GraGruX, Terceiro, SerieFch,
  Ficha, SrcNivel1, SrcNivel2, (*Misturou,*) GraGruY, SrcGGX,
  VSMulFrnCab, ClientMO, FornecMO, StqCenLoc: Integer;
  Pecas, PesoKg, AreaM2, AreaP2, ValorT: Double;
  SrcMovID, MovimID: TEstqMovimID;
  MovimNiv: TEstqMovimNiv;
  ExigeAreaouPeca: Boolean;
  Qry: TmySQLQuery;
begin
  SrcMovID       := TEstqmovimID(QrIMEIsMovimID.Value);
  SrcNivel1      := QrIMEIsCodigo.Value;
  SrcNivel2      := QrIMEIsControle.Value;
  SrcGGX         := QrIMEIsGraGruX.Value;
  //
  Codigo         := FCodigo;
  Controle       := 0;
  MovimCod       := FMovimCod;
  Empresa        := FEmpresa;
  ClientMO       := QrIMEIsClientMO.Value;
  FornecMO       := QrIMEIsFornecMO.Value;
  StqCenLoc      := QrIMEIsStqCenLoc.Value;
  Terceiro       := QrIMEIsTerceiro.Value;
  VSMulFrnCab    := QrIMEIsVSMulFrnCab.Value;
  DataHora       := Geral.FDT(DModG.ObtemAgora(), 109);
  MovimID        := emidForcado;
  MovimNiv       := eminSemNiv;
  Pallet         := QrIMEIsPallet.Value;
  GraGruX        := QrIMEIsGraGruX.Value;
  Pecas          := -QrIMEIsSdoVrtPeca.Value;
  PesoKg         := -QrIMEIsSdoVrtPeso.Value;
  AreaM2         := -QrIMEIsSdoVrtArM2.Value;
  AreaP2         := Geral.ConverteArea(AreaM2, ctM2toP2, cfQuarto);
  ValorT         := 0;
  if QrIMEIsAreaM2.Value > 0 then
    ValorT := AreaM2 / QrIMEIsAreaM2.Value * QrIMEIsValorT.Value
  else
  if QrIMEIsPecas.Value > 0 then
    ValorT := Pecas / QrIMEIsPecas.Value * QrIMEIsValorT.Value
  else
  if QrIMEIsPesoKg.Value > 0 then
    ValorT := PesoKg / QrIMEIsPesoKg.Value * QrIMEIsValorT.Value;
  Observ         := '';
  //
  SerieFch       := QrIMEIsSerieFch.Value;
  Ficha          := QrIMEIsFicha.Value;
  Marca          := QrIMEIsMarca.Value;
  //Misturou       := QrIMEIsMisturou.Value;
  //
  GraGruY        := QrIMEIsGraGruY.Value;
  //
(*
  ExigeAreaouPeca :=
    (QrIMEIsSdoVrtPeso.Value > 0) or (QrIMEIsSdoVrtArM2.Value > 0);
  //
  if VS_PF.VSFic(GraGruX, Empresa, Terceiro, Pallet, Ficha, Pecas,
  AreaM2, PesoKg, ValorT, EdGraGruX, EdPallet, EdFicha, EdPecas,
  EdAreaM2, EdPesoKg, EdValorT, ExigeFornecedor, GraGruY, ExigeAreaouPeca) then
    Exit;
*)
  //
  //Controle := UMyMod.BPGS1I32(CO_TAB_TAB_VMI, 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if not VS_PF.ObtemControleIMEI(ImgTipo.SQLType, Controle, EdSenha.Text) then
  begin
    Close;
    Exit;
  end;
(*
  if Dmod.InsUpdVSMovIts_(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn, Empresa, Terceiro,
  MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2, AreaP2, ValorT,
  DataHora, SrcMovID, SrcNivel1, SrcNivel2, Observ, LnkNivXtr1, LnkNivXtr2,
  CliVenda, Controle) then
*)
  if VS_PF.InsUpdVSMovIts3(ImgTipo.SQLType, Codigo, MovimCod, MovimTwn,
  Empresa, Terceiro, MovimID, MovimNiv, Pallet, GraGruX, Pecas, PesoKg, AreaM2,
  AreaP2, ValorT, DataHora, TEstqMovimID(SrcMovID), SrcNivel1, SrcNivel2,
  Observ, LnkNivXtr1, LnkNivXtr2, CliVenda, Controle, Ficha, (*Misturou,*)
  CO_CustoMOPc_ZERO, CustoMOKg, CustoMOM2, CustoMOTot, ValorMP, DstMovID, DstNivel1, DstNivel2, QtdGerPeca,
  QtdGerPeso, QtdGerArM2, QtdGerArP2, AptoUso, FornecMO, SerieFch,
  NotaMPAG, SrcGGX, DstGGX, Marca,
  TpCalcAuto, PedItsLib, PedItsFin, PedItsVda, GSPSrcMovID, GSPSrcNiv2,
  ReqMovEstq, StqCenLoc, ItemNFe, VSMulFrnCab, ClientMO,
  QtdAntPeca, QtdAntPeso, QtdAntArM2, QtdAntArP2,
  CO_0_PerceComiss, CO_0_CusKgComiss, CO_0_CustoComiss, CO_0_CredPereImposto, CO_0_CredValrImposto, CO_0_CusFrtAvuls,
  CO_0_GGXRcl,
  CO_RpICMS, CO_RpPIS, CO_RpCOFINS, CO_RvICMS, CO_RvPIS, CO_RvCOFINS, CO_RpIPI, CO_RvIPI,
  CO_0_JmpMovID,
  CO_0_JmpNivel1,
  CO_0_JmpNivel2,
  CO_0_JmpGGX,
  CO_0_RmsMovID,
  CO_0_RmsNivel1,
  CO_0_RmsNivel2,
  CO_0_RmsGGX,
  CO_0_GSPJmpMovID,
  CO_0_GSPJmpNiv2,
  CO_0_MovCodPai,
  CO_0_VmiPai,
  CO_0_IxxMovIX,
  CO_0_IxxFolha,
  CO_0_IxxLinha,
  CO_TRUE_ExigeClientMO,
  CO_TRUE_ExigeFornecMO,
  CO_TRUE_ExigeStqLoc,
  iuvpei058(*Baixa for�ada de couro na ribeira 2/2*)) then
  begin
    VS_PF.AtualizaTotaisVSXxxCab('vsbxacab', MovimCod);
    VS_PF.AtualizaSaldoIMEI(SrcNivel2, False);
    FControle := Controle;
  end;
end;

procedure TFmVSBxaItsIMEIs.BtNenhumClick(Sender: TObject);
begin
  SetaTodos(False);
end;

procedure TFmVSBxaItsIMEIs.BtOKClick(Sender: TObject);
var
  I, N, K: Integer;
  InitialTick, DifTick, FaltaTick: DWORD;
  sN, str: String;
begin
  N := DBGIMEIs.SelectedRows.Count;
  if (N > 0) then
  begin
    try
      PB1.Position;
      PB1.Max := N;
      InitialTick := GetTickCount;
      sN := ' de ' + Geral.FF0(N) + '. Tempo transcorrido: ';
      for I := 0 to N - 1 do
      begin
        //QrIMEIs.GotoBookmark(pointer(DBGIMEIs.SelectedRows.Items[I]));
        QrIMEIs.GotoBookmark(DBGIMEIs.SelectedRows.Items[I]);
        //
        BaixaIMEIAtual();
        DifTick := GetTickCount - InitialTick;
        K := N - I + 1;
        FaltaTick := Trunc(DifTick / (I + 1) * K);
        str := IntToStr(I+1) + sN + FormatDateTime('h:nn:ss', DifTick / 86400000) +
        '  Tempo restante aproximado: ' + FormatDateTime('h:nn:ss', FaltaTick / 86400000);
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, str);
      end;
    finally
      ReopenIMEIs();
    end;
    if not CkContinuar.Checked then
      Close;
  end else
    Geral.MB_Aviso('Nenhum IME-I foi selecionado!');
end;

procedure TFmVSBxaItsIMEIs.BtProvisorioClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProvisorio, BtProvisorio);
end;

procedure TFmVSBxaItsIMEIs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVSBxaItsIMEIs.BtTodosClick(Sender: TObject);
begin
  SetaTodos(True);
end;

procedure TFmVSBxaItsIMEIs.DBGGraGruYAfterMultiselect(aSender: TObject;
  aMultiSelectEventTrigger: TMultiSelectEventTrigger);
begin
  ReopenIMEIs();
  GBDadosItem1.Visible := True;
end;

procedure TFmVSBxaItsIMEIs.DBGIMEIsDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
var
  CorTexto, CorFundo: TColor;
  Texto: String;
begin
  // Problemas de desenha
(*
  if (Column.FieldName = 'MediaSdoVrtArM2') then
  begin
    if QrIMEIsMediaSdoVrtArM2.Value <= 0 then
    begin
      Cor := clRed;
      Font.Style := [fsBold];
    end else
    begin
      Cor := clBlack;
      Font.Style := [];
    end;
    MyObjects.DesenhaTextoEmDBGrid(TDBGrid(DBGIMEIs), Rect, cor, clWindow,
      Column.Alignment, Column.Field.DisplayText);
  end
  else
*)
(*
  if (Column.FieldName = 'SdoVrtArM2') then
  begin
    if QrIMEIsSdoVrtArM2.Value <= 0 then
    begin
      Cor := clRed;
      Font.Style := [fsBold];
    end else
    begin
      Cor := clBlack;
      Font.Style := [];
    end;
    MyObjects.DesenhaTextoEmDBGrid(TDBGrid(DBGIMEIs), Rect, cor, clWindow,
      Column.Alignment, Column.Field.DisplayText);
  end else
*)
  if Column.FieldName = 'FaixaMediaM2' then
  begin
    VS_CRC_PF.DadosDaFaixaDaMediaM2DoCouro(QrIMEIsFaixaMediaM2.Value,
    CorFundo, CorTexto, Texto);
    MyObjects.DesenhaTextoEmDBGrid(TDBGrid(DBGIMEIs), Rect, CorTexto, CorFundo,
      Column.Alignment, Texto(*Column.Field.DisplayText*));
  end;
end;

procedure TFmVSBxaItsIMEIs.EdControleRedefinido(Sender: TObject);
begin
  ReopenIMEIs();
end;

procedure TFmVSBxaItsIMEIs.EdCouNiv1Redefinido(Sender: TObject);
begin
  ReopenIMEIs();
end;

procedure TFmVSBxaItsIMEIs.EdCouNiv2Redefinido(Sender: TObject);
begin
  ReopenIMEIs();
end;

procedure TFmVSBxaItsIMEIs.EdGraGruXRedefinido(Sender: TObject);
begin
  ReopenIMEIs();
end;

procedure TFmVSBxaItsIMEIs.EdStqCenCadRedefinido(Sender: TObject);
begin
  ReopenIMEIs();
end;

procedure TFmVSBxaItsIMEIs.EdStqCenLocChange(Sender: TObject);
begin
  ReopenIMEIs();
end;

procedure TFmVSBxaItsIMEIs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmVSBxaItsIMEIs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrGraGruY, Dmod.MyDB);
  FControle := 0;
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  VS_PF.AbreGraGruXY(QrGraGruX, '');
  UnDmkDAC_PF.AbreQuery(QrCouNiv1, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCouNiv2, Dmod.MyDB);
end;

procedure TFmVSBxaItsIMEIs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVSBxaItsIMEIs.ObterIMEIs11Click(Sender: TObject);
var
  IMEIs: String;
begin
  IMEIs := '';
  QrIMEIS.First;
  while not QrIMEIS.Eof do
  begin
    IMEIs := IMEIs + Geral.FF0(QrIMEIsControle.Value) + ',';
    //
    QrIMEIs.Next;
  end;
  Geral.MB_Info(IMEIs);
end;

procedure TFmVSBxaItsIMEIs.QrStqCenCadAfterScroll(DataSet: TDataSet);
begin
  Grade_PF.ReopenStqCenLoc(QrStqCenLoc, QrStqCenCadCodigo.Value, 0);
end;

procedure TFmVSBxaItsIMEIs.ReopenIMEIs();
var
  I: Integer;
  GraGruYs, Sinal: String;
var
  Data: TDateTime;
  //Pallet, Ficha, IMEI,
  GraGruX, StqCenCad, StqCenloc, ImeiSrc, CouNiv1, CouNiv2: Integer;
  //SQL_GraGruX, SQL_Pallet, SQL_Ficha, SQL_IMEI: String;
var
  //SQL_Empresa, SQL_Pallet, SQL_Ficha, SQL_GraGruY, SQL_ImeiSrc, SQL_DataLimite, SQL_IMEI,
  SQL_GraGruYs, SQL_GraGruX, SQL_StqCenCad, SQL_StqCenLoc, SQL_CouNiv1,
  SQL_CouNiv2: String;
begin
  (*
  SQL_Pallet  := '';
  SQL_Ficha   := '';
  SQL_IMEI    := '';
  SQL_ImeiSrc   := '';
  SQL_DataLimite := '';
  *)
  SQL_GraGruYs := '';
  SQL_StqCenCad := '';
  SQL_StqCenLoc := '';
  SQL_CouNiv1 := '';
  SQL_CouNiv2 := '';

  //begin
  GraGruYs := '';
  if (DBGGraGruY.SelectedRows.Count > 0) and
  (DBGGraGruY.SelectedRows.Count < QrGraGruY.RecordCount) then
  begin
    for I := 0 to DBGGraGruY.SelectedRows.Count - 1 do
    begin
      //QrGraGruY.GotoBookmark(pointer(DBGGraGruY.SelectedRows.Items[I]));
      QrGraGruY.GotoBookmark(DBGGraGruY.SelectedRows.Items[I]);
      //
      if GraGruYs <> '' then
        GraGruYs := GraGruYs + ',';
      GraGruYs := GraGruYs + Geral.FF0(QrGraGruYCodigo.Value);
    end;
  end;
  if Trim(GraGruYs) <> '' then
    SQL_GraGruYs := 'AND ggx.GraGruY IN (' + GraGruYs + ') ';
  //
  (*
  Pallet     := EdPsqPallet.ValueVariant;
  Ficha      := EdPsqFicha.ValueVariant;
  IMEI       := EdPsqIMEI.ValueVariant;
  ImeiSrc    := EdImeiSrc.ValueVariant;
  *)
  StqCenCad  := EdStqCenCad.ValueVariant;
  StqCenLoc  := EdStqCenLoc.ValueVariant;
  CouNiv2    := EdCouNiv2.ValueVariant;
  CouNiv1    := EdCouNiv1.ValueVariant;
  GraGruX    := EdGraGruX.ValueVariant;
  //
(*
  if Pallet <> 0 then
    SQL_Pallet := 'AND vmi.Pallet=' + Geral.FF0(Pallet);
  if Ficha <> 0 then
    SQL_Ficha  := 'AND vmi.Ficha=' + Geral.FF0(Ficha);
  if IMEI <> 0 then
    SQL_IMEI  := 'AND vmi.Controle=' + Geral.FF0(IMEI);
  if ImeiSrc <> 0 then
    SQL_ImeiSrc := Geral.ATS([
    'AND vmi.MovimCod=( ',
    '  SELECT MovimCod ',
    '  FROM ' + CO_SEL_TAB_VMI + ' ',
    '  WHERE Controle=' + Geral.FF0(ImeiSrc),
    ') ']);
  if DataLimite > 0 then
    SQL_DataLimite := 'AND vmi.DataHora < + "' + Geral.FDT(DataLimite + 1, 1) + '"';
*)
  if GraGruX <> 0 then
    SQL_GraGruX := 'AND vmi.GraGruX=' + Geral.FF0(GraGruX);
  if StqCenCad <> 0 then
    SQL_StqCenCad := 'AND scc.Codigo=' + Geral.FF0(StqCenCad);
  if StqCenLoc <> 0 then
    SQL_StqCenLoc := 'AND vmi.StqCenLoc=' + Geral.FF0(StqCenLoc);
  if CouNiv1 <> 0 then
    SQL_CouNiv1 := 'AND xco.CouNiv1=' + Geral.FF0(CouNiv1);
  if CouNiv2 <> 0 then
    SQL_CouNiv2 := 'AND xco.CouNiv2=' + Geral.FF0(CouNiv2);
  //

  if RGPosiNega.ItemIndex = 0 then
    Sinal := '>'
  else
    Sinal := '<';
  UnDmkDAC_PF.AbreMySQLQuery0(QrIMEIs, Dmod.MyDB, [
  'SELECT vmi.SdoVrtArM2 / vmi.SdoVrtPeca  MediaSdoVrtArM2, ',
  'CASE ',
  'WHEN vmi.SdoVrtArM2 / vmi.SdoVrtPeca < cou.MediaMinM2 THEN 0.000 ',
  'WHEN vmi.SdoVrtArM2 / vmi.SdoVrtPeca > cou.MediaMaxM2 THEN 2.000 ',
  'ELSE 1.000 END FaixaMediaM2, ',
  'CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR, ggx.GraGruY, vmi.* ',
  'FROM ' + CO_SEL_TAB_VMI + ' vmi ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=vmi.GraGruX ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragruxcou cou ON cou.GraGruX=ggx.Controle ',
  'LEFT JOIN stqcenloc  scl ON scl.Controle=vmi.StqCenLoc ',
  'LEFT JOIN stqcencad  scc ON scc.Codigo=scl.Codigo ',
  // ini 2023-11-24
  //'WHERE vmi.SdoVrtPeca' + Sinal + '0 ',
  'WHERE (vmi.SdoVrtPeca' + Sinal + '0 OR (vmi.Pecas=0 AND vmi.SdoVrtPeso' + Sinal + '0))',
  // fim 2023-11-24
  'AND vmi.Empresa=' + Geral.FF0(FEmpresa),
  SQL_GraGruYs,
  SQL_GraGruX,
  SQL_StqCenCad,
  SQL_StqCenLoc,
  SQL_CouNiv1,
  SQL_CouNiv2,
  'ORDER BY vmi.Controle ',
  '']);
  //
  //Geral.MB_Teste(QrIMEIs.SQL.Text);
end;

procedure TFmVSBxaItsIMEIs.RGPosiNegaClick(Sender: TObject);
begin
  ReopenIMEIs();
  BtTodos.Enabled := RGPosiNega.ItemIndex = 1;
  BtNenhum.Enabled := RGPosiNega.ItemIndex = 1;
end;

procedure TFmVSBxaItsIMEIs.SetaTodos(Ativo: Boolean);
begin
  if Ativo then
    MyObjects.DBGridSelectAll(TDBGrid(DBGIMEIs))
  else
    DBGIMEIs.SelectedRows.Clear;
end;

end.
